import { api, LightningElement, track } from 'lwc';
import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();

export default class OrderEntryTvProductForm extends LightningElement {
	@track isRendered = true;

	@api products = [];
	@api addons = [];

	@api validate() {
		const productInput = this.template.querySelector('lightning-combobox');
		productInput.reportValidity();
		return productInput.checkValidity();
	}
	@track _selectedProduct;

	@api
	get selectedProduct() {
		return this._selectedProduct;
	}
	set selectedProduct(value) {
		this._selectedProduct = Object.assign({}, value);
		this.selectedProductId = this.selectedProduct?.id;
	}

	connectedCallback() {
		this.isRendered = false;
	}

	renderedCallback() {
		if (this.isRendered || this.products.find((el) => el.Id === this.selectedProduct.id) === undefined) {
			return;
		}
		this.isRendered = true;
		this.dispatchEvent(new CustomEvent('rendered', { detail: true, bubbles: false }));
	}

	/**
	 * Get list of formated products for picklist
	 */
	get formattedProducts() {
		return this.products.map((p) => ({
			value: p.Id,
			label: p.Name
		}));
	}

	@track selectedProductId;

	/**
	 * Handle change on product picklist
	 */
	handleProductChange(e) {
		if (e.detail.value !== this.selectedProduct.id) {
			this.selectedProduct.id = e.detail.value;
			this.selectProduct(e.detail.value);
		}
	}

	/**
	 * Set selected product and dispatch to parent components
	 */
	selectProduct(productId) {
		this.dispatchEvent(new CustomEvent('selectedproduct', { detail: productId, bubbles: false }));
	}
}
