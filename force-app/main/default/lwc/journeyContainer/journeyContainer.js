import { LightningElement, track, api, wire } from 'lwc';
import { publish, subscribe, MessageContext } from 'lightning/messageService';
import { NavigationMixin } from 'lightning/navigation';
import JOURNEY_MESSAGE_CHANNEL from '@salesforce/messageChannel/JourneyMessages__c';
import getJourneySteps from '@salesforce/apex/JourneyContainerController.getJourneySteps';
import updateJorneySteps from '@salesforce/apex/JourneyContainerController.updateJorneySteps';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class JourneyContainer extends NavigationMixin(LightningElement) {
	@wire(MessageContext)
	messageContext;
	@api opportunityId;
	@track waitResponse = false;
	@track payload = { journeySteps: [{ label: '', value: '', isActive: true }] };
	@track isRendered = true;

	@api
	get currentStepIndex() {
		return this.payload.journeySteps.indexOf(this.payload.journeySteps.find((el) => el.isActive === true));
	}
	@api
	get currentStepValue() {
		return this.payload.journeySteps.find((el) => el.isActive === true).value;
	}
	@api
	get addMobileProductsVisible() {
		return this.payload.journeySteps.find((el) => el.isActive === true).value === 'Add Mobile Products';
	}
	@api
	get banSelectroVisible() {
		return this.payload.journeySteps.find((el) => el.isActive === true).value === 'Billing Details';
	}
	@api
	get ctnInformationVisible() {
		return this.payload.journeySteps.find((el) => el.isActive === true).value === 'CTN Details';
	}
	@api
	get buttonNextFinishName() {
		return this.currentStepIndex + 1 !== this.payload.journeySteps.length ? 'Next' : 'Finish';
	}
	@api
	get hidePreviousButton() {
		return this.currentStepIndex === undefined || this.currentStepIndex === null || this.currentStepIndex === 0 ? false : true;
	}

	async connectedCallback() {
		this.payload = await getJourneySteps({
			oppId: this.opportunityId
		})
			.then((result) => {
				return JSON.parse(result);
			})
			.catch(() => {
				return JSON.parse({});
			});
		this.subscribeToMessageChannel();

		this.isRendered = false;
	}

	renderedCallback() {
		if (this.isRendered) {
			return;
		}
		this.isRendered = true;
		let index = 0;

		this.payload.journeySteps.forEach((el) => {
			index++;
			let style = document.createElement('style');
			style.innerText =
				index < this.payload.journeySteps.length
					? '.slds-progress__item:nth-child(' +
					  index +
					  ')::before{content: "' +
					  el.label +
					  '";top: 20px;position: absolute;display: inline-flex;transform: translateX(-50%)}'
					: '.slds-progress__item:nth-child(' +
					  index +
					  ')::before{content: "' +
					  el.label +
					  '";top: 20px;position: absolute;display: inline-flex;transform: translateX(-50%);right: -50px}';
			this.template.querySelector('.slds-progress__item').appendChild(style);
		});
	}

	subscribeToMessageChannel() {
		this.subscription = subscribe(this.messageContext, JOURNEY_MESSAGE_CHANNEL, (message) => {
			this.handleMessage(message);
		});
	}

	async handleMessage(response) {
		if (response.message && response.message === 'SUCCESS' && response.nextStep !== 'finish') {
			this.waitResponse = false;
			this.payload.journeySteps.forEach((step) => {
				step.isActive = false;
			});

			this.payload.journeySteps.find((el) => el.value === response.nextStep).isActive = true;
			this.updateOpportunityJourneySteps();
		} else if (response.message && response.message === 'Error') {
			this.waitResponse = false;
		} else if (response.message && response.message === 'SUCCESS' && response.nextStep === 'finish') {
			this.handleFinish();
		}
	}

	handleNext() {
		if (this.buttonNextFinishName === 'Next' && !this.waitResponse) {
			const payload = {
				message: '',
				currentStep: this.currentStepValue,
				nextStep:
					this.currentStepIndex + 1 < this.payload.journeySteps.length
						? this.payload.journeySteps[this.currentStepIndex + 1].value
						: this.currentStepValue
			};

			this.waitResponse = true;

			publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, payload);
		} else if (this.buttonNextFinishName === 'Finish') {
			const payload = {
				message: '',
				currentStep: this.currentStepValue,
				nextStep: 'finish'
			};
			this.waitResponse = true;
			publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, payload);
		}
	}

	async changeActiveStep(action) {
		let activeIndex = this.currentStepIndex;
		this.payload.journeySteps.forEach((step) => {
			step.isActive = false;
		});
		if (action === 'next') {
			if (activeIndex + 1 < this.payload.journeySteps.length) {
				this.payload.journeySteps[activeIndex + 1].isActive = true;
			} else {
				this.payload.journeySteps[activeIndex].isActive = true;
			}
		} else {
			if (activeIndex > 0) {
				this.payload.journeySteps[activeIndex - 1].isActive = true;
			} else {
				this.payload.journeySteps[activeIndex].isActive = true;
			}
		}
		this.updateOpportunityJourneySteps();
	}

	async updateOpportunityJourneySteps(isJourneyFinished = false) {
		this.payload.isFinished = isJourneyFinished;
		await updateJorneySteps({
			oppId: this.opportunityId,
			steps: JSON.stringify(this.payload)
		})
			.then((res) => {
				if (res !== true) {
					const event = new ShowToastEvent({
						variant: 'error',
						title: 'Error',
						message: 'Something is wrong! Please contact the Administrator!'
					});
					this.dispatchEvent(event);
				}
			})
			.catch((error) => {
				console.error('error', error);
			});
	}

	handlePrevious() {
		this.waitResponse = false;
		this.changeActiveStep('previous');
	}

	handleBackToOpportunity() {
		this[NavigationMixin.Navigate](
			{
				type: 'standard__webPage',
				attributes: {
					url: '/journey-path?opportunityId=' + this.opportunityId
				}
			},
			true
		);
	}

	handleFinish() {
		this.updateOpportunityJourneySteps(true);
		this[NavigationMixin.Navigate](
			{
				type: 'standard__webPage',
				attributes: {
					url: '/journey-path?opportunityId=' + this.opportunityId
				}
			},
			true
		);
	}

	changeStep(event) {
		let selectedStep = this.payload.journeySteps.indexOf(this.payload.journeySteps.find((el) => el.value === event.target.value));
		if (selectedStep < this.currentStepIndex) {
			this.payload.journeySteps.forEach((step) => {
				step.isActive = false;
			});
			this.payload.journeySteps[selectedStep].isActive = true;

			this.updateOpportunityJourneySteps();
		}
	}
}
