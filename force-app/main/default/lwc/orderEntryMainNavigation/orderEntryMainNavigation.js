import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import { APPLICATION_SCOPE, MessageContext, subscribe, unsubscribe } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';
import getOrderEntryData from '@salesforce/apex/OrderEntryController.getOrderEntryData';
import getHeaderInfo from '@salesforce/apex/OrderEntryController.getHeaderInfo';
import updateState from '@salesforce/apex/OrderEntryController.updateState';
import getAutomaticPromotions from '@salesforce/apex/OrderEntryPromotionsController.getAutomaticPromotions';
import { getConstants } from 'c/orderEntryConstants';
import updateOpportunityJourneyType from '@salesforce/apex/OrderEntryProductController.updateOpportunityJourneyType';

const CONSTANTS = getConstants();

export default class OrderEntryMainNavigation extends NavigationMixin(LightningElement) {
	@track disableNextButton = false;
	@track _recordId;
	journeyTypeChanged = false;

	@api
	get recordId() {
		return this._recordId;
	}
	set recordId(value) {
		this.setAttribute('recordId', value);
		this._recordId = value;
		this.getInitialData();
	}

	@track steps = [
		{
			name: 'product-selection',
			label: CONSTANTS.SELECT_PRODUCT
		},
		{
			name: 'account-and-contact',
			label: CONSTANTS.ACCOUNT_CONTACT
		},
		{
			name: 'address',
			label: CONSTANTS.ADDRESS
		},
		{
			name: 'product-config',
			label: CONSTANTS.PRODUCT_CONFIG
		},
		{
			name: 'discount-and-payment',
			label: CONSTANTS.DISCOUNT_PAYMENT
		},
		{
			name: 'installation',
			label: CONSTANTS.INSTALLATION
		},
		{
			name: 'quote-and-signature',
			label: CONSTANTS.QUOTE_SIGNATURE
		}
	];

	@track state = {};
	@track currentStep = null;
	@track headerInfo = '';
	@track error;
	@track showLoader = true;
	@track installationSite;

	@track automaticPromotions = [];
	@track showFinishModal = false;

	@wire(MessageContext)
	messageContext;

	subscription = null;
	subscribeToMessageChannel() {
		if (!this.subscription) {
			this.subscription = subscribe(this.messageContext, genericChannel, (message) => this.handleGenericMessage(message), {
				scope: APPLICATION_SCOPE
			});
		}
	}

	unsubscribeToMessageChannel() {
		unsubscribe(this.subscription);
		this.subscription = null;
	}

	connectedCallback() {
		this.subscribeToMessageChannel();
	}

	disconnectedCallback() {
		this.unsubscribeToMessageChannel();
	}

	toggleLoader(e) {
		this.showLoader = e.detail;
	}

	@wire(getHeaderInfo, { oppId: '$recordId' })
	initHeaderInfo({ error, data }) {
		if (data) {
			this.headerInfo = data;
		} else if (error) {
			this.error = error;
		}
	}

	async getInitialData() {
		var data = null;
		try {
			await getOrderEntryData({ oppId: this.recordId })
				.then((result) => {
					data = result;
				})
				.catch((error) => {
					console.error('Error => ', error);
					this.error = error;
					this.showLoader = false;
				});

			if (data) {
				this.setState(data);
				if (data.lastStep) {
					this.setCurrentStep(data.lastStep);
				} else {
					this.setCurrentStep(CONSTANTS.SELECT_PRODUCT);
				}
			}
			this.showLoader = false;
		} catch (error) {
			console.error('Error => ', error);
			this.error = error;
			this.showLoader = false;
		}
		console.log('data => ', data);
	}

	setCurrentStep(stepLabel) {
		this.currentStep = stepLabel;
		let isCurrentFound = false;
		this.steps.forEach((step) => {
			if (step.label === this.currentStep) {
				step.status = 'active';
				isCurrentFound = true;
			} else {
				if (isCurrentFound) {
					step.status = 'incomplete';
				} else {
					step.status = 'complete';
				}
			}
		});
		this.steps = Object.assign([], this.steps);
	}

	getStepByLabel(label) {
		return this.steps.filter((step) => {
			return step.label === label;
		})[0];
	}

	get isLastIndex() {
		return this.steps.indexOf(this.activeStep) === this.steps.length - 1;
	}
	get isFirstIndex() {
		return this.steps.indexOf(this.activeStep) === 0;
	}
	get activeStep() {
		return this.getStepByLabel(this.currentStep);
	}
	get activeStepComponent() {
		return this.template.querySelector(`c-order-entry-${this.activeStep.name}`);
	}
	get isProductSelection() {
		return this.currentStep === CONSTANTS.SELECT_PRODUCT;
	}
	get isAccountAndContact() {
		return this.currentStep === CONSTANTS.ACCOUNT_CONTACT;
	}
	get isAddress() {
		return this.currentStep === CONSTANTS.ADDRESS;
	}
	get isProductConfiguration() {
		return this.currentStep === CONSTANTS.PRODUCT_CONFIG;
	}
	get isDiscountAndPayment() {
		return this.currentStep === CONSTANTS.DISCOUNT_PAYMENT;
	}
	get isInstallation() {
		return this.currentStep === CONSTANTS.INSTALLATION;
	}
	get isQuoteSignature() {
		return this.currentStep === CONSTANTS.QUOTE_SIGNATURE;
	}

	get mobileProductsNotSelected() {
		return this.isProductConfiguration && this.state.journeyType === 'Mobile' && this.state.bundles.length === 0;
	}

	/**
	 * Routes Generic Messages thrown by child components
	 * @param {object} message Generic Message
	 */
	handleGenericMessage(message) {
		console.log('Generic message: ', message);
		if (message.sourceComponent === 'path') {
			if (message.payload.event === 'step-selected') {
				this.handleStepSelected(message.payload.data.selectedStep, true);
			}
		} else if (message.sourceComponent === 'order-entry-product-selection') {
			if (message.payload.event === 'product-selected') {
				this.handleMainProductSelected(message.payload.data.selectedProduct);
			}
		} else if (message.sourceComponent === 'order-entry-contact-details') {
			if (message.payload.event === 'contact-selected') {
				this.setStateProperty('primaryContactId', message.payload.data.contactId);
			}
		} else if (message.sourceComponent === 'order-entry-account-details') {
			if (message.payload.event === 'b2cInternetCustomer-changed') {
				this.setStateProperty('b2cInternetCustomer', message.payload.data.b2cInternetCustomer);
				this.updatePromos();
			} else if (message.payload.event === 'promotions-changed') {
				this.setStateProperty('bundles', message.payload.data.bundles);
			}
		} else if (message.sourceComponent === 'order-entry-installation-address') {
			if (message.payload.event === 'site-selected') {
				this.handleInstallationSiteSelected(message.payload.data.siteId);
			}
		} else if (message.sourceComponent === 'order-entry-address-check') {
			if (message.payload.event === 'address-check-changed') {
				this.handleSiteCheckChange(message.payload.data);
				this.updatePromos();
			}
		} else if (message.sourceComponent === 'order-entry-product-config-fixed') {
			if (message.payload.event === 'product-bundle-changed') {
				if (JSON.stringify(this.state.bundles[0]) !== JSON.stringify(message.payload.data.bundle)) {
					this.handleFixedBundleChange(message.payload.data.bundle);
					this.updatePromos();
				}
			}
		} else if (message.sourceComponent === 'order-entry-promotions-fixed') {
			if (message.payload.event === 'product-bundle-changed') {
				this.handleFixedBundleChange(message.payload.data.bundle);
			}
		} else if (message.sourceComponent === 'order-entry-payment-details') {
			if (message.payload.event === 'payment-changed') {
				this.setStateProperty('payment', message.payload.data.payment);
			}
		} else if (message.sourceComponent === 'order-entry-installation-fixed') {
			if (message.payload.event === 'installation-bundle-changed') {
				this.handleFixedBundleChange(message.payload.data.bundle);
			} else if (message.payload.event === 'notes-changed') {
				this.setStateProperty('notes', message.payload.data.notes);
			}
		} else if (message.sourceComponent === 'order-entry-product-config-mobile') {
			if (message.payload.event === 'product-bundles-add') {
				this.handleMobileBundlesAdd(message.payload.data.bundles);
			} else if (message.payload.event === 'product-bundle-change') {
				this.handleMobileBundleChange(message.payload.data.bundle, message.payload.data.index);
			} else if (message.payload.event === 'product-bundle-remove') {
				this.handleMobileBundleRemove(message.payload.data.index);
			}
		}
	}

	handleMobileBundlesAdd(bundles) {
		// TBD add product bundles in the state
		let mobileBundles = this.state.bundles.filter((x) => {
			return x.type === 'Mobile';
		});
		let fixedBundles = this.state.bundles.filter((x) => {
			return x.type === 'Fixed';
		});
		mobileBundles = mobileBundles.concat(bundles);
		console.log('Mobile Bundles');
		console.log(mobileBundles);
		// TBD add automatic promotions
		this.state.bundles = fixedBundles.concat(mobileBundles);
		// TBD Twice updating of state doesn't calculate summary second time
		this.updatePromos();
	}

	handleMobileBundleChange(bundle, index) {
		// TBD change product bundle in the state
		let mobileBundles = this.state.bundles.filter((x) => {
			return x.type === 'Mobile';
		});
		let fixedBundles = this.state.bundles.filter((x) => {
			return x.type === 'Fixed';
		});
		mobileBundles.splice(index, 1, bundle);
		// TBD add automatic promotions
		this.state.bundles = fixedBundles.concat(mobileBundles);
		// TBD Twice updating of state doesn't calculate summary second time
		this.updatePromos();
	}

	handleMobileBundleRemove(index) {
		// TBD remove bundle from state
		let mobileBundles = this.state.bundles.filter((x) => {
			return x.type === 'Mobile';
		});
		let fixedBundles = this.state.bundles.filter((x) => {
			return x.type === 'Fixed';
		});
		mobileBundles.splice(index, 1);
		this.setStateProperty('bundles', fixedBundles.concat(mobileBundles));
	}

	/**
	 * Handles selection of the step in the path
	 * @param {string} stepLabel Selected Step Label
	 * @param {boolean} preventNext Indicates if user is only able to navigate back
	 */
	async handleStepSelected(stepLabel, preventNext) {
		this.showLoader = true;
		const selectedStep = this.getStepByLabel(stepLabel);
		const activeIndex = this.steps.indexOf(this.activeStep);
		const selectedIndex = this.steps.indexOf(selectedStep);
		if (activeIndex === selectedIndex) {
			return;
		}
		if (preventNext && selectedIndex > activeIndex) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Unable to go to that step',
					message: 'Use Next button to go to that step',
					variant: 'warning'
				})
			);
		} else {
			this.setStateProperty('lastStep', stepLabel);
			await this.saveState();
			this.setCurrentStep(stepLabel);
		}
		this.showLoader = false;
	}

	/**
	 * Handles selection of Main Product
	 * @param {object} mainProduct Main Product
	 */
	handleMainProductSelected(mainProduct) {
		this.setStateProperty('mainProduct', mainProduct);
		this.journeyTypeChanged = this.journeyTypeChanged ? false : true;

		if (mainProduct.Journey_Type__c) {
			this.setStateProperty('journeyType', mainProduct.Journey_Type__c);
		}

		updateOpportunityJourneyType({
			oppId: this.state.opportunityId,
			type: mainProduct.Journey_Type__c
		});
	}

	/**
	 * Handles selection of new Installation Site
	 * @param {string} siteId Site ID
	 */
	handleInstallationSiteSelected(siteId) {
		let site = this.getInstallationSite();
		if (site) {
			site.siteId = siteId;
			site.type = 'Installation';
		} else {
			site = {
				Id: siteId,
				type: 'Installation'
			};

			this.state.sites.push(site);
		}
		delete site.siteCheck;
		delete site.addressCheckResult;
		delete site.offNetType;
		this.setState();
	}

	/**
	 * Handles changes of Site Check
	 * @param {object} siteCheckData Site Check Data
	 */
	handleSiteCheckChange(siteCheckData) {
		let site = this.getInstallationSite();
		site.siteCheck = siteCheckData.siteCheck;
		site.addressCheckResult = siteCheckData.addressCheckResult;
		site.offNetType = siteCheckData.offNetType;
		if (site.addressCheckResult !== 'On-Net') {
			this.state.b2cInternetCustomer = false;
		}
		this.setState();
	}

	getInstallationSite() {
		return this.state.sites.filter((site) => {
			return site.type === 'Installation';
		})[0];
	}

	handleFixedBundleChange(bundle) {
		var bundles = this.state.bundles.filter((bundle) => {
			return bundle.type !== 'Fixed';
		});
		bundles.push(bundle);
		this.setStateProperty('bundles', bundles);
	}

	/**
	 * Sets State Property to provided value
	 * @param {string} name Property Name
	 * @param {object} value Property Value
	 */
	setStateProperty(name, value) {
		console.log('****** ***** name, value ???????? ', name, value);

		this.setState(Object.assign({}, this.state, { [name]: value }));
	}

	// TODO
	setState(newState) {
		if (newState) {
			this.state = newState;
		} else {
			this.state = Object.assign({}, this.state);
		}

		this.installationSite = Object.assign({}, this.getInstallationSite());
	}

	async updatePromos() {
		let newStateClone = JSON.parse(JSON.stringify(this.state));

		this.cleanUnusedPromos(newStateClone);
		let promotionPromises = [];
		for (let i = 0; i < newStateClone.bundles.length; i++) {
			promotionPromises.push(this.addAutomaticPromotion(newStateClone, i));
		}
		return Promise.all(promotionPromises).then(() => {
			console.log('newStateClone', newStateClone);
			this.setState(newStateClone);
		});
	}

	/**
	 * @description Cleans unused promotions from state.
	 * @param {*} s State
	 */
	cleanUnusedPromos(s) {
		s.bundles.forEach((b) => {
			let products = new Set();
			let addons = new Set();
			b.products?.forEach((p) => products.add(p.id));
			b.addons?.forEach((a) => addons.add(a.id));
			b.promos = b.promos?.filter((p) => {
				return p.discounts?.some((d) => {
					let isProduct = d.product && products.has(d.product);
					let isAddon = d.addon && addons.has(d.addon);
					let contractTerms = new Set();
					let customerTypes = new Set();
					p.contractTerms?.split(';').forEach((t) => contractTerms.add(t));
					p.customerType?.split(';').forEach((c) => customerTypes.add(c));
					if (!contractTerms.has(b.contractTerm)) {
						return false;
					}
					if ((s.b2cInternetCustomer && !customerTypes.has('Existing')) || (!s.b2cInternetCustomer && !customerTypes.has('New'))) {
						return false;
					}
					return isProduct || isAddon;
				});
			});
		});
	}

	/**
	 * Saves State to JSON
	 */
	async saveState() {
		try {
			const result = await updateState({
				oppId: this.state.opportunityId,
				state: JSON.stringify(this.state)
			});
			if (result) {
				console.info('Saved State', this.state);
			}
		} catch (err) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Error',
					message: 'Something went wrong when saving state',
					variant: 'error'
				})
			);
		}
	}

	/**
	 * Update state from child component events
	 */
	async updateState(e) {
		this.setState(e.detail);
		console.log('setstate', e.detail);
	}

	// NAV BUTTONS

	/**
	 * Handles Next button in the navigation
	 */
	async next() {
		if (!this.state.journeyType) {
			this.setStateProperty('journeyType', 'Fixed');
		}

		if (this.journeyTypeChanged) {
			let bundles = [];
			this.setStateProperty('bundles', bundles);

			this.journeyTypeChanged = false;
		}

		if (this.mobileProductsNotSelected) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'No products selected',
					message: 'Please select products to proceed further',
					variant: 'warning',
					mode: 'dismissable'
				})
			);
		} else {
			this.disableNextButton = true;
			this.showLoader = true;
			const activeComponent = this.activeStepComponent;

			// Validate Active Step
			if (
				(activeComponent && typeof activeComponent.validate !== 'undefined' && (await activeComponent.validate())) ||
				typeof activeComponent.validate === 'undefined'
			) {
				// Check if after save function exists on active child component
				let isError = false;
				await this.saveState();
				if (typeof activeComponent.saveData !== 'undefined') {
					try {
						await activeComponent.saveData();
					} catch (e) {
						this.dispatchEvent(
							new ShowToastEvent({
								title: 'Error occured',
								message: e.body.message,
								variant: 'error',
								mode: 'sticky'
							})
						);
						isError = true;
					}
				}

				if (!isError) {
					// Move Next and save State
					let newStep = this.steps[this.steps.indexOf(this.activeStep) + 1];
					await this.handleStepSelected(newStep.label, false);
				}
			}
			this.showLoader = false;
			this.disableNextButton = false;
		}
	}

	/**
	 * Handles Back button in the navigation
	 */
	async back() {
		this.showLoader = true;
		let newStep = this.steps[this.steps.indexOf(this.activeStep) - 1];
		await this.handleStepSelected(newStep.label, false);
		this.showLoader = false;
	}

	/**
	 * Handles Close button that returns back to Opportunity
	 */
	backToOpp() {
		// Navigate back to Oppotrunity
		this[NavigationMixin.Navigate](
			{
				type: 'standard__recordPage',
				attributes: {
					recordId: this.recordId,
					actionName: 'view'
				}
			},
			true
		);
	}

	/**
	 * Get product from list of state products by product type
	 */
	getProductIndexByType(products, type) {
		return products ? products.findIndex((p) => p.type === type) : -1;
	}

	setDefaultProducts(products, bundle) {
		[CONSTANTS.INTERNET_PRODUCT_TYPE, CONSTANTS.TV_PRODUCT_TYPE, CONSTANTS.TELEPHONY_PRODUCT_TYPE].forEach((type) => {
			const productIndex = products.findIndex((p) => p.type === type);
			const id = bundle[`${type}_Product__c`];

			if (productIndex === -1 && id) {
				products.push({
					type,
					id
				});
			}
		});
		return products;
	}

	async addAutomaticPromotion(state, bundleIndex) {
		this.showLoader = true;

		this.automaticPromotions = await getAutomaticPromotions({
			opportunityId: state.opportunityId,
			b2cChecked: state.b2cInternetCustomer,
			contractTerm: state.bundles[bundleIndex].contractTerm,
			addressCheckResult: state.sites.addressCheckResult,
			offNetType: state.sites.offNetType,
			products: state.bundles[bundleIndex].products,
			addons: state.bundles[bundleIndex].addons
		});

		this.automaticPromotions.forEach(async (element) => {
			var promotionData = {
				id: element.Id,
				type: CONSTANTS.AUTOMATIC_PROMOTION_TYPE
			};

			this.setPromotion(state, await this.buildPromotionData(promotionData), bundleIndex);
		});

		await this.removeUnavailableAutomaticPromotions(state, this.automaticPromotions, bundleIndex);
	}

	setPromotion(state, promotionData, bundleIndex) {
		if (promotionData) {
			const promotions = state.bundles[bundleIndex].promos || [];

			let automaticPromotionIndex = -1;
			automaticPromotionIndex = promotions.findIndex((p) => p.id === promotionData.id);

			if (automaticPromotionIndex === -1) {
				promotions.push(promotionData);
			}
			state.bundles[bundleIndex].promos = promotions;
		}
	}

	buildPromotionData({ id, type }) {
		var promotion = {};

		promotion = this.automaticPromotions.find((ap) => ap.Id === id);

		if (promotion) {
			return {
				id,
				type,
				name: promotion.Promotion_Name__c,
				contractTerms: promotion.Contract_Term__c,
				customerType: promotion.Customer_Type__c,
				connectionType: promotion.Connection_Type__c,
				offNetType: promotion.Off_net_Type__c,
				discounts: this.buildDiscountData(promotion.Discounts__r)
			};
		}
		return null;
	}

	buildDiscountData(discounts) {
		return discounts.map((d) => ({
			id: d.Discount__c,
			name: d.Discount__r.Name,
			type: d.Discount__r.Type__c,
			value: d.Discount__r.Value__c,
			oneOffValue: d.Discount__r.One_Off_Value__c,
			duration: d.Discount__r.Duration__c,
			level: d.Discount__r.Level__c,
			product: d.Discount__r.Product__c,
			addon: d.Discount__r.Add_On__c
		}));
	}

	async removeUnavailableAutomaticPromotions(state, automaticPromotions, bundleIndex) {
		if (state && state.bundles[bundleIndex].promos && state.bundles[bundleIndex].promos.length > 0 && automaticPromotions) {
			let tepmPmomos = Object.assign([], state.bundles[bundleIndex].promos);
			state.bundles[bundleIndex].promos.forEach((p) => {
				if (p.type === CONSTANTS.AUTOMATIC_PROMOTION_TYPE) {
					let promoFound = -1;
					promoFound = automaticPromotions.findIndex((ap) => ap.Id === p.id);

					if (promoFound === -1) {
						tepmPmomos.splice(tepmPmomos.indexOf(p), 1);
					}
				}
			});
			state.bundles[bundleIndex].promos = tepmPmomos;
		}
		this.showLoader = false;
	}
	closeOrderEntryApp() {
		this.navigateToRecordPage();
	}

	handlePaymentDetailsRequired(e) {
		this.setCurrentStep(CONSTANTS.DISCOUNT_PAYMENT);
	}

	navigateToRecordPage() {
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: this.state.opportunityId,
				objectApiName: 'Opportunity',
				actionName: 'view'
			}
		});
	}
}
