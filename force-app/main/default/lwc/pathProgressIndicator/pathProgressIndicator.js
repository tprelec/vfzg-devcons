import { api, LightningElement } from "lwc";

export default class PathProgressIndicator extends LightningElement {
    @api
    steps;
    @api
    currentStep;
    @api readOnly = false;

    get progressContainerClass() {
        return this.readOnly ? "read-only-progress-indicator" : "";
    }
}