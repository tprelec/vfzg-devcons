/**
 * Generic Autocomplete
 * Use case: when there are a lot of elements to choose upon and not-so-many complex filters to apply
 * Throws an event whether the user selects an element or delete the selected one.
 */
import { LightningElement, api, wire } from "lwc";
import findSObjectsLwc from "@salesforce/apex/AutocompleteController.findSObjectsLwc";
import fetchDefaultRecord from "@salesforce/apex/AutocompleteController.fetchDefaultRecord";
import autocNoRecordsFound from "@salesforce/label/c.autocNoRecordsFound";
import autocRemoveSelected from "@salesforce/label/c.autocRemoveSelected";
import autocSearchLabel from "@salesforce/label/c.autocSearchLabel";
import { reduceErrors } from "c/ldsUtils";
const DELAY = 300; // delay apex callout timing in miliseconds

export default class Autocomplete extends LightningElement {
	labels = {
		autocNoRecordsFound,
		autocRemoveSelected,
		autocSearchLabel
	};
	// The label that will be shown in the upside of the input text.
	@api label = " ";
	// The inscription that will be shown as a placeholder on the input text.
	@api placeholder = autocSearchLabel;
	// The icon that will be shown in the left side of the dropdown elements.
	@api iconName = "standard:account";
	// The object which will be queried.
	@api sObjectApiName = "Ban__c";
	@api defaultRecordId = "";
	@api disabled = false;
	// Default field and value on which apply a dependant relationship
	// (i.e. Ban numbers usually are filtered by a specific accountId)
	// it can be set by any parent LWC dynamically.
	@api parentField = "";
	@api parentValue = "";

	@api setParentFilters(sField, sValue) {
		this.parentField = sField;
		this.parentValue = sValue;
	}

	@api setDefaultId(sDefaultId) {
		this.defaultRecordId = sDefaultId;
		if (sDefaultId == "") {
			this.handleRemove();
		}
	}
	// private properties
	lstResult = []; // to store list of returned records
	hasRecords = true;
	searchKey = ""; // to store input field value
	isSearchLoading = false; // to control loading spinner
	delayTimeout;
	selectedRecord = {}; // to store selected lookup record in object formate

	@wire(fetchDefaultRecord, {
		recordId: "$defaultRecordId"
	})
	fetchDefRec(value) {

		if (!this.defaultRecordId) {
			return;
		}
		const { data, error } = value; // destructure the provisioned value
		this.isSearchLoading = false;
		if (data) {
			this.selectedRecord = data;
			this.handleSelectRecordHelper(); // helper function to show/hide lookup result container on UI
		} else if (error) {
			this.error = reduceErrors(error);
			this.selectedRecord = {};
			console.log("(error(fetchDefaultRecord)-> " + this.error);
		}
	}

	// wire function property to fetch search record based on user input
	// String sObjectApiName, String key, String addFields, String parentField, String parentValue, Boolean bothSides
	@wire(findSObjectsLwc, {
		sObjectApiName: "$sObjectApiName",
		key: "$searchKey",
		addFields: "",
		parentField: "$parentField",
		parentValue: "$parentValue",
		bothSides: false
	})
	searchResult(value) {
		const { data, error } = value; // destructure the provisioned value
		this.isSearchLoading = false;

        console.log(JSON.stringify(value));
		if (data) {
			this.hasRecords = data.length == 0 ? false : true;
			// TODO: process "addField" fields.
			this.lstResult = JSON.parse(JSON.stringify(data));
			// console.log(Date.now() + "findSObjectsLwc -> parentValue: " + this.parentValue);
		} else if (error) {
			this.error = reduceErrors(error);
			console.log("(error(findSObjectsLwc)-> " + this.error);
		}
	}

	// update searchKey property on input field change
	handleKeyChange(event) {
		// Debouncing this method: Do not update the reactive property as long as this function is
		// being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
		this.isSearchLoading = true;
		window.clearTimeout(this.delayTimeout);
		const searchKey = event.target.value;
		this.delayTimeout = setTimeout(() => {
			this.searchKey = searchKey;
		}, DELAY);
	}

	// method to toggle lookup result section on UI
	toggleResult(event) {
		if (this.disabled) {
			return;
		}
		const lookupInputContainer = this.template.querySelector(".lookupInputContainer");
		const clsList = lookupInputContainer.classList;
		const whichEvent = event.target.getAttribute("data-source");
		switch (whichEvent) {
			case "searchInputField":
				clsList.add("slds-is-open");
				break;
			case "lookupContainer":
				clsList.remove("slds-is-open");
				break;
		}
	}

	// method to clear selected lookup record
	handleRemove() {
		if (this.disabled) {
			return;
		}
		this.searchKey = "";
		this.selectedRecord = {};
		this.lookupUpdatehandler(undefined); // update value on parent component as well from helper function

		// remove selected pill and display input field again
		const searchBoxWrapper = this.template.querySelector(".searchBoxWrapper");
		searchBoxWrapper.classList.remove("slds-hide");
		searchBoxWrapper.classList.add("slds-show");

		const pillDiv = this.template.querySelector(".pillDiv");
		pillDiv.classList.remove("slds-show");
		pillDiv.classList.add("slds-hide");
	}

	// method to update selected record from search result
	handleSelectedRecord(event) {
		var objId = event.target.getAttribute("data-recid"); // get selected record Id
		this.selectedRecord = this.lstResult.find((data) => data.Id === objId); // find selected record from list
		this.lookupUpdatehandler(this.selectedRecord); // update value on parent component as well from helper function
		this.handleSelectRecordHelper(); // helper function to show/hide lookup result container on UI
	}

	/*COMMON HELPER METHOD STARTED*/

	handleSelectRecordHelper() {
		this.template.querySelector(".lookupInputContainer").classList.remove("slds-is-open");

		const searchBoxWrapper = this.template.querySelector(".searchBoxWrapper");
		searchBoxWrapper.classList.remove("slds-show");
		searchBoxWrapper.classList.add("slds-hide");

		const pillDiv = this.template.querySelector(".pillDiv");
		pillDiv.classList.remove("slds-hide");
		pillDiv.classList.add("slds-show");
	}

	// send selected lookup record to parent component using custom event
	lookupUpdatehandler(value) {
		const oEvent = new CustomEvent("lookupupdate", {
			detail: { selectedRecord: value }
		});
		this.dispatchEvent(oEvent);
	}
}