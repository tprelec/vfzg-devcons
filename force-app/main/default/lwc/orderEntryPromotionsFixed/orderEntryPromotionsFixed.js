import { LightningElement, api, track, wire } from 'lwc';
import { getConstants } from 'c/orderEntryConstants';
import getBundlePromotions from '@salesforce/apex/OrderEntryPromotionsController.getBundlePromotions';
import getSpecialPromotions from '@salesforce/apex/OrderEntryPromotionsController.getSpecialPromotions';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

const CONSTANTS = getConstants();

export default class OrderEntryPromotionsFixed extends LightningElement {
	@wire(MessageContext)
	messageContext;
	@api opportunityId;
	@track _b2cInternetCustomer;
	@api
	get b2cInternetCustomer() {
		return this._b2cInternetCustomer;
	}
	set b2cInternetCustomer(value) {
		this._b2cInternetCustomer = value;
	}
	@api primaryContact;
	@api installationSite;

	@track _bundle;
	@api
	get bundle() {
		return this._bundle;
	}
	set bundle(value) {
		this._bundle = Object.assign({}, value);
		if (this._bundle) {
			this.init();
		}
	}

	api;
	get automaticPromotions() {
		return this.bundle?.promos?.filter((el) => el.type === CONSTANTS.AUTOMATIC_PROMOTION_TYPE);
	}

	@track bundlePromotions = [];
	@track specialPromotions = [];
	@track specialPromotionsExist = false;
	@track showLoader = true;

	connectedCallback() {}

	async init() {
		try {
			this.bundlePromotions = await this.getBundlePromotions();
			this.specialPromotions = await this.getSpecialPromotions();
			this.filterAvailablePromotions(this.specialPromotions, CONSTANTS.STANDARD_PROMOTION_TYPE);
			this.filterAvailablePromotions(this.bundlePromotions, CONSTANTS.SPECIAL_PROMOTION_TYPE);
			this.updateSpecialPromotionsExist();
			this.showLoader = false;
		} catch (e) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Error occured',
					message: e.body ? e.body.message : e.message,
					variant: 'error',
					mode: 'sticky'
				})
			);
		}
	}

	async getBundlePromotions() {
		return await getBundlePromotions({
			opportunityId: this.opportunityId,
			b2cChecked: this.b2cInternetCustomer,
			products: this.bundle.products,
			addons: this.bundle.addons,
			bundle: this.bundle,
			site: this.installationSite
		});
	}

	async getSpecialPromotions() {
		return await getSpecialPromotions({
			opportunityId: this.opportunityId,
			b2cChecked: this.b2cInternetCustomer,
			products: this.bundle.products,
			addons: this.bundle.addons,
			bundle: this.bundle,
			site: this.installationSite
		});
	}

	filterAvailablePromotions(promotions, type) {
		let selectedProdsAddons = [];

		if (this.bundle.promos) {
			this.bundle.promos.forEach((p) => {
				p.discounts.forEach((d) => {
					if (p.type === type) {
						if (d.product) {
							selectedProdsAddons.push(d.product);
						} else if (d.addon) {
							selectedProdsAddons.push(d.addon);
						}
					}
				});
			});
		}

		if (promotions) {
			let promos = Object.assign([], promotions);
			promos.forEach((p, index) => {
				p.Discounts__r.forEach((d) => {
					if (d.Discount__r) {
						if (
							(d.Discount__r.Product__c && selectedProdsAddons.includes(d.Discount__r.Product__c)) ||
							(d.Discount__r.Add_On__c && selectedProdsAddons.includes(d.Discount__r.Add_On__c))
						) {
							promos.splice(index, 1);
						}
					}
				});
			});
			promotions = promos;
		}
	}

	updateSpecialPromotionsExist() {
		let selectedPromotion = false;
		if (this.bundle.promos) {
			this.bundle.promos.forEach((prom) => {
				if (prom.type === CONSTANTS.SPECIAL_PROMOTION_TYPE) {
					selectedPromotion = true;
				}
			});
		}

		this.specialPromotionsExist =
			(this.specialPromotions && this.specialPromotions.length) ||
			(!(this.specialPromotions && this.specialPromotions.length) && selectedPromotion);
	}

	async handleSelectedPromotion(e) {
		if (e.detail.id) {
			const promotionData = this.buildPromotionData(e.detail);
			this.setPromotion(promotionData);
		} else {
			// Deselect bundle promotion
			let index = -1;
			index = this.bundle.promos.findIndex((p) => p.type === e.detail.type);
			let promos = Object.assign([], this._bundle.promos);

			promos.splice(index, 1);
			this._bundle.promos = promos;
		}

		this.showLoader = true;
		this.bundlePromotions = await this.getBundlePromotions();
		this.specialPromotions = await this.getSpecialPromotions();

		this.filterAvailablePromotions(this.specialPromotions, CONSTANTS.STANDARD_PROMOTION_TYPE);
		this.filterAvailablePromotions(this.bundlePromotions, CONSTANTS.SPECIAL_PROMOTION_TYPE);
		this.updateSpecialPromotionsExist();
		this.showLoader = false;
		this.publishProductChanged();
	}

	async handleRemoveSpecialPromotion(e) {
		let index = -1;
		index = this.bundle.promos.findIndex((p) => p.id === e.detail);

		let promos = Object.assign([], this._bundle.promos);
		promos.splice(index, 1);
		this._bundle.promos = promos;

		this.showLoader = true;
		this.bundlePromotions = await this.getBundlePromotions();
		this.filterAvailablePromotions(this.bundlePromotions, CONSTANTS.SPECIAL_PROMOTION_TYPE);
		this.updateSpecialPromotionsExist();
		this.showLoader = false;
		this.publishProductChanged();
	}

	setPromotion(promotionData) {
		if (promotionData) {
			const promotions = Object.assign([], this.bundle.promos) || [];
			if (promotionData.type === CONSTANTS.STANDARD_PROMOTION_TYPE) {
				let bundlePromotionIndex = -1;
				bundlePromotionIndex = promotions.findIndex((p) => p.type === CONSTANTS.STANDARD_PROMOTION_TYPE);

				if (bundlePromotionIndex === -1) {
					promotions.push(promotionData);
				} else {
					promotions[bundlePromotionIndex] = promotionData;
				}
			} else if (promotionData.type === CONSTANTS.SPECIAL_PROMOTION_TYPE) {
				let specialPromotionIndex = -1;
				specialPromotionIndex = promotions.findIndex((p) => p.id === promotionData.id);

				if (specialPromotionIndex === -1) {
					promotions.push(promotionData);
				}
			}
			this._bundle.promos = promotions;
			this.publishProductChanged();
		}
	}

	buildPromotionData({ id, type }) {
		let promotion = {};

		if (type === CONSTANTS.STANDARD_PROMOTION_TYPE) {
			promotion = this.bundlePromotions.find((bp) => bp.Id === id);
		} else if (type === CONSTANTS.SPECIAL_PROMOTION_TYPE) {
			promotion = this.specialPromotions.find((sp) => sp.Id === id);
		}

		if (promotion) {
			return {
				id,
				type,
				name: promotion.Promotion_Name__c,
				contractTerms: promotion.Contract_Term__c,
				customerType: promotion.Customer_Type__c,
				connectionType: promotion.Connection_Type__c,
				offNetType: promotion.Off_net_Type__c,
				discounts: this.buildDiscountData(promotion.Discounts__r)
			};
		}
		return null;
	}

	buildDiscountData(discounts) {
		return discounts.map((d) => ({
			id: d.Discount__c,
			name: d.Discount__r.Name,
			type: d.Discount__r.Type__c,
			value: d.Discount__r.Value__c,
			duration: d.Discount__r.Duration__c,
			level: d.Discount__r.Level__c,
			product: d.Discount__r.Product__c,
			addon: d.Discount__r.Add_On__c
		}));
	}

	publishProductChanged() {
		const msg = {
			sourceComponent: 'order-entry-promotions-fixed',
			payload: {
				event: 'product-bundle-changed',
				data: {
					bundle: this.bundle
				}
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}
}
