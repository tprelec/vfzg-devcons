import { api, LightningElement } from 'lwc';
import ERRORS_HEADER from '@salesforce/label/c.Page_Errors_Header';
import PERSON_DETAILS_ERROR from '@salesforce/label/c.Person_Details_Errors';

export default class ServiceApptToast extends LightningElement {
	@api
	message = PERSON_DETAILS_ERROR;

	@api
	header = ERRORS_HEADER;
}
