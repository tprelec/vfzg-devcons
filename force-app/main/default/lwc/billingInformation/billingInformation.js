import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { FlowAttributeChangeEvent } from 'lightning/flowSupport';
//import { getRecord } from "lightning/uiRecordApi";
import { getRecordNotifyChange } from 'lightning/uiRecordApi';
import { getObjectInfo, getPicklistValues } from 'lightning/uiObjectInfoApi';
import uId from '@salesforce/user/Id';

import getData from '@salesforce/apex/BillingInformationController.getData';
import getBanForAccountId from '@salesforce/apex/BillingInformationController.getBanForAccountId';
import getFAForBanId from '@salesforce/apex/BillingInformationController.getFAForBanId';
import getBillingArrangementForFinancialAccountId from '@salesforce/apex/BillingInformationController.getBillingArrangementForFinancialAccountId';
import getBillingArrangementForBanId from '@salesforce/apex/BillingInformationController.getBillingArrangementForBanId';

import save from '@salesforce/apex/BillingInformationController.save';
import requestNew from '@salesforce/apex/BillingInformationController.requestNew';
import getRecord from '@salesforce/apex/BillingInformationController.getRecord';
import getDependentPicklistValues from '@salesforce/apex/BillingInformationController.getDependentPicklistValues';
import getActivePicklistValues from '@salesforce/apex/BillingInformationController.getActivePicklistValues';
import isUserPartnerUser from '@salesforce/apex/BillingInformationController.isUserPartnerUser';

import BAN_ACCOUNT_FIELD from '@salesforce/schema/Ban__c.Account__c';
import FA_BAN_FIELD from '@salesforce/schema/Financial_Account__c.BAN__c';
import BA_FINANCIAL_FIELD from '@salesforce/schema/Billing_Arrangement__c.Financial_Account__c';
import BAN_STATUS from '@salesforce/schema/Ban__c.BAN_Status__c';
import FA_STATUS from '@salesforce/schema/Financial_Account__c.Status__c';
import BA_STATUS from '@salesforce/schema/Billing_Arrangement__c.Status__c';

import readyForOrderLabel from '@salesforce/label/c.BillingInformation_ReadyForOrderLabel';
import readyForOrderHelpText from '@salesforce/label/c.BillingInformation_ReadyForOrderHelpText';

import { publish, subscribe, MessageContext } from 'lightning/messageService';
import JOURNEY_MESSAGE_CHANNEL from '@salesforce/messageChannel/JourneyMessages__c';

export default class BillingInformation extends LightningElement {
	label = {
		readyForOrderLabel,
		readyForOrderHelpText
	};

	@wire(MessageContext)
	messageContext;

	@api recordId;
	@api required = false;
	@api isJourney = false;
	@api isOutputOnly = false;

	opportunityId;
	@track banObject = {};
	@track financialAccountObject = {};
	@track billingArrangementObject = {};
	@track banUnifyCustomerTypeOptions;
	@track allBanUnifyCustomerSubTypeOptions;
	@track filteredBanUnifyCustomerSubTypeOptions;
	dependantSubTypeMatrix;
	@track billingArrangementPaymentMethodOptions;
	@track billingArrangementBillingCountryOptions;
	@track billingArrangementBillFormatOptions;
	@track billingArrangementBillProductionOptions;
	@track billingArrangementStatusOptions;
	@track paymentMethodIsInvoice = true;
	ifSearchIsEmptyReturnAll = true;

	recordBanField = 'Ban__c';

	// only for screen flow to get notification of form completion
	@api outputValidation = null;
	@track eventMessage;

	@track banCreateNewConfigFields;
	@track financialAccountCreateNewConfigFields;
	@track billingArrangementCreateNewConfigFields;
	@track banFieldset = [];
	@track financialAccountFieldset = [];
	@track billingArrangementFieldset = [];
	@track banFieldValues = {};
	@track barFieldValues = {};
	@track isPartnerUser = true;
	@track isFieldReadOnly = true;
	@track billingArrangementPrimaryField;

	isLoading = true;
	isBanLoading = false;
	isIndirectOpportunity = false;
	isDeepSellOpportunity = false;
	isFinancialAccountLoading;
	isBillingArrangementLoading;

	accountId;
	banId;
	banName;
	financialAccountId;
	financialAccountName;
	billingArrangementId;
	billingArrangementName;
	_isBanInputChanged = false;
	_orderBillingValidationLoadedCount = 0;

	get disableFinancialAccountLookup() {
		return !this.banId;
	}

	get disableBillingArrangementLookup() {
		if (this.isDeepSellOpportunity) {
			return !this.banId;
		}
		return !this.financialAccountId;
	}

	get banFilter() {
		//return `${BAN_ACCOUNT_FIELD.fieldApiName} = '${this.accountId}' AND ${BAN_STATUS.fieldApiName} = 'Opened'`;
		return `${BAN_ACCOUNT_FIELD.fieldApiName} = '${this.accountId}' AND ${BAN_STATUS.fieldApiName} != 'Closed'`; //user should be able to search Requested BANs also
	}

	get financialAccountFilter() {
		return `${FA_BAN_FIELD.fieldApiName} = '${this.banId}' AND ${FA_STATUS.fieldApiName} = 'Open'`;
	}

	get billingArrangementFilter() {
		if (this.isDeepSellOpportunity) {
			return `Financial_Account__r.BAN__r.Id = '${this.banId}' AND ${BA_STATUS.fieldApiName} = 'Open'`;
		}
		return `${BA_FINANCIAL_FIELD.fieldApiName} = '${this.financialAccountId}' AND ${BA_STATUS.fieldApiName} = 'Open'`;
	}

	get activeSections() {
		return ['section1', 'section2', 'section3'];
	}

	get disableSaveOnProcessing() {
		return this.isLoading || this.isBanLoading || this.isFinancialAccountLoading || this.isBillingArrangementLoading;
	}

	async connectedCallback() {
		this.subscribeToMessageChannel();
		// eslint-disable-next-line @lwc/lwc/no-api-reassignments
		this.outputValidation = true;
		this.isLoading = true;
		const result = await getData({ recordId: this.recordId });

		const data = result.body;
		if (data) {
			this.accountId = data.accountId || null;
			this.opportunityId = data.opportunityId || null;
			this.banId = data.banId || null;
			this.banName = data.banName || '';
			this.financialAccountId = data.financialAccountId || null;
			this.financialAccountName = data.financialAccountName || '';
			this.billingArrangementId = data.billingArrangementId || '';
			this.billingArrangementName = data.billingArrangementName || '';
			this.banCreateNewConfigFields = data.banCreateNewConfigFields;
			this.financialAccountCreateNewConfigFields = data.financialAccountCreateNewConfigFields;

			this.banFieldset = data.banFieldset;
			this.financialAccountFieldset = data.financialAccountFieldset;
			this.billingArrangementFieldset = data.billingArrangementFieldset;

			this.isIndirectOpportunity = data.opportunityType === 'Indirect';
			this.isDeepSellOpportunity = data.journeyType === 'Add Mobile Product';
			this.billingArrangementPrimaryField = this.isIndirectOpportunity ? 'Unify_Ref_Id__c' : 'Name';
			this.billingArrangementCreateNewConfigFields = !this.isDeepSellOpportunity ? data.billingArrangementCreateNewConfigFields : null;
		}
		this.disableFinancialAccountSection();

		if (result.variant && result.message) {
			this._showToast(result.variant, result.message);
		}
		if (!this.banId) {
			const banResult = await getBanForAccountId({ accId: this.accountId });
			this.banObject = banResult;
			this.banId = banResult.length === 1 ? banResult[0].Id : null;
			this.banName = banResult.length === 1 ? banResult[0].Name : null;
		}

		for (let banField of this.banFieldset) {
			if (this.banObject[banField.name]) {
				banField.value = this.banObject[banField.name];
			}
		}

		await this.checkIsUserPartnerUser();
		await this.setBanInformation();
		await this.setFinancialAccount();
		this.isBillingArrangementLoading = true;
		await this.setBillingArrangement();

		this.disableBAFields();

		await this.setPaymentMethodOptions();
		this.isBillingArrangementLoading = false;

		this.isLoading = false;
		this._sendValidationNotification();
	}

	async checkIsUserPartnerUser() {
		this.isPartnerUser = await isUserPartnerUser();
	}

	disableFinancialAccountSection() {
		if (this.isDeepSellOpportunity) {
			this.template.querySelector('lightning-accordion-section.section2').classList.toggle('section_hidden');
		}
	}

	//according to the OrderForm the user can only edit information if BAN status is requested
	disableBAFields() {
		if (this.isOutputOnly) {
			this.isFieldReadOnly = true;
		} else if (this.banObject && this.banObject.BAN_Status__c == 'Requested') {
			this.isFieldReadOnly = false;
		} else {
			this.isFieldReadOnly = true;
		}
	}

	async setBanInformation() {
		this.isBanLoading = true;
		await this.fetchtDependentPicklistValues();
		await this.setBANUnifyCustomerTypeOptions();
		await this.setPaymentMethodOptions();
		await this.setBillingCountryOptions();
		await this.setBillFormatOptions();
		await this.setBillProductionIndicatorOptions();
		await this.setBillingArrangementStatusOptions();
		//await this.setBANUnifyCustomerSubTypeOptions();

		let banResult = await getBanForAccountId({ accId: this.accountId });
		this.banObject = this.findRecordById(banResult, this.banId);
		//moved from upper call because subtype wouldnt get loaded
		await this.setBANUnifyCustomerSubTypeOptions();
		this.isBanLoading = false;
	}

	async fetchtDependentPicklistValues() {
		return getDependentPicklistValues({ field: 'Ban__c.Unify_Customer_SubType__c' })
			.then((result) => {
				if (result) {
					this.dependantSubTypeMatrix = result;
				}
			})
			.catch((error) => {
				console.log(error);
			});
	}
	async setBANUnifyCustomerTypeOptions() {
		getActivePicklistValues({ field: 'Ban__c.Unify_Customer_Type__c' })
			.then((result) => {
				let options = [];
				if (result) {
					for (let x in result) {
						options.push({ label: x, value: result[x] });
					}
				}
				this.banUnifyCustomerTypeOptions = options;
			})
			.catch((error) => {
				console.log(error);
			});
	}
	handleBANUnifyCustomerTypeChange(event) {
		this.banFieldValues[event.currentTarget.dataset.fieldName] = event.detail.value;
		this.banObject.Unify_Customer_Type__c = event.detail.value;
		this.populateFilteredCustomerSubTypeOptions();
	}
	async setBANUnifyCustomerSubTypeOptions() {
		return getActivePicklistValues({ field: 'Ban__c.Unify_Customer_SubType__c' })
			.then((result) => {
				let subtypeOptions = [];
				if (result) {
					for (let y in result) {
						subtypeOptions.push({ label: y, value: result[y] });
					}
				}
				this.allBanUnifyCustomerSubTypeOptions = subtypeOptions;
				this.populateFilteredCustomerSubTypeOptions();
			})
			.catch((error) => {
				console.log(error);
			});
	}
	populateFilteredCustomerSubTypeOptions() {
		for (let typeOption of this.banUnifyCustomerTypeOptions) {
			if (typeOption.value === this.banObject.Unify_Customer_Type__c) {
				if (this.dependantSubTypeMatrix[typeOption.label]) {
					let subtypeOptions = [];
					for (let x of this.dependantSubTypeMatrix[typeOption.label]) {
						for (let y of this.allBanUnifyCustomerSubTypeOptions) {
							if (y.label === x) {
								subtypeOptions.push({ label: y.label, value: y.value });
							}
						}
					}
					if (subtypeOptions && subtypeOptions.length > 0) {
						this.filteredBanUnifyCustomerSubTypeOptions = subtypeOptions;
					}
				}
			}
		}
	}
	handleBANUnifyCustomerSubTypeChange(event) {
		this.banFieldValues[event.currentTarget.dataset.fieldName] = event.detail.value;
		this.banObject.Unify_Customer_SubType__c = event.detail.value;
	}
	async setPaymentMethodOptions() {
		return getActivePicklistValues({ field: 'Billing_Arrangement__c.Payment_method__c' })
			.then((result) => {
				if (result) {
					let options = [];
					if (result) {
						for (let x in result) {
							options.push({ label: x, value: result[x] });
						}
					}
					if (
						this.billingArrangementObject.Payment_method__c &&
						!Object.keys(result).includes(this.billingArrangementObject.Payment_method__c)
					) {
						options.push({
							label: this.billingArrangementObject.Payment_method__c,
							value: this.billingArrangementObject.Payment_method__c
						});
					}
					this.billingArrangementPaymentMethodOptions = options;
				}
			})
			.catch((error) => {
				console.log(error);
			});
	}
	handleBillingArrangementPaymentMethodChange(event) {
		this.barFieldValues[event.currentTarget.dataset.fieldName] = event.detail.value;
		this.billingArrangementObject.Payment_method__c = event.detail.value;
		this.paymentMethodIsInvoice = event.detail.value === 'Invoice';
	}
	async setBillFormatOptions() {
		return getActivePicklistValues({ field: 'Billing_Arrangement__c.Bill_Format__c' })
			.then((result) => {
				if (result) {
					let options = [];
					if (result) {
						for (let x in result) {
							options.push({ label: x, value: result[x] });
						}
					}
					this.billingArrangementBillFormatOptions = options;
				}
			})
			.catch((error) => {
				console.log(error);
			});
	}
	handleBillingArrangementBillFormatChange(event) {
		this.barFieldValues[event.currentTarget.dataset.fieldName] = event.detail.value;
		this.billingArrangementObject.Bill_Format__c = event.detail.value;
	}
	async setBillProductionIndicatorOptions() {
		return getActivePicklistValues({
			field: 'Billing_Arrangement__c.Bill_Production_Indicator__c'
		})
			.then((result) => {
				if (result) {
					let options = [];
					if (result) {
						for (let x in result) {
							options.push({ label: x, value: result[x] });
						}
					}
					this.billingArrangementBillProductionOptions = options;
				}
			})
			.catch((error) => {
				console.log(error);
			});
	}
	handleBillingArrangementBillProductionIndicatorChange(event) {
		this.barFieldValues[event.currentTarget.dataset.fieldName] = event.detail.value;
		this.billingArrangementObject.Bill_Production_Indicator__c = event.detail.value;
	}
	async setBillingArrangementStatusOptions() {
		return getActivePicklistValues({ field: 'Billing_Arrangement__c.Status__c' })
			.then((result) => {
				if (result) {
					let options = [];
					if (result) {
						for (let x in result) {
							options.push({ label: x, value: result[x] });
						}
					}
					this.billingArrangementStatusOptions = options;
				}
			})
			.catch((error) => {
				console.log(error);
			});
	}
	handleBillingArrangementStatusChange(event) {
		this.barFieldValues[event.currentTarget.dataset.fieldName] = event.detail.value;
		this.billingArrangementObject.Status__c = event.detail.value;
	}

	async setBillingCountryOptions() {
		return getActivePicklistValues({ field: 'Billing_Arrangement__c.Billing_Country__c' })
			.then((result) => {
				if (result) {
					let options = [];
					if (result) {
						for (let x in result) {
							options.push({ label: x, value: result[x] });
						}
					}
					this.billingArrangementBillingCountryOptions = options;
				}
			})
			.catch((error) => {
				console.log(error);
			});
	}
	handleBillingArrangementBillingCountryChange(event) {
		this.barFieldValues[event.currentTarget.dataset.fieldName] = event.detail.value;
		this.billingArrangementObject.Billing_Country__c = event.detail.value;
	}

	subscribeToMessageChannel() {
		this.subscription = subscribe(this.messageContext, JOURNEY_MESSAGE_CHANNEL, (message) => {
			this.eventMessage = message;
			this.handleMessage(message);
		});
	}

	handleMessage(message) {
		if (message.currentStep === 'Billing Details') {
			this.saveBillingInformation();
		}
	}

	handleOrderBillingValidation() {
		this._orderBillingValidationLoadedCount++;
		// trigger validation flag when all the ready for order component is loaded
		if (this._orderBillingValidationLoadedCount === 3) {
			this._sendValidationNotification();
		}
	}

	/*
	@wire(getRecord, {
		recordId: "$banId",
		fields: [
			"Ban__c.BAN_Name__c",
			"Ban__c.Unify_Customer_Type__c",
			"Ban__c.Unify_Customer_SubType__c",
			"Ban__c.BAN_Status__c"
		]
	})*/
	@wire(getRecord, { recordId: '$banId' }) //had to change to the apexController method because getRecord is forcing sharing rules when used from LWC
	banFieldsHandler({ error, data }) {
		this.isBanLoading = true;
		if (data && data[0]) {
			//const isEditable = data.fields.BAN_Status__c.value === "Requested";
			const isEditable = data[0].BAN_Status__c === 'Requested';

			for (const banField of this.banFieldset) {
				if (isEditable && ['Unify_Customer_Type__c', 'Unify_Customer_SubType__c'].includes(banField.name)) {
					banField.isEditAllowed = true;
				}
			}
			this._sendValidationNotification();
		} else if (error) {
			console.log('error', JSON.stringify(error));
			this._showToast('error', 'Unable to get ban data!');
			this.isBanLoading = false;
		}

		if (!this.banId) {
			this.isBanLoading = false;
		}
		//this.isBanLoading = false;
	}

	async requestNewBAN() {
		this.isLoading = true;
		requestNew({
			recordId: this.opportunityId,
			currentBanFieldName: this.recordBanField,
			accountId: this.accountId
		})
			.then((result) => {
				this.banId = result.Id;
				this.banName = result.Name;
				this.isLoading = false;
			})
			.catch((error) => {
				console.log('Error' + error.message);
				this.isLoading = false;
			});
	}

	async saveBillingInformation() {
		if (this.required) {
			this._showRequiredMessage();
		}

		const areFieldsEmpty = this._areMandatoryFieldsEmpty();
		this._sendValidationNotification(areFieldsEmpty);

		if (areFieldsEmpty) {
			this._showToast('error', 'Please fill in all the required fields!');

			if (this.isJourney) {
				publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
			}
			return;
		}

		this.isLoading = true && !this.isJourney;
		const data = {
			saveData: {
				recordId: this.recordId,
				banId: this.banId || null,
				financialAccountId: this.financialAccountId || null,
				billingArrangementId: this.billingArrangementId || null,
				banUpdatedFields: this.banFieldValues,
				barUpdatedFields: this.barFieldValues
			}
		};
		const result = await save(data);
		if (result.variant && result.message) {
			if (
				(this.isJourney && this.banId && !this.isDeepSellOpportunity) ||
				(this.isJourney && this.banId && this.billingArrangementId && this.isDeepSellOpportunity)
			) {
				this._showToast(result.variant, result.message);
			} else if (!this.isJourney) {
				this._showToast(result.variant, result.message);
				const banSavedEvent = new CustomEvent('bansaved');
				this.dispatchEvent(banSavedEvent);
			}
		}

		this._refreshReadyForOrderFlags();

		// notify the LDS of record edit form to get latest data
		const recordsToRefresh = [this.banId, this.financialAccountId, this.billingArrangementId]
			.filter((eachValue) => !!eachValue)
			.map((eachValue) => {
				if (eachValue) {
					return { recordId: eachValue };
				}
			});
		if (recordsToRefresh.length > 0) {
			getRecordNotifyChange(recordsToRefresh);
		}

		this.isLoading = false && !this.isJourney;

		if (this.isJourney) {
			if (!this.isDeepSellOpportunity) {
				if (this.banId) {
					publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, {
						nextStep: this.eventMessage.nextStep,
						message: 'SUCCESS'
					});
				} else {
					this._showToast('error', 'BAN is not selected');
					publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
				}
			} else {
				if (this.banId && this.billingArrangementId) {
					publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, {
						nextStep: this.eventMessage.nextStep,
						message: 'SUCCESS'
					});
				} else {
					if (!this.banId) {
						this._showToast('error', 'BAN is not selected');
						publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
					}
					if (!this.billingArrangementId) {
						this._showToast('error', 'Billing Arrangement is not selected');
						publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
					}
				}
			}
		}
	}

	async onBanUpdate(event) {
		this.isFinancialAccountLoading = true;
		this.banId = event.detail.recordId;
		if (!this.banId) {
			this.clearBanObject();
		} else {
			await this.setBanInformation();
		}
		this.isFinancialAccountLoading = false;
		if (this.isDeepSellOpportunity) {
			await this.setBillingArrangement();
		} else {
			await this.setFinancialAccount();
		}
	}

	clearBanObject() {
		this.banObject = {};
	}

	async setFinancialAccount() {
		this.isFinancialAccountLoading = true;
		if (!this.banId) {
			this.template.querySelector('c-lookup.financial-account')?.setDefaultId('');
		} else {
			if (!this.financialAccountObject.Id || !this.financialAccountId) {
				const finAccountResult = await getFAForBanId({ banId: this.banId });
				this.financialAccountObject = this.findRecordById(finAccountResult, this.financialAccountId);
				if (this.financialAccountObject.Id) {
					this.financialAccountObject.ContactName = this.financialAccountObject.Financial_Contact__r.Name;

					this.financialAccountObject.ContactUrl = '';
					if (this.isPartnerUser) {
						this.financialAccountObject.ContactUrl = '/partnerportal';
					}

					this.financialAccountObject.ContactUrl += '/' + this.financialAccountObject.Financial_Contact__c;
				}

				if (finAccountResult.length === 1) {
					this.financialAccountId = finAccountResult[0].Id;
					this.financialAccountName = finAccountResult[0].Name;
				}
			}
			this._updateBanIdOnFaCreateNewConfig(this.banId);
		}
		this.isFinancialAccountLoading = false;
	}

	findRecordById(records, recordId) {
		let recordToReturn = {};
		if (records) {
			for (let record of records) {
				if (record.Id === recordId) {
					recordToReturn = record;
				}
			}
		}

		return recordToReturn;
	}

	_updateBanIdOnFaCreateNewConfig(banId) {
		let field = this.financialAccountCreateNewConfigFields.find((f) => f.name === FA_BAN_FIELD.fieldApiName);
		field.value = banId;
	}

	async onFinancialAccountUpdate(event) {
		this.isFinancialAccountLoading = true;
		this.financialAccountId = event.detail.recordId;
		if (!this.financialAccountId) {
			this.clearFinancialAccount();
		} else {
			await this.setFinancialAccount();
		}
		if (!this.isDeepSellOpportunity) {
			this.isBillingArrangementLoading = true;
			await this.setBillingArrangement();
			this.isBillingArrangementLoading = false;
			this.isFinancialAccountLoading = false;
		}
	}

	clearFinancialAccount() {
		this.financialAccountObject = {};
	}

	async setBillingArrangement() {
		this.disableBAFields();
		if ((!this.financialAccountId && !this.isDeepSellOpportunity) || (!this.banId && this.isDeepSellOpportunity)) {
			this.template.querySelector('c-lookup.billing-arrangement').setDefaultId('');
		} else {
			if (!this.billingArrangementId || !this.billingArrangementObject.Id) {
				let billingArrangementResult = {};
				if (this.isDeepSellOpportunity) {
					billingArrangementResult = await getBillingArrangementForBanId({
						banId: this.banId
					});
				} else {
					billingArrangementResult = await getBillingArrangementForFinancialAccountId({
						finAccountId: this.financialAccountId
					});
				}
				this.billingArrangementObject = this.findRecordById(billingArrangementResult, this.billingArrangementId);

				this.billingArrangementObject.Bank_Account_Number__c = this.maskBankNumber(this.billingArrangementObject.Bank_Account_Number__c);

				this.paymentMethodIsInvoice = this.billingArrangementObject.Payment_method__c === 'Invoice';

				if (billingArrangementResult.length === 1) {
					this.billingArrangementId = billingArrangementResult[0].Id;
					this.billingArrangementName = billingArrangementResult[0].Name;
				}
				if (this.billingArrangementObject.Financial_Account__c) {
					this.financialAccountId = this.billingArrangementObject.Financial_Account__c;
				}
			}
			if (!this.isDeepSellOpportunity) {
				this._updateFaOnBaCreateNewConfig(this.financialAccountId);
			}
			//this.billingArrangementObject = JSON.parse(JSON.stringify(this.billingArrangementObject));
		}
	}

	maskBankNumber(bankNumber) {
		return bankNumber?.replace(/^[\d\w-\s]+(?=\d{4})/, 'XXXXXXXXXXXXXX');
	}

	_updateFaOnBaCreateNewConfig(financialAccountId) {
		let field = this.billingArrangementCreateNewConfigFields.find((f) => f.name === BA_FINANCIAL_FIELD.fieldApiName);
		field.value = financialAccountId;
	}

	async onBillingArrangementUpdate(event) {
		this.isBillingArrangementLoading = true;
		this.billingArrangementId = event.detail.recordId;
		if (this.billingArrangementId) {
			await this.setBillingArrangement();
			await this.setPaymentMethodOptions();
		} else {
			this.clearBillingArrangementObject();
		}
		this.isBillingArrangementLoading = false;
		this.isFinancialAccountLoading = false;
		if (this.isDeepSellOpportunity) {
			await this.setFinancialAccount();
			this.isBillingArrangementLoading = false;
			this.isFinancialAccountLoading = false;
		}
	}

	clearBillingArrangementObject() {
		this.billingArrangementObject = {};
	}

	handleOnFinancialAccountLoad() {
		this.isFinancialAccountLoading = false;
	}

	handleOnBillingArrangementLoad() {
		this.isBillingArrangementLoading = false;
	}

	bindBanInputsOnChange(event) {
		this.banFieldValues[event.currentTarget.dataset.fieldName] = event.detail.value;
	}

	bindBarInputsOnChange(event) {
		this.barFieldValues[event.currentTarget.dataset.fieldName] = event.detail.value;
	}

	_refreshReadyForOrderFlags() {
		this.template.querySelector('.ba-ready-for-order').evaluateValidationError();
		this.template.querySelector('.fa-ready-for-order').evaluateValidationError();
		this.template.querySelector('.bar-ready-for-order').evaluateValidationError();
	}

	_showToast(variant, message) {
		const event = new ShowToastEvent({
			variant: variant,
			message: message
		});
		this.dispatchEvent(event);
	}

	_sendValidationNotification(areFieldsEmpty) {
		if (areFieldsEmpty === undefined) {
			areFieldsEmpty = this._areMandatoryFieldsEmpty();
		}

		const doesReadyForOrderContainError = this._doesReadyForOrderContainError();
		// eslint-disable-next-line @lwc/lwc/no-api-reassignments
		this.outputValidation = !areFieldsEmpty && !doesReadyForOrderContainError;

		const attributeChangeEvent = new FlowAttributeChangeEvent('outputValidation', this.outputValidation);
		this.dispatchEvent(attributeChangeEvent);
	}

	_areMandatoryFieldsEmpty() {
		// check if  lookups are filled in
		let areLookupsEmpty = !this.banId || !this.financialAccountId || !this.billingArrangementId;

		// check if the inputs in record edit form is filled in
		const areRequiredInputsEmpty =
			[...this.template.querySelectorAll('lightning-input-field')].filter((field) => field.required && !field.value).length !== 0;

		return this.required && (areLookupsEmpty || areRequiredInputsEmpty);
	}

	_doesReadyForOrderContainError() {
		return (
			this.required &&
			(!this.template.querySelector('.ba-ready-for-order').hasErrors() ||
				!this.template.querySelector('.fa-ready-for-order').hasErrors() ||
				!this.template.querySelector('.bar-ready-for-order').hasErrors())
		);
	}

	_showRequiredMessage() {
		// report validity on lookups
		[...this.template.querySelectorAll('c-lookup')].forEach((field) => {
			field.reportValidity();
		});

		// report validity on required inputs from record edit form
		[...this.template.querySelectorAll('lightning-input-field')]
			.filter((field) => field.required)
			.forEach((field) => {
				field.reportValidity();
			});
	}
}
