import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class OrderEntryInstallationOverstapservice extends LightningElement {
	@api b2cInternetCustomer;
	@track _operatorSwitch;
	@api
	get operatorSwitch() {
		return this._operatorSwitch;
	}
	set operatorSwitch(value) {
		this._operatorSwitch = Object.assign({}, value);
		this.setInitialValue();
	}

	@track requested;
	@track currentProvider;
	@track currentContractNumber;
	@track potentialFeeAccepted;
	@track currentContractNumberRequired;
	@track disabledOperatorSwitch;
	get operatorSwitchNotRequested() {
		return !this.requested;
	}

	get currentProviderOptions() {
		return [
			{
				label: 'KPN',
				value: 'KPN',
				regex: '^([0-9]{10})$',
				errorMessage: 'Contract number is 10 digits long'
			},
			{
				label: 'Tele2 - Overstapdesk',
				value: 'Tele2 - Overstapdesk',
				regex: '^(5[0-9]{8}|[bB][0-9]{10}|[0-9]{1,7}|NL[0-9]{8})$',
				errorMessage:
					"Contract number must be '5' followed by eight digits OR 'B' followed by ten digits OR one to seven digits OR 'NL' followed by eight digits"
			},
			{
				label: 'T-Mobile Thuis Overstapdesk',
				value: 'T-Mobile Thuis Overstapdesk',
				regex: '^[a-zA-Z]{3}[0-9]{5}$',
				errorMessage: 'The contract number consists of 3 letters followed by 5 numbers. For example ABC12345'
			},
			{
				label: 'Breedband Helmond',
				value: 'Breedband Helmond',
				regex: '^[0-9]{5}$',
				errorMessage: 'Contract number must be 5 digits'
			},
			{
				label: 'Cambrium / Tweak',
				value: 'Cambrium / Tweak',
				regex: '^([A-Za-z]{2,5}[0-9]{7,})$',
				errorMessage: 'Contract number must start with 2 to 5 letters, followed by at least 7 numbers'
			},
			{
				label: 'Caiway',
				value: 'Caiway',
				regex: '^([0-9]{1,9})$',
				errorMessage: 'Contract number is max 9 digits long'
			},
			{
				label: 'DELTA / ZeelandNet',
				value: 'DELTA / ZeelandNet',
				regex: '^([0-9]{1,9})$',
				errorMessage: 'Contract number is max 9 digits long'
			},
			{
				label: 'Belcentrale B.V.',
				value: 'Belcentrale B.V.',
				regex: '^([0-9]{3,9})$',
				errorMessage: 'Contract number is 3 to 9 digits long'
			},
			{
				label: 'Digihero B.V.',
				value: 'Digihero B.V.',
				regex: '^([0-9]{3,9})$',
				errorMessage: 'Contract number is 3 to 9 digits long'
			},
			{
				label: 'Freedom Internet B.V.',
				value: 'Freedom Internet B.V.',
				regex: '^F[0-9]{7}$',
				errorMessage: 'Contract number must start with F and 7 digits'
			},
			{
				label: 'Glasnet',
				value: 'Glasnet',
				regex: '^1[0-9]{5}$',
				errorMessage: 'Contract number must start with 1 followed by 5 digits'
			},
			{
				label: 'Kabeltex B.V.',
				value: 'Kabeltex B.V.',
				regex: '^([0-9]{6})$',
				errorMessage: 'Contract number is 6 digits long'
			},

			{
				label: 'Online.nl - Overstapdesk',
				value: 'Online.nl - Overstapdesk',
				regex: '^([0-9]{8})$',
				errorMessage: 'Contract number is 8 digits long'
			},
			{
				label: 'Kabelnoord',
				value: 'Kabelnoord',
				regex: '^[0-9]{1,9}$',
				errorMessage: 'Contract number is 1 to 9 digits long'
			},
			{
				label: 'Netvisit overstappen desk',
				value: 'Netvisit overstappen desk',
				regex: '^[0-9]{6}$',
				errorMessage: 'Contract number is 6 digits long'
			},
			{
				label: 'Helpdesk NLEX',
				value: 'Helpdesk NLEX',
				regex: '^[0-9]{11}$',
				errorMessage: 'Contract number is 11 digits long'
			},
			{
				label: 'Onvi B.V',
				value: 'Onvi B.V',
				regex: '^(1[0-9]{6}|2[0-9]{6})$',
				errorMessage: 'Contract number must consist of 7 digits and start with 1 or 2'
			},
			{
				label: 'Plinq',
				value: 'Plinq',
				regex: '^([0-9]{5})$',
				errorMessage: 'Contract number must be five digits'
			},
			{
				label: 'Qonnected',
				value: 'Qonnected',
				regex: '^[1-9]{1}[0-9]{2,3}$',
				errorMessage: 'Contract number must consist of 3 or 4 digits, of which the first digits may be 1 to 9'
			},
			{
				label: 'Patrick Paul',
				value: 'Patrick Paul',
				regex: '^((TR|ZN|ZT)[0-9]{11,16}|MTTM[0-9]{9,14})$',
				errorMessage:
					"Contract number must start with 'ZN', 'ZT', 'TR' or 'MTTM' with numbers and be thirteen to eighteen characters in total"
			},
			{
				label: 'Multifiber',
				value: 'Multifiber',
				regex: '^[0-9]{9}$',
				errorMessage: 'Contract number is 9 digits long'
			},
			{
				label: 'RapidXS',
				value: 'RapidXS',
				regex: '^[0-9]{9}$',
				errorMessage: 'Contract number is 9 digits long'
			},
			{
				label: 'Weserve',
				value: 'Weserve',
				regex: '^[0-9]{9}$',
				errorMessage: 'Contract number is 9 digits long'
			},
			{
				label: 'Orderdesk solcon',
				value: 'Orderdesk solcon',
				regex: '^([1-9]{1}[0-9]{1,11})$',
				errorMessage: 'Contract number is max 12 digits long and zero must not be the first.'
			},
			{
				label: 'Servicedesk Speakup',
				value: 'Servicedesk Speakup',
				regex: '^.*$',
				errorMessage: 'Contract number must be valid'
			},
			{
				label: 'Stichting Kabeltelevisie Pijnacker',
				value: 'Stichting Kabeltelevisie Pijnacker',
				regex: '^[0-9]{1,}$',
				errorMessage: 'Only numbers.'
			},
			{
				label: 'TNOC / TriNed',
				value: 'TNOC / TriNed',
				regex: '^([0-9]{4,10})$',
				errorMessage: 'Contract number must be between 4 and 10 digits long.'
			},
			{
				label: 'Youfone Support',
				value: 'Youfone Support',
				regex: '^(1[0-9]{9}|8[0-9]{9}|0[0-9]{6,11})$',
				errorMessage:
					'Contract number must start with a 1 or 8 and be a total of 10 digits OR start with a 0 and be between 7 and 12 digits in total'
			}
		];
	}

	setInitialValue() {
		if (this.b2cInternetCustomer) {
			this.disabledOperatorSwitch = true;
			if (this.requested || this.requested === undefined) {
				this.requested = false;
				this.cleanOperatorSwitch();
				this.dispatchStageChange();
			}
		} else {
			this.disabledOperatorSwitch = false;
			this.requested = this.operatorSwitch.requested;
			this.currentProvider = this.operatorSwitch.currentProvider;
			this.currentContractNumber = this.operatorSwitch.currentContractNumber;
			this.potentialFeeAccepted = this.operatorSwitch.potentialFeeAccepted;
			this.setCurrentContractNumberRequired();
		}
	}
	requestedChange(event) {
		this.requested = event.detail.checked;
		this.cleanOperatorSwitch();
		this.dispatchStageChange();
	}

	cleanOperatorSwitch() {
		this.currentProvider = '';
		this.currentContractNumber = '';
		this.potentialFeeAccepted = false;
	}

	handleFormInputChange(event) {
		if (event.target.type === 'toggle') {
			this.potentialFeeAccepted = event.detail.checked;
		} else {
			this[event.target.name] = event.target.value;
		}
		this.dispatchStageChange();
		this.setCurrentContractNumberRequired();
	}

	setCurrentContractNumberRequired() {
		this.currentContractNumberRequired = this.currentProvider || this.currentProvider?.length !== 0;
	}

	dispatchStageChange() {
		var value = {
			requested: this.requested,
			currentProvider: this.currentProvider,
			currentContractNumber: this.currentContractNumber,
			potentialFeeAccepted: this.potentialFeeAccepted
		};

		this.dispatchEvent(
			new CustomEvent('overstapservicestagechange', {
				detail: value,
				bubbles: false
			})
		);
	}
	@api
	validateCurrentContractNumber() {
		if (this.currentProvider && this.currentContractNumber) {
			let selectedProvider = this.currentProviderOptions.find((el) => el.value === this.currentProvider);
			const rgx = new RegExp(selectedProvider.regex);

			if (!rgx.test(this.currentContractNumber)) {
				this.dispatchMessage('Something went wrong', selectedProvider.errorMessage, 'warning');
				return false;
			}
		}
		return true;
	}

	@api
	validatePotentialTerminationFee() {
		if (this.requested) {
			if (!this.potentialFeeAccepted) {
				this.dispatchMessage(
					'Potential termination fee not accepted',
					'If Overstapservice is checked customer must accept potential termination fee at the current provider',
					'warning'
				);

				return false;
			}

			const currentInternetProviderInput = this.template.querySelector('lightning-combobox.current-internet-provider');
			currentInternetProviderInput.reportValidity();
			const currentContractNumberInput = this.template.querySelector('lightning-input.current-contract-number');
			currentContractNumberInput.reportValidity();

			return currentInternetProviderInput.checkValidity() && currentContractNumberInput.checkValidity();
		}

		return true;
	}

	dispatchMessage(title, message, variant) {
		this.dispatchEvent(
			new ShowToastEvent({
				title,
				message,
				variant
			})
		);
	}
}
