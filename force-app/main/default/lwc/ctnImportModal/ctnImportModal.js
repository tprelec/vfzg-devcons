import { LightningElement, api, track  } from 'lwc';

export default class CtnImportModal extends LightningElement {
    @api recordId;
    @track failSelected = false;
    @track fileData;
    @track separator = ';';

    @api get fileName(){        
        return Object.assign({}, this.fileData) ? Object.assign({}, this.fileData).filename : ''
    } 
    @api
    get acceptedFormats() {
        return ['.csv'];
    }
    @api
    get options() {
		return [
			{ label: ";", value: ";" },
			{ label: ",", value: "," }
		];
	}
    async handleFileUpload(event) {
        this.failSelected=true;
        const file = event.detail.files[0];        
		let reader = new FileReader();
		reader.onload = () => {
			var base64 = reader.result.split(",")[1];
            
            console.log();
            
			this.fileData =Object.assign({}, {
				filename: file.name,
				base64: base64,
				recordId: this.recordId,
				separator: this.separator
			});
            
		};
		await reader.readAsDataURL(file);        
    }
    handleClose() {
       this.dispatchEventClick("closemodal", false);
        
    }
    dispatchEventClick(eventName, eventValue) {
		this.dispatchEvent(new CustomEvent(eventName, { detail: eventValue, bubbles: false }));
	}
    handleSubmit() {
        this.fileData = Object.assign({}, this.fileData);
        this.dispatchEventClick("submit",this.fileData);
    }
    handleChangeSeparator(event) {        
        this.separator = event.target.value;
    }

}