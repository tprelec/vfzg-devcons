import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();

export default class OrderEntryInstallationInformation extends LightningElement {
	@api addressCheckResult;
	@api b2cInternetCustomer;

	@track _installation;
	@api
	get installation() {
		return this._installation;
	}
	set installation(value) {
		this._installation = Object.assign({}, value);
	}

	@track _operatorSwitch;
	@api
	get operatorSwitch() {
		return this._operatorSwitch;
	}
	set operatorSwitch(value) {
		this._operatorSwitch = Object.assign({}, value);
		this.setValues();
	}

	@track
	customerInformed;
	_minDate = null;
	_maxDate = null;
	firstPass = true;
	overflowMessage = null;
	underflowMessage = null;
	defaultEarliestInstallationDateMessage = null;

	get maxDate() {
		if (this.isOnNet()) {
			let maxDate = new Date();
			maxDate.setDate(new Date().getDate() + 90);
			this._maxDate = maxDate.toISOString().slice(0, 10);
			this.overflowMessage = String('Not allowed after ' + this._maxDate);
		}
		return this._maxDate;
	}

	get minDate() {
		this.underflowMessage = String('Not allowed before ' + this.defaultEarliestInstallationDateMessage);
		return this._minDate;
	}

	customerInformedChange(event) {
		this.customerInformed = event.detail.checked;
		event.detail.checked ? this.dispatchStateChange('customerInformed', true) : this.dispatchStateChange('customerInformed', false);
	}

	@track earliestInstallationDate;

	operatorSwitchRequested = false;

	isOnNet() {
		return this.addressCheckResult !== 'Off-Net';
	}

	// Validate Customer informed about installation date assignment process
	@api
	validate() {
		if (!this.customerInformed) {
			this.handleError('Customer not informed', 'Please inform the customer how to plan the installation appointment.');
			return false;
		}
		return this.isEarliestInstallationDateValid();
	}

	isEarliestInstallationDateValid() {
		const earliestInstallationDateInput = this.template.querySelector('lightning-input.earliest-installation-date');
		earliestInstallationDateInput.reportValidity();
		return earliestInstallationDateInput.checkValidity();
	}

	handleError(title, message) {
		this.dispatchEvent(
			new ShowToastEvent({
				title,
				message,
				variant: 'warning',
				mode: 'dismissable'
			})
		);
	}

	// set value from state
	setValues() {
		// Check if Operator Switch has been changed
		let operatorSwitchChanged = false;
		if (this.operatorSwitchRequested !== this.operatorSwitch?.requested) {
			operatorSwitchChanged = true;
			this.operatorSwitchRequested = this.operatorSwitch?.requested;
		}

		let minDateTemp = new Date();
		if (this.isOnNet()) {
			// Calculate default earliest install date
			let defaultEarliestInstallationDate = new Date();
			if (operatorSwitchChanged && this.operatorSwitch.requested) {
				defaultEarliestInstallationDate.setDate(new Date().getDate() + CONSTANTS.INSTALLATION_DATE_DAYS_OVERSTAPSERVICE);
				minDateTemp.setDate(new Date().getDate() + CONSTANTS.INSTALLATION_DATE_DAYS_OVERSTAPSERVICE - 1);
			} else {
				defaultEarliestInstallationDate.setDate(new Date().getDate() + CONSTANTS.INSTALLATION_DATE_DAYS_DEFAULT);
				minDateTemp.setDate(new Date().getDate() + CONSTANTS.INSTALLATION_DATE_DAYS_DEFAULT - 1);
			}

			if (!this.installation.earliestInstallationDate || (operatorSwitchChanged && this.firstPass !== true)) {
				this.dispatchStateChange('earliestInstallationDate', defaultEarliestInstallationDate.toISOString().slice(0, 10));
				this._minDate = minDateTemp.toISOString();
				this.defaultEarliestInstallationDateMessage = defaultEarliestInstallationDate.toISOString().slice(0, 10);
				this.validate();
				this.earliestInstallationDate = defaultEarliestInstallationDate.toISOString().slice(0, 10);
			} else {
				this.earliestInstallationDate =
					typeof this.installation.earliestInstallationDate !== 'object' ? this.installation.earliestInstallationDate : null;
				if (this.firstPass === true) {
					this.firstPass = false;
					this._minDate = minDateTemp.toISOString();
					this.defaultEarliestInstallationDateMessage = defaultEarliestInstallationDate.toISOString().slice(0, 10);
				}
				this.validate();
			}
		} else {
			this.earliestInstallationDate =
				typeof this.installation.earliestInstallationDate !== 'object' ? this.installation.earliestInstallationDate : null;
		}
		this.customerInformed = this.installation.customerInformed ? this.installation.customerInformed : false;
	}

	earliestInstallationDateChange(event) {
		this[event.target.name] = event.target.value;
		if (event.target.name === 'earliestInstallationDate') {
			this.dispatchStateChange('earliestInstallationDate', event.target.value);
		}
	}

	// dispatch chagne without preferredInstalationdDate
	dispatchStateChange(key, value) {
		this.dispatchEvent(
			new CustomEvent('statechanged', {
				detail: {
					key,
					value
				},
				bubbles: false
			})
		);
	}
}
