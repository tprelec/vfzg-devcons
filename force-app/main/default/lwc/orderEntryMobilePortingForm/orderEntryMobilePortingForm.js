import { LightningElement, track, api } from 'lwc';

export default class OrderEntryMobilePortingForm extends LightningElement {
	@track mobileNumber = {};
	@track customerId = {};
	@track _porting = {
		contractEndDate: null,
		type: 'Business',
		currentSimType: 'Subscription',
		mobileNumber: '',
		contractNumber: ''
	};

	@api changeButtonTitle;
	@api
	get porting() {
		return this._porting;
	}
	set porting(value) {
		if (value) {
			this._porting = Object.assign({}, value);
		}
	}

	@track types = [
		{ label: 'Business', value: 'Business' },
		{ label: 'Residential', value: 'Residential' }
	];

	@track currentSimTypes = [
		{ label: 'Subscription', value: 'Subscription' },
		{ label: 'Prepaid', value: 'Prepaid' }
	];

	get getButtonTitle() {
		let str = 'Add';
		if (this.changeButtonTitle) {
			str = 'Save';
		}
		return str;
	}

	get maxDate() {
		let maxDate = new Date();
		maxDate.setDate(new Date().getDate() + 120);
		return maxDate.toISOString();
	}

	// Used for Contract Number
	get getIsRequired() {
		if (this.porting.type === 'Residential' && this.porting.currentSimType === 'Prepaid') {
			return false;
		}
		return true;
	}

	submitChange(name, detail) {
		this.dispatchEvent(new CustomEvent(name, { detail, bubbles: false }));
	}

	onDelete() {
		this.submitChange('delete', {});
	}

	onCancel() {
		this.submitChange('cancel', {});
	}

	/**
	 * change proprerty
	 * @param {*} e
	 */
	handleContractEndDateChange(e) {
		this._porting.contractEndDate = e.detail.value;
	}

	/**
	 * change proprerty
	 * @param {*} e
	 */
	handleTypeChange(e) {
		this._porting.type = e.detail.value;
	}

	/**
	 * change proprerty
	 * @param {*} e
	 */
	handleCurrentSimTypeChange(e) {
		this._porting.currentSimType = e.detail.value;
	}

	/**
	 * change property
	 * @param {*} e
	 */
	handleMobileNumberChange(e) {
		this._porting.mobileNumber = e.detail.value;
		this.isMobileNumberValid();
	}

	/**
	 * change property
	 * @param {*} e
	 */
	handleContractNumberChange(e) {
		this._porting.contractNumber = e.detail.value;
	}

	/**
	 * description checking input data
	 */
	@api validate() {
		const contractEndDateInput = this.template.querySelector('lightning-input.contract-end-date');
		contractEndDateInput.reportValidity();
		const isContractEndDateValid = contractEndDateInput.checkValidity();

		const typeInput = this.template.querySelector('lightning-combobox.type');
		typeInput.reportValidity();
		const isTypeValid = typeInput.checkValidity();

		const currentSimTypeInput = this.template.querySelector('lightning-combobox.current-sim-type');
		currentSimTypeInput.reportValidity();
		const isCurrentSimTypeValid = currentSimTypeInput.checkValidity();

		const contractNumberInput = this.template.querySelector('lightning-input.contract-number');
		contractNumberInput.reportValidity();
		const isContractNumberValid = contractNumberInput.checkValidity();

		return isContractEndDateValid && this.isMobileNumberValid() && isTypeValid && isCurrentSimTypeValid && isContractNumberValid;
	}

	/**
	 * Check mobile num. regex
	 */
	validateMobileNumber(mobileNumber) {
		if (mobileNumber && mobileNumber.length) {
			const rgx = new RegExp('06[0-9]{8}');
			if (mobileNumber.length !== 10 || !rgx.test(mobileNumber)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Mobile num. custom validation
	 */
	isMobileNumberValid() {
		const mobileNumberInput = this.template.querySelector('lightning-input.mobile-number');
		if (!this.validateMobileNumber(this.porting.mobileNumber)) {
			mobileNumberInput.setCustomValidity('Mobile number must start with 06 followed by 8 digits.');
			return false;
		}
		mobileNumberInput.setCustomValidity('');
		mobileNumberInput.reportValidity();
		return true;
	}

	/**
	 * description checking data and dispatch event
	 */
	onConfirm() {
		if (!this.validate()) return;
		this.submitChange('confirm', {
			contractEndDate: this.porting.contractEndDate,
			type: this.porting.type,
			currentSimType: this.porting.currentSimType,
			mobileNumber: this.porting.mobileNumber,
			contractNumber: this.porting.contractNumber
		});
	}
}
