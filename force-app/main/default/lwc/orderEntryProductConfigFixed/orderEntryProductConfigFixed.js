import { LightningElement, api, track, wire } from 'lwc';
import { getConstants } from 'c/orderEntryConstants';
import getInternetProducts from '@salesforce/apex/OrderEntryProductController.getInternetProducts';
import getTvAndTelephonyProducts from '@salesforce/apex/OrderEntryProductController.getTvAndTelephonyProducts';
import updateOpportunityContractTerm from '@salesforce/apex/OrderEntryProductController.updateOpportunityContractTerm';
import getProductDetails from '@salesforce/apex/OrderEntryProductController.getProductDetails';
import getDefaultBundle from '@salesforce/apex/OrderEntryProductController.getDefaultBundle';
import getBundle from '@salesforce/apex/OrderEntryProductController.getBundle';
import saveFixedPortingNumbers from '@salesforce/apex/OrderEntryPortingController.saveFixedPortingNumbers';
import { APPLICATION_SCOPE, MessageContext, subscribe, unsubscribe, publish } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

const CONSTANTS = getConstants();

export default class OrderEntryProductConfigFixed extends LightningElement {
	INTERNET_PRODUCT_TYPE = CONSTANTS.INTERNET_PRODUCT_TYPE;
	TV_PRODUCT_TYPE = CONSTANTS.TV_PRODUCT_TYPE;
	TELEPHONY_PRODUCT_TYPE = CONSTANTS.TELEPHONY_PRODUCT_TYPE;

	@track activeTab = CONSTANTS.INTERNET_PRODUCT_TYPE;
	@track internetProducts = [];
	@track tvProducts = [];
	@track telephonyProducts = [];
	@track _bundle = {
		type: 'Fixed',
		contractTerm: '1',
		products: [],
		addons: [],
		details: {}
	};

	@api opportunityId;
	@api mainProduct;
	@api installationSite;

	@api
	get bundle() {
		return this._bundle;
	}
	set bundle(value) {
		if (value && JSON.stringify(this._bundle) !== JSON.stringify(value)) {
			this.setAttribute('bundle', value);
			this._bundle = Object.assign({}, value);
		}
		if (!this.internetProducts.length) {
			this.init();
		}
	}

	@wire(MessageContext)
	messageContext;

	subscription = null;
	subscribeToMessageChannel() {
		if (!this.subscription) {
			this.subscription = subscribe(this.messageContext, genericChannel, (message) => this.handleGenericMessage(message), {
				scope: APPLICATION_SCOPE
			});
		}
	}

	unsubscribeToMessageChannel() {
		unsubscribe(this.subscription);
		this.subscription = null;
	}

	connectedCallback() {
		this.subscribeToMessageChannel();
	}

	disconnectedCallback() {
		this.unsubscribeToMessageChannel();
	}

	async init() {
		if (this.mainProduct && this.opportunityId && this.installationSite) {
			// Get default bundle
			let defaultBundle = await getDefaultBundle({
				opportunityId: this.opportunityId,
				mainProductId: this.mainProduct.Id
			});
			if (!defaultBundle) {
				console.error('Default bundle missing');
			}
			// Get internet products
			this.internetProducts = await getInternetProducts({
				oppId: this.opportunityId,
				site: this.installationSite
			});
			// If nothing selected, preselect
			if (!this.selectedInternetProduct) {
				let tmpBundle = Object.assign({}, this._bundle);
				tmpBundle.products.push({
					id: defaultBundle.Internet_Product__c,
					type: CONSTANTS.INTERNET_PRODUCT_TYPE
				});
				tmpBundle.products.push({
					id: defaultBundle.TV_Product__c,
					type: CONSTANTS.TV_PRODUCT_TYPE
				});
				if (defaultBundle.Telephony_Product__c) {
					tmpBundle.products.push({
						id: defaultBundle.Telephony_Product__c,
						type: CONSTANTS.TELEPHONY_PRODUCT_TYPE
					});
				}
				tmpBundle.bundle = await this.getBundle(tmpBundle.products);
				this._bundle = Object.assign({}, tmpBundle);

				this.publishChange();
			}
			await this.getTVTelephonyProducts();
		}
	}

	async setInternetProduct(internetProductId) {
		this.addProduct(
			this.internetProducts.find((product) => {
				return product.Id === internetProductId;
			})
		);
		await this.getTVTelephonyProducts();
		this.setTvProduct(this.selectedTVProduct?.id);
		this.setTelephonyProduct(this.selectedTelephonyProduct?.id);
	}

	/**
	 * Gets all TV and Telephony Products.
	 */
	getTVTelephonyProducts() {
		return getTvAndTelephonyProducts({
			oppId: this.opportunityId,
			internetProductId: this.selectedInternetProduct.id
		}).then((products) => {
			this.tvProducts = products.filter((product) => {
				return product.Type__c === CONSTANTS.TV_PRODUCT_TYPE;
			});
			this.telephonyProducts = products.filter((product) => {
				return product.Type__c === CONSTANTS.TELEPHONY_PRODUCT_TYPE;
			});
		});
	}

	setTvProduct(tvProductId) {
		if (
			tvProductId &&
			this.tvProducts.length &&
			this.tvProducts.find((product) => {
				return product.Id === tvProductId;
			})
		) {
			if (this.selectedTVProduct?.id !== tvProductId) {
				this.removeAddons([CONSTANTS.TV_PRODUCT_TYPE]);
			}
			this.addProduct(
				this.tvProducts.find((product) => {
					return product.Id === tvProductId;
				})
			);
		} else {
			this.removeProduct(CONSTANTS.TV_PRODUCT_TYPE);
			this.removeAddons([CONSTANTS.TV_PRODUCT_TYPE]);
		}
	}

	setTelephonyProduct(telephonyProductId) {
		if (
			telephonyProductId &&
			this.telephonyProducts.length &&
			this.telephonyProducts.find((product) => {
				return product.Id === telephonyProductId;
			})
		) {
			if (this.selectedTelephonyProduct?.id !== telephonyProductId) {
				this.removeAddons([CONSTANTS.TELEPHONY_PRODUCT_TYPE]);
			}
			this.addProduct(
				this.telephonyProducts.find((product) => {
					return product.Id === telephonyProductId;
				})
			);
		} else {
			this.removeProduct(CONSTANTS.TELEPHONY_PRODUCT_TYPE);
			this.removeAddons([CONSTANTS.TELEPHONY_PRODUCT_TYPE]);
		}
	}

	setSecondaryTelephonyProduct(telephonyProductId) {
		if (
			telephonyProductId &&
			this.telephonyProducts.length &&
			this.telephonyProducts.find((product) => {
				return product.Id === telephonyProductId;
			})
		) {
			if (this.selectedSecondaryTelephonyProduct?.id !== telephonyProductId) {
				this.removeAddons([CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE]);
			}
			this.addProduct(
				this.telephonyProducts.find((product) => {
					return product.Id === telephonyProductId;
				}),
				CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE
			);
		} else {
			this.removeProduct(CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE);
			this.removeAddons([CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE]);
		}
	}

	/**
	 * Adds Order Entry Product to the bundle
	 * @param {object} oeProduct Order Entry Product
	 * @param {string} type Product Type
	 */
	addProduct(oeProduct, type) {
		let productType = type ? type : oeProduct.Type__c;
		if (!this._bundle.products) {
			this._bundle.products = [];
		}
		// Prevent duplicates
		this.removeProduct(productType);
		this._bundle.products.push({
			id: oeProduct.Id,
			type: productType
		});
	}

	/**
	 * Removes Product from the bundle
	 * @param {string} productType Product Type
	 */
	removeProduct(productType) {
		if (this._bundle.products) {
			this._bundle.products = this.bundle.products.filter((product) => {
				return product.type !== productType;
			});
		}
	}

	/**
	 * Removes Addons from the bundle
	 * @param {string} productType Product Type
	 */
	removeAddons(productTypes) {
		if (this._bundle.addons) {
			this._bundle.addons = this.bundle.addons.filter((addon) => {
				return !productTypes.includes(addon.parentType);
			});
		}
	}

	/**
	 * Handles selection of a tab
	 * @param {object} e Event
	 */
	handleSelectTab(e) {
		this.activeTab = e.detail.name;
	}

	/**
	 * Gets bundle based on the selected products
	 */
	async getBundle(bundleProducts) {
		let products = Object.assign([], bundleProducts);
		products.push({ id: this.mainProduct.Id, type: this.mainProduct.Type__c });
		return getBundle({
			products: products
		});
	}

	get isInternet() {
		return this.activeTab === CONSTANTS.INTERNET_PRODUCT_TYPE ? '' : 'slds-hide';
	}
	get isTv() {
		return this.activeTab === CONSTANTS.TV_PRODUCT_TYPE ? '' : 'slds-hide';
	}
	get isTelephony() {
		return this.activeTab === CONSTANTS.TELEPHONY_PRODUCT_TYPE ? '' : 'slds-hide';
	}
	get tvProductsShow() {
		return this.tvProducts.length > 0;
	}
	get telephonyProductsShow() {
		return this.telephonyProducts.length > 0;
	}
	get selectedInternetProduct() {
		return this.getSelectedProductByType(CONSTANTS.INTERNET_PRODUCT_TYPE);
	}
	get selectedTVProduct() {
		return this.getSelectedProductByType(CONSTANTS.TV_PRODUCT_TYPE);
	}
	get selectedTelephonyProduct() {
		return this.getSelectedProductByType(CONSTANTS.TELEPHONY_PRODUCT_TYPE);
	}
	get selectedSecondaryTelephonyProduct() {
		return this.getSelectedProductByType(CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE);
	}
	getSelectedProductByType(productType) {
		return this._bundle.products
			? this._bundle.products.filter((product) => {
					return product.type === productType;
			  })[0]
			: undefined;
	}
	get selectedInternetAddons() {
		return this.getSelectedAddonsByType([CONSTANTS.INTERNET_PRODUCT_TYPE]);
	}
	get selectedTVAddons() {
		return this.tvProductsShow ? this.getSelectedAddonsByType([CONSTANTS.TV_PRODUCT_TYPE]) : [];
	}
	get selectedTelephonyAddons() {
		return this.telephonyProductsShow
			? this.getSelectedAddonsByType([CONSTANTS.TELEPHONY_PRODUCT_TYPE, CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE])
			: [];
	}
	getSelectedAddonsByType(productTypes) {
		return this._bundle.addons
			? this._bundle.addons.filter((addon) => {
					return productTypes.includes(addon.parentType);
			  })
			: [];
	}

	@api validate() {
		const internetFormValidation = this.template.querySelector('c-order-entry-product-internet');

		const tvFormValidation = this.tvProductsShow ? this.template.querySelector('c-order-entry-product-tv').validate() : true;

		const telephonyFormValidation = this.telephonyProductsShow ? this.template.querySelector('c-order-entry-product-telephony').validate() : true;

		return internetFormValidation.validate() && tvFormValidation && telephonyFormValidation;
	}

	/**
	 * Routes Generic Messages thrown by child components
	 * @param {object} message Generic Message
	 */
	async handleGenericMessage(message) {
		if (message.sourceComponent === 'order-entry-internet-products') {
			if (message.payload.event === 'product-changed') {
				await this.handleProductChanged(message.payload.data);
			} else if (
				message.payload.event === 'rendered-changed' &&
				!this._bundle.products.find((el) => el.type === CONSTANTS.INTERNET_PRODUCT_TYPE) &&
				this._bundle.addons.length <= 0
			) {
				await this.handleProductChanged(message.payload.data);
			}
		} else if (message.sourceComponent === 'order-entry-tv-products') {
			if (message.payload.event === 'product-changed') {
				await this.handleProductChanged(message.payload.data);
			} else if (message.payload.event === 'rendered-changed' && !this._bundle.products.find((el) => el.type === CONSTANTS.TV_PRODUCT_TYPE)) {
				await this.handleProductChanged(message.payload.data);
			}
		} else if (message.sourceComponent === 'order-entry-telephony-products') {
			if (message.payload.event === 'product-changed') {
				await this.handleProductChanged(message.payload.data);
			}
		}
	}

	/**
	 * Handles change of Products
	 * @param {object} data Payload from Products
	 */
	async handleProductChanged(data) {
		let internetAddons = this.selectedInternetAddons;
		let tvAddons = this.selectedTVAddons;
		let telephonyAddons = this.selectedTelephonyAddons;

		if (data.product?.Type__c === CONSTANTS.INTERNET_PRODUCT_TYPE) {
			let details = {};
			let productDetail = await getProductDetails({ productId: data.product.Id });

			details[CONSTANTS.DOWNLOAD_SPEED] = productDetail.find((el) => el.Name.includes('Download')).Value__c + ' Mbit/s';
			details[CONSTANTS.UPLOAD_SPEED] = productDetail.find((el) => el.Name.includes('Upload')).Value__c + ' Mbit/s';
			this._bundle.details = details;
			this.setInternetProduct(data.product.Id);

			this._bundle.addons = data.addons.concat(tvAddons).concat(telephonyAddons);

			this._bundle.contractTerm = data.contractTerm;
			if (!this._bundle.products.find((el) => el.type === CONSTANTS.TV_PRODUCT_TYPE) && this.tvProducts.length) {
				this.addProduct(this.tvProducts[0], CONSTANTS.TV_PRODUCT_TYPE);
			}

			if (!this.telephonyProductsShow) {
				this._bundle.products.forEach((element) => {
					if (element.type === CONSTANTS.TELEPHONY_PRODUCT_TYPE || element.type === CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE) {
						let index = this._bundle.products.indexOf(element);
						this._bundle.products.splice(index, 1);
					}
				});
				this._bundle.telephony = {};
			}
		} else if (data.product?.Type__c === CONSTANTS.TV_PRODUCT_TYPE) {
			this.setTvProduct(data.product?.Id);
			this._bundle.addons = internetAddons.concat(telephonyAddons).concat(data.addons);
		} else {
			this.setTelephonyProduct(data.primaryProduct?.Id);
			this.setSecondaryTelephonyProduct(data.secondaryProduct?.Id);
			this._bundle.addons = internetAddons.concat(tvAddons).concat(data.addons);
			this._bundle.telephony = data.telephony;
		}

		this._bundle.bundle = await this.getBundle(this._bundle.products);

		this.publishChange();
	}

	publishChange() {
		const msg = {
			sourceComponent: 'order-entry-product-config-fixed',
			payload: {
				event: 'product-bundle-changed',
				data: {
					bundle: this._bundle
				}
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}

	@api async saveData() {
		await updateOpportunityContractTerm({ oppId: this.opportunityId, contractTerm: this._bundle.contractTerm });
		await saveFixedPortingNumbers({ oppId: this.opportunityId });
	}
}
