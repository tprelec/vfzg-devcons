import { api, track, LightningElement } from 'lwc';
import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();
const DELAY = 400;

export default class OrderEntryTelephonyProductForm extends LightningElement {
	@api state = null;
	@api products = [];
	@api disableable = false;
	@api title = 'Telephony';
	@api productType = CONSTANTS.TELEPHONY_PRODUCT_TYPE;
	@api disabled = false;
	@track selectedProductId;
	@track _selectedProduct = {};
	@track _settings;
	@api
	get selectedProduct() {
		return this._selectedProduct;
	}
	set selectedProduct(value) {
		this._selectedProduct = Object.assign({}, value);
		this.selectedProductId = this.selectedProduct?.id;
	}
	@api
	get settings() {
		return this._settings;
	}
	set settings(value) {
		this._settings = Object.assign({}, value);
	}

	get isActive() {
		return this.settings[this.productType]?.enabled || !this.disableable;
	}

	get allDisabled() {
		return !this.isActive;
	}

	get isToggleDisabled() {
		return !this.selectedProduct?.id;
	}

	get formattedProducts() {
		return this.products.map((p) => ({
			value: p.Id,
			label: p.Name
		}));
	}

	get portingNumberEnabled() {
		return this.settings[this.productType]?.portingEnabled;
	}

	get portingNumberValue() {
		return this.settings[this.productType]?.portingNumber || null;
	}

	get isPortingNumberRequired() {
		return this.portingNumberEnabled;
	}

	get isPortingNumberDisabled() {
		return !this.portingNumberEnabled;
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */
	@api validate() {
		const input = this.template.querySelector('lightning-input[data-type="porting-number"]');
		input.reportValidity();
		return input.checkValidity();
	}

	@api removeForm() {
		this.dispatchEvent(
			new CustomEvent('selectedproduct', {
				detail: { id: null, type: this.productType },
				bubbles: false
			})
		);
	}
	/**
	 * handle product change
	 * @param {*} e
	 */
	handleProductChange(e) {
		this.dispatchEvent(
			new CustomEvent('selectedproduct', {
				detail: { id: e.detail.value, type: this.productType },
				bubbles: false
			})
		);
	}

	/**
	 * handle portingnumber change
	 * @param {*} e
	 */
	handlePortingNumberChange(e) {
		window.clearTimeout(this.delayTimeout);
		const telephonySettings = this.settings ? this.settings : {};
		const value = e.detail.value;

		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			telephonySettings[this.productType] = Object.assign({}, telephonySettings[this.productType] || {}, {
				portingNumber: value
			});

			this.dispatchEvent(
				new CustomEvent('updatedsettings', {
					detail: telephonySettings,
					bubbles: false
				})
			);
		}, DELAY);
	}

	/**
	 * handle porting status change
	 * @param {*} e
	 */
	handlePortingStatusChange(e) {
		const telephonySettings = this.settings ? this.settings : {};
		const checked = e.detail.checked;

		telephonySettings[this.productType] = Object.assign({}, telephonySettings[this.productType] || {}, {
			portingEnabled: checked,
			portingNumber: null
		});

		if (!checked) {
			// eslint-disable-next-line @lwc/lwc/no-async-operation
			setTimeout(() => {
				const input = this.template.querySelector('lightning-input[data-type="porting-number"]');
				input.reportValidity();
			}, 10);
		}

		this.dispatchEvent(
			new CustomEvent('updatedsettings', {
				detail: telephonySettings,
				bubbles: false
			})
		);
	}

	handleLineStatusChange(e) {
		const telephonySettings = this.settings ? this.settings : {};

		if (e.detail.checked) {
			telephonySettings[this.productType] = Object.assign({}, telephonySettings[this.productType] || {}, {
				enabled: e.detail.checked
			});
		} else {
			// eslint-disable-next-line @lwc/lwc/no-async-operation
			setTimeout(() => {
				const input = this.template.querySelector('lightning-input[data-type="porting-number"]');
				input.reportValidity();
			}, 10);

			delete telephonySettings[this.productType];
			this.handleRemoveProduct();
		}

		this.dispatchEvent(
			new CustomEvent('updatedsettings', {
				detail: telephonySettings,
				bubbles: false
			})
		);
	}

	handleRemoveProduct() {
		if (this.selectedProduct) {
			this.dispatchEvent(
				new CustomEvent('selectedproduct', {
					detail: { id: null, type: this.productType },
					bubbles: false
				})
			);
		}
	}
}
