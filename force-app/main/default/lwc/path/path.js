import { LightningElement, api, track, wire } from 'lwc';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

export default class Path extends LightningElement {
	@track pathSteps = [];

	@api
	get steps() {
		return this.pathSteps;
	}
	set steps(value) {
		let tempSteps = [];
		value.forEach((step) => {
			let tempStep = Object.assign({}, step);
			tempStep.computedClass = 'slds-path__item slds-is-' + tempStep.status;
			tempSteps.push(tempStep);
		});
		this.pathSteps = tempSteps;
	}

	@wire(MessageContext)
	messageContext;

	stepSelected(event) {
		const msg = {
			sourceComponent: 'path',
			payload: {
				event: 'step-selected',
				data: {
					selectedStep: event.currentTarget.getAttribute('alternative-text')
				}
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}
}
