/**
 * NewSpecialAppointment
 * Used with zipDeliveryPlanning component to show a modal screen to add a new special appointment
 * (appointments without any related Delivery Order)
 * @author: Sebastian Melgin
 */
import { LightningElement, track, api } from 'lwc';
import btnCancel from '@salesforce/label/c.appointmentBtnCancel';
import formTitle from '@salesforce/label/c.newSpecialAppointmentTitle';
import formSubtitle from '@salesforce/label/c.newSpecialAppointmentSubtitle';
import btnCreate from '@salesforce/label/c.appointmentBtnCreate';
import createNewContact from '@salesforce/label/c.createNewContact';

const EVENT_NEW_APPOINTMENT = 'new';

export default class NewSpecialAppointment extends LightningElement {
  labels = {
    btnCancel,
    btnCreate,
    formTitle,
    formSubtitle,
    createNewContact
  };
  isModalOpen;
  @api fieldEngineerId; // 0052p00000A5TcuAAF
  @api queueId; // 00G1w000003GcUmEAK
  dtNow = new Date();
  // Fields to put into a hidden field ...
  // OwnerId, Field_Engineer__c
  fieldSet = [
    { nm: 'Name', dv: '' },
    { nm: 'Start__c', dv: this.dtNow.toISOString() },
    { nm: 'Duration__c', dv: 120 },
    { nm: 'Location__c', dv: '' },
    { nm: 'Description__c', dv: '' }
  ];
  selectedContactId = '';

  // Used to close the modal without changing anything on the filters.
  handleClose() {
    this.isModalOpen = false;
    this.selectedContactId = '';
  }

  @api openModal() {
    this.isModalOpen = true;
  }

  handleSubmit(event) {
    event.preventDefault();       // stop the form from submitting
    let fields = event.detail.fields;
    fields.OwnerId = this.queueId;
    fields.Field_Engineer__c = this.fieldEngineerId;
    this.template.querySelector('lightning-record-edit-form').submit(fields);
  }

  handleSuccess(event) {
    const result = event.detail;
    const newAppt = {
      id: result.id,
      title: result.fields.Name.value,
      start: result.fields.Start__c.value,
      duration: result.fields.Duration__c.value,
      location: result.fields.Location__c.value
    };
    this.dispatchEvent(new CustomEvent(EVENT_NEW_APPOINTMENT, {
      detail: JSON.stringify(newAppt)
    }));
    this.handleClose();
  }

  handleError(event) {
    const payload = event;
    console.log('There is an error trying to save ... ');
    console.log(JSON.stringify(payload));
  }

  handleNewContact() {
    this.template.querySelector('c-create-new-contact').openModal();
  }

  handleOnNewContact(event) {
    let result = JSON.parse(event.detail);
    this.selectedContactId = result.id;
  }
}