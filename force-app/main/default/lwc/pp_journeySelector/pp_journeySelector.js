import { LightningElement, api, wire, track } from "lwc";

import LABEL_LOADING from "@salesforce/label/c.pp_Loading";

import getBppJourneys from "@salesforce/apex/PP_SearchAccountController.getBppJourneys";

export default class Pp_journeySelector extends LightningElement {
	label = {
		LABEL_LOADING
	};

	@api disabledComponent;
	@track bppJourneys;

	isLoading = true;
	accountIdLWC;

	@api
	get accountId() {
		return this.accountIdLWC;
	}
	set accountId(value) {
		this.isLoading = true;
		this.accountIdLWC = value;
	}

	get showBPPJourneyButtons() {
		return !!this.bppJourneys && !this.isLoading;
	}

	@wire(getBppJourneys, { accountId: "$accountIdLWC" })
	bppJourneysWired({ error, data }) {
		if (data) {
			this.bppJourneys = data;
			this.isLoading = false;
		} else if (error) {
			this.bppJourneys = undefined;
			this.isLoading = false;
		}
	}
}