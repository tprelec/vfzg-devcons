import { api, LightningElement } from 'lwc';
import SECTION_HEADER from '@salesforce/label/c.Service_Resource_Section';

export default class ServiceApptBusinessConsultantReview extends LightningElement {
	label = {
		header: SECTION_HEADER
	};

	@api
	businessConsultantId;

	load() {
		this.dispatchEvent(new CustomEvent('loadcomplete'));
	}
}
