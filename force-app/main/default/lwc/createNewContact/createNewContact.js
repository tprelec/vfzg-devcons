import { LightningElement, api } from 'lwc';
import btnCancel from '@salesforce/label/c.appointmentBtnCancel';
import formTitle from '@salesforce/label/c.newSpecialAppointmentTitle';
import formSubtitle from '@salesforce/label/c.newSpecialAppointmentSubtitle';
import btnCreate from '@salesforce/label/c.appointmentBtnCreate';
import createNewContact from '@salesforce/label/c.createNewContact';
import createNewContactSubtitle from '@salesforce/label/c.createNewContactSubtitle';
import createContact from '@salesforce/label/c.createContact';
import successTitle from '@salesforce/label/c.successTitle';
import contactSuccessfullyCreated from '@salesforce/label/c.contactSuccessfullyCreated';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class NewSpecialAppointment extends LightningElement {
   labels = {
     btnCancel,
     btnCreate,
     formTitle,
     formSubtitle,
     createNewContact,
     createNewContactSubtitle,
     createContact,
     successTitle,
     contactSuccessfullyCreated
   };
   isModalOpen;

   // Fields to put into a hidden field ...
   // OwnerId, Field_Engineer__c
   fieldSet = [
     { nm: 'Salutation', dv: '' },
     { nm: 'FirstName', dv: '' },
     { nm: 'LastName', dv: '' },
     { nm: 'AccountId', dv: '' },
     { nm: 'Email', dv: '' },
     { nm: 'Phone', dv: '' }
   ];
   // Used to close the modal without changing anything on the filters.
   handleClose() {
     this.isModalOpen = false;
   }
 
   @api openModal() {
     this.isModalOpen = true;
   }
 
   handleSubmit(event) {
     event.preventDefault();       // stop the form from submitting
     let fields = event.detail.fields;
     fields.OwnerId = this.queueId;
     fields.Field_Engineer__c = this.fieldEngineerId;
     this.template.querySelector('lightning-record-edit-form').submit(fields);
   }
 
   handleSuccess(event) {
    const result = event.detail;
    const newContact = {
      id: result.id
    };
    this.dispatchEvent(new CustomEvent('newcontact', {
      detail: JSON.stringify(newContact)
    }));
     this.handleClose();
     this.fireToaster(this.labels.successTitle, this.labels.contactSuccessfullyCreated, 'success');
   }
 
   handleError(event) {
     const payload = event;
     console.log('There is an error trying to save ... ');
     console.log(JSON.stringify(payload));
   }

   // fires the toaster
   fireToaster(pTitle, pMessage, pVariant) {
     const toasty = new ShowToastEvent({
       title: pTitle,
       message: pMessage,
       variant: pVariant,
       mode: 'dismissable'
     });
     this.dispatchEvent(toasty);
   }
}