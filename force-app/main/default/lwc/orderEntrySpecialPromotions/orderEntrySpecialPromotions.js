import { api, track, LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();

export default class OrderEntryBundlePromotion extends LightningElement {
	@api state = null;

	@api site;
	@api b2cInternetCustomer;

	@track _bundle;
	@api
	get bundle() {
		return this._bundle;
	}
	set bundle(value) {
		this._bundle = Object.assign({}, value);
	}

	@api promotions = [];

	@track selectedPromo = {};
	@track selectedPromotions = [];

	get formattedPromotions() {
		return this.promotions.map((sp) => ({
			value: sp.Id,
			label: sp.Promotion_Name__c
		}));
	}

	//SFO-2443
	lstAddonsByPromotion = [];
	lstSelectedAddons = [];

	connectedCallback() {
		const specialPromotions = this.bundle?.promos ? this.bundle.promos.filter((p) => p.type === CONSTANTS.SPECIAL_PROMOTION_TYPE) : [];

		//SFO-2443
		this.promotions.forEach((promotion) => {
			promotion.Discounts__r.forEach((discount) => {
				let lstAddonsTemp = [];

				if (this.lstAddonsByPromotion.find((map) => map.key === promotion.Id)) {
					lstAddonsTemp = this.lstAddonsByPromotion.find((map) => map.key === promotion.Id).value;
				}

				lstAddonsTemp.push(discount.Discount__r.Add_On__c);
				this.lstAddonsByPromotion.push({ key: promotion.Id, value: lstAddonsTemp });
			});
		});

		specialPromotions.forEach((sp) => {
			this.selectedPromotions.push({
				value: sp.id,
				label: sp.name
			});
			//SFO-2443
			this.lstSelectedAddons.push(...this.lstAddonsByPromotion.find((map) => map.key === sp.id).value);
		});
	}

	handlePromotionChange(e) {
		const id = e.detail.value;
		const name = e.target.options.find((opt) => opt.value === id).label;
		this.selectedPromo = { value: id, label: name };
	}

	/**
	 * Handle adding a special promotion
	 */
	addSpecialPromotion() {
		let promotionIndex = -1;
		let addonIndex = -1;
		let lstAddons = [];

		//SFO-2443
		if (this.selectedPromo.value) {
			lstAddons = this.lstAddonsByPromotion.find((map) => map.key === this.selectedPromo.value).value;

			promotionIndex = this.selectedPromotions.findIndex((sp) => sp.value === this.selectedPromo.value);

			for (let addonId of lstAddons) {
				addonIndex = this.lstSelectedAddons.findIndex((selectedAddonId) => selectedAddonId === addonId);
				if (addonIndex !== -1) {
					break;
				}
			}
		}

		if (promotionIndex === -1 && addonIndex === -1 && this.selectedPromo.value) {
			this.selectedPromotions.push(this.selectedPromo);
			this.lstSelectedAddons.push(...lstAddons);
			const id = this.selectedPromo.value;
			const type = CONSTANTS.SPECIAL_PROMOTION_TYPE;
			const promotionData = { id, type };

			const promotionPicklist = this.template.querySelector('lightning-combobox.special-promotions');
			promotionPicklist.value = null;
			this.selectedPromo = {};

			this.dispatchEvent(new CustomEvent('selectedpromotion', { detail: promotionData, bubbles: false }));
		} else if (!this.selectedPromo.value) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'No Promotion selected',
					message: 'Select a Promotion first',
					variant: 'warning'
				})
			);
		} else if (promotionIndex !== -1) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Unable to add Promotion',
					message: 'Promotion already added',
					variant: 'warning'
				})
			);
		} else if (addonIndex !== -1) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Unable to add Promotion',
					message: 'A promotion with the same category has been already added',
					variant: 'warning'
				})
			);
		}
	}

	/**
	 * Handle removing a special promotion
	 */
	removeSpecialPromotion(e) {
		const index = e.target.value;
		const id = this.selectedPromotions[index].value;
		this.selectedPromotions.splice(index, 1);

		//SFO-2443
		this.lstAddonsByPromotion
			.find((map) => map.key === id)
			.value.forEach((addonId) => {
				let addonIndex = this.lstSelectedAddons.findIndex((selectedAddon) => selectedAddon === addonId);
				this.lstSelectedAddons.splice(addonIndex, 1);
			});

		this.dispatchEvent(new CustomEvent('removespecialpromotion', { detail: id, bubbles: false }));
	}
}
