import { LightningElement, api } from 'lwc';
import getErrors from '@salesforce/apex/OrderBillingValidation.getErrors';
import getOpportunity from '@salesforce/apex/OrderBillingValidation.getOpportunity';

export default class OrderBillingValidation extends LightningElement {
	errorMessage;
	result;
	isLoading;

	@api sourceRecordId;
	@api opportunityId;
	@api showRequired;

	_billingObjectRecordId;
	_skipFlag = false;

	async checkSkipCriteria() {
		await getOpportunity({
			oppId: this.opportunityId,
			fields: ['Select_Journey__c']
		})
			.then((res) => {
				this._skipFlag = res.Select_Journey__c === 'Add Mobile Product' ? true : false;
			})
			.catch(() => {
				this._skipFlag = false;
			});
	}

	@api
	set billingObjectRecordId(value) {
		this._billingObjectRecordId = value;
		if (value !== undefined) {
			this.evaluateValidationError();
		}
	}
	get billingObjectRecordId() {
		return this._billingObjectRecordId;
	}

	get isSuccess() {
		this.checkSkipCriteria();
		if (this._skipFlag) {
			return true;
		}
		return this.hasErrors();
	}

	@api
	hasErrors() {
		return this.result !== undefined && this.result?.length === 0 && !this.errorMessage;
	}

	@api
	async evaluateValidationError() {
		this.errorMessage = undefined;
		this.result = undefined;

		if (this.billingObjectRecordId) {
			this.isLoading = true;
			let data = await getErrors({
				billingObjectRecordId: this.billingObjectRecordId,
				sourceRecordId: this.sourceRecordId
			});

			if (data) {
				this.result = data.body;
				if (data.message) {
					this.errorMessage = data.message;
				}
			}
		}
		this.dispatchEvent(new CustomEvent('load'));
		this.isLoading = false;
	}

	get hasRecordId() {
		return !!this.billingObjectRecordId;
	}

	get errors() {
		const resultWithKeys = this.result?.map((x, index) => {
			return { key: index, value: x };
		});
		return resultWithKeys;
	}

	get messagePanelContainerClass() {
		return `${this.showRequired ? 'error-message-panel-container' : 'slds-text-color_weak'}`;
	}

	get messagePanelClass() {
		return `${this.showRequired ? 'error-message-panel' : ''}`;
	}
}
