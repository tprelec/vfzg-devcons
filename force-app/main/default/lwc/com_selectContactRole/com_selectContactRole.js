import { api, LightningElement, track, wire } from 'lwc';
import { CloseActionScreenEvent } from 'lightning/actions';
import { refreshApex } from '@salesforce/apex';

import CONTACT_ACCOUNT_ID_FIELD from '@salesforce/schema/Contact.AccountId';
import CONTACT_EMAIL_FIELD from '@salesforce/schema/Contact.Email';

import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import getData from '@salesforce/apex/Com_SelectContactRoleController.getData';
import save from '@salesforce/apex/Com_SelectContactRoleController.save';

export default class Com_selectContactRole extends LightningElement {
	accountId;
	isLoading = true;
	@api recordId;
	@api objectApiName;

	@track rolesData;

	get contactFilter() {
		return `${CONTACT_ACCOUNT_ID_FIELD.fieldApiName} = '${this.accountId}' AND ${CONTACT_EMAIL_FIELD.fieldApiName} != NULL`;
	}

	get activeSections() {
		return ['section1'];
	}

	@wire(getData, { opportunityId: '$recordId' })
	wiredGetData(value) {
		// Hold on to the provisioned value so we can refresh it later.
		this.wiredContactRoleData = value;

		// Destructure the provisioned value
		const { data, error } = value;
		this.isLoading = true;
		if (data) {
			const response = data?.body;
			if (response) {
				this.accountId = response?.accountId;
				// response from wire is read only, clone the object for regular usage
				this.rolesData = { ...response };
			}
			if (data.variant && data.message) {
				this._showToast(data.variant, data.message);
			}
			this.isLoading = false;
		} else if (error) {
			console.log('error', JSON.stringify(error));
			this._showToast('error', error?.body?.message);
			this.isLoading = false;
		}
	}

	onCustomerMainContactUpdate(event) {
		this.rolesData.customerMainContactId = event.detail.recordId || null;
	}

	onCustomerMaintenanceContactUpdate(event) {
		this.rolesData.customerMaintenanceContactId = event.detail.recordId || null;
	}

	onCustomerIncidentContactUpdate(event) {
		this.rolesData.customerIncidentContactId = event.detail.recordId || null;
	}

	onCustomerChooserContactUpdate(event) {
		this.rolesData.customerChooserContactId = event.detail.recordId || null;
	}

	async saveContacts() {
		if (!this.rolesData.customerMainContactId) {
			this._showToast('error', 'Please fill in the required fields');
			this.template.querySelector('.main-contact-lookup').reportValidity();
			return;
		}

		this.isLoading = true;

		const data = {
			rolesData: this.rolesData
		};

		const result = await save(data);

		if (result.variant && result.message) {
			this._showToast(result.variant, result.message);
		}

		if (result.variant === 'success') {
			this.handleCancel();
		}

		refreshApex(this.wiredContactRoleData);
		this.isLoading = false;
	}

	handleCancel() {
		this.dispatchEvent(new CloseActionScreenEvent());
	}

	_showToast(variant, message) {
		const event = new ShowToastEvent({
			variant: variant,
			message: message
		});
		this.dispatchEvent(event);
	}
}
