import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadScript } from 'lightning/platformResourceLoader';
import pdfjs from '@salesforce/resourceUrl/pdfjs';
import getContent from '@salesforce/apex/PdfViewerController.getContent';

export default class PdfViewer extends LightningElement {
	@api
	fileId;
	file;
	isFilePreviewed = false;
	pdfJsInitialized = false;
	pdfjs;
	pdf;
	@track
	loading = false;
	index = 0;
	numberOfPDF = 0;

	get className() {
		this.index++;
		return 'canvas-container canvas-container-' + this.index;
	}

	async renderedCallback() {
		if (this.pdfJsInitialized) {
			return;
		}
		this.pdfJsInitialized = true;
		await Promise.all([loadScript(this, pdfjs + '/pdfjs/pdf.js')])
			.then(() => {
				this.initPdfJs();
			})
			.catch((error) => {
				this.handleError(error);
			});
	}

	initPdfJs() {
		this.pdfjs = pdfjsLib;
		this.pdfjs.GlobalWorkerOptions.workerSrc = pdfjs + '/pdfjs/pdf.worker.js';
	}

	@api
	preview() {
		this.index = 0;
		this.numberOfPDF = 0;
		this.loading = true;
		if (!this.pdfjs) {
			this.handleError({ body: 'PDF Library not initialized' });
			return;
		}
		if (!this.fileId) {
			this.handleError({ body: 'Please provide PDF Id' });
			return;
		}

		// eslint-disable-next-line consistent-return
		return getContent({ fileIds: this.fileId })
			.then((results) => {
				if (results) {
					const loadingTasks = [];
					results
						.sort((a, b) => b.name.localeCompare(a.name))
						.forEach((result) => {
							this.file = result;
							this.file.data = atob(this.file.data);
							this.isFilePreviewed = true;
							this.loading = false;
							loadingTasks.push(
								this.pdfjs.getDocument({
									data: this.file.data,
									ownerDocument: document
								})
							);
						});
					this.renderPDF(loadingTasks);
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	async renderPDF(loadingTasks) {
		this.index = 0;
		await loadingTasks[this.numberOfPDF].promise
			.then(async (pdf) => {
				this.pdf = pdf;
				this.numberOfPDF++;
				this.template.querySelector('div.canvas-container-' + this.numberOfPDF).innerHTML = '';
				await this.renderPDFPage(1, true, this.numberOfPDF);
				await this.sleep(0);
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			})
			.finally(() => {
				if (loadingTasks.length > this.numberOfPDF) {
					this.renderPDF(loadingTasks);
				}
			});
	}

	async renderPDFPage(pageNumber, renderNext, pdfFileId) {
		if (this.pdf.numPages < pageNumber) {
			return;
		}
		await this.pdf.getPage(pageNumber).then(async (page) => {
			var scale = 1.5;
			var viewport = page.getViewport({ scale: scale });
			// Prepare canvas using PDF page dimensions
			var canvas = document.createElement('canvas');
			var context = canvas.getContext('2d');
			canvas.height = viewport.height;
			canvas.width = viewport.width;
			canvas.style.marginTop = '20px';
			// Render PDF page into canvas context
			const renderContext = {
				canvasContext: context,
				viewport: viewport
			};
			await page.render(renderContext);

			this.template.querySelector('div.canvas-container-' + pdfFileId).appendChild(canvas);
			if (renderNext) {
				this.renderPDFPage(pageNumber + 1, true, pdfFileId);
			}
		});
	}

	handleError(error) {
		this.loading = false;
		this.isFilePreviewed = false;
		this.dispatchEvent(
			new ShowToastEvent({
				title: 'Something went wrong',
				message: error.body ? error.body.message : error.message,
				variant: 'warning'
			})
		);
	}

	@api
	close() {
		this.isFilePreviewed = false;
	}

	async sleep(ms) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}
}
