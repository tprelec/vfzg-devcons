/**
 * Created by rahul on 14-07-2021.
 */

import { LightningElement, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import getBasket from '@salesforce/apex/OppDocumentGenerationController.getBasket';
import getContracts from '@salesforce/apex/OppDocumentGenerationController.getContracts';
import createDocument from '@salesforce/apex/OppDocumentGenerationController.createDocument';
import sendForApproval from '@salesforce/apex/OppDocumentGenerationController.sendForApproval';
import basketNotPrimaryMessage from '@salesforce/label/c.OppDocumentGeneration_BasketNotPrimaryMessage';
import creditCheckNotApproved from '@salesforce/label/c.OppDocumentGeneration_CreditCheckNotApproved';
import basketIsPrimaryMessage from '@salesforce/label/c.OppDocumentGeneration_BasketIsPrimaryMessage';
import basketNotApprovedMessage from '@salesforce/label/c.OppDocumentGeneration_BasketNotApprovedMessage';
import basketApprovedMessage from '@salesforce/label/c.OppDocumentGeneration_BasketApprovedMessage';
import cardTitle from '@salesforce/label/c.OppDocumentGeneration_CardTitle';
import createDocumentButton from '@salesforce/label/c.OppDocumentGeneration_CreateDocumentButton';
import confirmationMessage from '@salesforce/label/c.OppDocumentGeneration_SendApprovalRequestConfirmationMessage';
import customURL from '@salesforce/label/c.pp_Community_Base_URL';
import signatureOtherMethodMessage from '@salesforce/label/c.OppDocumentGeneration_SignatureOtherMethodMessage';
import Id from '@salesforce/user/Id';

export default class OppDocumentGeneration extends NavigationMixin(LightningElement) {
	@api opportunityId;
	label = {
		cardTitle,
		basketNotPrimaryMessage,
		basketIsPrimaryMessage,
		basketNotApprovedMessage,
		basketApprovedMessage,
		createDocumentButton,
		signatureOtherMethodMessage,
		customURL,
		creditCheckNotApproved,
		confirmationMessage
	};
	isLoading = true;
	openModal = false;
	basket;
	userType;

	@wire(getRecord, { recordId: Id, fields: ['User.UserType'] })
	wireuser({ error, data }) {
		if (error) {
			this.isLoading = false;
			this.showToast('error', error.body.message);
		} else if (data) {
			this.userType = data.fields.UserType.value;
			this.isLoading = false;
		}
	}

	@wire(getBasket, { opportunityId: '$opportunityId' })
	wiredBasket({ error, data }) {
		if (data) {
			this.basket = data.body;
			this.isLoading = false;
		} else if (error) {
			this.isLoading = false;
			this.showToast('error', error.body.message);
		}
	}

	get basketRecordLink() {
		if (this.userType == 'PowerPartner') {
			return this.label.customURL + `/apex/csbb__CSBasketRedirect?id=${this.basket.id}&isdtp=mn`;
		} else {
			return `/${this.basket.id}`;
		}
	}

	get disableCreateDocument() {
		return this.isLoading === true;
	}

	get disableSendForApproval() {
		return !this.basket.isStatusApproved || this.isLoading;
	}

	async handleCreateDocument() {
		this.isLoading = true;
		createDocument({ opportunityId: this.opportunityId })
			.then((result) => {
				this.showToast(result.variant, result.message);
				if (result.variant === 'success') {
					// notify parent LWC component to fire force:refreshView for refreshing the page; to show new agreement record
					this.dispatchEvent(new CustomEvent('refreshView'));
				}
			})
			.catch((error) => {
				this.showToast('error', error.body.message);
			})
			.finally(() => {
				this.isLoading = false;
			});
	}

	async confirmContractRecords() {
		this.isLoading = true;
		getContracts({
			opportunityId: this.opportunityId
		})
			.then((result) => {
				if (result) {
					const contracts = result.body;
					if (contracts.length > 0) {
						console.log('Length ' + contracts.length);
						this.handleSendForApproval();
					} else {
						this.openModal = true;
					}
				}
			})
			.catch((error) => {
				this.isLoading = false;
				this.showToast('error', error.body.message);
			});
	}

	handleSendForApproval() {
		this.isLoading = true;
		sendForApproval({
			opportunityId: this.opportunityId,
			documentSignature: this.basket.documentSignature
		})
			.then((result) => {
				if (result.message) {
					this.showToast(result.variant, result.message);
				}
				if (result.variant === 'success') {
					// notify parent LWC component to fire force:refreshView for refreshing the page; to show new agreement record
					this.dispatchEvent(new CustomEvent('refreshView'));
				}
				const url = result?.body?.url;
				if (url) {
					this[NavigationMixin.GenerateUrl]({
						type: 'standard__webPage',
						attributes: {
							url: url
						}
					}).then((generatedUrl) => {
						// eslint-disable-next-line @lwc/lwc/no-async-operation
						setTimeout(() => {
							window.open(generatedUrl);
						}, 2000);
					});
				}
			})
			.catch((error) => {
				this.showToast('error', error.body.message);
			})
			.finally(() => {
				this.isLoading = false;
			});
	}

	handleConfirmation(event) {
		this.openModal = false;
		console.log(event.detail.value);
		if (event.detail.value) {
			this.handleSendForApproval();
		}
	}

	showToast(variant, message) {
		const event = new ShowToastEvent({
			variant: variant,
			message: message
		});
		this.dispatchEvent(event);
	}
}
