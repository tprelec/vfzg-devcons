import { LightningElement } from 'lwc';

export default class Cs_package_searchbox extends LightningElement {

    toogleResults = false;

    productType = '';
    addressId;
    addressName = '';

    get options() {
        return [
            { label: 'Business Internet', value: 'Business Internet' },
            { label: 'Business WiFi', value: 'Business WiFi' },
            { label: 'Managed Switch', value: 'Managed Switch' },
            { label: 'Mobile', value: 'Mobile' },
            { label: 'One Net', value: 'One Net' }
        ];
    }    

    addresses = [
        {
            'id': 'a4D3O0000004VVjUAM',
            'name': 'EINDHOVEN, Don Boscostraat, 951  - 5611KW',
            'vendor': 'ZIGGO',
            'technology': 'Coax'
        },
        {
            'id': 'a4D3O0000004VWCUA2',
            'name': 'Rotterdam, Driemanssteeweg, 805  - 3084CB',
            'vendor': 'ZIGGO',
            'technology': 'Coax'
        }
    ];

    productTypeChanged(event) {
        console.log('productTypeChanged...');
        this.productType = event.detail.value;
    }
    
    addressChanged(event) {

        if (event.target.value && event.target.value.length > 3) {
            console.log(event.target.value);
            this.addressName = event.target.value;
            this.toogleResults = true;
        }

        if (!event.target.value) {
            this.toogleResults = false;
            this.addressId = undefined;
            this.addressName = '';
        }
    }

    selectAddress(event) {
        console.log('selectAddress...');
        this.addressId = event.currentTarget.dataset.addressid;
        this.addressName = event.currentTarget.dataset.addressname;
        console.log(this.addressId);
        console.log(this.addressName);
        this.toogleResults = false;
    }
}