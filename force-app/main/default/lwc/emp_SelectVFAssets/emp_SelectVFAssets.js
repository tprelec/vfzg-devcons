import { LightningElement, api, track, wire } from 'lwc';
import getVFAssetList from '@salesforce/apex/EMP_ChangeProductSettingsController.getVFAssetList';
import createOLI from '@salesforce/apex/EMP_ChangeProductSettingsController.createOLI';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import STAGE_FIELD from '@salesforce/schema/Opportunity.StageName';
import JOURNEY_FIELD from '@salesforce/schema/Opportunity.Select_Journey__c';

export default class Emp_SelectVFAssets extends LightningElement {
    mapAllSelection;
    @api recordId;
    @api isQuickAction = false;
    showSpinner = false;
    //new for pagination
    @track data = [];
    recordId;
    pageNumber = 1;
    pageSize = 10;
    isLastPage = false;
    resultSize = 0;
    selection = [];
    selectedRows = [];
    hasPageChanged;
    initialLoad = true;
    error;
    //record count
    recordFrom = 0;
    recordTo = 0; 
    //for filter
    timerId;
    searchKey;
    //for wired call
    isError = false;



    columns = [{
        label: 'VF Asset name',
        fieldName: 'Name',
        type: 'text',
        sortable: false
    },
    {
        label: 'Assigned Product Id',
        fieldName: 'Assigned_Product_Id__c',
        type: 'text',
        sortable: false
    },
    {
        label: 'CTN',
        fieldName: 'CTN__c',
        type: 'text',
        sortable: false
    },
    {
        label: 'Priceplan Description',
        fieldName: 'Priceplan_Description__c',
        type: 'text',
        sortable: false
    },
    {
        label: 'Priceplan Class',
        fieldName: 'Priceplan_Class__c',
        type: 'text',
        sortable: false
    },
    {
        label: 'Retainable',
        fieldName: 'Retainable__c',
        type: 'checkbox',
        sortable: false
    },
    {
        label: 'Contract End Date',
        fieldName: 'Contract_End_Date__c',
        type: 'date',
        sortable: false
    },
    {
        label: 'Contract VF',
        fieldName: 'Contract_VF_Name__c',
        type: 'text',
        sortable: false
    },
    {
        label: 'BAN Number',
        fieldName: 'BAN_Number__c',
        type: 'text',
        sortable: false
    },
];

    opp;
    //	Closed Won
    @wire(getRecord, { recordId: '$recordId', fields: [STAGE_FIELD, JOURNEY_FIELD]})
    wiredRecord({ error, data }) {
        if (error) {
            let message = 'Unknown error';
            if (Array.isArray(error.body)) {
                message = error.body.map(e => e.message).join(', ');
            } else if (typeof error.body.message === 'string') {
                message = error.body.message;
            }
            this.isError = true;
            const disableButton = new CustomEvent('error');
            // Dispatches the event.
            this.dispatchEvent(disableButton);
        } else if (data) {
            console.log( JSON.stringify(data) );
            this.opp = data;
            if( this.opp.fields.Select_Journey__c.value === 'Change Product Settings' && 
                this.opp.fields.StageName.value !== 'Closed Won'){
                    this.getAccounts();
            } else {
                this.isError = true;
                const disableButton = new CustomEvent('error');
                // Dispatches the event.
                this.dispatchEvent(disableButton);
            }
        }
    }

    //function to save close and show toast Start
    @api
    close() {
        const closeQA = new CustomEvent('close');
        // Dispatches the event.
        this.dispatchEvent(closeQA);
    }

    @api 
    selectSaveOLI(){
        console.log("==rows been selected---" , this.mapAllSelection);
        debugger;
        let lst =[];
        for (let amount of this.mapAllSelection.values()) {
            let cont = { 'sobjectType': 'VF_Asset__c', ...amount };
            lst.push( cont );
        }
        if( this.mapAllSelection.size > 0  ){
            this.showSpinner = true;
            createOLI({strJSONToDeserialize: JSON.stringify(lst), oppId: this.recordId })
                .then(result => {
                    this.showSpinner =  false;
                    this.showToast('success', 'Opportunity line Items created');
                })
                .catch(error => {
                    this.showSpinner =  false;
                    console.log(JSON.stringify(error));
                    this.showToast('error', error.body.message);
                    this.error = error;
                });
        }
        else{
            this.showToast('warning', 'Please select at least one Asset');
        }
    }

    showToast(variant , msg) {
        const event = new ShowToastEvent({
            "title": variant,
            "variant" : variant,
            "message": msg
        });
        this.dispatchEvent(event);
    }

    //function to reset the variable for pagination when search
    resetPagination(){
        this.pageNumber = 1;
        this.isLastPage = false;
    }

    startSearchTimer(evt) {
        console.log( evt.target.value );
        this.searchKey = evt.target.value;
        this.data = [];
        clearTimeout(this.timerId);
        this.timerId = setTimeout(this.getAccounts.bind(this),500);//query the new records
        this.resetPagination(); //reset values to show the first page 
    }

    rowSelection(evt) {
        // Avoid any operation if page has changed
        // as this event will be fired when new data will be loaded in page
        // after clicking on next or prev page
        if (!this.hasPageChanged || this.initialLoad) {
            //set initial load to false
            this.initialLoad = false;
        
            //Get currently select rows, This will only give the rows available on 
            //current page
            let mapAllSelected = !this.mapAllSelection ? new Map(): this.mapAllSelection ;
            let mapSelectedInPage = new Map();
            evt.detail.selectedRows.forEach(element => {
                mapSelectedInPage.set( element.Id, element );
            });

            var table = this.template.querySelector('[data-id="datarow"]');
            var rows = table.data;
            rows.forEach(element=>{
                if( mapAllSelected.has( element.Id ) && !mapSelectedInPage.has( element.Id ) ){
                    mapAllSelected.delete( element.Id);
                }else if( !mapAllSelected.has( element.Id ) && mapSelectedInPage.has( element.Id ) ){
                    mapAllSelected.set( element.Id, element );
                }
            });
            
            this.mapAllSelection = mapAllSelected;
            let keys = Array.from( mapAllSelected.keys() );
            this.selection = keys;
        }
        else this.hasPageChanged = false;
    }
    
    previousEve() {
        //Setting current page number
        let pageNumber = this.pageNumber;
        this.pageNumber = pageNumber - 1;
        //Setting pageChange variable to true
        this.hasPageChanged = true;
        this.getAccounts();
    }
    
    nextEve() {
        //get current page number
        let pageNumber = this.pageNumber;
        //Setting current page number
        this.pageNumber = pageNumber + 1;
        //Setting pageChange variable to true
        this.hasPageChanged = true;
    
    
        this.getAccounts();
    }
    
    get recordCount() {
        this.recordFrom = this.pageNumber === 1 ? 1 : ((this.pageNumber - 1) * this.pageSize ) + 1;
        this.recordTo = this.pageNumber === 1 ? this.resultSize : (this.pageNumber - 1) * this.pageSize + this.resultSize;
        return `Page ${this.pageNumber} | Showing records from ${this.recordFrom} to ${this.recordTo}`;
    }
    
    get disPre() {
        return this.pageNumber === 1 ? true : false;
    }
    
    getAccounts() {

        getVFAssetList ({ 
            oppId: this.recordId , 
            pageSize: this.pageSize,
            pageNumber: this.pageNumber,
            searchKey: this.searchKey
        })
        .then(result => {
            let VFAssetData = JSON.parse(JSON.stringify(result));
            if (VFAssetData.length < this.pageSize) {
              this.isLastPage = true;
            } else {
              this.isLastPage = false;
            }

            this.resultSize = VFAssetData.length;
            this.data = VFAssetData;
    
            this.template.querySelector(
              '[data-id="datarow"]'
            ).selectedRows = this.selection;
        })
        .catch(error => {
            console.log(error);
            this.error = error;
        });
    }
}