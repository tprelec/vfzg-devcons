/**
 *  Miguel Alves
 *  @date 2022-05-24
 *  @description LWC component to create External Ticket
 *
 *  Luis Aguiar
 *  @date 2022-07-16
 *  @description Update to include file upload, to be associated with the ticket
 */
import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
import { CurrentPageReference } from 'lightning/navigation';

import sldsOverrides from '@salesforce/resourceUrl/sldsOverrides';
import { loadStyle } from 'lightning/platformResourceLoader';

import { createRecord } from 'lightning/uiRecordApi';

// ContentVersion fields
import TITLE_FIELD from '@salesforce/schema/ContentVersion.Title';
import PATH_FIELD from '@salesforce/schema/ContentVersion.PathOnClient';
import VERSION_FIELD from '@salesforce/schema/ContentVersion.VersionData';
import LOCATION_FIELD from '@salesforce/schema/ContentVersion.ContentLocation';

// ContentDocumentLink fields
import ENTITY_FIELD from '@salesforce/schema/ContentDocumentLink.LinkedEntityId';
import DOCUMENT_ENTITY_FIELD from '@salesforce/schema/ContentDocumentLink.ContentDocumentId';
import VISIBILITY_FIELD from '@salesforce/schema/ContentDocumentLink.Visibility';
import SHARE_FIELD from '@salesforce/schema/ContentDocumentLink.ShareType';

//import { getSObjectValue } from '@salesforce/apex';
//import getCaseNumber from '@salesforce/apex/CST_ExternalSystem.getCaseNumber';
import submitTicketToRemedy from '@salesforce/apex/CST_ExternalSystem.submitTicketToRemedy';
import getQuestions from '@salesforce/apex/CST_ExternalSystem.getQuestions';
import submitExternalTicket from '@salesforce/apex/CST_ExternalSystem.submitExternalTicket';
import getFiles from '@salesforce/apex/CST_ExternalSystem.getFiles';
import deleteFiles from '@salesforce/apex/CST_ExternalSystem.deleteFiles';

import SystemModstamp from '@salesforce/schema/Account.SystemModstamp';

const MAX_FILE_SIZE = 5000000;

export default class cST_SendRemedyTicket extends LightningElement {
	@api recordId;

	Case4Remedy;
	@track MAX_FILE_SIZE = 5242880;
	@track MAX_FILE_SIZE_display = Math.floor(this.MAX_FILE_SIZE / 1000000);
	@track MAX_UPLOAD_COUNT = 3;
	@track DomResult = {};
	@track hideModal;
	@track Quest4Remedy;
	@track answers = [];
	@track answers1 = [];
	@track answers2 = [];
	@track filesData = [];
	@track elems;
	@track cssClass;
	@track ticketStatus;
	@track ticketid;
	@track maxUploadReached = false;
	@track isUploadEnabled = true;
	@track isLoading = false;
	@track showScreen2 = false;
	@track btnNext;
	@track btnSubmit;
	@track toggleHide;
	@track files = [];
	@track fileCount = 0;
	@track isUploadDisabled = false;
	@track isNextDisabled = false;
	@track isSbmitDisabled = false;
	@track isNextDisabled = false;
	@track statusTitle;
	@track statusError = {
		warningFileCount: false,
		warningFilesize: false,
		warningFileDeleting: false
	};
	/**
	 * @description wired function, auto-loads with component.
	 * @param {CurrentPageReference} reference to get Case.Id, the current recordId
	 */
	@wire(CurrentPageReference)
	getStateParameters(currentPageReference) {
		if (currentPageReference && !this.recordId) {
			this.recordId = currentPageReference.state.recordId;
		}
	}

	/**
	 * @description callback for the successful render of the DOM structure
	 */
	renderedCallback() {
		// this.isLoading = false;
		this.toggleHide = this.template.querySelectorAll('.toggleHide');
		if (this.showScreen2) this.isLoading = false;

		console.log('recordId', this.recordId);
		console.log('toggleHide', this.toggleHide);
		console.log('showScreen2', this.showScreen2);

		loadStyle(this, sldsOverrides);
	}
	updateFileStatus(fileCount) {
		console.log('updateFileStatus');
		console.log('MAX_UPLOAD_COUNT', this.MAX_UPLOAD_COUNT);
		console.log('fileCount eval >', fileCount > this.MAX_UPLOAD_COUNT);
		console.log('fileCount eval >=', fileCount >= this.MAX_UPLOAD_COUNT);
		console.log('fileCount', fileCount);

		if (fileCount > this.MAX_UPLOAD_COUNT) {
			this.isSbmitDisabled = true;
			this.statusError.warningFileCount = true;
		} else {
			this.isSbmitDisabled = false;
			this.statusError.warningFileCount = false;
		}

		if (fileCount >= this.MAX_UPLOAD_COUNT) {
			this.isUploadDisabled = true;
		} else {
			this.isUploadDisabled = false;
		}

		this.statusError.warningFilesize = false;

		for (let f of this.filesData) {
			if (f.fileSize > this.MAX_FILE_SIZE) {
				console.log('fileSize toobig: ' + f.fileSize);
				console.log('this.MAX_FILE_SIZE: ' + this.MAX_FILE_SIZE);
				this.statusError.warningFilesize = true;
				this.isSbmitDisabled = true;
				break;
			}
		}
		if (this.statusError.warningFilesize) {
			//this.ShowToast('Warning', 'File size too big. The maximum supported filesize is 5 mb.', 'warning');
		}
		//if (this.statusError.warningFileCount) this.ShowToast('Warning',  `Too many files attached. The maximum supported number is ${MAX_UPLOAD_COUNT}.`, 'warning');

		console.log('isUploadDisabled', this.isUploadDisabled);
		console.log('isSbmitDisabled', this.isSbmitDisabled);
		console.table(this.statusError);
	}

	handleUploadFinished(event) {
		console.log('handleUploadFinished');
		this.isLoading = true;

		let files = event.detail.files;

		console.log('files', { files });
		console.log('ticketinfo', this.ticketinfo);
		console.log('ticketid', this.ticketid);

		// let obj;
		let ids = [];

		for (let f of files) {
			console.log(f.name + ' ' + f.documentId + ' ' + f.contentVersionId + ' ' + f.contentBodyId);

			ids.push(f.documentId);
		}

		let strIds = JSON.stringify(ids);
		console.log('getFiles strIds ' + strIds);
		console.log('getFiles MAX_FILE_SIZE ' + this.MAX_FILE_SIZE);

		getFiles({ ids: strIds })
			.then((resp) => {
				console.log('getFiles resp: ' + resp);
				let jsonResp = JSON.parse(resp);

				console.log('getFiles jsonResp', jsonResp);
				console.log('getFiles files', jsonResp.files);

				let obj;

				for (let f of jsonResp.files) {
					obj = {
						name: f.name,
						name: f.title,
						documentid: f.contentdocumentid,
						contentversionid: f.contentVersionId,
						contentbodyid: f.contentBodyId,
						fileSize: f.fileSize
					};
					obj.status = 'fileBtnLabel removeImage';
					if (f.fileSize > this.MAX_FILE_SIZE) {
						obj.status += ' bigFile';
					}
					this.filesData.push(obj);
				}

				console.log('getFiles this.filesData 2', this.filesData);

				this.updateFileStatus(this.filesData.length);
			})
			.then(() => {
				this.isLoading = false;
			});
	}

	get acceptedFormats() {
		return ['.pdf', '.png', '.jpg', '.gif', '.txt', '.csv'];
	}

	/**
	 * @description callback for the successful insertion of this component in the Salesforce Page
	 */
	connectedCallback() {
		console.log('connectedCallback ticketid', this.ticketid);

		if (this.recordId) {
			//getCaseNumber({ caseId: this.recordId })
			getQuestions({ caseId: this.recordId })
				.then((response) => {
					console.log('getQuestions response', response);

					var lineItemsListAux = [];
					if (response.length != 0) {
						for (var i = 0; i < response.length; i++) {
							//this.DomResult.Id = response.Id;
							//this.DomResult.CaseNumber = response.CaseNumber;
							var answer = { quest: response[i].CST_Question__c, ans: response[i].CST_Answer__c, key: response[i].Id };
							console.log('answer', answer);
							this.answers.push(answer);
						}
						/*lineItemsListAux.push({
                            caseNumber: response.CaseNumber,
                            key: response.Id
                        
                           // questionType: result[i].CST_Question_Type__c,
                            //isMandatory: isRequired,
                           // id: result[i].id,
                            //helptext: result[i].CST_Questionaire_HelpText__c
                        });*/
						for (var i = 0; i <= Math.ceil(this.answers.length / 2) - 1; i++) {
							console.log(this.answers[i]);
							this.answers1.push(this.answers[i]);
							console.log(this.answers[i]);
						}

						for (var j = 0, i = Math.ceil(this.answers.length / 2); i < this.answers.length; j++, i++) {
							console.log('Answers', this.answers[i]);
							this.answers2.push(this.answers[i]);
							console.log('Answers2', this.answers2[j]);
						}
					}

					//this.Case4Remedy = lineItemsListAux;
					//this.hideModal = false;
				})
				.catch((error) => {
					console.log('error');
					console.dir(error);
				});
		}
	}

	/*
	 * @description (handler for button click): to remove an image, from Upload button
	 * @return undefined
	 */
	handleDeleteFiles(event) {
		this.isLoading = true;
		// console.log('dataset', event.currentTarget.dataset.name);
		console.log('handleDeleteFiles');
		console.log('currentTarget', event.currentTarget);
		console.log('documentid', event.currentTarget.dataset.documentid);

		let info = JSON.parse(JSON.stringify(event.currentTarget));
		console.log('info', { info });
		console.log('name', event.currentTarget.title);
		console.log('this.filesData');
		console.dir(this.filesData);

		// event.currentTarget.parentNode.removeChild(event.currentTarget);

		let docId = event.currentTarget.dataset.documentid;

		// event.currentTarget.parentNode.removeChild(event.currentTarget);

		for (let i = this.filesData.length - 1; i >= 0; i--) {
			let dataElem = this.filesData[i];

			console.log('dataElem', dataElem);

			if (dataElem.documentid == docId) {
				console.log('docId', docId);
				console.log('eId', dataElem.documentid);
				console.log('eId == docId', dataElem.documentid == docId);
				console.log('eId != docId', dataElem.documentid != docId);
				this.filesData.splice(i, 1);
			}
		}

		console.log('docId', docId);

		deleteFiles({ recordid: docId }).then((resp) => {
			this.isLoading = false;
			console.log('handleDeleteFiles deleteFiles resp');
			console.log('deleteFiles resp', resp);
			console.log('deleteFiles docId', docId);
			console.log('deleteFiles parent', parent);

			console.log('deleteFiles handleDeleteFiles filesData', this.filesData);

			let jsonResp = JSON.parse(resp);
			console.log('deleteFiles jsonResp', { jsonResp });

			console.log('deleteFiles status', jsonResp.status);

			this.statusTitle = jsonResp.title;
			this.statusError.warningFileDeleting = jsonResp.status != 'success' ? true : false;
			this.updateFileStatus(this.filesData.length);

			// this.updateFileStatus(this.filesData.length);
		});
	}

	updateFileCount(fileCount) {
		console.log('updateFileCount');
		console.log('MAX_UPLOAD_COUNT', this.MAX_UPLOAD_COUNT);
		console.log('this.fileCount', fileCount);

		console.log('maxUploadReached eval', fileCount >= this.MAX_UPLOAD_COUNT);
		if (fileCount >= this.MAX_UPLOAD_COUNT) this.maxUploadReached = true;
		else this.maxUploadReached = false;

		if (fileCount > this.MAX_UPLOAD_COUNT) {
			this.isUploadDisabled = true;
			this.isUploadEnabled = false;
			this.fireToastMessage('Warning', `Too many files attached. The maximum supported number is ${this.MAX_UPLOAD_COUNT}.`, 'warning');
		} else {
			this.isUploadDisabled = false;
			this.isUploadEnabled = true;
		}

		console.log('maxUploadReached', this.maxUploadReached);

		this.execessiveFileCount = fileCount - this.MAX_UPLOAD_COUNT;
		console.log('execessiveFileCount', this.execessiveFileCount);
	}
	/**
	 * @description callback to cancel subission of form, an reset of selected information
	 */

	handleCancel() {
		console.log('handleCancelCleadData');
		const closeTabEvent = new CustomEvent('closeTabEvent', {
			detail: { close }
		});
		this.dispatchEvent(closeTabEvent);
		this.dispatchEvent(new CloseActionScreenEvent());
		this.updateRecordView();
	}
	handleSubmit() {
		console.log('handleSubmit');
		this.isLoading = true;

		// to use when we have real communication with Layer 7 System
		submitExternalTicket({ ticketid: this.ticketid }).then((response) => {
			console.log('handleSubmit success', response);

			this.isLoading = false;
			/*
                if (response != '') {
                    this.fireToastMessage('Ticket Created', 'Please check related ticket to know more');
                } else {
                    this.fireToastMessage('There was an error', 'Please check related ticket to know more', 'error');
                }
                this.updateRecordView();
                */
			//*
			let jsonResponse = JSON.parse(response);
			this.ticketStatus = jsonResponse;
			console.log('handleSubmit response', { jsonResponse });
			console.log('handleSubmit this.ticketStatus', this.ticketStatus);
			console.log('handleSubmit error: ', jsonResponse.error);
			console.log('handleSubmit error: ', jsonResponse.error == '');
			console.log('handleSubmit message: ', jsonResponse.message);
			console.log('handleSubmit ticketid: ', jsonResponse.ticketid);

			if (response != '' && this.ticketStatus && this.ticketStatus.error == '') {
				this.fireToastMessage('Ticket created', 'Please open the External Ticket record to check if the request was approved.');
				this.updateRecordView();
			} else {
				console.log('handleSubmit error', response);
				this.fireToastMessage('Warning', 'It was not possible to create External Ticket', 'error');
			}
			//*/
		});
	}
	/**
	 * @description callback to submit form, with the currently selected information
	 */
	handleNext() {
		console.log('this.recordId', this.recordId);
		this.isLoading = true;
		this.isNextDisabled = true;

		submitTicketToRemedy({ caseid: this.recordId })
			.then((response) => {
				console.log('createRemedyTicket response', response);
				//this.ticketStatus = jsonResponse;
				console.log('submitTicketToRemedy this.showScreen2', this.showScreen2);

				if (response != '') {
					let jsonResponse = JSON.parse(response);

					this.handleHideElements();

					this.ticketid = jsonResponse.ticketid;

					console.log('this.ticketid', this.ticketid);

					//}
					/*
                    this.updateRecordView();
                    else {
                    console.log(response);
                    console.log("Response", +response);
                    this.fireToastMessage("Ticket not created", "Something went wrong. Please review the provided data to Remedy.", "error");
                    this.handleCancel();
                    */
				}
			})
			.then((success) => {
				this.showScreen2 = true;
			})
			.catch((error) => {
				this.isLoading = false;
				console.log('error', { error });
				this.fireToastMessage('Ticket not created', 'External Ticket submission was not successful. Please check error message', 'error');
				this.handleCancel();
			});
	}
	handleHideElements() {
		console.log('toggleHide', this.toggleHide);
		this.toggleHide.forEach((element) => {
			element.classList.toggle('hidden');
		});
	}

	handleShowElements(elem) {
		elem.style.display = 'slds-show';
	}
	/**
	 * @description auxiliary method, to create Toaster messages
	 */
	fireToastMessage(title, message, variant) {
		let _v = variant ? variant : 'success';

		const toaster = new ShowToastEvent({
			title: title,
			message: message,
			variant: _v
		});
		this.dispatchEvent(toaster);
	}

	/**
	 * @description auxiliary method, to refresh the current page
	 */
	updateRecordView() {
		setTimeout(() => {
			this.dispatchEvent(new CloseActionScreenEvent());
			eval("$A.get('e.force:refreshView').fire();");
		}, 1000);
		this.hideModal = true;
	}
}
