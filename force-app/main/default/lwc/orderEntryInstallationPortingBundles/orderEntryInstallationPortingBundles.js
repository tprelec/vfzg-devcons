import { LightningElement, api } from 'lwc';

export default class OrderEntryInstallationPortingBundles extends LightningElement {
	@api bundles;
}
