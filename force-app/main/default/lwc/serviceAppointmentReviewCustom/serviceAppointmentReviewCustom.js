import { api, LightningElement } from 'lwc';
import reviewHeader from '@salesforce/label/c.Appointment_Review_Header';
import { FlowAttributeChangeEvent } from 'lightning/flowSupport';

export default class ServiceAppointmentReviewCustom extends LightningElement {
	// properties and their types needed for this component can be found in the below article:
	// https://help.salesforce.com/s/articleView?id=sf.ls_flowscreencmp_review_srvc_appt.htm&type=5
	@api
	appointmentType;

	@api
	businessConsultantId;

	@api
	parentRecordId;

	@api
	serviceAppointmentAdditionalInformation;

	@api
	serviceAppointmentCity;

	@api
	serviceAppointmentComments;

	@api
	serviceAppointmentCountry;

	@api
	serviceAppointmentSchedEndTime;

	@api
	serviceAppointmentPostalCode;

	@api
	serviceAppointmentSchedStartTime;

	@api
	serviceAppointmentState;

	@api
	serviceAppointmentStreet;

	@api
	serviceAppointmentServiceTerritoryId;

	@api
	workTypeGroupId;

	@api
	get serviceAppointmentFields() {
		return this._serviceAppointmentFields;
	}

	set serviceAppointmentFields(value) {
		this._serviceAppointmentFields = value;
	}

	_serviceAppointmentFields;

	@api
	enableGuestUser;

	@api
	get lead() {
		return this._lead;
	}

	set lead(value) {
		this._lead = value;
	}

	_lead;

	@api
	optionalAttendees;

	@api
	saveErrorMessage;

	@api
	schedulingPolicyName;

	@api
	serviceAppointment;

	@api
	showBusinessConsultant;

	@api
	timeZone;

	@api
	validations;

	@api
	get hasErrors() {
		return this._hasErrors;
	}

	set hasErrors(value) {
		this._hasErrors = value;
	}

	_hasErrors;

	label = {
		header: reviewHeader
	};

	businessConsultantLoad = false;
	appointmentTimeReviewLoad = false;
	generalInfoReviewLoad = false;
	personalDetailsLoad = false;
	_additionalInfo;

	// only when all child components have completely loaded all their required data, then we hide the spinner component
	get childrenLoadComplete() {
		return this.isBusinessConsultantLoad() && this.appointmentTimeReviewLoad && this.generalInfoReviewLoad && this.personalDetailsLoad;
	}

	isBusinessConsultantLoad() {
		// if we are showing the business consultant section (showBusinessConsultant=true), then we wait till the record is returned
		// from the server (businessConsultantLoadEvent), if not, we return true, since we don't need to wait for any server response
		return (this.showBusinessConsultant && this.businessConsultantLoad) || (!this.showBusinessConsultant && !this.businessConsultantLoad);
	}

	// notifies the business consultant component is completely loaded
	businessConsultantLoadEvent() {
		this.businessConsultantLoad = true;
	}

	// notifies the time review component is completely loaded
	apptTimeReviewLoadEvent() {
		this.appointmentTimeReviewLoad = true;
	}

	// notifies the general info component is completely loaded
	generalInfoLoadEvent() {
		this.generalInfoReviewLoad = true;
	}

	// notifies the personal details component is completely loaded
	personalDetailsLoadEvent() {
		this.personalDetailsLoad = true;
	}

	@api
	validate() {
		let leadReviewComponent = this.template.querySelector('c-service-appt-personal-details-review');
		// get update lead record from child component
		this._lead = leadReviewComponent.lead;
		this._hasErrors = leadReviewComponent.validate();

		// notify the flow with the newest lead version
		const leadFlowChangeEvent = new FlowAttributeChangeEvent('lead', this.lead);
		this.dispatchEvent(leadFlowChangeEvent);
		// show banner
		const errorFlowChangeEvent = new FlowAttributeChangeEvent('hasErrors', this._hasErrors);
		this.dispatchEvent(errorFlowChangeEvent);
		//	additional fields
		let additionalFields = this.basicFields;
		if (this._additionalInfo) {
			additionalFields = Object.assign(additionalFields, this._additionalInfo);
		}
		this._serviceAppointmentFields = JSON.stringify(additionalFields);

		const additionalFieldsChangeEvent = new FlowAttributeChangeEvent('serviceAppointmentFields', this._serviceAppointmentFields);
		this.dispatchEvent(additionalFieldsChangeEvent);
		return { isValid: !this._hasErrors, errorMessage: '' };
	}

	get basicFields() {
		return {
			AdditionalInformation: '',
			AppointmentType: this.appointmentType,
			Comments: '',
			ServiceTerritoryId: this.serviceAppointmentServiceTerritoryId,
			ServiceResourceId: this.businessConsultantId,
			WorkTypeGroupId: this.workTypeGroupId,
			SchedStartTime: this.serviceAppointmentSchedStartTime,
			SchedEndTime: this.serviceAppointmentSchedEndTime,
			Subject: '',
			Phone: '',
			Email: '',
			IsAnonymousBooking: false,
			isSlotChanged: false
		};
	}
	// every time a field is changed on the general info component, this handler function is triggered and
	// returns a JSON string with all fields (as specified: https://help.salesforce.com/s/articleView?id=sf.ls_flowscreencmp_review_srvc_appt.htm&type=5 )
	generalInfoHandler(event) {
		this._additionalInfo = event.detail;
	}
}
