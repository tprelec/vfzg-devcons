import { LightningElement, track, wire } from 'lwc';
import { loadScript, loadStyle } from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getFirstDayOfWeekInMs, addWeeks, arrayRemoveById, addDays, compareByStart } from 'c/vfzHelper';
import FullCalendarJS from '@salesforce/resourceUrl/FullCalendar';
import getRegions from '@salesforce/apex/ZipDeliveryPlanningController.getRegions';
import getEngByRegion from '@salesforce/apex/ZipDeliveryPlanningController.getUsersByRegion';
import getUnplannedDeliveryOrders from '@salesforce/apex/ZipDeliveryPlanningController.getUnplannedDeliveryOrders';
import getSettings from '@salesforce/apex/ZipDeliveryPlanningController.getSettings';
import NO_RESULTS_IMAGE from '@salesforce/resourceUrl/no_results_image';
import getAppointments from '@salesforce/apex/ZipDeliveryPlanningController.getAppointments';
import saveAppointment from '@salesforce/apex/ZipDeliveryPlanningController.saveAppointment';
import moveAppointment from '@salesforce/apex/ZipDeliveryPlanningController.moveAppointment';
import confirmAppointment from '@salesforce/apex/ZipDeliveryPlanningController.confirmAppointment';
import unconfirmAppointment from '@salesforce/apex/ZipDeliveryPlanningController.unconfirmAppointment';
import deleteAppointment from '@salesforce/apex/ZipDeliveryPlanningController.deleteAppointment';
import getCurrentUserPermission from '@salesforce/apex/ZipDeliveryPlanningController.getCurrentUserPermission';
/** Custom Labels used in this LWC */
import appointmentHasBeenSaved from '@salesforce/label/c.appointmentHasBeenSaved';
import appointmentBtnCancel from '@salesforce/label/c.appointmentBtnCancel';
import appointmentBtnSave from '@salesforce/label/c.appointmentBtnSave';
import appointmentSuccessfullyConfirmed from '@salesforce/label/c.appointmentSuccessfullyConfirmed';
import appointmentSuccessfullyUnconfirmed from '@salesforce/label/c.appointmentSuccessfullyUnconfirmed';
import appointmentSuccessfullyCreated from '@salesforce/label/c.appointmentSuccessfullyCreated';
import appointmentSuccessfullyDeleted from '@salesforce/label/c.appointmentSuccessfullyDeleted';
import appointmentAddSpecial from '@salesforce/label/c.appointmentAddSpecial';
import deliveryPlanning from '@salesforce/label/c.deliveryPlanning';
import editFilters from '@salesforce/label/c.editFilters';
import errorTitle from '@salesforce/label/c.errorTitle';
import errorUpdatingTheAppointment from '@salesforce/label/c.errorUpdatingTheAppointment';
import somethingWentWrongGettingAppointments from '@salesforce/label/c.somethingWentWrongGettingAppointments';
import thereAreNoAppointmentsForThatDay from '@salesforce/label/c.thereAreNoAppointmentsForThatDay';
import successTitle from '@salesforce/label/c.successTitle';
import unplannedDeliveryOrders from '@salesforce/label/c.unplannedDeliveryOrders';
import changeFilterSettings from '@salesforce/label/c.changeFilterSettings';
import confirmDeletion from '@salesforce/label/c.confirmDeletion';
import engineersSuscribedQueue from '@salesforce/label/c.engineersSuscribedQueue';
import selectEngineer from '@salesforce/label/c.selectEngineer';
import queuesRepresentingRegions from '@salesforce/label/c.queuesRepresentingRegions';

// used for customization of calendar
const CONFIRMED_APPT_BACKCOLOR = 'darkBlue';
const CONFIRMED_UNP_APPT_BACKCOLOR = 'mediumTurquoise';
const CONFIRMED_APPT_TEXTCOLOR = 'white';
const UNCONFIRMED_UNPROV_APPT_BACKCOLOR = 'mistyRose';
const SPECIAL_APPT_BACKCOLOR = 'white';
const SPECIAL_APPT_TEXTCOLOR = 'gray';
const SELECTED_APPT_BACKCOLOR = 'black';
const SELECTED_APPT_TEXTCOLOR = 'white';

const PREFIX_UNPROVISIONED_APPT = 'U* ';
/**
 * zipDeliveryPlanning component is designated primarily to work for Zakkelijk Internet Pro family products
 * in order to let the Delivery Coordinators to plan Service delivery for Field Engineers.
 * It could be easily adjusted to work with other products as well.
 */
export default class ZipDeliveryPlanning extends LightningElement {
  labels = {
    appointmentAddSpecial,
    appointmentBtnCancel,
    appointmentBtnSave,
    appointmentHasBeenSaved,
    appointmentSuccessfullyConfirmed,
    appointmentSuccessfullyUnconfirmed,
    appointmentSuccessfullyCreated,
    appointmentSuccessfullyDeleted,
    changeFilterSettings,
    confirmDeletion,
    deliveryPlanning,
    editFilters,
    engineersSuscribedQueue,
    errorTitle,
    errorUpdatingTheAppointment,
    queuesRepresentingRegions,
    somethingWentWrongGettingAppointments,
    thereAreNoAppointmentsForThatDay,
    selectEngineer,
    successTitle,
    unplannedDeliveryOrders
  };
  // Set filter values (header)
  @track currEngineer = '';
  @track currEngineerId = '';
  @track currRegion = '';
  @track currRegionId = '';
  @track openDialog = false;
  @track isFullUser = false;
  // Set calendar filter values
  filterEngineer;
  filterEngineerId;
  filterRegion;
  filterRegionId;
  // Used for the calendar.
  calendar;
  calendarCreated = false;
  calendarInitialized = false;

  @track regionItems = [];
  @track engineerItems = [];
  @track deliveryOrderItems = [];

  userEvents = [];

  // used to show an image to the user (no results)
  @track noDeliveryOrders = true;
  noResultsImage = NO_RESULTS_IMAGE;
  // used to pass this appointmentId to the Appt Details Pane
  @track selAppointmentId = null;
  @track selContactId = null
  @track selConfirmed = false;
  @track selSpecial = false;
  // track the last error
  @track lastError = '';
  // used to show the main spinning screen.
  @track waitDialog = true;
  scriptInitialized; // used for one-time setting on lwc events.

  // Paint the regions on the region's dropdown.
  @wire(getRegions)
  initializeRegions({ data, error }) {
    if (data) {
      //create array with elements which has been retrieved controller
      //here value will be Id and label of combobox will be Name
      for (let i = 0; i < data.length; i++) {
        this.regionItems = [...this.regionItems, { value: data[i].Id, label: data[i].Name }];
      }

      if (this.currRegionId === null || this.currRegionId === undefined || this.currRegionId === '') {
        this.currRegionId = data[0].Id;
        this.currRegion = data[0].Name;
      }
      //this.filterRegion = this.currRegion;
      //this.filterRegionId = this.currRegionId;
      this.filterRegion = data[0].Name;
      this.filterRegionId = data[0].Id;
      this.lastError = undefined;

    } else if (error) {
      this.lastError = error.body.message;
    }
  }

  // Make the engineers dropdown "dependant" from the region's dropdown.
  @wire(getEngByRegion, { regionId: '$filterRegionId' })
  initializeEngByRegion({ data, error }) {
    if (data) {
      this.engineerItems = [];
      for (let i = 0; i < data.length; i++) {
        this.engineerItems = [...this.engineerItems, { value: data[i].Id, label: data[i].Name }];
      }

      if(this.currEngineerId === null || this.currEngineerId === undefined || this.currEngineerId === '') {
        this.currEngineer = data[0].Name;
        this.currEngineerId = data[0].Id;
      }
      //this.filterEngineer = data[0].Name;
      //this.filterEngineerId = data[0].Id;
      this.lastError = undefined;

      // Call calendar builder once the plugins have been also loaded
      if (this.calendarCreated && !this.calendarInitialized) {
        getCurrentUserPermission()
          .then (result => {
            this.isFullUser = result==='Write' ? true : false;
            this.initializeFullCalendar();
            this.waitDialog = false;
            this.calendarInitialized = true;
          })
          .catch (subError => {
            this.lastError = subError;
          });
      }


    } else if (error) {
      this.lastError = error.body.message;
      console.log(error.body.message);
    }
  }

  // Initialize Unplanned DeliveryOrders when the Region changes.
  @wire(getUnplannedDeliveryOrders, { regionId: '$currRegionId' })
  initializeUnplannedDeliveryOrders({ data, error }) {
    if (data) {
      this.deliveryOrderItems = data;
      if (data.length === 0) {
        this.noDeliveryOrders = true;
      } else {
        this.noDeliveryOrders = false;
      }
      this.lastError = undefined;
    } else if (error) {
      this.deliveryOrderItems = [];
      this.fireToaster(this.labels.errorTitle, error.body.message, 'error');
    }
  }

   // Bring settings from the Org for the Screen
   // calendarStartTime, calendarEndTime, fieldEnginerProfile, weeksAhead
   // (please check COM_Delivery_Planning_Setting__mdt for further info)
  @wire(getSettings)
  initializeSettings({ data, error }) {
    if (data) {
      this.settings = data;
      this.lastError = undefined;
    } else if (error) {
      this.settings = undefined;
      this.lastError = error;
    }
  }

  get engineerOptions() {
    return this.engineerItems;
  }
  get regionOptions() {
    return this.regionItems;
  }

  // fires the toaster
  fireToaster(pTitle, pMessage, pVariant) {
    const toasty = new ShowToastEvent({
      title: pTitle,
      message: pMessage,
      variant: pVariant,
      mode: 'dismissable'
    });
    this.dispatchEvent(toasty);
  }
  // Reset the calendar (appointments), Appointment Details box and Unplanned DeliveryOrders Box
  resetCalendar() {
    this.waitDialog = true;
    this.calendar.refetchEvents();
    //let evSrc = this.calendar.getEventSources();
    //evSrc[0].refetch();
    this.selAppointmentId = null;
    this.selConfirmed = false;
    this.selSpecial = false;
    this.waitDialog = false;
  }

  //Update the calendar with the new updatedAppointment from the Details Panel
  updateApptOnCalendar(oldAppt, updatedAppt) {
    // if changed Field_Enginer__c then remove the appointment from the calendar.
    if (updatedAppt.Field_Engineer__c !== this.currEngineerId) {
      oldAppt.remove();
    } else {
      // Make again the appointment editable.
      if(this.isFullUser) {
        oldAppt.setProp('startEditable', true);
        oldAppt.setProp('durationEditable', true);
        oldAppt.setProp('editable', true);
      }
      let dEnd = new Date(updatedAppt.Start__c);
      dEnd.setMinutes(dEnd.getMinutes() + parseInt(updatedAppt.Duration__c, 10));
      if (oldAppt.start !== updatedAppt.Start__c) {
        oldAppt.setDates(updatedAppt.Start__c, dEnd);
      } else if (oldAppt.end !== dEnd) {
        oldAppt.moveDates({ minutes: updatedAppt.Duration__c });
      }
      if (oldAppt.title !== updatedAppt.Name) {
        oldAppt.setProp('title', updatedAppt.Name);
      }
      if (oldAppt.extendedProps.description !== updatedAppt.Description__c) {
        oldAppt.setExtendedProp('description', updatedAppt.Description__c);
      }
      if (oldAppt.extendedProps.location !== updatedAppt.Location__c) {
        oldAppt.setExtendedProp('location', updatedAppt.Location__c);
      }
    }
  }

  // Used to open the filters screen (modal)
  handleFilterOpen() {
    this.openDialog = true
    this.filterEngineer = this.currEngineer;
    this.filterEngineerId = this.currEngineerId;
    this.filterRegion = this.currRegion;
    this.filterRegionId = this.currRegionId;
  }
  // Used to close the modal without changing anything on the filters.
  handleFilterClose() {
    this.openDialog = false;
  }

  // Used by the filter screen to change the filter.
  handleFilterSave() {
    this.currRegion = this.filterRegion;
    this.currRegionId = this.filterRegionId;
    this.currEngineer = this.filterEngineer;
    this.currEngineerId = this.filterEngineerId;
    this.handleFilterClose();
    this.resetCalendar();
  }

  handleAddSpecialAppt() {
    this.template.querySelector('c-new-special-appointment').openModal();
  }

  // Fired when the user drops the deliveryOrder onto the calendar, creates a new appointment on the backend.
  eventReceiveCallback = (info) => {
    this.waitDialog = true;
    info.event.remove();
    if (!this.isFullUser) {
      return;
    }

    let wishDate = info.draggedEl.querySelector('.fc-wish-dt').innerText;
    let deliveryOrderId = info.draggedEl.querySelector('.fc-solution-id').innerText;
    let cityText = info.draggedEl.querySelector('.fc-city').innerText;
    let addressText = info.draggedEl.querySelector('.fc-address').innerText;
    let contactID = info.draggedEl.querySelector('.fc-contact-id').innerText;
    let accountName = info.draggedEl.querySelector('.fc-solution-el').innerText;
    let compositeAddr = addressText + (cityText !== '' ? (', ' + cityText) : '');
    const DEFAULT_DURATION = 120;

    if(wishDate !== null || wishDate !== '') {
      let dWishDate = new Date(wishDate);
      let dSelDate = new Date(info.event.start);

      if(dSelDate < dWishDate) {
        this.fireToaster(this.labels.somethingWentWrongGettingAppointments, 'You cannot set an appointment before the customer wish date.', 'error');
        return;
      }
    }
    let engineerID = this.currEngineerId;
    let regionID = this.currRegionId;

    // Save the recent appointment
    saveAppointment({
      appointmentId: '',
      subject: accountName + ',' + addressText,
      start: info.event.start,
      duration: DEFAULT_DURATION,
      location: compositeAddr,
      contactId: contactID,
      description: deliveryOrderId,
      fieldEngineerId: engineerID,
      relatedDeliveryOrderId: deliveryOrderId,
      regionId: regionID
    })
      // eslint-disable-next-line
      .then(result => {
        this.resetCalendar();
        this.fireToaster(this.labels.successTitle, this.labels.appointmentSuccessfullyCreated, 'success');
        //console.log(result);
        this.deliveryOrderItems = arrayRemoveById(this.deliveryOrderItems, deliveryOrderId);
        // Fast way:
        //info.draggedEl.parentNode.parentNode.removeChild(info.draggedEl.parentNode);
      })
      .catch(error => {
        console.log(error);
        info.event.remove();
        this.fireToaster(this.labels.somethingWentWrongGettingAppointments, error.body.message, 'error');
      });
  }
  // Used then an unconfirmed appointment has been deleted.
  insertUnplannedDeliveryOrder(clEvent) {
    let newOldDeliveryOrder = {
      Id: clEvent.extendedProps.deliveryOrderId,
      Order__r: { csord__Account__r: { Name: clEvent.extendedProps.account } },
      Site__r: {
        Site_Street__c: clEvent.extendedProps.location,
        Site_House_Number__c: '',
        Site_House_Number_Suffix__c: '',
        Site_Postal_Code__c: '',
        Site_City__c: ''
      },
      Technical_Contact__r: {
        Name: clEvent.extendedProps.contact,
        Phone: clEvent.extendedProps.phone
      },
      Wish_Date__c: clEvent.extendedProps.wishdate,
      Products__c: clEvent.extendedProps.products
    };
    //console.log(newOldDeliveryOrder);
    this.deliveryOrderItems = [...this.deliveryOrderItems, newOldDeliveryOrder];
  }

  // Fired when the appointment changes the size (duration)
  eventResizeCallback = (info) => {
    moveAppointment({ appointmentId: info.event.id, dtStart: info.event.start, dtEnd: info.event.end })
      .then(() => {
        console.log('moved:' + info.event.id);
      })
      .catch((error) => {
        console.log(error);
        info.revert();
        this.fireToaster(this.labels.somethingWentWrongGettingAppointments, error.body.message, 'error');
      });
  }

  // Fired when an appointment is moved on the Calendar
  eventDropCallback = (info) => {
    // TODO:
    moveAppointment({ appointmentId: info.event.id, dtStart: info.event.start, dtEnd: info.event.end })
      .then(() => {
        console.log('dropped: ' + info.event.id);
      })
      .catch((error) => {
        console.log(error);
        info.revert();
        this.fireToaster(this.labels.somethingWentWrongGettingAppointments, error.body.message, 'error');
      });
  }

  // Fired when the user sets on the Appointment details the "confirm" button,
  // It executes the confirmation request from appointmentDetailBox (backend)
  handleConfirm(event) {
    confirmAppointment({ appointmentId: event.detail })
      .then(() => {
        let confEvent = this.calendar.getEventById(event.detail);
        let bgColor = CONFIRMED_UNP_APPT_BACKCOLOR;
        if (confEvent !== null) {
          if (confEvent.extendedProps.provisioned === true) {
            bgColor = CONFIRMED_APPT_BACKCOLOR;
          }
          confEvent.setProp('backgroundColor', bgColor);
          confEvent.setProp('textColor', CONFIRMED_APPT_TEXTCOLOR);
          confEvent.setProp('startEditable', false);
          confEvent.setProp('durationEditable', false);
          confEvent.setProp('editable', false);
          confEvent.setExtendedProp('confirmed', true);
        }
        this.fireToaster(this.labels.successTitle, this.labels.appointmentSuccessfullyConfirmed, 'success');

      }).catch(error => {
        this.fireToaster(this.labels.errorTitle, error.body, 'error');
      });
    this.selAppointmentId = null;
  }

  // Fired when the user sets on the Appointments Details the "set to unconfirmed" button
  handleUnconfirm(event) {
    unconfirmAppointment({ appointmentId: event.detail })
      .then(() => {
        let confEvent = this.calendar.getEventById(event.detail);
        if (confEvent !== null) {
          if (confEvent.extendedProps.provisioned === false) {
            confEvent.setProp('backgroundColor', UNCONFIRMED_UNPROV_APPT_BACKCOLOR);
          } else {
            confEvent.setProp('backgroundColor', null);
          }
          confEvent.setProp('textColor', null);
          if(this.isFullUser) {
            confEvent.setProp('startEditable', true);
            confEvent.setProp('durationEditable', true);
            //confEvent.setProp('editable', false);
          }
          confEvent.setExtendedProp('confirmed', false);
          this.fireToaster(this.labels.successTitle, this.labels.appointmentSuccessfullyUnconfirmed, 'success');
        }
      }).catch(error => {
        this.fireToaster(this.labels.errorTitle, error.body.message, 'error');
      });
    this.selAppointmentId = null;
  }

  // Handle UI aspects of updated appointment (from AppointmentDetailBox)
  handleUpdated(event) {
    // Look into Calendar events for the updated one.
    let updatedAppt = JSON.parse(event.detail);
    let calUpdAppt = this.calendar.getEventById(updatedAppt.id);
    if (calUpdAppt === null) {
      this.fireToaster(this.labels.errorTitle, this.labels.errorUpdatingTheAppointment, 'error');
    } else {
      this.updateApptOnCalendar(calUpdAppt, updatedAppt);
      this.fireToaster(this.labels.successTitle, this.labels.appointmentHasBeenSaved, 'success');
    }
  }

  // Handles the deletion of the selected appointment.
  handleDelete(event) {
    // eslint-disable-next-line
    let answer = confirm(this.labels.confirmDeletion);
    if (answer === true) {
      deleteAppointment({ appointmentId: event.detail })
        .then(() => {
          let deletedEvent = this.calendar.getEventById(event.detail);
          if (deletedEvent != null) {
            // Refresh unplanned deliveryOrder pane.
            this.selAppointmentId = null;
            this.selConfirmed = false;
            this.selSpecial = false;
            this.insertUnplannedDeliveryOrder(deletedEvent);
            this.fireToaster(this.labels.successTitle, this.labels.appointmentSuccessfullyDeleted, 'success');
            deletedEvent.remove();
          }
        })
        .catch((error) => {
          console.log(error);
          this.fireToaster(this.labels.somethingWentWrongGettingAppointments, error.body.message, 'error');
        });
    }
  }
// Handle the creation on the calendar, of the new special (dummy) appointment.
  handleOnNewAppointment(event) {
    let oObject = JSON.parse(event.detail);
    let dEnd = new Date(oObject.start);
    dEnd.setMinutes(dEnd.getMinutes() + parseInt(oObject.duration, 10));
    let appt = {
      id: oObject.id,
      title: oObject.title,
      start: oObject.start,
      end: dEnd,
      backgroundColor: SPECIAL_APPT_BACKCOLOR,
      color: SPECIAL_APPT_TEXTCOLOR,
      extendedProps: {
        appointmentId: oObject.id,
        location: oObject.location,
        duration: oObject.duration,
        confirmed: false,
        provisioned: false,
        account: '',
        deliveryOrderId: '',
        contact: '',
        phone: '',
        wishdate: '',
        products: ''
      }
    };
    //console.log(appt);
    // Used in refresh to delete the manually added events.
    this.userEvents = [...this.userEvents, oObject.id];
    this.calendar.addEvent(appt);
    this.fireToaster(this.labels.successTitle, this.labels.appointmentSuccessfullyCreated, 'success');
  }

  // Fired when the user sets the Field Engineer dropdown list.
  handleEngineersListChange(event) {
    this.filterEngineerId = event.detail.value;
    this.filterEngineer = event.target.options.find(opt => opt.value === event.detail.value).label;
  }
  // Fired when the user sets the Regions dropdown list.
  handleRegionsListChange(event) {
    this.filterRegionId = event.detail.value;
    this.filterRegion = event.target.options.find(opt => opt.value === event.detail.value).label;
  }

  /**
   * Callback that executes when the user clicks on the Calendar's day header and
   * Open Google Maps with the route of the day.
   * @tutorial
   * For further information please go to http://www.fullcalendar.io
   */
  // eslint-disable-next-line
  navLinkDayClickCallback = (date, jsEvent) => {
    //console.log('coords', jsEvent.pageX, jsEvent.pageY);
    let nextDate = new Date(date);
    nextDate.setDate(nextDate.getDate() + 1);
    let appts = this.calendar.getEvents();
    let addrArray = [];
    for (let i = 0; i < appts.length; i++) {
      if (appts[i].start > date && appts[i].start < nextDate) {
        addrArray.push({ start: appts[i].start, loc: appts[i].extendedProps.location });
      }
    }
    if (addrArray.length > 0) {
      // Source: https://www.sitepoint.com/sort-an-array-of-objects-in-javascript/
      let arrOrdered = addrArray.sort(compareByStart);
      let joinedAddr = '';
      // console.log(arrOrdered);
      arrOrdered.forEach(element => {
        joinedAddr += element.loc + '/';
      });
      let baseUrl = 'https://www.google.com/maps/dir/';
      //let joinedAddr = addrArray.join('/');
      window.open(baseUrl + encodeURI(joinedAddr.substring(0, joinedAddr.length - 1)));
    } else {
      this.fireToaster('I apologize ...', this.labels.thereAreNoAppointmentsForThatDay, 'success');
    }
  }

  /**
   * Fired when the user clicks on each appointment on the Calendar.
   * Used to show information about the apponintment on the Appointment Details Pane.
   */
  eventClickCallback = (info) => {
    info.jsEvent.preventDefault();
    if(info.event.id === this.selAppointmentId) {
      return;
    }
    this.selSpecial = false;
    // This event shows information on Appointment Details Pane.
    if (Math.abs(info.event.id) > 0) {
      // is not a real Salesforce id so, show nothing on the pane.
      this.selAppointmentId = null;
      this.selConfirmed = false;

    } else {
      let oldEvent = this.calendar.getEventById(this.selAppointmentId);
      if (oldEvent != null && oldEvent !== undefined) {
        if (oldEvent.extendedProps.confirmed) {
          oldEvent.setProp('backgroundColor', oldEvent.extendedProps.provisioned === true? CONFIRMED_APPT_BACKCOLOR: CONFIRMED_UNP_APPT_BACKCOLOR);
          oldEvent.setProp('textColor', CONFIRMED_APPT_TEXTCOLOR);
        } else {
        // Check if its a Special appointment or not, before this.
          let currBackColor, currTextColor = '';
          if (!oldEvent.extendedProps.deliveryOrderId) {
            currBackColor = SPECIAL_APPT_BACKCOLOR;
            currTextColor = SPECIAL_APPT_TEXTCOLOR;
          } else if (oldEvent.extendedProps.provisioned === false) {
            currBackColor = UNCONFIRMED_UNPROV_APPT_BACKCOLOR;
          }

          oldEvent.setProp('backgroundColor', currBackColor);
          oldEvent.setProp('textColor', currTextColor);
          if(this.isFullUser) {
            oldEvent.setProp('startEditable', true);
            oldEvent.setProp('durationEditable', true);
            oldEvent.setProp('editable', true);
          }
        }
      }
      info.event.setProp('backgroundColor', SELECTED_APPT_BACKCOLOR);
      info.event.setProp('textColor', SELECTED_APPT_TEXTCOLOR);
      info.event.setProp('startEditable', false);
      info.event.setProp('durationEditable', false);
      info.event.setProp('editable', false);
      // set DetailBox
      this.template.querySelector('c-appointment-details-box').setSpinner();
      this.selAppointmentId = info.event.extendedProps.appointmentId;
      this.selContactId = info.event.extendedProps.contId;
      this.selConfirmed = info.event.extendedProps.confirmed;
      if (info.event.extendedProps.deliveryOrderId === undefined || info.event.extendedProps.deliveryOrderId === '') {
        this.selSpecial = true;
      }
    }
  }

  /**
   * Take an existing list of events from the backend and transform to an array accepted for the Calendar.
   * @param {JSON} dataset
   */
  parseAppointmentList(dataSet) {
    let dataEvents = [];
    for (let i = 0; i < dataSet.length; i++) {
        let dtEnd = new Date(dataSet[i].Start__c);
        dtEnd = dtEnd.setMinutes(dtEnd.getMinutes() + dataSet[i].Duration__c);
        let contactName, contactId, contactPhone, instWishDate, accountName, productNames, apptName, sProvisioned = '';
        let bProvisioned = false;
        if (dataSet[i].Contact__r !== undefined) {
          contactName = dataSet[i].Contact__r.Name;
          contactId = dataSet[i].Contact__c;
          contactPhone = dataSet[i].Contact__r.Phone;
        }
        if(dataSet[i].Related_Delivery_Order__r !== undefined) {
          productNames = dataSet[i].Related_Delivery_Order__r.Products__c;
          accountName = (dataSet[i].Related_Delivery_Order__r.Order__r === undefined || dataSet[i].Related_Delivery_Order__r.Order__r.csord__Account__r === undefined) ? 'N/A' : dataSet[i].Related_Delivery_Order__r.Order__r.csord__Account__r.Name;
          instWishDate = dataSet[i].Related_Delivery_Order__r.Wish_Date__c === undefined ? 'any' : dataSet[i].Related_Delivery_Order__r.Wish_Date__c;
          sProvisioned = dataSet[i].Related_Delivery_Order__r.Provisioned__c === false? PREFIX_UNPROVISIONED_APPT: '';
          bProvisioned = dataSet[i].Related_Delivery_Order__r.Provisioned__c;
        }
        apptName = sProvisioned + dataSet[i].Name;

        let appointment = {
          id: dataSet[i].Id,
          title: apptName,
          start: dataSet[i].Start__c,
          end: dtEnd,
          extendedProps: {
            appointmentId: dataSet[i].Id,
            location: dataSet[i].Location__c,
            duration: dataSet[i].Duration__c,
            confirmed: dataSet[i].Confirmed_Installation__c,
            provisioned: bProvisioned,
            account: accountName,
            deliveryOrderId: dataSet[i].Related_Delivery_Order__c,
            contact: contactName,
            contId: contactId,
            phone: contactPhone,
            wishdate: instWishDate,
            products: productNames
          }
        };
        if(!dataSet[i].Related_Delivery_Order__c) {
          appointment.backgroundColor = SPECIAL_APPT_BACKCOLOR;
          appointment.textColor = SPECIAL_APPT_TEXTCOLOR;
        } else if (dataSet[i].Confirmed_Installation__c) {
          appointment.backgroundColor = dataSet[i].Related_Delivery_Order__r.Provisioned__c === true? CONFIRMED_APPT_BACKCOLOR : CONFIRMED_UNP_APPT_BACKCOLOR;
          appointment.textColor = CONFIRMED_APPT_TEXTCOLOR;
          appointment.startEditable = false;
          appointment.durationEditable = false;
          appointment.editable = false;
        } else if (dataSet[i].Related_Delivery_Order__r.Provisioned__c === false) {
          appointment.backgroundColor = UNCONFIRMED_UNPROV_APPT_BACKCOLOR;
        }
        dataEvents.push(appointment);
    }
    return dataEvents;
  }

  // Callback used for the Calendar to show properly the appointments
  initAppointments = (info, successCallback, failureCallback) => {
    // Info properties example: endStr: "2020-07-19T00:00:00+02:00" | startStr: "2020-07-13T00:00:00+02:00" | timeZone: "local"
    getAppointments({ regionId: this.currRegionId, engineerId: this.currEngineerId, since: info.startStr, to: info.endStr })
      .then(result => {
        // Delete from the current calendar the events which has been added to userEvents Array (special appointments)
        for (let i = 0; i < this.userEvents.length; i++) {
          let currEvent = this.calendar.getEventById(this.userEvents[i]);
          if(currEvent !== null) {
            currEvent.remove();
          }
        }
        this.userEvents = [];
        let dataEvents = this.parseAppointmentList(result);
        successCallback(dataEvents);
      })
      .catch(error => {
        this.fireToaster(this.labels.errorTitle, this.labels.somethingWentWrongGettingAppointments, 'error');
        console.log('Failure on initAppointments (getAppointments)');
        console.log(error);
        failureCallback(error);
      });
      this.selAppointmentId = null;
  }

  // Executed for initialize the Calendar and Drag and Drop events.
  initializeFullCalendar() {
    // eslint-disable-next-line
    let Calendar = FullCalendar.Calendar;
    // eslint-disable-next-line
    let Draggable = FullCalendarInteraction.Draggable;

    let containerEl = this.template.querySelector('div.external-events');
    let calendarEl = this.template.querySelector('div.calendar');

    // eslint-disable-next-line
    new Draggable(containerEl, {
      itemSelector: '.fc-element',
      eventData: function (eventEl) {
        return {
          title: eventEl.innerText
        };
      }
    });

    // initialize the calendar
    // validRange: range in which to show the appointments
    // Calculate validRange: from this week, 1 week less and 2 weeks ahead.
    // eventConstraint: range in which the appointments can be changed, dropped.
    // Calculate dynamic eventConstraint (from this week, 3 week ahead)
    const DAYS_TO_LAST = 6;
    let dToday = new Date();
    let iAhead = parseInt(this.settings.weeksAhead, 10) + 1;
    let iBack = parseInt(this.settings.weeksBehind, 10) * -1 + 1;
    let iFofWeek = getFirstDayOfWeekInMs(dToday);
    let dFoWToday = new Date(+iFofWeek);
    let dFoWBackward = addWeeks(dFoWToday, iBack);
    let dFoWForward = addWeeks(dFoWToday, iAhead);
    let dLoWForward = addDays(dFoWForward, DAYS_TO_LAST);

    this.calendar = new Calendar(calendarEl, {
      plugins: ['interaction', 'dayGrid', 'timeGrid'],
      header: {
        left: 'prev,next',
        center: 'title',
        right: 'today'
      },
      validRange: {
        start: dFoWBackward,
        end: dLoWForward
      },
      eventConstraint: {
        start: dFoWToday,
        end: dLoWForward
      },
      nowIndicator: true,
      weekNumbers: true,
      navLinks: true,
      defaultView: 'timeGridWeek',
      allDaySlot: false,
      minTime: this.settings.calendarStartTime,
      maxTime: this.settings.calendarEndTime,
      hiddenDays: [0],
      editable: this.isFullUser,
      droppable: this.isFullUser,
      eventReceive: this.eventReceiveCallback,
      eventResize: this.eventResizeCallback,
      eventDrop: this.eventDropCallback,
      navLinkDayClick: this.navLinkDayClickCallback,
      eventClick: this.eventClickCallback,
      events: this.initAppointments
    });
    this.calendar.render();
  }

  // Initialization of the Calendar library/stylesheets
  paintCalendar() {
    Promise.all([
      // First step: load FullCalendar core
      loadStyle(this, FullCalendarJS + '/packages/core/main.min.css'),
      loadStyle(this, FullCalendarJS + '/packages/daygrid/main.min.css'),
      loadStyle(this, FullCalendarJS + '/packages/timegrid/main.min.css'),
      loadScript(this, FullCalendarJS + '/packages/core/main.js')
    ]).then(() => {
      // Second step: Load the plugins in a new promise
      Promise.all([
        loadScript(this, FullCalendarJS + '/packages/daygrid/main.min.js'),
        loadScript(this, FullCalendarJS + '/packages/timegrid/main.min.js'),
        loadScript(this, FullCalendarJS + '/packages/interaction/main.min.js')
      ]).then(() => {
        this.calendarCreated = true;
      });
    }).catch(error => {
      // Catch any error while loading the scripts here
      this.lastError = error;
      this.fireToaster(this.labels.errorTitle, error.body.message, 'error');
    })
  }

  connectedCallback() {
    if (this.scriptInitialized) {
      return;
    }
    this.scriptInitialized = true;
    this.waitDialog = true;
    this.paintCalendar();
  }
}