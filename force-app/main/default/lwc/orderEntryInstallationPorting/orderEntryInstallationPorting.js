import { LightningElement, api, track, wire } from 'lwc';
import { publish, MessageContext } from 'lightning/messageService';
import getBundle from '@salesforce/apex/OrderEntryProductController.getBundle';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

export default class OrderEntryInstallationPorting extends LightningElement {
	@wire(MessageContext)
	messageContext;

	@track isAddPorting = false;
	@track changeButtonTitle = false;
	index = -1;
	@track products = [];
	@track contractTerm = {};
	@track porting = {};

	@track _bundles;
	@api
	get bundles() {
		return this._bundles;
	}
	set bundles(value) {
		this._bundles = Object.assign([], value);
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */
	@api validate() {
		const productInput = this.template.querySelector('c-order-entry-mobile-porting-form');
		return productInput ? productInput.validate() : true;
	}

	handleCancel() {
		this.isAddPorting = false;
		this.changeButtonTitle = false;
	}

	/**
	 * descriptino handle delete
	 */
	async handleDelete() {
		let products = this.products;
		let contractTerm = this.contractTerm;
		let bundle = await getBundle({ products, contractTerm: contractTerm });
		let bundleJSON = {
			bundle,
			contractTerm,
			addons: [],
			promos: [],
			products,
			telephony: {},
			operatorSwitch: {},
			installation: {},
			type: 'Mobile'
		};
		// keep all the data in the bundle previously set
		bundleJSON = Object.assign({}, this._bundles[this.index], bundleJSON);
		delete bundleJSON.porting;
		this._bundles.splice(this.index, 1, bundleJSON);
		this.publishBundleChange(bundleJSON, this.index);
		this.isAddPorting = false;
		this.changeButtonTitle = false;
	}

	/**
	 * descriptino add porting
	 */
	addPortingPopUp(e) {
		this.index = e.target.dataset.index;
		this.contractTerm = this._bundles[e.target.dataset.index].contractTerm;
		this.products = this._bundles[e.target.dataset.index].products;
		this.porting = this.bundles[e.target.dataset.index]?.porting;
		this.isAddPorting = true;
		if (this.porting != null) {
			this.changeButtonTitle = true;
		}
	}

	/**
	 * descriptino handle confirm
	 */
	async handleConfirm(e) {
		let products = this.products;
		let contractTerm = this.contractTerm;
		let porting = {
			contractEndDate: e.detail.contractEndDate,
			type: e.detail.type,
			currentSimType: e.detail.currentSimType,
			mobileNumber: e.detail.mobileNumber,
			contractNumber: e.detail.contractNumber
		};
		let bundle = await getBundle({ products, contractTerm: contractTerm });
		let bundleJSON = {
			bundle,
			contractTerm,
			addons: [],
			promos: [],
			products,
			telephony: {},
			operatorSwitch: {},
			installation: {},
			type: 'Mobile',
			porting
		};
		// keep all the data in the bundle previously set
		bundleJSON = Object.assign({}, this._bundles[this.index], bundleJSON);
		this._bundles.splice(this.index, 1, bundleJSON);
		this.publishBundleChange(bundleJSON, this.index);
		this.isAddPorting = false;
		this.changeButtonTitle = false;
	}

	/**
	 * descriptino publish message
	 */
	publishBundleChange(bundle, index) {
		const msg = {
			sourceComponent: 'order-entry-product-config-mobile',
			payload: {
				event: 'product-bundle-change',
				data: { bundle, index }
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}
}
