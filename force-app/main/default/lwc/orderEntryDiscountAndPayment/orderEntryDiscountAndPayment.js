import { api, track, LightningElement, wire } from 'lwc';
import getBankAccountHolder from '@salesforce/apex/OrderEntryPaymentController.getBankAccountHolder';
import saveOpportunityProducts from '@salesforce/apex/OrderEntryProductController.saveOpportunityProducts';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSite from '@salesforce/apex/OrderEntrySiteController.getSite';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

export default class OrderEntryDiscountAndPayment extends LightningElement {
	@wire(MessageContext)
	messageContext;
	@api opportunityId;
	@api b2cInternetCustomer;
	@api primaryContact;
	@api journeyType;
	@track bankAccountHolder;
	@track showLoader = true;
	@track _payment;
	@track address = {};
	@track _installationSite = {};
	get isMobile() {
		return this.journeyType === 'Mobile';
	}
	@api
	get payment() {
		return this._payment;
	}
	set payment(value) {
		if (value) {
			this._payment = Object.assign({}, value);
		}
	}
	@api bundles;
	get fixedBundle() {
		return this.bundles?.find((bundle) => bundle.type === 'Fixed');
	}
	get mobileBundles() {
		return this.bundles.filter((bundle) => {
			return bundle.type === 'Mobile';
		});
	}
	@api
	get installationSite() {
		return this._installationSite;
	}
	set installationSite(value) {
		this._installationSite = Object.assign({}, value);
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */
	@api async validate() {
		const paymentDetailsCmp = this.template.querySelector('c-order-entry-payment-details');
		const validation = paymentDetailsCmp.validate();
		return validation;
	}

	/**
	 * save data
	 */
	@api async saveData() {
		await saveOpportunityProducts({ oppId: this.opportunityId });
		await this.template.querySelector('c-order-entry-payment-details').saveData();
	}

	/**
	 * get selected site data
	 * @returns sites
	 */
	getSelectedSiteData() {
		return getSite({ siteId: this._installationSite.siteId })
			.then((data) => {
				this.address = data;
			})
			.catch((error) => {
				this.handleError(error.description, error.message);
				console.error('Error: ' + JSON.stringify(error));
			});
	}

	// Set default values
	connectedCallback() {
		this.init();
	}

	/**
	 * set initial data
	 */
	async init() {
		try {
			this.bankAccountHolder = await getBankAccountHolder({
				contactId: this.primaryContact
			});
			this.showLoader = false;
		} catch (e) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Error occured',
					message: e.body ? e.body.message : e.message,
					variant: 'error',
					mode: 'sticky'
				})
			);
		}
		if (!this._payment) {
			await this.getSelectedSiteData();
			this._payment = {
				bankAccountHolder: this.bankAccountHolder || '',
				iban: '',
				paymentType: 'Direct Debit',
				billingChannel: 'Electronic Bill',
				street: this.address?.Site_Street__c || '',
				postalCode: this.address?.Site_Postal_Code__c || '',
				houseNumber: this.address?.Site_House_Number__c || '',
				houseNumberSuffix: this.address?.Site_House_Number_Suffix__c || '',
				city: this.address?.Site_City__c || ''
			};
		}
		this.publishMessage();
	}

	/**
	 * publis generci message
	 */
	publishMessage() {
		const msg = {
			sourceComponent: 'order-entry-payment-details',
			payload: {
				event: 'payment-changed',
				data: {
					payment: this._payment
				}
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}
}
