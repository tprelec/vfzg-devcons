import { LightningElement, api } from "lwc";

import LABEL_ADDRESS from "@salesforce/label/c.pp_Address";
import LABEL_KVK from "@salesforce/label/c.pp_KVK";
import LABEL_PHONE from "@salesforce/label/c.pp_Phone";

export default class RecordHeaderDetail extends LightningElement {
	@api record;
	@api objectName;

	label = {
		LABEL_ADDRESS,
		LABEL_KVK,
		LABEL_PHONE
	};
}