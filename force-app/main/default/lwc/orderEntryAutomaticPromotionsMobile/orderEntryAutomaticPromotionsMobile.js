import { LightningElement, api, track } from 'lwc';

export default class OrderEntryAutomaticPromotionsMobile extends LightningElement {
	@track _bundles;
	@api
	get bundles() {
		return this._bundles;
	}
	set bundles(value) {
		this._bundles = Object.assign([], value);
	}
}
