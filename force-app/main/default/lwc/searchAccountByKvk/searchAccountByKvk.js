import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import getRecentlyViewedAccounts from '@salesforce/apex/PP_SearchAccountController.getRecentlyViewedAccounts';
import searchAccountByKvkQuery from '@salesforce/apex/PP_SearchAccountController.searchAccountByKvk';

import LABEL_SEARCH from '@salesforce/label/c.LABEL_Search';
import LABEL_ERROR from '@salesforce/label/c.pp_Error';
import LABEL_CLEAR from '@salesforce/label/c.pp_Clear';
import LABEL_LOADING from '@salesforce/label/c.pp_Loading';
import LABEL_ADDRESS from '@salesforce/label/c.pp_Address';
import LABEL_KVK from '@salesforce/label/c.pp_KVK';
import LABEL_PHONE from '@salesforce/label/c.pp_Phone';
import LABEL_SEARCH_BY_KVK from '@salesforce/label/c.pp_Search_by_KVK';
import LABEL_ENTER_VALID_KVK from '@salesforce/label/c.pp_Enter_valid_KVK';
import LABEL_INVALID_KVK from '@salesforce/label/c.pp_Invalid_KVK';
import LABEL_OLBICO_SEARCH_ACCOUNT_ERROR from '@salesforce/label/c.olbico_Search_Account_Error';

const OBJECT_NAME_ACCOUNT = 'Account';

const ACCOUNT_NAME_FIELD = 'Name';
const ACCOUNT_PHONE_FIELD = 'Phone';
const ACCOUNT_KVK_NUMBER_FIELD = 'KVK_number__c';
const ACCOUNT_VISIT_ADDRESS_FIELD = 'LG_VisitAddress__c';

export default class SearchAccountByKvk extends LightningElement {
	ICON_URL_ACCOUNT = '/apexpages/slds/latest/assets/icons/standard-sprite/svg/symbols.svg#account';

	label = {
		LABEL_SEARCH,
		LABEL_CLEAR,
		LABEL_LOADING,
		LABEL_ADDRESS,
		LABEL_KVK,
		LABEL_PHONE,
		LABEL_SEARCH_BY_KVK,
		LABEL_ENTER_VALID_KVK,
		LABEL_INVALID_KVK
	};

	@api valueId;
	@api valueName;
	@api index;

	@track error;
	@track searchTermValue = '';

	accountName;
	accountAddress;
	accountPhone;

	recentlyViewedAccounts;
	selectedRecord;
	isLoading = false;

	disableSearch;

	@api
	get disabledComponent() {
		return this.disableSearch;
	}
	set disabledComponent(value) {
		this.disableSearch = value;
	}

	accountKvkNumberFromAura;

	@api
	get accountKvkNumber() {
		return this.accountKvkNumberFromAura;
	}
	set accountKvkNumber(value) {
		if (value) {
			this.searchTermValue = value;
			this.accountKvkNumberFromAura = value;
		}
	}

	handleKeyPress(event) {
		// Enter pressed
		if (event.keyCode === 13) {
			this.searchAccountByKvk();
		}
	}

	handleSearchIconClick() {
		this.recentlyViewedAccounts = null;
		this.searchAccountByKvk();
	}

	clearText() {
		this.searchTermValue = '';
		this.recentlyViewedAccounts = null;
		this.sendCustomEventAccountSelected(null, null, OBJECT_NAME_ACCOUNT);
	}

	handleFocus() {
		if (!this.searchTermValue) {
			this.showRecent();
		}
	}

	handleBlur() {
		this.recentlyViewedAccounts = null;
	}

	handleMouseDownListItem(event) {
		event.preventDefault();
	}

	handleKeyUp(event) {
		const searchKey = event.target.value;
		this.searchTermValue = searchKey;
		if (searchKey) {
			this.recentlyViewedAccounts = null;
		} else {
			this.showRecent();
		}
	}

	searchAccountByKvk() {
		if (/^([0-9]{8})$/.test(this.searchTermValue)) {
			this.hideErrorMessage();
		} else {
			this.showErrorMessage();
			return;
		}

		this.isLoading = true;

		searchAccountByKvkQuery({
			kvk: this.searchTermValue
		})
			.then((result) => {
				if (result.account) {
					this.selectedRecord = result.account;
					this.accountName = result.account[ACCOUNT_NAME_FIELD];
					this.accountAddress = result.account[ACCOUNT_VISIT_ADDRESS_FIELD];
					this.accountPhone = result.account[ACCOUNT_PHONE_FIELD];

					this.recentlyViewedAccounts = null;
					this.hideErrorMessage();
					this.sendCustomEventAccountSelected(this.selectedRecord, result.account.Id, OBJECT_NAME_ACCOUNT);
				} else {
					if (result.errorMessages) {
						this.showToastMessage(LABEL_ERROR, LABEL_OLBICO_SEARCH_ACCOUNT_ERROR, 'error');
					}
					this.sendCustomEventAccountSelected(null, null, OBJECT_NAME_ACCOUNT);
				}
			})
			.catch((error) => {
				this.showToastMessage(LABEL_ERROR, LABEL_OLBICO_SEARCH_ACCOUNT_ERROR, 'error');
				console.error('Error:', error);
			})
			.finally(() => {
				this.isLoading = false;
			});
	}

	showRecent() {
		getRecentlyViewedAccounts()
			.then((result) => {
				let stringResult = JSON.stringify(result);
				this.recentlyViewedAccounts = JSON.parse(stringResult);
			})
			.catch((error) => {
				console.error('Error:', error);
			});
	}

	connectedCallback() {
		if (this.valueId && this.valueName) {
			this.selectedRecord = {
				Name: this.valueName,
				Id: this.valueId
			};
		}
	}

	handleSelect(event) {
		let recordId = event.currentTarget.dataset.recordId;
		let selectRecord = this.recentlyViewedAccounts.find((selectedAccount) => {
			this.searchTermValue = selectedAccount[ACCOUNT_KVK_NUMBER_FIELD];
			this.accountName = selectedAccount[ACCOUNT_NAME_FIELD];
			this.accountAddress = selectedAccount[ACCOUNT_VISIT_ADDRESS_FIELD];
			this.accountPhone = selectedAccount[ACCOUNT_PHONE_FIELD];
			return selectedAccount.Id === recordId;
		});
		this.searchAccountByKvk();
		this.selectedRecord = selectRecord;
		this.recentlyViewedAccounts = null;
		this.hideErrorMessage();
		this.sendCustomEventAccountSelected(selectRecord, recordId, OBJECT_NAME_ACCOUNT);
	}

	showErrorMessage() {
		this.template.querySelector('[data-id="search-input"]').classList.add('slds-has-error');
		this.template.querySelector('[data-id="error-search-input"]').classList.add('slds-visible');
		this.template.querySelector('[data-id="error-search-input"]').classList.remove('slds-hidden');
	}

	hideErrorMessage() {
		this.template.querySelector('[data-id="search-input"]').classList.remove('slds-has-error');
		this.template.querySelector('[data-id="error-search-input"]').classList.add('slds-hidden');
		this.template.querySelector('[data-id="error-search-input"]').classList.remove('slds-visible');
	}

	sendCustomEventAccountSelected(selectedRecord, recordId, objectName) {
		const selectedEvent = new CustomEvent('accountselectedlwc', {
			bubbles: true,
			composed: true,
			cancelable: true,
			detail: {
				data: {
					record: selectedRecord,
					recordId: recordId,
					objectName: objectName
				}
			}
		});
		this.dispatchEvent(selectedEvent);
	}

	showToastMessage(title, message, variant) {
		const evt = new ShowToastEvent({
			title: title,
			message: message,
			variant: variant
		});
		this.dispatchEvent(evt);
	}
}
