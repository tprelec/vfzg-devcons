import { api, track, LightningElement } from 'lwc';
import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();

export default class OrderEntryBundlePromotion extends LightningElement {
	@api promotions = [];

	@api site;
	@api b2cInternetCustomer;

	@track _bundle;
	@api
	get bundle() {
		return this._bundle;
	}
	set bundle(value) {
		this._bundle = Object.assign({}, value);
	}

	@track selectedPromotion;

	get picklistPlaceholder() {
		return this.promotions && this.promotions.length > 0 ? 'Select Bundle Promotion' : 'No Bundle Promotions available';
	}

	get formattedPromotions() {
		return this.promotions?.map((bp) => ({
			value: bp.Id,
			label: bp.Promotion_Name__c
		}));
	}

	connectedCallback() {
		const bundlePromotion = this.bundle?.promos ? this.bundle.promos.find((p) => p.type === CONSTANTS.STANDARD_PROMOTION_TYPE) : null;
		this.selectedPromotion = bundlePromotion ? bundlePromotion.id : null;
	}

	/**
	 * Handle change on bundle promotion picklist
	 */
	handlePromotionChange(e) {
		if (e) {
			this.selectPromotion(e.detail.value);
		}
	}

	/**
	 * Set selected bundle promotion and dispatch to parent components
	 */
	selectPromotion(id) {
		const type = CONSTANTS.STANDARD_PROMOTION_TYPE;
		const promotionData = { id, type };
		this.dispatchEvent(new CustomEvent('selectedpromotion', { detail: promotionData, bubbles: false }));
	}

	handleRemovePromotion() {
		const promotionPicklist = this.template.querySelector('lightning-combobox.bundle-promotions');
		if (promotionPicklist.value) {
			promotionPicklist.value = null;
			this.selectPromotion(null);
		}
	}
}
