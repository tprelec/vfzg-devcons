import { api, LightningElement, track } from 'lwc';

export default class OrderEntrySummaryTable extends LightningElement {
	@track
	imeInterneta;

	@api
	bundles;
	get contractTerm() {
		let terms = [];
		if (this.bundles) {
			this.bundles.forEach((b) => {
				if (b.contractTerm) {
					let tempContractTerm = b.contractTerm === '0' ? '1' : b.contractTerm;
					if (terms.indexOf(tempContractTerm * 12) === -1) {
						terms.push(tempContractTerm * 12);
					}
				} else terms.push(0);
			});
		}
		return terms;
	}
	get showBody() {
		if (this.bundles && this.bundles[0]) {
			return true;
		}
		return false;
	}

	get discounts() {
		const discounts = [];
		this.bundles.forEach((b) => {
			(b.promos || []).forEach((p) => {
				discounts.push(...(p.discounts || []));
			});
			discounts.forEach((d) => {
				if (!d.duration) {
					// TBD commented for test
					//d.duration = b.contractTerm;
				}
			});
		});

		return discounts.sort(this.compare);
	}

	/**
	 * Map discounts to array of durations
	 */
	get discountDurations() {
		return this.discounts.map((d) => d.duration);
	}

	/**
	 * Get array of unique durations
	 */
	get uniqueDurations() {
		return [...new Set(this.discountDurations)];
	}

	/**
	 * Make array of duration breakpoints
	 */
	get discountDurationBreakpoints() {
		const breakpoints = [];
		const longestDuration = this.uniqueDurations[this.uniqueDurations.length - 1];
		let durations =
			!longestDuration || longestDuration <= Math.max.apply(null, this.contractTerm)
				? [...this.uniqueDurations, ...this.contractTerm]
				: [...this.uniqueDurations];
		durations = [...new Set(durations)];
		durations.sort(function (a, b) {
			return a - b;
		});
		let last = 0;
		if (!durations.length) {
			const from = 1;
			const to = this.contractTerm;
			breakpoints.push({
				label: `${from}-${to}`,
				from,
				to,
				to
			});
			return breakpoints;
		}
		durations.forEach((duration) => {
			const from = last + 1;
			const to = duration;
			const diff = to - last;
			last = to;
			breakpoints.push({
				label: `${from}-${to}`,
				from,
				to,
				diff
			});
		});
		if (breakpoints.length > 0 && breakpoints[0].to === 0) {
			breakpoints.splice(0, 1);
		}
		return breakpoints;
	}

	/**
	 * Extract all discounts from promotions and sort them by duration
	 */
	discountsSpecificBundle(bundle) {
		const discounts = [];

		(bundle.promos || []).forEach((p) => {
			discounts.push(...(p.discounts || []));
		});
		discounts.forEach((d) => {
			if (!d.duration) {
				// TBD commented for test
				//d.duration = bundle.contractTerm;
			}
		});

		return discounts.sort(this.compare);
	}

	/**
	 * Get selected bundle discount
	 */
	bundleDiscount(bundle) {
		return this.discountsSpecificBundle(bundle).filter((d) => d.level === 'Bundle');
	}

	/**
	 * Get only selected addon discounts
	 */
	get addonDiscounts() {
		return this.discounts.filter((d) => d.level === 'Add On');
	}

	get productsWithDiscounts() {
		const data = [];

		this.bundles.forEach((b) => {
			let bundle = b.bundle;
			let addons = b.addons || [];
			let details = this.getDetails(b);

			// build bundle item
			if (bundle) {
				data.push({
					...bundle,
					name: bundle.name,
					details,
					discount: this.bundleDiscount(b),
					prices: {
						monthly: this.discountDurationBreakpoints.map((bp) => {
							return {
								...bp,
								priceData: this.calculateRecurringDiscount(bp, bundle, this.bundleDiscount(b), b)
							};
						}),
						oneOff: {
							priceData: this.calculateOneOffDiscount(bundle, this.bundleDiscount(b))
						}
					},
					promoDescription: bundle.promoDescription,
					isLast: false
				});
			}

			// build addon items
			addons
				.filter((a) => a.quantity)
				.forEach((a) => {
					const adc = this.addonDiscounts.filter((ad) => ad.addon === a.id);
					data.push({
						...a,
						discount: adc,
						prices: {
							monthly: this.discountDurationBreakpoints.map((bp) => {
								return {
									...bp,
									priceData: this.calculateRecurringDiscount(bp, a, adc, b)
								};
							}),
							oneOff: {
								priceData: this.calculateOneOffDiscount(a, adc)
							}
						},
						isLast: false
					});
				});
			if (data.length) {
				data[data.length - 1].isLast = true;
			}
		});

		return data;
	}

	/**
	 * Calculate totals
	 */
	get totals() {
		const data = [];
		this.productsWithDiscounts.forEach((product) => {
			product.prices.monthly.forEach((p) => {
				let tempIndex = data.findIndex((d) => d.label === `${p.from}-${p.to}`);
				if (tempIndex === -1) {
					data.push({ label: `${p.from}-${p.to}`, price: p.priceData.price });
				} else {
					data[tempIndex].price = this.precisionRound(
						parseFloat(data[tempIndex].price.toString()) + parseFloat(p.priceData.price ? p.priceData.price.toString() : '0'),
						5
					);
				}
			});

			const oneOffLabel = 'oneOff';
			const oneOffTempIndex = data.findIndex((d) => d.label === oneOffLabel);

			if (oneOffTempIndex === -1) {
				data.push({
					label: oneOffLabel,
					price: this.precisionRound(
						parseFloat(product.prices.oneOff.priceData.price ? product.prices.oneOff.priceData.price.toString() : '0'),
						5
					)
				});
			} else {
				data[oneOffTempIndex].price = this.precisionRound(
					parseFloat(data[oneOffTempIndex].price.toString()) +
						parseFloat(product.prices.oneOff.priceData.price ? product.prices.oneOff.priceData.price.toString() : '0'),
					5
				);
			}
		});
		return data.map((i) => ({
			label: i.label,
			price: i.price
		}));
	}

	precisionRound(number, precision) {
		var factor = Math.pow(10, precision);
		return Math.round(number * factor) / factor;
	}
	/**
	 * Helper compare function for sorting discounts by duration
	 */
	compare(a, b) {
		const durationA = a.duration;
		const durationB = b.duration;

		let comparison = 0;
		if (durationA > durationB) {
			comparison = 1;
		} else if (durationA < durationB) {
			comparison = -1;
		}
		return comparison;
	}

	/**
	 * Calculate discount for product, breakpoint and discount for monthly recurring prices
	 */
	calculateRecurringDiscount(breakpoint, item, discounts, bundle) {
		let discountsList = discounts === undefined ? [] : discounts;

		// 1. Get recurring amount for 1 item
		let recurringUnitPrice = parseFloat(item.recurring);
		const oneOffPrice = parseFloat(item.totalOneOff);

		// 2. Calculate discount amounts (max = amount of 1 recurring amount)
		let discountAmounts = [];
		if (discountsList.length > 0) {
			discounts.forEach((discount) => {
				if (discount.duration >= breakpoint.to || discount.duration === 0) {
					if (discount.type === 'Percentage') {
						if (discount.value != null) discountAmounts.push((recurringUnitPrice * discount.value) / 100);
					} else if (discount.type === 'Discounted Amount') {
						if (discount.value != null) discountAmounts.push(discount.value);
					} else if (discount.type === 'Amount') {
						if (discount.value != null) discountAmounts.push(recurringUnitPrice - discount.value);
					}
				}
			});
		}
		let totalDiscount = 0;
		for (let i = 0; i < discountAmounts.length; i++) {
			totalDiscount += discountAmounts[i];
		}
		if (totalDiscount > recurringUnitPrice) {
			totalDiscount = recurringUnitPrice;
		}

		// 3. Calculate sum
		// Multiply quantity with recurring amount
		// Apply discount
		const quantity = item.quantity;
		let price = quantity * recurringUnitPrice - totalDiscount;
		if (bundle.contractTerm !== '0' && breakpoint.from > bundle.contractTerm * 12) {
			price = 0;
			return {
				hasPrice: false,
				isNull: price <= 0,
				formattedPrice: price !== undefined && price !== null ? parseFloat(price).toFixed(2) : null,
				price
			};
		}
		return {
			hasPrice: !!(price || !oneOffPrice),
			isNull: price <= 0,
			formattedPrice: price !== undefined && price !== null ? parseFloat(price).toFixed(2) : null,
			price
		};
	}

	/**
	 * Calculate discount for product, breakpoint and discount for one off prices
	 */
	calculateOneOffDiscount(item, discounts) {
		let oneOffUnitPrice = parseFloat(item.oneOff);

		let discountAmounts = [];
		if (oneOffUnitPrice && discounts && discounts.length > 0) {
			discounts.forEach((discount) => {
				if (discount.type === 'Percentage') {
					if (discount.oneOffValue != null) discountAmounts.push((oneOffUnitPrice * discount.oneOffValue) / 100);
				} else if (discount.type === 'Discounted Amount') {
					if (discount.oneOffValue != null) discountAmounts.push(discount.oneOffValue);
				} else if (discount.type === 'Amount') {
					if (discount.oneOffValue != null) discountAmounts.push(oneOffUnitPrice - discount.oneOffValue);
				}
			});
		}

		let totalDiscount = 0;
		for (let i = 0; i < discountAmounts.length; i++) {
			totalDiscount += discountAmounts[i];
		}
		if (totalDiscount > oneOffUnitPrice) {
			totalDiscount = oneOffUnitPrice;
		}

		const quantity = item.quantity;
		let price = quantity * oneOffUnitPrice - totalDiscount;

		return {
			hasPrice: price >= 0 && item.oneOff >= 0 && item.type !== 'Internet Security',
			isNull: price <= 0,
			formattedPrice: price !== undefined && price !== null ? parseFloat(price).toFixed(2) : null,
			price
		};
	}

	/**
	 * Get details of the products in the bundle [Call, Data, SMS]
	 * @param {*} bundle
	 * @returns
	 */
	getDetails(bundle) {
		let details = [];
		if (bundle.details) {
			// get details in the list of strings
			if (bundle.type === 'Fixed') {
				Object.entries(bundle.details).forEach(([key, value]) => {
					details.push(key + ': ' + value);
				});
			}
			if (bundle.details['Unlimited Voice'] === 'Yes') details.push('Calls: Unlimited');
			else if (bundle.details['Voice Minutes']) details.push('Calls: ' + bundle.details['Voice Minutes'] + ' Minutes');
			if (bundle.details['Unlimited SMS'] === 'Yes') details.push('SMS: Unlimited');
			else if (bundle.details['Number SMS']) details.push('SMS: ' + bundle.details['Number SMS']);
			if (bundle.details['Unlimited Internet'] === 'Yes') details.push('Data: Unlimited');
			else if (bundle.details['Data Size (GB)']) details.push('Data: ' + bundle.details['Data Size (GB)'] + 'GB');
		}
		return details;
	}
}
