import { LightningElement, api, wire, track } from 'lwc';
import getProducts from '@salesforce/apex/OrderEntryProductController.getProducts';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

const MAIN_PRODUCT_TYPE_NAME = 'Main';

export default class OrderEntryProductSelection extends LightningElement {
	@wire(MessageContext)
	messageContext;

	@track products = [];
	@track productOptions = [];
	@track selectedProductId = null;

	@api opportunityId;

	@api
	get productId() {
		return this.selectedProductId;
	}
	set productId(value) {
		this.selectedProductId = value;
		this.init();
	}

	init() {
		if (this.products.length) {
			if (!this.selectedProductId) {
				this.selectedProductId = this.productOptions[0].value;
				this.publishChange(this.selectedProductId);
			}
		}
	}

	@wire(getProducts, { oppId: '$opportunityId', productTypes: [MAIN_PRODUCT_TYPE_NAME] })
	productsWired({ error, data }) {
		if (data) {
			this.products = data;
			this.productOptions = data.map((p) => ({
				label: p.Name,
				value: p.Id
			}));
		} else if (error) {
			this.products = [];
			this.productOptions = [];
		}
		this.init();
	}

	/**
	 * checking input data
	 * @returns
	 */
	@api validate() {
		const input = this.template.querySelector('lightning-combobox');
		input.reportValidity();
		return input.checkValidity();
	}

	handleChange(e) {
		this.publishChange(e.detail.value);
	}

	/**
	 * publish generic message
	 * @param {*} productId
	 */
	publishChange(productId) {
		const msg = {
			sourceComponent: 'order-entry-product-selection',
			payload: {
				event: 'product-selected',
				data: {
					selectedProduct: this.products.filter((product) => {
						return product.Id === productId;
					})[0]
				}
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}
}
