import { api, LightningElement, track, wire } from 'lwc';
import getAddons from '@salesforce/apex/OrderEntryProductController.getAddons';
import getAddonPrice from '@salesforce/apex/OrderEntryProductController.getAddonPrice';
import { getConstants } from 'c/orderEntryConstants';

import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

const CONSTANTS = getConstants();

export default class OrderEntryProductTv extends LightningElement {
	@wire(MessageContext)
	messageContext;
	@api opportunityId;
	@track _products = [];
	@track addons;
	@track _selectedAddons = [];
	@track _selectedProduct;
	@api
	get addonsExist() {
		return this.addons && this.addons.length;
	}
	@api
	get products() {
		return this._products;
	}
	set products(value) {
		this._products = Object.assign([], value);
	}
	@api
	get selectedAddons() {
		return this._selectedAddons;
	}
	set selectedAddons(value) {
		this._selectedAddons = Object.assign([], value);
		this.init();
	}
	@api
	get selectedProduct() {
		return this._selectedProduct;
	}
	set selectedProduct(value) {
		this._selectedProduct = Object.assign({}, value);
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */
	@api validate() {
		const productInput = this.template.querySelector('c-order-entry-tv-product-form');
		const mediaboxInput = this.template.querySelector('c-order-entry-tv-additional-form');
		return productInput.validate() && (mediaboxInput ? mediaboxInput.validate() : true);
	}

	/**
	 * handle product change
	 * @param {*} event
	 */
	async handleProductChange(event) {
		this._selectedProduct = { id: event.detail, type: CONSTANTS.TV_PRODUCT_TYPE };
		this._selectedAddons = [];
		this.buildAddons();
		this.publishProductChanged();
	}

	/**
	 * set initial data
	 */
	async init() {
		this.buildAddons();
	}

	/**
	 * Get index of TV product
	 */
	get tvProductIndex() {
		return (this._state?.products || []).findIndex((p) => p.type === CONSTANTS.TV_PRODUCT_TYPE);
	}

	/**
	 * build addon data
	 * @param {*} param0
	 * @returns
	 */
	async buildAddonData({ type, id, quantity }) {
		const addon = this.addons.find((a) => a.Add_On__r.Id === id);

		if (addon) {
			const price = await getAddonPrice({ code: addon.Add_On__r.Product_Code__c });
			const recurring =
				addon.Recurring_Price_Override__c || addon.Recurring_Price_Override__c === 0
					? addon.Recurring_Price_Override__c
					: price.cspmb__Recurring_Charge__c;
			const oneOff =
				addon.One_Off_Price_Override__c || addon.One_Off_Price_Override__c === 0
					? addon.One_Off_Price_Override__c
					: price.cspmb__One_Off_Charge__c;
			return {
				id,
				quantity,
				type,
				name: addon.Add_On__r.Name,
				parentProduct: addon.Product__c,
				code: addon.Add_On__r.Product_Code__c,
				recurring: recurring,
				oneOff: oneOff,
				totalRecurring: parseFloat((recurring * quantity).toFixed(2)),
				totalOneOff: parseFloat((oneOff * quantity).toFixed(2)),
				bundleName: price.Product_Name__c,
				parentType: CONSTANTS.TV_PRODUCT_TYPE
			};
		}
		return null;
	}

	/**
	 * Handle change of addons from child components
	 */
	setAddon(addonData) {
		if (addonData) {
			const addons = this.selectedAddons || [];
			let addonIndex = -1;
			if (addonData.type === CONSTANTS.TV_MEDIABOX_TYPE || addonData.type === CONSTANTS.TV_CHANNELS_TYPE) {
				addonIndex = addons.findIndex((a) => a.id === addonData.id);
			} else {
				addonIndex = addons.findIndex((a) => a.type === addonData.type);
			}

			if (addonIndex === -1) {
				addons.push(addonData);
			} else if (addonIndex > -1 && addonData.quantity > 0) {
				addons[addonIndex] = addonData;
			} else if (addonIndex > -1 && addonData.quantity < 1) {
				addons.splice(addonIndex, 1);
			}

			this._selectedAddons = addons;
		}
	}

	/**
	 * Handle change of addons from child components
	 */
	async handleChangedAddon(e) {
		this.setAddon(await this.buildAddonData(e.detail));
		this.publishProductChanged();
	}

	handleRenderedChange() {
		if (this.products.find((el) => el.Id === this.selectedProduct.id) !== undefined) {
			this.publishProductChanged('rendered_changed');
		}
	}

	publishProductChanged(eventName) {
		const msg = {
			sourceComponent: 'order-entry-tv-products',
			payload: {
				event: eventName ? eventName : 'product-changed',
				data: {
					product: this.products.find((el) => el.Id === this.selectedProduct.id),
					addons: this.selectedAddons || []
				}
			}
		};

		publish(this.messageContext, genericChannel, msg);
	}

	async buildAddons() {
		this.addons = await getAddons({
			oppId: this.opportunityId,
			parentProduct: this.selectedProduct.id
		});

		for (let i = 0; i < this.addons.length; i++) {
			const addon = this.addons[i];
			if (addon.Automatic_Add_On__c) {
				this.setAddon(
					await this.buildAddonData({
						type: addon.Add_On__r.Type__c,
						id: addon.Add_On__r.Id,
						quantity: 1
					})
				);
			}
		}

		const stateAddons = this.selectedAddons || [];
		const otherAddons = stateAddons.filter((a) => a.parentType !== CONSTANTS.TV_PRODUCT_TYPE);
		const tvAddons = stateAddons.filter((a) => a.parentType === CONSTANTS.TV_PRODUCT_TYPE && a.parentProduct === this.selectedProduct.id);

		this._selectedAddons = [...otherAddons, ...tvAddons];
	}
}
