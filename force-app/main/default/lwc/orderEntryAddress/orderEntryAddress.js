import { LightningElement, api } from 'lwc';

export default class OrderEntryAddress extends LightningElement {
	@api site;
	@api accountId;
	@api opportunityId;
	@api journeyType;

	get isFixed() {
		return this.journeyType === 'Fixed';
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */
	connectedCallback() {}
	@api validate() {
		if (this.validateAddressCheck()) {
			return this.validateInstallationAddress();
		}
		return false;
	}

	validateInstallationAddress() {
		return this.template
			.querySelector('c-order-entry-installation-address')
			.validate()
			.then((result) => {
				return result;
			});
	}

	validateAddressCheck() {
		if (this.journeyType === 'Fixed') {
			return this.template.querySelector('c-order-entry-address-check').validate();
		}
		return true;
	}
}
