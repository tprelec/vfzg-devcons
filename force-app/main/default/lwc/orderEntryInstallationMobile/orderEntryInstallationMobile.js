import { LightningElement, api, track, wire } from 'lwc';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';
import updatePortingNumber from '@salesforce/apex/OrderEntryPortingController.updatePortingNumber';
import saveOpportunityDescription from '@salesforce/apex/OrderEntryInstallationController.saveOpportunityDescription';

export default class OrderEntryInstallationMobile extends LightningElement {
	@wire(MessageContext)
	messageContext;
	@api bundles;
	@api mainProduct;
	@api products;
	@api opportunityId;

	@track _notes = {};
	@api
	get notes() {
		return this._notes;
	}
	set notes(value) {
		this._notes = value;
	}

	/**
	 * description save data
	 */
	@api
	async saveData() {
		await updatePortingNumber({ opportunityId: this.opportunityId });
		await saveOpportunityDescription({
			opportunityId: this.opportunityId,
			notes: this.notes
		});
	}

	/**
	 * description checking input data
	 */
	@api
	validate() {
		var resultPorting = this.template.querySelector('c-order-entry-installation-porting').validate();
		return resultPorting;
	}

	/**
	 * description publish notes value
	 */
	notesChange(e) {
		this._notes = e.detail;
		this.publishMessage('notes-changed', { notes: this.notes });
	}

	/**
	 * description publish generic message
	 */
	publishMessage(event, data) {
		const msg = {
			sourceComponent: 'order-entry-installation-fixed',
			payload: {
				event,
				data
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}
}
