import { LightningElement, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSites from '@salesforce/apex/ZiggoMigrationFieldsFormController.getFields';
import save from '@salesforce/apex/ZiggoMigrationFieldsFormController.save';

import installationInformationCard from '@salesforce/label/c.ZiggoMigrationInformation_ZiggoMigrationInformationCard';
import saveButton from '@salesforce/label/c.ZiggoMigrationInformation_SaveButton';
import SiteActionsCard from '@salesforce/label/c.ZiggoMigrationInformation_SitesActionsCard';
import actionsHelpText from '@salesforce/label/c.ZiggoMigrationInformation_SiteActionsHelpText';
import SetForAllSitesButton from '@salesforce/label/c.ZiggoMigrationInformation_SetForAllSitesButton';
import ClearButton from '@salesforce/label/c.ZiggoMigrationInformation_ClearButton';
import installationSitesCard from '@salesforce/label/c.ZiggoMigrationInformation_SitesCard';
import fZiggoSmallProduct_Validation from '@salesforce/label/c.ZiggoMigrationInformation_fZiggoSmallProduct_Validation';
import coaxProductActive_Validation from '@salesforce/label/c.ZiggoMigrationInformation_CoaxProductActive_Validation';

export default class ZiggoMigrationFieldsForm extends LightningElement {
	@api recordId;

	inputJson;
	fields;
	sites;
	informedValues;
	isLoading;

	label = {
		saveButton,
		installationInformationCard,
		installationSitesCard,
		SiteActionsCard,
		actionsHelpText,
		SetForAllSitesButton,
		ClearButton,
		fZiggoSmallProduct_Validation,
		coaxProductActive_Validation
	};

	get showAccordionSection() {
		return this.inputJson !== null && this.inputJson !== undefined;
	}

	connectedCallback() {
		this.isLoading = true;
		getSites({ recordId: this.recordId })
			.then((result) => {
				if (result.body !== undefined) {
					this.sites = result.body;
					this.fields = new Set();
					for (let row of result.body[0].listRows) {
						for (let field of row.listFields) {
							this.fields.add(field);
						}
					}
					this.inputJson = JSON.stringify(result.body);
				} else if (result.body === undefined && result.variant && result.message) {
					if (result.variant === 'error') {
						this.isError = true;
					}
				}
			})
			.catch((error) => {
				this.isError = true;
				this.showToast('error', error.body.message);
			});
	}

	handleChange(event) {
		let fieldName = event.target.name;
		if (this.informedValues === null || this.informedValues === undefined) {
			this.informedValues = new Map();
		}
		this.informedValues.set(fieldName, event.detail.value);
	}

	setInformation(event) {
		let fieldName = event.target.name;
		let result = this.template.querySelector('c-accordion-form');
		result.setFieldValue(fieldName, this.informedValues.get(fieldName));
	}

	clearInformation(event) {
		let fieldName = event.target.name;
		this.template.querySelector('c-accordion-form').setFieldValue(fieldName, null);
		this.template.querySelectorAll('lightning-input, lightning-input-field').forEach((element) => {
			if (fieldName === '' + element.name) {
				element.value = null;
			}
		});
	}

	async save() {
		let valueDTV;
		let valueExistingSubcription;
		let coaxProductAlreadyActiveOnSite;
		let valueClientNumber;
		let valueContractNumber;
		let valueIPAddress;
		let valueInternet;
		let valueMultiWifi;
		let valueZakelijkHostedBellen;
		this.isLoading = true;
		let result = this.template.querySelector('c-accordion-form').getUpdatedInformation();
		for (let section of result) {
			for (let row of section.listRows) {
				for (let field of row.listFields) {
					if (field.fieldeReference === 'LG_DTV__c') {
						valueDTV = field.value;
					} else if (field.fieldeReference === 'LG_ExistingSubscription__c') {
						valueExistingSubcription = field.value;
					} else if (field.fieldeReference === 'Coax_Product_Already_Active_On_Site__c') {
						coaxProductAlreadyActiveOnSite = field.value;
					} else if (field.fieldeReference === 'LG_ClientNumber__c') {
						valueClientNumber = field.value;
					} else if (field.fieldeReference === 'Contract_Number__c') {
						valueContractNumber = field.value;
					} else if (field.fieldeReference === 'LG_IPAddress__c') {
						valueIPAddress = field.value;
					} else if (field.fieldeReference === 'LG_Internet__c') {
						valueInternet = field.value;
					} else if (field.fieldeReference === 'Multi_Wifi__c') {
						valueMultiWifi = field.value;
					} else if (field.fieldeReference === 'Zakelijk_Hosted_Bellen__c') {
						valueZakelijkHostedBellen = field.value;
					}
				}
			}
			if (!this.validationSuccessful(coaxProductAlreadyActiveOnSite, valueDTV, valueExistingSubcription)) {
				this.showToast('error', this.label.coaxProductActive_Validation);
				this.isLoading = false;
				return;
			}
			if (
				!this.validationZiggoSmallProductSuccessful(
					valueExistingSubcription,
					valueClientNumber,
					valueContractNumber,
					valueIPAddress,
					valueInternet,
					valueMultiWifi,
					valueZakelijkHostedBellen
				)
			) {
				this.showToast('error', this.label.fZiggoSmallProduct_Validation);
				this.isLoading = false;
				return;
			}
		}

		let resultSave = await save({
			recordId: this.recordId,
			jsonInput: JSON.stringify(result)
		});
		if (resultSave.variant && resultSave.message) {
			this.showToast(resultSave.variant, resultSave.message);
		}
		this.isLoading = false;
	}

	validationSuccessful(valueCoaxProductAlreadyActiveOnSite, valueDTV, valueExistingSubcription) {
		if (valueCoaxProductAlreadyActiveOnSite === 'Yes' && (!valueDTV || !valueExistingSubcription)) {
			return false;
		} else {
			return true;
		}
	}

	validationZiggoSmallProductSuccessful(ExistingSubscription, ClientNumber, ContractNumber, IPAddress, Internet, MultiWifi, ZakelijkHostedBellen) {
		if (
			ExistingSubscription === 'fZiggo Small product' &&
			(!Internet || !ClientNumber || !IPAddress || !MultiWifi || !ZakelijkHostedBellen || !ContractNumber)
		) {
			return false;
		} else {
			return true;
		}
	}

	onloadData() {
		this.template.querySelectorAll('lightning-input-field').forEach((element) => {
			if (element.value === undefined) {
				this.isLoading = true;
			} else {
				this.isLoading = false;
			}
		});
	}

	showToast(variant, message) {
		const event = new ShowToastEvent({
			variant: variant,
			message: message
		});
		this.dispatchEvent(event);
	}
}
