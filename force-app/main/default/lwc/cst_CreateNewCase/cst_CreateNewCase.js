import { LightningElement, api } from 'lwc';
import { FlowNavigationNextEvent } from 'lightning/flowSupport';
import Question_Image from '@salesforce/resourceUrl/CST_VFZiggo_Question';
import Change_Image from '@salesforce/resourceUrl/CST_VFZiggo_Change';
import Incident_Image from '@salesforce/resourceUrl/CST_VFZiggo_Incident';

//import Incident_Image from '@salesforce/resourceUrl/CST_VFZiggo_Change';
//import Question_Image from '@salesforce/resourceUrl/CST_VFZiggo_Change';

export default class cst_CreateNewCase extends LightningElement {
	@api availableActions = [];
	@api outputTest;

	questionImageURL = Question_Image;
	changeImageURL = Change_Image;
	incidentImageURL = Incident_Image;

	imageQuestionHandler(event) {
		if (this.availableActions.find((action) => action === 'NEXT')) {
			const navigateImageEvent = new FlowNavigationNextEvent();
			this.dispatchEvent(navigateImageEvent);
			this.outputTest = event.target.value;
		}
		return this.outputTest;
	}

	imageChangeHandler(event) {
		if (this.availableActions.find((action) => action === 'NEXT')) {
			const navigateImageEvent = new FlowNavigationNextEvent();
			this.dispatchEvent(navigateImageEvent);
			this.outputTest = event.target.value;
		}
		return this.outputTest;
	}

	imageIncidentHandler(event) {
		if (this.availableActions.find((action) => action === 'NEXT')) {
			const navigateImageEvent = new FlowNavigationNextEvent();
			this.dispatchEvent(navigateImageEvent);
			this.outputTest = event.target.value;
		}
		return this.outputTest;
	}
}
