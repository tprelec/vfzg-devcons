import { LightningElement, track } from 'lwc';


export default class CtnDownloadModal extends LightningElement {
    get options() {
		return [
			{ label: ";", value: ";" },
			{ label: ",", value: "," }
		];
	}
    @track separator = ';';

    handleClose() {
       this.dispatchEventClick("closemodal", false);
        
    }
    dispatchEventClick(eventName, eventValue) {
		this.dispatchEvent(new CustomEvent(eventName, { detail: eventValue, bubbles: false }));
	}

    handleDownload() {
        this.dispatchEventClick("download", this.separator);
    }

    handleChangeSeparator(event) {        
        this.separator = event.target.value;
    }
}