import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';

import getSites from '@salesforce/apex/InstallationInformationController.getSites';
import save from '@salesforce/apex/InstallationInformationController.save';

import PRODUCT_CONFIGURATION_OBJECT from '@salesforce/schema/cscfga__Product_Configuration__c';

import actionsHelpText from '@salesforce/label/c.InstallationInformation_SiteActionsHelpText';
import noSitesFoundtext from '@salesforce/label/c.InstallationInformation_NoInstallationSitesWereFound';
import installationInformationCard from '@salesforce/label/c.InstallationInformation_InstallationInformationCard';
import saveButton from '@salesforce/label/c.InstallationInformation_SaveButton';
import SiteActionsCard from '@salesforce/label/c.InstallationInformation_SitesActionsCard';
import SetForAllSitesButton from '@salesforce/label/c.InstallationInformation_SetForAllSitesButton';
import ClearButton from '@salesforce/label/c.InstallationInformation_ClearButton';
import installationSitesCard from '@salesforce/label/c.InstallationInformation_InstallationSitesCard';

export default class InstallationInformation extends LightningElement {
	@api recordId;
	@api readOnly;
	@api required;
	@api outputValidation = null;

	label = {
		actionsHelpText,
		noSitesFoundtext,
		installationInformationCard,
		saveButton,
		SiteActionsCard,
		SetForAllSitesButton,
		ClearButton,
		installationSitesCard
	};

	siteId;
	sites;
	objectApiName;
	technicalContactApiName;
	terminationReasonApiName;
	terminationSubReasonApiName;
	terminationDiscountTypeApiName;
	discountApproverApiName;
	wishDateFieldLabel;
	terminationDateLabel;
	mapSites;
	mapFullSites;
	accordionActiveSection;
	isLoading = true;
	onloadRunAlready = false;
	technicalContactValue = null;
	wishDateValue = null;
	terminationDate = null;
	terminationReason = null;
	terminationSubReason = null;
	terminationFeeDiscount = null;
	terminationFeeDiscountType = null;
	visible = false;
	sitesAvialableFlag = false;
	isMutation = false;
	isAcquisition = false;
	isMove = false;
	isError = false;

	get columSiteSize() {
		let columSize = this.readOnly ? 3 : 2;
		return `slds-col slds-size_${columSize}-of-3`;
	}

	get showReadOnlySection() {
		return !this.readOnly;
	}

	get isMutationOrMove() {
		return this.isMove || this.isMutation;
	}

	get showNoSitesSection() {
		return !this.sitesAvialableFlag && !this.isError;
	}

	get showActionSections() {
		return this.sitesAvialableFlag && !this.isError;
	}

	get isSetTechnicalButtonDisable() {
		return this.technicalContactValue === null || this.technicalContactValue === undefined;
	}

	get isSetWishDateButtonDisable() {
		return this.wishDateValue === null || this.wishDateValue === undefined;
	}

	get isSetTerminationDateButtonDisable() {
		return this.terminationDate === null || this.terminationDate === undefined;
	}

	get isSetTerminationReasonButtonDisable() {
		return this.terminationReason === null || this.terminationReason === undefined;
	}

	get isSetTerminationFeeDiscountButtonDisable() {
		return this.terminationFeeDiscount === null || this.terminationFeeDiscount === undefined;
	}

	get isSetTerminationSubReasonButtonDisable() {
		return this.terminationSubReason === null || this.terminationSubReason === undefined;
	}

	@wire(getObjectInfo, { objectApiName: PRODUCT_CONFIGURATION_OBJECT })
	productConfigurationInfoHandler({ data, error }) {
		if (data) {
			this.wishDateFieldLabel = data.fields.Installation_Wish_Date__c.label;
			this.terminationDateLabel = data.fields.Termination_Wish_Date__c.label;
			this.terminationDiscountLabel = data.fields.Termination_Fee_Discount__c.label;
		} else if (error) {
			this.isError = true;
			this._showToast('error', JSON.stringify(error));
		}
	}

	connectedCallback() {
		getSites({ recordId: this.recordId })
			.then((result) => {
				if (result.body !== undefined) {
					this.sites = result.body;
					this.objectApiName = 'Site__c';
					this.technicalContactApiName = 'Technical_Contact__c';
					this.terminationReasonApiName = 'Termination_Reason__c';
					this.terminationSubReasonApiName = 'Termination_Sub_Reason__c';
					this.terminationDiscountTypeApiName = 'Termination_Fee_Discount_Type__c';
					this.discountApproverApiName = 'Discount_Approver__c';
					this.sitesAvialableFlag = true;
					this.outputValidation = true;
					this.mapSites = new Map();
					this.mapFullSites = new Map();
					let sitesSet = new Set();

					for (let i = 0; i < this.sites.length; i++) {
						this.siteId = this.sites[i].siteId;
						this.sites[i].isRequired = false;

						if (this.sites[i].cancelledByChange) {
							this.isMutation = true;
							this.sites[i].isMutation = true;
							this.sites[i].isMutationOrMove = true;
							this.sites[i].isAcquisitionOrMutation = true;
						}

						if (!this.sites[i].cancelledByChange && !this.sites[i].moveSite) {
							this.isAcquisition = true;
							this.sites[i].isAcquisition = true;
							this.sites[i].isAcquisitionOrMutation = true;
							this.sites[i].todayDate = new Date().toLocaleDateString();
						}

						if (this.sites[i].moveSite) {
							this.isMove = true;
							this.sites[i].isMove = true;
							this.sites[i].isMutationOrMove = true;
						}

						if (this.isMove || this.readOnly) {
							this.sites[i].disableTerminationReasons = true;
						} else {
							this.sites[i].disableTerminationReasons = false;
						}

						this.sites[
							i
						].terminationDateError = `The Termination Wish Date can't be before the Contract Start Date ${this.sites[i].contractStartDate}.`;

						this.sites[i].inatallationDateError = `The Installation Wish Date can't be before today.`;

						if (!sitesSet.has(this.sites[i].siteId)) {
							sitesSet.add(this.sites[i].siteId);
							this.mapFullSites.set(this.sites[i].siteId, [this.sites[i]]);
							this.mapSites.set(this.sites[i].id, this.sites[i]);

							if (!this.sites[i].wishDate || !this.sites[i].technicalContact) {
								this.outputValidation = false;
							}

							if (this.sites[i].cancelledByChange || this.sites[i].isMove || this.required) {
								this.sites[i].isRequired = true;
							}
						} else {
							this.mapFullSites.get(this.sites[i].siteId).push(this.sites[i]);
						}
					}

					let calculationResult;
					for (let site of this.mapSites.values()) {
						calculationResult = this.calculateTerminationFeeTotal(site, this.mapFullSites.get(site.siteId));
						if (calculationResult) {
							site.terminationFeeWithFormat = '€ ' + calculationResult;
						} else {
							site.terminationFee = null;
						}
					}

					this.sites = Array.from(this.mapSites.values());
				} else if (result.body === undefined && result.variant && result.message) {
					if (result.variant === 'error') {
						this.isError = true;
					}
					this.showToast(result.variant, result.message);
				}
			})
			.catch((error) => {
				this.isError = true;
				this.showToast('error', error.body.message);
			})
			.finally(() => {
				this.isLoading = false;
			});
	}

	handleToggleSection(event) {
		this.accordionActiveSection = event.detail.openSections;
	}

	calculateTerminationFee(monthlyCharge, contractPeriod, contractEndDate, terminationWishDate, terminationDiscount) {
		if (!monthlyCharge || !contractEndDate || !terminationWishDate || !terminationDiscount) {
			return null;
		}
		contractEndDate = new Date(contractEndDate);
		terminationWishDate = new Date(terminationWishDate);
		let remainingContractPeriod = (contractEndDate.getFullYear() - terminationWishDate.getFullYear()) * 12;
		remainingContractPeriod -= terminationWishDate.getMonth();
		remainingContractPeriod += contractEndDate.getMonth();
		remainingContractPeriod <= 0 ? 0 : remainingContractPeriod;
		return monthlyCharge * remainingContractPeriod - monthlyCharge * remainingContractPeriod * (terminationDiscount / 100);
	}

	calculateTerminationFeeTotal(updatedProduct, products) {
		let terminationTotalFee = 0;
		let calculationResult;

		if (!products && products.length <= 0) {
			return null;
		}

		for (let product of products) {
			calculationResult = this.calculateTerminationFee(
				product.totalRecurringCharge,
				'',
				updatedProduct.contractEndDate,
				updatedProduct.terminationDate,
				updatedProduct.terminationDiscount
			);

			if (calculationResult) {
				if (calculationResult > 0) {
					product.terminationFee = calculationResult;
				} else {
					product.terminationFee = 0;
				}
				terminationTotalFee += product.terminationFee;
			}
		}

		for (let product of products) {
			product.terminationFeeTotal = terminationTotalFee;
		}
		return terminationTotalFee;
	}

	validateTerminationContractDates(terminationWishDate, contractStartDate) {
		contractStartDate = new Date(contractStartDate);
		terminationWishDate = new Date(terminationWishDate);
		if (terminationWishDate < contractStartDate) {
			return false;
		} else {
			return true;
		}
	}

	changeFieldHandler(event) {
		let newValue = event.target.value;
		const fieldName = event.target.name;
		const currentSection = this.accordionActiveSection;
		const tempObject = { ...this.mapSites.get(currentSection) };
		if (!newValue) {
			newValue = null;
		}
		tempObject[fieldName] = newValue;
		if (fieldName === 'discountApprover') {
			this.mapSites = this.mappingValuesToMap('discountApprover', newValue, this.mapSites);
			this.sites = Array.from(this.mapSites.values());
		} else if (fieldName === 'terminationDate' && newValue && this.validateTerminationContractDates(newValue, tempObject.contractStartDate)) {
			tempObject.terminationFeeWithFormat = '€ ' + this.calculateTerminationFeeTotal(tempObject, this.mapFullSites.get(tempObject.siteId));
		} else if (
			fieldName === 'terminationDiscount' &&
			newValue &&
			this.validateTerminationContractDates(tempObject.terminationDate, tempObject.contractStartDate)
		) {
			tempObject.terminationFeeWithFormat = '€ ' + this.calculateTerminationFeeTotal(tempObject, this.mapFullSites.get(tempObject.siteId));
		} else if (fieldName === 'terminationDate') {
			tempObject.terminationFee = null;
			tempObject.terminationFeeCalculation = null;
		}
		this.mapSites.set(tempObject.id, tempObject);
		this.sites = Array.from(this.mapSites.values());
	}

	handleChange(event) {
		let fieldName = event.target.name;
		switch (fieldName) {
			case 'technicalContact':
				this.technicalContactValue = event.detail.value[0];
				break;
			case 'wishDate':
				this.wishDateValue = event.detail.value;
				break;
			case 'terminationDate':
				this.terminationDate = event.detail.value;
				break;
			case 'terminationReason':
				this.terminationReason = event.detail.value;
				break;
			case 'terminationDiscount':
				this.terminationFeeDiscount = event.detail.value;
				break;
			case 'terminationSubReason':
				this.terminationSubReason = event.detail.value;
				break;
		}
	}

	setTechnicalContact() {
		this.mapSites = this.mappingValuesToMap('technicalContact', this.technicalContactValue, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	setWishDate() {
		this.mapSites = this.mappingValuesToMap('wishDate', this.wishDateValue, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	setTerminationWishDate() {
		let tempObject;
		this.mapSites = this.mappingValuesToMap('terminationDate', this.terminationDate, this.mapSites);
		for (let site of this.mapSites.values()) {
			tempObject = { ...site };
			if (this.terminationDate && this.validateTerminationContractDates(this.terminationDate, tempObject.contractStartDate)) {
				tempObject.terminationFeeWithFormat = '€ ' + this.calculateTerminationFeeTotal(tempObject, this.mapFullSites.get(tempObject.siteId));
			} else {
				tempObject.terminationFee = null;
				tempObject.terminationFeeCalculation = null;
			}
			this.mapSites.set(tempObject.id, tempObject);
		}
		this.sites = Array.from(this.mapSites.values());
		let elements = [...this.template.querySelectorAll('[data-element="terminationDateInput"]')];
		for (let i = 0; i < elements.length; i++) {
			elements[i].focus();
			elements[i].blur();
		}
	}

	setTerminationReason() {
		this.mapSites = this.mappingValuesToMap('terminationReason', this.terminationReason, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	setTerminationFeeDiscount() {
		let tempObject;
		this.mapSites = this.mappingValuesToMap('terminationDiscount', this.terminationFeeDiscount, this.mapSites);
		for (let site of this.mapSites.values()) {
			tempObject = { ...site };
			if (tempObject.terminationDate && this.validateTerminationContractDates(tempObject.terminationDate, tempObject.contractStartDate)) {
				tempObject.terminationFeeWithFormat = '€ ' + this.calculateTerminationFeeTotal(tempObject, this.mapFullSites.get(tempObject.siteId));
			} else {
				tempObject.terminationFee = null;
				tempObject.terminationFeeCalculation = null;
			}
			this.mapSites.set(tempObject.id, tempObject);
		}
		this.sites = Array.from(this.mapSites.values());
	}

	setTerminationSubReason() {
		this.mapSites = this.mappingValuesToMap('terminationSubReason', this.terminationSubReason, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	clearTechnicalContact() {
		this.template.querySelector('[data-element="technicalContact"]').value = null;
		this.technicalContactValue = null;
		this.mapSites = this.mappingValuesToMap('technicalContact', null, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	clearWishDate() {
		this.template.querySelector('[data-element="wishDate"]').value = null;
		this.wishDateValue = null;
		this.mapSites = this.mappingValuesToMap('wishDate', null, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	clearterminationDate() {
		this.template.querySelector('[data-element="terminationDate"]').value = null;
		this.terminationDate = null;
		this.mapSites = this.mappingValuesToMap('terminationDate', null, this.mapSites);
		this.mapSites = this.mappingValuesToMap('terminationFee', null, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	clearterminationReason() {
		this.template.querySelector('[data-element="terminationReason"]').value = null;
		this.terminationReason = null;
		this.mapSites = this.mappingValuesToMap('terminationReason', null, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	clearterminationFeeDiscount() {
		this.template.querySelector('[data-element="terminationDiscount"]').value = null;
		this.terminationFeeDiscount = null;
		this.mapSites = this.mappingValuesToMap('terminationDiscount', null, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	clearTerminationSubreason() {
		this.template.querySelector('[data-element="terminationSubReason"]').value = null;
		this.terminationFeeDiscountType = null;
		this.mapSites = this.mappingValuesToMap('terminationSubReason', null, this.mapSites);
		this.sites = Array.from(this.mapSites.values());
	}

	async save() {
		let jsonObject = {};
		let installationSite;
		let datesValidation = true;
		let installationDatesValidation = true;
		this.isLoading = true;
		this.outputValidation = true;
		this.mapSites.forEach((value, key) => {
			if (
				(this.required && value.isAcquisition && (!value.wishDate || !value.technicalContact)) ||
				(this.isMutation &&
					value.isMutation &&
					value.isRequired &&
					(!value.discountApprover ||
						!value.technicalContact ||
						!value.terminationDate ||
						!value.terminationDiscount ||
						!value.terminationReason ||
						!value.terminationSubReason)) ||
				(this.isMove && value.isMove && !value.terminationDate)
			) {
				this.outputValidation = false;
			}
			if (
				(value.isMove || value.isMutation) &&
				datesValidation &&
				!this.validateTerminationContractDates(value.terminationDate, value.contractStartDate)
			) {
				datesValidation = false;
				this.outputValidation = false;
			}
			if (this.required && value.isAcquisition && new Date(value.wishDate) < new Date(value.todayDate)) {
				installationDatesValidation = false;
				this.outputValidation = false;
			}
			if (this.mapFullSites.has(value.siteId)) {
				installationSite = this.mapFullSites.get(value.siteId);
				for (var i = 0; i < installationSite.length; i++) {
					installationSite[i].wishDate = value.wishDate;
					installationSite[i].technicalContact = value.technicalContact;
					installationSite[i].discountApprover = value.discountApprover;
					installationSite[i].terminationDate = value.terminationDate;
					installationSite[i].terminationReason = value.terminationReason;
					installationSite[i].terminationSubReason = value.terminationSubReason;
					installationSite[i].terminationDiscount = value.terminationDiscount;
					installationSite[i].terminationDiscountType = value.terminationDiscountType;
					jsonObject[installationSite[i].id] = installationSite[i];
				}
			}
		});
		if (
			(!this.required && !this.isMutation) ||
			(this.required && this.outputValidation) ||
			(this.isMutation && this.outputValidation && datesValidation && installationDatesValidation)
		) {
			const result = await save({
				recordId: this.recordId,
				installationSitesInput: JSON.stringify(jsonObject)
			});
			if (result.variant && result.message) {
				this.showToast(result.variant, result.message);
			}
		} else if (
			((this.required && !this.outputValidation) || (this.isMutation && !this.outputValidation) || (this.isMove && !this.outputValidation)) &&
			datesValidation &&
			installationDatesValidation
		) {
			this.showToast('error', 'All mandatory Information needs to be populated.');
		} else if (this.isMutation && !datesValidation) {
			this.showToast('error', 'Please check the Termination Wish Dates.');
		} else if (this.isAcquisition && !installationDatesValidation) {
			this.showToast('error', 'Please check the Installation Wish Dates.');
		}
		this.isLoading = false;
	}

	onloadData() {
		let lookupTechnicalContact = this.template.querySelector('[data-element="technicalContact"]');
		if (lookupTechnicalContact !== undefined && !this.onloadRunAlready) {
			lookupTechnicalContact.value = null;
			this.onloadRunAlready = true;
			this.isLoading = false;
		}
	}

	mappingValuesToMap(fieldName, fieldValue, mapObject) {
		let tempObject;
		for (let site of mapObject.values()) {
			tempObject = { ...site };
			tempObject[fieldName] = fieldValue;
			mapObject.set(tempObject.id, tempObject);
		}
		return mapObject;
	}

	showToast(variant, message) {
		const event = new ShowToastEvent({
			variant: variant,
			message: message
		});
		this.dispatchEvent(event);
	}
}
