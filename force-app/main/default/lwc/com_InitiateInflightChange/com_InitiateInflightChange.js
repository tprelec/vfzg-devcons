import { LightningElement, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
import { NavigationMixin } from 'lightning/navigation';

import getData from '@salesforce/apex/Com_InitiateInflightChangeController.getData';
import performInflightChangeForOrder from '@salesforce/apex/Com_InitiateInflightChangeController.performInflightChangeForOrder';
import performInflightChangeForDeliveryOrder from '@salesforce/apex/Com_InitiateInflightChangeController.performInflightChangeForDeliveryOrder';
import DELIVERY_ORDER from '@salesforce/schema/COM_Delivery_Order__c';
import ORDER_OBJECT from '@salesforce/schema/csord__Order__c';
import com_InitiateInflightChange_Confirmation_message from '@salesforce/label/c.Com_InitiateInflightChange_Confirmation_message';
import com_InitiateInflightChange_NoAvailableDeliveryMessage from '@salesforce/label/c.Com_InitiateInflightChange_NoAvailableDeliveryMessage';
import com_InitiateInflightChange_NoUnavailableDeliveryMessage from '@salesforce/label/c.Com_InitiateInflightChange_NoUnavailableDeliveryMessage';

export default class Com_InitiateInflightChange extends NavigationMixin(LightningElement) {
	label = {
		com_InitiateInflightChange_Confirmation_message,
		com_InitiateInflightChange_NoAvailableDeliveryMessage,
		com_InitiateInflightChange_NoUnavailableDeliveryMessage
	};

	@api recordId;
	@api objectApiName;
	isLoading;
	columns = [];
	availableDeliveries;
	unavailableDeliveries;
	selectedRecords = [];

	get isDeliveryObject() {
		return DELIVERY_ORDER.objectApiName === this.objectApiName;
	}

	get isOrderObject() {
		return ORDER_OBJECT.objectApiName === this.objectApiName;
	}

	get hasAvailableDeliveries() {
		return this.availableDeliveries?.length > 0;
	}

	get hasUnavailableDeliveries() {
		return this.unavailableDeliveries?.length > 0;
	}

	get showDeliveryList() {
		return this.hasAvailableDeliveries || this.hasUnavailableDeliveries;
	}

	get activeSections() {
		return ['section1', 'section2'];
	}

	get selectedRecordsCount() {
		return this.selectedRecords?.length;
	}

	get isConfirmDisabled() {
		return this.isLoading || (this.isOrderObject && this.showDeliveryList && this.selectedRecordsCount === 0);
	}

	@wire(getData, { recordId: '$recordId' })
	wiredGetData({ error, data }) {
		this.isLoading = true;
		if (data) {
			console.log('data', JSON.stringify(data));
			const response = data.body;
			if (response) {
				this.availableDeliveries = response.availableDeliveries;
				this.unavailableDeliveries = response.unavailableDeliveries;
				this.columns = response.columns;
			}
			if (data.variant && data.message) {
				this._showToast(data.variant, data.message);
			}
			this.isLoading = false;
		} else if (error) {
			this._showToast('error', error);
			this.isLoading = false;
		}
	}

	handleRowSelection(event) {
		this.selectedRecords = event.detail.selectedRows;
	}

	handleCancel() {
		this.dispatchEvent(new CloseActionScreenEvent());
	}

	async handleConfirm() {
		this.isLoading = true;

		let result;

		if (this.isDeliveryObject) {
			console.log(`Call the handleConfirm action for ${DELIVERY_ORDER.objectApiName}`);
			result = await performInflightChangeForDeliveryOrder({
				deliveryOrderId: this.recordId
			});
		} else if (this.isOrderObject) {
			console.log(`Call the handleConfirm action for ${ORDER_OBJECT.objectApiName}`);
			console.log('this.selectedRecords', JSON.stringify(this.selectedRecords));

			const selectedRecordIds = this.selectedRecords.map((eachRecord) => eachRecord.recordId);

			console.log('selectedRecordIds', JSON.stringify(selectedRecordIds));

			result = await performInflightChangeForOrder({
				orderId: this.recordId,
				selectedDeliveryOrderIds: selectedRecordIds
			});
		} else {
			this._showToast('error', `Object ${this.objectApiName} is not supported, please contact your system administrator.`);
		}

		console.log('result', result);

		if (result.variant && result.message) {
			this._showToast(result.variant, result.message);
		}

		if (result.body) {
			const basketId = result.body;
			console.log('perform navigation', basketId);
			this[NavigationMixin.Navigate]({
				type: 'standard__recordPage',
				attributes: {
					recordId: basketId,
					actionName: 'view'
				}
			});
		}

		this.isLoading = false;
	}

	_showToast(variant, message) {
		const event = new ShowToastEvent({
			variant: variant,
			message: message
		});
		this.dispatchEvent(event);
	}
}
