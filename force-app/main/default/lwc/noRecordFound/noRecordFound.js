import { LightningElement, api } from "lwc";

export default class NoRecordFound extends LightningElement {
	@api message;
}