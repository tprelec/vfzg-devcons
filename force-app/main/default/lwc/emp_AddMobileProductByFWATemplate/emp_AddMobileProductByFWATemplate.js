import { LightningElement, api, track, wire } from 'lwc';
import getFWATempFromAPI from '@salesforce/apex/EMP_AddmobileProductController.getFWATempFromAPI';
import createOLI from '@salesforce/apex/EMP_AddmobileProductController.createOLI';
import getOpportunityLineItems from '@salesforce/apex/EMP_AddmobileProductController.getOpportunityLineItems';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord, getRecordNotifyChange } from 'lightning/uiRecordApi';
import STAGE_FIELD from '@salesforce/schema/Opportunity.StageName';
import JOURNEY_FIELD from '@salesforce/schema/Opportunity.Select_Journey__c';
import BAN_NUMBER from '@salesforce/schema/Opportunity.BAN__r.Name';
import { loadStyle } from 'lightning/platformResourceLoader';
import WrappedHeaderTable from '@salesforce/resourceUrl/WrappedHeaderTable';
//Labels section
import errorNoAddMobileJourney from '@salesforce/label/c.EMP_ErrorNoAddMobile';
import errorCloseWonOpp from '@salesforce/label/c.EMP_ErrorCloseWonOpp';
import disclaimerEmailtoTES from '@salesforce/label/c.EMP_DisclaimerEmailTES';
import refreshCalloutMessage from '@salesforce/label/c.EMP_RefreshCalloutConfirmMessage';

import { publish, subscribe, MessageContext } from 'lightning/messageService';
import JOURNEY_MESSAGE_CHANNEL from '@salesforce/messageChannel/JourneyMessages__c';

export default class Emp_AddMobileProductByFWATemplate extends LightningElement {
	@wire(MessageContext)
	messageContext;
	@track eventMessage;
	@api isJourney = false;
	//for refresh binding
	@track wiredApexData;
	//used for setting timeouts do not initialize.
	timerId;
	//Used on the wired get record
	opp;
	@api recordId;
	//used in the modal pop-up component.
	@track originalMessage;
	message = '';
	/*
		initiate page variables for record count
	*/
	pageSize = 10;
	pageNumber = 1; //always start in 1
	recordFrom = 1; //the list starts index 0 but regarding record view this starts in 1
	recordTo = 0; //initialize in 0 until list retrieval.
	totalPages = 0; //initialize in 0 until list retrieval.

	/*
		for display in RecordPage
	*/
	@track isDialogVisible = false;
	@api isRecordPage = false;
	@api isQuickAction = false;
	showSpinner = false;
	//for wired call
	isError = false;
	//api loaded check
	isCallingApi = true;

	isSearch = false;
	initialLoad = true;

	@track data = [];
	selection = [];
	searchKey;
	allRecords;
	filteredRecords;
	//to inline edit
	draftValues = [];

	errors = {};

	//label object
	label = {
		errorNoAddMobileJourney,
		errorCloseWonOpp,
		disclaimerEmailtoTES,
		refreshCalloutMessage
	};
	//object used to stencil a table
	tableLst = [
		{
			id: 1,
			opacity: 'opacity : 1; border-bottom: 1px solid rgb(211,211,211);'
		},
		{
			id: 2,
			opacity: 'opacity : 0.9; border-bottom: 1px solid rgb(211,211,211,0.9);'
		},
		{
			id: 3,
			opacity: 'opacity : 0.8; border-bottom: 1px solid rgb(211,211,211,0.8);'
		},
		{
			id: 4,
			opacity: 'opacity : 0.7; border-bottom: 1px solid rgb(211,211,211,0.7);'
		},
		{
			id: 5,
			opacity: 'opacity : 0.6; border-bottom: 1px solid rgb(211,211,211,0.6);'
		},
		{
			id: 6,
			opacity: 'opacity : 0.5; border-bottom: 1px solid rgb(211,211,211,0.5);'
		},
		{
			id: 7,
			opacity: 'opacity : 0.4; border-bottom: 1px solid rgb(211,211,211,0.4);'
		},
		{
			id: 8,
			opacity: 'opacity : 0.3; border-bottom: 1px solid rgb(211,211,211,0.3);'
		},
		{
			id: 9,
			opacity: 'opacity : 0.2; border-bottom: 1px solid rgb(211,211,211,0.2);'
		},
		{
			id: 10,
			opacity: 'opacity : 0.1; border-bottom: 1px solid rgb(211,211,211,0.1);'
		}
	];

	columns = [
		{
			label: ' ',
			type: 'button-icon',
			fixedWidth: 40,
			typeAttributes: {
				iconName: { fieldName: 'cssButtonIcon' },
				title: 'Select/De-Select',
				variant: 'container',
				disabled: { fieldName: 'isRowDisabled' },
				alternativeText: 'Select/De-Select',
				class: { fieldName: 'cssButtonIconColor' }
			},
			cellAttributes: {
				class: { fieldName: 'cssRowClass' }
			}
		},

		{
			label: 'Contract Number',
			fieldName: 'contractNumber',
			cellAttributes: {
				class: { fieldName: 'cssRowClass' }
			},
			type: 'text',
			sortable: false,
			wrapText: true
		},
		{
			label: 'Template Id',
			fieldName: 'templateId',
			type: 'text',
			cellAttributes: {
				class: { fieldName: 'cssRowClass' }
			},
			sortable: false,
			wrapText: true
		},
		{
			label: 'Contract Signed Date',
			fieldName: 'contractSignedDate',
			type: 'date',
			typeAttributes: {
				day: 'numeric',
				month: 'short',
				year: 'numeric'
			},
			cellAttributes: {
				class: { fieldName: 'cssRowClass' }
			},
			initialWidth: 120,
			sortable: false,
			wrapText: true
		},
		{
			label: 'Description',
			fieldName: 'templateDescription',
			type: 'text',
			cellAttributes: {
				class: { fieldName: 'cssRowClass' }
			},
			initialWidth: 140,
			sortable: false,
			wrapText: true
		},
		{
			label: 'Base Plan Name',
			fieldName: 'basePlanName',
			type: 'text',
			cellAttributes: {
				class: { fieldName: 'cssRowClass' }
			},
			sortable: false,
			wrapText: true
		},
		{
			label: 'Resource Category',
			fieldName: 'resourceCategory',
			type: 'text',
			cellAttributes: {
				class: { fieldName: 'cssRowClass' }
			},
			sortable: false,
			wrapText: true
		},
		{
			label: 'Quantity Acquisition',
			fieldName: 'quantityAcquisition',
			type: 'number',
			editable: { fieldName: 'isRowEditable' },
			displayReadOnlyIcon: { fieldName: 'isRowDisabled' },
			sortable: false,
			wrapText: true
		},
		{
			label: 'Quantity Porting',
			fieldName: 'quantityPortin',
			type: 'number',
			editable: { fieldName: 'isRowEditable' },
			displayReadOnlyIcon: { fieldName: 'isRowDisabled' },
			sortable: false,
			wrapText: true
		},
		{
			label: 'Quantity Retention',
			fieldName: 'quantityRetention',
			type: 'number',
			editable: { fieldName: 'isRowEditable' },
			displayReadOnlyIcon: { fieldName: 'isRowDisabled' },
			sortable: false,
			wrapText: true
		}
	];

	connectedCallback() {
		this.subscribeToMessageChannel();
	}

	subscribeToMessageChannel() {
		this.subscription = subscribe(this.messageContext, JOURNEY_MESSAGE_CHANNEL, (message) => {
			this.eventMessage = message;
			this.handleMessage(message);
		});
	}

	handleMessage(message) {
		if (message.currentStep === 'Add Mobile Products') {
			this.selectSaveOLI();
		}
	}

	handleOpen() {
		//it can be set dynamically based on your logic
		this.originalMessage = 'Confirmation Dialog to allow the SAM & BP User to choose if wants to re-invoke callout to Unify';
		//shows the component
		this.isDialogVisible = true;
	}

	handleClick(event) {
		if (event.target && event.target.name) {
			if (event.target.name === 'confirmModal') {
				//when user clicks outside of the dialog area, the event is dispatched with detail value  as 1
				if (event.detail !== 1) {
					//you can do some custom logic here based on your scenario
					if (event.detail.status === 'confirm') {
						//do something
						this.dorefresh();
					}
				}
				//hides the component
				this.isDialogVisible = false;
			}
		}
	}

	dorefresh() {
		this.isError = false;
		this.initialLoad = true;
		this.isCallingApi = true;
		this.draftValues = [];
		this.selection = [];
		this.errors = {};
		this.allRecords = [];
		this.filteredRecords = [];
		getRecordNotifyChange([{ recordId: this.recordId }]);
		clearTimeout(this.timerId);
		this.timerId = window.setTimeout(this.wiredRecord.bind(this), 1000, this.wiredApexData);
	}

	renderedCallback() {
		if (!this.stylesLoaded) {
			Promise.all([loadStyle(this, WrappedHeaderTable)])
				.then(() => {
					this.stylesLoaded = true;
				})
				.catch((error) => {
					console.error('Error loading custom styles');
				});
		}
	}

	//	Closed Won
	@wire(getRecord, { recordId: '$recordId', fields: [STAGE_FIELD, JOURNEY_FIELD, BAN_NUMBER] })
	wiredRecord(result) {
		this.wiredApexData = result;
		const disableButton = new CustomEvent('error');
		// Dispatches the event.
		this.dispatchEvent(disableButton);
		if (result.error) {
			if (Array.isArray(result.error.body)) {
				this.message = result.error.body.map((e) => e.message).join(', ');
			} else if (typeof result.error.body.message === 'string') {
				this.message = result.error.body.message;
			}
			this.isError = true;
			this.isCallingApi = false;
		} else if (result.data) {
			this.opp = result.data;
			if (this.opp.fields.Select_Journey__c.value === 'Add Mobile Product') {
				if (this.opp.fields.StageName.value !== 'Closed Won') {
					this.getFWATempWithAPICall();
				} else {
					this.isError = true;
					this.isCallingApi = false;
					this.message = this.label.errorCloseWonOpp;
				}
			} else {
				this.message = this.label.errorNoAddMobileJourney;
				this.isError = true;
				this.isCallingApi = false;
			}
		}
	}

	startSearchTimer(evt) {
		this.searchKey = evt.target.value;
		clearTimeout(this.timerId);
		this.timerId = setTimeout(this.handleSearchText.bind(this), 200);
	}

	handleSearchText(evt) {
		this.isSearch = true;
		if (this.searchKey) {
			let filteredRecords = [];
			// eslint-disable-next-line guard-for-in
			for (let index in this.allRecords) {
				let record = this.allRecords[index];
				let foundKey = false;
				// eslint-disable-next-line guard-for-in
				for (let key in record) {
					let value = record[key].toString();
					if (key === 'templateDescription' || key === 'contractNumber' || key === 'basePlanName' || key === 'templateId') {
						if (value && value.length > 0 && value.toLowerCase().includes(this.searchKey.toLowerCase())) {
							foundKey = true;
						}
					}
				}
				if (foundKey) {
					filteredRecords.push(record);
				}
			}
			this.filteredRecords = filteredRecords;
		} else {
			this.filteredRecords = this.allRecords;
		}
		this.filterRecords(0);
	}

	previousEve() {
		this.pageNumber = this.pageNumber - 1;
		let _startingIndexToFilter = Number(this.pageSize) * this.pageNumber - Number(this.pageSize);
		this.filterRecords(_startingIndexToFilter);
	}

	nextEve() {
		let _startingIndexToFilter = Number(this.pageSize) * this.pageNumber;
		this.pageNumber = this.pageNumber + 1;
		this.filterRecords(_startingIndexToFilter);
	}

	calculatePaginationVariables(_indexToSetPagination) {
		let _lengthOfRecordsFiltered = this.filteredRecords.length;
		let _lengthOfRecordsDisplayed = this.data.length;
		/*
			every time the _indexToSetPagination is "0" "Zero" 
			means the table shows data in the First Page
			and the page number is One 
		*/
		if (_indexToSetPagination === 0) {
			this.pageNumber = 1;
			this.recordFrom = 1;
		} else {
			this.recordFrom = _indexToSetPagination + 1;
		}
		//the RecordTo variable is calculated based on index where counting should start and the length of records the user can see
		this.recordTo = _indexToSetPagination + _lengthOfRecordsDisplayed;
		//calculate Total Pages based on the length of the "filteredRecords" array
		this.totalPages = Math.ceil(_lengthOfRecordsFiltered / Number(this.pageSize));
	}

	filterRecords(_startingIndex) {
		//first calculate endIndex
		let _endIndex = _startingIndex + Number(this.pageSize);

		//the "data" is actually only the portion of the array the user can see.
		this.data = this.filteredRecords.slice(_startingIndex, _endIndex);

		//following SoC pattern
		this.calculatePaginationVariables(_startingIndex);

		this.updateSelection();
	}

	get recordCount() {
		return `Page ${this.pageNumber} | Showing records from ${this.recordFrom} to ${this.recordTo}`;
	}

	get disPre() {
		//preventing action on button "previous" less than 1
		return this.pageNumber <= 1 ? true : false;
	}

	get disNext() {
		return this.totalPages === this.pageNumber ? true : false;
	}

	getFWATempWithAPICall() {
		var vfAssetData;
		getFWATempFromAPI({
			strBANNumber: this.opp.fields.BAN__r.displayValue,
			oppId: this.recordId
		})
			.then((result) => {
				vfAssetData = JSON.parse(JSON.stringify(result));
				return this.setInitialData(vfAssetData);
			})
			.then(() => {
				this.isCallingApi = false;
				this.allRecords = vfAssetData;
				this.filteredRecords = vfAssetData;
				//0 meaning return all records
				this.filterRecords(0);
				this.initialLoad = false;
				const disableButton = new CustomEvent('error');
				// enable the button
				this.dispatchEvent(disableButton);
			})
			.catch((error) => {
				if (Array.isArray(error.body)) {
					this.message = error.body.map((e) => e.message).join(', ');
				} else if (typeof error.body.message === 'string') {
					this.message = error.body.message;
				}
				this.isError = true;
				this.isCallingApi = false;
			});
	}

	//function to save close and show toast Start
	@api
	close() {
		const closeQA = new CustomEvent('close');
		// Dispatches the event.
		this.dispatchEvent(closeQA);
	}

	validateInputsRow(_rowToValidate) {
		let _hasError = false;
		let _errorRowMessage = {};

		let _entries = Object.entries(_rowToValidate);

		let _arrayQuantities = _entries.filter((item) => {
			if (item[0] === 'quantityAcquisition' || item[0] === 'quantityPortin' || item[0] === 'quantityRetention') {
				return item;
			}
		});
		//check first if array contains values otherwise row is invalid
		if (Array.isArray(_arrayQuantities) && !_arrayQuantities.length) {
			_hasError = true;
			_errorRowMessage = {
				title: 'We found 1 error.',
				messages: ['Please Select One Quantity'],
				fieldNames: ['quantityAcquisition', 'quantityPortin', 'quantityRetention']
			};
		}

		let _arrQuantitiesWithError = _arrayQuantities
			.filter(([key, value]) => {
				if (value !== undefined && value) {
					const _valueToNumber = Number(value);
					if (!Number.isInteger(_valueToNumber) || _valueToNumber < 1) {
						return key;
					}
				}
			})
			.map(([key]) => {
				return key;
			});
		if (Array.isArray(_arrQuantitiesWithError) && _arrQuantitiesWithError.length) {
			_hasError = true;
			_errorRowMessage = {
				title: `We found ${_arrQuantitiesWithError.length} ${_arrQuantitiesWithError.length > 1 ? `errors` : `error`}`,
				messages: ['Please amend the highlighted values and try again'],
				fieldNames: [..._arrQuantitiesWithError]
			};
		}
		return { _hasError, _errorRowMessage };
	}

	@api
	selectSaveOLI() {
		let _rowsWithError = {};
		let _mapAllSelection = new Map();
		// Assign to map
		let setSelected = new Set();
		this.selection.forEach((keySel) => {
			setSelected.add(keySel);
		});
		this.allRecords.forEach((record) => {
			if (setSelected.has(record.keyId)) {
				_mapAllSelection.set(record.keyId, record);
			}
		});
		let draft = this.template.querySelector('[data-id="datarow"]').draftValues;
		draft.forEach((element) => {
			if (_mapAllSelection.has(element.keyId)) {
				let val = _mapAllSelection.get(element.keyId);
				val = {
					...val,
					quantityAcquisition: 'quantityAcquisition' in element ? element.quantityAcquisition : val.quantityAcquisition,
					quantityPortin: 'quantityPortin' in element ? element.quantityPortin : val.quantityPortin,
					quantityRetention: 'quantityRetention' in element ? element.quantityRetention : val.quantityRetention
				};
				_mapAllSelection.set(val.keyId, val);
			}
		});
		let isAllValid = true;
		if (_mapAllSelection && _mapAllSelection.size > 0) {
			let lst = [];
			for (let mapSel of _mapAllSelection.values()) {
				//Based on Separation of Concern
				const { _hasError, _errorRowMessage } = this.validateInputsRow(mapSel);

				if (!_hasError) {
					if (mapSel.quantityAcquisition) {
						let acq = {
							fwaId: mapSel.fwaId,
							templateId: mapSel.templateId,
							quantity: mapSel.quantityAcquisition,
							type: 'New',
							baseplan: mapSel.basePlanName,
							connectionType: mapSel.connectionType,
							resourceCat: mapSel.resourceCategory
						};
						lst.push(acq);
					}
					if (mapSel.quantityPortin) {
						let port = {
							fwaId: mapSel.fwaId,
							templateId: mapSel.templateId,
							quantity: mapSel.quantityPortin,
							type: 'Portin',
							baseplan: mapSel.basePlanName,
							connectionType: mapSel.connectionType,
							resourceCat: mapSel.resourceCategory
						};
						lst.push(port);
					}
					if (mapSel.quantityRetention) {
						let ret = {
							fwaId: mapSel.fwaId,
							templateId: mapSel.templateId,
							quantity: mapSel.quantityRetention,
							type: 'Retention',
							baseplan: mapSel.basePlanName,
							connectionType: mapSel.connectionType,
							resourceCat: mapSel.resourceCategory
						};
						lst.push(ret);
					}
				} else {
					isAllValid = false;
					//break;
					_rowsWithError[mapSel.keyId] = _errorRowMessage;
				}
			}
			if (isAllValid) {
				this.showSpinner = true && !this.isJourney;
				this.errors = {};
				createOLI({
					strJSONToDeserialize: JSON.stringify(lst),
					oppId: this.recordId
				})
					.then((result) => {
						this.showSpinner = false;
						this.showToast('Success', 'Opportunity line Items created');
						publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, {
							nextStep: this.eventMessage.nextStep,
							message: 'SUCCESS'
						});
					})
					.catch((error) => {
						this.showSpinner = false;
						this.showToast('Error', error.body.message);
						publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
					});
			} else {
				let _errors = { rows: {}, table: {} };
				_errors.rows = _rowsWithError;
				_errors.table = {
					title: 'We found 1 error(s)',
					messages: ['Please amend the errors!']
				};
				this.errors = _errors;
				this.template.querySelector('[data-id="datarow"]').errors = this.errors;
				this.showToast('Warning', 'One or more rows has Errors, Please amend the errors!');
				publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
			}
		} else {
			this.showToast('Warning', 'Please select at least one Asset');
			publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
		}
	}

	showToast(variant, msg) {
		const event = new ShowToastEvent({
			title: variant,
			variant: variant,
			message: msg
		});
		this.dispatchEvent(event);
	}

	get disableActions() {
		return this.isError || this.isCallingApi;
	}

	get variantButton() {
		return this.isRecordPage ? 'Neutral' : 'destructive-text';
	}

	handleRowAction(event) {
		let rowFromEvent = event.detail.row;
		//when the "rowFromEvent" IS SELECTED then the selectedRows NOT CONTAINS the id of the "rowFromEvent"
		//when the "rowFromEvent" IS DE-SELECTED then the selectedRows CONTAINS the id of the "rowFromEvent"
		//the "rowFromEvent" from the event +/- selectedRows target = the ROWS in the view PAGE NUMBER of the User
		let _isEvtRowAlreadySelected = event.target.selectedRows.some((evtRowId) => evtRowId === rowFromEvent.keyId);
		//when _isEvtRowAlreadySelected is true means it needs to be remove from "this . Selection" otherwise add It
		let _allSelectedRows = this.selection;

		if (_isEvtRowAlreadySelected) {
			//remove
			_allSelectedRows = _allSelectedRows.filter((item) => item !== rowFromEvent.keyId);
		} else {
			//add
			_allSelectedRows.push(rowFromEvent.keyId);
		}
		this.selection = _allSelectedRows;

		this.allRecords = this.allRecords.map((item) => {
			if (item.keyId == rowFromEvent.keyId) {
				item.isSelected = !_isEvtRowAlreadySelected;
				item.cssButtonIcon = !_isEvtRowAlreadySelected ? 'utility:check' : 'utility:add';
				item.cssButtonIconColor = !_isEvtRowAlreadySelected ? 'to-unselect' : 'to-select';
			}
			return item;
		});
		this.filteredRecords = this.filteredRecords.map((item) => {
			if (item.keyId == rowFromEvent.keyId) {
				item.isSelected = !_isEvtRowAlreadySelected;
				item.cssButtonIcon = !_isEvtRowAlreadySelected ? 'utility:check' : 'utility:add';
				item.cssButtonIconColor = !_isEvtRowAlreadySelected ? 'to-unselect' : 'to-select';
			}
			return item;
		});

		//get page number to know which dataset is displaying
		let _startIndexToUpdate = this.pageNumber === 1 ? 0 : Number(this.pageSize) * (this.pageNumber - 1);
		this.filterRecords(_startIndexToUpdate);
	}

	updateSelection() {
		//meaning no need to preSelectRows as per on initial load as the table doesn't exist.
		if (!this.initialLoad) {
			this.template.querySelector('[data-id="datarow"]').selectedRows = this.selection;
		}
	}

	async setInitialData(startingData) {
		return getOpportunityLineItems({
			opportunityId: this.recordId
		})
			.then((opportunityLineItems) => {
				if (opportunityLineItems.length > 0) {
					for (const opportunityLineItem of opportunityLineItems) {
						let rowId = opportunityLineItem.Framework_Agreement_Id__c + '-' + opportunityLineItem.Template_Id__c;
						if (!this.selection.includes(rowId)) {
							this.selection.push(rowId);
						}
						let currentData = this.findDataByKeyId(startingData, rowId);
						this.selectRowAndSetQuantity(currentData, opportunityLineItem.Quantity, opportunityLineItem.Deal_Type__c);
					}
				}
			})
			.catch((error) => {
				console.log('Problem fetching opportunity line items.');
			});
	}

	selectRowAndSetQuantity(dataRow, quantity, dealType) {
		dataRow.isSelected = true;
		dataRow.cssButtonIconColor = 'to-unselect';
		dataRow.cssButtonIcon = 'utility:check';
		if (quantity && quantity > 0) {
			if (dealType === 'New') {
				dataRow.quantityAcquisition = quantity;
			} else if (dealType === 'Porting') {
				dataRow.quantityPortin = quantity;
			} else if (dealType === 'Retention') {
				dataRow.quantityRetention = quantity;
			}
		}
	}

	findDataByKeyId(dataSet, keyId) {
		for (const currentData of dataSet) {
			if (currentData.keyId === keyId) {
				return currentData;
			}
		}
		return undefined;
	}
}
