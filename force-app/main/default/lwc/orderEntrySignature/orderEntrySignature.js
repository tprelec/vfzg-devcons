import { LightningElement, api, track } from 'lwc';
import { loadScript } from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import signature_pad from '@salesforce/resourceUrl/signature_pad';
import pdfLib from '@salesforce/resourceUrl/pdfLib';
import getContent from '@salesforce/apex/PdfViewerController.getContent';
import updateQuoteAttachment from '@salesforce/apex/OrderEntryQuoteController.updateQuoteAttachment';

export default class OrderEntryCaptureSignature extends LightningElement {
	_signature_padInitialized = false;
	@track signaturePad;
	@api fileId;
	@api nameOfSignatory;
	@track file;

	// import signature_pad and pdf-lib
	renderedCallback() {
		if (this._signature_padInitialized) {
			return;
		}
		this._signature_padInitialized = true;
		Promise.all([
			loadScript(this, signature_pad + '/signature_pad.js'),
			loadScript(this, pdfLib + '/pdf-lib/pdf-lib.js'),
			loadScript(this, pdfLib + '/pdf-lib/fontkit.js')
		])
			.then(() => {
				this.initSignaturePad();
			})
			.catch((error) => {
				this.handleError(error);
			});
	}

	// init signature pad and set width and height
	initSignaturePad() {
		let canvas = this.template.querySelector('.signature-pad');
		this.signaturePad = new SignaturePad(canvas, {
			backgroundColor: 'rgba(255, 255, 255, 0)',
			penColor: 'rgb(0, 0, 0)'
		});
		this.signaturePad.dotSize = 0;
		this.signaturePad.minWidth = 10;
		canvas.style.width = '100%';
		canvas.style.height = '100%';
		canvas.width = canvas.offsetWidth;
		canvas.height = canvas.offsetHeight;
	}

	handleError(error) {
		this.loading = false;
		this.dispatchEvent(
			new ShowToastEvent({
				title: 'Something went wrong',
				message: error.body ? error.body.message : error.message,
				variant: 'warning'
			})
		);
	}

	// cleare signature
	@api
	handleClearSignatureTest() {
		this.signaturePad.clear();
	}

	// get content from attachment
	getPDFBody() {
		return getContent({ fileIds: this.fileId })
			.then((result) => {
				if (result) {
					this.file = result;
					this.file[0].data = atob(this.file[0].data);
					this.addSignature();
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	// aadd signature to pdf
	@api handleConfirmSignature() {
		const canvas = this.template.querySelector('.signature-pad');

		if (!this.isCanvasBlank(canvas) && this.nameOfSignatory) {
			this.getPDFBody();
		} else {
			this.loading = false;
			this.dispatchEvent(
				new CustomEvent('confirmsignature', {
					detail: null,
					bubbles: false
				})
			);
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Signature is empty',
					message: 'The signature and Signed by must be filled!',
					variant: 'warning'
				})
			);
		}
	}

	// checking that the signature has been made
	isCanvasBlank(canvas) {
		let blank = this.template.querySelector('.blank');
		blank.width = canvas.width;
		blank.height = canvas.height;
		return canvas.toDataURL() === blank.toDataURL();
	}

	// add signature image on last page in pdf
	async addSignature() {
		let pdfDoc = await PDFLib.PDFDocument.load(this.base64ToUint8Array(this.file[0].data));
		const pages = pdfDoc.getPages();
		let lastPage = pages[pages.length - 1];
		let { width, height } = lastPage.getSize();
		const pngImage = await pdfDoc.embedPng(this.signaturePad.toDataURL('image/png'));
		const pngDims = pngImage.scale(0.5);
		lastPage.drawImage(pngImage, {
			x: width / 2 - pngDims.width / 6,
			y: height / 2 - pngDims.height < 330 ? height / 2 - pngDims.height - 250 : height / 2 - pngDims.height - 350,
			width: 250,
			height: 75
		});

		pdfDoc.registerFontkit(fontkit);
		let endPoint = pdfLib + '/pdf-lib/verdana.ttf';
		const fontBytes = await fetch(endPoint, {
			method: 'GET'
		}).then((res) => res.arrayBuffer());

		const customFont = await pdfDoc.embedFont(fontBytes);
		lastPage.drawText(
			this.nameOfSignatory +
				' ' +
				new Date().toLocaleDateString('en-GB') +
				' ' +
				new Date().toLocaleTimeString([], {
					hour: '2-digit',
					minute: '2-digit'
				}),
			{
				x: width / 2 - pngDims.width / 6 + 50,
				y: height / 2 - pngDims.height < 330 ? height / 2 - pngDims.height - 150 : height / 2 - pngDims.height - 250,
				size: 10,
				color: PDFLib.rgb(0.158, 0.158, 0.158),
				font: customFont
			}
		);

		const base64PDF = await pdfDoc.saveAsBase64();

		updateQuoteAttachment({
			fileId: this.fileId,
			attachmentBodyBlob: base64PDF
		})
			.then((res) => {
				this.dispatchEvent(
					new CustomEvent('confirmsignature', {
						detail: this.fileId,
						bubbles: false
					})
				);
			})
			.catch(() => {
				this.handleError({ message: 'Something went wrong' });
			});
	}

	base64ToUint8Array(base64) {
		const binary_string = base64;
		const len = binary_string.length;
		let bytes = new Uint8Array(len);
		for (let i = 0; i < len; i++) {
			bytes[i] = binary_string.charCodeAt(i);
		}
		return bytes.buffer;
	}

	handleError(error) {
		this.loading = false;
		this.isFilePreviewed = false;
		this.dispatchEvent(
			new ShowToastEvent({
				title: 'Something went wrong',
				message: error.body ? error.body.message : error.message,
				variant: 'warning'
			})
		);
	}
}
