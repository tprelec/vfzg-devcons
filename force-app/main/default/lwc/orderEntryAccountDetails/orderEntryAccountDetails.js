import { LightningElement, api, track, wire } from 'lwc';
import getAccountData from '@salesforce/apex/OrderEntryAccountController.getAccountData';
import saveAccountData from '@salesforce/apex/OrderEntryAccountController.saveAccountData';
import getBundlePromotions from '@salesforce/apex/OrderEntryPromotionsController.getBundlePromotions';
import getSpecialPromotions from '@salesforce/apex/OrderEntryPromotionsController.getSpecialPromotions';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';
import { publish, MessageContext } from 'lightning/messageService';
import { getConstants } from 'c/orderEntryConstants';
import { refreshApex } from '@salesforce/apex';
const CONSTANTS = getConstants();

export default class OrderEntryAccountDetails extends LightningElement {
	@wire(MessageContext)
	messageContext;
	@api accountId;
	@api journeyType;
	@api opportunityId;
	@track bundlePromotions = [];
	@track specialPromotions = [];
	@track specialPromotionsExist = false;
	@track _tempoBundle;
	@track _indexOfBundle = -1;
	@track _b2cInternetCustomer;
	@api site;
	@track _bundles;
	@api
	get bundles() {
		return this._bundles;
	}
	set bundles(value) {
		this._bundles = Object.assign([], value);
	}
	@track account = {
		Id: '',
		Name: '',
		KVK_number__c: '',
		Date_of_Establishment__c: '',
		LG_LegalForm__c: '',
		VAT_Number__c: ''
	};
	@api
	get b2cInternetCustomer() {
		return this._b2cInternetCustomer;
	}
	set b2cInternetCustomer(value) {
		this._b2cInternetCustomer = value;
	}

	get disabledB2cInternetCustomer() {
		return this.site?.addressCheckResult === 'Off-Net';
	}

	get b2cChecked() {
		return this.site?.addressCheckResult === 'Off-Net' ? false : this.b2cInternetCustomer;
	}

	get mobileJourney() {
		return this.journeyType === 'Mobile';
	}

	get fixedBundle() {
		this._fixedBundle = Object.assign(
			{},
			this.bundles?.find((bundle) => bundle.type === 'Fixed')
		);
		return this.bundles?.find((bundle) => bundle.type === 'Fixed');
	}

	get options() {
		return [
			{ label: '--None--', value: '' },
			{ label: 'B.V.', value: 'B.V.' },
			{ label: 'Buitenlandse Onderneming', value: 'Buitenlandse Onderneming' },
			{ label: 'C.V.', value: 'C.V.' },
			{
				label: 'Co-operatieve vennootschap',
				value: 'Co-operatieve vennootschap'
			},
			{ label: 'Eenmanszaak', value: 'Eenmanszaak' },
			{ label: 'Maatschap', value: 'Maatschap' },
			{ label: 'N.V.', value: 'N.V.' },
			{ label: 'Overheid', value: 'Overheid' },
			{ label: 'Private Limited Company (LTD)', value: 'Private Limited Company (LTD)' },
			{ label: 'Stichting', value: 'Stichting' },
			{ label: 'V.O.F.', value: 'V.O.F.' },
			{ label: 'Vereniging', value: 'Vereniging' },
			{ label: 'Anders', value: 'Anders' }
		];
	}

	get todaysDate() {
		let today = new Date();

		today.toString();

		return new Date().toISOString();
	}

	connectedCallback() {
		refreshApex();
		this.getAccountData();
	}

	/**
	 * description get account data
	 */
	getAccountData() {
		getAccountData({ accId: this.accountId })
			.then((result) => {
				this.account = {
					Id: result.Id,
					Name: result.Name,
					KVK_number__c: result.KVK_number__c,
					Date_of_Establishment__c: result.Date_of_Establishment__c,
					LG_LegalForm__c: result.LG_LegalForm__c,
					VAT_Number__c: result.VAT_Number__c
				};
			})
			.catch((error) => {
				this.handleError(error);
			});
	}
	/**
	 * description change account property
	 * @param {*} event
	 */
	handleFormInputChange(event) {
		this.account[event.target.name] = event.target.value;
	}

	/**
	 * description checking input data
	 * @returns  boolean
	 */
	@api validate() {
		let isValid = true;
		this.template.querySelectorAll('lightning-input').forEach((element) => {
			element.reportValidity();
			isValid = isValid && element.checkValidity();
		});

		this.template.querySelectorAll('lightning-combobox').forEach((element) => {
			element.reportValidity();
			isValid = isValid && element.checkValidity();
		});

		if (isValid) {
			return true;
		}

		return false;
	}

	/**
	 * description update account data
	 * @returns boolean
	 */
	@api async saveAccountData() {
		return await saveAccountData({ newAccount: this.account })
			.then((result, error) => {
				if (error) {
					return false;
				} else if (result) {
					return true;
				}
			})
			.catch((error) => {
				console.log('Something went wrong!');
				return false;
			});
	}

	/**
	 * description change B2C flag and update avaible promotions
	 * @param {*} e
	 */
	async handleB2cChange(e) {
		this._b2cInternetCustomer = e.detail.checked;
		if (this.bundles.length > 0) {
			await this.bundles.forEach(async (el) => {
				this._indexOfBundle++;
				this._tempoBundle = el;
				this.bundlePromotions = await this.getBundlePromotions(el);
				this.specialPromotions = await this.getSpecialPromotions(el);

				await this.removeUnavailablePromotions(this.bundlePromotions, this.specialPromotions);
				await this.filterAvailablePromotions(this.specialPromotions, CONSTANTS.SPECIAL_PROMOTION_TYPE);
				await this.updateSpecialPromotionsExist();

				this._bundles[this._indexOfBundle] = this._tempoBundle;
			});
			await this.sleep(0);
			this.publishProductChanged('promotions-changed', { bundles: this.bundles });
			this.publishProductChanged('b2cInternetCustomer-changed', {
				b2cInternetCustomer: this.b2cInternetCustomer
			});
		} else {
			this.publishProductChanged('b2cInternetCustomer-changed', {
				b2cInternetCustomer: this.b2cInternetCustomer
			});
		}
	}

	/**
	 *  descriotion get bundle promotinos
	 * @param {*} bundle
	 * @returns promotions lisl
	 */
	async getBundlePromotions(bundle) {
		// eslint-disable-next-line no-return-await
		return await getBundlePromotions({
			opportunityId: this.opportunityId,
			b2cChecked: this.b2cInternetCustomer,
			products: bundle?.products,
			addons: bundle?.addons,
			bundle: bundle,
			site: this.site
		});
	}

	/**
	 *  descriotion get specioal promotinos
	 * @param {*} bundle
	 * @returns promotions lisl
	 */
	async getSpecialPromotions(bundle) {
		// eslint-disable-next-line no-return-await
		return await getSpecialPromotions({
			opportunityId: this.opportunityId,
			b2cChecked: this.b2cInternetCustomer,
			products: bundle?.products,
			addons: bundle?.addons,
			bundle: bundle,
			site: this.site
		});
	}

	/**
	 * description remove promotions
	 * @param {*} bundlePromotions
	 * @param {*} specialPromotions
	 */
	removeUnavailablePromotions(bundlePromotions, specialPromotions) {
		if (this._tempoBundle?.promos && this._tempoBundle?.promos.length > 0) {
			const promotionsForDelete = [];
			this._tempoBundle?.promos.forEach((p) => {
				if (p.type !== CONSTANTS.AUTOMATIC_PROMOTION_TYPE) {
					let promoFound = -1;
					promoFound =
						p.type === CONSTANTS.STANDARD_PROMOTION_TYPE
							? bundlePromotions.findIndex((bp) => bp.Id === p.id)
							: specialPromotions.findIndex((sp) => sp.Id === p.id);

					if (promoFound === -1) {
						promotionsForDelete.push(p);
					}
				}
			});
			let promos = Object.assign([], this._tempoBundle.promos);
			if (promotionsForDelete.length > 0) {
				promotionsForDelete.forEach((p) => {
					promos.splice(promos.indexOf(p), 1);
				});
				this._tempoBundle.promos = promos;
			}
		}
	}

	/**
	 * description filter promotion
	 * @param {*} promotions
	 * @param {*} type
	 */
	filterAvailablePromotions(promotions, type) {
		let selectedProdsAddons = [];

		if (this._tempoBundle.promos) {
			this._tempoBundle.promos.forEach((p) => {
				p.discounts.forEach((d) => {
					if (p.type === type) {
						if (d.product) {
							selectedProdsAddons.push(d.product);
						} else if (d.addon) {
							selectedProdsAddons.push(d.addon);
						}
					}
				});
			});
		}

		if (promotions) {
			let promos = Object.assign([], promotions);
			promos.forEach((p, index) => {
				p.Discounts__r.forEach((d) => {
					if (d.Discount__r) {
						if (
							(d.Discount__r.Product__c && selectedProdsAddons.includes(d.Discount__r.Product__c)) ||
							(d.Discount__r.Add_On__c && selectedProdsAddons.includes(d.Discount__r.Add_On__c))
						) {
							promos.splice(index, 1);
						}
					}
				});
			});
			promotions = promos;
		}
	}

	/**
	 * change promotion in local list
	 */
	updateSpecialPromotionsExist() {
		let selectedPromotion = false;
		if (this._tempoBundle.promos) {
			this._tempoBundle.promos.forEach((prom) => {
				if (prom.type === CONSTANTS.SPECIAL_PROMOTION_TYPE) {
					selectedPromotion = true;
				}
			});
		}

		this.specialPromotionsExist =
			(this.specialPromotions && this.specialPromotions.length) ||
			(!(this.specialPromotions && this.specialPromotions.length) && selectedPromotion);
	}

	/**
	 * publish message
	 * @param {*} event
	 * @param {*} data
	 */
	publishProductChanged(event, data) {
		const msg = {
			sourceComponent: 'order-entry-account-details',
			payload: {
				event,
				data
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}
	async sleep(ms) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}
}
