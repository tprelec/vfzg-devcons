import { api, LightningElement, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import WORKTYPEGROUP_NAME from '@salesforce/schema/WorkTypeGroup.Name';
import getServiceAppointmentLabel from '@salesforce/apex/SchedulerReviewController.getServiceAppointmentLabel';
import GENERAL_INFO_HEADER from '@salesforce/label/c.General_Information_Header';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import SERVICE_APPOINTMENT from '@salesforce/schema/ServiceAppointment';

export default class ServiceApptGeneralInfoReview extends LightningElement {
	get label() {
		return {
			sectionHeader: GENERAL_INFO_HEADER,
			workType: this.workTypeLabel,
			address: this.addressLabel,
			description: this.descriptionLabel,
			numberOfEmployees: this.noOfEmployeesLabel,
			apptType: this.appointmentTypeLabel
		};
	}

	@wire(getObjectInfo, { objectApiName: SERVICE_APPOINTMENT })
	buildLabels({ data, error }) {
		if (data) {
			this.workTypeLabel = data.fields.WorkTypeId.label.replace('ID', '').trim();
			this.appointmentTypeLabel = data.fields.AppointmentType.label;
			this.noOfEmployeesLabel = data.fields.Number_of_employees__c.label;
			this.addressLabel = data.fields.Address.label;
			this.descriptionLabel = data.fields.Description.label;
		} else if (error) {
			console.error(error);
		}
	}

	workTypeLabel;
	appointmentTypeLabel;
	noOfEmployeesLabel;
	addressLabel;
	descriptionLabel;

	@api
	address;

	@api
	workTypeGroupId;

	@api
	appointmentType;

	@api
	additionalFields;

	_additionalFields = {
		Description: '',
		Number_of_employees__c: ''
	};
	description;
	numberOfEmployees;

	@wire(getRecord, { recordId: '$workTypeGroupId', fields: [WORKTYPEGROUP_NAME] })
	workTypeGroup;

	@wire(getServiceAppointmentLabel, { appointmentTypeValue: '$appointmentType' })
	serviceAppointmentLabel;

	get workTypeGroupName() {
		return getFieldValue(this.workTypeGroup.data, WORKTYPEGROUP_NAME);
	}

	connectedCallback() {
		this.dispatchEvent(new CustomEvent('loadcomplete'));
	}

	handleChange(event) {
		try {
			this._additionalFields[event.target.name] = event.target.value;
			this.dispatchEvent(new CustomEvent('infoupdated', { detail: this._additionalFields }));
		} catch (error) {
			console.error('something went wrong on: serviceApptGeneralInfoReview. %O', error);
		}
	}
}
