import { LightningElement, api } from 'lwc';

export default class OrderEntryAutomaticPromotions extends LightningElement {
	@api promotions;

	get showComponent() {
		return this.promotions?.length > 0;
	}
}
