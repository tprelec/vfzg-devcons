import { LightningElement, track, api, wire } from "lwc";
import { NavigationMixin } from "lightning/navigation";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getRecords from "@salesforce/apex/LookupController.getRecords";
import getRecord from "@salesforce/apex/LookupController.getRecord";
import { getObjectInfo } from "lightning/uiObjectInfoApi";
import { getRecordNotifyChange } from "lightning/uiRecordApi";

/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 300;

export default class Lookup extends NavigationMixin(LightningElement) {
    // attributes
    @api objectName;
    @api primaryField = "Name";
    @api secondaryField;
    @api lookupLabel;
    @api lookupPlaceholder;
    @api disabled = false;
    @api filter;
    @api required = false;
    isSearching = false;

    fields;

    @api createNewFields;

    showModal = false;

    objectLabel = "Record";

    _defaultRecordId;
    @api
    get defaultRecordId() {
        return this._defaultRecordId;
    }
    set defaultRecordId(value) {
        this._defaultRecordId = value;
        this.selectedRecordId = value;
    }

    _defaultRecordName;
    @api
    get defaultRecordName() {
        return this._defaultRecordName;
    }
    set defaultRecordName(value) {
        this._defaultRecordName = value;
        this.searchKey = value;
    }

    @api setDefaultId(recordId, recordName) {
        this.notifyChangeToParent(recordId || null);
        this.isSearching = false;
        this.records = [];
        this.searchKey = recordName || "";
        this.showError("");
    }

    // reactive private properties
    @track searchKey = "";
    @track records;
    @track error;
    @track selectedRecordId;
    placeholderLabel = "Search";
    themeInfo;

    isCreateNewFormLoading = false;

    showNoRecordsAvailableMessage = false;

    get comboBoxClass() {
        return `slds-combobox slds-dropdown-trigger slds-dropdown-trigger_click ${
            this.records?.length > 0 || this.showNoRecordsAvailableMessage
                ? "slds-is-open"
                : ""
        }`;
    }

    get iconColor() {
        return `background-color: #${this.themeInfo?.color};`;
    }

    @wire(getObjectInfo, { objectApiName: "$objectName" })
    handleResult({ error, data }) {
        if (data) {
            let objectInformation = data;
            if (this.lookupPlaceholder) {
                this.placeholderLabel = this.lookupPlaceholder;
            } else {
                this.placeholderLabel +=
                    " " +
                    (objectInformation && objectInformation.labelPlural
                        ? objectInformation.labelPlural
                        : "");
            }
            this.objectLabel = objectInformation.label || this.lookupLabel;
            this.themeInfo = objectInformation.themeInfo || {};
        }
        if (error) {
            this.showError(
                "You do not have the rights to object or object api name is invalid: " +
                    this.objectName
            );
            // eslint-disable-next-line @lwc/lwc/no-api-reassignments
            this.disabled = true;
        }
    }

    onResultClick(event) {
        this.notifyChangeToParent(event.currentTarget.dataset.recordId);
        this.searchKey = event.currentTarget.dataset.recordName;
        this.records = [];
        this.showNoRecordsAvailableMessage = false;
    }

    handleRemove() {
        this.searchKey = "";
        this.notifyChangeToParent("");

        // eslint-disable-next-line @lwc/lwc/no-async-operation
        setTimeout(() => {
            const searchInput = this.template.querySelector(".searchInput");
            searchInput.focus();
        }, 10);
    }

    handleKeyChange(event) {
        this.isSearching = true;
        this.showNoRecordsAvailableMessage = false;
        this.selectedRecordId = "";
        this.records = [];
        this.showError("");

        // Debouncing this method: Do not update the reactive property as long as this function is
        // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
        window.clearTimeout(this.delayTimeout);
        const searchKey = event.target.value;
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        this.delayTimeout = setTimeout(() => {
            this.searchKey = searchKey;
            if (searchKey) {
                this.queryRecords();
            } else {
                this.isSearching = false;
            }
        }, DELAY);
    }

    async queryRecords() {
        const lookupData = {
            data: {
                searchKey: this.searchKey,
                objectApiName: this.objectName,
                primaryField: this.primaryField,
                secondaryField: this.secondaryField,
                filter: this.filter
            }
        };
        const result = await getRecords(lookupData);

        this.records = result?.body || [];

        this.showNoRecordsAvailableMessage = this.records?.length === 0;
        if (result.variant === "error") {
            this.showError("Something went wrong, please contact admin!");
            this.showNoRecordsAvailableMessage = false;
        }

        this.isSearching = false;
    }

    handleBlur() {
        // timeout is added to avoid showing error when user selects a result
        // eslint-disable-next-line @lwc/lwc/no-async-operation
        setTimeout(() => {
            if (this.searchKey) {
                // clear out records when user types a keyword, does not select any record and clicks away
                if (!this.selectedRecordId && this.searchKey) {
                    this.records = [];
                }

                let message =
                    !this.selectedRecordId &&
                    this.searchKey &&
                    this.records?.length === 0
                        ? "Select an option from the picklist or remove the search term."
                        : "";
                if (message) {
                    this.showNoRecordsAvailableMessage = false;
                }
                this.showError(message);
            }
        }, 500);
    }

    showError(message) {
        const searchInput = this.template.querySelector(".searchInput");
        if (searchInput) {
            searchInput.setCustomValidity(message);
            searchInput.reportValidity();
        }
    }

    notifyChangeToParent(recordId) {
        if (this.selectedRecordId !== recordId) {
            this.selectedRecordId = recordId;
            let record = this.records?.find((c) => c.id === recordId) || {};

            this.dispatchEvent(
                new CustomEvent("selected", {
                    detail: { record: record }
                })
            );
        }
    }

    /*  Start: create new dialog  */
    handleNewRecordClick() {
        this.isCreateNewFormLoading = true;
        this.records = [];
        this.showNoRecordsAvailableMessage = false;
        this.searchKey = "";
        this.showError("");
        this.showModal = true;
    }

    handleOnLoad() {
        this.isCreateNewFormLoading = false;
    }

    handleSubmit() {
        this.isCreateNewFormLoading = true;
    }

    async handleSuccess(event) {
        const payload = event.detail;
        const recordId = payload.id;

        const lookupData = {
            data: {
                objectApiName: this.objectName,
                primaryField: this.primaryField,
                secondaryField: this.secondaryField,
                recordId: recordId
            }
        };
        const result = await getRecord(lookupData);

        if (result.variant === "error") {
            const errorMessage =
                "Unable to create a record, please contact admin!";
            this.showError(errorMessage);
            this.dispatchEvent(
                new ShowToastEvent({
                    variant: "error",
                    message: errorMessage
                })
            );
        } else {
            this.records = result?.body || [];
            this.setDefaultId(recordId, this.records[0].primaryLabel);
            this.dispatchEvent(
                new ShowToastEvent({
                    variant: "success",
                    message: `${this.objectLabel} was created.`
                })
            );
            // console.log("getRecordNotifyChange", [{ recordId: recordId }]);
            getRecordNotifyChange([{ recordId: recordId }]);
        }

        this.isCreateNewFormLoading = false;
        this.records = [];
        this.handleDialogClose();
    }

    handleError() {
        this.isCreateNewFormLoading = false;
    }

    handleCancel() {
        this.handleDialogClose();
    }

    handleDialogClose() {
        this.showModal = false;
    }

    /*  End: create new dialog  */
}