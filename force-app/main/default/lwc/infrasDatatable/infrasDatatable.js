import { LightningElement, api, track } from 'lwc';
import saveInfras from '@salesforce/apex/ManageExistingInfrasController.saveInfras';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadStyle } from 'lightning/platformResourceLoader';
import dataTablePicklist from '@salesforce/resourceUrl/dataTablePicklist';

export default class InfrasDatatable extends LightningElement {
	@track _infras;
	@track showSpinner;
	@track errorsList;
	@api accountId;
	@track oldInfrasList;
	@api
	get infras() {
		return this._infras;
	}
	set infras(value) {
		debugger;
		let val = value;
		console.log('value  ' + value);
		this._infras = JSON.parse(JSON.stringify(value));
		this.oldInfrasList = JSON.parse(JSON.stringify(value));
	}
	@track _columns;
	@api
	get columns() {
		return this._columns;
	}
	set columns(value) {
		this._columns = Object.assign([], value);
	}
	@track draftValues = [];

	@api errors;

	connectedCallback() {
		debugger;
		var data = this.infras;
		loadStyle(this, dataTablePicklist);
	}

	handleSaveEdition(event) {
		this.showSpinner = true;
		let errorList = [];
		event.stopPropagation();
		this.draftValues.forEach((element) => {
			errorList = this.checkRequiredFields(
				this._infras.find((el) => el.Id === element.Id) ? this._infras.find((el) => el.Id === element.Id) : {},
				element,
				element.Id === 'row-0'
			);
		});

		let errors = { rows: {}, table: {} };

		let mapErrors = errorList.map((el) => el.record);
		let setError = new Set(mapErrors);
		let errorMessages = [];
		console.log(this.infras);
		setError.forEach((id) => {
			errorMessages = errorMessages.concat(errorList.filter((el) => el.record === id).map((error) => error.message));
			errors.rows[id] = {
				title: 'Error(s)',
				messages: errorList.filter((el) => el.record === id).map((error) => error.message),
				fieldNames: errorList.filter((el) => el.record === id).map((error) => error.field)
			};
		});

		if (errorList.length === 0) {
			this.draftValues.forEach((el) => {
				el.AccountId = this.accountId;
				el.Id = el.Id !== '' && !el.Id.includes('row-') ? el.Id : undefined;
			});

			saveInfras({ spcs: this.draftValues }).then((res) => {
				if (res === 'SUCCESS') {
					this.errorList = [];
					this.draftValues = [];
					this.showToast('Success!', 'Record Updated', 'success');
					this.showSpinner = false;
					this.template.querySelector('c-custom-data-table').refreshAllCells();
					this.dispatchEvent(
						new CustomEvent('saveddata', {
							bubbles: false
						})
					);
				} else {
					this.showToast('Error!', JSON.parse(res).ExceptionMessage, 'error');
					this.showSpinner = false;
				}
			});
		} else {
			this.showToast('Error!', errorMessages.join(';  '), 'error');
			this.showSpinner = false;

			errors.table.title = 'Error while saving contact...';
			errors.table.messages = errorMessages;
			this.errorsList = errors;
		}
	}

	checkRequiredFields(oldRecord, newRecord, newRecordFlag) {
		var errorList = [];

		this.infras.forEach((infra) => {
			if (infra.Access_Vendor__c == newRecord.Access_Vendor__c && infra.Id != newRecord.Id) {
				errorList.push({
					record: newRecordFlag === true ? 'row-0' : oldRecord.Id,
					field: 'Access_Vendor__c',
					message: 'There can be only one infra per vendor for a site.'
				});
			}
		});

		return errorList;
	}

	handleCancel() {
		this._infras = this.oldInfrasList;
		this.template.querySelector('c-custom-data-table').refreshAllCells();
	}
	setCellEditable(id, field) {
		var row = this.getRowNumber(id);
		var column = this.getColumnNumber(field);

		this.template.querySelector('c-custom-data-table').setCellEdit(row, column);
	}

	getRowNumber(id) {
		if (id.includes('row-')) {
			return this._infras.length > 0 ? this._infras.length - 1 : 0;
		}
		for (let i = 0; i < this.infras.length; i++) {
			if (this.infras[i].Id === id) {
				return i;
			}
		}
	}

	handleRowAction(event) {
		this.dispatchEvent(
			new CustomEvent('rowaction', {
				bubbles: false,
				detail: event.detail
			})
		);
	}

	handleSelection(event) {
		console.log(event.detail.selectedRows);
		console.log(event.target.selectedRows);
		this.dispatchEvent(
			new CustomEvent('rowselection', {
				bubbles: false,
				selectedRows: event.detail.selectedRows,
				detail: event.detail,
				target: event.target
			})
		);
	}

	getColumnNumber(field) {
		let columnMap = {
			Access_Vendor__c: 2,
			Total_MB_Existing_EVC__c: 3,
			SiteName: 4,
			Technology_Type__c: 5,
			Accessclass__c: 6,
			Access_Max_Down_Speed__c: 7,
			Access_Max_Up_Speed__c: 8
		};
		return columnMap[field];
	}

	handlePicklistChange(event) {
		event.stopPropagation();
		let dataRecieved = JSON.parse(JSON.stringify(event.detail.data));
		let id = dataRecieved.context;
		id = id === '' ? 'row-0' : id;
		const field = dataRecieved.label;
		const value = dataRecieved.value;

		this.setCellEditable(id, field);
		this.updateDraftValues(id, field, value);
	}

	handleCellChange(event) {
		let field = Object.keys(event.detail.draftValues[0])[0];
		let id = event.detail.draftValues[0].Id;
		/* let id = idTemp.includes("row-")
			? this._infras[parseInt(idTemp.split("row-")[1], 10)].Id
			: idTemp; */

		let value = event.detail.draftValues[0][field];

		this.setCellEditable(id, field);
		this.updateDraftValues(id, field, value);
	}

	updateDraftValues(id, field, value) {
		this.updateDataValues(id, field, value);

		let draftValueChanged = false;
		let copyDraftValues = [...this.draftValues];
		copyDraftValues.forEach((item) => {
			if (item.Id === id) {
				item[field] = value;
				draftValueChanged = true;
			}
		});

		if (draftValueChanged) {
			this.draftValues = [...copyDraftValues];
		} else {
			let newDraft = {};
			newDraft[field] = value;
			newDraft.Id = id;
			this.draftValues = Object.assign([], [...copyDraftValues, newDraft]);
		}
	}
	updateDataValues(id, field, value) {
		let copyData = [...this._infras];
		if (copyData.find((el) => el.Id === id)) {
			copyData.find((el) => el.Id === id)[field] = value;
		} else if (copyData.find((el) => el.Id === '')) {
			copyData.find((el) => el.Id === '')[field] = value;
		}

		this._ifnras = [...copyData];
	}

	showToast(title, message, variant) {
		const event = new ShowToastEvent({
			title,
			variant,
			message
		});
		this.dispatchEvent(event);
	}
	@api
	toggleSpinner(value) {
		debugger;
		this.showSpinner = value;
	}
}
