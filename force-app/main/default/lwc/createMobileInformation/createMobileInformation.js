/* eslint-disable @lwc/lwc/no-async-operation */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-undef */
import { LightningElement, api, wire, track } from 'lwc';
import { refreshApex } from '@salesforce/apex';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getCTNDataTableMetadata from '@salesforce/apex/CreateMobileInformationController.getCTNDataTableMetadata';
import basketContractIsCreated from '@salesforce/apex/CreateMobileInformationController.basketContractIsCreated';
import getOpportunity from '@salesforce/apex/CreateMobileInformationController.getOpportunity';
import getViewColumns from '@salesforce/apex/CreateMobileInformationController.getViewColumns';
import save from '@salesforce/apex/CreateMobileInformationController.save';
import getNetProfitCtn from '@salesforce/apex/CreateMobileInformationController.getNetProfitCtn';
import importCSV from '@salesforce/apex/CreateMobileInformationController.importCSV';
import createNumberListPDF from '@salesforce/apex/CreateMobileInformationController.createNumberListPDF';
import getQuoteProfiles from '@salesforce/apex/CreateMobileInformationController.getQuoteProfiles';
import styleOverride from '@salesforce/resourceUrl/createMobileInformationStyle';
import { reduceErrors } from 'c/ldsUtils';
import { loadStyle } from 'lightning/platformResourceLoader';

import { publish, subscribe, MessageContext } from 'lightning/messageService';
import JOURNEY_MESSAGE_CHANNEL from '@salesforce/messageChannel/JourneyMessages__c';

/**
 *Commented sharing field, columns and picklist functionality due to Red Pro Issue.
 */
export default class CreateMobileInformation extends LightningElement {
	@api recordId;
	@api isJourney = false;
	@track netProfitCtns;
	@track showColumns;
	@track quoteProfiles;
	@track quoteProfilesDummy;
	@track doneLoading = false;
	@track previewState = true;
	@track showDownloadModal = false;
	@track showImportModal = false;
	@track showSpinner = false;
	@track isRendered = true;
	@track contractCreated = true;
	@track draftValues;
	@track columns = [];
	@track eventMessage;
	fileData;
	csvSeparator = ';';
	error;
	sortedBy = 'Quote_Profile__c';
	sortedDirection = 'asc';
	lastSavedData = [];
	resetSavedData = [];
	isScroll = false;
	scrollToCell = {};
	fieldToLabel = {};
	validationMessage = "Please correct the information for the following CTN's before progressing to the next step";

	@api
	get columnWidth() {
		return this.previewState ? 0 : 250;
	}

	get options() {
		return [
			{ label: ';', value: ';' },
			{ label: ',', value: ',' }
		];
	}

	@wire(MessageContext)
	messageContext;

	@wire(getCTNDataTableMetadata)
	async getCustomMetadata() {
		await getCTNDataTableMetadata()
			.then((data) => {
				let columMetadata = [];
				if (data) {
					data.forEach((row) => {
						let copyRow = { ...row };
						if (copyRow.typeAttributes__c !== undefined) {
							copyRow.typeAttributes__c = JSON.parse(copyRow.typeAttributes__c);
						}
						columMetadata.push(JSON.parse(JSON.stringify(copyRow).replaceAll('__c":', '":').replaceAll('"Label":', '"label":')));
					});
					this.columns = JSON.parse(JSON.stringify(columMetadata));
					this.fieldToLabel = Object.assign(
						{ Id: 'Id' },
						this.columns
							.filter((entry) => entry.exportable)
							.reduce(function (object, column) {
								object[column.label] = column.fieldName;
								return object;
							}, {})
					);

					this.error = undefined;
				}
			})
			.catch((error) => {
				this.error = error;
				this.columns = undefined;
				this.toastError(reduceErrors(error)[0]);
				publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
			});
	}

	@wire(getOpportunity, { oppId: '$recordId' }) theOpp;
	async getCTN() {
		let tempResult = [];
		await getNetProfitCtn({
			oppId: this.recordId,
			sortField: this.sortedBy,
			sortDirection: this.sortedDirection
		})
			.then((result) => {
				return result;
			})
			.then((result) => {
				result.forEach((element) => {
					tempResult.push(element);
				});
				this.showSpinner = false;
			});
		this.dummy = tempResult;
		if (tempResult) {
			this.netProfitCtns = tempResult;
			this.lastSavedData = JSON.parse(JSON.stringify(this.netProfitCtns));
			this.error = undefined;
		}
	}

	@wire(getQuoteProfiles, { oppId: '$recordId' })
	wiredQuoteProfiles(result) {
		this.quoteProfilesDummy = result;
		if (result.data) {
			this.quoteProfiles = result.data;
			this.error = undefined;
		} else if (result.error) {
			this.error = result.error;
			this.toastError(reduceErrors(this.error)[0]);
		}
	}

	closeModal() {
		this.showDownloadModal = false;
		this.showImportModal = false;
	}
	openDownloadModal() {
		this.showDownloadModal = true;
	}

	openImportModal() {
		this.showImportModal = true;
	}

	handPreviewButtonClick() {
		this.showSpinner = true;
		setTimeout(() => {
			this.previewState = !this.previewState;
			this.creatColumnList();
		}, 0);
		setTimeout(() => {
			this.validateData(this.netProfitCtns);
		}, 500);
	}

	connectedCallback() {
		this.showSpinner = true;
		this.getCustomMetadata();
		this.getBaskerConractIsCreated();
		this.subscribeToMessageChannel();
		this.creatColumnList();
		this.getCTN();
		Promise.all([loadStyle(this, styleOverride)]);
	}

	renderedCallback() {
		if (this.isScroll) {
			this.setCellFocus();
			this.isScroll = false;
			this.scrollToCell = {};
		}
	}

	subscribeToMessageChannel() {
		this.subscription = subscribe(this.messageContext, JOURNEY_MESSAGE_CHANNEL, (message) => {
			this.eventMessage = message;
			this.handleMessage(message);
		});
	}

	handleMessage(message) {
		if (message.currentStep === 'CTN Details') {
			this.doSave();
		}
	}

	handleImportSubmit(event) {
		this.showImportModal = false;
		this.showSpinner = true;
		setTimeout(async () => {
			this.fileData = Object.assign({}, event.detail);
			this.fileData.recordId = this.recordId;
			if (this.fileData && this.fileData.base64 !== undefined) {
				if (this.parseAndValidateCSV(this.fileData.base64, this.fileData.separator)) {
					this.toastError(this.validationMessage);
					this.showSpinner = false;
					return;
				}
				const { base64, filename, recordId, separator } = this.fileData;
				await importCSV({ base64, filename, recordId, separator })
					.then((result) => {
						this.fileData = null;
						let title = `${filename} uploaded successfully!!`;
						this.toastSucces(title);
						this.refresh();

						this.draftValues = null;
						this.resetSavedData = null;
					})
					.catch((error) => {
						this.toastError(reduceErrors(error)[0]);
						publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
						this.resetSavedData = JSON.parse(JSON.stringify(this.netProfitCtns));
					})
					.then((result) => {
						this.refresh();
						this.showSpinner = false;
					});
			} else {
				this.toastError('Please select a file to import');
				this.showSpinner = false;
			}
		}, 10);
	}

	exportToCSV(event) {
		this.showDownloadModal = false;
		this.csvSeparator = event.detail;
		let rowEnd = '\n';
		let csvString = '';

		let rowData = Object.keys(this.fieldToLabel);

		// splitting using ','
		csvString += rowData.join(this.csvSeparator);
		csvString += rowEnd;

		// main for loop to get the data based on key value
		for (let i = 0; i < this.netProfitCtns.length; i++) {
			let colValue = 0;

			// validating keys in data
			for (let key in rowData) {
				if (rowData.hasOwnProperty(key)) {
					// Key value
					// Ex: Id, Name
					let rowKey = rowData[key];
					if (colValue > 0) {
						csvString += this.csvSeparator;
					}
					let value =
						this.netProfitCtns[i][this.fieldToLabel[rowKey]] === undefined ? '' : this.netProfitCtns[i][this.fieldToLabel[rowKey]];
					if (rowKey == 'Discount %' && value != '') {
						value = (value * 100).toFixed(2);
					}
					if ((rowKey == 'Simcard Number VF' || rowKey == 'Contract Number Current Provider') && value != '') {
						value = '\t' + value;
					}
					if (rowKey == 'Porting Wishdate (yyyy-mm-dd)' && value != '') {
						value = '\t' + this.formatDate(value);
					}

					csvString += value;
					colValue++;
				}
			}
			csvString += rowEnd;
		}

		// Creating anchor element to download
		let downloadElement = document.createElement('a');

		// This  encodeURI encodes special characters, except: , / ? : @ & = + $ # (Use encodeURIComponent() to encode these characters).
		downloadElement.href = 'data:text/csv;charset=utf-8,' + encodeURI(csvString);
		downloadElement.target = '_self';
		// CSV File Name
		downloadElement.download = 'NetProfitCTN.csv';
		// below statement is required if you are using firefox browser
		document.body.appendChild(downloadElement);
		// click() Javascript function to download CSV file
		downloadElement.click();
	}

	doSave(event) {
		let merge = (orig, dest, key = 'Id') => orig.filter((elem) => !dest.find((subElem) => subElem[key] === elem[key])).concat(dest);
		this.draftValues = event ? merge(event.detail.draftValues, this.draftValues) : null;
		this.showSpinner = this.draftValues ? true : false;
		setTimeout(async () => {
			if (this.validateData(this.netProfitCtns)) {
				this.toastError(this.validationMessage);
				publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
				this.showSpinner = false;
			} else {
				save({
					original: this.netProfitCtns,
					valuesToSaveList: this.draftValues ? this.draftValues : this.netProfitCtns,
					oppId: this.recordId
				})
					.then((result) => {
						let title = `Values saved successfully!!`;
						this.toastSucces(title);

						this.showSpinner = false;
						if (this.draftValues === null) {
							publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, {
								nextStep: this.eventMessage.nextStep,
								message: 'SUCCESS'
							});
						}

						this.draftValues = null;
						this.resetSavedData = null;
					})
					.catch((error) => {
						this.toastError(reduceErrors(error)[0]);
						publish(this.messageContext, JOURNEY_MESSAGE_CHANNEL, { message: 'Error' });
						this.resetSavedData = JSON.parse(JSON.stringify(this.netProfitCtns));
					})
					.then((result) => {
						this.refresh();
						this.showSpinner = false;
					});
			}
		}, 10);
	}

	validateData(netProfitCTNList) {
		let validationFailed = false;
		let scrollToFirstErrorCell = {};
		netProfitCTNList.forEach((row, index) => {
			let netProfitCTN = this.netProfitCtns.find((netCTN) => netCTN.Id === row.Id);
			if (netProfitCTN === undefined) {
				return;
			}
			let updateItem = {};

			Object.entries(row).forEach(([key, value]) => {
				if (key === 'Id') {
					return;
				}
				let fieldMap = Object.entries(this.fieldToLabel).find((fieldKey) => fieldKey[0] === key || fieldKey[1] === key);
				let columnNameToSearch = fieldMap === undefined ? key : fieldMap[0];
				let columnDefinition = this.columns.find((column) => column.label === columnNameToSearch);
				let label = columnDefinition?.label;
				let fieldName = columnDefinition?.fieldName;
				let validation = columnDefinition?.validation;

				let columnNumber = this.showColumns.findIndex((column) => column.label === columnNameToSearch);
				if (label != null && validation != null) {
					let pattern = new RegExp(validation);
					if (!pattern.test(value.trim()) && value !== '' && columnNumber >= 0) {
						validationFailed = true;
						scrollToFirstErrorCell.row = index;
						scrollToFirstErrorCell.column = columnNumber + 1;
						this.template.querySelector('c-custom-data-table').setCellError(index, columnNumber + 1);
					} else {
						this.template.querySelector('c-custom-data-table').removeCellError(index, columnNumber + 1);
					}
				}

				if (
					netProfitCTN !== null &&
					fieldName !== undefined &&
					`${netProfitCTN[fieldName]}` !== `${value}`.trim() &&
					(`${netProfitCTN[fieldName]}` !== 'undefined' || `${value}`.trim() !== '')
				) {
					updateItem.Id = netProfitCTN.Id;
					updateItem[fieldName] = value;
				}
			});
			if (JSON.stringify(updateItem) !== '{}') {
				this.updateDraftValues(updateItem);
				this.updateDataValues(updateItem);
			}
		});

		if (JSON.stringify(scrollToFirstErrorCell) !== '{}') {
			this.scrollToCell = scrollToFirstErrorCell;
			this.isScroll = true;
		} else {
			this.isScroll = false;
		}
		return validationFailed;
	}

	parseAndValidateCSV(base64Data, delimiter) {
		let parsedCSVData = this.parseCSV(base64Data, delimiter);
		return this.validateData(parsedCSVData);
	}

	parseCSV(base64Data, delimiter = ';') {
		let decodedBase64Data = atob(base64Data).trim();

		const headers = decodedBase64Data.slice(0, decodedBase64Data.indexOf('\n')).split(delimiter);

		const rows = decodedBase64Data.slice(decodedBase64Data.indexOf('\n') + 1).split('\n');

		const rowData = rows.map(function (row) {
			const values = row.split(delimiter);
			const el = headers.reduce(function (object, header, index) {
				object[header.replace(/^"|"$/g, '')] = values[index].replace(/^"|"$/g, '');
				return object;
			}, {});
			return el;
		});

		return rowData;
	}

	refresh() {
		this.updateValues();
		this.doneLoading = true;
	}

	updateValues() {
		if (this.resetSavedData) {
			this.netProfitCtns = JSON.parse(JSON.stringify(this.resetSavedData));
		} else {
			this.getCTN();
		}
	}

	async getBaskerConractIsCreated() {
		await basketContractIsCreated({ oppId: this.recordId })
			.then((result) => {
				return result;
			})
			.then((result) => {
				this.contractCreated = result;
			});
	}

	async creatColumnList() {
		this.showColumns = !this.previewState
			? this.columns
			: await getViewColumns().then((result) => {
					return this.columns.filter((element) => result.includes(element.fieldName));
			  });

		this.showSpinner = false;
	}

	formatDate(date) {
		var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();

		if (month.length < 2) {
			month = '0' + month;
		}
		if (day.length < 2) {
			day = '0' + day;
		}
		let returnDate = [year, month, day].join('-');
		return returnDate;
	}

	createNumberList() {
		createNumberListPDF({ oppId: this.recordId, ctns: this.netProfitCtns })
			.then((result) => {
				let title = `Numberlist created successfully!!`;
				this.toastSucces(title);
				location.reload();
			})
			.catch((error) => {
				this.toastError(reduceErrors(error)[0]);
			});
	}

	toastSucces(title) {
		const toastEvent = new ShowToastEvent({
			title,
			variant: 'success'
		});
		this.dispatchEvent(toastEvent);
	}

	toastError(title) {
		const toastEvent = new ShowToastEvent({
			title,
			variant: 'error'
		});
		this.dispatchEvent(toastEvent);
	}

	updateColumnSorting(event) {
		this.sortedBy = event.detail.fieldName;
		this.sortedDirection = event.detail.sortDirection;

		getNetProfitCtn({
			oppId: this.recordId,
			sortField: this.sortedBy,
			sortDirection: this.sortedDirection
		})
			.then((result) => {
				this.netProfitCtns = result;
				this.error = undefined;
			})
			.catch((error) => {
				this.error = error;
				this.contacts = undefined;
				this.toastError(reduceErrors(this.error)[0]);
			});
	}

	updateDraftValues(updateItem) {
		if (this.draftValues == null) {
			this.draftValues = [];
		}
		let draftValueChanged = false;
		let copyDraftValues = [...this.draftValues];
		//store changed value to do operations
		//on save. This will enable inline editing &
		//show standard cancel & save button
		copyDraftValues.forEach((item) => {
			if (item.Id === updateItem.Id) {
				// eslint-disable-next-line guard-for-in
				for (let field in updateItem) {
					item[field] = updateItem[field];
				}
				draftValueChanged = true;
			}
		});

		if (draftValueChanged) {
			this.draftValues = [...copyDraftValues];
		} else {
			this.draftValues = [...copyDraftValues, updateItem];
		}

		for (let field in updateItem) {
			if (field === 'Id') {
				this.scrollToCell.row = this.getRowNumber(updateItem.Id);
			} else {
				this.scrollToCell.column = this.getReducedColumnNumber(field);
			}
		}
		if (this.scrollToCell.row >= 0 && this.scrollToCell.column >= 0) {
			this.isScroll = true;
		}
	}

	updateDataValues(updateItem) {
		let copyData = [...this.netProfitCtns];
		copyData.forEach((item) => {
			if (item.Id === updateItem.Id) {
				// eslint-disable-next-line guard-for-in
				for (let field in updateItem) {
					item[field] = updateItem[field];
				}
			}
		});

		//write changes back to original data
		this.netProfitCtns = [...copyData];
	}

	//listener handler to get the context and data
	//updates datatable
	picklistChanged(event) {
		event.stopPropagation();
		let dataRecieved = event.detail.data;
		let updateItem = {};
		if (dataRecieved.label === 'Data_Limit__c') {
			updateItem = {
				Id: dataRecieved.context,
				Data_Limit__c: dataRecieved.value
			};
			this.updateDraftValues(updateItem);
			this.updateDataValues(updateItem);
		}
		if (dataRecieved.label === 'dataBlock__c') {
			updateItem = {
				Id: dataRecieved.context,
				dataBlock__c: dataRecieved.value
			};
			this.updateDraftValues(updateItem);
			this.updateDataValues(updateItem);
		}
		if (dataRecieved.label === 'Salutation__c') {
			updateItem = {
				Id: dataRecieved.context,
				Salutation__c: dataRecieved.value
			};
			this.updateDraftValues(updateItem);
			this.updateDataValues(updateItem);
		}
	}

	handlePicklistClick(event) {
		event.stopPropagation();
		let dataRecieved = event.detail.data;
		this.scrollToCell.row = dataRecieved.context;
		this.scrollToCell.column = dataRecieved.label;
		this.setCellClick();
	}

	//handler to handle cell changes & update values in draft values
	handleCellChange(event) {
		event.stopPropagation();

		this.updateDraftValues(event.detail.draftValues[0]);
		this.updateDataValues(event.detail.draftValues[0]);
	}

	handleCancel() {
		//remove draftValues & revert data changes
		this.draftValues = [];
		this.netProfitCtns = JSON.parse(JSON.stringify(this.lastSavedData));
		this.template.querySelector('c-custom-data-table').refreshAllCells();
	}

	setCellClick() {
		this.template.querySelector('c-custom-data-table').setPicklistFocus(this.scrollToCell.row, this.scrollToCell.column);
	}

	setCellFocus() {
		this.template.querySelector('c-custom-data-table').scrollIntoCellView(this.scrollToCell.row, this.scrollToCell.column);
	}

	getRowNumber(id) {
		return this.netProfitCtns.findIndex((netProfitCtn) => netProfitCtn.Id === id);
	}

	getColumnNumber(field) {
		return this.showColumns.findIndex((column) => column.label === field || column.fieldName === field) + 1;
	}

	/*track added for grid index issue jira ticket SFO-2519*/
	@track columnsReduced = [
		{
			label: 'Model Number',
			fieldName: 'Quote_Profile__c',
			type: 'text',
			sortable: true
		} /**Model Number 1 **/,
		{
			label: 'Product Name',
			fieldName: 'Price_Plan_Description__c',
			type: 'text',
			sortable: true
		} /**Product Name 2 **/,
		{
			label: 'Deal Type',
			fieldName: 'Action__c',
			type: 'text',
			sortable: true
		} /**Deal Type 3 **/,
		{
			label: 'CTN',
			fieldName: 'CTN_Number__c',
			type: 'text',
			sortable: true,
			editable: true
		} /**CTN 4 **/,
		{
			label: 'Simcard Number VF',
			fieldName: 'Simcard_number_VF__c',
			type: 'text',
			sortable: true,
			editable: true
		} /**Simcard number Vodafone 5 **/ /****/,
		{
			label: 'Contract Number Current Provider',
			fieldName: 'Contract_Number_Current_Provider__c',
			type: 'text',
			sortable: true,
			editable: true
		} /**Contract Number Current Provider 6**/,
		{
			label: 'Current Provider Name',
			fieldName: 'Current_Provider_name__c',
			type: 'text',
			sortable: true,
			editable: true
		} /**Current Provider Name 7**/,
		{
			label: 'Porting Wishdate',
			fieldName: 'Contract_Enddate__c',
			type: 'date',
			sortable: true,
			editable: true
		} /**Wish Date 8**/
	];

	/*function added for grid index issue jira ticket SFO-2519*/

	getReducedColumnNumber(field) {
		return this.columnsReduced.indexOf(field);
	}

	/*end ticket SFO-2519*/
}
