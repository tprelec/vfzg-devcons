import { LightningElement, api, track, wire } from 'lwc';
import saveOpportunityDescription from '@salesforce/apex/OrderEntryInstallationController.saveOpportunityDescription';
import updateOpportunityOverstapService from '@salesforce/apex/OrderEntryInstallationController.updateOpportunityOverstapService';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class OrderEntryInstallationFixed extends LightningElement {
	@wire(MessageContext)
	messageContext;
	@api opportunityId;
	@api b2cInternetCustomer;
	@track _notes;
	@api
	get notes() {
		return this._notes;
	}
	set notes(value) {
		this._notes = value;
	}
	@track _bundle;
	@api
	get bundle() {
		return this._bundle;
	}
	set bundle(value) {
		this._bundle = Object.assign({}, value);
	}
	@api addressCheckResult;
	@track _installation;
	@api
	get installation() {
		return this._installation;
	}
	set installation(value) {
		this._installation = Object.assign({}, value);
	}
	@track _operatorSwitch;
	@api
	get operatorSwitch() {
		return this._operatorSwitch;
	}
	set operatorSwitch(value) {
		this._operatorSwitch = Object.assign({}, value);
	}

	/**
	 * descriptino checking input data
	 * @returns boolean value
	 */
	@api
	validate() {
		var resultInformation = this.template.querySelector('c-order-entry-installation-information').validate();
		var resultOverstapservice =
			this.template.querySelector('c-order-entry-installation-overstapservice').validateCurrentContractNumber() &&
			this.template.querySelector('c-order-entry-installation-overstapservice').validatePotentialTerminationFee();

		if (!resultInformation || !resultOverstapservice) {
			return false;
		}
		return true;
	}

	/**
	 * description save data on opportunit
	 */
	@api
	async saveData() {
		await saveOpportunityDescription({
			opportunityId: this.opportunityId,
			notes: this.notes
		});

		await updateOpportunityOverstapService({
			opportunityId: this.opportunityId,
			operatorSwitch: this.operatorSwitch.requested ? true : false,
			currentProvider: this.operatorSwitch.currentProvider,
			currentProviderContractNumber: this.operatorSwitch.currentContractNumber
		});
	}

	/**
	 * set initial data
	 */
	connectedCallback() {
		if (!this.installation?.earliestInstallationDate) {
			this._installation = {
				earliestInstallationDate: null
			};
		}
		if (!this.operatorSwitch) {
			this._operatorSwitch = {
				requested: false,
				currentProvider: '',
				currentContractNumber: '',
				potentialFeeAccepted: false
			};
		}
		if (!this.notes) {
			this._notes = '';
		}

		if (this.bundle) {
			this._bundle.installation = this.installation;
			this._bundle.operatorSwitch = this.operatorSwitch;
		}

		if (this.bundle.type === 'Fixed') {
			this.publishMessage('installation-bundle-changed', { bundle: this.bundle });
			this.publishMessage('notes-changed', { notes: this.notes });
		}
	}

	/**
	 * change installation information
	 * @param {*} e
	 */
	installationInformationChange(e) {
		if (typeof e.detail.value !== 'object' || e.detail.value === null) {
			this._installation[e.detail.key] = e.detail.value;
		} else {
			this._installation[e.detail.key] = Object.assign({}, e.detail.value);
		}

		if (this.bundle) {
			this._bundle.installation = this.installation;
		}

		if (this.bundle.type === 'Fixed') {
			this.publishMessage('installation-bundle-changed', { bundle: this.bundle });
		}
	}

	/**
	 * change overstapservice and publish data
	 * @param {*} e
	 */
	overstapservicestagechange(e) {
		var overstapservicestage = Object.assign({}, e.detail);
		Object.keys(overstapservicestage).forEach((k) => {
			this._operatorSwitch[k] = overstapservicestage[k];
		});

		if (this.bundle) {
			this._bundle.operatorSwitch = this.operatorSwitch;
		}
		if (this.bundle.type === 'Fixed') {
			this.publishMessage('installation-bundle-changed', { bundle: this.bundle });
		}
	}

	/**
	 * change notest i publish data
	 * @param {*} e
	 */
	notesChange(e) {
		this._notes = e.detail;

		if (this.bundle.type === 'Fixed') {
			this.publishMessage('notes-changed', { notes: this.notes });
		}
	}

	/**
	 * publish generic message
	 * @param {*} event
	 * @param {*} data
	 */
	publishMessage(event, data) {
		const msg = {
			sourceComponent: 'order-entry-installation-fixed',
			payload: {
				event,
				data
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}

	handleError(error) {
		this.dispatchEvent(
			new ShowToastEvent({
				title: 'Something went wrong',
				message: error,
				variant: 'warning'
			})
		);
	}
}
