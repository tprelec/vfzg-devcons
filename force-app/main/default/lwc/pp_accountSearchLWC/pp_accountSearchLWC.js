import { LightningElement, api, track } from 'lwc';

import LABEL_NO_ACCOUNT_SELECTED_KVK from '@salesforce/label/c.pp_No_Account_Selected_KVK';

const OBJECT_NAME_ACCOUNT = 'Account';

export default class Pp_accountSearchLWC extends LightningElement {
	label = {
		LABEL_NO_ACCOUNT_SELECTED_KVK
	};

	oppId;
	record;
	recordId;
	objectName;
	accountKvkNumber;
	disabledComponent;
	accountRecordFromAura;
	kvkInactive;

	@track isLoading;
	handleIsLoading(event) {
		this.isLoading = event.detail;
	}

	@api
	get accountRecord() {
		return this.accountRecordFromAura;
	}
	set accountRecord(value) {
		if (value) {
			this.accountRecordFromAura = value;
			if (this.accountRecordFromAura.Id !== this.recordId) {
				this.record = value;
				this.recordId = value.Id;
				this.accountKvkNumber = value.KVK_number__c;
				this.objectName = OBJECT_NAME_ACCOUNT;
			}
		}
	}

	@api
	get opportunityId() {
		return this.oppId;
	}
	set opportunityId(value) {
		this.disabledComponent = value ? true : false;
		this.oppId = value;
	}

	handleAccountSelected = (event) => {
		let data = event.detail.data;
		if (data && data.record) {
			this.record = data.record;
			this.recordId = data.recordId;
			this.objectName = data.objectName;
			this.kvkInactive = false;
			if (data.record.Location_Status__c === 'E') {
				this.kvkInactive = true;
			}
		} else {
			this.record = null;
			this.recordId = null;
		}
	};
}
