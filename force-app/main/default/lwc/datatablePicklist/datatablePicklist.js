import { LightningElement, api, track } from 'lwc';

export default class DatatablePicklist extends LightningElement {
	@api label;
	@api placeholder;
	@track _options;
	@api rowIdentifier;
	@api
	get options() {
		return this._options;
	}
	set options(value) {
		this._options = Object.assign([], value);
	}

	@track _value;
	@api
	get value() {
		return this._value;
	}
	set value(value) {
		this._value = value;
	}
	@api context;
	@api fieldname;
	@api disabled;

	handleChange(event) {
		//show the selected value on UI
		this._value = event.detail.value;
		this.dispatchEvent(
			new CustomEvent('picklistchanged', {
				composed: true,
				bubbles: true,
				cancelable: true,
				detail: {
					data: {
						label: this.label,
						rowIdentifier: this.rowIdentifier,
						context: this.context,
						value: this.value
					}
				}
			})
		);
	}

	handleMouseDown(event) {
		this.dispatchEvent(
			new CustomEvent('picklistclicked', {
				composed: true,
				bubbles: true,
				cancelable: true,
				detail: {
					data: { label: this.label, context: this.context, value: this.value }
				}
			})
		);
	}
}
