import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
import { CurrentPageReference } from 'lightning/navigation';

// apex classes & methods reference
//export default class HelptextBasic extends LightningElement {};
//este export é teste para o helptext
import getQuestionsFromBank from '@salesforce/apex/CST_QuestionBank_Handler.CST_GetQuestionsFromBank';
import handleApexSubmit from '@salesforce/apex/CST_QuestionBank_Handler.handleApexSubmit';
import checkExistingQuestionnaire from '@salesforce/apex/CST_QuestionBank_Handler.checkExistingQuestionnaire';

//custom labels
import existingQuestionnaireTitle from '@salesforce/label/c.CST_ExistingQuestionnaire_Title';
import existingQuestionnaireMessage from '@salesforce/label/c.CST_ExistingQuestionnaire_Message';
import questionnaireSuccessTitle from '@salesforce/label/c.CST_QuestionnaireSuccess_Title';
import questionnaireSuccessMessage from '@salesforce/label/c.CST_QuestionnaireSuccess_Message';
import noQuestionnaireTitle from '@salesforce/label/c.CST_NoQuestionnaireAvailable_Title';
import noQuestionnaireMessage from '@salesforce/label/c.CST_NoQuestionnaireAvailable_Message';
export default class Cst_questionaireSubmission extends LightningElement {
	@api recordId;
	@track existingQuestionnaire;
	@track hideModal;
	questionList;
	answerList = [];
	isLoading = false;
	isSubmitting = false;
	btnSubmitDisabled = false;

	@wire(CurrentPageReference)
	getStateParameters(currentPageReference) {
		if (currentPageReference) {
			this.recordId = currentPageReference.state.recordId;
		}
	}
	constructor() {
		super();
		this.existingQuestionnaire = true;
		this.hideModal = true;
	}
	renderedCallback() {}
	connectedCallback() {
		if (this.recordId) {
			checkExistingQuestionnaire({ caseId: this.recordId })
				.then((response) => {
					this.existingQuestionnaire = response;
					if (this.existingQuestionnaire === true) {
						this.fireToastMessage(existingQuestionnaireTitle, existingQuestionnaireMessage, 'warning');
						this.handleCancel();
					}
				})
				.then((response) => {
					if (this.existingQuestionnaire !== true) {
						getQuestionsFromBank({ caseId: this.recordId })
							.then((result) => {
								console.log('result', result);
								if (result.length == 0) {
									//this.existingQuestionnaire = true;
									this.fireToastMessage(noQuestionnaireTitle, noQuestionnaireMessage, 'error');
									this.handleCancel();
								} else {
									var lineItemsListAux = [];
									for (var i = 0; i < result.length; i++) {
										console.log('result[i].CST_Mandatory__c -> ' + result[i].CST_Mandatory__c);
										var isRequired = result[i].CST_Mandatory__c ? true : false;
										lineItemsListAux.push({
											question: result[i].CST_Question__c,
											questionType: result[i].CST_Question_Type__c,
											isMandatory: isRequired,
											id: result[i].id,
											helptext: result[i].CST_Questionaire_HelpText__c
										});
									}
									this.questionList = lineItemsListAux;
									console.log('questionList', this.questionList);
									this.hideModal = false;
								}
							})
							.catch((error) => {
								console.log('error');
								console.dir(error);
							});
					}
				})
				.catch((error) => {
					console.log('existingQuestionnaire error: ' + error);
					this.fireToastMessage('Something went wrong', 'Please check with support to see what went wrong.', 'error');
					this.handleCancel();
				});
		}
	}

	handleChange(event) {}

	fireToastMessage(title, message, variant) {
		const toaster = new ShowToastEvent({
			title: title,
			message: message,
			variant: variant
		});
		this.dispatchEvent(toaster);
	}

	handleSubmit(event) {
		const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, inputField) => {
			inputField.reportValidity();
			return validSoFar && inputField.checkValidity();
		}, true);
		if (isInputsCorrect) {
			this.submitQuestionnaire();
		}
	}

	submitQuestionnaire() {
		var answerCollection = [];
		var answerCollectionDOM = this.template.querySelectorAll('lightning-input');

		answerCollectionDOM.forEach((question) => {
			var questionValue;
			if (question.type == 'checkbox') {
				questionValue = question.checked ? 'Yes' : 'No';
			} else {
				questionValue = question.value;
			}

			var answer = {
				CST_Question__c: question.label,
				CST_Answer__c: questionValue,
				CST_Case__c: this.recordId,
				CST_QuestionType__c: question.type,
				CST_Mandatory__c: question.required,
				CST_Questionaire_HelpText__c: question.fieldLevelHelp
			};
			console.log('answer', { answer });
			answerCollection.push(answer);
		});

		this.isLoading = true;
		this.btnSubmitDisabled = true;

		handleApexSubmit({ answers: answerCollection, caseId: this.recordId })
			.then((response) => {
				var questionnaireUrl = window.location.origin + '/' + response.Id;
				var questionnaireName = response.Name;

				if (!this.existingQuestionnaire) {
					this.toaster = new ShowToastEvent({
						title: questionnaireSuccessTitle,
						message: questionnaireSuccessMessage,
						messageData: [
							'' + questionnaireName,
							{
								url: questionnaireUrl,
								label: '' + questionnaireName
							}
						],
						variant: 'success'
					});
					this.updateRecordView();
					this.dispatchEvent(this.toaster);
				}
			})
			.catch((error) => {
				this.isLoading = false;
				console.log('error');
				console.dir(error);
			});
	}

	get id() {
		return this.record.Id;
	}
	handleCancel() {
		this.dispatchEvent(new CloseActionScreenEvent());
		this.existingQuestionnaire = true;
		this.hideModal = true;
	}
	updateRecordView() {
		setTimeout(() => {
			this.dispatchEvent(new CloseActionScreenEvent());
			// updateRecord({ fields: { Id: this.recordId } });
			eval("$A.get('e.force:refreshView').fire();");
		}, 1000);
	}
}
