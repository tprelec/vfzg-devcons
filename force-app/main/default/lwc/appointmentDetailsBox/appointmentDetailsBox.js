/**
 * AppointmentDetailsBox
 * Used with zipDeliveryPlanning component to show information regarding appointments
 * Events fired:
 * 1- When the user Save the new information, if successful or if there is an error, send the error.
 * 2- When the user Deletes the Appointment, if successful or if there is an error, send the error.
 * 3- When the user Confirm the appointment.
 * @author: Rahul Sharma <rahul.sharma@vodafoneziggo.com>
 */
import { LightningElement, api, track } from 'lwc';
/** Custom Labels used in this LWC */
import selectAppointment from '@salesforce/label/c.selectAppointment';
import appointmentDetailsBoxTitle from '@salesforce/label/c.appointmentDetailsBoxTitle';
import appointmentBtnCancel from '@salesforce/label/c.appointmentBtnCancel';
import appointmentBtnConfirm from '@salesforce/label/c.appointmentBtnConfirm';
import appointmentBtnDel from '@salesforce/label/c.appointmentBtnDel';
import appointmentBtnEdit from '@salesforce/label/c.appointmentBtnEdit';
import appointmentBtnSave from '@salesforce/label/c.appointmentBtnSave';
import appointmentBtnUnconfirm from '@salesforce/label/c.appointmentBtnUnconfirm';
import createNewContact from '@salesforce/label/c.createNewContact';

const STATUS_UPDATED = 'updated';
const STATUS_CONFIRM = 'confirm';
const STATUS_UNCONFIRM = 'unconfirm';
const STATUS_DELETE = 'delete';

export default class AppointmentDetailsBox extends LightningElement {
  labels = {
    selectAppointment,
    appointmentDetailsBoxTitle,
    appointmentBtnCancel,
    appointmentBtnConfirm,
    appointmentBtnDel,
    appointmentBtnEdit,
    appointmentBtnSave,
    appointmentBtnUnconfirm,
    createNewContact
  };
  @track waitDialog;
  @api appointmentId; // 'aDI1w0000004oOIGAY';
  @api isConfirmed = false;
  @api isSpecial = false;
  @api isFullUser = false;

  isEdit = false;
  updatedFields = [];

  fieldSet = [
    { nm: 'Name', ro: false },
    { nm: 'Start__c', ro: false },
    { nm: 'Duration__c', ro: false },
    { nm: 'Contact__c', ro: true },
    { nm: 'Field_Engineer__c', ro: false },
    { nm: 'Location__c', ro: true },
    { nm: 'Description__c', ro: false },
    { nm: 'Related_Delivery_Order__c', ro: true },
    { nm: 'Products__c', ro: false }
  ];

  fieldSetEdit = [
    { nm: 'Name', ro: false },
    { nm: 'Start__c', ro: false },
    { nm: 'Duration__c', ro: false },
    { nm: 'Field_Engineer__c', ro: false },
    { nm: 'Location__c', ro: true },
    { nm: 'Description__c', ro: false },
    { nm: 'Related_Delivery_Order__c', ro: true }
  ];
  @api selectedContactId;

  @api
  setSpinner() {
    this.waitDialog = true;
  }

  handleChangeToEditView() {
    if(this.isSpecial === true) {
      this.fieldSet[5].ro = false;
    } else {
      this.fieldSet[5].ro = true;
    }
    this.isEdit = true;
  }

  handleConfirm() {
    this.dispatchEvent(new CustomEvent(STATUS_CONFIRM, { detail: this.appointmentId }));
  }

  handleUnconfirm() {
    this.dispatchEvent(new CustomEvent(STATUS_UNCONFIRM, { detail: this.appointmentId }));
  }

  handleDelete() {
    this.dispatchEvent(new CustomEvent(STATUS_DELETE, { detail: this.appointmentId }));
  }

  handleViewChange() {
    this.isEdit = false;
    this.selectedContactId = '';
  }

  handleSubmit(event) {
    this.updatedFields = event.detail.fields;
    this.updatedFields.id = this.appointmentId;
  }

  handleSuccess() {
    this.dispatchEvent(new CustomEvent(STATUS_UPDATED, {
      detail: JSON.stringify(this.updatedFields)
    }));
    this.handleViewChange();
  }

  hideSpinner() {
    this.waitDialog = false;
  }

  handleNewContact() {
    this.template.querySelector('c-create-new-contact').openModal();
  }

  handleOnNewContact(event) {
    let result = JSON.parse(event.detail);
    this.selectedContactId = result.id;
  }
}