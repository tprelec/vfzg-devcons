import { LightningElement } from 'lwc';

export default class Cs_packages extends LightningElement {

    products = [
        {
            "key": "SHSSHREHWERHW4ETJJWER",
            "name": "Start Xtra",
            "addressId": "a4D3O0000004VVjUAM",
            "productType": "Business Internet",
            "price": "€110,00",
            "details": {
                "access": "Coax aansluiting 1000/50 Mbps",
                "evc": "Service fee 50/50 Mbps",
                "cpe": "Flex CPE",
                "internet": "Internet toegang Coax"
            }
        },
        {
            "key": "FDSHSDJSDJDSDFGJDR",
            "name": "Complete Xtra",
            "addressId": "a4D3O0000004VVjUAM",
            "productType": "Business Internet",
            "price": "€100,00",
            "details": {
                "access": "Coax aansluiting 700/50 Mbps",
                "evc": "Service fee 700/700 Mbps",
                "cpe": "Flex CPE",
                "internet": "Internet toegang Coax"
            }
        },
        {
            "key": "PEGOKAKOGSADMFAKDS",
            "name": "Giga Xtra",
            "addressId": "a4D3O0000004VVjUAM",
            "productType": "Business Internet",
            "price": "€80,00",
            "details": {
                "access": "Coax aansluiting 500/50 Mbps",
                "evc": "Service fee 500/500 Mbps",
                "cpe": "Flex CPE",
                "internet": "Internet toegang Coax"
            }
        }
    ];

    communicator = {
        "exit": function() {
            window.parent.postMessage({ "action": "exit", "target": "packageModal" }, '*');
        },
        "notifyParent": function(message, action) {
            window.parent.postMessage({ "action": action, "message": message }, '*');
        }
    };

    exit = function(event) {
        this.communicator.exit();
    }
}