import { api, LightningElement, wire } from 'lwc';
import SECTION_HEADER from '@salesforce/label/c.Personal_Details_Header';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import LEAD_OBJECT from '@salesforce/schema/Lead';

export default class ServiceApptPersonalDetailsReview extends LightningElement {
	get label() {
		return {
			sectionHeader: SECTION_HEADER,
			fName: this.firstNameLabel,
			lName: this.lastNameLabel,
			email: this.emailLabel,
			phone: this.phoneLabel,
			company: this.companyLabel,
			optin: this.optInEmailLabel
		};
	}

	_lead;
	firstNameLabel;
	lastNameLabel;
	emailLabel;
	phoneLabel;
	companyLabel;
	optInEmailLabel;
	load = false;

	@wire(getObjectInfo, { objectApiName: LEAD_OBJECT })
	buildLabels({ data, error }) {
		if (data) {
			this.firstNameLabel = data.fields.FirstName.label;
			this.lastNameLabel = data.fields.LastName.label;
			this.emailLabel = data.fields.Email.label;
			this.phoneLabel = data.fields.Phone.label;
			this.companyLabel = data.fields.Company.label;
			this.optInEmailLabel = 'Opt-in Email Notifications';
		} else if (error) {
			console.error(error);
		}
	}

	get lead() {
		return this._lead;
	}

	@api
	set lead(value) {
		let temp = Object.assign({}, value);
		if (temp.Marketing_Commercial_News__c === '') {
			temp.optInEmailNotification = false;
		} else if (temp.Marketing_Commercial_News__c === 'Vodafone') {
			temp.optInEmailNotification = true;
		}
		this._lead = temp;
	}

	@api
	validate() {
		const allValid = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, inputComponent) => {
			inputComponent.reportValidity();
			return validSoFar && inputComponent.checkValidity();
		}, true);

		return !allValid;
	}

	connectedCallback() {
		this.dispatchEvent(new CustomEvent('loadcomplete'));
		this.load = true;
	}

	handleChange(event) {
		try {
			if (event.target.type === 'checkbox') {
				this._lead[event.target.name] = event.target.checked ? 'Vodafone' : '';
			} else {
				this._lead[event.target.name] = event.target.value;
			}
		} catch (error) {
			console.error('something went wrong on: ServiceApptPersonalDetailsReview. %O', error);
		}
	}
}
