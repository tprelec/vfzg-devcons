import { LightningElement, api } from 'lwc';

export default class OrderEntryAccountAndContact extends LightningElement {
	@api accountId;
	@api contactId;
	@api opportunityId;
	@api journeyType;
	@api b2cInternetCustomer;
	@api site;
	@api bundles;

	// Call the validation on Contact child component as Account component is read only
	@api async validate() {
		if (
			this.template.querySelector('c-order-entry-account-details').validate() &&
			this.template.querySelector('c-order-entry-contact-details').validate()
		) {
			return (
				(await this.template.querySelector('c-order-entry-account-details').saveAccountData()) &&
				(await this.template.querySelector('c-order-entry-contact-details').saveContact())
			);
		}

		return false;
	}
}
