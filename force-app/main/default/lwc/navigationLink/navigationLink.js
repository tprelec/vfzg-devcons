import { api, track, LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class NavigationLink extends NavigationMixin(LightningElement) {
	// public properties
	@api title;
	@api label;
	@api recordId;

	// reactive properties
	@track _url;

	// private properties
	type = 'standard__recordPage';
	actionName = 'view';
	target = '_blank';

	get url() {
		if (!this._url) {
			return `/${this.recordId}`;
		}
		return this._url;
	}

	renderedCallback() {
		this.navigationLinkRef = {
			type: this.type,
			attributes: {
				recordId: this.recordId,
				actionName: this.actionName
			}
		};

		if (this.navigationLinkRef) {
			this[NavigationMixin.GenerateUrl](this.navigationLinkRef).then((url) => {
				console.log(this.recordId, url);
				this._url = url;
			});
		}
	}
}
