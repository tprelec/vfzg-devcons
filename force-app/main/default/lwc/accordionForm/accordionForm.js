import { LightningElement, api } from 'lwc';
import parseJsonInput from '@salesforce/apex/AccordionFormController.parseJsonInput';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class AccordionForm extends LightningElement {
	@api recordId;
	@api inputJson;
	@api required = false;
	@api outputValidation = null;

	accordionActiveSection;
	sections;

	connectedCallback() {
		if (this.inputJson !== undefined) {
			parseJsonInput({ jsonInput: this.inputJson })
				.then((result) => {
					if (result.body !== undefined) {
						this.sections = result.body;
					} else if (result.body === undefined && result.variant && result.message) {
						if (result.variant === 'error') {
							this.isError = true;
						}
						this.showToast(result.variant, result.message);
					}
				})
				.catch((error) => {
					this.isError = true;
					console.dir(error);
					this.showToast('error', error.body.message);
				});
		}
	}

	changeFieldHandler(event) {
		let newValue = event.target.value;
		let fieldName = event.target.name;
		let currentSection = this.accordionActiveSection;
		for (let section of this.sections) {
			if (section.tittleReference === currentSection) {
				for (let row of section.listRows) {
					for (let field of row.listFields) {
						if (field.fieldeReference === fieldName) {
							field.value = newValue;
						}
					}
				}
			}
		}
	}

	@api
	setFieldValue(fieldReference, fieldValue) {
		this.template.querySelectorAll('lightning-input, lightning-input-field').forEach((element) => {
			if (fieldReference === '' + element.name) {
				element.value = fieldValue;
			}
		});
		for (let section of this.sections) {
			for (let row of section.listRows) {
				for (let field of row.listFields) {
					if (field.fieldeReference === fieldReference) {
						field.value = fieldValue;
					}
				}
			}
		}
	}

	@api
	getUpdatedInformation() {
		return this.sections;
	}

	handleToggleSection(event) {
		this.accordionActiveSection = event.detail.openSections;
	}

	showToast(variant, message) {
		const event = new ShowToastEvent({
			variant: variant,
			message: message
		});
		this.dispatchEvent(event);
	}
}
