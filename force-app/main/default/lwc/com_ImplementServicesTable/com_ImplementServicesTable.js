import { LightningElement, wire, api, track } from 'lwc';
import getServices from '@salesforce/apex/COM_ImplementServicesTblController.getServices';
import updateServices from '@salesforce/apex/COM_ImplementServicesTblController.updateServices';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { refreshApex } from '@salesforce/apex';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';

const columns = [
	{ label: 'Name', fieldName: 'parentServiceName', hideDefaultActions: true, wrapText: true },
	{ label: '', fieldName: 'name', hideDefaultActions: true, wrapText: true },
	{
		label: 'Implemented Date',
		fieldName: 'implementedDate',
		type: 'date',
		hideDefaultActions: true,
		wrapText: true,
		editable: true
	}
];

export default class COM_ImplementServicesTable extends LightningElement {
	tableData = [];
	tableColumns = columns;
	tableErrors = { rows: {}, table: {} };
	draftValues = [];

	@api recordId;
	@api constant = {
		ERROR_TITLE: 'Error Found',
		ERROR_IMPL_DATE_BEFORE_INSTALLATION: 'Implemented date cannot be set to before the installation date',
		ERROR_IMPL_DATE_FUTURE: 'Implemented date cannot be set to future',
		FNAME_IMPLEMENTED_DATE: 'implementedDate'
	};

	@wire(getServices, { recId: '$recordId' })
	servicesToUpdate;

	@wire(getServices, { recId: '$recordId' })
	wiredServices(result) {
		if (result.data) {
			this.tableData = result.data;
		} else if (result.error) {
			this.tableData = undefined;
			console.log(result.error);
		}
	}

	async handleSave(event) {
		const updatedFields = event.detail.draftValues;
		const notifyChangeIds = updatedFields.map((row) => {
			return { recordId: row.Id };
		});

		if (this.validateError(updatedFields)) {
			return;
		}

		let updatedServices = [];
		updatedFields.forEach((updatedRow) => {
			updatedServices.push({
				serviceId: this.tableData[updatedRow.id.split('-')[1]].serviceId,
				implementedDate: updatedRow.implementedDate
			});
		});

		try {
			const result = await updateServices({ data: updatedServices });
			this.draftValues = [];

			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Success',
					message: 'Services updated',
					variant: 'success'
				})
			);

			// Refresh LDS cache and wires
			getRecordNotifyChange(notifyChangeIds);

			// Display fresh data in the datatable
			refreshApex(this.servicesToUpdate).then(() => {
				// Clear all draft values in the datatable
				this.draftValues = [];
			});
		} catch (error) {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Error updating or refreshing records',
					message: error,
					variant: 'error'
				})
			);
		}
	}

	validateError(rowToValidate, mode) {
		try {
			//set the default return type to false
			let retType = false;

			//set tableErrors and rows
			this.tableErrors = {};
			this.tableErrors.rows = {};

			let todayDate = new Date().setHours(0, 0, 0, 0);
			//iterate to check for all changed rows
			rowToValidate.forEach((rowToValidate) => {
				let rowNumber = rowToValidate.id.split('-')[1];
				let implementedDate = new Date(rowToValidate.implementedDate).setHours(0, 0, 0, 0);
				if (implementedDate > todayDate) {
					this.tableErrors.rows[rowToValidate.id] = {
						title: [this.constant.ERROR_TITLE],
						messages: [this.constant.ERROR_IMPL_DATE_FUTURE],
						fieldNames: [this.constant.FNAME_IMPLEMENTED_DATE]
					};
					//change the return type to true if errors are there
					retType = true;
				} else if (implementedDate < new Date(this.tableData[rowNumber].installationDate).setHours(0, 0, 0, 0)) {
					this.tableErrors.rows[rowToValidate.id] = {
						title: [this.constant.ERROR_TITLE],
						messages: [this.constant.ERROR_IMPL_DATE_BEFORE_INSTALLATION],
						fieldNames: [this.constant.FNAME_IMPLEMENTED_DATE]
					};
					//change the return type to true if errors are there
					retType = true;
				}
			});

			return retType;
		} catch (errorMsg) {
			console.log('error occured inside validateError() method. See actual system message <' + errorMsg + '>');
		}
	}
}
