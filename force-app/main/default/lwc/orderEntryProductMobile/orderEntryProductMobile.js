import { api, LightningElement, track, wire } from 'lwc';
import { publish, MessageContext } from 'lightning/messageService';
import { getConstants } from 'c/orderEntryConstants';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getBundle from '@salesforce/apex/OrderEntryProductController.getBundle';
import getMobileContractTerms from '@salesforce/apex/OrderEntryProductController.getMobileContractTerms';
import getProductDependencies from '@salesforce/apex/OrderEntryProductController.getProductDependencies';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

const CONSTANTS = getConstants();

export default class OrderEntryProductMobile extends LightningElement {
	MOBILE_PRODUCT_TYPE = CONSTANTS.MOBILE_PRODUCT_TYPE;

	@wire(MessageContext)
	messageContext;
	@track selectedSimCardProduct;
	@track _products;
	@track _simCardProducts;
	@track productContractTerms;
	@track _contractTerm;
	@track _selectedProduct = {};
	@track _quantity = 1;
	@track _bundles = [];
	@track isAddProduct = false;
	@track isSimOnlyProduct = false;
	productDependencies;
	@track loading = true;
	index = -1; // used for edit and delete
	@api mainProduct;
	@api
	get isTable() {
		return this._bundles && this._bundles.length > 0;
	}
	@api
	get bundles() {
		return this._bundles;
	}
	set bundles(value) {
		this._bundles = Object.assign([], value);
	}
	@api
	get products() {
		return this._products;
	}
	set products(value) {
		this._products = Object.assign([], value);
		this.init();
	}
	@api
	get simCardProducts() {
		return this._simCardProducts;
	}
	set simCardProducts(value) {
		this._simCardProducts = Object.assign([], value);
	}
	@api
	get contractTerm() {
		return this._contractTerm;
	}
	set contractTerm(value) {
		this._contractTerm = value;
	}
	@api
	get selectedProduct() {
		return this._selectedProduct;
	}
	set selectedProduct(value) {
		this._selectedProduct = Object.assign({}, value);
	}

	@api
	get availableProducts() {
		let unavailableProducts = this.getUnavailableProducts();
		return this._products.filter((p) => {
			return !unavailableProducts.has(p.Id);
		});
	}

	/**
	 * Get products that are not allowed in the bundles
	 */
	getUnavailableProducts() {
		let currentProducts = new Set();
		let unavailableProducts = new Set();

		Object.values(this.productDependencies).forEach((vs) => {
			vs.forEach((v) => unavailableProducts.add(v));
		});
		this._bundles.forEach((b) => {
			b.products.forEach((p) => {
				currentProducts.add(p.id);
			});
		});
		currentProducts.forEach((p) => {
			if (this.productDependencies[p]) {
				this.productDependencies[p].forEach((v) => unavailableProducts.delete(v));
			}
		});

		return unavailableProducts;
	}

	get checkedAll() {
		return this.bundles.every((a) => a.checked);
	}
	/**
	 * checking input data
	 * @returns boolean value
	 */
	@api validate() {
		const productInput = this.template.querySelector('c-order-entry-mobile-product-form');
		return productInput ? productInput.validate() : true;
	}
	/**
	 * set initial value
	 */
	async init() {
		this.productDependencies = await getProductDependencies();
		this.productContractTerms = await getMobileContractTerms({ products: this._products });
		this.loading = false;
	}
	handleRenderedChange() {
		if (this.products.find((el) => el.Id === this.selectedProduct.id) !== undefined) {
			let data = {
				product: this.products.find((el) => el.Id === this.selectedProduct.id),
				addons: this.selectedAddons,
				contractTerm: this.contractTerm
			};
			this.publishMessage('product-changed', data);
		}
	}
	/**
	 * add product
	 */
	addProduct() {
		this._contractTerm = null;
		this.isAddProduct = true;
		this.isSimOnlyProduct = false;
	}
	/**
	 * close modal
	 */
	closeModal() {
		this.isAddProduct = false;
		this.isSimOnlyProduct = false;
	}
	/**
	 *  add sim only product
	 */
	addSimOnlyProduct() {
		this._selectedProduct = {};
		this.index = -1;
		this.isAddProduct = false;
		this.isSimOnlyProduct = true;
	}

	onCancel() {
		this.isAddProduct = false;
		this.isSimOnlyProduct = false;
	}

	/**
	 * handle edit product
	 * @param {*} e
	 */
	editProduct(e) {
		this.index = e.target.dataset.index;
		let selectedProduct = this._bundles[e.target.dataset.index].products.find((x) => x.type === CONSTANTS.MOBILE_PRODUCT_TYPE);
		let selectedSIMCard = this._bundles[e.target.dataset.index].products.find((x) => x.type === CONSTANTS.MOBILE_SIM_CARD_PRODUCT_TYPE);
		this._selectedProduct = this._products.find((x) => x.Id === selectedProduct.id);
		this.selectedSimCardProduct = this.simCardProducts.find((x) => x.Id === selectedSIMCard.id);
		this._contractTerm = this._bundles[e.target.dataset.index].contractTerm;
		this._quantity = 1;
		this.isAddProduct = false;
		this.isSimOnlyProduct = true;
	}

	/**
	 * handle delete product
	 * @param {*} e
	 */
	deleteProduct(e) {
		this._bundles.splice(e.target.dataset.index, 1);
		let index = e.target.dataset.index;
		this.publishMessage('product-bundle-remove', { index });
		// delete children without parent
		this.deleteOrphanBundles();
	}

	/**
	 * handle delete selected products
	 *
	 */
	deleteProducts() {
		let r = 0;
		this._bundles.forEach((a, i) => {
			if (a.checked) {
				this.publishMessage('product-bundle-remove', { index: i - r });
				r++;
			}
		});
		this._bundles = this._bundles.filter((a) => !a.checked);
		// delete children without parent
		this.deleteOrphanBundles();
	}
	/**
	 * deletes bundles that have orphan child products
	 *
	 */
	deleteOrphanBundles() {
		let unavailableProducts = this.getUnavailableProducts();
		let deleteIndexes = [];
		this._bundles.forEach((b, i) => {
			if (b.products.some((p) => unavailableProducts.has(p.id))) {
				deleteIndexes.push(i);
			}
		});
		if (deleteIndexes.length === 0) {
			return;
		}
		deleteIndexes.reverse().forEach((i) => {
			this._bundles.splice(i, 1);
			this.publishMessage('product-bundle-remove', { index: i });
		});
		this.deleteOrphanBundles();
	}
	/**
	 * handle confirm
	 * @param {*} e
	 */
	async handleConfirm(e) {
		let products = [
			{ id: this.mainProduct.Id, type: this.mainProduct.Type__c },
			{ id: e.detail.product.Id, type: e.detail.product.Type__c },
			{ id: e.detail.simCard.Id, type: e.detail.simCard.Type__c }
		];
		let bundle = await getBundle({ products, contractTerm: e.detail.contractTerm });
		let contractTerm = e.detail.contractTerm;
		let details = {};
		e.detail.product.Order_Entry_Product_Details__r.forEach((a) => {
			details[a.Name] = a.Value__c;
		});
		let bundleJSON = {
			bundle,
			contractTerm,
			simCard: this._simCardProducts.find((a) => a.Id === e.detail.simCard.Id)?.Name,
			addons: [],
			promos: [],
			products,
			details,
			telephony: {},
			operatorSwitch: {},
			installation: {},
			type: 'Mobile'
		};
		// owerwrite values like installation details and leave existing ones
		bundleJSON = Object.assign({}, this._bundles[this.index], bundleJSON);
		this._bundles.splice(this.index, 1, bundleJSON);
		this.closeModal();
		let data = {
			bundle: bundleJSON,
			index: this.index
		};
		this.publishMessage('product-bundle-change', data);
	}

	/**
	 * handle submint
	 * @param {*} e
	 */
	async handleSubmit(e) {
		let products = [
			{ id: this.mainProduct.Id, type: this.mainProduct.Type__c },
			{ id: e.detail.product.Id, type: e.detail.product.Type__c },
			{ id: e.detail.simCard.Id, type: e.detail.simCard.Type__c }
		];
		let bundle = await getBundle({ products, contractTerm: e.detail.contractTerm });

		let usedQuantity = e.detail.quantity ? parseInt(e.detail.quantity) : 0;
		if (e.detail.product.Max_Quantity__c) {
			this._bundles.forEach((element) => {
				if (element.products.find((prod) => prod.type === 'Mobile' && prod.id === e.detail.product.Id)) {
					usedQuantity += element.bundle.quantity;
				}
			});
		}
		if (!e.detail.product.Max_Quantity__c || (usedQuantity > 0 && usedQuantity <= e.detail.product.Max_Quantity__c)) {
			let contractTerm = e.detail.contractTerm;
			let details = {};
			e.detail.product.Order_Entry_Product_Details__r.forEach((a) => {
				details[a.Name] = a.Value__c;
			});
			let quantity = parseInt(e.detail.quantity, 10);
			let bundles = [];
			for (let i = 0; i < quantity; i++) {
				this._bundles.push({
					bundle: Object.assign({}, bundle),
					contractTerm,
					simCard: this._simCardProducts.find((a) => a.Id === e.detail.simCard.Id)?.Name,
					addons: [],
					promos: [],
					products: Object.assign([], products),
					details,
					telephony: {},
					operatorSwitch: {},
					installation: {},
					type: 'Mobile'
				});
				bundles.push({
					bundle: Object.assign({}, bundle),
					contractTerm,
					simCard: this._simCardProducts.find((a) => a.Id === e.detail.simCard.Id)?.Name,
					addons: [],
					promos: [],
					products: Object.assign([], products),
					details,
					telephony: {},
					operatorSwitch: {},
					installation: {},
					type: 'Mobile'
				});
			}
			this.closeModal();
			this.publishMessage('product-bundles-add', { bundles });
		} else {
			this.handleError('Max quantity for selected product is ' + e.detail.product.Max_Quantity__c);
		}
	}

	/**
	 * handle cancel
	 */
	handleCancel() {
		this.closeModal();
	}

	/**
	 * handle change checkbox on product
	 * @param {*} e
	 */
	handleChange(e) {
		let index = e.target.dataset.index;
		let bundle = Object.assign({}, this.bundles[index]);
		bundle.checked = !bundle.checked;
		this._bundles.splice(index, 1, bundle);
	}

	/**
	 * handle change checkbox on all products
	 */
	handleChangeAll() {
		let checked = this.bundles.every((a) => a.checked);
		this.bundles.forEach((a, i) => {
			let bundle = Object.assign({}, a);
			bundle.checked = !checked;
			this._bundles.splice(i, 1, bundle);
		});
	}

	publishMessage(event, data) {
		const msg = {
			sourceComponent: 'order-entry-mobile-products',
			payload: {
				event,
				data
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}

	handleError(error) {
		this.dispatchEvent(
			new ShowToastEvent({
				title: 'Something went wrong',
				message: error,
				variant: 'warning'
			})
		);
	}
}
