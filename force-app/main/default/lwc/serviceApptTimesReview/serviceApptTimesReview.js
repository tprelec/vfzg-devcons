import { api, LightningElement, wire } from 'lwc';
import HEADER_LABEL from '@salesforce/label/c.Scheduled_Times_Header';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import SERVICE_APPOINTMENT from '@salesforce/schema/ServiceAppointment';

export default class ServiceApptTimesReview extends LightningElement {
	get label() {
		return {
			sectionHeader: HEADER_LABEL,
			startTime: `${this.schedStartTimeLabel} (${this.timezone})`,
			endTime: `${this.schedEndTimeLabel} (${this.timezone})`
		};
	}

	@wire(getObjectInfo, { objectApiName: SERVICE_APPOINTMENT })
	buildLabels({ data, error }) {
		if (data) {
			this.schedStartTimeLabel = data.fields.SchedStartTime.label;
			this.schedEndTimeLabel = data.fields.SchedEndTime.label;
		} else if (error) {
			console.error(error);
		}
	}

	schedStartTimeLabel;
	schedEndTimeLabel;

	@api
	startDatetime;

	@api
	endDatetime;

	@api
	timezone;

	connectedCallback() {
		this.dispatchEvent(new CustomEvent('loadcomplete'));
	}
}
