import { LightningElement, track, api, wire } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getRecords from '@salesforce/apex/LookupController.getRecords';
import getRecord from '@salesforce/apex/LookupController.getRecord';
import getRecent from '@salesforce/apex/LookupController.getRecent';
import { getObjectInfo } from 'lightning/uiObjectInfoApi';
import { getRecordNotifyChange } from 'lightning/uiRecordApi';

import moreRecordsMessageLabel from '@salesforce/label/c.lookup_More_Records_Message';

/** The delay used when debouncing event handlers before invoking Apex. */
const DELAY = 300;

export default class Lookup extends NavigationMixin(LightningElement) {
	label = {
		moreRecordsMessageLabel
	};

	// public properties
	@api objectName;
	@api primaryField = 'Name';
	@api secondaryField;
	@api lookupLabel;
	@api lookupPlaceholder;
	@api disabled = false;
	@api filter;
	@api required = false;
	@api createNewFields;
	@api recordLimit;
	@api isOutputOnly = false;

	// reactive private properties
	@track searchKey = '';
	@track records;
	@track selectedRecordId = null;

	placeholderLabel = 'Search';
	themeInfo;
	isCreateNewFormLoading = false;
	showNoRecordsAvailableMessage = false;

	isSearching = false;
	showModal = false;
	objectLabel = 'Record';

	_isFocused = false;

	_defaultRecordId;
	@api
	get defaultRecordId() {
		return this._defaultRecordId;
	}
	set defaultRecordId(value) {
		this._defaultRecordId = value;
		if (value !== undefined) {
			this._populateLookup(value);
		}
	}

	@api
	setDefaultId(recordId) {
		this._populateLookup(recordId || null);
		this._showError('');
	}

	@api
	reportValidity() {
		this._showError('');
		if (this.required && !this.selectedRecordId && !this.disabled) {
			this._showError('This field is required.');
		}
	}

	@api checkValidity() {
		const searchInput = this.template.querySelector('.searchInput');
		if (!searchInput) {
			return true;
		}
		return this.required && searchInput.checkValidity();
	}

	get showDropDown() {
		return this._isFocused && (this.records?.length > 0 || this.showNoRecordsAvailableMessage);
	}

	get showMoreRecordsMessage() {
		return this._isFocused && this.records?.length > 0 && this.records?.length === (this.recordLimit || 10);
	}

	get moreRecordsMessage() {
		let moreRecordsMessage = this.label.moreRecordsMessageLabel.replaceAll('{Object}', this.lookupLabel);
		moreRecordsMessage = moreRecordsMessage.replace('{Limit}', this.recordLimit || 10);
		return moreRecordsMessage;
	}

	get comboBoxClass() {
		let cssClasses = ['slds-combobox', 'slds-dropdown-trigger', 'slds-dropdown-trigger_click'];
		if (this.showDropDown) {
			cssClasses.push('slds-is-open');
		}
		return cssClasses;
	}

	get iconColor() {
		return `background-color: #${this.themeInfo?.color};`;
	}

	@wire(getObjectInfo, { objectApiName: '$objectName' })
	handleResult({ error, data }) {
		if (data) {
			let objectInformation = data;
			if (this.lookupPlaceholder) {
				this.placeholderLabel = this.lookupPlaceholder;
			} else {
				this.placeholderLabel += ' ' + (objectInformation && objectInformation.labelPlural ? objectInformation.labelPlural : '');
			}
			this.objectLabel = objectInformation.label || this.lookupLabel;
			this.themeInfo = objectInformation.themeInfo || {};
		}
		if (error) {
			this._showError('You do not have the rights to object or object api name is invalid: ' + this.objectName);
			// eslint-disable-next-line @lwc/lwc/no-api-reassignments
			this.disabled = true;
		}
	}

	onInputFocus(event) {
		if (!this.disabled) {
			this._showError('');
			this._isFocused = event.type === 'focus';
			if (event.type === 'blur') {
				this._handleBlur();
			}
		}
	}

	_handleBlur() {
		this.reportValidity();
		if (!this.selectedRecordId) {
			this.records = [];
			if (this.searchKey) {
				this._showError('Select an option from the picklist or remove the search term.');
			}
		}
	}

	onInputClick() {
		if (!this.selectedRecordId && !this.disabled) {
			if (!this.searchKey) {
				this._showRecent();
			} else if (this.searchKey) {
				this._showSearchRecords();
			}
		}
	}

	onInputChange(event) {
		const searchKey = event.target.value;
		this.searchKey = searchKey;

		if (this.searchKey.length === 0) {
			this._showRecent();
		} else {
			// Debouncing this method: Do not update the reactive property as long as this function is
			// being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
			window.clearTimeout(this.delayTimeout);
			// eslint-disable-next-line @lwc/lwc/no-async-operation
			this.delayTimeout = setTimeout(() => {
				this._showSearchRecords();
			}, DELAY);
		}
	}

	onSelectItem(event) {
		this._setSelectedRecordId(event.currentTarget.dataset.recordId);
		this.searchKey = event.currentTarget.dataset.recordName;
		this.showNoRecordsAvailableMessage = false;
		this.records = [];
	}

	onClearLookup() {
		this.searchKey = '';
		this._setSelectedRecordId('');
		this._setFocusOnLookupInput();
		this.onInputClick();
	}

	_setFocusOnLookupInput() {
		// eslint-disable-next-line @lwc/lwc/no-async-operation
		setTimeout(() => {
			const searchInput = this.template.querySelector('.searchInput');
			searchInput.focus();
		}, 10);
	}

	async _showRecent() {
		this.isSearching = true;

		const lookupData = this._getSearchData();
		const result = await getRecent(lookupData);
		if (result.variant === 'error') {
			this._showError('Something went wrong: ' + result.message);
		}
		this.records = result.body;
		this.showNoRecordsAvailableMessage = this.records?.length === 0;
		this.isSearching = false;
	}

	async _showSearchRecords() {
		this.isSearching = true;
		const lookupData = this._getSearchData();
		const result = await getRecords(lookupData);
		this.records = result?.body || [];
		this.showNoRecordsAvailableMessage = this.records?.length === 0;
		if (result.variant === 'error') {
			this._showError('Something went wrong: ' + result.message);
		}
		this.isSearching = false;
	}

	_getSearchData(recordId) {
		return {
			data: {
				searchKey: this.searchKey,
				objectApiName: this.objectName,
				primaryField: this.primaryField,
				secondaryField: this.secondaryField || null,
				filter: this.filter || null,
				recordId: recordId || this.selectedRecordId || null,
				recordLimit: this.recordLimit || 10
			}
		};
	}

	_showError(message) {
		const searchInput = this.template.querySelector('.searchInput');
		if (searchInput) {
			searchInput.setCustomValidity(message);
			searchInput.reportValidity();
		}
	}

	_setSelectedRecordId(recordId) {
		if (this.selectedRecordId !== recordId) {
			this.selectedRecordId = recordId;

			this.dispatchEvent(
				new CustomEvent('selected', {
					detail: { recordId: this.selectedRecordId }
				})
			);
		}
	}

	/*  Start: create new dialog  */
	onNewRecordButtonClick() {
		this.isCreateNewFormLoading = true;
		this.showModal = true;
	}

	handleOnLoad() {
		this.isCreateNewFormLoading = false;
	}

	handleSubmit() {
		this.isCreateNewFormLoading = true;
	}

	async handleSuccess(event) {
		const recordId = event.detail.id;

		this._populateLookup(recordId);

		this.dispatchEvent(
			new ShowToastEvent({
				variant: 'success',
				message: `${this.objectLabel} was created.`
			})
		);
		getRecordNotifyChange([{ recordId: recordId }]);

		this.isCreateNewFormLoading = false;
		this.showModal = false;
	}

	async _populateLookup(recordId) {
		this._setSelectedRecordId(recordId);

		// if record id is null, set selected name as empty, otherwise get the name of the record
		let recordName = '';
		if (recordId) {
			const lookupData = this._getSearchData(recordId);
			const result = await getRecord(lookupData);
			recordName = result.body[0].primaryLabel ? result.body[0].primaryLabel : result.body[0].secondaryLabel;
		}
		this.searchKey = recordName || '';
	}

	handleError() {
		this.isCreateNewFormLoading = false;
	}

	closeCreateNewRecordDialog() {
		this.showModal = false;
		this._setFocusOnLookupInput();
	}

	/*  End: create new dialog  */
}
