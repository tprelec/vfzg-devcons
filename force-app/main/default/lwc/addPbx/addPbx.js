import { LightningElement, track, api } from 'lwc';
import getPbx from '@salesforce/apex/AddPbxController.getPbx';
import savePbx from '@salesforce/apex/AddPbxController.savePbx';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class AddPbx extends LightningElement {
	@api
	siteId;
	@api
	accountId;
	@track
	showSpinner;

	@track
	pbxTypes = [];

	@track newCompetitorAsset = {
		Account__c: '',
		Site__c: '',
		PBX_type__c: '',
		Maintenance_company__c: '',
		Maintenance_contact__c: '',
		Maintenance_contact_phone__c: ''
	};
	productNameOptions = [];
	softwareVersionOptions = [];
	@track pbx = {
		Id: '',
		Vendor__c: '',
		Product_Name__c: '',
		Software_version__c: '',
		Office_Voice_approved__c: '',
		Name: '',
		SIP_Certification__c: '',
		CNoIP_approved__c: '',
		VF_CPE__c: '',
		Remarks__c: ''
	};

	set siteId(value) {
		this.siteId = value;
	}

	get vendorOptions() {
		return [
			{ label: '3CX', value: '3CX' },
			{ label: '@COM', value: '@COM' },
			{ label: 'Aastra', value: 'Aastra' },
			{ label: 'Alcatel-Lucent', value: 'Alcatel-Lucent' },
			{ label: 'Asterisk', value: 'Asterisk' },
			{ label: 'AudioCodes', value: 'AudioCodes' },
			{ label: 'Avaya', value: 'Avaya' },
			{ label: 'Cisco', value: 'Cisco' },
			{ label: 'Digium', value: 'Digium' },
			{ label: 'Escaux', value: 'Escaux' },
			{ label: 'GrandStream', value: 'GrandStream' },
			{ label: 'Huawei', value: 'Huawei' },
			{ label: 'Imtech', value: 'Imtech' },
			{ label: 'Innovaphone', value: 'Innovaphone' },
			{ label: 'Interactive Intelligence', value: 'Interactive Intelligence' },
			{ label: 'Microsoft', value: 'Microsoft' },
			{ label: 'Mitel', value: 'Mitel' },
			{ label: 'NEC-Philips', value: 'NEC-Philips' },
			{ label: 'NeoNova', value: 'NeoNova' },
			{ label: 'OneAccess', value: 'OneAccess' },
			{ label: 'PEXIP', value: 'PEXIP' },
			{ label: 'Panasonic', value: 'Panasonic' },
			{ label: 'Samsung', value: 'Samsung' },
			{ label: 'Siemens / Unify', value: 'Siemens / Unify' },
			{ label: 'Swyx', value: 'Swyx' },
			{ label: 'Telios', value: 'Telios' },
			{ label: 'Trixbox', value: 'Trixbox' },
			{ label: 'Unexus', value: 'Unexus' },
			{ label: 'Vertical', value: 'Vertical' },
			{ label: 'Xelion', value: 'Xelion' }
		];
	}
	// Setting the selected PBX Type object Based on what Vendor, Product and Software version is selected
	setPbxValues() {
		let filteredPbx = this.pbxTypes.filter(
			(record) =>
				record.Vendor__c == this.pbx.Vendor__c &&
				record.Product_Name__c == this.pbx.Product_Name__c &&
				record.Software_version__c == this.pbx.Software_version__c
		);
		this.pbx = JSON.parse(JSON.stringify(filteredPbx[0]));
		this.newCompetitorAsset.PBX_type__c = this.pbx.Id;
	}

	async connectedCallback() {
		this.pbx.Vendor__c = '3CX';
		await this.getPbxTypes();
		this.calculatePicklistOptions();
		this.setPbxValues();
	}
	// Initial Fetching of all Pbx Types
	async getPbxTypes() {
		this.showSpinner = true;
		await getPbx()
			.then((result) => {
				this.showSpinner = false;
				this.pbxTypes = JSON.parse(JSON.stringify(result));
			})
			.catch((error) => {
				this.showSpinner = false;
				this.handleError(error);
			});
	}
	// Calculating picklist options based on what Vendor and then Product are selected
	calculatePicklistOptions() {
		let filteredProducts = this.pbxTypes.filter((record) => record.Vendor__c == this.pbx.Vendor__c);
		let optionList = [];
		let tempList = [];
		filteredProducts.forEach((element) => {
			optionList.push({ label: element.Product_Name__c, value: element.Product_Name__c });
			tempList.push(element.Product_Name__c);
		});
		if (!tempList.includes(this.pbx.Product_Name__c)) {
			this.pbx.Product_Name__c = optionList[0].value;
		}
		this.productNameOptions = optionList;

		filteredProducts = this.pbxTypes.filter(
			(record) => record.Vendor__c == this.pbx.Vendor__c && record.Product_Name__c == this.pbx.Product_Name__c
		);
		optionList = [];
		tempList = [];
		filteredProducts.forEach((element) => {
			optionList.push({ label: element.Software_version__c, value: element.Software_version__c });
			tempList.push(element.Software_version__c);
		});

		if (!tempList.includes(this.pbx.Software_version__c)) {
			this.pbx.Software_version__c = optionList[0].value;
		}
		this.softwareVersionOptions = optionList;

		this.setPbxValues();
	}

	handlePicklistChange(event) {
		this.showSpinner = true;
		this.pbx[event.target.name] = event.detail.value;
		this.calculatePicklistOptions();

		this.showSpinner = false;
	}

	handleMaintenanceChange(event) {
		this.newCompetitorAsset[event.target.name] = event.detail.value;
	}

	closeModal(event) {
		this.dispatchEvent(
			new CustomEvent('closemodal', {
				detail: { sourceComponent: 'addPbx' }
			})
		);
	}

	handleSave() {
		this.showSpinner = true;
		this.newCompetitorAsset.Account__c = this.accountId;
		this.newCompetitorAsset.Site__c = this.siteId;

		savePbx({ pbx: this.newCompetitorAsset })
			.then((result) => {
				this.showToast('SUCCESS', 'PBX added successfully.', 'Success');
				this.showSpinner = false;
				this.closeModal();
			})
			.catch((error) => {
				this.showSpinner = false;
				this.handleError(error);
			});
	}

	showToast(title, message, variant) {
		const event = new ShowToastEvent({
			title,
			variant,
			message
		});
		this.dispatchEvent(event);
	}
}
