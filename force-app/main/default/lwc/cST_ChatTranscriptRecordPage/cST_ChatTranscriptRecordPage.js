import { LightningElement, api, wire } from 'lwc';
import { getRecord, getFieldValue } from 'lightning/uiRecordApi';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import CASE_FIELD from '@salesforce/schema/LiveChatTranscript.CaseId';

const fields = [CASE_FIELD];

export default class CST_ChatTranscriptRecordPage extends LightningElement {
	@api recordId;
	//objectApiName = 'LiveChatTranscript';
	//modeName="Edit";
	//uiRecordView;
	//uiRecordEdit;
	//caseId;

	//@wire(getRecord, { recordId: '$recordId', layoutTypes: ['Full'], modes: [modeName] })
	//chatRecord;

	/*@wire(getRecordUi, {
        recordIds: "$recordId",
        layoutTypes: "Full",
        modes: "View",
      })
      wiredRecord({ error, data }) {
        if (data) {
          this.caseId = data.records[this.recordId].fields["CaseId"].value;
        } else{
          // TODO: Data handling
        }
      }*/

	@wire(getRecord, { recordId: '$recordId', fields })
	liveChatTranscript;

	get caseId() {
		return getFieldValue(this.liveChatTranscript.data, CASE_FIELD);
	}

	handleSuccess(event) {
		console.log('onsuccess event recordEditForm', event.detail.id);
		this.dispatchEvent(
			new ShowToastEvent({
				title: 'Success',
				message: 'Case was updated successfully.',
				variant: 'success'
			})
		);
	}

	handleSubmit(event) {
		console.log('onsubmit event recordEditForm', event.detail.id);
		const scrollOptions = {
			left: 0,
			top: 0,
			behavior: 'smooth'
		};
		window.scrollTo(scrollOptions);
	}

	/*showSuccessToast(){
        const event = new ShowToastEvent({
            title: 'Success!',
            message:
                'Case was updated successfully.',
        });
        this.dispatchEvent(event);
    }*/

	/*@wire(getRecordUi, {
        recordIds: "$caseId",
        layoutTypes: "Full",
        modes: "View",
    })
    wiredRecordView({ error, data }) {
        console.log('data -> '+data)
        if (data) {
            for (let layout of Object.values(data.layouts.Case)) {
                this.uiRecordView = layout.Full.View;
                break;
            }
        }
        else {
            // TODO: Data handling
        }
    }

    @wire(getRecordUi, {
        recordIds: "$caseId",
        layoutTypes: "Full",
        modes: "Edit",
    })
    wiredRecordEdit({ error, data }) {
        if (data) {
            for (let layout of Object.values(data.layouts.Case)) {
                this.uiRecordEdit = layout.Full.Edit;
                break;
            }
        }
        else {
            // TODO: Data handling
        }
    }*/
}
