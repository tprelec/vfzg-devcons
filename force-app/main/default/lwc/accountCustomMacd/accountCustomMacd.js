import { LightningElement, api, track, wire  } from 'lwc';

export default class AccountCustomMacd extends LightningElement {
    activeSectionsMessage = '';

    @track
    solutions = [
        {
            Id : '1',
            Name : 'Amsterdam, Keizersgracht 181',
            visible: 'visible',
            Subscriptions : [
                {
                    Name : 'Coax aansluiting 1000/50 Mbps',
                    Id : 'S1'
                },
                {
                    Name : 'Service fee 500/500 Mbps',
                    Id : 'S2'
                },
                {
                    Name : 'Internet toegang Coax 12 months',
                    Id : 'S3'
                }
            ]
        },
        {
            Id : '2',
            Name : 'Amsterdam, Raadhuisstraat 32',
            visible: 'visible',
            Subscriptions : [
                {
                    Name : 'Coax aansluiting 500/50 Mbps',
                    Id : 'S1'
                },
                {
                    Name : 'Service fee 500/500 Mbps',
                    Id : 'S2'
                },
                {
                    Name : 'Internet toegang Coax 36 months',
                    Id : 'S3'
                }
            ]
        },
        {
            Id : '3',
            Name : 'Amsterdam, Elandsgracht 134h, 1016 VC',
            visible: 'visible',
            Subscriptions : [
                {
                    Name : 'Coax aansluiting 100/50 Mbps',
                    Id : 'S1'
                },
                {
                    Name : 'Service fee 100/500 Mbps',
                    Id : 'S2'
                },
                {
                    Name : 'Internet toegang Coax 24 months',
                    Id : 'S3'
                }
            ]
        },
        {
            Id : '4',
            Name : 'Rotterdam, Englandergracht 194a, 2016 VX',
            visible: 'visible',
            Subscriptions : [
                {
                    Name : 'Coax aansluiting 100/50 Mbps',
                    Id : 'S1'
                },
                {
                    Name : 'Service fee 100/500 Mbps',
                    Id : 'S2'
                },
                {
                    Name : 'Internet toegang Coax 24 months',
                    Id : 'S3'
                }
            ]
        }
    ];

    nositesolutions = [
        {
            Name: 'Mobile Plan - ONE Mobile 4.0 data only - 1m',
            Id: 'SWARWEHWHWEMUF1',
            checked: false
        },
        {
            Name: 'Mobile Plan - ONE Mobile 4.0 data only - 1m',
            Id: 'SERHSERJTDFSFAD',
            checked: false
        },
        {
            Name: 'Mobile Plan - ONE Mobile 4.0 voice & data - 36m',
            Id: 'DSFHSDHSDJSSDFS',
            checked: false
        },
        {
            Name: 'Mobile Plan - ONE Mobile 4.0 data only - 36m',
            Id: 'UKMZZGGZUZFDGUM',
            checked: false
        }
    ];

    _recordId;
    
    @api
    get recordId() {
        return this._recordId;
    }
    set recordId(value) {
        if (this._recordId !== value) {
            this._recordId = value;
            console.log("Setting recordId to " + this._recordId);
        }
    }

    connectedCallback() {
        console.log("recordId", this.recordId);
    }

    handleSectionToggle(event) {
        const openSections = event.detail.openSections;

        if (openSections.length === 0) {
            this.activeSectionsMessage = 'All sections are closed';
        } else {
            this.activeSectionsMessage =
                'Open sections: ' + openSections.join(', ');
        }
    }

    filterdata(event) {

        const v = event.target.value.toLowerCase();

        for (let i = 0; i < this.solutions.length; i++) {
            this.solutions[i].Name.toLowerCase().indexOf(v) > -1 ? this.solutions[i].visible = "visible" : this.solutions[i].visible = "invisible"
        }
    }
}