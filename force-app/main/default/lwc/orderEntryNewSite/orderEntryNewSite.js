import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveSite from '@salesforce/apex/OrderEntrySiteController.saveSite';
import oblicoSearch from '@salesforce/apex/OrderEntrySiteController.oblicoSearch';

export default class OrderEntryNewSite extends LightningElement {
	@api accountId;
	@track error;
	@track loading = false;
	@track newSite = {};
	@track searchZip = '';
	@track returnedAddresses = [];
	@track selectedAddress;
	@track dropdownDisabled = true;

	get options() {
		return this.returnedAddresses.map((item) => {
			let label = item.label;
			let value = item.value;

			return {
				label,
				value
			};
		});
	}

	handleFormInputChange(event) {
		this.searchZip = event.target.value;
	}

	closeModal() {
		this.dispatchEvent(new CustomEvent('closemodal', { detail: {}, bubbles: false }));
	}

	/**
	 * search address using olbico search
	 */
	addressSearch() {
		this.loading = true;
		oblicoSearch({ zip: this.searchZip })
			.then((result) => {
				this.loading = false;
				if (result) {
					this.dropdownDisabled = false;
					for (let propName in result) {
						this.returnedAddresses.push({
							value: result[propName],
							label: propName
						});
					}
				}
			})
			.catch((error) => {
				this.loading = false;
				console.error('Error', error);
			});
	}

	handleSelected(event) {
		this.selectedAddress = event.detail.value;
	}

	/**
	 * handle save
	 */
	handleSave() {
		this.loading = true;
		if (this.selectedAddress !== undefined) {
			saveSite({ selectedAddress: this.selectedAddress, accId: this.accountId })
				.then((result) => {
					this.loading = false;
					if (result) {
						this.newSite = {
							Id: result.Id,
							Site_Street__c: result.Site_Street__c,
							Site_Postal_Code__c: result.Site_Postal_Code__c,
							Site_House_Number__c: result.Site_House_Number__c,
							Site_City__c: result.Site_City__c,
							Site_House_Number_Suffix__c: result.Site_House_Number_Suffix__c
						};
						this.dispatchEvent(new CustomEvent('closemodal', { detail: this.newSite, bubbles: false }));
					}
				})
				.catch((error) => {
					this.loading = false;
					console.error('Error', error);
					this.dispatchEvent(
						new ShowToastEvent({
							title: 'Site already exists.',
							message: 'Selected site is already linked to the Account.',
							variant: 'warning',
							mode: 'dismissable'
						})
					);
				});
		} else {
			this.loading = false;
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Something went wrong',
					message: 'No site selected',
					variant: 'warning'
				})
			);
		}
	}
}
