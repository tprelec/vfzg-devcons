import { api, LightningElement, track, wire } from 'lwc';
import getAddons from '@salesforce/apex/OrderEntryProductController.getAddons';
import getAddonPrice from '@salesforce/apex/OrderEntryProductController.getAddonPrice';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();

export default class OrderEntryProductInternet extends LightningElement {
	@wire(MessageContext)
	messageContext;
	@api opportunityId;
	@track isRendered = true;
	@track _contractTerm;
	@track _selectedProduct = {};
	@track _products = [];
	@track _selectedAddons = [];
	@track selectedProductItem = {};
	@track selectedAddonsList = [];
	@track addons = [];

	@api
	get products() {
		return this._products;
	}
	set products(value) {
		if (JSON.stringify(this._products) !== JSON.stringify(value)) {
			this._products = Object.assign([], value);
		}
	}
	@api
	get contractTerm() {
		return this._contractTerm;
	}
	set contractTerm(value) {
		if (this._contractTerm !== value) {
			this._contractTerm = value;
		}
	}
	@api
	get selectedProduct() {
		return this._selectedProduct;
	}
	set selectedProduct(value) {
		if (JSON.stringify(this._selectedProduct) !== JSON.stringify(value)) {
			this._selectedProduct = Object.assign({}, value);
		}
	}
	@api
	get selectedAddons() {
		return this._selectedAddons;
	}
	set selectedAddons(value) {
		if (JSON.stringify(this._selectedAddons) !== JSON.stringify(value)) {
			this._selectedAddons = Object.assign([], value);
			this.getAddons();
		} else if (!this._selectedAddons.length) {
			this.getAutomaticAddons('selectedAddons');
		}
	}

	get addonsShow() {
		return this.addons.length > 0;
	}

	/**
	 * get addons
	 */
	async getAddons() {
		this.addons = await getAddons({
			oppId: this.opportunityId,
			parentProduct: this.selectedProduct.id
		});
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */
	@api validate() {
		const internetAdditionalForm = this.addonsShow ? this.template.querySelector('c-order-entry-internet-additional-form').validate() : true;
		const productInput = this.template.querySelector('c-order-entry-internet-product-form').validate();

		return internetAdditionalForm && productInput;
	}

	/**
	 * get automatic addons
	 */
	async getAutomaticAddons() {
		this.addons = await getAddons({
			oppId: this.opportunityId,
			parentProduct: this.selectedProduct.id
		});

		let automaticAddons = this.addons.filter((el) => el.Automatic_Add_On__c === true);
		let buildedAddOns = [];
		for (let i = 0; i < automaticAddons.length; i++) {
			const addon = Object.assign({}, automaticAddons[i]);

			buildedAddOns.push(
				await this.buildAddonData({
					type: addon.Add_On__r.Type__c,
					id: addon.Add_On__r.Id,
					quantity: 1
				})
			);
		}

		let tempAddons = Object.assign([], this._selectedAddons);
		buildedAddOns.forEach(async (element) => {
			if (element) {
				const addons = this.selectedAddons;
				let addonIndex = -1;
				if (element.type === CONSTANTS.INTERNET_ADDITIONAL_TYPE) {
					addonIndex = addons.findIndex((a) => a.id === element.id);
				} else {
					addonIndex = addons.findIndex((a) => a.type === element.type);
				}

				if (addonIndex === -1 && element.quantity > 0) {
					tempAddons.push(element);
				} else if (addonIndex !== -1 && element.quantity > 0) {
					tempAddons[addonIndex] = element;
				} else if (addonIndex !== -1 && element.quantity <= 0) {
					tempAddons.splice(addonIndex, 1);
				}
			}
		});
		this._selectedAddons = Object.assign([], tempAddons);
	}
	/**
	 * build addon data
	 * @param {*} param0
	 * @returns
	 */
	buildAddonData({ id, quantity, type }) {
		const addon = Object.assign(
			{},
			this.addons.find((a) => a.Add_On__r.Id === id)
		);
		if (addon) {
			// eslint-disable-next-line no-return-await
			return getAddonPrice({ code: addon.Add_On__r.Product_Code__c }).then((price) => {
				const recurring =
					addon.Recurring_Price_Override__c || addon.Recurring_Price_Override__c === 0
						? addon.Recurring_Price_Override__c
						: price.cspmb__Recurring_Charge__c;

				const oneOff =
					addon.One_Off_Price_Override__c || addon.One_Off_Price_Override__c === 0
						? addon.One_Off_Price_Override__c
						: price.cspmb__One_Off_Charge__c;
				return {
					id,
					quantity,
					type,
					name: addon.Add_On__r.Name,
					parentProduct: addon.Product__c,
					code: addon.Add_On__r.Product_Code__c,
					recurring: recurring,
					oneOff: oneOff,
					totalRecurring: parseFloat((recurring * quantity).toFixed(2)),
					totalOneOff: parseFloat((oneOff * quantity).toFixed(2)),
					bundleName: price.Product_Name__c,
					parentType: CONSTANTS.INTERNET_PRODUCT_TYPE
				};
			});
		}
		return null;
	}

	/**
	 * set addon
	 * @param {*} addonData
	 */
	setAddon(addonData) {
		let addon = Object.assign({}, addonData);

		if (addon) {
			const addons = this.selectedAddons;
			let addonIndex = -1;
			if (addon.type === CONSTANTS.INTERNET_ADDITIONAL_TYPE) {
				addonIndex = addons.findIndex((a) => a.id === addon.id);
			} else {
				addonIndex = addons.findIndex((a) => a.type === addon.type);
			}

			if (addonIndex === -1 && addon.quantity > 0) {
				this._selectedAddons.push(addon);
			} else if (addonIndex !== -1 && addon.quantity > 0) {
				this._selectedAddons[addonIndex] = addon;
			} else if (addonIndex !== -1 && addon.quantity <= 0) {
				this._selectedAddons.splice(addonIndex, 1);
			}
		}
	}

	/**
	 * handle product change
	 * @param {*} event
	 */
	async handleProductChange(event) {
		this._selectedProduct = { id: event.detail.Id, type: CONSTANTS.INTERNET_PRODUCT_TYPE };
		this.selectedProductItem = event.detail;

		await this.buildAddons();
		this.publishProductChanged();
	}

	/**
	 * handle contract term change
	 * @param {*} event
	 */
	handleContractTermChange(event) {
		this._contractTerm = event.detail;
		this.publishProductChanged();
	}

	/**
	 * handle change addon
	 * @param {*} e
	 */
	async handleChangedAddon(e) {
		await this.buildAddonData(e.detail).then((result) => {
			this.setAddon(result);
		});
		this.publishProductChanged();
	}

	/**
	 * handle pinzekre change
	 * @param {*} e
	 */
	async handlePinZekerChange(e) {
		if (this.selectedAddons.find((el) => el.type === CONSTANTS.PIN_ZEKER_TYPE)) {
			this._selectedAddons.splice(this.selectedAddons.indexOf(this.selectedAddons.find((el) => el.type === CONSTANTS.PIN_ZEKER_TYPE)), 1);
		} else {
			await this.buildAddonData(e.detail).then((result) => {
				this.setAddon(result);
			});
		}
		this.publishProductChanged();
	}

	/**
	 * handle internet addon change
	 * @param {*} e
	 */
	async handleInternetAddonChange(e) {
		if (e.detail.quantity === 0) {
			this._selectedAddons.splice(this.selectedAddons.indexOf(this.selectedAddons.find((el) => el.id === e.detail.id)), 1);
		} else {
			await this.buildAddonData(e.detail).then((result) => {
				this.setAddon(result);
			});
		}
		this.publishProductChanged();
	}

	/**
	 * after rendered publish initial value
	 */
	async handleRenderedChange() {
		if (
			this._selectedProduct &&
			this.selectedProduct.id &&
			this.selectedAddons.length !== 0 &&
			this.products.find((el) => el.Id === this.selectedProduct.id)
		) {
			this.publishProductChanged('rendered-changed');
		} else if (!this.selectedAddons.length) {
			await this.getAutomaticAddons();
			this.publishProductChanged();
		}
	}

	/**
	 * build addons
	 */
	async buildAddons() {
		this.getAutomaticAddons();

		// rebuild list of selected addons and remove ones that are not related to selected internet product
		const stateAddons = this.selectedAddons || [];
		const otherAddons = stateAddons.filter((a) => a.parentType !== CONSTANTS.INTERNET_PRODUCT_TYPE);
		const internetAddons = stateAddons.filter(
			(a) => a.parentType === CONSTANTS.INTERNET_PRODUCT_TYPE && a.parentProduct === this.selectedProduct.id
		);
		this._selectedAddons = [...otherAddons, ...internetAddons];
	}
	/**
	 * publish generic message
	 */
	publishProductChanged(eventName) {
		const msg = {
			sourceComponent: 'order-entry-internet-products',
			payload: {
				event: eventName ? eventName : 'product-changed',
				data: {
					product: this.products.find((el) => el.Id === this.selectedProduct.id),
					addons: this.selectedAddons,
					contractTerm: this.contractTerm
				}
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}
}
