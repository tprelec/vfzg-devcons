import { LightningElement, api } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

import handleClick from '@salesforce/apex/IJourneyHandler.handleClick';

import LABEL_SUCCESS from '@salesforce/label/c.pp_Success';
import LABEL_OPPORTUNITY_CREATED from '@salesforce/label/c.pp_Opportunity_Created';

export default class Pp_journeyButton extends NavigationMixin(LightningElement) {
	@api bppJourney;
	@api accountId;

	disableButton;
	@api
	get disabledComponent() {
		return this.disableButton;
	}
	set disabledComponent(value) {
		this.disableButton = value;
	}

	async bppJourneyClicked() {
		if (this.bppJourney.handlerName) {
			this.dispatchCustomEvent('parentloading', true);

			await handleClick({
				handlerName: this.bppJourney.handlerName,
				accountId: this.accountId
			})
				.then((result) => {
					if (this.bppJourney.handlerName === 'AddMobileProductsJourneyHandler') {
						let parameters = [{ name: 'isOpenJourney', value: 'true' }];
						this.opportunityNotifyNavigate(result, parameters);
					} else if (this.bppJourney.handlerName === 'ManualOpportunityJourneyHandler') {
						let parameters = [{ name: 'editMode', value: 'true' }];
						this.opportunityNotifyNavigate(result, parameters);
					}
				})
				.catch((error) => {
					console.error('Error:', error);
				})
				.finally(() => {
					this.dispatchCustomEvent('parentloading', false);
				});
		}

		this.dispatchCustomEvent('gotostepjourney', { nextStep: this.bppJourney.nextStep });
	}

	dispatchCustomEvent(customEventName, detailData) {
		const selectedEvent = new CustomEvent(customEventName, {
			bubbles: true,
			composed: true,
			cancelable: true,
			detail: detailData
		});
		this.dispatchEvent(selectedEvent);
	}

	opportunityNotifyNavigate(result, parameters) {
		this.showToastMessage(LABEL_SUCCESS, LABEL_OPPORTUNITY_CREATED, 'success');
		let opportunityId = Object.keys(JSON.parse(JSON.stringify(result)))[0];
		this.navigateToOpportunity(opportunityId, parameters);
	}

	navigateToOpportunity(opportunityId, parameters) {
		let parametersForUrl = '';
		for (const parameter of parameters) {
			parametersForUrl += '&' + parameter.name + '=' + parameter.value;
		}

		this[NavigationMixin.Navigate](
			{
				type: 'standard__webPage',
				attributes: {
					url: '/journey-path?opportunityId=' + opportunityId + parametersForUrl
				}
			},
			true
		);
	}

	showToastMessage(title, message, variant) {
		const evt = new ShowToastEvent({
			title: title,
			message: message,
			variant: variant
		});
		this.dispatchEvent(evt);
	}
}
