import { LightningElement, api, track, wire } from 'lwc';
import { loadStyle } from 'lightning/platformResourceLoader';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getSites from '@salesforce/apex/OrderEntrySiteController.getSites';
import updateSite from '@salesforce/apex/OrderEntrySiteController.updateSite';
import orderEntryAssets from '@salesforce/resourceUrl/orderEntryAssets';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

export default class OrderEntryInstallationAddress extends LightningElement {
	@api siteId;
	@api accountId;
	@api opportunityId;
	@api journeyType;

	@track loading = false;
	@track openSelectionModal = false;
	@track openCreationModal = false;
	@track sites = [];
	@track site = {
		Id: '',
		Site_Street__c: '',
		Site_Postal_Code__c: '',
		Site_House_Number__c: '',
		Site_City__c: '',
		Site_House_Number_Suffix__c: ''
	};

	get title() {
		return this.journeyType === 'Fixed' ? 'Installation Address' : 'Delivery Address';
	}

	@wire(MessageContext)
	messageContext;

	/**
	 * Executed when component rendered
	 */
	async renderedCallback() {
		await Promise.all([loadStyle(this, orderEntryAssets + '/orderEntryInstallationAddress.css')]);
	}

	/**
	 * Executed when component loaded
	 */
	connectedCallback() {
		this.getAccountSites().then((res) => {
			this.getSite();
		});
	}

	/**
	 * Gets all Contacts for Provided Account
	 */
	getAccountSites() {
		return getSites({ accId: this.accountId })
			.then((result) => {
				if (this.sites.length === 0) {
					this.sites = result;
				}
			})
			.catch((error) => {
				this.handleError(error);
			});
	}

	/**
	 * Gets Site
	 */
	getSite() {
		if (this.sites && this.siteId) {
			this.site = this.sites.filter((site) => {
				return site.Id === this.siteId;
			})[0];
		}
	}

	/**
	 * Handles input changes
	 * @param {object} event Event
	 */
	handleFormInputChange(event) {
		this.site[event.target.name] = event.target.value;
	}

	/**
	 * Opens modal for Site selection
	 */
	selectSite() {
		this.openSelectionModal = true;
	}

	/**
	 * Opens modal for new Site
	 */
	newSite() {
		this.openCreationModal = true;
	}

	/**
	 * Closes Modal(s)
	 * @param {object} event Event
	 */
	closeModal(event) {
		const isNew = this.openCreationModal;
		this.openCreationModal = false;
		this.openSelectionModal = false;
		if (event.detail.Id) {
			this.site = {
				Id: event.detail.Id,
				Site_Street__c: event.detail.Site_Street__c,
				Site_Postal_Code__c: event.detail.Site_Postal_Code__c,
				Site_House_Number__c: event.detail.Site_House_Number__c,
				Site_City__c: event.detail.Site_City__c,
				Site_House_Number_Suffix__c: event.detail.Site_House_Number_Suffix__c
			};

			if (isNew) {
				let tempSites = [...this.sites];
				tempSites.push(this.site);
				this.sites = tempSites;
			}

			// Publish
			const msg = {
				sourceComponent: 'order-entry-installation-address',
				payload: {
					event: 'site-selected',
					data: {
						siteId: this.site.Id
					}
				}
			};
			publish(this.messageContext, genericChannel, msg);
		}
	}

	@api validate() {
		return updateSite({
			updatedSite: this.site,
			opportunityId: this.opportunityId
		})
			.then((result) => {
				if (result) {
					return true;
				}
			})
			.catch((error) => {
				this.handleError(error.body.message);
				return false;
			});
	}

	handleError(error) {
		this.dispatchEvent(
			new ShowToastEvent({
				title: 'Something went wrong',
				message: error,
				variant: 'warning'
			})
		);
	}
}
