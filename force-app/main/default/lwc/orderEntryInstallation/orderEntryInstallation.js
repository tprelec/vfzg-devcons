import { LightningElement, api, track } from 'lwc';

export default class OrderEntryInstallation extends LightningElement {
	@api installationSite;
	@api bundles;
	@api notes;
	@api b2cInternetCustomer;
	@api opportunityId;

	@api
	get fixedBundle() {
		return this.bundles?.find((bundle) => bundle.type === 'Fixed');
	}

	@api journeyType;
	get isMobile() {
		return this.journeyType === 'Mobile';
	}

	@api
	get installation() {
		return this.fixedBundle?.installation;
	}
	@api
	get operatorSwitch() {
		return this.fixedBundle?.operatorSwitch;
	}

	@api
	get mobileBundles() {
		return this.bundles.filter((bundle) => {
			return bundle.type === 'Mobile';
		});
	}

	get addressCheckResult() {
		return this.installationSite?.addressCheckResult;
	}
	/**
	 * description checking input data
	 * @returns boolean value
	 */
	@api
	validate() {
		const validation = this.isMobile
			? this.template.querySelector('c-order-entry-installation-mobile')
			: this.template.querySelector('c-order-entry-installation-fixed');
		return validation.validate();
	}

	/**
	 * description save data
	 */
	@api
	async saveData() {
		if (!this.isMobile) {
			const fixedChild = this.template.querySelector('c-order-entry-installation-fixed');
			if (fixedChild) {
				await fixedChild.saveData();
			}
		} else {
			const mobileChild = this.template.querySelector('c-order-entry-installation-mobile');
			if (mobileChild) {
				await mobileChild.saveData();
			}
		}
	}
}
