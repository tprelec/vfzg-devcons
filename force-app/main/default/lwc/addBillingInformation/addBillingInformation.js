import { LightningElement, api, wire } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
// import getData from "@salesforce/apex/AddBillingInformationController.getData";

import BASKET_OBJECT from "@salesforce/schema/cscfga__Product_Basket__c";
import ORDER_OBJECT from "@salesforce/schema/csord__Order__c";

import { getRecord } from "lightning/uiRecordApi";

const BASKET_FIELDS = [
    "cscfga__Product_Basket__c.csbb__Account__c",
    "cscfga__Product_Basket__c.BAN_Information__c",
    "cscfga__Product_Basket__c.BAN_Information__r.Name",
    "cscfga__Product_Basket__c.Financial_Account__c",
    "cscfga__Product_Basket__c.Financial_Account__r.Name",
    "cscfga__Product_Basket__c.Billing_Arrangement__c",
    "cscfga__Product_Basket__c.Billing_Arrangement__r.Name"
];

const ORDER_FIELDS = [
    "csord__Order__c.csbb__Account__c",
    "csord__Order__c.BAN_Information__c",
    "csord__Order__c.BAN_Information__r.Name",
    "csord__Order__c.Financial_Account__c",
    "csord__Order__c.Financial_Account__r.Name",
    "csord__Order__c.Billing_Arrangement__c",
    "csord__Order__c.Billing_Arrangement__r.Name"
];

import save from "@salesforce/apex/AddBillingInformationController.save";

export default class AddBillingInformation extends LightningElement {
    @api recordId;

    @api objectApiName;

    isLoading = true;

    accountId;
    banId;
    banName;
    financialAccountId;
    financialAccountName;
    billingArrangementId;
    billingArrangementName;

    get billingInformationFields() {
        // console.log(this.objectApiName, this.id);
        if (this.objectApiName === BASKET_OBJECT.objectApiName) {
            return BASKET_FIELDS;
        }

        if (this.objectApiName === ORDER_OBJECT.objectApiName) {
            return ORDER_FIELDS;
        }

        return [];
    }

    @wire(getRecord, {
        recordId: "$recordId",
        fields: "$billingInformationFields"
    })
    billingInformationHandler({ error, data }) {
        this.isLoading = true;
        // console.log("recordId", this.id);
        // console.log("objectApiName", this.objectApiName);
        // console.log("billingInformationFields", this.billingInformationFields);
        if (data) {
            // console.log("data", JSON.stringify(data));
            this.accountId = data.fields.csbb__Account__c.value || "";
            this.banId = data.fields.BAN_Information__c.value || "";
            this.banName = data.fields.BAN_Information__r?.displayValue || "";
            this.financialAccountId =
                data.fields.Financial_Account__c.value || "";
            this.financialAccountName =
                data.fields.Financial_Account__r?.displayValue || "";
            this.billingArrangementId =
                data.fields.Billing_Arrangement__c.value || "";
            this.billingArrangementName =
                data.fields.Billing_Arrangement__r?.displayValue || "";

            this.isLoading = false;
        } else if (error) {
            console.log("error", JSON.stringify(error));
            this._showToast(
                "error",
                "Something went wrong with loading the data!"
            );
            this.isLoading = false;
        }
    }

    async saveBillingInformation() {
        this.isLoading = true;
        const data = {
            saveData: {
                recordId: this.recordId,
                banId: this.banId || null,
                financialAccountId: this.financialAccountId || null,
                billingArrangementId: this.billingArrangementId || null
            }
        };
        console.log("data", data);
        const result = await save(data);
        if (result.variant && result.message) {
            this._showToast(result.variant, result.message);
        }
        this.isLoading = false;
    }

    onBanUpdate(event) {
        this.banId = event.detail.record.id;
        if (!event.detail.record.id) {
            this.template.querySelector("c-lookup.financial-account").setDefaultId("");
        }
    }

    onFinancialAccountUpdate(event) {
        this.financialAccountId = event.detail.record.id;
        if (!event.detail.record.id) {
            this.template.querySelector("c-lookup.billing-arrangement").setDefaultId("");
        }
    }

    onBillingArrangementUpdate(event) {
        this.billingArrangementId = event.detail.record.id;
    }

    get disableFinancialAccountLookup() {
        return !this.banId;
    }

    get disableBillingArrangementLookup() {
        return !this.financialAccountId;
    }

    get banFilter() {
        return `Account__c = '${this.accountId}'`;
    }

    get financialAccountFilter() {
        return `BAN__c = '${this.banId}'`;
    }

    get billingArrangementFilter() {
        return `Financial_Account__c = '${this.financialAccountId}'`;
    }

    get activeSections() {
        return ["section1", "section2", "section3"];
    }

    _showToast(variant, message) {
        const event = new ShowToastEvent({
            variant: variant,
            message: message
        });
        this.dispatchEvent(event);
    }
}