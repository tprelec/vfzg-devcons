import { LightningElement, api } from 'lwc';

export default class PageHeader extends LightningElement {
	@api iconName;
	@api title;
	@api subtitle;
}
