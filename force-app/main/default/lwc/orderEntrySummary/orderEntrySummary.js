import { api, LightningElement, track } from 'lwc';

export default class OrderEntrySummary extends LightningElement {
	@api state = null;
	@track _bundles;
	@api
	get bundles() {
		return this._bundles;
	}
	set bundles(value) {
		this._bundles = Object.assign([], value);
	}
	opened = false;

	get classes() {
		let clss = 'slds-docked-composer slds-grid slds-grid_vertical ';

		if (this.opened) {
			clss += 'slds-is-open';
		} else {
			clss += 'slds-is-closed';
		}

		return clss;
	}

	toggleVisibility() {
		this.opened = !this.opened;
	}
}
