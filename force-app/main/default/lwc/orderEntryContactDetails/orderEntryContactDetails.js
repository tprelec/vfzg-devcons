import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getContacts from '@salesforce/apex/OrderEntryContactController.getContacts';
import getContact from '@salesforce/apex/OrderEntryContactController.getContact';
import getCountries from '@salesforce/apex/OrderEntryContactController.getCountries';
import saveContact from '@salesforce/apex/OrderEntryContactController.saveContact';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';
import { refreshApex } from '@salesforce/apex';

export default class OrderEntryContactDetails extends LightningElement {
	@api contactId;
	@api opportunityId;
	@api accountId;
	@api journeyType;

	@track countriesList = [];
	@track loading = false;
	@track openSelectionModal = false;
	@track openModal = false;
	@track contacts = [];
	@track contact = {
		Id: '',
		FirstName: '',
		LastName: '',
		MobilePhone: '',
		Phone: '',
		Email: '',
		Birthdate: '',
		LG_PreferredCommunication__c: '',
		Document_Type__c: '',
		Document_Expiration_Date__c: '',
		Document_Number__c: '',
		Document_Issuing_Country__c: 'NL',
		Salutation: ''
	};

	@wire(MessageContext)
	messageContext;

	/**
	 * Executed when component loaded
	 */
	connectedCallback() {
		this.getAccountContacts();
		this.getCountriesList();
		if (this.contactId) {
			refreshApex();

			this.getContact();
		}
	}
	get mobileJourney() {
		return this.journeyType === 'Mobile';
	}
	get documentNumberFormat() {
		if (this.contact.Document_Issuing_Country__c == 'NL') {
			switch (this.contact.Document_Type__c) {
				case 'Driver License':
					return '^[0-9]{10}$';
				case 'Id Card':
					return '[A-Za-z]{2}[A-Za-z0-9]{6}[0-9]{1}';
				case 'Passport':
					return '[A-Za-z]{2}[A-Za-z0-9]{6}[0-9]{1}';
				default:
					return '';
			}
		}
		return '[a-zA-Z0-9]*';
	}

	get documentNumberErrorMessages() {
		if (this.contact.Document_Issuing_Country__c == 'NL') {
			switch (this.contact.Document_Type__c) {
				case 'Driver License':
					return 'Drivers license number must be 10 digits long.';
				case 'Id Card':
					return 'ID Card number must be 9 characters long and must start with 2 letters, followed by 6 digits or letters and ending with 1 digit. (e.g. NL123ABC9)';
				case 'Passport':
					return 'Passport number must be 9 characters long and must start with 2 letters, followed by 6 digits or letters and ending with 1 digit. (e.g. NL123ABC9)';
				default:
					return '';
			}
		}
		return '';
	}
	get documentTypes() {
		if (this.contact.Document_Issuing_Country__c == 'NL') {
			return [
				{
					label: 'Driver License',
					value: 'Driver License'
				},
				{ label: 'ID Card', value: 'Id Card' },
				{ label: 'Passport', value: 'Passport' }
			];
		}
		this.contact.Document_Type__c = 'Passport';
		return [{ label: 'Passport', value: 'Passport' }];
	}

	get todaysDate() {
		let today = new Date();

		today.toString();

		return new Date().toISOString();
	}

	get maximumBirthday() {
		let today = new Date();

		let year = today.getFullYear() - 110;
		today.setFullYear(year);
		return today.toISOString();
	}

	get minimumBirthday() {
		let today = new Date();

		let year = today.getFullYear() - 18;
		today.setFullYear(year);

		return today.toISOString();
	}

	get options() {
		return [
			{ label: 'None', value: 'Opt out all' },
			{ label: 'Sms', value: 'Sms' },
			{ label: 'Email', value: 'Email' }
		];
	}

	get salutationList() {
		return [
			{ label: 'de heer', value: 'de heer' },
			{ label: 'mevrouw', value: 'mevrouw' },
			{ label: 'Mr.', value: 'Mr.' },
			{ label: 'Ms.', value: 'Ms.' },
			{ label: 'Mrs.', value: 'Mrs.' },
			{ label: 'Dr.', value: 'Dr.' },
			{ label: 'Prof.', value: 'Prof.' },
			{ label: 'Drs.', value: 'Drs.' },
			{ label: 'Ing.', value: 'Ing.' },
			{ label: 'Ir.', value: 'Ir.' },
			{ label: 'heer', value: 'heer' }
		];
	}

	/**
	 * get countries list
	 */
	getCountriesList() {
		getCountries({})
			.then((result) => {
				let options = [];
				if (result) {
					for (const [key, value] of Object.entries(result)) {
						options.push({
							label: value,
							value: key
						});
					}

					this.countriesList = options;
				}
			})
			.catch((error) => {
				this.handleError(error);
			});
	}

	/**
	 * Gets all Contacts for Provided Account
	 */
	getAccountContacts() {
		getContacts({ accId: this.accountId })
			.then((result) => {
				if (this.contacts.length === 0) {
					this.contacts = result;
				}
			})
			.catch((error) => {
				this.handleError(error);
			});
	}

	/**
	 * Gets primary Contact
	 */
	getContact() {
		getContact({ contId: this.contactId })
			.then((result) => {
				this.contact = {
					Id: result.Id,
					FirstName: result.FirstName,
					LastName: result.LastName,
					MobilePhone: result.MobilePhone,
					Phone: result.Phone,
					Email: result.Email,
					Salutation: result.Salutation,
					Birthdate: result.Birthdate,
					LG_PreferredCommunication__c: result.LG_PreferredCommunication__c,
					Document_Type__c: result.Document_Type__c,
					Document_Expiration_Date__c: result.Document_Expiration_Date__c,
					Document_Number__c: result.Document_Number__c,
					Document_Issuing_Country__c: result.Document_Issuing_Country__c ? result.Document_Issuing_Country__c : 'NL'
				};
			})
			.catch((error) => {
				this.handleError(error);
			});
	}

	/**
	 * Handles input changes
	 * @param {object} event Event
	 */

	async handleFormInputChange(event) {
		this.contact[event.target.name] = event.target.value;

		if (event.target.name == 'Document_Type__c') {
			await this.sleep(0);
			this.template.querySelectorAll('lightning-input').forEach((element) => {
				element.reportValidity();
				element.checkValidity();
			});
		}

		this.validateSalutation();
	}

	/**
	 * Opens modal for Contact selection
	 */
	selectContact() {
		this.openSelectionModal = true;
	}

	validateSalutation() {
		if (this.contact.Salutation === 'None') {
			const salutationInput = this.template.querySelector('lightning-combobox.salutation');
			salutationInput.setCustomValidity('Complete this field.');
			salutationInput.checkValidity();
		} else {
			const salutationInput = this.template.querySelector('lightning-combobox.salutation');
			salutationInput.setCustomValidity('');
			salutationInput.checkValidity();
		}
	}

	/**
	 * Opens modal for new Contact
	 */
	newContact() {
		this.openModal = true;
	}

	/**
	 * Closes Modal(s)
	 * @param {object} event Event
	 */
	async closeModal(event) {
		const isNew = this.openModal;
		this.openModal = false;
		this.openSelectionModal = false;
		if (event.detail.Id) {
			let tempCont = {
				Id: event.detail.Id,
				FirstName: event.detail.FirstName,
				LastName: event.detail.LastName,
				MobilePhone: event.detail.MobilePhone,
				Phone: event.detail.Phone,
				Email: event.detail.Email,
				Birthdate: event.detail.Birthdate,
				Salutation: event.detail.Salutation,
				LG_PreferredCommunication__c: event.detail.LG_PreferredCommunication__c,
				Document_Type__c: event.detail.Document_Type__c,
				Document_Expiration_Date__c: event.detail.Document_Expiration_Date__c,
				Document_Number__c: event.detail.Document_Number__c,
				Document_Issuing_Country__c: event.detail.Document_Issuing_Country__c ? event.detail.Document_Issuing_Country__c : 'NL'
			};
			this.contact = Object.assign({}, tempCont);

			await this.sleep(0);
			this.template.querySelectorAll('lightning-input').forEach((element) => {
				element.reportValidity();
				element.checkValidity();
			});

			this.template.querySelectorAll('lightning-combobox').forEach((element) => {
				element.reportValidity();
			});

			this.validateSalutation();

			if (isNew) {
				let tempContacts = [...this.contacts];
				tempContacts.push(this.contact);
				this.contacts = tempContacts;
			}
			// Publish
			const msg = {
				sourceComponent: 'order-entry-contact-details',
				payload: {
					event: 'contact-selected',
					data: {
						contactId: this.contact.Id
					}
				}
			};

			publish(this.messageContext, genericChannel, msg);
		}
	}

	/**
	 * description checking input data
	 * @returns boolean value
	 */
	@api validate() {
		this.loading = true;

		let isValid = true;
		this.template.querySelectorAll('lightning-input').forEach((element) => {
			element.reportValidity();
			isValid = isValid && element.checkValidity();
		});

		this.template.querySelectorAll('lightning-combobox').forEach((element) => {
			element.reportValidity();
			isValid = isValid && element.checkValidity();
		});

		this.validateSalutation();

		// If no contact is selected as primary invalidate
		if (!this.contact.Id) {
			this.handleError('There is no primary contact selected.');
			return false;
		}
		if (isValid) {
			return true;
		}
		this.handleError('Please fill out the required fields');
		return false;
	}
	/**
	 * descriptino saving contatc data
	 * @returns boolean value
	 */
	@api saveContact() {
		return saveContact({ newContact: this.contact, oppId: this.opportunityId })
			.then((result) => {
				if (result) {
					return true;
				}
			})
			.catch((error) => {
				this.loading = false;
				// eslint-disable-next-line guard-for-in
				for (const key in error.body.fieldErrors) {
					this.handleError(error.body.fieldErrors[key][0].message);
				}
				return false;
			});
	}

	async sleep(ms) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}

	handleError(error) {
		this.dispatchEvent(
			new ShowToastEvent({
				title: 'Something went wrong',
				message: error,
				variant: 'warning'
			})
		);
	}
}
