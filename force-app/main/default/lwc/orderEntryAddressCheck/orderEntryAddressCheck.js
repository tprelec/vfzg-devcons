import { LightningElement, api, track, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import checkSite from '@salesforce/apex/OrderEntrySiteController.checkSite';
import getSite from '@salesforce/apex/OrderEntrySiteController.getSite';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

export default class OrderEntryAddressCheck extends LightningElement {
	@track loading = false;
	@track _site = {};
	@track address = {};

	@api
	get site() {
		return this._site;
	}
	set site(value) {
		this.setAttribute('site', value);

		const isNewSite = this._site.siteId !== value.siteId;
		this._site = Object.assign({}, value);
		if (isNewSite) {
			this.getSelectedSiteData().then((res) => {
				if (!this._site.siteCheck) {
					this.newAddressCheck();
				}
			});
		}
	}

	@wire(MessageContext)
	messageContext;

	get offNetTypeDisabled() {
		return this._site.addressCheckResult !== 'Off-Net';
	}
	get offNetTypeRequired() {
		return !this.offNetTypeDisabled;
	}
	get addressCheckResultOptions() {
		return [
			{
				label: 'On-Net',
				value: 'On-Net'
			},
			{
				label: 'Off-Net',
				value: 'Off-Net'
			}
		];
	}

	get offNetTypeOptions() {
		return [
			{
				label: 'MTC',
				value: 'MTC'
			},
			{
				label: 'Near-Net',
				value: 'Near-Net'
			},
			{
				label: 'BVG',
				value: 'BVG'
			}
		];
	}

	/**
	 * Gets Selected Site Data
	 */
	getSelectedSiteData() {
		return getSite({ siteId: this._site.siteId })
			.then((data) => {
				this.address = data;
			})
			.catch((error) => {
				this.handleError(error.description, error.message);
				console.error('Error: ' + JSON.stringify(error));
			});
	}

	/**
	 * Executes New Address Check
	 */
	newAddressCheck() {
		if (this.address.Site_Postal_Code__c) {
			this.loading = true;

			checkSite({
				postCode: this.address.Site_Postal_Code__c,
				houseNumber: this.address.Site_House_Number__c,
				extension: this.address.Site_House_Number_Suffix__c ? this.address.Site_House_Number_Suffix__c : ''
			})
				.then((data) => {
					this.loading = false;
					this.handleAddressCheckResponse(JSON.parse(data));
				})
				.catch((error) => {
					this.loading = false;
					this.handleError('Address Check Failed', 'Please make sure to select a valid address.');
					console.error('Error: ' + JSON.stringify(error));
				});
		} else {
			this.handleError('No Site Selected', 'Please select a Site');
		}
	}

	/**
	 * Handles Address Check Response
	 * @param {object} data Address Check Response Data
	 */
	handleAddressCheckResponse(data) {
		this._site.siteCheck = data;
		if (data.availability) {
			data.availability.forEach((item, index) => {
				// If internet is available, then it is On-Net
				if (item.name === 'internet') {
					if (item.available) {
						this._site.addressCheckResult = 'On-Net';
						this._site.offNetType = '';
					} else {
						this._site.addressCheckResult = 'Off-Net';
						this._site.offNetType = '';
					}
				}
			});
		} else {
			// Even if the site check fail save the result with blank availibility
			this._site.siteCheck = {
				street: this.address.Site_Street__c,
				houseNumber: this.address.Site_House_Number__c,
				houseNumberExt: this.address.Site_House_Number_Suffix__c,
				zipCode: this.address.Site_Postal_Code__c,
				city: this.address.Site_City__c,
				status: [],
				availability: [],
				footprint: 'ZIGGO'
			};
			this._site.addressCheckResult = 'Off-Net';
			stateClone.offNetType = '';
		}
		this.publishAddressCheck();
	}

	/**
	 * Publishes changes to parent components
	 */
	publishAddressCheck() {
		const msg = {
			sourceComponent: 'order-entry-address-check',
			payload: {
				event: 'address-check-changed',
				data: {
					siteCheck: this._site.siteCheck,
					addressCheckResult: this._site.addressCheckResult,
					offNetType: this._site.offNetType
				}
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}

	/**
	 * Handles Changes to the Address Check Result
	 * @param {object} event Change Event
	 */
	handleAddressCheckResultChange(event) {
		this._site.addressCheckResult = event.target.value;
		this._site.offNetType = '';
		this.publishAddressCheck();
	}

	/**
	 * Handles Changes to the Offnet Type Result
	 * @param {object} event Change Event
	 */
	handleOffNetChange(event) {
		this._site.offNetType = event.target.value;
		this.publishAddressCheck();
	}

	@api validate() {
		let result = true;

		const addressCheck = this.template.querySelector('lightning-combobox.address-check-result');
		const offNetType = this.template.querySelector('lightning-combobox.off-net-type');

		if (!this._site.addressCheckResult) {
			result = false;
			addressCheck.setCustomValidity('Please run address check or select values manually.');
		} else if (this.offNetTypeRequired && !this._site.offNetType) {
			result = false;
			addressCheck.setCustomValidity('');
		} else {
			result = true;
			addressCheck.setCustomValidity('');
		}
		addressCheck.reportValidity();
		if (offNetType) {
			offNetType.reportValidity();
		}
		return result;
	}

	handleError(title, error) {
		this.dispatchEvent(
			new ShowToastEvent({
				title: title,
				message: error,
				variant: 'warning'
			})
		);
	}
}
