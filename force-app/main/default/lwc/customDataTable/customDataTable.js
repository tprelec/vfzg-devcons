import LightningDatatable from 'lightning/datatable';
import DatatablePicklistTemplate from './picklist-template.html';
import { api } from 'lwc';

export default class CustomDataTable extends LightningDatatable {
	static customTypes = {
		picklist: {
			template: DatatablePicklistTemplate,
			standardCellLayout: false,
			typeAttributes: ['label', 'placeholder', 'rowIdentifier', 'disabled', 'options', 'value', 'context', 'fieldName']
		}
	};

	@api setCellEdit(rowNumber, columnNumber) {
		let rows = this.template.querySelectorAll('table tbody tr');
		let row = rows[rowNumber];
		let cells = row.querySelectorAll('th , td ');
		let cell = cells[columnNumber];
		cell.style.setProperty('background-color', 'rgb(250, 255, 189)', 'important');
		cell.style.setProperty('overflow', 'visible !important');
	}

	@api setCellError(rowNumber, columnNumber) {
		let rows = this.template.querySelectorAll('table tbody tr');
		let row = rows[rowNumber];
		let cells = row.querySelectorAll('th , td ');
		let cell = cells[columnNumber];
		cell.style.setProperty('background-color', 'rgb(250, 255, 189)', 'important');
		cell.style.setProperty('box-shadow', 'var(--lwc-colorBorderError,rgb(234, 0, 30)) 0 0 0 2px inset', 'important');
		cell.classList.add('slds-has-error');
	}

	@api removeCellError(rowNumber, columnNumber) {
		let rows = this.template.querySelectorAll('table tbody tr');
		let row = rows[rowNumber];
		let cells = row.querySelectorAll('th , td ');
		let cell = cells[columnNumber];
		cell.style.removeProperty('background-color');
		cell.style.removeProperty('box-shadow');
		cell.classList.remove('slds-has-error');
	}

	@api validateAllCells() {
		let rows = this.template.querySelectorAll('table tbody tr');
		for (let i = 0; i < rows.length; i++) {
			let row = rows[i];
			let cells = row.querySelectorAll('th, td ');
			for (let j = 0; j < cells.length; j++) {
				if (cells[j].dataset.label === undefined) {
					continue;
				}
				if (this.columns.find((cell) => cell.label === cells[j].dataset.label).error) {
					cells[j].classList.add('slds-has-error');
				} else {
					cells[j].classList.remove('slds-has-error');
				}
			}
		}
	}

	@api refreshAllCells() {
		let rows = this.template.querySelectorAll('table tbody tr');
		for (let i = 0; i < rows.length; i++) {
			let row = rows[i];
			let cells = row.querySelectorAll('th, td ');
			for (let j = 0; j < cells.length; j++) {
				cells[j].style.removeProperty('background-color');
				cells[j].style.removeProperty('box-shadow');
				cells[j].classList.remove('slds-has-error');
				cells[j].classList.remove('slds-has-focus');
			}
		}
	}

	@api setPicklistFocus(rowNumber, columnNumber) {
		let row = this.template.querySelectorAll('table tbody tr')[rowNumber];
		let cell = row.querySelectorAll('th, td ')[columnNumber];
		cell.click();
		cell.classList.add('slds-has-focus');
	}

	@api scrollIntoCellView(rowNumber, columnNumber) {
		let row = this.template.querySelectorAll('table tbody tr')[rowNumber];
		let cell = row.querySelectorAll('th, td ')[columnNumber];
		cell.click();
		cell.classList.add('slds-has-focus');
		cell.scrollIntoView({ block: 'nearest', inline: 'center' });
	}
}
