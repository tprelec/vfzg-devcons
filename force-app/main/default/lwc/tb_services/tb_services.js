import { LightningElement, wire, api } from 'lwc';
import getServices from '@salesforce/apex/TB_SrvController.getServices';
 
const columns = [
    { label: 'Name', fieldName: 'parentServiceName', hideDefaultActions : true, wrapText: true },
    { label: '', fieldName: 'name', hideDefaultActions : true, wrapText: true },
    { label: 'Delivery Component', fieldName: 'deliveryComponentName', hideDefaultActions : true, wrapText: true },
    { label: 'Delivery Article', fieldName: 'deliveryArticleName', hideDefaultActions : true, wrapText: true },
];
 
export default class Tb_services extends LightningElement {
    tableData = [];
    tableColumns = columns;
    @api recordId;
 
    @wire(getServices, { recId: '$recordId' })
    wiredServices(result){
        if (result.data) {
            this.tableData = result.data;
        } else if (result.error) {
            this.tableData = undefined;
            console.log(result.error);
        }
    }
}