import { LightningElement, api, track } from "lwc";
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getBanStatus from "@salesforce/apex/BanVerifierController.getBanStatus";
import getAssociatedAttributes from "@salesforce/apex/BanVerifierController.getAssociatedAttributes";
import sSave from "@salesforce/apex/BanVerifierController.save";
import requestNew from "@salesforce/apex/BanVerifierController.requestNew";
import { reduceErrors } from "c/ldsUtils";

/** Custom Labels used in this LWC */
import banVerifSave from "@salesforce/label/c.banVerifSave";
import banVerifReset from "@salesforce/label/c.banVerifReset";
import banVerifNew from "@salesforce/label/c.banVerifNew";
import banVerifValidations from "@salesforce/label/c.banVerifValidations";
import banVerifStatus from "@salesforce/label/c.banVerifStatus";
import banVerifAvailable from "@salesforce/label/c.banVerifAvailable";
import banVerifHasFixed from "@salesforce/label/c.banVerifHasFixed";
import banVerifErrInvalidSelection from "@salesforce/label/c.banVerifErrInvalidSelection";
import banVerifControlTitle from "@salesforce/label/c.banVerifControlTitle";
import autocRemoveSelected from "@salesforce/label/c.autocRemoveSelected";

export default class BanVerifier extends LightningElement {
	@api recordId; // Id value of parent record
	@api preventNew = false;
	@api readOnly = false;
	@track isClean = true;
	@track isNotAllowedSave = true;

	banObjectName = "BAN__c";
	@api banDependantField = "Account__c";

	@api currentValue; // The value until the user saves it.
	currentDependantValue; // set in this.retrieveAssociatedAttributes()
	selectedBanId;

	@api recordDependantField = "AccountId";
	@api recordBanField = "Ban__c";

	scriptInitialized = false;
	@track boxStyling = "slds-box hide-box";
	fieldSet = [];

	labels = {
		banVerifSave,
		banVerifReset,
		banVerifNew,
		banVerifValidations,
		banVerifStatus,
		banVerifAvailable,
		banVerifHasFixed,
		banVerifErrInvalidSelection,
		banVerifControlTitle,
		autocRemoveSelected
	};

	// Handle custom lookup component event
	lookupRecord(event) {
		if (event.detail.selectedRecord !== undefined) {
			this.isClean = false;
			this.isNotAllowedSave = false;
			// Gather the information about the selected BAN (is selectable?)
			this.selectedBanId = event.detail.selectedRecord.Id;
			this.retrieveBanStatus(event.detail.selectedRecord.Id);
		} else {
			if (this.currentValue == "") {
				this.isClean = true;
				this.isNotAllowedSave = true;
			} else {
				this.isClean = false;
				this.isNotAllowedSave = false;
				this.currentValue == "";
			}
			this.fieldSet = [];
		}
	}

	handleShowCurrent() {
		if (this.boxStyling == "slds-box hide-box") {
			this.boxStyling = "slds-box";
		} else {
			this.boxStyling = "slds-box hide-box";
		}
	}

	handleRequestNew() {
		this.isNotAllowedSave = true;
		this.banRequestNew();
	}

	handleSave() {
		this.isNotAllowedSave = true;
		this.banSave();
	}

	handleReset() {
		this.template.querySelector("c-autocomplete").setDefaultId(this.currentValue);
		this.fieldSet = [];
		this.isNotAllowedSave = true;
	}

	connectedCallback() {
		if (this.scriptInitialized) {
			return;
		}
		this.scriptInitialized = true;
		this.retrieveAssociatedAttributes();
	}

	// -- HELPER FUNCTIONS ---//
	banRequestNew() {
		requestNew({
			accountId: this.currentDependantValue
		})
			.then((result) => {
				console.log(result);
				this.fireToaster("", "´Ban´ creation requested successfully.", "success");
				this.isClean = true;
				this.currentValue = result.Id;
				this.template.querySelector("c-autocomplete").setDefaultId(result.Id);
			})
			.catch((error) => {
				let errMsg = reduceErrors(error);
				console.log(errMsg);
				this.fireToaster("", errMsg, "error");
				this.isClean = false;
				this.isNotAllowedSave = false;
			});
	}

	banSave() {
		sSave({
			recordId: this.recordId,
			currentBanId: this.selectedBanId,
			currentBanFieldName: this.recordBanField,
			accountId: this.currentDependantValue
		})
			.then((result) => {
				this.fireToaster("", "´Ban´ updated successfully.", "success");
				this.isClean = true;
				this.currentValue = this.selectedBanId;
			})
			.catch((error) => {
				let errMsg = reduceErrors(error);
				this.fireToaster("", errMsg, "error");
				this.isClean = false;
				this.isNotAllowedSave = false;
			});
	}

	retrieveAssociatedAttributes() {
		getAssociatedAttributes({
			objectId: this.recordId,
			banFieldName: this.recordBanField,
			acctFieldName: this.recordDependantField
		})
			.then((result) => {
				this.template
					.querySelector("c-autocomplete")
					.setParentFilters(this.banDependantField, result.acctFieldValue);

				const qryAutocomplete = this.template.querySelector("c-autocomplete");
				qryAutocomplete.setDefaultId(result.banFieldValue);
				if (result.disabled == "true") {
					qryAutocomplete.disabled = true;
					// TODO: disable also save, reset and addNew buttons here.
				}

				this.currentValue = result.banFieldValue;
				this.currentDependantValue = result.acctFieldValue;
			})
			.catch((error) => {
				let errMsg = reduceErrors(error);
				this.fireToaster("Error", errMsg, "error");
			});
	}

	retrieveBanStatus(selRecordId) {
		getBanStatus({
			banNumberId: selRecordId
		})
			.then((data) => {
				this.fieldSet = [];
				for (let key in data) {
					// Preventing unexcepted data
					if (data.hasOwnProperty(key)) {
						// Filtering the data in the loop
						this.fieldSet.push({ value: data[key], key: key });
					}
				}
				console.log(JSON.stringify(data));
			})
			.catch((error) => {
				let errMsg = reduceErrors(error);
				console.log(errMsg);
			});
	}

	// fires the toaster
	fireToaster(pTitle, pMessage, pVariant) {
		const toasty = new ShowToastEvent({
			title: pTitle,
			message: pMessage,
			variant: pVariant,
			mode: "dismissable"
		});
		this.dispatchEvent(toasty);
	}
}