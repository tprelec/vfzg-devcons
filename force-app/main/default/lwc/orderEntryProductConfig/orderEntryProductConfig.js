import { LightningElement, api } from 'lwc';

export default class OrderEntryProductConfig extends LightningElement {
	@api opportunityId;
	@api bundles;
	@api installationSite;
	@api mainProduct;
	@api journeyType;

	get fixedBundle() {
		return this.bundles.filter((bundle) => {
			return bundle.type === 'Fixed';
		})[0];
	}

	get mobileBundles() {
		return this.bundles.filter((bundle) => {
			return bundle.type === 'Mobile';
		});
	}

	get isFixed() {
		return this.journeyType === 'Fixed';
	}

	get isMobile() {
		return this.journeyType === 'Mobile';
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */
	@api validate() {
		const validation = this.isFixed
			? this.template.querySelector('c-order-entry-product-config-fixed')
			: this.template.querySelector('c-order-entry-product-config-mobile');
		return validation.validate();
	}

	@api async saveData() {
		if (this.isFixed) {
			const childComponent = this.template.querySelector('c-order-entry-product-config-fixed');
			await childComponent.saveData();
		}
	}
}
