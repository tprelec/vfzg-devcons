import { LightningElement, api, track } from 'lwc';
import saveContacts from '@salesforce/apex/PP_SearchContactsController.saveContacts';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { loadStyle } from 'lightning/platformResourceLoader';
import dataTablePicklist from '@salesforce/resourceUrl/dataTablePicklist';
import uId from '@salesforce/user/Id';

export default class ContactDatatable extends LightningElement {
	@track _contacts;
	@track showSpinner;
	@track errorsList;
	@api accountId;
	@track oldContactList;
	@track userId = uId;

	@api
	get contacts() {
		return this._contacts;
	}
	set contacts(value) {
		this._contacts = JSON.parse(JSON.stringify(value));
		this.oldContactList = JSON.parse(JSON.stringify(value));
	}

	@track _columns;
	@api
	get columns() {
		return this._columns;
	}
	set columns(value) {
		this._columns = Object.assign([], value);
	}

	@track _draftValues;
	@api
	get draftValues() {
		return this._draftValues;
	}
	set draftValues(value) {
		this._draftValues = value && value !== '[]' ? Object.assign([], JSON.parse(value)) : [];
	}

	@api errors;
	@api hideCheckboxColumn;

	connectedCallback() {
		loadStyle(this, dataTablePicklist);
	}

	handleSaveEdition(event) {
		this.showSpinner = true;
		let errorList = [];
		event.stopPropagation();
		this._draftValues.forEach((element) => {
			errorList = this.checkRequiredFields(
				this._contacts.find((el) => el.Id === element.Id) ? this._contacts.find((el) => el.Id === element.Id) : {},
				element,
				element.Id === 'row-0'
			);
		});

		let errors = { rows: {}, table: {} };

		let mapErrors = errorList.map((el) => el.record);
		let setError = new Set(mapErrors);
		let errorMessages = [];
		setError.forEach((id) => {
			errorMessages = errorMessages.concat(errorList.filter((el) => el.record === id).map((error) => error.message));
			errors.rows[id] = {
				title: 'Error(s)',
				messages: errorList.filter((el) => el.record === id).map((error) => error.message),
				fieldNames: errorList.filter((el) => el.record === id).map((error) => error.field)
			};
		});

		if (errorList.length === 0) {
			this._draftValues.forEach((el) => {
				el.AccountId = this.accountId;
				el.Id = el.Id !== '' && !el.Id.includes('row-') ? el.Id : undefined;
			});

			saveContacts({ contacts: this._draftValues }).then((res) => {
				if (res === 'SUCCESS') {
					this.errorList = [];
					this._draftValues = [];
					this.showToast('Success!', 'Record Updated', 'success');
					this.showSpinner = false;
					this.dispatchEvent(
						new CustomEvent('saveddata', {
							bubbles: false
						})
					);
				} else {
					this.showToast('Error!', JSON.parse(res).ExceptionMessage, 'error');
					this.showSpinner = false;
				}
			});
		} else {
			if (errorList.forEach((el) => el.field === 'Gender__c')) {
				let indexForDel = this._draftValues.indexOf(this._draftValues.find((el) => el.Gender__c !== undefined));
				this._draftValues[indexForDel].Gender__c = null;
			}

			this.showToast('Error!', errorMessages.join(';  '), 'error');
			this.showSpinner = false;

			errors.table.title = 'Error while saving contact...';
			errors.table.messages = errorMessages;
			this.errorsList = errors;
		}
	}

	handlePicklistClick(event) {
		event.stopPropagation();
		let dataRecieved = event.detail.data;
		var id = dataRecieved.context;
		var field = dataRecieved.label;
		var value = dataRecieved.value;
		this.setPicklistFocus(id, field);
	}

	setPicklistFocus(id, field) {
		var row = this.getRowNumber(id);
		var column = this.getColumnNumber(field);
		this.template.querySelector('c-custom-data-table').setPicklistFocus(row, column);
	}

	checkRequiredFields(oldRecord, newRecord, newRecordFlag) {
		var errorList = [];

		if ((!oldRecord.FirstName && !newRecord.FirstName) || newRecord.FirstName === '') {
			errorList.push({
				record: newRecordFlag === true ? 'row-0' : oldRecord.Id,
				field: 'FirstName',
				message: 'First Name required!'
			});
		}

		if ((!oldRecord.LastName && !newRecord.LastName) || newRecord.LastName === '') {
			errorList.push({
				record: newRecordFlag === true ? 'row-0' : oldRecord.Id,
				field: 'LastName',
				message: 'Last Name required!'
			});
		}

		if ((!oldRecord.Gender__c && !newRecord.Gender__c) || newRecord.Gender__c === '') {
			errorList.push({
				record: newRecordFlag === true ? 'row-0' : oldRecord.Id,
				field: 'Gender__c',
				message: 'Gender required!'
			});
		}

		if (
			oldRecord.Gender__c &&
			oldRecord.Gender__c !== this.oldContactList.find((el) => el.Id === oldRecord.Id).Gender__c &&
			oldRecord.OwnerId !== this.userId
		) {
			errorList.push({
				record: newRecordFlag === true ? 'row-0' : oldRecord.Id,
				field: 'Gender__c',
				message: 'Only the owner of the record can change the gender field!'
			});
		}

		if ((!oldRecord.Email && !newRecord.Email) || newRecord.Email === '') {
			errorList.push({
				record: newRecordFlag === true ? 'row-0' : oldRecord.Id,
				field: 'Email',
				message: 'Email required!'
			});
		}

		if ((!oldRecord.MobilePhone && !newRecord.MobilePhone) || newRecord.MobilePhone === '') {
			errorList.push({
				record: newRecordFlag === true ? 'row-0' : oldRecord.Id,
				field: 'MobilePhone',
				message: 'Mobile Phone required!'
			});
		}

		if (newRecord.MobilePhone) {
			let mobilePhone = newRecord.MobilePhone.replaceAll('(', '')
				.replaceAll(')', '')
				.replaceAll('-', '')
				.replaceAll(' ', '')
				.replaceAll('+', '');
			if (!mobilePhone.match(/^[0-9]{3,18}$/)) {
				errorList.push({
					record: newRecordFlag === true ? 'row-0' : oldRecord.Id,
					field: 'MobilePhone',
					message:
						'Only mobile numbers, dashes and spaces are allowed, and a starting plus-sign. Valid formats are e.g.: 0612345678 +31612345678'
				});
			}
		}

		if (newRecord.Phone) {
			let phone = newRecord.Phone.replaceAll('(', '').replaceAll(')', '').replaceAll('-', '').replaceAll(' ', '').replaceAll('+', '');
			if (!phone.match(/^[0-9]{3,18}$/)) {
				errorList.push({
					record: newRecordFlag === true ? 'row-0' : oldRecord.Id,
					field: 'Phone',
					message:
						'Only numbers, dashes and spaces are allowed, and a starting plus-sign. Valid formats are e.g.: 0123456789, 0031 12 3456789, 06 12345678, +31123456789'
				});
			}
		}

		return errorList;
	}

	handleCancel() {
		this._draftValues = [];
		this._contacts = JSON.parse(JSON.stringify(this.oldContactList));
		this.template.querySelector('c-custom-data-table').refreshAllCells();
	}

	setCellEditable(id, field) {
		var row = this.getRowNumber(id);
		var column = this.getColumnNumber(field);

		this.template.querySelector('c-custom-data-table').setCellEdit(row, column);
	}

	getRowNumber(id) {
		if (id.includes('row-')) {
			return this._contacts.length > 0 ? this._contacts.length - 1 : 0;
		}
		for (let i = 0; i < this.contacts.length; i++) {
			if (this.contacts[i].Id === id) {
				return i;
			}
		}
	}

	handleRowAction(event) {
		this.dispatchEvent(
			new CustomEvent('rowaction', {
				bubbles: false,
				detail: event.detail
			})
		);
	}

	getColumnNumber(field) {
		let columnMap = {
			FirstName: 1,
			MiddleName: 2,
			LastName: 3,
			Gender__c: 4,
			Title: 5,
			Email: 6,
			Phone: 7,
			MobilePhone: 8
		};
		return columnMap[field];
	}

	handlePicklistChange(event) {
		event.stopPropagation();
		let dataRecieved = JSON.parse(JSON.stringify(event.detail.data));
		let id = dataRecieved.context;
		id = id === '' ? 'row-0' : id;
		const field = dataRecieved.label;
		const value = dataRecieved.value;

		this.setCellEditable(id, field);
		this.updateDraftValues(id, field, value);
	}

	handleCellChange(event) {
		let field = Object.keys(event.detail.draftValues[0])[0];
		let id = event.detail.draftValues[0].Id;
		/* let id = idTemp.includes("row-")
			? this._contacts[parseInt(idTemp.split("row-")[1], 10)].Id
			: idTemp; */

		let value = event.detail.draftValues[0][field];

		this.setCellEditable(id, field);
		this.updateDraftValues(id, field, value);
	}

	updateDraftValues(id, field, value) {
		this.updateDataValues(id, field, value);

		let draftValueChanged = false;
		let copyDraftValues = [...this._draftValues];
		let cont = copyDraftValues.find((el) => el.Id === id);
		let indexOfContact = copyDraftValues.indexOf(cont);
		if (indexOfContact !== -1) {
			copyDraftValues[indexOfContact][field] = value;
			draftValueChanged = true;
		}

		if (field === 'Gender__c') {
			if (value === 'Male') {
				this.updateDataValues(id, 'Salutation', 'de heer');
				if (indexOfContact !== -1) {
					copyDraftValues[indexOfContact].Salutation = 'de heer';
					draftValueChanged = true;
				}
			} else if (value === 'Female') {
				this.updateDataValues(id, 'Salutation', 'mevrouw');
				if (indexOfContact !== -1) {
					copyDraftValues[indexOfContact].Salutation = 'mevrouw';
					draftValueChanged = true;
				}
			} else if (value === 'M/F') {
				this.updateDataValues(id, 'Salutation', '');
				if (indexOfContact !== -1) {
					copyDraftValues[indexOfContact].Salutation = '';
					draftValueChanged = true;
				}
			} else if (value === '.') {
				this.updateDataValues(id, 'Salutation', '');
				if (indexOfContact !== -1) {
					copyDraftValues[indexOfContact].Salutation = '';
					draftValueChanged = true;
				}
			}
		}
		if (draftValueChanged) {
			this._draftValues = [...copyDraftValues];
		} else {
			let newDraft = {};
			if (field === 'Gender__c') {
				if (value === 'Male') {
					newDraft.Salutation = 'de heer';
				} else if (value === 'Female') {
					newDraft.Salutation = 'mevrouw';
				} else if (value === 'M/F') {
					newDraft.Salutation = '';
				} else if (value === '.') {
					newDraft.Salutation = '';
				}
			}
			newDraft[field] = value;
			newDraft.Id = id;
			this._draftValues = Object.assign([], [...copyDraftValues, newDraft]);
		}
	}
	updateDataValues(id, field, value) {
		let copyData = [...this._contacts];
		if (copyData.find((el) => el.Id === id)) {
			copyData.find((el) => el.Id === id)[field] = value;
		} else if (copyData.find((el) => el.Id === '')) {
			copyData.find((el) => el.Id === '')[field] = value;
		}

		this._contacts = [...copyData];
	}

	showToast(title, message, variant) {
		const event = new ShowToastEvent({
			title,
			variant,
			message
		});
		this.dispatchEvent(event);
	}
}
