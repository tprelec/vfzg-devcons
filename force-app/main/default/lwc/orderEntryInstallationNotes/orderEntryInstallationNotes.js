import { LightningElement, api, track } from 'lwc';

export default class OrderEntryInstallationNotes extends LightningElement {
	@track _notes;
	@track timeout = null;

	@api
	get notes() {
		return this._notes;
	}
	set notes(value) {
		this._notes = value;
	}

	notesChange(event) {
		console.log('Instal. notes changed!');
		this._notes = event.target.value;
		clearTimeout(this.timeout);
		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.timeout = setTimeout(() => {
			this.dispatchStageChange();
		}, 1000);
	}

	dispatchStageChange() {
		this.dispatchEvent(
			new CustomEvent('noteschange', {
				detail: this.notes,
				bubbles: false
			})
		);
	}
}
