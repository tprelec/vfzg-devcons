import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';

import generateConfirmation from '@salesforce/apex/OrderEntryQuoteController.generateConfirmation';
import generateContractSummary from '@salesforce/apex/OrderEntryQuoteController.generateContractSummary';
import getDocumentRequest from '@salesforce/apex/OrderEntryQuoteController.getDocumentRequest';
import updateOpportunitySingedQuote from '@salesforce/apex/OrderEntryQuoteController.updateOpportunitySingedQuote';
import getOpportunity from '@salesforce/apex/OrderEntryController.getOpportunity';
import updateOpportunityAutomatedQuoteDelivery from '@salesforce/apex/OrderEntryQuoteController.updateOpportunityAutomatedQuoteDelivery';
import updateOpportunityStage from '@salesforce/apex/OrderEntryController.updateOpportunityStage';

export default class OrderEntryQuoteSignature extends NavigationMixin(LightningElement) {
	@api opportunityId;
	@api primaryContactId;
	@api journeyType;
	@track loading = false;
	docRequestConfirmation;
	docRequestSumarry;
	docRequestQuote;
	@track fileId = [];
	@track displaySignatureModal = false;
	@track showViewLastComponent = false;
	@track signedQuote = false;
	@track confirmationFileId;
	@track summaryFileId;

	detectMob() {
		var hasTouchscreen = 'ontouchstart' in window;
		if (hasTouchscreen ? true : false) {
			return hasTouchscreen ? true : false;
		}
	}

	get detectMobile() {
		if (this.detectMob()) {
			return true;
		} else if (!this.detectMob()) {
			return false;
		}
		return false;
	}

	get showSingButton() {
		if (this.detectMob() && !this.signedQuote) {
			return true;
		} else if (this.detectMob()) {
			return false;
		}
		return false;
	}

	get showSendButton() {
		if (this.detectMob() && this.signedQuote) {
			return true;
		}
		return false;
	}

	get showSendQuoteButton() {
		if (!this.detectMob()) {
			return true;
		} else if (this.detectMob && !this.signedQuote) {
			return true;
		}
		return false;
	}
	// getOpportunity information and set on form
	connectedCallback() {
		getOpportunity({
			oppId: this.opportunityId,
			fields: ['LG_SignedQuoteAttachmentId__c', 'LG_SignedQuoteAvailable__c']
		})
			.then((res) => {
				this.showViewLastComponent = res.LG_SignedQuoteAttachmentId__c ? true : false;
				this.signedQuote = res.LG_SignedQuoteAvailable__c === 'true' ? true : false;
			})
			.catch((error) => {
				this.handleError(error);
				this.showViewLastComponent = false;
			});
	}

	// get Last Quote and open in preview
	async viewLastQuote() {
		this.loading = true;
		await getOpportunity({
			oppId: this.opportunityId,
			fields: ['LG_SignedQuoteAttachmentId__c', 'Contract_Summary_Attachment_Id__c']
		})
			.then((res) => {
				if (res.LG_SignedQuoteAttachmentId__c) {
					var pdfViewer = this.template.querySelector('c-pdf-viewer');
					this.confirmationFileId = res.LG_SignedQuoteAttachmentId__c;
					this.summaryFileId = res.Contract_Summary_Attachment_Id__c;

					if (this.confirmationFileId && this.summaryFileId && this.summaryFileId !== this.confirmationFileId) {
						pdfViewer.fileId = [this.summaryFileId, this.confirmationFileId];
						pdfViewer.preview();
					}

					this.loading = false;
				}
			})
			.catch((error) => {
				this.handleError(error);
				this.loading = false;
			});
	}

	// open modal with signature component
	openSignatureModal() {
		var signatureModal = this.template.querySelector('c-order-entry-signature-modal');
		signatureModal.displayModal(true, this.confirmationFileId, this.primaryContactId);
	}

	// close modal
	closeModal() {
		var signatureModal = this.template.querySelector('c-order-entry-signature-modal');
		signatureModal.displayModal(false, this.confirmationFileId, this.primaryContactId);
	}

	// update opportuniti after signing
	confirmationSignature(e) {
		this.loading = false;
		if (e.detail) {
			this.confirmationFileId = e.detail;
			updateOpportunitySingedQuote({
				oppId: this.opportunityId,
				attachmentId: null,
				singed: true
			});
			this.signedQuote = true;
			this.showViewLastComponent = true;
			this.previewFile();
		}
	}

	// create confirmation
	@api async createConfirmation() {
		this.loading = true;
		/* this.fileId = []; */
		return await generateConfirmation({
			opportunityId: this.opportunityId,
			journeyType: this.journeyType
		})
			.then(async (result) => {
				if (result) {
					this.docRequestConfirmation = result;
					await this.checkDocRequestStatusForConfirmation();
					return true;
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	async createContractSummary() {
		return generateContractSummary({
			opportunityId: this.opportunityId,
			journeyType: this.journeyType
		})
			.then(async (result) => {
				if (result) {
					this.docRequestSumarry = result;
					await this.checkDocRequestStatusForSummary();
					return true;
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}
	async createDocuments() {
		this.loading = true;
		this.summaryFileId = '';
		this.confirmationFileId = '';
		let respConfirmation = Promise.resolve(this.createConfirmation());
		let respSummary = Promise.resolve(this.createContractSummary());
		await Promise.all([respConfirmation, respSummary]);
	}

	checkDocRequestStatusForConfirmation() {
		return getDocumentRequest({ docRequestId: this.docRequestConfirmation.Id })
			.then((result) => {
				if (result) {
					this.docRequestConfirmation = result;
					if (this.docRequestConfirmation.mmdoc__Status__c === 'Completed') {
						this.confirmationFileId = this.docRequestConfirmation.mmdoc__File_Id__c;
						this.signedQuote = false;
						updateOpportunitySingedQuote({
							oppId: this.opportunityId,
							attachmentId: this.confirmationFileId,
							singed: false
						});
						this.previewConfirmationAndSummary();
						this.showViewLastComponent = true;
						this.loading = false;
						return true;
					} else if (this.docRequestConfirmation.mmdoc__Status__c === 'Error') {
						this.handleError({
							body: {
								message: 'Error while generating document.'
							}
						});
						return true;
					}
					this.sleep(2000);
					this.checkDocRequestStatusForConfirmation();
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}
	checkDocRequestStatusForSummary() {
		return getDocumentRequest({ docRequestId: this.docRequestSumarry.Id })
			.then((result) => {
				if (result) {
					this.docRequestSumarry = result;

					if (this.docRequestSumarry.mmdoc__Status__c === 'Completed') {
						this.summaryFileId = this.docRequestSumarry.mmdoc__File_Id__c;
						this.previewConfirmationAndSummary();
						this.loading = false;
						return true;
					} else if (this.docRequestSumarry.mmdoc__Status__c === 'Error') {
						this.loading = false;
						this.handleError({
							body: {
								message: 'Error while generating document.'
							}
						});
						return true;
					}
					this.sleep(2000);
					this.checkDocRequestStatusForSummary();
				}
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	previewConfirmationAndSummary() {
		if (this.summaryFileId && this.confirmationFileId && this.summaryFileId !== this.confirmationFileId) {
			this.previewFile();
		}
	}

	// preview file
	previewFile() {
		var pdfViewer = this.template.querySelector('c-pdf-viewer');
		pdfViewer.fileId = [this.summaryFileId, this.confirmationFileId];
		pdfViewer.preview();
	}

	sleep(ms) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}

	handleError(error) {
		console.log(error);
		this.loading = false;
		this.dispatchMessage('Something went wrong', error.body.message, 'warning');
	}

	dispatchMessage(title, message, variant) {
		this.dispatchEvent(
			new ShowToastEvent({
				title,
				message,
				variant
			})
		);
	}

	sendConfirmation() {
		this.loading = true;
		updateOpportunityStage({
			oppId: this.opportunityId,
			value: 'Customer Approved'
		})
			.then(() => {
				this.navigateToRecordPage();
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	async sendQuote() {
		this.loading = true;
		return updateOpportunityAutomatedQuoteDelivery({
			oppId: this.opportunityId,
			value: 'Quote Requested'
		})
			.then(() => {
				this.navigateToRecordPage();
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	navigateToRecordPage() {
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: this.opportunityId,
				objectApiName: 'Opportunity',
				actionName: 'view'
			}
		});
		this.closeModal();
	}
}
