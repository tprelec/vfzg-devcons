import { LightningElement, api } from 'lwc';
import { FlowNavigationNextEvent, FlowNavigationBackEvent, FlowNavigationFinishEvent } from 'lightning/flowSupport';

export default class ControlFlowNavigation extends LightningElement {
    @api
    availableActions = [];
    @api
    allowGoNext = false;
    @api
    allowGoBack = false;  
    @api
    allowFinish = false;    
    @api
    moveNextFinish;
    isBackActionAvialable = false;
    isNextActionAvialable = false;
    isFinishActionAvialable = false;

    connectedCallback() {
        for (var i = 0; i < this.availableActions.length; i++) {
            if (this.availableActions[i] === 'NEXT' && this.allowGoNext) {
                this.isNextActionAvialable = true;
            } else if (this.availableActions[i] === 'BACK' && this.allowGoBack) {
                this.isBackActionAvialable = true;
            } else if (this.availableActions[i] === 'FINISH' && this.allowFinish) {
                this.isFinishActionAvialable = true;
            }
        }
    }

    handleGoNext() {
        if (this.availableActions.find((action) => action === 'NEXT')) {
            const navigateNextEvent = new FlowNavigationNextEvent();
            this.dispatchEvent(navigateNextEvent);
        }
    } 
    
    handleGoback() {
        if (this.availableActions.find((action) => action === 'BACK')) {
            const navigateBackEvent = new FlowNavigationBackEvent();
            this.dispatchEvent(navigateBackEvent);
        }
    }

    handleFinish() {      
        if (this.availableActions.find((action) => action === 'FINISH')) {
            const navigateFinishEvent = new FlowNavigationFinishEvent();
            this.dispatchEvent(navigateFinishEvent);
        }        
    }
}