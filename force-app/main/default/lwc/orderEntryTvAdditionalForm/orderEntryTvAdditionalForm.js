import { api, LightningElement, track } from 'lwc';
import { getConstants } from 'c/orderEntryConstants';
const CONSTANTS = getConstants();
export default class OrderEntryTvAdditionalForm extends LightningElement {
	@track _addons = [];
	@track _selectedAddons = [];
	@track mediaboxAddons = [];
	@track channelsAddons = [];
	@api
	get addons() {
		return this._addons;
	}
	set addons(value) {
		this._addons = Object.assign([], value);
	}
	@api
	get selectedAddons() {
		return this._selectedAddons;
	}
	set selectedAddons(value) {
		this._selectedAddons = Object.assign([], value);
		this.buildMediabox();
		this.buildToggles();
	}
	get mediaboxChunked() {
		return this.chunk(this.mediaboxAddons, 2);
	}
	get indexedMediaboxChunks() {
		return this.mediaboxChunked.map((c, i) => ({
			items: c,
			i
		}));
	}
	get channelsChunked() {
		return this.chunk(this.channelsAddons, 2);
	}
	get indexedChannelsChunks() {
		return this.channelsChunked.map((c, i) => ({
			items: c,
			i
		}));
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */
	@api validate() {
		const mediaboxInput = this.template.querySelector('lightning-input.mediabox');
		if (mediaboxInput) {
			mediaboxInput.reportValidity();
			return mediaboxInput.checkValidity();
		}
		return true;
	}

	chunk(arr, size) {
		return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) => arr.slice(i * size, i * size + size));
	}
	/**
	 * build media box
	 */
	buildMediabox() {
		this.mediaboxAddons = [];
		if (this.addons && this.addons.length) {
			this.addonsExist = true;
			const items = this.addons.filter((a) => a.Add_On__r.Type__c === CONSTANTS.TV_MEDIABOX_TYPE);
			const stateAddons = this.selectedAddons || [];
			this.mediaboxAddons = items.map((i) => {
				const stateAddon = stateAddons.find((sa) => sa.id === i.Add_On__r.Id);
				return {
					...i,
					quantity: stateAddon ? stateAddon.quantity : 0
				};
			});
		}
	}
	/**
	 * build toggles
	 */
	buildToggles() {
		this.channelsAddons = [];
		if (this.addons && this.addons.length) {
			this.addonsExist = true;
			const items = this.addons.filter((a) => a.Add_On__r.Type__c === CONSTANTS.TV_CHANNELS_TYPE);
			const stateAddons = this.selectedAddons || [];
			this.channelsAddons = items.map((i) => {
				const stateAddon = stateAddons.find((sa) => sa.id === i.Add_On__r.Id);
				return {
					...i,
					quantity: stateAddon ? stateAddon.quantity : 0
				};
			});
		}
	}

	/**
	 * handle mediabox change
	 * @param {*} e
	 */
	handleMediabox(e) {
		this.buildAddonData(CONSTANTS.TV_MEDIABOX_TYPE, e.target.name, parseInt(e.detail.value, 10));
	}
	/**
	 * handle tv channels change
	 * @param {*} e
	 */
	handleTvChannels(e) {
		this.toggleTvChannels(e.target.name, e.detail.checked);
	}
	/**
	 * toggle Tv Channels
	 * @param {*} id
	 * @param {*} value
	 */
	toggleTvChannels(id, value) {
		this.buildAddonData(CONSTANTS.TV_CHANNELS_TYPE, id, value === true ? 1 : 0);
	}

	/**
	 * build addon data and dispatch event
	 * @param {*} type
	 * @param {*} id
	 * @param {*} quantity
	 */
	buildAddonData(type, id, quantity) {
		const addonData = {
			id,
			quantity,
			type
		};
		this.dispatchEvent(new CustomEvent('changedaddon', { detail: addonData, bubbles: false }));
	}
}
