import { LightningElement, api } from "lwc";
export default class ModalPopupLWC extends LightningElement {
	@api isModalOpen = false;
	@api title;
	@api message;

	closeModal() {
		this.dispatchEvent(
			new CustomEvent("confirmation", { detail: { value: false }, bubbles: false })
		);
	}
	submitDetails() {
		this.dispatchEvent(
			new CustomEvent("confirmation", { detail: { value: true }, bubbles: false })
		);
	}
}