import { api, LightningElement, track, wire } from 'lwc';
import getAddons from '@salesforce/apex/OrderEntryProductController.getAddons';
import getAddonPrice from '@salesforce/apex/OrderEntryProductController.getAddonPrice';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';
import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();

export default class OrderEntryProductTelephony extends LightningElement {
	@wire(MessageContext)
	messageContext;
	@track selectedProducts = [];
	@api opportunityId;
	@track _products = [];
	@track _addons = [];
	@track _selectedProduct = {};
	@track _secondarySelectedProduct = {};
	@track _telephony;
	@track secondaryType = CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE;

	@api
	get products() {
		return this._products;
	}
	set products(value) {
		this._products = Object.assign([], value);
	}
	@api
	get addons() {
		return this._addons;
	}
	set addons(value) {
		this._addons = Object.assign([], value);
	}
	@api
	get selectedProduct() {
		return this._selectedProduct;
	}
	set selectedProduct(value) {
		this._selectedProduct = Object.assign({}, value);
		let index = this.selectedProducts.indexOf(
			this.selectedProducts.find((el) => el.id === this.selectedProduct?.id && el.type === this.selectedProduct?.type)
		);
		if (this.selectedProduct && index === -1 && value !== undefined) {
			this.selectedProducts.push(this.selectedProduct);
		} else if (this.selectedProduct && index > -1) {
			this.selectedProducts[index] = this.selectedProduct;
		}
	}
	@api
	get selectedAddons() {
		return this._selectedAddons;
	}
	set selectedAddons(value) {
		this._selectedAddons = Object.assign([], value);
	}
	@api
	get secondarySelectedProduct() {
		return this._secondarySelectedProduct;
	}
	set secondarySelectedProduct(value) {
		this._secondarySelectedProduct = Object.assign({}, value);
		let index = this.selectedProducts.indexOf(
			this.selectedProducts.find((el) => el.id === this.secondarySelectedProduct?.id && el.type === this.secondarySelectedProduct?.type)
		);
		if (this.secondarySelectedProduct && index === -1 && value !== undefined) {
			this.selectedProducts.push(this.secondarySelectedProduct);
		} else if (this.secondarySelectedProduct && index > -1) {
			this.selectedProducts[index] = this.secondarySelectedProduct;
		}
	}
	@api
	get telephony() {
		return this._telephony;
	}
	set telephony(value) {
		this._telephony = Object.assign({}, value);
	}
	@api validate() {
		let valid = true;
		this.template.querySelectorAll('c-order-entry-telephony-product-form').forEach((c) => {
			if (!c.validate()) {
				valid = false;
			}
		});

		return valid;
	}
	@api
	get telProducts() {
		return this.products.filter((p) => p.Type__c === CONSTANTS.TELEPHONY_PRODUCT_TYPE);
	}
	@api
	get secondaryDisabled() {
		return this.selectedProduct === undefined;
	}
	/**
	 * Dispatch changes on state to parent components
	 */
	dispatchStateChange() {
		this.dispatchEvent(new CustomEvent('statechanged', { detail: this._state, bubbles: false }));
	}

	async handleProductSelection(e) {
		let payloadProducts = [...this.selectedProducts];

		if (!e.detail.id) {
			if (e.detail.type === CONSTANTS.TELEPHONY_PRODUCT_TYPE) {
				payloadProducts = payloadProducts.filter(
					(p) => p.type !== CONSTANTS.TELEPHONY_PRODUCT_TYPE && p.type !== CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE
				);
				this._telephony = {};
			} else {
				payloadProducts = payloadProducts.filter((p) => p.type !== e.detail.type);
				delete this._telephony[e.detail.type];
			}
		} else {
			const productIndex = (this.selectedProducts || []).findIndex((p) => p.type === e.detail.type);

			if (productIndex === -1) {
				// if does not exist add it
				payloadProducts.push(e.detail);
			} else {
				// if exists update it
				payloadProducts[productIndex] = e.detail;
			}
		}

		this.selectedProducts = payloadProducts;
		if (e.detail.type === CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE) {
			this._secondarySelectedProduct = e.detail;
		} else {
			this._selectedProduct = e.detail;
		}
		// update products in payload clone
		await this.buildAddons(e.detail);
		this.publishProductChanged();
	}

	handleUpdatedSettings(e) {
		this._telephony = e.detail;
		this.publishProductChanged();
	}

	/**
	 * build addons
	 * @param {*} param0
	 */
	async buildAddons({ id, type }) {
		// rebuild list of selected addons and remove ones that are not related to selected telephony product
		const stateAddons = this.selectedAddons || [];
		const stateProducts = (this.selectedProducts || []).map((p) => p.id);
		const otherAddons = stateAddons.filter((a) => {
			if (type === CONSTANTS.TELEPHONY_PRODUCT_TYPE) {
				return stateProducts.indexOf(a.parentProduct) > -1;
			}
			return stateProducts.indexOf(a.parentProduct) > -1 && a.parentType !== type;
		});

		// get addons from api
		const newAddons = [];
		if (id) {
			const addons = await getAddons({ oppId: this.opportunityId, parentProduct: id });
			const automaticAddons = addons.filter((a) => {
				if (type === CONSTANTS.SECONDARY_TELEPHONY_PRODUCT_TYPE) {
					return a.Automatic_Add_On__c || a.Add_On__r.Type__c === CONSTANTS.TELEPHONY_LINE_TYPE;
				}
				return a.Automatic_Add_On__c;
			});

			for (const addon of automaticAddons) {
				// eslint-disable-next-line no-await-in-loop
				const builtAddon = await this.buildAddonData(addon, type);
				newAddons.push(builtAddon);
			}
		}
		this._selectedAddons = [...otherAddons, ...newAddons];
	}

	/**
	 * build addon data
	 * @param {*} addon
	 * @param {*} type
	 * @returns
	 */
	async buildAddonData(addon, type) {
		if (addon) {
			const quantity = 1;
			const price = await getAddonPrice({ code: addon.Add_On__r.Product_Code__c });
			const recurring =
				addon.Recurring_Price_Override__c || addon.Recurring_Price_Override__c === 0
					? addon.Recurring_Price_Override__c
					: price.cspmb__Recurring_Charge__c;
			const oneOff =
				addon.One_Off_Price_Override__c || addon.One_Off_Price_Override__c === 0
					? addon.One_Off_Price_Override__c
					: price.cspmb__One_Off_Charge__c;
			return {
				id: addon.Add_On__r.Id,
				quantity,
				type: addon.Add_On__r.Type__c,
				name: addon.Add_On__r.Name,
				parentProduct: addon.Product__c,
				code: addon.Add_On__r.Product_Code__c,
				recurring: recurring,
				oneOff: oneOff,
				totalRecurring: parseFloat((recurring * quantity).toFixed(2)),
				totalOneOff: parseFloat((oneOff * quantity).toFixed(2)),
				bundleName: price.Product_Name__c,
				parentType: type
			};
		}
		return null;
	}

	/**
	 * publis generic message
	 */
	publishProductChanged() {
		const msg = {
			sourceComponent: 'order-entry-telephony-products',
			payload: {
				event: 'product-changed',
				data: {
					primaryProduct: this.products.find((el) => el.Id === this.selectedProduct?.id),
					secondaryProduct: this.products.find((el) => el.Id === this.secondarySelectedProduct?.id),
					addons: this.selectedAddons,
					telephony: this.telephony
				}
			}
		};

		publish(this.messageContext, genericChannel, msg);
	}
}
