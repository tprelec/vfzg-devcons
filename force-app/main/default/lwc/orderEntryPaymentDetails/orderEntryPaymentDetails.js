import { api, LightningElement, wire, track } from 'lwc';
import getCountryIbanLength from '@salesforce/apex/OrderEntryPaymentController.getCountryIbanLength';
import createBillingAccount from '@salesforce/apex/OrderEntryPaymentController.createBillingAccount';
import { publish, MessageContext } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

export default class OrderEntryPaymentDetails extends LightningElement {
	@wire(MessageContext)
	messageContext;

	@api opportunityId;
	@api journeyType;
	@track _payment = {};
	@api
	get payment() {
		return this._payment;
	}
	set payment(value) {
		if (value) {
			this._payment = Object.assign({}, value);
		}
		if (this.journeyType === 'Mobile') {
			this._payment.paymentType = 'Direct Debit';
		}
	}
	get isMobile() {
		if (this.journeyType === 'Mobile') {
			return true;
		}
		return false;
	}
	paymentTypes = [
		{ label: 'Direct Debit', value: 'Direct Debit' },
		{ label: 'Bank Transfer', value: 'Bank Transfer' }
	];
	billingChannels = [
		{ label: 'Electronic Bill', value: 'Electronic Bill' },
		{ label: 'Paper Bill', value: 'Paper bill' }
	];

	@api async saveData() {
		this._payment = await createBillingAccount({ opportunityId: this.opportunityId });
		this.publishMessage();
	}

	handlePaymentChange(e) {
		let eventValue = e.target.value;
		if (e.target.dataset.id === 'iban') {
			eventValue = eventValue.toUpperCase();
		}
		this._payment[e.target.dataset.id] = eventValue;
		this.publishMessage();
		const ibanCode = this.payment.iban?.replace(/\s/g, '').toUpperCase();
		this.validateIban(ibanCode);
	}

	publishMessage() {
		const msg = {
			sourceComponent: 'order-entry-payment-details',
			payload: {
				event: 'payment-changed',
				data: {
					payment: this._payment
				}
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */

	@api async validate() {
		const postalCodeInput = this.template.querySelector('lightning-input.postal-code');
		const streetInput = this.template.querySelector('lightning-input.street');
		const houseNumberInput = this.template.querySelector('lightning-input.house-number');
		const cityInput = this.template.querySelector('lightning-input.city');
		const bankAccountHolderInput = this.template.querySelector('lightning-input.bank-account-holder');

		const ibanCode = this.payment.iban?.replace(/\s/g, '').toUpperCase();
		this.validateIban(ibanCode);

		postalCodeInput.reportValidity();
		streetInput.reportValidity();
		houseNumberInput.reportValidity();
		cityInput.reportValidity();
		bankAccountHolderInput.reportValidity();

		return (
			this.ibanAllTrue(ibanCode) &&
			bankAccountHolderInput.checkValidity() &&
			streetInput.checkValidity() &&
			houseNumberInput.checkValidity() &&
			cityInput.checkValidity() &&
			bankAccountHolderInput.checkValidity()
		);
	}

	async validateIban(ibanCode) {
		const ibanInput = this.template.querySelector('lightning-input.iban');

		if (this.ibanAllTrue(ibanCode)) {
			ibanInput.setCustomValidity('');
		} else if (
			!this.validatePaymentType(ibanCode) &&
			this.validateIbanFormat(ibanCode) &&
			this.validateIbanLength(ibanCode) &&
			this.validateIbanSanity(ibanCode)
		) {
			ibanInput.setCustomValidity('IBAN is required if Direct Debit is selected as Payment Type.');
		} else if (this.validatePaymentType(ibanCode)) {
			if (!this.validateIbanFormat(ibanCode)) {
				ibanInput.setCustomValidity('Invalid IBAN format.');
			} else if (!(await this.validateIbanLength(ibanCode))) {
				ibanInput.setCustomValidity('Invalid IBAN length.');
			} else if (!this.validateIbanSanity(ibanCode)) {
				ibanInput.setCustomValidity('IBAN sanity check failed.');
			}
		}
		ibanInput.reportValidity();
	}

	validatePaymentType(ibanCode) {
		if (!ibanCode?.length && this.payment.paymentType === 'Direct Debit') {
			return false;
		}
		return true;
	}

	validateIbanFormat(ibanCode) {
		if (ibanCode && ibanCode.length) {
			const rgx = new RegExp('^[A-Z]{2}[0-9]{2}[A-Z0-9]+$');

			if (ibanCode.length < 5 || !rgx.test(ibanCode)) {
				return false;
			}
		}
		return true;
	}

	async validateIbanLength(ibanCode) {
		if (ibanCode && ibanCode.length) {
			const countryCode = ibanCode.substring(0, 2);
			const countryIbanLength = await getCountryIbanLength({ countryCode: countryCode });

			if (countryIbanLength && countryIbanLength !== ibanCode.length) {
				return false;
			}
		}
		return true;
	}

	validateIbanSanity(ibanCode) {
		if (ibanCode && ibanCode.length) {
			const ibanCodeMap = this.createIbanCodeMap();
			const reformattedCode = ibanCode.substring(4) + ibanCode.substring(0, 4);
			const max = 999999999;
			let total = 0;
			let charValue;

			for (let i = 0; i < reformattedCode.length; i++) {
				charValue = ibanCodeMap.find((ibanCode) => ibanCode.char === reformattedCode.substring(i, i + 1)).code;
				total = (charValue > 9 ? total * 100 : total * 10) + charValue;

				if (total > max) {
					total = total % 97;
				}
			}
			if (total % 97 !== 1) {
				return false;
			}
		}
		return true;
	}

	ibanAllTrue(ibanCode) {
		return (
			this.validatePaymentType(ibanCode) &&
			this.validateIbanFormat(ibanCode) &&
			this.validateIbanLength(ibanCode) &&
			this.validateIbanSanity(ibanCode)
		);
	}

	createIbanCodeMap() {
		const allChars = [
			'0',
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'A',
			'B',
			'C',
			'D',
			'E',
			'F',
			'G',
			'H',
			'I',
			'J',
			'K',
			'L',
			'M',
			'N',
			'O',
			'P',
			'Q',
			'R',
			'S',
			'T',
			'U',
			'V',
			'W',
			'X',
			'Y',
			'Z'
		];
		let ibanCodeMap = [];

		allChars.forEach((c, index) => {
			ibanCodeMap.push({
				char: c,
				code: index
			});
		});

		return ibanCodeMap;
	}
}
