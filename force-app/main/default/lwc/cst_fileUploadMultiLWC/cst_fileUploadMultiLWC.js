// main Lightning methods
import { LightningElement, api, track, wire } from 'lwc';

// references to Apex methods
import createWorkLog from '@salesforce/apex/cst_FileUploadMultiController.createWorkLog';
import getTicketInfo from '@salesforce/apex/cst_FileUploadMultiController.getTicketInfo';
import getFiles from '@salesforce/apex/cst_FileUploadMultiController.getFiles';
import deleteFiles from '@salesforce/apex/cst_FileUploadMultiController.deleteFiles';

// to get Case.Id, in a QuickAction
import { CurrentPageReference } from 'lightning/navigation';

// methods for UI
import { CloseActionScreenEvent } from 'lightning/actions';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

// to load a static CSS resource called "sldsOverrides"
import sldsOverrides from '@salesforce/resourceUrl/sldsOverrides';

// method to load it on the DOM structure
import { loadStyle } from 'lightning/platformResourceLoader';

// const MAX_FILE_SIZE = 5000000;
// const MAX_FILE_SIZE = 5120; // dev
const MAX_UPLOAD_COUNT = 3;

export default class cst_fileUploadMultiLWC extends LightningElement {
	@api recordId;
	@track MAX_FILE_SIZE = 5242880;
	@track MAX_FILE_SIZE_display;
	@track MAX_UPLOAD_COUNT = 3;
	@track filesData = [];
	@track statusField;
	@track commentInput;
	@track defaultStatus;
	@track statusOptions;
	@track statusInput;
	@track ticketinfo;
	@track ticketid;
	@track showScreen2 = false;
	@track btnNext;
	@track btnSubmit;
	@track toggleHide;
	@track files = [];
	@track showSpinner = true;
	@track fileCount = 0;
	@track isUploadDisabled = false;
	@track isNextDisabled = false;
	@track isSbmitDisabled = false;
	@track statusTitle;
	@track statusError = {
		warningFileCount: false,
		warningFilesize: false,
		warningFileDeleting: false
	};
	value;

	/*
	 * @description getting Case.Id, either from the api lib (if this component is located in the layout) or from a page reference (if it's in a QuickAction). This is the starting point for the component to work
	 * @return JS Promise
	 */
	@wire(CurrentPageReference)
	getStateParameters(currentPageReference) {
		this.showSpinner = true;

		if (currentPageReference && this.recordId == undefined) {
			this.recordId = currentPageReference.state.recordId;
		}
		getTicketInfo({ caseId: this.recordId })
			.then((res) => {
				this.ticketinfo = JSON.parse(res);
				this.statusOptions = this.ticketinfo.statusLines;
				this.statusOptions.sort(this.sortingStatus);
				this.defaultStatus = this.statusOptions[0].value;
			})
			.then(() => {
				this.showSpinner = false;
			});
	}
	sortingStatus(a, b) {
		return a.index - b.index;
	}

	/*
	 * @description loading the extra CSS before the DOM page is rendered
	 * @return JS Promise
	 */
	connectedCallback() {
		this.showSpinner = false;
		console.log('recordId', this.recordId);
		loadStyle(this, sldsOverrides);
		this.MAX_FILE_SIZE_display = Math.floor(this.MAX_FILE_SIZE / 1000000);
	}

	/*
	 * @description we can scan the DOM for those elements, because this is aready rendered
	 * @return JS Promise
	 */
	renderedCallback() {
		this.commentInput = this.template.querySelector('.commentInput');
		this.statusInput = this.template.querySelector('.statusInput');
		this.btnNext = this.template.querySelector('#btnNext');
		this.btnSubmit = this.template.querySelector('#btnSubmit');
		this.toggleHide = this.template.querySelectorAll('.toggleHide');
		console.log('this.toggleHide', this.toggleHide);

		console.log('this.ticketinfo', JSON.stringify(this.ticketinfo));
		console.log('this.statusError', this.statusError);
		console.log('showScreen2', this.showScreen2);
		console.log('this.MAX_FILE_SIZE', this.MAX_FILE_SIZE);
		console.log('this.MAX_FILE_SIZE_display', this.MAX_FILE_SIZE_display);

		//if (this.showScreen2) {
		// this.updateFileStatus(this.filesData.length);
		//}
	}

	/*
	 * @description(handler for combobox selection change): updates the value in the dropbox,
	 * @return undefined
	 */
	handleStatusChange(event) {
		console.log('selected!');
		this.value = event.detail.value;
	}
	updateFileStatus(fileCount) {
		console.log('updateFileStatus');
		console.log('MAX_UPLOAD_COUNT', MAX_UPLOAD_COUNT);
		console.log('fileCount eval >', fileCount > MAX_UPLOAD_COUNT);
		console.log('fileCount eval >=', fileCount >= MAX_UPLOAD_COUNT);
		console.log('fileCount', fileCount);

		if (fileCount > MAX_UPLOAD_COUNT) {
			this.isSbmitDisabled = true;
			this.statusError.warningFileCount = true;
		} else {
			this.isSbmitDisabled = false;
			this.statusError.warningFileCount = false;
		}

		if (fileCount >= MAX_UPLOAD_COUNT) {
			this.isUploadDisabled = true;
		} else {
			this.isUploadDisabled = false;
		}

		this.statusError.warningFilesize = false;

		for (let f of this.filesData) {
			if (f.fileSize > this.MAX_FILE_SIZE) {
				console.log('fileSize toobig: ' + f.fileSize);
				console.log('this.MAX_FILE_SIZE: ' + this.MAX_FILE_SIZE);
				this.statusError.warningFilesize = true;
				this.isSbmitDisabled = true;
				break;
			}
		}
		if (this.statusError.warningFilesize) {
			//this.ShowToast('Warning', 'File size too big. The maximum supported filesize is 5 mb.', 'warning');
		}
		//if (this.statusError.warningFileCount) this.ShowToast('Warning',  `Too many files attached. The maximum supported number is ${MAX_UPLOAD_COUNT}.`, 'warning');

		console.log('isUploadDisabled', this.isUploadDisabled);
		console.log('isSbmitDisabled', this.isSbmitDisabled);
		console.table(this.statusError);
	}

	handleUploadFinished(event) {
		console.log('handleUploadFinished');
		this.showSpinner = true;

		let files = event.detail.files;

		console.log('files', { files });
		console.log('ticketinfo', this.ticketinfo);
		console.log('ticketid', this.ticketid);

		// let obj;
		let ids = [];

		for (let f of files) {
			console.log(f.name + ' ' + f.documentId + ' ' + f.contentVersionId + ' ' + f.contentBodyId);

			// obj = { name: f.name, documentid: f.documentId, contentversionid: f.contentVersionId, contentbodyid: f.contentBodyId, status: f.status, fileSize: f.fileSize };

			ids.push(f.documentId);

			// this.filesData.push(obj);
		}

		let strIds = JSON.stringify(ids);
		console.log('getFiles strIds ' + strIds);
		console.log('getFiles MAX_FILE_SIZE ' + this.MAX_FILE_SIZE);

		getFiles({ ids: strIds })
			.then((resp) => {
				console.log('getFiles resp: ' + resp);
				let jsonResp = JSON.parse(resp);

				console.log('getFiles jsonResp', jsonResp);
				console.log('getFiles files', jsonResp.files);

				let obj;

				for (let f of jsonResp.files) {
					obj = {
						name: f.name,
						name: f.title,
						documentid: f.contentdocumentid,
						contentversionid: f.contentVersionId,
						contentbodyid: f.contentBodyId,
						fileSize: f.fileSize
					};
					obj.status = 'fileBtnLabel removeImage';
					if (f.fileSize > this.MAX_FILE_SIZE) {
						obj.status += ' bigFile';
					}
					this.filesData.push(obj);
				}

				console.log('getFiles this.filesData 2', this.filesData);

				this.updateFileStatus(this.filesData.length);

				// this.updateFileStatus(this.filesData.length);
			})
			.then(() => {
				this.showSpinner = false;
			});
	}

	handleSubmit() {
		this.showSpinner = true;
		console.log('ticketinfo', this.ticketinfo);
		console.log('ticketid', this.ticketinfo.ticketid);

		this.ShowToast('WorkLog created', 'WorkLog was created and information successfully submitted.');
		this.handleCancel();
		this.updateRecordView();
	}
	handleHideElements() {
		console.log('toggleHide', this.toggleHide);
		this.toggleHide.forEach((element) => {
			element.classList.toggle('hidden');
		});
	}
	/*
	 * @description(handler for button click): and submission of component form
	 * @return undefined
	 */
	handleNext() {
		console.log('submitWorkLog');
		this.showSpinner = true;

		this.isNextDisabled = true;
		this.commentInput = this.template.querySelector('.commentInput');

		let comment = this.commentInput.value != '' ? this.commentInput.value : '';
		let status = this.statusInput.value != '' ? this.statusInput.value : '';
		console.log('comment', comment);
		console.log('status', status);

		// validating if a comment was included. This is the only required field
		if (!comment || comment == '') {
			this.ShowToast('Error', 'Please enter a comment.', 'warning');
			this.showSpinner = false;
			this.commentInput.classList.add('requiredField');
			this.commentInput.focus();
		} else {
			// all data is correct, call the apex method, with 4 parameters, using an imperative sintax, that returns a promise
			createWorkLog({
				recordId: this.recordId,
				comment: comment,
				status: status
			})
				// calling upon the returned promise
				.then((result) => {
					this.showSpinner = false;
					this.showScreen2 = true;
					this.handleHideElements();

					console.table(result);
					let respJSON = JSON.parse(result);
					console.table(respJSON);

					this.ticketinfo = respJSON;
					this.ticketid = this.ticketinfo.ticketid;

					console.table('ticketinfo', this.ticketinfo);
					console.log('ticketid', this.ticketinfo.ticketid);

					/*
                if(result && result != '') {
                    let msg = '';
                    
                    // adding information about uploaded files
                    if (this.filesData.length > 0) msg +=  ` ${this.filesData.length} file(s) Uploaded successfully`;
    
                    this.ShowToast('WorkLog created', 'Your WorkLog was created successfully.');
                    
                } else {
                    this.ShowToast('WorkLog not created',  'External Ticket not submitted. Please check error message.', 'error');
                }
                */

					// called if there is an error from Apex
				})
				.catch((error) => {
					if (error && error.body && error.body.message) {
						this.ShowToast('WorkLog not created', 'External Ticket not submitted. Please check error message.', 'error');
					}
					// after all the promises, close the screen
				})
				.finally(() => {
					// this.handleCancel();
					// this.updateRecordView();
				});
		}
	}
	/*
	 * @description (handler for button click): to remove an image, from Upload button
	 * @return undefined
	 */
	handleDeleteFiles(event) {
		this.showSpinner = true;
		// console.log('dataset', event.currentTarget.dataset.name);
		console.log('handleDeleteFiles');
		console.log('currentTarget', event.currentTarget);
		console.log('documentid', event.currentTarget.dataset.documentid);

		let info = JSON.parse(JSON.stringify(event.currentTarget));
		console.log('info', { info });
		console.log('name', event.currentTarget.title);
		console.log('this.filesData');
		console.dir(this.filesData);

		// event.currentTarget.parentNode.removeChild(event.currentTarget);

		let docId = event.currentTarget.dataset.documentid;

		// event.currentTarget.parentNode.removeChild(event.currentTarget);

		for (let i = this.filesData.length - 1; i >= 0; i--) {
			let dataElem = this.filesData[i];

			console.log('dataElem', dataElem);

			if (dataElem.documentid == docId) {
				console.log('docId', docId);
				console.log('eId', dataElem.documentid);
				console.log('eId == docId', dataElem.documentid == docId);
				console.log('eId != docId', dataElem.documentid != docId);
				this.filesData.splice(i, 1);
			}
		}

		console.log('docId', docId);

		deleteFiles({ recordid: docId }).then((resp) => {
			this.showSpinner = false;
			console.log('handleDeleteFiles deleteFiles resp');
			console.log('deleteFiles resp', resp);
			console.log('deleteFiles docId', docId);
			console.log('deleteFiles parent', parent);

			console.log('deleteFiles handleDeleteFiles filesData', this.filesData);

			let jsonResp = JSON.parse(resp);
			console.log('deleteFiles jsonResp', { jsonResp });

			console.log('deleteFiles status', jsonResp.status);

			this.statusTitle = jsonResp.title;
			this.statusError.warningFileDeleting = jsonResp.status != 'success' ? true : false;
			this.updateFileStatus(this.filesData.length);

			// this.updateFileStatus(this.filesData.length);
		});
	}

	/*
	 * @description getter for "acceptedFormats"propperty
	 */
	get acceptedFormats() {
		return ['.pdf', '.png', '.gif', '.jpg', '.jpeg', '.txt', '.xls', '.xlsx', '.docx', '.doc'];
	}

	/*
    * @description (handler for button click): to close current component, canceling submission
    * @return undefined
    * 
            if (this.statusError.findIndex((n) => n=='warningFilesize' ) != (-1)  ) {
                this.ShowToast('Warning', 'File size too big. The maximum supported filesize is 5 mb.', 'warning');
            }
            if (this.statusError.findIndex((n) => n == 'warningFileCount') != (-1)) this.ShowToast('Warning',  `Too many files attached. The maximum supported number is ${MAX_UPLOAD_COUNT}.`, 'warning');

    */
	configToasts(error) {
		let config = {
			warningFilesize: {
				title: 'Warning',
				variant: 'warning',
				message: 'File size too big. The maximum supported filesize is 5 mb.'
			},
			warningFileCount: {
				title: 'Warning',
				variant: 'warning',
				message: `Too many files attached. The maximum supported number is ${MAX_UPLOAD_COUNT}.`
			}
		};
		this.dispatchEvent(
			new ShowToastEvent({
				title: config[error].title,
				variant: config[error].variant,
				message: config[error].message
			})
		);
	}
	handleCancel() {
		console.log('handleCancelCleadData');
		const closeTabEvent = new CustomEvent('closeTabEvent', {
			detail: { close }
		});
		this.dispatchEvent(closeTabEvent);
		this.dispatchEvent(new CloseActionScreenEvent());
		this.updateRecordView();
	}
	/*
	 * @description (aux method): update Recordpage
	 * @return undefined
	 */
	ShowToast(title, message, variant) {
		if (!variant) variant = 'success';

		this.dispatchEvent(
			new ShowToastEvent({
				title: title,
				variant: variant,
				message: message
			})
		);
	}

	/*
	 * @description (aux method): after form submission, force refresing of browser window
	 * @return undefined
	 */
	updateRecordView(title, message) {
		this.filesData = [];
		setTimeout(() => {
			this.dispatchEvent(new CloseActionScreenEvent());
			eval("$A.get('e.force:refreshView').fire();");
		}, 500);
	}
}
