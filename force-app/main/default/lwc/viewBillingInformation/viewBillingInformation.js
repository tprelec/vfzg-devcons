import { LightningElement, api } from 'lwc';
import { CloseActionScreenEvent } from 'lightning/actions';
import modal from '@salesforce/resourceUrl/EMP_ModalCSS';
import { loadStyle } from 'lightning/platformResourceLoader';

export default class ViewBillingInformation extends LightningElement {
	@api recordId;

	connectedCallback() {
		Promise.all([loadStyle(this, modal)]);
	}

	handleCancel() {
		this.dispatchEvent(new CloseActionScreenEvent());
	}
}
