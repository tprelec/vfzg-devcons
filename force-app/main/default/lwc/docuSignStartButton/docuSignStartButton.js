import { LightningElement, api, track } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import resolveDocuSignFieldsOnOpportunity from '@salesforce/apex/DocuSignButtonController.resolveDocuSignFieldsOnOpportunity';
import isDocuSignButtonVisible from '@salesforce/apex/DocuSignButtonController.isDocuSignButtonVisible';
import prepareOpportunityAndCreateEnvelopeURL from '@salesforce/apex/DocuSignButtonController.prepareOpportunityAndCreateEnvelopeURL';
import lblPPCommunityBaseURL from '@salesforce/label/c.pp_Community_Base_URL';

export default class DocuSignStartButton extends NavigationMixin(LightningElement) {
	@api recordId;
	@api buttonName;
	componentVisible;
	@track errors = [];
	@track isLoading = false;
	@track openModal = false;
	@track message = '';

	connectedCallback() {
		isDocuSignButtonVisible({ opportunityId: this.recordId })
			.then((result) => {
				let inAppBuilder = this.inAppBuilder();
				this.componentVisible = inAppBuilder ? true : result;
			})
			.catch((error) => this.handleError(error));
	}

	inAppBuilder() {
		let urlToCheck = window.location.pathname.toLowerCase();
		return urlToCheck.indexOf('flexipageeditor') >= 0;
	}

	handleConfirmation(e) {
		this.openModal = false;
		if (e.detail.value) {
			this.isLoading = true;
			console.log('this.recordId: ' + this.recordId);

			prepareOpportunityAndCreateEnvelopeURL({
				oppyId: this.recordId,
				skipResolvingDocusignOppyFields: true
			})
				.then((result) => {
					const refToDocusignPage = result;
					console.log('refToDocusignPage: ' + refToDocusignPage);

					this[NavigationMixin.GenerateUrl]({
						type: 'standard__webPage',
						attributes: {
							url: refToDocusignPage
						}
					}).then((generatedUrl) => {
						let pageRedirect = generatedUrl;
						if (window.location.href.includes(lblPPCommunityBaseURL)) {
							pageRedirect = lblPPCommunityBaseURL + generatedUrl;
						}
						console.log('redirecting to: ' + pageRedirect);
						window.open(pageRedirect);
					});
					this.isLoading = false;
				})
				.catch((error) => {
					this.handleError(error);
					const evt = new ShowToastEvent({
						title: 'Application Error',
						message: error.body.message,
						variant: 'error',
						mode: 'dismissable'
					});
					this.dispatchEvent(evt);
				});
		}
	}

	handleError(error) {
		this.isLoading = false;
		const errorToAdd = JSON.parse(JSON.stringify(error)); // Because the original is immutable
		errorToAdd.index = this.errors.length.toString();
		this.errors = [...this.errors, errorToAdd];

		console.log('this.errors: ' + JSON.stringify(this.errors));
	}

	handleClick() {
		this.isLoading = true;
		this.errors = [];
		//TODO: disable docusign button here
		console.log('Resolving DocuSign fields on opportunity: ' + this.recordId);
		resolveDocuSignFieldsOnOpportunity({ oppyId: this.recordId })
			.then((result) => {
				this.isLoading = false;
				//TODO: enable docusign button here
				let oppyDocuSignData = JSON.parse(result);
				console.log('Response from apex/DocuSignButtonController.resolveDocuSignFieldsOnOpportunity: ' + JSON.stringify(oppyDocuSignData));
				this.message = this.assembleConfirmationBoxMessage(oppyDocuSignData);
				this.openModal = true;
			})
			.catch((error) => this.handleError(error));
	}

	assembleConfirmationBoxMessage(oppyDocuSignData) {
		let response = `<p>The system will now send DocuSign contract to recipients in the following order:<ol>`;
		if (oppyDocuSignData.DS_Authorized_Internal_Signer_1_Name != null) {
			response += `<li>${oppyDocuSignData.DS_Authorized_Internal_Signer_1_Name} - ${oppyDocuSignData.DS_Authorized_Internal_Signer_1_Mail}</li>`;
		}

		if (oppyDocuSignData.DS_Authorized_Internal_Signer_2_Name__c != null) {
			response += `<li>${oppyDocuSignData.DS_Authorized_Internal_Signer_2_Name__c} - ${oppyDocuSignData.DS_Authorized_Internal_Signer_2_Mail__c}</li>`;
		}

		if (oppyDocuSignData.DS_Authorized_to_sign_1st_Name__c != null) {
			response += `<li>${oppyDocuSignData.DS_Authorized_to_sign_1st_Name__c} - ${oppyDocuSignData.DS_Authorized_to_sign_1st_Mail__c}</li>`;
		}

		if (oppyDocuSignData.DS_Authorized_to_sign_2nd_Name__c != null) {
			response += `<li>${oppyDocuSignData.DS_Authorized_to_sign_2nd_Name__c} - ${oppyDocuSignData.DS_Authorized_to_sign_2nd_Mail__c}</li>`;
		}
		response += `</ol><p>OK to proceed?</p>`;

		return response;
	}
}
