import { api, LightningElement } from 'lwc';
import FORM_FACTOR from '@salesforce/client/formFactor';
import getAllDashboards from '@salesforce/apex/DashboardListController.getAllDashboards';
import totalDashboards from '@salesforce/apex/DashboardListController.getTotalDashboards';
import getAllDashboardFolders from '@salesforce/apex/DashboardListController.getAllDashboardFolders';

export default class VzDashboardsList extends LightningElement {
	@api
	title;

	@api
	icon;

	@api
	recordsPerPage;
	offset = 0;
	totalRecords;
	loadInProgress = true;
	@api
	dashboardUri;
	selectedFolder = '';
	exception = null;

	get isDesktop() {
		return FORM_FACTOR === 'Large';
	}

	get hasError() {
		return this.exception != null;
	}

	get errorMessage() {
		return `Something went wrong with your request, please send this code (${this.exception.status}) to your System Administrator`;
	}

	get _dashboardUri() {
		return this.dashboardUri;
	}

	get _title() {
		return this.title;
	}

	get _icon() {
		return this.icon;
	}

	get hasMoreItems() {
		return this.dashboards.length < this.totalRecords;
	}

	get hasDashboards() {
		return this.dashboards.length > 0;
	}

	get options() {
		let folderOptions = [{ label: 'All Folders', value: '' }].concat(
			this.folders.map((folder) => {
				return { label: folder.Name, value: folder.Id };
			})
		);
		return folderOptions;
	}

	folders = [];
	dashboards = [];
	tempDashboardList = [];

	connectedCallback() {
		this.getTotalDashboards();
		this.getDashboardList();
		getAllDashboardFolders()
			.then((result) => {
				this.folders = result;
			})
			.catch((error) => {
				this.exception = error;
				console.error('error: %O', error);
			})
			.finally(() => {
				this.loadInProgress = false;
			});
	}

	loadMore() {
		this.loadInProgress = true;
		this.offset += parseInt(this.recordsPerPage, 10);
		this.getDashboardList();
	}

	handleFolderChange(event) {
		this.loadInProgress = true;
		this.offset = 0;
		this.tempDashboardList = [];
		this.dashboards = [];
		this.selectedFolder = event.detail.value;
		this.getTotalDashboards();
		this.getDashboardList();
	}

	getDashboardList() {
		getAllDashboards({ queryLimit: this.recordsPerPage, offset: this.offset, folderId: this.selectedFolder })
			.then((result) => {
				this.tempDashboardList = [...this.dashboards];
				this.tempDashboardList.push(...result);
				this.dashboards = this.tempDashboardList;
				this.loadInProgress = false;
			})
			.catch((error) => {
				this.exception = error;
				console.error('error: %O', error);
			})
			.finally(() => {
				this.loadInProgress = false;
			});
	}

	getTotalDashboards() {
		totalDashboards({ folderId: this.selectedFolder })
			.then((result) => {
				this.totalRecords = result;
			})
			.catch((error) => {
				this.exception = error;
				console.error('error: %O', error);
			})
			.finally(() => {
				this.loadInProgress = false;
			});
	}
}
