import { api, LightningElement, track } from 'lwc';
import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();

export default class OrderEntryInternetProductForm extends LightningElement {
	@track isRendered = true;
	@track selectedAddonsForReturn = [];
	@track product;
	@track _contractTerm;
	@track selectedSecurityAddonId;
	@track _products = [];
	@track _addons = [];
	@track _selectedAddons = [];
	@track _selectedProduct = {};
	@track pinZeker = false;
	@track contractTerms = [
		{ label: '1 year', value: '1' },
		{ label: '2 years', value: '2' },
		{ label: '3 years', value: '3' }
	];
	@api
	get contractTerm() {
		return this._contractTerm;
	}
	set contractTerm(value) {
		if (this._contractTerm !== value) {
			this._contractTerm = value;
		}
	}
	@api
	get products() {
		return this._products;
	}
	set products(value) {
		if (Object.assign([], value).length && JSON.stringify(this._products) !== JSON.stringify(value)) {
			this._products = Object.assign([], value);
		}
	}
	@api
	get addons() {
		return this._addons;
	}
	set addons(value) {
		if (Object.assign([], value).length && JSON.stringify(this._addons) !== JSON.stringify(value)) {
			this._addons = Object.assign([], value);
		}
	}
	@api
	get selectedAddons() {
		return this._selectedAddons;
	}
	set selectedAddons(value) {
		if (Object.assign([], value).length && JSON.stringify(this._selectedAddons) !== JSON.stringify(value)) {
			this._selectedAddons = Object.assign([], value);
			this.selectedAddonsForReturn.length = 0;
			this.pinZeker = this.selectedAddons.find((el) => el.type === CONSTANTS.PIN_ZEKER_TYPE);
			if (this.selectedAddonsForReturn.length === 0) {
				this._selectedAddons.forEach((element) => {
					this.selectedAddonsForReturn.push({
						id: element.id,
						quantity: 1,
						type: element.type
					});
				});
			}
		}
	}
	@api
	get selectedProduct() {
		return this._selectedProduct;
	}
	set selectedProduct(value) {
		let tempValue = Object.assign({}, value);
		if (tempValue && Object.keys(tempValue).length !== 0 && Object.assign({}, this._selectedProduct).id !== Object.assign({}, value).id) {
			this._selectedProduct = Object.assign({}, value);
			this.pinZeker = false;
		}
	}

	@api
	get selectedPoductId() {
		return this._selectedProduct?.id;
	}
	get internetSecurityAddons() {
		return this._addons
			.filter((a) => a.Add_On__r.Type__c === CONSTANTS.INTERNET_SECURITY_TYPE)
			.map((a) => ({
				value: a.Add_On__r.Id,
				label: a.Add_On__r.Name
			}));
	}
	get pinZekerAddon() {
		return this._addons.find((a) => a.Add_On__r.Type__c === CONSTANTS.PIN_ZEKER_TYPE);
	}
	get internetSecurityAddonsShow() {
		return this.internetSecurityAddons.length > 0;
	}
	get selectedSecurityAddon() {
		return this.selectedAddons.find((a) => a?.type === CONSTANTS.INTERNET_SECURITY_TYPE)?.id;
	}
	get formattedProducts() {
		return this.products.map((p) => ({
			value: p.Id,
			label: p.Name
		}));
	}
	connectedCallback() {
		this.isRendered = false;
	}
	renderedCallback() {
		if (this.isRendered || (this._selectedProduct && !this.selectedProduct.id) || this.selectedAddons.length === 0) {
			return;
		}
		this.submitChange('rendered', true);
	}

	/**
	 * checking input data
	 * @returns boolean value
	 */
	@api validate() {
		const productInput = this.template.querySelector('lightning-combobox.internet-product');
		productInput.reportValidity();
		const isProductValid = productInput.checkValidity();

		const termInput = this.template.querySelector('lightning-combobox.contract-term');
		termInput.reportValidity();
		const isTermValid = termInput.checkValidity();

		return isProductValid && isTermValid;
	}

	/**
	 * handle product change
	 * @param {*} e
	 */
	handleProductChange(e) {
		if (e.detail.value && e.detail.value !== this.selectedProduct.id) {
			this._selectedProduct = { id: e.detail.value, type: CONSTANTS.INTERNET_PRODUCT_TYPE };
			this.submitChange(
				'productchange',
				this._products.find((el) => el.Id === this.selectedPoductId)
			);
		}
	}

	/**
	 * handle contract term change
	 * @param {*} e
	 */
	handleContractTermChange(e) {
		this._contractTerm = e.detail.value;
		this.submitChange('contracttermchange', this.contractTerm);
	}

	/**
	 * handle selected security change
	 * @param {*} e
	 */
	handleSelectedSecurity(e) {
		this.selectedSecurityAddonId = e.detail.value;

		this.submitChange('internetaddonchange', {
			type: CONSTANTS.INTERNET_SECURITY_TYPE,
			id: e.detail.value,
			quantity: 1
		});
	}

	/**
	 * handle pinzeler change
	 * @param {*} e
	 */
	handlePinZeker(e) {
		this.pinZeker = e.detail.checked;
		this.submitChange('pinzekerchange', {
			type: CONSTANTS.PIN_ZEKER_TYPE,
			id: this.pinZekerAddon.Add_On__r.Id,
			quantity: e.detail.checked ? 1 : 0
		});
	}

	/**
	 * dispatch event
	 * @param {*} name
	 * @param {*} detail
	 */
	submitChange(name, detail) {
		this.dispatchEvent(new CustomEvent(name, { detail, bubbles: false }));
	}
}
