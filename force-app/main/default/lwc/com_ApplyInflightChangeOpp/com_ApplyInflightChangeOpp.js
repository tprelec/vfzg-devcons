import { api, LightningElement, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
import getBasketId from '@salesforce/apex/COM_ApplyInflightChangeController.getProductBasketId';
import performAction from '@salesforce/apex/COM_ApplyInflightChangeController.performAction';

import LabelAppliedSuccessfuly from '@salesforce/label/c.COM_ApplyInflightChangeOpp_SuccessfullyApplied';
import LabelGetBasketError from '@salesforce/label/c.COM_ApplyInflightChangeOpp_GetBasketError';

export default class Com_ApplyInflightChangesOpp extends LightningElement {
	@api recordId;
	basketId;
	isLoading;

	@wire(getBasketId, { recordId: '$recordId' })
	wiredBasket({ error, data }) {
		console.log('wire started');
		this.isLoading = true;
		if (data) {
			this.basketId = data;
			this.isLoading = false;
		} else if (error) {
			console.log(error.body.message);
			this.showToast(LabelGetBasketError, 'error', 'sticky');
			this.dispatchEvent(new CloseActionScreenEvent());
		}
	}

	async applyInflightChanges() {
		try {
			await performAction({ basketId: this.basketId });
			this.showToast(LabelAppliedSuccessfuly, 'success');
			this.dispatchEvent(new CloseActionScreenEvent());
		} catch (error) {
			this.showToast(error.body.message, 'error', 'sticky');
			this.dispatchEvent(new CloseActionScreenEvent());
		}
	}

	handleCancel() {
		this.dispatchEvent(new CloseActionScreenEvent());
	}

	handleConfirm() {
		this.isLoading = true;
		this.applyInflightChanges();
	}

	showToast(message, variant, mode = 'dismissible') {
		const event = new ShowToastEvent({
			message: message,
			variant: variant,
			mode: mode
		});
		this.dispatchEvent(event);
	}
}
