import { api, LightningElement } from 'lwc';
import { subscribe, unsubscribe, onError } from 'lightning/empApi';
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
export default class Com_opportunityCloseWonRedirector extends NavigationMixin(LightningElement) {
	@api opportunityId;
	@api errorMessage;
	@api successMessage;

	channelName = '/event/O_S_Order_Generated__e';
	subscription = {};
	isLoading = true;
	hasError = false;
	orderId;
	isRendered = false;

	renderedCallback() {
		if (this.isRendered) {
			return;
		}
		// console.log("====renderedCallback");
		this.isRendered = true;
		this.registerErrorListener();
		this.subscribeToEvent();
	}

	disconnectedCallback() {
		// console.log("====disconnectedCallback");
		this.unsusbscribeToEvent();
	}

	subscribeToEvent() {
		// console.log("====subscribeToEvent");
		// Callback invoked whenever a new event message is received
		const messageCallback = (response) => {
			console.log('====New message received: ', JSON.stringify(response));
			// Response contains the payload of the new message received

			const payload = response.data.payload;
			const _opportunitId = payload.Opportunity_Id__c;
			const isSuccess = payload.IsSuccessful__c;
			const _orderId = payload.Order_Id__c;

			// console.log("payload", JSON.stringify(payload));
			console.log('_opportunitId', _opportunitId, this.opportunityId);

			// match the opportunityId from platform event with current opportunityId
			if (_opportunitId === this.opportunityId) {
				// unsusbscribe to more events for same opportunity when a valid one is received
				this.unsusbscribeToEvent();
				// console.log("_orderId", _orderId, isSuccess);
				if (_orderId && isSuccess) {
					// console.log("successMessage", this.successMessage);
					this._showToast('success', this.successMessage);
					this.navigateToRecord(_orderId);
				} else {
					this.isLoading = false;
					this.hasError = true;
				}
			}
		};

		// Invoke subscribe method of empApi. Pass reference to messageCallback
		subscribe(this.channelName, -1, messageCallback).then((response) => {
			console.log('====subscribe: ');
			// Response contains the subscription information on subscribe call
			this.subscription = response;
			console.log('this.subscription: ', JSON.stringify(this.subscription));
		});
	}

	unsusbscribeToEvent() {
		console.log('====unsusbscribeToEvent');
		// console.log("this.subscription: ", JSON.stringify(this.subscription));
		// Invoke unsubscribe method of empApi
		unsubscribe(this.subscription, (response) => {
			// Response is true for successful unsubscribe
			console.log('====unsubscribe() response: ', JSON.stringify(response));
		});
	}

	registerErrorListener() {
		// console.log("====registerErrorListener");
		// Invoke onError empApi method
		onError((error) => {
			// Error contains the server-side error
			console.log('====Received error from server: ', JSON.stringify(error));
			this.isLoading = false;
			this.hasError = true;
		});
	}

	_showToast(variant, message) {
		const event = new ShowToastEvent({
			variant: variant,
			message: message
		});
		this.dispatchEvent(event);
	}

	navigateToRecord(recordId) {
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: recordId,
				actionName: 'view'
			}
		});
	}
}
