import { LightningElement, api, wire, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { CloseActionScreenEvent } from 'lightning/actions';
import { CurrentPageReference } from 'lightning/navigation';

// reference to static CSS, called sldsOverrides
import sldsOverrides from '@salesforce/resourceUrl/sldsOverrides';

// method to load it on the DOM structure
import { loadStyle } from 'lightning/platformResourceLoader';

// apex methods
import getQuestionnaireLineItems from '@salesforce/apex/CST_Questionnaire_Handler.getQuestionnaireLineItems';
import updateLineItems from '@salesforce/apex/CST_QuestionBank_Handler.updateLineItems';

// custom labels
import questionnaireSuccessTitle from '@salesforce/label/c.CST_QuestionnaireSuccess_Title';
import questionnaireSuccessMessage from '@salesforce/label/c.CST_QuestionnaireSuccess_Message';

// loading the component as a JS module
export default class cst_editQuestionnaire extends LightningElement {
	@api recordId; // finding with the api interface, the Case.Id matching this record
	@track lineItemsList; // list of aswners to be edited

	// call to an wired method
	@wire(CurrentPageReference)
	getStateParameters(currentPageReference) {
		if (currentPageReference && this.recordId == undefined) {
			this.recordId = currentPageReference.state.recordId;
		}
	}

	// Called when the invoked methods return results or errors. Repeted for every answer
	// include the found recordId in an object, as parameter
	connectedCallback() {
		if (this.recordId) {
			getQuestionnaireLineItems({ questionnaireId: this.recordId })
				.then((result) => {
					if (result.length == 0) {
						// for an empty set of results
						this.fireToastMessage('No questions available!', 'This questionnaire does not have any questions to edit.', 'error');
						this.closeScreen();
					} else {
						// if there are results, compile them in this array and add them to this.lineItemsList
						var lineItemsListAux = [];
						for (var i = 0; i < result.length; i++) {
							var updatedAnswer;
							var booleanValue;
							if (result[i].CST_QuestionType__c == 'checkbox') {
								updatedAnswer = result[i].CST_Answer__c == 'Yes' ? true : false;
								booleanValue = updatedAnswer;
							} else {
								updatedAnswer = result[i].CST_Answer__c;
								booleanValue = false;
							}
							var isRequired = result[i].CST_Mandatory__c ? true : false;
							lineItemsListAux.push({
								answer: updatedAnswer,
								question: result[i].CST_Question__c,
								questionType: result[i].CST_QuestionType__c,
								isChecked: booleanValue,
								isMandatory: isRequired,
								id: result[i].Id,
								helptext: result[i].CST_Questionaire_HelpText__c
							});
						}
						this.lineItemsList = lineItemsListAux;
					}
				})
				.then((response) => {
					// loading CSS from Static File
					loadStyle(this, sldsOverrides);
				})
				.catch((error) => {
					// any errors in the promisses callout, are caught here
					console.log('error -> ' + JSON.stringify(error));
					this.fireToastMessage(
						'Something went wrong',
						'Please check with the administrator the error: ' + JSON.stringify(error) + '.',
						'error'
					);
					this.closeScreen();
				});
		}
	}

	get id() {
		return this.record.Id;
	}

	handleSubmit() {
		const isInputsCorrect = [...this.template.querySelectorAll('lightning-input')].reduce((validSoFar, inputField) => {
			inputField.reportValidity();
			return validSoFar && inputField.checkValidity();
		}, true);
		if (isInputsCorrect) {
			this.submitQuestionnaire();
		}
	}

	// invoked from the button, to send answers to Apex
	submitQuestionnaire() {
		var answerCollection = [];
		var answerCollectionDOM = this.template.querySelectorAll('lightning-input');

		// collecting aswers from <lightning-input> fields
		answerCollectionDOM.forEach((question) => {
			// convert boolean datatype to Yes / No
			var questionValue;
			if (question.type == 'checkbox') {
				questionValue = question.checked ? 'Yes' : 'No';
			} else {
				questionValue = question.value;
			}

			var answer = {
				CST_Question__c: question.label,
				CST_Answer__c: questionValue,
				Id: question.name,
				CST_Mandatory__c: question.required,
				CST_Questionaire_HelpText__c: question.fieldLevelHelp
			};

			// add each to the array
			answerCollection.push(answer);
		});

		//
		updateLineItems({ answers: answerCollection })
			.then((response) => {
				// create toaster message as feedback to user
				this.toaster = new ShowToastEvent({
					title: 'Questionnaire updated!', // create new custom label CST_UpdateQuestionnaire_Title
					message: 'Your questionnaire was updated successfully', // create new custom label CST_UpdateQuestionnaire_Message
					variant: 'success'
				});
				this.updateRecordView();
				this.dispatchEvent(this.toaster);
				this.closeScreen();
			})
			.catch((error) => {
				console.log('error');
				console.dir(error);
			});
	}

	get id() {
		return this.record.Id;
	}

	// auxiliary methods
	closeScreen() {
		const closeTabEvent = new CustomEvent('closeTabEvent', {
			detail: { close }
		});
		// Fire the custom event
		this.dispatchEvent(closeTabEvent);

		this.dispatchEvent(new CloseActionScreenEvent());
	}

	fireToastMessage(title, message, variant) {
		const toaster = new ShowToastEvent({
			title: title,
			message: message,
			variant: variant
		});
		this.dispatchEvent(toaster);
	}
	updateRecordView() {
		setTimeout(() => {
			this.closeScreen();
			eval("$A.get('e.force:refreshView').fire();");
		}, 1000);
	}
}
