import { LightningElement, api, wire } from "lwc";
import getResult from "@salesforce/apex/O2C_OrderValidation.getResult";

export default class OrderValidation extends LightningElement {
    @api recordId;

    errorMessage;
    result;

    @wire(getResult, {
        recordId: "$recordId"
    })
    wiredResult({ error, data }) {
        if (data) {
            this.errorMessage = undefined;
            this.result = data.body;
            if (data.message) {
                this.errorMessage = data.message;
            }
        } else if (error) {
            this.result = [];
            this.errorMessage = error.body.message;
        }
    }

    get hasRecordId() {
        return !!this.recordId;
    }

    get errors() {
        return this.result;
    }

    get isSuccess() {
        return this.result?.length === 0 && !this.errorMessage;
    }
}