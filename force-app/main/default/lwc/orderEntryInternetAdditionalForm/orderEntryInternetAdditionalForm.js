import { api, LightningElement, track } from 'lwc';
import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();

export default class OrderEntryInternetAdditionalForm extends LightningElement {
	@track isRendered = true;
	@track isValid = true;
	@track _selectedAddons;
	@track _addons = [];
	@track additionalAddons = [];
	@api
	get selectedAddons() {
		return this._selectedAddons;
	}
	set selectedAddons(value) {
		if (value && JSON.stringify(this._selectedAddons) !== JSON.stringify(value)) this._selectedAddons = Object.assign([], value);
		this.buildInputs();
	}
	@api
	validate() {
		return this.isValid;
	}
	@api
	get addons() {
		return this._addons;
	}
	set addons(value) {
		this._addons = Object.assign([], value);
	}
	get addonsChunked() {
		return this.chunk(this.additionalAddons, 2);
	}
	get indexedChunks() {
		return this.addonsChunked.map((c, i) => ({
			items: c,
			i
		}));
	}

	chunk(arr, size) {
		return Array.from({ length: Math.ceil(arr.length / size) }, (v, i) => arr.slice(i * size, i * size + size));
	}

	/**
	 * build inputs
	 */
	buildInputs() {
		if (this.addons && this.addons.length) {
			let items = [];
			items = this.addons.filter((a) => a.Add_On__r.Type__c === CONSTANTS.INTERNET_ADDITIONAL_TYPE);
			const stateAddons = this.selectedAddons || [];
			this.additionalAddons = [];

			this.additionalAddons = items.map((i) => {
				let stateAddon = stateAddons.find((sa) => sa.id === i.Add_On__r.Id);

				if (stateAddon) {
					return {
						...i,
						quantity: stateAddon.quantity ? stateAddon.quantity : 0
					};
				}

				return {
					...i,
					quantity: 0
				};
			});
		}
	}

	async connectedCallback() {
		this.isRendered = false;
	}
	renderedCallback() {
		if (this.isRendered || this.selectedAddons.length < 0) {
			return;
		}
		this.isRendered = true;
		this.dispatchEvent(new CustomEvent('rendered', { detail: true, bubbles: false }));
	}

	/**
	 * handle input change
	 * @param {*} e
	 */
	handleInputChange(e) {
		e.target.setCustomValidity('');
		e.target.checkValidity();

		let limit = 0;
		const selectedAddOnInput = this._addons.filter((a) => a.Add_On__r.Id === e.target.name);

		if (selectedAddOnInput[0].Add_On__r.hasOwnProperty('Max_Quantity__c')) {
			limit = selectedAddOnInput[0].Add_On__r.Max_Quantity__c;
		}

		if (limit === 0 || parseInt(e.detail.value ? e.detail.value : 0, 10) <= limit) {
			this.buildAddonData(e.target.name, parseInt(e.detail.value ? e.detail.value : 0, 10));
			this.isValid = true;
		} else {
			this.isValid = false;
			e.target.setCustomValidity(`Max Quantity is ${limit}.`);
			e.target.checkValidity();
		}
	}

	/**
	 * build addon data
	 * @param {*} id
	 * @param {*} quantity
	 */
	buildAddonData(id, quantity) {
		const addonData = {
			id,
			quantity,
			type: CONSTANTS.INTERNET_ADDITIONAL_TYPE
		};

		this.dispatchEvent(new CustomEvent('changedaddon', { detail: addonData, bubbles: false }));
	}
}
