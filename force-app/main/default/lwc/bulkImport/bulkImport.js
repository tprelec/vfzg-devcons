import { LightningElement, track, api } from 'lwc';
import bulkImportSite from '@salesforce/apex/SitesDatatableController.bulkImportSite';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import siteImportTemplate from '@salesforce/resourceUrl/siteImportTemplate';

export default class BulkImport extends LightningElement {
	@api
	accountId;
	@track
	showSpinner;
	@track
	fileName;
	fileData;

	@track
	separator;

	uploadedFile;
	// Starting import is disabled while the csv file is not uploaded
	get disableStartImport() {
		return this.uploadedFile && this.separator ? false : true;
	}
	// When file is uploaded the File Name is displayed
	get showFileName() {
		return this.uploadedFile ? true : false;
	}
	// Separator options
	get options() {
		return [
			{
				label: ';',
				value: ';'
			},
			{ label: ',', value: ',' }
		];
	}

	get acceptedFormats() {
		return ['.txt', '.csv'];
	}

	closeModal(event) {
		this.dispatchEvent(
			new CustomEvent('closemodal', {
				detail: { sourceComponent: 'bulkImport' }
			})
		);
	}
	// Set variables when upload is finished
	handleUploadFinished(event) {
		this.uploadedFile = event.detail.files;
		this.fileName = this.uploadedFile[0].name;
	}
	// Handle download Example file from Static Resource
	downloadExampleFile(event) {
		const resourcePath = siteImportTemplate;
		window.open(resourcePath);
		return null;
	}
	// Handle import of Sites
	startImport() {
		this.showSpinner = true;
		bulkImportSite({ contentDocumentId: this.uploadedFile[0].documentId, accountId: this.accountId, separator: this.separator })
			.then((res) => {
				this.errorList = [];
				this.draftValues = [];
				this.showToast('Success!', 'Sites added successfully.', 'success');
				this.showSpinner = false;
				this.closeModal();
			})
			.catch((error) => {
				this.showSpinner = false;

				this.showToast('Error!', error.body.message, 'error');
			});
	}

	handleSeparatorChange(event) {
		this.separator = event.detail.value;
	}

	showToast(title, message, variant) {
		const event = new ShowToastEvent({
			title,
			variant,
			message
		});
		this.dispatchEvent(event);
	}
}
