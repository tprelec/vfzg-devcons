import { api, LightningElement } from 'lwc';

export default class VzEnrichedListElement extends LightningElement {
	@api
	recordId;
	@api
	title;
	@api
	description;
	@api
	uri;

	get urlTarget() {
		return this.uri + '/' + this.recordId;
	}
}
