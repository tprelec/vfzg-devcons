import { LightningElement, api } from 'lwc';
import svgShowEmpty from './svgShowEmpty.html';
import svgBroken from './svgShowBroken.html';

export default class VzIllustrationProvider extends LightningElement {
	@api message;
	@api svgname;

	// add more templates as needed.
	renderHtml = {
		showempty: svgShowEmpty,
		broken: svgBroken
	};

	render() {
		if (this.renderHtml[this.svgname]) {
			return this.renderHtml[this.svgname];
		}
		return svgShowEmpty;
	}
}
