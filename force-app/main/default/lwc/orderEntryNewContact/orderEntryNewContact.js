import { LightningElement, api, track } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import saveContact from '@salesforce/apex/OrderEntryContactController.saveContact';
import getCountries from '@salesforce/apex/OrderEntryContactController.getCountries';
export default class OrderEntryNewContact extends LightningElement {
	@api accountId;
	@api opportunityId;
	@api journeyType;
	@track countriesList = [];
	@track error;
	@track loading = false;
	// Dutch phone numbers patern
	@track telPattern =
		'^((+|00(s|s?-s?)?)31(s|s?-s?)?((0)[-s]?)?|0)[1-9]((s|s?-s?)?[0-9])((s|s?-s?)?[0-9])((s|s?-s?)?[0-9])s?[0-9]s?[0-9]s?[0-9]s?[0-9]s?[0-9]$';
	@track newContact = {
		FirstName: '',
		LastName: '',
		MobilePhone: '',
		Phone: '',
		Email: '',
		Birthdate: '',
		LG_PreferredCommunication__c: '',
		Document_Type__c: '',
		Document_Expiration_Date__c: '',
		Document_Number__c: '',
		Document_Issuing_Country__c: 'NL',
		Salutation: ''
	};
	get mobileJourney() {
		return this.journeyType === 'Mobile';
	}
	get options() {
		return [
			{ label: 'None', value: 'Opt out all' },
			{ label: 'Sms', value: 'Sms' },
			{ label: 'Email', value: 'Email' }
		];
	}

	get salutationList() {
		return [
			{ label: 'de heer', value: 'de heer' },
			{ label: 'mevrouw', value: 'mevrouw' },
			{ label: 'Mr.', value: 'Mr.' },
			{ label: 'Ms.', value: 'Ms.' },
			{ label: 'Mrs.', value: 'Mrs.' },
			{ label: 'Dr.', value: 'Dr.' },
			{ label: 'Prof.', value: 'Prof.' },
			{ label: 'Drs.', value: 'Drs.' },
			{ label: 'Ing.', value: 'Ing.' },
			{ label: 'Ir.', value: 'Ir.' },
			{ label: 'heer', value: 'heer' }
		];
	}

	get todaysDate() {
		let today = new Date();

		today.toString();

		return new Date().toISOString();
	}
	get maximumBirthday() {
		let today = new Date();

		let year = today.getFullYear() - 110;
		today.setFullYear(year);
		return today.toISOString();
	}
	get minimumBirthday() {
		let today = new Date();

		let year = today.getFullYear() - 18;
		today.setFullYear(year);

		return today.toISOString();
	}
	get documentTypes() {
		if (this.newContact.Document_Issuing_Country__c == 'NL') {
			return [
				{
					label: 'Driver License',
					value: 'Driver License'
				},
				{ label: 'ID Card', value: 'Id Card' },
				{ label: 'Passport', value: 'Passport' }
			];
		}
		this.newContact.Document_Type__c = 'Passport';
		return [{ label: 'Passport', value: 'Passport' }];
	}
	get documentNumberFormat() {
		if (this.newContact.Document_Issuing_Country__c == 'NL') {
			switch (this.newContact.Document_Type__c) {
				case 'Driver License':
					return '^[0-9]{10}$';
				case 'Id Card':
					return '[A-Za-z]{2}[A-Za-z0-9]{6}[0-9]{1}';
				case 'Passport':
					return '[A-Za-z]{2}[A-Za-z0-9]{6}[0-9]{1}';
				default:
					return '';
			}
		}
		return '[a-zA-Z0-9]*';
	}
	get documentNumberErrorMessages() {
		if (this.newContact.Document_Issuing_Country__c == 'NL') {
			switch (this.newContact.Document_Type__c) {
				case 'Driver License':
					return 'Drivers license number must be 10 digits long.';
				case 'Id Card':
					return 'ID Card number must be 9 characters long and must start with 2 letters, followed by 6 digits or letters and ending with 1 digit. (e.g. NL123ABC9)';
				case 'Passport':
					return 'Passport number must be 9 characters long and must start with 2 letters, followed by 6 digits or letters and ending with 1 digit. (e.g. NL123ABC9)';
				default:
					return '';
			}
		}
		return '';
	}
	connectedCallback() {
		this.getCountriesList();
	}
	getCountriesList() {
		getCountries({})
			.then((result) => {
				let options = [];
				if (result) {
					for (const [key, value] of Object.entries(result)) {
						options.push({
							label: value,
							value: key
						});
					}

					this.countriesList = options;
				}
			})
			.catch((error) => {
				this.handleError(error);
			});
	}
	/**
	 * change contact property
	 */
	handleFormInputChange(event) {
		this.newContact[event.target.name] = event.target.value;
	}
	/**
	 * close modal
	 */
	closeModal() {
		this.dispatchEvent(new CustomEvent('closemodal', { detail: {}, bubbles: false }));
	}
	/**
	 *saving contact and checking input data
	 */
	handleSave() {
		const accId = { AccountId: this.accountId };
		this.newContact = { ...this.newContact, ...accId };
		if (!this.newContact.Document_Issuing_Country__c) {
			this.newContact.Document_Issuing_Country__c = 'Netherlands';
		}
		let isValid = true;
		this.template.querySelectorAll('lightning-input').forEach((element) => {
			element.reportValidity();
			isValid = isValid && element.checkValidity();
		});
		this.template.querySelectorAll('lightning-combobox').forEach((element) => {
			element.reportValidity();
			isValid = isValid && element.checkValidity();
		});
		if (isValid) {
			this.loading = true;
			saveContact({ newContact: this.newContact, oppId: this.opportunityId })
				.then((result) => {
					if (result) {
						this.newContact = result;
						this.dispatchEvent(
							new CustomEvent('closemodal', {
								detail: this.newContact,
								bubbles: false
							})
						);
					}
				})
				.catch((error) => {
					this.loading = false;
					console.error('Error', error);
					let errorMsg = '';

					for (let propName in error.body.fieldErrors) {
						if (error.body.fieldErrors.hasOwnProperty(propName)) {
							errorMsg = error.body.fieldErrors[propName][0].message.split('.')[0];
						}
					}
					if (errorMsg === '') {
						error.body.pageErrors.forEach((element) => {
							errorMsg = element.message.split('.')[0];
						});
					}
					this.dispatchEvent(
						new ShowToastEvent({
							title: 'Something went wrong',
							message: errorMsg,
							variant: 'warning'
						})
					);
				});
		} else {
			this.dispatchEvent(
				new ShowToastEvent({
					title: 'Something went wrong',
					message: 'Please enter the required fields',
					variant: 'warning'
				})
			);
		}
	}
}
