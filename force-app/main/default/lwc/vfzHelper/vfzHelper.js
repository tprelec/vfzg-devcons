const DAYS_IN_A_WEEK = 7;
const MILLISECONDS_IN_A_DAY = 86400000;
const MILLISECONDS_IN_AN_HOUR = 3600000;
const MILLISECONDS_IN_A_WEEK = 604800000;

/**
 * For a given date, get the ISO week number
 *
 * Algorithm is to find nearest thursday, it's year
 * is the year of the week number. Then get weeks
 * between that date and the first day of that year.
 *
 * Note that dates in one year can be weeks of previous
 * or next year, overlap is up to 3 days.
 *
 * e.g. 2014/12/29 is Monday in week  1 of 2015
 *      2012/1/1   is Sunday in week 52 of 2011
 * @param {date} currDate
 * @returns {array}
 */
const getWeekNumber = (currDate) => {
  // Copy date so don't modify original
  let dTemp = new Date(Date.UTC(currDate.getFullYear(), currDate.getMonth(), currDate.getDate()));
  // Set to nearest Thursday: current date + 4 - current day number
  // Make Sunday's day number 7
  dTemp.setUTCDate(dTemp.getUTCDate() + 4 - (dTemp.getUTCDay() || 7));
  // Get first day of year
  let yearStart = new Date(Date.UTC(dTemp.getUTCFullYear(), 0, 1));
  // Calculate full weeks to nearest Thursday
  let weekNo = Math.ceil((((dTemp - yearStart) / 86400000) + 1) / 7);
  // Return array of year and week number
  let currMonth = currDate.getMonth();
  if (currMonth < 2 && weekNo > 51) {
    weekNo = 1;
  } else if (currMonth >= 11 && weekNo === 1) {
    weekNo = 53;
  }
  return [dTemp.getUTCFullYear(), weekNo];
}
/**
 * Given a year and a weeknumber, calculates
 * the first date  and the last date,
 * Both in milliseconds
 * @param {int} year 
 * @param {int} week 
 * @returns {array}
 */
const getFirstLastDays = (year, week) => {
  let firstDay = new Date(year, 0, 1).getDay();
  let d = new Date("Jan 01, " + year + " 01:00:00");
  let w = d.getTime() - (MILLISECONDS_IN_AN_HOUR * 24 * (firstDay - 1)) + MILLISECONDS_IN_A_WEEK * (week - 1);
  //let fd = new Date(+w);
  //let ld = new Date(w + 518400000);
  return [+w, (w + 518400000)];
}

/**
 * Given a date and time, returns the first day of the same week (in milliseconds since 1/1/1970)
 * @param {Datetime} refDate 
 * @returns {integer}
 */
const getFirstDayOfWeekInMs = (refDate) => {
  let arrRefWeek = getWeekNumber(refDate);
  let arrDatesMill = getFirstLastDays(arrRefWeek[0], arrRefWeek[1]);
  return +arrDatesMill[0];
}

/**
 * Add numberOfWeeks to referenceDate. Returns the resulting date.
 * @param {Date} referenceDate 
 * @param {int} numberOfWeeks
 * @return {Date}
 */
const addWeeks = (referenceDate, numberOfWeeks) => {
  let iFirstDay = referenceDate.valueOf();
  let iDaysBack = numberOfWeeks * DAYS_IN_A_WEEK;
  let iFirstDayWeekBack = iFirstDay + (iDaysBack * MILLISECONDS_IN_A_DAY);
  let dFirstDayWeekBack = new Date(iFirstDayWeekBack);
  return dFirstDayWeekBack;
}

/**
 * Return the result of add/substract days to a referenceDate
 * @param {Date} referenceDate 
 * @param {int} numberOfDays 
 */
const addDays = (referenceDate, numberOfDays) => {
  let iFirstDay = referenceDate.valueOf();
  let iDayFwd = iFirstDay + (numberOfDays * MILLISECONDS_IN_A_DAY);
  let dResultDate = new Date(iDayFwd);
  return dResultDate;
}

/**
 * Returns a copy of original array without the element with the specified Id
 * var result = arrayRemoveById(array, 6); returns result = [1, 2, 3, 4, 5, 7, 8, 9, 0]    
 * @param {arrayWithIdProperty} arr 
 * @param {string} value 
 */
function arrayRemoveById(arr, value) {
  return arr.filter(function (ele) {
    return ele.Id != value;
  });
}

/**
 * Used bt Array.sort of an object array with a field "start"
 * @param {Date} a 
 * @param {Date} b 
 */
function compareByStart(a, b) {
  const bandA = a.start.valueOf();
  const bandB = b.start.valueOf();

  let comparison = 0;
  if (bandA > bandB) {
    comparison = 1;
  } else if (bandA < bandB) {
    comparison = -1;
  }
  return comparison;
}

export { getWeekNumber, getFirstLastDays, arrayRemoveById, getFirstDayOfWeekInMs, addWeeks, addDays, compareByStart }