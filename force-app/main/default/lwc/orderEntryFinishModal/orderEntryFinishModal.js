import { LightningElement, track, api, wire } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import { getRecord } from 'lightning/uiRecordApi';
import { NavigationMixin } from 'lightning/navigation';

import getOpportunity from '@salesforce/apex/OrderEntryController.getOpportunity';
import updateOpportunityAutomatedQuoteDelivery from '@salesforce/apex/OrderEntryQuoteController.updateOpportunityAutomatedQuoteDelivery';
import updateOpportunityStage from '@salesforce/apex/OrderEntryController.updateOpportunityStage';
import newOrder from '@salesforce/apex/OrderEntryController.newOrder';
import isCommunity from '@salesforce/apex/OrderEntryController.isCommunity';
import PROFILE_NAME_FIELD from '@salesforce/schema/User.Profile.Name';
import user from '@salesforce/user/Id';

export default class OrderEntryFinishModal extends NavigationMixin(LightningElement) {
	@api open;
	@api opportunityId;
	@api payment;
	@track fileId;
	@track displaySignatureModal = false;
	@track showViewLastComponent = false;
	@track signedQuote = false;
	@track profileName;
	@track probability;
	@track signedQuote = false;
	@track loading = false;
	@track isReady = false;
	@track showSendSignedConfirmation = false;
	@track sendConfirmationLabel = '';
	@wire(getRecord, {
		recordId: user,

		optionalFields: [PROFILE_NAME_FIELD]
	})
	async getProfileName({ error, data }) {
		this.isReady = false;
		if (data) {
			this.profileName = await data.fields.Profile.displayValue;
			this.isReady = true;
		} else if (error) {
			this.isReady = true;
			this.handleError(error);
		}
	}

	get openModal() {
		return this.open && this.isReadyAll;
	}

	get showSpinner() {
		return this.loading
			? 'slds-modal__content slds-p-around--x-small slds-size_1-of-1 slds-is-relative'
			: 'slds-hide';
	}

	get showSendQuote() {
		if (this.profileName === 'LG_NL D2D_Partner Sales User' && this.probability >= 85) {
			return false;
		}

		return true;
	}

	get isReadyAll() {
		return (
			this.isReady &&
			this.sendConfirmationLabel &&
			(this.showSendSignedConfirmation === true || this.showSendSignedConfirmation === false)
		);
	}

	connectedCallback() {
		this.isReady = false;
		getOpportunity({
			oppId: this.opportunityId,
			fields: ['LG_SignedQuoteAttachmentId__c', 'LG_SignedQuoteAvailable__c', 'Probability']
		})
			.then((res) => {
				this.showViewLastComponent = res.LG_SignedQuoteAttachmentId__c ? true : false;
				this.signedQuote = res.LG_SignedQuoteAvailable__c === 'true' ? true : false;
				this.probability = res.Probability;
				this.sendConfirmationLabel =
					this.signedQuote === false ? 'Send Confirmation' : 'Send Signed Confirmation';
				this.isReady = true;
			})
			.catch((error) => {
				this.isReady = true;
				this.handleError(error);
				this.showViewLastComponent = false;
			});

		if (!this.showViewLastComponent) {
			this.showSendSignedConfirmation = false;
		}
		this.showSendSignedConfirmation = !(
			this.profileName === 'LG_NL D2D_Partner Sales User' &&
			(this.probability >= 85 || this.signedQuote === false)
		);
	}

	closeModal() {
		this.dispatchEventClick('closemodal', false);
	}

	sendQuote() {
		this.loading = true;

		return updateOpportunityAutomatedQuoteDelivery({
			oppId: this.opportunityId,
			value: 'Quote Requested'
		})
			.then((result) => {
				this.navigateToRecordPage();
			})
			.catch((error) => {
				this.handleError(error);
				return false;
			});
	}

	async newOrder() {
		this.loading = true;
		let community = await isCommunity();
		let vUrl = 'https://' + window.location.hostname;
		newOrder({ oppId: this.opportunityId })
			.then((result) => {
				if (result) {
					if (community) {
						this[NavigationMixin.GenerateUrl]({
							type: 'comm__namedPage',
							attributes: {
								name: 'Order_Entry__c'
							},
							state: {
								c__oppId: result.Id
							}
						}).then((generatedUrl) => {
							let pageRedirect = vUrl + generatedUrl;

							location.replace(pageRedirect);
						});
					} else {
						this[NavigationMixin.Navigate]({
							type: 'standard__component',
							attributes: {
								componentName: 'c__OrderEntry'
							},
							state: {
								c__oppId: result.Id
							}
						});
					}
					this.closeModal();
				}
				this.loading = false;
			})
			.catch((error) => {
				this.handleError(error);
			});
	}

	async sendSignedConfirmation() {
		debugger;
		this.loading = true;
		if (this.payment && this.payment.paymentType) {
			return updateOpportunityStage({
				oppId: this.opportunityId,
				value: 'Customer Approved'
			})
				.then((result) => {
					this.navigateToRecordPage();
				})
				.catch((error) => {
					this.handleError(error);
					return false;
				});
		} else {
			this.dispatchMessage(
				'Payment information',
				'Payment details are required to send signed confirmation',
				'warning'
			);
		}
	}

	dispatchEventClick(eventName, eventValue) {
		this.dispatchEvent(new CustomEvent(eventName, { detail: eventValue, bubbles: false }));
	}

	handleError(error) {
		console.log(error);
		this.loading = false;
		this.dispatchMessage('Something went wrong', error, 'warning');
	}

	dispatchMessage(title, message, variant) {
		this.dispatchEvent(
			new ShowToastEvent({
				title,
				message,
				variant
			})
		);
	}

	closeApp() {
		this.navigateToRecordPage();
	}

	sleep(ms) {
		return new Promise((resolve) => setTimeout(resolve, ms));
	}

	// switch on opportunity record
	navigateToRecordPage() {
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: this.opportunityId,
				objectApiName: 'Opportunity',
				actionName: 'view'
			}
		});
		this.closeModal();
	}
}
