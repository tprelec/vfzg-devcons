import { LightningElement, api, track, wire } from 'lwc';
import { getConstants } from 'c/orderEntryConstants';
import getMobileProductsAndSIMCards from '@salesforce/apex/OrderEntryProductController.getMobileProductsAndSIMCards';
import getBundle from '@salesforce/apex/OrderEntryProductController.getBundle';
import { APPLICATION_SCOPE, MessageContext, subscribe, unsubscribe, publish } from 'lightning/messageService';
import genericChannel from '@salesforce/messageChannel/GenericMessageChannel__c';

const CONSTANTS = getConstants();

export default class OrderEntryProductConfigMobile extends LightningElement {
	MOBILE_PRODUCT_TYPE = CONSTANTS.MOBILE_PRODUCT_TYPE;

	@api selectedSimCardProduct;
	@track activeTab = CONSTANTS.MOBILE_PRODUCT_TYPE;
	@track mobileProducts = [];
	@track simCardProducts = [];
	@track _bundles;
	@track _opportunityId;
	@track _mainProduct;

	@api
	get mainProduct() {
		return this._mainProduct;
	}
	set mainProduct(value) {
		this._mainProduct = value;
		this.init();
	}

	@api
	get bundles() {
		return this._bundles;
	}
	set bundles(value) {
		this._bundles = Object.assign([], value);
	}

	@api
	get opportunityId() {
		return this._opportunityId;
	}
	set opportunityId(value) {
		this._opportunityId = value;
		this.init();
	}

	@wire(MessageContext)
	messageContext;

	subscription = null;
	subscribeToMessageChannel() {
		if (!this.subscription) {
			this.subscription = subscribe(this.messageContext, genericChannel, (message) => this.handleGenericMessage(message), {
				scope: APPLICATION_SCOPE
			});
		}
	}

	unsubscribeToMessageChannel() {
		unsubscribe(this.subscription);
		this.subscription = null;
	}

	connectedCallback() {
		this.subscribeToMessageChannel();
	}

	disconnectedCallback() {
		this.unsubscribeToMessageChannel();
	}

	async init() {
		if (this._mainProduct && this._opportunityId) {
			// Get mobile products
			let products = await getMobileProductsAndSIMCards({
				oppId: this.opportunityId
			});
			this.mobileProducts = products.filter((el) => el.Type__c === CONSTANTS.MOBILE_PRODUCT_TYPE);
			this.simCardProducts = products.filter((el) => el.Type__c === CONSTANTS.MOBILE_SIM_CARD_PRODUCT_TYPE);
		}
	}

	async setMobileProduct(mobileProductId) {
		this.addProduct(
			this.mobileProducts.find((product) => {
				return product.Id === mobileProductId;
			})
		);
	}

	/**
	 * Adds Order Entry Product to the bundle
	 * @param {object} oeProduct Order Entry Product
	 * @param {string} type Product Type
	 */
	addProduct(oeProduct, type) {
		let productType = type ? type : oeProduct.Type__c;
		if (!this._bundle.products) {
			this._bundle.products = [];
		}
		// Prevent duplicates
		this.removeProduct(productType);
		this._bundle.products.push({
			id: oeProduct.Id,
			type: productType
		});
	}

	/**
	 * Removes Product from the bundle
	 * @param {string} productType Product Type
	 */
	removeProduct(productType) {
		if (this._bundle.products) {
			this._bundle.products = this.bundle.products.filter((product) => {
				return product.type !== productType;
			});
		}
	}

	/**
	 * Removes Addons from the bundle
	 * @param {string} productType Product Type
	 */
	removeAddons(productTypes) {
		if (this._bundle.addons) {
			this._bundle.addons = this.bundle.addons.filter((addon) => {
				return !productTypes.includes(addon.parentType);
			});
		}
	}

	/**
	 * Handles selection of a tab
	 * @param {object} e Event
	 */
	handleSelectTab(e) {
		this.activeTab = e.detail.name;
	}

	/**
	 * Gets bundle based on the selected products
	 */
	async getBundle(bundleProducts) {
		let products = Object.assign([], bundleProducts);
		products.push({ id: this.mainProduct.Id, type: this.mainProduct.Type__c });
		return getBundle({
			products: products
		});
	}

	get isMobile() {
		return this.activeTab === CONSTANTS.MOBILE_PRODUCT_TYPE ? '' : 'slds-hide';
	}

	@api validate() {
		const mobileFormValidation = this.template.querySelector('c-order-entry-product-mobile');

		return mobileFormValidation.validate();
	}

	/**
	 * Routes Generic Messages thrown by child components
	 * @param {object} message Generic Message
	 */
	handleGenericMessage(message) {
		if (message.sourceComponent === 'order-entry-mobile-products') {
			let msg = Object.assign({}, message);
			msg.sourceComponent = 'order-entry-product-config-mobile';
			publish(this.messageContext, genericChannel, msg);
		}
	}

	/**
	 * Handles change of Products
	 * @param {object} data Payload from Products
	 */
	async handleProductChanged(data) {
		const mobileAddons = this.selectedMobileAddons;
		if (data.product?.Type__c === CONSTANTS.MOBILE_PRODUCT_TYPE) {
			await this.setMobileProduct(data.product.Id);
			x;
			this._bundle.contractTerm = data.contractTerm;
		} else {
			this._bundle.addons = data.addons.concat(mobileAddons);
			this.publishChange();
		}
		this._bundle.bundle = await this.getBundle(this._bundle.products);
		this.publishChange();
	}

	publishChange() {
		const msg = {
			sourceComponent: 'order-entry-product-config-mobile',
			payload: {
				event: 'product-bundles-changed',
				data: {
					bundles: this.bundles
				}
			}
		};
		publish(this.messageContext, genericChannel, msg);
	}
}
