import { api, LightningElement, track } from 'lwc';
import getDefaultSIM from '@salesforce/apex/OrderEntryProductController.getDefaultSIM';
import { getConstants } from 'c/orderEntryConstants';

const CONSTANTS = getConstants();

export default class OrderEntryMobileProductForm extends LightningElement {
	@api mainProduct;
	@api products = [];
	@api simCardProducts = [];
	@track isRendered = true;
	@track isEdit = false;
	@track loading = false;
	_contractTerm;
	_selectedProduct = {};
	_quantity;
	_productContractTerms;
	_selectedSimCardPoduct = {};
	@track contractTerms = [
		{ label: '1 year', value: '1' },
		{ label: '2 years', value: '2' }
	];
	get simSelected() {
		return this.selectedSimCardPoductId === undefined;
	}
	@api
	get contractTerm() {
		return this._contractTerm;
	}
	set contractTerm(value) {
		this._contractTerm = value;
	}
	@api
	get productContractTerms() {
		return this._productContractTerms;
	}
	set productContractTerms(value) {
		this._productContractTerms = Object.assign({}, value);
		this.init();
	}
	@api
	get selectedProduct() {
		return this._selectedProduct;
	}
	set selectedProduct(value) {
		this._selectedProduct = Object.assign({}, value);
		this.isEdit = value.Id ? true : false;
		this.init();
	}
	@api
	get selectedSimCardProduct() {
		return this._selectedSimCardPoduct;
	}
	set selectedSimCardProduct(value) {
		this._selectedSimCardPoduct = Object.assign({}, value);
	}
	@api
	get quantity() {
		return this._quantity;
	}
	set quantity(value) {
		this._quantity = value;
	}

	@api
	get selectedPoductId() {
		return this._selectedProduct?.Id;
	}
	get selectedSimCardPoductId() {
		return this._selectedSimCardPoduct?.Id;
	}
	get formattedProducts() {
		return this.products.map((p) => ({
			value: p.Id,
			label: p.Name
		}));
	}

	get formattedSimProducts() {
		return this.simCardProducts.map((p) => ({
			value: p.Id,
			label: p.Name
		}));
	}

	get showSpinner() {
		return this.loading ? 'slds-modal__content slds-p-around--x-small slds-size_1-of-1 slds-is-relative' : 'slds-hide';
	}

	async getDefaultSIM() {
		this.loading = true;
		let products = [
			{ id: this.mainProduct.Id, type: this.mainProduct.Type__c },
			{ id: this._selectedProduct.Id, type: CONSTANTS.MOBILE_PRODUCT_TYPE }
		];
		let defaultSim = await getDefaultSIM({ products, contractTerm: this.contractTerm });
		this._selectedSimCardPoduct = {
			Id: defaultSim,
			Type__c: CONSTANTS.MOBILE_SIM_CARD_PRODUCT_TYPE
		};

		this.loading = false;
	}

	/**
	 * checking inpupt data
	 * @returns boolean value
	 */
	@api validate() {
		const productInput = this.template.querySelector('lightning-combobox.mobile-product');
		productInput.reportValidity();
		const isProductValid = productInput.checkValidity();

		const termInput = this.template.querySelector('lightning-combobox.contract-term');
		termInput.reportValidity();
		const isTermValid = termInput.checkValidity();

		const quantityInput = this.template.querySelector('lightning-input.quantity');

		if (quantityInput) {
			quantityInput.setCustomValidity('');
			if (!this.validateQuantity()) {
				quantityInput.setCustomValidity('Above limit of 200');
			}
			quantityInput.reportValidity();
		}
		return isProductValid && isTermValid && this.validateQuantity();
	}

	/**
	 * Validate quantity
	 */
	validateQuantity() {
		const quantity = this._quantity;
		if (quantity > 200) {
			return false;
		}
		return true;
	}

	/**
	 * set initial value
	 */
	init() {
		if (this._productContractTerms && this._selectedProduct && this.isEdit) {
			this.buildContractTerms(this._selectedProduct.Id);
		}
	}
	/**
	 * handle product change
	 * @param {*} e
	 */
	handleProductChange(e) {
		if (e.detail.value !== this.selectedProductId) {
			this._selectedProduct = this.products.find((el) => el.Id === e.detail.value);
			// change contract terms accordingly
			this.buildContractTerms(e.detail.value);
		}
		if (this.contractTerm && this.selectedProduct) {
			this.getDefaultSIM();
		}
	}

	/**
	 * handle product change
	 * @param {*} e
	 */
	handleSimCardProductChange(e) {
		if (e.detail.value !== this.selectedSimCardPoductId) {
			this._selectedSimCardPoduct = this.simCardProducts.find((el) => el.Id === e.detail.value);
			// change contract terms accordingly
		}
	}

	/**
	 * build contract terms
	 * @param {*} productId
	 */
	buildContractTerms(productId) {
		let contractTerms = this._productContractTerms[productId];
		this.contractTerms = contractTerms.map((x) => {
			return { label: x + (x === '1' ? ' year' : ' years'), value: x };
		});
	}

	/**
	 * handle contract term chnage
	 * @param {*} e
	 */
	handleContractTermChange(e) {
		this._contractTerm = e.detail.value;
		if (this.contractTerm && this.selectedProduct) {
			this.getDefaultSIM();
		}
	}

	/**
	 * handle quntity change
	 * @param {*} e
	 */
	handleQuantityChange(e) {
		this._quantity = e.detail.value;
	}

	submitChange(name, detail) {
		this.dispatchEvent(new CustomEvent(name, { detail, bubbles: false }));
	}

	/**
	 * handle confirm and validate data
	 * @returns
	 */
	onConfirm() {
		if (!this.validate()) return;
		this.submitChange('confirm', {
			product: this._selectedProduct,
			contractTerm: this._contractTerm,
			simCard: this._selectedSimCardPoduct
		});
	}

	/**
	 * handle submit and validate data
	 * @returns
	 */
	onSubmit() {
		if (!this.validate()) return;
		this.submitChange('submit', {
			product: this._selectedProduct,
			contractTerm: this._contractTerm,
			quantity: this._quantity,
			simCard: this._selectedSimCardPoduct
		});
	}

	onCancel() {
		this.submitChange('cancel', {});
	}
}
