import { LightningElement, api } from 'lwc';
import modal from '@salesforce/resourceUrl/EMP_ModalCSS';
import { loadStyle } from 'lightning/platformResourceLoader';

export default class InstallationInformationEditableOnly extends LightningElement {
	@api recordId;

	connectedCallback() {
		Promise.all([loadStyle(this, modal)]);
	}
}
