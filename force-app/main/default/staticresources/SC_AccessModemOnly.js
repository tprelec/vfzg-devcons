// SC_AccessModemOnly
console.log('Loaded Access Modem Only plugin');

async function afterAccessModemOnlyConfigurationAdded(component, configuration) {
	await setAccessSitesIds(component, configuration);
	await setAttribute(component, configuration, SC_COMMON_ATTRIBUTES.TECHNOLOGY, 'Coax');
	return true;
}

async function setAccessSitesIds(component, configuration) {
	let activeSolution = await CS.SM.getActiveSolution();
	let mainComponentConfigurations = activeSolution.getAllConfigurations();
	for (const property in mainComponentConfigurations) {
		if (mainComponentConfigurations[property].name == 'Business Internet Modem Only ') {
			var o = mainComponentConfigurations[property];
		}
	}
	configuration.attributes.listofsiteids.value = o.attributes.accesssitesids.value;
}

async function afterAccessModemOnlyAttributeUpdated(component, configuration, attribute, oldValueMap) {
	console.log('wtf');
	if (attribute.name === SC_COMMON_ATTRIBUTES.SITE) {
		await afterSiteAttributeUpdated(component, configuration, attribute, oldValueMap);
	}
	if (attribute.name === SC_COMMON_ATTRIBUTES.CONTRACT_DURATION) {
		await setAttribute(component, configuration, SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT, '');
	} else if (attribute.name === SC_COMMON_ATTRIBUTES.SITE_AVAILABILITY) {
		await checkIfOneAccessPerLocation(configuration);
		await setAttribute(component, configuration, SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT, '');
	} else if (attribute.name === SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT) {
		await updateAccessModemOnlyName(component, configuration);

		if (attribute.value !== '') {
			const activeSolution = await CS.SM.getActiveSolution();
			const internetModemOnlyComponent = activeSolution.getComponentByName(SC_COMPONENTS.INTERNETMODEMONLY_COMPONENT);
			let internetConfigs = internetModemOnlyComponent.getAllConfigurations();
			let isExistingIntConfig = false;
			let internetGuid;

			if (internetConfigs !== undefined) {
				for (const internetConfig of Object.values(internetConfigs)) {
					if (internetConfig.attributes.accessguid.value === configuration.guid) {
						isExistingIntConfig = true;
						internetGuid = internetConfig.guid;
					}
				}
			}
			let accessModemOnlyConfiguration = await component.getConfiguration(configuration.guid);
			let contractDurationInternet = accessModemOnlyConfiguration.attributes.contractduration.value;
			let commercialProduct = await getCommercialProductInternetModemOnly(contractDurationInternet);

			if (!isExistingIntConfig) {
				let attributes = [];
				attributes.push(SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.ACCESS_GUID, accessModemOnlyConfiguration.guid, false));
				attributes.push(
					SC_Util.generateAttribute(
						SC_COMMON_ATTRIBUTES.CONTRACT_DURATION,
						accessModemOnlyConfiguration.attributes.contractduration.value,
						true
					)
				);
				attributes.push(
					SC_Util.generateLookupAttribute(
						SC_COMMON_ATTRIBUTES.SITE_AVAILABILITY,
						accessModemOnlyConfiguration.attributes.siteavailability.value,
						accessModemOnlyConfiguration.attributes.siteavailability.displayValue,
						true,
						false
					)
				);
				attributes.push(SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.SITE, accessModemOnlyConfiguration.attributes.sitename.value, false));
				attributes.push(SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.SITE_ID, accessModemOnlyConfiguration.attributes.site.value, false));

				let internetModemOnlyConfiguration = internetModemOnlyComponent.createConfiguration(attributes);
				await internetModemOnlyComponent.addConfiguration(internetModemOnlyConfiguration);

				let internetModemOnlyAttributes = [];
				internetModemOnlyAttributes.push(
					SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.INTERNET_PRODUCT, commercialProduct.commercialProduct.Id, false, true)
				);
				await internetModemOnlyComponent.updateConfigurationAttribute(
					internetModemOnlyConfiguration.guid,
					internetModemOnlyAttributes,
					false
				);

				let internetModemOnlyAttributes2 = [];
				internetModemOnlyAttributes2.push(
					SC_Util.generateLookupAttribute(
						SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT,
						commercialProduct.commercialProduct.Id,
						commercialProduct.commercialProduct.Name,
						true,
						true
					)
				);
				internetModemOnlyAttributes2.push(
					SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_NAME, commercialProduct.commercialProduct.Name, false, true)
				);
				await internetModemOnlyComponent.updateConfigurationAttribute(
					internetModemOnlyConfiguration.guid,
					internetModemOnlyAttributes2,
					false
				);

				internetModemOnlyComponent.validate();
			} else {
				let internetModemOnlyAttributes = [];
				internetModemOnlyAttributes.push(
					SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.SITE, accessModemOnlyConfiguration.attributes.sitename.value, false, true)
				);
				internetModemOnlyAttributes.push(
					SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.SITE_ID, accessModemOnlyConfiguration.attributes.site.value, false, true)
				);
				internetModemOnlyAttributes.push(
					SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.INTERNET_PRODUCT, commercialProduct.commercialProduct.Id, false, true)
				);
				internetModemOnlyAttributes.push(
					SC_Util.generateLookupAttribute(
						SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT,
						commercialProduct.commercialProduct.Id,
						commercialProduct.commercialProduct.Name,
						true,
						true
					)
				);
				internetModemOnlyAttributes.push(
					SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_NAME, commercialProduct.commercialProduct.Name, false, true)
				);
				internetModemOnlyAttributes.push(
					SC_Util.generateLookupAttribute(
						SC_COMMON_ATTRIBUTES.CONTRACT_DURATION,
						accessModemOnlyConfiguration.attributes.contractduration.value,
						accessModemOnlyConfiguration.attributes.contractduration.value,
						true,
						true
					)
				);
				internetModemOnlyAttributes.push(
					SC_Util.generateLookupAttribute(
						SC_COMMON_ATTRIBUTES.SITE_AVAILABILITY,
						accessModemOnlyConfiguration.attributes.siteavailability.value,
						accessModemOnlyConfiguration.attributes.siteavailability.displayValue,
						true,
						true
					)
				);

				await internetModemOnlyComponent.updateConfigurationAttribute(internetGuid, internetModemOnlyAttributes, false);

				internetModemOnlyComponent.validate();
			}
		}
	}
}
async function afterSiteAttributeUpdated(component, configuration, attribute, oldValueMap) {
	let activeSolution = await CS.SM.getActiveSolution();
	let mainComponentConfigurations = activeSolution.getAllConfigurations();
	var list = [];
	for (const property in mainComponentConfigurations) {
		if (mainComponentConfigurations[property].name == 'Business Internet Modem Only ') {
			var o = mainComponentConfigurations[property];
		}
		if (mainComponentConfigurations[property].name.includes('Access Modem Only')) {
			list.push(mainComponentConfigurations[property]);
		}
	}
	if (configuration.attributes.siteid.value != '') {
		var str = configuration.attributes.siteid.value + ',';
	}

	if (o.attributes.accesssitesids.value.includes(str)) {
		o.attributes.accesssitesids.value = o.attributes.accesssitesids.value.replace(str, '');
	}
	configuration.attributes.siteid.value = '';
	configuration.attributes.siteid.value = configuration.attributes.siteid.value + attribute.displayValue;
	if (configuration.attributes.siteid.value != '') {
		o.attributes.accesssitesids.value = o.attributes.accesssitesids.value + attribute.displayValue + ',';
	}

	list.forEach((config) => {
		config.attributes.listofsiteids.value = o.attributes.accesssitesids.value;
		console.log(config.attributes.listofsiteids.value);
	});
}

async function afterAccessModemOnlyConfigurationDeleted(component, configuration) {
	await checkInternetModemOnlyForDeletion(configuration);
}

async function beforeAccessModemOnlyConfigurationDeleted(component, configuration) {
	if (isBasketInflight()) {
		return checkIfInternetConfigInInflightBasket(configuration);
	}
	return true;
}
async function beforeAccessModemOnlyConfigurationDelete(component, configuration) {
	console.log('ahha');
	let activeSolution = await CS.SM.getActiveSolution();
	let mainComponentConfigurations = activeSolution.getAllConfigurations();
	var list = [];
	for (const property in mainComponentConfigurations) {
		if (mainComponentConfigurations[property].name == 'Business Internet Modem Only ') {
			var o = mainComponentConfigurations[property];
		}
		if (mainComponentConfigurations[property].name.includes('Access Modem Only')) {
			list.push(mainComponentConfigurations[property]);
		}
	}
	if (configuration.attributes.siteid.value != '') {
		var str = configuration.attributes.siteid.value + ',';
	}

	if (o.attributes.accesssitesids.value.includes(str)) {
		o.attributes.accesssitesids.value = o.attributes.accesssitesids.value.replace(str, '');
	}
	list.forEach((config) => {
		config.attributes.listofsiteids.value = o.attributes.accesssitesids.value;
	});
	return true;
}

async function afterAccessModemOnlyCloned(configurations) {
	console.log('***afterAccessModemOnlyCloned ');
	configurations.forEach(function (configuration) {
		checkIfOneAccessPerLocation(configuration);
	});
}

async function checkIfOneAccessPerLocation(configuration) {
	const basket = await CS.SM.getActiveBasket();
	const sites = new Array();
	const solutions = basket.getSolutions();
	let businessInternetSolutions = Object.values(solutions).filter((item) => item.name == 'Business Internet Modem Only');
	if (businessInternetSolutions !== undefined) {
		for (let i = 0; i < businessInternetSolutions.length; i++) {
			let businessInternetSolution = businessInternetSolutions[i];
			let configurations = await SC_Util.generateFlatConfigurationListAsync(businessInternetSolution);
			let accessConfigurations = Object.values(configurations).filter((item) => item.name == 'Access Modem Only');

			if (accessConfigurations !== undefined) {
				for (let j = 0; j < accessConfigurations.length; j++) {
					let accessConfiguration = accessConfigurations[j];

					if (accessConfiguration) {
						if (accessConfiguration.name === SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT) {
							if (accessConfiguration.value.attributes.siteavailability.displayValue !== '') {
								if (sites != null && sites.includes(accessConfiguration.value.attributes.siteavailability.displayValue)) {
									CS.SM.displayMessage(SC_ERROR_MESSAGES.SITE_ACCESS_ONLY_ONE, 'error');
									accessConfiguration.value.updateStatus({
										status: false,
										message: SC_ERROR_MESSAGES.SITE_ACCESS_ONLY_ONE
									});
									await validateConfiguration(accessConfiguration.value, false, SC_ERROR_MESSAGES.SITE_ACCESS_ONLY_ONE);
								} else {
									sites.push(accessConfiguration.value.attributes.siteavailability.displayValue);
									accessConfiguration.value.updateStatus({
										status: true,
										message: ''
									});
									await validateConfiguration(accessConfiguration.value, true, '');
								}
							}
						}
					}
				}
			}
		}
	}
}
async function getCommercialProductInternetModemOnly(contractTerm) {
	const basket = await CS.SM.getActiveBasket();
	const inputMapObject = {
		method: SC_REMOTE_CLASS_METHOD.GET_COMMERCIALPRODUCT_DETAILS,
		contractDuration: contractTerm
	};

	const result = await basket.performRemoteAction(SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS, inputMapObject);
	return result;
}

/**
 * @description Set Access Name
 */
async function updateAccessModemOnlyName(component, configuration) {
	let siteName = getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.SITE_NAME);
	let productVariantName = getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_NAME);
	let contractDuration = getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.CONTRACT_DURATION);
	let name = siteName + ' - ' + productVariantName + ', ' + contractDuration + ' maanden';

	const attributeData = [
		{
			name: SC_COMMON_ATTRIBUTES.ACCESS_NAME,
			value: name
		}
	];

	return component.updateConfigurationAttribute(configuration.guid, attributeData);
}
