'use strict';
/**
 * Helper for Solution Console API methods
 * @author Petar Miletic @ CloudSense
 * @since 09/04/2019
 */
console.log('Loaded Solution Console helper plugin');

let SC_Helper = {};

/*
 * Set solution field value (only applicable on main solutions)
 * solutionName - solution name
 * key - field name
 * value - field value
 * skipHooks - used to ignore before and after hooks
 */
SC_Helper.setMainSolutionFieldValueAsync = function (solution, key, value, displayValue, readOnly, showInUi, required) {
	const data = SC_Util.getUpdateAttributeMapGUID(key, value, displayValue, readOnly, showInUi, required);
	const rootKey = Object.keys(solution.schema.configurations)[0];
	const root = solution.schema.configurations[rootKey];

	SC_Util.getAttributeValue(root, key);

	return root.updateAttribute(key, data);
};

/*
 * Set attributes to all solutions with name
 * solutionName - solution name
 * attributeList - attribute list (key, value map)
 * skipHooks - used to ignore before and after hooks
 */
SC_Helper.setAttributesForAllSolutionsWithNameAsync = function (solution, solutionName, attributeList, skipHooks) {
	let promiseChain = Promise.resolve(true);

	let solutionx = solution.getComponentByName(solutionName);

	if (solutionx) {
		Object.keys(solutionx.schema.configurations).forEach((key, index) => {
			const component = solutionx.schema.configurations[key];

			for (let i = 0; i < attributeList.length; i++) {
				promiseChain = promiseChain.then(async function () {
					return await SC_Helper.updateAttributeAsync(solutionx, component.guid, attributeList[i].name, attributeList[i].value);
				});
			}
		});
	}

	return promiseChain;
};

/*
 * Set attributes to all solutions with name
 * solutionName - solution name
 * attributeList - attribute list (key, value map)
 * skipHooks - used to ignore before and after hooks
 */
SC_Helper.setAttributesForAllRelatedSolutionNamedAsync = function (parentName, solution, configurations, solutionName, attributeList) {
	let promiseChain = Promise.resolve(true);

	let solutionx = solution.getComponentByName(parentName);

	if (solutionx) {
		return SC_Util.getAllConfigurationsWithNameAsync(configurations, solutionName).then(function (subset) {
			Object.keys(subset).forEach((key, index) => {
				const component = SC_Util.unpackFlattConfiguration(subset[key]);

				for (let i = 0; i < attributeList.length; i++) {
					promiseChain = promiseChain.then(async function () {
						return await SC_Helper.updateAttributeAsync(solutionx, component.guid, attributeList[i].name, attributeList[i].value);
					});
				}
			});
		});
	}

	return promiseChain;
};

/*
 * Set attributes to all solutions with name and parentGuid
 * solutionName - solution name
 * parentGuid - parent GUID
 * attributeList - attribute list (key, value map)
 * skipHooks - used to ignore before and after hooks
 */
SC_Helper.setAttributesForAllSolutionsWithNameAndParentGuidAsync = function (
	solution,
	configuration,
	configurations,
	solutionName,
	parentGuid,
	attributeList,
	skipHooks
) {
	return SC_Util.getAllConfigurationsWithNameAndParentGuidAsync(configurations, solutionName, parentGuid).then(function (configs) {
		const sol = solution.getComponentByName(solutionName);

		let promiseChain = Promise.resolve(true);

		for (let i = 0; i < configs.length; i++) {
			for (let j = 0; j < attributeList.length; j++) {
				promiseChain = promiseChain.then(async function () {
					return await SC_Helper.updateAttributeAsync(sol, configs[i].key, attributeList[j].name, attributeList[j].value);
				});
			}
		}

		return promiseChain;
	});
};

/* Petar Matkovic April 17 2020
 * Set attributes to all related products with name
 * solutionName - related product name
 * configuration - parent configuration
 * configurations - related configurations
 * attributeList - attribute list (key, value map)
 * skipHooks - used to ignore before and after hooks
 */
SC_Helper.setAttributesForAllRelatedSolutionsWithName = function (solution, configuration, configurations, solutionName, attributeList, skipHooks) {
	return SC_Util.getAllRelatedConfigurationsWithName(configurations, solutionName).then(function (configs) {
		const sol = solution.getComponentByName(solutionName);

		let promiseChain = Promise.resolve(true);

		for (let i = 0; i < configs.length; i++) {
			for (let j = 0; j < attributeList.length; j++) {
				promiseChain = promiseChain.then(async function () {
					return await SC_Helper.updateAttributeAsync(sol, configs[i].guid, attributeList[j].name, attributeList[j].value);
				});
			}
		}

		return promiseChain;
	});
};

SC_Helper.updateAttributeAsync = async function (solution, guid, key, value, displayValue, readOnly, showInUi, required) {
	const data = SC_Util.getUpdateAttributeMapGUID(key, value, displayValue, readOnly, showInUi, required);

	if (!solution.updateConfigurationAttribute) {
		console.warn('Object instance is not of the Solution type!!!');
		return Promise.resolve(true);
	}

	return solution.updateConfigurationAttribute(guid, [data]);
};

SC_Helper.updateConfigurationsAsync = async function (solution, updateMap) {
	let promiseChain = Promise.resolve();

	Object.keys(updateMap).forEach((guid) => {
		promiseChain = promiseChain.then(async function () {
			return await solution.updateConfigurationAttribute(guid, updateMap[guid]);
		});
	});

	return promiseChain;
};

SC_Helper.updateLookupAttributeAsync = async function (config, guid, key, value, displayValue, readOnly, showInUi, required) {
	const data = SC_Util.getUpdateAttributeMapGUID(key, value, displayValue, readOnly, showInUi, required);

	if (!solution.updateConfigurationAttribute) {
		console.warn('Object instance is not of the Solution type!!!');
		return Promise.resolve(true);
	}

	return solution.updateConfigurationAttribute(guid, [data]);
};

/*
 * Copy field value from Parent to Related Product
 * solution - parent solution
 * configuration - configuration
 * relatedProduct - related product configuration
 * configAttributeName - source attribute name (attribute on the parent)
 * relatedAttributeName - target attribute name (attribute on the related/child)
 * skipHooks - used to ignore before and after hooks
 */
SC_Helper.copyAttributeValueToRelatedProductAsync = async function (
	solution,
	configuration,
	relatedProduct,
	configAttributeName,
	relatedAttributeName,
	skipHooks
) {
	if (configuration && configuration.attributes) {
		const attr = SC_Util.getAttributeByName(configuration, configAttributeName);

		if (attr) {
			return await SC_Helper.updateAttributeAsync(solution, relatedProduct.guid, relatedAttributeName, attr.value);
		}
	}

	return true;
};

SC_Helper.copyValueToRelatedProductAsync = async function (solution, relatedProduct, relatedAttributeName, value, skipHooks) {
	return await SC_Helper.updateAttributeAsync(solution, relatedProduct.guid, relatedAttributeName, value);
};

/*
 * Set Configuration Name
 * configuration - configuration
 * configAttributeName - target attribute name (attribute name we wish to set)
 * skipHooks - used to ignore before and after hooks
 */
SC_Helper.setConfigurationName = function (solution, configuration, relatedProduct, configAttributeName) {
	return SC_Helper.updateAttributeAsync(solution, relatedProduct.guid, configAttributeName, configuration.name);
};

SC_Helper.copyAttributeValueToAllRelatedProductsAsync = function (
	solution,
	relatedProductName,
	configuration,
	childConfigurations,
	parentConfigurationAttributeName,
	childConfigurationAttributeName,
	skipHooks
) {
	const parentAttributeValue = SC_Util.getAttributeValue(configuration, parentConfigurationAttributeName);

	if (!childConfigurations) {
		childConfigurations = configuration.getRelatedProducts();
	}

	let promiseChain = Promise.resolve(true);

	Object.keys(childConfigurations).forEach((key) => {
		let guid = '';
		if (childConfigurations[key].hasOwnProperty('key')) {
			guid = childConfigurations[key]['key'];
		} else {
			guid = childConfigurations[key].guid;
		}

		promiseChain = promiseChain.then(async function () {
			return await SC_Helper.updateAttributeAsync(solution, guid, childConfigurationAttributeName, parentAttributeValue);
		});
	});

	return promiseChain;
};

/*
 * Add configuration to the solution
 * configurationName - configuration name
 * configurations - list of configurations containing a list of attributes. Use Util.generateConfiguration, Util.generateAttribute methods to generate your attributes
 * skipHooks - boolean value which allows us to skip plugin actions on this update
 */
SC_Helper.addConfigurationsAsync = function (configurationName, configurations, skipHooks) {
	return CS.SM.addConfigurations(configurationName, configurations, skipHooks).then(function (component) {
		return component;
	});
};

/*
 * Set attribute properties
 * componentName - component name
 * configurationGuid - configuration GUID
 * attributes - array of attributes together with propery values (Use SC_Util.getUIBasicUIElement in order to generate basic UI element)
 * skipHooks - used to ignore before and after hooks
 */
SC_Helper.setAttributePropertiesAsync = function (configurations, componentName, configurationGuid, attributes, skipHooks) {
	function setAttributePropertiesInner() {
		let compName = SC_Util.clearTrailingNumbers(componentName);

		let updateMap = {};
		updateMap[configurationGuid] = [];

		let config = SC_Util.getFromFlattenedConfigurations(configurations, configurationGuid);

		for (let i = 0; i < attributes.length; i++) {
			let attribute = attributes[i];

			let configAttribute = SC_Util.getAttributeByName(config, attribute.name);

			if (!configAttribute) {
				continue;
			}

			let tmpObj = SC_Util.generateAttribute(attribute.name, '');
			let tmpVal = {};

			for (let key in attribute) {
				if (key !== 'name' && configAttribute.hasOwnProperty(key)) {
					tmpVal[key] = attribute[key];
				}
			}

			tmpObj.value = tmpVal;

			updateMap[configurationGuid].push(tmpObj);
		}

		if (Object.keys(updateMap).length > 0) {
			return CS.SM.updateConfigurationAttribute(compName, updateMap, skipHooks);
		}

		return Promise.resolve();
	}

	return setAttributePropertiesInner();
};

/*
 * Get attribute values from all configurations named
 * configurations - flat configuration list
 * configName - configuration name
 * key - attribute name
 */
SC_Helper.getAllAttributeValuesForConfigurationNameAsync = function (configurations, configName, key) {
	return SC_Util.getAllConfigurationsWithNameAsync(configurations, configName).then(function (subset) {
		let attributeValues = [];
		for (let i = 0; i < subset.length; i++) {
			let config = SC_Util.unpackFlattConfiguration(subset[i]);
			attributeValues.push(SC_Util.getAttributeValue(config, key));
		}

		return attributeValues;
	});
};

/*
 * Filter all root-level solutions using a value list
 * key - attribute name
 * mapUsingKey - true/false, key will be target attribute or solution Id
 * filters - filter by value, returns all if omitted
 */
SC_Helper.getAllRootLevelSolutionsUsingAttributeAsync = function (attributeName, mapUsingKey, filters) {
	let returnMap = {};

	return SC_Base.getActiveBasket().then((basket) => {
		Object.keys(basket.solutions).forEach((key) => {
			const target = SC_Util.getMainSolutionAttributeValue(basket.solutions[key], attributeName);

			if (!filters) {
				addToMap(target, mapUsingKey, returnMap, basket.solutions[key]);
			} else if (filters.indexOf(target) > -1) {
				addToMap(target, mapUsingKey, returnMap, basket.solutions[key]);
			}
		});

		return returnMap;
	});

	function addToMap(target, mapUsingKey, returnMap, solution) {
		if (mapUsingKey === true) {
			returnMap[target] = solution;
		} else {
			returnMap[solution.id] = solution;
		}
	}
};

SC_Helper.getAllRootLevelSolutionsUsingAttributeAndNameAsync = function (configName, key, mapUsingKey, filters) {
	let returnMap = {};

	return SC_Base.getActiveBasket().then((basket) => {
		Object.keys(basket.solutions).forEach(function (elem, index) {
			const solution = basket.solutions[elem];

			if (solution.name !== configName) {
				return;
			}

			const target = SC_Util.getMainSolutionAttributeValue(solution, key);

			if (!target) {
				return;
			}

			if (!filters) {
				addToMap(target, mapUsingKey, returnMap, solution);
				return;
			}

			if (filters.indexOf(target) > -1) {
				addToMap(target, mapUsingKey, returnMap, solution);
			}
		});

		return returnMap;
	});

	function addToMap(target, mapUsingKey, returnMap, solution) {
		if (mapUsingKey === true) {
			returnMap[target] = solution;
		} else {
			returnMap[solution.id] = solution;
		}
	}
};

SC_Helper.getAllConfigurationsKeyValuePairUsingAttributeAsync = function (configurations, configName, key, valueKey, filters) {
	return SC_Util.getAllConfigurationsWithNameAsync(configurations, configName).then(function (subset) {
		let configurationMap = {};

		for (let i = 0; i < subset.length; i++) {
			const config = SC_Util.unpackFlattConfiguration(subset[i]);
			const GUID = SC_Util.getAttributeValue(config, valueKey);
			const keyValue = SC_Util.getAttributeValue(config, key);

			if (keyValue && filters.indexOf(keyValue) !== -1) {
				configurationMap[keyValue] = GUID;
			}
		}

		return configurationMap;
	});
};

SC_Helper.getAllConfigurationsTargetedByFilterByAsync = function (configurations, configName, key, filters) {
	return SC_Util.getAllConfigurationsWithNameAsync(configurations, configName).then(function (configs) {
		return configs.filter(function (config) {
			const keyValue = SC_Util.getAttributeValue(config.value, key);
			return keyValue && filters.indexOf(keyValue) > -1;
		});
	});
};

SC_Helper.createMainConfigurationAsync = function (solutionTypeId) {
	return SC_Base.getActiveBasket().then((basket) => {
		return basket.createSolution(solutionTypeId, true).then(function (solutionId) {
			return basket.getSolution(solutionId);
		});
	});
};

SC_Helper.cloneConfigurationsAsync = function (rootTypeId, origin, excludeList, setMainSolutionValuesAsync) {
	if (!rootTypeId) {
		return Promise.resolve();
	}

	return SC_Util.generateFlatConfigurationListAsync().then((configurations) => {
		return CS.SM.getActiveSolution().then((solution) => {
			let objectMap = {};

			Object.keys(configurations).forEach((key) => {
				const config = configurations[key];
				const attributes = config.value.attributes;

				const configType = {};

				let configurationTemplate = [];

				Object.keys(attributes).forEach((attrKey) => {
					const attr = attributes[attrKey];

					if (attr.type === 'Lookup') {
						configurationTemplate.push(SC_Util.generateLookupAttribute(attr.name, attr.value, attr.displayValue, false));
					} else if (attr.name !== SC_Common_Attributes.SoftDelete) {
						configurationTemplate.push(SC_Util.generateAttribute(attr.name, attr.value, false));
					}
				});

				objectMap[config.key] = { name: config.name, template: configurationTemplate };
			});

			return SC_Base.getActiveBasket().then((basket) => {
				return basket.createSolution(rootTypeId, true).then(function (solutionId) {
					return CS.SM.setActiveSolution(solutionId, true).then(function (newSolution) {
						let promiseChain = Promise.resolve();

						Object.keys(configurations).forEach((key) => {
							promiseChain = promiseChain.then(function () {
								const config = configurations[key];
								let configType = getConfigType(newSolution, config, excludeList);

								/*
                                if (configType && configType.componentType !== 'Main') {
                                    
                                    const configurationInstance = configType.createConfiguration(objectMap[config.key].template);
                                    return configType.addConfiguration(configurationInstance).then(obj => {
                                        return addRelatedProductsAsync(obj, configurations, config, configType, objectMap);
                                    });
                                }
                                */

								if (configType && configType.componentType === 'Main' && setMainSolutionValuesAsync) {
									const configurationInstance = configType.createConfiguration(objectMap[config.key].template);
									return setMainSolutionValuesAsync(origin, configurationInstance, configType);
								}

								return;
							});
						});

						return promiseChain;
					});
				});
			});

			function getConfigType(solution, config, excludeList) {
				if (excludeList && excludeList.indexOf(config.name) > -1) {
					return undefined;
				}

				return solution.getComponentByName(config.name);
			}

			function addConfigType(objectMap, configType, config) {
				const configurationInstance = configType.createConfiguration(objectMap[config.key].template);
				configType.addConfiguration(configurationInstance);
			}

			function addRelatedProductsAsync(parent, configurations, config, configType, objectMap) {
				let promiseChain = Promise.resolve();

				Object.keys(configurations).forEach((key) => {
					if (configurations[key].parentGuid && configurations[key].parentGuid === config.key) {
						const relatedProductName = configurations[key].name;
						const configurationInstance = configType.createRelatedProduct(relatedProductName, objectMap[config.key].template);

						promiseChain = promiseChain.then(function () {
							if (relatedProductName === 'Add-On') {
								// return configType.addAddOn(config.key, configurationInstance);
								return true;
							} else {
								return configType.addRelatedProduct(config.key, configurationInstance);
							}
						});
					}

					return Promise.resolve(true);
				});

				return promiseChain;
			}
		});
	});
};

SC_Helper.findSolutionsAsync = function () {
	return SC_Base.getActiveBasket().then((basket) => {
		return basket.createSolution(rootTypeId, true).then(function (solutionId) {
			return CS.SM.setActiveSolution(solutionId, true).then(function (newSolution) {
				let promiseChain = Promise.resolve();

				Object.keys(configurations).forEach((key) => {
					promiseChain = promiseChain.then(function () {
						const config = configurations[key];
						let configType = getConfigType(newSolution, config, excludeList);

						if (configType && configType.componentType !== 'Main') {
							const configurationInstance = configType.createConfiguration(objectMap[config.key].template);
							return configType.addConfiguration(configurationInstance).then((obj) => {
								return addRelatedProductsAsync(obj, configurations, config, configType, objectMap);
							});
						}

						if (configType && configType.componentType === 'Main' && setMainSolutionValuesAsync) {
							const configurationInstance = configType.createConfiguration(objectMap[config.key].template);
							return setMainSolutionValuesAsync(origin, configurationInstance, configType);
						}

						return;
					});
				});

				return promiseChain;
			});
		});
	});
};

SC_Helper.copyConfigurationToAnotherSolutionAsync = function (
	originalConfiguration,
	originalConfigurationName,
	target,
	parentGuid,
	parentname,
	solutionName,
	type,
	availableAddons,
	resolveParentConfigurationAsync
) {
	return CS.SM.setActiveSolution(target.solutionId, true).then(function (activeSolution) {
		if (type === 'Related Component') {
			return SC_Helper.copyRelatedComponentAsync(
				activeSolution,
				originalConfiguration,
				target,
				parentname,
				solutionName,
				resolveParentConfigurationAsync
			).finally(function () {
				activeSolution.validate();
				return;
			});
		} else {
			return SC_Helper.copyConfigurationWithAddonsAsync(
				activeSolution,
				originalConfiguration,
				originalConfigurationName,
				availableAddons
			).finally(function () {
				activeSolution.validate();
				return;
			});
		}
	});
};

SC_Helper.copyRelatedComponentAsync = function (
	activeSolution,
	originalConfiguration,
	target,
	parentname,
	solutionName,
	resolveParentConfigurationAsync
) {
	const configurationTemplate = SC_Util.cloneConfiguration(originalConfiguration);
	const configType = activeSolution.getComponentByName(parentname);
	const configurationInstance = configType.createRelatedProduct(solutionName, configurationTemplate);

	return configType.addRelatedProduct(target.guid, configurationInstance, true).then((product) => {
		if (resolveParentConfigurationAsync && typeof resolveParentConfigurationAsync === 'function') {
			return resolveParentConfigurationAsync(configType, target, configurationInstance.configuration).finally(() => {
				return product.validate();
			});
		}

		return product.validate();
	});
};

SC_Helper.copyConfigurationWithAddonsAsync = function (activeSolution, originalConfiguration, originalConfigurationName, availableAddons) {
	const configurationTemplate = SC_Util.cloneConfiguration(originalConfiguration);
	const relatedProductTemplates = SC_Util.cloneRelatedProducts(originalConfiguration);

	const configType = activeSolution.getComponentByName(originalConfigurationName);
	const configurationInstance = configType.createConfiguration(configurationTemplate);

	return configType.addConfiguration(configurationInstance).then(function (addedConfiguration) {
		return SC_Util.addRelatedProductsToConfigurationAsync(configType, addedConfiguration, relatedProductTemplates)
			.then(function () {
				const addons = SC_Util.packAddOns(originalConfiguration, availableAddons);
				return SC_Util.addAddonsAsync(configType, addedConfiguration, addons);
			})
			.finally(function () {
				addedConfiguration.validate();
				return;
			});
	});
};

// Using global object until support for import, export is fully there
// This has to be the last line in the file
Object.freeze(SC_Helper);
window.SC_Helper = SC_Helper;
