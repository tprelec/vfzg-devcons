if (!CS || !CS.SM) {
	throw Error("Solution Console Api not loaded!");
}

// Register GenSecond Solution plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener("SolutionConsoleReady", registerGenSecond);
}
console.log("GenSecond...");
async function registerGenSecond() {
	const genFirst = await CS.SM.registerPlugin("GenSecond");
	updateGenSecondPlugin(genFirst);

	console.log("GenSecond plugin registered!");
	return;
}

// Hooks registration
function updateGenSecondPlugin(Plugin) {
	Plugin.afterAttributeUpdated = afterGenSecondAttributeUpdated;
}

// Hook Implementation
async function afterGenSecondAttributeUpdated(
	component,
	configuration,
	attribute,
	oldValueMap
) {

	if (component.name === 'GenSecond' && attribute.name === 'contractTerm' && attribute.value !== oldValueMap.value) {
		SC_MessageBroker.store.publish('root:ContractTerm', {
			"component": component,
			"configuration": configuration,
			"attribute": attribute,
			"component": oldValueMap
		});
	}

	// Implement component logic here
	scgetaccess.afterGenFirstAttributeUpdated(component, configuration, attribute, oldValueMap);
	scgetinternet.afterGenFirstAttributeUpdated(component, configuration, attribute, oldValueMap);

	return true;
}
