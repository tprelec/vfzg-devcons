'use strict';

/**
 * Utility helper for Solution Console API methods
 * @author Petar Miletic @ CloudSense
 * @since 09/04/2019
 */
console.log('Loaded Solution Console util plugin');

let SC_Util = {};

/*
 * Generates flat configuration list
 */
SC_Util.generateFlatConfigurationListAsync = async function (rootSolutionOptional) {
	const rootSolution = rootSolutionOptional ? rootSolutionOptional : await CS.SM.getActiveSolution();

	let configurationsMap = {};
	const mainGUID = Object.keys(rootSolution.schema.configurations)[0];

	const rootConfiguration = rootSolution.schema.configurations[mainGUID];
	console.log(rootSolution.isFromMacBasket);

	// guid, configurationName, configuration, parentGuid, parentName, mainGUID, addOnDisplayName, solution type
	addToConfigurationsMapInner(
		mainGUID,
		rootSolution.name,
		rootConfiguration,
		undefined,
		undefined,
		undefined,
		undefined,
		rootSolution.componentType
	);

	resolveComponents(rootSolution.components, rootSolution.name, mainGUID);

	if (Object.keys(rootConfiguration.orderEnrichments).length > 0) {
		resolveOrderEnrichments(rootConfiguration.orderEnrichments, mainGUID, rootSolution.name, mainGUID);
	}

	return configurationsMap;

	function resolveComponents(components, rootName, rootGUID) {
		Object.keys(components).forEach((key, index) => {
			const component = components[key];
			resolveConfigurations(component.schema.configurations, rootName, rootGUID, component.name, component.componentType);
		});
	}

	function resolveConfigurations(configurations, rootName, rootGUID, parentName, componentType) {
		Object.keys(configurations).forEach((key, index) => {
			const configuration = configurations[key];
			addToConfigurationsMapInner(configuration.guid, parentName, configuration, rootGUID, rootName, rootGUID, undefined, componentType);

			if (configuration.relatedProductList) {
				resolveRelatedComponentList(configuration.relatedProductList, parentName, key, rootGUID);
			}

			if (Object.keys(configuration.orderEnrichments).length > 0) {
				resolveOrderEnrichments(configuration.orderEnrichments, key, parentName, rootGUID);
			}
		});
	}

	function resolveRelatedComponentList(relatedConfigurations, relatedParentName, relatedParentGUID, rootGUID) {
		for (let i = 0; i < relatedConfigurations.length; i++) {
			const relatedProduct = relatedConfigurations[i];
			const guid = relatedProduct.configuration.guid;
			addToConfigurationsMapInner(
				guid,
				relatedProduct.relatedProductName,
				relatedProduct.configuration,
				relatedParentGUID,
				relatedParentName,
				rootGUID,
				relatedProduct.name,
				relatedProduct.type
			);
		}
	}

	function resolveOrderEnrichments(oeConfigurations, relatedParentGUID, relatedParentName, rootGUID) {
		Object.keys(oeConfigurations).forEach((key, index) => {
			const oe = oeConfigurations[key];
			Object.keys(oe).forEach((cKey, cIndex) => {
				const oeConfig = oe[cKey];
				addToConfigurationsMapInner(cKey, oeConfig.name, oeConfig, relatedParentGUID, relatedParentName, rootGUID, undefined, 'OE');
			});
		});
	}

	function addToConfigurationsMapInner(guid, configurationName, configuration, parentGuid, parentName, mainGUID, addOnDisplayName, type) {
		if (configurationsMap.hasOwnProperty(guid)) {
			console.error('Guid ' + guid + ' duplicated!!!');
		}

		configurationsMap[guid] = {
			key: guid,
			name: configurationName,
			value: configuration,
			parentGuid: parentGuid,
			parentName: parentName,
			mainGuid: mainGUID,
			addOnDisplayName: addOnDisplayName,
			type: type
		};
	}
};

/*
 * Get configuration from flattened map
 * configurationGuid - configuration GUID
 */
SC_Util.getFromFlattenedConfigurations = function (configurations, configurationGuid) {
	const config = configurations[configurationGuid];

	// if (config && config.parentGuid) {
	//     return config.value;
	// } else if (config && !config.parentGuid) {
	//     return config.value;
	// }

	return config.value;
};

/*
 * Returns configuration from the flat list object
 * config - flat config entry
 */
SC_Util.unpackFlattConfiguration = function (config) {
	if (config && config.parentGuid) {
		return config.value;
	} else if (config && !config.parentGuid) {
		return config.value;
	}

	return config;
};

/*
 * Add configuration to the flattened structure
 * configuration - configuration
 */
SC_Util.addConfigurationToTheFlattenedStructure = function (configurations, configuration) {
	if (!configurations) {
		configurations = {};
	}

	configurations[configuration.guid] = { key: configuration.guid, value: configuration, parentGuid: null };
};

/*
 * Add child configuration to the flattened structure
 * configuration - parent configuration
 * relatedProduct - child configuration
 */
SC_Util.addChildConfigurationToTheFlattenedStructure = function (configurations, configuration, relatedProduct) {
	if (!configurations) {
		configurations = {};
	}

	configurations[relatedProduct.guid] = { key: relatedProduct.guid, value: relatedProduct, parentGuid: configuration.guid };
};

/*
 * Generate update attribute map
 * config - configuration
 * key - field name
 * value - field value
 */
SC_Util.getUpdateAttributeMap = function (config, key, value) {
	let updateMap = {};

	updateMap[config.guid] = [];

	for (let j = 0; j < config.attributes.length; j++) {
		const attr = config.attributes[j];

		if (attr.name === key) {
			updateMap[config.guid].push({ name: key, value: { value: value } });
		}
	}

	return updateMap;
};

/*
 * Generate update attribute map
 * guid - configuration guid
 * key - field name
 * value - field value
 */
SC_Util.getUpdateAttributeMapGUID = function (key, value, displayValue, readOnly, showInUi, required) {
	let obj = { name: key, value: value };

	if (displayValue !== undefined) {
		obj.displayValue = displayValue;
	}

	if (readOnly !== undefined) {
		obj.readOnly = readOnly;
	}

	if (showInUi !== undefined) {
		obj.showInUi = showInUi;
	}

	if (required !== undefined) {
		obj.required = required;
	}

	return obj;
};

/*
 * Get attribute field value
 * config - configuration
 * key - field name
 */
SC_Util.getAttributeByName = function (config, key) {
	let attr = null;

	try {
		attr = config.getAttribute(key);
	} catch (error) {
		console.warn('Attribute is missing, Config: ' + config + ', Attribute: ' + key + ', error: ' + error);
	}

	return attr;
};

/*
 * Get attribute field value
 * config - configuration
 * key - field name
 */
SC_Util.getAttributeValue = function (config, key) {
	let value = '';

	try {
		const attr = config.getAttribute(key);
		value = attr.value;
	} catch (error) {
		console.warn('Attribute is missing, Config: ' + config + ', Attribute: ' + key + ', error: ' + error);
	}

	return value;
};

SC_Util.retrieveAttributeList = function (config) {
	return config.getAttributes();
};

SC_Util.getMainSolutionAttribute = function (config, key) {
	if (config.attributes) {
		return config.getAttribute(key);
	}

	const rootKey = Object.keys(config.schema.configurations)[0];
	const root = config.schema.configurations[rootKey];

	let attribute = {
		value: '',
		name: key
	};

	try {
		attribute = root.getAttribute(key);
	} catch (error) {
		console.warn('Attribute is missing, Config: ' + config + ', Attribute: ' + key + ', error: ' + error);
	}

	return attribute;
};

/*
 * Get attribute field value
 * config - configuration
 * key - field name
 */
SC_Util.getMainSolutionAttributeValue = function (config, key) {
	return SC_Util.getMainSolutionAttribute(config, key).value;
};

/*
 * Get Main solution using solution name
 * configName - configuration name
 */
SC_Util.getMainSolutionByNameAsync = function (configurations, configName) {
	return SC_Util.getAllConfigurationsWithNameAsync(configurations, configName).then(function (configs) {
		return configs ? SC_Util.unpackFlattConfiguration(configs[0]) : null;
	});
};

/*
 * Get all configuration with name
 * configName - configuration name
 */
SC_Util.getAllConfigurationsWithNameAsync = function (configurations, configName) {
	function getAllConfigurationsWithNameInner(configurations, configName) {
		let flattened = Object.values(configurations).filter(function (flat) {
			return flat.name === configName;
		});

		return flattened;
	}

	return Promise.resolve(getAllConfigurationsWithNameInner(configurations, configName));
};

SC_Util.getAllAddonsUsingDisplayNameAsync = function (configurations, configName) {
	function getAllConfigurationsWithNameInner(configurations, configName) {
		let flattened = Object.values(configurations).filter(function (flat) {
			return flat.addOnDisplayName === configName;
		});

		return flattened;
	}

	return Promise.resolve(getAllConfigurationsWithNameInner(configurations, configName));
};

SC_Util.getAllConfigurationsUsingParentGuidAsync = function (configurations, parentGuid) {
	function getAllConfigurationsWithNameInner(configurations, parentGuid) {
		let flattened = Object.values(configurations).filter(function (flat) {
			return flat.parentGuid && flat.parentGuid === parentGuid;
		});

		return flattened;
	}

	return Promise.resolve(getAllConfigurationsWithNameInner(configurations, parentGuid));
};

/*
 * Get all configuration using name and parentGuid
 * configName - configuration name
 * parentGuid - parent guid
 */
SC_Util.getAllConfigurationsWithNameAndParentGuidAsync = function (configurations, configName, parentGuid) {
	function getAllConfigurationsWithNameInner(configurations, configName) {
		let flattened = Object.values(configurations).filter(function (flat) {
			return flat.name === configName && flat.parentGuid === parentGuid;
		});

		return flattened;
	}

	return Promise.resolve(getAllConfigurationsWithNameInner(configurations, configName));
};

/* Petar Matkovic April 17
 * Get all related configuration using name of related configuration
 * configurations - related config list
 * name - name of related products to be extracted
 */
SC_Util.getAllRelatedConfigurationsWithName = function (configurations, configName) {
	function getAllRelatedConfigurationsWithNameInner(configurations, configName) {
		let flattened = Object.values(configurations).filter(function (flat) {
			return flat.name === configName;
		});

		return flattened;
	}

	return Promise.resolve(getAllRelatedConfigurationsWithNameInner(configurations, configName));
};

/*
 * Get all child configurations using parent configuration GUID
 * parentGuid - parent configuration GUID
 */
SC_Util.getAllChildConfigurationsAync = function (configurations, parentGuid) {
	function getAllChildConfigurationsInner(parentGuid) {
		return Object.values(configurations).filter(function (elem) {
			return elem.parentGuid === parentGuid;
		});
	}

	return new Promise(function (resolve) {
		resolve(getAllChildConfigurationsInner(parentGuid));
	});
};

/*
 * Generate attribute
 * name - attribute name
 * value - attribute value
 */
SC_Util.generateAttribute = function (name, value, showInUi, override) {
	if (!name) {
		throw 'Invalid name!';
	}

	let attribute = { name: name.toLowerCase(), value: { value: value, showInUi: showInUi } };

	if (override) {
		attribute = { name: name.toLowerCase(), value: value, showInUi: showInUi };
	}

	if (showInUi === undefined) {
		delete attribute.showInUi;
		delete attribute.value.showInUi;
	}

	return attribute;
};

/*
 * Generate lookup attribute
 * name - attribute name
 * value - attribute value
 * displayValue - attribute display value
 */
SC_Util.generateLookupAttribute = function (name, value, displayValue, showInUi, override) {
	if (!name) {
		throw 'Invalid name!';
	}

	if (override) {
		return { name: name.toLowerCase(), value: value, displayValue: displayValue, showInUi: showInUi };
	}

	return { name: name.toLowerCase(), value: { value: value, displayValue: displayValue, showInUi: showInUi } };
};

/*
 * Generate basic UI element
 * attributeName - attribute name
 * readOnly - readOnly flag, use undefined if not relevant
 * required - required flag, use undefined if not relevant
 * showInUi - showInUi flag, use undefined if not relevant
 * value - attribute value, use undefined if not relevant
 */
SC_Util.getUIBasicUIElement = function (attributeName, readOnly, required, showInUi, value) {
	let obj = {
		name: attributeName
	};

	if (readOnly !== undefined) {
		obj['readOnly'] = readOnly;
	}

	if (required !== undefined) {
		obj['required'] = required;
	}

	if (showInUi !== undefined) {
		obj['showInUi'] = showInUi;
	}

	if (value !== undefined) {
		obj['value'] = value ? value : '';
	}

	return obj;
};

/*
 * Clear trailing numbers
 * configurationName - configuration name
 */
SC_Util.clearTrailingNumbers = function (configurationName) {
	if (!configurationName) {
		configurationName = '';
	}

	return configurationName.replace(/\s\d+/g, '');
};

/*
 * Clear trailing numbers
 * configurationName - configuration name
 */
SC_Util.getConfigurationName = function (configurations, GUID) {
	return configurations[GUID].name;
};

SC_Util.getMainConfiguration = function (solution) {
	const rootKey = Object.keys(solution.schema.configurations)[0];
	const root = solution.schema.configurations[rootKey];

	return root;
};

SC_Util.getMainConfigurationGUID = function (solution) {
	const rootKey = Object.keys(solution.schema.configurations)[0];
	const root = solution.schema.configurations[rootKey];

	return root.guid;
};

SC_Util.getConfigurationIdAsync = function (solutionName, basket) {
	if (!basket) {
		return CS.SM.getActiveBasket().then(function (basket) {
			return resolveBasketValue(basket);
		});
	} else {
		return Promise.resolve(resolveBasketValue(basket));
	}

	function resolveBasketValue(basket) {
		const data = basket.getSolutionTemplates();
		const keys = Object.keys(data);

		let solution;
		for (let i = 0; i < keys.length; i++) {
			const key = keys[i];
			if (data[key].name === solutionName) {
				solution = data[key];
			}
		}

		return solution;
	}
};

SC_Util.getAddonsForConfigurationAsync = function (solution, componentName, componentGuid) {
	const componentType = solution.getComponentByName(componentName);
	return componentType.getAddonsForConfiguration(componentGuid).catch((err) => {
		if (err === 'Price item not found to fetch addons') {
			return {};
		}
		throw err;
	});
};

SC_Util.cloneConfiguration = function (originalConfiguration) {
	let configurationTemplate = [];

	const attributes = originalConfiguration.attributes;

	const skip = ['guid', 'solutionid'];

	Object.keys(attributes).forEach((attrKey) => {
		const attr = attributes[attrKey];

		if (skip.indexOf(attr.name.toLowerCase()) === -1) {
			console.log(attr);
			if (attr.type === 'Lookup') {
				configurationTemplate.push(SC_Util.generateLookupAttribute(attr.name, attr.value, attr.displayValue, attr.showInUi, false));
			} else {
				configurationTemplate.push(SC_Util.generateAttribute(attr.name, attr.value, attr.showInUi, false));
			}
		}
	});

	return configurationTemplate;
};

SC_Util.cloneRelatedProducts = function (originalConfiguration) {
	let relatedProductTemplates = [];

	const keys = Object.keys(originalConfiguration.relatedProducts);

	for (let i = 0; i < keys.length; i++) {
		const key = keys[i];
		const rp = originalConfiguration.relatedProducts[key];

		if (rp.type === 'Related Component') {
			relatedProductTemplates.push({ name: rp.name, template: SC_Util.cloneConfiguration(rp.configuration) });
		}
	}

	return relatedProductTemplates;
};

SC_Util.cloneAddons = function (originalConfiguration) {
	let relatedProductTemplates = [];

	const keys = Object.keys(originalConfiguration.relatedProducts);

	for (let i = 0; i < keys.length; i++) {
		const key = keys[i];
		const rp = originalConfiguration.relatedProducts[key];

		if (rp.type === 'Add On Component') {
			relatedProductTemplates.push({ name: rp.name, template: SC_Util.cloneConfiguration(rp.configuration) });
		}
	}

	return relatedProductTemplates;
};

SC_Util.addRelatedProductsToConfigurationAsync = function (configType, configuration, relatedProductTemplates) {
	let promiseChain = Promise.resolve();

	for (let i = 0; i < relatedProductTemplates.length; i++) {
		const relatedProductData = relatedProductTemplates[i];

		promiseChain = promiseChain.then(function () {
			const relatedProduct = configType.createRelatedProduct(relatedProductData.name, relatedProductData.template);
			return configType.addRelatedProduct(configuration.guid, relatedProduct, true).then((product) => {
				product.validate();
				return true;
			});
		});
	}

	return promiseChain;
};

SC_Util.packAddOns = function (originalConfiguration, availableAddons) {
	const originalAddons = originalConfiguration.getAddons();
	const keys = Object.keys(originalAddons);

	let addons = [];

	for (let i = 0; i < keys.length; i++) {
		const key = keys[i];
		const addonConfig = originalAddons[key].configuration;
		const addonConfigId = SC_Util.getAttributeValue(addonConfig, 'add on');
		const quantity = SC_Util.getAttributeValue(addonConfig, SC_Common_Attributes.Quantity);

		const addOn = SC_Util.recursiveSearch(availableAddons, 'Id', addonConfigId);
		console.log('addOn...');
		console.log(addOn);
		if (addOn) {
			addons.push({
				addonPriceItemId: addOn.cspmb__Add_On_Price_Item__c,
				associationId: addOn.Id,
				group: addOn.cspmb__Group__c,
				id: addOn.Id,
				name: addOn.cspmb__Add_On_Price_Item__r.Name,
				quantity: quantity ? quantity : 0,
				raw: addOn,
				relatedProductName: 'Add-On'
			});
		}
	}

	return addons;
};

SC_Util.addAddonsAsync = function (configType, configuration, addons) {
	if (!Array.isArray(addons)) {
		addons = [addons];
	}

	let addOnPromiseChain = Promise.resolve();

	for (let i = 0; i < addons.length; i++) {
		addOnPromiseChain = addOnPromiseChain.then(function () {
			return configType
				.addAddOn(configuration.guid, addons[i])
				.then((configuration) => {
					console.log(configuration);
				})
				.catch((err) => {
					console.log(err);
					return true;
				});
		});
	}

	return addOnPromiseChain;
};

SC_Util.recursiveSearch = function (obj, key, value) {
	let target;

	Object.keys(obj).some(function (elem) {
		if (elem === key && obj[elem] === value) {
			target = obj;
			return true;
		}

		if (obj[elem] && typeof obj[elem] === 'object') {
			target = SC_Util.recursiveSearch(obj[elem], key, value);
			return target !== undefined;
		}
	});

	return target;
};

SC_Util.getMainConfigurationPacked = function (configurations) {
	for (let i = 0; i < configurations.length; i++) {
		let config = configurations[i];

		if (config.type === 'Main') {
			return config;
		}
	}
};

SC_Util.getMainConfiguration = function (configurations) {
	let config = SC_Util.getMainConfigurationPacked(configurations);
	return SC_Util.unpackFlattConfiguration(config);
};

SC_Util.makeAllConfigsValid = function (configs) {
	Object.values(configs).forEach((item) => {
		item.value.updateStatus(true, '');
	});
};

// Using global object until support for import, export is fully there
// This has to be the last line in the file
Object.freeze(SC_Util);
window.SC_Util = SC_Util;
