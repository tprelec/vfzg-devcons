var one100IAD = 8;
var one700IAD = 30;
var oneFixedIADInfoMessage = 'Max of 3 IADs can be added!';

//Map that contains the constants for calculating minBanwidth for OneFixed
var oneFixedMap = new Map();
oneFixedMap.set('G711|ADSL', 111.3);
oneFixedMap.set('G711|SDSL', 114.3);
oneFixedMap.set('G711|VDSL', 105);
oneFixedMap.set('G711|VVDSL', 105);
oneFixedMap.set('G711|BVDSL', 105);
oneFixedMap.set('G711|Coax', 100);
oneFixedMap.set('G711|EthernetOverFiber', 101.64);
oneFixedMap.set('G711|FTTH', 101.64);
oneFixedMap.set('G711|EthernetOverCopper', 101.64);
oneFixedMap.set('G729|ADSL', 66.78);
oneFixedMap.set('G729|SDSL', 66.78);
oneFixedMap.set('G729|VDSL', 46.2);
oneFixedMap.set('G729|VVDSL', 46.2);
oneFixedMap.set('G729|BVDSL', 46.2);
oneFixedMap.set('G729|FTTH', 42.84);
oneFixedMap.set('G729|EthernetOverFiber', 42.84);
oneFixedMap.set('G729|EthernetOverCopper', 42.84);
oneFixedMap.set('G729|Coax', 50);

// added 26.07. CSP support
oneFixedMap.set('G711|EthernetOverCopperSingle', 101.64);

//Map that contains the constants for calculating minBanwidth for OneNet
var oneNetMap = new Map();
oneNetMap.set('G711|ADSL', 111.3);
oneNetMap.set('G711|SDSL', 114.3);
oneNetMap.set('G711|VDSL', 105);
oneNetMap.set('G711|VVDSL', 105);
oneNetMap.set('G711|BVDSL', 105);
oneNetMap.set('G711|Coax', 100);
oneNetMap.set('G711|EthernetOverFiber', 101.64);
oneNetMap.set('G711|FTTH', 101.64);
oneNetMap.set('G711|EthernetOverCopper', 101.64);

window.checkBundleSelectionError = function checkBundleSelectionError() {
	if (
		CS.getAttributeValue('Site_Check_0') != '' &&
		CS.Service.config[priceItemAttributes.bundles.relProductAttribute + ':' + priceItemAttributes.bundles.lookupAttributeChild]
	) {
		if (CS.getAttributeValue(priceItemAttributes.bundles.relProductAttribute + ':' + priceItemAttributes.bundles.lookupAttributeChild) == '') {
			CS.markConfigurationInvalid(priceItemAttributes.bundles.errorMessage);
		}
	}
};

window.hideModularRelatedProducts = function hideModularRelatedProducts() {
	hideRealtedProductAttribute('Access_Infrastructure_0');
	hideRealtedProductAttribute('EVC_PVC_dep_0');
	hideRealtedProductAttribute('SIP_Package_0');
	hideRealtedProductAttribute('SFP_0');
	hideRealtedProductAttribute('CPE_0');
	hideRealtedProductAttribute('CPE_SLA_items_0');
	hideRealtedProductAttribute('Price_Item_Wireless_0');
	//Voorloop_Wireless_items_0 should be visible for Bundles
	//hideRealtedProductAttribute('Voorloop_Wireless_items_0');
	hideRealtedProductAttribute('Other_price_items_0');
	hideRealtedProductAttribute('Miscellaneous_0');
};

window.showModularRelatedProducts = function showModularRelatedProducts() {
	showRealtedProductAttribute('Access_Infrastructure_0');
	showRealtedProductAttribute('EVC_PVC_dep_0');
	showRealtedProductAttribute('SIP_Package_0');
	showRealtedProductAttribute('SFP_0');
	showRealtedProductAttribute('CPE_0');
	showRealtedProductAttribute('CPE_SLA_items_0');
	showRealtedProductAttribute('Price_Item_Wireless_0');
	//Voorloop_Wireless_items_0 should be visible for Bundles
	//showRealtedProductAttribute('Voorloop_Wireless_items_0');
	showRealtedProductAttribute('Other_price_items_0');
	showRealtedProductAttribute('Miscellaneous_0');
};

window.showBundleRelatedProducts = function showBundleRelatedProducts() {
	showRealtedProductAttribute('Bundle_Products_0');
};

window.hideBundleRelatedProducts = function hideBundleRelatedProducts() {
	hideRealtedProductAttribute('Bundle_Products_0');
};

function hideRealtedProductAttribute(attrName) {
	$j('div[data-cs-binding = ' + attrName + ']').hide();
}

function showRealtedProductAttribute(attrName) {
	$j('div[data-cs-binding = ' + attrName + ']').show();
}

window.clearBundleConfigurationProductsOnly = function clearBundleConfigurationProductsOnly() {
	var bundleProductsDeleted = false;

	if (CS.Service.config['Bundle_Products_0'].relatedProducts.length > 0) {
		_.each(CS.Service.config['Bundle_Products_0'].relatedProducts, function (p) {
			CS.Service.removeRelatedProduct('Bundle_Products_0');
			bundleProductsDeleted = true;
		});
	}

	if (bundleProductsDeleted) {
		if (CS.getAttributeValue('Bundle_Coax_set_0') !== '') {
			CS.setAttributeValue('Bundle_Coax_set_0', '');
		}

		if (CS.getAttributeValue('Bundle_set_0') !== '') {
			CS.setAttributeValue('Bundle_set_0', '');
		}
	}
};

window.clearBundleConfiguration = function clearBundleConfiguration() {
	if (CS.getAttributeValue('Site_Check_0') !== '') {
		CS.Util.SelectListLookup.clear('Site_Check_0');
	}

	if (CS.getAttributeValue('Bundle_Coax_set_0') !== '') {
		CS.setAttributeValue('Bundle_Coax_set_0', '');
	}

	if (CS.getAttributeValue('Bundle_set_0') !== '') {
		CS.setAttributeValue('Bundle_set_0', '');
	}

	if (CS.Service.config['Bundle_Products_0'].relatedProducts.length > 0) {
		_.each(CS.Service.config['Bundle_Products_0'].relatedProducts, function (p) {
			CS.Service.removeRelatedProduct('Bundle_Products_0');
		});
	}
};

window.clearAccessConfiguration = function clearAccessConfiguration() {
	if (CS.UI.getCurrentInstance().getCurrentConfigReference() === '') {
		if (CS.getAttributeValue('Site_Check_0') !== '') {
			CS.Util.SelectListLookup.clear('Site_Check_0');
		}

		// commented for easy reversal if needed
		// if(CS.getAttributeValue('Target_access_infrastructure_0') !== ''){
		//     CS.Util.SelectListLookup.clear('Target_access_infrastructure_0')
		// }

		if (CS.getAttributeValue('SiteWithExistingInfraExists_0') !== '') {
			CS.setAttributeValue('SiteWithExistingInfraExists_0', '');
		}
		if (CS.getAttributeValue('ExistingInfraValueSet_0') !== '') {
			CS.setAttributeValue('ExistingInfraValueSet_0', '');
		}

		if (CS.getAttributeValue('Access_set_0') !== '') {
			CS.setAttributeValue('Access_set_0', '');
		}

		/*if(CS.getAttributeValue('Access_PGA_set_0') !== ''){ 
	CS.setAttributeValue('Access_PGA_set_0',''); 
	} */

		if (CS.getAttributeValue('ManagedInternetEVCPVCSet_0') !== '') {
			CS.setAttributeValue('ManagedInternetEVCPVCSet_0', '');
		}

		if (CS.getAttributeValue('ManagedInternetPgaSet_0') !== '') {
			CS.setAttributeValue('ManagedInternetPgaSet_0', '');
		}

		if (CS.getAttributeValue('ManagedInternetEVCPVCError_0') !== '') {
			CS.setAttributeValue('ManagedInternetEVCPVCError_0', '');
		}

		if (CS.getAttributeValue('CPE_set_0') !== '') {
			CS.setAttributeValue('CPE_set_0', '');
		}

		if (CS.getAttributeValue('CPE_PGA_set_0') !== '') {
			CS.setAttributeValue('CPE_PGA_set_0', '');
		}

		if (CS.getAttributeValue('CPE_SLA_PGA_set_0') !== '') {
			CS.setAttributeValue('CPE_SLA_PGA_set_0', '');
		}

		if (CS.getAttributeValue('EVC1_PGA_set_0') !== '') {
			CS.setAttributeValue('EVC1_PGA_set_0', '');
		}

		if (CS.getAttributeValue('EVC2_PGA_set_0') !== '') {
			CS.setAttributeValue('EVC2_PGA_set_0', '');
		}

		if (CS.getAttributeValue('Hardware_Toeslag_PGA_set_0') !== '') {
			CS.setAttributeValue('Hardware_Toeslag_PGA_set_0', '');
		}

		if (CS.getAttributeValue('Voorloop_Wireless_PGA_set_0') !== '') {
			CS.setAttributeValue('Voorloop_Wireless_PGA_set_0', '');
		}

		/*if(CS.getAttributeValue('Wireless_Installation_fee_PGA_set_0') != ''){ 
	CS.setAttributeValue('Wireless_Installation_fee_PGA_set_0',''); 
	}*/

		if (CS.getAttributeValue('Wireless_Price_Plan_PGA_set_0') !== '') {
			CS.setAttributeValue('Wireless_Price_Plan_PGA_set_0', '');
		}

		if (CS.getAttributeValue('EVC1_set_0') !== '') {
			CS.setAttributeValue('EVC1_set_0', '');
		}

		if (CS.getAttributeValue('EVC1_VLAN_set_0') !== '') {
			CS.setAttributeValue('EVC1_VLAN_set_0', '');
		}

		if (CS.getAttributeValue('SFP_Data_set_0') !== '') {
			CS.setAttributeValue('SFP_Data_set_0', '');
		}

		if (CS.getAttributeValue('EVC2_set_0') !== '') {
			CS.setAttributeValue('EVC2_set_0', '');
		}

		if (CS.getAttributeValue('EVC2_VLAN_set_0') !== '') {
			CS.setAttributeValue('EVC2_VLAN_set_0', '');
		}

		if (CS.getAttributeValue('EVC1_error_0') !== '') {
			CS.setAttributeValue('EVC1_error_0', '');
		}

		if (CS.getAttributeValue('EVC1_VLAN_error_0') !== '') {
			CS.setAttributeValue('EVC1_VLAN_error_0', '');
		}

		if (CS.getAttributeValue('EVC2_VLAN_error_0') !== '') {
			CS.setAttributeValue('EVC2_VLAN_error_0', '');
		}

		if (CS.getAttributeValue('TransportEVC_error_0') !== '') {
			CS.setAttributeValue('TransportEVC_error_0', '');
		}
		if (CS.getAttributeValue('TransportEVC_set_0') !== '') {
			CS.setAttributeValue('TransportEVC_set_0', '');
		}

		if (CS.getAttributeValue('AccessLineTypeInstallationFeeSet_0') !== '') {
			CS.setAttributeValue('AccessLineTypeInstallationFeeSet_0', '');
		}

		/*if(CS.getAttributeValue('AccessDealTypeInstallationFeeSet_0') != ''){ 
	CS.setAttributeValue('AccessDealTypeInstallationFeeSet_0',''); 
	} */

		if (CS.getAttributeValue('Total_Bandwidth_Up_0') !== '') {
			CS.setAttributeValue('Total_Bandwidth_Up_0', 0);
		}

		if (CS.getAttributeValue('Total_Bandwidth_Down_0') !== '') {
			CS.setAttributeValue('Total_Bandwidth_Down_0', 0);
		}

		if (CS.getAttributeValue('EVC_done_0') !== '') {
			CS.setAttributeValue('EVC_done_0', '');
		}

		if (CS.getAttributeValue('TransportEVC_Available_Down_BW_0') !== '') {
			CS.setAttributeValue('TransportEVC_Available_Down_BW_0', '');
		}

		if (CS.getAttributeValue('TransportEVC_Available_Up_BW_0') !== '') {
			CS.setAttributeValue('TransportEVC_Available_Up_BW_0', '');
		}

		if (CS.getAttributeValue('EVCPVC_OneFixed_set_0') !== '') {
			CS.setAttributeValue('EVCPVC_OneFixed_set_0', '');
		}

		if (CS.getAttributeValue('EVCPVC_OneFixed_error_0') !== '') {
			CS.setAttributeValue('EVCPVC_OneFixed_error_0', '');
		}

		if (CS.getAttributeValue('OneFixed_SIP_Trunk_error_0') !== '') {
			CS.setAttributeValue('OneFixed_SIP_Trunk_error_0', '');
		}

		if (CS.getAttributeValue('OneFixed_SIP_Trunk_set_0') !== '') {
			CS.setAttributeValue('OneFixed_SIP_Trunk_set_0', '');
		}

		if (CS.getAttributeValue('OneNet_SIP_Trunk_error_0') !== '') {
			CS.setAttributeValue('OneNet_SIP_Trunk_error_0', '');
		}

		if (CS.getAttributeValue('OneNet_SIP_Trunk_set_0') !== '') {
			CS.setAttributeValue('OneNet_SIP_Trunk_set_0', '');
		}

		if (CS.getAttributeValue('OneFixedIAD_error_0') !== '') {
			CS.setAttributeValue('OneFixedIAD_error_0', '');
		}

		if (CS.getAttributeValue('OneFixedIAD_set_0') !== '') {
			CS.setAttributeValue('OneFixedIAD_set_0', '');
		}

		if (CS.getAttributeValue('IAD_CPE_set_0') !== '') {
			CS.setAttributeValue('IAD_CPE_set_0', '');
		}

		if (CS.getAttributeValue('OneFixedCUBE_set_0') !== '') {
			CS.setAttributeValue('OneFixedCUBE_set_0', '');
		}

		if (CS.getAttributeValue('OneFixedCUBE_error_0') !== '') {
			CS.setAttributeValue('OneFixedCUBE_error_0', '');
		}

		if (CS.getAttributeValue('EVCPVC_OneNet_set_0') !== '') {
			CS.setAttributeValue('EVCPVC_OneNet_set_0', '');
		}

		if (CS.getAttributeValue('EVCPVC_OneNet_set_0') !== '') {
			CS.setAttributeValue('EVCPVC_OneNet_set_0', '');
		}

		//CU AN
		if (CS.getAttributeValue('CPE_error_0') !== '') {
			CS.setAttributeValue('CPE_error_0', '');
		}

		if (CS.getAttributeValue('Access_error_0') !== '') {
			CS.setAttributeValue('Access_error_0', '');
		}

		if (CS.getAttributeValue('OneFixed_SBC_SIP_Sessions_set_0') !== '') {
			CS.setAttributeValue('OneFixed_SBC_SIP_Sessions_set_0', '');
		}
		//end

		if (CS.Service.config['Access_Infrastructure_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['Access_Infrastructure_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('Access_Infrastructure_0');
			});
		}

		if (CS.Service.config['EVC_PVC_dep_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['EVC_PVC_dep_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('EVC_PVC_dep_0');
			});
		}

		if (CS.Service.config['SFP_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['SFP_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('SFP_0');
			});
		}

		if (CS.Service.config['CPE_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['CPE_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('CPE_0');
			});
		}

		if (CS.Service.config['CPE_SLA_items_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['CPE_SLA_items_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('CPE_SLA_items_0');
			});
		}

		if (CS.Service.config['Price_Item_Hardware_Toeslag_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['Price_Item_Hardware_Toeslag_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('Price_Item_Hardware_Toeslag_0');
			});
		}

		if (CS.Service.config['SIP_Package_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['SIP_Package_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('SIP_Package_0');
			});
		}

		if (CS.Service.config['Miscellaneous_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['Miscellaneous_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('Miscellaneous_0');
			});
		}

		//EVCPVC removal set
		if (CS.getAttributeValue('RemoveEVCCPE_0') !== '' && CS.getAttributeValue('RemoveEVCCPE_0') !== null) {
			CS.setAttributeValue('RemoveEVCCPE_0', false);
		}

		if (CS.getAttributeValue('EVCCPE_removed_0') !== '') {
			CS.setAttributeValue('EVCCPE_removed_0', '');
		}
	}
};

window.clearAccessConfigurationProductsOnly = function clearAccessConfigurationProductsOnly() {
	if (CS.UI.getCurrentInstance().getCurrentConfigReference() === '') {
		// commented for easy reversal if needed
		// if(CS.getAttributeValue('Target_access_infrastructure_0') !== ''){
		//     CS.Util.SelectListLookup.clear('Target_access_infrastructure_0')
		// }

		if (CS.getAttributeValue('SiteWithExistingInfraExists_0') !== '') {
			CS.setAttributeValue('SiteWithExistingInfraExists_0', '');
		}
		if (CS.getAttributeValue('ExistingInfraValueSet_0') !== '') {
			CS.setAttributeValue('ExistingInfraValueSet_0', '');
		}

		if (CS.getAttributeValue('Access_set_0') !== '') {
			CS.setAttributeValue('Access_set_0', '');
		}

		/*if(CS.getAttributeValue('Access_PGA_set_0') !== ''){ 
	CS.setAttributeValue('Access_PGA_set_0',''); 
	} */

		if (CS.getAttributeValue('ManagedInternetEVCPVCSet_0') !== '') {
			CS.setAttributeValue('ManagedInternetEVCPVCSet_0', '');
		}

		if (CS.getAttributeValue('ManagedInternetPgaSet_0') !== '') {
			CS.setAttributeValue('ManagedInternetPgaSet_0', '');
		}

		if (CS.getAttributeValue('ManagedInternetEVCPVCError_0') !== '') {
			CS.setAttributeValue('ManagedInternetEVCPVCError_0', '');
		}

		if (CS.getAttributeValue('CPE_set_0') !== '') {
			CS.setAttributeValue('CPE_set_0', '');
		}

		if (CS.getAttributeValue('CPE_PGA_set_0') !== '') {
			CS.setAttributeValue('CPE_PGA_set_0', '');
		}

		if (CS.getAttributeValue('CPE_SLA_PGA_set_0') !== '') {
			CS.setAttributeValue('CPE_SLA_PGA_set_0', '');
		}

		if (CS.getAttributeValue('EVC1_PGA_set_0') !== '') {
			CS.setAttributeValue('EVC1_PGA_set_0', '');
		}

		if (CS.getAttributeValue('EVC2_PGA_set_0') !== '') {
			CS.setAttributeValue('EVC2_PGA_set_0', '');
		}

		if (CS.getAttributeValue('Hardware_Toeslag_PGA_set_0') !== '') {
			CS.setAttributeValue('Hardware_Toeslag_PGA_set_0', '');
		}

		if (CS.getAttributeValue('Voorloop_Wireless_PGA_set_0') !== '') {
			CS.setAttributeValue('Voorloop_Wireless_PGA_set_0', '');
		}

		/*if(CS.getAttributeValue('Wireless_Installation_fee_PGA_set_0') != ''){ 
	CS.setAttributeValue('Wireless_Installation_fee_PGA_set_0',''); 
	}*/

		if (CS.getAttributeValue('Wireless_Price_Plan_PGA_set_0') !== '') {
			CS.setAttributeValue('Wireless_Price_Plan_PGA_set_0', '');
		}

		if (CS.getAttributeValue('EVC1_set_0') !== '') {
			CS.setAttributeValue('EVC1_set_0', '');
		}

		if (CS.getAttributeValue('EVC1_VLAN_set_0') !== '') {
			CS.setAttributeValue('EVC1_VLAN_set_0', '');
		}

		if (CS.getAttributeValue('SFP_Data_set_0') !== '') {
			CS.setAttributeValue('SFP_Data_set_0', '');
		}

		if (CS.getAttributeValue('EVC2_set_0') !== '') {
			CS.setAttributeValue('EVC2_set_0', '');
		}

		if (CS.getAttributeValue('EVC2_VLAN_set_0') !== '') {
			CS.setAttributeValue('EVC2_VLAN_set_0', '');
		}

		if (CS.getAttributeValue('EVC1_error_0') !== '') {
			CS.setAttributeValue('EVC1_error_0', '');
		}

		if (CS.getAttributeValue('EVC1_VLAN_error_0') !== '') {
			CS.setAttributeValue('EVC1_VLAN_error_0', '');
		}

		if (CS.getAttributeValue('EVC2_VLAN_error_0') !== '') {
			CS.setAttributeValue('EVC2_VLAN_error_0', '');
		}

		if (CS.getAttributeValue('TransportEVC_error_0') !== '') {
			CS.setAttributeValue('TransportEVC_error_0', '');
		}
		if (CS.getAttributeValue('TransportEVC_set_0') !== '') {
			CS.setAttributeValue('TransportEVC_set_0', '');
		}

		if (CS.getAttributeValue('AccessLineTypeInstallationFeeSet_0') !== '') {
			CS.setAttributeValue('AccessLineTypeInstallationFeeSet_0', '');
		}

		/*if(CS.getAttributeValue('AccessDealTypeInstallationFeeSet_0') != ''){ 
	CS.setAttributeValue('AccessDealTypeInstallationFeeSet_0',''); 
	} */

		if (CS.getAttributeValue('Total_Bandwidth_Up_0') !== '') {
			CS.setAttributeValue('Total_Bandwidth_Up_0', 0);
		}

		if (CS.getAttributeValue('Total_Bandwidth_Down_0') !== '') {
			CS.setAttributeValue('Total_Bandwidth_Down_0', 0);
		}

		if (CS.getAttributeValue('EVC_done_0') !== '') {
			CS.setAttributeValue('EVC_done_0', '');
		}

		if (CS.getAttributeValue('TransportEVC_Available_Down_BW_0') !== '') {
			CS.setAttributeValue('TransportEVC_Available_Down_BW_0', '');
		}

		if (CS.getAttributeValue('TransportEVC_Available_Up_BW_0') !== '') {
			CS.setAttributeValue('TransportEVC_Available_Up_BW_0', '');
		}

		if (CS.getAttributeValue('EVCPVC_OneFixed_set_0') !== '') {
			CS.setAttributeValue('EVCPVC_OneFixed_set_0', '');
		}

		if (CS.getAttributeValue('EVCPVC_OneFixed_error_0') !== '') {
			CS.setAttributeValue('EVCPVC_OneFixed_error_0', '');
		}

		if (CS.getAttributeValue('OneFixed_SIP_Trunk_error_0') !== '') {
			CS.setAttributeValue('OneFixed_SIP_Trunk_error_0', '');
		}

		if (CS.getAttributeValue('OneFixed_SIP_Trunk_set_0') !== '') {
			CS.setAttributeValue('OneFixed_SIP_Trunk_set_0', '');
		}

		if (CS.getAttributeValue('OneFixedIAD_error_0') !== '') {
			CS.setAttributeValue('OneFixedIAD_error_0', '');
		}

		if (CS.getAttributeValue('OneFixedIAD_set_0') !== '') {
			CS.setAttributeValue('OneFixedIAD_set_0', '');
		}

		if (CS.getAttributeValue('IAD_CPE_set_0') !== '') {
			CS.setAttributeValue('IAD_CPE_set_0', '');
		}

		if (CS.getAttributeValue('OneFixedCUBE_set_0') !== '') {
			CS.setAttributeValue('OneFixedCUBE_set_0', '');
		}

		if (CS.getAttributeValue('OneFixedCUBE_error_0') !== '') {
			CS.setAttributeValue('OneFixedCUBE_error_0', '');
		}

		//CU AN
		if (CS.getAttributeValue('CPE_error_0') !== '') {
			CS.setAttributeValue('CPE_error_0', '');
		}

		if (CS.getAttributeValue('Access_error_0') !== '') {
			CS.setAttributeValue('Access_error_0', '');
		}

		if (CS.getAttributeValue('OneFixed_SBC_SIP_Sessions_set_0') !== '') {
			CS.setAttributeValue('OneFixed_SBC_SIP_Sessions_set_0', '');
		}

		//end

		if (CS.Service.config['Access_Infrastructure_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['Access_Infrastructure_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('Access_Infrastructure_0');
			});
		}

		if (CS.Service.config['EVC_PVC_dep_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['EVC_PVC_dep_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('EVC_PVC_dep_0');
			});
		}

		if (CS.Service.config['SFP_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['SFP_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('SFP_0');
			});
		}

		if (CS.Service.config['CPE_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['CPE_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('CPE_0');
			});
		}

		if (CS.Service.config['CPE_SLA_items_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['CPE_SLA_items_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('CPE_SLA_items_0');
			});
		}

		if (CS.Service.config['Price_Item_Hardware_Toeslag_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['Price_Item_Hardware_Toeslag_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('Price_Item_Hardware_Toeslag_0');
			});
		}

		if (CS.Service.config['SIP_Package_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['SIP_Package_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('SIP_Package_0');
			});
		}

		if (CS.Service.config['Miscellaneous_0'].relatedProducts.length > 0) {
			_.each(CS.Service.config['Miscellaneous_0'].relatedProducts, function (p) {
				CS.Service.removeRelatedProduct('Miscellaneous_0');
			});
		}
	}
};

window.checkModeAndSet = function checkModeAndSet() {
	if (CS.getAttributeValue('Modular_pricing_0')) {
		modularMode();
	} else {
		bundleMode();
	}

	disableEVCCPEremovalCheckbox();
};

window.modularMode = function modularMode() {
	var configRef = CS.UI.getCurrentInstance() != undefined ? CS.UI.getCurrentInstance().getCurrentConfigReference() : '';
	if (configRef == '') {
		showModularRelatedProducts();
		hideBundleRelatedProducts();
		CS.EAPI.hideAllAdditionButtons();
	}
};

window.bundleMode = function bundleMode() {
	var configRef = CS.UI.getCurrentInstance() != undefined ? CS.UI.getCurrentInstance().getCurrentConfigReference() : '';

	if (configRef == '') {
		hideModularRelatedProducts();
		showBundleRelatedProducts();
		CS.EAPI.hideNewAdditionButton(priceItemAttributes.bundles.relProductAttribute);
	}
};

window.qosReadOnly = function qosReadOnly() {
	if (CS.Service.config['EVC_PVC_dep_0'].relatedProducts.length > 0) {
		//set QoS to readOnly
		CS.makeAttributeReadOnly(priceItemAttributes.evc1.qualityOfService);
		CS.makeAttributeReadOnly(priceItemAttributes.evc2.qualityOfService);
		CS.makeAttributeReadOnly(priceItemAttributes.evcOneFixed.qualityOfService);
	}
};

window.disableEVCCPEremovalCheckbox = function disableEVCCPEremovalCheckbox() {
	var skillLevel = CS.getAttributeValue('User_Skill_level_0');
	var modularPricing = CS.getAttributeValue('Modular_pricing_0');
	var existingInfra = CS.getAttributeValue('Existing_Infra_0');
	var region = CS.getAttributeValue('Region_0');
	var directIndirect = CS.getAttributeValue('BasketDirectIndirect_0');

	if (region == 'Flex' && directIndirect == 'Indirect' && modularPricing) {
		setTimeout(function () {
			showWholeAttribute('RemoveEVCCPE_0');
		}, 0);
		//CS.setAttributeValue('RemoveEVCCPE_0', true);
	} else {
		if (skillLevel == undefined || skillLevel < 2 || !modularPricing || !existingInfra) {
			setTimeout(function () {
				hideWholeAttribute('RemoveEVCCPE_0');
			}, 0);
		} else if (skillLevel != undefined && skillLevel >= 2 && modularPricing && existingInfra) {
			setTimeout(function () {
				showWholeAttribute('RemoveEVCCPE_0');
			}, 0);
		}
	}
};

window.hideWholeAttribute = function hideWholeAttribute(attName) {
	$j('label[data-cs-label = "' + attName + '"]')
		.parent()
		.parent()
		.hide();
};

window.showWholeAttribute = function showWholeAttribute(attName) {
	$j('label[data-cs-label = "' + attName + '"]')
		.parent()
		.parent()
		.show();
};

window.applySkillBasedLogic = function applySkillBasedLogic() {
	disableEVCCPEremovalCheckbox();

	var productConfigName = CS.Service.config[''].config.Name;
	var currentConfig = CS.UI.getCurrentInstance().getCurrentConfigReference();
	var skillLevel = CS.getAttributeValue('User_Skill_level_0');

	if (skillLevel !== undefined && skillLevel != '') {
		var attName = '';

		_.each(skillBasedLogic[productConfigName][skillLevel], function (element, index) {
			if (currentConfig == '') {
				attName = index;
			} else {
				attName = currentConfig + ':' + index;
			}

			if (CS.Service.config[attName] !== undefined) {
				if (element == 'H') {
					hideAttribute(attName);
					console.log('**** hideAttribute: ' + attName);
				} else if (element == 'R') {
					CS.makeAttributeReadOnly(attName);
					console.log('**** makeAttributeReadOnly: ' + attName);
				}
			}

			if (currentConfig == '') {
				manageRelatedProductAttributes(attName, element);
			}
		});
	}

	function hideAttribute(attName) {
		$j('label[data-cs-label = "' + attName + '"]').hide();
		$j('[data-cs-binding = "' + attName + '"]').hide();
		//CS.Service.config[attName].attr.cscfga__Hidden__c = true;
		//CS.binding.getBindings(attName)[0].element.css({'display':'none'});
	}

	function manageRelatedProductAttributes(attName, visibility) {
		if (visibility == 'H') {
			//hide columns in related product list
			$j('th[data-cs-binding$="' + attName + '"]').hide();
			$j('td[data-cs-binding$="' + attName + '"]').hide();
		} else if (visibility == 'R') {
			//make input elements read only
			$j('input[data-cs-binding$=":' + attName + '"]').prop('readonly', 'true');
			//make select elements read only
			$j('select[data-cs-binding$=":' + attName + '"]').prop('readonly', 'true');
		}
	}
};

/**
 *
 *Final validation -->
 * Valid access infrastructure needs to have
 * Price Item Access (if no existing infrastructure)
 * CPE
 * EVC
 * CPE SLA
 *
 * Update 4/2019 -> if EVC and CPE are removed intentionaly validation not needed
 */
window.accessFinalValidation = function accessFinalValidation() {
	var numberOfAccessItems = 0;
	var numberOfCPEItems = 0;
	var numberOfEVCItems = 0;
	var numberOfCPESLAItems = 0;

	//only for Modular pricing
	var modular = CS.getAttributeValue('Modular_pricing_0');

	//only if CPE and EVC are not deleted
	var deletedEVCandCPE = CS.getAttributeValue('RemoveEVCCPE_0');

	if (modular && !deletedEVCandCPE) {
		numberOfAccessItems = CS.Service.config['Access_Infrastructure_0'].relatedProducts.length;
		numberOfCPEItems = CS.Service.config['CPE_0'].relatedProducts.length;
		numberOfEVCItems = CS.Service.config['EVC_PVC_dep_0'].relatedProducts.length;
		numberOfCPESLAItems = CS.Service.config['CPE_SLA_0'].relatedProducts.length;

		var existingInfra = CS.getAttributeValue('Existing_Infra_0');
		var markInvalid = false;
		if (existingInfra) {
			if (numberOfCPEItems == 0 && numberOfEVCItems == 0 && numberOfCPESLAItems == 0) {
				markInvalid = true;
			}
		} else {
			if (numberOfAccessItems == 0 && numberOfCPEItems == 0 && numberOfEVCItems == 0 && numberOfCPESLAItems == 0) {
				markInvalid = true;
			}
		}

		if (markInvalid) {
			CS.markConfigurationInvalid('Please complete your access configuration.');
		}
	}
};

function checkSbcSipSessionPoolCpe() {
	// if OneFixed Configuration changed and technology is not 'IP' then remove CPE with Product group containing "SBC SIP SESSION LICENSE (POOL)"
	var technologyType = CS.getAttributeValue('Technology_type_0');
	if (technologyType != 'IP') {
		for (var i = 0; i < CS.Service.config['CPE_0'].relatedProducts.length; i++) {
			var productGroup = CS.getAttributeValue('CPE_' + i + ':Product_group_0');
			if (productGroup != undefined && productGroup.includes(';SBC SIP SESSION LICENSE (POOL)')) {
				CS.Service.removeRelatedProduct('CPE_' + i);
				if (CS.getAttributeValue('OneFixed_SBC_SIP_Sessions_set_0') != '') CS.setAttributeValue('OneFixed_SBC_SIP_Sessions_set_0', '');
				return;
			}
		}
	}

	// update Quantity if needed
	for (var i = 0; i < CS.Service.config['CPE_0'].relatedProducts.length; i++) {
		var productGroup = CS.getAttributeValue('CPE_' + i + ':Product_group_0');
		if (productGroup != undefined && productGroup.includes(';SBC SIP SESSION LICENSE (POOL)')) {
			CS.setAttributeValue('CPE_' + i + ':Quantity_0', CS.getAttributeValue('SIP_Channels_0'));
			return;
		}
	}
}
