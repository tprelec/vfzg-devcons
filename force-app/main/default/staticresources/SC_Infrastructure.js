if (!CS || !CS.SM) {
	throw Error('Solution Console Api not loaded!');
}

// Register Infrastructure Solution plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', registerInfrastructure);
}
console.log('Infrastructure...');
async function registerInfrastructure() {
	const InfrastructurePlugin = await CS.SM.registerPlugin(SC_SCHEMA_NAME.INFRASTRUCTURE_SOLUTION);
	updateInfrastructurePlugin(InfrastructurePlugin);
	return;
}

function updateInfrastructurePlugin(Plugin) {
	Plugin.beforeSave = beforeSolutionSave;
	Plugin.afterSolutionLoaded = afterSolutionLoaded;
	Plugin.beforeSolutionDelete = beforeSolutionDelete;
	Plugin.afterSolutionDelete = afterSolutionDelete;
	Plugin.afterSave = afterInfrastructureSolutionSave;
	Plugin.afterAttributeUpdated = afterInfrastructureAttributeUpdated;
	Plugin.beforeAddonDataInitialized = beforeAddOnDataInitialized;
	Plugin.afterConfigurationAdd = afterInfrastructureConfigurationAdd;
	Plugin.afterConfigurationDelete = afterInfrastructureConfigurationDelete;
}

async function afterSolutionLoaded(solutionBeforeUpdate, solutionAfterUpdate) {
	await setSdwanAddressList();
}

async function afterInfrastructureConfigurationAdd(component, configuration) {
	switch (component.name) {
		case SC_COMPONENTS.ACCESS_COMPONENT:
			return afterAccessConfigurationAdded(component, configuration);
		case SC_COMPONENTS.SITECONNECT_COMPONENT:
			return afterSiteConnectConfigurationAdded(component, configuration);
		case SC_COMPONENTS.SDWAN_COMPONENT:
			return afterSdwanConfigurationAdded(component, configuration);
		default:
	}
	return true;
}

async function afterInfrastructureConfigurationDelete(component, configuration) {
	switch (component.name) {
		case SC_COMPONENTS.ACCESS_COMPONENT:
			return afterAccessConfigurationDeleted(component, configuration);
		case SC_COMPONENTS.SITECONNECT_COMPONENT:
			return afterSiteConnectConfigurationDeleted(component, configuration);
		case SC_COMPONENTS.SDWAN_COMPONENT:
			return afterSdwanConfigurationDeleted(component, configuration);
		default:
	}
	return true;
}

async function beforeSolutionDelete(solution) {
	const configurations = await SC_Util.generateFlatConfigurationListAsync(solution);
	let siteConnectConfigurations = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.SITECONNECT_COMPONENT
	);
	window.deletedSiteComponentPC = new Array();
	if (siteConnectConfigurations != undefined) {
		siteConnectConfigurations.forEach((config) => {
			if (config.value.id != '' && config.value.id != undefined) {
				window.deletedSiteComponentPC.push(config.value.id);
			}
		});
	}
	return true;
}

async function afterSolutionDelete(solution) {
	if (solution.name == 'Infrastructure') {
		const basket = await CS.SM.getActiveBasket();
		let lastSol = null;
		const solutions = basket.getSolutions();
		let businessInternetSolutions = Object.values(solutions).filter(
			(item) => item.name == 'Business Internet'
		);
		if (businessInternetSolutions != undefined) {
			for (let i = 0; i < businessInternetSolutions.length; i++) {
				let businessInternetSolution = businessInternetSolutions[i];
				let configurations = await SC_Util.generateFlatConfigurationListAsync(
					businessInternetSolution
				);
				let internetConfiguration = Object.values(configurations).find(
					(item) => item.name == 'Internet'
				);

				if (
					window.deletedSiteComponentPC &&
					window.deletedSiteComponentPC.length != 0 &&
					internetConfiguration &&
					window.deletedSiteComponentPC.includes(
						internetConfiguration.value.attributes['site connect'].value
					)
				) {
					await SC_Helper.updateAttributeAsync(
						businessInternetSolution,
						internetConfiguration.key,
						'site connect',
						''
					);
					lastSol = businessInternetSolution;
				}
			}

			if (lastSol != null) {
				await CS.SM.setActiveSolution(lastSol.Id, false);
				await basket.submitSolution(lastSol);
			} else if (businessInternetSolutions[0]) {
				await CS.SM.setActiveSolution(businessInternetSolutions[0].Id, false);
				await basket.submitSolution(businessInternetSolutions[0]);
			}
			await refreshSiteConnectLookupAttribute();
		}
	}
}

/**
 * @description Perform logic required after Solution is saved
 */
async function afterInfrastructureSolutionSave(
	solutionResult,
	configurationsProcessed,
	saveOnlyAttachment,
	configurationGuids
) {
	await setSdwanAddressList();
	const solution = solutionResult.solution;
	if (!solution) {
		return;
	}
	return Promise.all([applyAutomaticDiscount()]);
}

async function afterInfrastructureAttributeUpdated(
	component,
	configuration,
	attribute,
	oldValueMap
) {
	switch (component.name) {
		case SC_COMPONENTS.INFRASTRUCTURE_SOLUTION:
			return afterInfrastructureSolutionAttributeUpdated(attribute);
		case SC_COMPONENTS.ACCESS_COMPONENT:
			return afterAccessAttributeUpdated(component, configuration, attribute, oldValueMap);
		case SC_COMPONENTS.SITECONNECT_COMPONENT:
			return afterSiteConnectAttributeUpdated(
				component,
				configuration,
				attribute,
				oldValueMap
			);
		case SC_COMPONENTS.SDWAN_COMPONENT:
			return afterSdwanAttributeUpdated(component, configuration, attribute, oldValueMap);
		default:
	}
	return true;
}

async function afterInfrastructureSolutionAttributeUpdated(attribute) {
	if (attribute.name === SC_COMMON_ATTRIBUTES.TODAY) {
		return Promise.all([
			propagateAttributeToComponent(
				SC_COMPONENTS.ACCESS_COMPONENT,
				attribute.name,
				attribute.value
			)
		]);
	}

	if (attribute.name === SC_COMMON_ATTRIBUTES.CONTRACT_TERM_BI) {
		return Promise.all([
			propagateAttributeToComponent(
				SC_COMPONENTS.ACCESS_COMPONENT,
				SC_COMMON_ATTRIBUTES.CONTRACT_DURATION,
				attribute.value
			),
			propagateAttributeToComponent(
				SC_COMPONENTS.SITECONNECT_COMPONENT,
				SC_COMMON_ATTRIBUTES.CONTRACT_DURATION,
				attribute.value
			)
		]);
	}
}

/**
 * @description Initialization logic
 */
async function initInfrastructureSolutionDefaults(solution) {
	if (!solution) {
		solution = await CS.SM.getActiveSolution();
	}

	const InfrastructureSolutionComponent = solution.getComponentByName(
		SC_SOLUTIONS.INFRASTRUCTURE_SOLUTION
	);

	if (!InfrastructureSolutionComponent) {
		return;
	}

	return Promise.all([
		setCurrentSystemDate(solution),
		updateFromBasket(solution, [SC_COMMON_ATTRIBUTES.ACCOUNT_ID]),
		setSdwanAddressList()
	]);
}
