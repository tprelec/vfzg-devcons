console.log('Loaded Constant file');

const SC_SOLUTIONS = Object.freeze({
	ONEMOBILE_SOLUTION: 'Mobile',
	BUSINESSINTERNET_SOLUTION: 'Business Internet',
	INFRASTRUCTURE_SOLUTION: 'Infrastructure',
	MANAGEDSWITCH_SOLUTION: 'Business Managed Switch',
	BUSINESSINTERNETMODEMONLY_SOLUTION: 'Business Internet Modem Only',
	BUSINESS_MOBILE_SOLUTION: 'Business Mobile'
});

const SC_SCHEMA_NAME = Object.freeze({
	ONEMOBILE_SOLUTION: 'Mobile Solution',
	BUSINESSINTERNET_SOLUTION: 'Business Internet',
	INFRASTRUCTURE_SOLUTION: 'Infrastructure',
	MANAGEDSWITCH_SOLUTION: 'Business Managed Switch',
	BUSINESSINTERNETMODEMONLY_SOLUTION: 'Business Internet Modem Only',
	BUSINESS_MOBILE_SOLUTION: 'Business Mobile Solution'
});

const SC_COMPONENTS = Object.freeze({
	ONEMOBILE_COMPONENT: 'Mobile',
	ONEMOBILE_PLAN_COMPONENT: 'Mobile Plan',
	ONEMOBILE_PLAN_OE_COMPONENT: 'Mobile Plan OE',
	BUSINESSINTERNET_COMPONENT: 'Business Internet',
	ACCESS_COMPONENT: 'Access',
	SITECONNECT_COMPONENT: 'Site Connect',
	INFRASTRUCTURE_SOLUTION: 'Infrastructure',
	INFRASTRUCTURE_OE: 'Infrastructure OE',
	INTERNET_SOLUTION: 'Internet',
	SDWAN_COMPONENT: 'SD-WAN',
	MANAGEDSWITCH_SOLUTION: 'Business Managed Switch',
	NETWORKPORTS_COMPONENT: 'Network Ports',
	BUSINESSINTERNETMODEMONLY_COMPONENT: 'Business Internet Modem Only',
	ACCESSMODEMONLY_COMPONENT: 'Access Modem Only',
	INTERNETMODEMONLY_COMPONENT: 'Internet Modem Only',
	OVERVIEW_COMPONENT: 'Overview',
	BUSINESS_MOBILE_COMPONENT: 'Business Mobile',
	MOBILE_PLAN_COMPONENT: 'Mobile Plan',
	BUSINESS_MOBILE_PLAN_OE_COMPONENT: 'Business Mobile Plan OE',
	BUSINESS_MOBILE_PLAN_OE_ADDON_COMPONENT: 'Business Mobile Plan Addon OE',
	MOBILE_DEVICE_COMPONENT: 'Mobile Device',
	MOBILE_ZAKELIJKE_COMPONENT: 'Mobile Zakelijke Toestelbetaling'
});

const SC_COMMON_ATTRIBUTES = Object.freeze({
	TODAY: 'Today',
	TECHNOLOGY: 'Technology',
	CONTRACT_TERM: 'ContractTerm',
	MOBILE_PLAN_NAME: 'MobilePlanName',
	TIER: 'tier',
	PRODUCT_VARIANT: 'productVariant',
	PRODUCT_VARIANT_NAME: 'productVariantName',
	PRODUCT_VARIANT_BAND_UP: 'productVariantBanUp',
	PRODUCT_VARIANT_BAND_DOWN: 'productVariantBanDown',
	SALES_CHANNEL: 'SalesChannel',
	QUANTITY: 'Quantity',
	GUID: 'guid',
	CONNECTION_TYPE: 'ConnectionType',
	CONFIG_ID: 'ConfigId',
	BASE_MRC: 'BaseMRC',
	ACCOUNT_ID: 'AccountID',
	CONTRACT_DURATION: 'contractDuration',
	CONTRACT_TERM_BI: 'contractTerm',
	INTERNET_NAME: 'InternetName',
	BASKET_ID: 'BasketId',
	SITE_AVAILABILITY: 'SiteAvailability',
	SITE: 'Site',
	ADDRESS: 'Address',
	ACCESS: 'Access',
	SITE_AVAILABILITY_ID: 'SiteAvailability Id',
	SITE_ID: 'SiteId',
	ACCESS_GUID: 'accessGuid',
	INTERNET_PRODUCT: 'InternetProduct',
	ACCESS_NAME: 'AccessName',
	SITE_NAME: 'siteName',
	DATA_ALLOWANCE: 'Data allowance',
	SOFT_DELETE: 'Soft Delete'
});

const SC_ADDON_GROUPS = Object.freeze({
	DATA_AND_VOICE_UPGRADES: 'Data and voice upgrades',
	ROAMING_UPGRADES: 'Roaming upgrades'
});

const SC_ADDON_NAMES = Object.freeze({
	DYNAMIC_IP: 'Dynamic IP'
});

const SC_RELATED_PRODUCT_TYPES = Object.freeze({
	ADDON: 'Add On Component',
	RELATED_PRODUCT: 'Related Component'
});

const SC_WARNING_MESSAGES = Object.freeze({
	CANNOT_ADD_ROAMING: "Can't add 'World permanent upgrade' Add-On without adding Add-On from the group 'Data and voice upgrades'",
	CANNOT_DELETE_LAST_VOICE_AND_DATA:
		"At least 1 Add-On from the group 'Data and voice upgrades' needs to be added in order to have Add-On 'World permanent upgrade'",
	QUANTITY_NOT_MATCH: "The Quantity on the configuration does not match the total Quantity of it's Order Enrichment Quantities",
	DUPLICATE_CONNECTION_TYPE: 'Duplicate Connection Type selected!',
	CANNOT_DELETE_INTERNET_MODEM_ONLY: 'Cannot delete Internet configuration when access configuration is available',
	ADD_CONFIGURATION_TO_INFLIGHT_BASKET: 'Please add following Internet configuration to the Inflight basket first: ',
	HOOK_NOT_SPECIFIED: 'Behavior not specified for '
});

const SC_ERROR_MESSAGES = Object.freeze({
	SITE_CONNECT_INVALID_BAN_UP: 'Site connect configuration is invalid due to insufficient upload bandwidth',
	SITE_CONNECT_INVALID_BAN_DOWN: 'Site connect configuration is invalid due to insufficient download bandwidth',
	SITE_CONNECT_MANDATORY: 'Site Connect is mandatory for each Access Configuration',
	SITE_BI_ONLY_ONE: 'Only 1 Business Internet allowed on a location',
	SITE_ACCESS_ONLY_ONE: 'Only 1 Access allowed on a location',
	ACCESS_SDWAN_MISSING: 'SDWAN Box missing for access site',
	SDWAN_QUANTITY_INSUFFICIENT: 'SDWAN Box quantity is insufficient',
	SDWAN_QUANTITY_TO_LARGE: 'SDWAN Box quantity to big - max 2 boxes per access location'
});

const SC_REMOTE_CLASS_NAME = Object.freeze({
	REMOTE_APEX_CLASS: 'RemoteActionProvider',
	SERVICE_INFO_REMOTE: 'CS_ServiceInfoProvider'
});

const SC_REMOTE_CLASS_METHOD = Object.freeze({
	PRODUCT_BASKET_DETAILS: 'productBasketDetails',
	APPLY_AUTOMATIC_DISCOUNT: 'applyAutomaticDiscount',
	APPLY_AUTOMATIC_DISCOUNT_ADDON: 'applyAutomaticDiscountAddOn',
	GET_SDWANS: 'getSdwans',
	DEFAULT_MAX_BAND_ACCESS: 'defaultHighestBandwidthAccess',
	DEFAULT_MAX_BAND_SITECON: 'defaultHighestBandwidthSiteConnect',
	GET_COMMERCIALPRODUCT_DETAILS: 'getCommercialProductDetails',
	INCREMENT_MACD_CONTRACT_NUMBER: 'incrementMacdContractNumber',
	CALCULATE_LIST_PRICE: 'calculateListPriceJS',
	CHECK_UNIQUE_SOLUTION_CONSTRAINT: 'checkUniqueSolutionConstraint'
});

const SC_ACTION = Object.freeze({
	DELETE: 'Delete',
	ADD: 'Add'
});

const SC_BASKET_FIELD_VALUE = Object.freeze({
	BASKET_CHANGE_TYPE_CHANGE_REQUEST: 'Change Request',
	BASKET_INFLIGHT_CHANGE_TYPE_SELECTIVE: 'Selective'
});

const SC_BUSSINESS_MOBILE_ATTRIBUTES = Object.freeze({
	TODAY: 'Today',
	PRODUCT_VARIANT: 'productVariant',
	PRODUCT_VARIANT_NAME: 'productVariantName',
	PRODUCT_VARIANT_CATEGORY: 'productVariantCategory',
	PRODUCT_VARIANT_TIER: 'productVariantTier',
	PRODUCT_VARIANT_SUBSCRIPTION: 'productVariantSubscriptionType',
	PRODUCT_VARIANT_CODE: 'productVariantCode',
	TOESTELBETALING_VARIANT: 'toestelbetaling',
	TOESTELBETALING_NAME: 'toestelbetalingName',
	TOESTELBETALING_CONFIGURATION_NAME: 'toestelbetalingProductName',
	MOBILE_DEVICE_CONFIGURATION_NAME: 'DeviceName',
	MOBILE_PLAN_CONFIGURATION_NAME: 'MobilePlanName',
	CONTRACT_TERM_D: 'ContractTermDevice',
	SCENARIO_MAIN: 'Scenario',
	CONTRACT_TERM: 'ContractTerm',
	CONNECTION_TYPE: 'connectionType',
	DATA_ALLOWANCE: 'Data allowance',
	DATA_CAP: 'dataCap',
	QUANTITY: 'Quantity',
	BRAND: 'Brand',
	STORAGE: 'Storage',
	COLOR_D: 'DeviceColor',
	SIM_TYPE: 'SIMtype',
	NETWORK_TYPE: 'NetworkType'
});

const SC_BUSSINESS_MOBILE_VALUES = Object.freeze({
	BM_REGULAR: 'Business Mobile Regular',
	BM_DATA_ONLY: 'Business Mobile data only',
	DIRECT_SALES_CHANNEL: 'Direct',
	VOICE_AND_DATA: 'Voice & Data'
});
