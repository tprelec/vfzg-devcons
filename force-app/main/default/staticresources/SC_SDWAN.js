// SC_SDWAN
console.log('Loaded SDWAN plugin');

async function resolveSdwan() {
	const configurations = await SC_Util.generateFlatConfigurationListAsync();
	const accessConfigs = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.ACCESS_COMPONENT
	);

	if (!accessConfigs || accessConfigs.length === 0) {
		return;
	}

	const existingSdwans = await getExistingSdwans(configurations);
	const activeSolution = await CS.SM.getActiveSolution();
	const accessSites = getAccessesPerSite(accessConfigs);

	// there is always 1 SDWAN commercial product so we can fetch it upfront
	const sdwanProductVariantResult = await selectSdwanCommercialProduct();
	Object.keys(accessSites).forEach(async (key) => {
		if (existingSdwans != undefined && existingSdwans.hasOwnProperty(key)) {
			console.log('Configuration for address ' + key + ' already exists');
			// there is a max of 2 Access configs per SDWAN box
			let sdwanQuantity = parseInt(Math.floor((accessSites[key].Quantity + 1) / 2));
			const sdwanComponent = activeSolution.getComponentByName(SC_COMPONENTS.SDWAN_COMPONENT);
			const attributeData = [
				{
					name: SC_COMMON_ATTRIBUTES.QUANTITY,
					value: sdwanQuantity
				}
			];
			sdwanComponent.updateConfigurationAttribute(existingSdwans[key].guid, attributeData);
		} else {
			if (key !== undefined && key != '') {
				// Create new SDWAN configuration
				let attributes = [];
				attributes.push(
					SC_Util.generateAttribute(
						SC_COMMON_ATTRIBUTES.ADDRESS,
						accessSites[key].Id,
						true
					)
				);
				attributes.push(
					SC_Util.generateAttribute(
						SC_COMMON_ATTRIBUTES.QUANTITY,
						accessSites[key].Quantity,
						true
					)
				);
				if (sdwanProductVariantResult.status) {
					attributes.push(
						SC_Util.generateLookupAttribute(
							'productVariant',
							sdwanProductVariantResult.productVariant,
							sdwanProductVariantResult.productVariantName,
							true
						)
					);
				}

				console.log('Attributes: ' + JSON.stringify(attributes));
				const sdwanComponent = activeSolution.getComponentByName(
					SC_COMPONENTS.SDWAN_COMPONENT
				);
				const sdwanConfiguration = sdwanComponent.createConfiguration(attributes);
				await sdwanComponent.addConfiguration(sdwanConfiguration);
			}
		}
	});
	await setSdwanAddressList();
	await sdwanFinalValidation();
}

async function afterSdwanConfigurationAdded(component, configuration) {
	await setSdwanAddressList();
	await sdwanFinalValidation();
}

async function afterSdwanConfigurationDeleted(component, configuration) {
	await sdwanFinalValidation();
}

async function afterSdwanAttributeUpdated(component, configuration, attribute, oldValueMap) {
	if (
		attribute.name === SC_COMMON_ATTRIBUTES.QUANTITY ||
		attribute.name === SC_COMMON_ATTRIBUTES.ADDRESS
	) {
		await sdwanFinalValidation();
	}
}

async function checkSdwanForDeletion(configuration) {
	let guid = SC_Util.getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.GUID);
	let addressAccess = SC_Util.getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.ADDRESS);
	let accessSiteId = SC_Util.getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.SITE_ID);
	console.log('DELETED Access with ID: ' + guid + ' address: ' + addressAccess);

	const activeSolution = await CS.SM.getActiveSolution();
	const configurations = await SC_Util.generateFlatConfigurationListAsync();
	const sdwans = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.SDWAN_COMPONENT
	);
	const accessConfigs = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.ACCESS_COMPONENT
	);
	const sdwanComponent = activeSolution.getComponentByName(SC_COMPONENTS.SDWAN_COMPONENT);

	const accessPerSite = getAccessesPerSite(accessConfigs);
	if (accessPerSite != undefined && accessPerSite.hasOwnProperty(accessSiteId)) {
		// update quantity
		let newQuantity = parseInt(Math.floor((accessPerSite[accessSiteId].Quantity + 1) / 2));

		sdwans.forEach((config) => {
			const unpacked = SC_Util.unpackFlattConfiguration(config);
			let addressSdwan = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.ADDRESS);
			if (addressSdwan === accessSiteId) {
				return SC_Helper.updateAttributeAsync(
					sdwanComponent,
					unpacked.guid,
					SC_COMMON_ATTRIBUTES.QUANTITY,
					newQuantity
				);
			}
		});
	} else {
		let guids = [];
		sdwans.forEach((config) => {
			const unpacked = SC_Util.unpackFlattConfiguration(config);
			let addressSdwan = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.ADDRESS);
			if (addressSdwan === accessSiteId) {
				guids.push(SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.GUID));
			}
		});

		guids.forEach((item) => {
			sdwanComponent.deleteConfiguration(item);
			console.log('Deleting SDWAN: ' + item);
		});
	}
}

async function getExistingSdwans(configurations) {
	let sdwanConfigs = {};
	const sdwans = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.SDWAN_COMPONENT
	);
	//console.log('All SDWAN configs: ' + JSON.stringify(sdwans));
	sdwans.forEach((config) => {
		//console.log('Existing SDWAN config: ' + JSON.stringify(config));
		let configInfo = {};
		const unpacked = SC_Util.unpackFlattConfiguration(config);
		configInfo.guid = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.GUID);
		configInfo.address = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.ADDRESS);
		configInfo.quantity = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.QUANTITY);
		sdwanConfigs[configInfo.address] = configInfo;
	});
	console.log('Collected existing SDWANs: ' + JSON.stringify(sdwanConfigs));

	return sdwanConfigs;
}

/**
 * @description collect addresses for SDWAN address dropdown;
 *              collect map(address, site) to be able to set proper site when address is selected manually
 */
function getAccessAddresses(accessConfigurations) {
	let addressList = [];
	let siteAddressMap = {};
	accessConfigurations.forEach((item) => {
		const unpacked = SC_Util.unpackFlattConfiguration(item);
		let address = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.ADDRESS);
		let site = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.SITE_ID);
		if (!siteAddressMap.hasOwnProperty(address)) {
			addressList.push({ value: site, label: address });
			siteAddressMap[site] = {};
			siteAddressMap[site].sites = new Set();
			siteAddressMap[site].sites.add(site);
		} else {
			siteAddressMap[site].sites.add(site);
		}
	});
	return [addressList, siteAddressMap];
}

async function setSdwanAddressList() {
	const configurations = await SC_Util.generateFlatConfigurationListAsync();
	const accessConfigs = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.ACCESS_COMPONENT
	);
	let addressOptionsAndMappings = getAccessAddresses(accessConfigs);
	const activeSolution = await CS.SM.getActiveSolution();
	const sdwanComponent = activeSolution.getComponentByName(SC_COMPONENTS.SDWAN_COMPONENT);
	const sdwans = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.SDWAN_COMPONENT
	);

	if (sdwans != undefined) {
		sdwans.forEach((config) => {
			const unpacked = SC_Util.unpackFlattConfiguration(config);
			let guid = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.GUID);
			console.log('Set SDWAN addresses');
			updateAttributeSelectOptions(
				sdwanComponent,
				guid,
				SC_COMMON_ATTRIBUTES.ADDRESS,
				addressOptionsAndMappings[0]
			);
		});
	}
}

async function updateAttributeSelectOptions(
	solution,
	configurationGuid,
	attributeName,
	attributePicklistOptions
) {
	let att = { name: attributeName, options: attributePicklistOptions };
	return solution.updateConfigurationAttribute(configurationGuid, [att]);
}

/**
 * @description get Id and Name of SDWAN commercial product for automatic population of productVariant attribute
 */
async function selectSdwanCommercialProduct() {
	const basket = await CS.SM.getActiveBasket();
	const inputMapObject = {
		method: SC_REMOTE_CLASS_METHOD.GET_SDWANS
	};

	const result = await basket.performRemoteAction(
		SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS,
		inputMapObject
	);

	if (result.status) {
		console.log('Successful remote action GetSdwans: ', result.message);
	} else {
		console.error('GetSdwans failed: ' + result.message);
	}
	return result;
}

/**
 * @description SDWAN validation
 */
async function sdwanFinalValidation() {
	let hasError = false;
	let allConfigs = await SC_Util.generateFlatConfigurationListAsync();
	const accessConfigs = await SC_Util.getAllConfigurationsWithNameAsync(
		allConfigs,
		SC_COMPONENTS.ACCESS_COMPONENT
	);
	const sdwanConfigs = await SC_Util.getAllConfigurationsWithNameAsync(
		allConfigs,
		SC_COMPONENTS.SDWAN_COMPONENT
	);
	const existingSdwans = await getExistingSdwans(allConfigs);
	const accessPerSite = getAccessesPerSite(accessConfigs);

	// accessSites are grouped per siteAvailabilityId --> we need to group it per Site Address
	for (const [accessSiteId, value] of Object.entries(accessPerSite)) {
		// - there should be at least one box per location
		if (existingSdwans != undefined && !existingSdwans.hasOwnProperty(accessSiteId)) {
			hasError = true;
			// there is no SDWAN for this access site --> mark configuration invalid
			CS.SM.displayMessage(SC_ERROR_MESSAGES.ACCESS_SDWAN_MISSING, 'error');
		} else {
			let sdwanQuantity = 0;
			let sdwanGuid;
			// iterate all SDWAN configs to get total SDWAN quantity
			sdwanConfigs.forEach((config) => {
				const unpacked = SC_Util.unpackFlattConfiguration(config);
				let sdwanAddress = SC_Util.getAttributeValue(
					unpacked,
					SC_COMMON_ATTRIBUTES.ADDRESS
				);
				if (sdwanAddress == accessSiteId) {
					sdwanQuantity += parseInt(
						SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.QUANTITY)
					);
					sdwanGuid = unpacked.guid; // theoreticaly there can be more SDWAN configs for specific Access Sites -> this will remember the last one
				}
			});

			// - there must be box for each access
			if (sdwanQuantity < Math.floor((value.Quantity + 1) / 2)) {
				hasError = true;
				CS.SM.displayMessage(SC_ERROR_MESSAGES.SDWAN_QUANTITY_INSUFFICIENT, 'error');
				await validateConfiguration(
					allConfigs[sdwanGuid].value,
					false,
					SC_ERROR_MESSAGES.SDWAN_QUANTITY_INSUFFICIENT
				);
			} else if (sdwanQuantity > value.Quantity * 2) {
				hasError = true;
				CS.SM.displayMessage(SC_ERROR_MESSAGES.SDWAN_QUANTITY_TO_LARGE, 'error');
				await validateConfiguration(
					allConfigs[sdwanGuid].value,
					false,
					SC_ERROR_MESSAGES.SDWAN_QUANTITY_TO_LARGE
				);
			} else {
				await validateConfiguration(allConfigs[sdwanGuid].value, true, '');
			}
		}
	}
	return hasError;
}
