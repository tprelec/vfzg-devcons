// SC_Access
/**
 * Plugin for Access
 * NOTE: Plugin must be invoked after SC_BusinessInternet_Solution.js
 */
console.log('Loaded Access plugin');
async function afterAccessConfigurationAdded(component, configuration) {
	if (configuration.attributes.contractduration.value == '') {
		await setAttribute(
			component,
			configuration,
			SC_COMMON_ATTRIBUTES.CONTRACT_DURATION,
			configuration.attributes.contractterm.value
		);
	}
	return true;
}

async function afterAccessConfigurationDeleted(component, configuration) {
	await checkSdwanForDeletion(configuration);
	await checkForSiteConnectDeletion(configuration);
}

async function afterAccessAttributeUpdated(component, configuration, attribute, oldValueMap) {
	if (attribute.name === SC_COMMON_ATTRIBUTES.SITE_AVAILABILITY) {
		await resolveSdwan();
	} else if (attribute.name === SC_COMMON_ATTRIBUTES.CONTRACT_DURATION) {
		await setAttribute(component, configuration, SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT, '');
	} else if (attribute.name === SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT) {
		await resolveSiteConnect();
		await clearAllRelatedSiteConnectBandwidthLookup(component, configuration);
	}
}

async function getDefaultMaxBandAccess(technology, contractDuration, siteVendor, bandwidthValues) {
	const basket = await CS.SM.getActiveBasket();
	const inputMapObject = {
		method: SC_REMOTE_CLASS_METHOD.DEFAULT_MAX_BAND_ACCESS,
		technology: technology,
		contractDuration: contractDuration,
		siteVendor: siteVendor,
		bandwidthDown: bandwidthValues[0],
		bandwidthUp: bandwidthValues[1]
	};

	const result = await basket.performRemoteAction(
		SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS,
		inputMapObject
	);

	return result;
}

async function clearAllRelatedSiteConnectBandwidthLookup(component, configuration) {
	let configurationsAll = await SC_Util.generateFlatConfigurationListAsync();
	console.log(component);
	console.log(configuration.guid);
	console.log(component.solutionId);
	let currentSolution = await CS.SM.getActiveSolution();
	for (let i in configurationsAll) {
		if (
			configurationsAll[i].name == SC_COMPONENTS.SITECONNECT_COMPONENT &&
			configurationsAll[i].value.solutionId == component.solutionId
		) {
			console.log(
				'Checking for access ' + configurationsAll[i].value.attributes.access.value
			);
			if (configurationsAll[i].value.attributes.access.value == configuration.guid) {
				await SC_Helper.updateAttributeAsync(
					currentSolution,
					configurationsAll[i].key,
					SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT,
					''
				);
			}
		}
	}
}

async function resolveInfrastructureOrderEnrichment() {
	const configurations = await SC_Util.generateFlatConfigurationListAsync();
	const accesses = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.ACCESS_COMPONENT
	);
	if (!accesses || accesses.length === 0) {
		return;
	}

	const rootGUID = accesses[0].mainGuid;
	const activeSolution = await CS.SM.getActiveSolution();
	const infrastructureOeType = activeSolution.getComponentByName(SC_COMPONENTS.INFRASTRUCTURE_OE);
	await removeInfrastructureOeConfigurations(rootGUID, activeSolution, configurations);

	const addresses = generateAddressInformation(accesses);
	Object.keys(addresses).forEach(async (key) => {
		const address = addresses[key];
		const orderEnrichmentConfiguration = infrastructureOeType.createConfiguration(
			getInfrastractureOETemplate(address)
		);
		await activeSolution.addOrderEnrichmentConfiguration(
			rootGUID,
			orderEnrichmentConfiguration
		);
	});
}

async function removeInfrastructureOeConfigurations(rootGUID, activeSolution, configurations) {
	Object.keys(configurations).forEach(async (key) => {
		const config = configurations[key];
		if (config.name.indexOf(SC_COMPONENTS.INFRASTRUCTURE_OE) !== -1) {
			await activeSolution.deleteOrderEnrichmentConfiguration(rootGUID, config.key, true);
		}
	});
}

function generateAddressInformation(accesses) {
	let data = {};
	for (let i = 0; i < accesses.length; i++) {
		const unpacked = SC_Util.unpackFlattConfiguration(accesses[i]);
		const site = SC_Util.getAttributeByName(unpacked, SC_COMMON_ATTRIBUTES.SITE_AVAILABILITY);
		const address = SC_Util.getAttributeByName(unpacked, SC_COMMON_ATTRIBUTES.ADDRESS);

		if (data.hasOwnProperty(site.value)) {
			data[site.value].Quantity = data[site.value].Quantity + 1;
		} else {
			data[site.value] = {
				Id: site.value,
				Name: address.value,
				Quantity: 1,
				AccessConfigId: unpacked.guid
			};
		}
	}
	return data;
}

function getAccessesPerSite(accesses) {
	let data = {};
	for (let i = 0; i < accesses.length; i++) {
		const unpacked = SC_Util.unpackFlattConfiguration(accesses[i]);
		const site = SC_Util.getAttributeByName(unpacked, SC_COMMON_ATTRIBUTES.SITE_ID);
		const address = SC_Util.getAttributeByName(unpacked, SC_COMMON_ATTRIBUTES.ADDRESS);
		const banUp = SC_Util.getAttributeByName(
			unpacked,
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_UP
		);
		const banDown = SC_Util.getAttributeByName(
			unpacked,
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_DOWN
		);
		const ownInfra = SC_Util.getAttributeByName(unpacked, 'owninfrastructuretoggle');

		if (data.hasOwnProperty(site.value)) {
			data[site.value].Quantity = data[site.value].Quantity + 1;
		} else {
			data[site.value] = {
				Id: site.value,
				Address: address.value,
				Quantity: 1,
				AccessConfigId: unpacked.guid,
				BandwidthUp: banUp,
				BandwidthDown: banDown,
				OwnInfra: ownInfra
			};
		}
	}
	return data;
}

function getAccessAttributes(accesses) {
	let data = {};
	for (let i = 0; i < accesses.length; i++) {
		const unpacked = SC_Util.unpackFlattConfiguration(accesses[i]);
		const site = SC_Util.getAttributeByName(unpacked, SC_COMMON_ATTRIBUTES.SITE_AVAILABILITY);
		const address = SC_Util.getAttributeByName(unpacked, SC_COMMON_ATTRIBUTES.ADDRESS);
		const banUp = SC_Util.getAttributeByName(
			unpacked,
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_UP
		);
		const banDown = SC_Util.getAttributeByName(
			unpacked,
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_DOWN
		);
		const ownInfra = SC_Util.getAttributeByName(unpacked, 'owninfrastructuretoggle');
		const contractTerm = SC_Util.getAttributeByName(
			unpacked,
			SC_COMMON_ATTRIBUTES.CONTRACT_TERM
		);

		data[unpacked.guid] = {
			Id: site.value,
			Address: address.value,
			Quantity: 1,
			AccessConfigId: unpacked.guid,
			BandwidthUp: banUp.value,
			BandwidthDown: banDown.value,
			OwnInfra: ownInfra.value,
			ContractTerm: contractTerm.value
		};
	}
	return data;
}

function getInfrastractureOETemplate(address) {
	let configurationTemplate = [];
	configurationTemplate.push(SC_Util.generateAttribute('AddressId', address.Id, false));
	configurationTemplate.push(SC_Util.generateAttribute('Address', address.Name, true));
	configurationTemplate.push(SC_Util.generateAttribute('Quantity', address.Quantity, true));

	return configurationTemplate;
}
