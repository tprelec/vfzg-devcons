console.log("Loaded Gen Access plugin");

// Subscribers
SC_MessageBroker.store.subscribe('root:ContractTerm', async (data) => {
    console.log('root:ContractTerms...');
	await propagateAttributeToComponent("Gen Access", data.attribute.name, data.attribute.value);
});

const scgetaccess = Object.freeze({

	"afterGenFirstAttributeUpdated": async function afterGenFirstAttributeUpdated(component, configuration, attribute, oldValueMap) {
		
		if (component.name === 'Gen Access' && attribute.name === 'technology' && attribute.value !== oldValueMap.value) {
			await setAttribute(component, configuration, 'quantity', 100);
		}
	
		return true;
	}
});
