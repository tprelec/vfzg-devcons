console.log('Loaded Mobile Plan plugin'); //SC_MobilePlan.js - part of Business Mobile Solution

/**
 * @description Setting the default value to the Contract term field.
 *              A default value is passed from a hidden field, which gets populated from a calculation.
 */
async function afterConfigurationAddedMobilePlan(component, configuration) {
	let valueFromMainComponent = configuration.attributes.contracttermmain.value;
	return Promise.all([
		setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.CONTRACT_TERM, valueFromMainComponent),
		restrictValueFromPicklist(configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.DATA_CAP, '0')
	]);
}

/**
 * @description Implementing the afterAttributeUpdated hook for the Mobile Plan component
 */
async function afterAttributeUpdatedMobilePlanComponent(component, configuration, attribute, oldValueMap) {
	switch (attribute.name) {
		case SC_BUSSINESS_MOBILE_ATTRIBUTES.CONTRACT_TERM:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.DATA_ALLOWANCE:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT:
			await renameMobilePlan(component, configuration);
			configuration.updateStatus({ status: true, statusMessage: null });

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CATEGORY:
			await setDataCap(configuration);

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.CONNECTION_TYPE:
			await renameMobilePlan(component, configuration);

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.QUANTITY:
			configuration.updateStatus({ status: true, statusMessage: null });
			break;

		default:
			break;
	}
	return true;
}

/**
 * @description Setting configuration name for Mobile Plan based on commercial product and contract duration
 */
async function renameMobilePlan(component, configuration) {
	let contractTerm = getAttributeValue(configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.CONTRACT_TERM);
	let productVariantName = getAttributeValue(configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_NAME);
	let connectionType = getAttributeValue(configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.CONNECTION_TYPE);
	let name;

	if (productVariantName) {
		name = productVariantName + ', ' + contractTerm + ' maanden, ' + connectionType;

		const attributeData = [
			{
				name: SC_BUSSINESS_MOBILE_ATTRIBUTES.MOBILE_PLAN_CONFIGURATION_NAME,
				value: name
			}
		];
		return component.updateConfigurationAttribute(configuration.guid, attributeData);
	}
	return true;
}

async function setDataCap(configuration) {
	let productVariantCategory = getAttributeValue(configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CATEGORY);
	if (productVariantCategory === SC_BUSSINESS_MOBILE_VALUES.BM_DATA_ONLY) {
		const updateData = { value: '0', displayValue: '-', readOnly: true };
		configuration.updateAttribute(SC_BUSSINESS_MOBILE_ATTRIBUTES.DATA_CAP, updateData);
	} else {
		const updateData = { value: '50', displayValue: '50 Euro', readOnly: false };
		configuration.updateAttribute(SC_BUSSINESS_MOBILE_ATTRIBUTES.DATA_CAP, updateData);
	}

	return true;
}

async function restrictValueFromPicklist(configuration, attributeName, valueToBeRemoved) {
	Object.values(configuration.attributes).forEach((attribute) => {
		if (attribute.name === attributeName) {
			const picklistValues = attribute.options;
			const itemToBeRemoved = { value: valueToBeRemoved };
			let position = picklistValues.findIndex((a) => a.value === itemToBeRemoved.value);
			if (position != -1) {
				picklistValues.splice(position, 1);
			}
			const updateData = { options: picklistValues };
			configuration = configuration.updateAttribute(attributeName, updateData);
		}
	});
	return true;
}

/**
 * @description Validation for Mobile Plan componenet
 */
async function businessMobileValidations(solution) {
	await clearValidationError(solution, SC_COMPONENTS.MOBILE_PLAN_COMPONENT);

	let resultToReturn = true;
	let BMcomponents = solution.components;
	let configsToCheck = filterAllMobilePlanConfigs(BMcomponents);

	if (configsToCheck.length == 1) {
		return checkIfCategory(
			solution,
			configsToCheck,
			SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CATEGORY,
			SC_BUSSINESS_MOBILE_VALUES.BM_DATA_ONLY
		);
	} else {
		let duplicateValidation = checkIfDuplicateProduct(solution, configsToCheck);
		if (!duplicateValidation) {
			return false;
		} else {
			resultToReturn = validateConfigsPairs(solution, configsToCheck);
		}
	}
	return resultToReturn;
}

function validateConfigsPairs(solution, configsToCheck) {
	let resultToReturn = true;
	let quantityCheckResult = true;
	let errorMsgTxt;

	for (let i = 0; i < configsToCheck.length - 1; i++) {
		for (let j = 1; j < configsToCheck.length; j++) {
			if (i !== j) {
				if (
					configsToCheck[i].get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CATEGORY) !=
					configsToCheck[j].get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CATEGORY)
				) {
					if (
						configsToCheck[i].get(SC_BUSSINESS_MOBILE_ATTRIBUTES.CONTRACT_TERM) ==
							configsToCheck[j].get(SC_BUSSINESS_MOBILE_ATTRIBUTES.CONTRACT_TERM) &&
						configsToCheck[i].get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_TIER) ==
							configsToCheck[j].get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_TIER)
					) {
						quantityCheckResult = checkQunatity(solution, configsToCheck[i], configsToCheck[j], configsToCheck);
						configsToCheck[i].set('configHasPair', true);
						configsToCheck[j].set('configHasPair', true);

						if (!quantityCheckResult) {
							return false;
						}
					}
				}
			}
		}
	}
	configsToCheck.forEach(function (config) {
		if (config.get('configHasPair') == false) {
			errorMsgTxt = 'Product ' + config.get('configName') + ' requires Business Mobile plan of the same type and term!';
			showErrorMsg(solution, config.get('configGuid'), errorMsgTxt);
			resultToReturn = false;
		}
	});
	return resultToReturn;
}

function checkQunatity(solution, config1, config2, configs) {
	let errorMsgTxt;

	let BMproductCode, BMDOproductCode, configGuid, configName;
	if (config1.get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CATEGORY) === 'Business Mobile') {
		BMproductCode = config1.get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CODE);
		BMDOproductCode = config2.get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CODE);
		configGuid = config2.get('configGuid');
		configName = config2.get('configName');
	} else {
		BMproductCode = config2.get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CODE);
		BMDOproductCode = config1.get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CODE);
		configGuid = config1.get('configGuid');
		configName = config1.get('configName');
	}

	let BMquantity = 0,
		BMDOquantity = 0;
	BMquantity = sumQuantity(configs, BMproductCode);
	BMDOquantity = sumQuantity(configs, BMDOproductCode);

	if (BMDOquantity > 2 * BMquantity) {
		errorMsgTxt = configName + ' is invalid since the quantity exceeds the limit. The maximum quantity allowed is ' + 2 * BMquantity;
		showErrorMsg(solution, configGuid, errorMsgTxt);
		return false;
	}

	function sumQuantity(configs, productCode) {
		let quantity = 0;
		configs.forEach(function (config) {
			if (config.get(SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CODE) == productCode) {
				quantity = quantity + Number(config.get(SC_BUSSINESS_MOBILE_ATTRIBUTES.QUANTITY));
			}
		});
		return quantity;
	}
	return true;
}

/**
 * @description Checks that the product is not repeated. When adding more of the same product, the quantity field must be updated.
 */
function checkIfDuplicateProduct(solution, configs) {
	let resultToReturn = true;
	let errorMsgTxt;
	let configGuid;
	for (let i = 0; i < configs.length; i++) {
		for (let j = 0; j < configs.length; j++) {
			if (i !== j) {
				if (configs[i].get('configName') === configs[j].get('configName')) {
					errorMsgTxt =
						'Only 1  ' +
						configs[j].get('configName') +
						' allowed. In case you need more of the same product, please update the quantity.';
					configGuid = configs[j].get('configGuid');
					resultToReturn = false;
					break; // terminate inner loop
				}
			}
		}
		if (!resultToReturn) {
			break; // terminate outer loop
		}
	}
	if (!resultToReturn) {
		showErrorMsg(solution, configGuid, errorMsgTxt);
	}
	return resultToReturn;
}

/**
 * @description In the case of a single configuration, check that the category is not Business Mobile data only
 */
function checkIfCategory(solution, configs, attName, attValue) {
	if (configs[0].get(attName) == attValue) {
		let errorMsgTxt = 'Product ' + configs[0].get('configName') + ' requires Business Mobile plan of the same type and term!';

		showErrorMsg(solution, configs[0].get('configGuid'), errorMsgTxt);
		return false;
	}
	return true;
}

/**
 * @description Filters attributes for a single configuration
 */
function filterMobilePlanConfig(allAttributesConfig, configName, configGuid, atributeNames) {
	let mapMobilePlan = new Map();

	mapMobilePlan.set('configName', configName);
	mapMobilePlan.set('configGuid', configGuid);
	mapMobilePlan.set('configHasPair', false);

	Object.keys(allAttributesConfig).forEach((key) => {
		const att = allAttributesConfig[key];

		//check that the attribute name is listed among the attributes to be filtered
		if (atributeNames.includes(att.name)) {
			mapMobilePlan.set(att.name, att.value);

			if (att.name == SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_SUBSCRIPTION && att.value == SC_BUSSINESS_MOBILE_VALUES.VOICE_AND_DATA) {
				mapMobilePlan.set('configHasPair', true);
			}
		}
	});
	return mapMobilePlan;
}

/**
 * @description This function finds the configuration attributes that need to be validated for components configurations, and returns an array of maps
 */
function filterAllMobilePlanConfigs(components) {
	let mapMobilePlan = new Map();
	let configsFiltered = new Array();

	Object.keys(components).forEach((key) => {
		const component = components[key];

		if (component.name == SC_COMPONENTS.MOBILE_PLAN_COMPONENT) {
			let configsMP = component.schema.configurations;

			Object.keys(configsMP).forEach((key) => {
				const configMP = configsMP[key];

				let allAttributes = configMP.attributes;

				//all attributes that need to be added to a map object
				const atributeNamesToInclude = [
					SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CATEGORY,
					SC_BUSSINESS_MOBILE_ATTRIBUTES.QUANTITY,
					SC_BUSSINESS_MOBILE_ATTRIBUTES.CONTRACT_TERM,
					SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_SUBSCRIPTION,
					SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_TIER,
					SC_BUSSINESS_MOBILE_ATTRIBUTES.CONNECTION_TYPE,
					SC_BUSSINESS_MOBILE_ATTRIBUTES.DATA_CAP,
					SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_CODE
				];
				mapMobilePlan = filterMobilePlanConfig(allAttributes, configMP.name, configMP.guid, atributeNamesToInclude);

				//adding map to array
				configsFiltered.push(mapMobilePlan);
			});
		}
	});
	return configsFiltered;
}

/**
 * @description Display the error message in the alert and on the configuration
 */
function showErrorMsg(solution, configurationGuid, errorMessage) {
	let config = solution.getConfiguration(configurationGuid);
	config.updateStatus({ status: false, message: errorMessage });
	CS.SM.displayMessage(errorMessage, 'error');
}

/**
 * @description Delete previous validation errors on configurations for componentName and validate solution
 */
async function clearValidationError(solution, componentName) {
	let allConfigs = await SC_Util.generateFlatConfigurationListAsync();
	for (let i in allConfigs) {
		if (allConfigs[i].name == componentName) {
			await validateConfiguration(allConfigs[i].value, true, '');
		}
	}
	const result = solution.validate();
}
