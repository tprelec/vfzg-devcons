//CS.EventHandler.subscribers
//
window.eventSubscribed = false;
//Access Infrastructure PD subscribers
//require(['cs_js/cs-full', 'cs_js/cs-event-handler','cs_js/csservice'], function(CS, EventHandler) {


function removeFirstColumnAndRemoveNameLinkMobileCTN(){
    jQuery("table[data-cs-control='OneNet_Integration_0'].list").each(function() {jQuery(this).find(' tr th:first').hide()});  
    jQuery("table[data-cs-control='OneNet_Integration_0'].list").each(function() {jQuery(this).find('tr').each(function(){jQuery(this).find('td:first').hide();})});
    
    jQuery("[data-cs-action='editRelatedProduct']").each(function() {    
    if(jQuery(this).text() != 'Edit') {
        jQuery(this).after("<span>" + jQuery(this).text() + "</span>");
        jQuery(this).remove();
    }
    });
}

function removeFirstColumnAndRemoveNameLink() {
       
    /*jQuery("table tr").each(function() {
        var attr = jQuery(this).attr('data-cs-ref');
        var cpeElement = false;
        // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
        
        if (typeof attr !== typeof undefined && attr !== false) {
            if(attr.indexOf("CPE_") >= 0 && attr.indexOf("CPE_SLA_") == -1) {
                cpeElement = true;
                jQuery(this).find("td:first:contains('Edit')").find("span:contains('Copy')").after("<span>-</span>").remove();
                jQuery(this).find("td:first:contains('Edit')").find("span:contains('Del')").after("<span>-</span>").remove();
            } else {
                jQuery(this).find("td:first:contains('Edit')").remove();
            }
            

        }
      
            if(cpeElement==false) {
                jQuery(this).find("th:first:contains('Action')").remove();
            }       

    });

    jQuery("[data-cs-action='editRelatedProduct']").each(function() {    
        if(jQuery(this).text() != 'Edit') {
            jQuery(this).after("<span>" + jQuery(this).text() + "</span>");
            jQuery(this).remove();
        }
    });*/
    
    /*
    jQuery("table tr").each(function() {
        var attr = jQuery(this).attr('data-cs-ref');
        var cpeElement = false;
        // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
        
        if (typeof attr !== typeof undefined && attr !== false) {
            if(attr.indexOf("CPE_") >= 0 && attr.indexOf("CPE_SLA_") == -1) {
                cpeElement = true;
                jQuery(this).find("td:first:contains('Edit')").find("span:contains('Copy')").after("<span>-</span>").remove();
                jQuery(this).find("td:first:contains('Edit')").find("span:contains('Del')").after("<span>-</span>").remove();
            } else {
                jQuery(this).find("td:first:contains('Edit')").remove();
            }
            

        }});
        */
    //jQuery("table:not([data-cs-control='CPE_0']).list").each(function() {jQuery(this).find(' tr th:first').hide()});
    //jQuery("table:not([data-cs-control='CPE_0']).list").each(function() {jQuery(this).find('tr').each(function(){jQuery(this).find('td:first').hide();})});
    
    jQuery("table.list").each(function() {jQuery(this).find(' tr th:first').hide()});
    jQuery("table.list").each(function() {jQuery(this).find('tr').each(function(){jQuery(this).find('td:first').hide();})});
    
    /*
    jQuery("table tr").each(function() {
        var attr = jQuery(this).attr('data-cs-ref');
        var cpeElement = false;
        // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
        
        if (typeof attr !== typeof undefined && attr !== false) {
            if(attr.indexOf("CPE_") >= 0 && attr.indexOf("CPE_SLA_") == -1) {
                cpeElement = true;
                jQuery(this).find("td:first:contains('Edit')").find("span:contains('Copy')").after("<span>-</span>").remove();
                jQuery(this).find("td:first:contains('Edit')").find("span:contains('Del')").after("<span>-</span>").remove();
            }
        }
    });
    */
    jQuery("[data-cs-action='editRelatedProduct']").each(function() {    
        if(jQuery(this).text() != 'Edit') {
            jQuery(this).after("<span>" + jQuery(this).text() + "</span>");
            jQuery(this).remove();
        }
    });
}


 require(['cs_js/cs-full', 'cs_js/cs-event-handler', 'cs_js/csservice'],
 function(CS, EventHandler, Service) {
     
//NewAddon button visibility
function hideAddonsButton(){ 
setTimeout(function(){
jQuery('span[data-cs-action="editRelatedProduct"][data-cs-ref^="Addons_"]').each(function(){jQuery(this).hide();})},0);
//jQuery("button[data-cs-ref='Addons_0']").hide();},0); 
} 

function hidePayAsYouUseButton(){ 
setTimeout(function(){
jQuery("button[data-cs-ref='PayAsYouUse_product_0']").hide();},0); 
} 

window.hidePayAsYouUseButtonEvents = function hidePayAsYouUseButtonEvents(){
    
    
    subscribeEvent(EventHandler.Event.AFTER_CONTINUE_ACTION, function(payload) { 
    setTimeout(function(){ hidePayAsYouUseButton();}, 0); 
    });
    
    subscribeEvent(EventHandler.Event.AFTER_NEXT_SCREEN_ACTION, function(payload) { 
    setTimeout(function(){ hidePayAsYouUseButton();}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.AFTER_PREVIOUS_SCREEN_ACTION, function(payload) { 
    setTimeout(function(){ hidePayAsYouUseButton()}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.CONFIGURATOR_READY, function(payload) { 
    setTimeout(function(){ hidePayAsYouUseButton()}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.RULES_ITERATION_END, function(payload) { 
    setTimeout(function(){ hidePayAsYouUseButton()}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.RULES_FINISHED, function(payload) { 
    setTimeout(function(){ hidePayAsYouUseButton()}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.AFTER_ATTRIBUTE_UPDATE, function(payload) { 
    setTimeout(function(){ hidePayAsYouUseButton()}, 0); 
    });
}

window.hideAddonsButtonEvents = function hideAddonsButtonEvents(){
    
    
    subscribeEvent(EventHandler.Event.AFTER_CONTINUE_ACTION, function(payload) { 
    setTimeout(function(){ hideAddonsButton();}, 0); 
    });
    
    subscribeEvent(EventHandler.Event.AFTER_NEXT_SCREEN_ACTION, function(payload) { 
    setTimeout(function(){ hideAddonsButton();}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.AFTER_PREVIOUS_SCREEN_ACTION, function(payload) { 
    setTimeout(function(){ hideAddonsButton()}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.CONFIGURATOR_READY, function(payload) { 
    setTimeout(function(){ hideAddonsButton()}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.RULES_ITERATION_END, function(payload) { 
    setTimeout(function(){ hideAddonsButton()}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.RULES_FINISHED, function(payload) { 
    setTimeout(function(){ hideAddonsButton()}, 0); 
    }); 
    
    subscribeEvent(EventHandler.Event.AFTER_ATTRIBUTE_UPDATE, function(payload) { 
    setTimeout(function(){ hideAddonsButton()}, 0); 
    });
}
//end

window.contractTermUpdate = function contractTermUpdate(payload) {
    CS.Service.invokePlugin('AIContractTermUpdatePlugin', CS.Service.config["Access_Infrastructure_0"].attr.cscfga__Product_Configuration__c);
}

window.accountServicesInvalidation = function accountServicesInvalidation(payload) {
	console.log('ASIP static resource entered'); 
    CS.Service.invokePlugin('AccountServicesInvalPlugin', CS.Service.config["Mobile_CTN_subscription_0"].attr.cscfga__Product_Configuration__c);
}

window.accessInfraAfterAttributeUpdate = function accessInfraAfterAttributeUpdate(payload){
    //moved to Rules 714 and 715
        /*if(payload.wrapper.reference == 'Modular_pricing_0' && payload.isValueUpdate){ 
            if(payload.properties.value == false){ 
                clearAccessConfiguration(); 
            } 
            else { 
                clearBundleConfiguration(); 
            } 
        } */
        
        //moved to Rule 716
        /*if(payload.wrapper.reference == 'Site_Check_0' && payload.isValueUpdate && payload.oldValue != ''){ 
            CS.setAttributeValue('CPE_SLA_0', ''); 
            clearAccessConfiguration(); 
            clearBundleConfiguration(); 
        } */
    
    //moved to Rule 718
        //delete all except site if contract duration is changed
        //UAT defect 11-5-2018
        /*if(payload.wrapper.reference == 'Contract_Duration_0' && payload.isValueUpdate && payload.oldValue != ''){ 
            clearAccessConfigurationProductsOnly(); 
            clearBundleConfigurationProductsOnly();
            //CS.setAttributeValue('Bundle_Products_0:BundlePGAset_0','');
        }*/
        
    //moved to Rule 312
        /*if(payload.wrapper.reference == 'Scenario_0' && payload.isValueUpdate){ 
            if(CS.getAttributeValue('Hardware_Toeslag_set_0') != ''){ 
                CS.setAttributeValue('Hardware_Toeslag_set_0',''); 
            } 
            if(CS.getAttributeValue('Hardware_Toeslag_Id_0') != ''){ 
                CS.setAttributeValue('Hardware_Toeslag_Id_0',''); 
            } 
    
            if(CS.getAttributeValue('EVC1_error_0') != ''){ 
                CS.setAttributeValue('EVC1_error_0',''); 
            } 
    
            if(CS.getAttributeValue('EVC2_error_0') != ''){ 
                CS.setAttributeValue('EVC2_error_0',''); 
            } 
    
            if(CS.getAttributeValue('EVC1_set_0') != ''){ 
                CS.setAttributeValue('EVC1_set_0',''); 
            } 
    
            if(CS.getAttributeValue('EVC2_set_0') != ''){ 
                CS.setAttributeValue('EVC2_set_0',''); 
            } 
    
            //clear site check instead of removing RP's 
            if(CS.getAttributeValue('Site_Check_0') != ''){ 
            console.log('>>>> clearing Site Check selection');  
            CS.Util.SelectListLookup.clear('Site_Check_0'); 
            } 
        } */
    
    //moved to Rule719
        /*if(payload.wrapper.reference == 'CPE_SLA_0' && payload.isValueUpdate && payload.oldValue != ''){ 
            if(CS.getAttributeValue('CPE_SLA_set_0') != ''){ 
                CS.setAttributeValue('CPE_SLA_set_0',''); 
            } 
            if(CS.Service.config["CPE_SLA_items_0"].relatedProducts.length > 0){ 
                CS.Service.removeRelatedProduct('CPE_SLA_items_0'); 
            } 
        } */
    
    //moved to Rules 720
        /*if(payload.wrapper.reference == 'SLA_Method_0' && payload.isValueUpdate&& payload.oldValue != ''){ 
            if(CS.getAttributeValue('CPE_SLA_set_0') != ''){ 
                CS.setAttributeValue('CPE_SLA_set_0',''); 
            } 
            if(CS.Service.config["CPE_SLA_items_0"].relatedProducts.length > 0){ 
                CS.Service.removeRelatedProduct('CPE_SLA_items_0'); 
            } 
        } */
    
    //moved to Rule 721
        /*if(payload.wrapper.reference == 'Quality_of_Service_0' && payload.isValueUpdate){ 
            if(CS.getAttributeValue('CPE_set_0') != ''){ 
                CS.setAttributeValue('CPE_set_0',''); 
            } 
    
            if(CS.getAttributeValue('Hardware_Toeslag_set_0') != ''){ 
                CS.setAttributeValue('Hardware_Toeslag_set_0',''); 
            } 
    
            if(CS.getAttributeValue('Hardware_Toeslag_Id_0') != ''){ 
                CS.setAttributeValue('Hardware_Toeslag_Id_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC1_error_0') != ''){ 
                CS.setAttributeValue('EVC1_error_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC2_error_0') != ''){ 
                CS.setAttributeValue('EVC2_error_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC1_set_0') != ''){ 
                CS.setAttributeValue('EVC1_set_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC2_set_0') != ''){ 
                CS.setAttributeValue('EVC2_set_0',''); 
            } 
    
            //clear site check instead of removing RP's 
            if(CS.getAttributeValue('Site_Check_0') != ''){ 
            console.log('>>>> clearing Site Check selection');  
            CS.Util.SelectListLookup.clear('Site_Check_0'); 
            } 
        } */
    
    //moved to Rule 722
       /* if(payload.wrapper.reference == 'Infra_SLA_0' && payload.isValueUpdate && payload.oldValue != ''){ 
            clearAccessConfigurationProductsOnly(); 
            clearBundleConfigurationProductsOnly(); 
        } */
    
    //moved to Rule 723
            //depends on parent or child context 
        /*if(payload.wrapper.reference == 'EVC_PVC_dep_0' && payload.isValueUpdate){ 
        
            if(CS.Service.config["Access_Infrastructure_0"].relatedProducts.length > 0){ 
            _.each(CS.Service.config["Access_Infrastructure_0"].relatedProducts,function(p){ 
            CS.Service.removeRelatedProduct('Access_Infrastructure_0'); 
            }); 
            } 
            
            if(CS.getAttributeValue('Access_set_0') !== ''){ 
            CS.setAttributeValue('Access_set_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC_done_0') !== ''){ 
            CS.setAttributeValue('EVC_done_0',''); 
            } 
        } */
        
        //moved to Rule 725
        /*if((payload.wrapper.reference == 'Bundle_Products_0' && payload.properties.value !== undefined && payload.properties.value !== '' && payload.isValueUpdate) 
            || (payload.wrapper.reference == 'Bundle_Products_0:Bundle_lookup_0' && payload.isValueUpdate)){
            if(CS.Service.config["Bundle_Products_0"].relatedProducts.length > 0){ 
                for(var i = 0; i < CS.Service.config["Bundle_Products_0"].relatedProducts.length; i++){
                    var hirarchyLevel = CS.getAttributeValue('Bundle_Products_'+ i +':HirarchyLevel_0');
                    
                    if(hirarchyLevel != 'Parent'){
                        CS.Service.removeRelatedProduct('Bundle_Products_'+ i); 
                        i--;
                    }
                }
                
                CS.setAttributeValue('Bundle_Products_0:BundlePGAset_0','',true);
            } 
        }*/
        
        //Rules+ 588, Rule 710, Rule 728, Rule 711, Rule 729
        //hide PBX if Site Check is empty or One Fixed is empty
        if((CS.getAttributeValue('One_Fixed_0')=='') || (CS.getAttributeValue('Site_Check_0')=='')){
            setTimeout(function(){ jQuery("[data-cs-label='PBX_0']").parent().hide();CS.setAttributeValue('PBX_0','');}, 0);
        }
        else{
           setTimeout(function(){ jQuery("[data-cs-label='PBX_0']").parent().show();}, 0); 
        }
        
        //moved to Rule 219
        //setTimeout(function(){ removeFirstColumnAndRemoveNameLink();}, 0);
    }
    
    window.accessInfraConfigReady = function accessInfraConfigReady(payload){
        console.log('CONFIGURATOR UI CONFIGURATOR_READY catched!'); 
        CS.EAPI.hideAllAdditionButtons(); 
        showHideModularSections(); 
        checkModeAndSet(); 
        
        CS.Rules.evaluateAllRules();

        setTimeout(function(){ renderLookupsForAccessInfra();}, 0);
    }
     
    window.accessInfraScreenListPopulated = function accessInfraScreenListPopulated(payload){
        console.log('CONFIGURATOR UI SCREEN_LIST_POPULATED catched!'); 
        CS.EAPI.hideAllAdditionButtons(); 
        showHideModularSections(); 
        checkModeAndSet(); 
        setTimeout(function(){ renderLookupsForAccessInfra();}, 0); 
        var x = '<script type=\"text\/html\" id=\"CS.MultipleRelatedProduct__tpl\">\r\n\t<%\tvar prefix = CS.Util.configuratorPrefix;\r\n\t\tvar max = definition[prefix + \'Max__c\'];\r\n\t\tvar inlineEdit = definition[\'Inline_Edit__c\'];\r\n\t\tvar disabled = ((max && relatedProducts.length >= max) ? \'disabled=\"disabled\"\' : \'\');\r\n\t\tvar prod;\r\n\t\tvar attRef;\r\n\t\tvar rowClass;\r\n\t\tvar errorClass;\r\n\t\tvar errorMessage;\r\n\t\tvar attr = anchor.attr;\r\n relatedProductsIds = new Array( );\r\n\t\tvar isReadOnly = attr[prefix + \'Is_Read_Only__c\'];\r\n\t\tvar isActive = attr[prefix + \'is_active__c\'];\r\n\t\tvar isRequired = attr[prefix + \'Is_Required__c\'];\r\n\t\tif (max && relatedProducts.length > max) {\r\n\t\t\terrorClass = \'attributeError\';\r\n\t\t\terrorMessage = \'<p class=\"attributeErrorMessage\">{!$Label.cscfga__attrcls_The_maximum_number_of} \' + max + \' {!$Label.cscfga__pchlpcls_exceeded_related_items}<\/p>\';\r\n\t\t}\r\n\t%>\r\n\t\t<div class=\"apexp\">\r\n\t\t\t<div class=\"individualPalette\">\r\n\t\t\t\t<div class=\"Custom24Block <%= errorClass %>\">\r\n\t\t\t\t\t<div class=\"bPageBlock brandSecondaryBrd apexDefaultPageBlock secondaryPalette\">\r\n\t\t\t\t\t\t<div class=\"pbHeader\">\r\n\t\t\t\t\t\t\t<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td class=\"pbTitle\" data-role=\"fieldcontain\">\r\n\t\t\t\t\t\t\t\t\t\t<h2 class=\"mainTitle\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span><%= definition.Name %><\/span>\r\n\t\t\t\t\t\t\t\t\t\t\t<% if (isRequired) { %><span class=\"required\">(Required)<\/span><% } %>\r\n\t\t\t\t\t\t\t\t\t\t\t<% if (isReadOnly) { %><span class=\"readOnly\">(Read Only)<\/span><% } %>\r\n\t\t\t\t\t\t\t\t\t\t<\/h2>\r\n\t\t\t\t\t\t\t\t\t<\/td>\r\n\t\t\t\t\t\t\t\t\t<td class=\"pbButton \">\r\n\t\t\t\t\t\t\t\t\t<% if (isActive && ! isReadOnly) { %>\r\n\t\t\t\t\t\t\t\t\t\t<% \r\n\t\t\t\t\t\t\t\t\t\t\tconsole.log(inlineEdit);\r\n\t\t\t\t\t\t\t\t\t\t\tif (inlineEdit == true) {\r\n\t\t\t\t\t\t\t\t\t\t%>\r\n\t\t\t\t\t\t\t\t\t\t\t<button class= \"btn btn-primary\" <%= disabled %> data-cs-control=\"<%= anchor.reference %>\" data-cs-ref=\"<%= anchor.reference %>\" onclick=\"CS.InlineEdit.addRelatedProduct(\'<%= anchor.reference %>\', \'<%= anchor.definitionId %>\')\" data-cs-type=\"add\" data-role=\"none\">New <%= definition.Name %><\/button>\r\n\t\t\t\t\t\t\t\t\t\t<%\r\n\t\t\t\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t\t\t\t\telse {\r\n\t\t\t\t\t\t\t\t\t\t%>\r\n\t\t\t\t\t\t\t\t\t\t\t<button <%= disabled %> class=\"add\" data-cs-control=\"<%= anchor.reference %>\" data-cs-ref=\"<%= anchor.reference %>\" data-cs-action=\"addRelatedProduct\" data-cs-type=\"add\" data-role=\"none\">New <%= (definition[prefix + \'Label__c\']) ? definition[prefix + \'Label__c\'] : definition.Name %><\/button>\r\n\t\t\t\t\t\t\t\t\t\t<%\r\n\t\t\t\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t\t\t\t%>\r\n\t\t\t\t\t\t\t\t\t<% } %>\r\n\t\t\t\t\t\t\t\t\t<\/td>\r\n\t\t\t\t\t\t\t\t<\/tr>\r\n\t\t\t\t\t\t\t<\/table>\r\n\t\t\t\t\t\t<\/div>\r\n\t\t\t\t\t\t<div class=\"pbBody\">\r\n\t<% if (relatedProducts.length > 0) { %>\r\n\t\t<table class=\"list\" data-cs-binding=\"<%= anchor.reference %>\" data-cs-control=\"<%= anchor.reference %>\" data-cs-type=\"list\">\r\n\t\t\t<thead class=\"rich-table-thead\">\r\n\t\t\t\t<tr class=\"headerRow\">\r\n\t\t\t\t\t<th class=\"headerRow\" style=\"width: 9em;\">Action<\/th>\r\n\t\t\t\t\t<th class=\"headerRow\" style=\"width: 6em;\">Status<\/th>\r\n\t\t\t\t\t<th class=\"headerRow\">Name<\/th>\r\n\t<%\tfor (var i = 0; i < cols.length; i++) {\r\n\t\t\tvar spec = colSpecs[cols[i]]; %>\r\n\t\t\t\t<th class=\"headerRow\">\r\n\t\t\t\t\t<%= spec.header %>\r\n\t\t\t\t<\/th>\r\n\t<%\t} %>\r\n\t\t\t\t<\/tr>\r\n\t\t\t<\/thead>\r\n\t<%\tfor (var i = 0; i < relatedProducts.length; i++) {\r\n\t\t\tprod = relatedProducts[i];\r\n\t\t\trowClass = \'dataRow \' + (i\/2 == Math.floor(i\/2) ? \'even \' : \'odd \') + (i == 0 ? \'first \' : \'\') + (i >= relatedProducts.length - 1 ? \'last\' : \'\');\r\n\t%>\r\n\t\t\t<tr class=\"<%= rowClass %>\" data-cs-ref=\"<%= prod.reference %>\">\r\n\t\t\t\t<td class=\"dataCell\">\r\n\t\t\t\t<% if (isActive && ! isReadOnly) { %>\r\n\t\t\t\t\t<span data-cs-action=\"editRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\">Edit<\/span> |\r\n\t\t\t\t\t<span data-cs-action=\"removeRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\">Del<\/span>\r\n\t\t\t\t\t<% if (disabled === \'\') { %>\r\n\t\t\t\t\t\t| <span data-cs-action=\"copyRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\">Copy<\/span>\r\n\t\t\t\t\t<% } %>\r\n\t\t\t\t<% } %>\r\n\t\t\t\t<\/td>\r\n\t\t\t\t<td class=\"dataCell\"><%= prod.config[prefix + \'Configuration_Status__c\'] %><\/td>\r\n\t\t\t\t<td class=\"dataCell\"><span data-cs-action=\"editRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\"><%= prod.config.Name %><\/span><\/td>\r\n\t<%\t\t\tvar prefix = CS.Util.configuratorPrefix;\r\n\t\t\t\tfor (var j = 0; j < cols.length; j++) {\r\n\t\t\t\tif (colSpecs[cols[j]].ref != undefined) {\r\n\t\t\t\t\tattRef = prod.reference + \':\' + colSpecs[cols[j]].ref;\r\n\t\t\t\t}\r\n\t\t\t\tif (attRef != undefined) {\r\n\t\t\t\t\tvar displayInfo = CS.Service.config[attRef][\'displayInfo\'];\r\n var definition = CS.Service.config[attRef].definition;\r\n console.log(displayInfo);\r\n\t\t\t\t\tif (\'User Input\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <td><input class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" type=\"text\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\"><\/input><\/td>\r\n <%\r\n } else if (\'Date\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <td><input class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" data-role=\"none\" \r\n\t\t\t\t\tonfocus=\"DatePicker.pickDate(true, \'<%=attRef%>\', false );\" size=\"12\" \/><\/input>\r\n\t\t\t\t\t <span class=\"dateFormat\">\r\n\t\t\t\t\t [&nbsp;<a href=\"\" class=\"todayPicker\" onclick=\"return (function(){ DatePicker.insertDate(CS.todayFormatted, \'<%=attRef%>\', true); return false; })();\"><%=CS.todayFormatted%><\/a>&nbsp;]\r\n\t\t\t\t <\/span><\/td>\r\n <%\r\n\t\t\t\t\t} else if (\'Checkbox\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <% if (CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\') {\r\n %>\r\n <td><input type=\"checkbox\" class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" checked=\"true\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" ><\/input><\/td>\r\n <%\r\n } else {\r\n %>\r\n <td><input type=\"checkbox\" class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" ><\/input><\/td>\r\n <% \r\n } %>\r\n <%\r\n\t\t\t\t\t \r\n\t\t\t\t\t} else if (\'Lookup\' === displayInfo && !definition[prefix + \'Select_List_Lookup__c\'] && inlineEdit){ \r\n\r\n lookups = true;\r\n %>\r\n <td>\r\n <span class=\"lookupInput\">\r\n <input type=\"text\"\r\n id=\"<%=attRef%>\"\r\n name=\"<%=attRef%>\"\r\n data-cs-binding=\"<%=attRef%>\"\r\n data-cs-select-list-lookup=\"false\"\r\n data-role=\"none\"\r\n data-mini=\"true\"\r\n value=\"\"\r\n size=\"20\"\r\n onchange=\"CS.InlineEdit.updateAttribute(this)\" \/>\r\n <\/span>\r\n <\/td>\r\n <%\r\n } else if (\'Lookup\' === displayInfo && definition[prefix + \'Select_List_Lookup__c\'] && inlineEdit){ \r\n\r\n lookups = true;\r\n %>\r\n <td>\r\n <div class=\"form-control form-control-select-lookup\">\r\n <span class=\"lookupInput\">\r\n <input style=\"border: none;\" type=\"text\"\r\n id=\"<%=attRef%>\"\r\n name=\"<%=attRef%>\"\r\n data-cs-binding=\"<%=attRef%>\"\r\n data-cs-select-list-lookup=\"true\"\r\n data-role=\"none\"\r\n data-mini=\"true\"\r\n value=\"\"\r\n size=\"20\"\r\n onchange=\"CS.InlineEdit.updateAttribute(this)\" \/>\r\n <\/span>\r\n <\/div>\r\n <\/td>\r\n <% \r\n if (relatedProductsIds.indexOf(attRef) == -1) {\r\n relatedProductsIds.push(attRef);\r\n }\r\n }\r\n else if (\'Select List\' === displayInfo && inlineEdit) {\r\n %>\r\n <td>\r\n <select class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" onchange=\"CS.InlineEdit.updateAttribute(this)\">\r\n <%\r\n var options = CS.Service.config[attRef][\'selectOptions\'];\r\n for (var k = 0; k < options.length; k++) {\r\n var selected = (options[k][\'Name\'] === CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\']);\r\n if (selected) {\r\n %>\r\n <option value=\"<%= options[k][\'cscfga__Value__c\'] %>\" selected=\"selected\"><%= options[k][\'Name\'] %><\/option>\r\n <%\r\n } else {\r\n %> \r\n <option value=\"<%= options[k][\'cscfga__Value__c\'] %>\"><%= options[k][\'Name\'] %><\/option>\r\n\t<%\t\t }\r\n }\r\n %> \r\n <\/select>\r\n <\/td>\r\n <%\r\n }\r\n else if (attRef.indexOf(\'Additional_Attributes\') != -1) {\r\n \r\n\t\t\t\t\t\tvar addAtts = CS.getAttributeValue(attRef);\r\n\t\t\t\t\t\tif (addAtts != \'\') {\r\n\t%>\r\n \t\t\t\t\t\t<td><table><tr>\r\n\t<%\r\n\t\t\t\t\t\t\taddAtts = addAtts.split(\',\');\r\n\t\t\t\t\t\t\tfor (var q = 0; q < addAtts.length; q++) {\r\n\t%>\r\n\t\t\t\t\t\t\t\t<th><%= addAtts[q] %><\/th>\r\n\t<%\r\n\t\t\t\t\t\t\t}\r\n\t%>\r\n\t\t\t\t\t\t\t<\/tr>\r\n\t\t\t\t\t\t\t<tr>\r\n\t<%\r\n\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\tfor (var q = 0; q < addAtts.length; q++) {\r\n\t\t\t\t\t\t\t\tvar addAttRef = prod.reference + \':\' + addAtts[q].split(\' \').join(\'_\') + \'_0\';\r\n\r\n\t%>\r\n\t<%\r\nvar displayInfo = CS.Service.config[addAttRef][\'displayInfo\'];\r\n var definition = CS.Service.config[addAttRef].definition;\r\n console.log(displayInfo);\r\n\t\t\t\t\tif (\'User Input\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <td><input class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" type=\"text\" value=\"<%= CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\"><\/input><\/td>\r\n <%\r\n } else if (\'Date\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <td><input class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" value=\"<%= CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" data-role=\"none\" \r\n\t\t\t\t\tonfocus=\"DatePicker.pickDate(true, \'<%=addAttRef%>\', false );\" size=\"12\" \/><\/input>\r\n\t\t\t\t\t <span class=\"dateFormat\">\r\n\t\t\t\t\t [&nbsp;<a href=\"\" class=\"todayPicker\" onclick=\"return (function(){ DatePicker.insertDate(CS.todayFormatted, \'<%=addAttRef%>\', true); return false; })();\"><%=CS.todayFormatted%><\/a>&nbsp;]\r\n\t\t\t\t <\/span><\/td>\r\n <%\r\n\t\t\t\t\t} else if (\'Checkbox\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <% if (CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\') {\r\n %>\r\n <td><input type=\"checkbox\" class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" checked=\"true\" value=\"<%= CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" ><\/input><\/td>\r\n <%\r\n } else {\r\n %>\r\n <td><input type=\"checkbox\" class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" value=\"<%= CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" ><\/input><\/td>\r\n <% \r\n } %>\r\n <% \r\n }\r\n else if (\'Select List\' === displayInfo && inlineEdit) {\r\n %>\r\n <td>\r\n <select class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" onchange=\"CS.InlineEdit.updateAttribute(this)\">\r\n <%\r\n var options = CS.Service.config[addAttRef][\'selectOptions\'];\r\n for (var k = 0; k < options.length; k++) {\r\n var selected = (options[k][\'Name\'] === CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\']);\r\n if (selected) {\r\n %>\r\n <option value=\"<%= options[k][\'cscfga__Value__c\'] %>\" selected=\"selected\"><%= options[k][\'Name\'] %><\/option>\r\n <%\r\n } else {\r\n %> \r\n <option value=\"<%= options[k][\'cscfga__Value__c\'] %>\"><%= options[k][\'Name\'] %><\/option>\r\n\t<%\t\t }\r\n }\r\n %> \r\n <\/select>\r\n <\/td>\r\n <%\r\n }\r\n %> \r\n\t\r\n\t<%\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t}\r\n\t%>\r\n\t\t\t\t\t\t\t<\/tr><\/table><\/td>\r\n <%\r\n\t\t\t\t\t\t} \r\n\t\r\n }\r\n else {\r\n %>\r\n <td><%= CS.getAttributeDisplayValue(attRef) %><\/td>\r\n <%\r\n }\r\n }\r\n }\r\n %>\r\n\t\t\t<\/tr>\r\n\t<% } %>\r\n\t\t<\/table>\r\n\t<% } else { %>\r\n\t\t<table class=\"list\" data-cs-control=\"<%= anchor.reference %>\" data-cs-type=\"list\">\r\n\t\t\t<tr class=\"dataRow even first last\">\r\n\t\t\t\t<td class=\"dataCell\">\r\n\t\t\t\t\tNo items to display\r\n\t\t\t\t<\/td>\r\n\t\t\t<\/tr>\r\n\t\t<\/table>\r\n\t<% } %>\r\n\t\t\t\t\t<\/div>\r\n\t\t\t\t\t\t<div class=\"pbFooter secondaryPalette\">\r\n\t\t\t\t\t\t\t<div class=\"bg\"><\/div>\r\n\t\t\t\t\t\t<\/div>\r\n\t\t\t\t\t<\/div>\r\n\t\t\t\t<%= errorMessage %>\r\n\t\t\t\t<\/div>\r\n\t\t\t<\/div>\r\n\t\t<\/div>\r\n\t<\/script>'; 
        CS.App.Components.Repository.addComponent('configurator', 'MultipleRelatedProductLE', x);
        
        applySkillBasedLogic();
    }
    
    window.AccessInfraEvents = function AccessInfraEvents(){
         if(eventSubscribed == false){
             eventSubscribed = true;
        console.log('Executing rules !!!!');
        //CS.setAttributeValue('EventsSubscribed_0',true,true);
        
        EventHandler.subscribe(EventHandler.Event.AFTER_ATTRIBUTE_UPDATE, function(payload) { 
        if(payload.wrapper.reference == 'Modular_pricing_0' && payload.isValueUpdate){ 
            if(payload.properties.value == false){ 
                clearAccessConfiguration(); 
            } 
            else { 
                clearBundleConfiguration(); 
            } 
        } 
        
        if(payload.wrapper.reference == 'Site_Check_0' && payload.isValueUpdate && payload.oldValue != ''){ 
            CS.setAttributeValue('CPE_SLA_0', ''); 
            clearAccessConfiguration(); 
            clearBundleConfiguration(); 
        } 
    
        //delete all except site if contract duration is changed
        //UAT defect 11-5-2018
        if(payload.wrapper.reference == 'Contract_Duration_0' && payload.isValueUpdate && payload.oldValue != ''){ 
            clearAccessConfigurationProductsOnly(); 
            clearBundleConfigurationProductsOnly();
        }
        
    
        if(payload.wrapper.reference == 'Scenario_0' && payload.isValueUpdate){ 
            if(CS.getAttributeValue('Hardware_Toeslag_set_0') != ''){ 
                CS.setAttributeValue('Hardware_Toeslag_set_0',''); 
            } 
            if(CS.getAttributeValue('Hardware_Toeslag_Id_0') != ''){ 
                CS.setAttributeValue('Hardware_Toeslag_Id_0',''); 
            } 
    
            if(CS.getAttributeValue('EVC1_error_0') != ''){ 
                CS.setAttributeValue('EVC1_error_0',''); 
            } 
    
            if(CS.getAttributeValue('EVC2_error_0') != ''){ 
                CS.setAttributeValue('EVC2_error_0',''); 
            } 
    
            if(CS.getAttributeValue('EVC1_set_0') != ''){ 
                CS.setAttributeValue('EVC1_set_0',''); 
            } 
    
            if(CS.getAttributeValue('EVC2_set_0') != ''){ 
                CS.setAttributeValue('EVC2_set_0',''); 
            } 
    
            //clear site check instead of removing RP's 
            if(CS.getAttributeValue('Site_Check_0') != ''){ 
            console.log('>>>> clearing Site Check selection');  
            CS.Util.SelectListLookup.clear('Site_Check_0'); 
            } 
        } 
    
        if(payload.wrapper.reference == 'CPE_SLA_0' && payload.isValueUpdate && payload.oldValue != ''){ 
            if(CS.getAttributeValue('CPE_SLA_set_0') != ''){ 
                CS.setAttributeValue('CPE_SLA_set_0',''); 
            } 
            if(CS.Service.config["CPE_SLA_items_0"].relatedProducts.length > 0){ 
                CS.Service.removeRelatedProduct('CPE_SLA_items_0'); 
            } 
        } 
    
        if(payload.wrapper.reference == 'SLA_Method_0' && payload.isValueUpdate&& payload.oldValue != ''){ 
            if(CS.getAttributeValue('CPE_SLA_set_0') != ''){ 
                CS.setAttributeValue('CPE_SLA_set_0',''); 
            } 
            if(CS.Service.config["CPE_SLA_items_0"].relatedProducts.length > 0){ 
                CS.Service.removeRelatedProduct('CPE_SLA_items_0'); 
            } 
        } 
    
        if(payload.wrapper.reference == 'Quality_of_Service_0' && payload.isValueUpdate){ 
            if(CS.getAttributeValue('CPE_set_0') != ''){ 
                CS.setAttributeValue('CPE_set_0',''); 
            } 
    
            if(CS.getAttributeValue('Hardware_Toeslag_set_0') != ''){ 
                CS.setAttributeValue('Hardware_Toeslag_set_0',''); 
            } 
    
            if(CS.getAttributeValue('Hardware_Toeslag_Id_0') != ''){ 
                CS.setAttributeValue('Hardware_Toeslag_Id_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC1_error_0') != ''){ 
                CS.setAttributeValue('EVC1_error_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC2_error_0') != ''){ 
                CS.setAttributeValue('EVC2_error_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC1_set_0') != ''){ 
                CS.setAttributeValue('EVC1_set_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC2_set_0') != ''){ 
                CS.setAttributeValue('EVC2_set_0',''); 
            } 
    
            //clear site check instead of removing RP's 
            if(CS.getAttributeValue('Site_Check_0') != ''){ 
            console.log('>>>> clearing Site Check selection');  
            CS.Util.SelectListLookup.clear('Site_Check_0'); 
            } 
        } 
    
        if(payload.wrapper.reference == 'Infra_SLA_0' && payload.isValueUpdate && payload.oldValue != ''){ 
            clearAccessConfigurationProductsOnly(); 
            clearBundleConfigurationProductsOnly(); 
        } 
    
            //depends on parent or child context 
        if(payload.wrapper.reference == 'EVC_PVC_dep_0' && payload.isValueUpdate){ 
        
            if(CS.Service.config["Access_Infrastructure_0"].relatedProducts.length > 0){ 
            _.each(CS.Service.config["Access_Infrastructure_0"].relatedProducts,function(p){ 
            CS.Service.removeRelatedProduct('Access_Infrastructure_0'); 
            }); 
            } 
            
            if(CS.getAttributeValue('Access_set_0') !== ''){ 
            CS.setAttributeValue('Access_set_0',''); 
            } 
            
            if(CS.getAttributeValue('EVC_done_0') !== ''){ 
            CS.setAttributeValue('EVC_done_0',''); 
            } 
        } 
        
        if((payload.wrapper.reference == 'Bundle_Products_0' && payload.properties.value !== undefined && payload.properties.value !== '' && payload.isValueUpdate) 
            || (payload.wrapper.reference == 'Bundle_Products_0:Bundle_lookup_0' && payload.isValueUpdate)){
            if(CS.Service.config["Bundle_Products_0"].relatedProducts.length > 0){ 
                for(var i = 0; i < CS.Service.config["Bundle_Products_0"].relatedProducts.length; i++){
                    var hirarchyLevel = CS.getAttributeValue('Bundle_Products_'+ i +':HirarchyLevel_0');
                    
                    if(hirarchyLevel != 'Parent'){
                        CS.Service.removeRelatedProduct('Bundle_Products_'+ i); 
                        i--;
                    }
                }
                
                CS.setAttributeValue('Bundle_Products_0:BundlePGAset_0','',true);
            } 
        }         

    });
    
    EventHandler.subscribe(EventHandler.Event.CONFIGURATOR_READY, function(payload) { 
        console.log('CONFIGURATOR UI CONFIGURATOR_READY catched!'); 
        CS.EAPI.hideAllAdditionButtons(); 
        showHideModularSections(); 
        checkModeAndSet(); 
        
        CS.Rules.evaluateAllRules();
        
        setTimeout(function(){ renderLookupsForAccessInfra();}, 0); 
        
    }); 
    
    EventHandler.subscribe(EventHandler.Event.SCREEN_LIST_POPULATED, function(payload) { 
        console.log('CONFIGURATOR UI SCREEN_LIST_POPULATED catched!'); 
        CS.EAPI.hideAllAdditionButtons(); 
        showHideModularSections(); 
        checkModeAndSet(); 
        setTimeout(function(){ renderLookupsForAccessInfra();}, 0); 
        var x = '<script type=\"text\/html\" id=\"CS.MultipleRelatedProduct__tpl\">\r\n\t<%\tvar prefix = CS.Util.configuratorPrefix;\r\n\t\tvar max = definition[prefix + \'Max__c\'];\r\n\t\tvar inlineEdit = definition[\'Inline_Edit__c\'];\r\n\t\tvar disabled = ((max && relatedProducts.length >= max) ? \'disabled=\"disabled\"\' : \'\');\r\n\t\tvar prod;\r\n\t\tvar attRef;\r\n\t\tvar rowClass;\r\n\t\tvar errorClass;\r\n\t\tvar errorMessage;\r\n\t\tvar attr = anchor.attr;\r\n relatedProductsIds = new Array( );\r\n\t\tvar isReadOnly = attr[prefix + \'Is_Read_Only__c\'];\r\n\t\tvar isActive = attr[prefix + \'is_active__c\'];\r\n\t\tvar isRequired = attr[prefix + \'Is_Required__c\'];\r\n\t\tif (max && relatedProducts.length > max) {\r\n\t\t\terrorClass = \'attributeError\';\r\n\t\t\terrorMessage = \'<p class=\"attributeErrorMessage\">{!$Label.cscfga__attrcls_The_maximum_number_of} \' + max + \' {!$Label.cscfga__pchlpcls_exceeded_related_items}<\/p>\';\r\n\t\t}\r\n\t%>\r\n\t\t<div class=\"apexp\">\r\n\t\t\t<div class=\"individualPalette\">\r\n\t\t\t\t<div class=\"Custom24Block <%= errorClass %>\">\r\n\t\t\t\t\t<div class=\"bPageBlock brandSecondaryBrd apexDefaultPageBlock secondaryPalette\">\r\n\t\t\t\t\t\t<div class=\"pbHeader\">\r\n\t\t\t\t\t\t\t<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\r\n\t\t\t\t\t\t\t\t<tr>\r\n\t\t\t\t\t\t\t\t\t<td class=\"pbTitle\" data-role=\"fieldcontain\">\r\n\t\t\t\t\t\t\t\t\t\t<h2 class=\"mainTitle\">\r\n\t\t\t\t\t\t\t\t\t\t\t<span><%= definition.Name %><\/span>\r\n\t\t\t\t\t\t\t\t\t\t\t<% if (isRequired) { %><span class=\"required\">(Required)<\/span><% } %>\r\n\t\t\t\t\t\t\t\t\t\t\t<% if (isReadOnly) { %><span class=\"readOnly\">(Read Only)<\/span><% } %>\r\n\t\t\t\t\t\t\t\t\t\t<\/h2>\r\n\t\t\t\t\t\t\t\t\t<\/td>\r\n\t\t\t\t\t\t\t\t\t<td class=\"pbButton \">\r\n\t\t\t\t\t\t\t\t\t<% if (isActive && ! isReadOnly) { %>\r\n\t\t\t\t\t\t\t\t\t\t<% \r\n\t\t\t\t\t\t\t\t\t\t\tconsole.log(inlineEdit);\r\n\t\t\t\t\t\t\t\t\t\t\tif (inlineEdit == true) {\r\n\t\t\t\t\t\t\t\t\t\t%>\r\n\t\t\t\t\t\t\t\t\t\t\t<button class= \"btn btn-primary\" <%= disabled %> data-cs-control=\"<%= anchor.reference %>\" data-cs-ref=\"<%= anchor.reference %>\" onclick=\"CS.InlineEdit.addRelatedProduct(\'<%= anchor.reference %>\', \'<%= anchor.definitionId %>\')\" data-cs-type=\"add\" data-role=\"none\">New <%= definition.Name %><\/button>\r\n\t\t\t\t\t\t\t\t\t\t<%\r\n\t\t\t\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t\t\t\t\telse {\r\n\t\t\t\t\t\t\t\t\t\t%>\r\n\t\t\t\t\t\t\t\t\t\t\t<button <%= disabled %> class=\"add\" data-cs-control=\"<%= anchor.reference %>\" data-cs-ref=\"<%= anchor.reference %>\" data-cs-action=\"addRelatedProduct\" data-cs-type=\"add\" data-role=\"none\">New <%= (definition[prefix + \'Label__c\']) ? definition[prefix + \'Label__c\'] : definition.Name %><\/button>\r\n\t\t\t\t\t\t\t\t\t\t<%\r\n\t\t\t\t\t\t\t\t\t\t\t}\r\n\t\t\t\t\t\t\t\t\t\t%>\r\n\t\t\t\t\t\t\t\t\t<% } %>\r\n\t\t\t\t\t\t\t\t\t<\/td>\r\n\t\t\t\t\t\t\t\t<\/tr>\r\n\t\t\t\t\t\t\t<\/table>\r\n\t\t\t\t\t\t<\/div>\r\n\t\t\t\t\t\t<div class=\"pbBody\">\r\n\t<% if (relatedProducts.length > 0) { %>\r\n\t\t<table class=\"list\" data-cs-binding=\"<%= anchor.reference %>\" data-cs-control=\"<%= anchor.reference %>\" data-cs-type=\"list\">\r\n\t\t\t<thead class=\"rich-table-thead\">\r\n\t\t\t\t<tr class=\"headerRow\">\r\n\t\t\t\t\t<th class=\"headerRow\" style=\"width: 9em;\">Action<\/th>\r\n\t\t\t\t\t<th class=\"headerRow\" style=\"width: 6em;\">Status<\/th>\r\n\t\t\t\t\t<th class=\"headerRow\">Name<\/th>\r\n\t<%\tfor (var i = 0; i < cols.length; i++) {\r\n\t\t\tvar spec = colSpecs[cols[i]]; %>\r\n\t\t\t\t<th class=\"headerRow\">\r\n\t\t\t\t\t<%= spec.header %>\r\n\t\t\t\t<\/th>\r\n\t<%\t} %>\r\n\t\t\t\t<\/tr>\r\n\t\t\t<\/thead>\r\n\t<%\tfor (var i = 0; i < relatedProducts.length; i++) {\r\n\t\t\tprod = relatedProducts[i];\r\n\t\t\trowClass = \'dataRow \' + (i\/2 == Math.floor(i\/2) ? \'even \' : \'odd \') + (i == 0 ? \'first \' : \'\') + (i >= relatedProducts.length - 1 ? \'last\' : \'\');\r\n\t%>\r\n\t\t\t<tr class=\"<%= rowClass %>\" data-cs-ref=\"<%= prod.reference %>\">\r\n\t\t\t\t<td class=\"dataCell\">\r\n\t\t\t\t<% if (isActive && ! isReadOnly) { %>\r\n\t\t\t\t\t<span data-cs-action=\"editRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\">Edit<\/span> |\r\n\t\t\t\t\t<span data-cs-action=\"removeRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\">Del<\/span>\r\n\t\t\t\t\t<% if (disabled === \'\') { %>\r\n\t\t\t\t\t\t| <span data-cs-action=\"copyRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\">Copy<\/span>\r\n\t\t\t\t\t<% } %>\r\n\t\t\t\t<% } %>\r\n\t\t\t\t<\/td>\r\n\t\t\t\t<td class=\"dataCell\"><%= prod.config[prefix + \'Configuration_Status__c\'] %><\/td>\r\n\t\t\t\t<td class=\"dataCell\"><span data-cs-action=\"editRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\"><%= prod.config.Name %><\/span><\/td>\r\n\t<%\t\t\tvar prefix = CS.Util.configuratorPrefix;\r\n\t\t\t\tfor (var j = 0; j < cols.length; j++) {\r\n\t\t\t\tif (colSpecs[cols[j]].ref != undefined) {\r\n\t\t\t\t\tattRef = prod.reference + \':\' + colSpecs[cols[j]].ref;\r\n\t\t\t\t}\r\n\t\t\t\tif (attRef != undefined) {\r\n\t\t\t\t\tvar displayInfo = CS.Service.config[attRef][\'displayInfo\'];\r\n var definition = CS.Service.config[attRef].definition;\r\n console.log(displayInfo);\r\n\t\t\t\t\tif (\'User Input\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <td><input class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" type=\"text\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\"><\/input><\/td>\r\n <%\r\n } else if (\'Date\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <td><input class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" data-role=\"none\" \r\n\t\t\t\t\tonfocus=\"DatePicker.pickDate(true, \'<%=attRef%>\', false );\" size=\"12\" \/><\/input>\r\n\t\t\t\t\t <span class=\"dateFormat\">\r\n\t\t\t\t\t [&nbsp;<a href=\"\" class=\"todayPicker\" onclick=\"return (function(){ DatePicker.insertDate(CS.todayFormatted, \'<%=attRef%>\', true); return false; })();\"><%=CS.todayFormatted%><\/a>&nbsp;]\r\n\t\t\t\t <\/span><\/td>\r\n <%\r\n\t\t\t\t\t} else if (\'Checkbox\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <% if (CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\') {\r\n %>\r\n <td><input type=\"checkbox\" class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" checked=\"true\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" ><\/input><\/td>\r\n <%\r\n } else {\r\n %>\r\n <td><input type=\"checkbox\" class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" ><\/input><\/td>\r\n <% \r\n } %>\r\n <%\r\n\t\t\t\t\t \r\n\t\t\t\t\t} else if (\'Lookup\' === displayInfo && !definition[prefix + \'Select_List_Lookup__c\'] && inlineEdit){ \r\n\r\n lookups = true;\r\n %>\r\n <td>\r\n <span class=\"lookupInput\">\r\n <input type=\"text\"\r\n id=\"<%=attRef%>\"\r\n name=\"<%=attRef%>\"\r\n data-cs-binding=\"<%=attRef%>\"\r\n data-cs-select-list-lookup=\"false\"\r\n data-role=\"none\"\r\n data-mini=\"true\"\r\n value=\"\"\r\n size=\"20\"\r\n onchange=\"CS.InlineEdit.updateAttribute(this)\" \/>\r\n <\/span>\r\n <\/td>\r\n <%\r\n } else if (\'Lookup\' === displayInfo && definition[prefix + \'Select_List_Lookup__c\'] && inlineEdit){ \r\n\r\n lookups = true;\r\n %>\r\n <td>\r\n <div class=\"form-control form-control-select-lookup\">\r\n <span class=\"lookupInput\">\r\n <input style=\"border: none;\" type=\"text\"\r\n id=\"<%=attRef%>\"\r\n name=\"<%=attRef%>\"\r\n data-cs-binding=\"<%=attRef%>\"\r\n data-cs-select-list-lookup=\"true\"\r\n data-role=\"none\"\r\n data-mini=\"true\"\r\n value=\"\"\r\n size=\"20\"\r\n onchange=\"CS.InlineEdit.updateAttribute(this)\" \/>\r\n <\/span>\r\n <\/div>\r\n <\/td>\r\n <% \r\n if (relatedProductsIds.indexOf(attRef) == -1) {\r\n relatedProductsIds.push(attRef);\r\n }\r\n }\r\n else if (\'Select List\' === displayInfo && inlineEdit) {\r\n %>\r\n <td>\r\n <select class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" onchange=\"CS.InlineEdit.updateAttribute(this)\">\r\n <%\r\n var options = CS.Service.config[attRef][\'selectOptions\'];\r\n for (var k = 0; k < options.length; k++) {\r\n var selected = (options[k][\'Name\'] === CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\']);\r\n if (selected) {\r\n %>\r\n <option value=\"<%= options[k][\'cscfga__Value__c\'] %>\" selected=\"selected\"><%= options[k][\'Name\'] %><\/option>\r\n <%\r\n } else {\r\n %> \r\n <option value=\"<%= options[k][\'cscfga__Value__c\'] %>\"><%= options[k][\'Name\'] %><\/option>\r\n\t<%\t\t }\r\n }\r\n %> \r\n <\/select>\r\n <\/td>\r\n <%\r\n }\r\n else if (attRef.indexOf(\'Additional_Attributes\') != -1) {\r\n \r\n\t\t\t\t\t\tvar addAtts = CS.getAttributeValue(attRef);\r\n\t\t\t\t\t\tif (addAtts != \'\') {\r\n\t%>\r\n \t\t\t\t\t\t<td><table><tr>\r\n\t<%\r\n\t\t\t\t\t\t\taddAtts = addAtts.split(\',\');\r\n\t\t\t\t\t\t\tfor (var q = 0; q < addAtts.length; q++) {\r\n\t%>\r\n\t\t\t\t\t\t\t\t<th><%= addAtts[q] %><\/th>\r\n\t<%\r\n\t\t\t\t\t\t\t}\r\n\t%>\r\n\t\t\t\t\t\t\t<\/tr>\r\n\t\t\t\t\t\t\t<tr>\r\n\t<%\r\n\t\t\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\tfor (var q = 0; q < addAtts.length; q++) {\r\n\t\t\t\t\t\t\t\tvar addAttRef = prod.reference + \':\' + addAtts[q].split(\' \').join(\'_\') + \'_0\';\r\n\r\n\t%>\r\n\t<%\r\nvar displayInfo = CS.Service.config[addAttRef][\'displayInfo\'];\r\n var definition = CS.Service.config[addAttRef].definition;\r\n console.log(displayInfo);\r\n\t\t\t\t\tif (\'User Input\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <td><input class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" type=\"text\" value=\"<%= CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\"><\/input><\/td>\r\n <%\r\n } else if (\'Date\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <td><input class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" value=\"<%= CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" data-role=\"none\" \r\n\t\t\t\t\tonfocus=\"DatePicker.pickDate(true, \'<%=addAttRef%>\', false );\" size=\"12\" \/><\/input>\r\n\t\t\t\t\t <span class=\"dateFormat\">\r\n\t\t\t\t\t [&nbsp;<a href=\"\" class=\"todayPicker\" onclick=\"return (function(){ DatePicker.insertDate(CS.todayFormatted, \'<%=addAttRef%>\', true); return false; })();\"><%=CS.todayFormatted%><\/a>&nbsp;]\r\n\t\t\t\t <\/span><\/td>\r\n <%\r\n\t\t\t\t\t} else if (\'Checkbox\' === displayInfo && inlineEdit) {\r\n\t%>\r\n <% if (CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\') {\r\n %>\r\n <td><input type=\"checkbox\" class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" checked=\"true\" value=\"<%= CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" ><\/input><\/td>\r\n <%\r\n } else {\r\n %>\r\n <td><input type=\"checkbox\" class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" value=\"<%= CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\" onchange=\"CS.InlineEdit.updateAttribute(this)\" ><\/input><\/td>\r\n <% \r\n } %>\r\n <% \r\n }\r\n else if (\'Select List\' === displayInfo && inlineEdit) {\r\n %>\r\n <td>\r\n <select class=\"form-control\" data-cs-binding=\"<%=addAttRef%>\" id=\"<%=addAttRef%>\" name=\"<%=addAttRef%>\" onchange=\"CS.InlineEdit.updateAttribute(this)\">\r\n <%\r\n var options = CS.Service.config[addAttRef][\'selectOptions\'];\r\n for (var k = 0; k < options.length; k++) {\r\n var selected = (options[k][\'Name\'] === CS.Service.config[addAttRef][\'attr\'][\'cscfga__Display_Value__c\']);\r\n if (selected) {\r\n %>\r\n <option value=\"<%= options[k][\'cscfga__Value__c\'] %>\" selected=\"selected\"><%= options[k][\'Name\'] %><\/option>\r\n <%\r\n } else {\r\n %> \r\n <option value=\"<%= options[k][\'cscfga__Value__c\'] %>\"><%= options[k][\'Name\'] %><\/option>\r\n\t<%\t\t }\r\n }\r\n %> \r\n <\/select>\r\n <\/td>\r\n <%\r\n }\r\n %> \r\n\t\r\n\t<%\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t}\r\n\t%>\r\n\t\t\t\t\t\t\t<\/tr><\/table><\/td>\r\n <%\r\n\t\t\t\t\t\t} \r\n\t\r\n }\r\n else {\r\n %>\r\n <td><%= CS.getAttributeDisplayValue(attRef) %><\/td>\r\n <%\r\n }\r\n }\r\n }\r\n %>\r\n\t\t\t<\/tr>\r\n\t<% } %>\r\n\t\t<\/table>\r\n\t<% } else { %>\r\n\t\t<table class=\"list\" data-cs-control=\"<%= anchor.reference %>\" data-cs-type=\"list\">\r\n\t\t\t<tr class=\"dataRow even first last\">\r\n\t\t\t\t<td class=\"dataCell\">\r\n\t\t\t\t\tNo items to display\r\n\t\t\t\t<\/td>\r\n\t\t\t<\/tr>\r\n\t\t<\/table>\r\n\t<% } %>\r\n\t\t\t\t\t<\/div>\r\n\t\t\t\t\t\t<div class=\"pbFooter secondaryPalette\">\r\n\t\t\t\t\t\t\t<div class=\"bg\"><\/div>\r\n\t\t\t\t\t\t<\/div>\r\n\t\t\t\t\t<\/div>\r\n\t\t\t\t<%= errorMessage %>\r\n\t\t\t\t<\/div>\r\n\t\t\t<\/div>\r\n\t\t<\/div>\r\n\t<\/script>'; 
        CS.App.Components.Repository.addComponent('configurator', 'MultipleRelatedProductLE', x); 
    }); 
     }
     
     }

    window.clfvAfterAttributeUpdate = function clfvAfterAttributeUpdate(payload){
        if(payload.wrapper.reference == 'DataStorageHidden_0' && payload.isValueUpdate){ 
                renderAlways(); 
        } 
    } 
    
    window.clfvScreenListPopulated = function clfvScreenListPopulated(payload){
        renderAlways();
    }
    
    window.CLFVEvents = function CLFVEvents(){
        if(eventSubscribed === false){
            eventSubscribed = true;
            EventHandler.subscribe(EventHandler.Event.AFTER_ATTRIBUTE_UPDATE, function(payload) {
                if(payload.wrapper.reference == 'DataStorageHidden_0' && payload.isValueUpdate){ 
                    renderAlways(); 
                } 

            });
            
            EventHandler.subscribe(EventHandler.Event.SCREEN_LIST_POPULATED, function(payload) { 
                renderAlways(); 
            });
        }
     }
     
     window.PriceItemBundlesEvents = function PriceItemBundlesEvents(){
         /*if(eventSubscribed === false){
            eventSubscribed = true;
            EventHandler.subscribe(EventHandler.Event.AFTER_ATTRIBUTE_UPDATE, function(payload) {
                //Parent Bundle changed - delete all Related price items and add new ones
                 if(payload.wrapper.reference == 'Bundle_Products_0:Bundle_lookup_0' && payload.isValueUpdate){ 
                debugger;
                    if(CS.Service.config["Bundle_Products_0"].relatedProducts.length > 0){ 
                        for(var i = 0; i < CS.Service.config["Bundle_Products_0"].relatedProducts.length; i++){
                            var hirarchyLevel = CS.getAttributeValue('Bundle_Products_'+ i +':HirarchyLevel_0');
                            
                            if(hirarchyLevel != 'Parent'){
                                CS.Service.removeRelatedProduct('Bundle_Products_'+ i); 
                                i--;
                            }
                        }
                        
                        CS.setAttributeValue('Bundle_Products_0:BundlePGAset_0','',false);
                    } 
                } 

            });
            

        }*/
     }
     
    window.mobileCTNProfileScreenListPopulated = function mobileCTNProfileScreenListPopulated(payload){
        renderCustomScreen();
        applySkillBasedLogic();
    }     
//afterConfiguratorReady
});


//a