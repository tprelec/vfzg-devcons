/**
 * Plugin for common functions being used by multiple products in Solution Console
 * The main purpose is to define the following functions:
 *  - Functions that are executing general logic (e.g. calculations), which is not attribute specific,
 *  - Functions that are attribute specific, but common for multiple products.
 *    This is valid for attributes that have same behaviour accros different products.
 *
 * NOTE: Must be invoked after SC_Constants and before all other product plugins!
 */

console.log('Loaded Common Functions plugin');

let basketDetails;
let serviceInfo;

// Add On Attributes
const ADDON_REF_ATTRIBUTE = 'AddOnReference';
const ADDON_GEN_ATTRIBUTE = 'AddOnGenerator';

// Map between Solution Main Component attributes and Basket fields
const SOLUTION_ATTRIBUTE_FIELD_MAPPING = {
	[SC_COMMON_ATTRIBUTES.SALES_CHANNEL]: 'opportunityDirectIndirect',
	[SC_COMMON_ATTRIBUTES.ACCOUNT_ID]: 'accountId'
};

// Register logic to be executed when Solution is set to active
if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionSetActive', initDefaults);
}

async function fetchBasketInformation() {
	return new Promise(async function (resolve, reject) {
		setInterval(async function () {
			const b = await CS.SM.getActiveBasket();
			if (b) {
				resolve(b);
			}
		}, 500);
	});
}

async function initDefaults() {
	const solution = await CS.SM.getActiveSolution();
	if (!solution) {
		return;
	}

	switch (solution.name) {
		case SC_SOLUTIONS.ONEMOBILE_SOLUTION:
			return initOneMobileSolutionDefaults(solution);
		case SC_SOLUTIONS.BUSINESSINTERNET_SOLUTION:
			return initBusinessInternetSolutionDefaults(solution);
		case SC_SOLUTIONS.INFRASTRUCTURE_SOLUTION:
			return initInfrastructureSolutionDefaults(solution);
		case SC_SOLUTIONS.MANAGEDSWITCH_SOLUTION:
			return initManagedSwitchSolutionDefaults(solution);
		case SC_SOLUTIONS.BUSINESSINTERNETMODEMONLY_SOLUTION:
			return initBusinessInternetModemOnlySolutionDefaults(solution);
		case SC_SOLUTIONS.BUSINESS_MOBILE_SOLUTION:
			return initBusinessMobileSolutionDefaults(solution);
		default:
			console.log('The defaults are not specified for ' + solution.name);
	}
}

/**
 * @description Set the current system date on main Solution component
 */
async function setCurrentSystemDate(solution) {
	const mainSolutionPC = getMainConfiguration(solution);
	const dateAttribute = [
		{
			name: SC_COMMON_ATTRIBUTES.TODAY,
			value: getCurrentISODateFormat()
		}
	];

	return updateConfigurationAttribute(solution, mainSolutionPC.guid, dateAttribute);
}

/**
 * @description Get configuration from main Solution component
 */
function getMainConfiguration(solution) {
	return solution.getConfigurations()[0];
}

/**
 * @description Prevent save of Solution with error; Invoked from all Solution plugins!
 */
async function beforeSolutionSave(solution, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
	let isDuplicateSolution = false;

	switch (solution.name) {
		case SC_SOLUTIONS.INFRASTRUCTURE_SOLUTION:
			let result = await infrastructureValidations(solution, configurationsProcessed, configurationGuids);
			if (result === false) {
				return false;
			}

		case SC_SOLUTIONS.BUSINESS_MOBILE_SOLUTION:
			//let validationResult = await businessMobileValidations(solution, configurationsProcessed, configurationGuids);
			let validationResult = await businessMobileValidations(solution);
			if (!validationResult) {
				return false;
			}

		case SC_SOLUTIONS.ONEMOBILE_SOLUTION:
		case SC_SOLUTIONS.BUSINESSINTERNETMODEMONLY_SOLUTION:
			isDuplicateSolution = await checkIfduplicateSolution(solution);
			if (isDuplicateSolution) {
				return false;
			}
		default:
	}

	const solutionValidForSave = await checkForSolutionConfigurations(solution);
	if (!solutionValidForSave) {
		return false;
	}
	return !solution.error;
}

async function validateConfiguration(configuration, valid, messageText) {
	configuration.updateStatus({ status: valid, message: messageText });
}

async function checkForSolutionConfigurations(solution) {
	var erroneousConfiguration = false;
	const configurations = solution.getAllConfigurations();
	Object.values(configurations).forEach(function (configuration) {
		if (!configuration.status) {
			erroneousConfiguration = true;
		}
	});
	if (erroneousConfiguration) {
		return false;
	}
	return Object.values(configurations).length > 1;
}

/**
 * @description Propagate requested attribute value to all configurations belonging to the requested Component
 */
async function propagateAttributeToComponent(componentName, attributeName, attributeValue) {
	const solution = await CS.SM.getActiveSolution();
	const component = solution.getComponentByName(componentName);
	if (!component) {
		return;
	}

	return Promise.all(
		Object.values(component.getConfigurations()).map((configuration) => setAttribute(component, configuration, attributeName, attributeValue))
	);
}

/**
 * @description Set requested attribute
 */
async function setAttribute(component, configuration, attributeName, attributeValue) {
	const attributeData = [
		{
			name: attributeName,
			value: attributeValue
		}
	];

	return updateConfigurationAttribute(component, configuration.guid, attributeData);
}

/**
 * @description Invokes package updateConfigurationAttribute function and initiates changes,
 *              only if current and new values are different
 * @param component Component that configuration belongs to
 * @param configurationGuid GUID of configuration which attributes should be updated
 * @param attributeList List of attributes that should be updated
 */
async function updateConfigurationAttribute(component, configurationGuid, attributeList) {
	const configuration = component.getConfiguration(configurationGuid);

	const newAttributeList = [];
	attributeList.forEach((attribute) => {
		const configAttribute = getAttribute(configuration, attribute.name);
		// TODO: Double-check validating attribute type (!= instead of !==)
		if (configAttribute && configAttribute.value !== attribute.value) {
			newAttributeList.push(attribute);
		}
	});

	if (newAttributeList && newAttributeList.length) {
		return component.updateConfigurationAttribute(configuration.guid, newAttributeList);
	}
}

async function beforeAddOnDataInitialized(aomData, component, configuration, priceItemAttribute) {
	return sortAddOns(aomData);
}

/**
 * @description Sorts AddOns by Comm. Prod. Add On Assoc. field Sequence ASC
 */
function sortAddOns(aomData) {
	if (Object.values(aomData.addOnData.data) && Object.values(aomData.addOnData.data).length) {
		Object.values(aomData.addOnData.data)[0].sort((a, b) => {
			return a.cspmb__Sequence__c - b.cspmb__Sequence__c;
		});
	}
	return aomData;
}

/**
 * @description Get current date in ISO format
 */
function getCurrentISODateFormat() {
	const todayDate = new Date().toISOString().slice(0, 10);
	return todayDate;
}

/**
 * Functions with backend interaction
 */
async function getBasketInfo() {
	const basket = await CS.SM.getActiveBasket();
	const inputMapObject = {
		method: SC_REMOTE_CLASS_METHOD.PRODUCT_BASKET_DETAILS,
		productBasketId: basket.basketId
	};

	basketDetails = await basket.performRemoteAction(SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS, inputMapObject);
	basketDetails.configurations = JSON.parse(basketDetails.configurations);
	return basketDetails;
}

/**
 * @description Set Sales Channel attribute on Main Solution Component, originally fetched from the Basket and Opportunity fields
 */
async function updateFromBasket(solution, attributeNames) {
	//console.log('***** updateFromBasket');
	let basketData;

	if (basketDetails === null || basketDetails === undefined) {
		basketData = await getBasketInfo();
		makeInflightConfigsReadonly();
		readOnlyAtribbuteInflightBasket(SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT, SC_COMMON_ATTRIBUTES.SITE_AVAILABILITY);
	}
	const mainSolutionPC = getMainConfiguration(solution);
	let attributeData = [];

	attributeNames.forEach((attName) => {
		const attValue = mainSolutionPC.getAttribute(attName).value;
		const basketFieldValue = basketData[SOLUTION_ATTRIBUTE_FIELD_MAPPING[attName]];
		//console.log('***** basketFieldValue' + basketFieldValue);

		if (attValue === '' && basketFieldValue != null) {
			attributeData.push({
				name: attName,
				value: basketFieldValue
			});
		}
	});

	return updateConfigurationAttribute(solution, mainSolutionPC.guid, attributeData);
}

async function setConfigIdAttribute(component, configuration) {
	const attributeData = [
		{
			name: SC_COMMON_ATTRIBUTES.CONFIG_ID,
			value: configuration.id
		}
	];

	return component.updateConfigurationAttribute(configuration.guid, attributeData);
}

async function infrastructureValidations(solution, configurationsProcessed, configurationGuids) {
	let hasError = await checkBandwidthOnSiteConnect();
	hasError = hasError || (await sdwanFinalValidation());
	return !hasError;
}

function isBasketInflight() {
	return (
		basketDetails.basketDetails.csordtelcoa__in_flight_change_type__c != null &&
		basketDetails.basketDetails.csordtelcoa__in_flight_change_type__c !== undefined &&
		basketDetails.basketDetails.csordtelcoa__in_flight_change_type__c === SC_BASKET_FIELD_VALUE.BASKET_INFLIGHT_CHANGE_TYPE_SELECTIVE
	);
}

async function makeInflightConfigsReadonly() {
	const solution = await CS.SM.getActiveSolution();
	if (isBasketInflight()) {
		// this is Inflight basket, configurations which are not applicable for Inflight should be "read only"
		for (const [key, value] of Object.entries(basketDetails.configurations)) {
			if (!value.inflightApplicable) {
				let cfg = solution.getConfiguration(key);
				makeConfigurationReadOnly(cfg);
				cfg.inflightApplicable = false;
			}
		}
	}
}

/**
 * @description Marks all configuration attributes readOnly = true
 */
function makeConfigurationReadOnly(configuration) {
	const readOnlySetting = { readOnly: true };

	for (const [key, value] of Object.entries(configuration.attributes)) {
		if (value.showInUi) {
			configuration.updateAttribute(value.name, readOnlySetting);
			configuration.inflightApplicable = false;
		}
	}
}

/**
 * @description Checks whether configuration can be deleted in case of Inflight basket
 */
function canAddOrDeleteInflightConfiguration(configuration, action) {
	// if this is Inflight basket, configurations which are not applicable for Inflight cannot be deleted
	// also, addons cannot be added to configurations which are not applicable for Inflight
	if (isBasketInflight()) {
		if (basketDetails.configurations[configuration.guid]) {
			if (!basketDetails.configurations[configuration.guid].inflightApplicable) {
				if (action == SC_ACTION.DELETE) {
					CS.SM.displayMessage('Cannot delete this configuration from Inflight basket (not Inflight Applicable)', 'error');
				} else if (action == SC_ACTION.ADD) {
					CS.SM.displayMessage('Cannot add this configuration to Inflight basket (not Inflight Applicable)', 'error');
				}
				return false;
			}
		}
	}
	return true;
}

/**
 * @description Checks whether configuration can be deleted in case of Macd basket
 */
async function canAddOrDeleteMacdConfiguration(configuration, action) {
	// if this is MACD basket, configurations which don't have their respective services Active cannot be deleted
	// also, addons cannot be added to configurations if they don't have their respective services Active
	const basket = await CS.SM.getActiveBasket();
	if (basket.basketChangeType == SC_BASKET_FIELD_VALUE.BASKET_CHANGE_TYPE_CHANGE_REQUEST) {
		for (let i = 0; i < serviceInfo.length; i++) {
			if (serviceInfo[i].configurationGuid == configuration.guid) {
				if (!serviceInfo[i].serviceActive) {
					if (action == SC_ACTION.DELETE) {
						CS.SM.displayMessage('Cannot delete this configuration from MACD basket (inactive service)', 'error');
					} else if (action == SC_ACTION.ADD) {
						CS.SM.displayMessage('Cannot add this configuration to MACD basket (inactive service)', 'error');
					}
					return false;
				}
				break;
			}
		}
		return true;
	}
	return true;
}

async function readOnlyAtribbuteInflightBasket(componentName, attributeName) {
	if (isBasketInflight()) {
		let activeSolution = await CS.SM.getActiveSolution();
		const components = activeSolution.components;
		Object.values(components).forEach((component) => {
			if (component.name === componentName) {
				const configurations = component.getConfigurations();
				Object.values(configurations).forEach((configuration) => {
					if (configuration.isFromMacBasket) {
						const updateData = { readOnly: true };
						configuration = configuration.updateAttribute(attributeName, updateData);
					}
				});
			}
		});
	}
}

async function incrementMacdContractNumber(configurationGuid) {
	const basket = await CS.SM.getActiveBasket();
	let configs = await SC_Util.generateFlatConfigurationListAsync();

	if (!configs[configurationGuid]) {
		console.log('There is no configuration with GUID: ' + configurationGuid);
		return;
	}

	// take configurationId for "root" configs or parentConfigurationId for addon configs
	let configurationId;
	if (configs[configurationGuid].value.id) {
		configurationId = configs[configurationGuid].value.id;
	} else {
		configurationId = configs[configs[configurationGuid].value.parentConfiguration].value.id;
	}

	const inputMapObject = {
		method: SC_REMOTE_CLASS_METHOD.INCREMENT_MACD_CONTRACT_NUMBER,
		basketId: basket.basketId,
		configurationGuid: configurationGuid,
		configurationId: configurationId
	};

	const result = await basket.performRemoteAction(SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS, inputMapObject);
	if (result.status) {
		console.log('incrementMacdContractNumber Successful: ', result.message);
	} else {
		console.error('incrementMacdContractNumber failed', result.message);
	}
}

async function calculateListPrice() {
	const basket = await CS.SM.getActiveBasket();
	const solutions = basket.getSolutions();

	let pcs = [];
	Object.values(solutions).forEach((solution) => {
		Object.values(solution.components).forEach((component) => {
			const configurations = component.getConfigurations();
			Object.values(configurations).forEach((configuration) => {
				let pcTemp = {
					configurationName: configuration.name,
					configurationGuid: configuration.guid
				};
				pcs.push(pcTemp);

				Object.values(configuration.relatedProductList).forEach((relatedProduct) => {
					let pcTemp = {
						configurationName: relatedProduct.configuration.name,
						configurationGuid: relatedProduct.configuration.guid
					};
					pcs.push(pcTemp);
				});
			});
		});
	});

	let activeSolution = await CS.SM.getActiveSolution();
	const configs = activeSolution.getConfigurations()[0];

	let pcTemp = {
		configurationName: configs.name,
		configurationGuid: configs.guid
	};

	pcs.push(pcTemp);
	console.log(pcs);

	const inputMapObject = {
		method: SC_REMOTE_CLASS_METHOD.CALCULATE_LIST_PRICE,
		basketId: basket.basketId,
		pcs: JSON.stringify(pcs)
	};

	const result = await basket.performRemoteAction(SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS, inputMapObject);
}

async function applyAutomaticDiscount() {
	const basket = await CS.SM.getActiveBasket();
	const solutions = basket.getSolutions();
	let pCcPMapLocal = [];
	let pCaddOnMapLocal = [];

	Object.values(solutions).forEach((solution) => {
		Object.values(solution.components).forEach((component) => {
			// Access
			// Business Internet
			// Business Connect
			// Site connect
			if (component.name === 'Access' || component.name === 'Access Modem Only') {
				const configurations = component.getConfigurations();
				Object.values(configurations).forEach((configuration) => {
					if (configuration.id != '') {
						let i = 0;
						// configuration.attributes['productVariant'].value gives an ID
						let cpId = undefined;

						if (configuration.attributes['productvariant'] != undefined) {
							cpId = configuration.attributes['productvariant'].value;
						} else {
							console.error('productvariant attribute could not be found - contact the administrator.');
							return;
						}

						// configuration.attributes['contractDuration'].value gives a contract duration
						let pcId = configuration.id;
						let pCCpTmp = {
							productConfigurationId: pcId,
							commercialProductId: cpId
						};
						pCcPMapLocal.push(pCCpTmp);
					}
				});
			}

			if (component.name === 'Internet Modem Only') {
				const configurations = component.getConfigurations();
				Object.values(configurations).forEach((configuration) => {
					Object.values(configuration.relatedProductList).forEach((relatedProduct) => {
						// in MACD scenario configuration.id can be empty
						// configuration.id is the SFDC Id of addon's parent configuration
						// in that case there is no need for automatic discount because this means there is not change on parent id and consequently on addon
						if (relatedProduct.groupName === 'Helpdesk' && configuration.id != '') {
							let addOnId = undefined;
							let pcId = relatedProduct.configuration.id;
							if (relatedProduct.configuration.attributes.addonreference.value !== undefined) {
								addOnId = relatedProduct.configuration.attributes.addonreference.value;
							} else {
								console.error('productvariant attribute could not be found - contact the administrator.');
								return;
							}
							let pCAddonTmp = {
								parentConfigurationId: configuration.id,
								addOnId: addOnId,
								configurationGuid: relatedProduct.configuration.guid
							};
							pCaddOnMapLocal.push(pCAddonTmp);
						}
					});
				});
			}
		});
	});

	if (pCcPMapLocal.length !== 0) {
		const inputMapObject = {
			method: SC_REMOTE_CLASS_METHOD.APPLY_AUTOMATIC_DISCOUNT,
			basketId: basket.basketId,
			pCcPMap: JSON.stringify(pCcPMapLocal)
		};
		const result = await basket.performRemoteAction(SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS, inputMapObject);
		if (result.status) {
			console.log('Automatic Discount is Successful: ', result.message);
		} else {
			console.error('Automatic discount failed - contact the administrator.');
		}
	}

	if (pCaddOnMapLocal.length !== 0) {
		const inputMapObject = {
			method: SC_REMOTE_CLASS_METHOD.APPLY_AUTOMATIC_DISCOUNT_ADDON,
			basketId: basket.basketId,
			pCaddOnMap: JSON.stringify(pCaddOnMapLocal)
		};
		const result = await basket.performRemoteAction(SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS, inputMapObject);
		if (result.status) {
			console.log('Automatic Discount is Successful: ', result.message);
		} else {
			console.error('Addon automatic discount failed - contact the administrator.');
		}
	}
}

async function canAddSolution(basketId, solutionName) {
	const basket = await CS.SM.getActiveBasket();
	const inputMapObject = {
		method: SC_REMOTE_CLASS_METHOD.CHECK_UNIQUE_SOLUTION_CONSTRAINT,
		basketId: basketId,
		solutionName: solutionName
	};
	const result = await basket.performRemoteAction(SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS, inputMapObject);
	if (result.status) {
		console.log('canAddSolution result: ', result.canAddSolution);
		return result.canAddSolution;
	} else {
		console.error('canAddSolution failed!');
		return false;
	}
}

/**
 * Attribute specific functions
 */
function getAttribute(configuration, attributeName) {
	return configuration.attributes[attributeName.toLowerCase()];
}

function getAttributeValue(configuration, attributeName) {
	return configuration.attributes[attributeName.toLowerCase()].value;
}

function getAttributeDisplayValue(configuration, attributeName) {
	return configuration.attributes[attributeName.toLowerCase()].displayValue;
}

function getAttributeVisibility(configuration, attributeName) {
	return configuration.attributes[attributeName.toLowerCase()].showInUi;
}

function getAttributeRequired(configuration, attributeName) {
	return configuration.attributes[attributeName.toLowerCase()].required;
}

function showProgressOverlay() {
	const event = new CustomEvent('SaveSolutionStart', {
		detail: { message: 'Working...' }
	});
	window.document.dispatchEvent(event);
}

function hideProgressOverlay() {
	const event = new CustomEvent('SaveSolutionEnd', { detail: {} });
	window.document.dispatchEvent(event);
}

/**
 * @description Check whether there are more same solutions per basket. Returns true if there are more duplicate solutions.
 */
async function checkIfduplicateSolution(mainSolution) {
	let existingSolutionsNames = new Array();
	const basket = await CS.SM.getActiveBasket();

	Object.keys(basket.solutions).forEach((key) => {
		if (basket.solutions[key].name === mainSolution.name) {
			existingSolutionsNames.push(basket.solutions[key].name);
		}
	});

	//if the array has more than one member throw a message
	if (existingSolutionsNames.length > 1) {
		let errorMsgTxt = 'There should be only one ' + mainSolution.name + '!';
		CS.SM.displayMessage(errorMsgTxt, 'error');
		let mainConfiguration = mainSolution.getConfigurations();
		mainConfiguration[0].updateStatus({ status: false, message: errorMsgTxt });
		return true;
	}
	return false;
}

/**
 * @description Function will set the field to read-only in the MACD scenario if the configuration is from a MAC basket
 */
async function readOnlyAtribbuteIsFromMacBasket(componentName, attributeName) {
	let activeSolution = await CS.SM.getActiveSolution();

	const isMacdSolution = activeSolution.isMACD;
	if (isMacdSolution) {
		const components = activeSolution.components;
		Object.values(components).forEach((component) => {
			if (component.name === componentName) {
				const configurations = component.getConfigurations();
				Object.values(configurations).forEach((configuration) => {
					if (configuration.isFromMacBasket) {
						const updateData = { readOnly: true };
						configuration = configuration.updateAttribute(attributeName, updateData);
					}
				});
			}
		});
	}
}

/**
 * @description Function will set the field to read-only in the MACD scenario on Main component
 */
async function readOnlyAtribbuteIfIsMacdOnMain(attributeName) {
	let activeSolution = await CS.SM.getActiveSolution();
	const isMacdSolution = activeSolution.isMACD;
	if (isMacdSolution) {
		let mainConfiguration = getMainConfiguration(activeSolution);
		const updateData = { readOnly: true };
		return mainConfiguration.updateAttribute(attributeName, updateData);
	}
}

async function getServiceInfo() {
	const basket = await CS.SM.getActiveBasket();

	if (basket.basketChangeType == SC_BASKET_FIELD_VALUE.BASKET_CHANGE_TYPE_CHANGE_REQUEST) {
		let configs = await SC_Util.generateFlatConfigurationListAsync();

		if (serviceInfo == null) {
			let mainConfigurations = [];

			Object.values(configs).forEach((config) => {
				if (config.value.replacedConfigId) {
					const unpacked = SC_Util.unpackFlattConfiguration(config);
					let configInfo = {};
					configInfo.configurationId = unpacked.replacedConfigId;
					configInfo.configurationGuid = unpacked.guid;
					mainConfigurations.push(configInfo);
				}
			});

			const inputMapObject = {
				configurationInput: JSON.stringify(mainConfigurations)
			};
			const result = await basket.performRemoteAction(SC_REMOTE_CLASS_NAME.SERVICE_INFO_REMOTE, inputMapObject);
			if (result.status) {
				console.log('getServiceInfo result: ', result.serviceInfo);
				serviceInfo = JSON.parse(result.serviceInfo);
			} else {
				console.error('getServiceInfo failed!');
			}
		}

		if (serviceInfo != null) {
			serviceInfo.forEach((serviceInfoItem) => {
				if (!serviceInfoItem.serviceActive) {
					let cfg = configs[serviceInfoItem.configurationGuid].value;
					makeConfigurationReadOnly(cfg);
				}
			});
		}
	}
}
