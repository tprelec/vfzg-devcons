console.log("Loaded Gen EVC plugin");

SC_MessageBroker.store.subscribe('root:ContractTerm', async (data) => {
    console.log('root:ContractTerms...');
	await propagateAttributeToComponent("Gen Internet", data.attribute.name, data.attribute.value);
});

const scgetinternet = Object.freeze({

	"afterGenFirstAttributeUpdated": async function afterGenFirstAttributeUpdated(component, configuration, attribute, oldValueMap) {
		
		if (component.name === 'Gen Internet' && attribute.name === 'technology' && attribute.value !== oldValueMap.value) {
			await setAttribute(component, configuration, 'quantity', 100);
		}
	
		return true;
	}
});
