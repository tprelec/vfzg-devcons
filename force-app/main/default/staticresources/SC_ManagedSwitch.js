if (!CS || !CS.SM) {
	throw Error("Solution Console Api not loaded!");
}

// Register Managed Switch Solution plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener(
		"SolutionConsoleReady",
		registerBusinessManagedSwitch
	);
}
console.log("Managed Switch...");
// name of the solution is "Business Managed Switch"

async function registerBusinessManagedSwitch() {
	const msPlugin = await CS.SM.registerPlugin(
		SC_SCHEMA_NAME.MANAGEDSWITCH_SOLUTION
	);
	updateManagedSwitchPlugin(msPlugin);

	console.log("Managed Switch plugin registered!");
	return;
}

function updateManagedSwitchPlugin(Plugin) {
	
	Plugin.beforeSave = beforeSolutionSave;

	Plugin.afterSolutionLoaded = afterSolutionLoaded;

	// Plugin.beforeSolutionDelete = beforeSolutionDelete;

	// Plugin.afterSolutionDelete = afterSolutionDelete;

	//Plugin.afterSave = afterManagedSwitchSolutionSave;

	Plugin.afterAttributeUpdated = afterManagedSwitchAttributeUpdated;

	Plugin.afterConfigurationAdd = afterManagedSwitchConfigurationAdd;

	Plugin.afterConfigurationDelete = afterManagedSwitchConfigurationDelete;
}

// TODO - vidjeti što ide u SC_CommonFunction

// COMMON
async function afterSolutionLoaded(solutionBeforeUpdate, solutionAfterUpdate) {

}

async function afterManagedSwitchConfigurationAdd(component, configuration) {
	// switch (component.name) {
	// 	case SC_COMPONENTS.NETWORKPORTS_COMPONENT:
	//  return someFunctionReturningBool;
	// }

	return true;
}

async function afterManagedSwitchConfigurationDelete(component, configuration) {
	// switch (component.name) {
	// 	case SC_COMPONENTS.NETWORKPORTS_COMPONENT:
	//  return someFunctionReturningBool;
	// }

	return true;
}

// async function beforeSolutionDelete(solution) {
// 	return true;
// }

// async function afterSolutionDelete(solution) {
// 	return true;
// }

/**
 * @description Perform logic required after Solution is saved
 */
async function afterManagedSwitchSolutionSave(
	solutionResult,
	configurationsProcessed,
	saveOnlyAttachment,
	configurationGuids
) {
	// const solution = solutionResult.solution;
	// if (!solution) {
	// 	return;
	// }
}

async function afterManagedSwitchAttributeUpdated(
	component,
	configuration,
	attribute,
	oldValueMap
) {
	console.log("afterManagedSwitchAttributeUpdated...");
	switch (component.name) {
		case SC_COMPONENTS.MANAGEDSWITCH_SOLUTION:
			return afterManagedSwitchSolutionAttributeUpdated(attribute);
		case SC_COMPONENTS.NETWORKPORTS_COMPONENT:
			return afterNetworkPortsAttributeUpdated(component, configuration, attribute);
	}

	return true;
}

async function afterManagedSwitchSolutionAttributeUpdated(attribute) {
	if (attribute.name === SC_COMMON_ATTRIBUTES.TODAY) {
		return Promise.all([
			propagateAttributeToComponent(
				SC_COMPONENTS.NETWORKPORTS_COMPONENT,
				attribute.name,
				attribute.value
			),
		]);
	}

	if (attribute.name === SC_COMMON_ATTRIBUTES.CONTRACT_DURATION) {
		return Promise.all([
			propagateAttributeToComponent(
				SC_COMPONENTS.NETWORKPORTS_COMPONENT,
				attribute.name,
				attribute.value
			)
		]);
	}
}

async function afterNetworkPortsAttributeUpdated(component, configuration, attribute) {
	if (attribute.name === SC_COMMON_ATTRIBUTES.CONTRACT_DURATION) {
        await setAttribute(component, configuration, SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT, "");
	}
}

/**
 *
 * Support functions
 *
 */

// Initialization logic
async function initManagedSwitchSolutionDefaults(solution) {
	console.log("Init ManagedSwitch Solution defaults");
	if (!solution) {
		solution = await CS.SM.getActiveSolution();
	}

	const ManagedSwitchSolutionComponent = solution.getComponentByName(
		SC_SOLUTIONS.MANAGEDSWITCH_SOLUTION
	);

	if (!ManagedSwitchSolutionComponent) {
		return;
	}

	return Promise.all([
		setCurrentSystemDate(solution),
		updateFromBasket(solution, [SC_COMMON_ATTRIBUTES.ACCOUNT_ID])
	]);
}