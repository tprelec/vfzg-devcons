if (!CS || !CS.SM) {
	throw Error("Solution Console Api not loaded!");
}

// Register GenFirst Solution plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener("SolutionConsoleReady", registerGenFirst);
}
console.log("GenFirst...");
async function registerGenFirst() {
	const genFirst = await CS.SM.registerPlugin("GenFirst");
	updateGenFirstPlugin(genFirst);

	console.log("GenFirst plugin registered!");
	return;
}

// Hooks registration
function updateGenFirstPlugin(Plugin) {
	Plugin.afterAttributeUpdated = afterGenFirstAttributeUpdated;
}

// Hook Implementation
async function afterGenFirstAttributeUpdated(
	component,
	configuration,
	attribute,
	oldValueMap
) {
	
	// Raise root level flags here
	if (component.name === 'GenFirst' && attribute.name === 'contractTerm' && attribute.value !== oldValueMap.value) {
		SC_MessageBroker.store.publish('root:ContractTerm', {
			"component": component,
			"configuration": configuration,
			"attribute": attribute,
			"component": oldValueMap
		});
	}

	// Implement component logic here
	scgetaccess.afterGenFirstAttributeUpdated(component, configuration, attribute, oldValueMap);
	scgetevc.afterGenFirstAttributeUpdated(component, configuration, attribute, oldValueMap);
	scgetinternet.afterGenFirstAttributeUpdated(component, configuration, attribute, oldValueMap);

	return true;
}
