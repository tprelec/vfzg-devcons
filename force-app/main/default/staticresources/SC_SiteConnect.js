// SC_Access
/**
 * Plugin for Access
 * NOTE: Plugin must be invoked after SC_BusinessInternet_Solution.js
 */
console.log('Loaded Site Connect plugin');
async function afterSiteConnectConfigurationAdded(component, configuration) {
	if (!configuration.attributes.contractduration.value) {
		await setAttribute(
			component,
			configuration,
			SC_COMMON_ATTRIBUTES.CONTRACT_DURATION,
			configuration.attributes.contractterm.value
		);
	}

	return true;
}

async function afterSiteConnectConfigurationDeleted(component, configuration) {
	let deletedConfig = configuration.id;
	const basket = await CS.SM.getActiveBasket();
	const solutions = basket.getSolutions();
	let businessInternetSolutions = Object.values(solutions).filter(
		(item) => item.name == 'Business Internet'
	);
	if (businessInternetSolutions != undefined) {
		for (let i = 0; i < businessInternetSolutions.length; i++) {
			let businessInternetSolution = businessInternetSolutions[i];
			let configurations = await SC_Util.generateFlatConfigurationListAsync(
				businessInternetSolution
			);
			let internetConfiguration = Object.values(configurations).find(
				(item) => item.name == 'Internet'
			);
			if (internetConfiguration.value.attributes['site connect'].value == deletedConfig) {
				await SC_Helper.updateAttributeAsync(
					businessInternetSolution,
					internetConfiguration.key,
					'site connect',
					''
				);
			}
		}
	}

	await checkBandwidthOnSiteConnect();
}

async function afterSiteConnectAttributeUpdated(component, configuration, attribute, oldValueMap) {
	if (attribute.name === SC_COMMON_ATTRIBUTES.CONTRACT_DURATION) {
		await setAttribute(component, configuration, SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT, '');
	}

	if (attribute.name === SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT) {
		await checkBandwidthOnSiteConnect();
	}

	if (attribute.name === SC_COMMON_ATTRIBUTES.ACCESS) {
		await checkBandwidthOnSiteConnect();
		await updateAccessRelatedAttributes(configuration, attribute);
	}
}

async function updateAccessRelatedAttributes(configuration, attribute) {
	let allConfigurations = await SC_Util.generateFlatConfigurationListAsync();
	const activeSolution = await CS.SM.getActiveSolution();
	if (attribute.value) {
		let accessConfig = allConfigurations[attribute.value];
		if (accessConfig) {
			let unpackedAccess = SC_Util.unpackFlattConfiguration(accessConfig);
			let siteId = SC_Util.getAttributeValue(unpackedAccess, SC_COMMON_ATTRIBUTES.SITE_ID);
			SC_Helper.updateAttributeAsync(
				activeSolution,
				configuration.guid,
				SC_COMMON_ATTRIBUTES.SITE_ID,
				siteId
			);
		} else {
			SC_Helper.updateAttributeAsync(
				activeSolution,
				configuration.guid,
				SC_COMMON_ATTRIBUTES.SITE_ID,
				''
			);
		}
	} else {
		SC_Helper.updateAttributeAsync(
			activeSolution,
			configuration.guid,
			SC_COMMON_ATTRIBUTES.SITE_ID,
			''
		);
	}
}

async function getDefaultMaxBandSiteConnect(contractTerm, accessUp, accessDown, ownInfra) {
	const basket = await CS.SM.getActiveBasket();
	const inputMapObject = {
		method: SC_REMOTE_CLASS_METHOD.DEFAULT_MAX_BAND_SITECON,
		contractDuration: contractTerm,
		accessUp: accessUp,
		accessDown: accessDown,
		ownInfra: ownInfra
	};

	const result = await basket.performRemoteAction(
		SC_REMOTE_CLASS_NAME.REMOTE_APEX_CLASS,
		inputMapObject
	);

	return result;
}

async function checkBandwidthOnSiteConnect() {
	let configurationsAll = await SC_Util.generateFlatConfigurationListAsync();

	//key, bandup, banddown
	let mapAccessConfigs = new Map();

	//key, accessKey, bandUp, bandDown
	let mapSiteConnectConfigs = new Map();

	for (let i in configurationsAll) {
		if (configurationsAll[i].name == SC_COMPONENTS.ACCESS_COMPONENT) {
			let configItem = {
				bandwidthUp: configurationsAll[i].value.attributes.productvariantbanup.value,
				bandwidthDown: configurationsAll[i].value.attributes.productvariantbandown.value,
				ownInfra: configurationsAll[i].value.attributes.owninfrastructuretoggle.value
			};
			mapAccessConfigs.set(i, configItem);
		}
		if (configurationsAll[i].name == SC_COMPONENTS.SITECONNECT_COMPONENT) {
			let configItem = {
				accessKey: configurationsAll[i].value.attributes.access.value,
				bandwidthUp: configurationsAll[i].value.attributes.productvariantbanup.value,
				bandwidthDown: configurationsAll[i].value.attributes.productvariantbandown.value
			};
			mapSiteConnectConfigs.set(i, configItem);
		}
	}

	for (let key of mapAccessConfigs.keys()) {
		let sumBanUp = 0;
		let sumBanDown = 0;
		let errorExists = false;

		mapSiteConnectConfigs.forEach((valueSiteConnect, keySiteConnect, map) => {
			if (valueSiteConnect.accessKey == key) {
				sumBanUp += parseFloat(valueSiteConnect.bandwidthUp);
				sumBanDown += parseFloat(valueSiteConnect.bandwidthDown);
			}
		});

		if (mapAccessConfigs.get(key).ownInfra === false) {
			if (mapAccessConfigs.get(key).bandwidthUp < sumBanUp) {
				errorExists = true;
				configurationsAll[key].value.updateStatus({
					status: false,
					message: SC_ERROR_MESSAGES.SITE_CONNECT_INVALID_BAN_UP
				});
				CS.SM.displayMessage(SC_ERROR_MESSAGES.SITE_CONNECT_INVALID_BAN_UP, 'error');
				await validateConfiguration(
					configurationsAll[key].value,
					false,
					SC_ERROR_MESSAGES.SITE_CONNECT_INVALID_BAN_UP
				);
			} else {
				configurationsAll[key].value.updateStatus({ status: true, message: '' });
				await validateConfiguration(configurationsAll[key].value, true, '');
			}

			if (mapAccessConfigs.get(key).bandwidthDown < sumBanDown) {
				CS.SM.displayMessage(SC_ERROR_MESSAGES.SITE_CONNECT_INVALID_BAN_DOWN, 'error');
				configurationsAll[key].value.updateStatus({
					status: false,
					message: SC_ERROR_MESSAGES.SITE_CONNECT_INVALID_BAN_DOWN
				});
				await validateConfiguration(
					configurationsAll[key].value,
					false,
					SC_ERROR_MESSAGES.SITE_CONNECT_INVALID_BAN_DOWN
				);
			} else if (errorExists === false) {
				configurationsAll[key].value.updateStatus({ status: true, message: '' });
				await validateConfiguration(configurationsAll[key].value, true, '');
			}

			// assumption is that each selected site connect uses bandwidth
			// hence condition below means that there are no site connects added for specific access config
			if (sumBanDown == 0 && sumBanUp == 0) {
				CS.SM.displayMessage(SC_ERROR_MESSAGES.SITE_CONNECT_MANDATORY, 'error');
				await validateConfiguration(
					configurationsAll[key].value,
					false,
					SC_ERROR_MESSAGES.SITE_CONNECT_MANDATORY
				);
			}
		}
	}
}

/**
 * @description When access is deleted
 */
async function checkForSiteConnectDeletion(accessConfiguration) {
	let guid = SC_Util.getAttributeValue(accessConfiguration, SC_COMMON_ATTRIBUTES.GUID);
	console.log('DELETED Access with ID: ' + guid);

	const activeSolution = await CS.SM.getActiveSolution();
	const configurations = await SC_Util.generateFlatConfigurationListAsync();
	const siteConnects = await getExistingSiteConnects(configurations);
	const siteConnectComponent = activeSolution.getComponentByName(
		SC_COMPONENTS.SITECONNECT_COMPONENT
	);

	let guids = [];
	Object.values(siteConnects).forEach((config) => {
		if (config.access == accessConfiguration.guid) {
			guids.push(config.guid);
		}
	});

	guids.forEach((item) => {
		siteConnectComponent.deleteConfiguration(item);
	});
}

/**
 * @description when access is added, i.e. productVariant on Access is selected
 */
async function resolveSiteConnect() {
	const configurations = await SC_Util.generateFlatConfigurationListAsync();
	const accessConfigs = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.ACCESS_COMPONENT
	);
	if (!accessConfigs || accessConfigs.length === 0) {
		return;
	}

	const activeSolution = await CS.SM.getActiveSolution();
	const siteConnectComponent = activeSolution.getComponentByName(
		SC_COMPONENTS.SITECONNECT_COMPONENT
	);
	const unpackedAccesses = getAccessAttributes(accessConfigs);
	const existingSiteConnects = await getExistingSiteConnects(configurations);
	// for each "own infrastructure" access configuration, we are generating Site Connect configuration (if it isn't there already)
	Object.keys(unpackedAccesses).forEach(async (accessGuid) => {
		if (existingSiteConnects !== undefined && existingSiteConnects.hasOwnProperty(accessGuid)) {
			console.log('reselect productVariant for SiteConnect (just PoC)');
		} else {
			// add new Site Connect
			let commercialProduct = await getDefaultMaxBandSiteConnect(
				unpackedAccesses[accessGuid].ContractTerm,
				unpackedAccesses[accessGuid].BandwidthUp,
				unpackedAccesses[accessGuid].BandwidthDown,
				unpackedAccesses[accessGuid].OwnInfra
			);
			if (commercialProduct.status) {
				let attributes = [];
				attributes.push(
					SC_Util.generateAttribute(SC_COMMON_ATTRIBUTES.ACCESS, accessGuid, true)
				);
				attributes.push(
					SC_Util.generateAttribute(
						SC_COMMON_ATTRIBUTES.CONTRACT_DURATION,
						unpackedAccesses[accessGuid].ContractTerm,
						true
					)
				);
				attributes.push(
					SC_Util.generateLookupAttribute(
						SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT,
						commercialProduct.commercialProducts.Id,
						commercialProduct.commercialProducts.Name,
						true
					)
				);
				attributes.push(
					SC_Util.generateAttribute(
						SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_UP,
						commercialProduct.commercialProducts.Available_bandwidth_up__c,
						false
					)
				);
				attributes.push(
					SC_Util.generateAttribute(
						SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_DOWN,
						commercialProduct.commercialProducts.Available_bandwidth_down__c,
						false
					)
				);
				const siteConnectComponent = activeSolution.getComponentByName(
					SC_COMPONENTS.SITECONNECT_COMPONENT
				);
				const siteConnectConfiguration =
					siteConnectComponent.createConfiguration(attributes);
				await siteConnectComponent.addConfiguration(siteConnectConfiguration);
			} else {
				console.error('resolveSiteConnect error: ' + commercialProduct.message);
			}
		}
	});
}

async function getExistingSiteConnects(configurations) {
	let siteConnectConfigs = {};
	const siteConnects = await SC_Util.getAllConfigurationsWithNameAsync(
		configurations,
		SC_COMPONENTS.SITECONNECT_COMPONENT
	);
	console.log('All Site Connect configs: ' + JSON.stringify(siteConnects));

	siteConnects.forEach((config) => {
		let configInfo = {};
		const unpacked = SC_Util.unpackFlattConfiguration(config);
		configInfo.guid = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.GUID);
		configInfo.access = SC_Util.getAttributeValue(unpacked, SC_COMMON_ATTRIBUTES.ACCESS);
		configInfo.contractTerm = SC_Util.getAttributeValue(
			unpacked,
			SC_COMMON_ATTRIBUTES.CONTRACT_ - TERM
		);
		configInfo.bandwidthUp = SC_Util.getAttributeValue(
			unpacked,
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_UP
		);
		configInfo.bandwidthDown = SC_Util.getAttributeValue(
			unpacked,
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_DOWN
		);
		siteConnectConfigs[configInfo.access] = configInfo;
	});
	console.log('Collected existing SiteConnects: ' + JSON.stringify(siteConnectConfigs));

	return siteConnectConfigs;
}
