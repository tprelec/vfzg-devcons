//SC_BusinessMobileSolution
if (!CS || !CS.SM) {
	throw Error('Solution Console Api not loaded!');
}

//Register Business Mobile Solution plugin when Solution Console becomes ready
if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', registerBusinessMobile);
}

async function registerBusinessMobile() {
	const businessMobilePlugin = await CS.SM.registerPlugin(SC_SCHEMA_NAME.BUSINESS_MOBILE_SOLUTION);
	updateBusinessMobilePlugin(businessMobilePlugin);
	return;
}

// Hooks registration
function updateBusinessMobilePlugin(Plugin) {
	//console.log(Plugin);
	Plugin.afterConfigurationAdd = afterConfigurationAddBusinessMobile;
	Plugin.afterAttributeUpdated = afterAttributeUpdatedBusinessMobile;
	Plugin.beforeSave = beforeSolutionSave;
}

/**
 * @description Initialization logic  - setting values for Today and Sales Channel
 */
async function initBusinessMobileSolutionDefaults(solution) {
	if (!solution) {
		solution = await CS.SM.getActiveSolution();
	}

	const businessMobileSolutionComponent = solution.getComponentByName(SC_SOLUTIONS.BUSINESS_MOBILE_SOLUTION);
	if (!businessMobileSolutionComponent) {
		return;
	}

	return Promise.all([
		setCurrentSystemDate(solution),
		updateFromBasket(solution, [SC_COMMON_ATTRIBUTES.SALES_CHANNEL]),
		readOnlyAtribbuteIfIsMacdOnMain('scenario'),
		readOnlyAtribbuteIsFromMacBasket(SC_COMPONENTS.MOBILE_PLAN_COMPONENT, SC_BUSSINESS_MOBILE_ATTRIBUTES.CONTRACT_TERM)
	]).then(() => {
		restrictValueFromPicklistIfDirectChannel(SC_BUSSINESS_MOBILE_ATTRIBUTES.SCENARIO_MAIN, SC_BUSSINESS_MOBILE_VALUES.BM_REGULAR);
	});
}

/**
 * @description Hook Implementation - afterConfigurationAdd
 */
async function afterConfigurationAddBusinessMobile(component, configuration) {
	switch (component.name) {
		case SC_COMPONENTS.MOBILE_PLAN_COMPONENT:
			return afterConfigurationAddedMobilePlan(component, configuration);
		case SC_COMPONENTS.MOBILE_DEVICE_COMPONENT:
			return afterConfigurationAddedMobileDevice(component, configuration);
		case SC_COMPONENTS.MOBILE_ZAKELIJKE_COMPONENT:
			return afterConfigurationAddedZakelijkeToestelbetaling(component, configuration);
		default:
			console.warn(SC_WARNING_MESSAGES.HOOK_NOT_SPECIFIED + component.name);
	}
	return true;
}

/**
 * @description Hook Implementation - afterAttributeUpdated
 */
async function afterAttributeUpdatedBusinessMobile(component, configuration, attribute, oldValueMap) {
	switch (component.name) {
		case SC_COMPONENTS.BUSINESS_MOBILE_COMPONENT:
			break;
		case SC_COMPONENTS.MOBILE_PLAN_COMPONENT:
			return afterAttributeUpdatedMobilePlanComponent(component, configuration, attribute, oldValueMap);
		case SC_COMPONENTS.MOBILE_DEVICE_COMPONENT:
			return afterAttributeUpdatedMobileDeviceComponent(component, configuration, attribute, oldValueMap);
		case SC_COMPONENTS.MOBILE_ZAKELIJKE_COMPONENT:
			return afterAttributeUpdatedZakelijkeToestelbetalingeComponent(component, configuration, attribute, oldValueMap);
		default:
			console.warn(SC_WARNING_MESSAGES.HOOK_NOT_SPECIFIED + component.name);
	}
	return true;
}

/**
 * @description Function removes the value from the picklist(attributeName) on the configuration if it is a direct channel
 */
async function restrictValueFromPicklistIfDirectChannel(attributeName, valueToBeRemoved) {
	let activeSolution = await CS.SM.getActiveSolution();
	let mainConfiguration = getMainConfiguration(activeSolution);
	let salesChannel = getAttributeValue(mainConfiguration, 'saleschannel');
	if (salesChannel == SC_BUSSINESS_MOBILE_VALUES.DIRECT_SALES_CHANNEL) {
		const attributes = mainConfiguration.attributes;
		Object.values(attributes).forEach((attribute) => {
			if (attribute.name === attributeName) {
				const picklistValues = attribute.options;
				const itemToBeRemoved = { value: valueToBeRemoved };
				let position = picklistValues.findIndex((a) => a.value === itemToBeRemoved.value);
				if (position != -1) {
					picklistValues.splice(position, 1);
				}
				const updateData = { options: picklistValues };
				mainConfiguration = mainConfiguration.updateAttribute(attributeName, updateData);
			}
		});
	}
	return true;
}
