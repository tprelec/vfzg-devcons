//console.log('SC_BMDevice.js'); //SC_BMDevice.js - part of Business Mobile Solution
/**
 * @description Implementing the afterConfigurationAdd hook for the Mobile Device component
 */
async function afterConfigurationAddedMobileDevice(component, configuration) {
	return true;
}

/**
 * @description Implementing the afterAttributeUpdated hook for the Mobile Device component
 */
async function afterAttributeUpdatedMobileDeviceComponent(component, configuration, attribute, oldValueMap) {
	switch (attribute.name) {
		case SC_BUSSINESS_MOBILE_ATTRIBUTES.SIM_TYPE:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.BRAND:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.STORAGE:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.COLOR_D:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.NETWORK_TYPE:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT:
			await renameMobileDevice(component, configuration);
			break;

		default:
			break;
	}
	return true;
}

/**
 * @description Setting configuration name for Mobile Device based on commercial product
 */
async function renameMobileDevice(component, configuration) {
	let productVariantName = getAttributeValue(configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_NAME);
	let name;

	if (productVariantName) {
		name = productVariantName;

		const attributeData = [
			{
				name: SC_BUSSINESS_MOBILE_ATTRIBUTES.MOBILE_DEVICE_CONFIGURATION_NAME,
				value: name
			}
		];
		return component.updateConfigurationAttribute(configuration.guid, attributeData);
	}
	return true;
}
