console.log('Package plugin loaded!');

let SC_Packages = {};

SC_Packages.appendPackageModal = function() {

    const innerHtml = `<div id="packageModal" class="cdk-overlay-container" style="z-index: 1001; display: block;">
        <div class="cdk-overlay-backdrop cdk-overlay-dark-backdrop cdk-overlay-backdrop-showing"></div>
        <div class="cdk-global-overlay-wrapper" dir="ltr" style="justify-content: center; align-items: center;">
            <div class="cdk-overlay-pane custom-dialog-container" style="max-width: 90vw; pointer-events: auto; position: static;">
                <div tabindex="0" class="cdk-visually-hidden cdk-focus-trap-anchor"></div>
                <div aria-modal="true" class="mat-dialog-container ng-tns-c21-0 ng-trigger ng-trigger-slideDialog ng-star-inserted" tabindex="-1" role="dialog" style="transform: none; opacity: 1;">
                    <div class="ng-star-inserted">
                        <div class="container" style="position:relative; z-index: 1001;">
                            <iframe id="package-iframe" name="package-iframe" style="left: 0; width: 100%; height: 85vh; overflow: hidden; z-index: 1001;" class="frame ng-star-inserted" frameborder="0" scrolling="yes"></iframe>
                        </div>
                    </div>
                </div>
                <div tabindex="0" class="cdk-visually-hidden cdk-focus-trap-anchor"></div>
            </div>
        </div>
    </div>`;

    const elems = document.getElementsByClassName('slds-page-header__row');

    if (elems && elems.length > 0) {

        try {
            
            let divContainer = document.createElement('div');
            divContainer.innerHTML = innerHtml;

            document.body.append(divContainer);

        } catch (err) {
            CS.SM.displayMessage('Error loading Packages!', 'error');
            console.log(err.message ? err.message : err);
        }
    }
};

SC_Packages.exit = function() {
    
    let elem = document.getElementById('packageModal');

    if (elem) {
        let parent = elem.parentNode;
        parent.removeChild(elem);
    }
}

// Freeze object to prevent accidental overrides
Object.freeze(SC_Packages);