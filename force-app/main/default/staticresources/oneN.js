var oneNetEnterpriseCodes = {
	ONE_Flex: 'C107973',
	One_Combi: 'C112368', //C107972
	One_Mobiel: 'C107974',
	One_Fixed: 'C107971',
	ONE_Integrate_Lite: '',
	ONE_Integrate_Premium: '',
	Site_Installation_Fee_5: 'C108017',
	Site_Installation_Fee_15: 'C108018',
	Site_Installation_Fee_50: 'C108019',
	Site_Installation_Fee_100: 'C108020',
	Site_Installation_Fee_200: 'C108021',
	Site_Installation_Fee_Over_200: 'C108022',
	ONE_Call_Center_agent: 'C108011',
	ONE_Call_Center_supervisor: 'C108012',
	IVRKeuzemenu: 'C107977'
};

var flags = {
	addSiteInstallationFeeToEachLocation: 'OneOffSiteInstallationFeeAdded_0'
};

var scenario = {
	Scenario_Enterprise: 'One Net Enterprise',
	Scenario_Express: 'One Net Express'
};

var oneNetExpressCodes = {
	Flex_Seat: 'C112369', //C108750
	Combi_seat: 'C112370', //C108752
	One_Mobiel: 'C107974',
	Mobiel_seat: 'C108751',
	ONE_Integrate_Lite: '',
	ONE_Integrate_Premium: '',
	SiteInstallationFee: 'C108688',
	ONE_Call_Center_agent: 'C108011',
	ONE_Call_Center_supervisor: 'C108012',
	ONX_Vast: 'C109357',
	IVRKeuzemenu: 'C107977'
};

var cCodes = {
	ONE_Call_Center_agent: 'C108011',
	ONE_Call_Center_supervisor: 'C108012',
	IVRKeuzemenu: 'C107977',
	OneNetBasic: 'C109124',
	OneNetIntegratePremium1: 'C108917',
	OneNetIntegratePremium2: 'C108918',
	OneCombi: {
		Enterprise: 'C112368',
		Express: 'C112370'
	},
	OneFixed: {
		Enterprise: 'C107971',
		Express: 'C112369'
	},
	OneFixedATA: {
		Enterprise: 'C107973',
		Express: 'C109357'
	},
	OneFlex: {
		Enterprise: 'C107973',
		Express: 'C108749'
	},
	OneMobiel: {
		Enterprise: 'C107974',
		Express: 'C108751'
	}
};

var productTypes = {
	AddOnLicenses: 'Add_on_Licenses_0',
	Hardware: 'Hardware_0',
	CoreLicenses: 'Core_Licenses_0',
	ImplementationAndTraining: 'Implementation_and_Training_0'
};

var productTypeLabels = {
	Add_on_Licenses_0: 'Add On Licenses',
	Hardware_0: 'Hardware',
	Core_Licenses_0: 'Core Licenses',
	Implementation_and_Training_0: 'Implementation And Training'
};

var oneNet_categories = {
	seats: 'Seat Licenses'
};

var productTypesArray = [productTypes.AddOnLicenses, productTypes.Hardware, productTypes.CoreLicenses, productTypes.ImplementationAndTraining];
var productTypesFO = [productTypes.AddOnLicenses, productTypes.CoreLicenses, productTypes.ImplementationAndTraining];

//
// Hide configuration quanitity from Addons
//

//get prod per group
window.getProdPerGroupON = function getProdPerGroupON(prodTypes) {
	var categoryStr = 'Category_0';
	var quantityStr = 'Quantity_0';

	var results = {};
	prodTypes.forEach(function (element) {
		for (var x in CS.Service.config[element].relatedProducts) {
			var itm = CS.Service.config[element].relatedProducts[x];
			if (itm.reference) {
				var category = CS.getAttributeValue(itm.reference + ':' + categoryStr);
				var quantity = CS.getAttributeValue(itm.reference + ':' + quantityStr);

				if (results[category]) {
					results[category].quantity += quantity;
				} else {
					results[category] = {};
					results[category].quantity = quantity;
				}
			}
		}
	});
	return results;
};

window.max50Seats = function max50Seats() {
	if (CS.getAttributeValue('Scenario_0') == scenario.Scenario_Express) {
		var cCodes = getProdPerGroupON(productTypesArray);

		var sumOfSeats = 0;

		if (cCodes && cCodes[oneNet_categories.seats]) {
			sumOfSeats = cCodes[oneNet_categories.seats].quantity;
		}

		if (sumOfSeats > 50) {
			CS.markConfigurationInvalid('Max 50 seat licenses can be added to your One Net configuration');
		}
	}
};

window.minSeats = function minSeats() {
	if (CS.getAttributeValue('Scenario_0') == scenario.Scenario_Express) {
		var cCodes = getProdPerGroupON(productTypesArray);

		var sumOfSeats = 0;

		if (cCodes && cCodes[oneNet_categories.seats]) {
			sumOfSeats = cCodes[oneNet_categories.seats].quantity;
		}

		if (sumOfSeats < 5) {
			CS.markConfigurationInvalid('Min 5 seat licenses must be added to your One Net configuration');
		}
	}
	if (CS.getAttributeValue('Scenario_0') == scenario.Scenario_Enterprise) {
		var cCodes = getProdPerGroupON(productTypesArray);

		var sumOfSeats = 0;

		if (cCodes && cCodes[oneNet_categories.seats]) {
			sumOfSeats = cCodes[oneNet_categories.seats].quantity;
		}

		if (sumOfSeats < 15) {
			CS.markConfigurationInvalid('Min 15 seat licenses must be added to your One Net configuration');
		}
	}
};

//One Net Flex
window.oneNetFlexRules = function oneNetFlexRules() {
	//addDayNight();
	//addDesktop();
	//removeDesktopIfNoOneFixedLicences('Add_on_Licenses_0');
	setTimeout(function () {
		disableAutomatic();
	}, 0);
	disableAutomatic();
};

function addDayNight() {
	//C108150
	var cCode = 'C108150';
	var flag = 'DayNightAdded_0';
	if (CS.getAttributeValue(flag) == '') {
		priceItemAutomaticAddition(cCode, flag, productTypes.AddOnLicenses);
	}
}

//Final validations
window.getNumOneFixedOneCombi = function getNumOneFixedOneCombi() {
	var sumOneFixed = 0;

	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	console.log(currentCCodesSum);

	var oneFixedCodes = getOneFixedOneCombiLicence();

	for (var idx in oneFixedCodes) {
		if (currentCCodesSum[oneFixedCodes[idx]]) {
			sumOneFixed += currentCCodesSum[oneFixedCodes[idx]].quantity;
		}
	}

	return sumOneFixed;
};

//remove if no combi/flex
window.removeSiteInstallationFeeToEachLocation = function removeSiteInstallationFeeToEachLocation(relType) {
	//if VoicemailAdded == 'set'
	var flag = CS.getAttributeValue('OneOffSiteInstallationFeeAdded_0');
	var numberOfOneFixedProducts = 0;
	if (flag == 'set') {
		//if Voicemail Addon exists

		var currentCCodes = getCCode();
		var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

		//
		//if No Fixed licences
		numberOfOneFixedProducts = getNumOneFixedOneCombi(); //getOneFixedOneCombiLicence();

		if (numberOfOneFixedProducts == 0) {
			//remove site installation

			var voiceIdx = getVoiceMailIndex('C108017');

			if (voiceIdx == -1) {
				voiceIdx = getVoiceMailIndex('C108018');
			}
			if (voiceIdx == -1) {
				voiceIdx = getVoiceMailIndex('C108019');
			}
			if (voiceIdx == -1) {
				voiceIdx = getVoiceMailIndex('C108020');
			}
			if (voiceIdx == -1) {
				voiceIdx = getVoiceMailIndex('C108021');
			}
			if (voiceIdx == -1) {
				voiceIdx = getVoiceMailIndex('C108022');
			}
			/*
            if(voiceIdx == -1){
                voiceIdx = getVoiceMailIndex('C108021');
            }
            */
			if (voiceIdx == -1) {
				voiceIdx = getVoiceMailIndex('C108688');
			}
			//reset flag
			CS.setAttributeValue('VoicemailAdded_0', '');

			var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';

			if (voiceIdx >= 0) {
				CS.Service.removeRelatedProduct(withoutIndex + voiceIdx);
				//reset flag
				CS.setAttributeValue('OneOffSiteInstallationFeeAdded_0', '');
			}
		}
	}
	//validation
	if (numberOfOneFixedProducts > 0) {
		//check if changed
		if (CS.getAttributeValue('Scenario_0') == scenario.Scenario_Enterprise) {
			//check if quantity has changed
			//if less than 5
			if (numberOfOneFixedProducts > 0 && numberOfOneFixedProducts <= 5) {
				checkForRightOneAndRemoveNotNeeded('C108017', numberOfOneFixedProducts, 0, 15, flags.addSiteInstallationFeeToEachLocation);
			} else if (numberOfOneFixedProducts > 5 && numberOfOneFixedProducts <= 15) {
				checkForRightOneAndRemoveNotNeeded('C108018', numberOfOneFixedProducts, 5, 15, flags.addSiteInstallationFeeToEachLocation);
			} else if (numberOfOneFixedProducts > 15 && numberOfOneFixedProducts <= 50) {
				checkForRightOneAndRemoveNotNeeded('C108019', numberOfOneFixedProducts, 15, 50, flags.addSiteInstallationFeeToEachLocation);
			} else if (numberOfOneFixedProducts > 50 && numberOfOneFixedProducts <= 100) {
				checkForRightOneAndRemoveNotNeeded('C108020', numberOfOneFixedProducts, 50, 100, flags.addSiteInstallationFeeToEachLocation);
			} else if (numberOfOneFixedProducts > 100 && numberOfOneFixedProducts <= 200) {
				checkForRightOneAndRemoveNotNeeded('C108021', numberOfOneFixedProducts, 100, 200, flags.addSiteInstallationFeeToEachLocation);
			} else if (numberOfOneFixedProducts > 200) {
				checkForRightOneAndRemoveNotNeeded('C108022', numberOfOneFixedProducts, 200, Infinity, flags.addSiteInstallationFeeToEachLocation);
			}
			/*
            else{
                CS.markConfigurationInvalid('--UNSUPPORTED SCENARIO--- Please ask Eelco');
            }
            */
		}
	}
};

function checkForRightOneAndRemoveNotNeeded(rightCode, numOfProd, minValEx, maxValIncl, flag) {
	var change = false;
	//
	if (numOfProd > minValEx && numOfProd <= maxValIncl && getVoiceMailIndex(rightCode) > 0) {
		console.log('check --all ok');
	} else {
		//not ok - will set val to true
		change = true;
	}
	if (change) {
		CS.setAttributeValue('Sum_Of_Licences_0', numOfProd);
		var voiceIdx = getVoiceMailIndex('C108017');

		if (voiceIdx == -1) {
			voiceIdx = getVoiceMailIndex('C108018');
		}
		if (voiceIdx == -1) {
			voiceIdx = getVoiceMailIndex('C108019');
		}
		if (voiceIdx == -1) {
			voiceIdx = getVoiceMailIndex('C108020');
		}
		if (voiceIdx == -1) {
			voiceIdx = getVoiceMailIndex('C108021');
		}
		if (voiceIdx == -1) {
			voiceIdx = getVoiceMailIndex('C108022');
		}
		/*
            if(voiceIdx == -1){
                voiceIdx = getVoiceMailIndex('C108021');
            }
            */
		if (voiceIdx == -1) {
			voiceIdx = getVoiceMailIndex('C108688');
		}

		var relType = 'Add_on_Licenses_0';
		var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';

		if (voiceIdx >= 0) {
			var prodId = CS.Service.config[''].config.cscfga__Product_Definition__c;
			var newDyn = {};
			newDyn['TempVal'] = rightCode;
			doLookupQueryAndInvalidateAddons('PriceItemFilterAutomatic', newDyn, prodId, CS.Service.config['AutomaticLookup_0']).then(function (
				pgaResult
			) {
				if (pgaResult.res == null) {
					doLookupQueryAndInvalidateAddons('PriceItemFilterAutomaticRec', newDyn, prodId, CS.Service.config['AutomaticLookup_0']).then(
						function (pgaResult) {
							_.each(pgaResult.res, function (result) {
								console.log('2nd option');

								//CS.setAttributeValue(withoutIndex + voiceIdx+':Product_0',result.Id,result.Category__c);

								CS.setAttributeValue(withoutIndex + voiceIdx + ':Product_0', result.Id);
								CS.setAttributeValue(withoutIndex + voiceIdx + ':Category_0', result.Category__c);
								CS.setAttributeValue(withoutIndex + voiceIdx + ':AdditionMethod_0', 'Automatic');
							});
						}
					);
				} else {
					_.each(pgaResult.res, function (result) {
						//priceItemsAddition('Addons_0',result.Id);
						//CS.setAttributeValue(withoutIndex + voiceIdx+':Product_0',result.Id,result.Category__c);

						CS.setAttributeValue(withoutIndex + voiceIdx + ':Product_0', result.Id);
						CS.setAttributeValue(withoutIndex + voiceIdx + ':Category_0', result.Category__c);
						CS.setAttributeValue(withoutIndex + voiceIdx + ':AdditionMethod_0', 'Automatic');
					});
				}
			});
		}
	}
}
//remove voice if no fixed addons
function removeVoicemailIfNoOneFixedLicences(relType) {
	//if VoicemailAdded == 'set'
	var flag = CS.getAttributeValue('VoicemailAdded_0');
	if (flag == 'set') {
		//if Voicemail Addon exists

		var currentCCodes = getCCode();
		var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

		var codeVoice = 'C106857';
		//
		//if No Fixed licences
		var numberOfOneFixedProducts = getNumOfOneFixed();

		if (numberOfOneFixedProducts == 0) {
			//remove Voicemail addon

			var voiceIdx = getVoiceMailIndex(codeVoice);
			if (voiceIdx >= 0) {
				var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';
				CS.Service.removeRelatedProduct(withoutIndex + voiceIdx);
				//reset flag
				CS.setAttributeValue('VoicemailAdded_0', '');
			}
		}
	}
}

function getVoiceMailIndex(codeVoice) {
	var relType = 'Add_on_Licenses_0';
	var ind = -1;
	productTypesArray.forEach(function (item, index, array) {
		if (item != undefined) {
			relType = item;
			if (CS.Service.config[relType] && CS.getAttributeWrapper(relType)) {
				if (CS.getAttributeWrapper(relType).relatedProducts.length > 0) {
					//for(var idx in CS.getAttributeWrapper(relType).relatedProducts){
					for (var idx = 0; idx < CS.getAttributeWrapper(relType).relatedProducts.length; idx++) {
						var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';
						if (
							CS.getAttributeValue(withoutIndex + idx + ':CCodeOneOff_0') == codeVoice ||
							CS.getAttributeValue(withoutIndex + idx + ':CCodeRecurring_0') == codeVoice
						) {
							ind = idx;
							break;
						}
					}
				}
			}
		}
	});
	/*
    if(CS.getAttributeWrapper('Add_on_Licenses_0').relatedProducts.length>0){
    for(var idx in CS.getAttributeWrapper(relType).relatedProducts){
        var withoutIndex = relType.substr(0, relType.lastIndexOf('_'))+'_';
        if((CS.getAttributeValue(withoutIndex+idx+':CCodeOneOff_0')==codeVoice)||(CS.getAttributeValue(withoutIndex+idx+':CCodeRecurring_0')==codeVoice)){
            ind = idx; 
            break;
        }
    }
        
    }
    */
	return ind;
}

//end removal

//removeDesktop Tool if no flexfixcombi
function removeDesktopIfNoOneFixedLicences(relType) {
	//if VoicemailAdded == 'set'
	var flag = CS.getAttributeValue('ONEDesktopToolAdded_0');
	if (flag == 'set') {
		var currentCCodes = getCCode();
		var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

		var codeVoice = 'C107975';
		//
		//if No FixedFlexCombi licences
		var oneFlexOneFixedOneCombiLicence = getOneFlexOneFixedOneCombiLicence();

		var sumOneFlexOneFixedOneCombiLicence = 0;
		for (var idx in oneFlexOneFixedOneCombiLicence) {
			if (currentCCodesSum[oneFlexOneFixedOneCombiLicence[idx]]) {
				sumOneFlexOneFixedOneCombiLicence += currentCCodesSum[oneFlexOneFixedOneCombiLicence[idx]].quantity;
			}
		}

		if (sumOneFlexOneFixedOneCombiLicence == 0) {
			//remove Voicemail addon

			var voiceIdx = getVoiceMailIndex(codeVoice);
			if (voiceIdx >= 0) {
				//var withoutIndex = relTyperelType.split('_')[0]+'_';
				var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';

				CS.Service.removeRelatedProduct(withoutIndex + voiceIdx);
				//reset flag
				CS.setAttributeValue('ONEDesktopToolAdded_0', '');
			}
		}
		if (sumOneFlexOneFixedOneCombiLicence > 0) {
			//set quantity of one desktop
			var voiceIdx = getVoiceMailIndex(codeVoice);
			if (voiceIdx >= 0) {
				var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';
				CS.setAttributeValue(withoutIndex + voiceIdx + ':Quantity_0', sumOneFlexOneFixedOneCombiLicence);
			}
		}
	}
}
//end

//removeOVB Tool if no fix
window.removeOVBNIfNoOneFixedLicences = function removeOVBNIfNoOneFixedLicences(relType) {
	//if VoicemailAdded == 'set'
	var flag = CS.getAttributeValue('OVBAdded_0');
	if (flag == 'set') {
		var currentCCodes = getCCode();
		var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

		var codeVoice = 'C112371'; // C108041
		//
		//if No FixedFlexCombi licences
		var oneFixedLicence = getOneFixedCodes();

		var sumOneFixedLicence = 0;
		for (var idx in oneFixedLicence) {
			if (currentCCodesSum[oneFixedLicence[idx]]) {
				sumOneFixedLicence += currentCCodesSum[oneFixedLicence[idx]].quantity;
			}
		}

		if (sumOneFixedLicence == 0) {
			//remove Voicemail addon

			var voiceIdx = getItemIndex(relType, '-OVB');
			if (voiceIdx >= 0) {
				var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';
				CS.Service.removeRelatedProduct(withoutIndex + voiceIdx);
				//reset flag
				CS.setAttributeValue('OVBAdded_0', '');
			}
		}

		if (sumOneFixedLicence > 1) {
			var voiceIdx = getItemIndex(relType, '-OVB');
			var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';
			var quantityAttStr = withoutIndex + voiceIdx + ':Quantity_0';
			var currentQuantityOfOVB = CS.getAttributeValue(quantityAttStr);

			if (currentQuantityOfOVB != sumOneFixedLicence) {
				CS.setAttributeValue(quantityAttStr, sumOneFixedLicence.toString());
			}
		}
	}
};

function getItemIndex(relType, suffix) {
	var result = -1;

	if (CS.Service.config[relType] && CS.getAttributeWrapper(relType)) {
		if (CS.getAttributeWrapper(relType).relatedProducts.length > 0) {
			for (var idx in CS.getAttributeWrapper(relType).relatedProducts) {
				var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';
				if (CS.getAttributeValue(withoutIndex + idx + ':AdditionMethod_0').indexOf(suffix) > -1) {
					result = idx;
					break;
				}
			}
		}
	}
	return result;
}

window.removeOneOffImplementationFeePerLocationPerSwitchIfNoSwitch = function removeOneOffImplementationFeePerLocationPerSwitchIfNoSwitch(relType) {
	//if VoicemailAdded == 'set'
	var flag = CS.getAttributeValue('SwitchInstallationFeeAdded_0');
	if (flag == 'set') {
		var codeVoice = 'C108031';
		//
		//If no switches
		var currentCCodes = getProductAddonGroup(relType);
		var currentCCodesSum = getTotalQuantityPerAddonGroup(currentCCodes);

		var sumOfCertainGroup = 0;

		if (currentCCodesSum['Standard Switches']) {
			sumOfCertainGroup += currentCCodesSum['Standard Switches'].quantity;
		}

		if (sumOfCertainGroup == 0) {
			//remove Voicemail addon

			var voiceIdx = getVoiceMailIndex(codeVoice);
			if (voiceIdx >= 0) {
				var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';
				CS.Service.removeRelatedProduct(withoutIndex + voiceIdx);
				//reset flag
				CS.setAttributeValue('SwitchInstallationFeeAdded_0', '');
			}
		} else if (sumOfCertainGroup > 0) {
			var voiceIdx = getVoiceMailIndex(codeVoice);
			console.log('voiceIdx ==' + voiceIdx);
			if (voiceIdx >= 0) {
				var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';
				var idx = withoutIndex + voiceIdx + ':Quantity_0';
				CS.setAttributeValue(idx, sumOfCertainGroup);
			}
		}
	}
};

function checkIfAllIsSelectedON() {
	var result = 0;
	var results = {};

	var showError = false;
	productTypesArray.forEach(function (element) {
		if (CS.Service.config[element].relatedProducts.length > 0) {
			for (var x in CS.Service.config[element].relatedProducts) {
				var itm = CS.Service.config[element].relatedProducts[x];
				if (itm.reference) {
					console.log(itm.reference);
					var product = CS.getAttributeValue(itm.reference + ':' + 'Product_0');

					if (product == '') {
						CS.markConfigurationInvalid('Please select a product');
					}
				}
			}
		}
	});
}

//
window.finalValidationOneNet = function finalValidationOneNet() {
	disableAutomatic();
	onlyMobileNotValid();
	checkIfLANAdded();

	checkIfSwitchAdded();

	//Commented out as per UAT test scenarios 31-1-2019
	//checkFixedConfigQuanitityAndVoice(productTypes.AddOnLicenses);

	//removeVoicemailIfNoOneFixedLicences('Add_on_Licenses_0');

	//removeDesktopIfNoOneFixedLicences('Add_on_Licenses_0');

	removeOVBNIfNoOneFixedLicences('Add_on_Licenses_0');

	removeOneOffImplementationFeePerLocationPerSwitchIfNoSwitch('Hardware_0');

	removeSiteInstallationFeeToEachLocation('Add_on_Licenses_0');

	max50Seats();

	checkIfAllIsSelectedON();
	setTimeout(function () {
		disableAutomatic();
	}, 0);
	disableAutomatic();

	//Commented out as per UAT 1-2-2019
	//minSeats();
};

// make sure that correct products are selected when contract duration changes
// this happens upon Access Infrastructure contract duration change
function checkProductDurations(relType) {
	var products = [];
	for (var x in CS.Service.config[relType].relatedProducts) {
		var itm = CS.Service.config[relType].relatedProducts[x];
		if (itm.reference) {
			var currentDuration = CS.getAttributeValue(itm.reference + ':Duration_0');
			var minDuration = CS.getAttributeValue(itm.reference + ':Min_Duration_0');
			var maxDuration = CS.getAttributeValue(itm.reference + ':Max_Duration_0');
			var product = CS.getAttributeValue(itm.reference + ':Product_0');
			if (product !== undefined && product != '' && !(currentDuration >= minDuration && currentDuration < maxDuration)) {
				products.push(CS.getAttributeValue(itm.reference + ':PartName_0'));
			}
		}
	}

	if (products.length > 0) {
		CS.markConfigurationInvalid(
			'Following products from related list ' + productTypeLabels[relType] + ' have to be removed and then selected again: ' + products.join()
		);
	}
}

function reselectProductsIfNeeded() {
	checkProductDurations(productTypes.CoreLicenses);
	checkProductDurations(productTypes.Hardware);
	checkProductDurations(productTypes.AddOnLicenses);
	checkProductDurations(productTypes.ImplementationAndTraining);
}

function setQuanitityVoicemailFixed(relType) {
	var oneOffCCode = 'CCodeOneOff_0';
	var recurringCCode = 'CCodeRecurring_0';
	var quantity = 'Quantity_0';
	var results = [];
	for (var x in CS.Service.config[relType].relatedProducts) {
		var itm = CS.Service.config[relType].relatedProducts[x];
		if (itm.reference) {
			var result = {};
			result.oneOffCCode = CS.getAttributeValue(itm.reference + ':' + oneOffCCode);
			result.recurringCCode = CS.getAttributeValue(itm.reference + ':' + recurringCCode);
			result.quantity = CS.getAttributeValue(itm.reference + ':' + quantity);
			results.push(result);
		}
	}
}
function checkFixedConfigQuanitityAndVoice(relType) {
	var flag = CS.getAttributeValue('VoicemailAdded_0');
	if (flag == 'set') {
		setQuanitityVoicemailFixed('Add_on_Licenses_0');
	}
}

function setQuanitityVoicemailFixed(relType) {
	var quanitityVal = getNumOfOneFixed();
	var code = 'C106857';
	var oneOffCCode = 'CCodeOneOff_0';
	var recurringCCode = 'CCodeRecurring_0';
	var quantity = 'Quantity_0';
	var results = [];
	for (var x in CS.Service.config[relType].relatedProducts) {
		var itm = CS.Service.config[relType].relatedProducts[x];
		if (itm.reference) {
			var result = {};
			if (
				CS.getAttributeValue(itm.reference + ':' + oneOffCCode) == code ||
				CS.getAttributeValue(itm.reference + ':' + recurringCCode) == code
			) {
				var refQuantity = itm.reference + ':' + quantity;
				CS.setAttributeValue(refQuantity, quanitityVal);
			}
		}
	}
}
/**
 * Get number of one fixed licences
 */

function getOneFixedCodes() {
	var codes = [];

	codes.push(cCodes.OneFixed.Enterprise);
	codes.push(cCodes.OneFixed.Express);

	//codes.push(cCodes.oneFixedCodes.Express);
	//codes.push(cCodes.oneFixedCodes.Enterprise);
	/*
    //Express
    codes.push('C112369');

    //Enterprose
    codes.push('C107971');
    codes.push('RCONEVASTESEAT');
    */
	return codes;
}

window.getNumOfOneFixed = function getNumOfOneFixed() {
	var sumOneFixed = 0;

	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	console.log(currentCCodesSum);

	var oneFixedCodes = getOneFixedCodes();

	for (var idx in oneFixedCodes) {
		if (currentCCodesSum[oneFixedCodes[idx]]) {
			sumOneFixed += currentCCodesSum[oneFixedCodes[idx]].quantity;
		}
	}

	return sumOneFixed;
};

/*
//CPQ automatically adds ONE Desktop Tool (previously One Toolbar) to ONE Flex, ONE Fixed and ONE Combi licenses and sales cannot change this. This add-on license will not be available for ONE Mobiel
//
*/

window.checkIfLANAdded = function checkIfLANAdded() {
	//if(CS.getAttributeValue('Scenario_0') == scenario.Scenario_Enterprise){
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	var oneFlexOneFixedOneCombiLicence = getLANCodes();

	var sumOneFlexOneFixedOneCombiLicence = 0;
	for (var idx in oneFlexOneFixedOneCombiLicence) {
		if (currentCCodesSum[oneFlexOneFixedOneCombiLicence[idx]]) {
			sumOneFlexOneFixedOneCombiLicence += currentCCodesSum[oneFlexOneFixedOneCombiLicence[idx]].quantity;
		}
	}

	if (sumOneFlexOneFixedOneCombiLicence == 0) {
		CS.markConfigurationInvalid('You need to add at least 1 Lan Survey product to your configuration');
	}
	// }
};

function getSwitchPortCode() {
	var codes = [];

	codes.push('C108742');

	return codes;
}
//checkIfSwitchAdded
window.checkIfSwitchAdded = function checkIfSwitchAdded() {
	if (CS.getAttributeValue('Scenario_0') == scenario.Scenario_Express) {
		var currentCCodes = getCCode();
		var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

		var oneFlexOneFixedOneCombiLicence = getSwitchPortCode();

		var sumOneFlexOneFixedOneCombiLicence = 0;
		for (var idx in oneFlexOneFixedOneCombiLicence) {
			if (currentCCodesSum[oneFlexOneFixedOneCombiLicence[idx]]) {
				sumOneFlexOneFixedOneCombiLicence += currentCCodesSum[oneFlexOneFixedOneCombiLicence[idx]].quantity;
			}
		}

		var alreadyAdded = CS.getAttributeValue('SwitchPortAdded_0');

		if (sumOneFlexOneFixedOneCombiLicence == 0 && alreadyAdded == '') {
			priceItemAutomaticAddition('C108742', 'SwitchPortAdded_0', productTypes.Hardware);
		}
	}
};

window.addDesktop = function addDesktop() {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	var oneFlexOneFixedOneCombiLicence = getOneFlexOneFixedOneCombiLicence();

	var sumOneFlexOneFixedOneCombiLicence = 0;
	for (var idx in oneFlexOneFixedOneCombiLicence) {
		if (currentCCodesSum[oneFlexOneFixedOneCombiLicence[idx]]) {
			sumOneFlexOneFixedOneCombiLicence += currentCCodesSum[oneFlexOneFixedOneCombiLicence[idx]].quantity;
		}
	}

	var alreadyAdded = CS.getAttributeValue('ONEDesktopToolAdded_0');

	if (sumOneFlexOneFixedOneCombiLicence > 0 && alreadyAdded == '') {
		priceItemAutomaticAddition('C107975', 'ONEDesktopToolAdded_0', productTypes.AddOnLicenses);
	}
};

function getLANCodes() {
	var codes = [];

	codes.push('IPCLAS');
	codes.push('C106917');
	codes.push('C108032');

	return codes;
}

function getOneFlexOneFixedOneCombiLicence() {
	var codes = [];

	codes.push(cCodes.OneFlex.Enterprise);
	codes.push(cCodes.OneFlex.Express);

	codes.push(cCodes.OneFixed.Enterprise);
	codes.push(cCodes.OneFixed.Express);

	codes.push(cCodes.OneCombi.Enterprise);
	codes.push(cCodes.OneCombi.Express);
	/*
    //Enterprise
    codes.push('C107973');
    codes.push('C107971');
    codes.push('C112368');
    codes.push('RCONEVASTESEAT');
    codes.push('RCONECOMBISEAT');
    codes.push('RCONEMOBBASSEAT');
*/
	return codes;
}

function getOneFixedOneCombiLicence() {
	var codes = [];

	codes.push(cCodes.OneFixed.Enterprise);
	codes.push(cCodes.OneFixed.Express);
	codes.push(cCodes.OneCombi.Enterprise);
	codes.push(cCodes.OneCombi.Express);

	/*
    //Enterprise
    codes.push('C112369');
    codes.push('C112370');
    codes.push('C112368');
    codes.push('C107971');
    codes.push('RCONEVASTESEAT');
    codes.push('RCONECOMBISEAT');
*/
	return codes;
}

//ONE Call Center agent and supervisor flavours can only be added
//to the product basket once the ONE Call Center licenses have been added.
//If Sales tries to finalise their ONE Call Center configuration with agent and/or supervisor flavours without having added ONE Call Center licenses ,
//then CPQ will show an error message "To offer ONE Call Center Agent and/or Supervisor licenses, please add ONE Call Center licenses to your One Net configuration first."
window.callCenterAgentSupervisor = function callCenterAgentSupervisor() {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	console.log(currentCCodesSum);

	var agentSupervisorCodes = getOneCallAgentSupervisorLicence();

	var sumAgentSupervisor = 0;

	for (var idx in agentSupervisorCodes) {
		if (currentCCodesSum[agentSupervisorCodes[idx]]) {
			sumAgentSupervisor += currentCCodesSum[agentSupervisorCodes[idx]].quantity;
		}
	}

	console.log('sumAgentSupervisor sum ==' + sumAgentSupervisor);

	if (sumAgentSupervisor > 0) {
		var callCenterLicence = getOneCallCenterLicence();

		var sumOfCallCenterLicence = 0;

		for (var idx in callCenterLicence) {
			if (currentCCodesSum[callCenterLicence[idx]]) {
				sumOfCallCenterLicence += currentCCodesSum[callCenterLicence[idx]].quantity;
			}
		}
		console.log('sumOfCallCenterLicence sum ==' + sumOfCallCenterLicence);
		if (sumOfCallCenterLicence == 0) {
			console.log('ERROR MISS');
			CS.markConfigurationInvalid(
				'To offer ONE Call Center Agent and/or Supervisor licenses, please add ONE Call Center licenses to your One Net configuration first.'
			);
		}
	}
};

//ONE Integrate Lite does not apply to ONE, only to ONX and only with ONE Flex, ONE Fixed and ONE Combi licenses

function getOneCallCenterLicence() {
	var codes = [];
	//Enterprise
	codes.push('C108010');

	//Express
	codes.push('C108010');
	codes.push('RCONECALLSEAT');
	return codes;
}

function getOneCallAgentSupervisorLicence() {
	var codes = [];
	//Enterprise
	codes.push('C108011');
	codes.push('C108012');

	//Express
	codes.push('RCONECALLSUPERV');
	codes.push('RCONECALLAGENT');
	return codes;
}

//ONE Integrate Lite does not apply to ONE, only to ONX and only with ONE Flex, ONE Fixed and ONE Combi licenses
window.compatibleWithIntegrateLite = function compatibleWithIntegrateLite() {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	console.log(currentCCodesSum);

	var oneLiteCodes = getOneIntegrateLite();

	var sumOneIntegrateLite = 0;

	for (var idx in oneLiteCodes) {
		if (currentCCodesSum[oneLiteCodes[idx]]) {
			sumOneIntegrateLite += currentCCodesSum[oneLiteCodes[idx]].quantity;
		}
	}

	if (sumOneIntegrateLite > 0) {
		var flexFixCombiExCodes = getFlexFixCombiExpress();

		var sumOfFlexFixCombi = 0;

		for (var idx in flexFixCombiExCodes) {
			if (currentCCodesSum[flexFixCombiExCodes[idx]]) {
				sumOfFlexFixCombi += currentCCodesSum[flexFixCombiExCodes[idx]].quantity;
			}
		}

		if ((sumOfFlexFixCombi = 0)) {
			CS.markConfigurationInvalid(
				'ONE Integrate Lite does not apply to ONX, only to ONE and only with ONE Flex, ONE Fixed and ONE Combi licenses'
			);
		}
	}
};

//ONE Integrate Lite does not apply to ONE, only to ONX and only with ONE Flex, ONE Fixed and ONE Combi licenses

function getFlexFixCombiExpress() {
	var codes = [];

	codes.push(cCodes.OneCombi.Enterprise);
	codes.push(cCodes.OneCombi.Express);

	codes.push(cCodes.OneFlex.Enterprise);
	codes.push(cCodes.OneFlex.Express);

	codes.push(cCodes.OneFixed.Enterprise);
	codes.push(cCodes.OneFixed.Express);
	/*
    codes.push('C108749');
    codes.push('C112369');
    codes.push('C112370');
    */
	return codes;
}

function getOneIntegrateLite() {
	var codes = [];
	codes.push(cCodes.OneNetBasic);
	codes.push(cCodes.OneNetIntegratePremium1);
	codes.push(cCodes.OneNetIntegratePremium2);
	/*
    codes.push('C109124');
    codes.push('RCONEINTBASIC');
    */
	return codes;
}

//One App, Reception/PC license, IVR/Keuzemenu, One Sync,
//One Integrate Plus, ONE Integrate Lite, ONE Call Center
//are only available as add-ons with ONE Fixed, ONE Flex and ONE Combi licenses
//and NOT with ONE Mobiel

//2019-change: One Integrate is MAX sum of (qty Fixed + Flex + Combi). So it can be sold if One Mobile is in config.
window.compatibleWithOneMobiel = function compatibleWithOneMobiel() {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	console.log(currentCCodesSum);
	var sumOfOneMobiel = 0;

	var sumOfNotCompatibleWithOneMobiel = 0;

	var sumOfFixedFlexCombi = 0;

	//codes Fixed, Flex Combi
	var codes = [];
	//Enterprise
	codes.push('C107971'); //fixed
	codes.push('C107973'); //flex
	codes.push('C112368'); //combi

	//Express
	codes.push('C108749'); //flex
	codes.push('C112370'); //combi
	//end codes

	var codesIntegrateBasicPremium = [];
	codesIntegrateBasicPremium.push('C108917'); //one integrate premium
	codesIntegrateBasicPremium.push('C108918'); //one integrate premium

	codesIntegrateBasicPremium.push('C109124'); //one integrate basic
	var sumOfIntegrateBasicPremium = 0;
	for (var idx in codesIntegrateBasicPremium) {
		if (currentCCodesSum[codesIntegrateBasicPremium[idx]]) {
			sumOfIntegrateBasicPremium += currentCCodesSum[codesIntegrateBasicPremium[idx]].quantity;
		}
	}

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfFixedFlexCombi += currentCCodesSum[codes[idx]].quantity;
		}
	}

	if (sumOfFixedFlexCombi < sumOfIntegrateBasicPremium) {
		CS.markConfigurationInvalid('One Integrate Basic and Premium are only available in combination with One Flex, One Fixed and One Combi');
	}
};

//One App, Reception/PC license, IVR/Keuzemenu, One Sync,
//One Integrate Plus, ONE Integrate Lite, ONE Call Center

//2019-change: One Integrate is MAX sum of (qty Fixed + Flex + Combi). So it can be sold if One Mobile is in config.
function getCodesNotCompatibleWithOneMobile() {
	var codes = [];

	codes.push('C109124'); //one integrate basic
	codes.push('C108010'); //one call centre

	codes.push('C111494'); //one app

	codes.push('C108026'); //one sync mobile
	codes.push('C108429'); //one sync

	//Express
	codes.push('C107976'); //one app
	codes.push('C106867'); //reception
	codes.push('C107977'); //ivr kreuzemenu
	codes.push('C108026'); //one sync mobile
	codes.push('C108028'); //one desktop integratie
	codes.push('C108917'); //one integrate premium
	codes.push('C108918'); //one integrate premium

	codes.push('C109124'); //one integrate basic
	codes.push('C108010'); //one call centre

	return codes;
}

//ONE Fixed, ONE Flex and ONE Comb
function getCodesFixedFlexCombi() {
	var codes = [];
	//Enterprise
	codes.push('C107971'); //fixed
	codes.push('C107973'); //flex
	codes.push('C112368'); //combi

	//Express
	codes.push('C112369'); //vast
	codes.push('C108749'); //flex
	codes.push('C112370'); //combi
	return codes;
}

function getCodesMobiel() {
	var codes = [];
	//Enterprise
	codes.push('C107974');

	//Express
	codes.push('C108751');

	return codes;
}

window.max1Wachtrij = function max1Wachtrij() {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	//same for Wachtrij addon. C codes for this product are: C108023 and C107978
	var codes = [];
	codes.push('C108023');
	//codes.push('C107978');

	sumOfLicences = 0;

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfLicences += currentCCodesSum[codes[idx]].quantity;
		}
	}
	if (sumOfLicences > 1) {
		CS.markConfigurationInvalid('Only 1 Wachtrij can be added per location for One Net Express.');
	}
};

window.max1IVRKeuzemenu = function max1IVRKeuzemenu() {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	var codes = getIVRKcode();

	var sumOfLicences = 0;

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfLicences += currentCCodesSum[codes[idx]].quantity;
		}
	}
	if (sumOfLicences > 1) {
		CS.markConfigurationInvalid('Only 1 IVR/Keuzemenu can be added per location for One Net Express.');
	}

	//same for Wachtrij addon. C codes for this product are: C108023 and C107978
	/*
     codes = [];
     codes.push('C108023');
     //codes.push('C107978');
     
     sumOfLicences = 0;
    
    for(var idx in codes){
        if(currentCCodesSum[codes[idx]]){
            sumOfLicences+= currentCCodesSum[codes[idx]].quantity;
        }
    }
    if(sumOfLicences>1){
        CS.markConfigurationInvalid('Only 1 Wachtrij can be added per location for One Net Express.'); 
    }
    */
};

//standalone one net

function getMobielAndFlex() {
	var codes = [];

	codes.push('C108749');
	codes.push('C107973');
	//codes.push('C108751');
	//C109357

	return codes;
}

window.minumum5FlexMobielLicences = function minumum5FlexMobielLicences() {
	//var currentCCodes = getCCode('oneNetStandalone');
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	var sumOfLicences = 0;

	var codes = getMobielAndFlex();

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfLicences += currentCCodesSum[codes[idx]].quantity;
		}
	}
	console.log('All codes per sum');
	console.log(currentCCodesSum);
	console.log('---end----');
	if (sumOfLicences < 5) {
		CS.markConfigurationInvalid('Please add a minimum of 5 Flex licenses.');
	}
};
//
window.minumum5Licences = function minumum5Licences() {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	var sumOfLicences = 0;

	var codes = getCombiFlexVastMobileCodes();

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfLicences += currentCCodesSum[codes[idx]].quantity;
		}
	}
	console.log('All codes per sum');
	console.log(currentCCodesSum);
	console.log('---end----');
	if (sumOfLicences < 5) {
		CS.markConfigurationInvalid(
			'Please add a minimum of 5 licenses (ONE Combi, ONE Flex, ONE Fixed, ONE Mobiel or a combination between them) to your location.'
		);
	}
};

function getIVRKcode() {
	var codes = [];
	codes.push('C107977');
	return codes;
}

function getCombiFlexVastMobileCodes() {
	var codes = [];
	codes.push('C112370');
	codes.push('C108749');
	codes.push('AO_ONX_Fixed_Seat');
	//codes.push('C109357'); ATA VAST EXCLUDED
	codes.push('RCONEATAPROFIEL');
	codes.push('C108751');
	codes.push('C112369');

	return codes;
}

/**
 * UAT new request:
 * The total of Combi or Fixed core licenses must be equal to the total of Addons in Addon group 'Hardware to Buy' or 'Hardware to Rent'
 */
window.combiFixedMatchHardwareToBuyAndHardwareToRent = function combiFixedMatchHardwareToBuyAndHardwareToRent() {
	var hardwareToBuyAddonGroup = 'Hardware to Buy';
	var hardwareToRentAddonGroup = 'Hardware to Rent';

	// get number of fixed & combi core licence addons
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	var sumOfFixedAndCombiLicences = 0;

	var codes = getOneFixedOneCombiLicence();

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfFixedAndCombiLicences += currentCCodesSum[codes[idx]].quantity;
		}
	}

	//get number of 'Hardware to Buy' and 'Hardware to Rent' categiry addons
	var currentCCodes = getProductAddonGroup(productTypes.AddOnLicenses);
	var currentCCodesSum = getTotalQuantityPerAddonGroup(currentCCodes);

	console.log('currentCCodesSum group = ' + JSON.stringify(currentCCodesSum));

	var sumOfHardwareToBuyRent = 0;
	var listOfHardwareGroupsToBuyAndRent = [];
	listOfHardwareGroupsToBuyAndRent.push('Phones to Rent');
	listOfHardwareGroupsToBuyAndRent.push('Phones to Buy');
	listOfHardwareGroupsToBuyAndRent.push('Analog Devices to Buy');
	listOfHardwareGroupsToBuyAndRent.push('Analog Devices to Rent');
	listOfHardwareGroupsToBuyAndRent.push('Accessories to Buy');
	listOfHardwareGroupsToBuyAndRent.push('Accessories to Rent');

	for (var idx in listOfHardwareGroupsToBuyAndRent) {
		var item = listOfHardwareGroupsToBuyAndRent[idx];
		if (currentCCodesSum[item]) {
			sumOfHardwareToBuyRent += currentCCodesSum[item].quantity;
		}
	}

	console.log('Num of hardware = ' + sumOfHardwareToBuyRent);

	if (sumOfHardwareToBuyRent != sumOfFixedAndCombiLicences) {
		CS.markConfigurationInvalid(
			'The total of Combi or Fixed core licenses must be equal to the total of Hardware to Buy and Hardware to Rent addons'
		);
	}
};

/**
 *
 *CPQ applies a one-off site installation fee to each location with One Fixed and/or One Combi core licenses automatically
 **--> For ONE the fee is a tiered fee which varies with the number of One Fixed AND One Combi core licenses per location
 **--> For ONX the fee is flat
 *
 */

window.addSiteInstallationFeeToEachLocation = function addSiteInstallationFeeToEachLocation(cCode, flag) {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	var sumOfLicences = 0;

	var codes = getOneFixedOneCombiLicence();

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfLicences += currentCCodesSum[codes[idx]].quantity;
		}
	}
	console.log('All codes per sum');
	console.log(currentCCodesSum);
	console.log('---end----');

	var alreadyAdded = CS.getAttributeValue(flag);
	if (sumOfLicences > 0 && alreadyAdded == '') {
		priceItemAutomaticAddition(cCode, flag, productTypes.AddOnLicenses);
	}
};
window.addSiteInstallationFeeToEachLocationEnterprise = function addSiteInstallationFeeToEachLocationEnterprise(flag) {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	var sumOfLicences = 0;

	var codes = getOneFixedOneCombiLicence();

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfLicences += currentCCodesSum[codes[idx]].quantity;
		}
	}
	console.log('All codes per sum');
	console.log(currentCCodesSum);
	console.log('---end----');

	var alreadyAdded = CS.getAttributeValue(flag);
	if (alreadyAdded == '') {
		CS.setAttributeValue('Sum_Of_Licences_0', sumOfLicences);
		if (sumOfLicences > 0 && sumOfLicences <= 5) {
			priceItemAutomaticAddition('C108017', flag, productTypes.AddOnLicenses);
		} else if (sumOfLicences > 5 && sumOfLicences <= 15) {
			priceItemAutomaticAddition('C108018', flag, productTypes.AddOnLicenses);
		} else if (sumOfLicences > 15 && sumOfLicences <= 50) {
			priceItemAutomaticAddition('C108019', flag, productTypes.AddOnLicenses);
		} else if (sumOfLicences > 51 && sumOfLicences <= 100) {
			priceItemAutomaticAddition('C108020', flag, productTypes.AddOnLicenses);
		} else if (sumOfLicences > 101 && sumOfLicences <= 200) {
			priceItemAutomaticAddition('C108021', flag, productTypes.AddOnLicenses);
		} else if (sumOfLicences > 200) {
			priceItemAutomaticAddition('C108022', flag, productTypes.AddOnLicenses);
		}
	}
};

/**
 *
 *One Onbeperkt Vast Bellen national will be added automatically to ONX quotes with ONE Fixed licenses and its price items will be discounted by 100%; for ONE this is an optional add-on that is available with ONE Fixed licenses
 *
 */
window.addOVBN = function addOVBN(flag) {
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	var sumOfLicences = 0;

	var codes = getOneFixedCodes();

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfLicences += currentCCodesSum[codes[idx]].quantity;
		}
	}
	console.log('All codes per sum');
	console.log(currentCCodesSum);
	console.log('---end----');

	var alreadyAdded = CS.getAttributeValue(flag);

	if (sumOfLicences > 0 && alreadyAdded == '') {
		//priceItemAutomaticAddition(cCode,flag, productTypes.AddOnLicenses);
		priceItemAdditionPerQuery(flag, productTypes.AddOnLicenses, 'OVBOneNetAutomaticRecQuery', 'OVBLookup_0', '-OVB', sumOfLicences.toString());
	}
};

/**
 *
 *For ONE ONLY (and not for ONX), CPQ applies a one-off implementation fee per location per switch
 *
 */
window.oneOffImplementationFeePerLocationPerSwitch = function oneOffImplementationFeePerLocationPerSwitch(groupNameForSwitch, cCode, flag) {
	var currentCCodes = getProductAddonGroup(productTypes.Hardware);
	var currentCCodesSum = getTotalQuantityPerAddonGroup(currentCCodes);

	var sumOfCertainGroup = 0;

	if (currentCCodesSum[groupNameForSwitch]) {
		sumOfCertainGroup += currentCCodesSum[groupNameForSwitch].quantity;
	}

	var alreadyAdded = CS.getAttributeValue(flag);

	if (sumOfCertainGroup > 0 && alreadyAdded == '') {
		//add item
		priceItemAutomaticAddition(cCode, flag, productTypes.Hardware);
	}
};

window.getProductAddonGroup = function getProductAddonGroup(relType) {
	var addonGroup = 'Category_0';
	var quantity = 'Quantity_0';
	var results = [];
	for (var x in CS.Service.config[relType].relatedProducts) {
		var itm = CS.Service.config[relType].relatedProducts[x];
		if (itm.reference) {
			var result = {};
			result.addonGroup = CS.getAttributeValue(itm.reference + ':' + addonGroup);
			result.quantity = CS.getAttributeValue(itm.reference + ':' + quantity);
			results.push(result);
		}
	}
	return results;
};

window.getTotalQuantityPerAddonGroup = function getTotalQuantityPerAddonGroup(allCCodes) {
	var totals = {};
	for (var x in allCCodes) {
		if (allCCodes[x].addonGroup != '' && allCCodes[x].addonGroup != undefined) {
			if (totals[allCCodes[x].addonGroup]) {
				totals[allCCodes[x].addonGroup].quantity += allCCodes[x].quantity;
			} else {
				totals[allCCodes[x].addonGroup] = {};
				totals[allCCodes[x].addonGroup].quantity = allCCodes[x].quantity;
			}
		}
	}

	return totals;
};

window.getCCode = function getCCode() {
	var oneOffCCode = 'CCodeOneOff_0';
	var recurringCCode = 'CCodeRecurring_0';
	var quantity = 'Quantity_0';
	var results = [];

	var ids = ['Add_on_Licenses_0', 'Hardware_0', 'Core_Licenses_0', 'Implementation_and_Training_0'];

	ids.forEach(function (element) {
		results = results.concat(queryRelProduct(element, oneOffCCode, recurringCCode, quantity));
	});

	return results;
};

function queryRelProduct(identificator, oneOffCCode, recurringCCode, quantity) {
	var results = [];
	if (CS.Service.config[identificator]) {
		for (var x in CS.Service.config[identificator].relatedProducts) {
			var itm = CS.Service.config[identificator].relatedProducts[x];
			if (itm.reference) {
				var result = {};
				result.oneOffCCode = CS.getAttributeValue(itm.reference + ':' + oneOffCCode);
				result.recurringCCode = CS.getAttributeValue(itm.reference + ':' + recurringCCode);
				result.quantity = CS.getAttributeValue(itm.reference + ':' + quantity);
				results.push(result);
			}
		}
	}
	return results;
}

window.getTotalQuantityPerCode = function getTotalQuantityPerCode(allCCodes) {
	var totals = {};
	for (var x in allCCodes) {
		if (allCCodes[x].oneOffCCode != '' && allCCodes[x].oneOffCCode != undefined) {
			if (totals[allCCodes[x].oneOffCCode]) {
				totals[allCCodes[x].oneOffCCode].quantity += allCCodes[x].quantity;
			} else {
				totals[allCCodes[x].oneOffCCode] = {};
				totals[allCCodes[x].oneOffCCode].quantity = allCCodes[x].quantity;
			}
		}

		if (allCCodes[x].recurringCCode != '' && allCCodes[x].recurringCCode != undefined) {
			if (totals[allCCodes[x].recurringCCode]) {
				totals[allCCodes[x].recurringCCode].quantity += allCCodes[x].quantity;
			} else {
				totals[allCCodes[x].recurringCCode] = {};
				totals[allCCodes[x].recurringCCode].quantity = allCCodes[x].quantity;
			}
		}
	}

	return totals;
};

/**
 * Price Item Addition
 */
function doLookupQueryAndInvalidateAddons(name, dynamicFilterMap, productDefinitionId, attWrapper) {
	return new Promise(function (resolve, reject) {
		Visualforce.remoting.Manager.invokeAction(
			'cscfga.UISupport.doMultiRowLookupQuery',
			name,
			dynamicFilterMap,
			productDefinitionId,
			function (result, event) {
				if (event.status) {
					var pgaResult = new Object();

					//pgaResult.dynamicFilterMap = dynamicFilterMap;
					pgaResult.lookupQueryName = name;
					pgaResult.attribute = attWrapper;
					pgaResult.res = result == null ? null : result;

					//pgaResult.productDefinitionId = productDefinitionId;
					//pgaResult.res = result==null ? undefined : result;

					resolve(pgaResult);
				} else {
					console.error(event);

					reject(event);
				}
			},
			{ escape: false }
		);
	});
}

function priceItemsAddition(rel, csAddon, category, additionSufix, productQuantity) {
	console.log('Started with addition');
	var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
	var relProductRef = rel;
	var lookupAttr = lookupAttr;

	var attr = CS.Service.config[rel];
	var emptyPromise = Promise.resolve();
	var parent = CS.getConfigurationWrapper(CS.Util.getParentReference(attr.reference));
	var defId = CS.Service.getAvailableProducts(attr.reference)[0].cscfga__Product_Definition__c;
	loadProduct(defId)
		.then(function () {
			CS.Service.addRelatedProduct(attr.reference, defId, false);
			var indexNumberOfCopiedRelProd = CS.getAttributeWrapper(relProductRef).relatedProducts.length;
			var indexItem = indexNumberOfCopiedRelProd - 1;

			var wrapperDef = relProductRef.substring(0, relProductRef.length - 1);
			var wrapperDef = relProductRef.substring(0, relProductRef.length - 1);

			var defStrItem = wrapperDef + indexItem + ':Product_0';

			var defStrQuantity = wrapperDef + indexItem + ':Quantity_0';

			var defCategory = wrapperDef + indexItem + ':Category_0';

			var defAdditionMethod = wrapperDef + indexItem + ':AdditionMethod_0';
			CS.setAttributeValue(defStrItem, csAddon);

			productQuantity = productQuantity || '1';
			CS.setAttributeValue(defStrQuantity, productQuantity);
			//CS.setAttributeValue(defStrQuantity,'1');
			CS.setAttributeValue(defCategory, category);
			var additionString = 'Automatic' + additionSufix;
			CS.setAttributeValue(defAdditionMethod, additionString);
			CS.Service.config[defStrQuantity].attr.cscfga__Value__c = 1;
			//CS.Service.config[wrapperDef+(indexItem)].customLookupReferencedAttributes = getCustomAttributesPerRef(relProductRef);
		})

		.then(function () {
			CS.Rules.evaluateAllRules();
		});
}

window.priceItemAdditionPerQuery = function priceItemAdditionPerQuery(
	flag,
	relType,
	queryName,
	queryLookupAtt,
	additionSuffix,
	quantityOnAddedProduct
) {
	quantityOnAddedProduct = quantityOnAddedProduct || 1;
	var flagVal = CS.getAttributeValue(flag);
	//debugger;
	console.log('---Checking flag ==' + flag);
	if (flagVal == '') {
		console.log('---Value will be set ==' + flagVal);
		CS.setAttributeValue(flag, 'set');
		var prodId = CS.Service.config[''].config.cscfga__Product_Definition__c;
		var newDyn = {};
		doLookupQueryAndInvalidateAddons(queryName, newDyn, prodId, CS.Service.config[queryLookupAtt]).then(function (pgaResult) {
			if (pgaResult.res != null) {
				_.each(pgaResult.res, function (result) {
					//priceItemsAddition('Addons_0',result.Id);
					priceItemsAddition(relType, result.Id, result.Category__c, additionSuffix, quantityOnAddedProduct);
				});
			}
		});
	} else {
		console.log('---Value will NOT be set ==' + flagVal);
	}
};

window.priceItemAutomaticAddition = function priceItemAutomaticAddition(cCode, flag, relType) {
	var flagVal = CS.getAttributeValue(flag);
	//debugger;
	console.log('---Checking flag ==' + flag);
	if (flagVal == '') {
		console.log('---Value will be set ==' + flagVal);
		CS.setAttributeValue(flag, 'set');
		var prodId = CS.Service.config[''].config.cscfga__Product_Definition__c;
		var newDyn = {};
		//newDyn["CS_PricePlan"] = CS.getAttributeValue('CS_PricePlan_0');
		newDyn['TempVal'] = cCode;
		doLookupQueryAndInvalidateAddons('PriceItemFilterAutomatic', newDyn, prodId, CS.Service.config['AutomaticLookup_0']).then(function (
			pgaResult
		) {
			if (pgaResult.res == null) {
				doLookupQueryAndInvalidateAddons('PriceItemFilterAutomaticRec', newDyn, prodId, CS.Service.config['AutomaticLookup_0']).then(
					function (pgaResult) {
						_.each(pgaResult.res, function (result) {
							console.log('2nd option');
							//priceItemsAddition('Addons_0',result.Id);
							priceItemsAddition(relType, result.Id, result.Category__c, '');
						});
					}
				);
			} else {
				_.each(pgaResult.res, function (result) {
					//priceItemsAddition('Addons_0',result.Id);
					priceItemsAddition(relType, result.Id, result.Category__c, '');
				});
			}
		});
	} else {
		console.log('---Value will NOT be set ==' + flagVal);
	}
};

/**
 *
 * Used in Company Level Fixed voice ruling - related to one net scenario
 */
window.companyLeveFixedVoiceOneNetScenarioProjectManagementValidation = function companyLeveFixedVoiceOneNetScenarioProjectManagementValidation() {
	console.log('--CLFV--');
	var currentCCodes = getCCode();
	var currentCCodesSum = getTotalQuantityPerCode(currentCCodes);

	console.log('currentCCodes');

	console.log(currentCCodes);

	var codes = getProductManagementCode();

	var sumOfLicences = 0;

	for (var idx in codes) {
		if (currentCCodesSum[codes[idx]]) {
			sumOfLicences += currentCCodesSum[codes[idx]].quantity;
		}
	}
	console.log('sumOfLicences=' + sumOfLicences);
	if (sumOfLicences > 0) {
		CS.markConfigurationInvalid('Basis project management is available only with One Net Enterprise.');
	}
};

function getProductManagementCode() {
	var codes = [];
	codes.push('C108016');
	return codes;
}

//UAT-1-2-2019
//Mobile ccodes are C108751 and C107974
window.getCCodeAndGroup = function getCCodeAndGroup(oneNetProductArray) {
	var oneOffCCode = 'CCodeOneOff_0';
	var recurringCCode = 'CCodeRecurring_0';
	var quantity = 'Quantity_0';
	var category = 'Category_0';
	var results = [];

	//var ids = ["Add_on_Licenses_0", "Hardware_0", "Core_Licenses_0", "Implementation_and_Training_0"];
	console.log(oneNetProductArray);
	oneNetProductArray.forEach(function (element) {
		console.log(element);
		results = results.concat(queryRelCcodeGroup(element, oneOffCCode, recurringCCode, quantity, category));
	});

	return results;
};

function queryRelCcodeGroup(identificator, oneOffCCode, recurringCCode, quantity, category) {
	var results = [];
	if (CS.Service.config[identificator]) {
		for (var x in CS.Service.config[identificator].relatedProducts) {
			var itm = CS.Service.config[identificator].relatedProducts[x];
			if (itm.reference) {
				var result = {};
				result.oneOffCCode = CS.getAttributeValue(itm.reference + ':' + oneOffCCode);
				result.recurringCCode = CS.getAttributeValue(itm.reference + ':' + recurringCCode);
				result.quantity = CS.getAttributeValue(itm.reference + ':' + quantity);
				result.category = CS.getAttributeValue(itm.reference + ':' + category);
				results.push(result);

				console.log(CS.getAttributeValue(itm.reference + ':' + oneOffCCode));
				console.log(CS.getAttributeValue(itm.reference + ':' + recurringCCode));
			}
		}
	}
	return results;
}

function groupCCodesPerGroup(currentCCodes, prodTypes) {
	var categoryStr = 'Category_0';
	var quantityStr = 'Quantity_0';
	var ccodeOFStr = 'CCodeOneOff_0';
	var ccodeReccStr = 'CCodeRecurring_0';

	var results = {};
	prodTypes.forEach(function (element) {
		for (var x in CS.Service.config[element].relatedProducts) {
			var itm = CS.Service.config[element].relatedProducts[x];
			if (itm.reference) {
				var category = CS.getAttributeValue(itm.reference + ':' + categoryStr);
				var quantity = CS.getAttributeValue(itm.reference + ':' + quantityStr);
				var ccodeOF = CS.getAttributeValue(itm.reference + ':' + ccodeOFStr);
				var ccodeRecc = CS.getAttributeValue(itm.reference + ':' + ccodeReccStr);

				if (results[category]) {
					if (ccodeOF != '') {
						results[category].push(ccodeOF);
					}
					if (ccodeRecc != '') {
						results[category].push(ccodeRecc);
					}
				} else {
					results[category] = [];
					if (ccodeOF != '') {
						results[category].push(ccodeOF);
					}
					if (ccodeRecc != '') {
						results[category].push(ccodeRecc);
					}
				}
			}
		}
	});

	return results;
}

window.onlyMobileNotValid = function onlyMobileNotValid() {
	var coreLicences = CS.Service.config['Core_Licenses_0'].relatedProducts;
	var dependentConfigurations = CS.getAttributeValue('Dependent_Configurations_0');
	var vodafoneCallingPresent =
		dependentConfigurations != undefined && dependentConfigurations != '' && dependentConfigurations.includes('[Vodafone Calling]');

	var hasOneMobiel = false;
	var hasOtherLicence = false;

	coreLicences.forEach(function (licence) {
		if (licence.config.Name == 'One Mobiel') {
			hasOneMobiel = true;
		} else {
			hasOtherLicence = true;
		}
	});

	/*if (hasOneMobiel && !hasOtherLicence && !vodafoneCallingPresent) {
        CS.markConfigurationInvalid('Only mobile seat licenses are not allowed. Please add another seat license.'); 
    } */
	var vastOpMobiel = CS.getAttributeValue('Add_on_Licenses_0:CCodeRecurring_0');
	if (!hasOneMobiel && !hasOtherLicence && vastOpMobiel != 'C113861') {
		CS.markConfigurationInvalid('Please add a seat licence.');
	}
};

window.disableAutomatic = function disableAutomatic() {
	productTypesArray.forEach(function (item, index, array) {
		if (item != undefined) {
			relType = item;
			if (CS.Service.config[relType]) {
				if (CS.Service.config[relType] && CS.getAttributeWrapper(relType)) {
					if (CS.getAttributeWrapper(relType).relatedProducts.length > 0) {
						for (var idx in CS.getAttributeWrapper(relType).relatedProducts) {
							var withoutIndex = relType.substr(0, relType.lastIndexOf('_')) + '_';

							//if(CS.Service.config[withoutIndex+idx+":AdditionMethod_0"]&&(CS.getAttributeValue(withoutIndex+idx+":AdditionMethod_0") == 'Automatic')){
							if (
								CS.Service.config[withoutIndex + idx + ':AdditionMethod_0'] &&
								CS.getAttributeValue(withoutIndex + idx + ':AdditionMethod_0').indexOf('Automatic') > -1
							) {
								jQuery('[name="' + withoutIndex + idx + ':Product_0"]')
									.parent()
									.find('input, textarea, select')
									.attr('readonly', 'readonly');
								jQuery('[name="' + withoutIndex + idx + ':Category_0"]')
									.parent()
									.find('input, textarea, select')
									.attr('disabled', 'disabled');
								jQuery('[name="' + withoutIndex + idx + ':Quantity_0"]')
									.parent()
									.find('input, textarea, select')
									.attr('readonly', 'readonly');

								jQuery("span[data-cs-ref='" + withoutIndex + idx + "'][data-cs-action='removeRelatedProduct']").hide();
							}
						}
					}
				}
			}
		}
	});
};
