if (!CS || !CS.SM) {
	throw Error('Solution Console Api not loaded!');
}

if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', registerBasketPlugins);
}

let basketPlugin;

async function registerBasketPlugins() {
	const settings = {
		serviceOnly: false,
		type: 'basket',
		name: 'SC_BasketPlugin',
		schemaName: '', // empty because isDefault is 'true'
		weight: -1,
		isDefault: true,
		enabled: true
	};

	basketPlugin = await CS.SM.solutionManager.registerPlugin('', settings);
	updateBasket(basketPlugin);

	console.log('Basket plugin registered!');
	return;
}

function updateBasket(Plugin) {
	Plugin.beforeBasketSubmit = beforeBasketSubmit;
	Plugin.afterBasketSubmit = afterBasketSubmit;
}

async function afterBasketSubmit(basketSubmitResult, solutionConfigMap) {
	console.log('afterBasketSubmit hook');
	return true;
}

async function beforeBasketSubmit(solutions) {
	console.warn('beforeBasketSubmit hook');
	console.log(solutions);

	for (const solution of solutions) {
		switch (solution.name) {
			case SC_SOLUTIONS.BUSINESS_MOBILE_SOLUTION:
				let validationResult = await businessMobileValidations(solution);
				if (!validationResult) {
					return false;
				}
				break;
			default:
				console.warn('Behavior not specified for ' + solution.name);
				break;
		}
	}
	return true;
}
