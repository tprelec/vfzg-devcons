// SC_InternetModemOnly
async function afterInternetModemOnlyAttributeUpdated(component, configuration, attribute, oldValueMap) {
	if (attribute.name === SC_COMMON_ATTRIBUTES.CONTRACT_DURATION) {
		await updateInternetModemOnlyName(component, configuration);
	} else if (attribute.name === SC_COMMON_ATTRIBUTES.SITE_AVAILABILITY) {
		await updateInternetModemOnlyName(component, configuration);
	} else if (attribute.name === SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT) {
		await updateInternetModemOnlyName(component, configuration);
	}
}
// Set Internet Name
async function updateInternetModemOnlyName(component, configuration) {
	let siteName = getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.SITE);
	let productVariantName = configuration.attributes.productvariant.displayValue;
	let contractDuration = getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.CONTRACT_DURATION);
	let name = siteName + ' - ' + productVariantName + ', ' + contractDuration + ' maanden';
	const attributeData = [
		{
			name: SC_COMMON_ATTRIBUTES.INTERNET_NAME,
			value: name
		}
	];

	return component.updateConfigurationAttribute(configuration.guid, attributeData);
}

async function checkInternetModemOnlyForDeletion(configuration) {
	let accessguid = SC_Util.getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.GUID);
	console.log('DELETED Access with ID: ' + accessguid);
	const activeSolution = await CS.SM.getActiveSolution();
	const internetModemOnlyComponent = activeSolution.getComponentByName(SC_COMPONENTS.INTERNETMODEMONLY_COMPONENT);
	let internetConfigs = internetModemOnlyComponent.getAllConfigurations();
	let internetguids = [];
	if (internetConfigs !== undefined) {
		for (const internetConfig of Object.values(internetConfigs)) {
			if (internetConfig.attributes.accessguid.value === accessguid) {
				internetguids.push(SC_Util.getAttributeValue(internetConfig, SC_COMMON_ATTRIBUTES.GUID));
			}
		}
		internetguids.forEach((item) => {
			internetModemOnlyComponent.deleteConfiguration(item);
			console.log('Deleting internet modem only: ' + item);
		});
	}
}

async function addInternetConfigToInflightBasket(configuration) {
	let accessguid = SC_Util.getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.GUID);
	console.log('Access with ID' + accessguid + ' is about to be deleted from Inflight basket');
	const activeSolution = await CS.SM.getActiveSolution();
	const internetModemOnlyComponent = activeSolution.getComponentByName(SC_COMPONENTS.INTERNETMODEMONLY_COMPONENT);
	let internetConfigs = internetModemOnlyComponent.getAllConfigurations();
	let internetguids = [];
	if (internetConfigs !== undefined) {
		for (const internetConfig of Object.values(internetConfigs)) {
			if (internetConfig.attributes.accessguid.value === accessguid) {
				internetguids.push(SC_Util.getAttributeValue(internetConfig, SC_COMMON_ATTRIBUTES.GUID));
			}
		}
		if (internetguids.length > 0) {
			const basket = await CS.SM.getActiveBasket();
			const solution = await CS.SM.getActiveSolution();
			const response = await basket.addConfigurationsToMACBasket(solution.id, internetguids);
			console.log('addConfigurationsToMACBasket response: ' + JSON.stringify(response));
		}
	}
}

async function checkIfInternetConfigInInflightBasket(configuration) {
	let accessguid = SC_Util.getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.GUID);
	console.log('Access with ID' + accessguid + ' is about to be deleted from Inflight basket, checking respective Internet config');
	const activeSolution = await CS.SM.getActiveSolution();
	const internetModemOnlyComponent = activeSolution.getComponentByName(SC_COMPONENTS.INTERNETMODEMONLY_COMPONENT);
	let internetConfigs = internetModemOnlyComponent.getAllConfigurations();
	if (internetConfigs !== undefined) {
		for (const internetConfig of Object.values(internetConfigs)) {
			if (internetConfig.attributes.accessguid.value === accessguid && internetConfig.disabled) {
				CS.SM.displayMessage(SC_WARNING_MESSAGES.ADD_CONFIGURATION_TO_INFLIGHT_BASKET + internetConfig.name, 'warning');
				return false;
			}
		}
	}
	return true;
}

async function beforeInternetModemOnlyConfigurationDelete(component, configuration) {
	const activeSolution = await CS.SM.getActiveSolution();
	const accessModemOnlyComponent = activeSolution.getComponentByName(SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT);
	let accessConfigs = accessModemOnlyComponent.getAllConfigurations();
	let isRelatedIntConfig = false;
	if (accessConfigs !== undefined) {
		for (const accessConfig of Object.values(accessConfigs)) {
			if (accessConfig.guid === configuration.attributes.accessguid.value && accessConfig.softDeleted !== true) {
				isRelatedIntConfig = true;
				CS.SM.displayMessage(SC_WARNING_MESSAGES.CANNOT_DELETE_INTERNET_MODEM_ONLY, 'warning');
				return false;
			}
		}
	}
	return true;
}

/**
 * @description Setting the default quantity value for automatically defined Addons
 */
async function updateAddOnQuanity(relatedProduct) {
	let quantity;
	const quantityAom = relatedProduct.configuration.attributes.aomquantity.value;

	if (quantityAom !== undefined) {
		quantity = quantityAom;
	} else {
		quantity = 1;
	}

	relatedProduct.configuration.attributes.aomquantity.displayValue = quantity;
	relatedProduct.configuration.attributes.aomquantity.value = quantity;
	return true;
}
