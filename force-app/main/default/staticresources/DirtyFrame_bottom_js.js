(function() {
    if (typeof PARENT_WINDOW_ORIGIN === 'undefined'
            || typeof PAGE_NAME === 'undefined') {
        return;
    }

    var dirtyFrame = new DirtyFrame( PARENT_WINDOW_ORIGIN );
    dirtyFrame.sendPageHeightMessage( PAGE_NAME );
})();