// SC_OneMobile_Plan
/**
 * Plugin for OneMobile Plan
 * NOTE: Plugin must be invoked after SC_OneMobile_Solution.js
 */

async function afterOneMobilePlanAttributeUpdated(component, configuration, attribute, oldValueMap) {

    if (attribute.name === SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT) {
        await updateMobilePlanName(component, configuration);
    } else if (attribute.name === SC_COMMON_ATTRIBUTES.QUANTITY) {
        if (configuration.orderEnrichmentList.length != 0) {
            const allOEConfig = Object.values(Object.values(configuration.getOrderEnrichments())[0]);
            var configurationQuantity = getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.QUANTITY);

            await afterQuantityAttributeUpdated(configuration, allOEConfig, configurationQuantity);
        }
    } else if (attribute.name === SC_COMMON_ATTRIBUTES.BASE_MRC && attribute.value != '') {
        configuration.recurringPrice = parseFloat(getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.BASE_MRC));
    }

    return true;
}

async function updateAttributeValue(component, configuration, attributeData) {
    
}

// Set Mobile Plan Name
async function updateMobilePlanName(component, configuration) {
    //let tier = getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.TIER);
    let productVariantName = getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_NAME);

    //let name = tier + " - " + productVariantName;
    let name = 'One Mobile' + " - " + productVariantName;

    const attributeData = [{
        name: SC_COMMON_ATTRIBUTES.MOBILE_PLAN_NAME,
        value: name
    }];

    return component.updateConfigurationAttribute(configuration.guid, attributeData);
}

// Function to invalidate conf. if all Add On from group Data and Voice upgrades are deleted
// and Add On from group Roaming Upgrades is present on the configuration
async function beforeOneMobilePlanRelatedProductDelete(component, configuration, relatedProduct) {
    if(!relatedProduct || relatedProduct.type !== SC_RELATED_PRODUCT_TYPES.ADDON) {
        return;
    }

    const roamingGroupAddons = configuration.relatedProductList.filter(rp => rp.groupName === SC_ADDON_GROUPS.ROAMING_UPGRADES);
    const dataAndVoiceGroupAddons = configuration.relatedProductList.filter(rp => rp.groupName === SC_ADDON_GROUPS.DATA_AND_VOICE_UPGRADES);

    if (roamingGroupAddons && roamingGroupAddons.length > 0 &&
        relatedProduct.groupName === SC_ADDON_GROUPS.DATA_AND_VOICE_UPGRADES &&
        dataAndVoiceGroupAddons && dataAndVoiceGroupAddons.length === 1) {
            CS.SM.displayMessage(SC_WARNING_MESSAGES.CANNOT_DELETE_LAST_VOICE_AND_DATA, 'warning');
            return false;
    }

    return true;
}

async function afterOneMobilePlanRelatedProductAdd(component, configuration, relatedProduct) {

    // If the AddOn you're trying to add is from group Roaming and there aren't any Addons from group Data and Voice,
    // finish hook execution and prevent adding
    if (relatedProduct.type === SC_RELATED_PRODUCT_TYPES.ADDON && relatedProduct.groupName === SC_ADDON_GROUPS.ROAMING_UPGRADES) {

        const dataAndVoiceGroupAddons = configuration.relatedProductList.filter(rp => rp.groupName === SC_ADDON_GROUPS.DATA_AND_VOICE_UPGRADES);

        if (dataAndVoiceGroupAddons && dataAndVoiceGroupAddons.length === 0) {
            await component.deleteAddon(configuration.guid, relatedProduct.guid, true);
            CS.SM.displayMessage(SC_WARNING_MESSAGES.CANNOT_ADD_ROAMING, 'warning');
        }

    }
}

// If the Quantity on all OE for specific configuration doesn't match the Quantity on the configuration,
// display warning message
async function afterOneMobilePlanOEAttributeUpdated(component, configuration, attribute, oldValueMap) {

    if (attribute.name === SC_COMMON_ATTRIBUTES.QUANTITY) {
        const solution = await CS.SM.getActiveSolution();
        // receives the parent configuration of changed OE
        const parentConfiguration = solution.getConfiguration(configuration.parentConfiguration);

        const allOEForParentConfig = Object.values(Object.values(parentConfiguration.getOrderEnrichments())[0]);
        var parentConfigurationQuantity = getAttributeValue(parentConfiguration, SC_COMMON_ATTRIBUTES.QUANTITY);

        await afterQuantityAttributeUpdated(parentConfiguration, allOEForParentConfig, parentConfigurationQuantity);
    } else if (attribute.name === SC_COMMON_ATTRIBUTES.CONNECTION_TYPE) {
        let status = true;
    	let message = '';
        const solution = await CS.SM.getActiveSolution();
        const parentConfiguration = solution.getConfiguration(configuration.parentConfiguration);

        const allOEForParentConfig = Object.values(Object.values(parentConfiguration.getOrderEnrichments())[0]);
        let selectedConnectionTypesMap = {};
        let duplicateConnectionType = false;
        allOEForParentConfig.forEach(confOE => {
            var connectionTypeValue = getAttributeValue(confOE, SC_COMMON_ATTRIBUTES.CONNECTION_TYPE);
            if (selectedConnectionTypesMap[connectionTypeValue] == undefined) {
                selectedConnectionTypesMap[connectionTypeValue] = 1;
            } else {
                selectedConnectionTypesMap[connectionTypeValue] += 1;
                duplicateConnectionType = true;
            }
        });
        
        if (duplicateConnectionType) {
            status = false;
            message = SC_WARNING_MESSAGES.DUPLICATE_CONNECTION_TYPE;
            CS.SM.displayMessage(SC_WARNING_MESSAGES.DUPLICATE_CONNECTION_TYPE, 'warning');
        }
        
        await validateConfiguration(parentConfiguration, status, message);
    }

    return true;
}

async function afterQuantityAttributeUpdated(parentConfiguration, allOEForParentConfig, parentConfigurationQuantity) {

    var quantityOECount = 0;
    var status = true;
    var message = '';

    allOEForParentConfig.forEach(confOE => {
            var OEQuantity = getAttributeValue(confOE, SC_COMMON_ATTRIBUTES.QUANTITY);
            quantityOECount += parseInt(OEQuantity);
        }
    )

    if (parseInt(parentConfigurationQuantity) != quantityOECount) {
        status = false;
        message = SC_WARNING_MESSAGES.QUANTITY_NOT_MATCH;
        CS.SM.displayMessage(SC_WARNING_MESSAGES.QUANTITY_NOT_MATCH, 'warning');
    }

    await validateConfiguration(parentConfiguration, status, message);
}