
var addOnManagerLoaded = false;
var addOnConfig = addOnConfig || {
	hideDefaultAddons: false,
	showManagerButton: true,
	hideAdded: true,
	deleteAddOnsOnTransition: false,
	allowQuantities: true,
	renderInSection: false
};

var aomPlugins = {};

function setAOMPlugin(plugin, event) {
	aomPlugins[event] = plugin;
}

function evaluatePlugins(event, params) {
	if (aomPlugins[event]) {
		return aomPlugins[event](params);
	} else {
		return null;
	}
}

function getObjectValues(o) {
	return Object.keys(o).map(function (k) {
		return o[k]
	})
}

// https://tc39.github.io/ecma262/#sec-array.prototype.includes
if (!Array.prototype.includes) {
	Object.defineProperty(Array.prototype, 'includes', {
		value: function (searchElement, fromIndex) {

			if (this == null) {
				throw new TypeError('"this" is null or not defined');
			}

			// 1. Let O be ? ToObject(this value).
			var o = Object(this);

			// 2. Let len be ? ToLength(? Get(O, "length")).
			var len = o.length >>> 0;

			// 3. If len is 0, return false.
			if (len === 0) {
				return false;
			}

			// 4. Let n be ? ToInteger(fromIndex).
			//    (If fromIndex is undefined, this step produces the value 0.)
			var n = fromIndex | 0;

			// 5. If n Ã¢â€°Â¥ 0, then
			//  a. Let k be n.
			// 6. Else n < 0,
			//  a. Let k be len + n.
			//  b. If k < 0, let k be 0.
			var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

			function sameValueZero(x, y) {
				return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
			}

			// 7. Repeat, while k < len
			while (k < len) {
				// a. Let elementK be the result of ? Get(O, ! ToString(k)).
				// b. If SameValueZero(searchElement, elementK) is true, return true.
				if (sameValueZero(o[k], searchElement)) {
					return true;
				}
				// c. Increase k by 1.
				k++;
			}

			// 8. Return false
			return false;
		}
	});
}

function loadAddOnManager() {
	if (!addOnManagerLoaded) {
		addOnManagerLoaded = true;
		CS.EventHandler.subscribe(CS.EventHandler.Event.CONFIGURATOR_READY, function (payload) {
			CS.AOM.WebService.runAddOnManager();
		});
		CS.EventHandler.subscribe(CS.EventHandler.Event.BUTTONS_DISPLAYED, function (payload) {
			CS.AOM.WebService.runAddOnManager();
			CS.AOM.UI.renderAOM();
		});
		CS.EventHandler.subscribe(CS.EventHandler.Event.RULES_FINISHED, function (payload) {
			CS.AOM.WebService.runAddOnManager();
			CS.AOM.UI.renderAOM();
			CS.AOM.initAOMData().then(function (res) {
				CS.AOM.checkMinMax().then(function (minMax) {
					let validationMessage = '';
					if (minMax) {
						getObjectValues(minMax).forEach(function (data) {
							if (data) {
								data.forEach(function (group) {
									if (group.msg && group.msg !== 'Ok') {
										if (group.count > group.max) {
											validationMessage += 'Please remove ' + (group.count - group.max) + ' add ons from ' + group.name + '.\n';
										} else {
											validationMessage += 'Please add ' + (group.min - group.count) + ' add ons from ' + group.name + '.\n';
										}
									}
								});
							}
						})
					}
					if (validationMessage && validationMessage !== '') {
						CS.markConfigurationInvalid('', validationMessage);
					}
				});
			});
		});


		function initCSAddonApi(addOnConfig) {

			var settings = addOnConfig;
			var addOnData = {};
			var defaultAddOns = {};
			var incompatibleAddOns = {};
			var relatedProductData = {};
			var labels = void 0;

			function resetAOM() {
				addOnData = {};
				defaultAddOns = {};
				incompatibleAddOns = {};
				relatedProductData = {};
			}

			function initAOMData(data) {
				if (data) {
					relatedProductData = data.relatedProductData;
					var pluginResponse = evaluatePlugins('afterInit', data.addOnData);
					if (pluginResponse) {
						data.addOnData = pluginResponse;
					}
					setAOM(data.addOnData);
				} else {
					return CS.AOM.WebService.getRelatedProductData().then(function (rps) {
						relatedProductData = rps;
						return CS.AOM.WebService.getAddonPriceItemAssociations().then(function (data) {
							if (data.data) {
								setLookupRecords(data.data);
							}
							var pluginResponse = evaluatePlugins('afterInit', data);
							if (pluginResponse) {
								data = pluginResponse;
							}
							if (data) {
								return setAOM(data);
							} else {
								return Promise.resolve({});
							}
						});
					});
				}
			}

			function setLookupRecords(data) {
				Object.keys(data).forEach(key => {
					data[key]?.forEach(association => {
						var newLookupRecord = {};
						Object.keys(association).forEach(assocKey => {
							if (assocKey.endsWith('__r')) {
								Object.keys(association[assocKey]).forEach(assocRelKey => {
									newLookupRecord[assocKey.toLowerCase() + '.' + assocRelKey.toLowerCase()] = association[assocKey][assocRelKey];
								})
							} else {
								newLookupRecord[assocKey.toLowerCase()] = association[assocKey];
							}
						})
						if (CS.lookupRecords) {
							CS.lookupRecords[association.Id] = newLookupRecord;
						}
					})
				})
			}

			function c(data) {
				defaultAddOns = {};
				incompatibleAddOns = {};
				labels = data.labels;
				addOnData = createAddonMap(data.data);
				var pluginResponse = evaluatePlugins('afterAddOnsPrepared', addOnData);
				if (pluginResponse) {
					addOnData = pluginResponse;
				}
				CS.AOM.UI.renderAOM();
				return Promise.resolve(addOnData);
			}

			function createAddonMap(data) {
				var addonMap = Object.keys(data).map(function (key) {
					var defaultAddons = data[key]?.filter(function (item) {
						return item.cspmb__Default_Quantity__c && item.cspmb__Default_Quantity__c > 0;
					});
					if (!defaultAddons) {
						defaultAddons = [];
					}
					var allAddons = data[key]?.sort(compareBySequence);
					if (!allAddons) {
						allAddons = [];
					}
					var groupNames = allAddons.map(function (item) {
						return item.cspmb__Group__c;
					}).filter(function (value, index, a) {
						return a.indexOf(value) === index;
					});
					var addOnGroups = groupNames.map(function (key) {
						var addonsFromGroup = allAddons.filter(function (item) {
							return item.cspmb__Group__c === key;
						});
						var addonIds = addonsFromGroup.map(function (item) {
							return item.cspmb__Add_On_Price_Item__c;
						});
						var min = void 0,
							max = void 0;
						if (addonsFromGroup.length > 0) {
							min = addonsFromGroup[0].cspmb__Min__c;
							max = addonsFromGroup[0].cspmb__Max__c;
						}
						return {min: min, max: max, name: key, addOns: addonsFromGroup, addonIds: addonIds};
					});
					incompatibleAddOns[key] = [];
					for (var j = 0; j < relatedProductData.length; j++) {
						if (relatedProductData[j].definitionId === key) {
							for (var l = 0; l < relatedProductData[j].added.length; l++) {
								var found = false;
								var _addonValue = relatedProductData[j].added[l];
								for (var k = 0; k < addOnGroups.length; k++) {
									var grp = addOnGroups[k];
									if (grp.addonIds.includes(_addonValue)) {
										found = true;
									}
								}
								if (!found) {
									incompatibleAddOns[key].push(_addonValue);
								}
							}
						}
					}
					var addonValue = {defaultAddons: defaultAddons, allAddons: allAddons, addOnGroups: addOnGroups};
					return {key: key, data: addonValue};
				}).reduce(function (map, obj) {
					map[obj.key] = obj.data;
					return map;
				}, {});
				return addonMap;
			}

			function getAOMData() {
				return addOnData;
			}

			function getDefaultAddOns() {
				var defaultAddOns = Object.keys(addOnData).map(function (key) {
					var defaultAddons = addOnData[key].defaultAddons;
					return {key: key, data: defaultAddons};
				}).reduce(function (map, obj) {
					map[obj.key] = obj.data;
					return map;
				}, {});
				return defaultAddOns;
			}

			function compareBySequence(a, b) {
				if (a.cspmb__Sequence__c && b.cspmb__Sequence__c && a.cspmb__Sequence__c < b.cspmb__Sequence__c || a.cspmb__Sequence__c && !b.cspmb__Sequence__c) {
					return 1;
				}
				if (a.cspmb__Sequence__c && b.cspmb__Sequence__c && a.cspmb__Sequence__c > b.cspmb__Sequence__c || !a.cspmb__Sequence__c && b.cspmb__Sequence__c) {
					return -1;
				}
				return 0;
			}

			function minMaxLogic() {
				var validationObj = {};
				if (relatedProductData) {
					var rpIds = Object.keys(relatedProductData);
					for (var i = 0; i < rpIds.length; i++) {
						var currentRp = relatedProductData[i];
						if (currentRp) {
							var rps = currentRp.added;
							var adds = addOnData[currentRp.definitionId];
							var rpData = [];
							if (adds && adds.addOnGroups) {
								for (var k = 0; k < adds.addOnGroups.length; k++) {
									var grp = adds.addOnGroups[k];
									var count = 0;
									if (rps && rps.length > 0) {
										for (var l = 0; l < rps.length; l++) {
											var addonValue = rps[l];
											if (grp.addonIds.includes(addonValue)) {
												count++;
											}
										}
									}
									grp.count = count;
									var msg = 'Ok';
									if (count > grp.max) {
										msg = 'Please remove ' + (count - grp.max) + ' add on(s).';
									} else if (count < grp.min) {
										msg = 'Please add more add ons.';
									}
									rpData.push({
										name: grp.name,
										min: grp.min,
										max: grp.max,
										count: count,
										msg: msg,
										addonIds: grp.addonIds
									});
								}
							}
							validationObj[currentRp.definitionId] = rpData;
						}
					}
				}
				return Promise.resolve(validationObj);
			}

			function checkMinMax(data) {
				if (data) {
					return minMaxLogic();
				} else {
					return CS.AOM.WebService.getRelatedProductData().then(function (relatedProductData) {
						return minMaxLogic();
					});
				}
			}

			function getIncompatibleAddOns() {
				return incompatibleAddOns;
			}

			return {
				getDefaultAddOns: getDefaultAddOns,
				getIncompatibleAddOns: getIncompatibleAddOns,
				initAOMData: initAOMData,
				checkMinMax: checkMinMax,
				getAOMData: getAOMData,
				resetAOM: resetAOM
			};
		}

		// ES6 Promise polyfill for IE
		!function (e) {
			function n() {
			}

			function t(e, n) {
				return function () {
					e.apply(n, arguments)
				}
			}

			function o(e) {
				if ("object" != typeof this) throw new TypeError("Promises must be constructed via new");
				if ("function" != typeof e) throw new TypeError("not a function");
				this._state = 0, this._handled = !1, this._value = void 0, this._deferreds = [], s(e, this)
			}

			function i(e, n) {
				for (; 3 === e._state;) e = e._value;
				return 0 === e._state ? void e._deferreds.push(n) : (e._handled = !0, void o._immediateFn(function () {
					var t = 1 === e._state ? n.onFulfilled : n.onRejected;
					if (null === t) return void(1 === e._state ? r : u)(n.promise, e._value);
					var o;
					try {
						o = t(e._value)
					} catch (i) {
						return void u(n.promise, i)
					}
					r(n.promise, o)
				}))
			}

			function r(e, n) {
				try {
					if (n === e) throw new TypeError("A promise cannot be resolved with itself.");
					if (n && ("object" == typeof n || "function" == typeof n)) {
						var i = n.then;
						if (n instanceof o) return e._state = 3, e._value = n, void f(e);
						if ("function" == typeof i) return void s(t(i, n), e)
					}
					e._state = 1, e._value = n, f(e)
				} catch (r) {
					u(e, r)
				}
			}

			function u(e, n) {
				e._state = 2, e._value = n, f(e)
			}

			function f(e) {
				2 === e._state && 0 === e._deferreds.length && o._immediateFn(function () {
					e._handled || o._unhandledRejectionFn(e._value)
				});
				for (var n = 0, t = e._deferreds.length; n < t; n++) i(e, e._deferreds[n]);
				e._deferreds = null
			}

			function c(e, n, t) {
				this.onFulfilled = "function" == typeof e ? e : null, this.onRejected = "function" == typeof n ? n : null, this.promise = t
			}

			function s(e, n) {
				var t = !1;
				try {
					e(function (e) {
						t || (t = !0, r(n, e))
					}, function (e) {
						t || (t = !0, u(n, e))
					})
				} catch (o) {
					if (t) return;
					t = !0, u(n, o)
				}
			}

			var a = setTimeout;
			o.prototype["catch"] = function (e) {
				return this.then(null, e)
			}, o.prototype.then = function (e, t) {
				var o = new this.constructor(n);
				return i(this, new c(e, t, o)), o
			}, o.all = function (e) {
				var n = Array.prototype.slice.call(e);
				return new o(function (e, t) {
					function o(r, u) {
						try {
							if (u && ("object" == typeof u || "function" == typeof u)) {
								var f = u.then;
								if ("function" == typeof f) return void f.call(u, function (e) {
									o(r, e)
								}, t)
							}
							n[r] = u, 0 === --i && e(n)
						} catch (c) {
							t(c)
						}
					}

					if (0 === n.length) return e([]);
					for (var i = n.length, r = 0; r < n.length; r++) o(r, n[r])
				})
			}, o.resolve = function (e) {
				return e && "object" == typeof e && e.constructor === o ? e : new o(function (n) {
					n(e)
				})
			}, o.reject = function (e) {
				return new o(function (n, t) {
					t(e)
				})
			}, o.race = function (e) {
				return new o(function (n, t) {
					for (var o = 0, i = e.length; o < i; o++) e[o].then(n, t)
				})
			}, o._immediateFn = "function" == typeof setImmediate && function (e) {
				setImmediate(e)
			} || function (e) {
				a(e, 0)
			}, o._unhandledRejectionFn = function (e) {
				"undefined" != typeof console && console && console.warn("Possible Unhandled Promise Rejection:", e)
			}, o._setImmediateFn = function (e) {
				o._immediateFn = e
			}, o._setUnhandledRejectionFn = function (e) {
				o._unhandledRejectionFn = e
			}, "undefined" != typeof module && module.exports ? module.exports = o : e.Promise || (e.Promise = o)
		}(this);

		function initApexPlugins() {
			var APEX_PLUGIN = 'getAddOnsApexPlugin';
			var priceItemAttrCache = void 0,
				relatedAttributesCache = void 0,
				firstRunAttributeRef = void 0,
				currentPI = undefined,
				labels = void 0,
				addingDefault = false;
			var piCache;

			function emptyPromise() {
				return Promise.resolve();
			}

			function getAddonPriceItemAssociations() {
				var attributeData = getAttributeData();
				var defIds = [];
				for (var i = 0; i < attributeData.attributeDefinitions.length; i++) {
					defIds.push(attributeData.attributeDefinitions[i].definitionId);
				}
				attributeData.attributeDefinitions = defIds;
				if (attributeData.priceItemId === '') {
					return Promise.resolve(false);
				} else if (piCache) {
					return Promise.resolve(piCache);
				} else {
					return CS.Service.invokePlugin(APEX_PLUGIN, JSON.stringify(attributeData)).then(function onSuccess(result) {
						labels = result.labels;
						piCache = result;
						return Promise.resolve(result);
					}, function onFailure(error) {
						return Promise.reject(error);
					});
				}
			}

			function findPriceItemAttributeForConfigRef() {
				var pi = priceItemAttrCache;
				if (pi) {
					return pi;
				}
				var pis = Object.keys(CS.Service.config).map(function (key) {
					if (CS.Service.config[key].attributeFields && CS.Service.config[key].attributeFields.hasOwnProperty('__PriceItemReference__') && CS.Util.getParentReference(CS.Service.config[key].reference) === '') {
						return CS.Service.config[key];
					} else {
						return null;
					}
				}).filter(function (item) {
					return item !== null;
				});
				if (pis.length > 1) {
					console.warn('Multiple __PriceItemReference__ markers found, model is invalid: ', pis);
				}
				priceItemAttrCache = pis[0] || null;
				return priceItemAttrCache;
			}

			function runAddOnManager() {
				var newCP = getNewCPValue();
				var ignoreDeleteOnLoad = false;
				if (newCP !== currentPI) {
					piCache = undefined;
					if (currentPI || currentPI === '') {
						setFirstRun('No');
						currentPI = newCP;
						ignoreDeleteOnLoad = false;
					} else {
						currentPI = newCP;
						ignoreDeleteOnLoad = true;
					}
					if (currentPI === '') {
						var incompatibleAddOns = CS.AOM.getIncompatibleAddOns();
						CS.AOM.resetAOM();
						deleteAllAddons(true).then(function (res) {
							setFirstRun('Yes');
							return CS.Rules.evaluateAllRules();
						});
					} else {
						CS.AOM.initAOMData().then(function (result) {
							return Promise.resolve(true);
						}).then(function (res) {
							if (ignoreDeleteOnLoad) {
								return Promise.resolve(true);
							}
							var incompatibleAddOns = CS.AOM.getIncompatibleAddOns();
							if (addOnConfig.deleteAddOnsOnTransition) {
								return deleteAllAddons(true);
							} else {
								return deleteAddons(incompatibleAddOns, true);
							}
						}).then(function (res) {
							var defaultAddOns = CS.AOM.getDefaultAddOns();
							var fisrtRun = getFirstRun();
							if (!fisrtRun) {
								addingDefault = true;
								return addDefaultAddons(defaultAddOns);
							} else {
								return emptyPromise();
							}
						}).then(function (res) {
							return updateAddOnReferences(CS.AOM.getAOMData());
						}).then(function (res) {
							setFirstRun('Yes');
							addingDefault = false;
							return CS.Rules.evaluateAllRules();
						});
					}
				}
			}

			function updateAddOnReferences(addOnData) {
				return getRelatedProductData().then(function (data) {
					data.forEach(function (rp) {
						if (addOnData[rp.definitionId]) {
							var addOns = addOnData[rp.definitionId].allAddons;
							if (rp.addedRefMap && rp.addedRefMap.length > 0) {
								rp.addedRefMap.forEach(function (added) {
									addOns.forEach(function (addOn) {
										if (addOn.cspmb__Add_On_Price_Item__c === added.id) {
											var addonIdRef = findAttrByRefField(rp.reference, '__AddonGenerator__');
											CS.setAttributeValue(added.reference + ':' + CS.Util.generateReference(addonIdRef.attr.Name), addOn.Id, true);
										}
									});
								});
							}
						}
					});
					return Promise.resolve(true);
				});
			}

			function getRelatedAttributes() {
				var relatedAttributes = Object.keys(CS.Service.config).map(function (key) {
					if (key.indexOf('_0') > -1 && CS.Service.config[key] && CS.Service.config[key].attributeFields && CS.Service.config[key].attributeFields.hasOwnProperty('__AddOn__')) {
						return CS.Service.config[key];
					} else {
						return null;
					}
				}).filter(function (item) {
					return item !== null;
				});
				relatedAttributesCache = relatedAttributes;
				return relatedAttributes;
			}

			function getAttributeData() {
				var priceItem = findPriceItemAttributeForConfigRef();
				var priceItemId = CS.getAttributeValue(priceItem.reference);
				var relatedAttributes = getRelatedAttributes();
				var relatedAttributesIds = relatedAttributes.map(function (obj) {

					var availableProducts = CS.SVC.getAvailableProducts(obj.reference, CS.Service.config);
					if (availableProducts === undefined || availableProducts.length < 1) {
						console.error('Addon Manager: no related product definitions avaialable; there should be exactly one for attribute ' + obj.reference);
					}
					var defId = availableProducts[0].cscfga__Product_Definition__c;
					return {
						definitionId: obj.definitionId,
						name: obj.attr.Name,
						reference: obj.reference,
						defId: defId
					};
				});
				var commercialAttributes = getCommercialAttributes();
				return {
					priceItemId: priceItemId,
					attributeDefinitions: relatedAttributesIds,
					attributes: commercialAttributes
				};
			}

			function getCommercialAttributes() {
				var commercialAttributes = Object.keys(CS.Service.config).map(function (key) {
					if (CS.Service.config[key].hasOwnProperty('definitionId')) {
						return CS.Service.config[key];
					}
				}).filter(function (item) {
					return item !== undefined;
				}).reduce(function (map, obj) {
					if (obj.attr) {
						map[obj.attr.Name] = obj.attr.cscfga__Value__c;
					}
					return map;
				}, {});

				return commercialAttributes;
			}

			function getNewCPValue() {
				var PIattr = findPriceItemAttributeForConfigRef();
				if (!PIattr) {
					return;
				}
				return CS.getAttributeValue(PIattr.reference);
			}

			function findFirstRunAttribute() {
				if (firstRunAttributeRef) {
					return firstRunAttributeRef;
				}
				var fr = Object.keys(CS.Service.config).map(function (key) {
					if (CS.Service.config[key].attributeFields && CS.Service.config[key].attributeFields.hasOwnProperty('__FirstRun__')) {
						return CS.Service.config[key];
					} else {
						return null;
					}
				}).filter(function (item) {
					return item !== null;
				});
				if (fr.length > 1) {
					console.warn('Multiple __FirstRun__ markers found, model is invalid: ', fr);
				}
				firstRunAttributeRef = fr[0].reference || null;
				return firstRunAttributeRef;
			}

			function setFirstRun(value) {
				if (findFirstRunAttribute()) {
					CS.setAttribute(findFirstRunAttribute(), value, true);
				}
			}

			function getFirstRun() {
				if (findFirstRunAttribute()) {
					return CS.getAttributeValue(findFirstRunAttribute()) === 'Yes';
				} else {
					return false;
				}
			}

			function flatten(arr) {
				var result = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : [];
				var i = 0,
					length = arr.length;
				for (; i < length; i++) {
					var value = arr[i];
					if (Array.isArray(value)) {
						flatten(value, result);
					} else {
						result.push(value);
					}
				}
				return result;
			}

			function findAttrByRefField(ref, fieldName) {
				var found = Object.keys(CS.Service.config).map(function (key) {
					return CS.Service.config[key];
				}).filter(function (item) {
					return item.attributeFields && item.reference.includes(ref) && item.attributeFields.hasOwnProperty(fieldName);
				});
				if (found.length > 0) {
					return found[0];
				} else {
					return null;
				}
			}

			function getRelatedProductData() {
				var relatedAttrs = getRelatedAttributes();
				var addedMap = {};
				var addedMapRef = {};
				relatedAttrs.forEach(function (attribute) {
					addedMap[attribute.definitionId] = [];
					addedMapRef[attribute.definitionId] = [];
					attribute.relatedProducts.forEach(function (item) {
						var addonIdRef = findAttrByRefField(item.reference, '__AddOnReference__');
						var addonAssocIdRef = findAttrByRefField(item.reference, '__AddonGenerator__');
						if (addonIdRef) {
							var addonValue = CS.getAttributeValue(addonIdRef.reference);
							var addonAssocValue = CS.getAttributeValue(addonAssocIdRef.reference);
							if (addonValue && addonValue !== '') {
								addedMap[attribute.definitionId].push(addonValue);
								addedMapRef[attribute.definitionId].push({reference: item.reference, id: addonValue});
							} else if (addonValue === '' && addonAssocValue && addonAssocValue !== '' && !addingDefault) {
								CS.Service.removeRelatedProduct(item.reference, true);
							}
						}
					});
				});
				var attributeData = getAttributeData();

				attributeData.attributeDefinitions.forEach(function (attribute) {
					if (addedMap[attribute.definitionId]) {
						attribute.added = addedMap[attribute.definitionId];
					}
					if (addedMapRef[attribute.definitionId]) {
						attribute.addedRefMap = addedMapRef[attribute.definitionId];
					}
					attribute.labels = labels;
				});
				return Promise.resolve(attributeData.attributeDefinitions);
			}

			function createRelatedProducts(addOnsList, suppressRules) {
				var promiseList = addOnsList.map(function (addon) {
					return function () {
						return new Promise(function (resolve, reject) {
							resolve(CS.Service.addRelatedProduct(addon.reference, addon.definitionId, suppressRules));
						}).then(function (config) {
							var attributePromiseList = [];
							var addonAttributeName = findAttrByRefField(config.reference, '__AddonGenerator__');
							var quantityAttributeName = findAttrByRefField(config.reference, '__AOM_Quantity__');

							if (addonAttributeName) {
								attributePromiseList.push(CS.setAttributeValue(addonAttributeName.reference, addon.id, suppressRules));
							} else {
								attributePromiseList.push(Promise.reject('Add On Association attribute is missing!'));
							}

							if (quantityAttributeName) {
								attributePromiseList.push(CS.setAttributeValue(quantityAttributeName.reference, addon.quantity, suppressRules));
							} else {
								//attributePromiseList.push(Promise.reject('Add On Association attribute is missing!'));
							}

							return attributePromiseList;
						});
					};
				});

				promiseList = flatten(promiseList);
				return promiseList.reduce(function (promise, func) {
					return promise.then(function (result) {
						return func().then(Array.prototype.concat.bind(result));
					});
				}, Promise.resolve([]));
			}

			function findAttrByRefFieldAndValue(ref, fieldName, value) {
				var found = Object.keys(CS.Service.config).map(function (key) {
					return CS.Service.config[key];
				}).filter(function (item) {
					return item.attributeFields && item.reference.includes(ref) && item.attributeFields.hasOwnProperty(fieldName) && item.attr.cscfga__Value__c === value;
				});
				if (found.length > 0) {
					return found[0];
				} else {
					return null;
				}
			}

			function deleteAddOnByRefAndValue(ref, value, suppresRules) {
				var attr = findAttrByRefFieldAndValue(ref.split('_0')[0], '__AddOnReference__', value);
				if (!attr) {
					return emptyPromise();
				}
				var confRef = attr.reference.split(':')[0];
				return CS.Service.removeRelatedProduct(confRef, false);
			}

			function deleteAddons(addOnsList, suppresRules) {
				var promiseList = emptyPromise();
				return getRelatedProductData().then(function (data) {
					getObjectValues(addOnsList).forEach(function (addons) {
						data.forEach(function (rp) {
							if (rp.addedRefMap && rp.addedRefMap.length > 0) {
								rp.addedRefMap.forEach(function (added) {
									if (addons.includes(added.id)) {
										promiseList = promiseList.then(deleteAddOnByRefAndValue(rp.reference, added.id, suppresRules));
									}
								});
							}
						});
					});
					return promiseList;
				});
			}

			function deleteAllAddons(suppresRules) {
				var promiseList = emptyPromise();
				return getRelatedProductData().then(function (data) {
					data.forEach(function (rp) {
						if (rp.addedRefMap && rp.addedRefMap.length > 0) {
							rp.addedRefMap.forEach(function (added) {
								promiseList = promiseList.then(deleteAddOnByRefAndValue(rp.reference, added.id, suppresRules));
							});
						}
					});
					return promiseList;
				});
			}

			function addDefaultAddons(defaultAddOns) {
				return getRelatedProductData().then(function (data) {
					var addList = [];
					data.forEach(function (rp) {
						var availableProducts = CS.SVC.getAvailableProducts(rp.reference, CS.Service.config);
						if (availableProducts === undefined || availableProducts.length < 1) {
							console.error('Addon Manager: no related product definitions avaialable; there should be exactly one for attribute ' + rp.reference);
						}
						var defId = availableProducts[0].cscfga__Product_Definition__c;
						if (defaultAddOns[rp.definitionId] && defaultAddOns[rp.definitionId].length > 0) {
							defaultAddOns[rp.definitionId].forEach(function (addOn) {
								if (!rp.added.includes(addOn.cspmb__Add_On_Price_Item__c)) {
									var newAddOn = {
										name: addOn.cspmb__Add_On_Price_Item__r.Name,
										id: addOn.Id,
										rpName: rp.name,
										reference: rp.reference,
										definitionId: defId
									};
									addList.push(newAddOn);
								}
							});
						}
					});
					return createRelatedProducts(addList, true);
				});
			}

			return {
				getAddonPriceItemAssociations: getAddonPriceItemAssociations,
				getRelatedProductData: getRelatedProductData,
				runAddOnManager: runAddOnManager,
				createRelatedProducts: createRelatedProducts
			};
		}

		function initCSAOMUI(addOnConfig) {
			var relatedProductData = void 0;
			var groupData = void 0;

			function initTabLinks() {
				jQuery('.addon-link').click(function () {
					jQuery(this).parent().parent().find('.slds-tabs--default__link').attr('aria-selected', 'false');
					jQuery(this).attr('aria-selected', 'true');
					jQuery(this).parent().parent().find('.slds-tabs--default__link').attr('tabindex', '-1');
					jQuery(this).attr('tabindex', '0');
					jQuery(this).parent().addClass('slds-active').siblings().removeClass('slds-active');
					jQuery(this).parent().parent().parent().find('.' + jQuery(this).parent().parent().parent().find('.slds-tabs--default__content,.slds-tabs--scoped__content')[0].classList[0]).removeClass('slds-show').addClass('slds-hide');
					jQuery(this).parent().parent().parent().find('#' + jQuery(this).attr('aria-controls')).removeClass('slds-hide').addClass('slds-show');
				});

				jQuery('.rp-item').click(function () {
					jQuery(this).addClass('slds-is-active').siblings().removeClass('slds-is-active');
					var rpId = jQuery(this).attr('id');
					jQuery('#' + rpId + 'tab').removeClass('slds-hide').siblings().addClass('slds-hide');
				});
				var first = jQuery('.rp-item')[0];
				if (first) {
					jQuery(first).addClass('slds-is-active').siblings().removeClass('slds-is-active');
					var rpId = jQuery(first).attr('id');
					jQuery('#' + rpId + 'tab').removeClass('slds-hide').siblings().addClass('slds-hide');
				}
				jQuery('#close-addons').click(function () {
					closeModal();
				});
				jQuery('#add-addons').click(function () {
					addSelectedAddons();
				});
				jQuery('.rp-all').click(function () {
					var checked = this.checked;
					var checkboxes = jQuery(this).closest('table').find('input[data-type="addon"]');
					checkboxes.prop('checked', checked);
				});
			}

			function showFullManager() {
				var checkedAddons = jQuery('input[data-type="addon"]:checked');
				jQuery(checkedAddons).prop('checked', false);
				var first = jQuery('.rp-item')[0];
				if (first) {
					jQuery(first).addClass('slds-is-active').siblings().removeClass('slds-is-active');
					var rpId = jQuery(first).attr('id');
					jQuery('#' + rpId + 'tab').removeClass('slds-hide').siblings().addClass('slds-hide');
				}
				jQuery('#add-navigation').removeClass('slds-hide');
				openModal();
			}

			function showManagerRP(ref) {
				var checkedAddons = jQuery('input[data-type="addon"]:checked');
				jQuery(checkedAddons).prop('checked', false);
				jQuery('#add-navigation').addClass('slds-hide');
				if (relatedProductData) {
					var found = relatedProductData.filter(function (item) {
						return item.reference == ref;
					});
					if (found.length) {
						jQuery('#' + found[0].definitionId + 'tab').removeClass('slds-hide').siblings().addClass('slds-hide');
					}
				}
				openModal();
			}

			function closeModal() {
				jQuery('#AddOnManagerModal').removeClass('slds-fade-in-open');
				jQuery('#AddOnManagerBackdrop').removeClass('slds-backdrop--open');
				jQuery('#addon-error').empty();
			}

			function openModal() {
				jQuery('#AddOnManagerBackdrop').addClass('slds-backdrop--open');
				jQuery('#AddOnManagerModal').addClass('slds-fade-in-open');
			}

			function addManagerButton() {
				var buttons = jQuery('.finishButtons');
				if (buttons.length && !addOnConfig.renderInSection) {
					jQuery(buttons[0]).prepend('<button id="addonman" class="slds-button slds-button--neutral">Add On Manager</button>');
					jQuery('#addonman').click(showFullManager);
				}
			}

			function renderAOM() {
				CS.AOM.WebService.getRelatedProductData().then(function (data) {
					relatedProductData = data;
					CS.AOM.checkMinMax(data).then(function (grpData) {
						groupData = grpData;
						refreshUIElements();
						if (addOnConfig.renderInSection) {
							jQuery('#addonSection').parent().parent().append(createMainDiv(null));
						} else {
							jQuery(jQuery('#configurationContainer')[0]).append(createMainModal(null));
						}

						initTabLinks();
					});
				});
			}

			function refreshUIElements() {
				var addonManager = jQuery('#AddonManager');
				if (addonManager.length) {
					jQuery(addonManager[0]).remove();
				}
				if (addOnConfig.renderInSection) {

				} else if (addOnConfig.showManagerButton) {
					var addOnButton = jQuery('#addonman');
					if (!addOnButton.length) {
						addManagerButton();
					}
				}
			}

			function createGroupTab(rp, name, slc, active) {
				if (!name) {
					name = 'Default';
				}
				return '<li class="slds-tabs--default__item slds-text-heading--label ' + active + '" role="presentation">' + '<a id="' + rp.definitionId + name.replace(/ /g, '') + 'item" class="slds-tabs--default__link addon-link" href="javascript:void(0);" role="tab" tabindex="0" aria-selected="' + slc + '" aria-controls="' + rp.definitionId + name.replace(/ /g, '') + '">' + name + '</a>' + '</li>';
			}

			function sortColumns(a, b) {

				if (a.cscfga__Sequence__c > b.cscfga__Sequence__c) {
					return 1;
				}
				if (a.cscfga__Sequence__c < b.cscfga__Sequence__c) {
					return -1;
				}
				return 0;
			}

			function getRPColumns(attRef) {
				var attrWrap = CS.getAttributeWrapper(attRef);
				if (attrWrap.attr.cscfga__Attribute_Definition__r) {
					var lookupConfig = attrWrap.attr.cscfga__Attribute_Definition__r.cscfga__Lookup_Config__c;
					var index = CS.Service.getProductIndex().all;
					var listColumns = index[lookupConfig].cscfga__List_Columns__c;
					var columns = Object.values(index).filter(function (item) {
						return item.cscfga__Object_Mapping__c && item.cscfga__Object_Mapping__c === listColumns;
					});
					return columns.sort(sortColumns);
				} else {
					return null;
				}

			}

			function createGroupContent(rp, group, cls) {
				var columns = void 0;
				if (rp && rp.reference && rp.labels) {
					var attColumns = getRPColumns(rp.reference);
					if (attColumns) {
						columns = [];
						attColumns.forEach(function (c) {
							var fieldName = '';
							if (c.cscfga__From_Field__c.indexOf('.') > -1) {
								fieldName = c.cscfga__From_Field__c.split('.')[1];
								columns.push({
									name: fieldName,
									isAssoc: false,
									label: rp.labels['AddOn'][fieldName.toLowerCase()]
								});
							} else {
								fieldName = c.cscfga__From_Field__c;
								columns.push({
									name: fieldName,
									isAssoc: true,
									label: rp.labels['Assoc'][fieldName.toLowerCase()]
								});
							}
						});
					}
					//columns = rpColumns[id];
				} else {
					columns = [{name: 'name', label: 'Name'}, {
						name: 'cspmb__add_on_price_item_description__c',
						label: 'Description'
					}];
				}


				var grpMsg = '';
				if (group.msg && group.msg !== '' && group.msg !== 'Ok') {
					grpMsg = '<div style="margin-left: 10px; margin-bottom: 10px;">' + '<span style="color:red"><b>' + group.msg + '</b></span>' + '</div>';
				}
				return '<div id="' + rp.definitionId + group.name.replace(/ /g, '') + '" class="slds-tabs--default__content ' + cls + '" role="tabpanel" aria-labelledby="' + rp.definitionId + group.name.replace(/ /g, '') + 'item">' + grpMsg + '<article class="slds-card"><div class="slds-grid"><div class="slds-media__body">' + '<table class="slds-table slds-table--cell-buffer slds-table--bordered">' + createTableHeader(columns) + createTableRows(rp, columns, group.addOns) + '</table></div></article></div>';
			}

			function createTableHeader(columns) {
				var quantityTitle = '';
				if (addOnConfig.allowQuantities) {
					quantityTitle = '<th class="slds-text-title_caps" scope="col"><div class="slds-truncate" title="">Quantity</div></th>'
				}
				if (!columns) {
					return ' <thead><tr class="slds-line-height_reset"><th><label class="slds-checkbox"><input class="rp-all" type="checkbox" name="groupAll">' + '<span class="slds-checkbox--faux"><span class="slds-form-element__label"></span></span></label></th>'
						+ quantityTitle + '</tr></thead>';
				}
				var headers = columns.map(function (item) {
					return '<th class="slds-text-title_caps" scope="col"><div class="slds-truncate" title="">' + item.label + '</div></th>';
				});
				return ' <thead><tr class="slds-line-height_reset"><th><label class="slds-checkbox"><input class="rp-all" type="checkbox" name="groupAll">' + '<span class="slds-checkbox--faux"><span class="slds-form-element__label"></span></span></label></th>' + quantityTitle + headers.reduce(function (acc, curr) {
					return acc + curr;
				}, '') + '</tr></thead>';
			}

			function createRowContent(columns, addon) {
				if (!columns) {
					return '';
				}
				return columns.map(function (item) {
					var lcAddon = convertObjectKeysToLowercase(addon);
					var val = void 0;
					if (item.isAssoc) {
						val = lcAddon[item.name.toLowerCase()] ? lcAddon[item.name.toLowerCase()] : '';
					} else {
						var addOn = convertObjectKeysToLowercase(lcAddon.cspmb__add_on_price_item__r);
						val = addOn[item.name.toLowerCase()] ? addOn[item.name.toLowerCase()] : '';
					}

					return '<td  colspan="1"><div class="slds-truncate">' + val + '</div></td>';
				});
			}

			function convertObjectKeysToLowercase(obj) {
				var key, keys = Object.keys(obj);
				var n = keys.length;
				var newobj = {}
				while (n--) {
					key = keys[n];
					newobj[key.toLowerCase()] = obj[key];
				}
				return newobj;
			}

			function createTableRows(rp, columns, addons) {
				if (!addons) {
					addons = [];
				}
				var rows = addons.map(function (item) {
					if (addOnConfig.hideAdded) {
						var found = false;
						relatedProductData.forEach(function (rpData) {
							if (rpData.definitionId === rp.definitionId && rpData.added && rpData.added.length > 0 && rpData.added.includes(item.cspmb__Add_On_Price_Item__c)) {
								found = true;
							}
						});
						if (found) {
							return '';
						}
					}
					var quantityInput = '';
					if (addOnConfig.allowQuantities) {
						quantityInput = '<td colspan="1"><input id="quantity-' + item.Id + '" type="number" class="slds-input"></td>';
					}
					var rowContent = createRowContent(columns, item);
					if (rowContent !== '') {
						return '<tr><td><label class="slds-checkbox"><input type="checkbox" name="options" data-rp="' + rp.defId + '"data-rpref="' + rp.reference + '" data-id="' + item.Id + '" data-priceitem="' + item.cspmb__Add_On_Price_Item__c + '" data-type="addon">' + '<span class="slds-checkbox--faux"><span class="slds-form-element__label"></span></span></label></td>' + quantityInput + rowContent.reduce(function (acc, curr) {
							return acc + curr;
						}, '') + '</tr>';
					} else {
						return '<tr><td><label class="slds-checkbox"><input type="checkbox" name="options" data-rp="' + rp.defId + '"data-rpref="' + rp.reference + '" data-id="' + item.Id + '" data-priceitem="' + item.cspmb__Add_On_Price_Item__c + '" data-type="addon">' + '<span class="slds-checkbox--faux"><span class="slds-form-element__label"></span></span></label></td>'
							+ quantityInput + '</tr>';
					}

				});
				return '<tbody>' + rows.reduce(function (acc, curr) {
					return acc + curr;
				}, '') + '</tbody>';
			}

			function createTabContainer(rp, item) {
				if (!groupData || !rp || !item || !item.allAddons) {
					return '';
				}
				var aomGroups = groupData[rp.definitionId];
				var defaultGrp = item.allAddons.filter(function (add) {
					return !add.cspmb__Group__c;
				});
				var grps = item.addOnGroups.map(function (group) {
					if (aomGroups) {
						aomGroups.forEach(function (grp) {
							if (!grp.name) {
								group.name = 'Default';
							}
							if (grp.name === group.name) {
								group.count = grp.count;
								group.msg = grp.msg;
							}
						});
					}
					return createGroupTab(rp, group.name, 'true', 'slds-active');
					/*
					if (item.addOnGroups[0].name == group.name) {
						return createGroupTab(rp, group.name, 'true', 'slds-active');
					} else {
						if (!group.name) {
							return createGroupTab(rp, 'Default', 'false', '');
						}
						return createGroupTab(rp, group.name, 'false', '');
					}
					*/
				});
				var grpContents = item.addOnGroups.map(function (group) {
					if (aomGroups) {
						aomGroups.forEach(function (grp) {
							if (grp.name === group.name) {
								group.count = grp.count;
								group.msg = grp.msg;
							}
						});
					}
					if (item.addOnGroups[0].name == group.name) {
						return createGroupContent(rp, group, 'slds-show');
					} else {
						return createGroupContent(rp, group, 'slds-hide');
					}
				});
				if (defaultGrp.length) {
					var defGrp = void 0;
					var defGrpContent = void 0;
					if (item.addOnGroups.length === 0) {
						defGrp = createGroupTab(rp, 'Default', 'true', 'slds-active');
						defGrpContent = createGroupContent(rp, {name: 'Default', min: 'N/A', max: 'N/A'}, 'slds-show');
					} else {
						defGrp = createGroupTab(rp, 'Default', 'false', '');
						defGrpContent = createGroupContent(rp, {name: 'Default', min: 'N/A', max: 'N/A'}, 'slds-hide');
					}
					grps.push(defGrp);
					grpContents.push(defGrpContent);
				}
				return '<div class="slds-tabs--default" id="' + rp.definitionId + 'tab">' + '<ul class="slds-scrollable slds-tabs--default__nav" role="tablist" id="groups_' + rp.definitionId + '">' + grps.reduce(function (acc, curr) {
					return acc + curr;
				}, '') + '</ul>' + grpContents.reduce(function (acc, curr) {
					return acc + curr;
				}, '') + '</div>';
			}

			function createMainModal(item) {
				if (!item) {
					item = {name: 'Add On Manager'};
				}
				return '<div class="slds-no-flex" id="AddonManager">' + '<div aria-labelledby="header43" class="slds-modal slds-modal--large" id="AddOnManagerModal" role="dialog" tabindex="-1">' + '<div class="slds-modal__container">' + '<div class="slds-modal__header" style="background-color: #f4f6f9;"><h2 class="slds-text-heading--medium" style="font-weight:400;">' + item.name + '</h2></div><div id="amMain" class="slds-modal__content slds-p-around--small">' + createMainSections() + '</div><div class="slds-modal__footer"><button class="slds-button slds-button--neutral" id="add-addons">' + 'Add addons</button><button class="slds-button slds-button--neutral" id="close-addons">Cancel</button>' + '</div></div></div><div class="slds-backdrop" id="AddOnManagerBackdrop"></div></div>';
			}

			function createMainDiv(item) {
				if (!item) {
					item = {name: 'Add On Manager'};
				}
				return '<div id="AddonManager">' + '<div class="slds-modal__header" style="background-color: #f4f6f9;"><h2 class="slds-text-heading--medium" style="font-weight:400;">' + item.name + '</h2></div><div id="amMain" class="slds-modal__content slds-p-around--small">' + createMainSections() + '</div><div class="slds-modal__footer"><button class="slds-button slds-button--neutral" id="add-addons">' + 'Add addons</button>' + '</div></div>';
			}

			function createMainSections() {
				var addonCache = CS.AOM.getAOMData();
				var relatedProducts = Object.values(relatedProductData);
				var content = '';
				var navigation = '<article style="box-shadow: 0 2px 2px 0 rgba(0,0,0,.1);border-radius: .25rem;border: 1px solid #dddbda;">' + '<div class="slds-grid slds-grid--vertical"><h2 class="slds-text-heading--label slds-p-around--small" ' + 'style="background-color: #f4f6f9;color:#000;font-weight:600;">Related Products</h2><ul class="slds-navigation-list--vertical slds-has-block-links--space">';
				if (relatedProducts && relatedProducts.length && addonCache) {
					for (var i = 0; i < relatedProducts.length; i++) {
						navigation += '<li style="margin-left:0px" class="rp-item" id="' + relatedProducts[i].definitionId + '">' + '<a href="javascript:void(0);" class="slds-navigation-list--vertical__action slds-text-link--reset"' + ' aria-describedby="entity-header">' + relatedProducts[i].name + '</a></li>';
						content += createTabContainer(relatedProducts[i], addonCache[relatedProducts[i].definitionId]);
					}
				}
				return '<div class="slds-grid" style="margin:10px" id="addon-error"></div><div class="slds-grid"><div style="width:20%;margin-right:10px;" class="slds-col" id="add-navigation">' + navigation + '</ul></div></article></div><div class="slds-col" style="width:75%">' + '<article style="box-shadow: 0 2px 2px 0 rgba(0,0,0,.1);border-radius: .25rem;border: 1px solid #dddbda;">' + content + '</div></article></div>';
			}

			function createErrorMsgs(errorObj) {
				return '<div class="slds-p-around_small slds-col slds-notify_alert slds-theme_alert-texture slds-theme_warning" role="alert">' + errorObj.reduce(function (acc, curr) {
					return acc + '<div><h1 style="font-weight:600">' + curr.msg + '</h1></div>';
				}, '') + '</div>';
			}

			function addSelectedAddons() {
				var checkedAddons = jQuery('input[data-type="addon"]:checked');

				jQuery('#addon-error').empty();
				if (checkedAddons.length) {
					var addMap = [];
					for (var i = 0; i < checkedAddons.length; i++) {
						var quantity = jQuery('#quantity-' + jQuery(checkedAddons[i]).data('id')).val();
						var addonId = jQuery(checkedAddons[i]).data('id');
						var definitionId = jQuery(checkedAddons[i]).data('rp');
						var reference = jQuery(checkedAddons[i]).data('rpref');
						var addOn = {id: addonId, definitionId: definitionId, reference: reference, quantity: quantity};
						addMap.push(addOn);
					}
					CS.AOM.WebService.createRelatedProducts(addMap, true).then(function (values) {
						return CS.Rules.evaluateAllRules();
					}).then(function (res) {
						CS.AOM.initAOMData();
					});
					closeModal();
				}
			}

			return {
				renderAOM: renderAOM,
				showManagerRP: showManagerRP
			};
		}

		if (CS) {
			CS.AOMInlineEdit = {};
			CS.AOMInlineEdit.init = function () {
				var newTemplate = "<%\r\nvar prefix = CS.Util.configuratorPrefix;\r\nvar max = definition[prefix + \'Max__c\'];\r\n\t\t\tvar inlineEdit = definition[\'csaom__inline_edit__c\'];\r\n\t\t\tvar disabled = ((max && relatedProducts.length >= max) ? \'disabled=\"disabled\"\' : \'\');\r\n\t\t\tvar prod;\r\n\t\t\tvar attRef;\r\n\t\t\tvar rowClass;\r\n\t\t\tvar errorClass;\r\n\t\t\tvar errorMessage;\r\n\t\t\tvar attr = anchor.attr;\r\n\t\t\trelatedProductsIds = new Array();\r\n\t\t\tvar isReadOnly = attr[prefix + \'Is_Read_Only__c\'];\r\n\t\t\tvar isActive = attr[prefix + \'is_active__c\'];\r\n\t\t\tvar isRequired = attr[prefix + \'Is_Required__c\'];\r\n\t\t\tif (max && relatedProducts.length > max) {\r\n\t\t\terrorClass = \'attributeError\';\r\n\t\t\terrorMessage = \'<p class=\"attributeErrorMessage\">The maximum number of \' + max + \' related items has been exceeded.<\/p>\';\r\n}\r\n%>\r\n<article class=\"slds-card\">\r\n\t<div class=\"slds-card__header slds-grid\">\r\n\t<header class=\"slds-media slds-media--center slds-has-flexi-truncate\">\r\n\t<div class=\"slds-media__body slds-truncate\">\r\n\t<h2>\r\n\t<span class=\"slds-icon_container slds-icon-custom-custom57 slds-m-right--x-small icon-list\"><\/span>\r\n<span class=\"definition-name\"><%= definition.Name %><\/span>\r\n<% if (isRequired) { %><span class=\"slds-text-color--error\">(Required)<\/span><% } %>\r\n\t<% if (isReadOnly) { %><span class=\"slds-text-color--error\">(Read Only)<\/span><% } %>\r\n\t\t<\/h2>\r\n\t\t<\/div>\r\n\t\t<\/header>\r\n\t\t<div class=\"slds-no-flex\">\r\n\t\t<%\r\n\t\tif (inlineEdit === \'Inline\') {\r\n\t\t%>\r\n\t\t<button class= \"slds-button slds-button--neutral\" <%= disabled %>  data-cs-control=\"<%= anchor.reference %>\" data-cs-ref=\"<%= anchor.reference %>\" onclick=\"CS.AOMInlineEdit.addRelatedProduct(\'<%= anchor.reference %>\', \'<%= anchor.definitionId %>\')\" data-cs-type=\"add\" data-role=\"none\">New <%= definition.Name %>\r\n\t\t<\/button><%\r\n\t} else if (inlineEdit === \'AddOn\') {\r\n\t\t%>\r\n\t\t<button class= \"slds-button slds-button--neutral\" <%= disabled %>  data-cs-control=\"<%= anchor.reference %>\" data-cs-ref=\"<%= anchor.reference %>\" onclick=\"CS.AOM.UI.showManagerRP(\'<%= anchor.reference %>\', \'<%= anchor.definitionId %>\')\" data-cs-type=\"add\" data-role=\"none\">New <%= definition.Name %>\r\n\t\t<\/button><%\r\n\t} else {\r\n\t\t%>\r\n\t\t<button <%= disabled %> class=\"slds-button slds-button--neutral\" data-cs-control=\"<%= anchor.reference %>\" data-cs-ref=\"<%= anchor.reference %>\" data-cs-action=\"addRelatedProduct\" data-cs-type=\"add\" data-role=\"none\">New <%= (definition[prefix + \'Label__c\']) ? definition[prefix + \'Label__c\'] : definition.Name %>\r\n\t\t<\/button><%\r\n\t}\r\n\t\t%>\r\n\t\t<\/div>\r\n\t\t<\/div>\r\n\t\t<% if (relatedProducts.length > 0) { %>\r\n\t\t<table class=\"slds-table slds-table--bordered slds-table--cell-buffer\" data-cs-binding=\"<%= anchor.reference %>\" data-cs-control=\"<%= anchor.reference %>\" data-cs-type=\"list\">\r\n\t\t<thead>\r\n\t\t<tr class=\"slds-text-title--caps\">\r\n\t\t<th scope=\"col\" width=\"140px\">\r\n\t\t<div class=\"slds-truncate\" title=\"\">Action<\/div>\r\n\t\t<\/th>\r\n\t\t<th scope=\"col\" width=\"130px\">\r\n\t\t<div class=\"slds-truncate\" title=\"\">Status<\/div>\r\n\t\t<\/th>\r\n\t\t<th scope=\"col\" width=\"300px\">\r\n\t\t<div class=\"slds-truncate\" title=\"\">Name<\/div>\r\n\t\t<\/th>\r\n\t\t<% for (var i = 0; i < cols.length; i++) {\r\n\t\tvar spec = colSpecs[cols[i]]; %>\r\n\t\t<th scope=\"col\">\r\n\t\t<div class=\"slds-truncate\" title=\"\"><%= spec.header %><\/div>\r\n\t\t<\/th>\r\n\t\t<% } %>\r\n\t\t<\/tr>\r\n\t\t<\/thead>\r\n\t\t<tbody>\r\n\t\t<% for (var i = 0; i < relatedProducts.length; i++) {\r\n\t\tprod = relatedProducts[i];\r\n\t\t%>\r\n\t\t<tr data-cs-ref=\"<%= prod.reference %>\">\r\n\t\t<td data-label=\"\"  width=\"130rem\">\r\n\t\t<% if (isActive && ! isReadOnly) { %>\r\n\t\t<a href=\"javascript:void(0);\"><span data-cs-action=\"editRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\">Edit<\/span><\/a>\r\n\t\t<% var attPrefix = prod.reference;\r\n\t\tvar ref = attPrefix + \':Default_0\';\r\n\t\tvar hasAttribute = false;\r\n\t\tif (CS.Service.config[ref] && CS.Service.config[ref].attr) {\r\n\t\tif (CS.getAttributeValue(ref) === \'1\') {\r\n\t\thasAttribute = true;\r\n\t}\r\n\t}\r\n\t\tif (!hasAttribute) {\r\n\t\t%>\r\n\t\t|\r\n\t\t<a href=\"javascript:void(0);\"><span data-cs-action=\"removeRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\">Del<\/span><\/a>\r\n\t\t<% } %>\r\n\t\t<% if (disabled === \'\' && inlineEdit !== \'AddOn\') { %>\r\n\t\t|\r\n\t\t<a href=\"javascript:void(0);\"><span data-cs-action=\"copyRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\">Copy<\/span><\/a>\r\n\t\t<% } %>\r\n\t\t<% } %>\r\n\t\t<\/td>\r\n\t\t<td data-label=\"\"  width=\"130rem\">\r\n\t\t<div class=\"slds-truncate\" title=\"<%= prod.config[prefix + \'Configuration_Status__c\'] %>\"><%= prod.config[prefix + \'Configuration_Status__c\'] %><\/div>\r\n\t\t\t\t\t\t<\/td>\r\n\t\t\t\t\t\t<td data-label=\"\">\r\n\t\t\t\t\t\t\t<div class=\"slds-truncate\" data-cs-action=\"editRelatedProduct\" data-cs-ref=\"<%= prod.reference %>\" title=\"<%= prod.config.Name %>\"><%= prod.config.Name %><\/div>\r\n\t\t\t\t\t\t<\/td>\r\n\t\t\t\t\t\t<% for (var j = 0; j < cols.length; j++) {\r\n\t\t\t\t\t\t\tif (colSpecs[cols[j]].ref != undefined) {\r\n\t\t\t\t\t\t\t\tattRef = prod.reference + \':\' + colSpecs[cols[j]].ref;\r\n\t\t}\r\n\t\t%>\r\n\t\t<td data-label=\"\">\r\n\t\t<% if (attRef != undefined) {\r\n\t\t\tvar displayInfo = CS.Service.config[attRef][\'displayInfo\'];\r\n\t\t\tif (CS.Service.config[attRef][\'attr\'] && (!CS.Service.config[attRef][\'attr\'][\'cscfga__is_active__c\'] || CS.Service.config[attRef][\'attr\'][\'cscfga__Is_Read_Only__c\'])) {\r\n\t\t\t\tdisplayInfo = \'RO\';\r\n\t\t\t}\r\n\t\t\tvar definition = CS.Service.config[attRef].definition;\r\n\t\t\tif (!inlineEdit || inlineEdit === \'Standard\') { %>\r\n\t\t\t<div class=\"slds-truncate\" title=\"\"><%= CS.getAttributeDisplayValue(attRef) %><\/div>\r\n\t\t\t<% } else if (displayInfo === \'User Input\') { %>\r\n\t\t\t\t<div class=\"slds-truncate\" title=\"\">\r\n\t\t\t\t<input class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\"\r\n\t\t\t\ttype=\"text\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.AOMInlineEdit.updateAttribute(this)\">\r\n\t\t\t\t<\/input>\r\n\t\t\t\t<\/div>\r\n\t\t\t\t<% } else if (displayInfo === \'Date\') {%>\r\n\t\t\t\t<div class=\"slds-truncate\" title=\"\">\r\n\t\t\t\t<input class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" value=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] %>\" onchange=\"CS.AOMInlineEdit.updateAttribute(this)\" data-role=\"none\"\r\n\t\t\t\tonfocus=\"DatePicker.pickDate(true, \'<%=attRef%>\', false );\" size=\"12\" \/>\r\n\t\t\t\t<\/input>\r\n\t\t\t\t<span class=\"dateFormat\">\r\n\t\t\t\t[&nbsp;\r\n\t\t\t\t<a href=\"\" class=\"todayPicker\" onclick=\"return (function(){ DatePicker.insertDate(CS.todayFormatted, \'<%=attRef%>\', true); return false; })();\"><%=CS.todayFormatted%>\r\n\t\t\t\t<\/a>&nbsp;]\r\n\t\t\t\t<\/span>\r\n\t\t\t\t<\/div>\r\n\t\t\t\t<% } else if (displayInfo === \'Checkbox\') {%>\r\n\t\t\t\t<div class=\"slds-truncate\" title=\"\">\r\n\t\t\t\t<input type=\"checkbox\" class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\"\r\n\t\t\t\tname=\"<%=attRef%>\" checked=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\"\r\n\t\t\t\tvalue=\"<%= CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\'] === \'Yes\' ? true : false %>\"  onchange=\"CS.AOMInlineEdit.updateAttribute(this)\" >\r\n\t\t\t\t<\/input>\r\n\t\t\t\t<\/div>\r\n\t\t\t\t<% } else if (displayInfo === \'Select List\') {%>\r\n\t\t\t\t<div class=\"slds-truncate\" title=\"\">\r\n\t\t\t\t<select class=\"form-control\" data-cs-binding=\"<%=attRef%>\" id=\"<%=attRef%>\" name=\"<%=attRef%>\" onchange=\"CS.AOMInlineEdit.updateAttribute(this)\"><%\r\n\t\t\t\tvar options = CS.Service.config[attRef][\'selectOptions\'];\r\n\t\t\t\tfor (var k = 0; k < options.length; k++) {\r\n\t\t\t\tvar selected = (options[k][\'Name\'] === CS.Service.config[attRef][\'attr\'][\'cscfga__Display_Value__c\']);\r\n\t\t\t\tif (selected) {\r\n\t\t\t\t%>\r\n\t\t\t\t<option value=\"<%= options[k][\'cscfga__Value__c\'] %>\" selected=\"selected\"><%= options[k][\'Name\'] %>\r\n\t\t\t\t<\/option><%\r\n\t\t\t} else {\r\n\t\t\t\t%>\r\n\t\t\t\t<option value=\"<%= options[k][\'cscfga__Value__c\'] %>\"><%= options[k][\'Name\'] %>\r\n\t\t\t\t<\/option><%\t\t  }\r\n\t\t\t}\r\n\t\t\t\t%>\r\n\t\t\t\t<\/select>\r\n\t\t\t\t<\/div>\r\n\t\t\t\t<% } else { %>\r\n\t\t\t\t<div class=\"slds-truncate\" title=\"\"><%= CS.getAttributeDisplayValue(attRef) %><\/div>\r\n\t\t\t\t<% } %>\r\n\t\t\t\t<% } %>\r\n\t\t\t\t<\/td>\r\n\t\t\t\t<%  } %>\r\n\t\t\t\t<\/tr>\r\n\t\t\t\t<% } %>\r\n\t\t\t\t<\/tbody>\r\n\t\t\t\t<\/table>\r\n\t\t\t\t<% } else { %>\r\n\t\t\t\t\t<table class=\"slds-table slds-table--bordered slds-table--cell-buffer\">\r\n\t\t\t\t\t<tbody>\r\n\t\t\t\t\t<tr class=\"cs-no-items-to-display\">\r\n\t\t\t\t\t<td scope=\"row\">\r\n\t\t\t\t\t<div class=\"slds-text-align--center\">No items to display<\/div>\r\n\t\t\t\t\t<\/td>\r\n\t\t\t\t\t<\/tr>\r\n\t\t\t\t\t<\/tbody>\r\n\t\t\t\t\t<\/table>\r\n\t\t\t\t\t<% } %>\r\n\t\t\t\t\t<\/article>\r\n";

				/**
				 * Updates configurator attribute and runs all rules
				 * @param input
				 */
				function updateAttribute(input) {
					var invalue;
					if (input.type === 'select-one') {
						invalue = jQuery(input).find('option:selected').val();
					} else if (input.className && input.className.indexOf('select2') != -1) {
						invalue = input.value;
					} else if (input.type === 'checkbox') {
						if (input.checked) {
							invalue = 'Yes';
						} else {
							invalue = false;
						}
					} else if (input.type === 'date') {
						invalue = CS.DataConverter.DATA_TYPE_CONVERTERS.String(input.valueAsDate);
					} else {
						invalue = input.value;
					}
					CS.setAttributeValue(input.id, invalue, true);
					CS.Rules.evaluateAllRules('after update');
				}

				/**
				 * Function used for adding related products without entering related product screen
				 * @param anchorRef
				 * @param definitionId
				 * @returns related product reference
				 */
				function addRelatedProduct(anchorRef, definitionId) {
					var attrWrap = CS.getAttributeWrapper(anchorRef);
					var templates = CS.RPUtils.getSelectProductOptionsForAttrId(attrWrap.definition.Id);
					if (templates && templates.ProductDefinitions && templates.ProductDefinitions.length > 0) {
						CS.Service.addRelatedProduct(anchorRef, templates.ProductDefinitions[0].Id, false);
					}
				}

				CS.AOMInlineEdit.updateAttribute = updateAttribute;
				CS.AOMInlineEdit.addRelatedProduct = addRelatedProduct;
				var el = jQuery('#CS\\.MultipleRelatedProductLE__tpl')[0];

				if (el !== undefined) {
					el.text = newTemplate;
					CS.App.Components.Repository.addComponent('configurator', 'MultipleRelatedProductLE', jQuery('#CS\\.MultipleRelatedProductLE__tpl')[0]);
				}
			};
		}

		CS.AOMInlineEdit.init();
		CS.AOM = initCSAddonApi(addOnConfig);
		CS.AOM.WebService = initApexPlugins();
		CS.AOM.UI = initCSAOMUI(addOnConfig);
	}
}