console.log('Loaded Internet plugin');
async function afterInternetAttributeUpdated(component, configuration, attribute, oldValueMap) {
	if (attribute.name === SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT) {
		await updateInternetName(component, configuration);
	} else if (attribute.name === SC_COMMON_ATTRIBUTES.CONTRACT_DURATION) {
		await setAttribute(component, configuration, SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT, '');
	} else if (attribute.name === SC_COMMON_ATTRIBUTES.ADDRESS) {
		await checkIfOneInternetPerLocation(configuration);
	}
}

/**
 * @description Set Internet Name
 */
async function updateInternetName(component, configuration) {
	let productVariantName = getAttributeValue(
		configuration,
		SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_NAME
	);
	let contractDuration = getAttributeValue(configuration, SC_COMMON_ATTRIBUTES.CONTRACT_DURATION);
	let name = productVariantName + ' ' + contractDuration + ' months';

	const attributeData = [
		{
			name: SC_COMMON_ATTRIBUTES.INTERNET_NAME,
			value: name
		}
	];
	return component.updateConfigurationAttribute(configuration.guid, attributeData);
}

async function refreshSiteConnectLookupAttribute() {
	const basket = await CS.SM.getActiveBasket();
	const solutions = basket.getSolutions();
	let businessInternetSolutions = Object.values(solutions).filter(
		(item) => item.name == 'Business Internet'
	);
	if (businessInternetSolutions != undefined) {
		for (let i = 0; i < businessInternetSolutions.length; i++) {
			let businessInternetSolution = businessInternetSolutions[i];
			let configurations = await SC_Util.generateFlatConfigurationListAsync(
				businessInternetSolution
			);
			let internetConfigs = await SC_Util.getAllConfigurationsWithNameAsync(
				configurations,
				'Internet'
			);
			for (let k = 0; k < internetConfigs.length; k++) {
				const unpacked = SC_Util.unpackFlattConfiguration(internetConfigs[i]);
				const siteId = SC_Util.getAttributeByName(unpacked, SC_COMMON_ATTRIBUTES.SITE_ID);
				const accountId = SC_Util.getAttributeByName(
					unpacked,
					SC_COMMON_ATTRIBUTES.ACCOUNT_ID
				);
				const basketId = SC_Util.getAttributeByName(
					unpacked,
					SC_COMMON_ATTRIBUTES.BASKET_ID
				);
				let lv = await basket.getLookupValues(
					businessInternetSolution.id,
					'Product Configuration Lookup',
					{ AccountID: accountId.value, BasketId: basketId.value, SiteId: siteId.value }
				);
				console.log('refreshSiteConnectLookupAttribute RESULT ' + JSON.stringify(lv));

				let att = { name: 'Site Connect', lookup: lv.result };
				await businessInternetSolution.updateConfigurationAttribute(unpacked.guid, [att]);
			}
		}
	}
}

async function loadDefaultAddons(component, internetConfig, attribute) {
	if (!attribute.value) {
		return;
	}

	// Dynamic IP
	const allAddons = await component.getAddonsForConfiguration(internetConfig.guid);
	if (Array.isArray(allAddons) && !allAddons.length) {
		return;
	}
	const dynamicIpAddon = Object.values(allAddons)[0].allAddons.find(
		(addOn) => addOn.cspmb__Add_On_Price_Item__r.Name === SC_ADDON_NAMES.DYNAMIC_IP
	);

	const dynamicIpMap = {
		addonPriceItemId: dynamicIpAddon.cspmb__Add_On_Price_Item__c,
		associationId: dynamicIpAddon.Id,
		group: dynamicIpAddon.cspmb__Group__c,
		quantity: 1,
		id: dynamicIpAddon.Id,
		name: dynamicIpAddon.cspmb__Add_On_Price_Item__r.Name,
		relatedProductName: 'InternetAddons',
		raw: dynamicIpAddon
	};

	return component.addAddOn(internetConfig.guid, dynamicIpMap);
}

async function afterInternetConfigurationAdded(component, configuration) {
	if (!configuration.attributes.contractduration.value) {
		await setAttribute(
			component,
			configuration,
			SC_COMMON_ATTRIBUTES.CONTRACT_DURATION,
			configuration.attributes.contractterm.value
		);
	}
	await setInternetBasketId(component, configuration);

	return true;
}

async function setInternetBasketId(component, configuration) {
	const url = new URL(document.location);
	const basketId = url.searchParams.get('basketId');

	await setAttribute(component, configuration, SC_COMMON_ATTRIBUTES.BASKET_ID, basketId);
	return;
}

async function checkIfOneInternetPerLocation(configuration) {
	const basket = await CS.SM.getActiveBasket();
	const sites = new Array();
	const solutions = basket.getSolutions();
	let businessInternetSolutions = Object.values(solutions).filter(
		(item) => item.name == 'Business Internet'
	);
	if (businessInternetSolutions != undefined) {
		for (let i = 0; i < businessInternetSolutions.length; i++) {
			let businessInternetSolution = businessInternetSolutions[i];
			let configurations = await SC_Util.generateFlatConfigurationListAsync(
				businessInternetSolution
			);
			let internetConfigurations = Object.values(configurations).filter(
				(item) => item.name == 'Internet'
			);

			if (internetConfigurations != undefined) {
				for (let i = 0; i < internetConfigurations.length; i++) {
					let internetConfiguration = internetConfigurations[i];

					if (internetConfiguration) {
						if (internetConfiguration.name == SC_COMPONENTS.INTERNET_SOLUTION) {
							if (
								sites != null &&
								sites.includes(internetConfiguration.value.attributes.address.value)
							) {
								CS.SM.displayMessage(SC_ERROR_MESSAGES.SITE_BI_ONLY_ONE, 'error');
								internetConfiguration.value.updateStatus({
									status: false,
									message: SC_ERROR_MESSAGES.SITE_BI_ONLY_ONE
								});
								await validateConfiguration(
									internetConfiguration.value,
									false,
									SC_ERROR_MESSAGES.SITE_BI_ONLY_ONE
								);
							} else {
								sites.push(internetConfiguration.value.attributes.address.value);
								internetConfiguration.value.updateStatus({
									status: true,
									message: ''
								});
								await validateConfiguration(internetConfiguration.value, true, '');
							}
						}
					}
				}
			}
		}
	}
}

async function generateInfrastructure(configurationGuid) {
	let initSolution = await CS.SM.getActiveSolution();
	const configurations = await SC_Util.generateFlatConfigurationListAsync();
	const packed = configurations[configurationGuid];
	const config = SC_Util.unpackFlattConfiguration(packed);
	const productVariant = SC_Util.getAttributeValue(config, SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT);

	// Get Main configuration info
	const rootConfigPacked = configurations[packed.mainGuid];
	const rootConfig = SC_Util.unpackFlattConfiguration(rootConfigPacked);
	const rootContractTerm = SC_Util.getAttributeValue(rootConfig, 'contractterm');

	if (!productVariant) {
		CS.SM.displayMessage(
			'Please select Internet commercial product before proceeding with infrastructure generation',
			'info'
		);
		return;
	}

	//keyAttributes
	let contractDuration = SC_Util.getAttributeByName(config, 'contractDuration').value;
	let technology = SC_Util.getAttributeByName(config, 'Technology').value;
	let siteVendor = SC_Util.getAttributeByName(config, 'Vendor').value;
	let selectedBandwidth = SC_Util.getAttributeByName(config, 'Tier').value;
	let bandwidthValues = ['0', '0'];

	if (selectedBandwidth != undefined && selectedBandwidth != null && selectedBandwidth != '') {
		bandwidthValues = selectedBandwidth.split('/');
	}

	let maxBandwidthAccess = await getDefaultMaxBandAccess(
		technology,
		contractDuration,
		siteVendor,
		bandwidthValues
	);
	let maxBandwidthSiteCon = await getDefaultMaxBandSiteConnect(
		contractDuration,
		maxBandwidthAccess.commercialProducts.Available_bandwidth_up__c,
		maxBandwidthAccess.commercialProducts.Available_bandwidth_down__c,
		true
	);
	let keyAttributes = {
		access: {
			contractDuration: SC_Util.getAttributeByName(config, 'contractDuration'),
			Technology: SC_Util.getAttributeByName(config, 'Technology'),
			SiteAvailability: SC_Util.getAttributeByName(config, 'SiteAvailability'),
			SiteId: SC_Util.getAttributeByName(config, 'SiteId'),
			sitevendor: SC_Util.getAttributeByName(config, 'Vendor'),
			Address: SC_Util.getAttributeByName(config, 'Address')
		},
		siteConnect: {
			contractDuration: SC_Util.getAttributeByName(config, 'contractDuration'),
			SiteId: SC_Util.getAttributeByName(config, 'SiteId')
		}
	};

	try {
		showProgressOverlay();
		let solutionId = null;
		const basket = await CS.SM.getActiveBasket();
		const solutions = basket.getSolutions();
		let existingSolution = Object.values(solutions).find(
			(item) => item.name == 'Infrastructure'
		);

		if (existingSolution) {
			solutionId = existingSolution.id;
			console.log('We have an existing Infratructure solution ' + solutionId);
		} else {
			const data = await basket.performRemoteAction('SC_GetMainSolutionId', {
				solutionName: SC_SOLUTIONS.INFRASTRUCTURE_SOLUTION
			});
			solutionId = await basket.createSolution(data.solutionId, true);
		}

		const activeSolution = await CS.SM.setActiveSolution(solutionId, true);
		const rootInfrastructureGuid = SC_Util.getMainConfigurationGUID(activeSolution);
		const infrastructureType = activeSolution.getComponentByName(
			SC_COMPONENTS.INFRASTRUCTURE_SOLUTION
		);
		const accessType = activeSolution.getComponentByName(SC_COMPONENTS.ACCESS_COMPONENT);
		const siteConnectType = activeSolution.getComponentByName(
			SC_COMPONENTS.SITECONNECT_COMPONENT
		);
		const accessInstance = accessType.createConfiguration(
			getAccessTemplate(keyAttributes, maxBandwidthAccess.commercialProducts)
		);
		const siteConnectInstance = siteConnectType.createConfiguration(
			getSiteConnectTemplate(
				keyAttributes,
				accessInstance,
				maxBandwidthSiteCon.commercialProducts
			)
		);
		const addedAccess = await accessType.addConfiguration(accessInstance);
		const addedSiteConnect = await siteConnectType.addConfiguration(siteConnectInstance);

		// adds SDWAN configs based on Access configs in the solution
		await resolveSdwan();
		await SC_Helper.updateAttributeAsync(
			infrastructureType,
			rootInfrastructureGuid,
			'solutionname',
			'Infrastructure - ' + rootContractTerm
		);
		await SC_Helper.updateAttributeAsync(
			infrastructureType,
			rootInfrastructureGuid,
			'contractterm',
			rootContractTerm
		);

		addedAccess.validate();
		addedSiteConnect.validate();
		activeSolution.validate();
		hideProgressOverlay();

		await basket.submitSolution(activeSolution);
		// return to Business Internet solution
		await CS.SM.setActiveSolution(initSolution.Id, true);
		return ['ok', null];
	} catch (error) {
		hideProgressOverlay();
		console.error(error);
		return [null, error];
	}
}

function getAccessTemplate(attributes, commercialProduct) {
	let configurationTemplate = [];
	const keys = Object.keys(attributes.access);
	for (let i = 0; i < keys.length; i++) {
		const key = keys[i];
		const attr = attributes.access[key];
		const showAtt = attr.name !== 'SiteId' && attr.name !== 'Vendor';

		if (attr.displayValue) {
			configurationTemplate.push(
				SC_Util.generateLookupAttribute(
					attr.name === 'Vendor' ? 'sitevendor' : attr.name,
					attr.value,
					attr.displayValue,
					showAtt
				)
			);
		} else {
			configurationTemplate.push(SC_Util.generateAttribute(attr.name, attr.value, showAtt));
		}
	}

	configurationTemplate.push(SC_Util.generateAttribute('owninfrastructuretoggle', true, true));
	// select product variant and set dependent calculation attributes
	configurationTemplate.push(
		SC_Util.generateLookupAttribute(
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT,
			commercialProduct.Id,
			commercialProduct.Name,
			true
		)
	);
	configurationTemplate.push(
		SC_Util.generateAttribute(
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_NAME,
			commercialProduct.Name,
			false
		)
	);
	configurationTemplate.push(
		SC_Util.generateAttribute(
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_UP,
			commercialProduct.Available_bandwidth_up__c,
			false
		)
	);
	configurationTemplate.push(
		SC_Util.generateAttribute(
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_DOWN,
			commercialProduct.Available_bandwidth_down__c,
			false
		)
	);

	return configurationTemplate;
}

function getSiteConnectTemplate(attributes, accessInstance, commercialProduct) {
	const AccessGUID = SC_Util.getAttributeValue(accessInstance, 'GUID');
	let configurationTemplate = [];
	const keys = Object.keys(attributes.siteConnect);

	for (let i = 0; i < keys.length; i++) {
		const key = keys[i];
		const attr = attributes.siteConnect[key];
		const showAtt = attr.name !== 'SiteId';

		if (attr.displayValue) {
			configurationTemplate.push(
				SC_Util.generateLookupAttribute(attr.name, attr.value, attr.displayValue, showAtt)
			);
		} else {
			configurationTemplate.push(SC_Util.generateAttribute(attr.name, attr.value, showAtt));
		}
	}

	configurationTemplate.push(SC_Util.generateAttribute('Access', AccessGUID, true));
	// select product variant and set dependent calculation attributes
	configurationTemplate.push(
		SC_Util.generateLookupAttribute(
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT,
			commercialProduct.Id,
			commercialProduct.Name,
			true
		)
	);
	configurationTemplate.push(
		SC_Util.generateAttribute(
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_UP,
			commercialProduct.Available_bandwidth_up__c,
			false
		)
	);
	configurationTemplate.push(
		SC_Util.generateAttribute(
			SC_COMMON_ATTRIBUTES.PRODUCT_VARIANT_BAND_DOWN,
			commercialProduct.Available_bandwidth_down__c,
			false
		)
	);

	return configurationTemplate;
}
