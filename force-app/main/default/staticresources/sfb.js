var sfbProductTypes = {
    "GeneralAddons" : "General_Addons_0",
    "Hardware" : "Hardware_0",
    "CoreLicenses" : "Core_Licenses_0",
    "ContactCenterFunctionalities" : "Contact_Center_functionalities_0",
    "PrePaidSupport" : "PrePaid_support_0",
    "AdoptionAndTraining" : "Adoption_and_Training_0"
    
}

var sfb__AdditionFlags = {
    "Onbeperkte" : "OnbAdded_0",
    "ZeroPrice" : "ZeroPrice_0"
}

var sfb_categoryNames = {
	"Seats" : "Seat Licenses",
	"Nummerblocks" :"Nummerblocks",
	"NationalFlatFee" : "National Flat Fee",
	"EmergencyRouting" : "Emergency Routing",
	"MicrosoftSPLA": "Microsoft SPLA licenses",
	"ReceptiePost": "Receptie post (incl 1e gebruiker), e.v",
	"ReceptiePT":"Receptiepost per tweede receptionist e.v",
	"Webchat" : "Webchat op KCC (per KCC)",
	"KCC" : "Klant Contact Center (KCC) per gebruiker",
	"Supervisor" : "Supervisor op Receptiepost/KCC (per superviser) ",
	"Rapportage":"Rapportage op Receptiepost",
	"AffKCC":"Afdelingsbereikbaarheid & KCC (per contract)",
	"Afdel ":"Department accessibility",
	"Onbeperkte":"Onbeperkte bereikbaarheids - functionaliteit (per contract)",
	"AfperGebruiker" : "Afdelingsbereikbaarheid per gebruiker in afdeling",
	"AfperAf" : "Afdelingsbereikbaarheid per afdeling",
	"Walbord" : "Wallboard op Receptiepost & KCC (per receptiepost & KCC)"


}

var sfb_errorMessages = {
	"1a" : "Seats must be minimun of 25 quantity",
	"1b" : "Every Seat  must be linked to a phonenumber as a minumum.",
	"2a" : "National Flat Fee must have same qty as core seat/license",
	"2b" : "Emergency routing quantity must be 1",
	"2c" : "Microsoft SPLA licenses: The sum of these products of this category can not exceed the qty of core seat/license",
	"2d" : "Incorrect Seat Licens - please change quantity or select another Seat License product.",
	"2e" : "Receptie post (incl 1e gebruiker) quantity must be 1",
	"2f" : "If Receptiepost per tweede receptionist e.v; has been added then at least 1 of Receptiepost (incl 1e gebruiker) needs to be added also",
	"2i" : "Quantity Webchat op KCC (per KCC) must be less than or equal to  Quantity Klant Contact Center (KCC) per gebruiker",
	"2g" : "Quantity Afdelingsbereikbaarheid per gebruiker in afdeling must be greater than or equal to  Quantity Afdelingsbereikbaarheid per afdeling",
	"2j" : "To add the Supervisor op Receptiepost/KCC (per superviser) flavor Sales need to have added at least 1 of Receptiepost (incl 1e gebruiker), e.v. or Klant Contact Center (KCC) per gebruiker. ",
	"2k" : "To add the Rapportage op Receptiepost, Afdelingsbereikbaarheid & KCC (per contract), Sales need to have added at least 1 of Receptiepost (incl 1e gebruiker), e.v., Afdelingsbereikbaarheid per afdeling, or Klant Contact Center (KCC) per gebruiker",
	"3b" : "Quantity Wallboard op Receptiepost & KCC (per receptiepost & KCC)  must be less than or equal to  Quantity Receptiepost (incl 1e gebruiker) e.v.+  Quantity of Klant Contact Center (KCC) per gebruiker+ Quantity Receptiepost per tweede receptionist e.v",
	"3b" : "Onbeperkte bereikbaarheids- functionaliteit (per contract) quantity must be 1",
	"allSelected" : "Please select a product!",
	"quantityError" : "Please select a seat license which matches the quantity"

}

var sfb_category = 'Category_0';
var sfb_quantity = 'Quantity_0';
var sfb_maxQuantity = 'Max_Quantity_0';
var sfb_minQuantity = 'Min_Quantity_0';
var sfb_product = 'Product_0';
var sfbProductTypesArray = [sfbProductTypes.AdoptionAndTraining,sfbProductTypes.PrePaidSupport,sfbProductTypes.GeneralAddons, sfbProductTypes.Hardware, sfbProductTypes.CoreLicenses, sfbProductTypes.ContactCenterFunctionalities];

window.getProdPerGroup = function getProdPerGroup(){
	
    var results = {};
    sfbProductTypesArray.forEach(function(element) {
	    for(var x in CS.Service.config[element].relatedProducts){
	        var itm = CS.Service.config[element].relatedProducts[x];
	        if(itm.reference){

	        	var category = CS.getAttributeValue(itm.reference+':'+sfb_category);
	        	var quantity = CS.getAttributeValue(itm.reference+':'+sfb_quantity);

	        	if(results[category]){
			    	results[category].quantity += quantity; 
			    }
			    else{
			    	
			    	results[category] = {};
			    	results[category].quantity = quantity;
			    }
	        }
	    }
	});
    return results;
}

window.sfbRules = function sfbRules(){
	var productsPerGroup = getProdPerGroup();

	rule_1a(productsPerGroup);
	rule_1b(productsPerGroup);
	rule_2a(productsPerGroup);
	rule_2b(productsPerGroup);
	rule_2c(productsPerGroup);
	rule_2e(productsPerGroup);
	rule_2f(productsPerGroup);
	rule_2g(productsPerGroup);
	rule_2i(productsPerGroup);
	rule_2j(productsPerGroup);
	rule_2k(productsPerGroup);
	rule_3a(productsPerGroup);
	rule_3b(productsPerGroup);
	
	//rule_2d();
	
	rule_3c(productsPerGroup);
	
	setTimeout(function(){disableAutomaticSFB();},0);
	
	checkIfAllIsSelected();
	checkQuantity();
	
}

function finalValidationSBF(){
    var product = 'Product_0';
    var additionMethod = 'AdditionMethod_0';
    
    var nbrAutomatic = 0;
    
    if(CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts.length>0){
        var relType = sfbProductTypes.ContactCenterFunctionalities;
        for(var idx in CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts){
            var withoutIndex = relType.substr(0, relType.lastIndexOf('_'))+'_';

            if(CS.Service.config[withoutIndex+idx+':'+product]&&(CS.getAttributeValue(withoutIndex+idx+':'+product) != '')&&(CS.getAttributeValue(withoutIndex+idx+':'+additionMethod)=='Automatic')){
                nbrAutomatic++;
            }
            
		}
	}
	
    if(nbrAutomatic==0 && (CS.getAttributeValue(sfb__AdditionFlags["Onbeperkte"])!='')){
        CS.setAttributeValue(sfb__AdditionFlags["Onbeperkte"],'');
        notZeroPrice();
    }
   
}
function checkQuantity(){
	var result = 0;

	var showError = false;
    var element= sfbProductTypes.CoreLicenses;

    if(CS.Service.config[element].relatedProducts.length>0)
	{
		for(var x in CS.Service.config[element].relatedProducts){
	        var itm = CS.Service.config[element].relatedProducts[x];
	        if(itm.reference){
				console.log(itm.reference);
	        	var product = CS.getAttributeValue(itm.reference+':'+sfb_product);
	        	var quantity = CS.getAttributeValue(itm.reference+':'+sfb_quantity);
	        	var maxQuantity = CS.getAttributeValue(itm.reference+':'+sfb_maxQuantity);
	        	var minQuantity = CS.getAttributeValue(itm.reference+':'+sfb_minQuantity);
	        	
	        	if(product!= ''){
	        		if(minQuantity<=quantity && quantity<maxQuantity){

	        		}
	        		else{
	        			CS.markConfigurationInvalid(sfb_errorMessages["quantityError"]);
	        		}
	        	}
	    	}
		}
	}
}

function checkIfAllIsSelected(){
	var result = 0;

	//max quantity
	//quantity
	var results = {};
	
	var showError = false;
    sfbProductTypesArray.forEach(function(element) {
        if(CS.Service.config[element].relatedProducts.length>0)
		{
			for(var x in CS.Service.config[element].relatedProducts){
		        var itm = CS.Service.config[element].relatedProducts[x];
		        if(itm.reference){
					console.log(itm.reference);
		        	var product = CS.getAttributeValue(itm.reference+':'+sfb_product);
		        	
		        	if(product== ''){
		        		CS.markConfigurationInvalid(sfb_errorMessages["allSelected"]);
		        	}
		        	
		    	}
	
			}
		}
	    
	});
    
}
//1a 
function rule_1a(ppg){
	if(ppg[sfb_categoryNames.Seats] && ppg[sfb_categoryNames.Seats].quantity<25){
		CS.markConfigurationInvalid(sfb_errorMessages["1a"]);
	}
}

//1b !!!!! FUINISH
function rule_1b(ppg){
	if(ppg[sfb_categoryNames.Seats]){
		 var seatQuantity = ppg[sfb_categoryNames.Seats].quantity;

		 var numberBlockQuantity = getNumBlockQuantity();

		 if(numberBlockQuantity < seatQuantity){
		 	CS.markConfigurationInvalid(sfb_errorMessages["1b"]);
		 }
	}
}

function getNumBlockQuantity(){
	var result = 0;

	//max quantity
	//quantity
	var results = {};
    sfbProductTypesArray.forEach(function(element) {
        if(CS.Service.config[element].relatedProducts.length>0)
		{
			for(var x in CS.Service.config[element].relatedProducts){
		        var itm = CS.Service.config[element].relatedProducts[x];
		        if(itm.reference){
					console.log(itm.reference);
		        	var category = CS.getAttributeValue(itm.reference+':'+sfb_category);
		        	var quantity = CS.getAttributeValue(itm.reference+':'+sfb_quantity);
		        	var maxQuantity = CS.getAttributeValue(itm.reference+':'+sfb_maxQuantity);
		        	console.log('category='+category);
		        	console.log('quantity='+quantity);
		        	console.log('maxQuantity='+maxQuantity);
		        	if(category == sfb_categoryNames.Nummerblocks){
		        		console.log('Correct category='+category);
		        		if(quantity!='' && maxQuantity!=''){
			        		console.log('quantity='+quantity);                
					    	if(results[category]){
					    		console.log('SUM');
					    		results[category] += quantity*maxQuantity; 
					    	}
					    	
						    
						    else{
						    	results[category] = {};
						    	results[category] = quantity*maxQuantity;
						    	console.log('NEW');
						    }
		        		}
		    		}
		    		
		    	}
	
			}
		}
	    
	});
    
    if(results[sfb_categoryNames.Nummerblocks]){
    	result = results[sfb_categoryNames.Nummerblocks];
    }

	return result;
}

//2a 
//"NationalFlatFee" : "National Flat Fee"
//"Seats" : "Seat Licenses",
function rule_2a(ppg){
	if(ppg[sfb_categoryNames.Seats] && ppg[sfb_categoryNames.NationalFlatFee]){
		if(ppg[sfb_categoryNames.Seats].quantity == ppg[sfb_categoryNames.NationalFlatFee].quantity){
			console.log("2a ok");
		}
		else{
		CS.markConfigurationInvalid(sfb_errorMessages["2a"]);
		}
	}
}

//2b
//"EmergencyRouting" : "Emergency Routing",
function rule_2b(ppg){
	if(ppg[sfb_categoryNames.EmergencyRouting] && ppg[sfb_categoryNames.EmergencyRouting].quantity!=1){
		CS.markConfigurationInvalid(sfb_errorMessages["2b"]);
	}
}

//2c
//"MicrosoftSPLA": "Microsoft SPLA licenses",
//"Seats" : "Seat Licenses",
function rule_2c(ppg){
	if(ppg[sfb_categoryNames.MicrosoftSPLA] && ppg[sfb_categoryNames.Seats]){
		if(ppg[sfb_categoryNames.MicrosoftSPLA].quantity <=ppg[sfb_categoryNames.Seats].quantity){
			console.log("2c ok");
		}
		else{
			CS.markConfigurationInvalid(sfb_errorMessages["2c"]);
		}
	}
	if(ppg[sfb_categoryNames.MicrosoftSPLA] && !ppg[sfb_categoryNames.Seats]){
	    CS.markConfigurationInvalid(sfb_errorMessages["2c"]);
	}
}
//2e
//"ReceptiePost": "Receptie post (incl 1e gebruiker)",
function rule_2e(ppg){
	if(ppg[sfb_categoryNames.ReceptiePost] && ppg[sfb_categoryNames.ReceptiePost].quantity!=1){
		CS.markConfigurationInvalid(sfb_errorMessages["2e"]);
	}
}

//2f
//"ReceptiePost": "Receptie post (incl 1e gebruiker)",
//"ReceptiePT":"Receptiepost per tweede receptionist e.v",
function rule_2f(ppg){
	if(ppg[sfb_categoryNames.ReceptiePT]){
		if(ppg[sfb_categoryNames.ReceptiePost] && ppg[sfb_categoryNames.ReceptiePost].quantity>=1){
			console.log("2f ok");
		}
		else{
			CS.markConfigurationInvalid(sfb_errorMessages["2f"]);
		}
	}
}


//2i
//"Webchat" : "Webchat op KCC (per KCC)",
//"KCC" : "Klant Contact Center (KCC) per gebruiker",
function rule_2i(ppg){
	if(ppg[sfb_categoryNames.Webchat] && ppg[sfb_categoryNames.KCC]){
		if(ppg[sfb_categoryNames.Webchat].quantity <= ppg[sfb_categoryNames.KCC].quantity){
			console.log("2i ok");
		}
		else{
			CS.markConfigurationInvalid(sfb_errorMessages["2i"]);
		}
	}
	if(ppg[sfb_categoryNames.Webchat] && !ppg[sfb_categoryNames.KCC]){
	    	CS.markConfigurationInvalid(sfb_errorMessages["2i"]);
	}
}

//2g
//"AfperGebruiker" : "Afdelingsbereikbaarheid per gebruiker in afdeling",
//"AfperAf" : "Afdelingsbereikbaarheid per afdeling"
function rule_2g(ppg){
	if(ppg[sfb_categoryNames.AfperGebruiker] || ppg[sfb_categoryNames.AfperAf]){
	    if(ppg[sfb_categoryNames.AfperAf] && (!ppg[sfb_categoryNames.AfperGebruiker])){
	        CS.markConfigurationInvalid(sfb_errorMessages["2g"]);
	    }
	    else if(ppg[sfb_categoryNames.AfperGebruiker] && (!ppg[sfb_categoryNames.AfperAf])){
	        console.log("2g ok");
	    }
		else if((ppg[sfb_categoryNames.AfperGebruiker] &&ppg[sfb_categoryNames.AfperAf]) &&(ppg[sfb_categoryNames.AfperGebruiker].quantity >=ppg[sfb_categoryNames.AfperAf].quantity)){
			console.log("2g ok");
		}
		else{
			CS.markConfigurationInvalid(sfb_errorMessages["2g"]);
		}
	}
}

//2j
//"Supervisor" : "Supervisor op Receptiepost/KCC (per superviser) ",
//"ReceptiePost": "Receptie post (incl 1e gebruiker)",
function rule_2j(ppg){
	if(ppg[sfb_categoryNames.Supervisor]){
		if(ppg[sfb_categoryNames.ReceptiePost] && ppg[sfb_categoryNames.ReceptiePost].quantity>=1){
			console.log("2j ok");
		}
		else{
			CS.markConfigurationInvalid(sfb_errorMessages["2j"]);
		}
	}
}

//2k
//"Rapportage":"Rapportage op Receptiepost
//"AffKCC":Afdelingsbereikbaarheid & KCC (per contract)",
//"Afdel ":"Afdelingsbereikbaarheid per afdeling",
//"KCC" : "Klant Contact Center (KCC) per gebruiker",
//"2k" : "To add the Rapportage op Receptiepost, Afdelingsbereikbaarheid & KCC (per contract), Sales need to have added at least 1 of Receptiepost (incl 1e gebruiker), e.v., Afdelingsbereikbaarheid per afdeling, or Klant Contact Center (KCC) per gebruiker",
function rule_2k(ppg){
	if((ppg[sfb_categoryNames.Rapportage])||(ppg[sfb_categoryNames.AffKCC])){

		if(ppg[sfb_categoryNames.Afdel] || ppg[sfb_categoryNames.ReceptiePost] || ppg[sfb_categoryNames.KCC]){
			console.log("2k ok");
		}
		else{
			CS.markConfigurationInvalid(sfb_errorMessages["2k"]);
		}
	}
}

//Quantity Wallboard op Receptiepost & KCC (per receptiepost & KCC) 
//<= Quantity Receptiepost (incl 1e gebruiker) e.v.+  Quantity of Klant Contact Center (KCC) per gebruiker
//+ Quantity Receptiepost per tweede receptionist e.v
//"Walbord" : "Wallboard op Receptiepost & KCC (per receptiepost & KCC)"
//	"ReceptiePost": "Receptie post (incl 1e gebruiker), e.v",
//	"ReceptiePT":"Receptiepost per tweede receptionist e.v",
//"KCC" : "Klant Contact Center (KCC) per gebruiker",
function rule_3a(ppg){
    var wallNbr = 0;
    var receptiePost = 0;
    var receptiePT = 0;
     var kcc = 0;
    if(ppg[sfb_categoryNames.Walbord]){
        wallNbr = sfb_categoryNames.Walbord.quantity;
    }
    
    if(ppg[sfb_categoryNames.ReceptiePost]){
        receptiePost = sfb_categoryNames.ReceptiePost.quantity;
    }
    
    if(ppg[sfb_categoryNames.ReceptiePT]){
        receptiePT = sfb_categoryNames.ReceptiePT.quantity;
    }
    
    if(ppg[sfb_categoryNames.KCC]){
        kcc = sfb_categoryNames.KCC.quantity;
    }
    if(wallNbr <= (receptiePost+receptiePT+kcc)){
			console.log("3a ok");
	}
	else{
	   CS.markConfigurationInvalid(sfb_errorMessages["3a"]); 
	}
    
}

//3b
//"Onbeperkte":"Onbeperkte bereikbaarheids - functionaliteit (per contract)",
function rule_3b(ppg){
	if(ppg[sfb_categoryNames.Onbeperkte] && ppg[sfb_categoryNames.Onbeperkte].quantity!=1){
		CS.markConfigurationInvalid(sfb_errorMessages["3b"]);
	}
}


//2d
//if quntity not correct show error message
function rule_2d(){
    var quantity='Quantity_0';
    var minQuantity = 'Min_Quantity_0';
    var maxQuantity = 'Max_Quantity_0';
    var product = 'Product_0';
    
    if(CS.getAttributeWrapper(sfbProductTypes.CoreLicenses).relatedProducts.length>0){
        for(var idx in CS.getAttributeWrapper(sfbProductTypes.CoreLicenses).relatedProducts){
            var withoutIndex = relType.substr(0, relType.lastIndexOf('_'))+'_';

            if(CS.Service.config[withoutIndex+idx+':'+product]&&(CS.getAttributeValue(withoutIndex+idx+':'+product) != '')&&(CS.getAttributeValue(withoutIndex+idx+':'+maxQuantity) != '')&&(CS.getAttributeValue(withoutIndex+idx+':'+minQuantity) != '')&&(CS.getAttributeValue(withoutIndex+idx+':'+quantity) != '')){
            	if((CS.getAttributeValue(withoutIndex+idx+':'+maxQuantity) > CS.getAttributeValue(withoutIndex+idx+':'+quantity)) && (CS.getAttributeValue(withoutIndex+idx+':'+minQuantity) <= CS.getAttributeValue(withoutIndex+idx+':'+quantity))){
            	    console.log('2d ok');
            	}
			    else{
			        CS.markConfigurationInvalid(sfb_errorMessages["2d"]);
			    }
            }

		}
	}
	
}

function rule_3c(ppg){
    //check price - recurring
    //if more than 825 add another item and set other items to 0
    
    var quantity='Quantity_0';
    var minQuantity = 'Min_Quantity_0';
    var maxQuantity = 'Max_Quantity_0';
    var product = 'Product_0';
    var recChange = 'Recurring_shadow_0';
    var additionMethod = 'AdditionMethod_0';
     var totalRecurring = 0.00;
     
    if(CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts.length>0){
        var relType = sfbProductTypes.ContactCenterFunctionalities;
        for(var idx in CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts){
            var withoutIndex = relType.substr(0, relType.lastIndexOf('_'))+'_';

            if(CS.Service.config[withoutIndex+idx+':'+product]&&(CS.getAttributeValue(withoutIndex+idx+':'+product) != '')&&(CS.getAttributeValue(withoutIndex+idx+':'+additionMethod)!='Automatic')){
            	totalRecurring += CS.getAttributeValue(withoutIndex+idx+':'+recChange);
            }
            
		}
	}
	
	if(totalRecurring > 825){
	    zeroPrice();
        automaticAdditionOfOnbeperkte();
    }
    
    if((totalRecurring<825)){
        notZeroPrice();
        onbeperkteRemove();
    }
    /*
    else{
        notZeroPrice();
        onbeperkteRemove();
    }
    */
}

function onbeperkteRemove(){
    if(CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts.length>0){
        var relType = sfbProductTypes.ContactCenterFunctionalities;
        for(var idx in CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts){
            var withoutIndex = relType.substr(0, relType.lastIndexOf('_'))+'_';

            if(CS.Service.config[withoutIndex+idx+":AdditionMethod_0"]&&(CS.getAttributeValue(withoutIndex+idx+":AdditionMethod_0") == 'Automatic')){
                
                CS.Service.removeRelatedProduct(withoutIndex + idx);
                CS.setAttributeValue(sfb__AdditionFlags.Onbeperkte,''); 
            }
            
		}
	}
}

function notZeroPrice(){
    if(CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts.length>0){
        var relType = sfbProductTypes.ContactCenterFunctionalities;
        for(var idx in CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts){
            var withoutIndex = relType.substr(0, relType.lastIndexOf('_'))+'_';

            if(CS.Service.config[withoutIndex+idx+":AdditionMethod_0"]&&(CS.getAttributeValue(withoutIndex+idx+":AdditionMethod_0") != 'Automatic')){
                CS.setAttributeValue(withoutIndex+idx+":"+sfb__AdditionFlags.ZeroPrice,''); 
            }
            
		}
	}
}

function zeroPrice(){
    if(CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts.length>0){
        var relType = sfbProductTypes.ContactCenterFunctionalities;
        for(var idx in CS.getAttributeWrapper(sfbProductTypes.ContactCenterFunctionalities).relatedProducts){
            var withoutIndex = relType.substr(0, relType.lastIndexOf('_'))+'_';

            if(CS.Service.config[withoutIndex+idx+":AdditionMethod_0"]&&(CS.getAttributeValue(withoutIndex+idx+":AdditionMethod_0") != 'Automatic')){
                CS.setAttributeValue(withoutIndex+idx+":"+sfb__AdditionFlags.ZeroPrice,'set'); 
            }
            
		}
	}
}

function disableAutomaticSFB(){
    sfbProductTypesArray.forEach(function(item, index, array) {
    if(item!=undefined){
        relType = item;
        if(CS.getAttributeWrapper(relType)){
        if(CS.getAttributeWrapper(relType).relatedProducts.length>0){
            for(var idx in CS.getAttributeWrapper(relType).relatedProducts){
                var withoutIndex = relType.substr(0, relType.lastIndexOf('_'))+'_';

                if(CS.Service.config[withoutIndex+idx+":AdditionMethod_0"]&&(CS.getAttributeValue(withoutIndex+idx+":AdditionMethod_0") == 'Automatic')){
                	jQuery('[name="'+withoutIndex+idx+':Product_0"]').parent().find('input, textarea, select').attr('readonly', 'readonly');
					jQuery('[name="'+withoutIndex+idx+':Category_0"]').parent().find('input, textarea, select').attr('disabled', 'disabled');
					jQuery('[name="'+withoutIndex+idx+':Quantity_0"]').parent().find('input, textarea, select').attr('readonly', 'readonly');
					
					jQuery("span[data-cs-ref='"+withoutIndex+idx+"'][data-cs-action='removeRelatedProduct']").hide();
				
                }

			}
		}
        }
		//
	}
})
}

function automaticAdditionOfOnbeperkte(){
    var scenario = CS.getAttributeValue('Scenario_0');
    var duration = CS.getAttributeValue('Contract_Duration_0');
    automaticAdditionCategory(sfb_categoryNames.Onbeperkte,scenario, duration,sfb__AdditionFlags["Onbeperkte"],sfbProductTypes.ContactCenterFunctionalities);
}

window.automaticAdditionCategory = function automaticAdditionCategory(category,scenario,duration,flag,relType){
    var flagVal = CS.getAttributeValue(flag);
    //debugger;
    console.log('---Checking flag =='+flag);
    if(flagVal == ''){
        console.log('---Value will be set =='+flagVal);
        CS.setAttributeValue(flag, 'set');
        var prodId = CS.Service.config[""].config.cscfga__Product_Definition__c;
        var newDyn = {};
        newDyn["TempVal"] = category;
        newDyn["Scenario"] = scenario;
         newDyn["Contract Duration"] = duration;
        doLookupQueryAndInvalidateAddons('PriceItemFilterAutomaticCategoryQuery', newDyn, prodId,CS.Service.config["AutomaticLookup_0"]).then(
        function(pgaResult){
            if(pgaResult.res != null){
                _.each(pgaResult.res, function(result){
                        priceItemsAdditionSFB(relType, result.Id,category);
                });
            }
            
            
        })
    }
    else{
        console.log('---Value will NOT be set =='+flagVal);
    }
}


function priceItemsAdditionSFB(rel, csAddon,category){
    console.log('Started with addition');
    var productId = CS.Service.getAvailableProducts(rel)[0].cscfga__Product_Definition__c;
    var relProductRef = rel;
    var lookupAttr = lookupAttr;
    
    var attr = CS.Service.config[rel];
    var emptyPromise = Promise.resolve();
    var parent = CS.getConfigurationWrapper(CS.Util.getParentReference(attr.reference));
    var defId = CS.Service.getAvailableProducts(attr.reference)[0].cscfga__Product_Definition__c;
    loadProduct(defId)
    .then(function() {
        
        CS.Service.addRelatedProduct(attr.reference, defId, false);
            var indexNumberOfCopiedRelProd = CS.getAttributeWrapper(relProductRef).relatedProducts.length;
            var indexItem = indexNumberOfCopiedRelProd -1;
    
            var wrapperDef = relProductRef.substring(0, relProductRef.length-1);
            var wrapperDef = relProductRef.substring(0, relProductRef.length-1);
   
            var defStrItem = wrapperDef+(indexItem)+':Product_0';
            
             var defStrQuantity = wrapperDef+(indexItem)+':Quantity_0';
             
             var defCategory = wrapperDef+(indexItem)+':Category_0';
        
            var defAdditionMethod = wrapperDef+(indexItem)+':AdditionMethod_0';
            CS.setAttributeValue(defStrItem,csAddon);
            CS.setAttributeValue(defStrQuantity,'1');
            CS.setAttributeValue(defCategory,category);
            CS.setAttributeValue(defAdditionMethod,'Automatic');
            CS.Service.config[defStrQuantity].attr.cscfga__Value__c = 1;
           
    })
    
    .then(function () {
        CS.Rules.evaluateAllRules();
    });
   
}



























