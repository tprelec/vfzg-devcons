console.log('Message Broker plugin loaded!');

// Event Listener
window.addEventListener('message', function (event) {

	if (typeof event.data === 'object' && event.data.action === 'exit' && event.data.target === 'packageModal') {
		SC_Packages.exit();
	}
});

let SC_MessageBroker = {

	store: new CS_PUBSUB()
};

function CS_PUBSUB() {

	const subscribers = {}

	function publish(eventName, data) {

		if (!Array.isArray(subscribers[eventName])) {
			return
		}

		subscribers[eventName].forEach((callback) => {
			callback(data)
		});
	};

	function subscribe(eventName, callback) {

		if (!Array.isArray(subscribers[eventName])) {
			subscribers[eventName] = [];
		}

		subscribers[eventName].push(callback);
		const index = subscribers[eventName].length - 1

		return {
			unsubscribe() {
				subscribers[eventName].splice(index, 1);
			},
		};
	};

	return {
		publish,
		subscribe,
	};
}

// Freeze object to prevent accidental overrides
Object.freeze(SC_MessageBroker);