console.log('Buttons plugin loaded!');

let SC_Buttons = {};

SC_Buttons.setButtonContainer = function() {

    let elems = document.getElementsByClassName('slds-page-header__row');

    function addRow(t, index, id) {
        let r = t.insertRow(index); 
        r.id = id;

        return r;
    }

    function addCell(r, index, id, child) {
        let c = r.insertCell(index);
        c.className = id;
        c.id = id;
        c.append(child);

        return c;
    }

    if (elems && elems.length > 0) {
        let r, t;

        try {
            t = document.createElement('table');
            t.setAttribute("style", "display: inline;");
            r = addRow(t, 0, 'btn_container');
            addCell(r, -1, 'btn_packages', SC_ButtonsHelper.createFormElement('Product Packages', '/apex/c__CS_PackageContainer', 'custom-package-form', 'Packages', 'package-iframe'));
            elems[0].append(t);
        } catch (e) {
            clearInterval(btn_container_interval);
        }

        clearInterval(btn_container_interval);
    }
};

let SC_ButtonsHelper = {};

SC_ButtonsHelper.createFormElement = function(label, action, formId, formName, formTarget) {

    function createButton() {

        let btn = document.createElement('button');
        btn.innerHTML = label;
        btn.classList = 'slds-button slds-button_neutral';
        btn.style = 'cursor: pointer;'
        btn.onclick = function() {
            SC_Packages.appendPackageModal();
            document.getElementById(formId).action = action;
        };
        return btn;
    }

	let formElem = document.createElement('form');
    
    formElem.method = 'slds-button slds-button_neutral';
    formElem.id = formId;
    formElem.name = formName;
    formElem.target = formTarget;
    formElem.method = 'POST';
    
    formElem.append(createButton());    

    return formElem;  
};

// Freeze object to prevent accidental overrides
Object.freeze(SC_Buttons);
Object.freeze(SC_ButtonsHelper);

var btn_container_interval = setInterval(SC_Buttons.setButtonContainer, 300);