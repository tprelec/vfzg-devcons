require(["cs_js/cs-full"], function (CS) {
    CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_SAVE_ACTION, function (payload) {
        console.log("STATE MANAGEMENT - AFTER_SAVE_ACTION");
    });
    
    CS.EventHandler.subscribe(CS.EventHandler.Event.CONFIGURATOR_READY, function (payload) {
        console.log("STATE MANAGEMENT - CONFIGURATOR_READY");

        var ProductName = getRootDefinitionName();
        if(ProductName == 'Mobile CTN profile'){
            hideSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Mobile_usage']");
            CS.Rules.evaluateAllRules();
        }
        
        if(ProductName == 'Company Level Fixed Voice') {
            showHideAddButtonCompanyLevelVoice();
            hideSpecificElement("a[data-cs-ref='Company_Level_Fixed_Voice:Addons']");
        }
        
    });
    
    
    CS.EventHandler.subscribe(CS.EventHandler.Event.SCREEN_LIST_POPULATED, function (payload) {
        console.log("STATE MANAGEMENT - SCREEN_LIST_POPULATED");
        
        frozenBasketWarning();

        var ProductName = getRootDefinitionName();
        if (ProductName == 'Mobile CTN profile') {
            mobileCTNProfileScreenListPopulated();
        }
        else if (ProductName == 'Zakelijke Toestelbetaling') {
            //removeDiscountedPriceForIndirect();
            CS.Rules.evaluateAllRules();
        }
    });
    
    

/*
    CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_ATTRIBUTE_VALUE_UPDATE, function (payload) {
        console.log("STATE MANAGEMENT - AFTER_ATTRIBUTE_UPDATE");
        if(payload.wrapper.reference == 'Mobile_CTN_addon_select_0') {
            beforeSave();
        }
        if(payload.wrapper.reference == 'Mobile_CTN_subscription_0') {
            beforeSave();
        }
    });
*/  
    CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_ATTRIBUTE_UPDATE, function (payload) {
        var ProductName = getRootDefinitionName();
        if (ProductName == 'Company Level Fixed Voice') {
            var attrVal = CS.getAttributeValue("ScreenLoadDone_0");
            if (payload.wrapper.reference == 'DDISectionEnabled_0' && attrVal == "Done") {
                // save fixed usage data before attribute update because once attribute is updated, renderAlways() will try to load that data from DataStorageHidden_0
                saveDataStorageHidden();
            }
        }
    });

    CS.EventHandler.subscribe(CS.EventHandler.Event.RULES_STARTED, function (payload) {
        console.log("STATE MANAGEMENT - RULES_STARTED");
    });

    CS.EventHandler.subscribe(CS.EventHandler.Event.RULES_FINISHED, function (payload) {
        console.log("STATE MANAGEMENT - RULES_FINISHED");

        var ProductName = getRootDefinitionName();

        
        if (ProductName === 'IPVPN' || ProductName == 'Company Level Fixed Voice') {
            if (typeof loadAddOnManager === 'function') {
                loadAddOnManager();
            }

            if (CS.AOM) {
                CS.AOM.WebService.runAddOnManager();
            }
        }

        if(ProductName == 'Company Level Fixed Voice') {
                showHideAddButtonCompanyLevelVoice();
                hideSpecificElement("a[data-cs-ref='Company_Level_Fixed_Voice:Addons']");
        }
        
        if (ProductName == 'Zakelijk Internet Pro') {
            console.log('CONFIGURATOR UI RULES_FINISHED catched!');
            calculateCFSMappingList();
            getDeliveryComponentName();
        }
        
        if (ProductName == 'Mobile CTN profile') {
            checkConnectionTypeForRedPro();
            checkToestelbetalingAndHardware();
            if (CS.UI.getCurrentInstance().getCurrentScreen().reference == 'Mobile_CTN_profile:Mobile_usage') {
                validateMUData();
            }
        }
        
        
    });


    subscribeEvent(CS.EventHandler.Event.AFTER_NEXT_SCREEN_ACTION, function (payload) {
        if (isScreenReferenceFound('Mobile_CTN_profile:Default_Screen')) {
            hideSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Mobile_usage']");
        }
        
        if (isScreenReferenceFound('Mobile_CTN_profile:Addons')) {
            showSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Mobile_usage']");
        }

        var ProductName = getRootDefinitionName();
        if (ProductName == 'Mobile CTN profile'){
            beforeSave();
        }
        
        /*
        if (isScreenReferenceFound('Company_Level_Fixed_Voice:Addons')) {
            addOnManagerLoaded = false;
            if (typeof loadAddOnManager === 'function') {
                loadAddOnManager();
            }
            
        }
        */
        
    }, true);
    
    
    subscribeEvent(CS.EventHandler.Event.AFTER_PREVIOUS_SCREEN_ACTION, function (payload) {
        if (isScreenReferenceFound('Mobile_CTN_profile:Default_Screen')) {
            hideSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Mobile_usage']");
        }
        
        if (isScreenReferenceFound('Mobile_CTN_profile:Addons')) {
            showSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Mobile_usage']");
        }

        var ProductName = getRootDefinitionName();
        if (ProductName == 'Mobile CTN profile'){
            beforeSave();
        }
        
        /*
        if (isScreenReferenceFound('Company_Level_Fixed_Voice:Addons')) {
            addOnManagerLoaded = false;
            if (typeof loadAddOnManager === 'function') {
                loadAddOnManager();
            }
            
        }
        */
        
    }, true);
    
    
    subscribeEvent(CS.EventHandler.Event.BEFORE_NEXT_SCREEN_ACTION, function (payload) {
        var ProductName = getRootDefinitionName();
        if (ProductName == 'Company Level Fixed Voice') {
            addOnManagerLoaded = false;
            if (typeof loadAddOnManager === 'function') {
                loadAddOnManager();
            }
        }
    }, true);

    CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_SHOW_SCREEN_ACTION, function (payload) {
        console.log("STATE MANAGEMENT - AFTER_SHOW_SCREEN_ACTION");

        if (isScreenReferenceFound('Mobile_CTN_profile:Default_Screen')) {
            hideSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Mobile_usage']");
        }
        
        if (isScreenReferenceFound('Mobile_CTN_profile:Addons')) {
            showSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Mobile_usage']");
        }
        var ProductName = getRootDefinitionName();
        if (ProductName == 'Mobile CTN profile'){
            beforeSave();
        } else if (ProductName == 'Company Level Fixed Voice') {
            var valueSLD = CS.getAttributeValue("ScreenLoadDone_0");
            if (valueSLD != "Done") {
                CS.setAttributeValue("ScreenLoadDone_0","Done");
            }
        }
    });
    
    CS.EventHandler.subscribe(CS.EventHandler.Event.BUTTONS_DISPLAYED, function (payload) {
        console.log("STATE MANAGEMENT - BUTTONS_DISPLAYED");
        
        frozenBasketWarning();

    });

    CS.EventHandler.subscribe(CS.EventHandler.Event.DEFERRED_UI_UPDATES_COMPLETE, function (payload) {
        console.log("STATE MANAGEMENT - DEFERRED_UI_UPDATES_COMPLETE");
        
        frozenBasketWarning();

        var ProductName = getRootDefinitionName();

        if (ProductName == 'Access Infrastructure') {
            jQuery("span.select2-hidden-accessible").hide();
            removeFirstColumnAndRemoveNameLink();
            renderLookupsForAccessInfra();
            CS.EAPI.hideAllAdditionButtons();
            
            //Access infra config ready removed from eapi2
            showHideModularSections(); 
            checkModeAndSet();
            togglePBXField(CS.getAttributeValue('Technology_type_0') === 'IP' && CS.getAttributeValue('One_Fixed_0') != '');
            hideOtherPriceItem();
        } 
        
        /*
        else if (ProductName == 'Company Level Fixed Voice') {
            showHideAddButtonCompanyLevelVoice();
             hideSpecificElement("a[data-cs-ref='Company_Level_Fixed_Voice:Addons']");
        }
        */
        else if (ProductName == 'Mobile CTN profile') {
            CS.UI.NotificationMessages.remove('Please proceed to Addons screen', { messageType: CS.UI.NotificationMessages.MESSAGE_TYPE.INFO, updateUI: true })
            CS.UI.NotificationMessages.remove('Please proceed to Mobile usage screen', { messageType: CS.UI.NotificationMessages.MESSAGE_TYPE.INFO, updateUI: true })
            CS.UI.NotificationMessages.remove('Please proceed to next screen', { messageType: CS.UI.NotificationMessages.MESSAGE_TYPE.INFO, updateUI: true })

            
            if (CS.UI.getCurrentInstance().getCurrentScreen().reference =='Mobile_CTN_profile:Default_Screen') {
                //show message and addons tab if addons can be generated
                if(checkIfMobileAddonsCanBeGenerated()){
                    CS.displayInfo('Please proceed to Addons screen');
                    showSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Addons']");
                }
                //if current configuration is still valid but addons cannot be generated
                else if(checkIfCurrentStateOfMobileCTNProfileIsValid()){
                    CS.displayInfo('Please proceed to next screen');
                    hideSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Addons']");
                }
                //current configuration is not valid - no message will be shown to the user
                else{
                    hideSpecificElement("a[data-cs-ref='Mobile_CTN_profile:Addons']");
                }

                if (CS.getAttributeValue('Scenario_0') == 'Flex Mobile IoT') {
                    jQuery('label[data-cs-label ="Add_Basic_VPN_0"]').closest('td').hide();
                    jQuery('label[data-cs-label ="Voice_Services_Needed_0"]').closest('td').hide();
                } else if (CS.getAttributeValue('Scenario_0') != 'Flex Mobile') {
                    jQuery('label[data-cs-label ="Add_Basic_VPN_0"]').closest('td').show();
                    jQuery('label[data-cs-label ="Voice_Services_Needed_0"]').closest('td').show();
                } else {
                    jQuery('label[data-cs-label ="Voice_Services_Needed_0"]').closest('td').show();
                }
            }
        
            else if (CS.Service.getCurrentScreen().reference == 'Mobile_CTN_profile:Addons') {
                CS.displayInfo('Please proceed to Mobile usage screen');
            }

            else if (CS.UI.getCurrentInstance().getCurrentScreen().reference == 'Mobile_CTN_profile:Mobile_usage') {
                if (CS.getAttributeValue('Scenario_0') == 'Flex Mobile IoT') {
                    jQuery('#muGeneralInformation').closest("section").hide();
                    jQuery('#muOutgoingCallsNetherlands').closest("section").hide();
                    jQuery('#muOutgoingCallsAbroad').closest("section").hide();
                    jQuery('#muIncomingCallsAbroad').closest("section").hide();
                    jQuery('#muOutgoingCallsToInternational').closest("section").hide();
                    jQuery('#muSmsUsageDetails').closest("section").hide();
                    hideSpecificElement("[data-cs-binding='Mobile_Usage_Product_0']");
                } else {
                    jQuery('#muGeneralInformation').closest("section").show();
                    jQuery('#muOutgoingCallsNetherlands').closest("section").show();
                    jQuery('#muOutgoingCallsAbroad').closest("section").show();
                    jQuery('#muIncomingCallsAbroad').closest("section").show();
                    jQuery('#muOutgoingCallsToInternational').closest("section").show();
                    jQuery('#muSmsUsageDetails').closest("section").show();
                    showSpecificElement("[data-cs-binding='Mobile_Usage_Product_0']");
                }
            }
        
        }
        else if (ProductName == 'Company Level Fixed Voice') {
            if (isScreenReferenceFound("Company_Level_Fixed_Voice:Addons")) {
                setTimeout(function(){ removeRelatedProductButtons(['Addons'],['Edit']); }, 0);
            }
        }
        else if (ProductName == 'Zakelijke Toestelbetaling') {
            //removeDiscountedPriceForIndirect();
        }
        
        // do this for all products
        renderLookups();
        if (ProductName != 'Zakelijk Internet'){
            disableRelatedProductNameClick();
        }
    });

    CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_FINISH_ACTION, function (payload) {
        console.log("STATE MANAGEMENT - BEFORE_FINISH_ACTION");
        if (ProductName == 'Mobile CTN profile' && CS.UI.getCurrentInstance().getCurrentScreen().reference == 'Mobile_CTN_profile:Mobile_usage') {
            storeData();            
        } else if (ProductName == 'Company Level Fixed Voice') {
            var valueSLD = CS.getAttributeValue("ScreenLoadDone_0");
            if (valueSLD == "Done") {
                CS.setAttributeValue("ScreenLoadDone_0","");
            }
        }
    });

    CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_SAVE_ACTION, function (payload) {
        console.log("STATE MANAGEMENT - BEFORE_SAVE_ACTION");
        if (ProductName == 'Mobile CTN profile' && CS.UI.getCurrentInstance().getCurrentScreen().reference == 'Mobile_CTN_profile:Mobile_usage') {
            storeData();
            validateMUData();
        }
    });
    
    function checkIfCurrentStateOfMobileCTNProfileIsValid(){
        var validationResult = CS.UI.getCurrentInstance().validateCurrentConfiguration(); 
        var configurationStatus = CS.getConfigurationProperty('', 'status');
        var result = false;
        if(configurationStatus != 'Incomplete'&& validationResult.isValid){
            return true;
        }
        else{
            return false;
        }
    }
    function checkIfMobileAddonsCanBeGenerated(){
        var result = false;
        var validationResult = CS.UI.getCurrentInstance().validateCurrentConfiguration(); 
        var configurationStatus = CS.getConfigurationProperty('', 'status');
        var nbrSubscription = CS.Service.config["Mobile_CTN_subscription_0"].relatedProducts.length;
        var nbrAddonSelect = CS.Service.config["Mobile_CTN_addon_select_0"].relatedProducts.length;
        
        if(nbrSubscription > 0 && nbrAddonSelect >0 && !checkIfEmptyValueInRelatedProduct('Mobile_CTN_addon_select_0', 'Mobile_addon_0') && checkIfCurrentStateOfMobileCTNProfileIsValid()){
            return true;
        }
        else{
            return false;
        }
    }
    function checkIfEmptyValueInRelatedProduct(element, attRef){
        var emptyValue = false;
        if(CS.Service.config[element]!=null){
            for(var x in CS.Service.config[element].relatedProducts){
                var itm = CS.Service.config[element].relatedProducts[x];
                if(itm.reference){
                    var value = CS.getAttributeValue(itm.reference+':'+attRef);
                    if(value=='' || value==null){
                        emptyValue = true;
                    }
                }
            }
        }
        return emptyValue;
    }

    function getRootDefinitionName() {
        var index = CS.Service.getProductIndex();
        var definitionId = CS.Service.config[""].config['cscfga__Product_Definition__c'];
        return index.all[definitionId].Name;
    }

    function isScreenReferenceFound(screenRefName) {
        return CS.Service.getCurrentScreen().reference === screenRefName;
    }

    function showSpecificElement(elementRef) {
        if(jQuery(elementRef) != undefined)
            jQuery(elementRef).show();
    }

    function hideSpecificElement(elementRef) {
        if(jQuery(elementRef) != undefined)
            jQuery(elementRef).hide();
    }

    function togglePBXField(isVisible) {
        if (isVisible) {
            jQuery("[data-cs-label='PBX_0']").closest('td').show();
        } else {
            jQuery("[data-cs-label='PBX_0']").closest('td').hide();
        }
    }

    function showHideAddButtonCompanyLevelVoice() {
        if(CS.Service.config["RoW_bundle_product_0"].relatedProducts.length > 0) {
            jQuery("button[data-cs-ref='RoW_bundle_product_0']").hide();
        } else {
            jQuery("button[data-cs-ref='RoW_bundle_product_0']").show();
        }
        jQuery("[data-cs-action='editRelatedProduct']").unbind();
    }

    function disableRelatedProductNameClick() {
        jQuery("[data-cs-action='editRelatedProduct']").each(function() {
            if(jQuery(this).text() != 'Edit') {
                jQuery(this).after("<span>" + jQuery(this).text() + "</span>");
                jQuery(this).remove();
            }
        });
    }
    
    function hideOtherPriceItem()
    {
        var disallowedProfiles = ["VF Partner Portal User"];

        if(disallowedProfiles.indexOf(CS.getUserProfile()) != -1)
        {   
            jQuery('[data-cs-binding="Other_price_items_0"]').hide();
        }
    }

    
    // Zakelijke Toestelbetaling
    
    function removeDiscountedPriceForIndirect() {
        var directIndirect = CS.Service.config[""].linkedObjectProperties.DirectIndirect__c;
        if (directIndirect == 'Indirect') {
            jQuery('label[data-cs-label ="DiscountedPrice_0"]').closest('td').hide();
        }
        else {
            jQuery('label[data-cs-label ="DiscountedPrice_0"]').closest('td').show();
        }
    }
});