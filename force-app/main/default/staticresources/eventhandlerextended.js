window.subscribedEvents = {};

window.subscribedEventsNumberOfExecutions = {};

function returnListOfAllSubscribedEvents() {
    return subscribedEvents;
}

function returnListOfExecutedEventsNumber() {
    return subscribedEventsNumberOfExecutions;
}

require(['cs_js/cs-full', 'cs_js/cs-event-handler', 'cs_js/csservice'],
 function(CS, EventHandler, Service) {

    window.enableEventInformation = function enableEventInformation() {
        console.warn('This is used only for troubleshooting purposes...');
        var events = CS.EventHandler.Event;

        var output = [], item;

        for (var type in events) {
            item = {};
            item.type = type;
            item.name = events[type];
            output.push(item);
        }

        output.forEach(function( index ) {
            CS.EventHandler.subscribe(index.name, function(payload) { 
                console.error('Running event: ' + index.name + ' with payload: ' + JSON.stringify(payload)); 
             });
        });
    }

    window.subscribeEvent =  function subscribeEvent(event, functionWithEventActions, force) {
        if(window.subscribedEvents[event] == null || window.subscribedEvents[event] == '') {
            subscribeEventInternal(event, functionWithEventActions);
            window.subscribedEventsNumberOfExecutions[event] = 0;
        } else {
            if(force) {
                console.warn('You are trying to subscribe Event that has already been subscribed. Force flag is true and previous event will be overwritten.');
                CS.EventHandler.unsubscribe(event, window.subscribedEvents[event]);
                subscribeEventInternal(event, functionWithEventActions);
            } else {
                console.warn('You are trying to subscribe Event that has already been subscribed. Force flag is false and previous event will NOT be overwritten.');
            }
        }
    }

    function subscribeEventInternal(event, functionWithEventActions) {
        window.subscribedEvents[event] = EventHandler.subscribe(event, function(payload) { 
            console.log('[EXECUTION START] event: ' + event + '; subscribed event id: ' + subscribedEvents[event]);
            window.subscribedEventsNumberOfExecutions[event]++;
            functionToExecute(payload, functionWithEventActions);
            console.log('[EXECUTION END] event: ' + event + '; subscribed event id: ' + subscribedEvents[event]);
        });
    }

    function functionToExecute(payload, callback) {
        callback(payload);
    }
});
