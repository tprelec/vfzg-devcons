'use strict';

// ES6 Promise polyfill for IE
!function(e){function n(){}function t(e,n){return function(){e.apply(n,arguments)}}function o(e){if("object"!=typeof this)throw new TypeError("Promises must be constructed via new");if("function"!=typeof e)throw new TypeError("not a function");this._state=0,this._handled=!1,this._value=void 0,this._deferreds=[],s(e,this)}function i(e,n){for(;3===e._state;)e=e._value;return 0===e._state?void e._deferreds.push(n):(e._handled=!0,void o._immediateFn(function(){var t=1===e._state?n.onFulfilled:n.onRejected;if(null===t)return void(1===e._state?r:u)(n.promise,e._value);var o;try{o=t(e._value)}catch(i){return void u(n.promise,i)}r(n.promise,o)}))}function r(e,n){try{if(n===e)throw new TypeError("A promise cannot be resolved with itself.");if(n&&("object"==typeof n||"function"==typeof n)){var i=n.then;if(n instanceof o)return e._state=3,e._value=n,void f(e);if("function"==typeof i)return void s(t(i,n),e)}e._state=1,e._value=n,f(e)}catch(r){u(e,r)}}function u(e,n){e._state=2,e._value=n,f(e)}function f(e){2===e._state&&0===e._deferreds.length&&o._immediateFn(function(){e._handled||o._unhandledRejectionFn(e._value)});for(var n=0,t=e._deferreds.length;n<t;n++)i(e,e._deferreds[n]);e._deferreds=null}function c(e,n,t){this.onFulfilled="function"==typeof e?e:null,this.onRejected="function"==typeof n?n:null,this.promise=t}function s(e,n){var t=!1;try{e(function(e){t||(t=!0,r(n,e))},function(e){t||(t=!0,u(n,e))})}catch(o){if(t)return;t=!0,u(n,o)}}var a=setTimeout;o.prototype["catch"]=function(e){return this.then(null,e)},o.prototype.then=function(e,t){var o=new this.constructor(n);return i(this,new c(e,t,o)),o},o.all=function(e){var n=Array.prototype.slice.call(e);return new o(function(e,t){function o(r,u){try{if(u&&("object"==typeof u||"function"==typeof u)){var f=u.then;if("function"==typeof f)return void f.call(u,function(e){o(r,e)},t)}n[r]=u,0===--i&&e(n)}catch(c){t(c)}}if(0===n.length)return e([]);for(var i=n.length,r=0;r<n.length;r++)o(r,n[r])})},o.resolve=function(e){return e&&"object"==typeof e&&e.constructor===o?e:new o(function(n){n(e)})},o.reject=function(e){return new o(function(n,t){t(e)})},o.race=function(e){return new o(function(n,t){for(var o=0,i=e.length;o<i;o++)e[o].then(n,t)})},o._immediateFn="function"==typeof setImmediate&&function(e){setImmediate(e)}||function(e){a(e,0)},o._unhandledRejectionFn=function(e){"undefined"!=typeof console&&console&&console.warn("Possible Unhandled Promise Rejection:",e)},o._setImmediateFn=function(e){o._immediateFn=e},o._setUnhandledRejectionFn=function(e){o._unhandledRejectionFn=e},"undefined"!=typeof module&&module.exports?module.exports=o:e.Promise||(e.Promise=o)}(this);

function openNav() {
    var mySideNav = document.getElementById("mySidenav");
    mySideNav.style.width = "840px";
    mySideNav.style.overflowX = 'hidden';
    var closeBtn = document.getElementById("mySideNavCloseBtn");
    closeBtn.style.display = 'block';
}

//VF
function recalculateTotals() {
    var totalGross = 0;
    var contractTerm = CS.getAttributeValue('Contractterm_0');
    jQuery('.gross').each(function() {
        if (jQuery(this).attr('value') != undefined && jQuery(this).attr('value') != '') {
            totalGross += parseFloat(jQuery(this).attr('value'));
        }   
    });
    var total = parseFloat(contractTerm) * totalGross;
    total = total.toFixed(2);
    totalGross = totalGross.toFixed(2);
    jQuery('#totalGross').html(totalGross).css("font-size","15px");
    jQuery('#netOrder').html(total).css("font-size","15px");
    CS.setAttribute('Net_order_gross_0', totalGross);
}
//end VF

function closeNav() {
    var mySideNav = document.getElementById("mySidenav");
    mySideNav.style.width = "0px";
    mySideNav.style.overflowX = 'visible';
    var closeBtn = document.getElementById("mySideNavCloseBtn");
    closeBtn.style.display = 'none';
}

function hideWidgetButton() {
    jQuery('.navButton').hide();
}

function showOnlyThisContainer(ContainerId) {
    jQuery('.tableContainer').hide();
    jQuery(ContainerId).show();
}

function findLastSelectOptionSelected(name) {
    return jQuery(jQuery('select[name^="' + name + '"]')[jQuery('select[name^="' + name + '"]').length - 1]).val();
}

// 'cs_js/cs-full' - this is needed for 1.461 config and higher
// './src/cs-full' - this is needed for 1.460 config and below
require(['cs_js/cs-full', 'cs_js/cs-event-handler'], function(CS, EventHandler) {
    var domRootElement = '.pmWidget';
    var preparingWidgetMessage = '<div>Preparing widget...</div>';
    var isInitialOpen = true;
    var priceItemId; // holds currently selected Price Item Id value
    var emptyPromise = Promise.resolve();
    var configAttrCache = {};
    var priceItemAttrCache = {}; // holds instance of __PriceItemReference__ attribute
    var allAddOnAssociationsForPriceItem = []; // holds all possible addon associations for currently selected Price Item Id
    var addOnsForPriceItemCache = {}; // holds all possible addons for Related Product and currently selected Price Item Id
    var groupMaxMap = {};

    var groupMinMaxValidation = {}; // RELATED_PRODUCT: { GROUP: {MIN_COUNT: COUNT, MAX_COUNT: COUNT}}

    var addOnIdsToAdd = [];
    var addOnIdsToDelete = [];
    var grpIds = [];
    var onlyDefaultAddOnPriceItems = []; // holds only DEFAULT QUANTITY > 0
    var allSelectedAddOnPriceItems = []; //holds all AddOnPriceItems which should be persisted during Price Item change process
    var selectedAddOn;
    var firstRunFlagRef;
    var priceItemObject;
    var currentConfigRef; // holds reference of config which opened PW
    var customPluginJsonObject = {}; // this variable will be populated dynamically from custom settings as an object
    
    var allRelatedProductAttributes;
    
    var priceItemAttr;
    
    //var relatedProductGroupsMinMax = {}; // will hold Related Products and its Groups with Min and Max per Group; Will be used for validation if Min per Group is added or stop the user adding more than Max per Group 
    
    var currentlySelectedAssociations = {}; // holds data relevant to selected associations and used in the moment of checking MIN and MAX validations for specific group in related product
    
    var refreshInvoked = false;
    var configuratorReady = false;
        
    //VF Update
    //var selectedAddOn;
    
    
    
    //new afterUpdate
    var priceItemAttrCache = {};
    var priceItemId = '';
    
    //CPE split start
    window.tryToFindAMach = function tryToFindAMach(results, numLan, numWan){
        var result = {};
        var bestIndex = [];
        for(var idx in results){
            console.log('Iteration '+idx);
            if(((results[idx].Number_of_LAN__c >= numLan) && (results[idx].Number_of_WAN__c >= numWan))
            || ((results[idx].Number_of_LAN__c >= numLan) && ((results[idx].Number_of_WAN__c + results[idx].Number_of_LANWAN__c) >= numWan)) 
            || ((results[idx].Number_of_WAN__c >= numWan) && ((results[idx].Number_of_LAN__c + results[idx].Number_of_LANWAN__c) >= numLan))  
            || (results[idx].Number_of_LANWAN__c >= (numLan + numWan))){
                bestIndex.push(results[idx]);
                console.log('Including '+results[idx].Id+' because ');
                if((results[idx].Number_of_LANWAN__c >= (numLan + numWan))){
                    console.log('option 4');
                }
    
                if((results[idx].Number_of_WAN__c >= numWan) && ((results[idx].Number_of_LAN__c + results[idx].Number_of_LANWAN__c) >= numLan))  {
                    console.log('option 3 ->');
                    console.log('numWan =='+numWan);
                    console.log('numLan =='+numLan);
                    console.log('results[idx].Number_of_LAN__c =='+results[idx].Number_of_LAN__c);
                    console.log('Number_of_LANWAN__c =='+results[idx].Number_of_LANWAN__c);
                    console.log('Number_of_WAN__c =='+results[idx].Number_of_WAN__c);
    
                }
    
                if((results[idx].Number_of_LAN__c >= numLan) && ((results[idx].Number_of_WAN__c + results[idx].Number_of_LANWAN__c) >= numWan))  {
                    console.log('option 2');
                }
    
                if((results[idx].Number_of_LAN__c >= numLan) && (results[idx].Number_of_WAN__c >= numWan)) {
                    console.log('option 1');
                }
            }
        }
    
        console.log('BEST INDEX ==');
        console.log(bestIndex);
        console.log(bestIndex.length);
        if(bestIndex.length>0){
            //return cheapest
            result = bestIndex[0];
            for(var idx in bestIndex){
                if(bestIndex[idx].cspmb__One_Off_Cost__c < result.cspmb__One_Off_Cost__c){
                    result = bestIndex[idx];
                }
            }
            return result;
        }
        else{
            return false;
        }
    }
    
    
    window.findBest = function findBest(results, numLan, numWan){
        var selected = [];
        console.log('Starting -- find best');
        var match = tryToFindAMach(results, numLan, numWan);
        console.log('Found a match = '+match);
        if(match){
            console.log('Found match ==');
            selected.push(match);
            console.log('---end---');
            return selected;
        }
        else{
            console.log('Did not found a match --- starting iterations ');
            var leftLan = 0;
            var leftWan = 0;
    
            if(numLan>0){
                leftLan = 1;
                numLan = numLan-1;
            }
    
            if(numWan>0){
                leftWan = 1;
                numWan = numWan-1;
            }
    
    
            while((numLan>0)||(numWan>0)){
                var match = tryToFindAMach(results, numLan, numWan);
    
                if(match){
                    selected.push(match);
                    numLan = 0;
                    numWan = 0;
                }
                else{
                    if(numLan>0){
                        leftLan = leftLan+1;
                        numLan = numLan-1;
                    }
                    if(numWan>0){
                        leftWan = leftWan+1;
                        numWan = numWan-1;
                    }
                }
            }
    
            console.log('Currently selected after 1st round ');
            console.log(selected);
            console.log('Remaining Lan='+leftLan+'  wan='+leftWan);
            console.log(selected);
            var match = tryToFindAMach(results, leftLan, leftWan);
    
            if(match){
                selected.push(match);
                
            }
            console.log('Currently selected after 2nd round ');
            console.log(selected);
    
            console.log('done selecting');
            return selected;
        }
    }
    //CPE split end
    
    function initAfterUpdateAccessInfra(context){
        /*
        var priceItemAttr = findPriceItemAttributeForConfigRef(context+'EVC_PVC_0');
        
        if (priceItemId != priceItemAttr.attr.cscfga__Value__c && priceItemAttr.attr.cscfga__Value__c != '') {
            priceItemId = priceItemAttr.attr.cscfga__Value__c;
            
            if(!CS.evcPvcOldValue){ 
                CS.evcPvcOldValue = CS.getAttributeValue(context+'EVC_PVC_0'); 
            }
            
            if(CS.Service.config["Access_Infrastructure_0"].relatedProducts.length > 0){
                _.each(CS.Service.config["Access_Infrastructure_0"].relatedProducts,function(p){
                           CS.Service.removeRelatedProduct('Access_Infrastructure_0'); 
                    });
                }
                
            if(CS.getAttributeValue('Access_set_0') !== ''){
                    CS.setAttributeValue('Access_set_0',''); 
                }
            
                if(CS.getAttributeValue('EVC_done_0') !== ''){ 
                CS.setAttributeValue('EVC_done_0',''); 
                } 
        }
        */
        
        console.log('Commented out');
    }
    
    
    function initAfterUpdateNew() {
                    console.log('--STARTED--'); 
                    var priceItemAttr = findPriceItemAttributeForConfigRef('Subscription_Profile_0');
                    
                        console.log('---FOUND ATTR---'+priceItemAttr); 
                        console.log(priceItemAttr);
                        
                        console.log('----priceItemId =='+priceItemId);
                        console.log('----priceItemAttr.attr.cscfga__Value__c =='+priceItemAttr.attr.cscfga__Value__c);
                    if (priceItemId != priceItemAttr.attr.cscfga__Value__c && priceItemAttr.attr.cscfga__Value__c != '') {
                        priceItemId = priceItemAttr.attr.cscfga__Value__c;
        
                        console.log('---priceItemId'+priceItemId); 
                        
                                                
                        if(!CS.subscriptionProfile){ 
                        CS.subscriptionProfile = CS.getAttributeValue('Subscription_Profile_0'); 
                        } 
        
                    //if(properties.value != CS.subscriptionProfile && att == 'Subscription_Profile_0'){    
                            console.log('Subscription Type changed new'); 
        
                            if(CS.getAttributeValue('Price_Item_set_0') != ''){ 
                            CS.setAttributeValue('Price_Item_set_0',''); 
                            } 
        
                            if(CS.getAttributeValue('Wireless_Only_PGA_set_0') != ''){ 
                            CS.setAttributeValue('Wireless_Only_PGA_set_0',''); 
                            } 
        
                                console.log('---BEFORE REMOVAL---'); 
                            _.each(CS.Service.config["Price_Item_Wireless_Standalone_0"].relatedProducts,function(p){ 
                            CS.Service.removeRelatedProduct('Price_Item_Wireless_Standalone_0'); 
                            }); 
                            CS.subscriptionProfile = CS.getAttributeValue('Subscription_Profile_0'); 
                            //} 
                            
                    }
                if (priceItemAttr.attr.cscfga__Value__c == '') {
                    priceItemId = '';
                }
            }
    
    function findPriceItemAttributeForConfigRef(ref) {
        var pi = priceItemAttrCache[ref];
        if (pi) {
            return pi;
        }
    
        var pis = CS.Service.config[ref];
        if (pis.length > 1) {
            console.warn('Multiple __PriceItemReference__ markers found, model is invalid: ', pis);
        }
    
        priceItemAttrCache[ref] = pis;
    
        return pis;
    }
    //end
    
    function buildCompatibilityFunctions() {
            
        function initializeWidget(ref, jsonObject) {
            customPluginJsonObject = jsonObject;
            
            if(customPluginJsonObject.hideWidgetButton) {
                hideWidgetButton();
                //return null;
                }
                
            currentConfigRef = ref;
            
            if(priceItemAttr === undefined)
                priceItemAttr = findPriceItemAttributeForConfigRef('');
    
            firstRunFlagRef = CS.Util.generateReference('First_Run', { ref: currentConfigRef });
                    
            if(!configuratorReady)
                return null;
                        
            // if price item changed
                    if (priceItemId != priceItemAttr.attr.cscfga__Value__c && priceItemAttr.attr.cscfga__Value__c != '') {
                        priceItemId = priceItemAttr.attr.cscfga__Value__c;
        
                if (attributeIsPresent(firstRunFlagRef)) {
                    CS.setAttributeValue(firstRunFlagRef, '', true);
                }

                addOnsForPriceItemCache = {};
                allAddOnAssociationsForPriceItem = [];
                onlyDefaultAddOnPriceItems = [];
                //relatedProductGroupsMinMax = {};
            } else {
                noPriceItemChange(currentConfigRef, priceItemAttr);
            }
                        
            if (priceItemAttr.attr.cscfga__Value__c == '') {
                priceItemId = '';
            }
                                                
            if (priceItemId == '' || priceItemId == undefined) {
                console.info('No Price Item selected - Widget is not refreshed!');
                return null;
                        } 
        
            var firstRunValue = CS.getAttributeValue(firstRunFlagRef);
        
            if (firstRunValue != 'true') {
                priceItemChanged(currentConfigRef, false);
            }
        } 
        
        EventHandler.subscribe(EventHandler.Event.RULES_STARTED, function(payload) {
            console.log('CONFIGURATOR UI RULES_STARTED catched!');
        });
        
        EventHandler.subscribe(EventHandler.Event.RULES_FINISHED, function(payload) {
            console.log('CONFIGURATOR UI RULES_FINISHED catched!');
                            }); 
                            
        EventHandler.subscribe(EventHandler.Event.RULES_ITERATION_START, function(payload) {
            console.log('CONFIGURATOR UI RULES_ITERATION_START catched!');
        });
    
        EventHandler.subscribe(EventHandler.Event.RULES_ITERATION_END, function(payload) {
            console.log('CONFIGURATOR UI RULES_ITERATION_END catched!');
        });
    
        CS.EventHandler.subscribe(CS.EventHandler.Event.AFTER_REMOVE_RELATED_PRODUCT_ACTION, function(payload) {
            console.log('CONFIGURATOR UI AFTER_REMOVE_RELATED_PRODUCT_ACTION catched!');
        });

        CS.EventHandler.subscribe(CS.EventHandler.Event.CONFIGURATOR_READY, function(payload) {
            console.log('CONFIGURATOR UI CONFIGURATOR_READY catched!');
    
            configuratorReady = true;
            widgetUIPrepare();
    
            if(priceItemAttr === undefined || priceItemAttr.attr.cscfga__Value__c === '') {
                return emptyPromise;
    }
        
            priceItemId = priceItemAttr.attr.cscfga__Value__c;
            priceItemChanged(currentConfigRef, true);
        });

        function turnLoadingOverlayOn() {
            var overlayElement = document.getElementById("widgetLoadingOverlay");

            if (overlayElement != undefined)
                overlayElement.style.display = "block";
        }

        function turnLoadingOverlayOff() {
            var overlayElement = document.getElementById("widgetLoadingOverlay");
            
            if (overlayElement != undefined)
                overlayElement.style.display = "none";
            }
            
        function noPriceItemChange(configRef, piAttr) {
            if(piAttr === undefined || piAttr.attr.cscfga__Value__c === '') {
                console.info('No Price Item selected - Widget is not refreshed!');
                turnLoadingOverlayOff();
                return emptyPromise;
            } else if(refreshInvoked) {
                console.info('Widget refresh already invoked!');
                return emptyPromise;
            } else {
                setRefreshInvokedFlag(true);
                // if price item havent change just refresh pricing widget
            
                //refresh it, it will be populated in build widget method
                currentlySelectedAssociations = {};

                displayPricingWidget(domRootElement)
                .then(function() {
                    // this should be developed more generic - but for now for VMB leave it this way
                    if(customPluginJsonObject.vmbImplementation) {
                    setConfigurationNames();
                    setPrices();
                    setLineItemDescriptions();
                    }
                });
        }
        }

        function priceItemChanged(configRef, isInitialOpen) {
            turnLoadingOverlayOn();

            if (attributeIsPresent(firstRunFlagRef)) {
                CS.setAttributeValue(firstRunFlagRef, 'true', true);
            }

            setRefreshInvokedFlag(true);
            priceItemObject = getPriceItemForConfig(configRef);

            priceItemObject
                .then(function(priceItem) {
                    // here maybe delete every related product and clear widget???
                    if (!priceItem || priceItem === '') {
                        console.info('no Price Item selected');
                        turnLoadingOverlayOff();
                        return emptyPromise;
                    } else {
                        //var relatedProductAttributes = findAddOnRelatedProductAttributes(configRef, priceItemObject);
                        var relatedProductAttributes = [];

                        if(allRelatedProductAttributes !== undefined && allRelatedProductAttributes.length > 0) {
                            relatedProductAttributes = Promise.resolve(allRelatedProductAttributes);
                    } else {
                            relatedProductAttributes = findAddOnRelatedProductAttributes(configRef, priceItemObject);
                    }
                        
                        return relatedProductAttributes
                            .then(function(relAttrs) {
                                allRelatedProductAttributes = relAttrs;
                                var listPromises = [];
                                _.each(relAttrs, function(it) {
                                    listPromises.push(getAddOnAssociationsListForAttrCached(it));   
                });

                                return Promise.all(listPromises);
                })
                            // fetched AddOn Assocs for each related product attribute
                            .then(function(values) {
                                for(var i=0; i<values.length;i++) {
                                    var addOns = values[i];
                                    _.each(addOns, function(item) {
                                        if (item.cspmb__default_quantity__c > 0) {
                                            addToADefaultAddOnPriceItemList(item.cspmb__add_on_price_item__c);
                                            addToAllSelectedAddOnPriceItemList(item.cspmb__add_on_price_item__c);
            } 
                });
                                }

                                if(isInitialOpen) {
                                    return displayPricingWidget(domRootElement)
                                    .then(function() {
                                        turnLoadingOverlayOff();
                            });
                    } else {
                                    //return addDefaultAddOns(allRelatedProductAttributes)
                                    return addDefaultAddOns(CS.Service.getCurrentConfigRef())
                                    .catch(function(e) {
                                        console.error('EAPI Compatibility: error adding default Add Ons', e);
                                    })
                                    .then(function() {
                                        CS.Rules.enableRules();
                                        CS.Rules.evaluateAllRules()
                                            .then(function afterRules() {
                                                return displayPricingWidget(domRootElement)
                                                .then(function() {
                                                    // this should be developed more generic - but for now for VMB leave it this way
                                                    if(customPluginJsonObject.vmbImplementation) {
                                                        setConfigurationNames();
                                                        setPrices();
                                                        setLineItemDescriptions();
        }

                                                    turnLoadingOverlayOff();
                        });
                    });
            });
        }
                            });
        }
                });
            }

        function widgetUIPrepare() {
            //show or hide delete selected button
            if(customPluginJsonObject.bulkDeletionEnabled)
                jQuery("#bulkDeleteButton").toggle(true);
            else
                jQuery("#bulkDeleteButton").toggle(false);

            // expand canvas
            jQuery('#configurationContainer').css('max-width', 'none');

            turnLoadingOverlayOff();
            }
            
        function checkMinMaxForGroupsInConfiguration() {
            var invalidGroups = '';
            _.each(groupMinMaxValidation, function(value, key) {
                // each group
                _.each(value, function(childValue, childKey) {
                    var minValue = childValue.min_count;
                    var maxValue = childValue.max_count;
                    var currentValue = childValue.current_count;

                    if(currentValue < minValue && customPluginJsonObject.minValidationEnabled) {
                        invalidGroups += 'Minimum AddOns for the group ' + childKey + ' is ' + minValue + '. ';
                    }

                    if(currentValue > maxValue && customPluginJsonObject.maxValidationEnabled){
                        invalidGroups += 'Maximum AddOns for the group ' + childKey + ' is ' + maxValue + '. ';
                            }
                        });
            });

            if (invalidGroups != '') {
                CS.markConfigurationInvalid(invalidGroups);
        }
        }
        
        /*
         * addDefaultAddOns
         *
         * When a Price Item-based product is first added to the basket, assess for default
         * Add Ons and if found, add the default Add Ons automatically.
         *
         * Since there is no 'add to basket' in the regular Configurator, a newly added
         * product is identified through a 'first run' Attribute used to flag whether this
         * logic has been performed for this configuration.
         */
        function addDefaultAddOns(configRef) {

            function chainAddDefaultAddons(promise, attr) {
                return promise.then(function() {
                    return addDefaultAddOnsForAttribute(attr);
                });
            }
            return getPriceItemForConfig(configRef)
                .then(function(priceItem) {
                    if (!priceItem) {
                        console.info('EAPI compatibility: Skipping addDefaultAddOns Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚Æ’Ã„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã„â€¦Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚ËœÃƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã„â€¦Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚ËœÃ„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Â¹Ãƒâ€šÃ‚ËœÃƒâ€žÃ¢â‚¬Å¡Ãƒâ€¹Ã‚ËœÃ„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚Â¬Ã„â€šÃ¢â‚¬Å¾Ãƒâ€žÃ¢â‚¬Â¦Ã„â€šÃ¢â‚¬Â¹ÃƒÂ¢Ã¢â€šÂ¬Ã‹â€¡Ã„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚Â¬Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã„â€¦Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚ËœÃ„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Â¹Ãƒâ€šÃ‚ËœÃƒâ€žÃ¢â‚¬Å¡Ãƒâ€¹Ã‚ËœÃ„â€šÃ‹ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬Ãƒâ€žÃ„â€¦Ãƒâ€¹Ã¢â‚¬Â¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚Â¬Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ã„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦Ãƒâ€žÃ¢â‚¬Å¡Ãƒâ€¹Ã‚ËœÃ„â€šÃ‹ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬Ãƒâ€žÃ„â€¦ÃƒÂ¢Ã¢â€šÂ¬Ã…Å¸ no Price Item selected');
                        return emptyPromise;
                    }

                    if (productIsNewlyAddedToBasket(configRef)) {
                        console.log('EAPI compatibility: adding default Add Ons');
                        return findAddOnRelatedProductAttributes(configRef, priceItem);
                    } else {
                        console.info('Skipping addDefaultAddOns Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚Æ’Ã„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã„â€¦Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚ËœÃƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã„â€¦Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚ËœÃ„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Â¹Ãƒâ€šÃ‚ËœÃƒâ€žÃ¢â‚¬Å¡Ãƒâ€¹Ã‚ËœÃ„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚Â¬Ã„â€šÃ¢â‚¬Å¾Ãƒâ€žÃ¢â‚¬Â¦Ã„â€šÃ¢â‚¬Â¹ÃƒÂ¢Ã¢â€šÂ¬Ã‹â€¡Ã„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚Â¬Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ã„â€šÃ‹ËœÃƒÂ¢Ã¢â‚¬Å¡Ã‚Â¬Ã„Â¹Ã‹â€¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã„â€¦Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚ËœÃ„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Â¹Ãƒâ€šÃ‚ËœÃƒâ€žÃ¢â‚¬Å¡Ãƒâ€¹Ã‚ËœÃ„â€šÃ‹ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬Ãƒâ€žÃ„â€¦Ãƒâ€¹Ã¢â‚¬Â¡Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ã„â€šÃ¢â‚¬Å¡Ãƒâ€šÃ‚Â¬Ãƒâ€žÃ¢â‚¬Å¡ÃƒÂ¢Ã¢â€šÂ¬Ã…Â¾Ã„â€šÃ¢â‚¬Å¾ÃƒÂ¢Ã¢â€šÂ¬Ã‚Â¦Ãƒâ€žÃ¢â‚¬Å¡Ãƒâ€¹Ã‚ËœÃ„â€šÃ‹ËœÃƒÂ¢Ã¢â€šÂ¬Ã…Â¡Ãƒâ€šÃ‚Â¬Ãƒâ€žÃ„â€¦ÃƒÂ¢Ã¢â€šÂ¬Ã…Å¸ first run complete');
                        return emptyPromise;
                    }
                })
                .then(function(attrList) {
                    return _.reduce(attrList, chainAddDefaultAddons, emptyPromise).then(function() {
      
                    });
                                             
                });

        }
        
        function displayPricingWidget(containerSelector) {
            jQuery(containerSelector).html(preparingWidgetMessage);

            if (!priceItemObject) {
                jQuery(containerSelector).html('');
                return emptyPromise;
        }
        
            var relatedProductAttributesFiltered = allRelatedProductAttributes;

            if(customPluginJsonObject.singleAddOnAssociationList) {
                var configAttrs = getAttributesForConfigRef(currentConfigRef);
        
                // fetch only one related product attribute - the one which has __AllAddOnLookup__ attribute field
                relatedProductAttributesFiltered = _.filter(configAttrs, function (it) {
                    return it.attr && it.attributeFields && it.attributeFields.__AllAddOnLookup__;
                    });
                }

            return buildPricingWidget(relatedProductAttributesFiltered, containerSelector)
                .then(function() {
                    setRefreshInvokedFlag(false);
                    checkMinMaxForGroupsInConfiguration();
                });
        }


        function buildPricingWidget(attrs, selector) {
            var table =
                '       <style type="text/css">  '  +
                '   /* Dropdown Button */  '  +
                '   .dropbtn {  '  +
                '       border: none;  '  +
                '       cursor: pointer;  '  +
                '   }  '  +
                '     '  +
                '   /* Dropdown button on hover & focus */  '  +
                '     '  +
                '     '  +
                '   /* Dropdown Content (Hidden by Default) */  '  +
                '   .dropdown-content {  '  +
                '       display: none;  '  +
                '       position: absolute;  '  +
                '       background-color: #f9f9f9;  '  +
                '       min-width: 500px;  '  +
                '       box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);  '  +
                '       z-index: 1;  '  +
                '   }  '  +
                '     '  +
                '     '  +
                '   /* Change color of dropdown links on hover */  '  +
                '   .dropdown-content a:hover {background-color: #f1f1f1}  '  +
                '     '  +
                '   /* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */  '  +
                '  .show {display:block;}  ' +
                '   </style>  '

                +'<select id="addOnSelect" class="slds-select">'
                + '<option ';

                if (!selectedAddOn || '' == selectedAddOn) {
                    selectedAddOn = 'empty';
                    table = table + 'selected ';
                }

                table = table + ' value="">Select Add On Group</option>'
                + '</select>';
                
                // this is Work In Progress - will be removed from the code and put as Plugin functionality
                // this is used by Virgin Media Business implementation
                /*if(customPluginJsonObject.useCustomGroups) {
                    table = table + '<select id="addOnSelect">'
                + '<option ';

                    if (!selectedAddOn || 'All' == selectedAddOn) {
                        selectedAddOn = 'All';
                    table = table + 'selected ';
                }

                    table = table + ' value="All">All</option>'
                + '</select>'
                };*/

                table = table +'<table class="list" id="addOnTable1" style="background: #fff; border-collapse: collapse; width: 100%" >'
                + '<div id="myDropdown" class="pbHeader dropdown-content">'
                + '<tr class="headerRow">'
                + '<th style="padding-left: 6px; padding-right: 6px; padding-top: 8px">Action</th>'
                + '<th style="padding-right: 6px; padding-top: 8px">Image</th>'
                + '<th style="padding-right: 6px; padding-top: 8px">Quantity</th>'
                + '<th style="padding-right: 6px; padding-top: 8px">Deal Type</th>'
                + '<th style="padding-right: 6px; padding-top: 8px">Add On</th>'
                + '<th style="padding-right: 6px; padding-top: 8px">Group</th>'
                + '<th style="padding-right: 6px; padding-top: 8px">One-Off Charge</th>'
                + '<th style="padding-right: 6px; padding-top: 8px">Recurring Charge</th>'
                + '<th style="padding-top: 8px">Message</th>'
                + '</tr>'
                + '</div>'
                + '</table>';

            table = jQuery(table);

            return _.reduce(attrs, buildPricingWidgetSectionForAttr, Promise.resolve(table))
                .then(function (table) {
                    
                    jQuery(selector).html(table);
                    var grpIds = [];
                    jQuery('.grp').each(function() {
                        grpIds.push(jQuery(this)[0].id);
                    });
                    var groupNames = _.uniq(grpIds);
     
                    _.each(groupNames, function(a) {
                        if ((a != 'Site Components')&&(a != 'undefined')) {
                            jQuery('div.pmWidget #addOnSelect').append(jQuery('<option>', {value: a, text: a}));
                        }
                    });

                    jQuery('div.pmWidget #addOnSelect').change(
                        function(item) {
                            selectedAddOn = item.currentTarget.value;
                            hideAddOns(selectedAddOn);
                        }
                    );

                    hideAddOns(selectedAddOn);
                    jQuery('div.pmWidget #addOnSelect').val(selectedAddOn).trigger("change");

                }).catch(function(err) {
                    console.error('EAPI compatibility: cannot display pricing widget.', err);
                    return Promise.reject(err);
                });
        }

        /*
         * finds last quantity input element. workaround
         *
         */
        function findLastInputElementValue(addOnAssocId) {
            return jQuery(jQuery('input[id^="' + addOnAssocId + '"]')[jQuery('input[id^="' + addOnAssocId + '"]').length - 1]).val();
        }

        /*
         * buildPricingWidgetSectionForAttr
         *
         * Used by buildPricingWidget to contruct the table section for the supplied
         * related product attribute, chained to the supplied promise.
         */
        function buildPricingWidgetSectionForAttr(promise, attr) {
            return promise.then(function(table) {
                var headerRow = jQuery('<tr><th style="background: #ddd; padding: 6px" colspan="9">Related Product: ' + (attr.attr.cscfga__Label__c || attr.attr.Name) + '</th></tr>');
                table.append(headerRow);
                return getAddOnAssociationsListForAttrCached(attr)
                    .then(function(assocs) {
                        var currentlySelectedAddOnIds = [];
                        if(customPluginJsonObject.singleAddOnAssociationList) {
                            //save all AddOns for selected Price Item - resolved promise
                            allAddOnAssociationsForPriceItem = assocs;
                            _.each(CS.Service.config, function (it) {
                                if (it && it.attributeFields && it.attributeFields.__AddonGenerator__ && it.reference.lastIndexOf(customPluginJsonObject.addOnPriceItemAssociationLookupAttributeName + '_0') != -1) {
                                    currentlySelectedAddOnIds.push(it.attr.cscfga__Value__c);
                                }
                            });
                        } else {
                            currentlySelectedAddOnIds = getSelectedAddOnIds(attr);
                }

                        if(customPluginJsonObject.hideDefaultAddOns) {
                            var withoutDefaults = [];

                            for(var i=0; i<assocs.length; i++) {
                                var assocTmp = assocs[i];

                                if(!_.includes(onlyDefaultAddOnPriceItems, assocTmp.cspmb__add_on_price_item__c)) {
                                    withoutDefaults.push(assocTmp);
                                }
                            }

                            assocs = withoutDefaults;
                        }

                        return buildAddOnListWithGroupAndCardinalityConstraints(assocs, currentlySelectedAddOnIds, attr);
                    })
                    .then(function(addOns) {

                        addOns.sort(function (a, b) {

                            // this is Work In Progress - will be removed from the code and put as Plugin functionality
                            // this is used by Virgin Media Business implementation
                            if(customPluginJsonObject.useCustomGroups) {
                                var descA = a.assoc.custom_group__c ? a.assoc.custom_group__c.toUpperCase() : 0;
                                var descB = b.assoc.custom_group__c ? b.assoc.custom_group__c.toUpperCase() : 0;

                                if (descA < descB) return -1;
                                if (descA > descB) return 1;
                            }
                            /********************************************/

                            var descA1 = a.assoc.add_on_price_item_name__c ? a.assoc.add_on_price_item_name__c.toUpperCase() : 0;
                            var descB1 = b.assoc.add_on_price_item_name__c ? b.assoc.add_on_price_item_name__c.toUpperCase() : 0;

                            if (descA1 < descB1) return -1;
                            if (descA1 > descB1) return 1;
                            return 0;

                        });

                        var recurringTotal = 0;
                        var oneOffTotal = 0;

                        _.each(addOns, function(it) {
                            var attrWrapper = attr;
                            var input;
                            var quantityjQuery ='';
                            var attrValue = 0;
                            var isNew = true;
                            var productReference;
                            var addOnPriceItemName = it.assoc.cspmb__add_on_price_item__r.name;
                            var addOnPriceItemId = it.assoc.cspmb__add_on_price_item__c;

                            if(customPluginJsonObject.singleAddOnAssociationList) {
                                var relatedProductDefinitionName = '';

                                // this should be developed more generic - but for now for VMB leave it this way
                                if(customPluginJsonObject.vmbImplementation) {
                                    relatedProductDefinitionName = it.assoc.cspmb__add_on_price_item__r.related_product_name__c;
                                } else {
                                    relatedProductDefinitionName = it.assoc.cspmb__add_on_price_item__r.cspmb__product_definition_name__c;
                                }

                                attrWrapper = getRelatedProductAttribute(relatedProductDefinitionName, '');

                                if(attrWrapper == null || attrWrapper == undefined || attrWrapper.length < 1 || attrWrapper.length > 1) {
                                    console.error('Related Attribute cannot be found or there are more than 1 found!');
                                    return null;
                                }

                                attrWrapper = attrWrapper[0];
                            }

                            if (jQuery('div[data-cs-binding="' + attr.reference + '"] span:contains("' + it.assoc.cspmb__add_on_price_item__r.name + '")')[0] && it.assoc.add_on_type__c == 'Quantity') {
                                isNew = false;
                                productReference = jQuery(jQuery('div[data-cs-binding="' + attr.reference + '"] span:contains("' + it.assoc.cspmb__add_on_price_item__r.name + '")')).attr( "data-cs-ref" );
                                attrValue = CS.getAttributeValue(productReference + ':Quantity_0');
                            }

                            // this is Work In Progress - will be removed from the code and put as Plugin functionality
                            // this is used by Virgin Media Business implementation
                            if (customPluginJsonObject.useCustomGroups && it.assoc.custom_group__c !== undefined) {
                                grpIds.push(it.assoc.custom_group__c);
                            }
                            /*********************************/

                            if (it.assoc.add_on_type__c == 'Quantity') {
                                input = '<input type="number" value="' + attrValue +'" min="0" id="Quantity' + it.assoc.id + '"></input>';
                                quantityjQuery = '\',CS.EAPI.findLastInputElementValue(\'' + 'Quantity' + it.assoc.id + '\')';
                            } else {
                                input = '<input type="number" value ="1" id="Checkbox' + it.assoc.id + '" readonly></input>';
                                quantityjQuery = '\',1';
                            }

                            var recurringCharge = 0;
                            if (it.assoc.cspmb__recurring_charge__c > 0) {
                                recurringCharge = it.assoc.cspmb__recurring_charge__c;
                            }

                            var oneOffCharge = 0;
                            if (it.assoc.cspmb__one_off_charge__c > 0) {
                                oneOffCharge = it.assoc.cspmb__one_off_charge__c;
                            }

                            //begin
                            /*var row = '';

                            if (customPluginJsonObject.useCustomGroups && it.assoc.custom_group__c !== undefined) {
                                row = '<tr class="grp" groupName="' + it.assoc.custom_group__c + '">';
                            } else {
                                row = '<tr class="grp" groupName="' + it.assoc.cspmb__group__c + '">';
                            }
                                
                            if (!it.isAvailable && it.max == 1 && it.selected) {
                                // check if CS_IsDefaultAddOn_0 exists and it is populated or disableDefaultAddOnsDeletion flag is checked -> remove DELETE link
                                if((_.includes(onlyDefaultAddOnPriceItems, addOnPriceItemId) && customPluginJsonObject.disableDefaultAddOnsDeletion) ||
                                    (attributeIsPresent(attrWrapper.reference + ':CS_IsDefaultAddOn_0') && CS.getAttributeValue(attrWrapper.reference + ':CS_IsDefaultAddOn_0') !== undefined && CS.getAttributeValue(attrWrapper.reference + ':CS_IsDefaultAddOn_0') !== '')) {
                                    // disable deletion link if this assoc is default one
                                    row = row + '<td style="padding: 6px;"></td>';
                                } else{
                                    row = row + '<td style="padding: 6px;"><a href="#!" onclick="CS.EAPI.deleteAddOn(\'' + attrWrapper.reference + '\',\'' + addOnPriceItemId + '\',\'' + it.assoc.cspmb__group__c + '\')">' + 'Delete' + '</a></td>';
                                }
                            } else if (!it.isAvailable && it.max == 1) {
                                row = row + '<td style="padding: 6px;"><a href="#!" onclick="CS.EAPI.changeAddOn(\'' + attrWrapper.reference + '\',\'' + it.assoc.id + '\',\'' + addOnPriceItemId + '\')">' + 'Change' + '</a></td>';
                            } else {
                                row = row + '<td style="padding: 6px;"><a href="#!" onclick="CS.EAPI.insertAddOn(\'' + attrWrapper.reference + '\',\'' + it.assoc.id + quantityjQuery + ',' + isNew + ',\'' + productReference + '\',\'' + addOnPriceItemId + '\'' + ')">' + (it.isAvailable ? 'Add' : '') + '</a></td>';
        }

                            //setting up checkboxes for ADDING or DELETION
                            if (it.isAvailable) {
                                row = row + '<td style="padding: 6px"><input group="' + it.assoc.cspmb__group__c + '" attrref="' + attr.reference + '"  type="checkbox" onclick="CS.EAPI.deselectOtherAddOns(this)" class="addOnSelect" id="' + it.assoc.id + '"></input>' + '</td>';
                            } else {
                                // add checkbox used for possible deletion of records with MAX = 1
                                if(customPluginJsonObject.bulkDeletionEnabled && it.max == 1 && it.selected) {
                                    // check if CS_IsDefaultAddOn_0 exists and it is populated or disableDefaultAddOnsDeletion flag is checked -> remove DELETE link
                                    if((_.includes(onlyDefaultAddOnPriceItems, addOnPriceItemId) && customPluginJsonObject.disableDefaultAddOnsDeletion) ||
                                        (attributeIsPresent(attrWrapper.reference + ':CS_IsDefaultAddOn_0') && CS.getAttributeValue(attrWrapper.reference + ':CS_IsDefaultAddOn_0') !== undefined && CS.getAttributeValue(attrWrapper.reference + ':CS_IsDefaultAddOn_0') !== '')) {
                                        // disable deletion link if this assoc is default one
                                        row = row + '<td style="padding: 6px;"></td>';
                                    } else{
                                        row = row + '<td style="padding: 6px"><input type="checkbox" class="deleteAddOnSelect" id="' + it.assoc.id + '"></input></td>';
                                    }
                                } else {
                                    row = row + '<td style="padding: 6px"></td>';
                            }
                        }

                            if (it.selected) {
                                addToAllSelectedAddOnPriceItemList(addOnPriceItemId);

                                recurringTotal += recurringCharge;
                                oneOffTotal += oneOffCharge;
                                row = row + '<td style="padding: 6px"><b>' + it.assoc.cspmb__add_on_price_item__r.name + '</b></td>';
                            } else {
                                row = row + '<td style="padding: 6px">' + it.assoc.cspmb__add_on_price_item__r.name + '</td>';
                    }

                            row = row + '<td style="text-align: left; padding: 6px">' + asDecimal(oneOffCharge, 2) + '</td>'
                                + '<td style="text-align: left; padding: 6px">' + asDecimal(recurringCharge, 2) + '</td>';

                            
                            // this is Work In Progress - will be removed from the code and put as Plugin functionality
                            // this is used by Virgin Media Business implementation
                            if (customPluginJsonObject.useCustomGroups && it.assoc.custom_group__c !== undefined) {
                                row = row + '<td style="text-align: left; padding: 6px">' + it.assoc.custom_group__c + '</td></tr>';
            } else {
                                row = row + '<td style="text-align: left; padding: 6px">' + it.assoc.cspmb__group__c + '</td></tr>';
                            }*/
                        //end
                        
                        //begin VF widget implementation
                        var row = '<tr>'
                                + '<td style="padding: 6px;"><a href="#" onclick="CS.EAPI.insertAddOn(\'' + attr.reference + '\',\'' + it.assoc.id + quantityjQuery + ',' + isNew + ',\'' + productReference + '\',\'' + it.assoc.id + '\')">' + (it.isAvailable ? 'Add' : '') + '</a></td>';

                            
                            if (it.assoc.cspmb__add_on_price_item__r.image_url__c) {
                                row += '<td style="padding: 6px;"><img alt="" src="' + it.assoc.cspmb__add_on_price_item__r.image_url__c + '" style="width:50px;height:50px;">' + '</td>';
                            } else {
                                row += '<td></td>';
                            }
                            
                            if(attr.attr.Name.indexOf('Addon') != -1){
                            row += '<td>'  + input  + '</td>'
                                + '<td style="padding: 6px">'
                                +                   '<select onchange="updateDropDownValues(this)" class="slds-select" name="dealtype' + it.assoc.id + '">'
                                +                       '<option value="acquisition">Acquisition</option>'
                                +                       '<option value="retention">Retention</option>'
                                +                       '<option value="migration">Migration</option>'
                                +                   '</select>'
                                + '</td>'
                                + '<td style="padding: 6px">' + it.assoc.cspmb__add_on_price_item__r.name + '</td>'
                                + '<td style="padding: 6px" class="grp" id="' + it.assoc.cspmb__group__c + '">' + it.assoc.cspmb__group__c + '</td>'
                                + '<td style="text-align: left; padding: 6px">' + asDecimal(it.assoc.cspmb__add_on_price_item__r.cspmb__one_off_charge__c, 2) + '</td>'
                                + '<td style="text-align: left; padding: 6px">' + asDecimal(it.assoc.cspmb__add_on_price_item__r.cspmb__recurring_charge__c, 2) + '</td>'
                                + '<td style="padding: 6px">' + (it.isAvailable ? '' : it.unavailableReason) + '</td>'
                                + '</tr>';
                        //end VF widget implementation
                        
                        
                            table.append(row);
                            }
                        });
                
                        // this should be developed more generic - but for now for VMB leave it this way
                        if(customPluginJsonObject.vmbImplementation) {
                            try {
                                var ref = CS.UI.getCurrentInstance().getCurrentConfigReference();
                    if (ref !== "") {
                                    ref + '\\:';
                                }

                                CS.setAttributeValue(ref + 'Add_On_Total_Recurring_Price_0', recurringTotal, true);
                                CS.setAttributeValue(ref + 'Add_On_Total_One_off_Price_0', oneOffTotal, true);
                            } catch (e) {
                                console.warn('Could not set attributes Add_On_Total_Recurring_Price_0 and Add_On_Total_One_off_Price_0. ' + e.message);
                            }
                    }

                        return table;
                    });
            });
                }
                
        /*
        *   VMB implementation needed method.
        *
        */
        function getRelatedProductAttribute(attrName, configRef) {
            return _.filter(CS.Service.config, function (it) {
                var ref = '';
                var n = it.reference.lastIndexOf(":");
                if (n != -1) {
                    ref = it.reference.substr(0, n);
            }
                return it.attr && it.attr.Name === attrName && ref === configRef
            });
        }

        /*
         * hideAddOns
         *
         * Hides add ons of specific group
         */
        function hideAddOns(groupName) {
            jQuery('.grp').each(function() {
                    jQuery(this).parent().show();
            });
            if (groupName != 'All') {
                jQuery('.grp').each(function() {
                    if (jQuery(this)[0].id != groupName) {
                        jQuery(this).parent().hide();
                    }
                });
       
            }
        }

        /*
         * Deselects othr add ons if more than group max of add ons selected
         *
         */
        function deselectOtherAddOns(checkbox) {
            var selected = jQuery(checkbox).is(":checked");
            if (selected) {
                var groupName = jQuery(checkbox).attr('group');
                var attrRef = jQuery(checkbox).attr('attrref');

                var groupMax = groupMinMaxValidation[attrRef][groupName].max_count;

                if (groupMax) {
                    var selectedGroupCount = 0;
                    _.each(jQuery('.addOnSelect'), function (it) {
                        var isCheckedAttr = jQuery(it).is(":checked");
                        var groupNameAttr = jQuery(it).attr('group');
                        var attrRefAttr = jQuery(it).attr('attrref');

                        if (isCheckedAttr && groupNameAttr === groupName && attrRefAttr === attrRef) {
                            selectedGroupCount++;
                        }
                    });

                    if (selectedGroupCount > groupMax) {
                        var diff = selectedGroupCount - groupMax;
                        _.each(jQuery('.addOnSelect'), function (it) {
                            var isCheckedAttr = jQuery(it).is(":checked");
                            var groupNameAttr = jQuery(it).attr('group');
                            var attrRefAttr = jQuery(it).attr('attrref');
                            
                            if (isCheckedAttr && groupNameAttr === groupName && attrRefAttr === attrRef
                                && diff > 0 && jQuery(it).attr('id') != jQuery(checkbox).attr('id')) {
                                diff--;
                                jQuery(it).attr('checked', false);
                }
            });
        }
                }
            }
        }

        /*
         * Save all function
         *
         */
        function bulkSave() {
            addOnIdsToAdd = [];

            _.each(jQuery('.addOnSelect'), function (it) {
                if (jQuery(it).is(":checked")) {
                    addOnIdsToAdd.push(jQuery(it).attr('id'));
                }
            });

            if(addOnIdsToAdd.length > 0)
                addSelectedRelatedProducts(addOnIdsToAdd);
        }

        /*
         * Delete all function
         *
         */
        function bulkDelete() {
            addOnIdsToDelete = [];

            _.each(jQuery('.deleteAddOnSelect'), function (it) {
                if (jQuery(it).is(":checked")) {
                    addOnIdsToDelete.push(jQuery(it).attr('id'));
                }
            });

            if(addOnIdsToDelete.length > 0)
                deleteSelectedRelatedProducts(addOnIdsToDelete);
        }

        function deleteSelectedRelatedProducts(assocsToDelete) {
            jQuery(domRootElement).html(preparingWidgetMessage);
            
            var listPromises = [];
            
            for(var i=0; i<assocsToDelete.length; i++) {
                var assocId = assocsToDelete[i];

                if(customPluginJsonObject.singleAddOnAssociationList) {
                    _.each(allAddOnAssociationsForPriceItem, function(assoc) {
                        if(assoc.id === assocId) {
                            refreshAllSelectedAddOnPriceItemList(assoc.cspmb__add_on_price_item__c);
                            refreshDefaultAddOnPriceItemList(assoc.cspmb__add_on_price_item__c);

                            var relatedProductDefinitionName = '';

                            // this should be developed more generic - but for now for VMB leave it this way
                            if(customPluginJsonObject.vmbImplementation) {
                                relatedProductDefinitionName = assoc.cspmb__add_on_price_item__r.related_product_name__c;
                            } else {
                                relatedProductDefinitionName = assoc.cspmb__add_on_price_item__r.cspmb__product_definition_name__c;
                            }

                            var attrWrapper = getRelatedProductAttribute(relatedProductDefinitionName, '');

                            listPromises.push(CS.Service.removeRelatedProduct(attrWrapper[0].reference, true));
                        }
                    });
                } else {
                    _.each(addOnsForPriceItemCache, function(value, key) {
                        _.each(value, function(assoc) {
                            if(assoc.id === assocId) {
                                refreshAllSelectedAddOnPriceItemList(assoc.cspmb__add_on_price_item__c);
                                refreshDefaultAddOnPriceItemList(assoc.cspmb__add_on_price_item__c);
    
                                listPromises.push(CS.Service.removeRelatedProduct(key, true));
                            }
                        });
                    });
                }
            }

            Promise.all(listPromises)
                .then(function(values) {
                    displayPricingWidget(domRootElement)
                            .then(function() {
                        CS.Rules.enableRules();
                        CS.Rules.evaluateAllRules()
                        .then(function afterRules() { });
                    });
                });
        }

        function addSelectedRelatedProducts(assocsToAdd) {
            jQuery(domRootElement).html(preparingWidgetMessage);
            
            var listPromises = [];
            
            for(var i=0; i<assocsToAdd.length; i++) {
                var assocId = assocsToAdd[i];
                var addOnAssocs = [];

                if(customPluginJsonObject.singleAddOnAssociationList) {
                    _.each(allAddOnAssociationsForPriceItem, function(assoc) {
                        if(assoc.id === assocId) {
                            addToAllSelectedAddOnPriceItemList(assoc.cspmb__add_on_price_item__c);

                            addOnAssocs.push(assoc);

                            var relatedProductDefinitionName = '';

                            // this should be developed more generic - but for now for VMB leave it this way
                            if(customPluginJsonObject.vmbImplementation) {
                                relatedProductDefinitionName = assoc.cspmb__add_on_price_item__r.related_product_name__c;
                            } else {
                                relatedProductDefinitionName = assoc.cspmb__add_on_price_item__r.cspmb__product_definition_name__c;
                            }

                            var attrWrapper = getRelatedProductAttribute(relatedProductDefinitionName, '');

                            listPromises.push(createRelatedProducts({ reference: attrWrapper[0].reference }, addOnAssocs, 1, true));
                            }
                    });
                            } else {
                    _.each(addOnsForPriceItemCache, function(value, key) {
                        _.each(value, function(assoc) {
                            if(assoc.id === assocId) {
                                addToAllSelectedAddOnPriceItemList(assoc.cspmb__add_on_price_item__c);
    
                                addOnAssocs.push(assoc);
                                listPromises.push(createRelatedProducts({ reference: key }, addOnAssocs, 1, true));
                            }
                            });
                    });
                }
                        }

            Promise.all(listPromises)
                .then(function(values) {
                    displayPricingWidget(domRootElement)
                    .then(function() {
                        CS.Rules.enableRules();
                        CS.Rules.evaluateAllRules()
                        .then(function afterRules() { });
                    });
                });
        }

        function refreshAllSelectedAddOnPriceItemList(addOnPriceItemId) {
            var tmpAddOns = _.without(allSelectedAddOnPriceItems, addOnPriceItemId);
            allSelectedAddOnPriceItems = tmpAddOns;
        }

        function refreshDefaultAddOnPriceItemList(addOnPriceItemId) {
            var tmpAddOns = _.without(onlyDefaultAddOnPriceItems, addOnPriceItemId);
            onlyDefaultAddOnPriceItems = tmpAddOns;
        }

        function addToAllSelectedAddOnPriceItemList(addOnPriceItemId) {
            if(!_.includes(allSelectedAddOnPriceItems, addOnPriceItemId))
                allSelectedAddOnPriceItems.push(addOnPriceItemId);
        }

        function addToADefaultAddOnPriceItemList(addOnPriceItemId) {
            if(!_.includes(onlyDefaultAddOnPriceItems, addOnPriceItemId))
                onlyDefaultAddOnPriceItems.push(addOnPriceItemId);
        }

        /*
         * changeAddOn
         *
         * Changes add on assoc Id for selected add on group if max = 1
         * Related Product attribute identified by attrRef.
         */
        function changeAddOn(attrRef, addOnId, addOnPriceItemId) {
            jQuery(domRootElement).html(preparingWidgetMessage);
            var context = {};
            context.ref = attrRef;

            refreshAllSelectedAddOnPriceItemList(addOnPriceItemId);
            refreshDefaultAddOnPriceItemList(addOnPriceItemId);

            var csAddOnAttributeReference = CS.Util.generateReference(customPluginJsonObject.addOnPriceItemAssociationLookupAttributeName, context);
            if(attributeIsPresent(csAddOnAttributeReference)) {
                return CS.setAttributeValue(csAddOnAttributeReference, addOnId, false)
                    .then(function() {
                        displayPricingWidget(domRootElement)
                        .then(function() {
                            //hideControlButtons(allRelatedProductAttributes);
                        });
            });
            } else {
                console.error('CS AddOn attribute is missing!');
                return Promise.reject('CS AddOn attribute is missing!');
            }
        }

        /*
         * deleteAddOn
         *
         * Deletes add on if max = 1
         * Related Product attribute identified by attrRef.
         */
        function deleteAddOn(attrRef, addOnPriceItemId, group) {
            jQuery(domRootElement).html(preparingWidgetMessage);
            setRefreshInvokedFlag(true);

            // this should be developed more generic - but for now for VMB leave it this way
            if(customPluginJsonObject.vmbImplementation) {
                //Changes to update the number range attribute
                if (attrRef == 'Number_Ranges_0') {
                    var ref = CS.UI.getCurrentInstance().getCurrentConfigReference();
                    if (ref !== "") {
                        ref = ref + '\\:';
                    }
                    if (CS.getAttributeValue('Total_number_of_number_ranges_0') != null) {
                        CS.setAttributeValue(ref + 'Total_number_of_number_ranges_0', 2, true);
                    }
                }

                //Changes to update the call package attribute          
                if (attrRef == 'Call_Packages_0') {
                    var ref = CS.UI.getCurrentInstance().getCurrentConfigReference();
                    if (ref !== "") {
                        ref = ref + '\\:';
                    }
                    if (CS.getAttributeValue('Total_number_of_number_ranges_0') != null) {
                        CS.setAttributeValue(ref + 'Total_number_of_number_ranges_0', 3, true);
                    } else if (CS.getAttributeValue('On_net_Off_net_0') != null) {
                        CS.setAttributeValue(ref + 'On_net_Off_net_0', 'On-Net', true);
                    }
                }
                    }

            refreshAllSelectedAddOnPriceItemList(addOnPriceItemId);
            refreshDefaultAddOnPriceItemList(addOnPriceItemId);

            return CS.Service.removeRelatedProduct(attrRef, false)
                .then(function() {
                    displayPricingWidget(domRootElement)
                    .then(function() {
                        CS.Rules.enableRules();
                        CS.Rules.evaluateAllRules()
                        .then(function afterRules() { });
                });
                });
        }

        /*
         * insertAddOn
         *
         * Inserts an instance of the Add On identified by addOnId into the
         * Related Product attribute identified by attrRef.
         */
        function insertAddOn(attrRef, addOnId, quantity, isNew, productReference, addOnPriceItemId, suppressRules) {
            jQuery(domRootElement).html(preparingWidgetMessage);
            setRefreshInvokedFlag(true);

            return emptyPromise
                .then(function() {
                    //addToAllSelectedAddOnPriceItemList(addOnPriceItemId);

                    if (isNew) {
                        return createRelatedProducts({ reference: attrRef }, [{ id: addOnId, cspmb__add_on_price_item__c: addOnPriceItemId }], quantity, suppressRules)
                            .then(function() {
                                displayPricingWidget(domRootElement)
                                .then(function() {
                                    CS.Rules.enableRules();
                                    CS.Rules.evaluateAllRules()
                                    .then(function afterRules() { });
                                });
                            });
                    } else {
                        return CS.setAttributeValue(productReference + ':Quantity_0', quantity, true)
                            .then(function() {
                                displayPricingWidget(domRootElement)
                                .then(function() {
                                    CS.Rules.enableRules();
                                    CS.Rules.evaluateAllRules()
                                    .then(function afterRules() { });
                                });
                            });
                    }
                });
        }

        function setRefreshInvokedFlag(state) {
            refreshInvoked = state;
        }

        function removeAndCheckDefaultAddOns(attr, addOns) {
            var indexesToRemove = [];
            var listPromises = [];
            _.each(attr.relatedProducts, function (rp) {
                var addOnId = CS.getAttributeValue(rp.reference + '\:' + customPluginJsonObject.addOnPriceItemAttributeName + '_0');
                var remove = true;

                for(var i = 0; i < addOns.length; i++) {
                    var tmpAddOn = addOns[i];
                    var index = -1;

                    if (tmpAddOn.cspmb__add_on_price_item__c === addOnId) {
                        remove = false;
                        indexesToRemove.push(i);
                        index = i;
                        
                        if(index > -1) {
                            //CS.setAttributeValue(attributeReference, sfdcRecordId, suppressRules) - returns a Promise object
                            listPromises.push(CS.setAttributeValue(rp.reference + '\:' + customPluginJsonObject.addOnPriceItemAssociationLookupAttributeName + '_0', tmpAddOn.id, true));
                            //addOns = []; // this will be needed for VMB
                        }
                    }
                }

                if (remove) {
                    refreshAllSelectedAddOnPriceItemList(addOnId);
                    listPromises.push(CS.Service.removeRelatedProduct(rp.reference, true));
                }
            });

            return Promise.all(listPromises)
                .then(function(values) {
                    _.pullAt(addOns, indexesToRemove);
                    return Promise.resolve(addOns);
                });
        }

        function addDefaultAddOnsForAttribute(attr) {
            if (allSelectedAddOnPriceItems != null && allSelectedAddOnPriceItems.length > 0) {
                return getAddOnAssociationsListForAttrCached(attr)
                    .then(function (addOns) {
                        return removeAndCheckDefaultAddOns(attr, addOns)
                            .then(function(addOnsValues){
                                return filterAddOnAssociationsByDefaultQuantity(addOnsValues);
                            });
                    })
                    .then(function(addOns) {
                        if (addOns != undefined && addOns.length > 0) {
                            //return createRelatedProducts(attr, [{ id: addOns.id, cspmb__add_on_price_item__c: addOns.id }], 1, false);
                            return createRelatedProducts(attr, addOns, 1, false);
                        }
                    });
            } else {
                return getAddOnAssociationsListForAttrCached(attr)
                    .then(filterAddOnAssociationsByDefaultQuantity)
                    .then(function (addOns) {
                        //return createRelatedProducts(attr, [{ id: addOns.id, cspmb__add_on_price_item__c: addOns.id }], 1, false);
                        return createRelatedProducts(attr, addOns, 1, false);
                    });
            }
        }

        function getAddOnAssociationsListForAttrCached(attrWrapper) {
            /*if(customPluginJsonObject.singleAddOnAssociationList) {
                // check if array already populated with AddOns Association records
                if (allAddOnAssociationsForPriceItem.length > 0) {
                    return Promise.resolve(allAddOnAssociationsForPriceItem);
                } else {
                    return getAddOnAssociationsListForAttr(attrWrapper);
                }
            } else {
                if (addOnsForPriceItemCache[attrWrapper.reference] != undefined && addOnsForPriceItemCache[attrWrapper.reference].length > 0) {
                    return Promise.resolve(addOnsForPriceItemCache[attrWrapper.reference]);
                } else {
                    return getAddOnAssociationsListForAttr(attrWrapper);
                }
            }*/

            if (addOnsForPriceItemCache[attrWrapper.reference] != undefined && addOnsForPriceItemCache[attrWrapper.reference].length > 0) {
                return Promise.resolve(addOnsForPriceItemCache[attrWrapper.reference]);
            } else {
                return getAddOnAssociationsListForAttr(attrWrapper);
            }
        }

        function getAddOnAssociationsListForAttr(attWrapper) {
            var configRef = CS.Util.getParentReference(attWrapper.reference);
            var prefix = configRef ? configRef + ':' : '';
            var regExp = new RegExp('^' + prefix + '[^:]+$');
            var index = CS.Service.getProductIndex('');
            var attrDef = index.all[attWrapper.definitionId];
            var productDefinitionId = CS.getConfigurationWrapper(configRef).config.cscfga__Product_Definition__c;
            var filterAttrNames;
            var filterAttrs = {};

            if (!attrDef) {
                console.warn('Could not find attribute definition id for attr ', attWrapper);
                return Promise.reject('Could not find attribute definition id for attr ' + attWrapper.reference);
            }
            var lc = index.all[attrDef.cscfga__Lookup_Config__c];
            if (!lc) {
                return Promise.reject('LookupConfig could not be found for attr ' + attWrapper.reference);
            }

            var lq = index.all[lc.cscfga__Filter__c];
            if (!lq) {
                return Promise.reject('LookupQuery filter could not be found for attr ' + attWrapper.reference);
            }

            if (lq.cscfga__Referenced_Attributes__c) {
                try {
                    filterAttrNames = JSON.parse(lq.cscfga__Referenced_Attributes__c);
                } catch (e) {
                    console.error('Could not parse referenced Attributes: ' + lq.Id + ' / ' + lq.cscfga__Referenced_Attributes__c);
                }
            } else {
                filterAttrNames = [];
            }

            _.each(CS.Service.config, function(node) {
                if (node.attr && regExp.exec(node.reference)) {
                    var name = node.attr.Name;
                    if (_.indexOf(filterAttrNames, name) > -1) {
                        var val = node.attr.cscfga__Value__c;
                        filterAttrs[name] = val;
                    }
                }
            });

            return doLookupQuery(lq.Name, filterAttrs, productDefinitionId)
                .then(function(fetchedRecords) {
                    addOnsForPriceItemCache[attWrapper.reference] = fetchedRecords;
                    return fetchedRecords;
                });
        }

        /**** Remote callouts ****/
        function doLookupQuery(name, dynamicFilterMap, productDefinitionId) {
            return new Promise(function (resolve, reject) {
                Visualforce.remoting.Manager.invokeAction(
                    'cscfga.UISupport.doMultiRowLookupQuery',
                    name,
                    dynamicFilterMap,
                    productDefinitionId,
                    function(result, event) {
                        if (event.status) {
                            resolve(keysToLowerCase(result));
                        } else {
                            console.error(event);
                            reject(event);
                        }
                    },
                    {escape: false}
                );
            });
        }

        /*
         * findAddOnRelatedProductAttributes
         *
         * Returns a Promise providing a list of add-on related product attributes in the specified config.
         * Add-on related product attributes have the following characteristics:
         *
         *  1. A single 'avaialble product definition'
         *  2. That product definition contains a lookup attribute with an attribute field named '__AddOnGenerator__'
         *
         */
        function findAddOnRelatedProductAttributes(configRef) {
            function addOnGeneratorLookupsFilter(index, configAttrs) {
                return function(avail) {
                        return _.filter(configAttrs, function(it) {
                        return it.attr.cscfga__Attribute_Definition__c === avail.cscfga__Attribute_Definition__c && it.reference.includes('Addons_0');
                        });
                    }
            }

            function findAddOnGeneratorLookups(configRef, products, index) {
                var configAttrs = getAttributesForConfigRef(configRef);
                return _.flatten(_.filter(_.map(products, addOnGeneratorLookupsFilter(index, configAttrs)), removeUndefinedFilter));
            }

            function loadProductReducer(promise, avail) {
                var defId = avail.cscfga__Product_Definition__c;
                return (!!defId && !CS.Service.getProductIndex(defId))
                    ? promise.then(function() {
                    return loadProduct(defId);
                })
                    : promise;
            }

            function removeUndefinedFilter(it) {
                return !!it;
            }


            var index = CS.Service.getProductIndex('');
            var currentProdDefId = CS.getConfigurationWrapper(configRef).config.cscfga__Product_Definition__c;

            var singleAvailableProducts = _.flatten(_.filter(
                _.filter(index.availableProductsByAttributeDef, {length: 1}),
                attributesForProductFilter(currentProdDefId, index)
            ));

            var loadProductsPromise = emptyPromise;
            return loadProductsPromise.then(function() {
                return findAddOnGeneratorLookups(configRef, singleAvailableProducts, index);
            });
        }

        /*
         * findPriceItemAttributeForConfigRef
         *
         * Returns the Attribute containing the Price Item for this Config.
         * This is identified by the presence of a __PriceItemReference__ marker AttributeField
         */
        function findPriceItemAttributeForConfigRef(ref) {
            var pi = priceItemAttrCache[ref];
            if (pi) {
                return pi;
            }

            var pis = _.filter(CS.Service.config, function(wrapper, reference) {
                return (wrapper.attributeFields
                && wrapper.attributeFields.__PriceItemReference__
                && CS.Util.getParentReference(reference) === ref);
            });

            if (pis.length > 1) {
                console.warn('Multiple __PriceItemReference__ markers found, model is invalid: ', pis);
            }

            priceItemAttrCache[ref] = pis[0];

            return pis[0];
        }

        function getPriceItemForConfig(ref) {
            var piAttr = findPriceItemAttributeForConfigRef(ref);

            if (!piAttr) {
                console.log('EAPI compatibility: No price item attribute found in config ref \'' + ref + '\'');
            }

            var priceItem = CS.getAttributeValue(piAttr.reference);

            if (!priceItem) {
                console.log('EAPI compatibility: No price item found in attribute ref ' + piAttr.reference);
            }

            return Promise.resolve(priceItem);
        }

        function buildAddOnListWithGroupAndCardinalityConstraints(assocs, selectedAddOnIds, attr) {
            var groups = buildAssociationGroups(assocs);

            groupMinMaxValidation[attr.reference] = groups;

            var groupCounts = getAssociationGroupCounts(assocs, selectedAddOnIds);
            var selectedAssocMap = _.fromPairs(_.map(selectedAddOnIds, function (assoc) {
                return [assoc, true];
            }));

            return _.map(assocs, function(assoc) {
                var grp = groups[assoc.cspmb__group__c];
                var max = grp.max_count;
                var count = groupCounts[assoc.cspmb__group__c];

                groupMinMaxValidation[attr.reference][assoc.cspmb__group__c].current_count = isNaN(Number(count)) ? 0 : Number(count);

                var isAvailable = true;
                var reason;

                if (count + 1 > max) {
                    isAvailable = false;
                    reason = 'There ' + (count === 1 ? 'is' : 'are') + ' already ' + count + ' of this type of add on selected.';
                }

                var selected = false;
                if (selectedAssocMap[assoc.id]) {
                    selected = true;
                }

                return { assoc: assoc, isAvailable: isAvailable, unavailableReason: reason, max: max, selected: selected };
            });
            return addOns;
        }

        /* Build groups and min and max for it */
        function buildAssociationGroups(assocs) {
            var groups = {};
            _.each(assocs, function(assoc) {
                var name = assoc.cspmb__group__c;
                groups[name] = { max_count: isNaN(Number(assoc.cspmb__max__c)) ? 0 : Number(assoc.cspmb__max__c), 
                    min_count: isNaN(Number(assoc.cspmb__min__c)) ? 0 : Number(assoc.cspmb__min__c),
                    current_count: 0 };
            });
            return groups;
        }

        /* Get count of added AddOns for each related product and group */
        function getAssociationGroupCounts(assocs, selectedAddOnIds) {
            var groupCounts = {};
            var assocsById = _.fromPairs(_.map(assocs, function(assoc) {
                return [assoc.id, assoc];
            }));

            _.each(selectedAddOnIds, function(id) {
                var assoc = assocsById[id];
                if (assoc) {
                    var count = groupCounts[assoc.cspmb__group__c] || (groupCounts[assoc.cspmb__group__c] = 0);
                    groupCounts[assoc.cspmb__group__c] = count + 1;
                        }
            });

            return groupCounts;
        }

        function filterAddOnAssociationsByDefaultQuantity(assocs) {
            return _.filter(assocs, function(assoc) {
                return assoc.cspmb__default_quantity__c > 0;
                });
        }

        /**** Utility functions ****/
        function productIsNewlyAddedToBasket(ref) {
            var firstRunFlagRef = CS.Util.generateReference('First_Run', {ref: ref});
            if (attributeIsPresent(firstRunFlagRef)) {
                if (CS.getAttributeValue(firstRunFlagRef) == 'false') {
                    CS.setAttributeValue(firstRunFlagRef, 'true', true);
                    return true;
                }
            } else {
                console.warn('EAPI compatibility: Cannot evaluate default add-ons as no first run attribute present');
            }
            return false;
        }
        
        function getAddOnAttributes() {
            return _.filter(CS.Service.config, function(it) {
                return it.attributeFields && it.attributeFields.__AddOn__
            });
        }
        
        function getFeatureAttribute(configRef) {
            
            return _.filter(CS.Service.config, function(it) {
                var ref = '';
                var n = it.reference.lastIndexOf(":");
                if (n != -1) {
                    ref =it.reference.substr(0, n);
                }
                return it.attributeFields && it.attributeFields.__Feature__ && ref === configRef
            });
        }
        
        function getRateCardAttribute(configRef) {
            return _.filter(CS.Service.config, function(it) {
                var ref = '';
                var n = it.reference.lastIndexOf(":");
                if (n != -1) {
                    ref =it.reference.substr(0, n);
                }
                return it.attributeFields && it.attributeFields.__RateCard__ && ref === configRef
            });
        }

        function getRateCardLineAttribute(configRef) {
            return _.filter(CS.Service.config, function(it) {
                var ref = '';
                var n = it.reference.lastIndexOf(":");
                if (n != -1) {
                    ref =it.reference.substr(0, n);
                }
                return it.attributeFields && it.attributeFields.__RateCardLine__ && ref === configRef
            });
        }
        
        function attributesForProductFilter(productId, index) {
            return function(it) {
                var attrDef = index.all[it[0].cscfga__Attribute_Definition__c];
                return attrDef
                    && attrDef.cscfga__Product_Definition__c == productId
                    && !!it[0].cscfga__Product_Definition__c;
            };
        }

        function getAttributesForConfigRef(ref) {
            var prefix = ref ? ref + ':' : '';
            var regExp = new RegExp('^' + prefix + '[^:]+$');
            var cached = configAttrCache[ref];

            if (cached) {
                return cached;
            }

            return configAttrCache[ref] = _.filter(CS.Service.config, function(node) {
                return node.attr && regExp.exec(node.reference);
            });
        }

        function getSelectedAddOnIds(addOnAttr) {
            return _.filter(_.map(
                addOnAttr.relatedProducts,
                function(it) {
                    return CS.getAttributeValue(makeRef(it.reference, customPluginJsonObject.addOnPriceItemAttributeName + '_0')); // This doesn't need to be hardcoded
                }),
                function(it) {
                    return it != null;
                }
            );
        }

        function attributeIsPresent(ref) {
            return (CS.Service.config[ref] && CS.Service.config[ref].attr);
        }

        function getAttributeDefinition(node) {
            var index = CS.Service.getProductIndex();
            return index.all[node.definitionId];
        }

        function asDecimal(val, scale) {
            var f = parseFloat(val);
            if (isNaN(f)) {
                return '';
            } else {
                return f.toFixed(scale);
            }
        }

        function error(err) {
            console.error(err);
            return Promise.reject(err);
        }

        function keysToLowerCase(obj) {
            var data = _.isArray(obj) ? [] : {};
            _.each(obj, function(val, key) {
                if (key.toLowerCase) {
                    key = key.toLowerCase();
                }
                if (val && typeof val === 'object') {
                    data[key] = keysToLowerCase(val);
                } else {
                    data[key] = val;
                }
            });
            return data;
        }

        function makeRef(ref, tail) {
            return (ref ? ref + ':' : '') + tail;
        }

        function createRelatedProducts(attr, addOns, quantity, suppressRules) {
            var availableProducts = CS.Service.getAvailableProducts(attr.reference);

            if (availableProducts == undefined || availableProducts.length < 1) {
                console.error('EAPI Compatibility: no related product definitions avaialable; there should be exactly one for attribute ' + attr.reference);
                return Promise.reject('No related product available.');
            }

            var defId = availableProducts[0].cscfga__Product_Definition__c;
            var promise = _.reduce(addOns, function(promise, addOn) {
            return promise.then(function() {
                    return createAddOnRelatedProductInline(attr, defId, addOn, quantity, suppressRules);
                    });
            }, emptyPromise);

            //return promise;
            //VF change
            return promise.then(function() {
                console.info('EAPI compatibility: evaluating rules after adding related product');
                CS.Rules.evaluateAllRules();
            });
        }
        

        function createAddOnRelatedProductInline(attr, defId, addOn, quantity, suppressRules) {
            var parent = CS.getConfigurationWrapper(CS.Util.getParentReference(attr.reference));
            return loadProduct(defId)
                .then(function() {
                    //CS.Service.addRelatedProduct(anchorAttributeReference, productDefinitionId, suppressRules) - returns a Promise object
                    
                    return CS.Service.addRelatedProduct(attr.reference, defId, suppressRules);
                })
                .then(function (config) {
                    console.info('Created Configuration: ', config);
                        var context = {ref: config.reference};

                    var quantityAttributeReference = CS.Util.generateReference('Quantity', context);
                    if(attributeIsPresent(quantityAttributeReference)) {
                        CS.setAttributeValue(quantityAttributeReference, quantity, true);    
                    } else {
                        console.error('Quantity attribute is missing!');
                        return Promise.reject('Quantity attribute is missing!');
                                }

                    //var csAddOnPriceItemAttributeReference = CS.Util.generateReference(customPluginJsonObject.addOnPriceItemAttributeName, context);
                    var csAddOnPriceItemAttributeReference = CS.Util.generateReference('CS_AddOn', context);
                    if(attributeIsPresent(csAddOnPriceItemAttributeReference)) {
                        if(addOn.cspmb__default_quantity__c !== undefined && addOn.cspmb__default_quantity__c > 0) {
                            return CS.SVC.loadLookupRecords([
                            { lookupRecordId: addOn.id, attribute: CS.Service.config[csAddOnPriceItemAttributeReference].attr }
                            ]).then(function(){
                                CS.setAttributeValue(csAddOnPriceItemAttributeReference, addOn.id, true);
                            });
                            
                        } else {
                            return CS.SVC.loadLookupRecords([
                            { lookupRecordId: addOn.cspmb__add_on_price_item__c, attribute: CS.Service.config[csAddOnPriceItemAttributeReference].attr }
                            ]).then(function(){
                                CS.setAttributeValue(csAddOnPriceItemAttributeReference, addOn.cspmb__add_on_price_item__c, true);
                            });
                        }
                    } else {
                        console.error(customPluginJsonObject.addOnPriceItemAttributeName + ' attribute is missing!');
                        return Promise.reject(customPluginJsonObject.addOnPriceItemAttributeName + ' attribute is missing!');
                    }
                    
                    //NC - this is commented out because the CS_PricePlan is taken from the Paren so doesn't need to be set here
                    /*var csAddOnAttributeReference = CS.Util.generateReference(customPluginJsonObject.addOnPriceItemAssociationLookupAttributeName, context);
                    if(attributeIsPresent(csAddOnAttributeReference)) {
                        CS.setAttributeValue(csAddOnPriceItemAttributeReference, addOn.id, true);
                    } else {
                        console.error(customPluginJsonObject.addOnPriceItemAssociationLookupAttributeName + ' attribute is missing!');
                        return Promise.reject(customPluginJsonObject.addOnPriceItemAssociationLookupAttributeName + ' attribute is missing!');
                    }*/

                });
        }

        function loadProduct(id) {
            var index = CS.Service.getProductIndex(id);
            if (!index) {
                return CS.Service.loadProduct(id, function(index) {
                    jQuery.extend(CS.screens, CS.DataBinder.prepareScreenTemplates(index));
                    return index;
                });
            } else {
                return emptyPromise;
            }
        }

        /*** VMB project specific methods ****/

        /*
         * setConfigurationNames
         *
         * Sets names of configurations in the model where an Attribute Field named '__ConfigName__' on the
         * Lookup Attribute linked to the Pricing Model provides an expression indicating the lookup field value to use.
         */
        function setConfigurationNames() {
            var pricingModelLookups = getPricingModelLookupAttributes();
            var lookups = _.filter(pricingModelLookups, function (it) { return !!it.attributeFields.__ConfigName__; });
            _.each(lookups, function (it) {
                var ref = CS.Util.getAnchorReference(CS.Util.getParentReference(it.reference));
                var wrapper = CS.getConfigurationWrapper(ref);
                _.each(wrapper.relatedProducts, function (rp) {
                    var reference = it.reference.replace(ref, rp.reference);
                    var name = resolveLookupValue(it.attributeFields.__ConfigName__.cscfga__Value__c, reference);
                    var parent = CS.Util.getParentReference(reference);
                    CS.setConfigurationProperty(parent, 'name', name);
            });
                /*_.each(CS.binding.getBindings(ref), function (binding) {
                    binding.handler.updateUI(binding);
                });*/
                    });
            }

        /*
         * setPrices
         *
         * Sets attribute prices. For any Attribute with an Attribute Field named '__Price__', uses the expression value
         * to find the Pricing Model lookup field used to set price on that Attribute.
         */
        function setPrices() {
            var pricingAttrs = _.filter(CS.Service.config, function (it) { return it.attr && it.attributeFields.__Price__; });
            _.each(pricingAttrs, function (it) {
                var af = it.attributeFields.__Price__.cscfga__Value__c;
                var lkVal = resolveLookupValue(af, it.reference);
                CS.setAttributeField(it.reference, 'price', lkVal);
                console.log('EAPI compatibility: Set price of ' + it.reference + ' to: ' + lkVal);
                if (lkVal == null) {
                    CS.setAttributeField(it.reference, 'islineitem', false);
                }
            });
        }
        
        function getPricingModelLookupAttributes() {
            var lookupConfigIds = getPricingModelLookupConfigIds();
            var pricingModelLookupAttrs = _.filter(CS.Service.config, function (it) {
                var def = getAttributeDefinition(it);
                return it.attr && _.includes(lookupConfigIds, def.cscfga__Lookup_Config__c);
                });
            return pricingModelLookupAttrs;
            }

        function getPricingModelLookupConfigIds() {
            return _.map(_.filter(CS.Service.getProductIndex().all, function (it) {
                return it.attributes &&
                    it.attributes.type === 'cscfga__Lookup_Config__c'
                    && (it.cscfga__Object__c === 'cspmb__price_item__c'
                        || it.cscfga__Object__c === 'cspmb__add_on_price_item__c'
                        || it.cscfga__Object__c === 'cspmb__price_item_add_on_price_item_association__c');
            }), function (it) {
                return it.Id;
            }
            );
                }

            /*
         * Resolve an expression of the form {lookupAttribute.field} to the lookup field value
            */
        function resolveLookupValue(exp, ctx) {
            var lk = exp ? exp.match(/\{([^\.]+)\.([^\}]+)\}/) : [];
            if (lk != null && lk.length == 3) {
                var lkName = lk[1];
                var lkField = lk[2];
                var ref = attNameToRef(lkName, CS.Util.getParentReference(ctx));
                if (!ref) {
                    console.error('Pricing lookup reference ' + exp + ' could not be resolved.');
                    return;
                                    }
                return CS.getLookupValue(ref, lkField);
        }
        }
        
        /*
         * setLineItemDescriptions
         *
         * For all line item attributes, where the line item description field contains a Pricing Model lookup
         * expression, set the line item description to the resolved field value.
         */
        function setLineItemDescriptions() {
            var pricingModelLookups = getPricingModelLookupAttributes();
            var lineItems = _.filter(CS.Service.config, function (it) { return it.attr && it.attr.cscfga__Is_Line_Item__c; });
            _.each(lineItems, function (it) {
                var lkVal = resolveLookupValue(it.definition.cscfga__Line_Item_Description__c, it.reference);
                if (lkVal) {
                    CS.getAttributeFieldValue(it.reference, 'lineitemdescription', lkVal);
                    }
            });
                }
                
        /*
         * Resolve an attribute name to its reference. Scoped to the context
         * of the current product (it may be anywhere in the hierarchy, must not
         * select an Attribute with the same name under a different product position)
         */
        function attNameToRef(name, context) {
            var atts = _.filter(CS.Service.config, function (it) { return it.attr && it.attr.Name == name && CS.Util.getParentReference(it.reference) == context; });
            if (atts.length == 1) return atts[0].reference;
            }
            
            
/*********** VF change
 * */
            //EVC test
        function createAddOnRelatedProductInlineTest(attr) {
            /*
            var defId = 'a4F9E0000009FOQ';
            var attrReference = 'EVC_PVC_dep_0';
            var parent = CS.getConfigurationWrapper(CS.Util.getParentReference(attrReference));
            */
            
            var availableProducts = CS.Service.getAvailableProducts(attr.reference);
            var defId = availableProducts[0].cscfga__Product_Definition__c;
            var currentRef = CS.Service.getCurrentConfigRef();
            var bindingCache = CS.binding;
            
            var parent = CS.getConfigurationWrapper(CS.Util.getParentReference(attr.reference));
            
            return loadProduct(defId)
                .then(function() {
                    return createConfiguration(attr.reference, defId, parent);
                })
                .then(function (config) {
                    console.info('Created Configuration: ', config);
                    var anchorRef = CS.Util.getAnchorReference(attr.reference);
                    var wrapper = CS.getConfigurationWrapper(anchorRef);
                    CS.binding.update(anchorRef, { 'relatedProducts': wrapper.relatedProducts });
                    var bindingCache = CS.binding;
                    var currentRef = CS.Service.getCurrentConfigRef();

                    return new Promise(function(resolve, reject) {
                        var offScreen = getOffScreenElement();
                        var ref = CS.Service.getCurrentScreenRef();

                        var html = CS.DataBinder.buildScreen(CS.Service.getCurrentScreenRef(), CS.Service.config, CS.screens);
                        var screen = jQuery(html);
                        CS.screens[CS.Service.getCurrentScreenIndex()] = screen;

                        
                        
                        //jQuery(':root').append('<div id="configurationContainerTmp"></div>');
                        CS.DataBinder.bind(CS.Service.config, CS.Service.getProductIndex(), jQuery('#configurationContainerTmp').add(CS.screens[CS.Service.getCurrentScreenIndex()]), config.reference);
                        CS.Service.displayScreen(CS.Service.getCurrentScreenIndex(), '#configurationContainerTmp', function() {
                            var newScreen = jQuery('#configurationContainerTmp').contents();
                            setTimeout(function() {
                                jQuery('#configurationContainer').empty().append(newScreen);
                                jQuery('#configurationContainerTmp').remove();
                                var html = '<div id="pmwgt" class="pmWidget dropdown pbButton CS_configButtons"><button class="dropbtn ppbButton CS_configButtons">Building Widget</button></div>';
                                if (jQuery('#pmwgt')[0] == undefined) {
                                    ref = CS.Service.getCurrentConfigRef(); 
                                    if (ref !== "") {
                                        ref = ref + '\\:';
                                    }
                                    //jQuery('[data-cs-binding=' + ref + 'PMWidget_0]').parent().parent().prepend(html); 
                                }
                                
                                CS.Rules.evaluateAllRules();

                                //displayButtons();
                                
                                
                            }, 300);

                        });
                        

                    })
                        .catch(function(error) {
                            console.error(error);
                        });
                });
        }
        
        function doLookupQueryTestNC(name, dynamicFilterMap, productDefinitionId) {
            return new Promise(function (resolve, reject) {
                Visualforce.remoting.Manager.invokeAction(
                    'cscfga.UISupport.doMultiRowLookupQuery',
                    name,
                    dynamicFilterMap,
                    productDefinitionId,
                    function(result, event) {
                        if (event.status) {
                            debugger;
                            resolve(keysToLowerCase(result));
                        } else {
                            console.error(event);
                            debugger;
                            reject(event);
                        }
                    },
                    {escape: false}
                );
            });
        }
        
        /*
         * getAddOnAssociationsListForAttr
         *
         * Finds the default add ons, if any, (i.e. the add ons which have a default quantity specified)
         * for the given related product attribute.
         */
        function getAddOnAssociationsListForAttrMoreThanOne(attWrapper) {
            var configRef = CS.Util.getParentReference(attWrapper.reference);
            var prefix = configRef ? configRef + ':' : '';
            var regExp = new RegExp('^' + prefix + '[^:]+$');
            var index = CS.Service.getProductIndex('');
            var attrDef = index.all[attWrapper.definitionId];
            var productDefinitionId = CS.getConfigurationWrapper(configRef).config.cscfga__Product_Definition__c;
            var filterAttrNames;
            var filterAttrs = {};

            if (!attrDef) {
                console.warn('Could not find attribute definition id for attr ', attWrapper);
                return Promise.reject('Could not find attribute definition id for attr ' + attWrapper.reference);
            }
            var lc = index.all[attrDef.cscfga__Lookup_Config__c];
            if (!lc) {
                return Promise.reject('LookupConfig could not be found for attr ' + attWrapper.reference);
            }

            //var lq = index.all[lc.cscfga__Filter__c];
            var lq = index.all[attrDef.cscfga__Lookup_Query__c];
            if (!lq) {
                return Promise.reject('LookupQuery filter could not be found for attr ' + attWrapper.reference);
            }

            if (lq.cscfga__Referenced_Attributes__c) {
                try {
                    filterAttrNames = JSON.parse(lq.cscfga__Referenced_Attributes__c);
                    
                } catch (e) {
                    console.error('Could not parse referenced Attributes: ' + lq.Id + ' / ' + lq.cscfga__Referenced_Attributes__c);
                }
            } else {
                filterAttrNames = [];
            }
            
            

            _.each(CS.Service.config, function(node) {
                if (node.attr && regExp.exec(node.reference)) {
                    var name = node.attr.Name;
                    if (_.indexOf(filterAttrNames, name) > -1) {
                        var val = node.attr.cscfga__Value__c;
                        filterAttrs[name] = val;
                    }
                }
            });
            if((attWrapper.reference == priceItemAttributes.access.lookupAttributeParent) || (attWrapper.reference == priceItemAttributes.evc1.lookupAttributeParent)|| (attWrapper.reference == priceItemAttributes.evc2.lookupAttributeParent)|| (attWrapper.reference == priceItemAttributes.cpe.lookupAttributeParent)){
                
                if(!filterAttrs["Access Type Calc"]){
                    filterAttrs["Access Type Calc"] = CS.Service.config["Access_Type_Calc_0"].attr.cscfga__Value__c;
                    
                        if(filterAttrs["Access Type"]){
                            delete filterAttrs["Access Type"];
                        }
                }
                
            }
            
            //return createAddOnRelatedProductInlineTest(lq.Name, filterAttrs, productDefinitionId);
            
            filterAttrs = modifyLookupFilter(filterAttrs, attWrapper);
            lq.Name = modifyLookuoQueryName(filterAttrs, lq.Name, attWrapper);
            return doLookupQueryAndInvalidate(lq.Name, filterAttrs, productDefinitionId,attWrapper);
        }
        
        //Nikola Culej
        //function to determine the filter and correct lookupQuery when automatically adding Access price item
        function modifyLookupFilter(filterAttr, attWrapper){
            //OR Added for CPE split
            if((attWrapper.reference == 'CPE_lookup_0')||(attWrapper.reference == 'CPE_Lookup_Split_0')){
                //var qualityOfService = CS.getAttributeValue("Quality_of_Service_0");
                
                if(filterAttr['Wireless backup'] == 'No'){
                    filterAttr['Wireless backup'] = false;
                }
                 if(filterAttr['Wireless backup'] == 'Yes'){
                    filterAttr['Wireless backup'] = true;
                }

                 if(filterAttr['Available bandwidth up']){
                    filterAttr['Total Bandwidth Up'] = CS.getAttributeValue('Total_Bandwidth_Up_0');
                    delete filterAttr['Available bandwidth up'];
                }
                
                if(filterAttr['Available bandwidth down']){
                    filterAttr['Total Bandwidth Down'] = CS.getAttributeValue('Total_Bandwidth_Down_0');
                     delete filterAttr['Available bandwidth down'];
                }
                 //delete filterAttr['Quality of Service'];
                 //delete filterAttr['Wireless backup'];
                 return filterAttr;
            }
            else if(attWrapper.reference == 'AccessDealTypeInstallationFee_0'){
                var accessType = filterAttr['Access Type Calc'];
                var vendor = filterAttr['Vendor'];
                
                if(((accessType.toLowerCase() == ',fiber,' && vendor.toLowerCase().indexOf('kpn') !== -1) || accessType.toLowerCase() == ',coax,')){
                    console.log('***Region is used for lookup criteria');
                } else {
                    delete filterAttr['Region'];
                }
                
                return filterAttr;
            }
            else {
                return filterAttr;
            }
            
        }
        
        function modifyLookuoQueryName(filterAttrs, lookupQueryName, attWrapper){
            var bandwidth = true;
            var region = true;
            var lineType = true;
            
            //CPE SPLIT --- ad QoS condition
            if((attWrapper.reference == 'CPE_lookup_0')||(attWrapper.reference == 'CPE_Lookup_Split_0')){
                //var qualityOfService = CS.getAttributeValue("Quality_of_Service_0");
                //QoS per EVC 
                var qualityOfService = getCPEQualityOfService();
                
                
                if(qualityOfService){
                    //return 'PriceItemCPEwithQoS';
                    return 'PriceItemCPEwithQoSSplit';
                }
                else{
                    //return 'PriceItemCPEwithoutQoS';
                    return 'PriceItemCPEwithoutQoSSplit';
                }
            }
            else if(attWrapper.reference == 'AccessDealTypeInstallationFee_0'){
                if(filterAttrs["Region"] === undefined){
                    region = false;
                }
                
                if(!region){
                    return 'DealTypeInstallationFeeNoRegion';
                } else {
                    return lookupQueryName;
                }
            }
            else {
                return lookupQueryName;
            }

            
        }
        
        //end
        /*
        function setRuleAttributes(attWrapper){
            if(attWrapper.reference == priceItemAttributes.redundancy.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.redundancy.errorFlag, priceItemAttributes.transportEVC.errorMessageMoreThanOne);
                var index = CS.Service.config[priceItemAttributes.redundancy.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.redundancy.relProductAttribute) + index + ':'+priceItemAttributes.redundancy.overbookingType, 'Redundancy');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.redundancy.relProductAttribute) + index + ':'+priceItemAttributes.redundancy.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.redundancy.relProductAttribute)+index+':'+priceItemAttributes.redundancy.hirarchyLevel, 'Parent');
            }
            else if(attWrapper.reference == priceItemAttributes.transportEVC.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.transportEVC.errorFlag, priceItemAttributes.transportEVC.errorMessageMoreThanOne);
                var index = CS.Service.config[priceItemAttributes.transportEVC.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.transportEVC.relProductAttribute) + index + ':'+priceItemAttributes.transportEVC.overbookingType, 'Transport EVC');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.transportEVC.relProductAttribute) + index + ':'+priceItemAttributes.transportEVC.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.transportEVC.relProductAttribute)+index+':'+priceItemAttributes.transportEVC.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.transportEVC.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.sipTrunk.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.sipTrunk.errorFlag, priceItemAttributes.sipTrunk.errorMessageMoreThanOne);
                var index = CS.Service.config[priceItemAttributes.sipTrunk.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunk.relProductAttribute) + index + ':'+priceItemAttributes.sipTrunk.overbookingType, 'SIP Trunk');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunk.relProductAttribute) + index + ':'+priceItemAttributes.sipTrunk.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.sipTrunk.relProductAttribute)+index+':'+priceItemAttributes.sipTrunk.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.sipTrunk.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.evcOneFixed.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evcOneFixed.errorFlag, priceItemAttributes.evcOneFixed.errorMessageMoreThanOne);
                var index = CS.Service.config[priceItemAttributes.evcOneFixed.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneFixed.relProductAttribute) + index + ':'+priceItemAttributes.evcOneFixed.overbookingType, 'OneFixed');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneFixed.relProductAttribute) + index + ':'+priceItemAttributes.evcOneFixed.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneFixed.relProductAttribute)+index+':'+priceItemAttributes.evcOneFixed.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.evcOneFixed.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.evcOneNet.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evcOneNet.errorFlag, priceItemAttributes.evcOneNet.errorMessageMoreThanOne);
                var index = CS.Service.config[priceItemAttributes.evcOneNet.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneNet.relProductAttribute) + index + ':'+priceItemAttributes.evcOneFixed.overbookingType, 'OneNet');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneNet.relProductAttribute) + index + ':'+priceItemAttributes.evcOneFixed.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evcOneNet.relProductAttribute)+index+':'+priceItemAttributes.evcOneFixed.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.evcOneNet.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.evc1.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evc1.errorFlag, priceItemAttributes.evc1.errorMessageMoreThanOne);
                var index = CS.Service.config[priceItemAttributes.evc1.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1.relProductAttribute) + index + ':'+priceItemAttributes.evc1.overbookingType, 'Data');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1.relProductAttribute) + index + ':'+priceItemAttributes.evc1.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1.relProductAttribute)+index+':'+priceItemAttributes.evc1.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.evc1.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.evc1Vlan.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evc1Vlan.errorFlag, priceItemAttributes.evc1Vlan.errorMessageMoreThanOne);
                var index = CS.Service.config[priceItemAttributes.evc1Vlan.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1Vlan.relProductAttribute) + index + ':'+priceItemAttributes.evc1Vlan.overbookingType, 'Transport Data');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1Vlan.relProductAttribute) + index + ':'+priceItemAttributes.evc1Vlan.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc1Vlan.relProductAttribute)+index+':'+priceItemAttributes.evc1Vlan.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.evc1Vlan.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.evc2.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.evc2.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.evc2.errorFlag, priceItemAttributes.evc2.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2.relProductAttribute) + index + ':'+priceItemAttributes.evc2.overbookingType, 'Voip');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2.relProductAttribute) + index + ':'+priceItemAttributes.evc2.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2.relProductAttribute)+index+':'+priceItemAttributes.evc2.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.evc2.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.evc2Vlan.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.evc2Vlan.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.evc2Vlan.errorFlag, priceItemAttributes.evc2Vlan.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2Vlan.relProductAttribute) + index + ':'+priceItemAttributes.evc2Vlan.overbookingType, 'Transport Voip');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2Vlan.relProductAttribute) + index + ':'+priceItemAttributes.evc2Vlan.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.evc2Vlan.relProductAttribute)+index+':'+priceItemAttributes.evc2Vlan.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.evc2Vlan.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.managedInternetEVC.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.managedInternetEVC.errorFlag, priceItemAttributes.managedInternetEVC.errorMessageMoreThanOne);
                var index = CS.Service.config[priceItemAttributes.managedInternetEVC.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVC.relProductAttribute) + index + ':'+priceItemAttributes.managedInternetEVC.overbookingType, 'Internet');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVC.relProductAttribute) + index + ':'+priceItemAttributes.managedInternetEVC.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVC.relProductAttribute)+index+':'+priceItemAttributes.managedInternetEVC.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.managedInternetEVC.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.managedInternetEVCVLAN.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.managedInternetEVCVLAN.errorFlag, priceItemAttributes.managedInternetEVCVLAN.errorMessageMoreThanOne);
                var index = CS.Service.config[priceItemAttributes.managedInternetEVCVLAN.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVCVLAN.relProductAttribute) + index + ':'+priceItemAttributes.managedInternetEVCVLAN.overbookingType, 'Transport Internet');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVCVLAN.relProductAttribute) + index + ':'+priceItemAttributes.managedInternetEVCVLAN.additionType, 'Automatic');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.managedInternetEVCVLAN.relProductAttribute)+index+':'+priceItemAttributes.managedInternetEVCVLAN.hirarchyLevel, 'Parent');
                //CS.setAttributeValue(priceItemAttributes.managedInternetEVCVLAN.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.access.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.access.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.access.errorFlag, priceItemAttributes.access.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.access.relProductAttribute)+index+':'+priceItemAttributes.access.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.access.relProductAttribute) + index + ':'+priceItemAttributes.access.overbookingType, 'Access');
                //CS.setAttributeValue(priceItemAttributes.access.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.cpeSla.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.cpeSla.errorFlag, priceItemAttributes.cpeSla.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.cpeSla.relProductAttribute)+index+':'+priceItemAttributes.cpeSla.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.cpeSla.relProductAttribute) + index + ':'+priceItemAttributes.cpeSla.overbookingType, 'CPE SLA');
                //CS.setAttributeValue(priceItemAttributes.cpeSla.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.voorloop.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.voorloop.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloop.relProductAttribute) + index + ':'+priceItemAttributes.voorloop.category, 'Voorloop');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloop.relProductAttribute)+index+':'+priceItemAttributes.voorloop.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloop.relProductAttribute) + index + ':'+priceItemAttributes.voorloop.overbookingType, 'Voorloop');
                CS.setAttributeValue(priceItemAttributes.voorloop.errorFlag, priceItemAttributes.voorloop.errorMessageMoreThanOne);
                //CS.setAttributeValue(priceItemAttributes.voorloop.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.voorloopInternet.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.voorloopInternet.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.voorloopInternet.errorFlag, priceItemAttributes.voorloopInternet.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute)+index+':'+priceItemAttributes.voorloopInternet.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute) + index + ':'+priceItemAttributes.voorloopInternet.overbookingType, 'Voorloop Internet');
                //CS.setAttributeValue(priceItemAttributes.voorloopInternet.setFlag, constants.set);
            }           
            else if (attWrapper.reference == priceItemAttributes.wirelessInstallation.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.wirelessInstallation.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.wirelessInstallation.errorFlag, priceItemAttributes.wirelessInstallation.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessInstallation.relProductAttribute)+index+':'+priceItemAttributes.wirelessInstallation.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessInstallation.relProductAttribute) + index + ':'+priceItemAttributes.wirelessInstallation.overbookingType, 'Wireless Installation');
                //CS.setAttributeValue(priceItemAttributes.wirelessInstallation.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.wirelessPricePlan.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.wirelessPricePlan.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.wirelessPricePlan.errorFlag, priceItemAttributes.wirelessPricePlan.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute)+index+':'+priceItemAttributes.wirelessPricePlan.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute) + index + ':'+priceItemAttributes.wirelessPricePlan.overbookingType, 'Wireless Price Plan');
                //CS.setAttributeValue(priceItemAttributes.wirelessPricePlan.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.wirelessPricePlanInternet.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.wirelessPricePlanInternet.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.wirelessPricePlanInternet.errorFlag, priceItemAttributes.wirelessPricePlanInternet.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute)+index+':'+priceItemAttributes.wirelessPricePlanInternet.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute) + index + ':'+priceItemAttributes.wirelessPricePlanInternet.overbookingType, 'Wireless Price Plan Internet');
                //CS.setAttributeValue(priceItemAttributes.wirelessPricePlanInternet.setFlag, constants.set);
            }           
            else if (attWrapper.reference == priceItemAttributes.cpe.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.cpe.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.cpe.errorFlag, priceItemAttributes.cpe.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.cpe.relProductAttribute)+index+':'+priceItemAttributes.cpe.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.cpe.relProductAttribute) + index + ':'+priceItemAttributes.cpe.overbookingType, 'CPE');
                //CS.setAttributeValue(priceItemAttributes.cpe.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.hardwareToeslagh.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.hardwareToeslagh.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.hardwareToeslagh.errorFlag, priceItemAttributes.hardwareToeslagh.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.hardwareToeslagh.relProductAttribute)+index+':'+priceItemAttributes.hardwareToeslagh.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.hardwareToeslagh.relProductAttribute) + index + ':'+priceItemAttributes.hardwareToeslagh.overbookingType, 'Hardware Toeslagh');
                //CS.setAttributeValue(priceItemAttributes.hardwareToeslagh.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.wirelessOnly.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.wirelessOnly.relProductAttribute].relatedProducts.length-1;
                CS.setAttributeValue(priceItemAttributes.wirelessOnly.errorFlag, priceItemAttributes.wirelessOnly.errorMessageMoreThanOne);
                CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute)+index+':'+priceItemAttributes.wirelessOnly.hirarchyLevel, 'Parent');
                CS.setAttributeValue(withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute) + index + ':'+priceItemAttributes.wirelessOnly.overbookingType, 'Wireless Only');
                //CS.setAttributeValue(priceItemAttributes.wirelessOnly.setFlag, constants.set);
                //CS.markConfigurationInvalid(priceItemAttributes.wirelessOnly.errorMessageMoreThanOne);
            }
        }
        */
        function setLookupAttributeIfNone(attWrapper){
            if(attWrapper.reference == priceItemAttributes.redundancy.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.redundancy.errorFlag, priceItemAttributes.redundancy.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.redundancy.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.evc1.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evc1.errorFlag, priceItemAttributes.evc1.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.evc1.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.evc1Vlan.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evc1Vlan.errorFlag, priceItemAttributes.evc1Vlan.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.evc1Vlan.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.oneFixedIAD.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.oneFixedIAD.errorFlag, priceItemAttributes.oneFixedIAD.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.oneFixedIAD.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.evc2.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evc2.errorFlag, priceItemAttributes.evc2.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.evc2.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.evc2Vlan.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evc2Vlan.errorFlag, priceItemAttributes.evc2Vlan.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.evc2Vlan.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.evcOneFixed.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evcOneFixed.errorFlag, priceItemAttributes.evcOneFixed.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.evcOneFixed.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.evcOneNet.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.evcOneNet.errorFlag, priceItemAttributes.evcOneNet.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.evcOneNet.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.access.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.access.errorFlag, priceItemAttributes.access.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.access.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.cpeSla.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.cpeSla.errorFlag,priceItemAttributes.cpeSla.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.cpeSla.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.voorloop.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.voorloop.errorFlag,priceItemAttributes.voorloop.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.voorloop.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.voorloopInternet.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.voorloopInternet.errorFlag,priceItemAttributes.voorloopInternet.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.voorloopInternet.setFlag, constants.set);
            }           
            else if (attWrapper.reference == priceItemAttributes.wirelessInstallation.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.wirelessInstallation.errorFlag,priceItemAttributes.wirelessInstallation.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.wirelessInstallation.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.wirelessPricePlan.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.wirelessPricePlan.errorFlag, priceItemAttributes.wirelessPricePlan.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.wirelessPricePlan.setFlag,constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.wirelessPricePlanInternet.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.wirelessPricePlanInternet.errorFlag, priceItemAttributes.wirelessPricePlanInternet.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.wirelessPricePlanInternet.setFlag,constants.set);
            }           
            else if ((attWrapper.reference == priceItemAttributes.cpe.lookupAttributeParent)||(attWrapper.reference == 'CPE_Lookup_Split_0')){
                CS.setAttributeValue(priceItemAttributes.cpe.errorFlag, priceItemAttributes.cpe.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.cpe.setFlag, constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.hardwareToeslagh.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.hardwareToeslagh.errorFlag, priceItemAttributes.hardwareToeslagh.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.hardwareToeslagh.setFlag,constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.wirelessOnly.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.wirelessOnly.errorFlag, priceItemAttributes.wirelessOnly.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.wirelessOnly.setFlag,constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.managedInternetEVC.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.managedInternetEVC.errorFlag, priceItemAttributes.managedInternetEVC.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.managedInternetEVC.setFlag,constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.managedInternetEVCVLAN.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.managedInternetEVCVLAN.errorFlag, priceItemAttributes.managedInternetEVCVLAN.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.managedInternetEVCVLAN.setFlag,constants.set);
            }
            else if (attWrapper.reference == priceItemAttributes.transportEVC.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.transportEVC.errorFlag, priceItemAttributes.transportEVC.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.transportEVC.setFlag,constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.sipTrunk.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.sipTrunk.errorFlag, priceItemAttributes.sipTrunk.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.sipTrunk.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.sipTrunkOneNet.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.sipTrunkOneNet.errorFlag, priceItemAttributes.sipTrunkOneNet.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.sipTrunkOneNet.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.oneFixedIAD.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.oneFixedIAD.errorFlag, priceItemAttributes.oneFixedIAD.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.oneFixedIAD.setFlag, constants.set);
            }
            else if(attWrapper.reference == priceItemAttributes.oneFixedCUBE.lookupAttributeParent){
                CS.setAttributeValue(priceItemAttributes.oneFixedCUBE.errorFlag, priceItemAttributes.oneFixedCUBE.errorMessageNone);
                CS.setAttributeValue(priceItemAttributes.oneFixedCUBE.setFlag, constants.set);
            }
        }

        //NC new Overbooking Type calculation for EVCs
        function getOverbookingTypeCalculated(){
            var anchorRef = CS.Service.config["EVC_PVC_dep_0"];
            var overbokingTypes = [];
            
            for (var i = 0; i < anchorRef.relatedProducts.length; i++) {
                //overbokingTypes.push(CS.getAttributeValue("EVC_PVC_dep_" + i + ":Overbooking_Type_0"));
                overbokingTypes.push(CS.getAttributeValue("EVC_PVC_dep_" + i + ":Overbooking_Type_for_mapping_0"));
            }
            
            return overbokingTypes.join(";");
            
        }
        //end //NC new Overbooking Type calculation for EVCs

		function setLookupAttributeIfOne(attWrapper, result, lookupAttr) {
			var neededLookupAttr = '';
			var valueToLoad = result.Id;
			
			//attWrapper.reference zamijeniti zadnji broj iza _ sa 0

			if(lookupAttr == priceItemAttributes.oneNetIntegration.lookupAttributeParentONE){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.oneNetIntegration.lookupAttributeParentONX){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.redundancy.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.redundancy.lookupAttributeChild;
            }           
            else if(lookupAttr == priceItemAttributes.transportEVC.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.transportEVC.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.oneFixedIAD.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.oneFixedIAD.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.oneFixedCUBE.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.oneFixedCUBE.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.sipTrunk.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.sipTrunk.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.sipTrunkOneNet.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.sipTrunkOneNet.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.evcOneFixed.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.evcOneFixed.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.evcOneNet.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.evcOneNet.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.evc2.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.evc2.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.evc2Vlan.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.evc2Vlan.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.evc1.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.evc1.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.evc1Vlan.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.evc1Vlan.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.managedInternetEVC.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.managedInternetEVC.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.managedInternetEVCVLAN.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.managedInternetEVCVLAN.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.access.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.access.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.cpeSla.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.cpeSla.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.voorloop.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.voorloop.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.voorloopInternet.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.wirelessInstallation.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.wirelessInstallation.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.wirelessPricePlan.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.wirelessPricePlanInternet.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.lookupAttributeChild;
            }
            else if ((lookupAttr == priceItemAttributes.cpe.lookupAttributeParent)||(lookupAttr == 'CPE_Lookup_Split_0')){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.cpe.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.cpe.lookupAttributeParentIAD){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.cpe.lookupAttributeChild;
            }
            else if (lookupAttr == priceItemAttributes.hardwareToeslagh.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.hardwareToeslagh.lookupAttributeChild;
            }
            else if(lookupAttr.includes(priceItemAttributes.access.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.access.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.access.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.cpeSla.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.cpeSla.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.cpeSla.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.cpe.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.cpe.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.cpe.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.hardwareToeslagh.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.hardwareToeslagh.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.hardwareToeslagh.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.evc1.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.evc1.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.evc1.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.evc2.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.evc2.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.evc2.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.wirelessPricePlan.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.wirelessPricePlanInternet.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }           
            else if(lookupAttr.includes(priceItemAttributes.voorloop.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.voorloop.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.voorloop.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.voorloopInternet.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr == priceItemAttributes.wirelessOnly.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.wirelessOnly.lookupAttributeChild;
            }
            else if(lookupAttr.includes(priceItemAttributes.wirelessOnly.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.wirelessOnly.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.sfp.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.sfp.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.sfp.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr == priceItemAttributes.sfp.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.sfp.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.access.LineTypeInstallationFeeLookup){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.access.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.access.DealTypeInstallationFeeLookup){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.access.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.payu.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.payu.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.mobileusage.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.mobileusage.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.fixedFlatFee.lookupAttributeParent){
                neededLookupAttr =  attWrapper.reference + ':' + priceItemAttributes.fixedFlatFee.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.fixedFlatFee4.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.fixedFlatFee4.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.fixedFlatFee8.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.fixedFlatFee8.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.row.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.row.lookupAttributeChild;
            }
            else if(lookupAttr.includes(priceItemAttributes.bundles.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.bundles.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.bundles.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr.includes(priceItemAttributes.redundancy.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.redundancy.relProductAttribute))){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.redundancy.lookupAttributeChild;
				valueToLoad = result.Secondary_Price_Item__c;
            }
            else if(lookupAttr == priceItemAttributes.vcBasicDeploymentService.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcBasicDeploymentService.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcPrivateInfrastructureProduct.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcPrivateInfrastructureProduct.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcPrivateInfrastructureDeployment.lookupAttributeParent){
				neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcPrivateInfrastructureDeployment.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcSipTrunk.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcSipTrunk.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcBasicSupport.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcBasicSupport.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcHighCapacity.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcHighCapacity.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcBasicAnywhere.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcBasicAnywhere.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcBasicAnywhereCPaaS.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcBasicAnywhereCPaaS.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcStandardCRMConnector.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcStandardCRMConnector.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcAnywhereHosting.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcAnywhereHosting.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcAnywhereHostingCPaaS.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcAnywhereHostingCPaaS.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcWorkplaceBuddy.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcWorkplaceBuddy.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.vcExtendedAnywhere.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcExtendedAnywhere.lookupAttributeChild;
            }  
            else if(lookupAttr == priceItemAttributes.vcReceptionQueue.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.vcExtendedAnywhere.lookupAttributeChild;
            }  
            else if(lookupAttr == priceItemAttributes.oneFixedSBCSIP.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.oneFixedSBCSIP.lookupAttributeChild;
            }
            else if(lookupAttr == priceItemAttributes.gdspPlatformFee.lookupAttributeParent){
                neededLookupAttr = attWrapper.reference + ':' + priceItemAttributes.gdspPlatformFee.lookupAttributeChild;
            }

			return CS.SVC.loadLookupRecords([{
				lookupRecordId: valueToLoad,
				attribute: CS.Service.config[neededLookupAttr].attr
			}]).then(function () {
				if(lookupAttr == priceItemAttributes.oneNetIntegration.lookupAttributeParentONE){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.overbookingType, 'ONE Integration', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.oneNetIntegration.lookupAttributeParentONX){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.overbookingType, 'ONX Integration', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneNetIntegration.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.redundancy.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.redundancy.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.redundancy.overbookingType, 'Redundancy', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.redundancy.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.redundancy.hirarchyLevel, 'Parent', true);
				}           
				else if(lookupAttr == priceItemAttributes.transportEVC.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.transportEVC.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.transportEVC.overbookingType, 'Transport EVC', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.transportEVC.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.transportEVC.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.oneFixedIAD.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneFixedIAD.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneFixedIAD.overbookingType, 'IAD', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneFixedIAD.overbookingTypeMapping, 'Voice', true); // W-002049
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneFixedIAD.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.oneFixedCUBE.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneFixedCUBE.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneFixedCUBE.overbookingTypeMapping, 'Voice', true); // W-002049
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneFixedCUBE.overbookingType, 'CUBE', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneFixedCUBE.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.sipTrunk.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunk.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunk.overbookingType, 'SIP Trunk', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunk.overbookingTypeMapping, 'Voice', true); // W-002049
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunk.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunk.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.sipTrunkOneNet.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunkOneNet.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunkOneNet.overbookingType, 'SIP Trunk', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunkOneNet.overbookingTypeMapping, getOverbookingTypeCalculated(), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunkOneNet.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sipTrunkOneNet.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.evcOneFixed.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evcOneFixed.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evcOneFixed.overbookingType, 'OneFixed', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evcOneFixed.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evcOneFixed.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.evcOneNet.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evcOneNet.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evcOneNet.overbookingType, 'OneNet', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evcOneNet.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evcOneNet.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.evc2.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2.overbookingType, 'Voip', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2.hirarchyLevel, 'Parent', true);
				}
				else if(lookupAttr == priceItemAttributes.evc2Vlan.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2Vlan.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2Vlan.overbookingType, 'Transport Voip', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2Vlan.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2Vlan.hirarchyLevel, 'Parent', true);
				}
				else if (lookupAttr == priceItemAttributes.evc1.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1.overbookingType, 'Data', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1.hirarchyLevel, 'Parent', true);
				}
				else if (lookupAttr == priceItemAttributes.evc1Vlan.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1Vlan.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1Vlan.overbookingType, 'Transport Data', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1Vlan.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1Vlan.hirarchyLevel, 'Parent', true);
				}
				else if (lookupAttr == priceItemAttributes.managedInternetEVC.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.managedInternetEVC.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.managedInternetEVC.overbookingType, 'Internet', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.managedInternetEVC.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.managedInternetEVC.hirarchyLevel, 'Parent', true);
				}
				else if (lookupAttr == priceItemAttributes.managedInternetEVCVLAN.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.managedInternetEVCVLAN.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.managedInternetEVCVLAN.overbookingType, 'Transport Internet', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.managedInternetEVCVLAN.additionType, 'Automatic', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.managedInternetEVCVLAN.hirarchyLevel, 'Parent', true);
				}
				else if (lookupAttr == priceItemAttributes.access.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.access.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.access.hirarchyLevel, 'Parent', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.access.overbookingType, 'Access', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.access.overbookingTypeMapping, getOverbookingTypeCalculated(), true);
				}
				else if (lookupAttr == priceItemAttributes.cpeSla.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpeSla.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpeSla.hirarchyLevel, 'Parent', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpeSla.overbookingTypeMapping, getOverbookingTypeCalculated(), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpeSla.overbookingType, 'CPE SLA', true);
				}
				else if (lookupAttr == priceItemAttributes.voorloop.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloop.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloop.category, 'Voorloop', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloop.overbookingType, 'Voorloop', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloop.hirarchyLevel, 'Parent', true);
				}
				else if (lookupAttr == priceItemAttributes.voorloopInternet.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.category, 'Voorloop Internet', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.overbookingType, 'Voorloop Internet', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.hirarchyLevel, 'Parent', true);
				}
				else if (lookupAttr == priceItemAttributes.wirelessInstallation.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessInstallation.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessInstallation.category, 'Wireless Installation Fee', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessInstallation.overbookingType, 'Wireless Installation Fee', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessInstallation.hirarchyLevel, 'Parent', true);
				}
				else if (lookupAttr == priceItemAttributes.wirelessPricePlan.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.category, 'Wireless price plan', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.overbookingType, 'Wireless price plan', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.hirarchyLevel, 'Parent', true);
				}
				else if (lookupAttr == priceItemAttributes.wirelessPricePlanInternet.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.category, 'Wireless price plan Internet', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.overbookingType, 'Wireless price plan Internet', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.hirarchyLevel, 'Parent', true);
				}           
				//CPE Split
				else if ((lookupAttr == priceItemAttributes.cpe.lookupAttributeParent)||(lookupAttr == 'CPE_Lookup_Split_0')){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.hardwareToeslagh, result.Hardware_Toeslag__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.hirarchyLevel, 'Parent', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.overbookingTypeMapping, getOverbookingTypeCalculated(), true);
					CS.setAttributeValue(attWrapper.reference + ':' +  priceItemAttributes.cpe.overbookingType, 'CPE', true);
					
					var qualityOfService = getCPEQualityOfService();
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.qualityOfService, 'true', true);
				}
				else if (lookupAttr == priceItemAttributes.cpe.lookupAttributeParentIAD){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.hardwareToeslagh, result.Hardware_Toeslag__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.hirarchyLevel, 'Parent', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.overbookingTypeMapping, getOverbookingTypeCalculated(), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.overbookingType, 'CPE IAD', true);
				}
				else if (lookupAttr == priceItemAttributes.hardwareToeslagh.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.hardwareToeslagh.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.hardwareToeslagh.hardwareToeslagh, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.hardwareToeslagh.hirarchyLevel, 'Parent', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.hardwareToeslagh.overbookingType, 'Hardware Toeslagh', true);
				}
				else if(lookupAttr.includes(priceItemAttributes.access.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.access.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.access.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.access.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0] + ':Overbooking_Type_0', true));
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.access.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0] + ':Overbooking_Type_for_mapping_0', true));
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}
				else if(lookupAttr.includes(priceItemAttributes.cpeSla.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.cpeSla.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpeSla.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpeSla.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpeSla.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}
				else if(lookupAttr.includes(priceItemAttributes.cpe.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.cpe.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.cpe.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}
				else if(lookupAttr.includes(priceItemAttributes.hardwareToeslagh.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.hardwareToeslagh.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.hardwareToeslagh.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.hardwareToeslagh.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.hardwareToeslagh.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
				}
				else if(lookupAttr.includes(priceItemAttributes.evc1.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.evc1.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc1.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}
				else if(lookupAttr.includes(priceItemAttributes.evc2.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.evc2.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.evc2.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}
				else if(lookupAttr.includes(priceItemAttributes.wirelessPricePlan.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.category, 'Wireless price plan', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlan.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}
				else if(lookupAttr.includes(priceItemAttributes.wirelessPricePlanInternet.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.category, 'Wireless price plan Internet', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessPricePlanInternet.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}           
				else if(lookupAttr.includes(priceItemAttributes.voorloop.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.voorloop.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloop.category, 'Voorloop', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloop.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloop.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloop.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);              
				}
				else if(lookupAttr.includes(priceItemAttributes.voorloopInternet.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.category, 'Voorloop Internet', true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.voorloopInternet.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}
				else if(lookupAttr == priceItemAttributes.wirelessOnly.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessOnly.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessOnly.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessOnly.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
				}
				else if(lookupAttr.includes(priceItemAttributes.wirelessOnly.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessOnly.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessOnly.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.wirelessOnly.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}
				else if(lookupAttr.includes(priceItemAttributes.sfp.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.sfp.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sfp.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sfp.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sfp.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_for_mapping_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
				}
				else if(lookupAttr == priceItemAttributes.sfp.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sfp.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.sfp.overbookingType, 'SFP', true);
					CS.setAttributeValue(attWrapper.reference + ':Quantity_0', calculateNumOfSFP(), true);
					CS.setAttributeValue(priceItemAttributes.sfp.setFlag, constants.set, true);
				}
				else if(lookupAttr == priceItemAttributes.access.LineTypeInstallationFeeLookup){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.access.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.access.DealTypeInstallationFeeLookup){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.access.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.payu.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.payu.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.mobileusage.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.mobileusage.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.fixedFlatFee.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.fixedFlatFee.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.fixedFlatFee4.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.fixedFlatFee4.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.fixedFlatFee8.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.fixedFlatFee8.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.row.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.row.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr.includes(priceItemAttributes.bundles.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.bundles.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.bundles.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Group_Name_0', result.Group_Name_del__c, true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Overbooking_Type_0', CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':Overbooking_Type_for_mapping_0', result.Proposition_Component__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Quantity_0', result.Default_Quantity__c, true);
					CS.setAttributeValue(attWrapper.reference + ':BundlePGAlookup_0', result.Id);
				}
				else if(lookupAttr.includes(priceItemAttributes.redundancy.pGALookupAttribute) && lookupAttr.includes(withoutIndex(priceItemAttributes.redundancy.relProductAttribute))){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.redundancy.lookupAttributeChild, result.Secondary_Price_Item__c, true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.redundancy.overbookingType, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':Overbooking_Type_0'), true);
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.redundancy.overbookingTypeMapping, CS.getAttributeValue(lookupAttr.split(':').slice()[0]+':);'), true);
					CS.setAttributeValue(attWrapper.reference + ':One_Off_Charge_Override_0', result.One_Off_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Recurring_Charge_Override_0', result.Recurring_Charge__c, true);
					CS.setAttributeValue(attWrapper.reference + ':Override_charges_0', result.Override_Charges__c, true);
				}
				else if(lookupAttr == priceItemAttributes.vcBasicDeploymentService.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcBasicDeploymentService.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.vcPrivateInfrastructureProduct.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcPrivateInfrastructureProduct.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.vcPrivateInfrastructureDeployment.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcPrivateInfrastructureDeployment.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.vcSipTrunk.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcSipTrunk.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.vcBasicSupport.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcBasicSupport.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.vcHighCapacity.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcHighCapacity.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.vcBasicAnywhere.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcBasicAnywhere.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':Category_0', 'Anywhere 365 Implementation', true);
				}
                else if(lookupAttr == priceItemAttributes.vcBasicAnywhereCPaaS.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcBasicAnywhereCPaaS.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':Category_0', 'Anywhere 365 Implementation', true);
				}
				else if(lookupAttr == priceItemAttributes.vcStandardCRMConnector.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcStandardCRMConnector.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.vcAnywhereHosting.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcAnywhereHosting.lookupAttributeChild, result.Id, true);
				}
                else if(lookupAttr == priceItemAttributes.vcAnywhereHostingCPaaS.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcAnywhereHostingCPaaS.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.vcWorkplaceBuddy.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcWorkplaceBuddy.lookupAttributeChild, result.Id, true);
				}
				else if(lookupAttr == priceItemAttributes.vcExtendedAnywhere.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcExtendedAnywhere.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':Category_0', 'Anywhere 365 Implementation', true);
				}  
				else if(lookupAttr == priceItemAttributes.vcReceptionQueue.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.vcExtendedAnywhere.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':Category_0', 'Anywhere 365 Implementation', true);
				}  
				else if(lookupAttr == priceItemAttributes.oneFixedSBCSIP.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.oneFixedSBCSIP.lookupAttributeChild, result.Id, true);
					CS.setAttributeValue(attWrapper.reference + ':Quantity_0', CS.getAttributeValue('SIP_Channels_0'), true);
				}
                else if(lookupAttr == priceItemAttributes.gdspPlatformFee.lookupAttributeParent){
					CS.setAttributeValue(attWrapper.reference + ':' + priceItemAttributes.gdspPlatformFee.lookupAttributeChild, result.Id, true);
				} 
			});
        }
        
        
        
        function doLookupQueryAndInvalidate(name, dynamicFilterMap, productDefinitionId,attWrapper) {
            return new Promise(function (resolve, reject) {
                Visualforce.remoting.Manager.invokeAction(
                    'cscfga.UISupport.doMultiRowLookupQuery',
                    name,
                    dynamicFilterMap,
                    productDefinitionId,
                    function(result, event) {
                        if (event.status) {
                            var pgaResult = new Object();

                            pgaResult.dynamicFilterMap = dynamicFilterMap;
                            pgaResult.lookupQueryName = name;
                            pgaResult.attribute = attWrapper;
                            pgaResult.res = result==null ? undefined : result;
                            
                            resolve(pgaResult);  
                              
                        } else {
                            console.error(event);
                            
                            //displayButtons(); VF update
                            reject(event);
                        }
                    },
                    {escape: false}
                );
                
                
            }).then(function(pgaResult){
                //cspmb__Price_Item_Group_Association__c object is deprecated and this has to be fixed so that it is no longer used
                if(pgaResult.res !== undefined && pgaResult.res[0].attributes.type == 'cspmb__Price_Item_Group_Association__c'){
                    
                    var pgaParams = getPGAparams(pgaResult.attribute.reference);
                    
                    _.each(pgaResult.res, function(result){
                        addRelProductInlineAndSetLookupPGA(pgaParams.rel, pgaParams.flagSetRef, pgaParams.lookupAttr ,result);
                    });
                } 
                else if(pgaResult.res !== undefined && pgaResult.res[0].attributes.type == 'Product_Group_Association__c'){
                    var pgaParams = getPGAparams(pgaResult.attribute.reference);
                    
                    _.each(pgaResult.res, function(result){
                        addRelProductInlineAndSetLookupPGA(pgaParams.rel, pgaParams.flagSetRef, pgaParams.lookupAttr ,result);
                    });
                    
                } else if(pgaResult.res !== undefined && pgaResult.res[0].attributes.type == 'cspmb__Price_Item__c') {
                    
                    var params = getRPparams(pgaResult.attribute.reference);
                    
                    if((pgaResult.lookupQueryName.indexOf('PriceItemEVC') !== -1) || (pgaResult.lookupQueryName.indexOf('ManagedInternetEVCPVC') !== -1)){
                    
                        var minUpstreamBandwidth;
                        var minDownstreamBandwidth;
                        var overbooking;
                    
                            if((pgaResult.lookupQueryName == 'PriceItemEVC1') || pgaResult.lookupQueryName == 'PriceItemEVC1VLAN'){
                                overbooking = CS.getAttributeValue('Overbooking_Data_0');
                                minUpstreamBandwidth = (pgaResult.dynamicFilterMap["Min. Upstream Bandwidth"] !== undefined && pgaResult.dynamicFilterMap["Min. Upstream Bandwidth"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min. Upstream Bandwidth"].replace(/\./g,'').replace(',','.')):0;
                                minDownstreamBandwidth = (pgaResult.dynamicFilterMap["Min.Downstream Bandwidth"] !== undefined && pgaResult.dynamicFilterMap["Min.Downstream Bandwidth"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min.Downstream Bandwidth"].replace(/\./g,'').replace(',','.')):0;     
                            }
                            else if (pgaResult.lookupQueryName == 'PriceItemEVC2' || pgaResult.lookupQueryName == 'PriceItemEVC2VLAN'){
                                overbooking = CS.getAttributeValue('Overbooking_Voip_0');
                                minUpstreamBandwidth = (pgaResult.dynamicFilterMap["Min. Upstream Bandwidth 2"] !== undefined && pgaResult.dynamicFilterMap["Min. Upstream Bandwidth 2"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min. Upstream Bandwidth 2"].replace(/\./g,'').replace(',','.')):0;
                                minDownstreamBandwidth = (pgaResult.dynamicFilterMap["Min.Downstream Bandwidth 2"]!== undefined && pgaResult.dynamicFilterMap["Min.Downstream Bandwidth 2"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min.Downstream Bandwidth 2"].replace(/\./g,'').replace(',','.')):0;                    
                                
                            }
                            else if(pgaResult.lookupQueryName == 'ManagedInternetEVCPVC' || pgaResult.lookupQueryName == 'ManagedInternetEVCPVCVLAN'){
                                overbooking = CS.getAttributeValue('Overbooking_Internet_0');
                                minUpstreamBandwidth = (pgaResult.dynamicFilterMap["Min. Upstream Bandwidth Internet"] !== undefined && pgaResult.dynamicFilterMap["Min. Upstream Bandwidth Internet"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min. Upstream Bandwidth Internet"].replace(/\./g,'').replace(',','.')):0;
                                minDownstreamBandwidth = (pgaResult.dynamicFilterMap["Min.Downstream Bandwidth Internet"] !== undefined && pgaResult.dynamicFilterMap["Min.Downstream Bandwidth Internet"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min.Downstream Bandwidth Internet"].replace(/\./g,'').replace(',','.')):0;
                            }
                            else if(pgaResult.lookupQueryName == 'PriceItemEVCOneFixed'){
                                overbooking = CS.getAttributeValue('Overbooking_OneFixed_0');
                                //minUpstreamBandwidth = (pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== undefined && pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"].replace(/\./g,'').replace(',','.')):0;
                                //minDownstreamBandwidth = (pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== undefined && pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"].replace(/\./g,'').replace(',','.')):0;
                                minUpstreamBandwidth = (pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== undefined && pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"]):0;
                                minDownstreamBandwidth = (pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== undefined && pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"]):0;
                            }
                            else if(pgaResult.lookupQueryName == 'PriceItemEVCOneNet'){
                                overbooking = CS.getAttributeValue('Overbooking_OneNet_0');
                                //minUpstreamBandwidth = (pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== undefined && pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"].replace(/\./g,'').replace(',','.')):0;
                                //minDownstreamBandwidth = (pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== undefined && pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min Bandwidth OneFixed"].replace(/\./g,'').replace(',','.')):0;
                                minUpstreamBandwidth = (pgaResult.dynamicFilterMap["Min Bandwidth OneNet"] !== undefined && pgaResult.dynamicFilterMap["Min Bandwidth OneNet"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min Bandwidth OneNet"]):0;
                                minDownstreamBandwidth = (pgaResult.dynamicFilterMap["Min Bandwidth OneNet"] !== undefined && pgaResult.dynamicFilterMap["Min Bandwidth OneNet"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min Bandwidth OneNet"]):0;
                            }
                            else if(pgaResult.lookupQueryName == 'PriceItemEVCTransport'){
                                overbooking = CS.getAttributeValue('Overbooking_Transport_0');
                                
                                var minUpstreamBandwidthEVC1 = 0;
                                var minDownstreamBandwidthEVC1 = 0;
                                
                                var minUpstreamBandwidthEVC2 = 0;
                                var minDownstreamBandwidthEVC2 = 0;
                                
                                var minUpstreamBandwidthInternet = 0;
                                var minDownstreamBandwidthInternet = 0;
                                
                                if(CS.getAttributeValue('Transport_EVC_IPVPN_0') == true){
                                    if(CS.getAttributeValue('Overbooking_Data_0') != ''){
                                        minUpstreamBandwidthEVC1 = (pgaResult.dynamicFilterMap["Min. Upstream Bandwidth"] !== undefined && pgaResult.dynamicFilterMap["Min. Upstream Bandwidth"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min. Upstream Bandwidth"].replace(/\./g,'').replace(',','.')):0;
                                        minDownstreamBandwidthEVC1 = (pgaResult.dynamicFilterMap["Min.Downstream Bandwidth"] !== undefined && pgaResult.dynamicFilterMap["Min.Downstream Bandwidth"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min.Downstream Bandwidth"].replace(/\./g,'').replace(',','.')):0;     
                                    }
                                    
                                    if(CS.getAttributeValue('Overbooking_Voip_0') != ''){
                                        minUpstreamBandwidthEVC2 = (pgaResult.dynamicFilterMap["Min. Upstream Bandwidth 2"] !== undefined && pgaResult.dynamicFilterMap["Min. Upstream Bandwidth 2"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min. Upstream Bandwidth 2"].replace(/\./g,'').replace(',','.')):0;
                                        minDownstreamBandwidthEVC2 = (pgaResult.dynamicFilterMap["Min.Downstream Bandwidth 2"]!== undefined && pgaResult.dynamicFilterMap["Min.Downstream Bandwidth 2"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min.Downstream Bandwidth 2"].replace(/\./g,'').replace(',','.')):0;      
                                    }
                                    
                                }
                                
                                if(CS.getAttributeValue('Transport_EVC_Internet_0') == true && CS.getAttributeValue('Overbooking_Internet_0') != ''){
                                    minUpstreamBandwidthInternet = (pgaResult.dynamicFilterMap["Min. Upstream Bandwidth Internet"] !== undefined && pgaResult.dynamicFilterMap["Min. Upstream Bandwidth Internet"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min. Upstream Bandwidth 2"].replace(/\./g,'').replace(',','.')):0;
                                    minDownstreamBandwidthInternet = (pgaResult.dynamicFilterMap["Min.Downstream Bandwidth Internet"]!== undefined && pgaResult.dynamicFilterMap["Min.Downstream Bandwidth Internet"] !== '') ? parseFloat(pgaResult.dynamicFilterMap["Min.Downstream Bandwidth 2"].replace(/\./g,'').replace(',','.')):0;                    
                                }
                                
                                minUpstreamBandwidth = minUpstreamBandwidthEVC1 + minUpstreamBandwidthEVC2 + minUpstreamBandwidthInternet;
                                minDownstreamBandwidth = minDownstreamBandwidthEVC1 + minDownstreamBandwidthEVC2 + minDownstreamBandwidthInternet;
                            }
                    
                            //var params = getRPparams(pgaResult.attribute.reference);
                            var exactMatch = false;
                            var addRelProduct = false;
                                        
                            var totalBandwidthUp;
                            var totalBandwidthDown;
                            var siteCheckAvailableBandwidthDown;
                            var siteCheckAvailableBandwidthUp;
                            var index = 0;
                            
                            var sitePremiumApplicable = CS.getAttributeValue('Premium_Applicable_0','Boolean');
                            
                        
                            if(sitePremiumApplicable){
                                
                                if(overbooking == 'Entry'){
                                    totalBandwidthUp = CS.getAttributeValue('Total_Bandwidth_Up_Entry_0'); 
                                    totalBandwidthDown = CS.getAttributeValue('Total_Bandwidth_Down_Entry_0'); 
                                    siteCheckAvailableBandwidthDown = CS.getAttributeValue('Available_bandwidth_down_0'); 
                                    siteCheckAvailableBandwidthUp = CS.getAttributeValue('Available_bandwidth_up_0'); 
                                }
                                else if(overbooking == 'Premium'){
                                    totalBandwidthUp = CS.getAttributeValue('Total_Bandwidth_Up_Premium_0');
                                    totalBandwidthDown = CS.getAttributeValue('Total_Bandwidth_Down_Premium_0');
                                    siteCheckAvailableBandwidthDown = CS.getAttributeValue('Bandwidth_Down_Premium_0');
                                    siteCheckAvailableBandwidthUp = CS.getAttributeValue('Bandwidth_Up_Premium_0'); 
                                }
                            
                                for(var i = 0; i < pgaResult.res.length; i++){
                                    if((pgaResult.res[i].Available_bandwidth_down__c == minDownstreamBandwidth && minDownstreamBandwidth != 0 && pgaResult.res[i].Available_bandwidth_up__c == minUpstreamBandwidth && minUpstreamBandwidth != 0)
                                        || (pgaResult.res[i].Available_bandwidth_down__c == minDownstreamBandwidth && minDownstreamBandwidth != 0 && minUpstreamBandwidth == 0)
                                        || (pgaResult.res[i].Available_bandwidth_up__c == minUpstreamBandwidth && minUpstreamBandwidth != 0 && minDownstreamBandwidth == 0)){
                                            if(pgaResult.res[i].Available_bandwidth_down__c <= (siteCheckAvailableBandwidthDown - totalBandwidthDown) && pgaResult.res[i].Available_bandwidth_up__c <= (siteCheckAvailableBandwidthUp - totalBandwidthUp)){
                                                exactMatch = true;
                                                index = i;
                                                break;
                                            }
                                    }
                                    else if(pgaResult.res[i].Available_bandwidth_down__c >= minDownstreamBandwidth && pgaResult.res[i].Available_bandwidth_down__c <= (siteCheckAvailableBandwidthDown - totalBandwidthDown) && pgaResult.res[i].Available_bandwidth_up__c >= minUpstreamBandwidth && pgaResult.res[i].Available_bandwidth_up__c <= (siteCheckAvailableBandwidthUp - totalBandwidthUp)){
                                        exactMatch = false;
                                        addRelProduct = true;
                                    }
                                }
                                
                                if(exactMatch){     
                                    if(overbooking == 'Entry'){
                                        CS.setAttributeValue('Total_Bandwidth_Up_Entry_0',totalBandwidthUp + pgaResult.res[index].Available_bandwidth_up__c);
                                        CS.setAttributeValue('Total_Bandwidth_Down_Entry_0',totalBandwidthDown + pgaResult.res[index].Available_bandwidth_down__c);
                                        
                                        //automatically add SFP if Price item covers the requirenments
                                        addSfpPriceItem(pgaResult.res[index], pgaResult.lookupQueryName);
                                    }
                                    else if(overbooking == 'Premium'){
                                        CS.setAttributeValue('Total_Bandwidth_Up_Premium_0',totalBandwidthUp + pgaResult.res[index].Available_bandwidth_up__c);
                                        CS.setAttributeValue('Total_Bandwidth_Down_Premium_0',totalBandwidthDown + pgaResult.res[index].Available_bandwidth_down__c);
                                        
                                        //automatically add SFP if Price item covers the requirenments
                                        addSfpPriceItem(pgaResult.res[index], pgaResult.lookupQueryName);
                                    }
                                    addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[index]);                            
                                }
                                else if(!exactMatch && addRelProduct){
                                    addRelProductInline(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[0],pgaResult.attribute);
                                    // AN CU setRuleAttributes(pgaResult.attribute);                            
                                }
                                else {
                                    setLookupAttributeIfNone(pgaResult.attribute);
                                }
                                
                            } else {
                                    totalBandwidthUp = CS.getAttributeValue('Total_Bandwidth_Up_0');
                                    totalBandwidthDown = CS.getAttributeValue('Total_Bandwidth_Down_0');
                                    
                                    siteCheckAvailableBandwidthDown = CS.getAttributeValue('Available_bandwidth_down_0');
                                    siteCheckAvailableBandwidthUp = CS.getAttributeValue('Available_bandwidth_up_0');
                                    
                                for(var i = 0; i < pgaResult.res.length; i++){
                                    if((pgaResult.res[i].Available_bandwidth_down__c == minDownstreamBandwidth && minDownstreamBandwidth != 0 && pgaResult.res[i].Available_bandwidth_up__c == minUpstreamBandwidth && minUpstreamBandwidth != 0)
                                        || (pgaResult.res[i].Available_bandwidth_down__c == minDownstreamBandwidth && minDownstreamBandwidth != 0 && minUpstreamBandwidth == 0)
                                        || (pgaResult.res[i].Available_bandwidth_up__c == minUpstreamBandwidth && minUpstreamBandwidth != 0 && minDownstreamBandwidth == 0)){
                                            if(pgaResult.res[i].Available_bandwidth_down__c <= (siteCheckAvailableBandwidthDown - totalBandwidthDown) && pgaResult.res[i].Available_bandwidth_up__c <= (siteCheckAvailableBandwidthUp - totalBandwidthUp)){
                                                exactMatch = true;
                                                index = i;
                                                break;
                                            }
                                        }
                                        else if(pgaResult.res[i].Available_bandwidth_down__c >= minDownstreamBandwidth && pgaResult.res[i].Available_bandwidth_down__c <= (siteCheckAvailableBandwidthDown - totalBandwidthDown) && pgaResult.res[i].Available_bandwidth_up__c >= minUpstreamBandwidth && pgaResult.res[i].Available_bandwidth_up__c <= (siteCheckAvailableBandwidthUp - totalBandwidthUp)){
                                            exactMatch = false;
                                            addRelProduct = true;
                                        }
                                }
                                
                                if(exactMatch){
                                    CS.setAttributeValue('Total_Bandwidth_Up_0',totalBandwidthUp + pgaResult.res[index].Available_bandwidth_up__c);
                                    CS.setAttributeValue('Total_Bandwidth_Down_0',totalBandwidthDown + pgaResult.res[index].Available_bandwidth_down__c);
                                    
                                    //automatically add SFP if Price item covers the requirenments      
                                    addSfpPriceItem(pgaResult.res[index], pgaResult.lookupQueryName);       
                                    addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[index]);                            
                                }
                                else if(!exactMatch && addRelProduct){
                                    addRelProductInline(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[0],pgaResult.attribute);
                                    //AN CU setRuleAttributes(pgaResult.attribute);                         
                                }
                                else {
                                    setLookupAttributeIfNone(pgaResult.attribute);
                                }
                        
                            }
                    }
                    else if(pgaResult.lookupQueryName == 'PriceItemOneFixedSIPTrunk' || pgaResult.lookupQueryName == 'PriceItemOneFixedCUBE'){
                        if(pgaResult.res.length == 1){
                            addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[0]);
                        } 
                        else if(pgaResult.res.length > 1){
                            pgaResult.res.sort(function(a,b){
                                return a.OneFixed_SIP_Channels__c - b.OneFixed_SIP_Channels__c}
                                );
                                
                            addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[0]);
                        }
                    }
                    else if(pgaResult.lookupQueryName == 'PriceItemOneFixedIAD'){
                            var technologyType = CS.getAttributeValue('Technology_type_0'); 
                            var numOfSIPChannels = parseInt(CS.getAttributeValue('SIP_Channels_0'));
                            var numberOfIADs = parseInt(CS.getAttributeValue('Num_IADs_0'));
                            
                            if(numberOfIADs > 1 && numberOfIADs < 4){
                                //another CPE has to be added automatically if 2 or more IADs are added?
                            }
                            
                            if(numberOfIADs > 0 && numberOfIADs < 5){
                                for(var i = 0; i < numberOfIADs; i++){
                                    //add IAD price item to CPE Related Product
                                    addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[0]); 
                                }
                            }
                            else {
                                //set info message to something like: "This configuration can support a max of 4 IADs"
                                CS.setAttributeValue('OneFixedIAD_error_0',oneFixedIADInfoMessage);
                            }
                    }
                    else if(pgaResult.lookupQueryName == 'PriceItemAccess'){
                        //implement rules specific for Access
                        var accessType = pgaResult.dynamicFilterMap["Access Type Calc"];
                        var vendor = pgaResult.dynamicFilterMap["Vendor"];
                        var numOfEVCPVC = CS.getAttributeValue('Total_Num_EVCPVC_0');
                        
                        if((vendor.includes('KPN') && accessType.includes('EthernetOverFiber')) || (accessType.includes('Coax'))){
                            //filter on region also
                            var region = CS.getAttributeValue('Region_0');
                            pgaResult.res = pgaResult.res.filter(function (value, index, array){
                                return (value.Region__c == region || value.Region__c == '*');
                                
                            });
                            
                        } else if(accessType.includes('EthernetOverCopper')){
                            //filter on total required bandwidth
                            // to be removed by changes on W-002457/W-002445
                            /*
                            var minUpData = CS.getAttributeValue("Min__Upstream_Bandwidth_0") == '' ? 0 : parseFloat(CS.getAttributeValue("Min__Upstream_Bandwidth_0").replace(/\./g,'').replace(',','.'));
                            var minDownData = CS.getAttributeValue("Min_Downstream_Bandwidth_0") == '' ? 0 : parseFloat(CS.getAttributeValue("Min_Downstream_Bandwidth_0").replace(/\./g,'').replace(',','.'));
                            
                            var minUpVoice = CS.getAttributeValue("Min__Upstream_Bandwidth_2_0") == '' ? 0 : parseFloat(CS.getAttributeValue("Min__Upstream_Bandwidth_2_0").replace(/\./g,'').replace(',','.'));
                            var minDownVoice = CS.getAttributeValue("Min_Downstream_Bandwidth_2_0") == '' ? 0 : parseFloat(CS.getAttributeValue("Min_Downstream_Bandwidth_2_0").replace(/\./g,'').replace(',','.'));
                            
                            var minUpInternet = CS.getAttributeValue("Min__Upstream_Bandwidth_Internet_0") == '' ? 0 : parseFloat(CS.getAttributeValue("Min__Upstream_Bandwidth_Internet_0").replace(/\./g,'').replace(',','.'));
                            var minDownInternet = CS.getAttributeValue("Min_Downstream_Bandwidth_Internet_0") == '' ? 0 : parseFloat(CS.getAttributeValue("Min_Downstream_Bandwidth_Internet_0").replace(/\./g,'').replace(',','.'));
                            
                            //var minUpOneFixed = CS.getAttributeValue("Min_Bandwidth_OneFixed_0") == '' ? 0 : parseFloat(CS.getAttributeValue("Min_Bandwidth_OneFixed_0").replace(/\./g,'').replace(',','.'));
                            //var minDownOneFixed = CS.getAttributeValue("Min_Bandwidth_OneFixed_0") == '' ? 0 : parseFloat(CS.getAttributeValue("Min_Bandwidth_OneFixed_0").replace(/\./g,'').replace(',','.'));
                            var minUpOneFixed = CS.getAttributeValue("Min_Bandwidth_OneFixed_0");
                            var minDownOneFixed = CS.getAttributeValue("Min_Bandwidth_OneFixed_0");

                            var totalReqBWUp = minUpData + minUpVoice + minUpInternet + minDownOneFixed;  
                            var totalReqBWDown = minDownData + minDownVoice + minDownInternet + minDownOneFixed;
                            */
                           
                            var totalReqBWUp = 0;  
                            var totalReqBWDown = 0;

                            // new logic according to chatter post from W-002457
                            // just sum all the bandwidths from all EVCPVCs
                            for(var index=0; index<CS.Service.config["EVC_PVC_dep_0"].relatedProducts.length; index++)
                            {
                                if ((CS.getAttributeValue('EVC_PVC_dep_' + index + ':Category_Name_0') == 'EVC' || 
                                CS.getAttributeValue('EVC_PVC_dep_' + index + ':Category_Name_0') == 'PVC') &&
                                CS.getAttributeValue('EVC_PVC_dep_' + index + ':EVC_PVC_0') != '') 
                                {
                                    totalReqBWDown += Number(CS.getAttributeValue('EVC_PVC_dep_' + index + ':EVC_Available_Bandwidth_Down_0', 'Decimal'));
                                    totalReqBWUp += Number(CS.getAttributeValue('EVC_PVC_dep_' + index + ':EVC_Available_Bandwidth_Up_0', 'Decimal'));
                                }
                            }
                            
                            pgaResult.res = pgaResult.res.filter(function (value, index, array){
                                return ((value.Available_bandwidth_down__c  >= totalReqBWDown) && (value.Available_bandwidth_up__c  >= totalReqBWUp));
                            });
                        }
                        
                        
                        if(pgaResult.res.length > 0){
                            pgaResult.res.sort(function(a,b){
                                return ((a.Available_bandwidth_down__c - b.Available_bandwidth_down__c) && (a.Available_bandwidth_up__c - b.Available_bandwidth_up__c))}
                                );
                                
                            if(accessType.includes('Coax')){
                                 for(var i = 0; i < numOfEVCPVC; i++){
                                    addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[0]); 
                                }
                            } else {
                                //AN CU - check
                                //addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[0], pgaResult.attribute);
                                addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[0]);
                            }     
                        } else {
                            setLookupAttributeIfNone(pgaResult.attribute);
                        }
                    }
                    //CPE split
                    else if((pgaResult.lookupQueryName == 'PriceItemCPEwithoutQoSSplit')||(pgaResult.lookupQueryName == 'PriceItemCPEwithQoSSplit')){
                        if(pgaResult.res.length > 0){
                            console.log('All results==');
                            console.log(pgaResult.res);
                            var resultsSplit = findBest(pgaResult.res, CS.getAttributeValue('Num_LAN_Total_0'), CS.getAttributeValue('Num_WAN_Total_0'));
                            console.log('Best Option::');
                            console.log(resultsSplit);
                            
                            if(resultsSplit.length > 0){
                                 _.each(resultsSplit, function(result){
                                    addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr,result);
                                });
                            }
                            else{
                                console.log("ERROR WHILE GETTING THE BEST ONE");
                                setLookupAttributeIfNone(pgaResult.attribute);
                                /*
                                if(pgaResult.res.length > 0){
                                    pgaResult.res.sort(function(a,b){
                                        if(a.cspmb__one_off_cost__c!=null && a.cspmb__one_off_cost__c!=''&& b.cspmb__one_off_cost__c!=null && b.cspmb__one_off_cost__c!=''){
                                            return (a.cspmb__one_off_cost__c - b.cspmb__one_off_cost__c)
                                        }
                                        else if(b.cspmb__one_off_cost__c!=null && b.cspmb__one_off_cost__c!='' && (a.cspmb__one_off_cost__c==null || a.cspmb__one_off_cost__c=='')){
                                            return 1;
                                        }
                                        else if(a.cspmb__one_off_cost__c!=null && a.cspmb__one_off_cost__c!='' && (b.cspmb__one_off_cost__c==null || b.cspmb__one_off_cost__c=='')){
                                            return -1;
                                        }
                                        else{
                                            return 0;
                                        }
                                    });
                                    addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr, pgaResult.res[0]);
                                } else {
                                    setLookupAttributeIfNone(pgaResult.attribute);
                                }
                                */
                            }
            
                            //addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr, pgaResult.res[0]);
                        } else {
                            setLookupAttributeIfNone(pgaResult.attribute);
                        }
                    }
                    //CPE lowest price improvement  --commented out because of CPE split
                    /*
                    else if((pgaResult.lookupQueryName == 'PriceItemCPEwithoutQoS')||(pgaResult.lookupQueryName == 'PriceItemCPEwithQoS')){
                        if(pgaResult.res.length > 0){
                            pgaResult.res.sort(function(a,b){
                                if(a.cspmb__one_off_cost__c!=null && a.cspmb__one_off_cost__c!=''&& b.cspmb__one_off_cost__c!=null && b.cspmb__one_off_cost__c!=''){
                                    return (a.cspmb__one_off_cost__c - b.cspmb__one_off_cost__c)
                                }
                                else if(b.cspmb__one_off_cost__c!=null && b.cspmb__one_off_cost__c!='' && (a.cspmb__one_off_cost__c==null || a.cspmb__one_off_cost__c=='')){
                                    return 1;
                                }
                                else if(a.cspmb__one_off_cost__c!=null && a.cspmb__one_off_cost__c!='' && (b.cspmb__one_off_cost__c==null || b.cspmb__one_off_cost__c=='')){
                                    return -1;
                                }
                                else{
                                    return 0;
                                }
                            });
                            addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr, pgaResult.res[0]);
                        } else {
                            setLookupAttributeIfNone(pgaResult.attribute);
                        }
                    }
                    */
                    //end CPE lowest price improvement
                    else {
                        if(pgaResult.res.length == 1){
                            _.each(pgaResult.res, function(result){
                                addRelProductInlineAndSetLookupPGA(params.rel, params.flagSetRef, params.lookupAttr ,result);
                            });     
                        } else if(pgaResult.res.length > 1){
                            addRelProductInline(params.rel, params.flagSetRef, params.lookupAttr ,pgaResult.res[0],pgaResult.attribute);
                            //AN CU setRuleAttributes(pgaResult.attribute);
                        }                       
                    }
                } 
                else if(pgaResult.res == null){
                    setLookupAttributeIfNone(pgaResult.attribute);
                }
                
                
                //Removed by COM - 07/2020
                /*
                EventHandler.subscribe(EventHandler.Event.RULES_FINISHED, function(payload) {
                       CS.Service.evaluateRules();
                 });
               */
               
                
            //}).then(function(){CS.UI.getCurrentInstance().displayScreen(0,null,null, false);CS.Rules.evaluateAllRules();})
        //access infra config ready removed 
            //}).then(function(){CS.UI.getCurrentInstance().displayScreen(0,null,null, true).then(function(){accessInfraConfigReady();})})
            }).then(function(){CS.UI.getCurrentInstance().displayScreen(0,null,null, true)})
       
            }
        
        //end EVC test
        
    function addSfpPriceItem(priceItemRecord, lookupQueryName){
        //addition of SFPs should only work for - PriceItemEVCOneFixed
        var accessType = priceItemRecord.Access_Type_Text__c;
        var vendor = priceItemRecord.Vendor__c
        
        if((priceItemRecord.Available_bandwidth_down__c > 102400) && (priceItemRecord.Available_bandwidth_up__c > 102400) 
            && (accessType.toLowerCase().indexOf(',ethernetoverfiber,') != -1) && (vendor.toLowerCase().indexOf('ziggo') != -1)){
                if(lookupQueryName == 'PriceItemEVC1'){
                    checkPGA('SFP_0','SFP1_set_0','SFP_Lookup_0');
                }
                else if(lookupQueryName == 'PriceItemEVC2'){
                    checkPGA('SFP_0','SFP2_set_0','SFP_Lookup_0');
                }
                else if(lookupQueryName == 'ManagedInternetEVCPVC'){
                    checkPGA('SFP_0','SFP3_set_0','SFP_Lookup_0');
                } 
            }
    }    
        
    //NC - test - change logic for adding RP's. First check for results and then create RP's and set attribute values, NOT create RP's and then check for results and set attribute values
    function getRPparams(relatedProductType){
        var retValue = new Object();

            if(relatedProductType == priceItemAttributes.oneNetIntegration.lookupAttributeParentONE){
                   retValue.rel = priceItemAttributes.oneNetIntegration.relProductAttribute;
                   retValue.flagSetRef = priceItemAttributes.oneNetIntegration.setFlagONE;
                   retValue.lookupAttr = priceItemAttributes.oneNetIntegration.lookupAttributeParentONE;
               }
            else if(relatedProductType == priceItemAttributes.oneNetIntegration.lookupAttributeParentONX){
                retValue.rel = priceItemAttributes.oneNetIntegration.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.oneNetIntegration.setFlagONX;
                retValue.lookupAttr = priceItemAttributes.oneNetIntegration.lookupAttributeParentONX;
           } 
           else if(relatedProductType == priceItemAttributes.redundancy.lookupAttributeParent){
                   retValue.rel = priceItemAttributes.redundancy.relProductAttribute;
                   retValue.flagSetRef = priceItemAttributes.redundancy.setFlag;
                   retValue.lookupAttr = priceItemAttributes.redundancy.lookupAttributeParent;
               } 
            else if(relatedProductType == priceItemAttributes.access.lookupAttributeParent){
                   retValue.rel = priceItemAttributes.access.relProductAttribute;
                   retValue.flagSetRef = priceItemAttributes.access.setFlag;
                   retValue.lookupAttr = priceItemAttributes.access.lookupAttributeParent;
               }  
           else if(relatedProductType == priceItemAttributes.access.LineTypeInstallationFeeLookup){
                retValue.rel = priceItemAttributes.access.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.access.LineTypeInstallationFeeSet;
               retValue.lookupAttr = priceItemAttributes.access.LineTypeInstallationFeeLookup;
            }
            else if(relatedProductType == priceItemAttributes.access.DealTypeInstallationFeeLookup){
                retValue.rel = priceItemAttributes.access.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.access.DealTypeInstallationFeeSet;
               retValue.lookupAttr = priceItemAttributes.access.DealTypeInstallationFeeLookup;
            }
            else if(relatedProductType == priceItemAttributes.evc1.lookupAttributeParent){
                retValue.rel = priceItemAttributes.evc1.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.evc1.setFlag;
               retValue.lookupAttr = priceItemAttributes.evc1.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.evc1Vlan.lookupAttributeParent){
                retValue.rel = priceItemAttributes.evc1Vlan.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.evc1Vlan.setFlag;
               retValue.lookupAttr = priceItemAttributes.evc1Vlan.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.evcOneFixed.lookupAttributeParent){
                retValue.rel = priceItemAttributes.evcOneFixed.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.evcOneFixed.setFlag;
               retValue.lookupAttr = priceItemAttributes.evcOneFixed.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.evcOneNet.lookupAttributeParent){
                retValue.rel = priceItemAttributes.evcOneNet.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.evcOneNet.setFlag;
               retValue.lookupAttr = priceItemAttributes.evcOneNet.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.evc2.lookupAttributeParent){
                retValue.rel = priceItemAttributes.evc2.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.evc2.setFlag;
               retValue.lookupAttr = priceItemAttributes.evc2.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.evc2Vlan.lookupAttributeParent){
                retValue.rel = priceItemAttributes.evc2Vlan.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.evc2Vlan.setFlag;
               retValue.lookupAttr = priceItemAttributes.evc2Vlan.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.wirelessPricePlan.lookupAttributeParent){
                retValue.rel = priceItemAttributes.wirelessPricePlan.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.wirelessPricePlan.setFlag;
               retValue.lookupAttr = priceItemAttributes.wirelessPricePlan.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.voorloop.lookupAttributeParent){
                retValue.rel = priceItemAttributes.voorloop.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.voorloop.setFlag;
               retValue.lookupAttr = priceItemAttributes.voorloop.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.wirelessPricePlanInternet.lookupAttributeParent){
                retValue.rel = priceItemAttributes.wirelessPricePlanInternet.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.wirelessPricePlanInternet.setFlag;
               retValue.lookupAttr = priceItemAttributes.wirelessPricePlanInternet.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.voorloopInternet.lookupAttributeParent){
                retValue.rel = priceItemAttributes.voorloopInternet.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.voorloopInternet.setFlag;
               retValue.lookupAttr = priceItemAttributes.voorloopInternet.lookupAttributeParent;
            }           
            else if(relatedProductType == priceItemAttributes.sfp.lookupAttributeParent){
                retValue.rel = priceItemAttributes.sfp.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.sfp.setFlag;
               retValue.lookupAttr = priceItemAttributes.sfp.lookupAttributeParent;
            }
            //CPE SPLIT or
            else if((relatedProductType == priceItemAttributes.cpe.lookupAttributeParent)||(relatedProductType == 'CPE_Lookup_Split_0')){
                retValue.rel = priceItemAttributes.cpe.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.cpe.setFlag;
               retValue.lookupAttr = priceItemAttributes.cpe.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.cpeSla.lookupAttributeParent){
                retValue.rel = priceItemAttributes.cpeSla.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.cpeSla.setFlag;
               retValue.lookupAttr = priceItemAttributes.cpeSla.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.wirelessOnly.lookupAttributeParent){
                retValue.rel = priceItemAttributes.wirelessOnly.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.wirelessOnly.setFlag;
               retValue.lookupAttr = priceItemAttributes.wirelessOnly.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.managedInternetEVC.lookupAttributeParent){
               retValue.rel = priceItemAttributes.managedInternetEVC.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.managedInternetEVC.setFlag;
               retValue.lookupAttr = priceItemAttributes.managedInternetEVC.lookupAttributeParent; 
            }
            else if(relatedProductType == priceItemAttributes.managedInternetEVCVLAN.lookupAttributeParent){
               retValue.rel = priceItemAttributes.managedInternetEVCVLAN.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.managedInternetEVCVLAN.setFlag;
               retValue.lookupAttr = priceItemAttributes.managedInternetEVCVLAN.lookupAttributeParent; 
            }
            else if(relatedProductType == priceItemAttributes.transportEVC.lookupAttributeParent){
               retValue.rel = priceItemAttributes.transportEVC.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.transportEVC.setFlag;
               retValue.lookupAttr = priceItemAttributes.transportEVC.lookupAttributeParent; 
            }
            else if(relatedProductType == priceItemAttributes.sipTrunk.lookupAttributeParent){
                retValue.rel = priceItemAttributes.sipTrunk.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.sipTrunk.setFlag;
               retValue.lookupAttr = priceItemAttributes.sipTrunk.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.sipTrunkOneNet.lookupAttributeParent){
                retValue.rel = priceItemAttributes.sipTrunkOneNet.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.sipTrunkOneNet.setFlag;
               retValue.lookupAttr = priceItemAttributes.sipTrunkOneNet.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.oneFixedIAD.lookupAttributeParent){
                retValue.rel = priceItemAttributes.oneFixedIAD.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.oneFixedIAD.setFlag;
               retValue.lookupAttr = priceItemAttributes.oneFixedIAD.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.oneFixedCUBE.lookupAttributeParent){
                retValue.rel = priceItemAttributes.oneFixedCUBE.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.oneFixedCUBE.setFlag;
               retValue.lookupAttr = priceItemAttributes.oneFixedCUBE.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.fixedFlatFee.lookupAttributeParent){
                retValue.rel = priceItemAttributes.fixedFlatFee.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.fixedFlatFee.setFlag;
               retValue.lookupAttr = priceItemAttributes.fixedFlatFee.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.fixedFlatFee4.lookupAttributeParent){
                retValue.rel = priceItemAttributes.fixedFlatFee4.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.fixedFlatFee4.setFlag;
               retValue.lookupAttr = priceItemAttributes.fixedFlatFee4.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.fixedFlatFee8.lookupAttributeParent){
                retValue.rel = priceItemAttributes.fixedFlatFee8.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.fixedFlatFee8.setFlag;
               retValue.lookupAttr = priceItemAttributes.fixedFlatFee8.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.row.lookupAttributeParent){
                retValue.rel = priceItemAttributes.row.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.row.setFlag;
               retValue.lookupAttr = priceItemAttributes.row.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.payu.lookupAttributeParent){
                retValue.rel = priceItemAttributes.payu.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.payu.setFlag;
               retValue.lookupAttr = priceItemAttributes.payu.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.mobileusage.lookupAttributeParent){
               retValue.rel = priceItemAttributes.mobileusage.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.mobileusage.setFlag;
               retValue.lookupAttr = priceItemAttributes.mobileusage.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.vcBasicDeploymentService.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcBasicDeploymentService.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcBasicDeploymentService.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcBasicDeploymentService.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.vcPrivateInfrastructureProduct.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcPrivateInfrastructureProduct.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcPrivateInfrastructureProduct.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcPrivateInfrastructureProduct.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.vcPrivateInfrastructureDeployment.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcPrivateInfrastructureDeployment.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcPrivateInfrastructureDeployment.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcPrivateInfrastructureDeployment.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.vcSipTrunk.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcSipTrunk.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcSipTrunk.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcSipTrunk.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.vcBasicSupport.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcBasicSupport.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcBasicSupport.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcBasicSupport.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.vcHighCapacity.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcHighCapacity.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcHighCapacity.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcHighCapacity.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.vcBasicAnywhere.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcBasicAnywhere.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcBasicAnywhere.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcBasicAnywhere.lookupAttributeParent;
             }
            else if(relatedProductType == priceItemAttributes.vcBasicAnywhereCPaaS.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcBasicAnywhereCPaaS.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcBasicAnywhereCPaaS.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcBasicAnywhereCPaaS.lookupAttributeParent;
            }
            else if(relatedProductType == priceItemAttributes.vcStandardCRMConnector.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcStandardCRMConnector.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcStandardCRMConnector.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcStandardCRMConnector.lookupAttributeParent;
             }
           
             else if(relatedProductType == priceItemAttributes.vcAnywhereHosting.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcAnywhereHosting.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcAnywhereHosting.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcAnywhereHosting.lookupAttributeParent;
             }
            else if(relatedProductType == priceItemAttributes.vcAnywhereHostingCPaaS.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcAnywhereHostingCPaaS.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcAnywhereHostingCPaaS.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcAnywhereHostingCPaaS.lookupAttributeParent;
            }
             else if(relatedProductType == priceItemAttributes.vcWorkplaceBuddy.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcWorkplaceBuddy.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcWorkplaceBuddy.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcWorkplaceBuddy.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.vcExtendedAnywhere.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcExtendedAnywhere.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcExtendedAnywhere.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcExtendedAnywhere.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.vcReceptionQueue.lookupAttributeParent){
                retValue.rel = priceItemAttributes.vcReceptionQueue.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.vcReceptionQueue.setFlag;
                retValue.lookupAttr = priceItemAttributes.vcReceptionQueue.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.oneFixedSBCSIP.lookupAttributeParent){
                retValue.rel = priceItemAttributes.oneFixedSBCSIP.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.oneFixedSBCSIP.setFlag;
                retValue.lookupAttr = priceItemAttributes.oneFixedSBCSIP.lookupAttributeParent;
             }
             else if(relatedProductType == priceItemAttributes.gdspPlatformFee.lookupAttributeParent){
                retValue.rel = priceItemAttributes.gdspPlatformFee.relProductAttribute;
                retValue.flagSetRef = priceItemAttributes.gdspPlatformFee.setFlag;
                retValue.lookupAttr = priceItemAttributes.gdspPlatformFee.lookupAttributeParent;
             }
           return retValue;
    }
        
       //Nikola Culej
       //Get correct params to invoke addRelProductInlineAndSetLookupPGA method
       function getPGAparams(relatedProductType){
           var retValue = new Object();
           
           if(relatedProductType.substring(0,relatedProductType.indexOf(':') - 1) == 'Miscellaneous_'){
               /*retValue.rel = priceItemAttributes.access.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.access.pgaSetFlag;
               retValue.lookupAttr = priceItemAttributes.access.relProductAttribute + ':' + priceItemAttributes.access.pGALookupAttribute;*/
               
               var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
               retValue.rel = priceItemAttributes.redundancy.relProductAttribute;
               retValue.flagSetRef = withoutIndex(priceItemAttributes.redundancy.relProductAttribute) + index +':' +priceItemAttributes.redundancy.pgaSetFlag;
               retValue.lookupAttr = withoutIndex(priceItemAttributes.redundancy.relProductAttribute) + index +':' + priceItemAttributes.redundancy.pGALookupAttribute;
               
           }
           else if(relatedProductType.substring(0,relatedProductType.indexOf(':') - 1) == 'Access_Infrastructure_'){
               /*retValue.rel = priceItemAttributes.access.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.access.pgaSetFlag;
               retValue.lookupAttr = priceItemAttributes.access.relProductAttribute + ':' + priceItemAttributes.access.pGALookupAttribute;*/
               
               var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
               retValue.rel = priceItemAttributes.access.relProductAttribute;
               retValue.flagSetRef = withoutIndex(priceItemAttributes.access.relProductAttribute) + index +':' +priceItemAttributes.access.pgaSetFlag;
               retValue.lookupAttr = withoutIndex(priceItemAttributes.access.relProductAttribute) + index +':' + priceItemAttributes.access.pGALookupAttribute;
               
           } else if(relatedProductType.substring(0,relatedProductType.indexOf(':') - 1) == 'CPE_'){
               /*retValue.rel = priceItemAttributes.cpe.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.cpe.pgaSetFlag;
               retValue.lookupAttr = priceItemAttributes.cpe.relProductAttribute + ':' + priceItemAttributes.cpe.pGALookupAttribute;*/
               
               var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
               retValue.rel = priceItemAttributes.cpe.relProductAttribute;
               retValue.flagSetRef = withoutIndex(priceItemAttributes.cpe.relProductAttribute) + index +':' +priceItemAttributes.cpe.pgaSetFlag;
               retValue.lookupAttr = withoutIndex(priceItemAttributes.cpe.relProductAttribute) + index +':' + priceItemAttributes.cpe.pGALookupAttribute;
               
           } else if(relatedProductType.substring(0,relatedProductType.indexOf(':') - 1) == 'CPE_SLA_items_'){
               /*retValue.rel = priceItemAttributes.cpeSla.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.cpeSla.pgaSetFlag;
               retValue.lookupAttr = priceItemAttributes.cpeSla.relProductAttribute + ':' + priceItemAttributes.cpeSla.pGALookupAttribute;*/
               
               var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
               retValue.rel = priceItemAttributes.cpeSla.relProductAttribute;
               retValue.flagSetRef = withoutIndex(priceItemAttributes.cpeSla.relProductAttribute) + index +':' +priceItemAttributes.cpeSla.pgaSetFlag;
               retValue.lookupAttr = withoutIndex(priceItemAttributes.cpeSla.relProductAttribute) + index +':' + priceItemAttributes.cpeSla.pGALookupAttribute;
               
           } else if(relatedProductType.substring(0,relatedProductType.indexOf(':') - 1) == 'Price_Item_Hardware_Toeslag_'){
               /*retValue.rel = priceItemAttributes.hardwareToeslagh.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.hardwareToeslagh.pgaSetFlag;
               retValue.lookupAttr = priceItemAttributes.hardwareToeslagh.relProductAttribute + ':' + priceItemAttributes.hardwareToeslagh.pGALookupAttribute;*/
               
               var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
               retValue.rel = priceItemAttributes.hardwareToeslagh.relProductAttribute;
               retValue.flagSetRef = withoutIndex(priceItemAttributes.hardwareToeslagh.relProductAttribute) + index +':' +priceItemAttributes.hardwareToeslagh.pgaSetFlag;
               retValue.lookupAttr = withoutIndex(priceItemAttributes.hardwareToeslagh.relProductAttribute) + index +':' + priceItemAttributes.hardwareToeslagh.pGALookupAttribute;
               
           } else if(relatedProductType.substring(0,relatedProductType.indexOf(':') - 1) == 'EVC_PVC_dep_'){
                /*retValue.rel = priceItemAttributes.evc1.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.evc1.pgaSetFlag;
               retValue.lookupAttr = priceItemAttributes.evc1.relProductAttribute + ':' + priceItemAttributes.evc1.pGALookupAttribute;*/
               
               var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
               retValue.rel = priceItemAttributes.evc1.relProductAttribute;
               retValue.flagSetRef = withoutIndex(priceItemAttributes.evc1.relProductAttribute) + index +':' +priceItemAttributes.evc1.pgaSetFlag;
               retValue.lookupAttr = withoutIndex(priceItemAttributes.evc1.relProductAttribute) + index +':' + priceItemAttributes.evc1.pGALookupAttribute;
               
           /*} else if(relatedProductType.substring(0,relatedProductType.indexOf(':')) == 'EVC_PVC_dep_1'){
               
               var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
               retValue.rel = priceItemAttributes.evc2.relProductAttribute;
               retValue.flagSetRef = withoutIndex(priceItemAttributes.evc2.relProductAttribute) + index +':' +priceItemAttributes.evc2.pgaSetFlag;
               retValue.lookupAttr = withoutIndex(priceItemAttributes.evc2.relProductAttribute) + index +':' + priceItemAttributes.evc2.pGALookupAttribute;*/
               
           } else if(relatedProductType.substring(0,relatedProductType.indexOf(':')) == 'Price_Item_Wireless_0'){
               if(relatedProductType.split(':').pop() == 'PGALookup_0'){
                    /*retValue.rel = priceItemAttributes.wirelessPricePlan.relProductAttribute;
                    retValue.flagSetRef = priceItemAttributes.wirelessPricePlan.pgaSetFlag;
                    retValue.lookupAttr = priceItemAttributes.wirelessPricePlan.relProductAttribute + ':' + priceItemAttributes.wirelessPricePlan.pGALookupAttribute;*/
                    
                var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
                retValue.rel = priceItemAttributes.wirelessPricePlan.relProductAttribute;
                retValue.flagSetRef = withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute) + index +':' +priceItemAttributes.wirelessPricePlan.pgaSetFlag;
                retValue.lookupAttr = withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute) + index +':' + priceItemAttributes.wirelessPricePlan.pGALookupAttribute;
               
               } else if(relatedProductType.split(':').pop() == 'PGALookupInternet_0'){
                    /*retValue.rel = priceItemAttributes.wirelessPricePlanInternet.relProductAttribute;
                    retValue.flagSetRef = priceItemAttributes.wirelessPricePlanInternet.pgaSetFlag;
                    retValue.lookupAttr = priceItemAttributes.wirelessPricePlanInternet.relProductAttribute + ':' + priceItemAttributes.wirelessPricePlanInternet.pGALookupAttribute;*/
                    
                var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
                retValue.rel = priceItemAttributes.wirelessPricePlanInternet.relProductAttribute;
                retValue.flagSetRef = withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute) + index +':' +priceItemAttributes.wirelessPricePlanInternet.pgaSetFlag;
                retValue.lookupAttr = withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute) + index +':' + priceItemAttributes.wirelessPricePlanInternet.pGALookupAttribute;
               }
               

           }
           else if(relatedProductType.substring(0,relatedProductType.indexOf(':')) == 'Voorloop_Wireless_items_0'){
               if(relatedProductType.split(':').pop() == 'PGALookup_0'){
                    /*retValue.rel = priceItemAttributes.voorloop.relProductAttribute;
                    retValue.flagSetRef = priceItemAttributes.voorloop.pgaSetFlag;
                    retValue.lookupAttr = priceItemAttributes.voorloop.relProductAttribute + ':' + priceItemAttributes.voorloop.pGALookupAttribute; */
                    
                    var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
                retValue.rel = priceItemAttributes.voorloop.relProductAttribute;
                retValue.flagSetRef = withoutIndex(priceItemAttributes.voorloop.relProductAttribute) + index +':' +priceItemAttributes.voorloop.pgaSetFlag;
                retValue.lookupAttr = withoutIndex(priceItemAttributes.voorloop.relProductAttribute) + index +':' + priceItemAttributes.voorloop.pGALookupAttribute;
                   
               }
               else if(relatedProductType.split(':').pop() == 'PGALookupInternet_0'){
                    /*retValue.rel = priceItemAttributes.voorloopInternet.relProductAttribute;
                    retValue.flagSetRef = priceItemAttributes.voorloopInternet.pgaSetFlag;
                    retValue.lookupAttr = priceItemAttributes.voorloopInternet.relProductAttribute + ':' + priceItemAttributes.voorloopInternet.pGALookupAttribute;*/
                    
                    var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
                retValue.rel = priceItemAttributes.voorloopInternet.relProductAttribute;
                retValue.flagSetRef = withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute) + index +':' +priceItemAttributes.voorloopInternet.pgaSetFlag;
                retValue.lookupAttr = withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute) + index +':' + priceItemAttributes.voorloopInternet.pGALookupAttribute;
               }               
           }
           else if(relatedProductType.substring(0,relatedProductType.indexOf(':') - 1) == 'Price_Item_Wireless_Standalone_'){
               /*retValue.rel = priceItemAttributes.wirelessOnly.relProductAttribute;
               retValue.flagSetRef = priceItemAttributes.wirelessOnly.pgaSetFlag;
               retValue.lookupAttr = priceItemAttributes.wirelessOnly.relProductAttribute + ':' + priceItemAttributes.wirelessOnly.pGALookupAttribute;*/
               
                var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
                retValue.rel = priceItemAttributes.wirelessOnly.relProductAttribute;
                retValue.flagSetRef = withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute) + index +':' +priceItemAttributes.wirelessOnly.pgaSetFlag;
                retValue.lookupAttr = withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute) + index +':' + priceItemAttributes.wirelessOnly.pGALookupAttribute;
           } 
           else if(relatedProductType.substring(0,relatedProductType.indexOf(':') - 1) == 'Bundle_Products_'){
               var index = relatedProductType.substring(relatedProductType.indexOf(':') - 1, relatedProductType.indexOf(':'));
               retValue.rel = priceItemAttributes.bundles.relProductAttribute;
               retValue.flagSetRef = withoutIndex(priceItemAttributes.bundles.relProductAttribute) + index +':' +priceItemAttributes.bundles.pgaSetFlag;
               retValue.lookupAttr = withoutIndex(priceItemAttributes.bundles.relProductAttribute) + index +':' + priceItemAttributes.bundles.pGALookupAttribute;
           }
            
             else {
               retValue.rel = '';
               retValue.flagSetRef = ''; 
               retValue.lookupAttr = '';
           }
           
           return retValue;
       }
       //end
        
        
        //set correct index - not used, remove
        function setCorectSetValue(attrLookup){
            if(attrLookup.reference == 'EVCPVC_1_0'){
                 CS.setAttributeValue('EVC1_set_0','set'); 
            }
            if(attrLookup.reference == 'EVCPVC_2_0'){
                 CS.setAttributeValue('EVC2_set_0','set'); 
            }
            if(attrLookup.reference == 'Acccess_0'){
                 CS.setAttributeValue('Access_set_0','set'); 
            }
        }
        
        function getCorectSetValue(attrLookup){
            var setResult ='';
            if(attrLookup.reference == 'EVCPVC_1_0'){
                 setResult = CS.getAttributeValue('EVC1_set_0'); 
            }
            if(attrLookup.reference == 'EVCPVC_2_0'){
                 setResult = CS.getAttributeValue('EVC2_set_0'); 
            }
            if(attrLookup.reference == 'Acccess_0'){
                 setResult = CS.getAttributeValue('Access_set_0'); 
            }
            
            return setResult;
        }
        //end
        
        function calculateNumOfSFP(){
            var numOfSFP = 0;
            var bandwidthUp = 0.00;
            var bandwidthDown = 0.00;

            _.each(CS.Service.config["EVC_PVC_dep_0"].relatedProducts, function (index, value) { 

                bandwidthUp = CS.getAttributeValue(index.reference+":EVC_Available_Bandwidth_Up_0");
                bandwidthDown = CS.getAttributeValue(index.reference+":EVC_Available_Bandwidth_Down_0");

                if(bandwidthUp > 102400 && bandwidthDown > 102400){
                    numOfSFP++;
                }
            });

            return numOfSFP;
        }

        function resetWarningMessages(attrRef){
            
            if(attrRef == priceItemAttributes.evc2.lookupAttributeParent){
                var hasEmpty = findIfMissingVal(priceItemAttributes.evc2.relProductAttribute,priceItemAttributes.evc2.lookupAttributeChild,'Overbooking_Type_0', 'Voip');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.evc2.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.evc2.relProductAttribute].relatedProducts.length-1;
                var lookupAttr = withoutIndex(priceItemAttributes.evc2.relProductAttribute)+index+':'+priceItemAttributes.evc2.lookupAttributeChild;
                var errorRef = priceItemAttributes.evc2.errorFlag;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                */
            }
            else if (attrRef == priceItemAttributes.evc1.lookupAttributeParent){
                var hasEmpty = findIfMissingVal(priceItemAttributes.evc1.relProductAttribute,priceItemAttributes.evc1.lookupAttributeChild,'Overbooking_Type_0', 'Data');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.evc1.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.evc1.relProductAttribute].relatedProducts.length-1;
                var lookupAttr = withoutIndex(priceItemAttributes.evc1.relProductAttribute)+index+':'+priceItemAttributes.evc1.lookupAttributeChild;
                var errorRef = priceItemAttributes.evc1.errorFlag;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                */
            }
            else if (attrRef == priceItemAttributes.access.lookupAttributeParent){
                 var hasEmpty = findIfMissingVal(priceItemAttributes.access.relProductAttribute,priceItemAttributes.access.lookupAttributeChild,'Overbooking_Type_0', 'Access');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.access.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.access.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.access.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.access.relProductAttribute)+index+':'+priceItemAttributes.access.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
             */   
            }
            else if (attrRef == priceItemAttributes.cpeSla.lookupAttributeParent){
                var hasEmpty = findIfMissingVal(priceItemAttributes.cpeSla.relProductAttribute,priceItemAttributes.cpeSla.lookupAttributeChild,'Overbooking_Type_0', 'CPE SLA');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.cpeSla.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.cpeSla.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.cpeSla.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.cpeSla.relProductAttribute)+index+':'+priceItemAttributes.cpeSla.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                
                */
            }
            //--postGoLIve - one net and one fixed
            else if (attrRef == priceItemAttributes.evcOneNet.lookupAttributeParent){
                var hasEmpty = findIfMissingVal(priceItemAttributes.evcOneNet.relProductAttribute,priceItemAttributes.evcOneNet.lookupAttributeChild,'Overbooking_Type_0', 'OneNet');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.evcOneNet.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                
            }
            else if (attrRef == priceItemAttributes.evcOneFixed.lookupAttributeParent){
                var hasEmpty = findIfMissingVal(priceItemAttributes.evcOneFixed.relProductAttribute,priceItemAttributes.evcOneFixed.lookupAttributeChild,'Overbooking_Type_0', 'OneFixed');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.evcOneFixed.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                
            }
            //CPE SPLIT
            else if ((attrRef == priceItemAttributes.cpe.lookupAttributeParent)||(attrRef == 'CPE_Lookup_Split_0')){
                
                var hasEmpty = findIfMissingVal(priceItemAttributes.cpe.relProductAttribute,priceItemAttributes.cpe.lookupAttributeChild,'Overbooking_Type_0', 'CPE');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.cpe.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.cpe.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.cpe.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.cpe.relProductAttribute)+index+':'+priceItemAttributes.cpe.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
             */   
            }
            else if (attrRef == priceItemAttributes.hardwareToeslagh.lookupAttributeParent){
                var hasEmpty = findIfMissingVal(priceItemAttributes.hardwareToeslagh.relProductAttribute,priceItemAttributes.hardwareToeslagh.lookupAttributeChild,'Overbooking_Type_0', 'Hardware Toeslagh');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.hardwareToeslagh.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.hardwareToeslagh.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.hardwareToeslagh.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.hardwareToeslagh.relProductAttribute)+index+':'+priceItemAttributes.hardwareToeslagh.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                */
            }
            else if (attrRef == priceItemAttributes.wirelessPricePlan.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.wirelessPricePlan.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.wirelessPricePlan.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.wirelessPricePlan.relProductAttribute)+index+':'+priceItemAttributes.wirelessPricePlan.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
            }
            else if (attrRef == priceItemAttributes.wirelessInstallation.lookupAttributeParent){
                
                var hasEmpty = findIfMissingVal(priceItemAttributes.wirelessInstallation.relProductAttribute,priceItemAttributes.wirelessInstallation.lookupAttributeChild,'Overbooking_Type_0', 'Wireless Installation Fee');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.wirelessInstallation.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                
                /*
                var index = CS.Service.config[priceItemAttributes.wirelessInstallation.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.wirelessInstallation.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.wirelessInstallation.relProductAttribute)+index+':'+priceItemAttributes.wirelessInstallation.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
             */   
            }
            else if (attrRef == priceItemAttributes.wirelessOnly.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.wirelessOnly.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.wirelessOnly.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.wirelessOnly.relProductAttribute)+index+':'+priceItemAttributes.wirelessOnly.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                
                 //if checkbox unset and error there -> remove error
                if((CS.getAttributeValue('Wireless_backup_0') == false) &&(CS.getAttributeValue(errorRef) != '')&&(CS.Service.config[priceItemAttributes.wirelessOnly.relProductAttribute].relatedProducts.length == 0)){
                    CS.setAttributeValue(errorRef, constants.unset);
                }
            }
            else if (attrRef == priceItemAttributes.wirelessPricePlanInternet.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.wirelessPricePlanInternet.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.wirelessPricePlanInternet.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.wirelessPricePlanInternet.relProductAttribute)+index+':'+priceItemAttributes.wirelessPricePlanInternet.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                
                 //if checkbox unset and error there -> remove error
                if((CS.getAttributeValue('Wireless_backup_internet_0') == false) &&(CS.getAttributeValue(errorRef) != '')&&(CS.Service.config[priceItemAttributes.wirelessPricePlanInternet.relProductAttribute].relatedProducts.length == 0)){
                    CS.setAttributeValue(errorRef, constants.unset);
                }
            }
            
            else if (attrRef == priceItemAttributes.voorloop.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.voorloop.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.voorloop.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.voorloop.relProductAttribute)+index+':'+priceItemAttributes.voorloop.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                
                //if checkbox unset and error there -> remove error
                if((CS.getAttributeValue('Voorloop_Wireless_0') == false) &&(CS.getAttributeValue(errorRef) != '')&&(CS.Service.config[priceItemAttributes.voorloop.relProductAttribute].relatedProducts.length == 0)){
                    CS.setAttributeValue(errorRef, constants.unset);
                }
            }
            
            
            else if (attrRef == priceItemAttributes.voorloopInternet.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.voorloopInternet.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.voorloopInternet.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.voorloopInternet.relProductAttribute)+index+':'+priceItemAttributes.voorloopInternet.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                
                 //if checkbox unset and error there -> remove error
                if((CS.getAttributeValue('Voorloop_Wireless_internet_0') == false) &&(CS.getAttributeValue(errorRef) != '')&&(CS.Service.config[priceItemAttributes.voorloopInternet.relProductAttribute].relatedProducts.length == 0)){
                    CS.setAttributeValue(errorRef, constants.unset);
                }
            }
            
            
            
            else if (attrRef == priceItemAttributes.managedInternetEVC.lookupAttributeParent){
                
                var hasEmpty = findIfMissingVal(priceItemAttributes.managedInternetEVC.relProductAttribute,priceItemAttributes.managedInternetEVC.lookupAttributeChild,'Overbooking_Type_0', 'Internet');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.managedInternetEVC.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                
                /*
                var index = CS.Service.config[priceItemAttributes.managedInternetEVC.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.managedInternetEVC.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.managedInternetEVC.relProductAttribute)+index+':'+priceItemAttributes.managedInternetEVC.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                */
            }
            else if (attrRef == priceItemAttributes.transportEVC.lookupAttributeParent){
                
                 var hasEmpty = findIfMissingVal(priceItemAttributes.transportEVC.relProductAttribute,priceItemAttributes.transportEVC.lookupAttributeChild,'Overbooking_Type_0', 'Transport EVC');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.transportEVC.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                
                /*
                var index = CS.Service.config[priceItemAttributes.transportEVC.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.transportEVC.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.transportEVC.relProductAttribute)+index+':'+priceItemAttributes.transportEVC.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
            */    
            }
            else if (attrRef == priceItemAttributes.managedInternetEVCVLAN.lookupAttributeParent){
                
                var hasEmpty = findIfMissingVal(priceItemAttributes.managedInternetEVCVLAN.relProductAttribute,priceItemAttributes.managedInternetEVCVLAN.lookupAttributeChild,'Overbooking_Type_0', 'Transport Internet');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.managedInternetEVCVLAN.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                
                /*
                var index = CS.Service.config[priceItemAttributes.managedInternetEVCVLAN.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.managedInternetEVCVLAN.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.managedInternetEVCVLAN.relProductAttribute)+index+':'+priceItemAttributes.managedInternetEVCVLAN.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
            */    
            }   
            else if (attrRef == priceItemAttributes.evc1Vlan.lookupAttributeParent){
                var hasEmpty = findIfMissingVal(priceItemAttributes.evc1Vlan.relProductAttribute,priceItemAttributes.evc1Vlan.lookupAttributeChild,'Overbooking_Type_0', 'Transport Data');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.evc1Vlan.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.evc1Vlan.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.evc1Vlan.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.evc1Vlan.relProductAttribute)+index+':'+priceItemAttributes.evc1Vlan.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                */
            }
           else if (attrRef == priceItemAttributes.evc2Vlan.lookupAttributeParent){
               var hasEmpty = findIfMissingVal(priceItemAttributes.evc2Vlan.relProductAttribute,priceItemAttributes.evc2Vlan.lookupAttributeChild,'Overbooking_Type_0', 'Transport Voip');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.evc2Vlan.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.evc2Vlan.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.evc2Vlan.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.evc2Vlan.relProductAttribute)+index+':'+priceItemAttributes.evc2Vlan.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
            */    
            }
            else if (attrRef == priceItemAttributes.evcOneNet.lookupAttributeParent){
                
                var hasEmpty = findIfMissingVal(priceItemAttributes.evcOneNet.relProductAttribute,priceItemAttributes.evcOneFixed.lookupAttributeChild,'Overbooking_Type_0', 'OneNet');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.evcOneNet.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
            }
            else if (attrRef == priceItemAttributes.evcOneFixed.lookupAttributeParent){
                
                var hasEmpty = findIfMissingVal(priceItemAttributes.evcOneFixed.relProductAttribute,priceItemAttributes.evcOneFixed.lookupAttributeChild,'Overbooking_Type_0', 'OneFixed');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.evcOneFixed.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                
                /*
                var index = CS.Service.config[priceItemAttributes.evcOneFixed.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.evcOneFixed.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.evcOneFixed.relProductAttribute)+index+':'+priceItemAttributes.evcOneFixed.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                */
            }
            else if (attrRef == priceItemAttributes.sipTrunk.lookupAttributeParent){
                var hasEmpty = findIfMissingVal(priceItemAttributes.sipTrunk.relProductAttribute,priceItemAttributes.sipTrunk.lookupAttributeChild,'Overbooking_Type_0', 'SIP Trunk');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.sipTrunk.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.sipTrunk.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.sipTrunk.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.sipTrunk.relProductAttribute)+index+':'+priceItemAttributes.sipTrunk.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                */
            }
             else if (attrRef == priceItemAttributes.sipTrunkOneNet.lookupAttributeParent){
                var hasEmpty = findIfMissingVal(priceItemAttributes.sipTrunkOneNet.relProductAttribute,priceItemAttributes.sipTrunkOneNet.lookupAttributeChild,'Overbooking_Type_0', 'SIP Trunk');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.sipTrunkOneNet.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                
            }
            else if (attrRef == priceItemAttributes.oneFixedIAD.lookupAttributeParent){
                 var hasEmpty = findIfMissingVal(priceItemAttributes.oneFixedIAD.relProductAttribute,priceItemAttributes.oneFixedIAD.lookupAttributeChild,'Overbooking_Type_0', 'IAD');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.oneFixedIAD.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                /*
                var index = CS.Service.config[priceItemAttributes.oneFixedIAD.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.oneFixedIAD.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.oneFixedIAD.relProductAttribute)+index+':'+priceItemAttributes.oneFixedIAD.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                */
            }
            else if (attrRef == priceItemAttributes.oneFixedCUBE.lookupAttributeParent){
                
                 var hasEmpty = findIfMissingVal(priceItemAttributes.oneFixedCUBE.relProductAttribute,priceItemAttributes.oneFixedCUBE.lookupAttributeChild,'Overbooking_Type_0', 'CUBE');
                
                if(!hasEmpty){
                    var errorRef = priceItemAttributes.oneFixedCUBE.errorFlag;
                    CS.setAttributeValue(errorRef, constants.unset);
                }
                
                /*
                var index = CS.Service.config[priceItemAttributes.oneFixedCUBE.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.oneFixedCUBE.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.oneFixedCUBE.relProductAttribute)+index+':'+priceItemAttributes.oneFixedCUBE.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
                */
            }
            else if (attrRef == priceItemAttributes.fixedFlatFee.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.fixedFlatFee.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.fixedFlatFee.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.fixedFlatFee.relProductAttribute)+index+':'+priceItemAttributes.fixedFlatFee.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
            }
            else if (attrRef == priceItemAttributes.row.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.row.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.row.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.row.relProductAttribute)+index+':'+priceItemAttributes.row.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
            }
            else if (attrRef == priceItemAttributes.payu.lookupAttributeParent){
                var index = CS.Service.config[priceItemAttributes.payu.relProductAttribute].relatedProducts.length-1;
                var errorRef = priceItemAttributes.payu.errorFlag;
                var lookupAttr = withoutIndex(priceItemAttributes.payu.relProductAttribute)+index+':'+priceItemAttributes.payu.lookupAttributeChild;
                if(CS.Service.config[lookupAttr]){
                    var lookupVal = CS.getAttributeValue(lookupAttr);
                    if(lookupVal!=''){
                        CS.setAttributeValue(errorRef, constants.unset);
                    }
                }
            }
        }
        
        function withoutIndex(attrRef){
            return attrRef.substring(0, attrRef.length-1);
            
        }
        
        function triggerWarningMessages(attrRef){
            
           if((CS.Service.config[attrRef]) && CS.getAttributeValue(attrRef)!=''){
               var errorMessage = CS.getAttributeValue(attrRef);
               CS.markConfigurationInvalid(errorMessage);
           }
        }
        
        function checkIfRelProductAdded(attrRef, errorMessage){
            //findDataVoipEVC();
           if((CS.Service.config[attrRef]) && (CS.Service.config[attrRef].relatedProducts.length==0)){
               CS.markConfigurationInvalid(errorMessage);
           }
        }
        
        
        function dataOrVoip(attrLookup){
            var dataVoipResult ="";
            if(attrLookup.reference == 'EVCPVC_1_0'){
                dataVoipResult = 'Data';
                 
            }
            if(attrLookup.reference == 'EVCPVC_2_0'){
               dataVoipResult = 'Voip';
            }
            return dataVoipResult;
        }
        
        function setDataOrVoipError(typeEVC){
            
            if(typeEVC == 'Data'){
                CS.setAttributeValue('EVC1_error_0', 'Please select Data EVC');
            }
            if(typeEVC == 'Voip'){
                CS.setAttributeValue('EVC2_error_0', 'Please select Voip EVC');
            }
        }
        function resetDataOrVoipError(typeEVC){
            
            if(typeEVC == 'Data'){
                CS.setAttributeValue('EVC1_error_0', '');
            }
            if(typeEVC == 'Voip'){
                CS.setAttributeValue('EVC2_error_0', '');
            }
        }
        
        function findDataVoipEVC(){
            //find data or voip
            var index = CS.Service.config["EVC_PVC_dep_0"].relatedProducts.length;
            for (var i=0; i<index; i++){
                var lookupValueSet = CS.getAttributeValue('EVC_PVC_dep_'+i+':EVC_PVC_0');
                var evcTypeSet = CS.getAttributeValue('EVC_PVC_dep_'+i+':Overbooking_Type_0');
                if(lookupValueSet ==''){
                    setDataOrVoipError(evcTypeSet);
                }
               
                if(lookupValueSet !=''){
                    resetDataOrVoipError(evcTypeSet);
                }
            }
        }
        //end2
        
        
        function findIfMissingVal(relItem,childLookup,refAttribute,refVal){
            //find data or voip
            var lookupEmpty = false;
            
            var hasAddedItem = false;
            var index = CS.Service.config[relItem].relatedProducts.length;
            
            if(index == 0){
                return false;
            }
            else{
                for (var i=0; i<index; i++){
                    var indexUnderscore = relItem.lastIndexOf('_');
                    var firstPart = relItem.substring(0,indexUnderscore)+'_';
                    var lookupValueSet = CS.getAttributeValue(firstPart+i+':' +childLookup);
                    var refAttributeVal = CS.getAttributeValue(firstPart+i+':' +refAttribute); //Internet/Data/Overbooking
                    
                    if(refAttributeVal == refVal){
                        hasAddedItem = true;
                    }
                    
                    if(lookupValueSet == '' && refAttributeVal == refVal){
                        lookupEmpty = true;
                    }
                    
                    
                }
                
                if(!hasAddedItem){
                    return true;
                }
                else{
                    return lookupEmpty;
                }
            }
        }
        
        
        
        function hideNewAdditionButton(btnRef){
                jQuery("button[data-cs-ref='"+btnRef+"']").hide();
        }
        
        function hideAllAdditionButtons(){
            setTimeout(function(){
            hideNewAdditionButton(priceItemAttributes.access.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.cpe.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.evc1.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.cpeSla.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.wirelessInstallation.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.voorloop.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.hardwareToeslagh.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.wirelessOnly.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.sfp.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.sipTrunk.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.bundles.relProductAttribute);
            hideNewAdditionButton(priceItemAttributes.redundancy.relProductAttribute);
            },0);
        }


/********** end VF change****        
            
            
            
        /**************************/
               
        return {
            initializeWidget:                   initializeWidget,
            addDefaultAddOns:                   addDefaultAddOns,
            insertAddOn:                        insertAddOn,
            createRelatedProducts:              createRelatedProducts,
            displayPricingWidget:               displayPricingWidget,
            findAddOnRelatedProductAttributes:  findAddOnRelatedProductAttributes,
            findLastInputElementValue:          findLastInputElementValue,
            changeAddOn:                        changeAddOn,
            deleteAddOn:                        deleteAddOn,
            deselectOtherAddOns:                deselectOtherAddOns,
            bulkSave:                           bulkSave,
            bulkDelete:                         bulkDelete,
            
            //VF change
            hideAllAdditionButtons:hideAllAdditionButtons,
            hideNewAdditionButton:hideNewAdditionButton,
            resetWarningMessages:resetWarningMessages,
            checkIfRelProductAdded:checkIfRelProductAdded,
            triggerWarningMessages:triggerWarningMessages,
            getAddOnAssociationsListForAttrMoreThanOne:getAddOnAssociationsListForAttrMoreThanOne,
            getAddOnAssociationsListForAttr:getAddOnAssociationsListForAttr,
            doLookupQuery:doLookupQuery,
            setLookupAttributeIfOne:            setLookupAttributeIfOne,
            initAfterUpdateNew:                 initAfterUpdateNew,
            initAfterUpdateAccessInfra:         initAfterUpdateAccessInfra
            //end VF change
        };
    }

    CS.EAPI = CS.EAPI || {};
    _.extend(CS.EAPI, buildCompatibilityFunctions());
    
    /* 28.07.2020 - Commented out to remove widget sidebar
    // load side panel
    var html = ''
        + '<div id="mySidenav" class="sidenav">'
        + '  <a href="javascript:void(0)" id="mySideNavCloseBtn" class="closebtn" onclick="closeNav()">Finish</a>'
        + '  <div id="tabsContainer1">'
        + '      <button class="btn" onclick="CS.EAPI.bulkSave()">Add Selected</button>'
        + '      <button class="btn" id="bulkDeleteButton" onclick="CS.EAPI.bulkDelete()">Delete Selected</button>'
        + '      <div id="addOnTableContainer" class="tableContainer">'
        + '        <div class="pmWidget dropdown"></div>'
        + '      </div>'
        + '      </div>'
        + '  <div id="widgetLoadingOverlay" style="position: absolute; width:100%; height:100%; font-size: 50px; color: white; background-color:rgba(66,190,215,0.3); z-index:1000;">'
        + '     <div id="widgetLoadingImage" style="text-align: center; position: relative;">'
        + '         <img src="https://www.dreamlines.fr/wp-content/themes/dreamlines/assets/images/icons/loading.gif"/>'
        + '    </div>'
        + '  </div>'
        + '</div>';

    if (window.location.href.match(/ScreenFlowName/)) {
        //Do Nothing since its InFlight configuration - may be planning details section
    }
    else {
        html = html + '<span class="navButton" style="height:65px;" onclick="openNav()">&#9776; AddOns</span>';
        jQuery('#screensList').after(html);
    }
    */

});