if (!CS || !CS.SM) {
	throw Error('Solution Console Api not loaded!');
}

// Register Business Internet Solution plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', registerBusinessInternet);
}

async function registerBusinessInternet() {
	const businessInternetPlugin = await CS.SM.registerPlugin(
		SC_SCHEMA_NAME.BUSINESSINTERNET_SOLUTION
	);
	updateBusinessInternetPlugin(businessInternetPlugin);
	return;
}

function updateBusinessInternetPlugin(Plugin) {
	Plugin.beforeSave = beforeSolutionSave;
	Plugin.afterSave = afterBusinessInternetSolutionSave;
	Plugin.afterAttributeUpdated = afterBusinessInternetAttributeUpdated;
	Plugin.beforeAddonDataInitialized = beforeAddOnDataInitialized;
	Plugin.afterConfigurationAdd = afterBusinessInternetConfigurationAdd;
}

async function afterBusinessInternetConfigurationAdd(component, configuration) {
	switch (component.name) {
		case SC_COMPONENTS.INTERNET_SOLUTION:
			return afterInternetConfigurationAdded(component, configuration);
		default:
	}
	return true;
}

/**
 * @description Perform logic required after Solution is saved
 */
async function afterBusinessInternetSolutionSave(
	solutionResult,
	configurationsProcessed,
	saveOnlyAttachment,
	configurationGuids
) {
	const solution = solutionResult.solution;
	if (!solution) {
		return;
	}
	return true;
}

async function afterBusinessInternetAttributeUpdated(
	component,
	configuration,
	attribute,
	oldValueMap
) {
	switch (component.name) {
		case SC_COMPONENTS.BUSINESSINTERNET_COMPONENT:
			return afterBusinessInternetSolutionAttributeUpdated(attribute);

		case SC_COMPONENTS.INTERNET_SOLUTION:
			return afterInternetAttributeUpdated(component, configuration, attribute, oldValueMap);
	}
	return true;
}

async function afterBusinessInternetSolutionAttributeUpdated(attribute) {
	if (attribute.name === SC_COMMON_ATTRIBUTES.TODAY) {
		return Promise.all([
			propagateAttributeToComponent(
				SC_COMPONENTS.ACCESS_COMPONENT,
				attribute.name,
				attribute.value
			)
		]);
	}

	if (attribute.name === SC_COMMON_ATTRIBUTES.CONTRACT_TERM_BI) {
		return Promise.all([
			propagateAttributeToComponent(
				SC_COMPONENTS.INTERNET_SOLUTION,
				SC_COMMON_ATTRIBUTES.CONTRACT_DURATION,
				attribute.value
			)
		]);
	}
}

/**
 * @description Initialization logic
 */
async function initBusinessInternetSolutionDefaults(solution) {
	if (!solution) {
		solution = await CS.SM.getActiveSolution();
	}
	const businessInternetSolutionComponent = solution.getComponentByName(
		SC_SOLUTIONS.BUSINESSINTERNET_SOLUTION
	);
	if (!businessInternetSolutionComponent) {
		return;
	}

	return Promise.all([
		setCurrentSystemDate(solution),
		updateFromBasket(solution, [SC_COMMON_ATTRIBUTES.ACCOUNT_ID])
	]);
}
