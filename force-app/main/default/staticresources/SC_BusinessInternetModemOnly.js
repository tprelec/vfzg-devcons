// SC_BusinessInternetModemOnly
if (!CS || !CS.SM) {
	throw Error('Solution Console Api not loaded!');
}

// Register Business Internet Solution plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener('SolutionConsoleReady', registerBusinessInternetModemOnly);
}

async function registerBusinessInternetModemOnly() {
	const businessInternetModemOnlyPlugin = await CS.SM.registerPlugin(SC_SCHEMA_NAME.BUSINESSINTERNETMODEMONLY_SOLUTION);
	updateBusinessInternetModemOnlyPlugin(businessInternetModemOnlyPlugin);
	return;
}

let checkSolutionNeededBIMO;

function updateBusinessInternetModemOnlyPlugin(Plugin) {
	Plugin.beforeSave = beforeSolutionSave;
	Plugin.afterSave = afterBusinessInternetModemOnlySolutionSave;
	Plugin.afterAttributeUpdated = afterBusinessInternetModemOnlyAttributeUpdated;
	Plugin.beforeAddonDataInitialized = beforeAddOnDataInitialized;
	Plugin.beforeConfigurationDelete = beforeBusinessInternetModemOnlyConfigurationDelete;
	Plugin.beforeConfigurationAdd = beforeBimoConfigurationAdd;
	Plugin.afterConfigurationAdd = afterBusinessInternetModemOnlyConfigurationAdd;
	Plugin.afterConfigurationDelete = afterBusinessInternetModemOnlyConfigurationDelete;
	Plugin.afterConfigurationAddedToMacBasket = afterConfigurationAddedToMacBasket;
	Plugin.afterConfigurationClone = afterConfigurationClone;
	Plugin.beforeRelatedProductAdd = beforeBimoRelatedProductAdd;
	Plugin.beforeRelatedProductDelete = beforeBimoRelatedProductDelete;
}

async function beforeBimoConfigurationAdd(component, configuration) {
	if (isBasketInflight()) {
		CS.SM.displayMessage('Cannot add access configuration to Inflight basket', 'warning');
		return false;
	}

	return true;
}

async function beforeBimoRelatedProductAdd(component, configuration, relatedProduct) {
	console.log('Before add related product');
	if (canAddOrDeleteInflightConfiguration(configuration, SC_ACTION.ADD)) {
		return canAddOrDeleteMacdConfiguration(configuration, SC_ACTION.ADD);
	}
	return false;
}

async function beforeBimoRelatedProductDelete(component, configuration, relatedProduct) {
	console.log('Before delete related product');
	if (canAddOrDeleteInflightConfiguration(configuration, SC_ACTION.DELETE)) {
		return canAddOrDeleteMacdConfiguration(configuration, SC_ACTION.DELETE);
	}
	return false;
}

async function afterConfigurationClone(component, configurations, copies) {
	console.log(JSON.stringify(configurations));
	if (component.name == SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT) {
		afterAccessModemOnlyCloned(configurations);
	}
}

async function afterConfigurationAddedToMacBasket(component, configurationGuid) {
	// await incrementMacdContractNumber(configurationGuid);
	await getBasketInfo();
	await makeInflightConfigsReadonly();
}

async function afterBusinessInternetModemOnlyConfigurationAdd(component, configuration) {
	switch (component.name) {
		case SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT:
			return afterAccessModemOnlyConfigurationAdded(component, configuration);
		default:
	}
	return true;
}

async function afterBusinessInternetModemOnlyConfigurationDelete(component, configuration) {
	switch (component.name) {
		case SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT:
			return afterAccessModemOnlyConfigurationDeleted(component, configuration);

		default:
	}
	return true;
}
async function afterBusinessInternetModemOnlyAttributeUpdated(component, configuration, attribute, oldValueMap) {
	switch (component.name) {
		case SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT:
			return afterAccessModemOnlyAttributeUpdated(component, configuration, attribute, oldValueMap);

		case SC_COMPONENTS.INTERNETMODEMONLY_COMPONENT:
			return afterInternetModemOnlyAttributeUpdated(component, configuration, attribute, oldValueMap);
		default:
	}
	return true;
}
async function beforeBusinessInternetModemOnlyAttributeUpdated(component, configuration, attribute, oldValueMap) {
	switch (component.name) {
		case SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT:
			return beforeAccessModemOnlyAttributeUpdated(component, configuration, attribute, oldValueMap);

		case SC_COMPONENTS.INTERNETMODEMONLY_COMPONENT:
			return beforeInternetModemOnlyAttributeUpdated(component, configuration, attribute, oldValueMap);
		default:
	}
	return true;
}

async function beforeBusinessInternetModemOnlyConfigurationDelete(component, configuration) {
	switch (component.name) {
		case SC_COMPONENTS.INTERNETMODEMONLY_COMPONENT:
			return beforeInternetModemOnlyConfigurationDelete(component, configuration);
		case SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT:
			return beforeAccessModemOnlyConfigurationDelete(component, configuration);

		default:
	}
	if (canAddOrDeleteInflightConfiguration(configuration, SC_ACTION.DELETE)) {
		if (isBasketInflight() && component.name == SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT) {
			// if we are about to delete Access configuration from the Inflight basket, first we have to make sure that associated Internet configuration is in the basket
			// so it can be deleted as well
			return beforeAccessModemOnlyConfigurationDeleted(component, configuration);
		}
		return canAddOrDeleteMacdConfiguration(configuration, SC_ACTION.DELETE);
	}
	return false;
}

async function beforeAccessDeleteInflightBasket(configuration) {
	if (isBasketInflight()) {
	}
}

/**
 * @description Perform logic required after Solution is saved
 */
async function afterBusinessInternetModemOnlySolutionSave(solutionResult, configurationsProcessed, saveOnlyAttachment, configurationGuids) {
	const solution = solutionResult.solution;

	if (!solution) {
		return;
	}
	return Promise.all([calculateListPrice(), applyAutomaticDiscount()]);
}

// to create diff for deployment
async function afterBimoSolutionLoaded(solution) {
	if (solution) {
		if (solution.changeType == '') {
			var configs = await solution.getConfigurations();
			//console.log('configs ' + JSON.stringify(configs));
			if (Object.keys(configs).length == 1) {
				console.log('Checking unique solution contraint');
				var canAddSolutionResult = await canAddSolution(solution.basketId, SC_SOLUTIONS.BUSINESSINTERNETMODEMONLY_SOLUTION);
				if (!canAddSolutionResult) {
					let errorMsgTxt = 'Cannot add new ' + solution.name + ' solution to current account basket!';
					CS.SM.displayMessage(errorMsgTxt, 'error');
					configs[0].updateStatus({ status: false, message: errorMsgTxt });
				}
			}
		}
	}
}

/**
 * @description Initialization logic
 */
async function initBusinessInternetModemOnlySolutionDefaults(solution) {
	if (!solution) {
		solution = await CS.SM.getActiveSolution();
	}

	const businessInternetModemOnlySolutionComponent = solution.getComponentByName(SC_SOLUTIONS.BUSINESSINTERNETMODEMONLY_SOLUTION);
	if (!businessInternetModemOnlySolutionComponent) {
		return;
	}

	return Promise.all([
		setCurrentSystemDate(solution),
		readOnlyAtribbuteIfIsMacBasket(SC_COMPONENTS.ACCESSMODEMONLY_COMPONENT, 'contractduration'),
		updateFromBasket(solution, [SC_COMMON_ATTRIBUTES.ACCOUNT_ID]),
		afterBimoSolutionLoaded(solution),
		getServiceInfo()
	]);
}

/**
 * @description Function will set the field to read-only in the MACD scenario if it is part of the MAC basket (as defined through the isFromMacBasket property).
 */
async function readOnlyAtribbuteIfIsMacBasket(componentName, attributeName) {
	let activeSolution = await CS.SM.getActiveSolution();

	const isMacdSolution = activeSolution.isMACD;
	if (isMacdSolution) {
		const components = activeSolution.components;
		Object.values(components).forEach((component) => {
			if (component.name === componentName) {
				const configurations = component.getConfigurations();
				Object.values(configurations).forEach((configuration) => {
					if (configuration.isFromMacBasket) {
						const updateData = { readOnly: true };
						configuration = configuration.updateAttribute(attributeName, updateData);
					}
				});
			}
		});
	}
}
