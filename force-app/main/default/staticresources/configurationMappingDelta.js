function subscribe() {
	// adds all untracked attributes that map to configuration field that have a different value compared to the output field to delta
	CS.EventHandler.subscribe(CS.EventHandler.Event.BEFORE_FINISH_ACTION, () => {
		Object.values(CS.Service.config)
			.filter(CS.SVC.isAttributeWrapper)
			.filter((wrapper) => {
				return (
					wrapper['definition'].hasOwnProperty('cscfga__configuration_output_mapping__c') &&
					CS.Service.config[CS.Util.getParentReference(wrapper.reference)].config[
						wrapper['definition']['cscfga__configuration_output_mapping__c']
					] != wrapper.attr['cscfga__Value__c']
				);
			})
			.filter((wrapper) => !wrapper.hasOwnProperty('deltaTracking'))
			.forEach((wrapper) => CS.SVC.PersistUtilities.addAttributeToDelta(wrapper.reference, CS.Service.config));
	});
}

function subscribeWhenReady() {
	if (typeof CS !== 'undefined' && CS.EventHandler !== undefined) {
		subscribe();
	} else {
		setTimeout(subscribeWhenReady, 1000);
	}
}

subscribeWhenReady();
