console.log("Loaded Gen EVC plugin");

SC_MessageBroker.store.subscribe('root:ContractTerm', async (data) => {
    console.log('root:ContractTerms...');
	await propagateAttributeToComponent("Gen EVC", data.attribute.name, data.attribute.value);
});

const scgetevc = Object.freeze({

	"afterGenFirstAttributeUpdated": async function afterGenFirstAttributeUpdated(component, configuration, attribute, oldValueMap) {
		
		if (component.name === 'Gen EVC' && attribute.name === 'technology' && attribute.value !== oldValueMap.value) {
			await setAttribute(component, configuration, 'quantity', 100);
		}
	
		return true;
	}
});
