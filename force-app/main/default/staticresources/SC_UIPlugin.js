if (!CS || !CS.SM) {
	throw Error("Solution Console Api not loaded!");
}

// Register UI plugin
if (CS.SM.registerPlugin) {
	window.document.addEventListener("SolutionConsoleReady", registerUIPlugin);
}

async function registerUIPlugin() {
	if (CS.SM.registerUIPlugin) {
		// CS.SM.UIPlugin.afterImport = afterImport; // component=>Promise.resolve(true)
		// CS.SM.UIPlugin.afterNavigate = afterNavigate; // (currentComponent,previousComponent)=>Promise.resolve(true)
		// CS.SM.UIPlugin.afterOEExport = afterOEExport; // component=>Promise.resolve(true)
		// CS.SM.UIPlugin.afterOEImport = afterOEImport; // component=>Promise.resolve(true)
		// CS.SM.UIPlugin.afterOESplit = afterOESplit; // (component,configurationGuid,OEConfigurationGuids)=> {â€¦}
		// CS.SM.UIPlugin.afterQuantityUpdated = afterQuantityUpdated; // (component,configurationGuid,oldQuantity,newQuantity)=> {â€¦}
		// CS.SM.UIPlugin.alterAlertMessage = alterAlertMessage; // (message,type)=>Promise.resolve({message,type})
		// CS.SM.UIPlugin.beforeImport = beforeImport; // (component,fileData)=>Promise.resolve(fileData)
		// CS.SM.UIPlugin.beforeNavigate = beforeNavigate; // (nextComponent,currentComponent)=> {â€¦}
		// CS.SM.UIPlugin.beforeOEExport = beforeOEExport; // component=>Promise.resolve(true)
		// CS.SM.UIPlugin.beforeOEImport = beforeOEImport; // (component,fileData)=>Promise.resolve(fileData)
		// CS.SM.UIPlugin.beforeOESplit = beforeOESplit; // (component,configurationGuid,OEConfigurationGuids)=> {â€¦}
		// CS.SM.UIPlugin.beforeQuantityUpdated = beforeQuantityUpdated; // (component,configurationGuid,oldQuantity,newQuantity)=> {â€¦}
		CS.SM.UIPlugin.buttonClickHandler = buttonClickHandler; // async settings=> {â€¦}
		// CS.SM.UIPlugin.buttonIframeClosed = buttonIframeClosed; // (dialogResult,settings)=>Promise.resolve(true)
		// CS.SM.UIPlugin.onCustomAttributeClose = onCustomAttributeClose; // async(solutionName,componentName,configurationGuid,attributeName)=> {â€¦}
		// CS.SM.UIPlugin.onCustomAttributeEdit = onCustomAttributeEdit; // async Æ’ onCustomAttributeEdit(solutionName, componentName, configurationGuid, attributeName)
		// CS.SM.UIPlugin.onCustomAttributeFormat = onCustomAttributeFormat; // async Æ’ onCustomAttributeFormat(solutionName, componentName, configurationGuid, attributeName)
		// CS.SM.UIPlugin.setupNetworkMapData = setupNetworkMapData; // solution=>Promise.resolve(true)

		CS.SM.registerUIPlugin();
	}
}

async function buttonClickHandler(buttonSettings) {
	// {
	// 	configurationGuid: "d1a319fc-094b-15e1-25b2-88f21792daac"
	// 	context: "Configuration"
	// 	displayType: "component"
	// 	id: "D232F43G5532F77U5774"
	// 	label: "Generate Infrastructure"
	// 	methodName: "generateInfrastructure"
	// 	type: "action"
	// }
	if (buttonSettings.id === "D232F43G5532F77U5774") {
		await generateInfrastructure(buttonSettings.configurationGuid);
	} else if (buttonSettings.id === "backToBasketInternal") {
		return Promise.resolve("/" + buttonSettings.basketId);
	}

	return Promise.resolve(true);
}
