//console.log('***** SC_BMZakelijkeToestelbetaling.js'); //SC_BMZakelijkeToestelbetaling.js - part of Business Mobile Solution

/**
 * @description Implementing the afterConfigurationAdd hook for the Zakelijke Toestelbetaling component
 */
async function afterConfigurationAddedZakelijkeToestelbetaling(component, configuration) {
	let valueFromMainComponent = configuration.attributes.contracttermmain.value;
	let contractTermDevice = parseInt(valueFromMainComponent) == 1 || parseInt(valueFromMainComponent) == 36 ? '24' : valueFromMainComponent;
	return Promise.all([setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.CONTRACT_TERM_D, contractTermDevice)]);
}

/**
 * @description Implementing the afterAttributeUpdated hook for the Mobile Device component
 */
async function afterAttributeUpdatedZakelijkeToestelbetalingeComponent(component, configuration, attribute, oldValueMap) {
	switch (attribute.name) {
		case SC_BUSSINESS_MOBILE_ATTRIBUTES.CONTRACT_TERM_D:
			Promise.all([
				setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, ''),
				setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.TOESTELBETALING_VARIANT, '')
			]);

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT:
			await renameToestelbetaling(component, configuration);
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.BRAND:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.STORAGE:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.COLOR_D:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.SIM_TYPE:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.NETWORK_TYPE:
			await setAttribute(component, configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT, '');
			break;

		case SC_BUSSINESS_MOBILE_ATTRIBUTES.TOESTELBETALING_VARIANT:
			await renameToestelbetaling(component, configuration);
			break;

		default:
			break;
	}
	return true;
}

/**
 * @description Setting configuration name
 */
async function renameToestelbetaling(component, configuration) {
	let productVariantName = getAttributeValue(configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.PRODUCT_VARIANT_NAME);
	let toestelbetalingName = getAttributeValue(configuration, SC_BUSSINESS_MOBILE_ATTRIBUTES.TOESTELBETALING_NAME);
	let name;

	if (productVariantName) {
		name = toestelbetalingName + ', ' + productVariantName;

		const attributeData = [
			{
				name: SC_BUSSINESS_MOBILE_ATTRIBUTES.TOESTELBETALING_CONFIGURATION_NAME,
				value: name
			}
		];
		return component.updateConfigurationAttribute(configuration.guid, attributeData);
	}
	return true;
}
