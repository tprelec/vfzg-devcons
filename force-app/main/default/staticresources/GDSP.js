var errorMessages = {
	"atLeastOneTariff": "Please add at least one Data Tariff.",
	"maxFiveTariffs": "A maximum of 5 data tariffs is allowed.",
	"addSimType": "Please add a SIM type.",
	"maxOneSimType": "A maximum of 1 SIM type is allowed.",
	"simCommitmentWholeNumbers": "Please only enter whole numbers as Commitment of new SIMs, and enter a value for each year.",
	"simCommitmentZero": "Commitment of new SIMs cannot be 0.",
	"proceedToNextPage": "Please proceed to the next page",
	"onlyOneExtraOption": "A maximum of 1 Extra Option is allowed."
}

// CUSTOMER COMMITMENT ATTRIBUTE FUNCTIONS
function initializeGDSPCustomScreen() {
	removeRedundantComponents();
	initializeCustomerCommitment();
	initializeFootprint();
}

function removeRedundantComponents() {
	jQuery('[data-cs-screen-section-ref="Footprint-0"]').parent().remove();
}

// Initial function that builds the table and populates the fields
function initializeCustomerCommitment() {
	renderCustomerCommitmentTable();
	populateCustomerCommitmentsFromJSON();
	calculateTotalCustomerCommitment();
	console.log("******NO PROBLEMS COMMITMENT");
}

// Builds the initial table that's later populated and appended to the configuration screen
function renderCustomerCommitmentTable() {
	console.log("******RENDERCUSTOMERCOMMTIMENT");
	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;

	if (jQuery('#customerCommitment')[0] !== undefined) {
		jQuery('#customerCommitment')[0].remove();
	}
	var numberOfYears = parseInt(CS.getAttributeValue('Duration_0')) / 12;
	var divElement = document.createElement('div');
	divElement.setAttribute('id', 'customerCommitment');

	var customerCommitmentTable = document.createElement('table');
	customerCommitmentTable.setAttribute('class', 'list');
	customerCommitmentTable.setAttribute('id', 'customerCommitmentTable');

	var confirmButton = createButton('createCustomerCommitmentJSONEntry()', 'Confirm', null);

	var headerTr = document.createElement('tr');
	headerTr.setAttribute('class', 'headerRow');
	var headerTh1 = document.createElement('th');
	headerTh1.setAttribute('class', 'firstColumn');
	var headerTh2 = document.createElement('th');
	var label1 = createLabel('Year');
	var label2 = createLabel('Minimum volume commitment');

	headerTh1.appendChild(label1);
	headerTh2.appendChild(label2);

	headerTh1.rowSpan = "2";
	headerTh2.colSpan = "" + numberOfTariffs;
	headerTh2.style = "text-align:center";
	
	headerTr.appendChild(headerTh1);
	headerTr.appendChild(headerTh2);

	headerTr2 = document.createElement('tr');
	headerTr2.setAttribute('class', 'headerRow2');

	for (var i = 0; i < numberOfTariffs; i++) {
		var num = i+1;
		var labelString = 'Tariff ' + num;
		var labelHeader = createLabel(labelString);
		var thTariff = document.createElement('th');
		thTariff.appendChild(labelHeader);
		headerTr2.appendChild(thTariff);
	}

	customerCommitmentTable.appendChild(headerTr);
	customerCommitmentTable.appendChild(headerTr2);

	for (var i = 1; i <= numberOfYears; i++) {
		var tempTr = document.createElement('tr');

		var tempTd1 = document.createElement('td');
		var tempLabel = createLabel('Year ' + i);
		tempTd1.appendChild(tempLabel);
		
		tempTr.appendChild(tempTd1);

		for(var j = 0; j < numberOfTariffs; j++) {
			num = j+1;
			var tariffTd = document.createElement('td');
			var inputLabel1 = 'customerCommitmentYear' + i + 'Tariff' + num;
			var inputLabel2 = 'customerCommitmentByYear' + 'Tariff' + num;
			var tempInput = createInputElement(inputLabel1, inputLabel2, false);
			tariffTd.appendChild(tempInput);
			tempTr.appendChild(tariffTd);
		}

		customerCommitmentTable.appendChild(tempTr);
	}

	var trTotal = document.createElement('tr');

	var tdTotal1 = document.createElement('td');
	var labelTotal = createLabel('Total');
	tdTotal1.appendChild(labelTotal);
	
	
	trTotal.appendChild(tdTotal1);
	for(var i = 0; i < numberOfTariffs; i++) {
		num = i+1;
		var inputTotalLabel = 'customerCommitmentTotalTariff'+num;
		var tdTotal2 = document.createElement('td');
		var inputTotal = createInputElement(inputTotalLabel, inputTotalLabel, true);

		tdTotal2.appendChild(inputTotal);
		trTotal.appendChild(tdTotal2);
	}

	customerCommitmentTable.appendChild(trTotal);

	divElement.appendChild(customerCommitmentTable);
	divElement.appendChild(confirmButton);

	jQuery('[data-cs-binding="Customer_Commitment_0"]').html(null).append(divElement);
}

// Function that calculates the totals and populates the Total field
function calculateTotalCustomerCommitment() {
	console.log("******CALCULATETOTALCUSTOMERCOMMITMENT");
	var numberOfYears = parseInt(CS.getAttributeValue('Duration_0')) / 12;
	var commitmentsJSON = CS.getAttributeValue('Customer_Commitment_0');
	var totalCommitmentByTariff = {};
	var totalCommitment = 0;
	if (commitmentsJSON) commitmentsJSON = JSON.parse(commitmentsJSON);

	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;

	if (commitmentsJSON.length > 0) {
		commitmentsJSON.forEach(function (element) {
			var year = element.year;
			for ( var i = 0; i <numberOfTariffs; i++) {
				var num = i +1;
				var keyTariff = 'tariff' + num;
				if (num in totalCommitmentByTariff) {						
					totalCommitmentByTariff[num] += element["tariffs"][keyTariff] || 0;
				} else {
					totalCommitmentByTariff[num] = element["tariffs"][keyTariff] || 0;
				}
			}
		}) 
	}


	for(var i = 0; i < numberOfTariffs; i++) {
		var num = i +1;
		keyLabel =  'customerCommitmentTotalTariff' + num;
		if (document.getElementById(keyLabel)) {
			document.getElementById(keyLabel).value = totalCommitmentByTariff[num] || 0;
		}

		if( num in totalCommitmentByTariff) {
			var label = "Data_Tariffs_" + i + ":Quantity_0";
			CS.setAttributeValue(label, totalCommitmentByTariff[num]);
			totalCommitment += totalCommitmentByTariff[num];
		}

	}

	var numberOfSimTypes = CS.Service.config["SIM_Types_0"].relatedProducts.length;

	if (numberOfSimTypes != 0) {
		CS.setAttributeValue("SIM_Types_0:Quantity_0", totalCommitment);
	}

}

function resetCommitmentJSON(){
	console.log("******RESETCOMMITMENTJSON");
	var numberOfYears = parseInt(CS.getAttributeValue('Duration_0')) / 12;
	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;

	var commitmentsJSON = [];

	for (var i = 0; i < numberOfYears; i++) {
		var yearValue = i + 1;
		var yearDict = {};
		var tariffs = {};

		for (var j=0; j < numberOfTariffs; j++) {
			var num = j+1;
			var keyTariff = "tariff" + num;
			value = 0;
			tariffs[keyTariff] = value;
		}

		yearDict["year"] = yearValue;
		yearDict["tariffs"] = tariffs;
		commitmentsJSON.push(yearDict);

	}


	CS.setAttributeValue('Customer_Commitment_0', JSON.stringify(commitmentsJSON));
	calculateTotalCustomerCommitment();

	return commitmentsJSON;
}

// Populates the table using a JSON that's fetched from the Customer Commitment attribute on the GDSP Product Configuration
function populateCustomerCommitmentsFromJSON() {
	console.log("******POPULATECUSTOMERCOMMITMENT");

	var numberOfYears = parseInt(CS.getAttributeValue('Duration_0')) / 12;
	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;

	var commitmentsJSON = CS.getAttributeValue('Customer_Commitment_0');
	if (commitmentsJSON) commitmentsJSON = JSON.parse(commitmentsJSON);

	if ( Object.keys(commitmentsJSON).length != 0){
		var keyName = Object.keys(commitmentsJSON)[0];
		var firstValue = commitmentsJSON[keyName];
		var tariffs = firstValue["tariffs"];
		if (Object.keys(tariffs).length > numberOfTariffs) {
			commitmentJSON = resetCommitmentJSON();
		}

	}

	if(commitmentsJSON.length > numberOfYears) {
		commitmentJSON = resetCommitmentJSON();
	}

	if (commitmentsJSON.length > 0) {
		commitmentsJSON.forEach(function (element) {
			var year = element.year;
			for (var i = 0; i <numberOfTariffs; i++) {
				var num = i +1;
				var keyTariff = 'tariff' + num;
				var tariffElementId = 'customerCommitmentYear' + year + 'Tariff' + num;
				if (document.getElementById(tariffElementId)) {
					if (element["tariffs"][keyTariff] === undefined) {
						document.getElementById(tariffElementId).value = 0;
					} else {
						document.getElementById(tariffElementId).value = element["tariffs"][keyTariff] || 0;
					}
				}
			}
		}) 
	}
}

// Creates a JSON that's pushed to the Customer Commitment attribute on the GDSP Product Configuration
function createCustomerCommitmentJSONEntry() {
	console.log("******CREATECUSTOMERCOMMITMENTJSON");

	var numberOfYears = parseInt(CS.getAttributeValue('Duration_0')) / 12;

	
	calculateTotalCustomerCommitment();
	

	var commitmentsByYear = document.getElementsByName("customerCommitmentByYear");
	var commitmentsJSON = [];

	for (var i = 0; i < numberOfYears; i++) {
		var commitmentByYearJSON = getCommitmentByYear(i);
		commitmentsJSON.push(commitmentByYearJSON);
	}

	var previousJSON = CS.getAttributeValue('Customer_Commitment_0');
	CS.setAttributeValue('Customer_Commitment_0', JSON.stringify(commitmentsJSON));

}

// Creates dictionary for specific year in customer commitment table
function getCommitmentByYear(year) {
	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;
	var yearValue = year + 1;
	var yearDict = {};
	var tariffs = {};

	for (var i=0; i < numberOfTariffs; i++) {
		var num = i+1;
		var keyTariff = "tariff" + num;
		var tariffElementId = 'customerCommitmentYear' + yearValue + 'Tariff' + num;
		var value = 0;
		if(document.getElementById(tariffElementId)) {
			value = parseInt(document.getElementById(tariffElementId).value);
		}
		tariffs[keyTariff] = value;
	}

	yearDict["year"] = yearValue;
	yearDict["tariffs"] = tariffs;

	return yearDict;
}

// FOOTPRINT ATTRIBUTE FUNCTIONS

// Initial function that builds the table and populates the fields
function initializeFootprint() {
	renderFootprintTable();
	populateFootprintDataFromJSON();
	footprintConfirm();
	console.log("******NO PROBLEMS FOOTPRINT");
}

// Builds the initial table that's later populated and appended to the configuration screen
function renderFootprintTable() {

	console.log("******RENDERFOOTPRINTTALBE");

	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;

	if (jQuery('#footprint')[0] !== undefined) {
		jQuery('#footprint')[0].remove();
	}
	var div = document.createElement('div');
	div.setAttribute('id', 'footprint');

	var table = document.createElement('table');
	table.setAttribute('class', 'list');
	table.setAttribute('id', 'footprintTable');

	var tbody = document.createElement('tbody');
	tbody.setAttribute('id', 'footprintTableBody')

	var tr1 = document.createElement('tr');
	tr1.setAttribute('class', 'headerRow');

	var th11 = document.createElement('th');
	th11.setAttribute('class', 'firstColumn');
	var label11 = createLabel('Tier');

	var confirmButton = createButton('footprintConfirm()', 'Confirm', null);


	th11.appendChild(label11);

	tr1.appendChild(th11);

	for (var i=0; i < numberOfTariffs; i++) {
		var num = i+1;
		var labelString = 'Tariff ' + num + ' (%)';
		var label = createLabel(labelString);
		var th = document.createElement('th');
		th.appendChild(label);
		tr1.appendChild(th);
	}


	tbody.appendChild(tr1);

	var row1 = createRowForNetworkUsage('Vodafone Netherlands & Vodafone tier 1', numberOfTariffs);
	var row2 = createRowForNetworkUsage('European networks & Vodafone tier 2', numberOfTariffs);
	var row3 = createRowForNetworkUsage('Preferred Partner networks outside Europe', numberOfTariffs);
	var row4 = createRowForNetworkUsage('RoWTier1', numberOfTariffs);
	var row5 = createRowForNetworkUsage('RoWTier2', numberOfTariffs);
	var row6 = createRowForNetworkUsage('RoWTier3', numberOfTariffs);
	var row7 = createRowForNetworkUsage('RoWTier5', numberOfTariffs);

	tbody.appendChild(row1);
	tbody.appendChild(row2);
	tbody.appendChild(row3);
	tbody.appendChild(row4);
	tbody.appendChild(row5);
	tbody.appendChild(row6);
	tbody.appendChild(row7);


	table.appendChild(tbody);



	div.appendChild(table);
	div.appendChild(confirmButton);

	jQuery('[data-cs-binding="Footprint_0"]').html(null).append(div);
}



// Populates the table using a JSON that's fetched from the Footprint attribute on the GDSP Product Configuration
function populateFootprintDataFromJSON() {
	console.log("******POPULATEFOOTPRINTDATAFROMJSON");

	var footprintJSON = CS.getAttributeValue('Footprint_0');
	if (footprintJSON) footprintJSON = JSON.parse(footprintJSON);
	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;

	if (Object.keys(footprintJSON).length != 0) {
		var keyName = Object.keys(footprintJSON)[0];
		var firstValue = footprintJSON[keyName];
		var tariffs = firstValue["tariffs"];
		if (Object.keys(tariffs).length > numberOfTariffs) {
			footprintJSON = resetFootprintJSON(footprintJSON);
		}
	}

	if (footprintJSON.length > 0) {
		footprintJSON.forEach(function (element) {
			var footprintTierLabel = getFootprintTierLabel(element.tier);
			for (var i = 0; i < numberOfTariffs; i++) {
				var num = i+1;
				var keyTariff = "tariff" + num;
				var keyLabel= "footprint_" + footprintTierLabel + "_" + num;
				if (document.getElementById(keyLabel)) {
					if (element["tariffs"][keyTariff] === undefined) {
						document.getElementById(keyLabel).value = 0;
					} else {
						document.getElementById(keyLabel).value = element["tariffs"][keyTariff] || 0;
					}
				}
			}
		});
	}

}

function resetFootprintJSON(original) {
	console.log("******RESETFOOTPRINTJSON");

	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;

	newJSON = [];

	if (original.length > 0) {
		original.forEach(function (element) {
			var footprintTierName = element.tier;
			var tierDict = {};
			var tariffs = {};
			for (var i = 0; i < numberOfTariffs; i++) {
				var num = i+1;
				var keyTariff = "tariff" + num;
				var value = 0;
				tariffs[keyTariff] = value;
			}
			tierDict["tier"] = footprintTierName;
			tierDict["tariffs"] = tariffs;
			newJSON.push(tierDict);
		});

		CS.setAttributeValue('Footprint_0', JSON.stringify(newJSON));
		return newJSON;
	}
	return original;
}


// Function for adding a custom country to the table
function footprintAddCountry() {
	var footprintTableBody = document.getElementById("footprintTableBody");
	footprintTableBody.appendChild(addFootprintTableRow(null));
}

// Function that calculates the total Data Allowance, performs validations and pushes the data to the Footprint attribute on the GDSP Product Configuration
function footprintConfirm() {
	CS.UI.clearWarningMessages();
	console.log("******FOOTPRINTCONFIRM");

	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;

	var footprintJSON = [];
	var footprintNL = getFootprintByTier("Vodafone Netherlands & Vodafone tier 1", numberOfTariffs);
	var footprintEU = getFootprintByTier("European networks & Vodafone tier 2", numberOfTariffs);
	var footprintPP = getFootprintByTier("Preferred Partner networks outside Europe", numberOfTariffs);
	var footprintRT1 = getFootprintByTier("RoWTier1", numberOfTariffs);
	var footprintRT2 = getFootprintByTier("RoWTier2", numberOfTariffs);
	var footprintRT3 = getFootprintByTier("RoWTier3", numberOfTariffs);
	var footprintRT5 = getFootprintByTier("RoWTier5", numberOfTariffs);

	
	footprintJSON.push(footprintNL);
	footprintJSON.push(footprintEU);
	footprintJSON.push(footprintPP);
	footprintJSON.push(footprintRT1);
	footprintJSON.push(footprintRT2);
	footprintJSON.push(footprintRT3);
	footprintJSON.push(footprintRT5);

	totalPercentages = {};

	for (var i=0; i<footprintJSON.length; i++) {
		var singleTier =  footprintJSON[i];
		for (var j = 0; j < numberOfTariffs; j++) {
			var num  =  j+1;
			var keyName = "tariff" + num;
			var value = singleTier.tariffs[keyName];
			if ( j in totalPercentages ) {
				totalPercentages[j] += value;
			} else {
				totalPercentages[j] = value;
			}
		}
	}

	var thereIsError = false;

	for (var i=0; i < numberOfTariffs; i++) {
		var num = i + 1;
		var tariffName = "Tariff " + num;

		if (totalPercentages[i] != 100) {
			var errorMessage = 'Data Allowance Percentage for ' + tariffName + ' must be 100%!';
			CS.markConfigurationInvalid(errorMessage);
			thereIsError = true;
		}
		
	}


	if (thereIsError === false) {
		CS.setAttributeValue('Footprint_0', JSON.stringify(footprintJSON));
		CS.UI.clearWarningMessages();
	}

}

// Creates dictionary for specific tier in footprint table
function getFootprintByTier(footprintTierName, numberOfTariffs) {
	var footprintTierLabel = getFootprintTierLabel(footprintTierName);
	var tierDict = {};
	var tariffs = {};

	for (var i=0; i<numberOfTariffs; i++) {
		var num = i+1;
		var keyTariff = "tariff" + num;
		var keyLabel= "footprint_" + footprintTierLabel + "_" + num;
		var value = 0;
		if (document.getElementById(keyLabel)) {
			value = parseInt(document.getElementById(keyLabel).value) || 0;
		}
		tariffs[keyTariff] = value;
	}

	tierDict["tier"] = footprintTierName;

	tierDict["tariffs"] = tariffs;
	return tierDict;
}

// Placeholder function for creating a correctly-formatted entry for the JSON that's later pushed to the Footprint attribute on the GDSP Product Configuration
function createFootprintJSONEntry(id, country, percentage) {
	var tempArray = {
		"id": id,
		"country": country,
		"percentage": parseInt(percentage)
	};

	return tempArray;
}

// Function that adds a custom country when clicking "Add country"
function addFootprintTableRow(countryData) {
	var numberOfCustomCountries = document.getElementsByName("footprintAllowanceCustom").length;

	var tableRow = document.createElement('tr');
	var id;

	var td1 = document.createElement('td');
	var td2 = document.createElement('td');

	var input1 = createInputElement(null, 'footprintCountryName', false);
	var input2 = createInputElement(null, 'footprintAllowanceCustom', false);

	if (countryData) {
		id = countryData.id;
		input1.setAttribute('id', id + 'Name');
		input1.value = countryData.country;
		input2.setAttribute('id', id);
		input2.value = countryData.percentage;
	} else {
		id = 'footprintCustom' + (numberOfCustomCountries + 1)
		input1.setAttribute('id', id + 'Name');
		input2.setAttribute('id', id);
	}

	tableRow.appendChild(td1);
	tableRow.appendChild(td2);

	td1.appendChild(input1);
	td2.appendChild(input2);

	td2.appendChild(createButton('removeFootprintTableRow("' + id + '")', 'Remove', 'removeFootprint' + (numberOfCustomCountries + 1)));

	return tableRow;
}

// Function that removes a custom country 
function removeFootprintTableRow(id) {

	var entryValues = document.getElementsByName("footprintAllowanceCustom");

	var elementToDelete = document.getElementById(id);
	var flag = false;

	entryValues.forEach(function (element, index) {
		if (flag) {
			document.getElementById('footprintCustom' + (index + 1)).setAttribute('id', 'footprintCustom' + index);
			document.getElementById('footprintCustom' + (index + 1) + 'Name').setAttribute('id', 'footprintCustom' + index + 'Name');
			var button = document.getElementById('removeFootprint' + (index + 1));
			button.setAttribute('id', 'removeFootprint' + index);
			button.setAttribute('onclick', 'removeFootprintTableRow("footprintCustom' + index + '")');
		}
		if (elementToDelete == element) {
			flag = true;
		}
	});


	elementToDelete.parentElement.parentElement.remove();

}

// HTML ELEMENT BUILDERS

function createInputElement(id, name, disabled) {
	var input = document.createElement('input');
	input.setAttribute('type', 'text');
	input.setAttribute('step', 'any');
	input.setAttribute('class', 'slds-input');
	if (id) input.setAttribute('id', id);
	if (name) input.setAttribute('name', name);
	if (disabled) input.setAttribute('disabled', '');
	input.setAttribute('style', 'width: 50%');


	return input;
}

function createButton(onClickFunction, text, id) {
	var button = document.createElement('button');
	button.setAttribute('onclick', onClickFunction);
	button.textContent = text;
	if (id) button.setAttribute('id', id);
	return button;
}

function createLabel(content) {
	var label = document.createElement('label');
	label.textContent = content;
	return label;
}

// Creates label for footprint table in order to get the elements stored in  that tiers row
function getFootprintTierLabel(tierName) {
	var nameField = tierName.split(" ");
	var labelName = '';
	for (var i = 0; i < nameField.length; i++) {
		if ( i != nameField.length -1) {
			labelName = labelName + nameField[i] + '_';
		} else {
			labelName = labelName + nameField[i];
		}
	}

	return labelName;
}

// creates table rows for tiers in footprint table
function createRowForNetworkUsage(tierName, numberOfTariffs){
	var labelName = getFootprintTierLabel(tierName);

	var tr =  document.createElement('tr');

	var tdName = document.createElement('td');
	var label = createLabel(tierName);

	tdName.appendChild(label);
	tr.appendChild(tdName);

	for (var i = 0; i < numberOfTariffs; i++) {
		var num = i+1;
		var td =  document.createElement('td');
		var id = 'footprint_' + labelName + '_' + num;
		var name = 'footPrint_Tariff_' + i+1;
		var input =  createInputElement(id, name, false);
		td.appendChild(input);
		tr.appendChild(td);
	}

	return tr;

}

function relatedProductQuantityCheck() {
	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;
	var numberOfSimTypes = CS.Service.config["SIM_Types_0"].relatedProducts.length;
	var numberOfExtraOptions = CS.Service.config["Extra_Options_0"].relatedProducts.length;
	
	if (numberOfTariffs == 0) {
		CS.markConfigurationInvalid(errorMessages.atLeastOneTariff);
	} else if (numberOfTariffs > 5) {
		CS.markConfigurationInvalid(errorMessages.maxFiveTariffs);
	}
	
	if (numberOfSimTypes == 0) {
		CS.markConfigurationInvalid(errorMessages.addSimType);
	} else if (numberOfSimTypes > 1) {
		CS.markConfigurationInvalid(errorMessages.maxOneSimType);
	}
	
	if (numberOfExtraOptions > 1) {
		CS.markConfigurationInvalid(errorMessages.onlyOneExtraOption);
	}
}

function setNetworkPreferences() {
	var numberOfTariffs = CS.Service.config["Data_Tariffs_0"].relatedProducts.length;

	for (var i = 0; i < numberOfTariffs; i++) {
		var vodafoneTier1 = CS.getAttributeValue('Data_Tariffs_' + i + ':Vodafone_tier_1_0');
		var vodafoneTier2 = CS.getAttributeValue('Data_Tariffs_' + i + ':Vodafone_tier_2_0');
		var preferredPartners = CS.getAttributeValue('Data_Tariffs_' + i + ':Preferred_Partner_networks_outside_Europe_0');
		var RoWTier1 = CS.getAttributeValue('Data_Tariffs_' + i + ':RoWTier1_0');
		var RoWTier2 = CS.getAttributeValue('Data_Tariffs_' + i + ':RoWTier2_0');
		var RoWTier3 = CS.getAttributeValue('Data_Tariffs_' + i + ':RoWTier3_0');
		var RoWTier5 = CS.getAttributeValue('Data_Tariffs_' + i + ':RoWTier5_0');
		
		var networkPreferences = {
			"vodafoneTier1": vodafoneTier1,
			"vodafoneTier2": vodafoneTier2,
			"preferredPartners": preferredPartners,
			"RoWTier1": RoWTier1,
			"RoWTier2": RoWTier2,
			"RoWTier3": RoWTier3,
			"RoWTier5": RoWTier5,
		}
		
		if (CS.getAttributeValue('Data_Tariffs_' + i + ':Network_Preferences_0') != JSON.stringify(networkPreferences)) {
			CS.setAttributeValue('Data_Tariffs_' + i + ':Network_Preferences_0', JSON.stringify(networkPreferences));
		}
	}
}

function relatedProductInlineEdit() {
	_.each(CS.Service.config["SIM_Types_0"].relatedProducts, function (index, value) {
		if (!relatedProductsIds.includes('SIM_Types_' + value + ':Price_item_0')) {
			relatedProductsIds.push('SIM_Types_' + value + ':Price_item_0');
		}
	});
	_.each(CS.Service.config["Extra_Options_0"].relatedProducts, function (index, value) {
		if (!relatedProductsIds.includes('Extra_Options_' + value + ':Price_item_0')) {
			relatedProductsIds.push('Extra_Options_' + value + ':Price_item_0');
		}
	});
	for (var i = 0; i < relatedProductsIds.length; i++) {
		var x = document.getElementById(relatedProductsIds[i]);
		var el = jQuery(x).parent().find('[data-cs-select-list-lookup="true"]');
		var attrRef = el.attr('data-cs-binding');

		if (attrRef !== undefined) {
			CS.InlineEdit.initSll(relatedProductsIds[i]);
		}
	}
}

function removeActionButtonsGDSP() {
	var editButtons = jQuery('[data-cs-action="editRelatedProduct"]');
	var copyButtons = jQuery('[data-cs-action="copyRelatedProduct"]');

	var relevantRef = [
		'SIM_Types',
		'Extra_Options'
	];

	for (var i = 0; i < editButtons.size(); i++) {

		if (editButtons[i].innerHTML != 'Edit') continue;

		var ref = editButtons[i].attributes["data-cs-ref"].value;
		relevantRef.forEach(x => {
			if (ref.includes(x)) {
				editButtons[i].remove();
			}
		});

		if (ref.includes('Platform_fee')) {
		editButtons[i].parentElement.innerHTML = '-';
		}
	}

	for (var i = 0; i < copyButtons.size(); i++) {
		if (copyButtons[i].innerHTML != 'Copy') continue;
		var ref = copyButtons[i].attributes["data-cs-ref"].value;
		relevantRef.forEach(x => {
			if (ref.includes(x)) {
				copyButtons[i].remove();
			}
		});
	}

	jQuery("button[data-cs-ref='Platform_fee_0']").remove();
	removeRelatedProductButtons(['Platform_fee_'],['All']);
}

function addPlatformFee() {
	checkPGA('Platform_fee_0','Platform_fee_set_0','Platform_fee_lookup_0');
}