<!--
	@author Rahul Sharma
	@description custom component that listen's to toast event from LWC and shows on Visualforce when page is part of lightning out
-->
<apex:component>
	<apex:slds />

	<style>
		#message-placeholder {
			position: absolute;
			top: 1rem;
			left: 1rem;
			right: 1rem;
		}
	</style>

	<div id="loading-container" class="slds-align_absolute-center slds-m-around_medium">
		<img src="/img/loading.gif" />
		<span class="slds-m-left_xx-small slds-text-heading_medium">Loading...</span>
	</div>

	<script>
		function stopInitialLoadingMessage() {
			const loadingContainer = document.querySelector('#loading-container');
			if (loadingContainer) {
				loadingContainer.style.display = 'none';
			}
		}

		document.addEventListener('lightning__showtoast', function (event) {
			const data = event.toastAttributes;
			showToastMessage(data);
		});

		function removeToastMessage() {
			event.srcElement.closest('.c-vf-toast-container').remove();
		}

		function showToastMessage(data) {
			const { message, type, mode, duration } = data;
			const toastMessageElement = _formattedToastMessageElement(message, type);
			_removeDismisableToastAfterTimeout(toastMessageElement, mode, duration);
			document.querySelector('#message-placeholder').appendChild(toastMessageElement);
		}

		function _formattedToastMessageElement(message, type) {
			const messageTemplate = document.getElementById('toast-template').content.cloneNode(true);
			messageTemplate.querySelector('#c-vf-message').innerHTML = message;
			messageTemplate.querySelector('#notify-body').classList.add('slds-notify', 'slds-notify_toast', `slds-theme_${type}`);
			messageTemplate
				.querySelector('#notify-icon-container')
				.classList.add('slds-m-right_small', 'slds-no-flex', 'slds-align-top', 'slds-icon_container', `slds-icon-utility-${type}`);
			const messageIcon = messageTemplate.querySelector('#c-message-icon');
			const messageIconUrl = messageIcon.getAttributeNS('http://www.w3.org/1999/xlink', 'href').split('#')[0];
			messageIcon.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', `${messageIconUrl}#${type}`);
			return messageTemplate;
		}

		function _removeDismisableToastAfterTimeout(toastMessageElement, mode, duration) {
			if (mode === 'dismissible' && duration) {
				toastMessageElement.querySelector('.c-vf-toast-container').classList.add('dismissible');
				setTimeout(() => {
					const firstDismissibleMessage = document.querySelector('.dismissible');
					if (firstDismissibleMessage) {
						firstDismissibleMessage.remove();
					}
				}, duration);
			}
		}
	</script>

	<div id="message-placeholder"></div>

	<template id="toast-template">
		<div class="c-vf-toast-container">
			<div class="slds-notify_container slds-is-relative">
				<div id="notify-body" role="status">
					<span class="slds-assistive-text">success</span>
					<span id="notify-icon-container">
						<svg class="slds-icon slds-icon_small" aria-hidden="true">
							<use
								id="c-message-icon"
								xmlns:xlink="http://www.w3.org/1999/xlink"
								xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#success')}"
							></use>
						</svg>
					</span>
					<div class="slds-notify__content">
						<h2 id="c-vf-message" class="slds-text-heading_small">Something went wrong, please contact system admin!</h2>
					</div>
					<div class="slds-notify__close">
						<button class="slds-button slds-button_icon slds-button_icon-inverse" title="Close" onclick="removeToastMessage()">
							<svg class="slds-button__icon slds-button__icon_large" aria-hidden="true">
								<use
									xmlns:xlink="http://www.w3.org/1999/xlink"
									xlink:href="{!URLFOR($Asset.SLDS, '/assets/icons/utility-sprite/svg/symbols.svg#close')}"
								></use>
							</svg>
							<span class="slds-assistive-text">Close</span>
						</button>
					</div>
				</div>
			</div>
		</div>
	</template>
</apex:component>
