<apex:component allowDML="true">
    <apex:attribute name="c" description="This is the apex controller."  type="OrderFormCustomerDataController"/>
    	<apex:actionRegion >
    <apex:pageBlock title="Customer and Contract Data - {!c.theContract.Account__r.Name}">
    	<apex:actionFunction action="{!refresh}" name="refreshFlags"/>		
       	<apex:pageBlockButtons >
            <apex:commandButton id="customerSaveButton" 
                                action="{!c.saveCustomer}" 
                                value="Save Customer/Contract changes" 
                                status="loading-status"  
                                styleclass="hide"
                                rendered="{!!c.Locked}" 
                                rerender="page-container,menu-container,orderform-container"
                                />  
       	</apex:pageBlockButtons>
       	
    	<apex:outputPanel id="customerDetailPanel">
	        <apex:pageBlock title="Account" >
		        <apex:pageBlockSection columns="2">
		            <apex:pageBlockSectionItem >
		                <apex:outputLabel for="arfo" value="Ready for Order" />	            
		                <c:OrderValidationFlag vObject="{!c.theContract.Account__r}" vOrder="{!c.theOrderWrapper.theOrder}" id="arfo" />						
					</apex:pageBlockSectionItem>

				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Bespoke" />	  
					<apex:outputField value="{!c.theContract.Opportunity__r.Bespoke__c}"  />	 
				</apex:pageBlockSectionItem>
		        </apex:pageBlockSection>
		        <apex:pageBlockSection columns="2">
		            <apex:outputField value="{!c.theContract.Account__c}" />
		            <apex:pageBlockSectionItem >
		            	<apex:outputLabel value="BAN + BOPCode"/>
		            	<apex:outputPanel >
			            	<apex:outputField title="BAN" value="{!c.theContract.Opportunity__r.Ban__r.Ban_Number__c}" />
			            	&nbsp;-&nbsp;
			            	<apex:outputField value="{!c.theContract.Opportunity__r.Ban__r.BOPCode__c}" />
		            	</apex:outputPanel>
		            </apex:pageBlockSectionItem>	
 
		            <apex:outputField value="{!c.theContract.Account__r.KVK_number__c}" />	            
		            <apex:inputField value="{!c.theContract.Account__r.Authorized_to_sign_1st__c}" rendered="{!!c.customerLocked}" id="authToSignInput" />
		            <apex:outputField value="{!c.theContract.Account__r.Authorized_to_sign_1st__c}" rendered="{!c.customerLocked}" />
					<apex:inputField value="{!c.theContract.Account__r.Phone}" rendered="{!!c.customerLocked}" id="phoneInput" />
					<apex:outputField value="{!c.theContract.Account__r.Phone}" rendered="{!c.customerLocked}" />							
				</apex:pageBlockSection>								            
				<apex:pageBlockSection columns="2">
		            <apex:inputField value="{!c.theContract.Account__r.Unify_Account_Type__c}" rendered="{!!c.customerLocked && !c.theOrderWrapper.theOrder.IsPartner__c}" id="acctTSelect" />
					<apex:outputField value="{!c.theContract.Account__r.Unify_Account_Type__c}" rendered="{!c.customerLocked || c.theOrderWrapper.theOrder.IsPartner__c}" />
		            <apex:inputField value="{!c.theContract.Account__r.Unify_Account_SubType__c}" rendered="{!!c.customerLocked && !c.theOrderWrapper.theOrder.IsPartner__c}" id="acctSTSelect"  onclick="initializeSaveButton();"/> <!-- requires an extra onclick because it is a dependent picklist-->
					<apex:outputField value="{!c.theContract.Account__r.Unify_Account_SubType__c}" rendered="{!c.customerLocked || c.theOrderWrapper.theOrder.IsPartner__c}" />	
		        </apex:pageBlockSection>
			</apex:pageBlock>

		    <apex:pageBlock title="Customer contacts">
		        <apex:pageBlockSection >
		            <apex:outputLabel > <i>Note that an <b>Email address</b> is required for making a Contact selectable</i></apex:outputLabel>
		            <apex:outputLabel value="" />
             
		            <apex:pageBlockSectionItem helpText="{!$ObjectType.Order__c.fields.Customer_Main_Contact__c.inlineHelpText}">
		                <apex:outputLabel >{!$ObjectType.Order__c.fields.Customer_Main_Contact__c.label}</apex:outputLabel>
		                <apex:outputPanel >
		                    <apex:outputField value="{!c.theOrderWrapper.customerMainContact.Contact__c}" rendered="{!c.Locked}"/>
		                    <c:OrderFormContactPicklist required="true" 
		                        rendered="{!!c.Locked}"
		                        contactSelected="{!c.mainContactSelected}" 
		                        contactRecordSelected="{!c.theOrderWrapper.customerMainContact}"
		                        theOrder="{!c.theOrderWrapper.theOrder}" 
		                        showPartnerContacts="false"/>
		                </apex:outputPanel>
		            </apex:pageBlockSectionItem>
		            <apex:pageBlockSectionItem helpText="{!$ObjectType.Order__c.fields.Customer_Maintenance_Contact__c.inlineHelpText}">
		                <apex:outputLabel >{!$ObjectType.Order__c.fields.Customer_Maintenance_Contact__c.label}</apex:outputLabel>
		                <apex:outputPanel >
		                    <apex:outputField value="{!c.theOrderWrapper.customerMaintenanceContact.Contact__c}" rendered="{!c.Locked}"/>                
		                    <c:OrderFormContactPicklist required="false" 
		                        rendered="{!!c.Locked}" 
		                        contactSelected="{!c.maintenanceContactSelected}" 
		                        contactRecordSelected="{!c.theOrderWrapper.customerMaintenanceContact}"
		                        theOrder="{!c.theOrderWrapper.theOrder}" 
		                        showPartnerContacts="true"/>
		                </apex:outputPanel>
		            </apex:pageBlockSectionItem>
		            <apex:pageBlockSectionItem rendered="{!!c.Locked}" helpText="{!$ObjectType.Order__c.fields.Customer_Incident_Contact__c.inlineHelpText}">
		                <apex:outputLabel >{!$ObjectType.Order__c.fields.Customer_Incident_Contact__c.label}</apex:outputLabel>
		                <apex:outputPanel >
		                    <apex:outputField value="{!c.theOrderWrapper.customerIncidentContact.Contact__c}" rendered="{!c.Locked}"/>                
		                    <c:OrderFormContactPicklist required="false" 
		                        rendered="{!!c.Locked}" 
		                        contactSelected="{!c.incidentContactSelected}" 
		                        contactRecordSelected="{!c.theOrderWrapper.customerIncidentContact}"
		                        theOrder="{!c.theOrderWrapper.theOrder}"
		                        showPartnerContacts="true"/>
		                </apex:outputPanel>
		            </apex:pageBlockSectionItem>
		            <apex:pageBlockSectionItem rendered="{!!c.Locked}" helpText="{!$ObjectType.Order__c.fields.Customer_Incident_Contact__c.inlineHelpText}">
		                <apex:outputLabel >
		                	<c:CustomHelptextBubble label="Chooser Contact" helptext="{!$Label.HELPTEXT_Choose_Contact}" />

		                </apex:outputLabel>
		                <apex:outputPanel >
		                    <apex:outputField value="{!c.theOrderWrapper.customerChooserContact.Contact__c}" rendered="{!c.Locked}"/>                
		                    <c:OrderFormContactPicklist required="false" 
		                        rendered="{!!c.Locked}" 
		                        contactSelected="{!c.chooserContactSelected}" 
		                        contactRecordSelected="{!c.theOrderWrapper.customerChooserContact}"
		                        theOrder="{!c.theOrderWrapper.theOrder}" 
		                        showPartnerContacts="true"/>
		                </apex:outputPanel>
		            </apex:pageBlockSectionItem>            
		        </apex:pageBlockSection>
		    </apex:pageBlock>

			<apex:pageBlock title="Contract" >
		        <apex:pageBlockSection columns="1">
		            <apex:pageBlockSectionItem >
		                <apex:outputLabel for="crfo" value="Ready for Order" />
		                <c:OrderValidationFlag vObject="{!c.theContract}" vOrder="{!c.theOrderWrapper.theOrder}" id="crfo"/>
		            </apex:pageBlockSectionItem>
		        </apex:pageBlockSection>
		        <apex:pageBlockSection columns="2">
		            <apex:pageBlockSectionItem >
		                <apex:outputLabel value="Contract" />
		                <apex:outputLink value="/{!c.theContract.id}" >
		                    <apex:outputText value="{!c.theContract.Name}"/>  
		                </apex:outputLink>                      
		            </apex:pageBlockSectionItem>            
					<apex:pageBlockSectionItem >
		                <apex:outputLabel for="acctmgr" value="Opportunity Owner" />
		                <apex:outputPanel >
		            		<apex:outputField value="{!c.theContract.Opportunity__r.OwnerId}" id="acctmgr" />&nbsp;
		            		<c:OrderValidationFlag vObject="{!c.theContract.Opportunity__r.Owner}" vOrder="{!c.theOrderWrapper.theOrder}" />
		            	</apex:outputPanel>
		            </apex:pageBlockSectionItem>	 		           		            	
		            <apex:inputField value="{!c.theContract.Contract_Sign_Date__c}" rendered="{!!c.Locked}" id="signDateInput" />
		            <apex:outputField value="{!c.theContract.Contract_Sign_Date__c}" rendered="{!c.Locked}" />
		            <apex:inputField value="{!c.theContract.Signed_by_Customer__c}" rendered="{!!c.Locked}" id="signedByInput" />
		            <apex:outputField value="{!c.theContract.Signed_by_Customer__c}" rendered="{!c.Locked}" />	
 	                <apex:inputField value="{!c.theContract.Apple_DEP_ID__c}" rendered="{!c.theContract.Count_Apple_DEP__c != 0}" id="appleDepInput"  onclick="initializeSaveButton();"/>
					<apex:inputField value="{!c.theContract.Document_Type__c}"  id="docTypeInput"/> 			
					<apex:inputField value="{!c.theContract.Opportunity__r.Ziggo_Order_Reference__c}"  rendered="{!c.theOrderWrapper.theOrder.orderType__r.name=='Ziggo'}" id="zorInput"/>
					<apex:inputField value="{!c.theContract.Document_Number__c}"  id="docNumberInput"/>
		        </apex:pageBlockSection>
	        </apex:pageBlock>
        </apex:outputPanel>
    </apex:pageBlock>

        </apex:actionRegion>	
        <c:OrderFormAttachmentManager />    
    
</apex:component>