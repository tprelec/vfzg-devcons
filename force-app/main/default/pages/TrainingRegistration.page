<apex:page controller="TrainingRegistrationController"
           extensions="PP_LightningController"
           title="My SFDC Training"
           action="{! checkAndUpdatePublicCalendars }"
           showHeader="{! NOT(isLightningCommunity) }"
           sidebar="{! NOT(isLightningCommunity) }">

    <apex:slds rendered="{! isLightningCommunity }" />
    
    <style type="text/css">
        .hasMotif {
            margin : 0px;
        }
    </style>

    <!-- Show the training chatter feed at the top of the page -->
    <chatter:feed entityId="{!trainingChatterId}" rendered="{!ChatterAccess}"/>

    <apex:form >
        <apex:pageBlock title="My SFDC Trainings" id="trainingBlock">
        <!-- Table used so that the Search filters sit on the left of the results -->
        <table width="100%" border="0">
            <tr>  
                <td width="100" valign="top">
                    <apex:pageBlock title="Search" mode="edit" id="criteria">
                        <apex:outputLabel value="Training"/>
                        <apex:inputText value="{!filterTraining}"/>                    
                        <apex:outputLabel value="Location"/>
                        <apex:inputText value="{!filterLocation}"/>
                        <apex:outputLabel value="Date"/><br/>   
                        <apex:inputField value="{!filterDate.Activation_date__c}" />
                        <!-- Put the Search button in the center so it looks nice -->
                        <!-- Note that it doesn't run any code directly - it refreshes the page -->
                        <!-- It works when clicked, and also will capture the Enter key -->
                        <apex:pageBlockSection columns="1">                                
                            <apex:pageBlockSectionItem >
                                <apex:outputPanel >
                                    <div style="text-align: center">
                                        <apex:commandButton action="{!clear}" value="Clear" id="clear" rerender="trainingBlock"/>
                                        <apex:commandButton action="{!search}" value="Search" id="search" rerender="trainingBlock"/>  
                                    </div>
                                </apex:outputPanel>
                            </apex:pageBlockSectionItem>                                   
                        </apex:pageBlockSection> 
                    </apex:pageBlock>     
                </td>
                <td valign="top">
                    <apex:pageMessages />
                    <apex:pageBlock title="Enroll for new training">
                        <!-- available trainings -->                        
                        <apex:pageBlockTable value="{!availableTrainings}" var="t" id="trainTable">

                            <apex:column headerValue="Action" styleClass="actionColumn">
                                <apex:commandLink action="{!enroll}"  value="Enroll" rerender="trainingBlock" rendered="{!OR(t.Max_Participants__c=null, t.EventRelations.size < t.Max_Participants__c)}">
                                    <apex:param name="trainingId" value="{!t.id}" assignTo="{!selectedTrainingId}"/> 
                                </apex:commandLink>
                                <apex:outputLabel value="FULL" style="color:red" rendered="{!AND(t.Max_Participants__c!=null, t.EventRelations.size >= t.Max_Participants__c)}"/>
                            </apex:column>
                            <!-- Training column with sorting functionality -->
                            <!-- the orderbyColumn parameter takes the column to be sorted and the sort method processes that -->
                            <!-- Images are applied to the column so the user can see which direction the sort is -->
                            <apex:column >
                                <apex:facet name="header"> 
                                    <apex:outputpanel > 
                                        <apex:commandlink style="text-decoration:underline" action="{!sort}" value="Training Name" rerender="trainTable"> 
                                            <apex:param name="orderByColumn" value="Subject" assignTo="{!orderByColumn}" />
                                        </apex:commandlink>
                                        <apex:image value="{!$Resource.UpArrow}" width="12" height="12" rendered="{!orderByColumn=='Subject' && SortAscending}"> </apex:image> 
                                        <apex:image value="{!$Resource.downArrow}" width="12" height="12" rendered="{!orderByColumn=='Subject' && !SortAscending}"> </apex:image>
                                    </apex:outputpanel>
                                </apex:facet>                     
                                <apex:outputLink target="_top" value="/{!t.Id}">{!t.Subject}</apex:outputLink>
                            </apex:column>   
                            <apex:column headerValue="Focus Group" value="{!t.Public_Calendar__c}" />                  
                            <apex:column value="{!t.Type}" />

                            <!-- Location column with sorting functionality -->
                            <apex:column >
                                <apex:facet name="header"> 
                                    <apex:outputpanel > 
                                        <apex:commandlink style="text-decoration:underline" action="{!sort}" value="Location" rerender="trainTable"> 
                                            <apex:param name="orderByColumn" value="Location" assignTo="{!orderByColumn}" />
                                        </apex:commandlink>
                                        <apex:image value="{!$Resource.UpArrow}" width="12" height="12" rendered="{!orderByColumn=='Location' && SortAscending}"> </apex:image> 
                                        <apex:image value="{!$Resource.downArrow}" width="12" height="12" rendered="{!orderByColumn=='Location' && !SortAscending}"> </apex:image>
                                    </apex:outputpanel>
                                </apex:facet>                     
                                <apex:outputfield value="{!t.Location}" />
                            </apex:column>  

                            <!-- Start date column with sorting functionality -->
                            <apex:column >
                                <apex:facet name="header"> 
                                    <apex:outputpanel > 
                                        <apex:commandlink style="text-decoration:underline" action="{!sort}" value="Start" rerender="trainTable"> 
                                            <apex:param name="orderByColumn" value="StartDateTime" assignTo="{!orderByColumn}" />
                                        </apex:commandlink>
                                        <apex:image value="{!$Resource.UpArrow}" width="12" height="12" rendered="{!orderByColumn=='StartDateTime' && SortAscending}"> </apex:image> 
                                        <apex:image value="{!$Resource.downArrow}" width="12" height="12" rendered="{!orderByColumn=='StartDateTime' && !SortAscending}"> </apex:image>
                                    </apex:outputpanel>
                                </apex:facet>                     
                                <apex:outputfield value="{!t.StartDateTime}" />
                            </apex:column>  

                            <apex:column value="{!t.EndDateTime}" />
                            <apex:column headerValue="Inschrijvingen" value="{!t.EventRelations.size}" />
                            <apex:column headerValue="Plaatsen" value="{!t.Max_Participants__c}" />
                        </apex:pageBlockTable>
                    </apex:pageBlock>

                </td>
            </tr>
        </table>

        <apex:pageBlock id="currentRegistrationBlock" title="Your current Registrations">
            <apex:pageBlockTable value="{!CurrentTrainingRegistrations}" var="tr">

                <apex:column headerValue="Action" styleClass="actionColumn">
                    <apex:commandLink action="{!deleteEnrollment}"  value="Cancel" rerender="trainingBlock">
                        <apex:param name="enrollmentId" value="{!tr.id}" assignTo="{!selectedEnrollmentId}"/> 
                    </apex:commandLink>
                </apex:column>
                <apex:column headerValue="Training Name">
                    <apex:outputLink target="_top" value="/{!tr.EventId}">{!tr.Event.Subject}</apex:outputLink>
                </apex:column>              

                <apex:column value="{!tr.Status}" />
                <apex:column value="{!tr.Event.Type}" />
                <apex:column value="{!tr.Event.Location}" />
                <apex:column value="{!tr.Event.StartDateTime}" />
                <apex:column value="{!tr.Event.EndDateTime}" />
            </apex:pageBlockTable>
        </apex:pageBlock> 

        </apex:pageBlock>
    </apex:form>

</apex:page>