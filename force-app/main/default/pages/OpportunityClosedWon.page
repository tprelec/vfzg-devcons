<apex:page standardController="Opportunity"
           extensions="OpportunityClosedWonController,PP_LightningController"
           showHeader="{! NOT(isLightningCommunity) }"
           sidebar="{! NOT(isLightningCommunity) }"
           lightningStylesheets="true"
           title="Close Won Opportunity">

    <apex:slds />

    <apex:outputPanel rendered="{! isLightningCommunity }">
        <script type="text/javascript">
            var PARENT_WINDOW_ORIGIN = "{! 'https://' + $Site.Domain }";
            var PAGE_NAME = "{! $CurrentPage.Name }";
        </script>
        <script type="text/javascript" src="{! $Resource.DirtyFrame_top_js }"></script>
    </apex:outputPanel>

    <apex:form styleClass="slds-scope">

        <apex:actionStatus id="status">
            <apex:facet name="start">
                <div class="slds-spinner_container slds-is-fixed">
                    <div role="status" class="slds-spinner slds-spinner_medium">
                        <span class="slds-assistive-text">Loading</span>
                        <div class="slds-spinner__dot-a"></div>
                        <div class="slds-spinner__dot-b"></div>
                    </div>
                </div>
            </apex:facet>
        </apex:actionStatus>

        <!-- Debug panel that shows the value of all the booleans. Only if the url parameter showBooleans=1 -->
        <apex:outputPanel rendered="{!showBooleans}">
            <apex:outputText value="Errorfound: {!errorFound}" /><br/>
            <apex:outputText value="showClosingBySAGMessage: {!showClosingBySAGMessage}" /><br/>
            <apex:outputText value="showMixedProductWarning: {!showMixedProductWarning}" /><br/>
            <apex:outputText value="showMissingTypeInfo: {!showMissingTypeInfo}" /><br/>
            <apex:outputText value="showClosedWonWithRedirect: {!showClosedWonWithRedirect}" /><br/>
            <apex:outputText value="showClosedWonWithEmail: {!showClosedWonWithEmail}" /><br/>
        </apex:outputPanel>
        <!-- //Debug panel -->

        <apex:outputPanel rendered="{! NOT(isLightningCommunity) }">
            <apex:detail subject="{!Opportunity.Id}" showChatter="true"/>
        </apex:outputPanel>


        <!-- Modal -->
        <section role="dialog" class="{!IF(isLightningCommunity, '', 'slds-modal slds-fade-in-open slds-modal_large')}">
            <div class="{!IF(isLightningCommunity, '', 'slds-modal__container')}" >

                <apex:outputPanel id="popup" layout="none">

                    <!-- Header -->
                    <apex:outputPanel layout="none" rendered="{!NOT(isLightningCommunity)}">
                        <header class="slds-modal__header">
                            <h2 class="slds-modal__title slds-hyphenate">Close Won Opportunity</h2>
                        </header>
                    </apex:outputPanel>
                    <!-- //Header -->

                    <!-- Content -->
                    <div class="slds-modal__content slds-p-around_medium">

                        <apex:outputPanel rendered="{! lightningAlreadyClosed }">
                            <apex:pageMessage summary="{!$Label.ERROR_Closed_Opportunity}" severity="error" strength="1" />
                        </apex:outputPanel>
                        <apex:outputPanel rendered="{! !lightningAlreadyClosed}">
                            <apex:pageMessages />
                        </apex:outputPanel>

                        <!-- Closing by SAG -->
                        <apex:outputPanel rendered="{!showClosingBySAGMessage && !errorFound}" layout="none">
                            <div class="slds-m-bottom_small"><h3 class="slds-text-heading_small">Non-standard Opportunity</h3></div>
                            <apex:pageMessage rendered="{!!OppHasZiggoProduct}" strength="1" severity="info" summary="{!$Label.LABEL_Opportunity_Closing_By_SAG}" />
                            <apex:pageMessage rendered="{!OppHasZiggoProduct}" strength="1" severity="info" summary="{!$Label.LABEL_Opportunity_Closing_By_SAG_Ziggo}" />
                            <apex:pageMessage rendered="{!oppOwnedByPartner}" strength="1" severity="warning" summary="{!$Label.LABEL_Opportunity_Closing_Site_Warning}" />
                            <apex:pageBlock rendered="{!!OppHasZiggoProduct}">
                                <apex:pageBlockSection columns="1">
                                    <apex:inputField value="{!theOpp.Solution_Sales__c}" id="solSalesInput" required="true" />
                                </apex:pageBlockSection>
                            </apex:pageBlock>
                        </apex:outputPanel>
                        <!-- //Closing by SAG -->

                        <!-- Mixed Product -->
                        <apex:outputPanel rendered="{!showMixedProductWarning && !errorFound}" layout="none">
                            <div class="slds-m-bottom_small"><h3 class="slds-text-heading_small">Confirm Mixed Products</h3></div>
                            <apex:pageMessage strength="1" severity="warning" summary="{!$Label.ERROR_Mixed_Products_Found}" />
                            <apex:pageBlock >
                                <apex:pageBlockSection columns="1">
                                    <apex:pageBlockTable value="{!manualProducts}" var="prod">
                                        <apex:column value="{!prod.PricebookEntry.ProductCode}" />
                                        <apex:column value="{!prod.PricebookEntry.Name}" />
                                        <apex:column value="{!prod.Quantity}" />
                                        <apex:column value="{!prod.Duration__c}" />
                                        <apex:column value="{!prod.UnitPrice}" />
                                        <apex:column value="{!prod.TotalPrice}" />
                                    </apex:pageBlockTable>
                                </apex:pageBlockSection>
                                <apex:pageBlockSection columns="1">
                                    <apex:inputField value="{!theOpp.Confirm_mixed_opportunity__c}" />
                                </apex:pageBlockSection>
                            </apex:pageBlock>
                        </apex:outputPanel>
                        <!-- // Mixed Product -->

                        <!-- Missing Type -->
                        <apex:outputPanel rendered="{!showMissingTypeInfo}" layout="none">
                            <div class="slds-m-bottom_small"><h3 class="slds-text-heading_small">Account/BAN types</h3></div>
                            <apex:pageBlock >
                                <apex:outputPanel rendered="{!theOpp.Account.Unify_Account_SubType__c=null}">
                                    <apex:pageMessage strength="1" severity="warning" summary="Please fill in Account Type and Account Subtype on the Account" />
                                    <apex:pageBlockSection columns="1">
                                        <apex:inputField value="{!theOpp.Account.Unify_Account_Type__c}"  />
                                        <apex:inputField value="{!theOpp.Account.Unify_Account_SubType__c}" />
                                    </apex:pageBlockSection>
                                </apex:outputPanel>
                                <apex:outputPanel rendered="{!theOpp.BAN__r.Unify_Customer_SubType__c=null}">
                                    <apex:pageMessage strength="1" severity="warning" summary="Please fill in Customer Type and Customer SubType on the BAN" /> <apex:pageBlockSection columns="1">
                                        <apex:inputField value="{!theOpp.BAN__r.Unify_Customer_Type__c}" />
                                        <apex:inputField value="{!theOpp.BAN__r.Unify_Customer_SubType__c}" />
                                    </apex:pageBlockSection>
                                </apex:outputPanel>
                            </apex:pageBlock>
                        </apex:outputPanel>
                        <!-- //Missing Type -->

                        <!-- Standard Close Won Panel -->
                        <apex:outputPanel rendered="{!showClosedWonWithRedirect && !errorFound}" layout="none">
                            <div class="slds-m-bottom_small"><h3 class="slds-text-heading_small">Close Won Opportunity</h3></div>
                            <apex:pageMessage strength="1" severity="error" summary="{!$Label.ERROR_Close_Date_In_Future}" rendered="{!theOpp.CloseDate > TODAY()}" />
                            <apex:pageMessage strength="1" severity="error" summary="{!closeDateErrorLabel}" rendered="{!theOpp.CloseDate < minClosedDate}" />
                            <apex:pageBlock >
                                <apex:pageBlockSection columns="1">
                                    <apex:inputField value="{!theOpp.CloseDate}">
                                        <apex:actionSupport event="onchange" action="{!null}" rerender="popup" />
                                    </apex:inputField>
                                    <apex:inputField value="{!theOpp.Solution_Sales__c}" rendered="{!!OppHasZiggoProduct && theOpp.RecordType.Name == 'MAC'}" id="solSalesInput" required="true"/>
                                    <apex:inputField value="{!theOpp.Ziggo_Order_Reference__c}" rendered="{!OppHasZiggoProduct}" required="true"/>
                                    <apex:inputField value="{!theOpp.Customer_Reference__c}" rendered="{!CustomerReferenceOptional}"/>
                                </apex:pageBlockSection>

                                <div class="slds-notify slds-notify_alert slds-theme_alert-texture slds-theme_info" role="alert">
                                    <apex:outputText value="{!$Label.LABEL_Confirm_Close} {!IF(theOpp.No_contract_required__c,$Label.LABEL_Opportunity_Page,$Label.LABEL_Contract_Page)}" />
                                </div>
                            </apex:pageBlock>
                        </apex:outputPanel>
                        <!-- Standard Close Won Panel -->

                        <!-- Closed Won with Emails -->
                        <apex:outputPanel id="closedWonWithEmailPanel" rendered="{!showClosedWonWithEmail && !errorFound && !showQuickNewContact}">
                            <div class="slds-m-bottom_small"><h3 class="slds-text-heading_small">Close Won Opportunity + Welcome Emails</h3></div>
                            <apex:pageMessage strength="1" severity="error" summary="{!$Label.ERROR_Close_Date_In_Future}" rendered="{!theOpp.CloseDate > TODAY()}" />
                            <apex:pageMessage strength="1" severity="error" summary="{!closeDateErrorLabel}" rendered="{!theOpp.CloseDate < minClosedDate}" />
                            <apex:pageBlock >
                                <div class="slds-m-bottom_x-small slds-p-around_x-small slds-theme_shade">
                                    <h4 class="slds-text-heading_small">Select recipients</h4>
                                </div>
                                <div class="slds-grid">
                                    <div class="slds-col slds-size_1-of-5">
                                        <p class="slds-p-around_medium">
                                            Please select the welcome email recipients and confirm that you want to close the opportunity.
                                        </p>
                                        <p class="slds-p-around_medium">
                                            After confirming, the welcome email will be sent to the selected recipients{!IF(theOpp.No_contract_required__c,'.',' and you will be taken to the Order Form.')}
                                        </p>
                                    </div>
                                    <div class="slds-col slds-size_4-of-5">
                                        <apex:pageBlockTable value="{!contacts}" var="c">
                                            <apex:column headerValue="Select">
                                                <apex:actionRegion >
                                                    <apex:inputCheckbox id="cb" value="{!c.selected}" disabled="{!c.theContact.Email = ''}">
                                                        <apex:actionSupport event="onchange" action="{!checkNoneSelected}" rerender="noneSelectedMessage" />
                                                    </apex:inputCheckbox>
                                                </apex:actionRegion>
                                                <apex:image rendered="{!c.theContact.Received_Welcome_Email__c}" value="/img/msg_icons/error16.png" title="This Contact already received a welcome mail in the past, possibly for another product." />
                                            </apex:column>
                                            <apex:column value="{!c.theContact.Name}" />
                                            <apex:column value="{!c.theContact.Title}" />
                                            <apex:column value="{!c.theContact.Email}" />
                                        </apex:pageBlockTable>
                                        <div class="slds-p-vertical_small align-right">
                                            <apex:commandLink value="Quick-create new Contact" action="{!createNewContact}" rerender="popup" status="status" styleClass="slds-button slds-button_neutral" />
                                        </div>
                                    </div>
                                </div>

                                <div class="slds-m-bottom_x-small slds-p-around_x-small slds-theme_shade">
                                    <h4 class="slds-text-heading_small">Additional information</h4>
                                </div>
                                <apex:pageBlockSection columns="1">
                                    <apex:pageBlockSectionItem >
                                        <apex:outputLabel value="Welcome Email Send Date:" />
                                        <apex:inputField value="{!dummyContact.Welcome_Email_Date__c}" />
                                    </apex:pageBlockSectionItem>
                                    <apex:pageBlockSectionItem >
                                        <apex:outputLabel value="Opportunity Close Date:" />
                                        <apex:inputField value="{!theOpp.CloseDate}">
                                            <apex:actionSupport event="onchange" action="{!null}" rerender="popup" />
                                        </apex:inputField>
                                    </apex:pageBlockSectionItem>
                                    <apex:pageBlockSectionItem rendered="{!OppHasZiggoProduct}">
                                        <apex:outputLabel value="Ziggo Order Reference:"/>
                                        <apex:inputField value="{!theOpp.Ziggo_Order_Reference__c}" required="true"/>
                                    </apex:pageBlockSectionItem>
                                    <apex:pageBlockSectionItem rendered="{!CustomerReferenceOptional}">
                                        <apex:outputLabel value="Customer Reference:"/>
                                        <apex:inputField value="{!theOpp.Customer_Reference__c}"/>
                                    </apex:pageBlockSectionItem>
                                    <apex:pageBlockSectionItem >
                                        <apex:outputLabel value="Check this box to receive a CC:" />
                                        <apex:inputCheckbox value="{!ccToCurrentUser}"/>
                                    </apex:pageBlockSectionItem>
                                </apex:pageBlockSection>

                                <apex:outputPanel id="noneSelectedMessage">
                                    <apex:pageMessage strength="2" severity="warning" summary="No email will be sent if no Contact is selected!" rendered="{!noneSelected}" />
                                </apex:outputPanel>

                            </apex:pageBlock>

                            <apex:pageBlock >
                                <div class="slds-m-bottom_x-small slds-p-around_x-small slds-theme_shade">
                                    <h4 class="slds-text-heading_small">Email preview (selected Contacts will appear as receivers, your Head of Sales will be displayed as signer (ondergetekende))</h4>
                                </div>
                                <div class="slds-align_absolute-center">
                                    <apex:iframe title="Preview" id="previewFrame" scrolling="no" frameborder="0" height="961" width="665"
                                    src="{!ContentUrl}/email/templaterenderer?id={!templateId}&related_to_id={!theOpp.Id}&preview_frame=previewFrame&base_href={!BaseUrl}&render_type=REPLACED_HTML_BODY" />
                                </div>
                            </apex:pageBlock>

                        </apex:outputPanel>

                        <apex:outputPanel rendered="{!showQuickNewContact}">
                            <div class="slds-m-bottom_small"><h3 class="slds-text-heading_small">Contact Details</h3></div>
                            <apex:pageBlock >
                                <apex:pageBlockButtons location="bottom">
                                    <apex:commandButton value="Save" action="{!saveQuickNewContact}" rerender="popup" status="status" styleClass="slds-button slds-button_brand"/>
                                    <apex:commandButton value="Cancel" action="{!backToForm}" rerender="popup" immediate="true" status="status" styleClass="slds-button slds-button_neutral"/>
                                </apex:pageBlockButtons>
                                <apex:pageBlockSection >
                                    <apex:outputField value="{!quickNewContact.AccountId}" />
                                    <apex:outputLabel />
                                    <apex:inputField value="{!quickNewContact.Salutation}" />
                                    <apex:inputField value="{!quickNewContact.FirstName}" />
                                    <apex:inputField value="{!quickNewContact.LastName}" required="true"/>
                                    <apex:inputField value="{!quickNewContact.Email}" required="true"/>
                                    <apex:inputField value="{!quickNewContact.Phone}" required="true"/>
                                    <apex:inputField value="{!quickNewContact.MobilePhone}" />
                                    <apex:inputField value="{!quickNewContact.Title}" />
                                </apex:pageBlockSection>
                            </apex:pageBlock>
                        </apex:outputPanel>

                    </div>
                    <!-- //Content -->

                    <!-- Footer -->
                    <footer class="slds-modal__footer ">

                        <apex:outputPanel rendered="{! !showQuickNewContact}" layout="block">
                            <apex:commandButton value="Back to Opportunity"
                                action="{!cancelClosedWon}"
                                rendered="{!OR(AND(errorFound, NOT(isLightningCommunity)), !errorFound)}"
                                styleClass="slds-button slds-button_neutral"
                                rerender="popup"
                                status="status"
                                immediate="true"/>

                            <apex:commandButton value="Confirm"
                                rendered="{!AND(showClosingBySAGMessage, !errorFound)}"
                                action="{!confirmClosingBySAG}"
                                styleClass="slds-button slds-button_brand"
                                rerender="popup"
                                status="status"/>

                            <apex:commandButton value="Confirm"
                                rendered="{!AND(showMixedProductWarning, !errorFound)}"
                                action="{!confirmMixedOpportunity}"
                                styleClass="slds-button slds-button_brand"
                                rerender="popup"
                                status="status"/>

                            <apex:commandButton value="Confirm"
                                rendered="{!showMissingTypeInfo}"
                                action="{!confirmMissingType}"
                                styleClass="slds-button slds-button_brand"
                                rerender="popup"
                                status="status"/>

                            <apex:commandButton value="Confirm"
                                rendered="{!OR(showClosedWonWithRedirect, showClosedWonWithEmail) && !errorFound}"
                                action="{!confirmClosedWon}"
                                styleClass="slds-button slds-button_brand"
                                rerender="popup"
                                status="status"
                                disabled="{!theOpp.CloseDate < minClosedDate || theOpp.CloseDate > TODAY()}" />

                        </apex:outputPanel>

                    </footer>
                    <!-- //Footer -->

                    <apex:outputText value="{! scriptToRun }" escape="false" id="run-script-box" />

                </apex:outputPanel>
            </div>
        </section>
        <apex:outputPanel layout="none" rendered="{!NOT(isLightningCommunity)}">
            <div class="slds-backdrop slds-backdrop_open"></div>
        </apex:outputPanel>
        <!-- // Modal -->

    </apex:form>


    <style type="text/css">
        .helpButton img, .helpButtonOn img {
            display: none;
        }
        .datePicker {
            z-index: 9999 !important;
        }
        .messageTable td:first-child {
            width: 25px;
        }
        table.detailList {
            width: auto;
        }
        .align-right {
            text-align: right;
        }
        .slds-modal__content {
            min-height: 240px;
        }
    </style>

    <apex:outputPanel rendered="{! isLightningCommunity }">
        <script type="text/javascript" src="{! $Resource.DirtyFrame_bottom_js }"></script>
    </apex:outputPanel>

</apex:page>