trigger OpportunityLineItemTrigger on OpportunityLineItem(
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete
) {
	// we have to put this here (and not in the TriggerHandler), otherwise the trigger context variables are not properly passed to the trigger - GC 2019-08-23
	if (!TestUtils.disableDlrs) {
		dlrs.RollupService.triggerHandler(opportunitylineitem.SObjectType);
	}

	new OpportunityLineItemTriggerHandler().execute();
}
