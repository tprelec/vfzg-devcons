trigger OrderEntryDiscount on OE_Discount__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {

        new OrderEntryDiscountTriggerHandler().execute();
}