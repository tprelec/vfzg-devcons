trigger BanTrigger on Ban__c(
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update
) {
    new BanTriggerHandler().execute();
}