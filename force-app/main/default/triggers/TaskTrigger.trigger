/**
 * @description         This is the Task trigger.
 * @author              Guy Clairbois
 */
trigger TaskTrigger on Task(before delete, before insert, before update, after delete, after insert, after update, after undelete) {
	TriggerFactory.createHandler(Task.sObjectType);
}
