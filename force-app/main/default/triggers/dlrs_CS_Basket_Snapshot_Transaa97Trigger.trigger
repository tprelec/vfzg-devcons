/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_CS_Basket_Snapshot_Transaa97Trigger on CS_Basket_Snapshot_Transactional__c(
	before delete,
	before insert,
	before update,
	after delete,
	after insert,
	after undelete,
	after update
) {
	if (!TestUtils.disableDlrs) {
		dlrs.RollupService.triggerHandler(CS_Basket_Snapshot_Transactional__c.SObjectType);
	}
}
