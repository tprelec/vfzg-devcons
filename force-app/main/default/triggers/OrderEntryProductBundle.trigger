trigger OrderEntryProductBundle on OE_Product_Bundle__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {

        new OrderEntryProductBundleTriggerHandler().execute();
}