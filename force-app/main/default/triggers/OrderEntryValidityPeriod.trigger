trigger OrderEntryValidityPeriod on OE_Validity_Period__c  (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {

        new OrderEntryValidityPeriodTriggerHandler().execute();
}