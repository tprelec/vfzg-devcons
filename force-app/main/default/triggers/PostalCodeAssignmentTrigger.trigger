/**
 * @description         This is the NetProfit Information trigger.
 * @author              Stjepan Pavuna
 */

trigger PostalCodeAssignmentTrigger on Postal_Code_Assignment__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new PostalCodeAssignmentTriggerHandler().execute();
}