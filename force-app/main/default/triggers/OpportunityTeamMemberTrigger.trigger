trigger OpportunityTeamMemberTrigger on OpportunityTeamMember(
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete
) {
	new OpportunityTeamMemberTriggerHandler().execute();
}