trigger ContractedProductsTrigger on Contracted_Products__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new ContractedProductsTriggerHandler().execute();
}