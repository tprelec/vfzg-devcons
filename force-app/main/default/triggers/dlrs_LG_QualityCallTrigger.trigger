/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_LG_QualityCallTrigger on LG_QualityCall__c(
	before delete,
	before insert,
	before update,
	after delete,
	after insert,
	after undelete,
	after update
) {
	if (!TestUtils.disableDlrs) {
		dlrs.RollupService.triggerHandler(LG_QualityCall__c.SObjectType);
	}
}
