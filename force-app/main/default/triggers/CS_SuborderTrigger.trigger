trigger CS_SuborderTrigger on Suborder__c (after update) {
    new CS_SolutionTriggerHandler().execute();
}