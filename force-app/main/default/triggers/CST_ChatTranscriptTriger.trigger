/*
 * @author Luis Aguiar
 * @date 2022
 * @description Trigger uses the TriggerHandler structure
 */
trigger CST_ChatTranscriptTriger on LiveChatTranscript(before update) {
	// this class will extend TriggerHandler to increase reusability
	new CST_ChatTranscriptTrigerHandler().execute();
}
