trigger CS_ServiceTrigger on csord__Service__c (before insert, before update, after insert, after update) {
    new CS_ServiceTriggerHandler().execute();
}