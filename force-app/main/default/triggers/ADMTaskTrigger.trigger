trigger ADMTaskTrigger on agf__ADM_Task__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(agf__ADM_Task__c.sObjectType);

}