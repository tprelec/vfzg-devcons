trigger BatchErrorEventTrigger on BatchApexErrorEvent(after insert) {
    new BatchErrorEventTriggerHandler().handleBatchErrors(Trigger.new);
}