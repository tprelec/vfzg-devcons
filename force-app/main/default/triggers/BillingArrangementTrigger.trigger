/**
 * @description         This is the Billing_Arrangement__c trigger.
 * @author              Guy Clairbois
 */
trigger BillingArrangementTrigger on Billing_Arrangement__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new BillingArrangementTriggerHandler().execute();
}