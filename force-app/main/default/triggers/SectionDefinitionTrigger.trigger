trigger SectionDefinitionTrigger on csclm__Section_Definition__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new SectionDefinitionTriggerHandler().execute();
}
