/**
 * @description         This is the AccountRevenue trigger.
 * @author              Guy Clairbois
 */
trigger AccountRevenueTrigger on Account_Revenue__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Account_Revenue__c.sObjectType);
}