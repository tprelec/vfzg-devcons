trigger OrderEntryProductDetail on Order_Entry_Product_Detail__c  (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {

        new OrderEntryProductDetailTriggerHandler().execute();
}