trigger ProductGroupAssTrigger on Product_Group_Association__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new ProductGroupAssTriggerHandler().execute();
}
