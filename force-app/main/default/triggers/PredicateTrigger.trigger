trigger PredicateTrigger on csclm__Predicate__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new PredicateTriggerHandler().execute();
}
