trigger LG_AttributeTrigger on cscfga__Attribute__c (after delete, after insert, after update, 
before delete, before insert, before update) 
{
    
    String limitInformation = 'Number of Queries used in this Apex code so far: ' + Limits.getQueries() + ' ( ' + Limits.getLimitQueries() + ' ) ' + ' [ ' + (100.00 * Limits.getQueries() / Limits.getLimitQueries()).setScale(2) + '% ] ';
        limitInformation += '\n Number of rows queried in this Apex code so far: ' + Limits.getDmlRows() + ' ( ' + Limits.getLimitDmlRows() + ' ) ' + ' [ ' + (100.00 * Limits.getDmlRows() / Limits.getLimitDmlRows()).setScale(2) + '% ] ';
        limitInformation += '\n Number of DML statements used so far: ' +  Limits.getDmlStatements() + ' ( ' + Limits.getLimitDmlStatements() + ' ) ' + ' [ ' + (100.00 * Limits.getDmlStatements() / Limits.getLimitDmlStatements()).setScale(2) + '% ] ';
        limitInformation += '\n Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime() + ' ( ' + Limits.getLimitCpuTime() + ' ) ' + ' [ ' + (100.00 * Limits.getCpuTime() / Limits.getLimitCpuTime()).setScale(2) + '% ] ';
        limitInformation += '\n Amount of heap memory (in bytes) used so far: ' + Limits.getHeapSize() + ' ( ' + Limits.getLimitHeapSize() + ' ) ' + ' [ ' + (100.00 * Limits.getHeapSize() / Limits.getLimitHeapSize()).setScale(2) + '% ] ';
        
    System.Debug('Limits LG_AttributeTrigger: ' + limitInformation);
    
    List<cscfga__Attribute__c> recordsList = Trigger.isDelete ? Trigger.Old : Trigger.New;

    if (CS_Brand_Resolver.getBrandForSObjectList(recordsList) != 'Ziggo') {
        System.Debug('Opp brand LG_AttributeTrigger: Vodafone');
        return;
    }
    

    System.Debug('Opp brand LG_AttributeTrigger : Ziggo');

    No_Triggers__c notriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
    
    if (notriggers == null || !notriggers.Flag__c)
    {
        if (trigger.isInsert && trigger.isBefore) {
            LG_AttributeTriggerHandler.updateOrderNumberOnMACDChange(trigger.new);
        }
        
        if ((trigger.isInsert) && (trigger.IsAfter))
        {
            LG_AttributeTriggerHandler.AfterInsertHandle(trigger.new);
            
            for (integer i=0;i<trigger.new.size();++i)
            {
                system.debug('*** after insert trigger.new[' + i + ']=' + trigger.new[i].Name);
            }
        }
        
        if ((trigger.isUpdate) && (trigger.IsAfter))
        {
            LG_AttributeTriggerHandler.AfterUpdateHandle(trigger.new, trigger.old);

            for (integer i=0;i<trigger.new.size();++i)
            {
                system.debug('*** after update trigger.new[' + i + ']=' + trigger.new[i].Name);
            }

        }
        
    }

}