/**
 * @author: Rahul Sharma (07-09-2021)
 * @description: Subscription for platform event: Site postal check event
 * Jira ticket reference: COM-1960
 */

trigger SitePostalCheckEventTrigger on SitePostalCheckEvent__e(after insert) {
    new SitePostalCheckEventTriggerHandler().afterInsert(Trigger.new);
}