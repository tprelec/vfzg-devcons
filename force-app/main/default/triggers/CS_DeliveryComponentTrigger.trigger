trigger CS_DeliveryComponentTrigger on Delivery_Component__c (after update, after insert, before update, before insert) {
    new CS_DeliveryComponentTriggerHandler().execute();
}