trigger VF_Family_TagTrigger on VF_Family_Tag__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new VF_Family_TagTriggerHandler().execute();
}
