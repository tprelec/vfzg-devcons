/**
 * Created by juan.cardona on 28/04/2020.
 */

trigger QueueUserSharingTrigger on Queue_User_Sharing__c (before insert, before update) {

    TriggerFactory.createHandler(Queue_User_Sharing__c.sObjectType);

}