trigger ClosingOpportunityEventTrigger on Closing_Opportunity__e(after insert) {
	new ClosingOpportunityEventTriggerHandler().afterInsert(Trigger.new);
}