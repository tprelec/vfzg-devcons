trigger UnicaBaseAllocationTrigger on Unica_Base_Allocation__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    new UnicaBaseAllocationTriggerHandler().execute();
}