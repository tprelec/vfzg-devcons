trigger ADMWorkTrigger on agf__ADM_Work__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(agf__ADM_Work__c.sObjectType);

}