/**
 * @description       : Apex Trigger for the D2D Compensation Object
 * @author            : Filip Vucic (filip.vucic@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 03-11-2022
 * @last modified by  : Filip Vucic (filip.vucic@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                        Modification
 * 1.0   03-11-2022   Filip Vucic (filip.vucic@vodafoneziggo.com)   Initial Version
 **/
trigger D2DCompensationTrigger on D2D_Compensation__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new D2DCompensationTriggerHandler().execute();
}
