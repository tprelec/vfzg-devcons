trigger DocumentDefinitionTrigger on csclm__Document_Definition__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new DocumentDefinitionTriggerHandler().execute();
}
