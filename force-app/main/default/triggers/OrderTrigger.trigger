/**
 * @description         This is the Order__c trigger.
 * @author              Guy Clairbois
 */
trigger OrderTrigger on Order__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(Order__c.sObjectType);
}
