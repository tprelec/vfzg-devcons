trigger COM_DeliveryOrderTrigger on COM_Delivery_Order__c (after update) {
    new COM_DeliveryOrderTriggerHandler().execute();
}