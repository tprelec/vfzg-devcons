trigger VendorTrigger on Vendor__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new VendorTriggerHandler().execute();
}
