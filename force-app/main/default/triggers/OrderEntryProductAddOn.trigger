trigger OrderEntryProductAddOn on OE_Product_Add_On__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {

        new OrderEntryProductAddOnTriggerHandler().execute();
}