/**
 * @description         This is the VF_Asset trigger.
 * @author              Guy Clairbois
 */
trigger VF_AssetTrigger on VF_Asset__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(VF_Asset__c.sObjectType);
	if (!TestUtils.disableDlrs) {
		dlrs.RollupService.triggerHandler(VF_Asset__c.SObjectType);
	}
}
