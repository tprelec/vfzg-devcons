/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_VF_ContractTrigger on VF_Contract__c(before insert) {
    // an empty trigger because it is needed by the dlrs package. We have put the code that triggers the logic in the regular task trigger 
    // so we have control over the order in which the logic runs
}