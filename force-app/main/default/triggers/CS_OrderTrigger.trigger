trigger CS_OrderTrigger on csord__Order__c (after update, before update, before insert, after insert) {
    new CS_OrderTriggerHandler().execute();
}