/**
 * @description         This is the VF_Contract trigger.
 * @author              Guy Clairbois
 */
trigger VF_ContractTrigger on VF_Contract__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	TriggerFactory.createHandler(VF_Contract__c.sObjectType);
	if (!TestUtils.disableDlrs) {
		dlrs.RollupService.triggerHandler(VF_Contract__c.SObjectType);
	}
}
