/**
 * Created by nikola.culej on 8.6.2020..
 */

trigger CS_SubscriptionTrigger on csord__Subscription__c (after update) {
    new CS_SubscriptionTriggerHandler().execute();
}