trigger SectionClauseDefinitionAssociationTrigger on csclm__Section_Clause_Definition_Association__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new SectionClauseDefAssTriggerHandler().execute();
}
