trigger ProductConfigurationTrigger on cscfga__Product_Configuration__c (after delete, after insert, after undelete, 
    after update, before delete, before insert, before update) {
    TriggerFactory.createHandler(cscfga__Product_Configuration__c.sObjectType);
}