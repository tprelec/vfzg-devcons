trigger OrderEntryPromotionDiscount on OE_Promotion_Discount__c (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {

        new OEPromotionDiscountTriggerHandler().execute();
}