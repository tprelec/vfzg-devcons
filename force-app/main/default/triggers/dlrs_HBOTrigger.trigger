/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
trigger dlrs_HBOTrigger on HBO__c(before delete, before insert, before update, after delete, after insert, after undelete, after update) {
	if (!TestUtils.disableDlrs) {
		dlrs.RollupService.triggerHandler(HBO__c.SObjectType);
	}
}
