trigger OrderEntryProduct on OE_Product__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new OrderEntryProductTriggerHandler().execute();
}