/**
 * @description       : Apex Trigger for the Deal Item Object
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

trigger DealItemTrigger on Deal_Item__c(after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new DealItemTriggerHandler().execute();
}
