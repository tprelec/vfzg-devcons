trigger BillingAccountTrigger on csconta__Billing_Account__c(
    before insert,
    before update
) {
    new BillingAccountTriggerHandler().execute();
}