trigger CST_ExternalSystem_TriggerHandler on CST_External_Ticket__c(
	before insert,
	before update,
	before delete,
	after insert,
	after update,
	after delete,
	after undelete
) {
	new CST_ExternalSystem().execute();
}
