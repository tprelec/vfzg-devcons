trigger csclmRulePredicateAssociationTrigger on csclm__Rule_Predicate_Association__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new RulePredicateAssociationHandler().execute();
}
