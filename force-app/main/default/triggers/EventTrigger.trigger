/**
 * @description         This is the Event trigger.
 * @author              Guy Clairbois
 */
trigger EventTrigger on Event (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	new EventTriggerHandler().execute();
}