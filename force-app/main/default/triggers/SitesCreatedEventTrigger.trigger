trigger SitesCreatedEventTrigger on Sites_Created__e(after insert) {
	Set<Id> basketIds = new Set<Id>();
	for (Sites_Created__e event : Trigger.New) {
		if (
			Id.valueOf(event.Related_To_Id__c).getSObjectType().getDescribe().getName() ==
			'cscfga__Product_Basket__c'
		) {
			basketIds.add(event.Related_To_Id__c);
		}
	}
	Map<Id, cscfga__Product_Basket__c> baskets = new Map<Id, cscfga__Product_Basket__c>(
		[
			SELECT Id, csbb__Account__c, CustomData__c
			FROM cscfga__Product_Basket__c
			WHERE Id IN :basketIds
		]
	);
	Integer counter = 0;
	for (Sites_Created__e event : Trigger.New) {
		// Increase batch counter.
		counter++;
		// Only process the first 200 event messages
		if (counter > 200) {
			// Resume after the last successfully processed event message after the trigger stops running.
			break;
		}

		if (
			Id.valueOf(event.Related_To_Id__c).getSObjectType().getDescribe().getName() ==
			'cscfga__Product_Basket__c'
		) {
			OnlineOrderFlowOrchestration onlineOrderFlowOrch = new OnlineOrderFlowOrchestration(
				baskets.get(event.Related_To_Id__c).CustomData__c,
				baskets.get(event.Related_To_Id__c).csbb__Account__c,
				baskets.get(event.Related_To_Id__c).Id
			);
			onlineOrderFlowOrch.fetchSitesAndCreateObjects();
		}

		// Set Replay ID after which to resume event processing
		// in new trigger execution.
		EventBus.TriggerContext.currentContext().setResumeCheckpoint(event.ReplayId);
	}
}