/**
 * @description       : Apex Trigger for the Matrix Line Object
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   20-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

trigger MatrixLineTrigger on appro__Matrix_Line__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new MatrixLineTriggerHandler().execute();
}
