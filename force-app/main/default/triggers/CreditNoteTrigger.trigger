/**
 * @description         This is the Ban trigger.
 * @author              Guy Clairbois
 */
trigger CreditNoteTrigger on Credit_Note__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
    new CreditNoteTriggerHandler().execute();
}