trigger OrderEntryPromotion on OE_Promotion__c  (
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update) {

        new OrderEntryPromotionTriggerHandler().execute();
}