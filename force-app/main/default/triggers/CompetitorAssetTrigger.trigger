trigger CompetitorAssetTrigger on Competitor_Asset__c(
    after delete,
    after insert,
    after undelete,
    after update,
    before delete,
    before insert,
    before update
) {
    new CompetitorAssetTriggerHandler().execute();
}