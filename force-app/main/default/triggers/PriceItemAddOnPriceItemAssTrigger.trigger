trigger PriceItemAddOnPriceItemAssTrigger on cspmb__Price_Item_Add_On_Price_Item_Association__c(
	after delete,
	after insert,
	after undelete,
	after update,
	before delete,
	before insert,
	before update
) {
	new PriceItemAddOnPriceItemAssHandler().execute();
}
