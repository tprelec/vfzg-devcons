/**
 * Created by bojan.zunko on 11/05/2020.
 */

public with sharing class ETLCallout {
	private static final String HANDLER_NAME = 'Digital Commerce ETL';

	public static void doCallout(String publicationID) {
		PublicationPayload payload = getETLPayload(publicationID);

		csam.ObjectGraphCalloutHandler.queueJsonPayloadMessageFromId(HANDLER_NAME, ID.valueOf(publicationID), JSON.serialize(payload));
	}

	@TestVisible
	private static PublicationPayload getETLPayload(String publicationID) {
		CCETLSettings__c etlSettings = CCETLSettings__c.getInstance();
		DCETLCatalogImportOptions__c etlCatalogImportOptions = DCETLCatalogImportOptions__c.getInstance();

		if (String.isBlank(etlSettings.Client_ID__c)) {
			throw new CalloutException('Please define CC ETL custom settings to define the Client ID');
		}

		CatalogImportOptions catalogImportOptions = new CatalogImportOptions();
		catalogImportOptions.displayLabels = etlCatalogImportOptions.Import_Display_Labels__c;
		catalogImportOptions.categories = etlCatalogImportOptions.Import_Categories__c;
		catalogImportOptions.userDefinedAttributes = etlCatalogImportOptions.Import_User_Defined_Attributes__c;

		ImportOptions importOptions = new ImportOptions();
		importOptions.priceBook = etlSettings.Import_Pricebooks__c;
		importOptions.inventory = etlSettings.Import_Inventory__c;
		importOptions.identifiers = etlSettings.Import_Catalog_Identifiers__c;
		importOptions.recommendations = etlSettings.Import_Recommendation__c;
		importOptions.catalog = etlSettings.Import_Catalog__c;
		importOptions.catalogImportOptions = catalogImportOptions;

		//Prepare inner structure for easier JSON
		PublicationPayload payload = new PublicationPayload();

		List<csb2c__E_Commerce_Publication__c> catalogeInProcess = [
			SELECT
				id,
				(
					SELECT id, csb2c__Catalogue__c, csb2c__Catalogue__r.Name, csb2c__Catalogue__r.cc_etl_custom_field_schema__c
					FROM csb2c__Publication_Catalogue_Associations__r
				)
			FROM csb2c__E_Commerce_Publication__c
			WHERE id = :publicationID
		];
		payload.publicationId = publicationID;
		payload.elasticCatalogId = catalogeInProcess[0].csb2c__Publication_Catalogue_Associations__r[0].csb2c__Catalogue__c;
		payload.elasticCatalogName = catalogeInProcess[0].csb2c__Publication_Catalogue_Associations__r[0].csb2c__Catalogue__r.Name;
		if (
			String.isNotBlank(catalogeInProcess[0].csb2c__Publication_Catalogue_Associations__r[0].csb2c__Catalogue__r.cc_etl_custom_field_schema__c)
		) {
			payload.customFieldsSchema = (Map<String, Object>) JSON.deserializeUntyped(
				catalogeInProcess[0].csb2c__Publication_Catalogue_Associations__r[0].csb2c__Catalogue__r.cc_etl_custom_field_schema__c
			);
		}
		payload.clientId = etlSettings.Client_ID__c;
		payload.importOptions = importOptions;

		payload.prgs = new List<PRG>();
		List<cspmb__Pricing_Rule_Group__c> prgs = getPRGsForPublication(ID.valueOf(publicationID));

		for (cspmb__Pricing_Rule_Group__c iter : prgs) {
			PRG creatingPRG = new PRG();
			creatingPRG.id = iter.cspmb__pricing_rule_group_code__c;
			creatingPRG.name = iter.Name;
			creatingPRG.currencyCode = String.isBlank(iter.sfcc_currency_code__c) ? 'USD' : iter.sfcc_currency_code__c;
			payload.prgs.add(creatingPRG);
		}

		return payload;
	}

	/*
	 * Method parameter is publication ID for the current ongoing publication.
	 * Method returns List of Pricing Rule Groups objects that have the Id,Name and cspmb__pricing_rule_group_code__c populated.
	 */
	@TestVisible
	private static List<cspmb__Pricing_Rule_Group__c> getPRGsForPublication(ID publicationID) {
		List<cspmb__Pricing_Rule_Group__c> returnList = new List<cspmb__Pricing_Rule_Group__c>();
		Set<String> prgCodes = new Set<String>();

		// @deprecated - Instead use cspsi module to register default prgs
		List<csb2c__B2C_Setting__mdt> prgSetting = [
			SELECT id, DeveloperName, csb2c__text_value__c
			FROM csb2c__B2C_Setting__mdt
			WHERE DeveloperName = 'PS_DefaultPRGCodes'
			LIMIT 1
		];
		if (prgSetting.size() > 0) {
			prgCodes.addAll(prgSetting[0].csb2c__text_value__c.split(','));
		}

		prgCodes.addAll(new List<String>(cspsi.PrgSelector.selectPrgs(null, null, null, null)));
		if (prgCodes.size() > 0) {
			returnList = [
				SELECT Id, Name, cspmb__pricing_rule_group_code__c, sfcc_currency_code__c
				FROM cspmb__Pricing_Rule_Group__c
				WHERE cspmb__pricing_rule_group_code__c IN :prgCodes
			];
		}
		return returnList;
	}

	public class PublicationPayload {
		public String publicationId;
		public String clientId;
		public String elasticCatalogId;
		public String elasticCatalogName;
		public List<PRG> prgs;
		public Map<String, Object> customFieldsSchema;
		public ImportOptions importOptions;
	}

	public class PRG {
		public String id;
		public String name;
		public String currencyCode;
	}

	public class CatalogImportOptions {
		public Boolean displayLabels;
		public Boolean categories;
		public Boolean userDefinedAttributes;
	}

	public class ImportOptions {
		public Boolean priceBook;
		public Boolean inventory;
		public Boolean identifiers;
		public Boolean recommendations;
		public Boolean catalog;
		public CatalogImportOptions catalogImportOptions;
	}
}
