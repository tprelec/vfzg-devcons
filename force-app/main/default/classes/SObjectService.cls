public inherited sharing class SObjectService {
	private static Map<SObjectType, Map<String, Schema.SObjectField>> objectTypeToFields = new Map<sObjectType, Map<String, Schema.SObjectField>>();

	public static String getObjectFieldsAsCSV(SObjectType objectType) {
		Map<String, Schema.SObjectField> fieldMap = getObjectFields(objectType);
		return String.join(new List<String>(fieldMap.keySet()), ',');
	}

	public static Map<String, Schema.SObjectField> getObjectFields(SObjectType objectType) {
		Map<String, Schema.SObjectField> fieldMap;
		if (objectTypeToFields.containsKey(objectType)) {
			return objectTypeToFields.get(objectType);
		}
		fieldMap = objectType.getDescribe().fields.getMap();
		objectTypeToFields.put(objectType, fieldMap);
		return fieldMap;
	}

	public static Schema.DisplayType getFieldDisplayType(SObjectType objectType, String fieldName) {
		Map<String, Schema.SObjectField> fieldMap = getObjectFields(objectType);
		Schema.sObjectField fld = fieldMap.get(fieldName.toLowerCase());
		return fld.getDescribe().getType();
	}
}