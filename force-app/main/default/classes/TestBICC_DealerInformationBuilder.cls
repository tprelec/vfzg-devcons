/**
 * @description:    Test class for BICC_DealerInformationBuilder
 **/
@isTest
public class TestBICC_DealerInformationBuilder {
	private static final String TEST_BICC_DEALER_NAME = 'TestBICC_DealerInformationBuilder';
	private static final String TEST_BICC_DEALER_CODE = '12345';
	private static final String TEST_BICC_DEALER_CODE_PREFIX = '00';
	private static final String TEST_BICC_PARENT_DEALER_CODE = '98765';
	private static final String TEST_BICC_PARENT_DEALER_CODE_PREFIX = '1122';
	private static final String TEST_BICC_DEALER_CONTACT_EMAIL_PREFIX = 'TestBICC_DealerInformationBuilder';
	private static final String TEST_BICC_DEALER_CONTACT_EMAIL_SUFIX = '@test.test.com';
	private static final String TEST_BICC_SALES_CHANNEL_INDIRECT = Constants.DEALER_INFORMATION_SALES_CHANNEL_INDIRECT;
	private static final String TEST_BICC_NOT_ALLOWED_SALES_CHANNEL_SOHO = Constants.DEALER_INFORMATION_SALES_CHANNEL_SOHO;
	private static final Integer TEST_BICC_DEALER_STATUS_ACTIVE_INTEGER = Constants.BICC_DEALER_INFORMATION_STATUS_ACTIVE;
	private static final String TEST_DEALER_INFORMATION_STATUS_ACTIVE_STRING = Constants.DEALER_INFORMATION_STATUS_ACTIVE;
	private static final Integer TEST_BICC_DEALER_STATUS_EXPIRED_INTEGER = Constants.BICC_DEALER_INFORMATION_STATUS_EXPIRED;

	private static final String TEST_ACCOUNT_TYPE_DEALER = Constants.ACCOUNT_TYPE_DEALER;

	/**
	 * @description:    Tests attaching error message to BICC dealer information
	 *                  and not creating dealer information when dealer contact was
	 *                  not found
	 **/
	@isTest
	static void shouldAttachErrorMessageToBICCDealerInformationAndNotCreateDealerInformationWhenDealerContactWasNotFound() {
		// prepare data
		createBICCDealerInformation(TEST_BICC_DEALER_STATUS_ACTIVE_INTEGER, TEST_BICC_SALES_CHANNEL_INDIRECT, 1, false);
		List<BICC_Dealer_Information__c> biccDealerInformations = getBICCDealerInformations();

		// perform testing
		Test.startTest();
		Map<Id, sObject> generatedRecordPerBICCSobjectId = new BICC_DealerInformationBuilder()
			.withSobjectFieldsPerFieldName(new Map<String, Schema.SObjectField>())
			.withBICCSObjectRecords(biccDealerInformations)
			.withBICCFieldSyncMappingForDestinationField(new Map<String, Field_Sync_Mapping__c>())
			.build()
			.getRecordsPerId();
		Test.stopTest();

		// verify results
		System.assertEquals(0, generatedRecordPerBICCSobjectId.size(), 'No Dealer_Information__c records should be generated');

		for (Integer i = 0; i < biccDealerInformations.size(); i++) {
			String expectedErrorMessage = String.format(
				BICC_DealerInformationBuilder.ERROR_MESSAGE_DEALER_CONTACT_NOT_FOUND,
				new List<String>{ TEST_BICC_DEALER_CONTACT_EMAIL_PREFIX + i + TEST_BICC_DEALER_CONTACT_EMAIL_SUFIX }
			);

			System.assert(
				expectedErrorMessage.equalsIgnoreCase(biccDealerInformations[i].Error__c),
				'BICC_Dealer_Information__c.Error__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				true,
				biccDealerInformations[i].Processed__c,
				'BICC_Dealer_Information__c.Processed__c field should be properly populated for ' +
				i +
				'. record'
			);
		}
	}

	/**
	 * @description:    Tests attaching error message to BICC dealer information
	 *                  and not creating dealer information when not allowed sales
	 * 					channel is passed
	 **/
	@isTest
	static void shouldAttachErrorMessageToBICCDealerInformationAndNotCreateDealerInformationWhenNotAllowedSalesChannelIsPassed() {
		// prepare data
		createBICCDealerInformation(TEST_BICC_DEALER_STATUS_ACTIVE_INTEGER, TEST_BICC_NOT_ALLOWED_SALES_CHANNEL_SOHO, 1, false);
		Contact dealerContact = createDealerContact();
		List<BICC_Dealer_Information__c> biccDealerInformations = getBICCDealerInformations();

		// perform testing
		Test.startTest();
		Map<Id, sObject> generatedRecordPerBICCSobjectId = new BICC_DealerInformationBuilder()
			.withSobjectFieldsPerFieldName(new Map<String, Schema.SObjectField>())
			.withBICCSObjectRecords(biccDealerInformations)
			.withBICCFieldSyncMappingForDestinationField(new Map<String, Field_Sync_Mapping__c>())
			.build()
			.getRecordsPerId();
		Test.stopTest();

		// verify results
		System.assertEquals(0, generatedRecordPerBICCSobjectId.size(), 'No Dealer_Information__c records should be generated');

		for (Integer i = 0; i < biccDealerInformations.size(); i++) {
			String expectedErrorMessage = String.format(
				BICC_DealerInformationBuilder.ERROR_MESSAGE_NOT_ALLOWED_SALES_CHANNEL,
				new List<String>{
					biccDealerInformations[i].Sales_Channel__c,
					String.join(BICC_DealerInformationBuilder.ALLOWED_SALES_CHANNELS, ', ')
				}
			);

			System.assert(
				expectedErrorMessage.equalsIgnoreCase(biccDealerInformations[i].Error__c),
				'BICC_Dealer_Information__c.Error__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				true,
				biccDealerInformations[i].Processed__c,
				'BICC_Dealer_Information__c.Processed__c field should be properly populated for ' +
				i +
				'. record'
			);
		}
	}

	/**
	 * @description:    Tests attaching error message to BICC dealer information
	 *                  and not creating dealer information when expired status is sent
	 **/
	@isTest
	static void shouldAttachErrorMessageToBICCDealerInformationAndNotCreateDealerInformationWhenExpiredStatusIsSent() {
		// prepare data
		createBICCDealerInformation(TEST_BICC_DEALER_STATUS_EXPIRED_INTEGER, TEST_BICC_SALES_CHANNEL_INDIRECT, 1, false);
		Contact dealerContact = createDealerContact();
		List<BICC_Dealer_Information__c> biccDealerInformations = getBICCDealerInformations();

		// perform testing
		Test.startTest();
		Map<Id, sObject> generatedRecordPerBICCSobjectId = new BICC_DealerInformationBuilder()
			.withSobjectFieldsPerFieldName(new Map<String, Schema.SObjectField>())
			.withBICCSObjectRecords(biccDealerInformations)
			.withBICCFieldSyncMappingForDestinationField(new Map<String, Field_Sync_Mapping__c>())
			.build()
			.getRecordsPerId();
		Test.stopTest();

		// verify results
		System.assertEquals(0, generatedRecordPerBICCSobjectId.size(), 'No Dealer_Information__c records should be generated');

		String expectedErrorMessage = BICC_DealerInformationBuilder.ERROR_MESSAGE_EXPIRED_STATUS_NOT_IMPLEMENTED;

		for (Integer i = 0; i < biccDealerInformations.size(); i++) {
			System.assert(
				expectedErrorMessage.equalsIgnoreCase(biccDealerInformations[i].Error__c),
				'BICC_Dealer_Information__c.Error__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				true,
				biccDealerInformations[i].Processed__c,
				'BICC_Dealer_Information__c.Processed__c field should be properly populated for ' +
				i +
				'. record'
			);
		}
	}

	/**
	 * @description:    Tests attaching error message to BICC dealer information
	 *                  and not creating dealer information when dealer with dealer
	 *                  code already exists
	 **/
	@isTest
	static void shouldAttachErrorMessageToBICCDealerInformationAndNotCreateDealerInformationWhenDealerWithDealerCodeAlreadyExists() {
		// prepare data
		createBICCDealerInformation(TEST_BICC_DEALER_STATUS_ACTIVE_INTEGER, TEST_BICC_SALES_CHANNEL_INDIRECT, 1, false);
		createDealerInformationForContact(createDealerContact());
		List<BICC_Dealer_Information__c> biccDealerInformations = getBICCDealerInformations();

		// perform testing
		Test.startTest();
		Map<Id, sObject> generatedRecordPerBICCSobjectId = new BICC_DealerInformationBuilder()
			.withSobjectFieldsPerFieldName(new Map<String, Schema.SObjectField>())
			.withBICCSObjectRecords(biccDealerInformations)
			.withBICCFieldSyncMappingForDestinationField(new Map<String, Field_Sync_Mapping__c>())
			.build()
			.getRecordsPerId();
		Test.stopTest();

		// verify results
		System.assertEquals(0, generatedRecordPerBICCSobjectId.size(), 'No Dealer_Information__c records should be generated');

		String expectedErrorMessage = BICC_DealerInformationBuilder.ERROR_MESSAGE_UPDATE_OF_EXISTING_DEALER_NOT_IMPLEMENTED;

		for (Integer i = 0; i < biccDealerInformations.size(); i++) {
			System.assert(
				expectedErrorMessage.equalsIgnoreCase(biccDealerInformations[i].Error__c),
				'BICC_Dealer_Information__c.Error__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				true,
				biccDealerInformations[i].Processed__c,
				'BICC_Dealer_Information__c.Processed__c field should be properly populated for ' +
				i +
				'. record'
			);
		}
	}

	/**
	 * @description:    Tests creating dealer information from BICC dealer
	 *                  information when valid BICC dealer information is provided
	 *                  with dealer code prefix populated
	 **/
	@isTest
	static void shouldCreateDealerInformationFromBICCDealerInformationWhenCalledWithValidBICCDealerInformationWithDealerCodePrefix() {
		// prepare data
		createBICCDealerInformation(TEST_BICC_DEALER_STATUS_ACTIVE_INTEGER, TEST_BICC_SALES_CHANNEL_INDIRECT, 1, true);
		Contact dealerContact = createDealerContact();
		List<BICC_Dealer_Information__c> biccDealerInformations = getBICCDealerInformations();

		Map<String, Field_Sync_Mapping__c> biccFieldSyncMappingForDestinationField = getBICCFieldSyncMappingForDestinationField();
		Map<String, Schema.SObjectField> testDealerInformationSObjectFieldsPerFieldName = getTestDealerInformationSObjectFieldsPerFieldName();

		// perform testing
		Test.startTest();
		Map<Id, SObject> generatedRecordPerBICCSobjectId = (Map<Id, SObject>) new BICC_DealerInformationBuilder()
			.withSobjectFieldsPerFieldName(testDealerInformationSObjectFieldsPerFieldName)
			.withBICCSObjectRecords(biccDealerInformations)
			.withBICCFieldSyncMappingForDestinationField(biccFieldSyncMappingForDestinationField)
			.build()
			.getRecordsPerId();
		Test.stopTest();

		// verify results
		System.assertEquals(1, generatedRecordPerBICCSobjectId.size(), '1 Dealer_Information__c record should be generated');

		for (Integer i = 0; i < biccDealerInformations.size(); i++) {
			for (String destinationField : biccFieldSyncMappingForDestinationField.keySet()) {
				String sourceField = biccFieldSyncMappingForDestinationField.get(destinationField).Source_Field_Name__c;

				Object expectedDestinationFieldObjectValue = String.valueOf(biccDealerInformations[i].get(sourceField));

				System.assertEquals(
					expectedDestinationFieldObjectValue,
					generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get(destinationField),
					'Dealer_Information__c.' +
					destinationField +
					' field should be properly populated for ' +
					i +
					'. record'
				);
			}

			System.assertEquals(
				TEST_DEALER_INFORMATION_STATUS_ACTIVE_STRING,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Status__c'),
				'Dealer_Information__c.Status__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				dealerContact.Id,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Contact__c'),
				'Dealer_Information__c.Contact__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_DEALER_CODE + i,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Dealer_Code__c'),
				'Dealer_Information__c.Dealer_Code__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_DEALER_CODE_PREFIX,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Dealer_Code_Prefix__c'),
				'Dealer_Information__c.Dealer_Code_Prefix__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_PARENT_DEALER_CODE + i,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Parent_Dealer_Code__c'),
				'Dealer_Information__c.Parent_Dealer_Code__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_PARENT_DEALER_CODE_PREFIX,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Parent_Dealer_Code_Prefix__c'),
				'Dealer_Information__c.Parent_Dealer_Code_Prefix__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				true,
				biccDealerInformations[i].Processed__c,
				'BICC_Dealer_Information__c.Processed__c field should be properly populated for ' +
				i +
				'. record'
			);
		}
	}

	/**
	 * @description:    Tests creating dealer information from BICC dealer
	 *                  information when valid BICC dealer information is provided
	 *                  without dealer code prefix populated
	 **/
	@isTest
	static void shouldCreateDealerInformationFromBICCDealerInformationWhenCalledWithValidBICCDealerInformationWithoutDealerCodePrefix() {
		// prepare data
		createBICCDealerInformation(TEST_BICC_DEALER_STATUS_ACTIVE_INTEGER, TEST_BICC_SALES_CHANNEL_INDIRECT, 1, false);
		Contact dealerContact = createDealerContact();
		List<BICC_Dealer_Information__c> biccDealerInformations = getBICCDealerInformations();

		Map<String, Field_Sync_Mapping__c> biccFieldSyncMappingForDestinationField = getBICCFieldSyncMappingForDestinationField();
		Map<String, Schema.SObjectField> testDealerInformationSObjectFieldsPerFieldName = getTestDealerInformationSObjectFieldsPerFieldName();

		// perform testing
		Test.startTest();
		Map<Id, SObject> generatedRecordPerBICCSobjectId = (Map<Id, SObject>) new BICC_DealerInformationBuilder()
			.withSobjectFieldsPerFieldName(testDealerInformationSObjectFieldsPerFieldName)
			.withBICCSObjectRecords(biccDealerInformations)
			.withBICCFieldSyncMappingForDestinationField(biccFieldSyncMappingForDestinationField)
			.build()
			.getRecordsPerId();
		Test.stopTest();

		// verify results
		System.assertEquals(1, generatedRecordPerBICCSobjectId.size(), '1 Dealer_Information__c record should be generated');

		for (Integer i = 0; i < biccDealerInformations.size(); i++) {
			for (String destinationField : biccFieldSyncMappingForDestinationField.keySet()) {
				String sourceField = biccFieldSyncMappingForDestinationField.get(destinationField).Source_Field_Name__c;

				Object expectedDestinationFieldObjectValue = String.valueOf(biccDealerInformations[i].get(sourceField));

				System.assertEquals(
					expectedDestinationFieldObjectValue,
					generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get(destinationField),
					'Dealer_Information__c.' +
					destinationField +
					' field should be properly populated for ' +
					i +
					'. record'
				);
			}

			System.assertEquals(
				TEST_DEALER_INFORMATION_STATUS_ACTIVE_STRING,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Status__c'),
				'Dealer_Information__c.Status__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				dealerContact.Id,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Contact__c'),
				'Dealer_Information__c.Contact__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_DEALER_CODE + i,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Dealer_Code__c'),
				'Dealer_Information__c.Dealer_Code__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				'',
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Dealer_Code_Prefix__c'),
				'Dealer_Information__c.Dealer_Code_Prefix__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_PARENT_DEALER_CODE + i,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Parent_Dealer_Code__c'),
				'Dealer_Information__c.Parent_Dealer_Code__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				'',
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Parent_Dealer_Code_Prefix__c'),
				'Dealer_Information__c.Parent_Dealer_Code_Prefix__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				true,
				biccDealerInformations[i].Processed__c,
				'BICC_Dealer_Information__c.Processed__c field should be properly populated for ' +
				i +
				'. record'
			);
		}
	}

	/**
	 * @description:    Tests creating dealer information from BICC dealer
	 *                  information when called without field sync mappings
	 **/
	@isTest
	static void shouldCreateDealerInformationFromBICCDealerInformationWhenCalledWithoutFieldSyncMappings() {
		// prepare data
		createBICCDealerInformation(TEST_BICC_DEALER_STATUS_ACTIVE_INTEGER, TEST_BICC_SALES_CHANNEL_INDIRECT, 1, true);
		Contact dealerContact = createDealerContact();
		List<BICC_Dealer_Information__c> biccDealerInformations = getBICCDealerInformations();

		// perform testing
		Test.startTest();
		Map<Id, SObject> generatedRecordPerBICCSobjectId = (Map<Id, SObject>) new BICC_DealerInformationBuilder()
			.withSobjectFieldsPerFieldName(new Map<String, Schema.SObjectField>())
			.withBICCSObjectRecords(biccDealerInformations)
			.withBICCFieldSyncMappingForDestinationField(new Map<String, Field_Sync_Mapping__c>())
			.build()
			.getRecordsPerId();
		Test.stopTest();

		// verify results
		System.assertEquals(1, generatedRecordPerBICCSobjectId.size(), '1 Dealer_Information__c record should be generated');

		for (Integer i = 0; i < biccDealerInformations.size(); i++) {
			System.assertEquals(
				TEST_DEALER_INFORMATION_STATUS_ACTIVE_STRING,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Status__c'),
				'Dealer_Information__c.Status__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				dealerContact.Id,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Contact__c'),
				'Dealer_Information__c.Contact__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_DEALER_CODE + i,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Dealer_Code__c'),
				'Dealer_Information__c.Dealer_Code__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_DEALER_CODE_PREFIX,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Dealer_Code_Prefix__c'),
				'Dealer_Information__c.Dealer_Code_Prefix__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_PARENT_DEALER_CODE + i,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Parent_Dealer_Code__c'),
				'Dealer_Information__c.Parent_Dealer_Code__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				TEST_BICC_PARENT_DEALER_CODE_PREFIX,
				generatedRecordPerBICCSobjectId.get(biccDealerInformations[i].Id).get('Parent_Dealer_Code_Prefix__c'),
				'Dealer_Information__c.Parent_Dealer_Code_Prefix__c field should be properly populated for ' +
				i +
				'. record'
			);
			System.assertEquals(
				true,
				biccDealerInformations[i].Processed__c,
				'BICC_Dealer_Information__c.Processed__c field should be properly populated for ' +
				i +
				'. record'
			);
		}
	}

	/**
	 * @description:    Creates BICC dealer information records
	 * @param 			status - Status of the BICC dealer information
	 * @param 			salesChannel - Sales channel
	 * @param 			numberOfRecords - Number of records to be created
	 * @param 			usePrefix - should prefix be used
	 * @return 			List<BICC_Dealer_Information__c> - List of created BICC
	 * 					dealer information records
	 **/
	private static void createBICCDealerInformation(Integer status, String salesChannel, Integer numberOfDealers, Boolean usePrefix) {
		List<BICC_Dealer_Information__c> biccDealerInformations = new List<BICC_Dealer_Information__c>();

		for (Integer i = 0; i < numberOfDealers; i++) {
			String dealerCodeLoop = TEST_BICC_DEALER_CODE + i;
			String parentDealerCodeLoop = TEST_BICC_PARENT_DEALER_CODE + i;
			String dealerContactEmailLoop = TEST_BICC_DEALER_CONTACT_EMAIL_PREFIX + i + TEST_BICC_DEALER_CONTACT_EMAIL_SUFIX;

			biccDealerInformations.add(
				new BICC_Dealer_Information__c(
					Name = TEST_BICC_DEALER_NAME + i,
					Dealer_Code__c = usePrefix ? TEST_BICC_DEALER_CODE_PREFIX + dealerCodeLoop : dealerCodeLoop,
					Parent_Dealer_Code__c = usePrefix ? TEST_BICC_PARENT_DEALER_CODE_PREFIX + parentDealerCodeLoop : parentDealerCodeLoop,
					Dealer_Contact_Email__c = dealerContactEmailLoop,
					Sales_Channel__c = salesChannel,
					Status__c = status
				)
			);
		}

		insert biccDealerInformations;
	}

	/**
	 * @description:    Creates Dealer Contact record
	 * @return			Contact - created Dealer Contact record
	 **/
	private static Contact createDealerContact() {
		Account dealerAccount = TestUtils.createPartnerAccount();

		dealerAccount.Type = TEST_ACCOUNT_TYPE_DEALER;
		dealerAccount.IsPartner = true;
		dealerAccount.Dealer_code__c = TEST_BICC_DEALER_CODE + '0';
		update dealerAccount;

		TestUtils.autoCommit = false;
		Contact dealerAccountContact = TestUtils.createContact(dealerAccount);
		dealerAccountContact.Email = TEST_BICC_DEALER_CONTACT_EMAIL_PREFIX + '0' + TEST_BICC_DEALER_CONTACT_EMAIL_SUFIX;
		insert dealerAccountContact;
		TestUtils.autoCommit = true;

		return dealerAccountContact;
	}

	/**
	 * @description:    Creates Dealer information for dealer contact
	 * @param			contact - dealer information Contact
	 **/
	private static void createDealerInformationForContact(Contact contact) {
		insert new Dealer_Information__c(
			Name = TEST_BICC_DEALER_NAME,
			Dealer_Code__c = TEST_BICC_DEALER_CODE + '0',
			Dealer_Code_Prefix__c = TEST_BICC_DEALER_CODE_PREFIX,
			Parent_Dealer_Code__c = TEST_BICC_PARENT_DEALER_CODE + '0',
			Parent_Dealer_Code_Prefix__c = TEST_BICC_PARENT_DEALER_CODE_PREFIX,
			Dealer_Contact_Email__c = TEST_BICC_DEALER_CONTACT_EMAIL_PREFIX + TEST_BICC_DEALER_CONTACT_EMAIL_SUFIX,
			Sales_Channel__c = TEST_BICC_SALES_CHANNEL_INDIRECT,
			Status__c = TEST_DEALER_INFORMATION_STATUS_ACTIVE_STRING,
			Contact__c = contact.Id
		);
	}

	/**
	 * @description:    Retrieves BICC_Dealer_Information__c records
	 * @return			List<BICC_Dealer_Information__c> - List of
	 * 					BICC_Dealer_Information__c records
	 **/
	private static List<BICC_Dealer_Information__c> getBICCDealerInformations() {
		return [
			SELECT Id, Name, Dealer_Code__c, Parent_Dealer_Code__c, Dealer_Contact_Email__c, Sales_Channel__c, Status__c, Error__c, Processed__c
			FROM BICC_Dealer_Information__c
		];
	}

	/**
	 * @description:    Retrieves test Dealer_Information__c fields per field name
	 * @return			Map<String, Schema.SObjectField> - SObject
	 * 					fields per field name
	 **/
	private static Map<String, Schema.SObjectField> getTestDealerInformationSObjectFieldsPerFieldName() {
		return new Map<String, Schema.SObjectField>{
			Dealer_Information__c.Dealer_Contact_Email__c.getDescribe().getName() => Dealer_Information__c.Dealer_Contact_Email__c,
			Dealer_Information__c.Name.getDescribe().getName() => Dealer_Information__c.Name,
			Dealer_Information__c.Sales_Channel__c.getDescribe().getName() => Dealer_Information__c.Sales_Channel__c
		};
	}

	/**
	 * @description:    Retrieves BICC field sync mapping for destination field
	 * @return			Map<String, Field_Sync_Mapping__c> - BICC field sync
	 * 					mapping for destination field
	 **/
	private static Map<String, Field_Sync_Mapping__c> getBICCFieldSyncMappingForDestinationField() {
		return new Map<String, Field_Sync_Mapping__c>{
			Dealer_Information__c.Dealer_Contact_Email__c.getDescribe().getName() => new Field_Sync_Mapping__c(
				Target_Field_Name__c = Dealer_Information__c.Dealer_Contact_Email__c.getDescribe().getName(),
				Source_Field_Name__c = BICC_Dealer_Information__c.Dealer_Contact_Email__c.getDescribe().getName(),
				Insert_Only__c = true,
				Object_Mapping__c = BICC_Dealer_Information__c.sObjectType.getDescribe().getName() +
					'->' +
					Dealer_Information__c.sObjectType.getDescribe().getName()
			),
			Dealer_Information__c.Name.getDescribe().getName() => new Field_Sync_Mapping__c(
				Target_Field_Name__c = Dealer_Information__c.Name.getDescribe().getName(),
				Source_Field_Name__c = BICC_Dealer_Information__c.Name.getDescribe().getName(),
				Insert_Only__c = true,
				Object_Mapping__c = BICC_Dealer_Information__c.sObjectType.getDescribe().getName() +
					'->' +
					Dealer_Information__c.sObjectType.getDescribe().getName()
			),
			Dealer_Information__c.Sales_Channel__c.getDescribe().getName() => new Field_Sync_Mapping__c(
				Target_Field_Name__c = Dealer_Information__c.Sales_Channel__c.getDescribe().getName(),
				Source_Field_Name__c = BICC_Dealer_Information__c.Sales_Channel__c.getDescribe().getName(),
				Insert_Only__c = true,
				Object_Mapping__c = BICC_Dealer_Information__c.sObjectType.getDescribe().getName() +
					'->' +
					Dealer_Information__c.sObjectType.getDescribe().getName()
			)
		};
	}
}
