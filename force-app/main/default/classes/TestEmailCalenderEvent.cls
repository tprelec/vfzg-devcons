@IsTest
public class TestEmailCalenderEvent {
	@IsTest
	private static void testSendEmailCalenderEventInvocable() {		
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        User testUser = new User(FirstName='testFN',LastName='testLN', Email='standarduser@testorg.com');
        Lead testCon = new Lead(LastName='testLN',FirstName='testFN',Email='testem@test.com');
		Test.startTest();
		EmailCalenderEvent.EmailRequest req = new EmailCalenderEvent.EmailRequest();
        req.organizer=testUser;
        req.attendee=testCon;
		req.toAddresses = new List<String>{ 'test1@test.com' };
		req.textBody = 'testitem';
		req.textSubject='test';
		req.eventStart=DateTime.now();
		req.eventEnd=DateTime.now().addHours(1);
		EmailCalenderEvent.sendEmail(new List<EmailCalenderEvent.EmailRequest>{ req });
		Test.stopTest();
		if (EmailService.checkDeliverability()) {
			List<EmailMessage> emails = [SELECT Id FROM EmailMessage];
			System.assert(!emails.isEmpty(), 'Email should be sent');
		}
	}
}