public with sharing class SyncWithOpportunityUtility {
	public static List<Opportunity> opportunities;

	public static void UnSyncProductBasketsAfterInsertUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB) {
		//to be called after insert or after update,
		//if newly updated Product Baskets were synced then un-sync all others which have same Opportunity
		//if newly inserted Product Baskets are synced then un-sync all others which have same Opportunity

		Set<string> setOpportunityId = new Set<string>();
		Set<string> setSyncedProductBasketId = new Set<string>();
		Boolean Pass;

		for (integer i = 0; i < lstNewPB.size(); ++i) {
			Pass = false;

			if (lstOldPB == null) {
				if (lstNewPB[i].csordtelcoa__Synchronised_with_Opportunity__c)
					Pass = true;
			} else {
				if ((lstNewPB[i].csordtelcoa__Synchronised_with_Opportunity__c) && (!lstOldPB[i].csordtelcoa__Synchronised_with_Opportunity__c))
					Pass = true;
			}

			if ((Pass) && (lstNewPB[i].cscfga__Opportunity__c != null)) {
				setOpportunityId.add(lstNewPB[i].cscfga__Opportunity__c);
				setSyncedProductBasketId.add(lstNewPB[i].Id);
			}
		}

		if (setOpportunityId.size() > 0) {
			List<cscfga__Product_Basket__c> lstAllProductBasket = [
				SELECT Id, csordtelcoa__Synchronised_with_Opportunity__c, cscfga__Opportunity__c
				FROM cscfga__Product_Basket__c
				WHERE cscfga__Opportunity__c IN :setOpportunityId
			];

			List<cscfga__Product_Basket__c> lstProductBasketUpdate = new List<cscfga__Product_Basket__c>();

			for (cscfga__Product_Basket__c tmpPB : lstAllProductBasket) {
				if (!setSyncedProductBasketId.contains(tmpPB.Id)) {
					if (tmpPB.csordtelcoa__Synchronised_with_Opportunity__c) {
						tmpPB.csordtelcoa__Synchronised_with_Opportunity__c = false;
						lstProductBasketUpdate.add(tmpPB);
					}
				}
			}

			if (lstProductBasketUpdate.size() > 0)
				update lstProductBasketUpdate;
		}
	}

	public static void DeleteOLIsProductDetailsAfterUpdate(List<cscfga__Product_Basket__c> lstNewPB, List<cscfga__Product_Basket__c> lstOldPB) {
		Set<string> setUnSyncedProductBasketId = new Set<string>();

		for (integer i = 0; i < lstNewPB.size(); ++i) {
			if ((!lstNewPB[i].csordtelcoa__Synchronised_with_Opportunity__c) && (lstOldPB[i].csordtelcoa__Synchronised_with_Opportunity__c)) {
				setUnSyncedProductBasketId.add(lstNewPB[i].Id);
			}
		}

		if (setUnSyncedProductBasketId.size() > 0) {
			ProductUtility.DeleteHardOLIs(setUnSyncedProductBasketId);
		}
	}

	public static void InsertOLIsProductDetailsAfterUpdate(
		List<cscfga__Product_Basket__c> lstNewPB,
		List<cscfga__Product_Basket__c> lstOldPB,
		Map<Id, Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>>> pcOeAttrMap
	) {
		system.debug('**** InsertOLIsProductDetailsAfterUpdate');
		Set<string> setSyncedProductBasketId = new Set<string>();
		Set<string> setSyncedOppId = new Set<string>();

		for (integer i = 0; i < lstNewPB.size(); ++i) {
			if ((lstNewPB[i].csordtelcoa__Synchronised_with_Opportunity__c) && (!lstOldPB[i].csordtelcoa__Synchronised_with_Opportunity__c)) {
				setSyncedProductBasketId.add(lstNewPB[i].Id);
				setSyncedOppId.add(lstNewPB[i].cscfga__Opportunity__c);
			}
		}

		if (setSyncedProductBasketId.size() > 0) {
			//before insert - delete the ones we know for sure will not be needed

			List<cscfga__Product_Basket__c> lstAllProductBasket = [
				SELECT Id, cscfga__Opportunity__c
				FROM cscfga__Product_Basket__c
				WHERE cscfga__Opportunity__c IN :setSyncedOppId
			];

			Set<string> setUnsyncedProductBasketId = new Set<string>();

			for (cscfga__Product_Basket__c tmpProductBasket : lstAllProductBasket) {
				if (!setSyncedProductBasketId.contains(tmpProductBasket.Id)) {
					setUnsyncedProductBasketId.add(tmpProductBasket.Id);
				}
			}

			if (setUnSyncedProductBasketId.size() > 0) {
				ProductUtility.DeleteHardOLIs(setUnSyncedProductBasketId);
			}

			if (setSyncedProductBasketId.size() > 0) {
				system.debug('**** ProductUtility.CreateOLIs');
				//NC - delete existing OLIs for already synched baskets
				ProductUtility.DeleteHardOLIs(setSyncedProductBasketId);
				ProductUtility.CreateOLIs(setSyncedProductBasketId, pcOeAttrMap);
			}
		}
	}

	public static void SynchronizeProfitLossOppFields(cscfga__Product_Basket__c basket) {
		getOpportunities(basket);

		if (basket.ProfitLoss_JSON__c == null) {
			System.debug('**** ProfitLoss_JSON__c is null');
			return;
		}

		Map<String, String> allFields = (Map<String, String>) JSON.deserialize(basket.ProfitLoss_JSON__c, Map<String, String>.class);
		if (allFields == null) {
			System.debug('**** allFields for profit and loss are NULL so sync of P&L parameters to Opportunity wont work');
			return;
		} else if (allFields.size() == 0) {
			System.debug(
				'**** allFields for profit and loss is empty so sync of P&L parameters to Opportunity wont work (all fields are set to zero)'
			);
		}

		opportunities[0].Total_credit_amount_loyalty__c = GetFieldValue('Total credit amount / loyalty bonus', allFields);
		opportunities[0].Total_cost_of_sales__c = GetFieldValue('Total costs of sales', allFields);
		opportunities[0].Revenue_share__c = GetFieldValue('Revenue share', allFields);
		opportunities[0].Overhead_allocation__c = GetFieldValue('Overhead allocation', allFields);
		opportunities[0].Other_Costs__c = GetFieldValue('Other Costs', allFields);
		opportunities[0].Opportunity_cost__c = GetFieldValue('Opportunity cost', allFields);
		opportunities[0].Operator_other_costs_other_migration__c = GetFieldValue('Operator other costs / other migration costs', allFields);
		opportunities[0].Network_Opex__c = GetFieldValue('Network Opex', allFields);
		opportunities[0].Network_Depreciation__c = GetFieldValue('Network Depreciation', allFields);
		opportunities[0].Incremental_OPEX__c = GetFieldValue('Incremental OPEX', allFields);
		opportunities[0].Direct_Capex__c = GetFieldValue('Direct Capex', allFields);
		opportunities[0].Capex__c = GetFieldValue('Capex', allFields);
		opportunities[0].Benchmark__c = GetFieldValue('Benchmark', allFields);
		opportunities[0].A_R__c = GetFieldValue('A&R', allFields);
		opportunities[0].Flexibility__c = GetFieldValue('Flexibility', allFields);

		//update opportunities[0];
	}

	public static void getOpportunities(cscfga__Product_Basket__c basket) {
		if (opportunities == null) {
			opportunities = [
				SELECT
					Id,
					Name,
					Opportunity_Block_Expiration__c,
					Opportunity_Block_Expiration_Date__c,
					Total_credit_amount_loyalty__c,
					Total_cost_of_sales__c,
					Revenue_share__c,
					Overhead_allocation__c,
					Other_Costs__c,
					Opportunity_cost__c,
					Operator_other_costs_other_migration__c,
					Network_Opex__c,
					Network_Depreciation__c,
					Incremental_OPEX__c,
					Direct_Capex__c,
					Capex__c,
					Benchmark__c,
					A_R__c,
					No_Contract__c
				FROM Opportunity
				WHERE Id = :basket.cscfga__Opportunity__c
			];
		}
	}

	public static void SyncBlockExpiration(cscfga__Product_Basket__c basket) {
		System.debug('**** SyncBlockExpiration');
		getOpportunities(basket);

		opportunities[0].Opportunity_Block_Expiration__c = basket.Block_Expiration__c;
		opportunities[0].Opportunity_Block_Expiration_Date__c = basket.Block_Expiration__c ? basket.Contract_end_Mobile__c : null;

		//update opportunities[0];
	}

	public static void setOpportunitNoContract(cscfga__Product_Basket__c basket, List<cscfga__Product_Configuration__c> configurations) {
		Boolean allNewDeliveryModel = true;
		// we don't change No_Contract__c for legacy opportunities
		System.debug('Basket New portfolio: ' + basket.New_Portfolio__c);
		if (!basket.New_Portfolio__c) {
			return;
		}
		getOpportunities(basket);
		for (cscfga__Product_Configuration__c configuration : configurations) {
			if (!configuration.New_Delivery_Model__c) {
				allNewDeliveryModel = false;
				break;
			}
		}
		System.debug('allNewDeliveryModel: ' + allNewDeliveryModel);
		opportunities[0].No_Contract__c = allNewDeliveryModel;
	}

	public static void syncOpportunityFields(cscfga__Product_Basket__c basket, List<cscfga__Product_Configuration__c> configurations) {
		SynchronizeProfitLossOppFields(basket);
		SyncBlockExpiration(basket);
		setOpportunitNoContract(basket, configurations);
		System.debug('Before oppty update: ' + opportunities[0]);
		update opportunities[0];
	}

	public static void syncOpportunityFieldsOnlineScenario(cscfga__Product_Basket__c basket) {
		//SynchronizeProfitLossOppFields(basket);
		SyncBlockExpiration(basket);
		//setOpportunitNoContract(basket,configurations);
		update opportunities[0];
	}

	private static Decimal GetFieldValue(string fieldName, Map<string, string> allFields) {
		return changeStringToDecimal(allFields.get(fieldName));
	}

	// changes P&L values locale format to "standard" - replaces commas and dots where needed
	public static Decimal changeStringToDecimal(String totalString) {
		if (totalString == null || totalString == '')
			return 0;

		totalString = totalString.replace('.', '');

		if (totalString.substring(totalString.length() - 3, totalString.length() - 2) == ',') {
			totalString = totalString.replace(',', '.');
		}
		Decimal total = Decimal.valueOf(totalString);

		return total;
	}
}
