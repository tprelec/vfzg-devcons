/**
 * @description       : Apex Class Handler of the Category Trigger
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

public class CategoryTriggerHandler extends TriggerHandler {
	private List<Category__c> lstNewCategory;

	private Map<Id, Category__c> mapOldCategory;
	private Map<Id, Category__c> mapNewCategory;

	/**
	 * @description Initialize Class Variables
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	private void init() {
		lstNewCategory = (List<Category__c>) this.newList;
		mapOldCategory = (Map<Id, Category__c>) this.oldMap;
		mapNewCategory = (Map<Id, Category__c>) this.newMap;
	}

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public void setExternalIds() {
		Sequence objSequence = new Sequence('Category');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (Category__c objCategory : lstNewCategory) {
				objCategory.ExternalID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
