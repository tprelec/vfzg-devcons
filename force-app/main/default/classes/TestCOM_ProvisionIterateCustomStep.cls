@isTest
private class TestCOM_ProvisionIterateCustomStep {

	@IsTest
    static void testProvisionIterateCustomStep() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            ord.csord__Account__c = testAccount.Id;
            insert ord;

            Site__c testSite = CS_DataTest.createSite('Test Site',testAccount,'1032AB','Street','City',10.0);
            testSite.Footprint__c = null;
            insert testSite;
            
            COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', ord.Id, false);
            deliveryOrder1.Product_Abbreviations__c = 'ACCESS,ZIPRO';
            deliveryOrder1.Site__c = testSite.Id;
            insert deliveryOrder1;

            csord__Subscription__c subscription= CS_DataTest.createSubscription(testConfConf.Id);
            subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            subscription.csord__Order__c = ord.Id;
            insert subscription;

            csord__Service__c parentService= CS_DataTest.createService(testConfConf.Id,subscription, 'Service Created');
            parentService.csord__Identification__c = 'testSubscription';
            parentService.COM_Delivery_Order__c = deliveryOrder1.Id;
            parentService.csord__Order__c = ord.Id;
            parentService.VFZ_Product_Abbreviation__c = 'ZIPRO';
            insert parentService;

            List<Delivery_Component__c> deliveryComponents = CS_DataTest.createDeliveryComponents(parentService,'TestComponent', 3, false);
            for (Delivery_Component__c dc : deliveryComponents) {
                dc.COM_Delivery_Order__c = deliveryOrder1.Id;
                dc.MACD_Action__c = 'Add';
                dc.Article_Name__c = 'Coax/HFC';
                dc.Article_Code__c = 'Coax_HFC';
            }
            insert deliveryComponents;

            List<Delivery_Component_Attribute__c> deliveryComponentAttributes = CS_DataTest.createDeliveryComponentAttributes(5, false, 'TestAttribute', '');

            for (Delivery_Component__c deliveryComponent : deliveryComponents) {
                for (Delivery_Component_Attribute__c attribute : deliveryComponentAttributes) {
                    attribute.Delivery_Component__c = deliveryComponent.Id;
                }
            }
            insert deliveryComponentAttributes;

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', false);
            testProcessTemplate.MACD_Flow_Type__c = 'New Add Provisioning';
            insert testProcessTemplate;
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.COM_Delivery_Order__c = deliveryOrder1.Id;
            testProcess.Order__c = ord.Id;
            insert testProcess;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, false);
            step1.COM_Task_Status__c = 'Closed - Planning Rescheduled';
            //step1.CSPOFA__Case_Record_Type__c = 'CS COM Install DeliveryOrder';
            insert step1;

            Task testTask = CS_DataTest.createTask('Provisioning robot', 'COM Delivery', simpleUser, false);
            testTask.WhatId = deliveryOrder1.Id;
            testTask.Type = 'COM_Provisioning';
            //testCase1.Order__c = ord.Id;
            testTask.CSPOFA__Orchestration_Step__c = step1.Id;
            insert testTask;
            
            COM_ProvisionIterateCustomStep provisionIterateCustomStep = new COM_ProvisionIterateCustomStep();
            
            Boolean processPassed = true;
            try {
                COM_ProvisionIterateCustomStep.getAlldeliveryComponentNamesByDeliveryOrder(deliveryOrder1.Id, deliveryComponents);
                COM_ProvisionIterateCustomStep.getAllDeliveryComponents(new Set<Id>{deliveryOrder1.Id});
                COM_ProvisionIterateCustomStep.getComponentActivities();
                
                provisionIterateCustomStep.getComponentActivitiesByDeliveryOrder(COM_ProvisionIterateCustomStep.getAlldeliveryComponentNamesByDeliveryOrder(deliveryOrder1.Id, deliveryComponents), COM_ProvisionIterateCustomStep.getComponentActivities());
                CS_OrchestratorStepUtility.getStepList(new List<SObject>{step1});
                provisionIterateCustomStep.kickOffSubprocess(new List<CSPOFA__Orchestration_Step__c>{step1});
                provisionIterateCustomStep.process(new List<SObject>{step1});
                provisionIterateCustomStep.createProcessRequest(step1, 'Test');
            } catch (Exception ex) {
                processPassed = false;
            }
            System.assertEquals(true, processPassed);
            
            String result = provisionIterateCustomStep.getQueueNameFromProcessName('Test Name');
            System.assertEquals('', result);
            
            COM_Delivery_Component_Activity__mdt activity = new COM_Delivery_Component_Activity__mdt();
            activity.Provisioning_Queue__c = 'Test';
            activity.Deprovisioning_Queue__c = 'Test2';
            activity.Change_Provisioning_Queue__c = 'Test3';
            activity.Reverse_Provisioning_Queue__c = 'Test4';
            
            System.assertEquals('COM Test', provisionIterateCustomStep.getQueueBasedOnMACDType('Provisioning', activity));
            System.assertEquals('COM Test2', provisionIterateCustomStep.getQueueBasedOnMACDType('Deprovisioning', activity));
            System.assertEquals('COM Test3', provisionIterateCustomStep.getQueueBasedOnMACDType('Change', activity));
            System.assertEquals('COM Test4', provisionIterateCustomStep.getQueueBasedOnMACDType('Reverse Provisioning', activity));

            activity.Provisioning_Queue__c = null;
            System.assertEquals(null, provisionIterateCustomStep.getQueueBasedOnMACDType('Provisioning', activity));
        }
    }

}