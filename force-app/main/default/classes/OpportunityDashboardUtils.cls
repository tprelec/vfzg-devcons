public with sharing class OpportunityDashboardUtils {

  public String filters;
  public String exclusions;
  public String inclusions;
  public Integer fiscalYearInteger;
  public Set<String> visibleRoleNames;
  public String soqlFilters;
  public String roleField;

  public static String ceoRole = 'Director B2B';
  public static String ceoRoleId {
    get{
      return GeneralUtils.roleNameToRoleId.get(ceoRole);
    }
    set;
  }
  private Set<String> roles;
  private Set<String> excludedRoles;
  private Set<String> includedRoles;

  public OpportunityDashboardUtils() {
      filters = ApexPages.CurrentPage().getParameters().get('filters');
      exclusions = ApexPages.CurrentPage().getParameters().get('exclusions');
      inclusions = ApexPages.CurrentPage().getParameters().get('inclusions');
      String fiscalYearString = ApexPages.CurrentPage().getParameters().get('fiscalYear');
      if(fiscalYearString != null) fiscalYearInteger = Integer.valueOf(fiscalYearString);
      Id runAsUserId;
      if(ApexPages.CurrentPage().getParameters().get('runAsUserId') != '') 
        runAsUserId = ApexPages.CurrentPage().getParameters().get('runAsUserId');

      visibleRoleNames = new Set<String>();
      if(runAsUserId != null){
        visibleRoleNames = GeneralUtils.getAllSubRoleNames(new Set<Id>{runAsUserId});   
        visibleRoleNames.add(GeneralUtils.RoleIdToRoleName.get(runAsUserId)); 
      }


      soqlFilters = ApexPages.CurrentPage().getParameters().get('soqlFilter');
      system.debug(ApexPages.CurrentPage().getParameters());
      if(ApexPages.CurrentPage().getParameters().containsKey('sourcePage') && ApexPages.CurrentPage().getParameters().get('sourcePage').contains('specialist')){
        roleField = 'RoleCreateOrChangeSolutionSales__c';
      } else {
        roleField = 'RoleCreateOrChangeOwner__c';
      }

      system.debug(filters);
      system.debug(exclusions);
      system.debug(inclusions);
      system.debug(fiscalYearInteger);
      system.debug(runAsUserId);
      system.debug(hiddenProductFamilies);
      system.debug(visibleRoleNames);
      system.debug(soqlFilters);
  }

  public List<OpportunityLineItem> oppLineItems{
    get{
      List<OpportunityLineItem> returnList = new List<OpportunityLineItem>();
      system.debug(hiddenProductFamilies);

      String oppLiQuery = 'Select Id, Name, OpportunityId, Opportunity.Name, Opportunity.Owner.Name, Product2.VF_Product__r.Family__c, Product2.Taxonomy__r.Product_Family__c,TotalPrice, Quantity, MRR1__c, ';
      oppLiQuery += 'Opportunity.StageName, Opportunity.CloseDate, Opportunity.RoleCreateOrChangeOwner__c, ';
      oppLiQuery += 'Opportunity.RoleCreateOrChangeSolutionSales__c ,Opportunity.Account.Name, Opportunity.AccountId ';
      // first apply soql filters
      oppLiQuery += soqlFilters;
      // then apply pivot filters
      system.debug(oppLiQuery);

      for(String s : filters.split(',')){
        List<String> filterparts = s.split('=');
        if(filterparts[0] == 'Type'){
          if(filterparts[1] == 'Commit'){
            oppLiQuery += ' AND Opportunity.StageName in (\'Closed Won\',\'Closing\',\'Closing by SAG\') ';
          } else if(filterparts[1] == 'Closed Won'){
            oppLiQuery += ' AND Opportunity.StageName = \'Closed Won\' ';
          } else if(filterparts[1] == 'Target'){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Drilldown on targets is not supported yet.'));
            return null;
          } else if(filterparts[1] == 'Gap'){
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Drilldown on gap is not supported yet.'));
            return null;
          } else if(filterparts[1] == 'Offer'){
            oppLiQuery += ' AND Opportunity.StageName = \'Offer\' ';
          } else if(filterparts[1] == 'Funnel'){                                          
            oppLiQuery += ' AND Opportunity.StageName in (\'Prospect\',\'Qualify\') ';
          } else if(filterparts[1] == 'Coverage'){  
            ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Drilldown on coverage is not supported yet.'));
            return null;
          }

        } else if (filterparts[0] == 'Period'){
          oppLiQuery += ' AND FISCAL_QUARTER(Opportunity.CloseDate) = ' +filterparts[1].subString(1,2);
        } else if (filterparts[0] == 'Vertical'){
          oppLiQuery += ' AND Opportunity.'+roleField+' LIKE \'%' +filterparts[1]+'%\'';
        } else if (filterparts[0] == 'Segment'){
          system.debug(filterparts[1]);
          if(filterparts[1] == 'Other'){
            // fetch all mapped roles and all partner roles to create a non-mapping list
            roles = GeneralUtils.RoleToSegment.keySet();
            oppLiQuery += ' AND (NOT Opportunity.'+roleField+' in :roles) ';
            oppLiQuery += ' AND (NOT Opportunity.RoleCreateOrChangeOwner__c LIKE \'%Partner User\')   ';
          } else if(filterparts[1].contains('Business Partner')){
            // all bp roles
            oppLiQuery += ' AND Opportunity.RoleCreateOrChangeOwner__c like \'%Partner User\' ';
          } else if(filterparts[1].contains('Enterprise Services')){
            oppLiQuery += ' AND Product2.VF_Product__r.Family__c = \'Enterprise Services\' '; // TAXONOMY HAZARD!
          } else if(filterparts[1].contains('Cloud Productivity')){
            oppLiQuery += ' AND Product2.VF_Product__r.Family__c = \'Microsoft Solutions\' '; // TAXONOMY HAZARD!
          } else {

            roles = GeneralUtils.SegmentToRoles.get(filterparts[1]);
            system.debug(GeneralUtils.SegmentToRoles);


            system.debug(roles);
            if(roles == null){
              // we might be in ceo-mode. Try main segments instead of segments
              roles = GeneralUtils.MainSegmentToRoles.get(filterparts[1]);
            }
            if(roles == null) {
              // if still not found raise an error
              ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Segment not found in Role-Segment mapping.'));
              return null;
            }
            oppLiQuery += ' AND Opportunity.'+roleField+' in :roles ';
          }
        } else if (filterparts[0] == 'ProductFamily'){
          String productFamily = filterparts[1];
          String subfamily;
          // split off the subfamily for enterprise services
          if(productFamily.contains(' - ')){
            subfamily = productFamily.split(' - ')[1];
            productFamily = productFamily.split(' - ')[0];
          }
          if(productFamily == 'null') {
              oppLiQuery += ' AND Product2.Taxonomy__r.Product_Family__c = null ';              
          } else {
              //oppLiQuery += ' AND Product2.VF_Product__r.Family__c = \''+productFamily+ '\'';
              oppLiQuery += ' AND Product2.Taxonomy__r.Product_Family__c = \''+productFamily+ '\'';              
          }


          if(subfamily != null){
            oppLiQuery += ' AND PricebookEntry.Product2.Model__c = \''+subfamily+ '\'';
          }
        } else if (filterparts[0] == 'AccountManager'){
          oppLiQuery += ' AND (Opportunity.Owner.Name = \''+filterparts[1]+ '\'';
          oppLiQuery += ' OR Opportunity.Solution_Sales__r.Name = \''+filterparts[1]+ '\'';
          oppLiQuery += ' OR Opportunity.Hidden_Inside_Sales_Specialist__r.Name = \''+filterparts[1]+ '\'';
          oppLiQuery += ' OR Opportunity.Hidden_Microsoft_Solution_Specialist__r.Name = \''+filterparts[1]+ '\'';
          oppLiQuery += ' OR Opportunity.Hidden_Ent_Services_Solution_Specialist__r.Name = \''+filterparts[1]+ '\')';
        }
      }

      // then apply pivot inclusions (only used for segments)
      system.debug(inclusions);

      for(String s : inclusions.unescapeHtml4().split('\\|')){ // used a pipe to split the different kinds of exclusions, to differentiate from comma if multiple values are selected within 1 type of exclusion
        
        // remove leading comma
        if(s.startsWith(',')) s = s.right(s.length()-1);
        system.debug(s);

        List<String> filterparts = s.split('=');

        if (filterparts[0] == 'Segment'){
          includedRoles = new Set<String>();
          Boolean filterContainsOther = false;
          Boolean filterContainsPartner = false;
          for(String p : filterparts[1].split(',')){
            system.debug(p);
            if(p == 'Other'){
              filterContainsOther = true;
            } else if(p.contains('Business Partner')){
              filterContainsPartner = true;
            } else {
              Set<String> newRoles = GeneralUtils.SegmentToRoles.get(p);
              if(newRoles == null){
                // we might be in ceo-mode. Try main segments instead of segments
                newRoles = GeneralUtils.MainSegmentToRoles.get(filterparts[1]);
              }
              if(newRoles == null) {
                // if still not found raise an error
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Segment not found in Role-Segment mapping.'));
                return null;
              }            
              includedRoles.addAll(newRoles);
            }
          }
          oppLiQuery += ' AND (Opportunity.'+roleField+' in :includedRoles )';

          // if 'Other' is part of the included roles, the role has to be in one of the mapped roles
          if(filterContainsOther){
            // fetch all mapped roles to create a non-mapping list
            roles = GeneralUtils.RoleToSegment.keySet();
            oppLiQuery += ' AND NOT (Opportunity.'+roleField+' in :roles) ';
          }
          if(filterContainsPartner){
            oppLiQuery += ' AND (Opportunity.RoleCreateOrChangeOwner__c like \'%Partner User\') ';  
          }
          system.debug(includedRoles);
          
        } 
      }  

      // then apply pivot exclusions
      system.debug(exclusions);

      for(String s : exclusions.unescapeHtml4().split('\\|')){ // used a pipe to split the different kinds of exclusions, to differentiate from comma if multiple values are selected within 1 type of exclusion
        
        // remove leading comma
        if(s.startsWith(',')) s = s.right(s.length()-1);
        system.debug(s);

        List<String> filterparts = s.split('=');
        if(filterparts[0] == 'Type'){
          for(String p : filterparts[1].split(',')){
            if(p == 'Commit'){
              oppLiQuery += ' AND Opportunity.StageName NOT in (\'Closed Won\',\'Closing\',\'Closing by SAG\') ';
            } else if(p == 'Closed Won'){
              oppLiQuery += ' AND Opportunity.StageName != \'Closed Won\' ';
            } else if(p == 'Target'){
              ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Drilldown on targets is not supported yet.'));
              return null;
            } else if(p == 'Gap'){
              ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Drilldown on gap is not supported yet.'));
              return null;
            } else if(p == 'Offer'){
              oppLiQuery += ' AND Opportunity.StageName != \'Offer\' ';
            } else if(p == 'Funnel'){                                          
              oppLiQuery += ' AND Opportunity.StageName NOT in (\'Prospect\',\'Qualify\') ';
            } else if(p == 'Coverage'){  
              ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Drilldown on coverage is not supported yet.'));
              return null;
            }
          }
        } else if (filterparts[0] == 'Period'){
          for(String p : filterparts[1].split(',')){
            system.debug(p);
            oppLiQuery += ' AND FISCAL_QUARTER(Opportunity.CloseDate) != '+p.subString(1,2);
          }          
          //oppLiQuery += ' AND FISCAL_QUARTER(Opportunity.CloseDate) != ' +filterparts[1].subString(1,2);
        } else if (filterparts[0] == 'Vertical'){
          for(String p : filterparts[1].split(',')){
            oppLiQuery += ' AND (NOT Opportunity.'+roleField+' LIKE \'' +p+'%\')';
          }
        } else if (filterparts[0] == 'Segment'){
          excludedRoles = new Set<String>();
          Boolean filterContainsOther = false;
          Boolean filterContainsPartner = false;
          for(String p : filterparts[1].split(',')){
            system.debug(p);
            if(p == 'Other'){
              filterContainsOther = true;
            } else if(p.contains('Business Partner')){
              filterContainsPartner = true;
            } else {
              Set<String> newRoles = GeneralUtils.SegmentToRoles.get(p);
              if(newRoles == null){
                // we might be in ceo-mode. Try main segments instead of segments
                newRoles = GeneralUtils.MainSegmentToRoles.get(filterparts[1]);
              }
              if(newRoles == null) {
                // if still not found raise an error
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Segment not found in Role-Segment mapping.'));
                return null;
              }            
              excludedRoles.addAll(newRoles);
            }
          }
          oppLiQuery += ' AND (NOT Opportunity.'+roleField+' in :excludedRoles )';

          // if 'Other' is part of the excluded roles, the role has to be in one of the mapped roles
          if(filterContainsOther){
            // fetch all mapped roles to create a non-mapping list
            roles = GeneralUtils.RoleToSegment.keySet();
            oppLiQuery += ' AND Opportunity.'+roleField+' in :roles ';
          }
          if(filterContainsPartner){
            oppLiQuery += ' AND NOT(Opportunity.RoleCreateOrChangeOwner__c like \'%Partner User\') ';  
          }
          system.debug(excludedRoles);
          
          //for(String p : filterparts[1].split(',')){
          //  oppLiQuery += ' AND Opportunity.Segment__c != \''+p+ '\'';
          //}
        } else if (filterparts[0] == 'ProductFamily'){
          for(String p : filterparts[1].split(',')){

            String productFamily = p;
            String subfamily;
            // split off the subfamily for enterprise services
            if(productFamily.contains(' - ')){
              subfamily = productFamily.split(' - ')[1];
              productFamily = productFamily.split(' - ')[0];
            }
            if(productFamily == 'null') {
                oppLiQuery += ' AND Product2.Taxonomy__r.Product_Family__c != null ';              
            } else {
              //oppLiQuery += ' AND Product2.VF_Product__r.Family__c != \''+productFamily+ '\'';
              oppLiQuery += ' AND Product2.Taxonomy__r.Product_Family__c != \''+productFamily+ '\'';
            }            


            if(subfamily != null){
              oppLiQuery += ' AND PricebookEntry.Product2.Model__c != \''+subfamily+ '\'';
            }

          }
        } else if (filterparts[0] == 'AccountManager'){
          for(String p : filterparts[1].split(',')){
            system.debug(p);
            oppLiQuery += ' AND (Opportunity.Owner.Name != \''+p+ '\'';
            oppLiQuery += ' AND Opportunity.Solution_Sales__r.Name != \''+p+ '\'';
            oppLiQuery += ' AND Opportunity.Hidden_Inside_Sales_Specialist__r.Name != \''+p+ '\'';
            oppLiQuery += ' AND Opportunity.Hidden_Microsoft_Solution_Specialist__r.Name != \''+p+ '\'';
            oppLiQuery += ' AND Opportunity.Hidden_Ent_Services_Solution_Specialist__r.Name != \''+p+ '\')';
          }          
        }
      }  
      oppLiQuery += ' LIMIT 10000 ';    
      
      system.debug(fiscalYearInteger);
      system.debug(hiddenProductFamilies);
      system.debug(visibleRoleNames);
      system.debug(roles);
      system.debug(oppLiQuery);

      returnList = Database.query(oppLiQuery);
      TCVTotal = 0;
      CTNTotal = 0;
      MRRTotal = 0;
      for(OpportunityLineItem oli : returnList){
        TCVTotal += oli.TotalPrice;
        CTNTotal += oli.Quantity;
        MRRTotal += oli.MRR1__c;
      }

      return returnList;
    }
    set;
  }

  public Decimal TCVTotal {get;set;}
  public Decimal CTNTotal {get;set;}
  public Decimal MRRTotal {get;set;}

  public static final List<String> ctnProductFamilies = new List<String>{'Acq Voice','Ret Voice','Voice'}; 
  public static final List<String> enterpriseServiceLicenseProductSubFamilies = new List<String>{'EMM Licenses'}; 

  public static final List<String> solSalesProductFamilies = new List<String>{'Fixed Voice','Fixed Data','PBX','One Net','Microsoft Solutions','Enterprise Services','Ziggo Only','Ziggo Xsell'};  
  public static final List<String> hiddenProductFamilies = new List<String>{''}; //'Fixed Data',

  public static String roleToVertical (String roleString){
    if(roleString != null){
      return roleString.replace('Account Manager','').replace('Sales Manager','').replace(' - Dealer Partner User','').replace('Solution Sales','').trim();
    } else {
      return 'None';
    }
  }  

  public static Set<String> solSalesRoles {
      get{
          if(solSalesRoles == null){
              solSalesRoles = new Set<String>();
              for(String role : GeneralUtils.MainSegmentToRoles.get('Solution Sales')){
                  solSalesRoles.add(role);
              }
          }
          return solSalesRoles;
      }
      set;
  }  

  public static final List<String> customSegmentOrder = new List<String>{'Corporate Government Local',
                                                                      'Corporate Government National',
                                                                      'Corporate Healthcare & Insurance',
                                                                      'Corporate Healthcare & Hospitals',
                                                                      'Corporate Commercial Industries',
                                                                      'Corporate Commercial Services',
                                                                      'Corporate Commercial New Business',
                                                                      'Corporate Commercial Large Enterprise',
                                                                      'Corporate IAM'
                                                                    };
  public static final List<String> customRoleOrder = new List<String>{                                                                      
                                                                      'Government Local Sales Manager',
                                                                      'Government National Sales Manager',
                                                                      'Healthcare & Insurance Sales Manager',
                                                                      'Healthcare & Hospitals Sales Manager',
                                                                      'Commercial Industries Sales Manager',
                                                                      'Commercial Services Sales Manager',
                                                                      'Commercial New Business Sales Manager',
                                                                      'Commercial Large Enterprise Sales Manager',
                                                                      'IAM Sales Manager'
                                                                    };

    public static List<SelectOption> getDashboardTypeOptions(){
        List<SelectOption> returnList = new List<SelectOption>();
        if(GeneralUtils.checkVisualforcePageAccess('OpportunityDashboardBRM',null)) returnList.add(new SelectOption('director','Sales Director Dashboard'));
        if(GeneralUtils.checkVisualforcePageAccess('OpportunityDashboardBRMSM',null)) returnList.add(new SelectOption('salesmanager','Sales Manager Dashboard'));
        if(GeneralUtils.checkVisualforcePageAccess('OpportunityDashboardBRMSpecialist',null)) returnList.add(new SelectOption('specialist','Solution Sales Dashboard')); 

        return returnList;

    }

    public static PageReference dashboardVFPage(String dashboardTypeSelected){
      if(dashboardTypeSelected == 'director'){
        return Page.OpportunityDashboardBRM;
      } else if(dashboardTypeSelected == 'salesmanager'){
        return Page.OpportunityDashboardBRMSM;
      } else if(dashboardTypeSelected == 'specialist'){
      return Page.OpportunityDashboardBRMSpecialist;
      } else return null;
    }

    public pageReference loadDefaultDashboard(){
      // first determine based on role
      if(GeneralUtils.getAllCommercialDirectorRoleIds().contains(UserInfo.getUserRoleId())) {
        return dashboardVFPage('director');
      } else if (GeneralUtils.getAllSalesManagerRoleIds().contains(UserInfo.getUserRoleId())){
        return dashboardVFPage('salesmanager');
      } else if(!getDashboardTypeOptions().isEmpty()){
        // if no director or salesmgr, just pick the top one
        return dashboardVFPage(getDashboardTypeOptions()[0].getValue());
      } else {
        Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'You do not have any BRM dashboard access. Please contact your administrator if you think you should have access.'));
        return null;
      }

    }

    public static String getBrmSettingCookieString(String dashboardName,String runAsUserRoleId, String fiscalYear,String dataType){
      String brmSettingCookieString = dashboardName+'|';
      brmSettingCookieString += runAsUserRoleId+'|';
      brmSettingCookieString += fiscalYear+'|';
      brmSettingCookieString += dataType;

      return brmSettingCookieString;
    }


    public static List<String> getBrmSettingCookie(String cookieString){
      List<String> output = cookieString.split('\\|');
      return output;
    }
}