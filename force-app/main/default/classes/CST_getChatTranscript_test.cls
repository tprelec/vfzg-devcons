@isTest
public class CST_getChatTranscript_test {
	@testSetup
	public static void testSetup() {
		List<Id> members = new List<Id>();

		for (GroupMember m : [SELECT UserOrGroupId, id FROM GroupMember WHERE group.developername = 'CST_Technical_Services']) {
			members.add(m.UserOrGroupId);
		}

		User usr;

		if (members != null && members.size() > 0) {
			usr = [SELECT id, username FROM user WHERE id IN :members LIMIT 1];
		}

		Account acc1 = TestUtils.createAccount(usr);
		Account acc2 = TestUtils.createAccount(usr);
		Contact con = TestUtils.createContact(acc1);
		Contact con2 = TestUtils.createContact(acc2);
		Case newCase = TestUtils.createCase(acc1);

		LiveChatVisitor lcv = new LiveChatVisitor();

		try {
			insert lcv;
		} catch (Exception e) {
			System.debug(' Exception caught: ' + e.getMessage());
		}

		LiveChatTranscript lct = new LiveChatTranscript();
		lct.LiveChatVisitorid = lcv.id;
		// lct.contactId = con.id;
		// lct.accountId = acc1.id;
		lct.caseId = newCase.id;
		lct.body = '<p>stuff</p>';

		try {
			insert lct;
		} catch (Exception e) {
			System.debug(' Exception caught: ' + e.getMessage());
		}

		LiveChatTranscriptEvent le = new LiveChatTranscriptEvent();
		le.LiveChatTranscriptId = lct.id;
		le.type = 'ChatRequest';
		le.time = system.now();

		try {
			insert le;
		} catch (Exception e) {
			System.debug(' Exception caught: ' + e.getMessage());
		}
	}

	@isTest
	public static void test_DownloadPDF() {
		Test.startTest();

		PageReference pageRef = Page.CST_getChatTranscript;
		Case newCase = [SELECT id FROM case LIMIT 1];

		pageRef.getParameters().put('Id', String.valueOf(newCase.Id));
		system.debug('pageRef: ' + pageRef);

		Test.setCurrentPage(pageRef);

		ApexPages.StandardController sc = new ApexPages.StandardController(newCase);
		CST_getChatTranscript testAccPlan = new CST_getChatTranscript(sc);

		Test.stopTest();
	}
	@isTest
	public static void test_UpdateTranscriptAccount() {
		LiveChatTranscript lct = [SELECT id, AccountId, ContactId FROM LiveChatTranscript LIMIT 1];
		//Contact con1 = [select id from Contact where id = :lct.ContactId limit 1];
		// Contact con2 = [select id from Contact where id != :lct.ContactId limit 1];
		List<Contact> conts = [SELECT id FROM Contact];
		Account acc = [SELECT id FROM Account WHERE id != :lct.AccountId LIMIT 1];

		User usr = [SELECT id FROM user WHERE name = 'Automated Process' LIMIT 1];

		Test.startTest();
		System.runAs(usr) {
			lct.ContactId = conts[0].id;
			update lct;
			lct.ContactId = conts[1].id;
			update lct;
		}

		Test.stopTest();
	}
}
