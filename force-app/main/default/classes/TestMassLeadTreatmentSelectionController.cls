@isTest
private class TestMassLeadTreatmentSelectionController {
	
	@isTest static void testMethodOne() {
		// Implement test code

		//create lead with lead.Treatment_01__c - lead.Treatment_10__c
		Lead myLead = TestUtils.createLead();

		myLead.Treatment_01__c = 'firstTreatment';
		myLead.Treatment_02__c = 'secondTreatment';
		myLead.Treatment_03__c = 'thirdTreatment';

		update myLead;

		//initiate controller
		ApexPages.StandardController stdController = new ApexPages.StandardController(myLead);
		
		Test.setCurrentPageReference(new PageReference('Page.myPage'));
		System.currentPageReference().getParameters().put('LeadId', myLead.id);

		MassLeadTreatmentSelectionController cntrlr = new MassLeadTreatmentSelectionController(stdController);

		//set it's LeadId parameter

		

		//call changeTreatment
		cntrlr.changeTreatment();


		//in filteredLeadsW have some leads have .selected = true
		for(MassLeadTreatmentSelectionController.LeadWrapper oneLeadW : cntrlr.filteredLeadsW){

			oneLeadW.selected = true;
				
		}

		//call updateTreatments
		cntrlr.updateTreatments();

		//call updateStatuses
		cntrlr.updateStatuses();
		
		//call getSelected
		cntrlr.getSelected();
		
		//call reloadWithNewParameter
		cntrlr.reloadWithNewParameter();
		
		//refreshFilters
		cntrlr.refreshFilters();

		String getStuff = cntrlr.selectedTreatment;
		cntrlr.selectedTreatment = 'test';

		String getStuff2 = cntrlr.selectedStatus ;
		cntrlr.selectedStatus  = 'test';

		List<SelectOption> statuses = cntrlr.statuses;

	}

	
	

	


	
}