/**
 * @author: Rahul Sharma (07-09-2021)
 * @description: ISW Site Check outbound Rest service
 *      TestSitePostalCheckEventTriggerHandler covers unit test of this class
 *      Jira ticket reference: COM-1960
 * Note:
 *      WSDLtoApex doesn't support SOAP version 1.1 which was provided by ISW interface team, due to which a REST call is being made.
 */

public with sharing class ISWSiteCheckRest {

    @TestVisible
    private static final String ENDPOINT_URL =
        'callout:ISW_Site_Check_Credentials';

    @TestVisible
    private static final String TCP_SERVICE_FALSE = 'false';
    @TestVisible
    private static final String ORDERING_POSSIBLE_Y = 'Y';
    @TestVisible
    private static final String SERVICE_TYPE_GIGA = 'GIGA';
    @TestVisible
    private static final String ERROR_CODE_0 = '0';
    @TestVisible
    private static final Integer HTTP_STATUS_CODE_200 = 200;

    private static final String SOAP_RESPONSE_NAMESPACE = 'http://xy.spatial-eye.com/soap';

    public Response makeCall(Request request) {
        try {
            Id siteId = request.siteId;
            String requestBody = getRequestBody(siteId, request);
            System.debug('@@@@requestBody: ' + requestBody);

            HttpRequest req = new HttpRequest();
            // no username and password is needed/provided by ISW team
            // authentication happens with certificate shared between Salesforce and ISW, its attached to named credentials with which ISW authorizes the API call
            req.setEndpoint(ENDPOINT_URL);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/soap+xml');
            req.setHeader('SOAPAction', 'GetServiceAvailabilityRequest_2');
            req.setBody(requestBody);
            req.setTimeout(60000);
            HTTPResponse response = new Http().send(req);

            System.debug('@@@@response.getBody(): ' + response.getBody());

            if (response.getStatusCode() == HTTP_STATUS_CODE_200) {
                try {
                    return parseResponse(response.getBody());
                } catch (Exception objParsingException) {
                    return new Response(
                        System.Label.ISWSiteCheck_ResponseFormatNotValid
                    );
                }
            } else {
                return new Response(
                    getHttpFailureMessage(response.getStatusCode())
                );
            }
        } catch (Exception objEx) {
            return new Response(objEx.getMessage());
        }
    }

    private String getRequestBody(Id siteId, Request request) {
        return '<?xml version="1.0" encoding="UTF-8"?>' +
            '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope" xmlns:soap="http://xy.spatial-eye.com/soap">' +
            '<soapenv:Body>' +
            '<soap:GetServiceAvailabilityRequest_2>' +
            '<soap:Header>' +
            '<soap:BusinessTransactionID>' +
            siteId +
            '</soap:BusinessTransactionID>' +
            '<soap:SentTimestamp>' +
            DateTime.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX') +
            '</soap:SentTimestamp>' +
            '<soap:SourceContext>' +
            '<soap:Host>' +
            System.URL.getSalesforceBaseUrl().getHost() +
            '</soap:Host>' +
            '<soap:Application>SFDC</soap:Application>' +
            '</soap:SourceContext>' +
            '</soap:Header>' +
            '<soap:Body>' +
            '<soap:EXACTMATCHFLAG>Y</soap:EXACTMATCHFLAG>' +
            '<soap:HOUSENUMBER>' +
            request.houseNumber +
            '</soap:HOUSENUMBER>' +
            (String.isEmpty(request.houseNumberExtension)
                ? ''
                : '<soap:HOUSENUMBEREXTENSION>' +
                  request.houseNumberExtension +
                  '</soap:HOUSENUMBEREXTENSION>') +
            '<soap:POSTCODE>' +
            request.postCode +
            '</soap:POSTCODE>' +
            '</soap:Body>' +
            '</soap:GetServiceAvailabilityRequest_2>' +
            '</soapenv:Body>' +
            '</soapenv:Envelope>';
    }

    private Response parseResponse(String responseBody) {
        Dom.Document doc = new Dom.Document();
        doc.load(responseBody);

        Dom.XmlNode parentNode = doc.getRootElement().getChildElements()[0]
            .getChildElements()[0];
        Dom.XmlNode bodyNode = parentNode.getChildElements()[1];

        Dom.XmlNode tcpLinesNode = getChildElement(bodyNode, 'TCPLINES');
        Dom.XmlNode errorCodeNode = getChildElement(bodyNode, 'ErrorCode');
        String errorCode = errorCodeNode.getText();
        // System.debug('@@@@errorCode: ' + errorCode);

        if (!tcpLinesNode.getChildElements().isEmpty()) {
            Dom.XmlNode tcpLine_2Node = tcpLinesNode.getChildElements()[0];
            String tcpService = getChildElement(tcpLine_2Node, 'TCPSERVICE')
                .getText();
            String footprint = getChildElement(tcpLine_2Node, 'FOOTPRINT')
                .getText();

            Dom.XmlNode tcpServiceLineNode = tcpLine_2Node.getChildElement(
                'TCPSERVICELINES',
                SOAP_RESPONSE_NAMESPACE
            );

            String gigaOrderingPossible;

            if (tcpServiceLineNode != null) {
                gigaOrderingPossible = getOrderingPossibleForGiga(
                    tcpServiceLineNode
                );
            }
            Response response = new Response(
                tcpService,
                footprint,
                gigaOrderingPossible
            );
            System.debug('@@@@response: ' + response);
            return response;
        } else {
            String errorMessage = System.Label.ISWSiteCheck_NoTcpLinesMessage;
            if (errorCode != ERROR_CODE_0) {
                errorMessage = getResponseFailureMessage(errorCode);
            }
            return new Response(errorMessage);
        }
    }

    private String getOrderingPossibleForGiga(Dom.XmlNode node) {
        String gigaForCoax = '';
        for (Dom.XmlNode childNode : node.getChildElements()) {
            String serviceType = childNode.getChildElement(
                    'TCPPOSSIBLESERVICETYPE',
                    SOAP_RESPONSE_NAMESPACE
                )
                .getText();

            if (serviceType == SERVICE_TYPE_GIGA) {
                gigaForCoax = childNode.getChildElement(
                        'FUNCTIONALORDERINGPOSSIBLE',
                        SOAP_RESPONSE_NAMESPACE
                    )
                    .getText();
                break;
            }
        }
        return gigaForCoax;
    }

    private Dom.XmlNode getChildElement(
        Dom.XmlNode parentNode,
        String nodeName
    ) {
        return parentNode.getChildElement(nodeName, SOAP_RESPONSE_NAMESPACE);
    }

    @TestVisible
    private static String getResponseFailureMessage(String errorCode) {
        return String.format(
            System.Label.ISWSiteCheck_ResponseFailureMessage,
            new List<String>{ errorCode }
        );
    }

    @TestVisible
    private static String getHttpFailureMessage(Integer statusCode) {
        return String.format(
            System.Label.ISWSiteCheck_HttpFailureMessage,
            new List<String>{ '' + statusCode }
        );
    }

    public class ISWSiteCheckException extends Exception {
    }

    public class Request {
        public Id siteId;
        public String postCode;
        public String houseNumber;
        public String houseNumberExtension;
        public Request(
            Id siteId,
            String postCode,
            String houseNumber,
            String houseNumberExtension
        ) {
            this.siteId = siteId;
            this.postCode = postCode;
            this.houseNumber = houseNumber;
            this.houseNumberExtension = houseNumberExtension;
        }
    }

    public class Response {
        public Boolean isTcpServiceNo;
        public String footprint;
        public Boolean isCoaxSpeedGiga;
        public String calloutFailureMessage;
        public Response(
            String tcpService,
            String footprint,
            String gigaOrderingPossible
        ) {
            this.isTcpServiceNo = (tcpService == TCP_SERVICE_FALSE);
            this.footprint = footprint;
            this.isCoaxSpeedGiga = (gigaOrderingPossible ==
            ORDERING_POSSIBLE_Y);
        }

        public Response(String calloutFailureMessage) {
            this.calloutFailureMessage = calloutFailureMessage;
        }
    }
}