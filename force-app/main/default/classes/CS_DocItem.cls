public class CS_DocItem {
	@AuraEnabled
	public String name { get; set; }

	@AuraEnabled
	public String contractConditionId { get; set; }

	@AuraEnabled
	public String docId { get; set; }

	@AuraEnabled
	public Boolean isContractSummary { get; set; }

	@AuraEnabled
	public Boolean isServiceDescription { get; set; }

	@AuraEnabled
	public Boolean isTariffDescription { get; set; }

	@AuraEnabled
	public String contractSummaryUrl { get; set; }

	@AuraEnabled
	public String serviceDescriptionUrl { get; set; }

	public String language { get; set; }
}
