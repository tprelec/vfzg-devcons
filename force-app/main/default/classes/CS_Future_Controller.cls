public class CS_Future_Controller {
    
    public Id parentId;
    
    public CS_Future_Controller(Id basketId, String description, String successUrl, String failureUrl) {
        CS_Future__c newParentProcess = createNewParentProcess(basketId, description, successUrl, failureUrl);
        parentId = newParentProcess.Id;
    }
    
    public static String getLimitInformation() {
        String limitInformation = 'Number of Queries used in this Apex code so far: ' + Limits.getQueries() + ' ( ' + Limits.getLimitQueries() + ' ) ' + ' [ ' + (100.00 * Limits.getQueries() / Limits.getLimitQueries()).setScale(2) + '% ] ';
        limitInformation += '\n Number of rows queried in this Apex code so far: ' + Limits.getDmlRows() + ' ( ' + Limits.getLimitDmlRows() + ' ) ' + ' [ ' + (100.00 * Limits.getDmlRows() / Limits.getLimitDmlRows()).setScale(2) + '% ] ';
        limitInformation += '\n Number of DML statements used so far: ' +  Limits.getDmlStatements() + ' ( ' + Limits.getLimitDmlStatements() + ' ) ' + ' [ ' + (100.00 * Limits.getDmlStatements() / Limits.getLimitDmlStatements()).setScale(2) + '% ] ';
        limitInformation += '\n Amount of CPU time (in ms) used so far: ' + Limits.getCpuTime() + ' ( ' + Limits.getLimitCpuTime() + ' ) ' + ' [ ' + (100.00 * Limits.getCpuTime() / Limits.getLimitCpuTime()).setScale(2) + '% ] ';
        limitInformation += '\n Amount of heap memory (in bytes) used so far: ' + Limits.getHeapSize() + ' ( ' + Limits.getLimitHeapSize() + ' ) ' + ' [ ' + (100.00 * Limits.getHeapSize() / Limits.getLimitHeapSize()).setScale(2) + '% ] ';
        return limitInformation;
    }
    
    private CS_Future__c createNewParentProcess(Id basketId, String description, String successUrl, String failureUrl) {
        CS_Future__c newProcess = new CS_Future__c();
        newProcess.Start_Limit_Information__c = getLimitInformation();
        newProcess.Start_Date__c = getDateTime();
        newProcess.Product_Basket__c = basketId;
        newProcess.Description__c = description;
        newProcess.Success_Url__c = successUrl;
        newProcess.Failure_Url__c = failureUrl;
        newProcess.Status__c = 'Waiting For Child Processes';
        insert newProcess;
        return newProcess;
    }
    
    private static String getDateTime() {
        return DateTime.Now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
    }
    
    private CS_Future__c createNewChildProcess(String description) {
        CS_Future__c newProcess = new CS_Future__c();
        newProcess.Start_Date__c = getDateTime();
        newProcess.Description__c = description;
        newProcess.Parent_Process__c = parentId;
        insert newProcess;
        return newProcess;
    }
    
    public Id defineNewFutureJob(String description) {
        CS_Future__c newProcess = createNewChildProcess(description);
        return newProcess.Id;
    }
    
    public void setQueueableJobId(Id futureJobId, Id queueableJobId) {
        CS_Future__c futureJob = [SELECT id, Queueable_Job_Id__c FROM CS_Future__c WHERE id =:futureJobId];
        futureJob.Queueable_Job_Id__c = 'SELECT ApexClassID,CompletedDate,ExtendedStatus,JobType,MethodName,JobItemsProcessed,TotalJobItems,Status,NumberOfErrors FROM AsyncApexJob WHERE Id=\'' + queueableJobId + '\'';
        update futureJob;
    }
    
    public static void startFutureJob(Id futureJobId) {
        CS_Future__c futureJob = [SELECT id, Status__c, Start_Limit_Information__c, Start_Date__c FROM CS_Future__c WHERE id =:futureJobId];
        futureJob.Start_Limit_Information__c = getLimitInformation();
        futureJob.Start_Date__c = getDateTime();
        futureJob.Status__c = 'In Progress';
        update futureJob;
    }
    
    public static void endFutureJob(Id futureJobId) {
        CS_Future__c futureJob = [SELECT id, Status__c, End_Limit_Information__c, End_Date__c FROM CS_Future__c WHERE id =:futureJobId];
        futureJob.End_Limit_Information__c = getLimitInformation();
        futureJob.End_Date__c = getDateTime();
        futureJob.Status__c = 'Finished';
        update futureJob;
    }
    
    public static void failFutureJob(Id futureJobId, String failureReason) {
        CS_Future__c futureJob = [SELECT id, Status__c, Additional_Status_Information__c, End_Date__c FROM CS_Future__c WHERE id =:futureJobId];
        futureJob.Additional_Status_Information__c = failureReason;
        futureJob.End_Date__c = getDateTime();
        futureJob.Status__c = 'Failure';
        update futureJob;
    }
}