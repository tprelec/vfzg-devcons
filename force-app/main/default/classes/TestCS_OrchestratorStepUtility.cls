@IsTest
private class TestCS_OrchestratorStepUtility {
	@IsTest
	static void testUpdateTasksAndPauseSteps() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();
		String status = 'Closed - Delivery Cancelled';

		System.runAs(simpleUser) {
			Task testTask = CS_DataTest.prepareDataForCustomStepCompleteTask(status, simpleUser);
			CSPOFA__Orchestration_Step__c step1 = [
				SELECT Id, COM_Task_Status__c
				FROM CSPOFA__Orchestration_Step__c
				WHERE Id = :testTask.CSPOFA__Orchestration_Step__c
			];
			Map<Id, CSPOFA__Orchestration_Step__c> deliveryOrderStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
			deliveryOrderStepMap.put(testTask.WhatId, step1);

			Test.startTest();
			CS_OrchestratorStepUtility.updateTasksAndPauseSteps(
				new List<Task>{
					[
						SELECT Id, CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c
						FROM Task
						WHERE Id = :testTask.Id
					]
				},
				deliveryOrderStepMap
			);
			Test.stopTest();

			List<Task> resultTask = [SELECT Id, Subject, Status FROM Task WHERE Id = :testTask.Id];
			System.assertEquals(status, resultTask[0].Status, 'Status is not the same.');
		}
	}

	@IsTest
	static void testSetStepsToErrorAndComplete() {
		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
		CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', false);
		insert step1Template;

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			true
		);
		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);
		CSPOFA__Orchestration_Step__c step2 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);

		String testMsg = 'testMsg';
		COM_CustomException exc = new COM_CustomException();
		exc.setMessage(testMsg);

		Test.startTest();
		step1 = CS_OrchestratorStepUtility.setStepToComplete(step1);
		step2 = CS_OrchestratorStepUtility.setStepToError(step2, exc);
		Test.stopTest();

		System.assertEquals(Constants.ORCHESTRATOR_STEP_COMPLETE, step1.CSPOFA__Status__c, 'Status is not the same.');
		System.assertNotEquals(null, step1.CSPOFA__Completed_Date__c, 'Date is empty.');
		System.assertEquals('Custom step succeeded', step1.CSPOFA__Message__c, 'Message is not the same.');
		System.assertEquals(Constants.ORCHESTRATOR_STEP_ERROR, step2.CSPOFA__Status__c, 'Status is not the same.');
		System.assertEquals(true, step2.CSPOFA__Message__c.contains(testMsg), 'Message not found.');
	}

	@isTest
	static void testGenerateRandomString() {
		Integer len = 5;

		Test.startTest();
		String str1 = CS_OrchestratorStepUtility.generateRandomString(len);
		String str2 = CS_OrchestratorStepUtility.generateRandomString(len);
		Test.stopTest();

		System.assertNotEquals(str1, str2, 'Strings are not the same.');
	}
}
