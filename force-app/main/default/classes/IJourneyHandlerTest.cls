@IsTest
private class IJourneyHandlerTest {
	private static User portalUser;
	private static Account accountVisible;

	public static final String HANDLER_NAME_ADD_MOBILE_PRODUCTS = 'AddMobileProductsJourneyHandler';

	private static void createTestData() {
		portalUser = PP_TestUtils.createPortalUser();

		TestUtils.autoCommit = false;
		accountVisible = TestUtils.createAccount(portalUser);
		accountVisible.Name = 'Customer';
		insert accountVisible;
	}

	@IsTest
	static void testCheckVisibilityFalse() {
		createTestData();

		Boolean checkVisibilityResult;

		System.runAs(portalUser) {
			checkVisibilityResult = IJourneyHandler.checkVisibility(
				HANDLER_NAME_ADD_MOBILE_PRODUCTS,
				accountVisible.Id
			);
		}

		System.assertEquals(false, checkVisibilityResult, 'Expected checkVisibility to be false.');
	}

	@IsTest
	static void testHandleClick() {
		createTestData();

		Map<String, Object> handleClickResult = new Map<String, Object>();

		System.runAs(portalUser) {
			handleClickResult = IJourneyHandler.handleClick(
				HANDLER_NAME_ADD_MOBILE_PRODUCTS,
				accountVisible.Id
			);
		}

		System.assertEquals(
			true,
			handleClickResult.keySet().size() > 0,
			'Expected handleClickResult to have some results in Map.'
		);
	}
}