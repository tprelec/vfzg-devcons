/**
 * @description Service class for operations on Commercial product object
 */

public with sharing class CommercialProductService {
	/**
	 * @description Queries detail fields for a given Commercial product record. Used by the RemoteActionProvider class.
	 *
	 * @return Commercial product  and all necessary fields queried
	 */
	public cspmb__Price_Item__c queryCommercialProductDetailsAccess(
		String siteVendor,
		String technology,
		String contractDuration,
		Integer bandwidthDown,
		Integer bandwidthUp
	) {
		String query =
			'SELECT ' +
			'Name,' +
			'cspmb__Contract_Term__c,' +
			'cspmb__recurring_charge__c,' +
			'cspmb__One_Off_Charge__c,' +
			'Contract_Term_Number__c,' +
			'Available_bandwidth_down__c,' +
			'Available_bandwidth_up__c ' +
			'FROM cspmb__Price_Item__c ' +
			'WHERE ' +
			'cspmb__Is_Active__c = TRUE ' +
			'AND Access_Type_Text__c LIKE \'%' +
			technology +
			'%\' ' +
			'AND cspmb__Effective_End_Date__c >= TODAY ' +
			'AND cspmb__Effective_Start_Date__c <= TODAY ' +
			'AND Vendor_lookup__r.Name = :siteVendor ' +
			'AND Contract_Term_Number__c = :contractDuration ' +
			'AND cspmb__Master_Price_item__c != \'\' ' +
			'AND cspmb__Product_Definition_Name__c = \'Access\'';

		if (bandwidthDown > 0 && bandwidthUp > 0) {
			query += ' AND Available_bandwidth_down__c <= :bandwidthDown AND Available_bandwidth_up__c <= :bandwidthUp';
		}

		List<cspmb__Price_Item__c> accessPriceItems = Database.query(query);

		cspmb__Price_Item__c priceItemMaxBandwith = findMaxBandwidthItem(accessPriceItems);
		return priceItemMaxBandwith;
	}

	/**
	 * @description Queries detail fields for a given Commercial product record. Used by the RemoteActionProvider class.
	 *
	 * @return Commercial product  and all necessary fields queried
	 */
	public cspmb__Price_Item__c queryCommercialProductDetailsSiteConnect(
		String contractDuration,
		Integer bandwidthUpInt,
		Integer bandwidthDownInt,
		Boolean ownInfrastructure
	) {
		Decimal bandwidthUpDec = decimal.valueOf(bandwidthUpInt);
		Decimal bandwidthDownDec = decimal.valueOf(bandwidthDownInt);
		List<cspmb__Price_Item__c> siteConnectPriceItems;

		String query =
			'SELECT ' +
			'Name,' +
			'cspmb__Contract_Term__c,' +
			'cspmb__recurring_charge__c,' +
			'cspmb__One_Off_Charge__c,' +
			'Contract_Term_Number__c,' +
			'Available_bandwidth_down__c,' +
			'Available_bandwidth_up__c ' +
			'FROM cspmb__Price_Item__c ' +
			'WHERE ' +
			'cspmb__Is_Active__c = TRUE ' +
			'AND cspmb__Effective_End_Date__c >= TODAY ' +
			'AND cspmb__Effective_Start_Date__c <= TODAY ' +
			'AND cspmb__Product_Definition_Name__c = \'Site Connect\' ' +
			'AND Contract_Term_Number__c = :contractDuration ' +
			'AND cspmb__Master_Price_item__c != \'\'';

		try {
			if (ownInfrastructure) {
				query += ' AND Available_bandwidth_down__c >= :bandwidthDownDec AND Available_bandwidth_up__c >= :bandwidthUpDec';
				System.debug('ownInfrastructure query: ' + query);
				siteConnectPriceItems = Database.query(query);
				System.debug('siteConnectPriceItems ' + JSON.serializePretty(siteConnectPriceItems));
				return findMinBandwidthItem(siteConnectPriceItems);
			} else {
				query += ' AND Available_bandwidth_down__c <= :bandwidthDownDec AND Available_bandwidth_up__c <= :bandwidthUpDec';
				System.debug('NOT ownInfrastructure query: ' + query);
				siteConnectPriceItems = Database.query(query);
				System.debug('siteConnectPriceItems ' + JSON.serializePretty(siteConnectPriceItems));
				return findMaxBandwidthItem(siteConnectPriceItems);
			}
		} catch (Exception ex) {
			System.debug('Exception ' + ex);
		}
		return null;
	}
	//COM-2220 by sowparnika
	/**
	 * @description Queries detail fields for a given Commercial product record. Used by the RemoteActionProvider class.
	 *
	 * @return Commercial product  and all necessary fields queried
	 */
	public cspmb__Price_Item__c queryCommercialProductDetailsInternetModemOnly(String contractDuration) {
		return [
			SELECT id, Name, cspmb__Contract_Term__c
			FROM cspmb__Price_Item__c
			WHERE
				cspmb__Is_Active__c = TRUE
				AND cspmb__Effective_End_Date__c >= TODAY
				AND cspmb__Effective_Start_Date__c <= TODAY
				AND Contract_Term_Number__c = :contractDuration
				AND cspmb__Master_Price_item__c != ''
				AND Category__c = 'Business Internet'
			LIMIT 1
		];
	}

	//COM-2220 by sowparnika

	private cspmb__Price_Item__c findMaxBandwidthItem(List<cspmb__Price_Item__c> inputList) {
		if (inputList.isEmpty()) {
			return null;
		}

		cspmb__Price_Item__c priceItemMaxBandwith = inputList[0];
		for (cspmb__Price_Item__c currentItem : inputList) {
			if (currentItem.Available_bandwidth_up__c == null) {
				continue;
			}
			if (priceItemMaxBandwith.Available_bandwidth_down__c < currentItem.Available_bandwidth_down__c) {
				priceItemMaxBandwith = currentItem;
			} else if (
				(priceItemMaxBandwith.Available_bandwidth_down__c == currentItem.Available_bandwidth_down__c) &&
				priceItemMaxBandwith.Available_bandwidth_up__c < currentItem.Available_bandwidth_up__c
			) {
				priceItemMaxBandwith = currentItem;
			}
		}

		return priceItemMaxBandwith;
	}

	private cspmb__Price_Item__c findMinBandwidthItem(List<cspmb__Price_Item__c> inputList) {
		if (inputList.isEmpty()) {
			return null;
		}

		cspmb__Price_Item__c priceItemMinBandwith = inputList[0];
		for (cspmb__Price_Item__c currentItem : inputList) {
			if (currentItem.Available_bandwidth_up__c == null) {
				continue;
			}
			if (priceItemMinBandwith.Available_bandwidth_down__c > currentItem.Available_bandwidth_down__c) {
				priceItemMinBandwith = currentItem;
			} else if (
				(priceItemMinBandwith.Available_bandwidth_down__c == currentItem.Available_bandwidth_down__c) &&
				priceItemMinBandwith.Available_bandwidth_up__c > currentItem.Available_bandwidth_up__c
			) {
				priceItemMinBandwith = currentItem;
			}
		}

		return priceItemMinBandwith;
	}
}
