@SuppressWarnings('PMD.AvoidGlobalModifier')
global with sharing class CustomButtonSynchronizeWithOpportunity extends csbb.CustomButtonExt {
	private static final String ONE_MOBILE = 'OneMobile';
	private static final String BUSINESS_INTERNET = 'Internet'; //COM-2039
	public String performAction(String basketId) {
		//Success & redirect
		//'{"status":"ok","redirectURL":"http://www.google.com","target":"1"}';

		// Success and remain in page
		//return '{"status":"ok","title":"Success","text":"Synchronize to opportunity successful"}';

		try {
			String newUrl = CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(basketId, null);
			String reqExp = '^/' + '\\' + 'w{15,18}';
			Pattern ptr = Pattern.compile(reqExp);
			Matcher m = ptr.matcher(newUrl);

			if (m.matches() && m.hitEnd()) {
				return '{"status":"ok","text":"Basket has been synced successfully","redirectURL":"' + newUrl + '"}';
			} else {
				return '{"status":"error","text":"Basket was not Synched correctly"}';
			}
		} catch (Exception e) {
			return '{"status":"error","text":"' + e.getMessage() + '"}';
		}
	}

	private static Boolean checkCombinations(List<cscfga__Product_Configuration__c> configs) {
		Boolean isValid = true;

		Map<String, List<Id>> scenarioConfig = new Map<String, List<Id>>();
		for (cscfga__Product_Configuration__c pc : configs) {
			if (scenarioConfig.containsKey(pc.Mobile_Scenario__c)) {
				scenarioConfig.get(pc.Mobile_Scenario__c).add(pc.Id);
			} else {
				List<Id> ids = new List<Id>();
				ids.add(pc.Id);
				scenarioConfig.put(pc.Mobile_Scenario__c, ids);
			}
		}

		//rules Do not allow sales to submit a quote with combinations between RedPro and OneMobile or OneBusiness and show error message
		//red pro cannot be sold with one business or onemobile

		//rules Do not allow sales to submit a quote with combinations between RedPro and OneMobile or OneBusiness and show error message
		//red pro cannot be sold with one business or onemobile row 11
		if (
			scenarioConfig.containsKey(Label.RedProScenario) &&
			(scenarioConfig.containsKey(Label.OneBusinessScenario) ||
			scenarioConfig.containsKey(Label.OneMobileScenario) ||
			scenarioConfig.containsKey(Label.OneBusinessIOTscenario))
		) {
			//isValid = false;
			isValid = false;
		}

		//row 7 Do not allow sales to submit a quote with combinations between OneBusiness and other mobile products  [i.e.OneBusiness IOT, OneMobile, RedPro, Data only (MBB Group Bundles)] and show error message
		if (
			scenarioConfig.containsKey(Label.OneBusinessScenario) &&
			(scenarioConfig.containsKey(Label.OneBusinessIOTscenario) ||
			scenarioConfig.containsKey(Label.OneMobileScenario) ||
			scenarioConfig.containsKey(Label.RedProScenario) ||
			scenarioConfig.containsKey(Label.DataOnlyScenario))
		) {
			isValid = false;
		}

		//row 8 Do not allow sales to submit a quote with combinations between OneBusiness IOT and other mobile products  [i.e.OneBusiness, OneMobile, RedPro, Data only (MBB Group Bundles)] and show error message
		if (
			scenarioConfig.containsKey(Label.OneBusinessIOTscenario) &&
			(scenarioConfig.containsKey(Label.OneMobileScenario) ||
			scenarioConfig.containsKey(Label.OneBusinessScenario) ||
			scenarioConfig.containsKey(Label.RedProScenario) ||
			scenarioConfig.containsKey(Label.DataOnlyScenario))
		) {
			isValid = false;
		}

		//row 10 Do not allow sales to submit a quote with combinations between RedPro and other mobile products  [i.e.OneBusiness, OneMobile, RedPro, Data only (MBB Group Bundles)] and show error message
		if (
			scenarioConfig.containsKey(Label.OneMobileScenario) &&
			(scenarioConfig.containsKey(Label.DataOnlyScenario) ||
			scenarioConfig.containsKey(Label.OneBusinessScenario) ||
			scenarioConfig.containsKey(Label.OneBusinessIOTscenario))
		) {
			isValid = false;
		}

		//row 9 Do not allow sales to submit a quote with combinations between RedPro and other mobile products  [i.e.OneBusiness, OneMobile, RedPro, Data only (MBB Group Bundles)] and show error message
		if (
			scenarioConfig.containsKey(Label.DataOnlyScenario) &&
			(scenarioConfig.containsKey(Label.OneMobileScenario) ||
			scenarioConfig.containsKey(Label.OneBusinessScenario) ||
			scenarioConfig.containsKey(Label.OneBusinessIOTscenario))
		) {
			isValid = false;
		}

		return isValid;
	}

	public static Map<Id, Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>>> solutionOESchemaSimplifier(
		Map<Id, List<cssmgnt.ProductProcessingUtility.Component>> oeMap
	) {
		Map<Id, Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>>> pcOeAttrMap = null;

		if (oeMap == null) {
			return pcOeAttrMap;
		}

		for (Id configId : oeMap.keySet()) {
			Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>> oeAttrMap = new Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>>();
			List<cssmgnt.ProductProcessingUtility.Configuration> configList = new List<cssmgnt.ProductProcessingUtility.Configuration>();
			Map<String, String> oeConfigNameMap = new Map<String, String>();
			List<cssmgnt.ProductProcessingUtility.Component> objList = oeMap.get(configId);

			for (cssmgnt.ProductProcessingUtility.Component comp : objList) {
				configList.addAll(comp.configurations);
			}

			for (cssmgnt.ProductProcessingUtility.Configuration config : configList) {
				Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>> attrMap = new Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>>();

				attrMap.put(config.guid, config.attributes);
				oeconfigNamemap.put(config.guid, config.ConfigurationName);

				for (String oeguid : attrMap.keySet()) {
					if (oeConfigNameMap != null && oeConfigNameMap.containsKey(oeguid) && oeConfigNameMap.get(oeguid) != null) {
						oeAttrMap.put(oeguid, attrMap.get(oeguid));
					}
				}
			}

			if (pcOeAttrMap == null) {
				pcOeAttrMap = new Map<Id, Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>>>();
			}

			pcOeAttrMap.put(configId, oeAttrMap);
		}

		return pcOeAttrMap;
	}

	public static Map<Id, List<cssmgnt.ProductProcessingUtility.Component>> retrieveOeMap(String basketId) {
		List<Id> configIds = new List<Id>();
		List<cscfga__Product_Configuration__c> pcList = [
			SELECT Id, cscfga__Product_Family__c
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c = :basketId
		];

		for (cscfga__Product_Configuration__c pc : pcList) {
			if (pc.cscfga__Product_Family__c != null && pc.cscfga__Product_Family__c.contains(ONE_MOBILE)) {
				configIds.add(pc.Id);
			}
		}

		Map<Id, List<cssmgnt.ProductProcessingUtility.Component>> oeMap = null;
		if (configIds.size() > 0) {
			oeMap = cssmgnt.API_1.getOEData(configIds);
		}

		return oeMap;
	}

	public static String syncWithOpportunity(String basketId, Map<Id, List<cssmgnt.ProductProcessingUtility.Component>> oeMap) {
		List<Id> configIds = new List<Id>();
		List<cscfga__Product_Configuration__c> pcList = [
			SELECT
				Id,
				Name,
				Mobile_Scenario__c,
				cscfga__Product_Basket__c,
				cscfga__Product_Definition__c,
				cscfga__Product_Definition__r.Name,
				cscfga__Quantity__c,
				Package_Name__c,
				cscfga__Product_Family__c,
				ContractNumber_JSON__c,
				Contract_Number__c,
				Contract_Number_Group__c,
				csordtelcoa__Replaced_Product_Configuration__c,
				cscfga__Product_Basket__r.csordtelcoa__Change_Type__c,
				BAN_Information__c,
				Financial_Account__c,
				Billing_Arrangement__c,
				New_Portfolio__c,
				New_Delivery_Model__c,
				cscfga__Parent_Configuration__c,
				cscfga__Parent_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,
				cscfga__Parent_Configuration__r.ContractNumber_JSON__c,
				cscfga__Parent_Configuration__r.Contract_Number__c
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c = :basketId
		];

		cscfga__Product_Basket__c pb = [
			SELECT
				id,
				Error_message__c,
				Total_Number_of_Expected_Locations__c,
				cscfga__opportunity__c,
				Block_Expiration__c,
				Contract_end_Mobile__c,
				csbb__Synchronised_with_Opportunity__c,
				cscfga__Basket_Status__c,
				ProfitLoss_JSON__c,
				New_Portfolio__c
			FROM cscfga__Product_Basket__c
			WHERE id = :basketId
		];

		List<cscfga__Product_Configuration__c> mobileConfigs = new List<cscfga__Product_Configuration__c>();
		for (cscfga__Product_Configuration__c pc : pcList) {
			if (pc.cscfga__Product_Family__c != null && pc.cscfga__Product_Family__c.contains(ONE_MOBILE)) {
				configIds.add(pc.Id);
			}

			if (pc.Mobile_Scenario__c != null) {
				mobileConfigs.add(pc);
			}
		}

		if (oeMap == null && configIds.size() > 0) {
			oeMap = cssmgnt.API_1.getOEData(configIds);
		}

		Map<Id, Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>>> pcOeAttrMap = solutionOESchemaSimplifier(oeMap);

		Boolean mobileRulesValid = checkCombinations(mobileConfigs);

		CS_ContractNumberGenerator.generateContractNumbers(pcList);

		if (!mobileRulesValid) {
			return Label.MobileCombinationErrorMessage;
		}

		if (pb.Error_message__c != null) {
			return 'Please correct the errors described in the warning section.';
		}

		if (pb.Total_Number_of_Expected_Locations__c != null) {
			return 'There is a total number of expected locations specified. Please remove this value and make sure all required locations are included in the configurations.';
		}

		//unsync first
		if (pb.csbb__Synchronised_with_Opportunity__c) {
			pb.csbb__Synchronised_with_Opportunity__c = false;
			update pb;
		}

		//then sync
		pb.csbb__Synchronised_with_Opportunity__c = true;
		pb.csordtelcoa__Synchronised_with_Opportunity__c = true;

		update pb;

		//Nikola  - call methods required for Syncing Basket with Opportunity

		List<cscfga__Product_Basket__c> synchronizedBaskets = new List<cscfga__Product_Basket__c>();
		List<cscfga__Product_Basket__c> oldBaskets = new List<cscfga__Product_Basket__c>();

		synchronizedBaskets.add(pb);
		oldBaskets.add(
			new cscfga__Product_Basket__c(csbb__Synchronised_with_Opportunity__c = false, csordtelcoa__Synchronised_with_Opportunity__c = false)
		);

		// COM-2755: Copy the billing fields from Opportunity to product configurations
		BasketBillingInformationHelper.copyFieldsFromOpportunityToPCs(basketId, pcList);

		SyncWithOpportunityUtility.UnSyncProductBasketsAfterInsertUpdate(synchronizedBaskets, oldBaskets);
		SyncWithOpportunityUtility.DeleteOLIsProductDetailsAfterUpdate(synchronizedBaskets, oldBaskets);
		SyncWithOpportunityUtility.InsertOLIsProductDetailsAfterUpdate(synchronizedBaskets, oldBaskets, pcOeAttrMap);
		// SyncWithOpportunityUtility.SynchronizeProfitLossOppFields(pb);
		// SyncWithOpportunityUtility.SyncBlockExpiration(pb);
		SyncWithOpportunityUtility.syncOpportunityFields(pb, pcList);

		//end

		Opportunity oppToGoBackTo = [SELECT Id FROM Opportunity WHERE id = :pb.cscfga__Opportunity__c];

		PageReference oppPage = new ApexPages.StandardController(oppToGoBackTo).view();

		return oppPage.getUrl();
	}

	public static Boolean syncWithOpportunityOnlineScenario(String basketId, Map<Id, CCAQDProcessor.CCAQDStructure> pcAQDDataMap) {
		cscfga__Product_Basket__c pb = [
			SELECT
				id,
				Error_message__c,
				Total_Number_of_Expected_Locations__c,
				cscfga__opportunity__c,
				Block_Expiration__c,
				Contract_end_Mobile__c,
				csbb__Synchronised_with_Opportunity__c,
				cscfga__Basket_Status__c,
				ProfitLoss_JSON__c,
				Used_Snapshot_Objects__c
			FROM cscfga__Product_Basket__c
			WHERE id = :basketId
		];

		pb.csbb__Synchronised_with_Opportunity__c = true;
		pb.csordtelcoa__Synchronised_with_Opportunity__c = true;

		if (
			(pb.Used_Snapshot_Objects__c == '') ||
			(pb.Used_Snapshot_Objects__c == null) ||
			(pb.Used_Snapshot_Objects__c != '[CS_Basket_Snapshot_Transactional__c]')
		) {
			pb.Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]';
		}

		update pb;

		Set<String> pbSet = new Set<String>();
		pbSet.add(pb.Id);

		List<cscfga__Product_Basket__c> baskets = new List<cscfga__Product_Basket__c>();
		baskets.add(pb);

		//CS_BasketSnapshotManager.TakeBasketSnapshot(baskets, false);
		//fix issue in test this line is commented out on test env and it shouldnt be
		CS_BasketSnapshotManager.TakeBasketSnapshotJsonSource(baskets, false, pcAQDDataMap, true);
		ProductUtility.CreateOLIsOnlineScenario(pbSet, pcAQDDataMap);
		//SyncWithOpportunityUtility.SynchronizeProfitLossOppFields(pb);
		SyncWithOpportunityUtility.syncOpportunityFieldsOnlineScenario(pb);

		return true;
	}
}
