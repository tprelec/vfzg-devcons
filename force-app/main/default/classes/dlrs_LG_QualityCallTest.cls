/**
 * Auto Generated and Deployed by the Declarative Lookup Rollup Summaries Tool package (dlrs)
 **/
@IsTest
private class dlrs_LG_QualityCallTest
{
    @IsTest
    private static void testTrigger()
    {
        // Force the dlrs_LG_QualityCallTrigger to be invoked, fails the test if org config or other Apex code prevents this.
        dlrs.RollupService.testHandler(new LG_QualityCall__c());
    }
}