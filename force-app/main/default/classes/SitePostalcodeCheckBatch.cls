/**
 * @description     This is the class that contains logic for running the postalcode check in batches
 * @author        Guy Clairbois
 */
global class SitePostalcodeCheckBatch implements Database.Batchable <sObject>, Database.AllowsCallouts{
 
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('SELECT Id,Postal_Code_Check_Status__c FROM Site__c Where Postal_Code_Check_Status__c != null AND Postal_Code_Check_Status__c != \'Completed\' ');
    } 
 
    global void execute(Database.BatchableContext BC, List<Site__c> scope){
      // do pccheck here
        Map<String,Set<Id>> requesttypeToSites = new Map<String,Set<Id>>();
        requesttypeToSites.put('dsl',new Set<Id>());
        requesttypeToSites.put('fiber',new Set<Id>());

        for(Site__c s : scope){
          if(s.Postal_Code_Check_Status__c == 'DSL check requested'){
            //PostalCodeCheckController.doOnlinePostalCodeCheckForSites(new Set<Id>{s.Id},'dsl');   
            requesttypeToSites.get('dsl').add(s.Id);
            //return;
          } else if(s.Postal_Code_Check_Status__c == 'Fiber check requested'){
            //PostalCodeCheckController.doOnlinePostalCodeCheckForSites(new Set<Id>{s.Id},'fiber');   
            requesttypeToSites.get('fiber').add(s.Id);
            //return;
          } else if(s.Postal_Code_Check_Status__c == 'Combined check requested'){
          	// do both checks separately in order to make sure that sdslbis is also done
            //PostalCodeCheckController.doOnlinePostalCodeCheckForSites(new Set<Id>{s.Id},'dsl');    
            //PostalCodeCheckController.doOnlinePostalCodeCheckForSites(new Set<Id>{s.Id},'fiber');   
            requesttypeToSites.get('dsl').add(s.Id);
            requesttypeToSites.get('fiber').add(s.Id);
            //return;
          }
      }
      PostalCodeCheckController.doBatchCheck(requesttypeToSites);

      
    }
 
    global void finish(Database.BatchableContext BC){
    	// cleanup any remaining jobs
		for(CronTrigger ct : [SELECT Id, State, CronJobDetail.Id, CronJobDetail.Name, CronJobDetail.JobType 
		    					FROM CronTrigger 
		    					WHERE State = 'DELETED' 
		    					AND CronJobDetail.JobType = '7'  
		    					AND cronjobdetail.name LIKE 'Postalcode check%']){
			System.abortJob(ct.Id);
		}    	

      	// start next job
      	List<Site__c> remainingSites = [SELECT Id,Postal_Code_Check_Status__c FROM Site__c Where Postal_Code_Check_Status__c = 'DSL check requested' OR Postal_Code_Check_Status__c = 'Fiber check requested' OR Postal_Code_Check_Status__c = 'Combined check requested'];
      	if(!remainingSites.isEmpty()){
	        DateTime n = datetime.now().addSeconds(10);
	        String cron = '';

	        cron += n.second();
	        cron += ' ' + n.minute();
	        cron += ' ' + n.hour();
	        cron += ' ' + n.day();
	        cron += ' ' + n.month();
	        cron += ' ' + '?';
	        cron += ' ' + n.year();

	        Id scheduleId = System.schedule('Postalcode check '+system.now(), cron, new ScheduleSitePostalCodeCheckBatch());        

      	}
    }
 
}