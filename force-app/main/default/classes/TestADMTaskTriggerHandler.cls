@isTest
private class TestADMTaskTriggerHandler {

	@testSetup
	static void makeData() {
		User owner = TestUtils.createAdministrator();
		agf__ADM_Scrum_Team__c team = TestUtils.createScrumTeams(1, true)[0];
		agf__ADM_Product_Tag__c productTag = TestUtils.createProductTags(1, true, team.Id)[0];
		TestUtils.createWorkItems(1, true, owner.Id, productTag.Id);
		TestUtils.createThemes(1, true, owner.Id);
		TestUtils.createThemes(1, true, UserInfo.getUserId());
	}

	@isTest
	static void testThemeAssignment() {

		// Get User
		User u = getUser();
		// Get Work
		agf__ADM_Work__c work = getWork();

		Test.startTest();

		// Create New Task
		agf__ADM_Task__c tsk = new agf__ADM_Task__c(
			agf__Assigned_To__c = u.Id,
			agf__Work__c = work.id,
			agf__Subject__c = 'Test'
		);
		insert tsk;

		// Check if Theme is assigned
		System.assertEquals(1, getThemeAssignmentCount());

		// Update Task
		tsk.agf__Subject__c = 'Test2';
		tsk.agf__Assigned_To__c = UserInfo.getUserId();
		update tsk;

		// Check if Theme is assigned
		System.assertEquals(1, getThemeAssignmentCount());

		// Delete Task
		delete tsk;
		// Check if Theme is unassigned
		System.assertEquals(0, getThemeAssignmentCount());

		Test.stopTest();
	}

	private static User getUser() {
		return [
			SELECT
				Id
			FROM
				User
			WHERE
				CreatedDate = TODAY
				AND Email = 'testuser@vodafone.com'
			LIMIT 1
		];
	}

	private static agf__ADM_Work__c getWork() {
		return [
			SELECT
				Id
			FROM
				agf__ADM_Work__c
			LIMIT 1
		];
	}

	private static Integer getThemeAssignmentCount() {
		return Database.countQuery('SELECT COUNT() FROM agf__ADM_Theme_Assignment__c');

	}
}