public with sharing class AddBillingInformationController {
    /*
    @AuraEnabled
    public static LightningResponse getData(Id recordId) {
        try {
            LightningResponse lightningResponse;
            System.debug(recordId + '----' + recordId?.getSobjectType());
            if (
                recordId?.getSobjectType() ==
                Schema.cscfga__Product_Basket__c.SObjectType
            ) {
                System.debug(Schema.cscfga__Product_Basket__c.SObjectType);
                cscfga__Product_Basket__c basket = [
                    SELECT
                        Id,
                        csbb__Account__c,
                        BAN_Information__c,
                        Financial_Account__c,
                        Billing_Arrangement__c
                    FROM cscfga__Product_Basket__c
                    WHERE Id = :recordId
                ];
                System.debug(
                    'new BillingInformation(basket): ' +
                    new BillingInformation(basket)
                );
                return new LightningResponse()
                    .setBody(new BillingInformation(basket));
            } else if (
                recordId?.getSobjectType() == Schema.csord__Order__c.SObjectType
            ) {
                System.debug(Schema.csord__Order__c.SObjectType);
                return new LightningResponse()
                    .setError('Work in progress for order object');
            } else {
                return new LightningResponse()
                    .setError('The record Id is not supported: ' + recordId);
            }

        } catch (Exception objEx) {
            return setError(null, objEx.getMessage());
        }
    }

    */

    @AuraEnabled
    public static LightningResponse save(SaveBillingInformation saveData) {
        Savepoint savepoint = Database.setSavePoint();
        System.debug('@@@@saveData: ' + saveData);
        System.debug(
            '@@@@saveData.recordId?.getSobjectType(): ' +
            saveData.recordId?.getSobjectType()
        );
        try {
            if (
                saveData.recordId?.getSobjectType() ==
                Schema.cscfga__Product_Basket__c.SObjectType
            ) {
                cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
                    Id = saveData.recordId,
                    BAN_Information__c = saveData.banId,
                    Financial_Account__c = saveData.financialAccountId,
                    Billing_Arrangement__c = saveData.billingArrangementId
                );
                System.debug('@@@@basket: ' + basket);
                update basket;

                return new LightningResponse()
                    .setSuccess('Billing information is saved successfully to basket!');
            } else {
                csord__Order__c order = new csord__Order__c(
                    Id = saveData.recordId,
                    BAN_Information__c = saveData.banId,
                    Financial_Account__c = saveData.financialAccountId,
                    Billing_Arrangement__c = saveData.billingArrangementId
                );
                System.debug('@@@@order: ' + order);
                update order;

                return new LightningResponse()
                    .setSuccess('Billing information is saved successfully to order!');
            }
        } catch (DMLException objEx) {
            return setError(savepoint, objEx.getDmlMessage(0));
        } catch (Exception objEx) {
            return setError(savepoint, objEx.getMessage());
        }
        // return new LightningResponse().setBody('recordId');
    }

    @TestVisible
    private static LightningResponse setError(
        Savepoint savepoint,
        String message
    ) {
        if (savepoint != null) {
            Database.rollback(savepoint);
        }
        return new LightningResponse().setError(message);
    }

    public class BillingInformation {
        @AuraEnabled
        public Id accountId { get; set; }
        @AuraEnabled
        public Id banId { get; set; }
        @AuraEnabled
        public Id financialAccountId { get; set; }
        @AuraEnabled
        public Id billingArrangementId { get; set; }

        public BillingInformation(cscfga__Product_Basket__c basket) {
            this.accountId = basket.csbb__Account__c;
            this.banId = basket.BAN_Information__c;
            this.financialAccountId = basket.Financial_Account__c;
            this.billingArrangementId = basket.Billing_Arrangement__c;
        }
    }

    public class SaveBillingInformation {
        @AuraEnabled
        public Id recordId { get; set; }
        @AuraEnabled
        public Id banId { get; set; }
        @AuraEnabled
        public Id financialAccountId { get; set; }
        @AuraEnabled
        public Id billingArrangementId { get; set; }
    }
}