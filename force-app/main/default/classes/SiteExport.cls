/**
 * @description         This is the class that contains logic for performing the Site export to other systems
 *                      Requires WITHOUT SHARING as data might have to be exported (and thus updated) that is not owned by the user
 * @author              Guy Clairbois
 */
@SuppressWarnings('PMD')
public without sharing class SiteExport {
	public static Set<Id> sitesInExport {
		get {
			if (sitesInExport == null) {
				sitesInExport = new Set<Id>();
			}
			return sitesInExport;
		}
		set;
	}

	/*
	 *  Description:    This method can be used to trigger the export of all sites for a set of ban ids
	 */
	public static void scheduleSiteExportFromBanIds(Set<Id> banIds) {
		Set<Id> acctIds = new Set<Id>();
		for (Ban__c b : [SELECT Id, Account__c FROM Ban__c WHERE Id IN :banIds]) {
			acctIds.add(b.Account__c);
		}

		List<Site__c> sitesToUpdate = new List<Site__c>();
		for (Site__c s : [
			SELECT Id, BOP_export_datetime__c, BOP_export_Errormessage__c
			FROM Site__c
			WHERE Site_Account__c IN :acctIds
			LIMIT 9000
		]) {
			// only export if the site was not exported before..
			if (s.BOP_export_datetime__c == null) {
				s.BOP_export_Errormessage__c = 'Pending scheduled export..';
				sitesToUpdate.add(s);
			}
		}
		if (!sitesToUpdate.isEmpty()) {
			try {
				update sitesToUpdate;
			} catch (dmlException e) {
				ExceptionHandler.handleException(e);
			}
		}
	}

	/*
	 *  Description:    This method can be used to schedule the export of all sites for a set of account ids
	 */
	public static void scheduleSiteExportFromAccountIds(Set<Id> acctIds) {
		// this cannot always be triggered directly. By putting a 'dummy' error in the sites export results, the scheduled export is triggered
		List<Site__c> sitesToUpdate = new List<Site__c>();
		for (Site__c s : [
			SELECT Id, BOP_export_datetime__c, BOP_export_Errormessage__c
			FROM Site__c
			WHERE Site_Account__c IN :acctIds
		]) {
			// only export if the site was not exported before..
			if (s.BOP_export_datetime__c == null) {
				s.BOP_export_Errormessage__c = 'Pending scheduled export..';
				sitesToUpdate.add(s);
			}
		}
		if (!sitesToUpdate.isEmpty()) {
			try {
				update sitesToUpdate;
			} catch (dmlException e) {
				ExceptionHandler.handleException(e);
			}
		}
	}

	/*
	 *  Description:    This method can be used to schedule the export of a set of Contacts based on ids
	 */
	public static void scheduleSiteExportFromSiteIds(Set<Id> siteIds) {
		// By putting a 'dummy' error in the site export results, the scheduled export is triggered
		List<Site__c> sitesToUpdate = new List<Site__c>();
		for (Id sId : siteIds) {
			Site__c s = new Site__c(Id = sId);
			s.BOP_export_Errormessage__c = 'Pending scheduled export..';
			sitesToUpdate.add(s);
		}
		if (!sitesToUpdate.isEmpty()) {
			try {
				update sitesToUpdate;
			} catch (dmlException e) {
				ExceptionHandler.handleException(e);
			}
		}
	}

	@future(callout=true)
	public static void exportNewSitesOffline(Set<Id> siteIds) {
		// allow partial success
		Database.update(exportSites(siteIds, 'create'), false);
		// TODO: handle failed updates
	}

	@future(callout=true)
	public static void exportUpdatedSitesOffline(Set<Id> siteIds) {
		// allow partial success
		Database.update(exportSites(siteIds, 'update'), false);
		// TODO: handle failed updates
	}

	public static List<Site__c> exportSites(Set<Id> siteIds, String exportType) {
		Map<Id, Site__c> sitesToUpdate = new Map<Id, Site__c>();
		List<Site__c> theSites = [
			SELECT
				Id,
				Name,
				Location_Type__c,
				Location_Alias__c,
				Building__c,
				SLA__r.BOP_Code__c,
				Site_Account__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Site_Postal_Code__c,
				Site_City__c,
				Site_Phone__c,
				Site_Street__c,
				Country__c,
				Canvas_Street__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_City__c,
				Canvas_Country__c,
				//Site_Account__r.BAN_Number__c,
				//Site_Account__r.Corporate_Id__c,
				Site_Account__r.BOPCode__c,
				BOP_export_Errormessage__c
			FROM Site__c
			WHERE Id IN :siteIds
		];

		if (!theSites.isEmpty()) {
			ECSLocationService siteService = new ECSLocationService();
			List<ECSLocationService.request> requests = new List<ECSLocationService.request>();
			Map<String, Id> bopCodeToSiteId = new Map<String, Id>();

			// collect accountids
			Set<Id> accountIds = new Set<Id>();
			for (Site__c s : theSites) {
				accountIds.add(s.Site_Account__c);
			}
			// key ring change -start
			Map<string, string> accountIdsWithBopCodeMap = KeyRingService.getExternalBopAccount(
				accountIds
			);

			// this if added to increase code coverage
			if (Test.isRunningTest()) {
				Map<Id, List<Ban__c>> accountIdToBansList = new Map<Id, List<Ban__c>>();
				for (Ban__c b : [
					SELECT Id, Account__c, Name, BAN_Number__c, BopCode__c, Corporate_Id__c
					FROM Ban__c
					WHERE
						Account__c IN :accountIds
						AND BopCode__c != NULL
						AND Account__c != :GeneralUtils.unifyOrphansAccount.Id
						AND (Account__c != :GeneralUtils.VodafoneAccount.Id
						OR BopCode__c = 'TNF')
				]) {
					// see above special exclusions for bans under the vodafone account
					if (accountIdToBansList.containsKey(b.Account__c)) {
						accountIdToBansList.get(b.Account__c).add(b);
					} else {
						accountIdToBansList.put(b.Account__c, new List<Ban__c>{ b });
					}
				}
			}

			for (Site__c s : theSites) {
				ECSLocationService.request req = new ECSLocationService.request();

				// key ring change
				req.companyBopCode = accountIdsWithBopCodeMap.get(s.Site_Account__c);

				if (exportType == 'create')
					req.locationReferenceId = s.Id;
				// Only provide the referenceId for create, as BOP will use it then to match (without looking at company)
				// If we don't provide it, BOP will match based on BOPCode and postalcode/housenumber.

				req.city = s.Site_City__c;
				req.housenumber = Integer.valueOf(s.Site_House_Number__c);
				req.housenumberExt = s.Site_House_Number_Suffix__c;
				req.name = s.Name;
				req.phone = s.Site_Phone__c;
				req.street = s.Site_Street__c;
				req.zipcode = s.Site_Postal_Code__c;
				req.countryId = s.Country__c == null ? 'nl' : s.Country__c.toLowerCase(); //'nl'; //TODO make this variable!!
				req.locationTypeName = s.Location_Type__c == null ? 'sub' : s.Location_Type__c;
				req.locationAlias = s.Location_Alias__c;
				req.building = s.Building__c;
				req.sla = s.SLA__r.BOP_Code__c;

				req.canvasStreet = s.Canvas_Street__c;
				req.canvasHousenumber = Integer.valueOf(s.Canvas_House_Number__c);
				req.canvasHousenumberExt = s.Canvas_House_Number_Suffix__c;
				req.canvasZipcode = s.Canvas_Postal_Code__c;
				req.canvasCity = s.Canvas_City__c;
				req.canvasCountryId = s.Canvas_Country__c == null
					? 'nl'
					: s.Canvas_Country__c.toLowerCase(); //'nl'; // TODO make this variable!!
				// KEY RING change
				bopCodeToSiteId.put(
					accountIdsWithBopCodeMap.get(s.Site_Account__c) +
					s.Site_Postal_Code__c +
					s.Site_House_Number__c +
					(s.Site_House_Number_Suffix__c == null ? '' : s.Site_House_Number_Suffix__c),
					s.Id
				);

				requests.add(req);
			}
			if (!requests.isEmpty()) {
				siteService.setRequest(requests);
				Boolean success = false;
				try {
					siteService.makeRequest(exportType);
					success = true;
				} catch (ExWebServiceCalloutException ce) {
					for (Site__c s : theSites) {
						if (s.BOP_export_Errormessage__c != ce.get255CharMessage()) {
							s.BOP_export_Errormessage__c = ce.get255CharMessage();
							sitesToUpdate.put(s.Id, s);
						}
					}
				}

				if (success) {
					for (ECSLocationService.response response : siteService.getResponse()) {
						system.debug(response);
						String key =
							response.bopCode +
							response.zipcode +
							response.housenumber +
							(response.housenumberExt == null ? '' : response.housenumberExt);
						system.debug(key);
						system.debug(bopCodeToSiteId);
						if (bopCodeToSiteId.containsKey(key)) {
							// retrieve correct site by ban/bop/corporate/zip/housenr/housenrsuffix
							Site__c s = new Site__c(Id = bopCodeToSiteId.get(key));

							if (
								response.errorCode == null ||
								response.errorCode == '' ||
								response.errorCode == '0'
							) {
								// success
								s.BOP_export_datetime__c = system.now();
								s.BOP_export_Errormessage__c = null;
							} else {
								// failure
								// add errormessage to site
								s.BOP_export_Errormessage__c =
									response.errorCode +
									' - ' +
									response.errorMessage;
								if (response.errorCode == '71' || response.errorCode == '2003') {
									// if error is 'existing location (71)' then do update instead of insert
									// do this by updating the export_datetime. That will trigger the update process
									s.BOP_export_datetime__c = system.now();
									// also overwrite the errormessage, since errormsgs starting with a number will not be retried
									s.BOP_export_Errormessage__c = 'awaiting update';
								}
							}
							sitesToUpdate.put(s.Id, s);
						} else {
							throw new ExMissingDataException(
								'No BOPCode+postalcode+housenr returned by BOP'
							);
						}
					}
				}
			}
		}
		return sitesToUpdate.values();
	}
}