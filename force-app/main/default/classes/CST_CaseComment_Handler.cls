public class CST_CaseComment_Handler extends TriggerHandler {
	private List<CaseComment> newCaseComments = (List<CaseComment>) this.newList;
	private Map<Id, CaseComment> newCaseCommentMap = (Map<Id, CaseComment>) this.newMap;
	private Map<Id, CaseComment> oldCaseCommentMap = (Map<Id, CaseComment>) this.oldMap;

	private void init() {
		newCaseComments = (List<CaseComment>) this.newList;
		newCaseCommentMap = (Map<Id, CaseComment>) this.newMap;
		oldCaseCommentMap = (Map<Id, CaseComment>) this.oldMap;
	}

	public override void beforeUpdate() {
		init();
		validateWorkLog('edit');
	}

	public override void beforeDelete() {
		init();
		validateWorkLog('delete');
	}
	public override void afterInsert() {
		init();
		sendUserNotifications();
	}
	private void sendUserNotifications() {
		Set<String> parentIds = new Set<String>();

		for (CaseComment c : newCaseComments) {
			parentIds.add(c.parentId);
		}

		Map<String, Case> caseMap = new Map<String, Case>(
			[
				SELECT
					Id,
					owner.username,
					SystemModstamp,
					OwnerId,
					CST_End2End_Owner__r.id,
					Origin,
					CreatedById,
					Contact.Userid__c,
					Contact.Email,
					caseNumber
				FROM Case
				WHERE Id IN :parentIds AND CST_Is_CST_Record_Type__c = TRUE
				ORDER BY SystemModstamp DESC
			]
		);

		List<String> ids2Evaluate = new List<String>();

		if (caseMap.isEmpty())
			return;

		for (Case c : caseMap.values()) {
			if (!String.isBlank(c.OwnerId))
				ids2Evaluate.add(c.OwnerId);
			System.debug('ids2Evaluate ' + c.OwnerId + ' username ');
			if (!String.isBlank(c.CST_End2End_Owner__c))
				ids2Evaluate.add(c.CST_End2End_Owner__r.id);
			if (c.Origin == 'Customer Portal' || c.Origin == 'Chat')
				ids2Evaluate.add(c.Contact.Userid__c);
			System.debug('Case origin ' + c.Origin);
		}

		Map<String, User> ids2Notify = isUserNotifiable(ids2Evaluate);

		// fireNotification('Case '+mycase.CaseNumber+': Case Closed', mycase.CST_Dealer_Contact__r.Name +' closed the case.', mycase.Id, mycase.CST_End2End_Owner__c);

		List<Messaging.SingleEmailMessage> mailingList = new List<Messaging.SingleEmailMessage>();

		Id cstNotificationType = [SELECT Id FROM CUstomNotificationType WHERE DeveloperName = 'CST_Notification_External_Ticket'].Id;

		for (CaseComment cc : newCaseCommentMap.values()) {
			if (!cc.isPublished)
				break;

			Case cs = caseMap.get(cc.ParentId);

			String commenterId = cc.CreatedById;

			User usrNotif = ids2Notify.get(cc.CreatedById);
			//System.debug('CreatedById:' + cc.CreatedById);

			if (ids2Notify.containsKey(commenterId)) {
				for (String idUsr : ids2Notify.keySet()) {
					if (idUsr == commenterId) {
						ids2Notify.remove(idUsr);
					}
				}
			}

			String mesgTitle = 'A New Case Comment has been created';
			String msgBody = 'Please note, a new case comment has been added to the Case ' + cs.caseNumber + ' with: ' + cc.CommentBody;

			if (ids2Notify != null && !ids2Notify.isEmpty())
				fireNotification(mesgTitle, msgBody, cs.Id, ids2Notify, cstNotificationType);

			System.debug('cs.Origin: ' + cs.Origin);
			//System.debug('profile.name: ' + usrNotif.profile.name);
			System.debug('profile.name2: ' + cs.CreatedById);

			User usr = [SELECT Id, Name, Email, Profile_Name__c FROM User WHERE Id = :cs.CreatedById];

			System.debug('usr: ' + usr.Email);

			if (cs.Origin == 'Customer Portal' && usr.Id != commenterId) {
				System.debug('email: ' + cs.Contact.email);
				System.debug('cs.id: ' + cs.id);
				//Messaging.SingleEmailMessage sem = createEmailNotification(usr.Email, cs.id);
				Messaging.SingleEmailMessage sem = createEmailNotification(usr.Email, cs.id);
				System.debug('sem: ' + sem);

				mailingList.add(sem);
			}
		}
		sendEmailNotification(mailingList);
	}

	private static Map<String, User> isUserNotifiable(List<String> oIds) {
		Map<String, User> newUserMap = new Map<String, User>(
			[
				SELECT id, profile.name, contactId, contact.email
				FROM User
				WHERE id IN :oIds AND profile.name IN ('System Administrator', 'VF CST Basic', 'VF CST Customer') AND Alias != 'integ'
			]
		);

		return newUserMap;
	}

	public static void fireNotification(String title, String message, String caseId, Map<String, User> end2endUserId, Id notificId) {
		Messaging.CustomNotification notification = new Messaging.CustomNotification();
		notification.setNotificationTypeId(notificId);
		notification.setTitle(title);
		notification.setBody(message);
		notification.setTargetId(caseId);
		notification.setSenderId(Userinfo.getUserId());
		notification.send(end2endUserId.keySet());
	}
	private void validateWorkLog(String action) {
		List<CaseComment> list2iterate = (action == 'edit') ? newCaseComments : oldCaseCommentMap.values();
		for (CaseComment cc : list2iterate) {
			String comment = cc.CommentBody;
			if (comment.contains('[WorkLog]')) {
				cc.addError('It is not possible to ' + action + ' a WorkLog comment');
			}
		}
	}

	public static void sendEmailNotification(List<Messaging.SingleEmailMessage> mailingList) {
		String fromEmail = 'customerservices@vodafone.nl'; //= [SELECT Id, Address FROM OrgWideEmailAddress WHERE DisplayName = 'Customer Services' AND  LIMIT 1].Address;
		Messaging.sendEmail(mailingList);
	}
	public static Messaging.SingleEmailMessage createEmailNotification(String toEmail, String caseId) {
		EmailTemplate template = [
			SELECT Id, DeveloperName, Name, FolderId, FolderName, IsActive, Encoding, Description, Subject, HtmlValue, Body
			FROM EmailTemplate
			WHERE DeveloperName = 'CST_New_Case_Comment'
		];

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		String[] toAddresses = new List<String>{ toEmail };

		Case cs = [SELECT id, CaseNumber, ContactId FROM Case WHERE id = :caseId];

		//template.Body = template.Body.replace('{!Case.CaseNumber}', cs.CaseNumber);
		//template.HtmlValue = template.HtmlValue.replace('{!Case.CaseNumber}', cs.CaseNumber);
		OrgWideEmailAddress[] owea = [SELECT Id FROM OrgWideEmailAddress WHERE Address = 'customerservices@vodafone.nl'];

		mail.setToAddresses(toAddresses);
		mail.setOrgWideEmailAddressId(owea.get(0).Id);
		mail.setReplyTo(toEmail);
		//mail.setSenderDisplayName('Customer Services');
		mail.setSubject(template.Subject);
		mail.setBccSender(false);
		mail.setPlainTextBody(template.Body);
		mail.setHtmlBody(template.HtmlValue);
		mail.setTemplateId(template.id);
		mail.setWhatId(cs.id);
		mail.setTreatBodiesAsTemplate(true);
		mail.setTargetObjectId(cs.ContactId);

		System.debug('mail: ' + mail);
		System.debug('caseId: ' + caseId);
		System.debug('toAddresses: ' + toAddresses);
		System.debug('body: ' + template.Body);
		System.debug('htmlValue: ' + template.HtmlValue);

		return mail;
	}
}
