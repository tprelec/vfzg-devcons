@IsTest
private class TestCS_CustomStepUpdateCommercialCmps {
    @IsTest
    static void testBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            insert ord;

            Suborder__c solution1 = CS_DataTest.createSolution('Solution 1', ord.Id, false);
            solution1.Product_Abbreviations__c = 'ACCESS,ZIPRO';
            solution1.Status__c = 'Solution Created';
            insert solution1;

            csord__Subscription__c subscription= CS_DataTest.createSubscription(testConfConf.Id);
            subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            subscription.Suborder__c = solution1.Id;
            insert subscription;

            csord__Service__c service = CS_DataTest.createService(testConfConf.Id, subscription, 'Service Created');
            service.csord__Identification__c = 'testSubscription';
            service.Suborder__c = solution1.Id;
            service.VFZ_Product_Abbreviation__c = 'ZIPRO';
            service.Installation_Required__c = true;
            insert service;


            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', false);
            insert step1Template;

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.Suborder__c = solution1.Id;
            insert testProcess;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, false);
            step1.COM_Status__c = 'Ready for MERE';
            step1.COM_Provisioning_Queue__c = 'Provisioning Robot';
            step1.COM_Update_Services__c = true;
            step1.COM_Update_Solutions__c = true;
            insert step1;
            List<CSPOFA__Orchestration_Step__c> stepList = [SELECT Id,
                    CSPOFA__Orchestration_Process__r.Suborder__c,
                    COM_Provisioning_Queue__c,
                    COM_Status__c
            FROM CSPOFA__Orchestration_Step__c
            WHERE Id = :step1.Id];

            CS_CustomStepUpdateStatusCommercialCmps customStepUpdateStatus = new CS_CustomStepUpdateStatusCommercialCmps();
            customStepUpdateStatus.process(stepList);

            List<csord__Service__c> newServices = [SELECT Id, Suborder__c, csord__Service__c, csord__Service__r.Suborder__c, csord__Status__c
            FROM csord__Service__c
            WHERE Suborder__c = :solution1.Id
            OR csord__Service__r.Suborder__c = :solution1.Id];

            for (csord__Service__c newServiceRecord : newServices) {
                System.debug('*** service: ' + JSON.serializePretty(newServiceRecord));
                System.assertEquals('Ready for MERE', newServiceRecord.csord__Status__c);
            }
        }
    }
}