global with sharing class CS_BusinessMobileDeviceLookup extends cscfga.ALookupSearch {
	public override Object[] doLookupSearch(
		Map<String, String> searchFields,
		String productDefinitionID,
		Id[] excludeIds,
		Integer pageOffset,
		Integer pageLimit
	) {
		Decimal contractTerm = Decimal.valueOf(searchFields.get('ContractTermDevice'));
		Date today = Date.valueOf(searchFields.get('Today'));
		String mobileScenario = '%' + searchFields.get('MobileScenarioDevice') + '%';
		String simType = ((searchFields.get('SIMtype') == null) || (searchFields.get('SIMtype') == ''))
			? '%%'
			: '%' + searchFields.get('SIMtype') + '%';
		String brand = ((searchFields.get('Brand') == null) || (searchFields.get('Brand') == '')) ? '%%' : '%' + searchFields.get('Brand') + '%';
		String networkType = searchfields.get('NetworkType');
		String color = searchFields.get('DeviceColor');
		String storage = searchFields.get('Storage');

		System.debug(
			Logginglevel.DEBUG,
			'CS_BusinessMobileDeviceLookup Input parameters for query: ' +
			' contractTerm => ' +
			contractTerm +
			' mobileScenario => ' +
			mobileScenario +
			' simType => ' +
			simType +
			' networkType => ' +
			networkType +
			' storage => ' +
			storage +
			' brand => ' +
			brand +
			' color => ' +
			color +
			' today => ' +
			today
		);

		String selectPart =
			'SELECT ' +
			'Name, ' +
			'Id, ' +
			'Vendor_lookup__r.Name, ' +
			'cspmb__Contract_Term__c, ' +
			'cspmb__recurring_charge__c, ' +
			'cspmb__One_Off_Charge__c, ' +
			'Contract_Term_Number__c, ' +
			'Available_bandwidth_down__c, ' +
			'Available_bandwidth_up__c, ' +
			'Recurring_Charge_Product__r.Taxonomy__r.Portfolio__c, ' +
			'One_Off_Charge_Product__r.Unify_Charge_Description__c, ' +
			'One_Off_Charge_Product__r.Unify_Charge_Code__c, ' +
			'One_Off_Charge_Product__r.ProductCode, ' +
			'One_Off_Charge_Product__r.Taxonomy__r.Name, ' +
			'One_Off_Charge_Product__r.Taxonomy__r.Portfolio__c, ' +
			'One_Off_Charge_Product__r.Taxonomy__r.Product_Family__c, ' +
			'Recurring_Charge_Product__r.Unify_Charge_Description__c, ' +
			'Recurring_Charge_Product__r.Unify_Charge_Code__c, ' +
			'Recurring_Charge_Product__r.ProductCode, ' +
			'Recurring_Charge_Product__r.Taxonomy__r.Name, ' +
			'Recurring_Charge_Product__r.Taxonomy__r.Product_Family__c, ' +
			'cspmb__Is_Active__c, ' +
			'Category__c, ' +
			'VFZ_Variation_Tier_Name__c, ' +
			'cspmb__Effective_End_Date__c, ' +
			'cspmb__Effective_Start_Date__c, ' +
			'cspmb__Master_Price_item__c, ' +
			'cspmb__Recurring_Charge_External_Id__c, ' +
			'Recurring_Charge_Product__c, ' +
			'One_Off_Charge_Product__r.Name, ' +
			'LG_UploadSpeed__c, ' +
			'VFZ_Commercial_Article_Name__c, ' +
			'LG_DownloadSpeed__c, ' +
			'VFZ_Commercial_Component_Name__c,' +
			'LG_ProductFamily__c, ' +
			'Recurring_Charge_Product__r.Name, ' +
			'cspmb__One_Off_Charge_External_Id__c, ' +
			'cspmb__Product_Definition_Name__c, ' +
			'One_Off_Charge_Product__c, ' +
			'Mobile_Add_on_category__c, ' +
			'cspmb__One_Off_Cost__c, ' +
			'One_Off_Charge_Product__r.ThresholdGroup__c, ' +
			'Mobile_Scenario__c, ' +
			'cspmb__Recurring_Cost__c, ' +
			'Recurring_Charge_Product__r.ThresholdGroup__c, ' +
			'Group__c ' +
			'FROM cspmb__Price_Item__c ';

		String wherePart =
			'WHERE ' +
			'cspmb__Is_Active__c = TRUE ' +
			'AND Mobile_Add_on_category__c = \'Mobile hardware\' ' +
			'AND cspmb__Effective_Start_Date__c <= :today ' +
			'AND cspmb__Effective_End_Date__c >= :today ' +
			'AND Min_Duration__c <= :contractTerm ' +
			'AND Max_Duration__c >= :contractTerm ' +
			'AND Mobile_Scenario_Text__c LIKE :mobileScenario ' +
			'AND SimType__c LIKE :simType ' +
			'AND Mobile_Vendor__c LIKE :brand ' +
			'AND Network_Technology__c INCLUDES (:networkType) ';

		if (color == null || color == '') {
			wherePart += 'AND (Color__c = \'\' OR Color__c LIKE \'%%\') ';
		} else {
			wherePart += 'AND Color__c = :color ';
		}

		if (storage == null || storage == '') {
			wherePart += 'AND (Storage_Capacity__c  = \'\' OR Storage_Capacity__c  LIKE \'%%\') ';
		} else {
			wherePart += 'AND Storage_Capacity__c  = :storage ';
		}

		String query = selectPart + wherePart;
		List<cspmb__Price_Item__c> priceItems = Database.query(query);

		System.debug(Logginglevel.DEBUG, 'Number of priceItems: ' + priceItems.size());
		return priceItems;
	}

	public override String getRequiredAttributes() {
		return '["ContractTermDevice","MobileScenarioDevice","SIMtype","NetworkType","Storage","Brand","DeviceColor","Today"]';
	}
}
