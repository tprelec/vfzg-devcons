// Name TestCustomButtonOrderDecompositionSimulation was too long
@isTest
private with sharing class TestCustomButtonODS {
	@isTest
	private static void testPerformAction() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;
		System.runAs(simpleUser) {
			Test.startTest();

			Account testAccount = CS_DataTest.createAccount('Test Account');
			insert testAccount;

			Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', simpleUser.id);
			insert testOpp;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
			basket.Primary__c = true;
			basket.cscfga__Basket_Status__c = 'Valid';
			insert basket;

			CustomButtonOrderDecompositionSimulation button = new CustomButtonOrderDecompositionSimulation();

			String result = button.performAction(String.valueOf(basket.Id));
			System.assertNotEquals('', result);

			Test.stopTest();
		}
	}
}
