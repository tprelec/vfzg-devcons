@isTest
public with sharing class DealSummaryTotalOverviewDataTest {
    public DealSummaryTotalOverviewDataTest() {

    }

    @isTest
    static void test()
    {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
	    
    	System.runAs (simpleUser) {
    	    PriceReset__c priceResetSetting = new PriceReset__c();
           
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
            
            insert priceResetSetting;
    
            Account testAccount = CS_DataTest.createAccount('Test Account 1');
            Account testAccount2 = CS_DataTest.createAccount('Test Account 2');
            List<Account> accList = new List<Account>{testAccount,testAccount2};
            insert accList;
            
            Contact contact1 = new Contact(
                AccountId = testAccount.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'   
            );
            insert contact1;
            
            testAccount.Authorized_to_sign_1st__c = contact1.Id;
            testAccount.Contract_rule_no_mailing__c = true;
            testAccount.Frame_Work_Agreement__c = 'VFZA-2018-8';
            testAccount.Version_FWA__c = 4;
            testAccount.Framework_agreement_date__c = Date.today();
            update testAccount;
            
            Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Contract opportunity',simpleUser.id);
            Opportunity opp2 = new Opportunity();
            opp2.Name = 'testName';
            opp2.Account = testAccount2;
            opp2.StageName= 'Qualification';
            opp2.CloseDate = Date.Today()+10;
            
            List<Opportunity> oppList = new List<Opportunity>{opp, opp2};
            insert oppList;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Contract Basket');
            basket.Name = 'Contract Basket';
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = testAccount.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 24;
            basket.OneNet_Scenario__c = 'One Net Enterprise';
            basket.Fixed_Scenario__c = 'One Fixed Enterprise';
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 02, 02);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 02, 02);
            basket.Number_of_SIP__c = 12;
            basket.Approved_Date__c = Date.today() - 4;
            basket.PortingPercentage__c = 10;
            
            cscfga__Product_Basket__c basket2 = CS_DataTest.createProductBasket(opp2, 'Contract Basket 2');
            basket2.Name = 'Contract Basket 2';
            basket2.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket2.cscfga__Basket_Status__c = 'Valid';
            basket2.Primary__c = true;
            basket2.csbb__Synchronised_with_Opportunity__c = true;
            basket2.csbb__Account__c = testAccount.Id;
            basket2.Contract_duration_Mobile__c = '24';
            basket2.Contract_duration_Fixed__c = 24;
            basket2.OneNet_Scenario__c = 'One Net Enterprise';
            basket2.Fixed_Scenario__c = 'One Fixed Enterprise';
            basket2.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 02, 02);
            basket2.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 02, 02);
            basket2.Number_of_SIP__c = 12;
            basket2.Approved_Date__c = Date.today() - 4;
            basket2.PortingPercentage__c = 10;

            cscfga__Product_Definition__c mobCTNSubsDef = CS_DataTest.createProductDefinition('Mobile CTN subscription');
            mobCTNSubsDef.Product_Type__c = 'Mobile';
            insert mobCTNSubsDef;

            cscfga__Product_Configuration__c mobCTNSubs = CS_DataTest.createProductConfiguration(mobCTNSubsDef.Id, 'Flex Mobile Basic Voice',basket.Id);
            mobCTNSubs.Deal_Type__c = 'Disconnect';
            insert mobCTNSubs;
            
            List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>{basket, basket2};
            insert basketList;

            CS_Basket_Snapshot_Transactional__c basketSnapTrans1 = new CS_Basket_Snapshot_Transactional__c();
        	basketSnapTrans1.Name = 'Access Infrastructure 1';
            basketSnapTrans1.FinalPriceOneOff__c = 9;
            basketSnapTrans1.FinalPriceRecurring__c  = 110;
            basketSnapTrans1.Quantity__c  = 10;
        	basketSnapTrans1.DiscountOneOff__c = 11;
        	basketSnapTrans1.DiscountRecurring__c = 12;
            basketSnapTrans1.Product_Basket__c = basket.id;
            basketSnapTrans1.Product_Configuration__c = mobCTNSubs.Id;
            
            CS_Basket_Snapshot_Transactional__c basketSnapTrans2 = new CS_Basket_Snapshot_Transactional__c();
        	basketSnapTrans2.Name = 'Access Infrastructure 2';
            basketSnapTrans2.FinalPriceOneOff__c = 9;
            basketSnapTrans2.FinalPriceRecurring__c  = 120;
            basketSnapTrans2.Quantity__c  = 5;
        	basketSnapTrans2.DiscountOneOff__c = 11;
        	basketSnapTrans2.DiscountRecurring__c = 12;
            basketSnapTrans2.Product_Basket__c = basket.id;
            
            List<CS_Basket_Snapshot_Transactional__c> snaps = new List<CS_Basket_Snapshot_Transactional__c> {basketSnapTrans1, basketSnapTrans2};
            insert snaps;

            System.debug('Basket.id: ' + basket.Id);

            Test.startTest();

            List<CS_Basket_Snapshot_Transactional__c> snapshots = CS_SnapshotHelper.getBasketSnapshots(String.valueOf(basket.Id));
            DealSummaryTotalOverviewData data = new DealSummaryTotalOverviewData(snapshots, basket);

            Test.stopTest();

        }
    }
}