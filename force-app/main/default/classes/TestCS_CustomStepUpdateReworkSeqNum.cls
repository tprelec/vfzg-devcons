@IsTest
private class TestCS_CustomStepUpdateReworkSeqNum {
    @IsTest
    static void testBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            List<SObject> objects = new List<SObject>();
            Map<Id, VF_Contract__c> vfContractsBeforeMap = new Map<Id, VF_Contract__c>();
            Map<Id, VF_Contract__c> vfContractsAfterMap;

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);
            CSPOFA__Orchestration_Step_Template__c step2Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 2', '2', true);

            VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(simpleUser, false);
            vfContract.Rework_Sequence_Nr__c = 0;
            insert vfContract;
            vfContractsBeforeMap.put(vfContract.Id, vfContract);

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.Contract_VF__c = vfContract.Id;
            insert testProcess;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);
            objects.add(step1);

            CSPOFA__Orchestration_Step__c step2 = CS_DataTest.createOrchStep(step2Template.Id, testProcess.Id, true);
            objects.add(step2);

            CS_CustomStepUpdateReworkSequenceNumber csUpdateRewSeqNum = new CS_CustomStepUpdateReworkSequenceNumber();
            List<CSPOFA__Orchestration_Step__c> result = csUpdateRewSeqNum.process(objects);

            for (CSPOFA__Orchestration_Step__c step : result) {
                System.assert(step.CSPOFA__Status__c <> 'Error', 'Process step ended with exceptions');
            }

            vfContractsAfterMap = new Map<Id, VF_Contract__c>([
                SELECT Id,
                    Rework_Sequence_Nr__c
                FROM VF_Contract__c
                WHERE Id IN :vfContractsBeforeMap.keySet()
            ]);

            for (VF_Contract__c vfCon : vfContractsAfterMap.values()) {
                System.assertEquals(vfContractsBeforeMap.get(vfCon.Id).Rework_Sequence_Nr__c + 1, vfContractsAfterMap.get(vfCon.Id).Rework_Sequence_Nr__c);
            }
        }
    }
}