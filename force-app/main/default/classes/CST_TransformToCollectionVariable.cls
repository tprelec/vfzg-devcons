global with sharing class CST_TransformToCollectionVariable {
	@InvocableMethod
	public static List<list<string>> CheckValues(List<string> values) {
		list<list<string>> finalLst = new List<list<string>>();
		List<Site__c> sitesList = [SELECT id, name FROM Site__c WHERE Active__c = TRUE];
		System.debug('values -> ' + values);
		System.debug('values.size() -> ' + values.size());
		System.debug('values.isEmpty() -> ' + values.isEmpty());
		System.debug('values.is null? -> ' + values == null);

		if (sitesList.size() == 1) {
			finalLst.add(new List<String>{ sitesList[0].id });
			System.debug('finalLst with empty values -> ' + finalLst);
			return finalLst;
		} else if (!(values.isEmpty())) {
			string tempStr = values[0];
			List<String> lstnew = tempStr.split(';');
			finalLst.add(lstnew);
			System.debug('finalLst with values not empty-> ' + finalLst);
			return finalLst;
		} else
			return null;
	}
}
