global without sharing class CS_CustomStepCompleteTaskReschedule implements CSPOFA.ExecutionHandler {
	global List<SObject> process(List<SObject> steps) {
		List<SObject> result = new List<sObject>();
		Map<Id, CSPOFA__Orchestration_Step__c> deliveryOrderStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
		Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c>();

		List<CSPOFA__Orchestration_Step__c> stepList = [
			SELECT
				ID,
				Name,
				CSPOFA__Orchestration_Process__c,
				CSPOFA__Orchestration_Process__r.Id,
				CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c,
				CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c,
				CSPOFA__Status__c,
				CSPOFA__Completed_Date__c,
				CSPOFA__Message__c,
				CSPOFA__Expression_Result__c,
				CSPOFA__Related_Object__c,
				CSPOFA__Related_Object_ID__c,
				COM_Task_Status__c
			FROM CSPOFA__Orchestration_Step__c
			WHERE Id IN :steps
		];

		try {
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				deliveryOrderStepMap.put(step.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c, step);
				processesMap.put(step.CSPOFA__Orchestration_Process__r.Id, step.CSPOFA__Orchestration_Process__r);
			}

			List<Task> tasks = [
				SELECT
					Id,
					Subject,
					IsClosed,
					RecordType.Name,
					CSPOFA__Orchestration_Step__c,
					CSPOFA__Orchestration_Step__r.OnSiteVisit__c,
					CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__c,
					CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c
				FROM Task
				WHERE
					IsClosed = FALSE
					AND Type = 'COM_Installation'
					AND CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__c IN :processesMap.keySet()
			];

			CS_OrchestratorStepUtility.updateTasksAndPauseSteps(tasks, deliveryOrderStepMap);

			for (CSPOFA__Orchestration_Step__c step : stepList) {
				result.add(CS_OrchestratorStepUtility.setStepToComplete(step));
			}
		} catch (Exception ex) {
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				result.add(CS_OrchestratorStepUtility.setStepToError(step, ex));
			}
		}
		return result;
	}
}
