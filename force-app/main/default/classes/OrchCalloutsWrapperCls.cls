public class OrchCalloutsWrapperCls {
    public class CalloutData{
        public Boolean success;
        public String strReq {get;set;}
        public String strRes {get;set;}
        public String errorMessage;
        public Map<String,String> infoToCOM;
        public calloutData( Boolean success, String errorMessage, Map<String,String> infoToCOM ) {
            this.success = success;
            this.errorMessage = errorMessage;
            this.infoToCOM = infoToCOM;
        }
    }
}