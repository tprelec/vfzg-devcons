global class CsEventBatch implements Database.Batchable<sObject>, Schedulable{

    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('Select ID, KVKNummer__c FROM DWH_Events_SF_Interface__c WHERE Processed__c = false');
    }

    public void execute(Database.BatchableContext context, List<SObject> records) {
        Set<String> kvkSet = new Set<String>();
        Map<String, Id> eventToLeadMap = new Map<String, Id>();
        Map<String, Id> eventToAccountMap = new Map<String, Id>();

        for (SObject obj : records) {

            DWH_Events_SF_Interface__c eventRec = (DWH_Events_SF_Interface__c)obj;
            if(eventRec.KVKNummer__c.length() == 7) {
                eventRec.KVKNummer__c = '0' + eventRec.KVKNummer__c;
            }
            kvkSet.add(eventRec.KVKNummer__c);
        }
        List<Lead> leadList = [Select Id, KVK_number__c From Lead WHERE KVK_number__c IN : kvkSet];
        List<Account> accList = [Select Id, KVK_number__c From Account WHERE KVK_number__c IN : kvkSet];

        for (Lead l : leadList) {
            eventToLeadMap.put(l.KVK_number__c, l.Id);
        }
        for (Account acc : accList) {
            eventToAccountMap.put(acc.KVK_number__c, acc.Id);
        }
        for (SObject obj : records) {
            DWH_Events_SF_Interface__c eventRec = (DWH_Events_SF_Interface__c)obj;
            if (eventToAccountMap.containsKey(eventRec.KVKNummer__c)) {
                eventRec.Account__c = eventToAccountMap.get(eventRec.KVKNummer__c);
            } else if (eventToLeadMap.containsKey(eventRec.KVKNummer__c)) {
                eventRec.Lead__c = eventToLeadMap.get(eventRec.KVKNummer__c);
            }
            eventRec.Processed__c = true;
        }
        try {
            update records;
        } catch(Exception e) {
            system.debug('##error: '+ e.getMessage());
        }
    }

    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new CsEventBatch());
    }

    global void finish(Database.BatchableContext BC) {}
}