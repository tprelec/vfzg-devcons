global with sharing class CustomButtonInflightSimulation extends csbb.CustomButtonExt{
    public String performAction (String basketId) {
        PageReference editPage = new PageReference('/apex/csordtelcoa__InflightSimulation?id=' + basketId);
        System.debug('CustomButtonInflightSimulation URL: ' + editPage.getUrl());
        return '{"status":"ok","redirectURL":"' + editPage.getUrl() + '"}';
    }
}