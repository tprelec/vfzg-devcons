public with sharing class COM_CDOcreateTenant {
	public static void createTenantService(Id accountId) {
		COM_CDOcreateTenantService request = COM_CDOcreateTenantService.generatePayload();
		createTenantService(request, accountId);
	}

	private static void createTenantService(COM_CDOcreateTenantService req, Id accountId) {
		COM_Integration_setting__mdt createTenantSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);

		HTTPResponse response = sendCreateTenantRequest(req, createTenantSetting, accountId);

		if (failedResponse(response)) {
			CreateTenantSyncErrorResponse responseDeserialized = (CreateTenantSyncErrorResponse) JSON.deserialize(
				response.getBody(),
				CreateTenantSyncErrorResponse.class
			);

			CreateTenantAccountUpdateData accountUpdate = new CreateTenantAccountUpdateData();
			accountUpdate.accountId = accountId;
			accountUpdate.successful = false;
			accountUpdate.tenantId = null;
			accountUpdate.umbrellaService = null;
			accountUpdate.errorMessage = responseDeserialized.message;

			updateAccount(accountUpdate);
		} else {
			String reqBody = COM_CDOServiceCharacteristicHelper.modifyRequestBody(response.getBody());
			COM_CDOcreateTenantService body = (COM_CDOcreateTenantService) JSON.deserializeStrict(reqBody, COM_CDOcreateTenantService.class);
			String umbrellaService = COM_CDOcreateTenant.getUmbrellaService(body);

			CreateTenantAccountUpdateData accountUpdate = new CreateTenantAccountUpdateData();
			accountUpdate.accountId = accountId;
			accountUpdate.successful = true;
			accountUpdate.tenantId = null;
			accountUpdate.umbrellaService = umbrellaService;
			accountUpdate.errorMessage = null;

			updateAccount(accountUpdate);
		}
	}

	private static Boolean failedResponse(HTTPResponse response) {
		Boolean result = false;
		try {
			CreateTenantSyncErrorResponse responseDeserialized = (CreateTenantSyncErrorResponse) JSON.deserialize(
				response.getBody(),
				CreateTenantSyncErrorResponse.class
			);
			if (responseDeserialized.code != null) {
				result = true;
			}
		} catch (Exception e) {
			result = false;
		}

		return result;
	}
	public static HTTPResponse sendCreateTenantRequest(
		COM_CDOcreateTenantService req,
		COM_Integration_setting__mdt createTenantSetting,
		Id accountId
	) {
		Boolean doNotExecuteReq = createTenantSetting != null ? createTenantSetting.Execute_mock__c : true;
		Boolean doNotExecuteStatus = createTenantSetting != null ? createTenantSetting.Mock_Execution_result__c : true;
		Boolean generateAttachment = createTenantSetting != null ? createTenantSetting.Generate_attachment__c : true;

		String namedCredentials = createTenantSetting.NamedCredential__c;

		HttpRequest reqData = new HttpRequest();
		Http http = new Http();

		String endpoint = COM_WebServiceConfig.getEndpoint(namedCredentials, COM_CDO_Constants.COM_WEB_PARAM);

		reqData.setHeader('Content-Type', 'application/json');
		reqData.setEndpoint(endpoint);

		String requestBody = COM_CDOcreateTenantService.generatePayloadJson(req);
		reqData.setBody(requestBody);
		reqData.setMethod('POST');
		HTTPResponse response = new HttpResponse();
		if (doNotExecuteReq && !Test.isRunningTest()) {
			response.setHeader('Content-Type', 'application/json');
			if (doNotExecuteStatus) {
				String mockSuccess = COM_CDOHelper.getMockResponse(
					COM_CDO_Constants.COM_CREATE_TENANT_SYNC_SUCCESS_MOCK,
					COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
				);
				response.setBody(mockSuccess);
				response.setStatusCode(200);
			} else {
				String mockFailed = COM_CDOHelper.getMockResponse(
					COM_CDO_Constants.COM_CREATE_TENANT_SYNC_FAILED_MOCK,
					COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
				);
				response.setBody(mockFailed);

				response.setStatusCode(400);
			}
		} else {
			response = http.send(reqData);
		}

		if (generateAttachment) {
			COM_CDOHelper.createPayloadAttachment(accountId, COM_CDO_Constants.COM_CDO_CREATE_TENANT_PAYLOAD_ATTACHMENT_NAME, requestBody);
		}

		return response;
	}

	public static Id getAccountIdByUmbrellaService(String umbrellaService) {
		Account acc = [SELECT Id FROM Account WHERE CDO_Umbrella_Service_Id__c = :umbrellaService LIMIT 1];
		if (acc != null) {
			return acc.Id;
		} else {
			return null;
		}
	}

	public static String getUmbrellaService(COM_CDOcreateTenantService req) {
		String result = '';
		for (COM_CDOcreateTenantService.OrderItem orderItem : req.orderItem) {
			result = orderItem.service.id;
		}
		return result;
	}

	public static void updateAccount(CreateTenantAccountUpdateData accountUpdate) {
		Account acc = new Account();
		acc.CDO_Umbrella_Service_Id__c = accountUpdate.umbrellaService != null ? accountUpdate.umbrellaService : '';
		acc.Id = accountUpdate.accountId;

		if (accountUpdate.successful) {
			if (accountUpdate.tenantId != null) {
				acc.CDO_Tenant_Id__c = accountUpdate.tenantId;
				acc.CDO_Integration_Status__c = COM_Constants.CDO_INTEGRATION_STATUS_COMPLETE;
			} else {
				acc.CDO_Integration_Status__c = '';
			}
			acc.CDO_Error_Message__c = '';
		} else {
			acc.CDO_Integration_Status__c = COM_Constants.CDO_INTEGRATION_STATUS_FAILURE;
			acc.CDO_Error_Message__c = accountUpdate.errorMessage;
		}
		update acc;
	}

	public class CreateTenantSyncErrorResponse {
		public Integer code;
		public String reason;
		public String message;
	}

	public class CreateTenantAccountUpdateData {
		public Id accountId;
		public Boolean successful;
		public String tenantId;
		public String umbrellaService;
		public String errorMessage;
	}
}
