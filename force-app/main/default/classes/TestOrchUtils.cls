@isTest
public class TestOrchUtils {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			ban,
			owner
		);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(
			claimerContact.id,
			'123321'
		);
		contr.Dealer_Information__c = dealerInfo.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;
	}

	@isTest
	public static void testOrchUtilsCreation() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String strOrderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', strOrderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);

		Set<Id> resultIds = new Set<Id>();
		resultIds.add(steps[0].CSPOFA__Orchestration_Process__c);
		Test.startTest();
		OrchUtils.getProcessDetails(resultIds);

		System.debug(LoggingLevel.INFO, 'orderId testing' + OrchUtils.mapOrderId); //to test getter setter
		System.debug(LoggingLevel.INFO, 'salesJourney testing' + OrchUtils.mapsalesJourney); //to test getter setter
		System.debug(LoggingLevel.INFO, 'contractId testing' + OrchUtils.mapcontractId); //to test getter setter
		System.debug(LoggingLevel.INFO, 'billingCustomer testing' + OrchUtils.mapbillingCustomer); //to test getter setter
		System.debug(LoggingLevel.INFO, 'mapUnifyOrderId testing' + OrchUtils.mapUnifyOrderId); //to test getter setter
		System.debug(
			LoggingLevel.INFO,
			'mapcontractedProductUnifyOrderId testing' + OrchUtils.mapcontractedProductUnifyOrderId
		); //to test getter setter
		System.debug(
			LoggingLevel.INFO,
			'contractedProductId testing' + OrchUtils.mapcontractedProductId
		); //to test getter setter
		System.debug(LoggingLevel.INFO, 'cpFrameworkId testing' + OrchUtils.mapcpFrameworkId); //to test getter setter
		CSPOFA__Orchestration_Step__c stepAssert = OrchUtils.setStepRecord(
			steps[0],
			false,
			'success'
		);
		System.assertEquals(
			'success',
			stepAssert.CSPOFA__Message__c,
			'Meaning that the step from Orchestrator was well returned'
		);
		System.assertEquals(
			'Complete',
			stepAssert.CSPOFA__Status__c,
			'Meaning that the step from Orchestrator was well returned'
		);
		Test.stopTest();
	}

	@isTest
	public static void testOrchGARHandlerErrorCatch() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			processTemplate,
			fieldKeyMap,
			true
		);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(
			processes,
			stepTemplates,
			true
		);

		Test.startTest();
		List<CSPOFA__Orchestration_Step__c> results = (List<CSPOFA__Orchestration_Step__c>) OrchUtils.tryUpdateRelatedRecord(
			steps,
			new List<sObject>(),
			null
		);

		System.assertEquals('Error', results[0].CSPOFA__Status__c, 'NO CTNs to Update');
		Test.stopTest();
	}
}