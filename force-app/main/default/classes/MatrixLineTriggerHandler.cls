/**
 * @description       : Apex Class Handler of the Matrix Line Trigger
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   20-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

public class MatrixLineTriggerHandler extends TriggerHandler {
	private List<appro__Matrix_Line__c> lstNewMatrixLine;

	private Map<Id, appro__Matrix_Line__c> mapOldMatrixLine;
	private Map<Id, appro__Matrix_Line__c> mapNewMatrixLine;

	/**
	 * @description Initialize Class Variables
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	private void init() {
		lstNewMatrixLine = (List<appro__Matrix_Line__c>) this.newList;
		mapOldMatrixLine = (Map<Id, appro__Matrix_Line__c>) this.oldMap;
		mapNewMatrixLine = (Map<Id, appro__Matrix_Line__c>) this.newMap;
	}

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public void setExternalIds() {
		Sequence objSequence = new Sequence('Matrix_Lines');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (appro__Matrix_Line__c objMatrixLine : lstNewMatrixLine) {
				objMatrixLine.ExternalID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
