@IsTest
public class TestOrderEntryWebCart {
	private static final String CART_ID = 'cf49b52c-9275-45ea-98fa-778c1c1a858e';

	@TestSetup
	static void makeData() {
		// TBD prepare test opportunity and other data
	}

	@IsTest
	static void testCreateCart() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCreateJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String cartId = OrderEntryWebCart.createCart(true);
		Test.stopTest();
		System.assertEquals(CART_ID, cartId, 'Cart ID should be returned');
	}

	@IsTest
	static void testSetChosenOrderType() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartChosenOrderTypeJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setChosenOrderType(CART_ID, true);
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testSetPersonalData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCustomerPersonalJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setPersonalData(CART_ID, Date.newInstance(1979, 7, 7), 'MALE', 'MD', 'Dr.', 'Dvecko');
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testSetContactData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCustomerContactJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setContactData(CART_ID, 'marko@mozzaik.io', '0911234567', null);
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testSetIdentificationData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCustomerIdentificationJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setIdentificationData(CART_ID, Date.newInstance(2024, 11, 11), 'AB1234C23', 'ID_CARD', 'NLD');
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testSetCompanyData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCustomerCompanyJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setCompanyData(
			CART_ID,
			'Steenwijk',
			'Generaal',
			'51',
			null,
			'8333DV',
			'59300787',
			Date.newInstance(2013, 11, 1),
			'RED HOT CLASSICS',
			'NL123456789B99',
			OrderEntryWebCart.legalFormToEnum('B.V.')
		);
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testSetDeliveryAddressData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartDeliveryAddressJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setDeliveryAddressData(CART_ID, 'Steenwijk', 'Generaal', '51', null, '8333DV');
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testAddItemsToCart() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartItemsJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		List<OrderEntryWebCartJSON.Item> items = new List<OrderEntryWebCartJSON.Item>();
		OrderEntryWebCartJSON.Item item = new OrderEntryWebCartJSON.Item();
		item.sku = 's_4100958';
		item.action = 'ADD';
		item.quantity = 1;
		item.withSim = false;
		items.add(item);
		String response = OrderEntryWebCart.addItemsToCart(CART_ID, items);
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testSetMobileNumberPorting() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartMobileNumberPortingsJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setMobileNumberPorting(CART_ID, '1', 'TEST', '33915856208', 1);
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testSetTermsAgreed() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartTermsAgreedJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setTermsAgreed(CART_ID);
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testSetSummaryTermsAgreed() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartSummaryTermsAgreedJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setSummaryTermsAgreed(CART_ID);
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testSetPaymentData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartPaymentInfoJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		String response = OrderEntryWebCart.setPaymentData(CART_ID, 'nvt', 'NL46ABNA0582434874');
		Test.stopTest();

		System.assert(response != null, 'Response should be returned');
	}

	@IsTest
	static void testPrepareCart() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCreateJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrderEntryData d = new OrderEntryData();
		d.bundles = new List<OrderEntryData.OrderEntryBundle>();
		OrderEntryWebCart.sendOrder(
			new Contact(),
			new Account(),
			new csconta__Billing_Account__c(),
			d,
			new Site__c(),
			new Map<Id, OE_Product_Bundle__c>()
		);
		Test.stopTest();

		// TODO > Enhance
	}

	@IsTest
	static void testPlaceOrder() {
		// TBD place order
	}

	@IsTest
	static void testProcessOrders() {
		// TBD test with one opportunity Id
	}
}
