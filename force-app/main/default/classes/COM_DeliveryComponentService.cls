public without sharing class COM_DeliveryComponentService {
	public Map<Id, COM_Delivery_Component__mdt> getDeliveryComponentMap(Set<Id> deliveryComponentIds) {
		Map<Id, COM_Delivery_Component__mdt> deliveryComponentsMap = new Map<Id, COM_Delivery_Component__mdt>();

		deliveryComponentsMap = new Map<Id, COM_Delivery_Component__mdt>(
			[
				SELECT Id, Label, DeveloperName, Description__c, Installation__c, Provisioning__c, Provisioning_System__c
				FROM COM_Delivery_Component__mdt
				WHERE Id IN :deliveryComponentIds
			]
		);

		return deliveryComponentsMap;
	}

	public Map<Id, COM_Decomposition__mdt> getDecompositionsMap(Set<String> serviceCommercialComponents, Set<String> serviceCommercialArticles) {
		Map<Id, COM_Decomposition__mdt> decompositionsMap = new Map<Id, COM_Decomposition__mdt>(
			[
				SELECT
					Id,
					DeveloperName,
					Commercial_Article__c,
					Commercial_Component__c,
					Delivery_Article__c,
					Delivery_Article__r.Delivery_Component__c
				FROM COM_Decomposition__mdt
				WHERE Commercial_Article__c IN :serviceCommercialArticles AND Commercial_Component__c IN :serviceCommercialComponents
			]
		);

		return decompositionsMap;
	}

	public Map<Id, COM_Delivery_Article__mdt> getDeliveryArticlesMap(Set<Id> deliveryArticleIds) {
		Map<Id, COM_Delivery_Article__mdt> deliveryArticlesMap = new Map<Id, COM_Delivery_Article__mdt>();

		deliveryArticlesMap = new Map<Id, COM_Delivery_Article__mdt>(
			[
				SELECT Id, Label, DeveloperName, Delivery_Article_Code__c, Delivery_Article_Name__c, Delivery_Component__c
				FROM COM_Delivery_Article__mdt
				WHERE Id IN :deliveryArticleIds
			]
		);

		return deliveryArticlesMap;
	}

	public List<COM_Delivery_Article_Materials__mdt> getDeliveryArticleMaterials(Set<Id> deliveryArticleIds) {
		List<COM_Delivery_Article_Materials__mdt> deliveryComponentArticleMaterials = new List<COM_Delivery_Article_Materials__mdt>();

		deliveryComponentArticleMaterials = [
			SELECT
				Id,
				DeveloperName,
				COM_Delivery_Article__c,
				COM_Delivery_Article__r.DeveloperName,
				COM_Delivery_Article__r.Label,
				COM_Delivery_Article__r.Delivery_Article_Name__c,
				COM_Delivery_Article__r.Delivery_Component__r.DeveloperName,
				COM_Delivery_Materials__c,
				COM_Delivery_Materials__r.DeveloperName,
				COM_Delivery_Materials__r.Label,
				COM_Delivery_Materials__r.Returnable__c,
				COM_Delivery_Materials__r.Returnable_by_Box__c,
				COM_Delivery_Materials__r.Model_Name__c,
				COM_Delivery_Materials__r.BOP_C_code__c,
				COM_Delivery_Materials__r.Generate_Asset__c,
				COM_Delivery_Materials__r.Type__c
			FROM COM_Delivery_Article_Materials__mdt
			WHERE COM_Delivery_Article__c IN :deliveryArticleIds
		];

		return deliveryComponentArticleMaterials;
	}
}
