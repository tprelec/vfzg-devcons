@isTest
private class CustomButtonReviseContractTest {
	private static testMethod void test() {
		List<Profile> pList = [
			SELECT Id, Name
			FROM Profile
			WHERE Name = 'System Administrator'
			LIMIT 1
		];
		List<UserRole> roleList = [
			SELECT Id, Name, DeveloperName
			FROM UserRole u
			WHERE ParentRoleId = NULL
		];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Test.startTest();

			No_Triggers__c notriggers = new No_Triggers__c();
			notriggers.SetupOwnerId = simpleUser.Id;
			notriggers.Flag__c = true;
			insert notriggers;

			Account tmpAcc = CS_DataTest.createAccount('Test Account');
			insert tmpAcc;

			Opportunity tmpOpp = CS_DataTest.createOpportunity(
				tmpAcc,
				'Test Opportunity',
				simpleUser.Id
			);
			insert tmpOpp;

			cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(
				tmpOpp,
				'Test basket',
				tmpAcc
			);
			insert tmpProductBasket;

			cscfga__Product_Basket__c tmpProductBasket2 = CS_DataTest.createProductBasket(
				tmpOpp,
				'Test basket',
				tmpAcc
			);
			tmpProductBasket2.cscfga__Basket_Status__c = 'Contract Created';
			insert tmpProductBasket2;

			csclm__Document_Definition__c docDef = new csclm__Document_Definition__c();
			docDef.name = 'Temp Doc Def';
			docDef.csclm__Document_Type__c = 'Contract';
			docDef.csclm__Linked_Object__c = 'cscfga__Product_Basket__c';
			docDef.ExternalID__c = 'SomeId2';
			insert docDef;

			csclm__Document_Template__c docTemp = new csclm__Document_Template__c();
			docTemp.csclm__Document_Definition__c = docDef.Id;
			docTemp.name = 'Temp Doc';
			docTemp.csclm__Active__c = true;
			docTemp.csclm__Valid__c = true;
			docTemp.ExternalID__c = 'SomeId2';
			insert docTemp;

			csclm__Agreement__c agreement = new csclm__Agreement__c();
			agreement.Name = 'Test Agreement';
			agreement.csclm__Output_Format__c = 'pdf';
			agreement.csclm__Document_Template__c = docTemp.Id;
			agreement.Product_Basket__c = tmpProductBasket2.Id;
			insert agreement;

			CustomButtonReviseContract reviseContract = new CustomButtonReviseContract();
			reviseContract.performAction(tmpProductBasket2.Id);

			Test.stopTest();
		}
	}

	private static testmethod void testDeleteBasketAgreementNegative() {
		Boolean result;
		Test.startTest();
		result = DeleteBasketAgreementWithoutSharing.deleteBasketAgreement(null);
		Test.stopTest();

		System.assertEquals(result, false, 'Method shouldn\'t delete anything and return false');
	}
}