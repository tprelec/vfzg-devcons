public with sharing class OrderEntryWebJob implements Schedulable {
	/**
	 * Map of SIM card names
	 */
	public static final Map<String, String> SIM_NAME_MAP = new Map<String, String>{
		'Postpaid Simcard 48k' => '3-in-1 simkaart (48K)',
		'ESIM QR Voucher' => 'eSIM'
	};

	public void execute(SchedulableContext cs) {
		executeFuture();
	}

	/**
	 * @description Execute whole logic in future because of the callouts
	 */
	@future(callout=true)
	public static void executeFuture() {
		List<String> jsonProducts = retrieveProducts();
		List<OrderEntryWebJSON> products = new List<OrderEntryWebJSON>();
		for (String p : jsonProducts) {
			OrderEntryWebJSON product = (OrderEntryWebJSON) JSON.deserialize(p, OrderEntryWebJSON.class);
			products.add(product);
		}

		// merge products into the first
		Iterator<OrderEntryWebJSON> it = products.iterator();
		OrderEntryWebJSON all = it.next();
		while (it.hasNext()) {
			all.data.simOnlyBundles.addAll(it.next().data.simOnlyBundles);
		}

		mapProducts(all);
	}

	/**
	 * @description Get WebShop products from REST API
	 * @return       REST response
	 */
	public static List<String> retrieveProducts() {
		List<String> results = new List<String>();
		for (String body : getRequestBodies()) {
			HttpRequest request = new HttpRequest();
			request.setEndpoint('callout:WebShopApi/shop-product-discovery/v1/graphql');
			request.setMethod('POST');
			request.setHeader('Content-Type', 'application/graphql');
			request.setBody(body);
			Http http = new Http();
			HttpResponse response = http.send(request);
			results.add(response.getBody());
		}
		return results;
	}

	/**
	 * @description Get WebShop request body for products from static resource
	 * @return       JSON request
	 */
	private static List<String> getRequestBodies() {
		StaticResource resource1 = [SELECT Id, Body FROM StaticResource WHERE Name = 'OrderEntryWebGraphQLBusiness' LIMIT 1];
		StaticResource resource2 = [SELECT Id, Body FROM StaticResource WHERE Name = 'OrderEntryWebGraphQLConsumer' LIMIT 1];
		return new List<String>{ resource1.Body.toString(), resource2.Body.toString() };
	}

	/**
	 * @description Map WebShop products to Salesforce model
	 * @param  response WebShop products
	 */
	public static void mapProducts(OrderEntryWebJSON response) {
		mapProduct2(response);

		mapOEProduct(response);

		mapOEProductBundle(response);

		mapOEPromotion(response);

		mapOEValidityPeriod(response);
	}

	/**
	 * @description Map WebShop products to Product2, PriceBookEntry and cspmb__Price_Item__c
	 * @param  response WebShop products
	 */
	private static void mapProduct2(OrderEntryWebJSON response) {
		// get Mobile order type
		Id ordertypeId = null;
		List<OrderType__c> ots = [SELECT Id FROM OrderType__c WHERE Name = 'Mobile'];
		if (ots.size() != 0) {
			ordertypeId = ots[0].Id;
		}
		// get Red Consumer taxonomy Id
		Id taxonomyId = null;
		List<Taxonomy__c> ts = [SELECT Id FROM Taxonomy__c WHERE Name = 'Red Consumer'];
		if (ts.size() != 0) {
			taxonomyId = ts[0].Id;
		}

		Id standardPricebookId = [SELECT Id FROM Pricebook2 WHERE IsStandard = TRUE LIMIT 1].Id;

		// Product2
		List<Product2> products = new List<Product2>();
		List<PricebookEntry> entries = new List<PricebookEntry>();
		Set<String> simCards = new Set<String>();
		Set<String> productCodes = new Set<String>();
		for (OrderEntryWebJSON.Bundle bundle : response.data.simOnlyBundles) {
			if (productCodes.contains(bundle.sku)) {
				continue;
			}
			Product2 p = new Product2();
			p.Name = bundle.subscription.name + ' SIM Only';
			p.ProductCode = bundle.sku;
			p.Quantity_type__c = 'CTN';
			p.Product_Brand__c = 'Ziggo';
			p.OrderType__c = orderTypeId;
			p.Family = 'Voice';
			p.Product_Line__c = 'Consumer';
			p.Product_Group__c = 'Priceplan';
			p.Taxonomy__c = taxonomyId;
			p.IsActive = true;
			p.Marketing_Description__c = null; // TBD
			p.ExternalID__c = bundle.sku;
			products.add(p);
			PricebookEntry e = new PricebookEntry();
			e.IsActive = true;
			e.UnitPrice = 0;
			e.Pricebook2Id = standardPricebookId;
			e.Product2 = new Product2(ExternalID__c = bundle.sku);
			e.External_ID__c = bundle.sku;
			entries.add(e);
			productCodes.add(bundle.sku);
		}
		Database.upsert(products, Product2.ExternalID__c, true);
		Database.upsert(entries, PricebookEntry.External_ID__c, true);

		// cspmb__Price_Item__c
		List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>();
		simCards.clear();
		productCodes.clear();
		for (OrderEntryWebJSON.Bundle bundle : response.data.simOnlyBundles) {
			if (productCodes.contains(bundle.sku)) {
				continue;
			}
			cspmb__Price_Item__c pi = new cspmb__Price_Item__c();
			pi.Name = bundle.subscription.name + ' SIM Only';
			pi.cspmb__Price_Item_Code__c = bundle.sku;
			pi.cspmb__Effective_Start_Date__c = Date.today();
			pi.cspmb__Is_Active__c = true;
			pi.cspmb__Recurring_Charge__c = bundle.price.recurringCharge.normalPrice.excludingVAT;
			pi.cspmb__Recurring_Charge_External_ID__c = bundle.sku;
			pi.Recurring_Charge_Product__r = new Product2(ExternalID__c = bundle.sku);
			pi.cspmb__One_Off_Charge__c = bundle.price.oneTimeCharge.normalPrice.excludingVAT;
			pi.cspmb__One_Off_Charge_External_ID__c = bundle.sku;
			pi.One_Off_Charge_Product__r = new Product2(ExternalID__c = bundle.sku);
			pi.External_ID__c = bundle.sku;
			pi.cspmb__Type__c = 'Commercial Product';
			pi.cspmb__Role__c = 'Master';
			priceItems.add(pi);
			productCodes.add(bundle.sku);
		}
		Database.upsert(priceItems, cspmb__Price_Item__c.External_ID__c, true);
	}

	/**
	 * @description Map WebShop products to OE_Product__c and Order_Entry_Product_Detail__c
	 * @param  response WebShop products
	 */
	private static void mapOEProduct(OrderEntryWebJSON response) {
		// OE_Product__c
		List<OE_Product__c> oeProducts = new List<OE_Product__c>();
		Set<String> familyIds = new Set<String>();
		Set<String> sims = new Set<String>();
		for (OrderEntryWebJSON.Bundle bundle : response.data.simOnlyBundles) {
			if (familyIds.contains(bundle.subscription.family.id)) {
				// skip already mapped family
				continue;
			}
			OE_Product__c oe = new OE_Product__c();
			oe.Name = bundle.subscription.family.name;
			oe.Type__c = 'Mobile';
			oe.External_ID__c = bundle.subscription.family.id;
			oeProducts.add(oe);
			familyIds.add(bundle.subscription.family.id);
			// SIM
			for (OrderEntryWebJSON.SimCard sim : bundle.subscription.simCards) {
				if (sims.contains(sim.sku)) {
					// skip if already added Sim
					continue;
				}
				OE_Product__c oes = new OE_Product__c();
				// TBD check name in the map
				oes.Name = getSimName(sim.name);
				oes.Type__c = 'Mobile SIM Card';
				oes.Product_Code__c = sim.sku;
				oes.External_ID__c = sim.sku;
				oeProducts.add(oes);
				sims.add(sim.sku);
			}
		}
		Database.upsert(oeProducts, OE_Product__c.External_ID__c, true);

		// Order_Entry_Product_Detail__c
		List<Order_Entry_Product_Detail__c> productDetails = new List<Order_Entry_Product_Detail__c>();
		familyIds.clear();
		for (OrderEntryWebJSON.Bundle bundle : response.data.simOnlyBundles) {
			if (familyIds.contains(bundle.subscription.family.id)) {
				// Skip already mapped family
				continue;
			}
			Order_Entry_Product_Detail__c pd;
			pd = new Order_Entry_Product_Detail__c();
			pd.Product__r = new OE_Product__c(External_ID__c = bundle.subscription.family.id);
			pd.Name = 'Unlimited Internet';
			pd.Type__c = 'Bundle Detail';
			pd.Value__c = bundle.subscription.hasUnlimitedInternet ? 'Yes' : 'No';
			pd.External_ID__c = bundle.subscription.family.id + '-1';
			pd.Category__c = 'Mobile';
			productDetails.add(pd);
			if (bundle.subscription.internetGB != null) {
				pd = new Order_Entry_Product_Detail__c();
				pd.Product__r = new OE_Product__c(External_ID__c = bundle.subscription.family.id);
				pd.Name = 'Data Size (GB)';
				pd.Type__c = 'Bundle Detail';
				pd.Value__c = bundle.subscription.internetGB;
				pd.External_ID__c = bundle.subscription.family.id + '-2';
				pd.Category__c = 'Mobile';
				productDetails.add(pd);
			}
			pd = new Order_Entry_Product_Detail__c();
			pd.Product__r = new OE_Product__c(External_ID__c = bundle.subscription.family.id);
			pd.Name = 'Unlimited Voice';
			pd.Type__c = 'Bundle Detail';
			pd.Value__c = bundle.subscription.hasUnlimitedVoice ? 'Yes' : 'No';
			pd.External_ID__c = bundle.subscription.family.id + '-3';
			pd.Category__c = 'Mobile';
			productDetails.add(pd);
			if (bundle.subscription.voiceMinutes != null) {
				pd = new Order_Entry_Product_Detail__c();
				pd.Product__r = new OE_Product__c(External_ID__c = bundle.subscription.family.id);
				pd.Name = 'Voice Minutes';
				pd.Type__c = 'Bundle Detail';
				pd.Value__c = bundle.subscription.voiceMinutes;
				pd.External_ID__c = bundle.subscription.family.id + '-4';
				pd.Category__c = 'Mobile';
				productDetails.add(pd);
			}
			pd = new Order_Entry_Product_Detail__c();
			pd.Product__r = new OE_Product__c(External_ID__c = bundle.subscription.family.id);
			pd.Name = 'Unlimited SMS';
			pd.Type__c = 'Bundle Detail';
			pd.Value__c = bundle.subscription.hasUnlimitedSMS ? 'Yes' : 'No';
			pd.External_ID__c = bundle.subscription.family.id + '-5';
			pd.Category__c = 'Mobile';
			productDetails.add(pd);
			if (bundle.subscription.numberSMS != null) {
				pd = new Order_Entry_Product_Detail__c();
				pd.Product__r = new OE_Product__c(External_ID__c = bundle.subscription.family.id);
				pd.Name = 'Number SMS';
				pd.Type__c = 'Bundle Detail';
				pd.Value__c = bundle.subscription.numberSMS;
				pd.External_ID__c = bundle.subscription.family.id + '-6';
				pd.Category__c = 'Mobile';
				productDetails.add(pd);
			}
			familyIds.add(bundle.subscription.family.id);
		}
		Database.upsert(productDetails, Order_Entry_Product_Detail__c.External_ID__c, true);
	}

	/**
	 * @description Map WebShop products to OE_Product_Bundle__c
	 * @param  response WebShop products
	 */
	private static void mapOEProductBundle(OrderEntryWebJSON response) {
		// get oeMobile product
		Id oeMobileProductId = [SELECT Id FROM OE_Product__c WHERE Name = 'Mobile' LIMIT 1].Id;

		// OE_Product_Bundle__c
		List<OE_Product_Bundle__c> oeProductBundles = new List<OE_Product_Bundle__c>();
		Set<String> ids = new Set<String>();
		for (OrderEntryWebJSON.Bundle bundle : response.data.simOnlyBundles) {
			// SIM
			for (OrderEntryWebJSON.SimCard sim : bundle.subscription.simCards) {
				if (ids.contains(bundle.sku + '-' + sim.sku)) {
					continue;
				}
				OE_Product_Bundle__c pb = new OE_Product_Bundle__c();
				pb.Name = bundle.subscription.name + ' SIM Only';
				pb.Main_Product__c = oeMobileProductId;
				pb.Mobile_Product__r = new OE_Product__c(External_ID__c = bundle.subscription.family.id);
				pb.SIM_Card_Product__r = new OE_Product__c(External_ID__c = sim.sku);
				pb.Bundle_Product_Code__c = bundle.sku;
				pb.Type__c = 'Mobile';
				pb.Default__c = bundle.subscription.defaultSimCard.sku == sim.sku;
				pb.External_ID__c = bundle.sku + '-' + sim.sku;
				if (bundle.subscription.durationMonths != null) {
					pb.Contract_Term__c = String.valueOf(bundle.subscription.durationMonths / 12);
				}
				oeProductBundles.add(pb);
				ids.add(bundle.sku + '-' + sim.sku);
			}
		}
		Database.upsert(oeProductBundles, OE_Product_Bundle__c.External_ID__c, true);
	}

	/**
	 * @description Map WebShop products to OE_Discount__c, OE_Promotion__c and OE_Promotion_Discount
	 * @param  response WebShop products
	 */
	private static void mapOEPromotion(OrderEntryWebJSON response) {
		// OE_Discount__c
		List<OE_Discount__c> discounts = new List<OE_Discount__c>();
		// OE_Promotion__c
		//Set<String> promotionSet = new Set<String>();
		List<OE_Promotion__c> promotions = new List<OE_Promotion__c>();
		// OE_Promotion_Discount__c
		List<OE_Promotion_Discount__c> promotionDiscounts = new List<OE_Promotion_Discount__c>();
		// IDs
		Set<String> ids = new Set<String>();
		for (OrderEntryWebJSON.Bundle bundle : response.data.simOnlyBundles) {
			for (OrderEntryWebJSON.Promotion promotion : bundle.subscription.promotions) {
				if (ids.contains(bundle.sku + '-' + promotion.sku)) {
					continue;
				}
				OE_Discount__c d = new OE_Discount__c();
				d.Name = promotion.name;
				if (promotion.promotionType == 'NSGE') {
					d.Type__c = 'Discounted Amount';
					d.Value__c = promotion.price.recurringCharge.excludingVAT;
					d.One_Off_Value__c = null;
				}
				if (promotion.promotionType == 'REGULAR') {
					d.Type__c = 'Amount';
					d.Value__c = promotion.price.recurringCharge.excludingVAT;
					d.One_Off_Value__c = null;
				}
				if (promotion.promotionType == 'CONNECTION_COSTS_WAIVER') {
					d.Type__c = 'Percentage';
					d.Value__c = null;
					d.One_Off_Value__c = 100.0 - promotion.price.recurringCharge.excludingVAT;
				}
				d.Level__c = 'Bundle';
				d.Duration__c = bundle.subscription.durationMonths;
				d.Product__r = new OE_Product__c(External_ID__c = bundle.subscription.family.id);
				d.External_ID__c = bundle.sku + '-' + promotion.sku;
				discounts.add(d);

				OE_Promotion__c p = new OE_Promotion__c();
				p.Promotion_Name__c = promotion.name;
				p.Type__c = 'Automatic';
				if (promotion.promotionType == 'NSGE') {
					p.Customer_Type__c = 'Existing';
				} else {
					p.Customer_Type__c = 'New;Existing';
				}
				if (bundle.subscription.durationMonths != null) {
					p.Contract_Term__c = String.valueOf(bundle.subscription.durationMonths / 12);
				}
				p.Active__c = true;
				p.External_ID__c = bundle.sku + '-' + promotion.sku;
				promotions.add(p);

				OE_Promotion_Discount__c pd = new OE_Promotion_Discount__c();
				pd.Discount__r = new OE_Discount__c(External_ID__c = bundle.sku + '-' + promotion.sku);
				pd.Promotion__r = new OE_Promotion__c(External_ID__c = bundle.sku + '-' + promotion.sku);
				pd.External_ID__c = bundle.sku + '-' + promotion.sku;
				promotionDiscounts.add(pd);
				ids.add(bundle.sku + '-' + promotion.sku);
			}
		}

		Database.upsert(discounts, OE_Discount__c.External_ID__c, true);
		Database.upsert(promotions, OE_Promotion__c.External_ID__c, true);
		Database.upsert(promotionDiscounts, OE_Promotion_Discount__c.External_ID__c, true);
	}

	/**
	 * @description Map WebShop products to OE_Validity_Period__c
	 * @param  response WebShop products
	 */
	private static void mapOEValidityPeriod(OrderEntryWebJSON response) {
		// Retrieve all webshop validity periods
		List<OE_Validity_Period__c> currentValidities = [
			SELECT Id, External_ID__c
			FROM OE_Validity_Period__c
			WHERE Source__c = 'WebShop' AND To__c >= TODAY
		];
		Set<String> currentIds = new Set<String>();
		Set<String> newIds = new Set<String>();
		for (OE_Validity_Period__c vp : currentValidities) {
			currentIds.add(vp.External_ID__c);
		}

		// OE_Validity_Period__c
		List<OE_Validity_Period__c> validities = new List<OE_Validity_Period__c>();
		for (OrderEntryWebJSON.Bundle bundle : response.data.simOnlyBundles) {
			if (!currentIds.contains(bundle.subscription.family.id) && !newIds.contains(bundle.subscription.family.id)) {
				OE_Validity_Period__c v = new OE_Validity_Period__c();
				v.From__c = Date.today();
				v.To__c = Date.newInstance(2099, 12, 31);
				v.Order_Entry_Product__r = new OE_Product__c(External_ID__c = bundle.subscription.family.id);
				v.Source__c = 'WebShop';
				v.External_ID__c = bundle.subscription.family.id;
				validities.add(v);
			}
			newIds.add(bundle.subscription.family.id);
			for (OrderEntryWebJSON.Promotion promotion : bundle.subscription.promotions) {
				if (newIds.contains(bundle.sku + '-' + promotion.sku)) {
					// skip if already added
					continue;
				}
				if (!currentIds.contains(bundle.sku + '-' + promotion.sku)) {
					OE_Validity_Period__c vp = new OE_Validity_Period__c();
					vp.From__c = Date.today();
					vp.To__c = Date.newInstance(2099, 12, 31);
					vp.Order_Entry_Promotion__r = new OE_Promotion__c(External_ID__c = bundle.sku + '-' + promotion.sku);
					vp.Source__c = 'WebShop';
					vp.External_ID__c = bundle.sku + '-' + promotion.sku;
					validities.add(vp);
				}
				newIds.add(bundle.sku + '-' + promotion.sku);
			}
			// SIM
			for (OrderEntryWebJSON.SimCard sim : bundle.subscription.simCards) {
				if (newIds.contains(sim.sku)) {
					// skip if already added
					continue;
				}
				if (!currentIds.contains(sim.sku)) {
					OE_Validity_Period__c vps = new OE_Validity_Period__c();
					vps.From__c = Date.today();
					vps.To__c = Date.newInstance(2099, 12, 31);
					vps.Order_Entry_Product__r = new OE_Product__c(External_ID__c = sim.sku);
					vps.Source__c = 'WebShop';
					vps.External_ID__c = sim.sku;
					validities.add(vps);
				}
				newIds.add(sim.sku);
			}
		}
		Database.upsert(validities, OE_Validity_Period__c.External_ID__c, true);

		Set<String> oldIds = currentIds;
		oldIds.removeAll(newIds);
		List<OE_Validity_Period__c> oldValidities = [SELECT To__c, External_ID__c FROM OE_Validity_Period__c WHERE External_ID__c IN :oldIds];
		for (OE_Validity_Period__c v : oldValidities) {
			v.To__c = Date.today().addDays(-1);
		}
		update oldValidities;
	}

	/**
	 * Get sim card name from the map.
	 * @param name
	 */
	private static String getSimName(String name) {
		String result = SIM_NAME_MAP.get(name);
		if (result == null) {
			result = name;
		}
		return result;
	}
}
