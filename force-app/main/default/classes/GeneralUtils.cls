public without sharing class GeneralUtils {
	// Get a map of all recordTypes SObjectType->DeveloperName->recordTypeId
	public static Map<String, Map<String, Id>> recordTypeMap {
		get {
			if (recordTypeMap == null) {
				recordTypeMap = new Map<String, Map<String, Id>>();
				for (RecordType rt : [SELECT SObjectType, DeveloperName, Id FROM RecordType]) {
					Map<String, Id> tempMap = new Map<String, Id>();
					if (recordTypeMap.containsKey(rt.SObjectType)) {
						tempMap = recordTypeMap.get(rt.SObjectType);
					}
					tempMap.put(rt.DeveloperName, rt.Id);
					recordTypeMap.put(rt.SObjectType, tempMap);
				}
			}
			return recordTypeMap;
		}
		private set;
	}

	// Get a map of all orderType records used to reduce the number of queries in triggers than fire multiple times
	public static Map<Id, OrderType__c> orderTypeMap {
		get {
			if (orderTypeMap == null) {
				orderTypeMap = new Map<Id, OrderType__c>([SELECT Name, ExportSystem__c, Status__c FROM ordertype__c]);
			}
			return orderTypeMap;
		}
		private set;
	}

	// Get a map of all orderType records by Name used to reduce the number of queries in triggers than fire multiple times
	public static Map<String, Id> orderTypeMapByName {
		get {
			if (orderTypeMapByName == null) {
				orderTypeMapByName = new Map<String, Id>();
				for (OrderType__c ot : [SELECT Id, Name FROM ordertype__c]) {
					orderTypeMapByName.put(ot.Name, ot.Id);
				}
			}
			return orderTypeMapByName;
		}
		private set;
	}

	// Get a map of all vendors portfolio->vendor
	public static Map<String, String> portfolioVendorMap {
		get {
			if (portfolioVendorMap == null) {
				portfolioVendorMap = new Map<String, String>();
				for (PortfolioVendorMapping__c pvm : PortfolioVendorMapping__c.getall().values()) {
					portfolioVendorMap.put(pvm.Name, pvm.Vendor__c);
				}
			}
			return portfolioVendorMap;
		}
		private set;
	}

	// Get the Vodafone User record
	public static User vodafoneUser {
		get {
			if (vodafoneUser == null) {
				vodafoneUser = [SELECT Id FROM User WHERE Name = 'Vodafone' AND IsActive = TRUE];
			}
			return vodafoneUser;
		}
		private set;
	}

	// Get the Vodafone Account record
	public static Account vodafoneAccount {
		get {
			if (vodafoneAccount == null) {
				List<Account> vodafoneAccounts = [
					SELECT Id, BAN_Number__c, BOPCode__c, Unify_Ref_Id__c
					FROM Account
					WHERE BOPCode__c = 'TNF'
					LIMIT 1
				];
				if (!vodafoneAccounts.isEmpty()) {
					vodafoneAccount = vodafoneAccounts[0];
				}
			}
			// if still null (e.g. in test classes) return a dummy
			if (vodafoneAccount == null) {
				vodafoneAccount = new Account(BAN_Number__c = '318837537', BOPCode__c = 'TNF', Unify_Ref_Id__c = '1234');
			}
			return vodafoneAccount;
		}
		private set;
	}

	// Get the Partner Queue record
	public static Id partnerQueueId {
		get {
			if (partnerQueueId == null) {
				partnerQueueId = [
					SELECT Id, QueueId
					FROM QueueSobject
					WHERE SobjectType = 'Lead' AND Queue.DeveloperName = 'Partners_Automatic_Distribution'
				]
				.QueueId;
			}
			return partnerQueueId;
		}
		private set;
	}

	// Creates a set of parameters given an sobject. If the label is mentioned in the URL Get Parameter
	// and has a value, this label is translated to the ID and linked to the value
	public static Map<String, String> createLabelParameters(PageReference pReference, sObject oGeneric) {
		//store the html from the page reference
		String html = getPageReferenceContent(pReference);
		// Capture the accountId of the case
		String defAccountId = getPageReferenceParam(System.currentPageReference(), 'def_account_id');
		//Create the map that stores the label to label ID
		Map<String, String> mLabelToID = getLabelIdMap(html);
		// Create new parameter set
		Map<String, String> params = new Map<String, String>();
		//for each field in our object
		for (SObjectField field : oGeneric.getSObjectType().getDescribe().fields.getMap().values()) {
			//Get the field description object
			DescribeFieldResult f = field.getDescribe();
			String label = f.getLabel(); //Get label name
			String apiName = f.getName(); //Get API name
			//Get the parameter from GET
			Object value = getPageReferenceParam(ApexPages.currentPage(), apiName);
			if (value != null && mLabelToID.containsKey(label)) {
				// Format the given value as required
				String valueAsString = getValueAsString(f, value);
				//If it is a lookup field, we expect to add the ID as well
				if (!f.getReferenceTo().isEmpty()) {
					//We assume that the variable will contain ID_ for a lookup parameter
					String strIDLookup = ApexPages.currentPage().getParameters().get('ID_' + apiName).trim();
					//If not null, add the parameter (use _lkid to reference the ID field)
					if (strIDLookup != null) {
						params.put(mLabelToID.get(label) + '_lkid', strIDLookup);
					}
				}
				//Set the ID to the value
				params.put(mLabelToID.get(label), valueAsString);
			}
		}
		// ugly hack to include the account id
		if (ApexPages.currentPage().getParameters().containsKey('accid')) {
			params.put('accid', getPageReferenceParam(ApexPages.currentPage(), 'accid'));
		}
		if (ApexPages.currentPage().getParameters().containsKey('retURL')) {
			params.put('retURL', getPageReferenceParam(ApexPages.currentPage(), 'retURL'));
		}
		//Check if we also have to set the Record Type
		String strRecordType = getPageReferenceParam(ApexPages.currentPage(), 'RecordType');
		try {
			Id recTypeId = getRecordTypeIdByName(oGeneric.getSObjectType().getDescribe().getName(), strRecordType.escapeHtml4());
			params.put('RecordType', recTypeId);
		} catch (Exception ex) {
		}
		//Populate the accountId if its already present
		if (defAccountId != null) {
			params.put('def_account_id', defAccountId);
		}
		//Return the set of label ID's to values
		return params;
	}

	private static String getPageReferenceContent(PageReference ref) {
		if (Test.isRunningTest()) {
			return '<label for="CF00NK0000000Yt1b"><span class="requiredMark">*</span>Name</label>';
		} else {
			return ref.getContent().toString();
		}
	}

	private static Map<String, String> getLabelIdMap(String html) {
		Map<String, String> mLabelToID = new Map<String, String>();
		//Create the regular expression to fetch all labels
		Matcher m = Pattern.compile('<label for="(.*?)">(<span class="requiredMark">\\*</span>)?(.*?)</label>').matcher(html);
		//While there are labels
		while (m.find()) {
			//Label is stored in the 4th column
			String label = m.group(3);
			//Id is stored in the second column
			String id = m.group(1);
			//Add it to the map (split on _, on occasion we have a _top somewhere)
			mLabelToID.put(label, id.split('_')[0]);
		}
		return mLabelToId;
	}

	private static String getPageReferenceParam(PageReference ref, String param) {
		String paramValue = ref.getParameters().get(param);
		if (paramValue != null) {
			paramValue = paramValue.escapeHtml4();
		}
		return paramValue;
	}

	private static String getValueAsString(DescribeFieldResult fieldDescribe, Object fieldValue) {
		if (fieldDescribe.getType() == DisplayType.Date) {
			return ((Date) fieldValue).format();
		} else if (fieldDescribe.getType() == DisplayType.Datetime) {
			return ((Datetime) fieldValue).format();
		} else if (fieldDescribe.getType() == DisplayType.Boolean) {
			return ((Boolean) fieldValue) ? '1' : '0';
		} else {
			return String.valueOf(fieldValue);
		}
	}

	public static Organization currentOrganization {
		get {
			if (currentOrganization == null) {
				currentOrganization = [SELECT Id, IsSandbox FROM Organization LIMIT 1];
			}
			return currentOrganization;
		}
		private set;
	}

	public static Boolean isProduction() {
		return !currentOrganization.IsSandbox;
	}

	public static User currentUser {
		get {
			if (currentUser == null) {
				currentUser = [
					SELECT Id, Username, Profile.Name, Name, Email, FirstName, LastName, IsActive, UserType, AccountId, SalesChannel__c, Ziggo__c
					FROM User
					WHERE Id = :UserInfo.getUserId()
				];
			}
			return currentUser;
		}
		private set;
	}

	public static Boolean currentUserIsPartnerUser() {
		return isPartnerUser(currentUser);
	}

	public static Boolean isPartnerUser(User u) {
		return u.UserType == 'PowerPartner';
	}

	public static Boolean isPortalUser(User u) {
		return u.UserType == 'CspLitePortal';
	}

	@TestVisible
	public static Map<Id, User> userMap {
		get {
			if (userMap == null) {
				userMap = new Map<Id, User>(
					[
						SELECT
							Id,
							IsActive,
							Ziggo__c,
							Profile.Name,
							UserType,
							AccountId,
							Account.Dealer_Code__c,
							UserRole.Name,
							ManagerId,
							Manager.ManagerId,
							Non_Personal_User__c
						FROM User
					]
				);
			}
			return userMap;
		}
		private set;
	}

	public static Map<Id, String> profileIdToProfileName {
		get {
			if (profileIdToProfileName == null) {
				profileIdToProfileName = new Map<Id, String>();
				for (Profile p : [SELECT Id, Name FROM Profile]) {
					profileIdToProfileName.put(p.Id, p.Name);
				}
			}
			return profileIdToProfileName;
		}
		set;
	}

	public static Map<Id, String> roleIdToRoleName {
		get {
			if (roleIdToRoleName == null) {
				roleIdToRoleName = new Map<Id, String>();
				for (UserRole ur : [SELECT Id, Name FROM UserRole]) {
					roleIdToRoleName.put(ur.Id, ur.Name);
				}
			}
			return roleIdToRoleName;
		}
		set;
	}

	public static Map<String, Id> roleNameToRoleId {
		get {
			if (roleNameToRoleId == null) {
				roleNameToRoleId = new Map<String, Id>();
				for (UserRole ur : [SELECT Id, Name FROM UserRole]) {
					roleNameToRoleId.put(ur.Name, ur.Id);
				}
			}
			return roleNameToRoleId;
		}
		set;
	}

	public static Map<Id, Set<Id>> roleIdToSubRoleIds {
		get {
			if (roleIdToSubRoleIds == null) {
				roleIdToSubRoleIds = new Map<Id, Set<Id>>();
				for (UserRole userRole : [SELECT Id, ParentRoleId, Name FROM UserRole WHERE ParentRoleId != NULL]) {
					if (!roleIdToSubRoleIds.containsKey(userRole.ParentRoleId)) {
						roleIdToSubRoleIds.put(userRole.ParentRoleId, new Set<Id>());
					}
					roleIdToSubRoleIds.get(userRole.ParentRoleId).add(userRole.Id);
				}
			}
			return roleIdToSubRoleIds;
		}
		set;
	}

	public static Set<Id> getAllSubRoleIds(Set<Id> roleIds) {
		Set<Id> currentRoleIds = new Set<Id>();
		for (Id roleId : roleIds) {
			if (roleIdToSubRoleIds.containsKey(roleId)) {
				currentRoleIds.addAll(roleIdToSubRoleIds.get(roleId));
			}
		}
		if (!currentRoleIds.isEmpty()) {
			currentRoleIds.addAll(getAllSubRoleIds(currentRoleIds));
		}
		return currentRoleIds;
	}

	public static Set<String> getAllSubRoleNames(Set<Id> roleIds) {
		Set<String> currentRoleNames = new Set<String>();
		Set<Id> currentRoleIds = getAllSubRoleIds(roleIds);

		for (Id roleId : currentRoleIds) {
			currentRoleNames.add(RoleIdToRoleName.get(roleId));
		}

		return currentRoleNames;
	}

	public static Set<Id> getAllSalesManagerRoleIds() {
		Set<Id> smRoleIds = new Set<Id>();

		List<UserRole> userRoles = [
			SELECT Id
			FROM UserRole
			WHERE Name != 'Cloud Productivity Sales Manager' AND Name LIKE '%Sales Manager%' AND (NOT Name LIKE '%Solution%')
		];

		for (UserRole roleRecord : userRoles) {
			smRoleIds.add(roleRecord.Id);
		}

		return smRoleIds;
	}

	public static Set<Id> getAllCommercialDirectorRoleIds() {
		Set<Id> directorRoleIds = new Set<Id>();

		Set<String> commercialDirectorRoles = new Set<String>{
			'Director B2B',
			'Director Business Partner Sales',
			'Director Corporate Sales',
			'Director Small Business Sales',
			'Director Solution Sales',
			'Director VGE'
		};

		for (UserRole roleRecord : [SELECT Id FROM UserRole WHERE Name IN :commercialDirectorRoles]) {
			directorRoleIds.add(roleRecord.Id);
		}

		return directorRoleIds;
	}

	public static Map<String, String> roleToSegment {
		get {
			if (roleToSegment == null) {
				roleToSegment = new Map<String, String>();
				for (Role_Segment_Mapping__c rsm : [SELECT Id, Name, Segment__c FROM Role_Segment_Mapping__c]) {
					roleToSegment.put(rsm.Name, rsm.Segment__c);
				}
			}
			return roleToSegment;
		}
		set;
	}

	public static Map<String, Set<String>> segmentToRoles {
		get {
			if (segmentToRoles == null) {
				segmentToRoles = new Map<String, Set<String>>();
				for (String role : RoleToSegment.keySet()) {
					if (!segmentToRoles.containsKey(RoleToSegment.get(role))) {
						segmentToRoles.put(RoleToSegment.get(role), new Set<String>());
					}
					segmentToRoles.get(RoleToSegment.get(role)).add(role);
				}
			}
			return segmentToRoles;
		}
		set;
	}

	public static Map<String, String> roleToMainSegment {
		get {
			if (roleToMainSegment == null) {
				roleToMainSegment = new Map<String, String>();
				for (Role_Segment_Mapping__c rsm : [SELECT Id, Name, Main_Segment__c FROM Role_Segment_Mapping__c]) {
					roleToMainSegment.put(rsm.Name, rsm.Main_Segment__c);
				}
			}
			return roleToMainSegment;
		}
		set;
	}

	public static Map<String, Set<String>> mainSegmentToRoles {
		get {
			if (mainSegmentToRoles == null) {
				mainSegmentToRoles = new Map<String, Set<String>>();

				for (String role : RoleToMainSegment.keySet()) {
					if (!mainSegmentToRoles.containsKey(RoleToMainSegment.get(role))) {
						mainSegmentToRoles.put(RoleToMainSegment.get(role), new Set<String>());
					}
					mainSegmentToRoles.get(RoleToMainSegment.get(role)).add(role);
				}
			}
			return mainSegmentToRoles;
		}
		set;
	}

	public static Boolean compareDealerCodes(String dealerCode1, String dealerCode2) {
		return stripDealercode(dealerCode1) == stripDealercode(dealerCode2);
	}

	public static String stripDealercode(String dealerCode) {
		if (dealerCode == null) {
			return null;
		}
		return dealerCode.startsWith('00') ? dealerCode.subString(2, dealerCode.length()) : dealerCode;
	}

	public static Id getPartnerUserRoleGroupIdFromUserId(Id userId) {
		return getPartnerUserRoleGroupIdFromUserId(new Set<Id>{ userId }).get(userId);
	}

	public static Map<Id, Id> getPartnerUserRoleGroupIdFromUserId(Set<Id> userIds) {
		List<User> partnerUsers = filterPartnerUsers([SELECT Id, UserRoleId, UserRole.Name, UserType FROM User WHERE Id IN :userIds]);

		Map<Id, Id> userIdToUserGroupId = new Map<Id, Id>();
		Map<String, Id> userRoleNameToId = new Map<String, Id>();
		Map<String, Id> userRoleIdToGroupId = new Map<String, Id>();
		Map<Id, String> originalUserRoleIdToUserRoleName = new Map<Id, String>();

		// derive user-level partner role names
		for (User u : partnerUsers) {
			// GC changing this as partner partnerUsers can't see the rolename field and all partner roles are 'User' level.
			userRoleIdToGroupId.put(u.UserRoleId, null);
		}
		if (userRoleIdToGroupId.isEmpty()) {
			return userIdToUserGroupId;
		}

		// fetch user-level role Ids for non-User-level roles (by name)
		for (UserRole ur : [SELECT Id, Name FROM UserRole WHERE Name IN :userRoleIdToGroupId.keySet()]) {
			userRoleIdToGroupId.put(ur.Id, null);
			if (userRoleIdToGroupId.containsKey(ur.Name)) {
				userRoleIdToGroupId.remove(ur.Name);
			}
			userRoleNameToId.put(ur.Name, ur.Id);
		}

		// get group Ids from user-level role Ids
		for (Group g : [SELECT Id, RelatedId FROM Group WHERE Type = 'Role' AND RelatedId IN :userRoleIdToGroupId.keySet()]) {
			userRoleIdToGroupId.put(g.RelatedId, g.Id);
		}

		if (!userRoleIdToGroupId.isEmpty()) {
			for (User u : partnerUsers) {
				Id tempId = originalUserRoleIdToUserRoleName.containsKey(u.UserRoleId)
					? userRoleNameToId.get(originalUserRoleIdToUserRoleName.get(u.UserRoleId))
					: u.UserRoleId;
				userIdToUserGroupId.put(u.id, userRoleIdToGroupId.get(tempId));
			}
		}
		return userIdToUserGroupId;
	}

	private static List<User> filterPartnerUsers(List<User> users) {
		List<User> result = new List<User>();
		for (User u : users) {
			if (GeneralUtils.isPartnerUser(u)) {
				result.add(u);
			}
		}
		return result;
	}

	// Returns the parent partner portal user id by id
	public static Map<Id, Id> getParentPartnerUserIdsFromUserIds(Set<Id> userIds) {
		// get user accountids (we can't be 100% sure that the source user is the main contact for the partner account)
		Map<Id, Id> userIdToAccountId = new Map<Id, Id>();
		List<User> users = [SELECT Id, AccountId, UserRoleId, UserRole.Name, UserType FROM User WHERE Id IN :userIds AND AccountId != NULL];

		for (User u : users) {
			userIdToAccountId.put(u.Id, u.AccountId);
		}
		// fetch all parent dealercodes of dealercodes referring that account's contacts
		Map<String, Id> parentDealerCodeToAccountId = new Map<String, Id>();
		List<Dealer_Information__c> dealerInfos = [
			SELECT Id, Dealer_Code__c, Parent_Dealer_Code__c, Contact__c, Contact__r.AccountId
			FROM Dealer_Information__c
			WHERE Contact__r.AccountId IN :userIdToAccountId.values() AND HasParent__c = TRUE AND Status__c = 'Active'
		];
		for (Dealer_Information__c dealerInfo : dealerInfos) {
			parentDealerCodeToAccountId.put(dealerInfo.Parent_Dealer_Code__c, dealerInfo.Contact__r.AccountId);
		}
		// fetch any parent dealercodes and add those contacts' users to the return set
		Map<Id, Id> parentDealerContactToAccountId = new Map<Id, Id>();
		List<Dealer_Information__c> parentDealerInfos = [
			SELECT Id, Dealer_Code__c, Contact__c
			FROM Dealer_Information__c
			WHERE Dealer_Code__c IN :parentDealerCodeToAccountId.keySet() AND Status__c = 'Active' AND Contact__c != NULL
		];
		for (Dealer_Information__c parentDealerInfo : parentDealerInfos) {
			parentDealerContactToAccountId.put(parentDealerInfo.Contact__c, parentDealerCodeToAccountId.get(parentDealerInfo.Dealer_Code__c));
		}
		Map<Id, Id> accountIdToParentDealerUser = new Map<Id, Id>();
		for (User u : [SELECT Id, ContactId FROM User WHERE ContactId IN :parentDealerContactToAccountId.keySet()]) {
			accountIdToParentDealerUser.put(parentDealerContactToAccountId.get(u.ContactId), u.Id);
		}
		Map<Id, Id> userIdToParentPartnerUserId = new Map<Id, Id>();
		for (Id userId : userIds) {
			if (userIdToAccountId.containsKey(userId)) {
				if (accountIdToParentDealerUser.containsKey(userIdToAccountId.get(userId))) {
					userIdToParentPartnerUserId.put(userId, accountIdToParentDealerUser.get(userIdToAccountId.get(userId)));
				}
			}
		}
		return userIdToParentPartnerUserId;
	}

	// Returns the 'standard' pbxtype id for ISDN2
	public static Id defaultISDN2PBXType {
		get {
			if (defaultISDN2PBXType == null) {
				defaultISDN2PBXType = [SELECT Id FROM PBX_Type__c WHERE Vendor__c = 'ISDN PBX' AND Product_Name__c = 'Standard ISDN2' LIMIT 1].Id;
			}
			return defaultISDN2PBXType;
		}
		private set;
	}

	// Returns the 'standard' pbxtype id for ISDN30
	public static Id defaultISDN30PBXType {
		get {
			if (defaultISDN30PBXType == null) {
				defaultISDN30PBXType = [SELECT Id FROM PBX_Type__c WHERE Vendor__c = 'ISDN PBX' AND Product_Name__c = 'Standard ISDN30' LIMIT 1].Id;
			}
			return defaultISDN30PBXType;
		}
		private set;
	}

	// Returns the 'standard' pbxtype id for the virtual Onenet PBX
	public static Id defaultOnenetPBXType {
		get {
			if (defaultOnenetPBXType == null) {
				defaultOnenetPBXType = [SELECT Id FROM PBX_Type__c WHERE Name = 'Onenet Virtual PBX' LIMIT 1].Id;
			}
			return defaultOnenetPBXType;
		}
		private set;
	}

	public static Map<String, String> postalCodeToZSPMap {
		get {
			if (postalCodeToZSPMap == null) {
				postalCodeToZSPMap = new Map<String, String>();

				for (Postal_Code_Assignment__c pca : [SELECT Id, Name, UserId__c FROM Postal_Code_Assignment__c WHERE UserId__c != NULL]) {
					postalCodeToZSPMap.put(pca.Name, pca.UserId__c);
				}
			}
			return postalCodeToZSPMap;
		}
		private set;
	}

	// Validates if string is in Salesforce ID format
	public static Boolean isValidId(String idToCheck) {
		String id = String.escapeSingleQuotes(idToCheck);
		return (id.length() == 15 || id.length() == 18) && Pattern.matches('^[a-zA-Z0-9]*$', id);
	}

	// Checks if the account in question is "Orphans from Unify" account.
	// Any data received from Unify that cannot be tied to an existing account will be attached to this.
	public static Account unifyOrphansAccount {
		get {
			if (unifyOrphansAccount == null) {
				List<Account> accs = [
					SELECT Id, Name, KVK_Number__c, DUNS_Number__c, Unify_Ref_Id__c
					FROM Account
					WHERE Name = 'Orphans from Unify'
					LIMIT 1
				];
				if (!accs.isEmpty()) {
					unifyOrphansAccount = accs[0];
				}
			}
			// if still null (e.g. in test classes) return a dummy
			if (unifyOrphansAccount == null) {
				unifyOrphansAccount = new Account(
					Name = 'Orphans from Unify',
					BAN_Number__c = '13371337',
					DUNS_Number__c = '13371337',
					KVK_Number__c = '13371337'
				);
			}
			return unifyOrphansAccount;
		}
		private set;
	}

	// Checks if 2 sets have any intersection (overlapping values) without modifying the sets
	public static Boolean setsIntersect(Set<String> set1, Set<String> set2) {
		Set<String> tempSet1 = convertSetToLowerCase(set1).clone();
		tempSet1.retainAll(convertSetToLowerCase(set2));
		return tempSet1.isEmpty() ? false : true;
	}

	public static Set<String> convertSetToLowerCase(Set<String> theSet) {
		Set<String> returnSet = new Set<String>();
		for (String s : theSet) {
			returnSet.add(s.toLowerCase());
		}
		return returnSet;
	}

	public static Boolean checkVisualforcePageAccess(String pageName, String nameSpace) {
		SetupEntityAccess[] accessSettings = [
			SELECT Id
			FROM SetupEntityAccess
			WHERE
				SetupEntityId IN (SELECT Id FROM ApexPage WHERE NamespacePrefix = :nameSpace AND Name = :pageName)
				AND ParentId IN (SELECT PermissionSetId FROM PermissionSetAssignment WHERE AssigneeId = :UserInfo.getUserId())
			LIMIT 1
		];

		return accessSettings.isEmpty() ? false : true;
	}

	//Marcel Vreuls: Method to support not to send email on sandboxes as it is disabled on sandboxes. Tests will fail
	public static Boolean runningInASandbox() {
		return [SELECT Id, IsSandbox FROM Organization LIMIT 1].IsSandbox;
	}

	// Creates Set from list of SObjects based on a provided field.
	public static Set<Object> getSetFromList(List<SObject> objectList, String fieldName) {
		Set<Object> result = new Set<Object>();
		List<String> fields = fieldName.split('\\.');
		List<String> parentFields = fields.clone();
		parentFields.remove(fields.size() - 1);
		String field = fields[fields.size() - 1];
		for (SObject so : objectList) {
			SObject temp = so;
			for (String parentField : parentFields) {
				temp = temp.getSObject(parentField);
				if (temp == null) {
					break;
				}
			}
			result.add(temp == null ? null : temp.get(field));
		}
		return result;
	}

	//	Creates Set of IDs from list of SObjects based on a provided field.
	public static Set<Id> getIDSetFromList(List<SObject> objectList, String fieldName) {
		Set<Object> objectSet = getSetFromList(objectList, fieldName);
		Set<Id> result = new Set<Id>();

		for (Object o : objectSet) {
			result.add((Id) o);
		}
		return result;
	}

	public static Set<Id> getIDSetFromListIgnoreNull(List<SObject> objectList, String fieldName) {
		Set<Object> objectSet = getSetFromList(objectList, fieldName);
		Set<Id> result = new Set<Id>();

		for (Object o : objectSet) {
			if (o != null) {
				result.add((Id) o);
			}
		}
		return result;
	}

	// 	Creates Set of Strings from list of SObjects based on a provided field.
	public static Set<String> getStringSetFromList(List<SObject> objectList, String fieldName) {
		Set<Object> objectSet = getSetFromList(objectList, fieldName);
		Set<String> result = new Set<String>();

		for (Object o : objectSet) {
			result.add((String) o);
		}
		return result;
	}

	// Checks if record fields have been changed.
	// Used in triggers to verify if field is changed as a part of DML.
	public static Boolean isRecordFieldChanged(SObject record, Map<Id, SObject> oldRecords, List<String> fields, Boolean allOrOne) {
		if (oldRecords == null) {
			return true;
		}

		Boolean isChanged = allOrOne;
		sObject oldRecord = oldRecords.get(record.Id);

		for (String field : fields) {
			Boolean isFieldChanged = record.get(field) != oldRecord.get(field);
			isChanged = allOrOne ? isChanged && isFieldChanged : isChanged || isFieldChanged;
		}
		return isChanged;
	}

	//	Checks if record field has been changed.
	//	Used in triggers to verify if field is changed as a part of DML.
	public static Boolean isRecordFieldChanged(SObject record, Map<Id, SObject> oldRecords, String field) {
		return isRecordFieldChanged(record, oldRecords, new List<String>{ field }, true);
	}

	// Filters records that have been changed as a part of the DML.
	public static List<SObject> filterChangedRecords(List<SObject> records, Map<Id, SObject> oldRecords, List<String> fields, Boolean allOrOne) {
		List<SObject> result = new List<SObject>();
		for (SObject record : records) {
			if (isRecordFieldChanged(record, oldRecords, fields, allOrOne)) {
				result.add(record);
			}
		}
		return result;
	}

	// Filters records that have been changed as a part of the DML.
	public static List<SObject> filterChangedRecords(List<SObject> records, Map<Id, SObject> oldRecords, String field) {
		return filterChangedRecords(records, oldRecords, new List<String>{ field }, true);
	}

	// Groups records by ID field.
	public static Map<Id, List<SObject>> groupByIDField(List<SObject> records, String field) {
		Map<Id, List<SObject>> result = new Map<Id, List<SObject>>();
		for (SObject record : records) {
			Id fieldValue = (Id) record.get(field);
			if (!result.containsKey(fieldValue)) {
				result.put(fieldValue, new List<SObject>());
			}
			result.get(fieldValue).add(record);
		}
		return result;
	}

	public static Map<String, List<SObject>> groupByStringField(List<SObject> records, String field) {
		Map<String, List<SObject>> result = new Map<String, List<SObject>>();
		for (SObject record : records) {
			String fieldValue = (String) record.get(field);
			if (!result.containsKey(fieldValue)) {
				result.put(fieldValue, new List<SObject>());
			}
			result.get(fieldValue).add(record);
		}
		return result;
	}

	// Gets record type ID for given object and record type developer name.
	public static Id getRecordTypeIdByDeveloperName(String objectName, String recordTypeDeveloperName) {
		return ((SObject) Type.forName(objectName).newInstance())
			.getSObjectType()
			.getDescribe()
			.getRecordTypeInfosByDeveloperName()
			.get(recordTypeDeveloperName)
			.getRecordTypeId();
	}

	// Gets record type ID for given object and record type name.
	public static Id getRecordTypeIdByName(String objectName, String recordTypeName) {
		return ((SObject) Type.forName(objectName).newInstance())
			.getSObjectType()
			.getDescribe()
			.getRecordTypeInfosByName()
			.get(recordTypeName)
			.getRecordTypeId();
	}

	// Dragan: used in Big Machines
	public static final Set<String> SAGROLENAMES = new Set<String>{
		'Sales Assistance Group',
		'Sales Assistance Group Direct',
		'Sales Assistance Group Indirect'
	};

	public static Map<String, sObject> getMapByUniqueKey(String key, List<sObject> recordList) {
		Map<String, sObject> recordMap = new Map<String, sObject>();
		for (sObject record : recordList) {
			recordMap.put((String) record.get(key), record);
		}
		return recordMap;
	}

	public static String getEscapedSOQLList(List<String> stringList) {
		List<String> escapedList = new List<String>();
		for (String item : stringList) {
			escapedList.add('\'' + item + '\'');
		}
		return '(' + String.join(escapedList, ',') + ')';
	}

	public static String getEscapedSOQLList(Set<String> stringSet) {
		return getEscapedSOQLList(new List<String>(stringSet));
	}

	public static String getEscapedSOQLList(Set<Id> idSet) {
		List<String> stringList = new List<String>();
		for (Id i : idSet) {
			stringList.add(i);
		}
		return getEscapedSOQLList(stringList);
	}
}
