public without sharing class COM_OrderGenerationObserverHandler {
	public List<COM_Delivery_Order__c> createParentDeliveryOrder(List<csord__Service__c> servicesList) {
		List<csord__Order__c> orderList = COM_Helper.getOrders(servicesList);
		List<COM_Delivery_Order__c> deliveryOrderList = new List<COM_Delivery_Order__c>();

		for (csord__Order__c order : orderList) {
			deliveryOrderList.add(new COM_Delivery_Order__c(Name = order.Name, Order__c = order.Id, Account__c = order.csord__Account__c));
		}
		insert deliveryOrderList;

		return deliveryOrderList;
	}

	private String createDeliveryOrderName(csord__Service__c service) {
		String siteAppendix = '';
		if (service.Site__c != null) {
			siteAppendix += ' - ' + service.Site__r.Name;
		}

		return COM_Constants.DELIVERY_ORDER_NAME_PREFIX + COM_Helper.getAccountName(service) + siteAppendix;
	}

	public List<csord__Service__c> linkServiceToDeliveryOrder(List<csord__Service__c> serviceList, List<COM_Delivery_Order__c> deliveryOrderList) {
		List<csord__Service__c> servicesToUpdate = new List<csord__Service__c>();

		for (csord__Service__c serviceIterate : serviceList) {
			for (COM_Delivery_Order__c deliveryOrderIterate : deliveryOrderList) {
				if (
					(serviceIterate.csord__Service__c == null && deliveryOrderIterate.Site__c == serviceIterate.Site__c) ||
					(serviceIterate.csord__Service__c != null &&
					deliveryOrderIterate.Site__c == serviceIterate.csord__Service__r.Site__c)
				) {
					servicesToUpdate.add(new csord__Service__c(Id = serviceIterate.Id, COM_Delivery_Order__c = deliveryOrderIterate.Id));
				}
			}
		}

		COM_Helper.updateList(servicesToUpdate);

		return servicesToUpdate;
	}

	private Id getParentDeliveryOrder(csord__Service__c service, List<COM_Delivery_Order__c> parentDeliveryOrders) {
		Id orderId = COM_Helper.getOrderId(service);

		for (COM_Delivery_Order__c deliveryOrderIterate : parentDeliveryOrders) {
			if (deliveryOrderIterate.Order__c == orderId) {
				return deliveryOrderIterate.Id;
			}
		}

		return null;
	}

	public List<COM_Delivery_Order__c> createDeliveryOrders(
		List<csord__Service__c> serviceList,
		List<COM_Delivery_Order__c> parentDeliveryOrders,
		String deliveryComponentMACDType
	) {
		List<COM_Delivery_Order__c> deliveryOrdersToCreate = new List<COM_Delivery_Order__c>();
		Map<String, Set<String>> siteIdProductsMap = new Map<String, Set<String>>();
		Id deliveryCoordinatorQueueId = fetchDeliveryCoordinatorQueueId();

		for (csord__Service__c serviceIterate : serviceList) {
			String siteId = COM_Helper.getSiteId(serviceIterate);
			if (serviceIterate.csord__Service__c == null) {
				if (!siteIdProductsMap.containsKey(siteId)) {
					COM_Delivery_Order__c deliveryOrderToCreate = new COM_Delivery_Order__c(
						Name = createDeliveryOrderName(serviceIterate),
						Order__c = COM_Helper.getOrderId(serviceIterate),
						Parent_Delivery_Order__c = getParentDeliveryOrder(serviceIterate, parentDeliveryOrders),
						Status__c = COM_Constants.DELIVERY_ORDER_CREATED_MESSAGE,
						Account__c = serviceIterate.csord__Order__r.csord__Account__c,
						Site__c = siteId,
						Technical_Contact__c = COM_Helper.getTechnicalContactId(serviceIterate),
						Wish_Date__c = serviceIterate.Installation_Wishdate__c,
						MACD_Type__c = COM_Constants.DEFAULT_MACD_TYPE,
						OwnerId = deliveryCoordinatorQueueId
					);

					if (serviceIterate.csordtelcoa__Cancelled_By_Change_Process__c) {
						deliveryOrderToCreate.MACD_Type__c = 'Termination';
					}

					if (deliveryComponentMACDType != null) {
						deliveryOrderToCreate.MACD_Type__c = deliveryComponentMACDType;
					}

					if (deliveryOrderToCreate.Site__c != null) {
						deliveryOrdersToCreate.add(deliveryOrderToCreate);
					}
					siteIdProductsMap.put(siteId, new Set<String>());
				}

				siteIdProductsMap.get(siteId).add(serviceIterate.Name);
			}
		}

		if (deliveryOrdersToCreate.size() > 0) {
			for (COM_Delivery_Order__c deliveryOrder : deliveryOrdersToCreate) {
				deliveryOrder.Products__c = String.join(new List<String>(siteIdProductsMap.get(deliveryOrder.Site__c)), ',');
			}

			assignTeamToDeliveryOrders(deliveryOrdersToCreate, siteIdProductsMap.keySet());
			insert deliveryOrdersToCreate;
		}

		return deliveryOrdersToCreate;
	}

	@TestVisible
	private Id fetchDeliveryCoordinatorQueueId() {
		Group deliveryCoordinatorQueue = [
			SELECT Id
			FROM Group
			WHERE Type = 'Queue' AND DeveloperName = :COM_Constants.DELIVERY_COODINATOR_QUEUE_DEV_NAME
		];

		return deliveryCoordinatorQueue.Id;
	}

	@TestVisible
	private void assignTeamToDeliveryOrders(List<COM_Delivery_Order__c> deliveryOrders, Set<String> setSiteIds) {
		Map<Id, String> mapTeamsIdBySiteId = this.getTeamsBySiteIds(setSiteIds);
		for (COM_Delivery_Order__c deliveryOrder : deliveryOrders) {
			deliveryOrder.Team__c = mapTeamsIdBySiteId.get(deliveryOrder.Site__c);
		}
	}

	private Map<Id, String> getTeamsBySiteIds(Set<String> setSiteIds) {
		Map<String, Id> mapSiteIdsPostalCodeId = new Map<String, Id>();
		for (Site__c sSite : [SELECT Id, Site_Postal_Code__c FROM Site__c WHERE Id IN :setSiteIds]) {
			mapSiteIdsPostalCodeId.put(sSite.Site_Postal_Code__c.left(4), sSite.Id);
		}

		Map<Id, String> mapPostalCodeRegionCode = new Map<Id, String>();
		for (Postal_Code__c pc : [SELECT Name, Region_Code__c FROM Postal_Code__c WHERE Name IN :mapSiteIdsPostalCodeId.keySet()]) {
			mapPostalCodeRegionCode.put(mapSiteIdsPostalCodeId.get(pc.Name), pc.Region_code__c.trim());
		}

		return mapPostalCodeRegionCode;
	}

	public void linkVFOrderToDeliveryOrder(List<csord__Service__c> serviceList, List<COM_Delivery_Order__c> deliveryOrderList) {
		Map<Id, Id> opportunityIdByOrderId = new Map<Id, Id>();
		for (csord__Service__c serv : serviceList) {
			opportunityIdByOrderId.put(serv.csord__Order__c, serv.csord__Order__r.csordtelcoa__Opportunity__c);
		}

		Map<Id, Id> deliveryOrderIdByOpportunityId = new Map<Id, Id>();
		for (COM_Delivery_Order__c delOrd : deliveryOrderList) {
			deliveryOrderIdByOpportunityId.put(opportunityIdByOrderId.get(delOrd.Order__c), delOrd.Id);
		}

		List<Order__c> VFOrders = [
			SELECT Id, VF_Contract__r.Opportunity__c
			FROM Order__c
			WHERE VF_Contract__r.Opportunity__c IN :deliveryOrderIdByOpportunityId.keySet()
		];

		for (Order__c ord : VFOrders) {
			ord.COM_Delivery_Order__c = deliveryOrderIdByOpportunityId.get(ord.VF_Contract__r.Opportunity__c);
		}

		update VFOrders;
	}
}
