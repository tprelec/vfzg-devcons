public with sharing class CS_ServiceTriggerHandler extends TriggerHandler {
	public override void beforeInsert() {
		System.debug(LoggingLevel.DEBUG, '1: beforeInsert service trigger handler: ' + Limits.getQueries());
		calculateEndDate();
		System.debug(LoggingLevel.DEBUG, '2: beforeInsert service trigger handler: ' + Limits.getQueries());
	}

	public override void afterInsert() {
		List<csord__Service__c> newServices = (List<csord__Service__c>) Trigger.new;
		Map<Id, csord__Service__c> oldMap = (Map<Id, csord__Service__c>) Trigger.oldMap;
		CS_ContractEndDateCalculator.setSubscriptionEndDate(newServices, oldMap);
	}

	public override void beforeUpdate() {
		System.debug(LoggingLevel.DEBUG, '1: beforeUpdate service trigger handler: ' + Limits.getQueries());
		calculateEndDate();
		System.debug(LoggingLevel.DEBUG, '2: beforeUpdate service trigger handler: ' + Limits.getQueries());
	}

	public override void afterUpdate() {
		List<csord__Service__c> newServices = (List<csord__Service__c>) Trigger.new;
		Map<Id, csord__Service__c> oldMap = (Map<Id, csord__Service__c>) Trigger.oldMap;
		CS_ContractEndDateCalculator.setSubscriptionEndDate(newServices, oldMap);

		createBenefitOrchestatorInstance();
	}

	private void calculateEndDate() {
		List<csord__Service__c> newServices = (List<csord__Service__c>) Trigger.new;
		Map<Id, csord__Service__c> oldMap;
		if (Trigger.isUpdate) {
			oldMap = (Map<Id, csord__Service__c>) Trigger.oldMap;
		}

		Set<Id> configIds = new Set<Id>();
		Set<Id> replacedServiceIds = new Set<Id>();
		for (csord__Service__c targetService : newServices) {
			if (targetService.csordtelcoa__Product_Configuration__c != null) {
				configIds.add(targetService.csordtelcoa__Product_Configuration__c);
			}
			if (targetService.csordtelcoa__Replaced_Service__c != null) {
				replacedServiceIds.add(targetService.csordtelcoa__Replaced_Service__c);
			}
		}
		Map<Id, cscfga__Product_Configuration__c> configMap = new Map<Id, cscfga__Product_Configuration__c>();
		if (!configIds.isEmpty()) {
			configMap = new Map<Id, cscfga__Product_Configuration__c>(
				[SELECT Id, cscfga__Contract_Term__c, Contract_Number_Group__c FROM cscfga__Product_Configuration__c WHERE Id = :configIds]
			);
		}

		Map<Id, csord__Service__c> replacedServiceMap = new Map<Id, csord__Service__c>();
		if (!replacedServiceIds.isEmpty()) {
			replacedServiceMap = new Map<Id, csord__Service__c>(
				[SELECT Id, Contract_End_Date__c FROM csord__Service__c WHERE Id = :replacedServiceIds]
			);
		}

		for (csord__Service__c targetService : newServices) {
			CS_ContractEndDateCalculator.revalidateServiceContractEndDate(
				targetService,
				Trigger.isUpdate ? oldMap.get(targetService.Id) : null,
				targetService.csordtelcoa__Product_Configuration__c != null
					? configMap.get(targetService.csordtelcoa__Product_Configuration__c)
					: null,
				targetService.csordtelcoa__Replaced_Service__c != null ? replacedServiceMap.get(targetService.csordtelcoa__Replaced_Service__c) : null
			);
		}
	}

	private void createBenefitOrchestatorInstance() {
		Map<Id, csord__Service__c> newMap = (Map<Id, csord__Service__c>) Trigger.newMap;
		Map<Id, csord__Service__c> oldMap = (Map<Id, csord__Service__c>) Trigger.oldMap;
		List<csord__Service__c> parentServices = new List<csord__Service__c>();
		csord__Service__c oldService;
		for (csord__Service__c service : newMap.values()) {
			oldService = oldMap.get(service.Id);
			if (
				oldService.csord__Status__c != service.csord__Status__c &&
				service.csord__Status__c == 'Implemented' &&
				service.csord__Service__c == null
			) {
				parentServices.add(service);
			}
		}
		if (!parentServices.isEmpty()) {
			new CS_ComProcessCreator(parentServices).run();
		}
	}
}
