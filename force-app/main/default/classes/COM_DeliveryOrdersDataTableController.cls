/**
 * Created by nikola.culej on 4.6.2020.
 * Edited by Marijan Barisin 11.11.2021.
 */

public with sharing class COM_DeliveryOrdersDataTableController {
	@AuraEnabled
	public static List<COM_Delivery_Order__c> getDeliveryOrderList(Id recordId) {
		Task task = [SELECT Id, Order__c, WhatId FROM Task WHERE Id = :recordId];

		return [
			SELECT Id, Site__c, Name, Technical_Contact__c, Site__r.Unify_Site_Name__c, Technical_Contact__r.Name, Products__c, Wish_Date__c
			FROM COM_Delivery_Order__c
			WHERE Parent_Delivery_Order__c = :task.WhatId
		];
	}

	@AuraEnabled
	public static String updateDeliveryOrders(List<sObject> records) {
		return JSON.serialize(Database.update(records));
	}
}
