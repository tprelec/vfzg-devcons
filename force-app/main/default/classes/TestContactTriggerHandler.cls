/**
 *  @description    This class contains unit tests for the ContactTriggerHandler class
 *  @Author         Guy Clairbois
 */
@isTest
@SuppressWarnings('PMD') // For Key Ring; To be refactored later
private class TestContactTriggerHandler {
	static testMethod void testCreateContact() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'ACD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		Ban__c ban = TestUtils.createBan(acct);
		//ban.BOPCode__c = 'TST';
		ban.BOP_export_datetime__c = system.now();
		update ban;
		Contact cont = TestUtils.createContact(acct);

		System.assert(true, 'dummy assertion');
	}

	static testMethod void testUpdateContact() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'ACD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		Ban__c ban = TestUtils.createBan(acct);
		//ban.BOPCode__c = 'TST';
		ban.BOP_export_datetime__c = system.now();
		update ban;
		Contact cont = TestUtils.createContact(acct);

		Test.startTest();

		cont.BOP_export_datetime__c = system.now();
		cont.Email = 'test@test.nl';
		cont.Phone = '9876543210';
		cont.HomePhone = '9876543210';
		cont.MobilePhone = '9876543210';
		cont.AssistantPhone = '9876543210';
		cont.OtherPhone = '9876543210';
		cont.Direct_Phone_Number__c = '9876543210';
		cont.Fax = '425';
		update cont;

		Test.stopTest();

		System.assert(true, 'dummy assertion');
	}
	static testMethod void testCreateContact1() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'ACD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		Ban__c ban = TestUtils.createBan(acct);
		//ban.BOPCode__c = 'TST';
		ban.BOP_export_datetime__c = system.now();
		update ban;
		Contact cont = TestUtils.createContact(acct);

		System.assert(true, 'dummy assertion');
		delete cont;
	}
	static testMethod void testUpdateContact1() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'ACD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		Ban__c ban = TestUtils.createBan(acct);
		//ban.BOPCode__c = 'TST';
		ban.BOP_export_datetime__c = system.now();
		update ban;
		Contact cont = TestUtils.createContact(acct);

		Test.startTest();

		cont.userid__c = owner.Id;
		cont.BOP_export_datetime__c = system.now();
		cont.Email = 'test@test.nl';
		cont.Phone = '9876543210';
		cont.HomePhone = '9876543210';
		cont.MobilePhone = '9876543210';
		cont.AssistantPhone = '9876543210';
		cont.OtherPhone = '9876543210';
		cont.Direct_Phone_Number__c = '9876543210';
		cont.Fax = '425';
		cont.Contact_Hotness_Summary__c = 'test';
		update cont;

		Test.stopTest();

		System.assert(true, 'dummy assertion');
	}
}