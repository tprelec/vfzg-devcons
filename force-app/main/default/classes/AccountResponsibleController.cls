public class AccountResponsibleController {
	public static String STATUS_NOT_STARTED = 'Not Started';
	public static String STATUS_PENDING = 'Pending';

	@AuraEnabled
	public static Map<String, Object> fetchAll(String recordId) {
		if (recordId == null)
			throw new AuraHandledException('Something went wrong.');

		Map<String, Object> res = new Map<String, Object>();
		Dealer_Information__c cvmDealer = Test.isRunningTest()
			? null
			: [SELECT Id, ContactUserID__c FROM Dealer_Information__c WHERE Dealer_Code__c = :Label.Dealer_Code_CVM_Owner];
		Account acc = [
			SELECT
				Id,
				Name,
				Ultimate_Parent_Account__c,
				Ziggo_Customer__c,
				OwnerId,
				Owner.Email,
				Invoiced_Vodafone_Fixed_Converged__c,
				Vodafone_Customer__c,
				Mobile_Dealer__c,
				x1st_Approval_Status__c,
				x2nd_Approval_Status__c,
				x3rd_Approval_Status__c,
				Approval_Request_Comment__c
			FROM Account
			WHERE Id = :recordId
		];
		res.put('acc', acc);

		// check if already in approval/locked
		if (
			Approval.isLocked(acc.Id) ||
			(acc.x1st_Approval_Status__c == STATUS_PENDING ||
			acc.x2nd_Approval_Status__c == STATUS_PENDING ||
			acc.x3rd_Approval_Status__c == STATUS_PENDING)
		) {
			res.put('msg', Label.LABEL_Account_claim_in_process);
			return res;
		}

		Map<Id, User> users = new Map<Id, User>(
			[
				SELECT Id, Name, UserType, Email, ManagerId, Manager.ManagerId, AccountId, Partner_User__c, isActive
				FROM User
				WHERE Id = :UserInfo.getUserId() OR Id = :acc.OwnerId
			]
		);
		User ui = users.get(UserInfo.getUserId());
		res.put('currUser', ui);
		res.put('currOwner', users.get(acc.OwnerId));

		// prevent user to claim an account
		if (
			acc.Ultimate_Parent_Account__c != null ||
			!users.get(acc.OwnerId).isActive ||
			GeneralUtils.isPartnerUser(ui) ||
			(cvmDealer != null &&
			acc.OwnerId == cvmDealer.ContactUserId__c &&
			acc.Mobile_Dealer__c == cvmDealer.Id) ||
			(acc.Ziggo_Customer__c &&
			!acc.Vodafone_Customer__c &&
			!acc.Invoiced_Vodafone_Fixed_Converged__c) ||
			ui.Id == acc.OwnerId
		) {
			res.put('msg', Label.LABEL_Account_claim_criteria_error_message);
			return res;
		}

		try {
			// get the latest dealer code id
			List<Dealer_Information__c> di = [
				SELECT Id, Name, CreatedDate
				FROM Dealer_Information__c
				WHERE ContactUserId__c = :UserInfo.getUserId() AND Status__c = 'Active'
				ORDER BY CreatedDate DESC
			];
			res.put('dealer', di[0]);
		} catch (Exception e) {
			res.put('msg', Label.LABEL_Dealercode_need_for_claim);
		}

		return res;
	}

	@AuraEnabled
	public static Account claim(String recordId, String comment) {
		Account acc = [
			SELECT
				Id,
				Name,
				Ultimate_Parent_Account__c,
				Ziggo_Customer__c,
				Invoiced_Vodafone_Fixed_Converged__c,
				Vodafone_Customer__c,
				Mobile_Dealer__c,
				OwnerId,
				Owner.Email,
				Account_Owner_Manager__c
			FROM Account
			WHERE Id = :recordId
		];

		// set Sales Director
		Map<Id, User> users = new Map<Id, User>(
			[
				SELECT Id, UserType, Email, ManagerId, Manager.ManagerId, AccountId, Partner_User__c
				FROM User
				WHERE Id = :UserInfo.getUserId() OR Id = :acc.ownerId
			]
		);
		if (acc.Account_Owner_Manager__c == null)
			acc.Account_Owner_Manager__c = users.get(acc.OwnerId).ManagerId;
		if (users.get(UserInfo.getUserId()).Manager.ManagerId == users.get(acc.OwnerId).Manager.ManagerId) {
			acc.Sales_Director__c = users.get(UserInfo.getUserId()).Manager.ManagerId;
		}

		// set new owner, start approval process & refresh page
		acc.User__c = UserInfo.getUserId();
		acc.New_Dealer__c = Test.isRunningTest()
			? null
			: [
					SELECT Id, Name, CreatedDate
					FROM Dealer_Information__c
					WHERE ContactUserId__c = :UserInfo.getUserId() AND Status__c = 'Active'
					ORDER BY CreatedDate DESC
					LIMIT 1
			  ]
			  .Id;
		acc.X1st_Approval_Status__c = STATUS_NOT_STARTED;
		acc.X2nd_Approval_Status__c = STATUS_NOT_STARTED;
		acc.X3rd_Approval_Status__c = STATUS_NOT_STARTED;
		acc.Approval_Request_Comment__c = comment;

		update acc;
		return acc;
	}
}
