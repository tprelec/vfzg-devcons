public with sharing class PP_SearchContactsController {
	private static final Integer QUERY_LIMIT = 15;

	@AuraEnabled
	public static List<Contact> getContacts(String accountId, String offsetString) {
		Integer offset = 0;
		try {
			offset = Integer.valueOf(offsetString);
		} catch (Exception exc) {
			System.debug(LoggingLevel.ERROR, exc);
			offset = 0;
		}

		List<Contact> contacts = [
			SELECT
				Id,
				FirstName,
				MiddleName,
				LastName,
				Title,
				Email,
				Phone,
				Gender__c,
				Salutation,
				MobilePhone,
				Authorized_to_Sign_1st__c,
				Account.Authorized_to_sign_1st__c,
				Account.Authorized_to_sign_2nd__c,
				OwnerId
			FROM Contact
			WHERE AccountId = :accountId
			ORDER BY LastName
			LIMIT :QUERY_LIMIT
			OFFSET :offset
		];
		return contacts;
	}

	@AuraEnabled
	public static Contact getContact(String contactId) {
		Contact contact = [
			SELECT
				Id,
				FirstName,
				MiddleName,
				LastName,
				Title,
				Email,
				Phone,
				MobilePhone,
				Gender__c,
				Salutation,
				Email_or_Mobile_Phone_Missing__c,
				Authorized_to_Sign_1st__c,
				Account.Authorized_to_sign_1st__c,
				Account.Authorized_to_sign_2nd__c,
				OwnerId
			FROM Contact
			WHERE Id = :contactId
		];
		return contact;
	}

	@AuraEnabled
	public static String saveContacts(List<Contact> contacts) {
		try {
			upsert contacts;
			return 'SUCCESS';
		} catch (DMLException ex) {
			System.debug(LoggingLevel.INFO, ex.getMessage());
			Map<String, Object> exceptionJSON = new Map<String, Object>();
			exceptionJSON.put('ExceptionMessage', ex.getMessage());
			exceptionJSON.put('DMLMessage', ex.getDmlMessage(0));
			return JSON.serialize(exceptionJSON);
		} catch (Exception ex) {
			System.debug(LoggingLevel.INFO, ex.getMessage());
			return ex.getMessage();
		}
	}

	@AuraEnabled
	public static AuraActionResult authorizeToSignFirst(String accountId, String contactId) {
		try {
			//1. Check that contact belongs to account
			List<Contact> contacts = [SELECT Id, AccountId FROM Contact WHERE Id = :contactId AND AccountId = :accountId];

			Account account = [SELECT Id, Authorized_to_sign_1st__c, Authorized_to_sign_2nd__c FROM Account WHERE Id = :accountId];

			if (contacts.size() != 1) {
				return new AuraActionResult(false, new AuraMessage('Unexpected error', AuraMessageSeverity.ERROR));
			}

			account.Authorized_to_sign_1st__c = contacts.get(0).Id;
			if (account.Authorized_to_sign_2nd__c == contacts.get(0).Id) {
				account.Authorized_to_sign_2nd__c = null;
			}

			update account;

			return new AuraActionResult(true);
		} catch (Exception exc) {
			return new AuraActionResult(false, new AuraMessage(exc.getMessage(), AuraMessageSeverity.ERROR));
		}
	}

	@AuraEnabled
	public static AuraActionResult authorizeToSignSecond(String accountId, String contactId) {
		try {
			List<Contact> contacts = [SELECT Id, AccountId FROM Contact WHERE Id = :contactId AND AccountId = :accountId];

			Account account = [SELECT Id, Authorized_to_sign_1st__c, Authorized_to_sign_2nd__c FROM Account WHERE Id = :accountId];

			if (contacts.size() != 1) {
				return new AuraActionResult(false, new AuraMessage('Unexpected error', AuraMessageSeverity.ERROR));
			}

			account.Authorized_to_sign_2nd__c = contacts.get(0).Id;
			if (account.Authorized_to_sign_1st__c == contacts.get(0).Id) {
				account.Authorized_to_sign_1st__c = null;
			}

			update account;

			return new AuraActionResult(true);
		} catch (Exception exc) {
			return new AuraActionResult(false, new AuraMessage(exc.getMessage(), AuraMessageSeverity.ERROR));
		}
	}

	@AuraEnabled
	public static AuraActionResult removeAuthorizeToSign(String accountId, String contactId) {
		try {
			List<Contact> contacts = [SELECT Id, AccountId FROM Contact WHERE Id = :contactId AND AccountId = :accountId];

			Account account = [SELECT Id, Authorized_to_sign_1st__c, Authorized_to_sign_2nd__c FROM Account WHERE Id = :accountId];

			if (contacts.size() != 1) {
				return new AuraActionResult(false, new AuraMessage('Unexpected error', AuraMessageSeverity.ERROR));
			}

			if (account.Authorized_to_sign_1st__c == contacts.get(0).Id) {
				account.Authorized_to_sign_1st__c = null;
			}

			if (account.Authorized_to_sign_2nd__c == contacts.get(0).Id) {
				account.Authorized_to_sign_2nd__c = null;
			}

			update account;

			return new AuraActionResult(true);
		} catch (Exception exc) {
			return new AuraActionResult(false, new AuraMessage(exc.getMessage(), AuraMessageSeverity.ERROR));
		}
	}

	/*
    @AuraEnabled
    public static AuraActionResult deleteContact(String contactId)
    {
        try
        {
            if (!Schema.sObjectType.Contact.isDeletable()) {
                return new AuraActionResult(false,
                        new AuraMessage('You don\'t have rights to delete this record', AuraMessageSeverity.ERROR));
            }

            delete new Contact(Id = contactId);
        }
        catch( Exception exc )
        {
            return new AuraActionResult(false,
                    new AuraMessage(exc.getMessage(), AuraMessageSeverity.ERROR));
        }

        return new AuraActionResult(true,
                new AuraMessage('Contact has been deleted', AuraMessageSeverity.INFO));
    }
    */
}
