global class CS_Profit_And_Loss_Helper {
	private String fieldForType = 'cspl__Type__c';
	private String attributeNameForTaxonomy = 'Taxonomy';
	private String fieldForContractTerm = 'cscfga__Contract_Term__c';
	private String fieldForPricePerMonth = 'Recurring_Product_List_Price__c';
	private String fieldForProductDefinition = 'cscfga__Product_Definition__c';
	private String fieldForProductDefinitionName = 'cscfga__Product_Definition__r.Name';
	private String fieldForPriceOneOff = 'OneOff_List_Price__c'; // TODO updated from cscfga__total_one_off_charge__c
	private String fieldForListPriceOneOff = 'OneOff_Product_List_Price__c';
	private String fieldForCostRecurring = 'RC_Cost__c';
	private String fieldForCostOneOff = 'NRC_Cost__c'; // TODO fill price one off
	private String fieldParentConfigurationId = 'cscfga__Parent_Configuration__c';
	private String fieldDealType = 'Deal_type__c';
	private String fieldProposition = 'Proposition__c';
	private String attributeForQuantity = 'CTN quantity'; // TODO fill field for quantity
	private String stringValueOfMobileCTNProfileType = 'Mobile CTN profile';
	private String keyValuePairDelimiter = '\\_';
	private String keyValueDelimiter = '\\;';
	private cscfga__Product_Basket__c basketDetails { get; set; }
	private Map<Id, cspl.ProductConfiguration> configDetails { get; set; }
	private Map<Id, CS_Basket_Snapshot_Transactional__c> configSnapshots { get; set; }
	private Map<Id, Integer> siteCountForConfig = new Map<Id, Integer>();
	private Map<String, cspl.Calculation> calculations { get; set; }
	private Integer numberOfMonthsInYear = 12;
	private String stringForRecurringChargeProduct = 'Recurring_Charge_Product__c';

	public CS_Profit_And_Loss_Helper(
		cscfga__Product_Basket__c basketDetails,
		Map<Id, cspl.ProductConfiguration> configDetails,
		Map<String, cspl.Calculation> calculations
	) {
		this.basketDetails = basketDetails;
		this.configDetails = configDetails;
		this.calculations = calculations;
		getSnapshotObjects();
		CS_PlInputs plInputs = CS_PlInputsSerilizer.getPlInputsFromSerializedString(basketDetails.PL_Inputs__c);
		if (plInputs != null) {
			siteCountForConfig = plInputs.siteCountForConfig;
		}
	}

	public List<Id> getListOfConfigurationIds() {
		List<Id> listOfConfigurationIds = new List<Id>();
		for (Id id : configDetails.keySet()) {
			listOfConfigurationIds.add(id);
		}
		return listOfConfigurationIds;
	}

	public Boolean isIndirectSales() {
		if (basketDetails.DirectIndirect__c != null) {
			if (basketDetails.DirectIndirect__c == 'Indirect') {
				return true;
			}
		}
		return false;
	}

	public String getCurrentYear(Id id, Integer monthOrderNumber) {
		Date startDate = System.today();
		if (isConfigurationMobile(id) || isBmsMobile(id) || isConfigurationCustomerServices(id)) {
			startDate = Date.valueOf(basketDetails.Expected_delivery_date_for_Mobile__c);
		} else {
			startDate = Date.valueOf(basketDetails.Expected_delivery_date_for_Fixed__c);
		}
		Date newDate = startDate.addMonths(monthOrderNumber);
		return String.valueOf(newDate.Year());
	}

	public Decimal getSubscriptionHardwareCommision(Id id) {
		for (Id pcId : configDetails.keySet()) {
			if (pcId == id) {
				if (configSnapshots.get(id) != null) {
					if (configSnapshots.get(id).SubscriptionHardwareCommision__c != null) {
						return configSnapshots.get(id).SubscriptionHardwareCommision__c;
					}
				}
			}
		}
		return 0;
	}

	public Decimal getAddonHardwareCommision(Id id) {
		for (Id pcId : configDetails.keySet()) {
			if (pcId == id) {
				if (configSnapshots.get(id) != null) {
					if (configSnapshots.get(id).AddonHardwareCommision__c != null) {
						return configSnapshots.get(id).AddonHardwareCommision__c;
					}
				}
			}
		}
		return 0;
	}

	public Decimal getThresholdAmount(Id id) {
		for (Id pcId : configDetails.keySet()) {
			if (pcId == id) {
				if (configSnapshots.get(id) != null) {
					if (configSnapshots.get(id).Treshold_Amount__c != null) {
						return configSnapshots.get(id).Treshold_Amount__c;
					}
				}
			}
		}
		return 0;
	}

	public Set<String> getAllDealItems() {
		Set<String> allDealItems = new Set<String>();

		for (Id id : configDetails.keySet()) {
			if (configSnapshots.get(id) != null) {
				if (configSnapshots.get(id).Deal_Item_Recurring__c != null) {
					allDealItems.add(configSnapshots.get(id).Deal_Item_Recurring__c);
				}

				if (configSnapshots.get(id).Deal_Item_One_Off__c != null) {
					allDealItems.add(configSnapshots.get(id).Deal_Item_One_Off__c);
				}
			}
		}

		return allDealItems;
	}

	public Decimal getRecurringCost(Id id) {
		if (configSnapshots.get(id) != null) {
			if (getFieldValue(id, fieldForCostRecurring) != null) {
				return Decimal.valueOf(getFieldValue(id, fieldForCostRecurring));
			}
		}
		return 0;
	}

	public Decimal getOneOffCost(Id id) {
		if (configSnapshots.get(id) != null) {
			if (getFieldValue(id, fieldForCostOneOff) != null) {
				return Decimal.valueOf(getFieldValue(id, fieldForCostOneOff));
			}
		}
		return 0;
	}

	public Id getRecurringProductId(Id id) {
		if (configSnapshots.get(id) != null) {
			return getFieldValue(id, stringForRecurringChargeProduct);
		}
		return null;
	}

	public String getDealType(Id id) {
		if (configSnapshots.get(id) != null) {
			return getFieldValue(id, fieldDealType);
		}
		return null;
	}

	public String getConfigurationProposition(Id id) {
		if (configSnapshots.get(id) != null) {
			return getFieldValue(id, fieldProposition);
		}
		return null;
	}

	public String getDealItemRecurring(Id id) {
		if (configSnapshots.get(id) != null) {
			return configSnapshots.get(id).Deal_Item_Recurring__c;
		}
		return null;
	}

	public String getDealItemOneOff(Id id) {
		if (configSnapshots.get(id) != null) {
			return configSnapshots.get(id).Deal_Item_One_Off__c;
		}
		return null;
	}

	public void getSnapshotObjects() {
		List<Id> listOfConfigurationIds = getListOfConfigurationIds();
		List<CS_Basket_Snapshot_Transactional__c> snapshotObjects = [
			SELECT
				SubscriptionHardwareCommision__c,
				AddonHardwareCommision__c,
				Treshold_Amount__c,
				Deal_Item_One_Off__c,
				Deal_Item_Recurring__c,
				Product_Configuration__c
			FROM CS_Basket_Snapshot_Transactional__c
			WHERE Product_Configuration__c IN :listOfConfigurationIds
		];
		configSnapshots = new Map<Id, CS_Basket_Snapshot_Transactional__c>();

		for (CS_Basket_Snapshot_Transactional__c snapshotObject : snapshotObjects) {
			configSnapshots.put(snapshotObject.Product_Configuration__c, snapshotObject);
		}
	}

	public cspl.Calculation getCalculation(String calculationName) {
		return calculations.get(calculationName);
	}

	public String getTypeOf(Id id) {
		return getFieldValue(id, fieldForType);
	}

	public Id getParentConfigurationId(Id id) {
		return getFieldValue(id, fieldParentConfigurationId);
	}

	public Boolean checkIfManualFormulasAreNeeded(List<CS_Profit_And_Loss_Formula_Category> taxonomyMap) {
		for (CS_Profit_And_Loss_Formula_Category taxonomie : taxonomyMap) {
			//go througth all taxonomies
			if (taxonomie.profitAndLossFormulaCategory == 0) {
				//if all of the config has formula = 0 dont touch anything
				continue;
			} else {
				// if any of them got category different than 1 the we do manual formula
				return true;
			}
		}
		return false;
	}

	public List<Decimal> addListZeroElements(List<Decimal> oldList, Integer newSize) {
		List<Decimal> newList = new List<Decimal>(oldList);

		if (newSize > oldList.size()) {
			for (Integer i = 0; i < (newSize - oldList.size()); i++) {
				newList.add(0);
			}
		}
		return newList;
	}

	public List<Decimal> setListValuesToZero(List<Decimal> oldList) {
		List<Decimal> newList = new List<Decimal>();
		for (Decimal element : oldList) {
			newList.add(0);
		}
		return newList;
	}

	// TODO cleanup method
	public List<Decimal> getDiscountValue(cspl.ProductConfiguration config) {
		if (config != null) {
			Decimal oneOffAmount = 0;
			Decimal oneOffPercentage = 0;
			Decimal recurringAmount = 0;
			Decimal recurringPercentage = 0;
			//List<cscfga.ProductConfiguration.Discount> discounts;
			cscfga.ProductConfiguration.ProductDiscount prodDiscount;
			String discountJSONString = (String) config.config.get('cscfga__discounts__c');
			if (discountJSONString != null) {
				//Map<String, Object> discountJSON = (Map<String, Object>) JSON.deserializeUntyped(discountJSONString);
				prodDiscount = (cscfga.ProductConfiguration.ProductDiscount) JSON.deserializeStrict(
					discountJSONString,
					cscfga.ProductConfiguration.ProductDiscount.class
				);
				//discounts = prodDiscount.discounts;
				/**/
				/*if (discountJSON.get('discounts') != null) {	
                    List<Object> discountStringList = (List<Object>) discountJSON.get('discounts');	
                    List<csdiscounts.ConfigurationQueryResult.Discount> newDiscountList = (List<csdiscounts.ConfigurationQueryResult.Discount>) JSON.deserialize(JSON.serialize(discountStringList), List<csdiscounts.ConfigurationQueryResult.Discount>.class);	
                    discounts = newDiscountList;	
                }*/
			}
			if (prodDiscount != null) {
				for (cscfga.ProductConfiguration.Discount iter : prodDiscount.discounts) {
					for (cscfga.ProductConfiguration.Discount iterChild : iter.memberDiscounts) {
						if (iterChild.chargeType == 'recurring') {
							if (iterChild.type == 'absolute') {
								recurringAmount = iterChild.amount;
							} else {
								recurringPercentage = iterChild.amount;
							}
						} else if (iterChild.chargeType == 'oneOff') {
							if (iterChild.type == 'absolute') {
								oneOffAmount = iterChild.amount;
							} else {
								oneOffPercentage = iterChild.amount;
							}
						}
					}
				}
			}

			List<Decimal> returnDiscount = new List<Decimal>();
			returnDiscount.add(oneOffAmount);
			returnDiscount.add(oneOffPercentage);
			returnDiscount.add(recurringAmount);
			returnDiscount.add(recurringPercentage);

			return returnDiscount;
		}
		return null;
	}

	public Map<String, CS_Profit_And_Loss_Formula_Category> getTaxonomyPerProposition(
		Map<Id, cspl.ProductConfiguration> configDetails,
		String proposition
	) {
		Map<String, CS_Profit_And_Loss_Formula_Category> allTaxonomies = new Map<String, CS_Profit_And_Loss_Formula_Category>();
		for (Id id : configDetails.keySet()) {
			cspl.ProductConfiguration config = configDetails.get(id);
			if (getFieldValue(id, 'cspl__Type__c') == proposition) {
				if (getTaxonomieValue(config) != null && getTaxonomieValue(config) != '') {
					if (
						getAttributeValue(config, 'PLFormula') ==
						'Price Per Month * Quantity Average Base * Number Of Months In Contract For Current Year'
					) {
						allTaxonomies.put(
							getTaxonomieValue(config),
							new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 1)
						);
					} else if (
						getAttributeValue(config, 'PLFormula') ==
						'Price Per Month * Quantity Closing Base * Number Of Months In Contract For Current Year'
					) {
						allTaxonomies.put(
							getTaxonomieValue(config),
							new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 1, 1)
						);
					} else if (
						getAttributeValue(config, 'PLFormula') ==
						'Price * Quantity * Duration * ( Sum Of Closing Bases Over All Months In Contract For Current Year / Sum Of Closing Bases Over All Months In Contract )'
					) {
						allTaxonomies.put(
							getTaxonomieValue(config),
							new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 2)
						);
					} else if (
						getAttributeValue(config, 'PLFormula') ==
						'Price * Quantity * Duration * ( Number Of Months In Current Year % Number Of Months In Contract )'
					) {
						allTaxonomies.put(
							getTaxonomieValue(config),
							new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 3)
						);
					} else if (getAttributeValue(config, 'PLFormula') == 'Fixed usage') {
						allTaxonomies.put(
							getTaxonomieValue(config),
							new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 4)
						);
					} else {
						allTaxonomies.put(
							getTaxonomieValue(config),
							new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 0)
						);
					}
				}
			}
		}
		return allTaxonomies;
	}

	public Map<String, CS_Profit_And_Loss_Formula_Category> getTaxonomyMap(List<cspl.ProductConfiguration> configDetails) {
		Map<String, CS_Profit_And_Loss_Formula_Category> allTaxonomies = new Map<String, CS_Profit_And_Loss_Formula_Category>();
		for (cspl.ProductConfiguration config : configDetails) {
			if (getTaxonomieValue(config) != null && getTaxonomieValue(config) != '') {
				if (
					getAttributeValue(config, 'PLFormula') ==
					'Price Per Month * Quantity Average Base * Number Of Months In Contract For Current Year'
				) {
					allTaxonomies.put(
						getTaxonomieValue(config),
						new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 1)
					);
				} else if (
					getAttributeValue(config, 'PLFormula') ==
					'Price Per Month * Quantity Closing Base * Number Of Months In Contract For Current Year'
				) {
					allTaxonomies.put(
						getTaxonomieValue(config),
						new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 1, 1)
					);
				} else if (
					getAttributeValue(config, 'PLFormula') ==
					'Price * Quantity * Duration * ( Sum Of Closing Bases Over All Months In Contract For Current Year / Sum Of Closing Bases Over All Months In Contract )'
				) {
					allTaxonomies.put(
						getTaxonomieValue(config),
						new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 2)
					);
				} else if (
					getAttributeValue(config, 'PLFormula') ==
					'Price * Quantity * Duration * ( Number Of Months In Current Year % Number Of Months In Contract )'
				) {
					allTaxonomies.put(
						getTaxonomieValue(config),
						new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 3)
					);
				} else if (getAttributeValue(config, 'PLFormula') == 'Fixed usage') {
					allTaxonomies.put(
						getTaxonomieValue(config),
						new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 4)
					);
				} else {
					allTaxonomies.put(
						getTaxonomieValue(config),
						new CS_Profit_And_Loss_Formula_Category(new List<String>{ getTaxonomieValue(config) }, 0)
					);
				}
			} else {
				if (
					!config.config.Name.Contains('Package') &&
					getFieldValue(config.config.Id, 'cspl__Type__c') != '' &&
					getFieldValue(config.config.Id, 'cspl__Type__c') != null
				) {
					allTaxonomies.put(
						getFieldValue(config.config.Id, 'cspl__Type__c'),
						new CS_Profit_And_Loss_Formula_Category(new List<String>{ getFieldValue(config.config.Id, 'cspl__Type__c') }, 0)
					);
				}
			}
		}
		return allTaxonomies;
	}

	public String getName(Id id) {
		return getFieldValue(id, 'Name');
	}

	public Set<String> getPropositionList(Map<Id, cspl.ProductConfiguration> configDetails) {
		Set<String> allPropositions = new Set<String>();

		for (Id id : configDetails.keySet()) {
			if (getFieldValue(id, 'cspl__Type__c') != null) {
				allPropositions.add(getFieldValue(id, 'cspl__Type__c'));
			}
		}
		return allPropositions;
	}

	public String getAttributeValue(cspl.ProductConfiguration config, String attributeName) {
		if (config != null) {
			if (config.attributes.get(attributeName) != null) {
				return JSON.serialize(config.attributes.get(attributeName).value).replace('"', '');
			}
		}
		return null;
	}

	public String getTaxonomieValue(cspl.ProductConfiguration config) {
		return getAttributeValue(config, attributeNameForTaxonomy);
	}

	public Integer numberOfMonthsInYear(Integer yearOrderNumber) {
		Integer numberOfMonths = 0;
		for (Integer i = 1; i <= Integer.valueOf(basketDetails.Contract_duration_Mobile__c); i++) {
			if (calculateYearOrderForMobile(i) == yearOrderNumber) {
				numberOfMonths++;
			}
		}
		return numberOfMonths;
	}

	public Integer calculateYearOrderForMobile(Integer monthOrderNumber) {
		Integer yearOrderNumber = Integer.valueOf(
			Math.Floor(Decimal.valueOf(monthOrderNumber + getContractOffsetInMonthsForMobile() - 2) / numberOfMonthsInYear)
		);
		return yearOrderNumber + 1;
	}

	public Integer calculateYearOrderForFixed(Integer monthOrderNumber) {
		Integer yearOrderNumber = Integer.valueOf(
			Math.Floor(Decimal.valueOf(monthOrderNumber + getContractOffsetInMonthsForFixed() - 2) / numberOfMonthsInYear)
		);
		return yearOrderNumber + 1;
	}

	public Decimal getAverageBase(Integer monthOrderNumber, Id id) {
		Decimal averageBase = 0;

		String averageBaseContainer = getAttributeValue(id, 'AverageBaseContainer');
		if (averageBaseContainer == null) {
			return averageBase;
		}
		List<String> averageBaseContainerSplitted = splitByDelimiter(averageBaseContainer, keyValuePairDelimiter);
		Integer i = 1;
		for (String str : averageBaseContainerSplitted) {
			if (i == (monthOrderNumber + 1)) {
				averageBase = Decimal.valueOf(splitByDelimiter(str, keyValueDelimiter)[1]);
				return averageBase;
			}
			i++;
		}

		return averageBase;
	}

	public Integer getQuantity(Id id) {
		if (configDetails != null && configDetails.get(id) != null) {
			return Integer.valueOf(configDetails.get(id).config.cscfga__Quantity__c);
		}
		return 0;
	}

	public Integer getClosingBase(Integer monthOrderNumber, Id id) {
		Integer closingBase = 0;

		String closingBaseContainer = getAttributeValue(id, 'ClosingBaseContainer');
		if (closingBaseContainer == null) {
			return Integer.valueOf(configDetails.get(id).config.cscfga__Quantity__c);
		}
		List<String> closingBaseContainerSplitted = splitByDelimiter(closingBaseContainer, keyValuePairDelimiter);
		Integer i = 1;
		for (String str : closingBaseContainerSplitted) {
			if (i == (monthOrderNumber + 1)) {
				closingBase = Integer.valueOf(splitByDelimiter(str, keyValueDelimiter)[1]);
				return closingBase;
			}
			i++;
		}

		return closingBase;
	}

	public Boolean isConfigurationAddon(Id id) {
		if (configDetails.get(id).config.cscfga__Product_Definition__r.Name == 'Addon') {
			return true;
		}
		return false;
	}

	public Boolean isParentConfigurationCLFV(Id id) {
		if (configDetails.get(id).config.cscfga__Parent_Configuration__r.Name == 'Company Level Fixed Voice') {
			return true;
		}
		return false;
	}

	public Boolean isConfigurationMobileAddon(Id id) {
		if (configDetails.get(id).config.cscfga__Product_Definition__r.Name == 'Mobile CTN addons') {
			return true;
		}
		return false;
	}

	public Boolean isConfigurationMobileSubscription(Id id) {
		if (configDetails.get(id).config.cscfga__Product_Definition__r.Name == 'Mobile CTN subscription') {
			return true;
		}
		return false;
	}

	public Boolean isConfigurationMobile(Id id) {
		if (
			configDetails.get(id).config.cscfga__Product_Definition__r.Name != null &&
			configDetails.get(id).config.cscfga__Product_Definition__r.Name.Contains('Mobile')
		) {
			return true;
		}
		return false;
	}

	public Boolean isConfigurationCustomerServices(Id id) {
		if (
			configDetails.get(id).config.cscfga__Product_Definition__r.Name.containsIgnoreCase('Customer Services') ||
			configDetails.get(id).config.cscfga__Product_Definition__r.Name.containsIgnoreCase('Customer Services Product')
		) {
			return true;
		}
		return false;
	}

	// BMS can be sold with both mobile and fixed
	// check situation when BMS is sold "as mobile"
	public Boolean isBmsMobile(Id id) {
		if (
			configDetails.get(id).config.cscfga__Product_Definition__r.Name != null &&
			(configDetails.get(id).config.cscfga__Product_Definition__r.Name.containsIgnoreCase('Business Managed Services') ||
			configDetails.get(id).config.cscfga__Product_Definition__r.Name.containsIgnoreCase('BMS product')) &&
			basketDetails.Expected_delivery_date_for_Fixed__c == null &&
			basketDetails.Expected_delivery_date_for_Mobile__c != null
		) {
			return true;
		}
		return false;
	}

	public Decimal getFlexibilityMargin(String configurationProposition, Map<String, Double> orderformPropositions) {
		Decimal margin;
		List<String> configurationPropositions = new List<String>{ 'Flex Mobile', 'OneBusiness', 'OneMobile', 'RedPro', 'Data only', 'OneMobile4' };
		if (configurationPropositions.contains(configurationProposition)) {
			margin = orderformPropositions.get(configurationProposition);
			if (margin != null) {
				margin = margin / 100;
			}
		} else {
			margin = null;
		}
		return margin;
	}

	public List<Decimal> calculateFlexibility(
		Integer disconnectQuantity,
		Decimal reccuringCharge,
		Integer durationInMonths,
		Decimal flexibilityMargin
	) {
		System.debug(
			'Flexibility calculations parameters - Quantity: ' +
			disconnectQuantity +
			' ; Charge: ' +
			reccuringCharge +
			'; Duration: ' +
			durationInMonths +
			'; Margin: ' +
			flexibilityMargin
		);
		Decimal monthlyDisconnects = ((Decimal) disconnectQuantity / (Decimal) durationInMonths) * flexibilityMargin;
		List<Decimal> flexibilityMontlyValues = new List<Decimal>(durationInMonths);
		Decimal accumulatedMonthlyDisconnectValue = 0;
		for (Integer i = 0; i < durationInMonths; i++) {
			accumulatedMonthlyDisconnectValue += monthlyDisconnects;
			flexibilityMontlyValues[i] = (accumulatedMonthlyDisconnectValue * reccuringCharge) * (1 - flexibilityMargin);
		}
		return flexibilityMontlyValues;
	}

	public List<Decimal> calculateFlexibility(List<CS_Profit_And_Loss_FlexibilityItem> items) {
		if (items.size() == 0)
			return null;

		Integer disconnectQuantity = 0;
		for (CS_Profit_And_Loss_FlexibilityItem item : items) {
			disconnectQuantity += item.Quantity;
		}

		if (items[0].Margin == null) {
			System.debug('Flexibility margin is not available for proposition: ' + items[0].Proposition);
			return null;
		}

		if (items[0].Duration <= 0) {
			System.debug('Flexibility cannot be calculated because of the following mobile contract duration value: ' + items[0].Duration);
			return null;
		}

		return calculateFlexibility(disconnectQuantity, items[0].RecurringPrice, items[0].Duration, items[0].Margin);
	}

	public Integer calculateQuantity(Integer monthOrderNumber, Id id) {
		if (isConfigurationMobile(id)) {
			return getClosingBase(monthOrderNumber, id);
		}
		Integer siteMultiplier = siteCountForConfig.containsKey(id) ? siteCountForConfig.get(id) : 1;
		return Integer.valueOf(configDetails.get(id).config.cscfga__Quantity__c) * siteMultiplier;
	}

	public Integer getSumClosingBase() {
		Integer sumOfClosingBase = 0;
		for (Id id : configDetails.keySet()) {
			String typeOf = getTypeOf(id);
			if (typeOf == stringValueOfMobileCTNProfileType) {
				String closingBaseContainer = getAttributeValue(id, 'ClosingBaseContainer');
				List<String> closingBaseContainerSplitted = splitByDelimiter(closingBaseContainer, keyValuePairDelimiter);
				Integer i = 1;
				for (String str : closingBaseContainerSplitted) {
					if (i == 1) {
						// Because total is calculated into the sum.
						i++;
						continue;
					}
					Integer valueOfClosingBaseForMonth = Integer.valueOf(splitByDelimiter(str, keyValueDelimiter)[1]);
					sumOfClosingBase = sumOfClosingBase + valueOfClosingBaseForMonth;
					i++;
				}
			}
		}
		return sumOfClosingBase;
	}

	public Decimal getSumOfAllFieldValuesForType(List<String> typeList, String field) {
		Decimal sumOfAllFields = 0;
		for (Id id : configDetails.keySet()) {
			String typeOf = getTypeOf(id);
			for (String type : typeList) {
				if (typeOf == type) {
					String stringValueOfField = getFieldValue(id, field);
					Decimal valueOfField = Decimal.valueOf(stringValueOfField);
					if (valueOfField != null) {
						sumOfAllFields = sumOfAllFields + valueOfField;
					}
				}
			}
		}
		return sumOfAllFields;
	}

	public Decimal getSumOfAllAttributeValuesForType(List<String> typeList, String attribute) {
		Decimal sumOfAllAttributes = 0;
		for (Id id : configDetails.keySet()) {
			String typeOf = getTypeOf(id);
			for (String type : typeList) {
				if (typeOf == type) {
					String stringValueOfAttribute = getAttributeValue(id, attribute);
					if (stringValueOfAttribute != null) {
						Decimal valueOfAttribute = Decimal.valueOf(stringValueOfAttribute);
						if (valueOfAttribute != null) {
							sumOfAllAttributes = sumOfAllAttributes + valueOfAttribute;
						}
					}
				}
			}
		}
		return sumOfAllAttributes;
	}

	public Integer getMobileDuration() {
		if (basketDetails.Contract_duration_Mobile__c != null && basketDetails.Contract_duration_Mobile__c != '') {
			return Integer.valueOf(basketDetails.Contract_duration_Mobile__c);
		}
		return 0;
	}

	public Integer getBMSDuration() {
		if (basketDetails.Contract_duration_BMS__c != null && basketDetails.Contract_duration_BMS__c != '') {
			return Integer.valueOf(basketDetails.Contract_duration_BMS__c);
		}
		return 0;
	}

	public Integer getFixedDuration() {
		if (basketDetails.Contract_duration_Fixed__c != null) {
			return Integer.valueOf(basketDetails.Contract_duration_Fixed__c);
		}
		return 0;
	}

	public Integer getDuration() {
		Integer maxDuration = 0;

		if (getMobileDuration() > maxDuration) {
			maxDuration = getMobileDuration();
		}

		if (getFixedDuration() > maxDuration) {
			maxDuration = getFixedDuration();
		}

		if (getBMSDuration() > maxDuration) {
			maxDuration = getBMSDuration();
		}

		return maxDuration;
	}

	public Integer getContractOffsetInMonthsForMobile() {
		if (basketDetails.Expected_delivery_date_for_Mobile__c == null) {
			return 0;
		}
		Date startDate = Date.valueOf(basketDetails.Expected_delivery_date_for_Mobile__c);
		return startDate.month();
	}

	public Integer getContractOffsetInMonthsForFixed() {
		Date startDate = Date.valueOf(basketDetails.Expected_delivery_date_for_Fixed__c);
		return startDate.month();
	}

	public Integer getSumOfClosingBasesOverAllMonthsInContract() {
		Integer sumOfClosingBases = 0;
		for (Integer i = 0; i < Integer.valueOf(basketDetails.Contract_duration_Mobile__c); i++) {
		}
		return sumOfCLosingBases;
	}

	public Integer getContractTerm(Id id) {
		if (getFieldValue(id, fieldForContractTerm) != null) {
			return Integer.valueOf(getFieldValue(id, fieldForContractTerm));
		}

		return getDuration();
	}

	public String getAttributeValue(Id id, String attributeName) {
		if (configDetails.get(id) != null) {
			if (configDetails.get(id).attributes.get(attributeName) != null) {
				// System.debug('Value from JSON:' + JSON.serialize(configDetails.get(id).attributes.get(attributeName).value).replace('"', ''));
				// TODO have no idea why JSON holds " sign inside of string
				String result = JSON.serialize(configDetails.get(id).attributes.get(attributeName).value).replace('"', '').replace('\'', '');
				if (result == 'null') {
					return null;
				} else {
					return result;
				}
			}
		}
		return null;
	}

	public Decimal getOneOffListPriceValue(Id id) {
		return Decimal.valueOf(getFieldValue(id, fieldForListPriceOneOff));
	}

	public Decimal getOneOffValue(Id id) {
		Integer siteMultiplier = siteCountForConfig.containsKey(id) ? siteCountForConfig.get(id) : 1;
		return Decimal.valueOf(getFieldValue(id, fieldForPriceOneOff)) * siteMultiplier;
	}

	public Decimal getPricePerMonthValue(Id id) {
		return Decimal.valueOf(getFieldValue(id, fieldForPricePerMonth));
	}

	public String getFieldValue(Id id, String fieldName) {
		if (configDetails.get(id) != null) {
			if (configDetails.get(id).config.get(fieldName) != null) {
				return String.valueOf(configDetails.get(id).config.get(fieldName));
			}
		}
		return null;
	}

	public List<String> splitByDelimiter(String text, String delimiter) {
		if (text != null && text != '' && delimiter != null && delimiter != '') {
			return text.Split(delimiter);
		}
		return new List<String>();
	}

	public CS_Profit_And_Loss_Formula_Argument getFormulaArgumentForCategory(
		CS_Profit_And_Loss_Formula_Category formulaCategory,
		Integer monthOrderNumber,
		Id id
	) {
		if (formulaCategory.profitAndLossFormulaCategory == 1) {
			if (formulaCategory.profitAndLossFormulaSubCategory == 0) {
				return categoryOneFormulaArgument(monthOrderNumber, formulaCategory.profitAndLossSubElementTypes, id);
			} else if (formulaCategory.profitAndLossFormulaSubCategory == 1) {
				return categoryOneSubCategoryOneFormulaArgument(monthOrderNumber, formulaCategory.profitAndLossSubElementTypes, id);
			} else if (formulaCategory.profitAndLossFormulaSubCategory == 2) {
				return categoryOneSubCategoryTwoFormulaArgument(monthOrderNumber, formulaCategory.profitAndLossSubElementTypes);
			} else if (formulaCategory.profitAndLossFormulaSubCategory == 3) {
				return categoryOneSubCategoryThreeFormulaArgument(monthOrderNumber, formulaCategory.profitAndLossSubElementTypes);
			} else if (formulaCategory.profitAndLossFormulaSubCategory == 4) {
				return categoryOneSubCategoryFourFormulaArgument(monthOrderNumber, formulaCategory.profitAndLossSubElementTypes);
			}
		} else if (formulaCategory.profitAndLossFormulaCategory == 2) {
			return categoryTwoFormulaArgument(monthOrderNumber, formulaCategory.profitAndLossSubElementTypes, id);
		} else if (formulaCategory.profitAndLossFormulaCategory == 3) {
			return categoryThreeFormulaArgument(monthOrderNumber, formulaCategory.profitAndLossSubElementTypes);
		} else if (formulaCategory.profitAndLossFormulaCategory == 4) {
			return categoryFourFormulaArgument();
		} else if (formulaCategory.profitAndLossFormulaCategory == 6) {
			return categorySixFormulaArgument(monthOrderNumber, formulaCategory.profitAndLossSubElementTypes, id);
		} else if (formulaCategory.profitAndLossFormulaCategory == 7) {
			return categorySevenFormulaArgument();
		} else if (formulaCategory.profitAndLossFormulaCategory == 8) {
			return categoryEightFormulaArgument();
		} else if (formulaCategory.profitAndLossFormulaCategory == 10) {
			return categoryTenFormulaArgument();
		} else if (formulaCategory.profitAndLossFormulaCategory == 0) {
			return categoryZeroFormulaArgument(monthOrderNumber, formulaCategory.profitAndLossSubElementTypes, id);
		}
		return null;
	}

	public CS_Profit_And_Loss_Formula_Argument categoryZeroFormulaArgument(
		Integer monthOrderNumber,
		List<String> profitAndLossSubElementTypes,
		Id id
	) {
		CS_Profit_And_Loss_Formula_Argument argument = new CS_Profit_And_Loss_Formula_Argument();
		argument.recurringCharge = getPricePerMonthValue(id);
		argument.quantity = calculateQuantity(monthOrderNumber, id);
		return argument;
	}

	public CS_Profit_And_Loss_Formula_Argument categoryOneFormulaArgument(
		Integer monthOrderNumber,
		List<String> profitAndLossSubElementTypes,
		Id id
	) {
		CS_Profit_And_Loss_Formula_Argument argument = new CS_Profit_And_Loss_Formula_Argument();
		argument.price = getPricePerMonthValue(id);
		argument.quantity = getAverageBase(monthOrderNumber, id);
		argument.duration = 1; //numberOfMonthsInYear(calculateYearOrderForMobile(monthOrderNumber));
		return argument;
	}

	public CS_Profit_And_Loss_Formula_Argument categoryOneSubCategoryOneFormulaArgument(
		Integer monthOrderNumber,
		List<String> profitAndLossSubElementTypes,
		Id id
	) {
		CS_Profit_And_Loss_Formula_Argument argument = new CS_Profit_And_Loss_Formula_Argument();
		argument.price = getPricePerMonthValue(id);
		argument.quantity = getClosingBase(monthOrderNumber, id);
		argument.duration = 1; //numberOfMonthsInYear(calculateYearOrderForMobile(monthOrderNumber));
		return argument;
	}

	public CS_Profit_And_Loss_Formula_Argument categoryOneSubCategoryTwoFormulaArgument(
		Integer monthOrderNumber,
		List<String> profitAndLossSubElementTypes
	) {
		CS_Profit_And_Loss_Formula_Argument argument = new CS_Profit_And_Loss_Formula_Argument();

		argument.price = getSumOfAllFieldValuesForType(profitAndLossSubElementTypes, fieldForPricePerMonth);

		argument.quantity = getSumOfAllAttributeValuesForType(profitAndLossSubElementTypes, attributeForQuantity);
		argument.duration = 1; //numberOfMonthsInYear(calculateYearOrderForMobile(monthOrderNumber));
		return argument;
	}

	public CS_Profit_And_Loss_Formula_Argument categoryOneSubCategoryThreeFormulaArgument(
		Integer monthOrderNumber,
		List<String> profitAndLossSubElementTypes
	) {
		CS_Profit_And_Loss_Formula_Argument argument = new CS_Profit_And_Loss_Formula_Argument();
		argument.price = getSumOfAllFieldValuesForType(profitAndLossSubElementTypes, fieldForPricePerMonth);
		argument.quantity = getSumOfAllAttributeValuesForType(profitAndLossSubElementTypes, attributeForQuantity);
		// argument.duration = getClosingBase(monthOrderNumber);
		return argument;
	}

	public CS_Profit_And_Loss_Formula_Argument categoryOneSubCategoryFourFormulaArgument(
		Integer monthOrderNumber,
		List<String> profitAndLossSubElementTypes
	) {
		CS_Profit_And_Loss_Formula_Argument argument = new CS_Profit_And_Loss_Formula_Argument();
		argument.price = getSumOfAllFieldValuesForType(profitAndLossSubElementTypes, fieldForPricePerMonth);
		argument.quantity = 1; // Quantity is not needed but using 1 because going to be multiplied.
		argument.duration = 1; //numberOfMonthsInYear(calculateYearOrderForMobile(monthOrderNumber));
		return argument;
	}

	public CS_Profit_And_Loss_Formula_Argument categoryTwoFormulaArgument(
		Integer monthOrderNumber,
		List<String> profitAndLossSubElementTypes,
		Id id
	) {
		CS_Profit_And_Loss_Formula_Argument argument = new CS_Profit_And_Loss_Formula_Argument();
		argument.price = getPricePerMonthValue(id);
		argument.quantity = Decimal.valueOf(getAttributeValue(id, attributeForQuantity));
		argument.duration = Integer.valueOf(basketDetails.Contract_duration_Mobile__c);

		argument.sumOfClosingBasesOverAllMonthsInContractForCurrentYear = getClosingBase(monthOrderNumber, id);
		argument.sumOfClosingBasesOverAllMonthsInContract = getSumClosingBase();

		return argument;
	}

	public CS_Profit_And_Loss_Formula_Argument categoryThreeFormulaArgument(Integer monthOrderNumber, List<String> profitAndLossSubElementTypes) {
		CS_Profit_And_Loss_Formula_Argument argument = new CS_Profit_And_Loss_Formula_Argument();
		argument.price = getSumOfAllFieldValuesForType(profitAndLossSubElementTypes, fieldForPricePerMonth);
		argument.quantity = getSumOfAllAttributeValuesForType(profitAndLossSubElementTypes, attributeForQuantity);
		argument.duration = Integer.valueOf(basketDetails.Contract_duration_Mobile__c);

		argument.durationMonthsInContractForCurrentYear = numberOfMonthsInYear(calculateYearOrderForMobile(monthOrderNumber));
		argument.durationMonthsInContract = argument.duration;

		return argument;
	}

	public CS_Profit_And_Loss_Formula_Argument categoryFourFormulaArgument() {
		return CS_Profit_And_Loss_Formula_Argument.getDummyData();
	}

	public CS_Profit_And_Loss_Formula_Argument categorySixFormulaArgument(
		Integer monthOrderNumber,
		List<String> profitAndLossSubElementTypes,
		Id id
	) {
		CS_Profit_And_Loss_Formula_Argument argument = new CS_Profit_And_Loss_Formula_Argument();
		argument.usageValue = Decimal.valueOf(getAttributeValue(id, 'TotalGrossRevenue'));
		argument.duration = Integer.valueOf(basketDetails.Contract_duration_Fixed__c);

		return argument;
	}

	public CS_Profit_And_Loss_Formula_Argument categorySevenFormulaArgument() {
		return CS_Profit_And_Loss_Formula_Argument.getDummyData();
	}

	public CS_Profit_And_Loss_Formula_Argument categoryEightFormulaArgument() {
		return CS_Profit_And_Loss_Formula_Argument.getDummyData();
	}

	public CS_Profit_And_Loss_Formula_Argument categoryTenFormulaArgument() {
		return CS_Profit_And_Loss_Formula_Argument.getDummyData();
	}
}
