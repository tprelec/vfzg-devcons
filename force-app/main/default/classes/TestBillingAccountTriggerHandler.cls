@isTest
public with sharing class TestBillingAccountTriggerHandler {
    final static String BILLING_CHANNEL = 'Paper bill';
    final static String PAYMENT_TYPE = 'Bank Transfer';
    final static String DEFAULT_BA_NAME = 'BA 1';
    final static String DEFAULT_BA_FINANCIAL_NUM = 'SFDT-59 Bill1';

    @testSetup
    private static void setupTestData() {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;

        List<Account> accounts = createCustomerAccounts();
        createBillingAccounts(accounts);

        noTriggers.Flag__c = false;
        upsert noTriggers;
    }

    @isTest
    private static void testSetDefaultBillingAccount() {
        List<Account> accounts = getAccountsAndBilling();

        Test.startTest();
        csconta__Billing_Account__c ba = new csconta__Billing_Account__c(
            csconta__Financial_Account_Number__c = 'SFDT-59 Bill5',
            csconta__Account__c = accounts[0].Id,
            csconta__Billing_Channel__c = BILLING_CHANNEL,
            LG_PaymentType__c = PAYMENT_TYPE,
            LG_Default__c = true,
            LG_HouseNumber__c = '1'
        );
        insert ba;
        Test.stopTest();

        List<Account> updatedAccounts = getAccountsAndBilling();

        System.assertEquals(
            1,
            accounts[0].csconta__Billing_Accounts__r.size(),
            'Only one default billings account expected'
        );
        System.assertEquals(
            1,
            accounts[1].csconta__Billing_Accounts__r.size(),
            'Only one default billings account expected'
        );
        System.assertEquals(
            ba.csconta__Financial_Account_Number__c,
            updatedAccounts[0].csconta__Billing_Accounts__r[0].csconta__Financial_Account_Number__c,
            'Default billing account is invalid'
        );
    }

    private static List<Account> createCustomerAccounts() {
        String customerAccountName = 'Test customer Account';

        List<Account> customerAccounts = new List<Account>();
        for (Integer i = 1; i <= 2; i++) {
            Account customerAccount = new Account(Name = customerAccountName + i);
            customerAccount.KVK_number__c='0000123' + i;
            customerAccount.LG_Footprint__c='Ziggo';
            customerAccount.LG_VisitHouseNumber__c=String.valueOf(i);
            customerAccount.LG_VisitPostalCode__c=String.valueOf(2000 + i) + 'AB';
            customerAccount.LG_PostalHouseNumber__c=String.valueOf(i);
            customerAccount.LG_PostalPostalCode__c=String.valueOf(2000 + i) + 'AB';
            
            customerAccounts.add(customerAccount);
        }
        insert customerAccounts;

        return customerAccounts;
    }

    private static void createBillingAccounts(List<Account> accounts) {
        List<csconta__Billing_Account__c> billAccounts = new List<csconta__Billing_Account__c>();
        billAccounts.add(
            new csconta__Billing_Account__c(
                LG_BillingAccountName__c = 'BA1',
                csconta__Account__c = accounts[0].Id,
                csconta__Billing_Channel__c = BILLING_CHANNEL,
                LG_PaymentType__c = PAYMENT_TYPE,
                LG_HouseNumber__c = '1',
                csconta__Postcode__c = '2002EF',
                csconta__Financial_Account_Number__c = 'SFDT-59 Bill1',
                LG_Default__c = true
            )
        );
        billAccounts.add(
            new csconta__Billing_Account__c(
                LG_BillingAccountName__c = 'BA2',
                csconta__Account__c = accounts[1].Id,
                csconta__Billing_Channel__c = BILLING_CHANNEL,
                LG_PaymentType__c = PAYMENT_TYPE,
                LG_HouseNumber__c = '2',
                csconta__Postcode__c = '2000AB',
                csconta__Financial_Account_Number__c = 'SFDT-59 Bill2',
                LG_Default__c = true
            )
        );
        billAccounts.add(
            new csconta__Billing_Account__c(
                LG_BillingAccountName__c = 'BA3',
                csconta__Account__c = accounts[1].Id,
                csconta__Billing_Channel__c = BILLING_CHANNEL,
                LG_PaymentType__c = PAYMENT_TYPE,
                LG_HouseNumber__c = '3',
                csconta__Postcode__c = '2001CD',
                csconta__Financial_Account_Number__c = 'SFDT-59 Bill3',
                LG_Default__c = false
            )
        );
        insert billAccounts;
    }

    private static List<Account> getAccountsAndBilling() {
        return [
            SELECT
                Id,
                Name,
                (
                    SELECT Name, csconta__Financial_Account_Number__c, csconta__Street__c
                    FROM csconta__Billing_Accounts__r
                    WHERE LG_Default__c = TRUE
                )
            FROM Account
        ];
    }
}