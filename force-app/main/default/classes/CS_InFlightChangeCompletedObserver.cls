global with sharing class CS_InFlightChangeCompletedObserver implements csordcb.ObserverApi.IObserver {
	global void execute(csordcb.ObserverApi.Observable o, Object arg) {
		csordtelcoa.InflightObservable observable = (csordtelcoa.InflightObservable) o;
		Map<Id, Id> ordIdByBasketId = observable.getOrderIdByBasketIdMap();
		List<Id> orderIds = ordIdByBasketId.values();

		System.debug('Executed cloning of the Inflight');

		updatePreHashFieldOnServices(orderIds);

		Map<Id, csord__Subscription__c> subscriptions = new Map<Id, csord__Subscription__c>(
			[SELECT id FROM csord__Subscription__c WHERE csord__Order__c IN :orderIds]
		);
		cssmgnt.API_1.linkSubscriptions(new List<Id>(subscriptions.keySet()));

		csord__Order__c rootOrder = [SELECT Id, csordtelcoa__Opportunity__c FROM csord__Order__c WHERE Id IN :orderIds LIMIT 1];

		List<csord__Solution__c> solutions = [
			SELECT Id
			FROM csord__Solution__c
			WHERE
				cssdm__product_basket__r.cscfga__Opportunity__c = :rootOrder.csordtelcoa__Opportunity__c
				AND (cssdm__product_basket__r.csbb__Synchronised_With_Opportunity__c = TRUE
				OR cssdm__product_basket__r.csordtelcoa__Synchronised_with_Opportunity__c = TRUE)
		];

		for (csord__Solution__c obj : solutions) {
			obj.csord__Order__c = orderIds[0];
		}

		// Link Solutions and Root Order
		if (!solutions.isEmpty()) {
			update solutions;
		}
	}

	private void updatePreHashFieldOnServices(List<Id> orderIds) {
		List<csord__Service__c> servicesToBeUpdated = [
			SELECT id, Commercial_Component_Hash__c, Commercial_Component_Hash_Pre_Inflight__c, csord__Order__c
			FROM csord__Service__c
			WHERE csord__Order__c IN :orderIds
		];

		for (csord__Service__c serviceToBeUpdated : servicesToBeUpdated) {
			serviceToBeUpdated.Commercial_Component_Hash_Pre_Inflight__c = serviceToBeUpdated.Commercial_Component_Hash__c;
		}

		update servicesToBeUpdated;
	}
}
