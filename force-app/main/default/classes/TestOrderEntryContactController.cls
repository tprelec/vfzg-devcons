@isTest
public with sharing class TestOrderEntryContactController {
	@TestSetup
	static void makeData() {
		TestUtils.createCompleteOpportunity();
	}

	@IsTest
	static void testGetContacts() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
		List<Contact> conList = new List<Contact>();

		Test.startTest();

		conList = OrderEntryContactController.getContacts(opp.AccountId);

		Test.stopTest();

		System.assertEquals('TestLastname', conList.get(0).LastName, 'Contact is returned.');
	}

	@IsTest
	static void testGetContact() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
		Contact con = [SELECT Id FROM Contact WHERE AccountId = :opp.AccountId LIMIT 1];
		Test.startTest();

		con = OrderEntryContactController.getContact(con.Id);

		Test.stopTest();

		System.assertEquals('TestFirstname', con.FirstName, 'Contact is returned.');
	}

	@IsTest
	static void testSaveContact() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		List<Contact> cont = [SELECT Id FROM Contact];

		insert new OpportunityContactRole(ContactId = cont[0].Id, OpportunityId = opp.Id, IsPrimary = true);

		Contact newCont = new Contact(FirstName = 'Test13', LastName = 'Testoni');

		Test.startTest();
		Contact nc = OrderEntryContactController.saveContact(newCont, opp.Id);
		Test.stopTest();

		OpportunityContactRole ocr = [SELECT Id, Role, IsPrimary FROM OpportunityContactRole WHERE ContactId = :nc.Id LIMIT 1];

		List<Contact> contPost = [SELECT Id FROM Contact];

		System.assert(contPost.size() > cont.size(), 'There should be one more contact.');
		System.assertEquals('Administrative Contact', ocr.Role, 'Wrong role.');
		System.assertEquals(true, ocr.isPrimary, 'Newly created contact role should be se as primary.');
	}

	@IsTest
	static void testGetCountries() {
		Map<String, String> countries = new Map<String, String>();

		Test.startTest();
		countries = OrderEntryContactController.getCountries();
		Test.stopTest();

		System.assertEquals('Netherlands', countries.get('NL'), 'Returned country is NL.');
	}
}
