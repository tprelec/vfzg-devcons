public class InternetSiteCheck implements ISiteCheck {
	public List<Object> getSites(
		Map<String, String> searchFields,
		String productDefinitionID,
		Id[] excludeIds,
		Integer pageOffset,
		Integer pageLimit
	) {
		System.debug('***** InternetSiteCheck (01)');

		List<Site_Availability__c> finalSites = new List<Site_Availability__c>();

		final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1;
		Integer recordOffset = pageOffset * pageLimit;

		System.debug('***searchFields' + searchFields);
		String accountId = searchFields.get('AccountID');
		String technology = searchFields.get('Technology');
		String searchValue = searchFields.get('searchValue') == '' ? '%' : '%' + searchFields.get('searchValue') + '%';

		System.debug('---+ this is accountId= ' + accountId);
		System.debug('---+ this is technology= ' + technology);
		System.debug('---+ this is searchValue= ' + searchValue);

		Map<Id, Site_Availability__c> siteChecks = new Map<Id, Site_Availability__c>(
			[
				SELECT
					Id,
					Name,
					Access_Infrastructure__c,
					Bandwith_Down_Entry__c,
					Bandwith_Down_Premium__c,
					Bandwith_Sellable_Down__c,
					Bandwith_Sellable_Up__c,
					Bandwith_Up_Entry__c,
					Bandwith_Up_Premium__c,
					Premium_Applicable__c,
					Premium_Vendor__c,
					Usable__c,
					Vendor__c,
					Site__c,
					Site__r.Site_Account__c,
					Site__r.Site_PABX_Certified__c,
					Result_Check__c,
					Offer_amount__c,
					Promo__c,
					Access_Offer_Amount__c,
					Region__c,
					Existing_Infra__c
				FROM Site_Availability__c
				WHERE
					Site__r.Site_Account__c = :accountId
					AND Usable__c = TRUE
					AND Access_Infrastructure__c = 'Coax'
					AND (Vendor__c LIKE :searchValue
					OR Name LIKE :searchValue
					OR Site__r.Site_City__c LIKE :searchValue
					OR Site__r.Site_Postal_Code__c LIKE :searchValue
					OR Site__r.Site_Street__c LIKE :searchValue
					OR Site__r.Building__c LIKE :searchValue
					OR Site__r.Name LIKE :searchValue)
			]
		);

		finalSites = [
			SELECT
				Id,
				Name,
				Order__c,
				Site__c,
				Site__r.Name,
				Vendor__c,
				Bandwith_Sellable_Down__c,
				Bandwith_Sellable_Up__c,
				Bandwith_Down_Premium__c,
				Bandwith_Up_Premium__c,
				Access_Infrastructure__c,
				Existing_Infra__c,
				Region__c,
				Result_Check__c,
				Site__r.Site_Account__c,
				LastModifiedDate,
				Usable__c,
				Site__r.Site_City__c,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_Street__c,
				Site__r.Building__c,
				Premium_Applicable__c,
				Site__r.Site_PABX_Certified__c,
				Offer_amount__c,
				Promo__c,
				Access_Offer_Amount__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Unify_Site_Name__c
			FROM Site_Availability__c
			WHERE Id IN :siteChecks.keySet()
			ORDER BY Site__r.Name, Order__c
			LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT
			OFFSET :recordOffset
		];

		return finalSites;
	}
}
