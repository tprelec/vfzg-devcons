@IsTest
public with sharing class TestQueryExecutorInvocable {
	@TestSetup
	static void makeData() {
		TestUtils.createAccount(null);
	}

	@IsTest
	private static void queryRequest() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		QueryExecutorInvocable.requestQuery reqQuery = new QueryExecutorInvocable.requestQuery();
		reqQuery.criteria1 = new List<String>{ acc.Id };
		reqQuery.query = 'SELECT Id FROM Account WHERE Id IN :criteria1';
		Test.startTest();
		List<List<SObject>> listResponse = QueryExecutorInvocable.queryRequest(new List<QueryExecutorInvocable.requestQuery>{ reqQuery });
		Test.stopTest();
		System.assert(listResponse != null, 'The response is null.');
		for (List<SObject> lstSobjects : listResponse) {
			for (SObject objectFound : lstSobjects) {
				System.assertEquals(objectFound.get('Id'), acc.Id, 'The expected records have not been found');
			}
		}
	}
}
