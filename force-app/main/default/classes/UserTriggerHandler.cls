@SuppressWarnings('PMD') // To be refactored later
public without sharing class UserTriggerHandler extends TriggerHandler {
	List<User> newUsers;
	Map<Id, User> newUsersMap;
	Map<Id, User> oldUsersMap;

	private void init() {
		this.newUsers = (List<User>) this.newList;
		this.newUsersMap = (Map<Id, User>) this.newMap;
		this.oldUsersMap = (Map<Id, User>) this.oldMap;
		preventRecursiveTrigger(2);
	}

	public override void beforeInsert() {
		init();

		validateLocale();
		setZiggoPartnerUserDefaults();
	}

	public override void afterInsert() {
		init();

		assignLicenses();
		checkLicenses(); // TODO
		checkPrimaryPartnerUser();
		prepareForCases(); // TODO
		updateConnectedContactRecords();
		assignPermissionSetToPrimaryPartnerUser(); // TODO
		removePermissionSetsFromInactiveUsers();
		assignPermSetLicsForD2D(); // TODO
		assignPermissionSetsAndGroups();
	}

	public override void beforeUpdate() {
		init();

		validateLocale();
		preventDisablePortalUser(); // TODO
		checkPrimaryPartnerUser();
		checkRelatedRecordsOnDeactivation(); // TODO
		setZiggoPartnerUserDefaults();
	}

	public override void afterUpdate() {
		init();

		freeUpBigMachinesLicense();
		assignLicenses();
		checkLicenses(); // TODO
		prepareForCases(); // TODO
		updateConnectedContactRecords();
		assignPermissionSetToPrimaryPartnerUser(); // TODO
		cleanupPartnerQueues(); // TODO
		removePermissionSetsFromInactiveUsers();
		assignPermSetLicsForD2D(); // TODO
		assignPermissionSetsAndGroups();
		// SF-874: re-assign Account User/Queue to Partner Manager when Sales User is Deactivated
		reassignAccountUserQueueToPartnerManager();
	}

	/**
	 * Assign automatic licenses to Acitve users and delete for Inactive users
	 */
	private void assignLicenses() {
		List<User> usersToProcess = GeneralUtils.filterChangedRecords(newUsers, oldUsersMap, new List<String>{ 'ProfileId', 'IsActive' }, false);
		if (usersToProcess.isEmpty()) {
			return;
		}
		UserService usvc = new UserService(new Map<Id, User>(usersToProcess).keySet());
		usvc.assignLicenses();
	}

	/**
	 * Removes Big Machines License when user is deactivated.
	 */
	private void freeUpBigMachinesLicense() {
		Set<Id> deactivatedUsers = new Set<Id>();
		for (User u : newUsers) {
			if (u.BigMachines__Provisioned__c && !u.IsActive && oldUsersMap.get(u.Id).IsActive) {
				deactivatedUsers.add(u.Id);
			}
		}
		if (!deactivatedUsers.isEmpty()) {
			System.enqueueJob(new UserTriggerHandlerBatch(deactivatedUsers));
		}
	}

	/**
	 * Validates if correct User Locale is selected
	 */
	private void validateLocale() {
		for (User u : newUsers) {
			if (u.LocaleSidKey == 'en_US' || u.LocaleSidKey == 'en_CA') {
				u.LocaleSidKey.addError(
					'Locales English (United States), English (Canada) are blocked by VF NL because they cause invalid /// number formats.'
				);
			} else if (u.LocaleSidKey == 'nl') {
				u.LocaleSidKey.addError('Please select Dutch(Netherlands) for dutch locale.');
			}
		}
	}

	private void checkRelatedRecordsOnDeactivation() {
		Set<Id> userIdsToCheck = new Set<Id>();
		Set<Id> userIdsWithRecords = new Set<Id>();

		// find all deactivated users
		for (User u : newUsers) {
			if (u.IsActive == false && oldUsersMap.get(u.Id).IsActive == true) {
				userIdsToCheck.add(u.Id);
			}
		}

		Map<Id, List<Dealer_Information__c>> userIdToDealerInformation = new Map<Id, List<Dealer_Information__c>>();
		Map<Id, List<Account>> userIdToAccount = new Map<Id, List<Account>>();
		Map<Id, List<AccountTeamMember>> userIdToAccountTeamMember = new Map<Id, List<AccountTeamMember>>();
		Map<Id, List<Opportunity>> userIdToOpportunity = new Map<Id, List<Opportunity>>();
		Map<Id, List<Opportunity>> userIdToOpportunitySolutionSales = new Map<Id, List<Opportunity>>();
		Map<Id, List<Lead>> userIdToLead = new Map<Id, List<Lead>>();
		Map<Id, List<User>> userIdToChildUsers = new Map<Id, List<User>>();
		Map<Id, List<Dashboard>> userIdToDashboards = new Map<Id, List<Dashboard>>();

		if (!userIdsToCheck.isEmpty()) {
			for (Dealer_Information__c di : [
				SELECT Contact__r.UserId__c
				FROM Dealer_Information__c
				WHERE Contact__r.UserId__c IN :userIdsToCheck AND Is_Dealer_in_SFDC__c = TRUE
			]) {
				userIdsWithRecords.add(di.Contact__r.UserId__c);
				if (userIdToDealerInformation.containsKey(di.Contact__r.UserId__c)) {
					userIdToDealerInformation.get(di.Contact__r.UserId__c).add(di);
				} else {
					userIdToDealerInformation.put(di.Contact__r.UserId__c, new List<Dealer_Information__c>{ di });
				}
			}

			for (Account a : [
				SELECT OwnerId
				FROM Account
				WHERE OwnerId IN :userIdsToCheck OR Mobile_Dealer__r.Contact__r.UserId__c IN :userIdsToCheck
			]) {
				userIdsWithRecords.add(a.OwnerId);
				if (userIdToAccount.containsKey(a.OwnerId)) {
					userIdToAccount.get(a.OwnerId).add(a);
				} else {
					userIdToAccount.put(a.OwnerId, new List<Account>{ a });
				}
			}

			// partner account team roles
			//Map<Id,List<AccountTeamMember>> userIdToAccountTeamMember = new Map<Id,List<AccountTeamMember>>();
			for (AccountTeamMember atm : [
				SELECT AccountId, UserId, TeamMemberRole
				FROM AccountTeamMember
				WHERE UserId IN :userIdsToCheck AND Account.Type = 'Dealer'
			]) {
				userIdsWithRecords.add(atm.UserId);
				if (userIdToAccountTeamMember.containsKey(atm.UserId)) {
					userIdToAccountTeamMember.get(atm.UserId).add(atm);
				} else {
					userIdToAccountTeamMember.put(atm.UserId, new List<AccountTeamMember>{ atm });
				}
			}

			for (Opportunity o : [SELECT OwnerId FROM Opportunity WHERE OwnerId IN :userIdsToCheck AND IsClosed = FALSE]) {
				userIdsWithRecords.add(o.OwnerId);
				if (userIdToOpportunity.containsKey(o.OwnerId)) {
					userIdToOpportunity.get(o.OwnerId).add(o);
				} else {
					userIdToOpportunity.put(o.OwnerId, new List<Opportunity>{ o });
				}
			}

			for (Opportunity o : [SELECT Solution_Sales__c FROM Opportunity WHERE Solution_Sales__c IN :userIdsToCheck AND IsClosed = FALSE]) {
				userIdsWithRecords.add(o.Solution_Sales__c);
				if (userIdToOpportunitySolutionSales.containsKey(o.Solution_Sales__c)) {
					userIdToOpportunitySolutionSales.get(o.Solution_Sales__c).add(o);
				} else {
					userIdToOpportunitySolutionSales.put(o.Solution_Sales__c, new List<Opportunity>{ o });
				}
			}

			for (Lead l : [SELECT OwnerId FROM Lead WHERE OwnerId IN :userIdsToCheck AND IsClosed__c = FALSE AND isconverted = FALSE]) {
				userIdsWithRecords.add(l.OwnerId);
				if (userIdToLead.containsKey(l.OwnerId)) {
					userIdToLead.get(l.OwnerId).add(l);
				} else {
					userIdToLead.put(l.OwnerId, new List<Lead>{ l });
				}
			}

			for (User u : [SELECT Id, ManagerId FROM User WHERE ManagerId IN :userIdsToCheck AND IsActive = TRUE]) {
				userIdsWithRecords.add(u.ManagerId);
				if (userIdToChildUsers.containsKey(u.ManagerId)) {
					userIdToChildUsers.get(u.ManagerId).add(u);
				} else {
					userIdToChildUsers.put(u.ManagerId, new List<User>{ u });
				}
			}

			for (Dashboard d : [SELECT Id, RunningUserId FROM Dashboard WHERE RunningUserId IN :userIdsToCheck]) {
				userIdsWithRecords.add(d.RunningUserId);
				if (userIdToDashboards.containsKey(d.RunningUserId)) {
					userIdToDashboards.get(d.RunningUserId).add(d);
				} else {
					userIdToDashboards.put(d.RunningUserId, new List<Dashboard>{ d });
				}
			}
		}

		for (User u : newUsers) {
			// only go through the users that actually have data to be moved and raise errors for those.
			if (userIdsWithRecords.contains(u.Id)) {
				String link = '<a href="/apex/UserDeactivation?Id=' + u.Id + '" >user deactivation page</a>';

				if (userIdToDealerInformation.containsKey(u.Id)) {
					u.addError(
						'This user is the main contact for Dealer Information(s). Please go to the ' +
						link +
						' to deactivate this user.',
						false
					);
				}
				if (userIdToAccount.containsKey(u.Id)) {
					u.addError(
						'This user is the Account Owner for one or more accounts. Please go to the ' +
						link +
						' to deactivate this user.',
						false
					);
				}
				if (userIdToAccountTeamMember.containsKey(u.Id)) {
					u.addError(
						'This user is an Account Team Member for one or more Partner Accounts. Please go to the ' +
						link +
						' to deactivate this user.',
						false
					);
				}
				if (userIdToOpportunity.containsKey(u.Id)) {
					u.addError(
						'This user is the owner of one or more open Opportunities. Please go to the ' +
						link +
						' to deactivate this user.',
						false
					);
				}

				if (userIdToOpportunitySolutionSales.containsKey(u.ID)) {
					u.addError(
						'This user is the solution sales of one or more open Opportunities. Please go to the ' +
						link +
						' to deactivate this user.',
						false
					);
				}

				if (userIdToLead.containsKey(u.Id)) {
					u.addError('This user is the owner of one or more open Leads. Please go to the ' + link + ' to deactivate this user.', false);
				}
				if (userIdToChildUsers.containsKey(u.Id)) {
					u.addError('This user is the manager of one or more other users. Please go to the ' + link + ' to deactivate this user.', false);
				}
				if (userIdToDashboards.containsKey(u.Id)) {
					u.addError(
						'This user is the running user on one or more dashboards. Please go to the ' +
						link +
						' to deactivate this user.',
						false
					);
				}
			}
		}
	}

	private void checkLicenses() {
		// Sort users per account
		Map<Id, List<User>> usersPerAccountPerTrigger = new Map<Id, List<User>>();
		for (User u : newUsers) {
			if (oldUsersMap != null) {
				if (
					(u.AccountId != null ||
					oldUsersMap.get(u.Id).AccountId != null) &&
					GeneralUtils.IsPartnerUser(u) &&
					u.IsActive != oldUsersMap.get(u.Id).IsActive
				) {
					Id accountId = u.AccountId != null ? u.AccountId : oldUsersMap.get(u.Id).AccountId;
					if (!usersPerAccountPerTrigger.containsKey(accountId)) {
						usersPerAccountPerTrigger.put(accountId, new List<User>());
					}
					usersPerAccountPerTrigger.get(accountId).add(u);
				}
			} else if (u.AccountId != null && GeneralUtils.IsPartnerUser(u) && u.IsActive) {
				if (!usersPerAccountPerTrigger.containsKey(u.AccountId)) {
					usersPerAccountPerTrigger.put(u.AccountId, new List<User>());
				}
				usersPerAccountPerTrigger.get(u.AccountId).add(u);
			}
		}

		// Sort all active users per account to be able to check licenses
		Map<Id, List<User>> usersPerAccount = new Map<Id, List<User>>();
		if (!usersPerAccountPerTrigger.isEmpty()) {
			// TO DO: User just one query, instead of 2 : Account.Total_Licenses__c
			for (User u : [SELECT Id, AccountId FROM User WHERE AccountId IN :usersPerAccountPerTrigger.keySet() AND IsActive = :true]) {
				if (!usersPerAccount.containsKey(u.AccountId)) {
					usersPerAccount.put(u.AccountId, new List<User>());
				}
				usersPerAccount.get(u.AccountId).add(u);
			}

			// Check final amount of licenses
			List<Account> accountList = [SELECT Id, Total_Licenses__c FROM Account WHERE Id IN :usersPerAccountPerTrigger.keySet()];
			for (Account acc : accountList) {
				if (acc.Total_Licenses__c == null) {
					for (User u : usersPerAccountPerTrigger.get(acc.Id)) {
						u.addError(
							'There are no licenses defined. Either enter the total amount of licenses, or request these at your Vodafone partner manager'
						);
					}
				} else if (usersPerAccount.get(acc.Id) != null && acc.Total_Licenses__c < usersPerAccount.get(acc.Id).size()) {
					for (User u : usersPerAccountPerTrigger.get(acc.Id)) {
						if (u.Isactive) {
							u.addError(
								'There are no available licenses left. Either disable another user, or request more licenses at your Vodafone partner manager'
							);
						}
					}
				}
			}
			if (!usersPerAccountPerTrigger.keySet().isEmpty())
				updateAccounts(usersPerAccountPerTrigger.keySet());
		}

		//special check for disabling users though the portal
		Map<Id, List<User>> specialMap = new Map<Id, List<User>>();
		for (User u : newUsers) {
			if (oldUsersMap != null) {
				if (
					u.IsPortalEnabled == false &&
					oldUsersMap.get(u.Id).IsPortalEnabled == true &&
					u.IsActive &&
					GeneralUtils.IsPartnerUser(u) &&
					u.AccountId == oldUsersMap.get(u.Id).AccountId
				) {
					Id accountId = u.AccountId != null ? u.AccountId : oldUsersMap.get(u.Id).AccountId;
					if (!specialMap.containsKey(accountId)) {
						specialMap.put(accountId, new List<User>());
					}
					specialMap.get(accountId).add(u);
				}
			}
		}
		if (!specialMap.isEmpty()) {
			List<Account> specialList = [SELECT Id, Total_Licenses__c FROM Account WHERE Id IN :specialMap.keySet()];
			for (Account acc : specialList) {
				if (acc.Total_Licenses__c == null) {
					for (User u : specialMap.get(acc.Id)) {
						u.addError(
							'There are no licenses defined. Either enter the total amount of licenses, or request these at your Vodafone partner manager'
						);
					}
				}
			}
			Map<Id, Integer> specialMap2 = new Map<Id, Integer>();
			if (!specialMap.keySet().isEmpty()) {
				for (Id test : specialMap.keySet()) {
					specialMap2.put(test, specialMap.get(test).size());
				}
				specialUpdateAccounts(specialMap2);
			}
		}
	}

	@future
	private static void updateAccounts(Set<Id> accountIds) {
		List<Account> accountList = [SELECT Id, Used_Licenses__c FROM Account WHERE Id IN :accountIds];
		Map<Id, List<User>> usersPerAccount = new Map<Id, List<User>>();
		for (User u : [SELECT Id, AccountId FROM User WHERE AccountId IN :accountIds AND IsActive = :true]) {
			if (usersPerAccount.get(u.AccountId) == null) {
				usersPerAccount.put(u.AccountId, new List<User>());
			}
			usersPerAccount.get(u.AccountId).add(u);
		}
		for (Account acc : accountList) {
			if (usersPerAccount.get(acc.Id) != null) {
				acc.Used_Licenses__c = usersPerAccount.get(acc.Id).size();
			}
		}
		if (Test.isRunningTest()) {
		} else {
			update accountList;
		}
	}

	@future
	private static void specialUpdateAccounts(Map<Id, Integer> specialMap2) {
		List<Account> accountList = [SELECT Id, Used_Licenses__c FROM Account WHERE Id IN :specialMap2.keySet()];
		Map<Id, List<User>> usersPerAccount = new Map<Id, List<User>>();
		for (User u : [SELECT Id, AccountId FROM User WHERE AccountId IN :specialMap2.keySet() AND IsActive = :true]) {
			if (usersPerAccount.get(u.AccountId) == null) {
				usersPerAccount.put(u.AccountId, new List<User>());
			}
			usersPerAccount.get(u.AccountId).add(u);
		}
		for (Account acc : accountList) {
			acc.Used_Licenses__c = usersPerAccount.get(acc.Id).size() - specialMap2.get(acc.Id);
		}
		if (Test.isRunningTest()) {
			// start new context via system.runAs() for the same user for test code only
			System.runAs(new User(Id = Userinfo.getUserId())) {
				update accountList;
			}
		} else {
			// in non-test code insert normally
			update accountList;
		}
	}

	private void preventDisablePortalUser() {
		String currentProfile = GeneralUtils.currentUser.Profile.Name;
		for (User u : newUsers) {
			if (u.IsPortalEnabled == false && oldUsersMap.get(u.Id).IsPortalEnabled == true && currentProfile != 'System Administrator') {
				u.addError('You can\'t deactivate a user via this button.');
				u.IsPrmSuperUser = true;
			}
		}
	}

	/**
	 * Checks for Primary Partner User.
	 */
	private void checkPrimaryPartnerUser() {
		Set<Id> excludingIds = new Set<Id>();
		Map<Id, Integer> accountUsers = new Map<Id, Integer>();
		for (User u : newUsers) {
			if (u.AccountId != null) {
				excludingIds.add(u.Id);
				if (accountUsers.get(u.AccountId) == null) {
					accountUsers.put(u.AccountId, 0);
				}
				if (u.Primary_Partner__c == true) {
					accountUsers.put(u.AccountId, accountUsers.get(u.AccountId) + 1);
				}
			}
		}
		if (!accountUsers.isEmpty()) {
			for (User u : [
				SELECT AccountId
				FROM User
				WHERE accountId IN :accountUsers.keyset() AND Primary_Partner__c = :true AND Id NOT IN :excludingIds
			]) {
				accountUsers.put(u.AccountId, accountUsers.get(u.AccountId) + 1);
			}
		}
		for (User u : newUsers) {
			if (
				u.Primary_Partner__c &&
				accountUsers.get(u.AccountId) > 1 &&
				GeneralUtils.isRecordFieldChanged(u, oldUsersMap, 'Primary_Partner__c')
			) {
				u.addError('There can only be one Primary Partner User');
			}
		}
	}

	public static PermissionSet delegatedAdminPS {
		get {
			if (delegatedAdminPS == null)
				delegatedAdminPS = [SELECT Id FROM PermissionSet WHERE Name = :'Delegated_External_User_Administrator' LIMIT 1];
			return delegatedAdminPS;
		}
		private set;
	}

	private void assignPermissionSetToPrimaryPartnerUser() {
		Map<Id, PermissionSetAssignment> usersWithAssignment = new Map<Id, PermissionSetAssignment>();
		List<PermissionSetAssignment> insertAssignment = new List<PermissionSetAssignment>();
		List<PermissionSetAssignment> deleteAssignment = new List<PermissionSetAssignment>();

		for (PermissionSetAssignment psa : [SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId = :delegatedAdminPS.Id]) {
			usersWithAssignment.put(psa.AssigneeId, psa);
		}

		for (User u : newUsers) {
			//Update
			if (oldUsersMap != null) {
				if (u.Admin_Partner__c != oldUsersMap.get(u.Id).Admin_Partner__c) {
					if (u.Admin_Partner__c == true) {
						if (!usersWithAssignment.containsKey(u.Id)) {
							insertAssignment.add(new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = delegatedAdminPS.Id));
						}
					} else {
						if (usersWithAssignment.containsKey(u.Id)) {
							deleteAssignment.add(usersWithAssignment.get(u.Id));
						}
					}
				}
				//Insert
			} else {
				if (u.Admin_Partner__c == true) {
					if (!usersWithAssignment.containsKey(u.Id)) {
						insertAssignment.add(new PermissionSetAssignment(AssigneeId = u.Id, PermissionSetId = delegatedAdminPS.Id));
					}
				}
			}
		}
		insert insertAssignment;
		delete deleteAssignment;
	}

	private void prepareForCases() {
		Id recordType = GeneralUtils.recordTypeMap.get('Contact').get('Internal_Users');
		Map<Id, Contact> existingContacts = new Map<Id, Contact>();
		List<String> serializedContacts = new List<String>();
		for (Contact c : [
			SELECT Id, Userid__c, Firstname, LastName, Email, Title, Phone, MobilePhone, IsActive__c, ReportsToId
			FROM Contact
			WHERE accountId = :GeneralUtils.vodafoneAccount.Id
		]) {
			existingContacts.put(c.Userid__c, c);
		}
		for (User u : newUsersMap.values()) {
			// only for internal users
			if (!GeneralUtils.IsPartnerUser(u) && !GeneralUtils.IsPortalUser(u)) {
				// only for new user or if relevant data changed
				if (
					oldUsersMap == null ||
					(u.FirstName != oldUsersMap.get(u.Id).FirstName ||
					u.LastName != oldUsersMap.get(u.Id).LastName ||
					u.Email != oldUsersMap.get(u.Id).Email ||
					u.Title != oldUsersMap.get(u.Id).Title ||
					u.Phone != oldUsersMap.get(u.Id).Phone ||
					u.MobilePhone != oldUsersMap.get(u.Id).MobilePhone ||
					u.IsActive != oldUsersMap.get(u.Id).IsActive ||
					u.ManagerId != oldUsersMap.get(u.Id).ManagerId)
				) {
					Contact c;
					Boolean populate;
					ID ReportsToId = (u.ManagerId != null
						? (existingContacts.containsKey(u.ManagerId) ? existingContacts.get(u.ManagerId).id : null)
						: null);

					// Use existing contact if we have it and check if a value actually changed otherwise we are doing things we just dont need to do
					if (existingContacts.containsKey(u.Id)) {
						c = existingContacts.get(u.Id);
						if (
							u.FirstName != c.FirstName ||
							u.LastName != c.LastName ||
							u.Email != c.Email ||
							u.Title != c.Title ||
							u.Phone != c.Phone ||
							u.MobilePhone != c.MobilePhone ||
							u.IsActive != c.IsActive__c ||
							ReportsToId != c.ReportsToId
						) {
							populate = true;
						} else {
							populate = false;
						}
					} else {
						// Otherwise create a new contact
						c = new Contact();
						populate = true;
					}

					if (populate) {
						c.Userid__c = u.Id;
						c.Firstname = u.FirstName;
						c.LastName = u.LastName;
						c.AccountId = GeneralUtils.vodafoneAccount.Id;
						c.Email = u.Email;
						c.Title = u.Title;
						c.Phone = u.Phone;
						c.MobilePhone = u.MobilePhone;
						c.OwnerId = u.Id;
						c.IsActive__c = u.IsActive;
						c.Hidden_allow_update_contact__c = system.now();
						c.RecordTypeId = recordType;
						c.ReportsToId = ReportsToId;
						String s = JSON.serialize(c);
						serializedContacts.add(s);
					}
				}
			}
		}
		if (!serializedContacts.isEmpty()) {
			contactsForCases(serializedContacts);
		}
	}

	private static void updatePartnerContacts(List<String> serializedContacts) {
		if (system.isFuture() || System.isBatch()) {
			updatePartnerContactsOnline(serializedContacts);
		} else {
			updatePartnerContactsOffline(serializedContacts);
		}
	}

	// @future is used because User and Contact sObject can't be updated in a single transaction, during to Salesforce limitation
	@future
	private static void updatePartnerContactsOffline(List<String> serializedContacts) {
		updatePartnerContactsOnline(serializedContacts);
	}
	private static void updatePartnerContactsOnline(List<String> serializedContacts) {
		List<Contact> contacts = new List<Contact>();
		for (String s : serializedContacts) {
			Contact c = (Contact) JSON.deserialize(s, Contact.class);
			contacts.add(c);
		}

		List<Database.SaveResult> results = Database.update(contacts, false);
		List<Contact> failedContactsToUpdate = new List<Contact>();
		for (Database.SaveResult result : results) {
			if (!result.isSuccess()) {
				String errorMessage = '';
				for (Database.Error err : result.getErrors()) {
					errorMessage += err.getMessage() + ' ; ';
				}
				failedContactsToUpdate.add(new Contact(Id = result.getId(), Error_Message__c = errorMessage));
			}
		}

		if (!failedContactsToUpdate.isEmpty()) {
			String logMessage = '';
			results = Database.update(failedContactsToUpdate, false);
			for (Database.SaveResult result : results) {
				if (!result.isSuccess()) {
					logMessage += result.getId() + ' = ';
					for (Database.Error err : result.getErrors()) {
						logMessage += err.getMessage() + '; ';
					}
				}
			}
			if (String.isNotBlank(logMessage)) {
				LoggerService.log(logMessage);
			}
		}
	}

	/**
	 * @description     Method to create or update Contacts from Users under the Vodafone NL Account for case management
	 */
	private static void contactsForCases(List<String> serializedContacts) {
		if (system.isFuture() || System.isBatch()) {
			contactsForCasesOnline(serializedContacts);
		} else {
			contactsForCasesOffline(serializedContacts);
		}
	}

	@future
	private static void contactsForCasesOffline(List<String> serializedContacts) {
		contactsForCasesOnline(serializedContacts);
	}
	private static void contactsForCasesOnline(List<String> serializedContacts) {
		List<Contact> contacts = new List<Contact>();
		for (String s : serializedContacts) {
			Contact c = (Contact) JSON.deserialize(s, Contact.class);
			contacts.add(c);
		}
		/**This will fail if a contact is created and then updated for test purposes
		 because the method is future*/

		Database.upsert(contacts, false);
	}

	private void cleanupPartnerQueues() {
		Set<Id> usersToRemoveFromQueue = new Set<Id>();
		List<Id> groupMembersToRemove = new List<Id>();
		for (User u : newUsers) {
			if (u.IsActive == false && oldUsersMap.get(u.Id).IsActive == true && u.UserType == 'PowerPartner') {
				usersToRemoveFromQueue.add(u.Id);
			}
		}
		if (!usersToRemoveFromQueue.isEmpty()) {
			groupMembersToRemove.addAll(
				new Map<Id, GroupMember>([SELECT Id FROM GroupMember WHERE UserOrGroupId IN :usersToRemoveFromQueue]).keySet()
			);
			Database.delete(groupMembersToRemove, false);
		}
	}

	private void removePermissionSetsFromInactiveUsers() {
		Map<Id, List<PermissionSetAssignment>> permissionSetAssignmentMap = new Map<Id, List<PermissionSetAssignment>>();
		List<PermissionSetAssignment> deleteAssignmentList = new List<PermissionSetAssignment>();

		List<User> usersToDeactivate = new List<User>();
		for (User user : newUsersMap.values()) {
			if (user.Profile_Name__c == Constants.PROFILE_D2D_PARTNER_SALES && !user.IsActive) {
				usersToDeactivate.add(user);
			}
		}

		if (usersToDeactivate.isEmpty()) {
			return;
		}
		for (PermissionSetAssignment permissionSetAssignment : [
			SELECT AssigneeId
			FROM PermissionSetAssignment
			WHERE
				Assignee.isActive = FALSE
				AND AssigneeId IN :newUsersMap.keySet()
				AND PermissionSetId IN (SELECT Id FROM PermissionSet WHERE IsOwnedByProfile = FALSE)
		]) {
			if (permissionSetAssignmentMap.containsKey(permissionSetAssignment.AssigneeId)) {
				permissionSetAssignmentMap.get(permissionSetAssignment.AssigneeId).add(permissionSetAssignment);
			} else {
				permissionSetAssignmentMap.put(permissionSetAssignment.AssigneeId, new List<PermissionSetAssignment>{ permissionSetAssignment });
			}
		}

		for (User user : usersToDeactivate) {
			if (user.Profile_Name__c == Constants.PROFILE_D2D_PARTNER_SALES) {
				List<PermissionSetAssignment> permissionSetAssignemtList = permissionSetAssignmentMap.get(user.Id);
				if (permissionSetAssignemtList != null) {
					for (PermissionSetAssignment permissionSetAssignment : permissionSetAssignemtList) {
						if (!user.IsActive) {
							deleteAssignmentList.add(permissionSetAssignment);
						}
					}
				}
			}
		}

		if (deleteAssignmentList.size() > 0) {
			delete deleteAssignmentList;
		}
	}

	private void assignPermSetLicsForD2D() {
		List<PermissionSetLicense> permSetLicences = [SELECT Id FROM PermissionSetLicense WHERE MasterLabel = 'Salesforce Maps'];
		Map<Id, PermissionSetLicenseAssign> permissionSetLicenseAssignment = new Map<Id, PermissionSetLicenseAssign>();
		List<PermissionSetLicenseAssign> insertAssignments = new List<PermissionSetLicenseAssign>();
		List<PermissionSetLicenseAssign> deleteAssignments = new List<PermissionSetLicenseAssign>();

		for (PermissionSetLicenseAssign psla : [
			SELECT AssigneeId, PermissionSetLicenseId
			FROM PermissionSetLicenseAssign
			WHERE AssigneeId IN :newUsersMap.keySet() AND PermissionSetLicenseId = :permSetLicences[0].Id
		]) {
			permissionSetLicenseAssignment.put(psla.AssigneeId, psla);
		}

		for (User u : newUsersMap.values()) {
			if (u.Profile_Name__c == 'LG_NL D2D_Partner Sales User') {
				for (PermissionSetLicense psl : permSetLicences) {
					if (u.IsActive && !permissionSetLicenseAssignment.containsKey(u.Id)) {
						insertAssignments.add(new PermissionSetLicenseAssign(AssigneeId = u.Id, PermissionSetLicenseId = psl.Id));
					} else if (!u.IsActive && permissionSetLicenseAssignment.containsKey(u.Id)) {
						deleteAssignments.add(permissionSetLicenseAssignment.get(u.Id));
					}
				}
			}
		}

		if (insertAssignments.size() > 0) {
			insert insertAssignments;
		}

		if (deleteAssignments.size() > 0) {
			delete deleteAssignments;
		}
	}

	private void assignPermissionSetsAndGroups() {
		List<User> usersToProcess = GeneralUtils.filterChangedRecords(newUsers, oldUsersMap, new List<String>{ 'ProfileId' }, false);
		if (usersToProcess.isEmpty()) {
			return;
		}
		UserService usvc = new UserService(new Map<Id, User>(usersToProcess).keySet());
		usvc.assignPermissionSets();
		usvc.assignPermissionSetGroups();
	}

	/**
	 * Sets default values for Ziggo Partner Sales users (Sender Email and Company)
	 */
	private void setZiggoPartnerUserDefaults() {
		List<User> ziggoPartnerUsers = new List<User>();
		Set<Id> roleIds = new Set<Id>();
		for (User u : newUsers) {
			if (u.IsActive && u.Profile_Name__c == Constants.PROFILE_D2D_PARTNER_SALES) {
				ziggoPartnerUsers.add(u);
				roleIds.add(u.UserRoleId);
			}
		}
		if (ziggoPartnerUsers.isEmpty()) {
			return;
		}

		OrgWideEmailAddress ziggoZakelijkEmail = EmailService.getOrgWideEmailAddress('zakelijk@ziggo.com');

		Map<Id, UserRole> roles = new Map<Id, UserRole>([SELECT Id, Name FROM UserRole WHERE Id IN :roleIds]);

		Map<String, Decimal> mapViewDefaultSettings = getMapViewDefaultSettings();
		for (User u : ziggoPartnerUsers) {
			u.LG_SalesChannel__c = 'D2D';
			u.SenderEmail = ziggoZakelijkEmail.Address;
			u.CompanyName = Partner_User_Role_Company_Mapping__c.getValues(roles.get(u.UserRoleId)?.Name)?.Company__c;
			// Setting default view to The Netherlands on Salesforce Maps https://jiranl.vodafoneziggo.com/browse/SFLO-312
			u.maps__DefaultLatitude__c = mapViewDefaultSettings.get('latitude');
			u.maps__DefaultLongitude__c = mapViewDefaultSettings.get('longitude');
			u.maps__DefaultZoomLevel__c = mapViewDefaultSettings.get('zoom');
		}
	}

	private Map<String, Decimal> getMapViewDefaultSettings() {
		Map<String, Decimal> mapViewDefaultSettings = new Map<String, Decimal>();
		mapViewDefaultSettings.put('zoom', Decimal.valueOf(maps__MapsSetting__c.getValues('NL_Default_Zoom_Level').maps__Value__c));
		mapViewDefaultSettings.put('latitude', Decimal.valueOf(maps__MapsSetting__c.getValues('NL_Default_Latitude').maps__Value__c));
		mapViewDefaultSettings.put('longitude', Decimal.valueOf(maps__MapsSetting__c.getValues('NL_Default_Longitude').maps__Value__c));
		return mapViewDefaultSettings;
	}

	private void reassignAccountUserQueueToPartnerManager() {
		Map<Id, Id> managerIdByUser = new Map<Id, Id>();
		for (User u : newUsers) {
			User ou = oldUsersMap.get(u.Id);
			// add user to map if IsActive field value has been changed from True to False AND Profile is PROFILE_D2D_PARTNER_SALES
			if (!u.IsActive && ou.IsActive && u.ManagerId != null && u.Profile_Name__c == Constants.PROFILE_D2D_PARTNER_SALES) {
				managerIdByUser.put(u.Id, u.ManagerId);
			}
		}
		if (!managerIdByUser.isEmpty()) {
			System.enqueueJob(new AccountQueueUserSharingBatch(managerIdByUser));
		}
	}

	/**
	 * @description Internal and partner contacts should have user records connected to them.
	 *  For internal contacts, this connection is made by the support team from a contact record
	 *  to user record via UserId__c field. For partner contacts, this connection is made by
	 *  Salesforce from User record to a contact record via ContactId field.
	 *
	 *  Business wants to ensure that you can't update Phone, MobilePhone, Email, FirstName or LastName
	 *  on contacts connected to a user record. Changes on the User record should automatically be
	 *  reflected on the Contact.
	 *
	 *  This method makes sure that for newly created partner users the link is made between the contact
	 *  and a user record and that changes to certain fields on a user record that has a contact record
	 *  associated to it, are reflected there as well.
	 **/
	private void updateConnectedContactRecords() {
		List<Id> contactIds = new List<Id>();
		List<Id> userIds = new List<Id>();
		List<User> relevantUsers = new List<User>();

		for (User user : newUsers) {
			Boolean isPartnerUser = GeneralUtils.IsPartnerUser(user);
			Boolean changesDetected = GeneralUtils.isRecordFieldChanged(
				user,
				oldUsersMap,
				new List<String>{ 'Phone', 'MobilePhone', 'Email', 'FirstName', 'LastName' },
				false
			);
			if (oldUsersMap == null && isPartnerUser) {
				contactIds.add(user.ContactId);
				relevantUsers.add(user);
			} else if (changesDetected && isPartnerUser) {
				contactIds.add(user.ContactId);
				relevantUsers.add(user);
			} else if (changesDetected) {
				userIds.add(user.Id);
				relevantUsers.add(user);
			}
		}

		if (relevantUsers.isEmpty()) {
			return;
		}

		Map<Id, Contact> contactIdToContactRecordMap = new Map<Id, Contact>(
			[SELECT Phone, MobilePhone, Email, FirstName, LastName, UserId__c FROM Contact WHERE UserId__c IN :userIds OR Id IN :contactIds]
		);
		Map<Id, Contact> userIdToContactRecordMap = new Map<Id, Contact>();
		for (Contact contact : contactIdToContactRecordMap.values()) {
			if (contact.UserId__c == null) {
				continue;
			}
			Id userId = Id.valueOf(contact.UserId__c);
			userIdToContactRecordMap.put(userId, contact);
		}

		List<String> serializedContactsToUpdate = new List<String>();
		for (User user : relevantUsers) {
			Contact relatedContact;
			if (user.ContactId != null) {
				relatedContact = contactIdToContactRecordMap.get(user.ContactId);
			} else {
				relatedContact = userIdToContactRecordMap.get(user.Id);
			}

			if (relatedContact == null) {
				continue;
			}

			if (
				relatedContact.Phone != user.Phone ||
				relatedContact.MobilePhone != user.MobilePhone ||
				relatedContact.Email != user.Email ||
				relatedContact.FirstName != user.FirstName ||
				relatedContact.LastName != user.LastName ||
				relatedContact.Userid__c != user.Id
			) {
				relatedContact.Phone = user.Phone;
				relatedContact.MobilePhone = user.MobilePhone;
				relatedContact.Email = user.Email;
				relatedContact.FirstName = user.FirstName;
				relatedContact.LastName = user.LastName;
				relatedContact.Hidden_allow_update_contact__c = system.now();
				relatedContact.Userid__c = user.Id;

				String serializedContact = JSON.serialize(relatedContact);
				serializedContactsToUpdate.add(serializedContact);
			}
		}

		if (!serializedContactsToUpdate.isEmpty()) {
			updatePartnerContacts(serializedContactsToUpdate);
		}
	}
}
