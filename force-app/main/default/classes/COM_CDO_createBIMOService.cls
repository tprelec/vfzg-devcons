public with sharing class COM_CDO_createBIMOService {
	/*
	@TestVisible
	public static final String COM_CDO_INTEGRATION_SETTING_NAME = 'createBIMOService';

	private static final String MOCKSYNCSUCCESS = '{ "id":"b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "href":"https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId":"COM", "priority":"1", "description":"Internet Modem Only Service Order", "category":"Internet Modem Only Service", "state":"acknowledged", "orderDate":"2022-02-09T10:37:15.97221Z", "@type":"standard", "orderItem":[ { "id":"1", "action":"add", "state":"acknowledged", "@type":"standard", "service":{ "id":"4ed3dece-d5bd-442f-a1f9-687c0a1c52221", "serviceCharacteristic":[ { "name":"sourcesystem", "valueType":"string", "value":{ "@type":"string", "sourcesystem":"SF" } }, { "name":"tenantId", "valueType":"string", "value":{ "@type":"string", "tenantId":"Some Tenant Id" } }, { "name":"accessL1PostalCode", "valueType":"string", "value":{ "@type":"string", "accessL1PostalCode":"123456" } }, { "name":"accessL1HouseNumber", "valueType":"integer", "value":{ "@type":"integer", "accessL1HouseNumber":"101" } }, { "name":"accessL1HouseNumberExt", "valueType":"string", "value":{ "@type":"string", "accessL1HouseNumberExt":"9" } }, { "name":"accessL1Article", "valueType":"string", "value":{ "@type":"string", "accessL1Article":"HFC" } }, { "name":"circuitL2Article", "valueType":"string", "value":{ "@type":"string", "circuitL2Article":"int_l2_zz1" } }, { "name":"circuitL2Quality", "valueType":"string", "value":{ "@type":"string", "circuitL2Quality":"entry" } }, { "name":"internetL3Article", "valueType":"string", "value":{ "@type":"string", "internetL3Article":"int_service_l3" } }, { "name":"ipv4BlockIPPool", "valueType":"string", "value":{ "@type":"string", "ipv4BlockIPPool":"/32" } } ] }, "serviceSpecification":{ "id":"bdab731e-d5de-4135-8849-5c724ea01041", "invariantID":"930d7161-0eb8-444f-8d11-5971812246d5", "version":"1.0.0", "name":"Internet Modem Only Service", "@type":"ServiceSpecification" } } ] }';
	private static final String MOCKSYNCFAILURE = '{ "code": 40020, "reason": "SERVICE_ORDER_CHARACTERISTICS_VALIDATION_ERROR", "message": "Request validation has failed due the following issue(s) - [ Input parameter \'accessL1Article\' violates constraint, Invalid value ]" }';

	public static final String COM_CDO_INTEGRATION_FAILED_STATUS = 'Failure';
	public static final String COM_CDO_INTEGRATION_COMPLETE_STATUS = 'Complete';

	private static final String ACTION_CREATE = 'add';
	private static final String ACTION_MODIFY = 'modify';

	public static final String MOCK_CREATE_BIMO_SYNC_FAILED = 'COM_CDO_Create_BIMO_Service_Sync_Resp_Failure';
	public static final String MOCK_CREATE_BIMO_SYNC_SUCCESS = 'COM_CDO_Create_BIMO_Service_Sync_Resp_Success';

	public static COM_MetadataInformation metadata;

	public static String generateBIMOService(Id deliveryOrderId, Boolean isModify) {
		List<csord__Service__c> servicesFromOrder = COM_CDOHelper.getServicesFromDeliveryOrder(
			deliveryOrderId
		);

		COM_CDO_BIMOService request = getRequest(deliveryOrderId, servicesFromOrder, isModify);

		RequestGenerateData requestData = new RequestGenerateData();
		requestData.deliveryOrderId = deliveryOrderId;
		requestData.isModify = isModify;
		requestData.request = request;
		requestData.servicesList = servicesFromOrder;

		String result = createBIMOService(requestData);
		return result;
	}

	public static String createBIMOService(Id deliveryOrderId) {
		String result = generateBIMOService(deliveryOrderId, false);
		return result;
	}

	private static String createBIMOService(RequestGenerateData requestData) {
		COM_Integration_setting__mdt createBIMOServiceSetting = requestData.isModify
			? COM_Integration_setting__mdt.getInstance(COM_CDO_INTEGRATION_SETTING_NAME)
			: COM_Integration_setting__mdt.getInstance(
					COM_CDOmodifyBIMOService.COM_CDO_INTEGRATION_SETTING_NAME
			  );

		Boolean doNotExecuteReq = createBIMOServiceSetting != null
			? createBIMOServiceSetting.Execute_mock__c
			: true;
		Boolean doNotExecuteStatus = createBIMOServiceSetting != null
			? createBIMOServiceSetting.Mock_Execution_result__c
			: true;

		HTTPResponse response = sendCreateBIMOServiceRequest(
			requestData.request,
			doNotExecuteReq,
			doNotExecuteStatus
		);

		String result = '';

		//if failed response
		if (failedResponse(response)) {
			CreateBIMOServiceSyncErrorResponse responseDeserialized = (CreateBIMOServiceSyncErrorResponse) JSON.deserializeStrict(
				response.getBody(),
				CreateBIMOServiceSyncErrorResponse.class
			);

			COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
			updates.deliveryOrderId = requestData.deliveryOrderId;
			updates.status = COM_CDO_INTEGRATION_FAILED_STATUS;
			updates.errorMessage = responseDeserialized.message;
			updates.success = false;

			COM_CDOHelper.performOrderUpdatesAfterResponse(updates, null);
		} else {
			String reqBody = COM_CDOServiceCharacteristicHelper.modifyRequestBody(
				response.getBody()
			);

			result = reqBody;
			COM_CDO_BIMOService body = (COM_CDO_BIMOService) JSON.deserializeStrict(
				reqBody,
				COM_CDO_BIMOService.class
			);

			COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
			updates.deliveryOrderId = requestData.deliveryOrderId;
			updates.status = '';
			updates.errorMessage = '';
			updates.success = true;
			COM_CDOHelper.performOrderUpdatesAfterResponse(updates, body);

			String umbrellaServiceId = COM_CDOHelper.getUmbrellaServiceId(body);
			COM_CDOHelper.updateServices(requestData.servicesList, umbrellaServiceId);
		}

		return result;
	}

	private static Boolean failedResponse(HTTPResponse response) {
		Boolean result = false;
		try {
			CreateBIMOServiceSyncErrorResponse responseDeserialized = (CreateBIMOServiceSyncErrorResponse) JSON.deserializeStrict(
				response.getBody(),
				CreateBIMOServiceSyncErrorResponse.class
			);
			if (responseDeserialized.code != null) {
				result = true;
			}
		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	private static HTTPResponse sendCreateBIMOServiceRequest(
		COM_CDO_BIMOService req,
		Boolean doNotExecute,
		Boolean doNotExecuteStatus
	) {
		HttpRequest reqData = new HttpRequest();
		Http http = new Http();
		reqData.setEndpoint('callout:CDO_createTenant?format=json');

		String requestBody = COM_CDO_createBIMOService.generatePayloadJson(req);

		System.debug(LoggingLevel.DEBUG, 'REQUEST==' + requestBody);
		reqData.setBody(requestBody);
		reqData.setMethod('POST');
		System.debug(LoggingLevel.DEBUG, '@@@@ reqData: ' + reqData);
		HTTPResponse response = new HttpResponse();
		if (doNotExecute && !Test.isRunningTest()) {
			response.setHeader('Content-Type', 'application/json');
			if (doNotExecuteStatus) {
				String mockSuccess = req.orderItem[0].action == ACTION_CREATE
					? COM_CDOHelper.getMockResponse(MOCK_CREATE_BIMO_SYNC_SUCCESS)
					: COM_CDOHelper.getMockResponse(
							COM_CDOmodifyBIMOService.MOCK_MODIFY_BIMO_SYNC_SUCCESS
					  );
				mockSuccess = mockSuccess != null ? mockSuccess : MOCKSYNCSUCCESS;
				response.setBody(mockSuccess);
				response.setStatusCode(200);
			} else {
				String mockFailed = req.orderItem[0].action == ACTION_CREATE
					? COM_CDOHelper.getMockResponse(MOCK_CREATE_BIMO_SYNC_FAILED)
					: COM_CDOHelper.getMockResponse(
							COM_CDOmodifyBIMOService.MOCK_MODIFY_BIMO_SYNC_FAILED
					  );
				mockFailed = mockFailed != null ? mockFailed : MOCKSYNCFAILURE;
				response.setBody(mockFailed);

				response.setStatusCode(400);
			}
		} else {
			response = http.send(reqData);
		}

		System.debug(LoggingLevel.DEBUG, '@@@@ response: ' + response.getBody());
		return response;
	}

	private static COM_CDO_BIMOService getRequest(
		Id deliveryOrderId,
		List<csord__Service__c> servicesFromOrder,
		Boolean isModify
	) {
		COM_CDO_integration_settings__mdt comSetting = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_INTEGRATION_SETTING_NAME
		);

		COM_CDO_BIMOService request = new COM_CDO_BIMOService();

		request.externalId = comSetting.externalId__c;
		request.priority = comSetting.priority__c;
		request.description = comSetting.Description__c;
		request.category = comSetting.Category__c;
		request.typeVal = comSetting.requestType__c;
		request.orderItem = new List<COM_CDO_BIMOService.OrderItem>();

		COM_CDO_BIMOService.OrderItem orderitem = new COM_CDO_BIMOService.OrderItem();
		orderItem.id = '1';
		orderItem.action = isModify ? ACTION_MODIFY : ACTION_CREATE;
		orderItem.typeVal = 'standard';

		COM_CDO_BIMOService.Service service = new COM_CDO_BIMOService.Service();

		COM_CDO_BIMOService.ServiceSpecification specification = new COM_CDO_BIMOService.ServiceSpecification();
		specification.id = comSetting.serviceSpecificationId__c;
		specification.invariantID = comSetting.serviceSpecification_invariantID__c;
		specification.version = comSetting.serviceSpecification_version__c;
		specification.name = comSetting.serviceSpecification_name__c;
		specification.typeVal = 'ServiceSpecification';
		service.serviceSpecification = specification;

		COM_CDO_BIMOService.ServiceCharacteristic serviceCharacteristic = new COM_CDO_BIMOService.ServiceCharacteristic();
		serviceCharacteristic.name = 'sourcesystem';
		serviceCharacteristic.valueType = 'string';

		COM_CDO_BIMOService.GenValue genValue = new COM_CDO_BIMOService.GenValue();
		genValue.typeVal = 'string';
		genValue.valueOf = 'SF';
		serviceCharacteristic.value = genValue;

		service.id = isModify ? COM_CDOHelper.getUmbrellaServices(deliveryOrderId)[0] : null;

		service.serviceCharacteristic = new List<COM_CDO_BIMOService.ServiceCharacteristic>();
		service.serviceCharacteristic.add(serviceCharacteristic);
		service.serviceCharacteristic.addAll(
			COM_CDOServiceCharacteristicHelper.getServiceCharacteristics(servicesFromOrder)
		);
		orderItem.service = service;
		request.orderItem.add(orderItem);

		return request;
	}

	private static String generatePayloadJson(COM_CDO_BIMOService req) {
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeStringField('externalId', req.externalId);
		gen.writeStringField('priority', req.priority);
		gen.writeStringField('description', req.description);
		gen.writeStringField('category', req.category);
		gen.writeStringField('@type', req.typeVal);
		gen.writeFieldName('orderItem');
		gen.writeStartArray();
		for (COM_CDO_BIMOService.OrderItem orderItem : req.orderItem) {
			gen.writeStartObject();
			gen.writeStringField('id', orderItem.id);
			gen.writeStringField('action', orderItem.action);
			gen.writeStringField('@type', orderItem.typeVal);
			gen.writeFieldName('service');
			gen.writeStartObject();

			if (orderItem.service.id != null) {
				gen.writeStringField('id', orderItem.service.id);
			}

			gen.writeStringField('@type', req.typeVal);
			gen.writeFieldName('serviceSpecification');
			gen.writeStartObject();
			gen.writeStringField('id', orderItem.service.serviceSpecification.id);
			gen.writeStringField('invariantID', orderItem.service.serviceSpecification.invariantID);
			gen.writeStringField('version', orderItem.service.serviceSpecification.version);
			gen.writeStringField('name', orderItem.service.serviceSpecification.name);
			gen.writeStringField('@type', orderItem.service.serviceSpecification.typeVal);
			gen.writeEndObject();
			gen.writeFieldName('serviceCharacteristic');
			gen.writeStartArray();
			for (
				COM_CDO_BIMOService.ServiceCharacteristic serviceCharacteristic : orderItem.service.serviceCharacteristic
			) {
				gen.writeStartObject();
				gen.writeStringField('name', serviceCharacteristic.name);
				gen.writeStringField('valueType', serviceCharacteristic.valueType);
				gen.writeFieldName('value');
				gen.writeStartObject();
				gen.writeStringField('@type', serviceCharacteristic.value.typeVal);
				String value = serviceCharacteristic.value.valueOf != null
					? serviceCharacteristic.value.valueOf
					: '';
				gen.writeStringField(serviceCharacteristic.name, value);
				gen.writeEndObject();
				gen.writeEndObject();
			}
			gen.writeEndArray();
			gen.writeEndObject();
		}
		gen.writeEndObject();
		gen.writeEndArray();
		gen.writeEndObject();

		return gen.getAsString();
	}

	private class CreateBIMOServiceSyncErrorResponse {
		private Integer code;
		private String reason;
		private String message;
	}

	private class RequestGenerateData {
		private COM_CDO_BIMOService request;
		private Id deliveryOrderId;
		private List<csord__Service__c> servicesList;
		private Boolean isModify;
	}
	*/
}
