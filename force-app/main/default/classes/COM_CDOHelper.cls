public with sharing class COM_CDOHelper {
	public static Map<String, List<csord__Service__c>> getAllUmbrellaServices(List<csord__Service__c> services) {
		Map<String, List<csord__Service__c>> result = new Map<String, List<csord__Service__c>>();

		for (csord__Service__c service : services) {
			if (service.CDO_Umbrella_Service_Id__c != null && service.CDO_Umbrella_Service_Id__c != '') {
				if (result.containsKey(service.CDO_Umbrella_Service_Id__c)) {
					result.get(service.CDO_Umbrella_Service_Id__c).add(service);
				} else {
					List<csord__Service__c> servicesInList = new List<csord__Service__c>();
					servicesInList.add(service);
					result.put(service.CDO_Umbrella_Service_Id__c, servicesInList);
				}
			} else {
				if (result.containsKey(COM_CDO_Constants.COM_CDO_NEW_CONS)) {
					result.get(COM_CDO_Constants.COM_CDO_NEW_CONS).add(service);
				} else {
					List<csord__Service__c> servicesInList = new List<csord__Service__c>();
					servicesInList.add(service);
					result.put(COM_CDO_Constants.COM_CDO_NEW_CONS, servicesInList);
				}
			}
		}

		return result;
	}

	public static String getServiceOrderId(COM_CDO_ServiceOrder response) {
		String result = '';
		result = response.id;
		return result;
	}

	public static String getUmbrellaServiceId(COM_CDO_ServiceOrder response) {
		String result = '';
		for (COM_CDO_ServiceOrder.OrderItem orderItem : response.orderItem) {
			result = orderItem.service.id;
		}
		return result;
	}

	public static Id getDeliveryOrderPerServiceRequestOrder(String requestOrderId) {
		Id result;
		COM_Delivery_Order__c order = [SELECT Id FROM COM_Delivery_Order__c WHERE CDO_Service_Order_Request_Id__c = :requestOrderId];

		if (order != null) {
			result = order.Id;
		}

		return result;
	}

	public static void updateServices(List<csord__Service__c> services, String umbrellaServiceId) {
		List<csord__Service__c> servicesUpdate = new List<csord__Service__c>();

		for (csord__Service__c service : services) {
			service.CDO_Umbrella_Service_Id__c = umbrellaServiceId;
			servicesUpdate.add(service);
		}

		update servicesUpdate;
	}

	private static void updateDeliveryOrder(DeliveryOrderUpdates updates) {
		COM_Delivery_Order__c deliveryOrder = new COM_Delivery_Order__c();
		deliveryOrder.Id = updates.deliveryOrderId;

		if (updates.success) {
			deliveryOrder.CDO_Service_Order_Request_Id__c = updates.serviceOrderId;
			deliveryOrder.CDO_Error_Message__c = '';
		} else {
			deliveryOrder.CDO_Error_Message__c = updates.errorMessage;
		}
		deliveryOrder.CDO_Integration_Status__c = updates.status;

		try {
			update deliveryOrder;
		} catch (Exception eee) {
			LoggerService.log(LoggingLevel.DEBUG, 'Error in CDO Delivery order update processing: : ' + eee.getMessage());
		}
	}

	public static List<csord__Service__c> getServicesFromDeliveryOrder(Id orderId) {
		List<csord__Service__c> result = new List<csord__Service__c>();

		result = [
			SELECT
				Id,
				VFZ_Commercial_Article_Name__c,
				VFZ_Commercial_Component_Name__c,
				Site__c,
				CDO_Umbrella_Service_Id__c,
				OSS10_Service_Id__c,
				csordtelcoa__Cancelled_By_Change_Process__c
			FROM csord__Service__c
			WHERE COM_Delivery_Order__c = :orderId
		];

		return result;
	}

	public static List<csord__Service__c> getServicesPerUmbrellaService(String umbrellaService) {
		List<csord__Service__c> result = new List<csord__Service__c>();

		result = [
			SELECT
				Id,
				VFZ_Commercial_Article_Name__c,
				VFZ_Commercial_Component_Name__c,
				Site__c,
				CDO_Umbrella_Service_Id__c,
				OSS10_Service_Id__c,
				csordtelcoa__Cancelled_By_Change_Process__c
			FROM csord__Service__c
			WHERE CDO_Umbrella_Service_Id__c = :umbrellaService
		];

		return result;
	}

	public static List<String> getUmbrellaServices(Id deliveryOrderId) {
		List<String> result = new List<String>();
		Set<String> umbrellaServices = new Set<String>();

		List<csord__Service__c> services = getServicesFromDeliveryOrder(deliveryOrderId);

		for (csord__Service__c service : services) {
			if (service.CDO_Umbrella_Service_Id__c != null) {
				umbrellaServices.add(service.CDO_Umbrella_Service_Id__c);
			}
		}

		result = new List<String>(umbrellaServices);

		return result;
	}

	public static List<csord__Service__c> retrievePartialMACDServices(List<csord__Service__c> deliveryOrderServices) {
		List<csord__Service__c> result = new List<csord__Service__c>();
		for (csord__Service__c service : deliveryOrderServices) {
			if (service.csordtelcoa__Cancelled_By_Change_Process__c == false) {
				result.add(service);
			}
		}

		Set<String> umbrellaServices = getUmbrellaServiceIds(result);

		if (umbrellaServices != null) {
			result.addAll(getServicesPerUmbrellaService(umbrellaServices));
		}
		return result;
	}

	private static Set<String> getUmbrellaServiceIds(List<csord__Service__c> services) {
		Set<String> result = new Set<String>();

		for (csord__Service__c service : services) {
			if (service.CDO_Umbrella_Service_Id__c != null) {
				result.add(service.CDO_Umbrella_Service_Id__c);
			}
		}

		return result;
	}
	public static void performOrderUpdatesAfterResponse(DeliveryOrderUpdates updates, COM_CDO_ServiceOrder body) {
		if (body != null) {
			String serviceOrderId = getServiceOrderId(body);
			updates.serviceOrderId = serviceOrderId;
			if (updates.deliveryOrderId == null) {
				updates.deliveryOrderId = getDeliveryOrderPerServiceRequestOrder(serviceOrderId);
			}
		}
		updateDeliveryOrder(updates);
	}

	private static List<csord__Service__c> getServicesPerUmbrellaService(Set<String> umbrellaServices) {
		List<csord__Service__c> result = new List<csord__Service__c>();

		result = [
			SELECT
				Id,
				VFZ_Commercial_Article_Name__c,
				VFZ_Commercial_Component_Name__c,
				Site__c,
				CDO_Umbrella_Service_Id__c,
				OSS10_Service_Id__c,
				csordtelcoa__Cancelled_By_Change_Process__c
			FROM csord__Service__c
			WHERE
				CDO_Umbrella_Service_Id__c IN :umbrellaServices
				AND csordtelcoa__Cancelled_By_Change_Process__c = FALSE
				AND csordtelcoa__Replacement_Service__c = NULL
				AND csord__Status__c = 'Active'
		];

		return result;
	}

	public static String getMockResponse(String resourceName, String settingName) {
		String result = null;
		try {
			StaticResource sr = [SELECT id, body FROM StaticResource WHERE Name = :resourceName];
			result = sr != null ? sr.body.toString() : '';
			result = result != '' ? modifyMockResponsePerSettings(result, settingName) : '';
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	private static String modifyMockResponsePerSettings(String rawResponse, String settingName) {
		String regExpression = '"serviceSpecification"[^}]+[}]';
		String replacement = generateServiceSpecification(settingName);
		String result = rawResponse.replaceAll(regExpression, replacement);

		return result;
	}

	private static String generateServiceSpecification(String settingName) {
		String result = '';

		COM_CDO_integration_settings__mdt comSetting = COM_CDO_integration_settings__mdt.getInstance(settingName);

		result += '"' + COM_CDO_Constants.COM_CDO_SERVICE_SPECIFICATION + '":{';
		result += generateLine(COM_CDO_Constants.COM_CDO_ID, comSetting.serviceSpecificationId__c) + ',';
		result += generateLine(COM_CDO_Constants.COM_CDO_INVARIANTID, comSetting.serviceSpecification_invariantID__c) + ',';
		result += generateLine(COM_CDO_Constants.COM_CDO_VERSION, comSetting.serviceSpecification_version__c) + ',';
		result += generateLine(COM_CDO_Constants.COM_CDO_NAME, comSetting.serviceSpecification_name__c) + ',';
		result += generateLine(COM_CDO_Constants.COM_CDO_TYPE, comSetting.serviceSpecificationType__c) + '}';
		return result;
	}

	private static String generateLine(String elementName, String value) {
		String result = '"' + elementName + '":"' + value + '"';
		return result;
	}

	public static void createPayloadAttachment(Id parentId, String attachmentName, String content) {
		Attachment attachment = new Attachment();
		attachment.Body = Blob.valueOf(content);
		attachment.Name = String.valueOf(attachmentName);
		attachment.ParentId = parentId;
		insert attachment;
	}

	public class DeliveryOrderUpdates {
		public Id deliveryOrderId;
		public String serviceOrderId;
		public String errorMessage;
		public String status;
		public Boolean success;
	}
}
