/**
 * 	@description	This class contains unit tests for the OrderStatuscontroller class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestOrderStatuscontroller {
	@isTest
	static void testResetOrder() {
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);

		TestUtils.createCompleteContract();
		Test.startTest();
		TestUtils.autoCommit = true;
		Order__c ord = TestUtils.createOrder(TestUtils.theContract);
		ord.Status__c = 'Accepted';
		update ord;

		PageReference pageRef = Page.OrderStatusReset;
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController stdController = new ApexPages.StandardController(ord);
		OrderStatuscontroller controller = new OrderStatuscontroller(stdController);

		controller.confirmReset();

		Test.stopTest();
	}

	@isTest
	static void testResetOrderWithInvalidStatus() {
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);

		TestUtils.createCompleteContract();
		Test.startTest();
		TestUtils.autoCommit = true;
		Order__c ord = TestUtils.createOrder(TestUtils.theContract);

		ord.Status__c = 'Rejected';
		update ord;

		PageReference pageRef = Page.OrderStatusReset;
		Test.setCurrentPage(pageRef);

		ApexPages.StandardController stdController = new ApexPages.StandardController(ord);
		OrderStatuscontroller controller = new OrderStatuscontroller(stdController);

		controller.confirmReset();
		controller.cancel();

		Test.stopTest();
	}
}
