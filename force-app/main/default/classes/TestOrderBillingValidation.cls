/*
 * @author Rahul Sharma
 *
 * @description Test class for OrderBillingValidation
 */
@IsTest
public class TestOrderBillingValidation {
	@TestSetup
	static void makeData() {
		// insert the configuration validation fields
		TestUtils.createOrderValidationBilling();
		// insert the configuration validations
		TestUtils.createOrderValidations();
	}

	@IsTest
	private static void testNoRecordId() {
		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(null, null);
		Test.stopTest();
		System.assertNotEquals(null, response.message, 'expecting null message');
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant, 'expecting variant');
	}

	@IsTest
	private static void testIncorrectBillingRecordId() {
		Account account = TestUtils.createAccount(null);
		cscfga__Product_Basket__c basket = createBasket(account);
		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(account.Id, basket.Id);
		Test.stopTest();
		System.assertNotEquals(null, response.message, 'expecting null message');
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant, 'expecting variant');
	}

	@IsTest
	private static void testBanValidationOnBasket() {
		Account account = TestUtils.createAccount(null);
		cscfga__Product_Basket__c basket = createBasket(account);

		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(basket.cscfga__Opportunity__r.Ban__c, basket.Id);
		Test.stopTest();

		System.assertNotEquals(null, response, 'expecting response');
		System.assertNotEquals(null, response.message, 'expecting null message');
		System.assertNotEquals(null, response.variant, 'expecting variant');
		System.assertEquals(null, response.body, 'expecting response');
	}

	@IsTest
	private static void testFinancialAccountValidationOnBasket() {
		Account account = TestUtils.createAccount(null);
		cscfga__Product_Basket__c basket = createBasket(account);

		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(basket.cscfga__Opportunity__r.Financial_Account__c, basket.Id);
		Test.stopTest();

		System.assertNotEquals(null, response, 'expecting response');
		System.assertNotEquals(null, response.message, 'expecting null message');
		System.assertNotEquals(null, response.variant, 'expecting variant');
		System.assertEquals(null, response.body, 'expecting response');
	}

	@IsTest
	private static void testBillingArrangementValidationOnBasket() {
		Account account = TestUtils.createAccount(null);
		cscfga__Product_Basket__c basket = createBasket(account);

		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(basket.cscfga__Opportunity__r.Billing_Arrangement__c, basket.Id);
		Test.stopTest();

		System.assertNotEquals(null, response, 'expecting response');
		System.assertNotEquals(null, response.message, 'expecting null message');
		System.assertNotEquals(null, response.variant, 'expecting variant');
		System.assertEquals(null, response.body, 'expecting response');
	}

	@IsTest
	private static void testBanValidationOnOrder() {
		Account account = TestUtils.createAccount(null);
		csord__Order__c order = createOrder(account);
		Ban__c ban = createBan(account);

		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(ban.Id, order.Id);
		Test.stopTest();

		System.assertNotEquals(null, response, 'expecting response');
		System.assertEquals(null, response.message, 'expecting null message');
		System.assertEquals(null, response.variant, 'expecting variant');
		System.assertNotEquals(null, response.body, 'expecting response');

		List<String> allErrors = (List<String>) response.body;
		System.assertNotEquals(0, allErrors.size(), 'there must be no errors');
	}
	@IsTest
	private static void testFinancialAccountValidationOnOrder() {
		Account account = TestUtils.createAccount(null);
		csord__Order__c order = createOrder(account);
		Financial_Account__c fa = createFa(account);

		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(fa.Id, order.Id);
		Test.stopTest();

		System.assertNotEquals(null, response, 'expecting response');
		System.assertEquals(null, response.message, 'expecting null message');
		System.assertEquals(null, response.variant, 'expecting variant');
		System.assertNotEquals(null, response.body, 'expecting response');

		List<String> allErrors = (List<String>) response.body;
		System.assertNotEquals(0, allErrors.size(), 'there must be no errors');
	}
	@IsTest
	private static void testBillingArrangementValidationOnOrder() {
		Account account = TestUtils.createAccount(null);
		csord__Order__c order = createOrder(account);
		Billing_Arrangement__c bar = createBar(account);
		bar.Bill_Format__c = '';
		update bar;

		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(bar.Id, order.Id);
		Test.stopTest();

		System.assertNotEquals(null, response, 'Expecting response');
		System.assertEquals(null, response.message, 'Expecting null message');
		System.assertEquals(null, response.variant, 'Expecting variant');
		System.assertNotEquals(null, response.body, 'Expecting response');

		List<String> allErrors = (List<String>) response.body;
		System.assertEquals(1, allErrors.size(), 'There must be an error');
		System.assert(allErrors[0].contains('BA Bill Format is empty.'), 'Error regarding BA Bill Format is expected.');
	}

	@IsTest
	private static void testBanOrderCheckOnBasket() {
		String validationMessage = 'BAN Name is empty from test.';
		Order_Validation__c orderValidation = new Order_Validation__c(
			Active__c = true,
			API_Fieldname__c = 'BAN_Name__c',
			Error_message__c = validationMessage,
			Export_System__c = 'SIAS',
			Name = 'BAN Name - test1',
			Object__c = 'Ban__c',
			Operator__c = 'isblank',
			Proposition__c = 'One Fixed;One Net;Numberporting;One Fixed Express;IPVPN;Legacy;Internet;Mobile',
			Who__c = 'Inside Sales;' + OrderBillingValidation.VALIDATION_WHO_ORDER_CHECK
		);
		insert orderValidation;

		Account account = TestUtils.createAccount(null);
		cscfga__Product_Basket__c basket = createBasket(account);
		Ban__c ban = new Ban__c(Account__c = account.Id, Unify_Customer_Type__c = 'C', Unify_Customer_SubType__c = 'B', Name = '388888856');
		insert ban;

		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(ban.Id, basket.Id);
		Test.stopTest();

		System.assertNotEquals(null, response, 'expecting response');
		System.assertEquals(null, response.message, 'expecting null message');
		System.assertEquals(null, response.variant, 'expecting variant');
		System.assertNotEquals(null, response.body, 'expecting response');

		List<String> allErrors = (List<String>) response.body;
		System.assertNotEquals(0, allErrors.size(), 'there must be no errors');
		System.assertEquals(false, allErrors.contains(validationMessage), 'We do not expect the order check validation');
	}

	@IsTest
	private static void testBanOrderCheckOnOrder() {
		String validationMessage = 'BAN Name is empty from test.';
		Order_Validation__c orderValidation = new Order_Validation__c(
			Active__c = true,
			API_Fieldname__c = 'BAN_Name__c',
			Error_message__c = validationMessage,
			Export_System__c = 'SIAS',
			Name = 'BAN Name - test2',
			Object__c = 'Ban__c',
			Operator__c = 'isblank',
			Proposition__c = 'One Fixed;One Net;Numberporting;One Fixed Express;IPVPN;Legacy;Internet;Mobile',
			Who__c = 'Inside Sales;' + OrderBillingValidation.VALIDATION_WHO_ORDER_CHECK
		);
		insert orderValidation;

		Account account = TestUtils.createAccount(null);
		csord__Order__c order = createOrder(account);
		Ban__c ban = new Ban__c(Account__c = account.Id, Unify_Customer_Type__c = 'C', Unify_Customer_SubType__c = 'B', Name = '388888856');
		insert ban;

		Test.startTest();
		LightningResponse response = OrderBillingValidation.getErrors(ban.Id, order.Id);
		Test.stopTest();

		System.assertNotEquals(null, response, 'expecting response');
		System.assertEquals(null, response.message, 'expecting null message');
		System.assertEquals(null, response.variant, 'expecting variant');
		System.assertNotEquals(null, response.body, 'expecting response');

		List<String> allErrors = (List<String>) response.body;
		System.assertNotEquals(0, allErrors.size(), 'there must be no errors');
		System.assertEquals(true, allErrors.contains(validationMessage), 'We expect the order check validation');
	}

	private static cscfga__Product_Basket__c createBasket(Account account) {
		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opportunity.id, csbb__Account__c = account.Id);
		insert basket;
		return basket;
	}

	private static csord__Order__c createOrder(Account account) {
		csord__Order__c order = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978', csord__Account__c = account.Id);
		insert order;
		return order;
	}

	private static Billing_Arrangement__c createBar(Account account) {
		Financial_Account__c fa = createFa(account);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);
		return bar;
	}

	private static Financial_Account__c createFa(Account account) {
		Ban__c ban = createBan(account);
		Contact contact = TestUtils.createContact(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);
		return fa;
	}

	private static Ban__c createBan(Account account) {
		Ban__c ban = TestUtils.createBan(account);
		return ban;
	}
}
