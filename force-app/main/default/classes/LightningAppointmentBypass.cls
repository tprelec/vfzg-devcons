/**
 * @description       : provides Invocable method to be used by `Inbound Lightning Appointment Custom` flow
 * @last modified on  : 06-20-2022
 **/
public with sharing class LightningAppointmentBypass {
	/**
    * @description takes in a string parameter, and return its WorkTypeGroup Id to bypass the screen selection
                   a per the requirements on https://jiranl.vodafoneziggo.com/browse/SFLO-387
    * @param workTypeNames
    * @return List<String>
    **/
	@InvocableMethod(label='Get Work Type Group Id' description='returns the WorkTypeGroup Id for the given WorkTypeGroup Name')
	public static List<String> getWorkTypeGroupId(List<String> workTypeNames) {
		List<String> workTypeIds = new List<String>();
		if (!workTypeNames.isEmpty() && String.isNotBlank(workTypeNames.get(0))) {
			// Decodes a string in application/x-www-form-urlencoded format using UTF-8 as encoding scheme
			String workTypeName = EncodingUtil.urlDecode(workTypeNames.get(0), 'UTF-8');
			List<WorkTypeGroup> workTypeGroups = [SELECT Id FROM WorkTypeGroup WHERE Name = :workTypeName WITH SECURITY_ENFORCED];
			if (!workTypeGroups.isEmpty()) {
				workTypeIds.add(workTypeGroups.get(0).Id);
			}
		}
		return workTypeIds;
	}
}
