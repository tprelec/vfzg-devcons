public without sharing class COM_ImplementServicesTblController {
	@AuraEnabled(cacheable=true)
	public static List<ServiceWrapper> getServices(String recId) {
		List<ServiceWrapper> retlist = new List<ServiceWrapper>();

		for (csord__Service__c srv : [
			SELECT
				Id,
				Name,
				csord__Service__c,
				Implemented_Date__c,
				COM_Delivery_Order__r.Installation_Start__c,
				(
					SELECT
						Id,
						Name,
						Implemented_Date__c,
						csord__Service__c,
						COM_Delivery_Order__r.Installation_Start__c,
						csordtelcoa__Cancelled_By_Change_Process__c
					FROM csord__Services__r
					WHERE Installation_Required__c = TRUE AND csordtelcoa__Cancelled_By_Change_Process__c = FALSE
				)
			FROM csord__Service__c
			WHERE COM_Delivery_Order__c = :getDeliveryOrderId(recId) AND csord__Service__c = NULL
			ORDER BY Name
		]) {
			retList.add(new ServiceWrapper(srv));
			for (csord__Service__c childSrv : srv.csord__Services__r) {
				retList.add(new ServiceWrapper(childSrv));
			}
		}

		return retList;
	}

	@AuraEnabled
	public static String updateServices(Object data) {
		List<ServiceWrapper> updatedWrappers = (List<ServiceWrapper>) JSON.deserialize(JSON.serialize(data), List<ServiceWrapper>.class);
		List<csord__Service__c> servicesToUpdate = new List<csord__Service__c>();

		for (ServiceWrapper sw : updatedWrappers) {
			servicesToUpdate.add(
				new csord__Service__c(Id = sw.serviceId, Implemented_Date__c = sw.implementedDate, csord__Status__c = 'Implemented')
			);
		}
		try {
			update servicesToUpdate;
			return 'Success: services updated successfully';
		} catch (Exception e) {
			return 'The following exception has occurred: ' + e.getMessage();
		}
	}

	private static Id getDeliveryOrderId(Id recId) {
		Id deliveryOrderId = recId;

		if (recId.getSObjectType().getDescribe().getName() == 'Task') {
			Task taskRec = [SELECT Id, COM_Delivery_Order__c FROM Task WHERE Id = :recId];
			deliveryOrderId = taskRec.COM_Delivery_Order__c;
		}
		return deliveryOrderId;
	}

	public class ServiceWrapper {
		@AuraEnabled
		public String parentServiceName;
		@AuraEnabled
		public String name;
		@AuraEnabled
		public Datetime implementedDate;
		@AuraEnabled
		public Datetime installationDate;
		@AuraEnabled
		public String serviceId;

		public ServiceWrapper(csord__Service__c service) {
			this.parentServiceName = service.csord__Service__c == null ? service.Name : null;
			this.name = service.csord__Service__c == null ? null : service.Name;
			this.implementedDate = service.Implemented_Date__c;
			this.installationDate = service.COM_Delivery_Order__r.Installation_Start__c;
			this.serviceId = service.Id;
		}
	}
}
