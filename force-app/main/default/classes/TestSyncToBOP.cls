@isTest
private class TestSyncToBOP {
	
	@isTest static void test_SyncToBopController() {
        // Setup data for test
    	Account partnerAccount = TestUtils.createPartnerAccount();
        Contact con = TestUtils.createContact(partnerAccount);    	
        con.email='testemail@gmail.com';
        update con;
		Site__c s = TestUtils.createSite(partnerAccount);

        // Start test
        test.startTest();

        // Create an instance of the controller
        ApexPages.StandardController sc = new ApexPages.StandardController(partnerAccount);     
		SyncToBopController controller = new SyncToBopController(sc); 
		Boolean b = controller.getEnableSyncToBOP();
		controller.sync();

        // Stop test
        test.stopTest();    	
	}
	
}