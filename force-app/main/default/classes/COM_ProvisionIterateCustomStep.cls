global without sharing class COM_ProvisionIterateCustomStep implements CSPOFA.ExecutionHandler{
    
    Map<Id, CSPOFA__Orchestration_Step__c> deliveryOrderStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
    private String PROCESS_NAME_DELIMITER = ' by - ';

    global List<SObject> process(List<SObject> steps) {
        List<SObject> result = new List<sObject>();
        Map<Id, CSPOFA__Orchestration_Step__c> processStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();

        List<CSPOFA__Orchestration_Step__c> stepList = CS_OrchestratorStepUtility.getStepList(steps);

        try {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                processStepMap.put(step.CSPOFA__Orchestration_Process__c, step);
                deliveryOrderStepMap.put(step.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c, step);
            }

            Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c>([
                    SELECT Id
                    FROM CSPOFA__Orchestration_Process__c
                    WHERE Id IN :processStepMap.keySet()
            ]);

            kickOffSubprocess(stepList);

            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = 'Waiting';
                step.CSPOFA__Completed_Date__c = Date.today();
                step.CSPOFA__Message__c = 'Custom step succeeded'; 
                result.add(step);
                
            }
        } catch (Exception ex) {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
                step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
                if(step.CSPOFA__Message__c.length() > 255 ){
                    step.CSPOFA__Message__c = step.CSPOFA__Message__c.substring(0, 254);
                }
                result.add(step);
            }
        }
        return result;
    }
    
    @TestVisible
    private static List<String> getAlldeliveryComponentNamesByDeliveryOrder(Id deliveryOrderId, List<Delivery_Component__c> deliveryComponents){
        List<String> deliveryOrderDeliveryComponentNames = new List<String>();

        for (Delivery_Component__c deliveryComponent : deliveryComponents) {
            if(deliveryComponent.COM_Delivery_Order__c == deliveryOrderId) {
                deliveryOrderDeliveryComponentNames.add(deliveryComponent.Name);
            }
        }

        return deliveryOrderDeliveryComponentNames;
    }
    
    @TestVisible
    private static List<Delivery_Component__c> getAllDeliveryComponents(Set<Id> deliveryOrderIdList){
        List<Delivery_Component__c> deliveryComponents = [SELECT Id,
                                                                Name,
                                                                Service__c,
                                                                Service__r.csordtelcoa__Replaced_Service__c,
                                                                Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c,
                                                                Service__r.csord__Subscription__c,
                                                                Service__r.csord__Service__r.csord__Subscription__c,
                                                                Service__r.VFZ_Depends_On_These_Service_Identifiers__c,
                                                                Service__r.csord__Service__r.VFZ_Depends_On_These_Service_Identifiers__c,
                                                                Article_Name__c,
                                                                Article_Code__c,
                                                                Service__r.Name,
                                                                Implemented_Date__c,
                                                                COM_Delivery_Order__c,
                                                                MACD_Action__c,
                                                        (SELECT Id,
                                                                Name,
                                                                Attribute_Value__c,
                                                                Delivery_Component__r.Article_Name__c,
                                                                Delivery_Component__r.Article_Code__c,
                                                                Delivery_Component__r.Name,
                                                                Delivery_Component__c
                                                        FROM Delivery_Component_Attributes__r)
                                                        FROM Delivery_Component__c
                                                        WHERE COM_Delivery_Order__c IN :deliveryOrderIdList
                                                        AND COM_Delivery_Order__c != null
                                                        AND MACD_Action__c != 'N/A'
                                                        ];
        return deliveryComponents;
    }
    
    @TestVisible
    private static List<COM_Delivery_Component_Activity__mdt> getComponentActivities(){
        List<COM_Delivery_Component_Activity__mdt> returnValue;

        returnValue = [SELECT Id,
                DeveloperName,
                Label,
                Delivery_Component__r.DeveloperName,
                Show_Related_Service_Data__c,
                Delivery_Component__r.Label,
                Provisioning_Queue__c,
                Installation__c,
                Provisioning__c,
                Porting__c,
                Deprovisioning_Queue__c,
                Reverse_Provisioning_Queue__c,
                Change_Provisioning_Queue__c,
                Provisioning_System__c,
                Provisioning_Type__c
        FROM COM_Delivery_Component_Activity__mdt
        WHERE Provisioning__c = true];

        return returnValue;
    }
    
    @TestVisible
    private List<COM_Delivery_Component_Activity__mdt> getComponentActivitiesByDeliveryOrder(List<String> deliveryComponentNamesByDeliveryOrder, List<COM_Delivery_Component_Activity__mdt> componentActivities) {
        
        List<COM_Delivery_Component_Activity__mdt> componentActivitiesByDeliveryOrder = new List<COM_Delivery_Component_Activity__mdt>();
        
        for(COM_Delivery_Component_Activity__mdt componentActivity : componentActivities) {
            for(String deliveryComponentNameByDeliveryOrder : deliveryComponentNamesByDeliveryOrder) {
                if(componentActivity.Delivery_Component__r.Label == deliveryComponentNameByDeliveryOrder) {
                    componentActivitiesByDeliveryOrder.add(componentActivity);
                    break;
                }
            }
        }
        
        return componentActivitiesByDeliveryOrder;
    }
    
    @TestVisible
    private String getQueueBasedOnMACDType(String comMACDType, COM_Delivery_Component_Activity__mdt activity) {
        String queue = null;
        String queueValue = null;
        
        if(comMACDType == 'Provisioning') {
            queueValue = activity.Provisioning_Queue__c;
        } else if(comMACDType == 'Deprovisioning') {
            queueValue = activity.Deprovisioning_Queue__c;
        } else if(comMACDType == 'Change') {
            queueValue = activity.Change_Provisioning_Queue__c;
        } else if(comMACDType == 'Reverse Provisioning') {
            queueValue = activity.Reverse_Provisioning_Queue__c;
        } 

        if(queueValue != null) {
            queue = 'COM ' + queueValue;
        }
        
        return queue;
    }
    
    @TestVisible
    private CSPOFA.ProcessRequest createProcessRequest(CSPOFA__Orchestration_Step__c step, String queue) {
        return new CSPOFA.ProcessRequest().templateName('Provisioning_Deprovisioning').relationships( new Map<Schema.SObjectField, Id>{
                    CSPOFA__Orchestration_Process__c.CSPOFA__Root_Process__c => step.CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c,
                    CSPOFA__Orchestration_Process__c.CSPOFA__Parent_Process__c => step.CSPOFA__Orchestration_Process__c, 
                    CSPOFA__Orchestration_Process__c.COM_Iterate_Step__c => step.Id,
                    CSPOFA__Orchestration_Process__c.COM_Delivery_Order__c => step.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c}
                ).processName(step.COM_MACD_Type__c + PROCESS_NAME_DELIMITER + queue);
    }
    
    @TestVisible
    private void kickOffSubprocess(List<CSPOFA__Orchestration_Step__c> stepList){
        Map<Id, String> orchestrationStepQueueMap = new Map<Id, String>();    
        List<Delivery_Component__c> deliveryComponents = COM_ProvisionIterateCustomStep.getAllDeliveryComponents(deliveryOrderStepMap.keySet());
        
        List<COM_Delivery_Component_Activity__mdt> componentActivities = COM_ProvisionIterateCustomStep.getComponentActivities(); //step.COM_MACD_Type__c
        
        List<CSPOFA.ProcessRequest> processRequests = new List<CSPOFA.ProcessRequest>();
        for (CSPOFA__Orchestration_Step__c step : stepList) {
            List<String> deliveryComponentNamesByDeliveryOrder = COM_ProvisionIterateCustomStep.getAlldeliveryComponentNamesByDeliveryOrder(step.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c, deliveryComponents);
            List<COM_Delivery_Component_Activity__mdt> componentActivitiesByDeliveryOrder = getComponentActivitiesByDeliveryOrder(deliveryComponentNamesByDeliveryOrder, componentActivities);
            
            Set<String> queuesCreated = new Set<String>();
            
            for(COM_Delivery_Component_Activity__mdt activity : componentActivitiesByDeliveryOrder) {                
                String queue = getQueueBasedOnMACDType(step.COM_MACD_Type__c, activity);
                
                if(queue == null || queuesCreated.contains(queue)) {
                    continue;
                } else {
                    queuesCreated.add(queue);
                }
                
                CSPOFA.ProcessRequest processRequest = createProcessRequest(step, queue);
                processRequests.add(processRequest);
            }
        }

        List<CSPOFA.ApiResult> results = CSPOFA.API_V1.processes.create(processRequests);
        Set<Id> successfulProcesesIdSet = new Set<Id>();
        for (CSPOFA.ApiResult result : results) { 
            if (result.isSuccess()) {
                String resourceId = result.getId(); 
                successfulProcesesIdSet.add(resourceId);
            } 
            else {
                List<CSPOFA.ApiError> errors = result.getErrors();
            }
        }
        
        if(successfulProcesesIdSet.size() > 0) {
            List<CSPOFA__Orchestration_Process__c> orcestratorProcessList = [SELECT id, COM_Delivery_Queue__c, COM_Iterate_Step__c, Name FROM CSPOFA__Orchestration_Process__c WHERE id IN :successfulProcesesIdSet];
            
            for(CSPOFA__Orchestration_Process__c orchestrationProcess : orcestratorProcessList) {
                orchestrationProcess.COM_Delivery_Queue__c = getQueueNameFromProcessName(orchestrationProcess.Name);
            }
            
            update orcestratorProcessList;
        }
    }
    
    // This is the only way how to get the queue name. If someone has better ideas please update this method :)
    @TestVisible
    private String getQueueNameFromProcessName(String processName) {
        String queueName = '';
        
        if(processName != null) {
            String queueNameWithoutUnderscores = processName.substringAfter(PROCESS_NAME_DELIMITER);
            queueName = queueNameWithoutUnderscores.replaceAll(' ', '_');
        }
        
        return queueName;
    }
}