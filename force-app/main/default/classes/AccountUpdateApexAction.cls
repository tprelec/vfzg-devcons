/**
 * @description           This class is responsible for updating the Account Object using flows
 * @author                Sinisa Kovacevic
 *
 * @Defaults
 *
 *
 */
public with sharing class AccountUpdateApexAction {
	@InvocableMethod
	public static List<String> updateAccount(List<Id> accountIds) {
		List<Id> accountIdList = new List<Id>();
		Account acc = new Account();

		List<Account> accounts = [SELECT Id, Name, KVK_Number__c FROM Account WHERE Id IN :accountIds];

		for (Account account : accounts) {
			accountIdList.add(account.Id);
			acc = account;
		}

		if (acc.KVK_number__c != null) {
			OlbicoServiceJSON jService = new OlbicoServiceJSON();

			try {
				jService.setRequestRestJson(acc.KVK_number__c);
				if (!Test.isRunningTest()) {
					jService.makeReqestRestJsonUpdate(acc.KVK_number__c, acc.Id);
				}
			} catch (Exception ex) {
				LoggerService.log(ex);
				System.debug(LoggingLevel.DEBUG, 'Database update exception: ' + ex);
			}
		}

		return accountIdList;
	}
}
