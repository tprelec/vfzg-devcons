public with sharing class SLATriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<SLA__c> newProducts = (List<SLA__c>) this.newList;
		Sequence seq = new Sequence('SLA');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (SLA__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
