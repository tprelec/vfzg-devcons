/*
	Author: Juan Cardona
	Description: Tests class for COM_CancelledSitesController.cls
	Jira ticket: COM-3181
*/

@isTest
private class TestCOM_CancelledSitesController {
	@testSetup
	static void prepareCommonTestData() {
		Account account = CS_DataTest.createAccount('Test Account');
		insert account;

		OrderType__c orderType = CS_DataTest.createOrderType('MAC');
		insert orderType;

		Product2 prod21 = CS_DataTest.createProduct2('Recurring Price Basic 1', 'Access Definition', orderType, true);
		prod21.Quantity_type__c = 'Monthly';

		Product2 prod22 = CS_DataTest.createProduct2('Recurring Price Basic 2', 'Access Definition', orderType, true);
		prod22.Quantity_type__c = 'Monthly';

		List<Product2> products = new List<Product2>{ prod21, prod22 };
		insert products;

		cscfga__Product_Definition__c pdAccess = CS_DataTest.createProductDefinitionRegular('Access');
		insert pdAccess;

		Site__c testSite = CS_DataTest.createSite('testSite', account, '1234AA', 'teststreet', 'testTown', 3);
		Site__c testSite2 = CS_DataTest.createSite('testSite2', account, '1234AB', 'teststreet2', 'testTown2', 3);
		List<Site__c> siteList = new List<Site__c>{ testSite, testSite2 };
		insert siteList;

		// old basket
		Opportunity oldOpportunity = CS_DataTest.createOpportunity(account, 'Test ACQ', System.UserInfo.getUserId());
		insert oldOpportunity;

		cscfga__Product_Basket__c oldBasket = CS_DataTest.createProductBasket(oldOpportunity, 'VZ-Test-01');
		oldBasket.cscfga__Basket_Status__c = 'Valid';
		oldBasket.Primary__c = true;
		oldBasket.csbb__Synchronised_with_Opportunity__c = true;
		insert oldBasket;

		cscfga__Product_Configuration__c oldPcAccess = CS_DataTest.createProductConfiguration(pdAccess.Id, 'Access 1', oldBasket.Id);
		oldPcAccess.cscfga__Contract_Term__c = 24;
		oldPcAccess.Recurring_Charge_Product__c = prod21.Id;
		oldPcAccess.Site__c = testSite.Id;
		insert oldPcAccess;

		// new basket
		Opportunity newOpportunity = CS_DataTest.createOpportunity(account, 'Test ACQ New', System.UserInfo.getUserId());
		insert newOpportunity;

		cscfga__Product_Basket__c newBasket = CS_DataTest.createProductBasket(newOpportunity, 'VZ-Test-02');
		newBasket.cscfga__Basket_Status__c = 'Valid';
		newBasket.Primary__c = true;
		newBasket.New_Portfolio__c = true;
		newBasket.csbb__Synchronised_with_Opportunity__c = true;
		insert newBasket;

		cscfga__Product_Configuration__c newPcAccess = CS_DataTest.createProductConfiguration(pdAccess.Id, 'Access 2', newBasket.Id);
		newPcAccess.cscfga__Contract_Term__c = 24;
		newPcAccess.Recurring_Charge_Product__c = prod22.Id;
		newPcAccess.csordtelcoa__Replaced_Product_Configuration__c = oldPcAccess.Id;
		newPcAccess.csordtelcoa__cancelled_by_change_process__c = true;
		newPcAccess.Site__c = testSite2.Id;
		insert newPcAccess;

		// attributes
		cscfga__Attribute__c attributeOneOffOld = CS_DataTest.createAttribute(oldPcAccess.Id, 'OneOff', '10');
		attributeOneOffOld.cscfga__Is_Line_Item__c = true;
		attributeOneOffOld.cscfga__Recurring__c = false;
		attributeOneOffOld.cscfga__Price__c = 10;
		attributeOneOffOld.cscfga__Line_Item_Description__c = 'OneOff component';

		cscfga__Attribute__c attributeRecurringOld = CS_DataTest.createAttribute(oldPcAccess.Id, 'Recurring', '20');
		attributeRecurringOld.cscfga__Is_Line_Item__c = true;
		attributeRecurringOld.cscfga__Recurring__c = true;
		attributeRecurringOld.cscfga__Price__c = 20;
		attributeRecurringOld.cscfga__Line_Item_Description__c = 'Recurring component';

		cscfga__Attribute__c attributeOneOffNew = CS_DataTest.createAttribute(newPcAccess.Id, 'OneOff', '10');
		attributeOneOffNew.cscfga__Is_Line_Item__c = true;
		attributeOneOffNew.cscfga__Recurring__c = false;
		attributeOneOffNew.cscfga__Price__c = 10;
		attributeOneOffNew.cscfga__Line_Item_Description__c = 'OneOff component';

		cscfga__Attribute__c attributeRecurringNew = CS_DataTest.createAttribute(newPcAccess.Id, 'Recurring', '20');
		attributeRecurringNew.cscfga__Is_Line_Item__c = true;
		attributeRecurringNew.cscfga__Recurring__c = true;
		attributeRecurringNew.cscfga__Price__c = 20;
		attributeRecurringNew.cscfga__Line_Item_Description__c = 'Recurring component';

		List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>{
			attributeOneOffOld,
			attributeRecurringOld,
			attributeOneOffNew,
			attributeRecurringNew
		};
		insert attributes;

		ProductUtility.CreateOLIs(new Set<String>{ newBasket.Id }, null);

		OpportunityLineItem oppLineItem = [SELECT Id, Name, Proposition__c FROM OpportunityLineItem LIMIT 1];

		oppLineItem.Proposition__c = COM_CancelledSitesController.BUSINESS_INTERNET_PROPOSITION;
		update oppLineItem;
	}

	@isTest
	static void testGetCancellationSites() {
		Opportunity newOpp = [SELECT Id, Name FROM Opportunity WHERE Name = 'Test ACQ New'];
		Test.startTest();
		COM_CancelledSitesController comSiteController = new COM_CancelledSitesController();
		comSiteController.opportunityId = newOpp.Id;
		Map<String, List<COM_CancelledSitesController.TerminatedProduct>> mapComCancelled = comSiteController.getCancelledProducts();
		Test.stopTest();

		System.assertNotEquals(mapComCancelled, null, 'Cancelled Sites not found');
		System.assertNotEquals(mapComCancelled.size(), 0, 'Cancelled Sites should be created!');
	}
}
