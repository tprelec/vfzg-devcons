@isTest
public class CS_BusinessMobileLookupTest {
	@TestSetup
	static void makeData() {
		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c bmDefinition = CS_DataTest.createProductDefinition('Business Mobile');
		bmDefinition.RecordTypeId = productDefinitionRecordType;
		insert bmDefinition;

		OrderType__c orderType = CS_DataTest.createOrderType();
		insert orderType;

		Product2 productTest = CS_DataTest.createProduct('BM product', orderType);
		insert productTest;

		Category__c categoryTest = CS_DataTest.createCategory('Business Mobile');
		insert categoryTest;

		Vendor__c tstVend = CS_DataTest.createVendor('testVendor');
		insert tstVend;

		cspmb__Price_Item__c priceItemMaster = new cspmb__Price_Item__c(Name = 'Master Price Item', cspmb__Is_Active__c = true);
		priceItemMaster.cspmb__Product_Definition_Name__c = bmDefinition.Name;
		priceItemMaster.cspmb__Type__c = 'Commercial Product';
		priceItemMaster.cspmb__Role__c = 'Master';
		priceItemMaster.cspmb__Price_Item_Code__c = 'masterCode';
		insert priceItemMaster;

		cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(productTest, categoryTest, 100, 30, tstVend, '', '');
		priceItem1.Name = 'BM1';
		priceItem1.cspmb__Master_Price_item__c = priceItemMaster.Id;
		priceItem1.cspmb__Product_Definition_Name__c = bmDefinition.Name;
		priceItem1.cspmb__Effective_Start_Date__c = Date.today().addDays(-30);
		priceItem1.cspmb__Effective_End_Date__c = Date.today().addDays(30);
		priceItem1.cspmb__Type__c = 'Commercial Product';
		priceItem1.cspmb__Role__c = 'Variant';
		priceItem1.cspmb__Price_Item_Code__c = 'priceItem1Code';
		priceItem1.Min_Duration__c = 12;
		priceItem1.Max_Duration__c = 13;
		priceItem1.Sales_Channel__c = 'Direct';
		priceItem1.Mobile_Scenario__c = 'BM Scenario';
		priceItem1.Mobile_Add_on_category__c = '*Subscription*';
		priceItem1.Category__c = 'Business Mobile';

		cspmb__Price_Item__c priceItem2 = CS_DataTest.createPriceItem(productTest, categoryTest, 100, 30, tstVend, '', '');
		priceItem2.Name = 'BM2';
		priceItem2.cspmb__Master_Price_item__c = priceItemMaster.Id;
		priceItem2.cspmb__Product_Definition_Name__c = bmDefinition.Name;
		priceItem2.cspmb__Effective_Start_Date__c = Date.today().addDays(-30);
		priceItem2.cspmb__Effective_End_Date__c = Date.today().addDays(30);
		priceItem2.cspmb__Type__c = 'Commercial Product';
		priceItem2.cspmb__Role__c = 'Variant';
		priceItem2.cspmb__Price_Item_Code__c = 'priceItem2Code';
		priceItem2.Min_Duration__c = 12;
		priceItem2.Max_Duration__c = 13;
		priceItem2.Sales_Channel__c = 'Direct';
		priceItem2.Mobile_Scenario__c = 'Business Mobile OneMonth';
		priceItem2.Mobile_Add_on_category__c = '*Subscription*';
		priceItem2.Category__c = 'Business Mobile';

		List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>{ priceItem1, priceItem2 };
		insert priceItems;
	}

	@isTest
	private static void testRequiredAttributes() {
		CS_BusinessMobileLookup caLookup = new CS_BusinessMobileLookup();
		String result = caLookup.getRequiredAttributes();
		System.assertNotEquals(null, result, 'Return string must not be null');
	}

	@isTest
	public static void lookupTest() {
		cscfga__Product_Definition__c bmDefinition = [SELECT Id FROM cscfga__Product_Definition__c WHERE Name = 'Business Mobile' LIMIT 1];
		CS_BusinessMobileLookup bmLookup = new CS_BusinessMobileLookup();

		Map<String, String> searchFields1 = new Map<String, String>();
		searchFields1.put('ContractTerm', '12');
		searchFields1.put('MobileScenario', 'BM Scenario');
		searchFields1.put('SalesChannel', 'Direct');
		searchFields1.put('Today', string.valueOf(Date.today()));
		List<Object> result = bmLookup.doLookupSearch(searchFields1, String.valueOf(bmDefinition.Id), null, 0, 0);
		System.assertEquals(2, result.size(), 'Items should exist.');

		Map<String, String> searchFields2 = new Map<String, String>();
		searchFields2.put('ContractTerm', '12');
		searchFields2.put('MobileScenario', 'BM Scenario');
		searchFields2.put('SalesChannel', 'Indirect');
		searchFields2.put('Today', string.valueOf(Date.today()));
		result = bmLookup.doLookupSearch(searchFields2, String.valueOf(bmDefinition.Id), null, 0, 0);
		System.assertEquals(0, result.size(), 'Items should not exist.');

		Map<String, String> searchFields3 = new Map<String, String>();
		searchFields3.put('ContractTerm', '12');
		searchFields3.put('MobileScenario', 'Some Scenario');
		searchFields3.put('SalesChannel', 'Direct');
		searchFields3.put('Today', string.valueOf(Date.today()));
		result = bmLookup.doLookupSearch(searchFields3, String.valueOf(bmDefinition.Id), null, 0, 0);
		System.assertEquals(1, result.size(), 'One item should exist.');
	}
}
