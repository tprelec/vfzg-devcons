@IsTest
public class LG_CheckRecursiveTest {
	@IsTest
    public static void test1() {
        System.assertEquals(true, LG_CheckRecursive.runPBOnceInsert());
        System.assertEquals(false, LG_CheckRecursive.runPBOnceInsert());
        
        System.assertEquals(true, LG_CheckRecursive.runPBOnceUpdate());
        System.assertEquals(false, LG_CheckRecursive.runPBOnceUpdate());
        
        System.assertEquals(true, LG_CheckRecursive.runPBOnceDelete());
        System.assertEquals(false, LG_CheckRecursive.runPBOnceDelete());
        
        System.assertEquals(true, LG_CheckRecursive.runSEPAOnce());
        System.assertEquals(false, LG_CheckRecursive.runSEPAOnce());
        
        System.assertEquals(true, LG_CheckRecursive.countInvalidatedPremisesOnce());
        System.assertEquals(false, LG_CheckRecursive.countInvalidatedPremisesOnce());
        
        System.assertEquals(true, LG_CheckRecursive.runPEALOnce());
        System.assertEquals(false, LG_CheckRecursive.runPEALOnce());
        
    }
}