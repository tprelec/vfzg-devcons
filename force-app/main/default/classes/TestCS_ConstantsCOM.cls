@isTest
private with sharing class TestCS_ConstantsCOM {
	@isTest
	private static void testConstants() {
		System.assertNotEquals('', CS_ConstantsCOM.CONTRACT_STATUS_IMPLEMENTATION, 'Implementation');
		System.assertNotEquals('', CS_ConstantsCOM.CONTRACT_STATUS_AMENDED, 'Amended');

		System.assertNotEquals('', CS_ConstantsCOM.PRODUCT_DEFINITION_ACCESS, 'Access');
		System.assertNotEquals('', CS_ConstantsCOM.PRODUCT_DEFINITION_INTERNET, 'Internet');
		System.assertNotEquals('', CS_ConstantsCOM.PRODUCT_DEFINITION_BUSINESS_INTERNET, 'Business Internet');

		System.assertNotEquals('', CS_ConstantsCOM.SOLUTION_DEFINITION_TYPE_MAIN, 'Main');
		System.assertNotEquals('', CS_ConstantsCOM.SOLUTION_DEFINITION_TYPE_COMPONENT, 'Component');

		System.assertNotEquals('', CS_ConstantsCOM.SOLUTION_DEFINITION_NAME_BIMO, 'Business Internet Modem Only');

		System.assertNotEquals('', CS_ConstantsCOM.OLI_ACTIVITY_TYPE_ADD, 'Add');
		System.assertNotEquals('', CS_ConstantsCOM.OLI_ACTIVITY_TYPE_CEASE, 'Cease');

		System.assertNotEquals('', CS_ConstantsCOM.BASKET_CHANGE_TYPE_CHANGE_SOLUTION, 'Change Solution');
	}
}
