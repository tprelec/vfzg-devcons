public with sharing class OrderEntryPromotionTriggerHandler extends TriggerHandler {
	private List<OE_Promotion__c> newOEPromotions;
	private Map<Id, OE_Promotion__c> newOEPromotionsMap;
	private Map<Id, OE_Promotion__c> oldOEPromotionsMap;

	private void init() {
		newOEPromotions = (List<OE_Promotion__c>) this.newList;
		newOEPromotionsMap = (Map<Id, OE_Promotion__c>) this.newMap;
		oldOEPromotionsMap = (Map<Id, OE_Promotion__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	public void setExternalIds() {
		Sequence seq = new Sequence('OE_Promotion');

		if (seq.canBeUsed()) {
			seq.startMutex();
			for (OE_Promotion__c o : newOEPromotions) {
				o.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}