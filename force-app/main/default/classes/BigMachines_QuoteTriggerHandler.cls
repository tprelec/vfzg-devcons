/**
 * @description			This is the Vodafone-internal trigger handler for the BigMachines__Quote sObject.
 * @author				Guy Clairbois
 */
public without sharing class BigMachines_QuoteTriggerHandler extends TriggerHandler {

    

    /**
     * @description         This handles the before insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeInsert(){
        List<BigMachines__Quote__c> newQuotes = (List<BigMachines__Quote__c>) this.newList; 
        copyOpportunityOwner(newQuotes);
    }

    /**
     * @description         This handles the after insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterInsert(){
        List<BigMachines__Quote__c> newQuotes = (List<BigMachines__Quote__c>) this.newList; 
        
        createSharingRulesFromOpportunityTeam(newQuotes);
        updateSyncingQuoteOnOpportunity(newQuotes,null);
        createSharingRulesForSagCreator(newQuotes); // test comment
    }    


    /**
     * @description         This handles the before update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeUpdate(){
        List<BigMachines__Quote__c> newQuotes = (List<BigMachines__Quote__c>) this.newList; 
        Map<Id,BigMachines__Quote__c> oldQuotesMap = (Map<Id,BigMachines__Quote__c>) this.oldMap;

    }
    

    /**
     * @description         This handles the after update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterUpdate(){
        List<BigMachines__Quote__c> newQuotes = (List<BigMachines__Quote__c>) this.newList; 
        List<BigMachines__Quote__c> oldQuotes = (List<BigMachines__Quote__c>) this.oldList;
        Map<Id,BigMachines__Quote__c> oldQuotesMap = (Map<Id,BigMachines__Quote__c>) this.oldMap;
        
        updateSyncingQuoteOnOpportunity(newQuotes,oldQuotesMap);
    }


    /*
     *      Description:    update the quote owner to the opportunity owner
     */
    private void copyOpportunityOwner(List<BigMachines__Quote__c> newQuotes){
        // gather opp ids
        Map<Id,Id> oppIdToOwner  = new Map<Id,Id>();
        for(BigMachines__Quote__c bmq : newQuotes){
            oppIdToOwner.put(bmq.BigMachines__Opportunity__c,null);
        }
        // gather opp owners
        for(Opportunity opp : [Select Id, OwnerId From Opportunity Where Id in :oppIdToOwner.keySet()]){
            oppIdToOwner.put(opp.Id,opp.OwnerId);
        }
        // assign opp owners to quotes
        for(BigMachines__Quote__c bmq : newQuotes){
            bmq.OwnerId = oppIdToOwner.get(bmq.BigMachines__Opportunity__c);
        }

    }

    /*
     *      Description:    update the lookup field 'primay quote' on the opportunity
     */
    private void updateSyncingQuoteOnOpportunity(List<BigMachines__Quote__c> newQuotes, Map<Id,BigMachines__Quote__c> oldQuotesMap){
        // bigmachines already makes sure only 1 primary quote exists at all time so we don't have to check that here

        Map<Id,Opportunity> oppsToUpdate = new Map<Id,Opportunity>();
        for(BigMachines__Quote__c bmq : newQuotes){
            // if new quote is primary, or existing is changed from non-primary to primary
            if(bmq.BigMachines__Is_Primary__c && (oldQuotesMap == null || !oldQuotesMap.get(bmq.Id).BigMachines__Is_Primary__c)){
                oppsToUpdate.put(bmq.BigMachines__Opportunity__c,new Opportunity(Id=bmq.BigMachines__Opportunity__c,Primary_Quote__c=bmq.Id));
            }
            // if existing quote is changed from primary to non-primary
            if(oldQuotesMap != null && !bmq.BigMachines__Is_Primary__c &&  oldQuotesMap.get(bmq.Id).BigMachines__Is_Primary__c){
                // only remove if no new primary quote is provided in this batch
                if(oppsToUpdate.containsKey(bmq.BigMachines__Opportunity__c)){
                   oppsToUpdate.put(bmq.BigMachines__Opportunity__c,new Opportunity(Id=bmq.BigMachines__Opportunity__c,Primary_Quote__c=null));
                }
            }            

        }
        if(!oppsToUpdate.isEmpty())
            update oppsToUpdate.values();

    }

    /*
     *      Description:    when the quote is created, apply the sharing rules from the opp team
     */
	private void createSharingRulesFromOpportunityTeam(List<BigMachines__Quote__c> quotes){

		set<Id> oppIds = new Set<Id>();
		for(BigMachines__Quote__c q : quotes){
			oppIds.add(q.BigMachines__Opportunity__c);
		}

        // fetch opportunity info
        List<BigMachines__Quote__c> quotesWithOpp = [Select Id,
                                        BigMachines__Opportunity__c,
                                        BigMachines__Opportunity__r.OwnerId
                                    From
                                        BigMachines__Quote__c
                                    Where
                                        BigMachines__Opportunity__c in :oppIds];        

        List<BigMachines__Quote__Share> quoteSharingRulesToInsert = new List<BigMachines__Quote__Share>();        
        Map<Id, List<OpportunityTeamMember>> newOppIdsToMember = new Map<Id, List<OpportunityTeamMember>>();
        Set<Id> allTeamMemberIds = new Set<Id>();

        List<OpportunityTeamMember> newOpps = [Select 
                                                    Id, 
                                                    userId, 
                                                    User.IsActive,
                                                    User.Name,                                                    
                                                    OpportunityId, 
                                                    OpportunityAccessLevel,
                                                    TeamMemberRole 
                                                From 
                                                    OpportunityTeamMember 
                                                Where 
                                                    OpportunityId in :oppIds];

        // Build map
        for(OpportunityTeamMember otm : newOpps){
            if(otm.User.IsActive == false){
                for(BigMachines__Quote__c q : quotes){
                    q.addError('Opportunity Team Member '+otm.User.Name+' is inactive. Please replace by an active User first.');
                }
                return;
            }              
            if(newOppIdsToMember.containsKey(otm.OpportunityId)){
                newOppIdsToMember.get(otm.OpportunityId).add(otm);
            } else {
                newOppIdsToMember.put(otm.OpportunityId,new List<OpportunityTeamMember>{otm});
            }
            allTeamMemberIds.add(otm.userId);
        }

        for(BigMachines__Quote__c bmq : quotesWithOpp){
            allTeamMemberIds.add(bmq.BigMachines__Opportunity__r.OwnerId);
        }        

        // add parent owners
        Map<Id,Id> parentPartnerUserIds = GeneralUtils.getParentPartnerUserIdsFromUserIds(allTeamMemberIds);


        if(!newOppIdsToMember.isEmpty()){

            // create new sharing rules for all quotes for all members
            for(BigMachines__Quote__c quote : [Select Id, BigMachines__Opportunity__c From BigMachines__Quote__c Where BigMachines__Opportunity__c in :newOppIdsToMember.keySet()]){
                for(OpportunityTeamMember otm : newOppIdsToMember.get(quote.BigMachines__Opportunity__c)){
                    BigMachines__Quote__Share bqs = new BigMachines__Quote__Share(
                        parentId = quote.Id,
                        UserOrGroupId = otm.UserId,
                        RowCause = Schema.BigMachines__Quote__Share.RowCause.Opportunity_Team__c
                    );
                    // accesslevel 'None' is assigned to the Opportunity owner, so should also result in Edit rights
                    if(otm.OpportunityAccessLevel == 'All' || otm.OpportunityAccessLevel == 'None'){
                        bqs.AccessLevel = 'Edit';
                    } else {
                        bqs.AccessLevel = otm.OpportunityAccessLevel;
                    }
                    system.debug(bqs);
                    quoteSharingRulestoInsert.add(bqs);
                    // add sharing rule for parent partners
                    if(parentPartnerUserIds.containsKey(otm.UserId)){
                        BigMachines__Quote__Share bqsParent = new BigMachines__Quote__Share(
                            parentId = quote.Id,
                            UserOrGroupId = parentPartnerUserIds.get(otm.UserId),
                            RowCause = Schema.BigMachines__Quote__Share.RowCause.Opportunity_Team_Parent__c,
                            AccessLevel = 'Edit'
                        );
                        quoteSharingRulestoInsert.add(bqsParent);
                    }
                }

            }            
        }
        // create new sharing rules for opp owner parents (if any)
        for(BigMachines__Quote__c bmq : quotesWithOpp){
            if(parentPartnerUserIds.containsKey(bmq.BigMachines__Opportunity__r.OwnerId)){
                BigMachines__Quote__Share bqs = new BigMachines__Quote__Share(RowCause = Schema.BigMachines__Quote__Share.RowCause.Opportunity_Owner_Parent__c);
                bqs.parentId = bmq.Id;
                bqs.UserOrGroupId = parentPartnerUserIds.get(bmq.BigMachines__Opportunity__r.OwnerId);
                bqs.AccessLevel = 'Edit';
                
                system.debug(bqs);
                quoteSharingRulestoInsert.add(bqs);                
            }
        }

        Savepoint sp = Database.setSavepoint();
        try{
            WithoutSharingMethods wsm = new WithoutSharingMethods();
            wsm.insertQuoteSharingRules(quoteSharingRulestoInsert);
        } catch (dmlException de){
            Database.rollback(sp);
            throw new ExInvalidStateException('Error while creating Quote sharing rules: '+de);
        }

	}

    /*
     *      Description:    when the quote is created, apply the sharing rules from the opp team
     */
    private void createSharingRulesForSagCreator(List<BigMachines__Quote__c> quotes){

        set<Id> oppIds = new Set<Id>();
        for(BigMachines__Quote__c q : quotes){
            oppIds.add(q.BigMachines__Opportunity__c);
        }

        // fetch opportunity info
        List<BigMachines__Quote__c> quotesWithOpp = [Select Id,
                                        BigMachines__Opportunity__c,
                                        BigMachines__Opportunity__r.OwnerId,
                                        OwnerId
                                    From
                                        BigMachines__Quote__c
                                    Where
                                        BigMachines__Opportunity__c in :oppIds];        

        List<BigMachines__Quote__Share> quoteSharingRulesToInsert = new List<BigMachines__Quote__Share>();        
        Map<Id, List<BigMachines__Quote__c>> ownerIdToQuotes = new Map<Id, List<BigMachines__Quote__c>>();
        Set<Id> allOppOwnerIds = new Set<Id>();
        List<BigMachines__Quote__c> quotesToChangeOwner = new List<BigMachines__Quote__c>();
        List<Id> quoteOwnerIds = new List<Id>();

        for(BigMachines__Quote__c bmq : quotesWithOpp){
            quoteOwnerIds.add(bmq.OwnerId);
        }

        Map<Id,User> allUsers = new Map<Id,User>([select id, Name, UserRole.Name from User where id in : quoteOwnerIds]);

        for(BigMachines__Quote__c bmq : quotesWithOpp){
            
            //if SAG is current owner, share the quote with that SAG user
            if(allUsers.get(bmq.OwnerId) != null && GeneralUtils.SAGRoleNames.contains(allUsers.get(bmq.OwnerId).UserRole.Name)){

                if(ownerIdToQuotes.containsKey(bmq.OwnerId)){
                    ownerIdToQuotes.get(bmq.OwnerId).add(bmq);
                } else {
                    ownerIdToQuotes.put(bmq.OwnerId,new List<BigMachines__Quote__c>{bmq});
                } 

                //also change the owner to be the opp owner
                bmq.OwnerId = bmq.BigMachines__Opportunity__r.OwnerId;
                quotesToChangeOwner.add(bmq);
            }
        }  

        if(!ownerIdToQuotes.isEmpty()){

            for(Id ownerId : ownerIdToQuotes.keyset()){

                for(BigMachines__Quote__c bmq : ownerIdToQuotes.get(ownerId)){

                    BigMachines__Quote__Share bqs = new BigMachines__Quote__Share(
                        parentId = bmq.Id,
                        UserOrGroupId = ownerId,
                        RowCause = Schema.BigMachines__Quote__Share.RowCause.SAG_Creator__c,
                        AccessLevel = 'Edit'
                    );
                    system.debug('***SP*** bqs: '+ bqs); 
                    quoteSharingRulestoInsert.add(bqs);
                }

            }


        Savepoint sp = Database.setSavepoint();
        try{
            WithoutSharingMethods wsm = new WithoutSharingMethods();
            wsm.insertQuoteSharingRules(quoteSharingRulestoInsert);
        } catch (dmlException de){
            Database.rollback(sp);
            throw new ExInvalidStateException('Error while creating Quote sharing rules: '+de);
        }

        Savepoint sp2 = Database.setSavepoint();
        try{
            WithoutSharingMethods wsm = new WithoutSharingMethods();
            wsm.updateQuoteOwnership(quotesToChangeOwner);
        } catch (dmlException de){
            Database.rollback(sp2);
            throw new ExInvalidStateException('Error while updating Quote ownership to Oopportunity owner: '+de);
        }
    }

    }  

    public without sharing class WithoutSharingMethods{
        public void insertQuoteSharingRules(List<BigMachines__Quote__Share> quoteSharingRulestoInsert){
            insert quoteSharingRulestoInsert;
        }

        public void updateQuoteOwnership(List<BigMachines__Quote__c> quotesToUpdate){
            update quotesToUpdate;
        }
    }

}