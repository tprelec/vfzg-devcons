public with sharing class ITWBatchExecuteController {

	public ITWBatchExecuteController(ApexPages.StandardSetController ignored) {

	}

	public PageReference callBatch(){
		ITWBatch controller = new ITWBatch();
		database.executebatch(controller);
		Schema.DescribeSObjectResult prefix = ITW_Account_Info__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}
}