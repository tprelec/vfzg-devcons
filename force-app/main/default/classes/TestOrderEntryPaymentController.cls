@isTest
public with sharing class TestOrderEntryPaymentController {
	@TestSetup
	static void makeData() {
		TestUtils.theAdministrator = new User(Id = UserInfo.getUserId());
		MavenDocumentsTestFactory.createMavenDocumentsRecords();
		OrderEntryTestDataFactory.createOpportunityWithAllRelatedRecords();
		OrderEntryTestDataFactory.createOrderEntryAddOns();
		OrderEntryTestDataFactory.createOrderEntryProducts();
		OrderEntryTestDataFactory.createPromotions();
		Id mainProductId;
		Id internetProductId;
		for (OE_Product__c prod : [SELECT Id, Type__c FROM OE_Product__c]) {
			if (prod.Type__c == 'Main') {
				mainProductId = prod.Id;
			} else if (prod.Type__c == 'Internet') {
				internetProductId = prod.Id;
			}
		}
		OrderEntryTestDataFactory.createOEProductBundle(mainProductId, internetProductId, null);
	}

	@isTest
	static void testGetOpportunityData() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		List<Attachment> attPre = [SELECT Id FROM Attachment WHERE ParentId = :opp.Id AND Name = 'OrderEntryData.json'];

		Test.startTest();
		OrderEntryData oed = OrderEntryController.getOrderEntryData(opp.Id);
		Test.stopTest();

		List<Attachment> attPost = [SELECT Id FROM Attachment WHERE ParentId = :opp.Id AND Name = 'OrderEntryData.json'];
		System.assertEquals(oed.opportunityId, opp.Id, 'Wrong opportunity Id.');
		System.assertEquals(oed.accountId, opp.AccountId, 'Wrong account Id.');
		System.assert(attPost.size() > 0, 'OrderEntryData attachment should be generated.');
	}

	@isTest
	static void testGetBankAccountHolder() {
		Contact c = new Contact(LastName = 'Test', Email = 'test@test.com');
		insert c;

		Test.startTest();
		String result = OrderEntryPaymentController.getBankAccountHolder(c.Id);
		Test.stopTest();

		System.assertEquals(result, 'Test', 'Contact name should be returned.');
	}
	@isTest
	static void testGetCountryIbanLength() {
		LG_IBAN__c nlIBAN = new LG_IBAN__c(Name = 'NL', LG_Length__c = 18);
		insert nlIBAN;
		Test.startTest();
		Integer result = OrderEntryPaymentController.getCountryIbanLength('NL');
		Test.stopTest();

		System.assertEquals(result, 18, 'Contact name should be returned.');
	}
	@isTest
	static void testCreateBillingAccount() {
		OrderEntryTestDataFactory.insertMockJsonData(false);

		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
		List<OE_Product__c> oeProducts = [SELECT Id FROM OE_Product__c];
		List<OE_Add_On__c> oeAddons = [SELECT Id FROM OE_Add_On__c];
		List<OrderEntryData.OrderEntryProduct> jsonProducts = new List<OrderEntryData.OrderEntryProduct>();

		OrderEntryData.OrderEntryProduct prod1 = new OrderEntryData.OrderEntryProduct();
		prod1.id = oeProducts[0].Id;
		prod1.type = 'Main';
		jsonProducts.add(prod1);

		OrderEntryData.OrderEntryProduct prod2 = new OrderEntryData.OrderEntryProduct();
		prod2.id = oeProducts[1].Id;
		prod2.type = 'Internet';
		jsonProducts.add(prod2);

		OrderEntryData.OrderEntryProduct prod3 = new OrderEntryData.OrderEntryProduct();
		prod3.id = oeProducts[2].Id;
		prod3.type = 'TV';
		jsonProducts.add(prod3);

		List<OrderEntryData.OrderEntryAddon> jsonAddons = new List<OrderEntryData.OrderEntryAddon>();

		OrderEntryData.OrderEntryAddon addon1 = new OrderEntryData.OrderEntryAddon();
		addon1.bundleName = 'Safe Online';
		addon1.code = 'PD-00060';
		addon1.id = oeAddons[0].Id;
		addon1.name = 'Safe Online';
		addon1.oneOff = 0;
		addon1.parentProduct = oeProducts[1].Id;
		addon1.parentType = 'Internet';
		addon1.quantity = 1;
		addon1.recurring = 0;
		addon1.totalOneOff = 0;
		addon1.totalRecurring = 0;
		addon1.type = 'Internet Security';
		jsonAddons.add(addon1);

		OrderEntryData.OrderEntryAddon addon2 = new OrderEntryData.OrderEntryAddon();
		addon2.bundleName = 'Extra internetpunt via stopcontact';
		addon2.code = 'PD-00095';
		addon2.id = oeAddons[1].Id;
		addon2.name = 'Extra internetpunt via stopcontact';
		addon2.oneOff = 53.72;
		addon2.parentProduct = oeProducts[1].Id;
		addon2.parentType = 'Internet';
		addon2.quantity = 0;
		addon2.recurring = 0;
		addon2.totalOneOff = 0;
		addon2.totalRecurring = 0;
		addon2.type = 'Internet Additional';
		jsonAddons.add(addon2);

		Test.startTest();
		OrderEntryData.Payment payment = OrderEntryPaymentController.createBillingAccount(opp.Id);
		Test.stopTest();

		System.assertEquals('NOORDEINDE', payment.street, 'Billing Account is created.');
	}
}
