public with sharing class CS_CloseCaseController {

    @AuraEnabled
    public static String closeCase(Id recordId) {
        String result = '';
        List<Case> casesToUpdate = new List<Case>();
        List<VF_Contract__c> vfContractsToUpdate = new List<VF_Contract__c>();
        List<Case> cases = [
            SELECT Id,
                Case_Record_Type_Text__c,
                Contract_VF__c,
                Responsible_Quality_Officer__c,
                Contract_VF__r.Responsible_Quality_Officer__c,
                FQC_Result__c,
                FQC_Comment__c,
                Contract_VF__r.Contract_Cleaning_Result__c
            FROM Case
            WHERE Id = :recordId
        ];
        String cleaningResult = null;       

        for (Case c : cases) {
            if (c.Case_Record_Type_Text__c == 'CS_MF_Final_Quality_Check') {
                vfContractsToUpdate.add(new VF_Contract__c(
                    Id = c.Contract_VF__c,
                    Responsible_Quality_Officer__c = c.Responsible_Quality_Officer__c,
                    FQC_Result__c = c.FQC_Result__c,
                    FQC_Comment__c = c.FQC_Comment__c
                ));
            }            
            cleaningResult = (c.Case_Record_Type_Text__c == 'CS_MF_Order_Cleaning')?c.Contract_VF__r.Contract_Cleaning_Result__c:null;
        }

        casesToUpdate.add(new Case(
            Id = recordId,
            Contract_Cleaning_Result__c = cleaningResult,
            Status = 'Closed'
        ));

        try {
            if (!vfContractsToUpdate.isEmpty()) {
                Database.SaveResult[] updateResult2 = Database.update(vfContractsToUpdate);
            }
            Database.SaveResult[] updateResult = Database.update(casesToUpdate);
        } catch (Exception e) {
            return e.getMessage();
        }

        return result;
    }
}