public with sharing class CS_ComDeliveryComponentWrapperDetail {
    public CS_ComDeliveryComponentWrapperDetail(){

    }

    @AuraEnabled
    public String name;

    @AuraEnabled
    public String attributeValue;

    @AuraEnabled
    public Boolean readOnly;

    @AuraEnabled
    public String serviceId;

    @AuraEnabled
    public String regexp;

    @AuraEnabled
    public String uiLabel;

    @AuraEnabled
    public String attributeId;

    @AuraEnabled
    public String inputType;
    
    @AuraEnabled
    public String deliveryComponentId;

    @AuraEnabled
    public List<COM_DeliveryComponentWrapperSelectOption> selectOptions;
}