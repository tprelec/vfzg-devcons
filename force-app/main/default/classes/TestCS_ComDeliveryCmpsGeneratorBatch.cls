@IsTest
private class TestCS_ComDeliveryCmpsGeneratorBatch {
    @IsTest
    static void testBehavior() {
        List<Id> orderIds = new List<Id>();
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            csord__Order_Request__c orderRequest = CS_DataTest.createOrderRequest(true);
            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            testOpp.csordtelcoa__Change_Type__c = 'Change';
            insert testOpp;

            List<csord__Order__c> orderList = CS_DataTest.createOrders(2,'Test Order',testAccount,'Created',orderRequest,testOpp,true);

            for (csord__Order__c orderRecord : orderList) {
                orderIds.add(orderRecord.Id);
            }

            Test.startTest();
            Database.executeBatch(new CS_ComDeliveryComponentsGeneratorBatch(orderIds), 2);
            Test.stopTest();

            List<csord__Order__c> orders = [
                    SELECT Id, csord__status2__c
                    FROM csord__Order__c
                    WHERE Id IN :orderIds
            ];

            for(csord__Order__c orderRecord : orders){
                System.debug('*** orderRecord: ' + JSON.serializePretty(orderRecord));
                System.assertEquals('In Delivery',orderRecord.csord__status2__c);
            }
        }
    }
}