public class FieldSyncMappingController
{	
	//Constructor
	public FieldSyncMappingController(ApexPages.StandardController ignored)
	{
		renderFieldsCheck = false;
	}
	
	//Variables
	public String sourceObject {get;set;}
	public String sourceField {get;set;}
	public String targetObject {get;set;}
	public String targetField {get;set;}
	public Boolean renderFieldsCheck{get;set;}
	
	public void renderFields()
    {
      renderFieldsCheck=true;     
	}
	
	//Used to select on what to sort
    public enum FieldToSort
	{
        Label, Value
    }
	
	//Get list of objects
	public List<SelectOption> getObjects()
	{
		List<SelectOption> options = new List<SelectOption>();
        
		Schema.DescribeFieldResult fieldResult = Field_Sync_Mapping__c.Source_Object__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();        
        for( Schema.PicklistEntry f : ple)
        {
          options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        /*for(Schema.SObjectType f : ProcessInstance.TargetObjectId.getDescribe().getReferenceTo()) {
			if(!f.getDescribe().isCustomSetting()){
				options.add(new SelectOption(f.getDescribe().getName(),f.getDescribe().getName()));
			}
		}*/
   		
        return options;
    }
    
    //Get list of source fields
    public List<SelectOption> getSourceFields()
	{
		List<SelectOption> options = new List<SelectOption>();
		List<String> objects = new list<String>{sourceObject};		
        
        for(Schema.DescribeSObjectResult dsor : Schema.DescribeSObjects(objects)){
        	for(Schema.SObjectField f : dsor.fields.getMap().values()){
				options.add(new SelectOption(f.getDescribe().getName(),f.getDescribe().getName()));
			}
        }
        dosort(options, FieldToSort.Label);
        return options;
    }
    
    //Get list of target fields
    public List<SelectOption> getTargetFields()
	{
		List<SelectOption> options = new List<SelectOption>();
		List<String> objects = new list<String>{targetObject};		
        
        for(Schema.DescribeSObjectResult dsor : Schema.DescribeSObjects(objects)){
        	for(Schema.SObjectField f : dsor.fields.getMap().values()){
				options.add(new SelectOption(f.getDescribe().getName(),f.getDescribe().getName()));
			}
        }
        dosort(options, FieldToSort.Label);
        return options;
    }
	    
    //Sort options list
    public static void doSort(List<Selectoption> opts, FieldToSort sortField)
    {        
        Map<String, Selectoption> mapping = new Map<String, Selectoption>();        
        Integer suffix = 1;
        for (Selectoption opt : opts) {
            if (sortField == FieldToSort.Label) {
                mapping.put((opt.getLabel() + suffix++),opt);
            } else {
                mapping.put((opt.getValue() + suffix++),opt);
            }
        }
        
        List<String> sortKeys = new List<String>();
        sortKeys.addAll(mapping.keySet());
        sortKeys.sort();        
        opts.clear();
        
        for (String key : sortKeys) {
            opts.add(mapping.get(key));
        }
    }
    
    public Pagereference Save(){
    	Field_Sync_Mapping__c fsm = new Field_Sync_Mapping__c(Object_Mapping__c = sourceObject + ' -> ' + targetObject,
    														  Source_Field_Name__c = sourceField,
    														  Target_Field_Name__c = targetField);
    	insert fsm;
    	Pagereference pageRef = New PageReference('/'+fsm.Id);
    	return pageRef;
    }
}