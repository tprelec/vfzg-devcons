@isTest
public class CS_StackTest {
    private static testMethod void initStack() {
        CS_Stack stack = new CS_Stack();
        System.assertNotEquals(null, stack);
    }
    
    private static testMethod void newStackSize() {
        CS_Stack stack = new CS_Stack();
        System.assertEquals(0, stack.size());
    }
    
    private static testMethod void newStackIsEmpty() {
        CS_Stack stack = new CS_Stack();
        System.assertEquals(true, stack.isEmpty());
    }
    
    private static testMethod void stackPush() {
        CS_Stack stack = new CS_Stack();
        String testString = 'testString';
        stack.push(testString);
        System.assertEquals(false, stack.isEmpty());
    }
    
    private static testMethod void stackPop() {
        CS_Stack stack = new CS_Stack();
        String testString = 'testString';
        stack.push(testString);
        stack.pop();
        System.assertEquals(true, stack.isEmpty());
    }
    
    private static testMethod void stackPopEmpty() {
        CS_Stack stack = new CS_Stack();
        
        Boolean exceptionThrown = false;
        try {
            stack.pop();
        } catch (CS_Stack.StackUnderflowException e) {
            exceptionThrown = true;
        }
        
        System.assertEquals(true, exceptionThrown);
    }
    
    private static testMethod void stackPeekEmpty() {
        CS_Stack stack = new CS_Stack();
        Boolean exceptionThrown = false;
        try {
            stack.peek();
        } catch (CS_Stack.StackUnderflowException e) {
            exceptionThrown = true;
        }
        
        System.assertEquals(true, exceptionThrown);
    }
    
    private static testMethod void stackPeekNotEmpty() {
        CS_Stack stack = new CS_Stack();
        Boolean exceptionThrown = false;
        String testString = 'testString';
        stack.push(testString);
        try {
            stack.peek();
        } catch (CS_Stack.StackUnderflowException e) {
            exceptionThrown = true;
        }
        
        System.assertEquals(false, exceptionThrown);
    }
}