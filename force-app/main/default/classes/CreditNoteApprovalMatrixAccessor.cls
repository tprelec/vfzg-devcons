/**
 * @description:    CreditNote_Approval_Matrix__c accessor class
 * @testClass:      TestCreditNoteApprovalMatrixAccessor
 **/
public inherited sharing class CreditNoteApprovalMatrixAccessor {
	private static List<CreditNote_Approval_Matrix__c> allCreditNoteApprovalMatrices;

	static {
		initCreditNoteApprovalMatrixMapping(getCreditNoteApprovalMatrices());
	}

	/**
	 * @description:    Retrieves all credit note approval matrices
	 * @return          List<CreditNote_Approval_Matrix__c> - all credit note approval matrices
	 */
	public static List<CreditNote_Approval_Matrix__c> getAllCreditNoteApprovalMatrices() {
		return allCreditNoteApprovalMatrices;
	}

	/**
	 * @description:    Initializes credit note approval matrix mapping
	 * @param           creditNoteApprovalMatrices - list of credit note
	 * 					approval matrices
	 */
	@TestVisible
	private static void initCreditNoteApprovalMatrixMapping(List<CreditNote_Approval_Matrix__c> creditNoteApprovalMatrices) {
		allCreditNoteApprovalMatrices = creditNoteApprovalMatrices;
	}

	/**
	 * @description:    Retrieves all CreditNote_Approval_Matrix__c records
	 * @return          List<RecordType> - list of CreditNote_Approval_Matrix__c
	 */
	private static List<CreditNote_Approval_Matrix__c> getCreditNoteApprovalMatrices() {
		return [
			SELECT
				Id,
				CreditNoteRecordType__c,
				CreditNoteCreditType__c,
				CreditNoteSubscription__c,
				AmountFrom__c,
				AmountTo__c,
				ApprovalAssignmentRule__c,
				UserApprover__c,
				UserApprover__r.User__c,
				QueueApprover__c,
				Level__c,
				Is_Unanimous_Approval__c
			FROM CreditNote_Approval_Matrix__c
			ORDER BY Level__c ASC, UserApprover__c ASC NULLS LAST
		];
	}
}
