@IsTest
private class TestCS_CustomStepCompleteTaskReschedule {
	@IsTest
	static void testSettingTaskToValueThroughOrchestrator() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();
		String status = 'Closed - Planning Rescheduled';

		System.runAs(simpleUser) {
			Task testTask = CS_DataTest.prepareDataForCustomStepCompleteTask(status, simpleUser);
			CSPOFA__Orchestration_Step__c step1 = [SELECT Id FROM CSPOFA__Orchestration_Step__c WHERE Id = :testTask.CSPOFA__Orchestration_Step__c];

			Test.startTest();
			CS_CustomStepCompleteTaskReschedule customStep = new CS_CustomStepCompleteTaskReschedule();
			customStep.process(new List<CSPOFA__Orchestration_Step__c>{ step1 });
			Test.stopTest();

			List<Task> resultTask = [SELECT Id, Subject, Status FROM Task WHERE Id = :testTask.Id];
			System.assertEquals(status, resultTask[0].Status, 'Status is not the same.');
		}
	}
}
