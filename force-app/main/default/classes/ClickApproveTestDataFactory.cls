@isTest
public with sharing class ClickApproveTestDataFactory {
	public static CSCAP__Click_Approve_Setting__c createClickApproveSettings() {
		CSCAP__Click_Approve_Setting__c settings = new CSCAP__Click_Approve_Setting__c();
		settings.Name = 'Sites Approval setting - Opportunities';
		settings.CSCAP__Approval_Type__c = 'Anyone_Approve';
		settings.CSCAP__Associated_Email_Template_Name__c = 'Ziggo Zakelijk Quote Template';
		settings.CSCAP__From_Email_Address__c = 'zakelijk@ziggo.com';
		settings.CSCAP__Sites_Approval_Title__c = 'Maak een opdracht van uw offerte';
		settings.CSCAP__Status__c = 'Active';
		settings.CSCAP__Task_Owner_Field__c = 'CSCAP__Opportunity__c';
		insert settings;
		return settings;
	}

	public static CSCAP__Customer_Approval__c createApproval(Id opportunityId, Id settingId) {
		CSCAP__Customer_Approval__c approval = new CSCAP__Customer_Approval__c(
			CSCAP__Click_Approve_Setting__c = settingId,
			CSCAP__Opportunity__c = opportunityId
		);
		insert approval;
		return approval;
	}

	public static CSCAP__ClickApprove_Approver__c createApprover(Id approvalId, Id contactId) {
		CSCAP__ClickApprove_Approver__c approver = new CSCAP__ClickApprove_Approver__c(
			CSCAP__Customer_Approval__c = approvalId,
			CSCAP__Contact__c = contactId
		);
		insert approver;
		return approver;
	}
}
