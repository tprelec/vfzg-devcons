@RestResource(urlMapping='/createaccount/*')
global without sharing class BdsRestCreateAccount {
	@HttpPost
	global static void doPost(String kvk, List<ContactClass> contact, List<BanClass> ban, List<SiteClass> site) {
		createAccountWithKvk(kvk, contact, ban, site);
	}

	// WARNING: This is used also in TestBdsRestUploadFiles.
	@TestVisible
	private static void createAccountWithKvk(String kvk, List<ContactClass> contact, List<BanClass> ban, List<SiteClass> site) {
		RestResponse res = RestContext.response;
		if (res == null) {
			res = new RestResponse();
			RestContext.response = res;
			res.addHeader('Content-Type', 'text/json');
			res.statusCode = 200;
		}

		try {
			List<BdsResponseClasses.ValidationError> errFields = BdsRestCreateAccount.validateValues(kvk, contact, ban, site);

			if (errFields.size() > 0) {
				BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
				returnClass.errors = errFields;
				res.statusCode = 400;
				System.debug(JSON.serializePretty(returnClass));
				res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
			} else {
				Boolean errorFound = false;
				BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
				List<BdsResponseClasses.ValidationError> errFieldsList = new List<BdsResponseClasses.ValidationError>();

				List<Account> accList = [SELECT Id FROM Account WHERE KVK_number__c = :kvk];
				Id accId;
				if (accList.size() == 1) {
					accId = accList[0].Id;
				} else {
					accId = BdsRestCreateAccount.createAccount(kvk);
				}

				if (accId != null) {
					Boolean primarycontactselected = false;
					Id accPrimaryContact;

					if (contact != null) {
						for (ContactClass cc : contact) {
							ContactResult conClass = BdsRestCreateAccount.createContact(cc, accId);
							if (!conClass.errorFound) {
								if (!primarycontactselected) {
									accPrimaryContact = conClass.Id;
									primarycontactselected = true;
								}
							} else {
								errorFound = true;
								BdsResponseClasses.ValidationError err = new BdsResponseClasses.ValidationError('Contact Error', conClass.errorMsg);
								errFieldsList.add(err);
							}
						}
					}

					if (site != null) {
						List<Site__c> siteList = new List<Site__c>();
						for (SiteClass st : site) {
							Site__c requestedSite = BdsRestCreateAccount.createSite(accId, kvk, st);
							if (requestedSite != null)
								siteList.add(requestedSite);
						}
						siteList = filterOnExistingSitesForAccount(accId, siteList);
						try {
							insert siteList;
						} catch (DMLException e) {
							System.debug('BdsRestCreateAccount - error on sites insert : ' + e.getMessage());
							errorFound = true;
							BdsResponseClasses.ValidationError err = new BdsResponseClasses.ValidationError('Site Error', e.getMessage());
							errFieldsList.add(err);
						}
					}

					if (ban != null) {
						for (BanClass bn : ban) {
							Id banId = BdsRestCreateAccount.createBan(bn.financialcontact, accId);
						}
					}

					/** Add primary contact contact to account record */
					Account tmpAcc = [SELECT Id, Authorized_to_sign_1st__c, Frame_Work_Agreement__c FROM Account WHERE Id = :accId];
					if (accPrimaryContact != null) {
						if (tmpAcc.Authorized_to_sign_1st__c != accPrimaryContact) {
							tmpAcc.Authorized_to_sign_1st__c = accPrimaryContact;
							update tmpAcc;
						}
					}
					if (String.isEmpty(tmpAcc.Frame_Work_Agreement__c)) {
						tmpAcc.Frame_Work_Agreement__c = '0123456789';
						update tmpAcc;
					}
					if (!errorFound) {
						AccountClass accClass = BdsRestCreateAccount.getAccount(kvk);
						res.responseBody = Blob.valueOf(JSON.serializePretty(accClass));
					} else {
						returnClass.errors = errFieldsList;
						String sInspectError = JSON.serializePretty(returnClass);
						res.statusCode = 400;
						res.responseBody = Blob.valueOf(sInspectError);
					}
				} else {
					returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
					errFieldsList = new List<BdsResponseClasses.ValidationError>();
					BdsResponseClasses.ValidationError err = new BdsResponseClasses.ValidationError('KVK', 'No company information found');
					errFieldsList.add(err);
					returnClass.errors = errFieldsList;
					res.statusCode = 400;
					res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
				}
			}
		} catch (Exception e) {
			BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
			List<BdsResponseClasses.ValidationError> errFieldsList = new List<BdsResponseClasses.ValidationError>();
			BdsResponseClasses.ValidationError err = new BdsResponseClasses.ValidationError('Error while processing data', e.getMessage());
			System.debug('Error in webservice  -- ' + e.getMessage());
			errFieldsList.add(err);
			returnClass.errors = errFieldsList;
			res.statusCode = 400;
			res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
		}
	}

	@testVisible
	private static List<Site__c> filterOnExistingSitesForAccount(Id accId, List<Site__c> requestedSites) {
		if (accId == null || requestedSites.size() == 0) {
			return requestedSites;
		}
		List<Site__c> existingSites = [
			SELECT Id, Site_Postal_Code__c, Site_House_Number__c, Site_House_Number_Suffix__c
			FROM Site__c
			WHERE Site_Account__c = :accId
		];
		List<Site__c> sitesToReturn = new List<Site__c>();
		if (existingSites.isEmpty()) {
			return requestedSites;
		}

		for (Site__c requestedSite : requestedSites) {
			Boolean requestedSiteDoesNotExist = true;
			for (Site__c existingSite : existingSites) {
				if (String.isBlank(existingSite.Site_House_Number_Suffix__c) && String.isBlank(requestedSite.Site_House_Number_Suffix__c)) {
					if (
						requestedSite.Site_House_Number__c == existingSite.Site_House_Number__c &&
						requestedSite.Site_Postal_Code__c == existingSite.Site_Postal_Code__c
					) {
						requestedSiteDoesNotExist = false;
						continue;
					}
				}
				if (
					requestedSite.Site_House_Number__c == existingSite.Site_House_Number__c &&
					requestedSite.Site_House_Number_Suffix__c == existingSite.Site_House_Number_Suffix__c &&
					requestedSite.Site_Postal_Code__c == existingSite.Site_Postal_Code__c
				) {
					requestedSiteDoesNotExist = false;
				}
			}
			if (requestedSiteDoesNotExist) {
				sitesToReturn.add(requestedSite);
			}
		}
		return sitesToReturn;
	}

	public static AccountClass getAccount(String kvk) {
		BdsRestCreateAccount.AccountClass accClass = new BdsRestCreateAccount.AccountClass();
		Account acc = [
			SELECT
				Id,
				Name,
				Authorized_to_sign_1st__c,
				VAT_Number__c,
				Frame_Work_Agreement__c,
				VZ_Framework_Agreement__c,
				Date_of_Establishment__c,
				Framework_agreement_date__c,
				VZ_Framework_agreement_date__c,
				LG_VisitCountry__c,
				LG_VisitPostalCode__c,
				LG_VisitHouseNumber__c,
				LG_VisitHouseNumberExtension__c,
				LG_VisitStreet__c,
				LG_VisitCity__c,
				(SELECT Id, Name, Corporate_Id__c FROM BAN_Informations__r WHERE CreatedById = :UserInfo.getUserId()),
				(SELECT Id, FirstName, LastName, Email, Phone, MobilePhone, Gender__c FROM Contacts),
				(
					SELECT
						Id,
						Location_Type__c,
						Postal_Code_Check_Status__c,
						Site_Street__c,
						Site_House_Number__c,
						Site_House_Number_Suffix__c,
						Site_Postal_Code__c,
						Site_City__c,
						Country__c,
						Canvas_Country__c
					FROM Sites__r
				)
			FROM Account
			WHERE KVK_number__c = :kvk
		];
		accClass.kvk = kvk;
		accClass.name = acc.Name;
		accClass.agreementNumber = acc.Frame_Work_Agreement__c;
		accClass.vzagreementNumber = acc.VZ_FrameWork_Agreement__c; //SFO-1119
		accClass.vatNumber = acc.VAT_Number__c;
		accClass.dateOfEstablishment = acc.Date_of_Establishment__c;
		accClass.masterContractCreationDate = acc.Framework_agreement_date__c;
		accClass.vzmasterContractCreationDate = acc.VZ_Framework_agreement_date__c; //SFO-1119
		accClass.country = acc.LG_VisitCountry__c;
		accClass.postalCode = acc.LG_VisitPostalCode__c;
		accClass.houseNumber = acc.LG_VisitHouseNumber__c;
		accClass.houseNumberExtension = acc.LG_VisitHouseNumberExtension__c;
		accClass.street = acc.LG_VisitStreet__c;
		accClass.city = acc.LG_VisitCity__c;

		for (Contact con : acc.Contacts) {
			BdsRestCreateAccount.ContactClass conClass = new BdsRestCreateAccount.ContactClass();
			conClass.email = con.Email;
			conClass.phone = con.Phone;
			conClass.mobilephone = con.MobilePhone;
			conClass.lastname = con.LastName;
			conClass.firstname = con.FirstName;
			conClass.gender = con.Gender__c;
			if (acc.Authorized_to_sign_1st__c == con.Id) {
				conClass.isprimary = 'true';
			} else {
				conClass.isprimary = 'false';
			}
			accClass.con.add(conClass);
		}
		Map<Id, Ban__c> banMap = new Map<Id, Ban__c>();
		for (Ban__c ban : acc.BAN_Informations__r) {
			banMap.put(ban.Id, ban);
		}
		List<Financial_Account__c> faList = [
			SELECT Id, BAN__c, Financial_Contact__r.Email
			FROM Financial_Account__c
			WHERE BAN__c IN :banMap.keySet()
		];
		for (Financial_Account__c fa : faList) {
			BdsRestCreateAccount.BanClass banC = new BdsRestCreateAccount.BanClass();
			banC.financialcontact = fa.Financial_Contact__r.Email;
			Ban__c b = banMap.get(fa.BAN__c);
			banC.ban = b.Name;
			banC.corporateId = b.Corporate_Id__c;
			accClass.ban.add(banC);
		}

		Map<Id, List<Site_Postal_Check__c>> zipcodeSiteMap = new Map<Id, List<Site_Postal_Check__c>>();
		Map<Id, Site__c> siteMap = new Map<Id, Site__c>(acc.Sites__r);

		if (!siteMap.keySet().isEmpty()) {
			List<Site_Postal_Check__c> zipcodeCheckList = [
				SELECT
					Id,
					Access_Vendor__c,
					Access_Site_ID__r.Id,
					Access_Max_Up_Speed__c,
					Access_Max_Down_Speed__c,
					Technology_Type__c,
					Accessclass__c,
					Access_Infrastructure__c,
					Access_Active_and_Usable__c,
					Access_Priority__c,
					Access_Region__c
				FROM Site_Postal_Check__c
				WHERE Access_Site_ID__r.Id IN :siteMap.keySet() AND Access_Active_and_Usable__c = TRUE
			];
			for (Site_Postal_Check__c spc : zipcodeCheckList) {
				if (zipcodeSiteMap.containsKey(spc.Access_Site_ID__r.Id)) {
					List<Site_Postal_Check__c> tmpList = zipcodeSiteMap.get(spc.Access_Site_ID__r.Id);
					tmpList.add(spc);
					zipcodeSiteMap.put(spc.Access_Site_ID__r.Id, tmpList);
				} else {
					zipcodeSiteMap.put(spc.Access_Site_ID__r.Id, new List<Site_Postal_Check__c>{ spc });
				}
			}
		}

		for (Site__c st : acc.Sites__r) {
			BdsRestCreateAccount.SiteClass siteC = new BdsRestCreateAccount.SiteClass();
			siteC.street = st.Site_Street__c;
			siteC.housenumber = Integer.valueOf(st.Site_House_Number__c);
			siteC.housenumberExt = st.Site_House_Number_Suffix__c;
			siteC.zipcode = st.Site_Postal_Code__c;
			siteC.city = st.Site_City__c;
			siteC.postalCodeCheckStatus = st.Postal_Code_Check_Status__c;
			siteC.country = st.Country__c;
			siteC.locationType = st.Location_Type__c;
			if (zipcodeSiteMap.containsKey(st.Id)) {
				List<Site_Postal_Check__c> zipcodeCHeckList = zipcodeSiteMap.get(st.Id);
				siteC.PostalcodeCheck = new List<BdsRestCreateAccount.SitePostalCodeCheckClass>();
				for (Site_Postal_Check__c spc : zipcodeCHeckList) {
					BdsRestCreateAccount.SitePostalCodeCheckClass pcc = new BdsRestCreateAccount.SitePostalCodeCheckClass();
					pcc.vendor = spc.Access_Vendor__c;
					pcc.accessMaxUpSpeed = Integer.ValueOf(spc.Access_Max_Up_Speed__c);
					pcc.accessMaxDownSpeed = Integer.ValueOf(spc.Access_Max_Down_Speed__c);
					pcc.technologyType = spc.Technology_Type__c;
					pcc.accessClass = spc.Accessclass__c;
					pcc.accessInfrastructure = spc.Access_Infrastructure__c;
					pcc.accessRegion = spc.Access_Region__c;
					if (spc.Access_Priority__c != null) {
						pcc.accessPriority = Integer.valueOf(spc.Access_Priority__c);
					}
					siteC.PostalcodeCheck.add(pcc);
				}
			}

			accClass.site.add(siteC);
		}
		return accClass;
	}

	public class AccountClass {
		public String kvk;
		public String name;
		public String vatNumber;
		public String agreementNumber;
		public String vzagreementNumber; //SFO-1119
		public Date dateOfEstablishment;
		public Date masterContractCreationDate;
		public Date vzmasterContractCreationDate; //SFO-1119
		public String country;
		public String postalCode;
		public String houseNumber;
		public String houseNumberExtension;
		public String street;
		public String city;
		public List<BdsRestCreateAccount.ContactClass> con = new List<BdsRestCreateAccount.ContactClass>();
		public List<BdsRestCreateAccount.BanClass> ban = new List<BdsRestCreateAccount.BanClass>();
		public List<BdsRestCreateAccount.SiteClass> site = new List<BdsRestCreateAccount.SiteClass>();
	}

	public static Id createAccount(String kvk) {
		Id returnId;
		OlbicoServiceJSON jService = new OlbicoServiceJSON();
		jService.setRequestRestJson(kvk);
		jService.makeReqestRestJson(kvk);

		if (jService.getAcc() != null) {
			returnId = jService.getAcc().Id;
			System.debug(LoggingLevel.DEBUG, '##jService.getAcc(): ' + jService.getAcc());
		}
		return returnId;
	}

	public static Id createBan(String financialContactEmail, Id accId) {
		Id returnId;
		/** Check if already a BAN Exists */
		List<Financial_Account__c> faList = [
			SELECT Id
			FROM Financial_Account__c
			WHERE BAN__r.Account__c = :accId AND Financial_Contact__r.Email = :financialContactEmail
		];
		if (faList.size() == 0) {
			/** Account Info */
			Account acc = [SELECT Id, Name FROM Account WHERE Id = :accId];

			Ban__c ban = new Ban__c();
			String banName = 'Requested New Unify BAN ';
			banName += String.valueOf(Date.Today().Year());
			banName += '-';
			if (Date.Today().Month() < 10) {
				banName += '0';
			}
			banName += String.valueOf(Date.Today().Month());
			banName += '-';
			if (Date.Today().Day() < 10) {
				banName += '0';
			}
			banName += String.valueOf(Date.Today().Day());
			banName += ' ';
			Datetime dt = Datetime.now();
			if (dt.Hour() < 10) {
				banName += '0';
			}
			banName += String.valueOf(dt.Hour());
			banName += ':';
			if (dt.Minute() < 10) {
				banName += '0';
			}
			banName += String.valueOf(dt.Minute());
			banName += ':';
			if (dt.Second() < 10) {
				banName += '0';
			}
			banName += String.valueOf(dt.Second());
			ban.Name = banName;
			ban.BAN_Status__c = 'Requested';
			ban.Account__c = acc.Id;
			ban.BAN_Name__c = acc.Name;
			ban.Unify_Customer_SubType__c = 'BU';
			ban.Unify_Customer_Type__c = 'B';
			insert ban;

			if (!String.isEmpty(ban.Id)) {
				List<Contact> conList = [SELECT Id FROM Contact WHERE AccountId = :accId AND Email = :financialContactEmail];
				if (conList != null && conList.size() == 1) {
					Financial_Account__c fa = new Financial_Account__c();
					fa.BAN__c = ban.Id;
					fa.Financial_Contact__c = conList[0].Id;
					insert fa;
				}
			}
		}
		return returnId;
	}

	public static ContactResult createContact(BdsRestCreateAccount.ContactClass cc, Id accId) {
		ContactResult returnClass;
		if (cc != null) {
			Boolean conExists = false;
			try {
				Contact conSearch = [SELECT Id FROM Contact WHERE Email = :cc.email AND AccountId = :accId];
				conExists = true;

				if (!String.isEmpty(cc.firstname)) {
					conSearch.FirstName = cc.firstname;
				}
				if (!String.isEmpty(cc.lastname)) {
					conSearch.LastName = cc.lastname;
				}
				if (!String.isEmpty(cc.email)) {
					conSearch.Email = cc.email;
				}
				if (!String.isEmpty(cc.phone)) {
					conSearch.Phone = cc.phone;
				}
				if (!String.isEmpty(cc.mobilephone)) {
					conSearch.Mobilephone = cc.mobilephone;
				}
				if (!String.isEmpty(cc.gender)) {
					conSearch.Gender__c = cc.gender;
				}
				if (!String.isEmpty(cc.updateemail)) {
					if (cc.updateemail != cc.email) {
						conSearch.Email = cc.updateemail;
					}
				}
				update conSearch;
				returnClass = new ContactResult(conSearch.Id, false, null);
			} catch (Exception e) {
				System.debug(LoggingLevel.DEBUG, e.getMessage() + e.getStackTraceString());
				returnClass = new ContactResult(null, true, e.getMessage());
			}

			if (!conExists) {
				Contact con = new Contact();
				con.AccountId = accId;
				if (!String.isEmpty(cc.firstname)) {
					con.FirstName = cc.firstname;
				}
				if (!String.isEmpty(cc.lastname)) {
					con.LastName = cc.lastname;
				}
				if (!String.isEmpty(cc.email)) {
					con.Email = cc.email;
				}
				if (!String.isEmpty(cc.phone)) {
					con.Phone = cc.phone;
				}
				if (!String.isEmpty(cc.mobilephone)) {
					con.Mobilephone = cc.mobilephone;
				}
				if (!String.isEmpty(cc.gender)) {
					con.Gender__c = cc.gender;
				}
				try {
					insert con;
					returnClass = new ContactResult(con.Id, false, null);
				} catch (Exception e) {
					returnClass = new ContactResult(null, true, e.getMessage());
				}
			}
		}
		return returnClass;
	}

	public static Site__c createSite(Id accId, String kvk, BdsRestCreateAccount.SiteClass site) {
		Site__c st;
		if (site != null) {
			st = new Site__c();
			st.Site_Account__c = accId;
			st.KVK_Number__c = kvk;
			st.Site_Street__c = site.street;
			st.Site_House_Number__c = site.housenumber;
			st.Site_House_Number_Suffix__c = site.housenumberExt;
			st.Site_Postal_Code__c = site.zipcode;
			st.Site_City__c = site.city;
			st.Olbico__c = true;
			st.Name = site.street;
			if (!String.isEmpty(site.country)) {
				st.Country__c = site.country;
				st.Canvas_Country__c = site.country;
			}
		}
		return st;
	}

	public static List<BdsResponseClasses.ValidationError> validateValues(
		String kvk,
		List<ContactClass> contact,
		List<BanClass> ban,
		List<SiteClass> site
	) {
		List<BdsResponseClasses.ValidationError> errFields = new List<BdsResponseClasses.ValidationError>();

		if (String.isEmpty(kvk)) {
			BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('kvk', 'kvk Missing');
			errFields.add(errField);
		}

		if (contact == null) {
			/** Contact can only be empty when there is already a contact available */
			if (!BdsRestCreateAccount.hasAccountContact(kvk)) {
				BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('contact', 'contact missing');
				errFields.add(errField);
			}
		} else {
			if (contact.size() == 0) {
				/** Contact can only be empty when there is already a contact available */
				if (!BdsRestCreateAccount.hasAccountContact(kvk)) {
					BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError(
						'contact',
						'Contact missing, contact info can only be empty when account has no contacts'
					);
					errFields.add(errField);
				}
			}
		}
		if (site != null) {
			if (site.size() > 0) {
				Integer i = 0;
				for (SiteClass st : site) {
					if (String.isEmpty(st.street)) {
						BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError(
							'site[' +
							i +
							'].street',
							'street missing'
						);
						errFields.add(errField);
					}
					if (String.isEmpty(st.zipcode)) {
						BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError(
							'site[' +
							i +
							'].zipcode',
							'zipcode missing'
						);
						errFields.add(errField);
					}
					if (String.isEmpty(st.city)) {
						BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('site[' + i + '].city', 'city missing');
						errFields.add(errField);
					}
					i += 1;
				}
			}
		}

		return errFields;
	}

	public static Boolean hasAccountContact(String kvk) {
		Boolean returnBool = false;
		if (!String.isEmpty(kvk)) {
			List<Account> accList = [SELECT Id, (SELECT Id FROM Contacts) FROM Account WHERE KVK_number__c = :kvk];
			if (accList.size() > 0) {
				List<Contact> conList = accList[0].Contacts;
				if (conList.size() > 0) {
					returnBool = true;
				}
			}
		}
		return returnBool;
	}

	public class ContactResult {
		public Id id { get; set; }
		public Boolean errorFound {
			get {
				if (errorFound == null) {
					errorFound = true;
				}
				return errorFound;
			}
			private set;
		}
		public String errorMsg { get; set; }

		public ContactResult(Id id, Boolean error, String msg) {
			if (!String.isEmpty(Id)) {
				this.Id = Id;
			}
			errorFound = error;
			if (!String.isEmpty(msg)) {
				this.errorMsg = msg;
			}
		}
	}

	global class ContactClass {
		public String email;
		public String firstname;
		public String lastname;
		public String phone;
		public String mobilephone;
		public String isprimary;
		public String gender;
		public String updateemail;
	}

	global class BanClass {
		public String financialcontact;
		public String ban;
		public String corporateId;
	}

	global class SiteClass {
		public String street;
		public Integer housenumber;
		public String housenumberExt;
		public String zipcode;
		public String city;
		public String postalCodeCheckStatus;
		public String country;
		public String locationType;
		public List<SitePostalCodeCheckClass> postalcodeCheck;
	}

	global class SitePostalCodeCheckClass {
		public String vendor;
		public Integer accessMaxUpSpeed;
		public Integer accessMaxDownSpeed;
		public String technologyType;
		public String accessClass;
		public String accessInfrastructure;
		public String accessRegion;
		public Integer accessPriority;
		public String accessResultCheck;
		public String remarks;
	}
}
