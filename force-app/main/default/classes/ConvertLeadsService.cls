public without sharing class ConvertLeadsService {
	private List<Lead> leads;
	private Map<Id, Lead> leadsMap;
	private List<Database.LeadConvertResult> leadConvertResults;
	private Map<Id, Site__c> leadIdToSite;
	private Map<Id, OpportunityContactRole> leadIdToContactRole;
	private Map<String, Account> accsWithConsByKVK;
	public class InvalidKVKException extends Exception {
	}
	/**
	 * @description constructor
	 * @param  leads List<Lead>
	 */
	public ConvertLeadsService(List<Lead> leads) {
		this.leads = leads;
		this.leadsMap = new Map<Id, Lead>();
		for (Lead l : leads) {
			leadsMap.put(l.Id, l);
		}
	}

	/**
	 * @description Convert Lead and create related records
	 * @param  process String
	 * @return List<Database.LeadConvertResult> result of lead conversion
	 */
	public List<Database.LeadConvertResult> convertLeads(String process) {
		convertLeads();
		if (process == 'Ziggo') {
			handleRelatedZiggoRecords();
		}
		return leadConvertResults;
	}

	/**
	 * @description Convert Lead and create related records
	 * @return List<Database.LeadConvertResult> result of lead conversion
	 */
	public List<Database.LeadConvertResult> convertLeads() {
		getAccountsWithContacts();
		List<Database.LeadConvert> leadConverts = new List<Database.LeadConvert>();
		for (Lead l : leads) {
			l.Workflow_Cancel__c = true;
			Account acc = accsWithConsByKVK.get(l.KVK_number__c);
			Contact con = getMatchingContact(l, acc?.Contacts);

			if (acc == null) {
				OlbicoServiceJSON jService = new OlbicoServiceJSON();
				jService.setRequestRestJson(l.KVK_number__c);
				jService.makeReqestRestJson(l.KVK_number__c);
				acc = jService.getAcc();
				con = getMatchingContact(l, jService.getContacts());
				System.debug(acc);
			}

			leadConverts.add(buildLeadConvert(l, acc, con));
		}
		update leads;
		leadConvertResults = Database.convertLead(leadConverts);
		return leadConvertResults;
	}

	/**
	 * @description get Accounts record for Leads KVK
	 */
	private void getAccountsWithContacts() {
		Set<String> kvkNumbers = GeneralUtils.getStringSetFromList(leads, 'KVK_number__c');
		// Get Accounts and Contacts
		try {
			List<Account> accounts = [SELECT Id, KVK_number__c, (SELECT Id, Email FROM Contacts) FROM Account WHERE KVK_number__c IN :kvkNumbers];
			accsWithConsByKVK = new Map<String, Account>();
			for (Account acc : accounts) {
				accsWithConsByKVK.put(acc.KVK_number__c, acc);
			}
		} catch (Exception e) {
			System.debug('Exception has occured: ' + e.getMessage());
		}
	}

	/**
	 * @description matching Contac record and Lead Record
	 * @param Lead l
	 * @param List<Contact> contacts
	 */
	private Contact getMatchingContact(Lead l, List<Contact> contacts) {
		if (contacts == null) {
			return null;
		}
		for (Contact c : contacts) {
			if (l.Email != null && c.Email != null && l.Email.toLowerCase() == c.Email.toLowerCase()) {
				return c;
			}
		}
		return null;
	}
	/**
	 * @description build LeadConvert record
	 * @param Lead lead
	 * @param Account acc
	 * @param Account con
	 */

	private Database.LeadConvert buildLeadConvert(Lead lead, Account acc, Contact con) {
		Database.LeadConvert leadConvert = new Database.LeadConvert();
		leadConvert.setOwnerId(UserInfo.getUserId());
		leadConvert.setLeadId(lead.Id);
		leadConvert.setConvertedStatus(leadConvertStatus.MasterLabel);

		// Attach to existing Account and Contact if it was found
		if (acc != null) {
			leadConvert.setAccountId(acc.Id);
		}
		if (con != null) {
			leadConvert.setContactId(con.Id);
		}

		return leadConvert;
	}

	private static LeadStatus leadConvertStatus {
		private get {
			if (leadConvertStatus == null) {
				return [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1];
			}
			return leadConvertStatus;
		}
	}

	private void handleRelatedZiggoRecords() {
		handleOpportunityContactRoles();
		handleSites();
		handleOpportunities();
	}

	/**
	 * @description Insert new and update existing OpportunityContacRoles
	 */
	private void handleOpportunityContactRoles() {
		leadIdToContactRole = new Map<Id, OpportunityContactRole>();
		// Get all Contact Roles related to Opportunitites
		Set<Id> oppIds = new Set<Id>();
		for (Database.LeadConvertResult lcr : leadConvertResults) {
			oppIds.add(lcr.getOpportunityId());
		}
		List<OpportunityContactRole> allContactRoles = [
			SELECT Id, OpportunityId, ContactId, Role
			FROM OpportunityContactRole
			WHERE OpportunityId IN :oppIds
		];
		// Create new Roles or update existing
		List<OpportunityContactRole> contactRolesForUpsert = new List<OpportunityContactRole>();
		for (Database.LeadConvertResult lcr : leadConvertResults) {
			OpportunityContactRole contactRole;
			for (OpportunityContactRole ocr : allContactRoles) {
				if (lcr.getOpportunityId() == ocr.OpportunityId && lcr.getContactId() == ocr.ContactId) {
					contactRole = ocr;
					break;
				}
			}
			if (contactRole == null) {
				contactRole = new OpportunityContactRole(OpportunityId = lcr.getOpportunityId(), ContactId = lcr.getContactId());
			}
			contactRole.Role = 'Administrative Contact';
			contactRole.IsPrimary = true;
			contactRolesForUpsert.add(contactRole);
			leadIdToContactRole.put(lcr.getLeadId(), contactRole);
		}
		upsert contactRolesForUpsert;
	}

	/**
	 * @description creating new sites
	 */
	private void handleSites() {
		leadIdToSite = new Map<Id, Site__c>();
		// Get all Sites related to Accounts
		Set<Id> accountIds = new Set<Id>();
		for (Database.LeadConvertResult lcr : leadConvertResults) {
			accountIds.add(lcr.getAccountId());
		}
		List<Site__c> allSites = [SELECT Id, Site_Unique_ID__c, Site_House_Number_Suffix__c FROM Site__c WHERE Site_Account__c IN :accountIds];
		Map<String, Site__c> allSitesMap = new Map<String, Site__c>();
		for (Site__c site : allSites) {
			allSitesMap.put(site.Site_Unique_ID__c, site);
		}

		// Create new Sites if they don't exist already
		List<Site__c> newSites = new List<Site__c>();
		for (Database.LeadConvertResult lcr : leadConvertResults) {
			Lead l = leadsMap.get(lcr.getLeadId());

			String siteKey = buildSiteKey(l);
			Site__c site = allSitesMap.get(siteKey);

			if (site == null) {
				site = new Site__c(
					Site_Account__c = lcr.getAccountId(),
					KVK_Number__c = l.KVK_number__c,
					Active__c = true,
					Site_Street__c = l.LG_VisitStreet__c,
					Site_House_Number__c = Decimal.valueof(l.LG_VisitHouseNumber__c),
					Site_House_Number_Suffix__c = l.LG_VisitHouseNumberExtension__c,
					Site_Postal_Code__c = clearPostcode(l.Visiting_postalcode_merged_input__c),
					Site_City__c = l.LG_VisitCity__c,
					Country__c = l.Visiting_Country__c
				);
				newSites.add(site);
			}

			leadIdToSite.put(l.Id, site);
		}
		if (!newSites.isEmpty()) {
			insert newSites;
		}
	}

	private String buildSiteKey(Lead l) {
		// Build Site Key
		String siteKey = '';
		siteKey += nullSafeString(l.KVK_number__c);
		siteKey += clearPostcode(l.Visiting_postalcode_merged_input__c);
		siteKey += nullSafeString(String.valueOf(l.LG_VisitHouseNumber__c)).substringBefore('.');
		siteKey += nullSafeString(l.LG_VisitHouseNumberExtension__c);
		return siteKey;
	}

	private String nullSafeString(String s) {
		if (s == null) {
			return '';
		} else {
			return s;
		}
	}

	private String clearPostcode(String postcode) {
		return nullSafeString(postcode).toUpperCase().replaceAll(' ', '');
	}

	/**
	 * @description Update opportuniti with related data and create OrderEntry Attachment
	 */
	private void handleOpportunities() {
		List<Opportunity> opps = new List<Opportunity>();
		List<Attachment> atts = new List<Attachment>();
		Set<Id> oppIds = new Set<Id>();
		Id ziggoOppRecordTypeId = GeneralUtils.getRecordTypeIdByName('Opportunity', 'Ziggo');

		for (Database.LeadConvertResult lcr : leadConvertResults) {
			Opportunity opp = new Opportunity(Id = lcr.getOpportunityId());
			oppIds.add(lcr.getOpportunityId());
			opp.RecordTypeId = ziggoOppRecordTypeId;
			opp.LG_PrimaryContact__c = lcr.getContactId();
			opp.StageName = 'Awareness of interest';
			opp.CloseDate = Date.today();
			opps.add(opp);

			Id leadId = lcr.getLeadId();
			OrderEntryData oed = new OrderEntryData();
			oed.accountId = lcr.getAccountId();
			oed.opportunityId = lcr.getOpportunityId();
			oed.primaryContactId = leadIdToContactRole.get(leadId)?.ContactId;
			if (leadIdToSite.get(leadId)?.Id != null) {
				try {
					List<OrderEntryData.OrderEntrySite> sites = new List<OrderEntryData.OrderEntrySite>();
					OrderEntryData.OrderEntrySite site = new OrderEntryData.OrderEntrySite();
					site.siteId = leadIdToSite.get(leadId).Id;
					site.type = 'Installation';
					sites.add(site);
					oed.sites = sites;
				} catch (NullPointerException e) {
					System.debug(LoggingLevel.Error, 'Error while assingning a site ' + e);
				}
			}
			oed.b2cInternetCustomer = leadsMap.get(leadId).LG_InternetCustomer__c;
			oed.init();

			Attachment att = new Attachment(Name = 'OrderEntryData.json', Body = Blob.valueOf(JSON.serializePretty(oed)), ParentId = opp.Id);
			atts.add(att);
		}
		update opps;
		insert atts;
	}
}
