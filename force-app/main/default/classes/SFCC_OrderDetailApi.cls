/*
	Author: Rahul Sharma
	Description: Order Detail API, Salesforce commerce cloud uses this API to retrieve order details
	Jira ticket: COM-3242
*/

@SuppressWarnings('PMD.ExcessivePublicCount') // excessive params are needed for business logic
@RestResource(urlMapping='/orders/getDetails')
global with sharing class SFCC_OrderDetailApi {
	private static final String CHARGE_TYPE_RECURRING = 'Recurring charge';
	private static final String CHARGE_TYPE_ONE_OFF = 'One off charge';
	private static final String SERVICE_LINE_TYPE_DISCOUNT_CHARGE = 'Discount Charge';

	@TestVisible
	private static final Integer SUCCESS_CODE_200 = 200;
	@TestVisible
	private static final Integer SUCCESS_CODE_400 = 400;

	/*
	 * HTTP post method to get order details based on orderId passed via RestContext.request
	 */
	@HttpPost
	global static void getDetail() {
		RequestWrapper request = parseRequestData();

		// in case of bad request, send appropriate status code
		if (request == null) {
			addDataToResponse(SUCCESS_CODE_400, new ResponseWrapper(System.Label.SFCC_OrderApi_UnableToParseRequestMessage));
			return;
		}

		try {
			// send error message when orderId is invalid
			if (
				String.isEmpty(request.orderId) ||
				!(request.orderId instanceof Id) ||
				!(Id.valueOf(request.orderId).getSObjectType() == Order__c.SObjectType ||
				Id.valueOf(request.orderId).getSObjectType() == csord__Order__c.SObjectType)
			) {
				addDataToResponse(SUCCESS_CODE_400, new ResponseWrapper(System.Label.SFCC_OrderApi_InvalidOrderIdMessage));
				return;
			}

			// get order based on orderId and send in respose body
			OrderWrapper order = getOrder(request);
			ResponseWrapper responseWrapper = new ResponseWrapper(order);
			addDataToResponse(SUCCESS_CODE_200, responseWrapper);
		} catch (NoOrderFoundException objNoOrderFoundException) {
			addDataToResponse(SUCCESS_CODE_400, new ResponseWrapper(objNoOrderFoundException.getMessage()));
		} catch (Exception objException) {
			LoggerService.log(LoggingLevel.DEBUG, '@@@@objException.getStackTraceString(): ' + objException.getStackTraceString());
			addDataToResponse(SUCCESS_CODE_400, new ResponseWrapper(System.label.SFCC_OrderApi_UnhandledExceptionMessage));
		}
	}

	private static RequestWrapper parseRequestData() {
		try {
			String requestString = RestContext.request.requestBody.toString();
			LoggerService.log(LoggingLevel.DEBUG, '@@@@requestString: ' + requestString);
			return (RequestWrapper) JSON.deserialize(requestString, RequestWrapper.class);
		} catch (Exception objException) {
			return null;
		}
	}

	/*
	 * Add and send status code and body to RestContext.response
	 */
	private static void addDataToResponse(Integer statusCode, ResponseWrapper responseWrapper) {
		String data = JSON.serialize(responseWrapper);

		if (statusCode != SUCCESS_CODE_200) {
			LoggerService.log(LoggingLevel.ERROR, RestContext.request.requestBody.toString() + ' - ' + data);
		}

		RestContext.response.statusCode = statusCode;
		RestContext.response.responseBody = Blob.valueOf(data);
	}

	/*
	 * get order response based on request parameter
	 */
	private static OrderWrapper getOrder(RequestWrapper request) {
		if (Id.valueOf(request.orderId).getSObjectType() == Order__c.sObjectType) {
			return getLegacyOrder(request);
		} else {
			return getCSOrder(request);
		}
	}

	/*
	 * return response for legacy order
	 */
	private static OrderWrapper getLegacyOrder(RequestWrapper request) {
		List<Order__c> orders = [
			SELECT Id, Name, Account__c, SFCC_Status__c, Order_Date__c, VF_Contract__r.Opportunity__c, VF_Contract__r.Opportunity__r.Channel__c
			FROM Order__c
			WHERE Id = :request.orderId
		];
		if (orders.isEmpty()) {
			throwNoOrderFoundException(request.orderId);
		}
		List<Configuration> configurations = getLegacyConfigurations(orders[0]);
		return new OrderWrapper(orders[0], configurations);
	}

	/*
	 * return order lines based on contracted product related to legacy order
	 */
	private static List<Configuration> getLegacyConfigurations(Order__c order) {
		List<Configuration> configurations = new List<Configuration>();

		// prepare grouping of site with its related contracted products
		Map<Site__c, List<Contracted_Products__c>> mapSiteWithContractedProducts = new Map<Site__c, List<Contracted_Products__c>>();
		for (Contracted_Products__c contractedProduct : [
			SELECT
				Id,
				Proposition__c,
				Product__r.Name,
				Site__c,
				Site__r.Site_Street__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_City__c,
				Quantity__c,
				Quantity_Duration__c,
				Is_Recurring__c,
				Totalprice__c,
				Discount__c,
				Discount_description__c
			FROM Contracted_Products__c
			WHERE Order__c = :order.Id
		]) {
			if (!mapSiteWithContractedProducts.containsKey(contractedProduct.Site__r)) {
				mapSiteWithContractedProducts.put(contractedProduct.Site__r, new List<Contracted_Products__c>());
			}
			mapSiteWithContractedProducts.get(contractedProduct.Site__r).add(contractedProduct);
		}

		// prepare an order line for each site and its related contracted products
		for (Site__c site : mapSiteWithContractedProducts.keySet()) {
			configurations.add(new Configuration(site, mapSiteWithContractedProducts.get(site)));
		}

		return configurations;
	}

	/*
	 * return response for CS order
	 */
	private static OrderWrapper getCSOrder(RequestWrapper request) {
		List<csord__Order__c> orders = [
			SELECT
				Id,
				csord__Order_Number__c,
				Ecommerce_Order_Reference_Id__c,
				csord__Account__c,
				SFCC_Status__c,
				CreatedDate,
				csordtelcoa__Opportunity__c,
				csordtelcoa__Opportunity__r.Channel__c
			FROM csord__Order__c
			WHERE Id = :request.orderId
		];
		if (orders.isEmpty()) {
			throwNoOrderFoundException(request.orderId);
		}
		List<Configuration> configurations = getCSConfigurations(orders[0]);
		return new OrderWrapper(orders[0], configurations);
	}

	/*
	 * return order lines based on services related to CS order
	 */
	private static List<Configuration> getCSConfigurations(csord__Order__c order) {
		List<Configuration> configurations = new List<Configuration>();

		// prepare grouping of site with its related services
		Map<Site__c, List<csord__Service__c>> mapSiteWithServices = new Map<Site__c, List<csord__Service__c>>();
		for (csord__Service__c service : [
			SELECT
				Id,
				Name,
				Site__c,
				Site__r.Site_Street__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_City__c,
				csord__Subscription__r.csordtelcoa__Product_Configuration__r.Proposition__c,
				csord__Subscription__r.Name,
				csordtelcoa__Product_Configuration__r.cscfga__Contract_Term__c,
				csordtelcoa__Product_Configuration__r.cscfga__One_Off_Charge__c,
				csordtelcoa__Product_Configuration__r.cscfga__Quantity__c,
				csordtelcoa__Product_Configuration__r.cscfga__Recurring_Charge__c,
				(
					SELECT Id, csord__Discount_Type__c, csord__Discount_Value__c, csord__Line_Description__c
					FROM csord__Service_line_Items__r
					WHERE csord__line_item_type__c = :SERVICE_LINE_TYPE_DISCOUNT_CHARGE
					LIMIT 1
				)
			FROM csord__Service__c
			WHERE csord__Order__c = :order.Id
		]) {
			if (!mapSiteWithServices.containsKey(service.Site__r)) {
				mapSiteWithServices.put(service.Site__r, new List<csord__Service__c>());
			}
			mapSiteWithServices.get(service.Site__r).add(service);
		}

		// prepare an order line for each site and its related services
		for (Site__c site : mapSiteWithServices.keySet()) {
			configurations.add(new Configuration(site, mapSiteWithServices.get(site)));
		}
		return configurations;
	}

	private static void throwNoOrderFoundException(Id orderId) {
		throw new NoOrderFoundException(getOrderNotFoundMessage(orderId));
	}

	@TestVisible
	private static String getOrderNotFoundMessage(Id orderId) {
		return System.Label.SFCC_OrderApi_OrderNotFoundMessage + ': ' + orderId;
	}

	public class RequestWrapper {
		public String accountId;
		public String orderId;
		public RequestWrapper(String orderId) {
			this.accountId = accountId;
			this.orderId = orderId;
		}
	}

	public class ResponseWrapper {
		public OrderWrapper order;
		public String errorMessage;
		public ResponseWrapper(OrderWrapper order) {
			this.order = order;
		}
		public ResponseWrapper(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	}

	public class OrderWrapper {
		public String orderId;
		public String orderNumber;
		public String ecomReference;
		public String accountId;
		public String orderStatus;
		public Datetime orderDate;
		public String channel;
		public List<Configuration> configurations;

		public OrderWrapper(Order__c order, List<Configuration> configurations) {
			this.orderId = order.Id;
			this.orderNumber = order.Name;
			this.ecomReference = null;
			this.accountId = order.Account__c;
			this.orderStatus = order.SFCC_Status__c;
			this.orderDate = order.Order_Date__c;
			this.channel = order?.VF_Contract__r?.Opportunity__r?.Channel__c;
			this.configurations = configurations;
		}

		public OrderWrapper(csord__Order__c order, List<Configuration> configurations) {
			this.orderId = order.Id;
			this.orderNumber = order.csord__Order_Number__c;
			this.ecomReference = order.Ecommerce_Order_Reference_Id__c;
			this.accountId = order.csord__Account__c;
			this.orderStatus = order.SFCC_Status__c;
			this.orderDate = order.CreatedDate;
			this.channel = order?.csordtelcoa__Opportunity__r.Channel__c;
			this.configurations = configurations;
		}
	}
	public class Configuration {
		public Site site;
		public List<ProductService> productServices = new List<ProductService>();

		public Configuration(Site__c siteObject, List<Contracted_Products__c> contractedProducts) {
			this.site = new Site(siteObject);
			for (Contracted_Products__c contractedProduct : contractedProducts) {
				this.productServices.add(new ProductService(contractedProduct));
			}
		}

		public Configuration(Site__c siteObject, List<csord__Service__c> services) {
			this.site = new Site(siteObject);
			for (csord__Service__c service : services) {
				this.productServices.add(new ProductService(service));
			}
		}
	}

	public class Site {
		public String street;
		public Decimal houseNumber;
		public String houseNumberExtension;
		public String postalCode;
		public String city;

		public Site(Site__c site) {
			this.street = site?.Site_Street__c;
			this.houseNumber = site?.Site_House_Number__c;
			this.houseNumberExtension = site?.Site_House_Number_Suffix__c;
			this.postalCode = site?.Site_Postal_Code__c;
			this.city = site?.Site_City__c;
		}
	}

	public class ProductService {
		public String productType;
		public String productName;
		public String productParent;
		public Decimal quantity;
		public Decimal contractDuration;
		public Price price;

		public ProductService(csord__Service__c service) {
			this.productType = service?.csord__Subscription__r?.csordtelcoa__Product_Configuration__r?.Proposition__c;
			this.productName = service?.Name;
			this.productParent = service?.csord__Subscription__r?.Name;
			cscfga__Product_Configuration__c serviceProductConfiguration = service?.csordtelcoa__Product_Configuration__r;
			this.quantity = serviceProductConfiguration?.cscfga__Quantity__c;
			this.contractDuration = serviceProductConfiguration?.cscfga__Contract_Term__c;
			this.price = new Price(service);
		}

		public ProductService(Contracted_Products__c contractedProduct) {
			this.productType = contractedProduct?.Proposition__c;
			this.productName = contractedProduct?.Product__r?.Name;
			this.productParent = null; // attribute not required for legacy order
			this.quantity = contractedProduct?.Quantity__c;
			this.contractDuration = contractedProduct?.Quantity_Duration__c;
			this.price = new Price(contractedProduct);
		}
	}

	public class Price {
		public String chargeType;
		public Decimal value;
		public String discountType;
		public Decimal discountValue;
		public String discountDescription;

		public Price(csord__Service__c service) {
			cscfga__Product_Configuration__c productConfiguration = service?.csordtelcoa__Product_Configuration__r;

			if (productConfiguration?.cscfga__Recurring_Charge__c > 0) {
				this.chargeType = CHARGE_TYPE_RECURRING;
				this.value = productConfiguration?.cscfga__Recurring_Charge__c;
			} else if (productConfiguration?.cscfga__One_Off_Charge__c > 0) {
				this.chargeType = CHARGE_TYPE_ONE_OFF;
				this.value = productConfiguration?.cscfga__One_Off_Charge__c;
			}

			List<csord__Service_Line_Item__c> serviceLineItems = service?.csord__Service_line_Items__r;
			csord__Service_Line_Item__c serviceLineItem = serviceLineItems.isEmpty() ? new csord__Service_Line_Item__c() : serviceLineItems[0];

			this.discountType = serviceLineItem?.csord__Discount_Type__c;
			this.discountValue = serviceLineItem?.csord__Discount_Value__c;
			this.discountDescription = serviceLineItem?.csord__Line_Description__c;
		}

		public Price(Contracted_Products__c contractedProduct) {
			this.chargeType = contractedProduct?.Is_Recurring__c ? CHARGE_TYPE_RECURRING : CHARGE_TYPE_ONE_OFF;
			this.value = contractedProduct?.Totalprice__c;
			this.discountType = null; // attribute not required for legacy order
			this.discountValue = contractedProduct?.Discount__c;
			this.discountDescription = contractedProduct?.Discount_description__c;
		}
	}

	public class NoOrderFoundException extends Exception {
	}
}
