@IsTest
public with sharing class TestOrderFormBillingDataController {
	@TestSetup
	static void makeData() {
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationBilling();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;
		TestUtils.createContact(TestUtils.theAccount);
		TestUtils.createFinancialAccount(TestUtils.theBan, TestUtils.theContact.Id);
		TestUtils.createBillingArrangement(TestUtils.theFA);
		TestUtils.createOrder(TestUtils.theContract);
	}

	@IsTest
	static void testControllerInitError() {
		Order__c order = getOrder();

		Test.startTest();

		PageReference pageRef = Page.OrderFormBillingDetails;
		Test.setCurrentPage(pageRef);
		OrderFormBillingDataController ctrl = new OrderFormBillingDataController();

		Test.stopTest();

		System.assert(ctrl.errorFound, 'Required parameters should be provided.');
	}

	@IsTest
	static void testUpdateBillingDetails() {
		Order__c order = getOrder();

		Test.startTest();

		PageReference pageRef = Page.OrderFormBillingDetails;
		Test.setCurrentPage(pageRef);
		pageRef.getParameters().put('contractId', order.VF_Contract__c);
		pageRef.getParameters().put('orderId', order.Id);

		OrderFormBillingDataController ctrl = new OrderFormBillingDataController();
		ctrl.refresh();
		List<SelectOption> faOptions = ctrl.faSelectOptions;
		ctrl.faIdSelected = faOptions[1].getValue();
		ctrl.changeFA();

		List<SelectOption> barOptions = ctrl.barSelectOptions;
		ctrl.barIdSelected = barOptions[1].getValue();
		ctrl.changeBAR();

		List<SelectOption> contactOptions = ctrl.faContactSelectOptions;
		String selectedContactId = ctrl.faContactIdSelected;

		ctrl.saveBilling();

		Test.stopTest();

		order = getOrder();
		System.assertEquals(
			order.Billing_Arrangement__c,
			ctrl.barIdSelected,
			'Order Billing Arrangment should be set'
		);
		System.assertEquals(
			order.VF_Contract__r.Opportunity__r.Ban__c,
			ctrl.banIdSelected,
			'Opportunity BAN should be set'
		);
	}

	static Order__c getOrder() {
		return [
			SELECT
				Id,
				Account__c,
				VF_Contract__c,
				Billing_Arrangement__c,
				VF_Contract__r.Opportunity__r.Ban__c
			FROM Order__c
			LIMIT 1
		];
	}
}