/*
    Author: Rahul Sharma
    Description: Tests class for Com_SelectContactRoleController.cls
    Jira ticket: COM-3956
*/

@IsTest
private class TestCom_SelectContactRoleController {
	@TestSetup
	private static void createTestData() {
		// insert the configuration validation fields
		TestUtils.createOrderValidationContactRoles();

		Account account = CS_DataTest.createAccount('Test Account');
		insert account;

		Opportunity opportunity = CS_DataTest.createOpportunity(account, 'Test Opp', UserInfo.getUserId());
		insert opportunity;
	}

	@IsTest
	static void testGetDataWithoutOpportunityId() {
		Test.startTest();

		LightningResponse response = Com_SelectContactRoleController.getData(null);
		Com_SelectContactRoleController.RolesData data = (Com_SelectContactRoleController.RolesData) response.body;

		Test.stopTest();

		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
		System.assertEquals(System.Label.Com_SelectContactRole_NoOpportunityMessage, response.message);
		System.assertEquals(null, data);
	}

	@IsTest
	static void testGetDataWithOpportunityIdAndNoContactRole() {
		Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];

		Test.startTest();

		LightningResponse response = Com_SelectContactRoleController.getData(opportunity.Id);
		Com_SelectContactRoleController.RolesData data = (Com_SelectContactRoleController.RolesData) response.body;

		Test.stopTest();

		System.assertEquals(null, response.variant);
		System.assertEquals(null, response.message);
		System.assertNotEquals(null, data.accountId);
	}

	@IsTest
	static void testGetDataWithOpportunityIdAndWithContactRole() {
		Opportunity opportunity = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		Contact contactMain = createContact(opportunity.AccountId);
		Contact contactMaintenance = createContact(opportunity.AccountId);
		Contact contactIncident = createContact(opportunity.AccountId);
		Contact contactChooser = createContact(opportunity.AccountId);

		Contact_Role__c contactRoleMain = createContactRole(opportunity.AccountId, contactMain.Id, Com_SelectContactRoleController.ROLE_MAIN, true);
		Contact_Role__c contactRoleMaintenance = createContactRole(
			opportunity.AccountId,
			contactMaintenance.Id,
			Com_SelectContactRoleController.ROLE_MAINTENANCE,
			true
		);
		Contact_Role__c contactRoleIncident = createContactRole(
			opportunity.AccountId,
			contactIncident.Id,
			Com_SelectContactRoleController.ROLE_INCIDENT,
			true
		);
		Contact_Role__c contactRoleChooser = createContactRole(
			opportunity.AccountId,
			contactChooser.Id,
			Com_SelectContactRoleController.ROLE_CHOOSER,
			true
		);

		Test.startTest();

		LightningResponse response = Com_SelectContactRoleController.getData(opportunity.Id);
		Com_SelectContactRoleController.RolesData data = (Com_SelectContactRoleController.RolesData) response.body;

		Test.stopTest();

		System.assertEquals(null, response.variant);
		System.assertEquals(null, response.message);
		System.assertEquals(opportunity.AccountId, data.accountId);
		System.assertEquals(contactMain.Id, data.customerMainContactId);
		System.assertEquals(contactMaintenance.Id, data.customerMaintenanceContactId);
		System.assertEquals(contactIncident.Id, data.customerIncidentContactId);
		System.assertEquals(contactChooser.Id, data.customerChooserContactId);
		System.assertNotEquals(null, data.customerMainContactRoleId);
		System.assertNotEquals(null, data.customerMaintenanceContactRoleId);
		System.assertNotEquals(null, data.customerIncidentContactRoleId);
		System.assertNotEquals(null, data.customerChooserContactRoleId);

		System.assertEquals(
			4,
			[
				SELECT COUNT()
				FROM Contact_Role__c
				WHERE Account__c = :opportunity.accountId AND Type__c IN :Com_SelectContactRoleController.CONTACT_ROLES_VALUES AND Active__c = TRUE
			]
		);
	}

	@IsTest
	static void testSaveDataWithExistingContactRole() {
		Opportunity opportunity = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		Contact contactMain = createContact(opportunity.AccountId);
		Contact contactMaintenance = createContact(opportunity.AccountId);
		Contact contactIncident = createContact(opportunity.AccountId);
		Contact contactChooser = createContact(opportunity.AccountId);

		Contact_Role__c contactRoleMain = createContactRole(opportunity.AccountId, contactMain.Id, Com_SelectContactRoleController.ROLE_MAIN, true);

		Contact contactOther = createContact(opportunity.AccountId);

		Contact_Role__c contactRoleMaintenance = createContactRole(
			opportunity.AccountId,
			contactOther.Id,
			Com_SelectContactRoleController.ROLE_MAINTENANCE,
			false
		);

		insertContactRoleValidation();

		Test.startTest();

		Com_SelectContactRoleController.RolesData rolesData = new Com_SelectContactRoleController.RolesData(
			opportunity.AccountId,
			contactOther.Id,
			contactOther.Id,
			contactOther.Id,
			contactOther.Id
		);

		LightningResponse response = Com_SelectContactRoleController.save(rolesData);

		Test.stopTest();

		System.assertEquals(System.Label.Com_SelectContactRole_SaveSuccessMessage, response.message);
		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant);

		System.assertEquals(
			4,
			[
				SELECT COUNT()
				FROM Contact_Role__c
				WHERE Account__c = :opportunity.accountId AND Type__c IN :Com_SelectContactRoleController.CONTACT_ROLES_VALUES AND Active__c = TRUE
			]
		);
	}

	@IsTest
	static void testSaveDataWithContactValidationError() {
		Opportunity opportunity = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		// create contact role with no first name
		Contact contactMain = createContact(opportunity.AccountId, null);

		Contact_Role__c contactRoleMain = createContactRole(opportunity.AccountId, contactMain.Id, Com_SelectContactRoleController.ROLE_MAIN, true);

		Contact_Role__c contactRoleMaintenance = createContactRole(
			opportunity.AccountId,
			contactMain.Id,
			Com_SelectContactRoleController.ROLE_MAINTENANCE,
			true
		);

		insertContactRoleValidation();

		Test.startTest();

		Com_SelectContactRoleController.RolesData rolesData = new Com_SelectContactRoleController.RolesData(
			opportunity.AccountId,
			contactMain.Id,
			null,
			null,
			null
		);

		LightningResponse response = Com_SelectContactRoleController.save(rolesData);

		Test.stopTest();

		System.assertEquals(System.Label.Com_SelectContactRole_ContactValidationError, response.message);
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);

		System.assertEquals(
			1,
			[
				SELECT COUNT()
				FROM Contact_Role__c
				WHERE Account__c = :opportunity.accountId AND Type__c IN :Com_SelectContactRoleController.CONTACT_ROLES_VALUES AND Active__c = TRUE
			]
		);
	}

	private static void insertContactRoleValidation() {
		Order_Validation__c orderValidation = new Order_Validation__c(
			Active__c = true,
			API_Fieldname__c = 'Contact__r.FirstName',
			Error_message__c = 'Contact Mobile phone must not be empty.',
			Export_System__c = 'SIAS',
			Name = 'Contactrole FirstName',
			Object__c = 'contact_role__c',
			Operator__c = 'isblank',
			Proposition__c = 'One Fixed;One Net;Numberporting;One Fixed Express;IPVPN;Legacy;Internet;Mobile',
			Who__c = 'Inside Sales;'
		);
		insert orderValidation;
	}

	private static Contact createContact(Id accountId) {
		return createContact(accountId, 'TestFirstname');
	}

	private static Contact createContact(Id accountId, String firstName) {
		Contact contact = new Contact(
			AccountId = accountId,
			LastName = 'TestLastname',
			FirstName = firstName,
			MobilePhone = '123456789',
			Email = 'contact-' + TestUtils.getRandomString(15) + '@vodafonetest.com'
		);
		insert contact;
		return contact;
	}

	private static Contact_Role__c createContactRole(Id accountId, Id contactId, String type, Boolean isActive) {
		Contact_Role__c contactRole = new Contact_Role__c(Account__c = accountId, Active__c = isActive, Contact__c = contactId, Type__c = type);
		insert contactRole;
		return contactRole;
	}
}
