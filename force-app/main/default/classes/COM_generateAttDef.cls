//Used for generating attribute definitions
//Please DO NOT deploy to testcons
public class COM_generateAttDef {
public static Boolean generateAttDef(Id prodDefId, String name, String defVal){
        cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
        attDef.cscfga__Type__c = 'Calculation';
        attDef.cscfga__Data_Type__c =  'String';
        attDef.cscfga__store_as_json__c = true;
        attDef.cscfga__Product_Definition__c = prodDefId;
        attDef.cscfga__Default_Value__c = defVal;
        attDef.Name = name;
        
        try{
            insert attDef;
            System.debug('ATT='+attDef.Id);
            return true;
        }
        catch(Exception e){
            System.debug(e);
            return false;
        }
        
    }

    public static void generateAllAtts(Id prodDefId) {
        generateAttDef(prodDefId, 'OneOffChargeDescription', '{productVariant.One_Off_Charge_Product__r.Unify_Charge_Description__c}');
        generateAttDef(prodDefId, 'OneOffChargeCode', '{productVariant.One_Off_Charge_Product__r.Unify_Charge_Code__c}');
        generateAttDef(prodDefId, 'OneOffBITag1', '{productVariant.One_Off_Charge_Product__r.ProductCode}');
        generateAttDef(prodDefId, 'OneOffBITag2', '{productVariant.One_Off_Charge_Product__r.Taxonomy__r.Name}');
        generateAttDef(prodDefId, 'OneOffBITag3', '{productVariant.One_Off_Charge_Product__r.Taxonomy__r.Product_Family__c}');
        generateAttDef(prodDefId, 'OneOffBITag4', '{productVariant.One_Off_Charge_Product__r.Taxonomy__r.Portfolio__c}');
        
        
        generateAttDef(prodDefId, 'RecurringChargeDescription', '{productVariant.Recurring_Charge_Product__r.Unify_Charge_Description__c}');
        generateAttDef(prodDefId, 'RecurringChargeCode', '{productVariant.Recurring_Charge_Product__r.Unify_Charge_Code__c}');
        generateAttDef(prodDefId, 'RecurringBITag1', '{productVariant.Recurring_Charge_Product__r.ProductCode}');
        generateAttDef(prodDefId, 'RecurringBITag2', '{productVariant.Recurring_Charge_Product__r.Taxonomy__r.Name}');
        generateAttDef(prodDefId, 'RecurringBITag3', '{productVariant.Recurring_Charge_Product__r.Taxonomy__r.Product_Family__c}');
        generateAttDef(prodDefId, 'RecurringBITag4', '{productVariant.Recurring_Charge_Product__r.Taxonomy__r.Portfolio__c}');
    
    }

    public static void generateAllAttsAddon(Id prodDefId) {
        generateAttDef(prodDefId, 'OneOffChargeDescription', '{AddonGenerator.cspmb__Add_On_Price_Item__r.One_Off_Charge_Product__r.Unify_Charge_Description__c}');
        generateAttDef(prodDefId, 'OneOffChargeCode', '{AddonGenerator.cspmb__Add_On_Price_Item__r.One_Off_Charge_Product__r.Unify_Charge_Code__c}');
        generateAttDef(prodDefId, 'OneOffBITag1', '{AddonGenerator.cspmb__Add_On_Price_Item__r.One_Off_Charge_Product__r.ProductCode}');
        generateAttDef(prodDefId, 'OneOffBITag2', '{AddonGenerator.cspmb__Add_On_Price_Item__r.One_Off_Charge_Product__r.Taxonomy__r.Name}');
        generateAttDef(prodDefId, 'OneOffBITag3', '{AddonGenerator.cspmb__Add_On_Price_Item__r.One_Off_Charge_Product__r.Taxonomy__r.Product_Family__c}');
        generateAttDef(prodDefId, 'OneOffBITag4', '{AddonGenerator.cspmb__Add_On_Price_Item__r.One_Off_Charge_Product__r.Taxonomy__r.Portfolio__c}');
        
        
        generateAttDef(prodDefId, 'RecurringChargeDescription', '{AddonGenerator.cspmb__Add_On_Price_Item__r.Recurring_Charge_Product__r.Unify_Charge_Description__c}');
        generateAttDef(prodDefId, 'RecurringChargeCode', '{AddonGenerator.cspmb__Add_On_Price_Item__r.Recurring_Charge_Product__r.Unify_Charge_Code__c}');
        generateAttDef(prodDefId, 'RecurringBITag1', '{AddonGenerator.cspmb__Add_On_Price_Item__r.Recurring_Charge_Product__r.ProductCode}');
        generateAttDef(prodDefId, 'RecurringBITag2', '{AddonGenerator.cspmb__Add_On_Price_Item__r.Recurring_Charge_Product__r.Taxonomy__r.Name}');
        generateAttDef(prodDefId, 'RecurringBITag3', '{AddonGenerator.cspmb__Add_On_Price_Item__r.Recurring_Charge_Product__r.Taxonomy__r.Product_Family__c}');
        generateAttDef(prodDefId, 'RecurringBITag4', '{AddonGenerator.cspmb__Add_On_Price_Item__r.Recurring_Charge_Product__r.Taxonomy__r.Portfolio__c}');
    
    }

}