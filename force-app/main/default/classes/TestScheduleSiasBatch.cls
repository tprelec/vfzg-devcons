/**
 * Test class for ScheduleSiasBatch.cls
 * 
 * @author: Rahul Sharma
 */
@IsTest
public class TestScheduleSiasBatch {

    @IsTest 
    private static void validateSendOrdersToSias_statusCode200_StatusOk() {
        
        Order__c order = createOrderWithContractedProducts(2, Constants.PRODUCT2_BILLINGTYPE_STANDARD);

        // change the status run once flag as trigger may already ran
        TriggerHandler.resetAlreadyModified();
        TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

        // verify customer assets are generated
        // System.assertEquals(2, [SELECT COUNT() FROM Customer_Asset__c WHERE Order__c = :order.Id]);

        List<Contracted_Products__c> cps = [SELECT Id FROM
            Contracted_Products__c WHERE 
            Order__c = :order.Id];
        for(Contracted_Products__c cp: cps) {
            cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED; 
            cp.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
            cp.ProductCode__c = 'C106929';
        }
        update cps;


        // verify that there are expected CPs records for batch logic to pick
        // System.assertEquals(2, [SELECT COUNT() FROM
        //     Contracted_Products__c WHERE 
        //     Order__c = :order.Id AND
        //     Delivery_Status__c = :Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED AND 
        //     Billing_Status__c = :Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW AND
        //     CLC__c = :Constants.CONTRACTED_PRODUCT_CLC_ACQ AND
        //     Customer_Asset__c != null AND
        //     Product__r.Billing_Type__c != :Constants.PRODUCT2_BILLINGTYPE_SPECIAL
        // ]);

        // create custom setting with integration data
        External_WebService_Config__c config = new External_WebService_Config__c(
            Name = CreateBSSOrderRest.INTEGRATION_SETTING_NAME,
            URL__c = CreateBSSOrderRest.MOCK_URL, 
            Username__c = 'username',
            Password__c = 'password'
        );
        insert config;
        System.assertNotEquals(null, config.Id);

        setMock(config.URL__c, CreateBSSOrderRest.STATUS_OK, 200);
        
        Test.startTest();
        
        // call schedule execute method
        new ScheduleSiasBatch().execute(null);

        Test.stopTest();

        // verify contracted products billing status
        for(Contracted_Products__c cp: [SELECT Id, Billing_Status__c, Number_of_Failed_Requests__c FROM Contracted_Products__c WHERE Order__c = :order.Id]) {
            System.assertEquals(CreateBSSOrderRest.BILLING_STATUS_REQUESTED, cp.Billing_Status__c);
            System.assertEquals(0, cp.Number_of_Failed_Requests__c);
        }
        
        // verify the transaction record is created
        System.assertEquals(1, [SELECT COUNT() FROM Order_Billing_Transaction__c]);
    }

    @IsTest 
    private static void validateSendOrdersToSias_statusCode200_StatusOk_Special() {
        TestUtils.createOrderValidationOrder();
        TestUtils.createOrderValidationContractedProducts();
        Order__c order = createOrderWithContractedProducts(2, Constants.PRODUCT2_BILLINGTYPE_SPECIAL);

        // change the status run once flag as trigger may already ran
        TriggerHandler.resetAlreadyModified();
        TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

        // verify customer assets are generated
        // System.assertEquals(2, [SELECT COUNT() FROM Customer_Asset__c WHERE Order__c = :order.Id]);

        List<Contracted_Products__c> cps = [SELECT Id FROM
            Contracted_Products__c WHERE 
            Order__c = :order.Id];
        for(Contracted_Products__c cp: cps) {
            cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED; 
            cp.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
            cp.ProductCode__c = 'C106929';
        }
        update cps;


        // verify that there are expected CPs records for batch logic to pick
        // System.assertEquals(2, [SELECT COUNT() FROM
        //     Contracted_Products__c WHERE 
        //     Order__c = :order.Id AND
        //     Delivery_Status__c = :Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED AND 
        //     Billing_Status__c = :Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW AND
        //     CLC__c = :Constants.CONTRACTED_PRODUCT_CLC_ACQ AND
        //     Customer_Asset__c != null AND
        //     Product__r.Billing_Type__c != :Constants.PRODUCT2_BILLINGTYPE_SPECIAL
        // ]);

        // create custom setting with integration data
        External_WebService_Config__c config = new External_WebService_Config__c(
            Name = CreateBSSOrderRest.INTEGRATION_SETTING_NAME,
            URL__c = CreateBSSOrderRest.MOCK_URL, 
            Username__c = 'username',
            Password__c = 'password'
        );
        insert config;
        System.assertNotEquals(null, config.Id);

        setMock(config.URL__c, CreateBSSOrderRest.STATUS_OK, 200);
        
        Test.startTest();
        
        // call schedule execute method
        new ScheduleSiasBatch().execute(null);

        Test.stopTest();

        // verify contracted products billing status
        for(Contracted_Products__c cp: [SELECT Id, Billing_Status__c, Number_of_Failed_Requests__c FROM Contracted_Products__c WHERE Order__c = :order.Id]) {
            System.assertEquals(Constants.CUSTOMER_ASSET_BILLING_STATUS_SPECIAL_BILLING, cp.Billing_Status__c);
            System.assertEquals(0, cp.Number_of_Failed_Requests__c);
        }
        
        // verify the transaction record is created
        System.assertEquals(0, [SELECT COUNT() FROM Order_Billing_Transaction__c]);
    }

    @IsTest 
    private static void validateSendOrdersToSias_statusCode400() {
        
        TestUtils.createOrderValidationOrder();
        TestUtils.createOrderValidationContractedProducts();

        Order__c order = createOrderWithContractedProducts(2, Constants.PRODUCT2_BILLINGTYPE_STANDARD);

        // change the status run once flag as trigger may already ran
        TriggerHandler.resetAlreadyModified();
        TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

        // verify customer assets are generated
        // System.assertEquals(2, [SELECT COUNT() FROM Customer_Asset__c WHERE Order__c = :order.Id]);

        List<Contracted_Products__c> cps = [SELECT Id FROM
            Contracted_Products__c WHERE 
            Order__c = :order.Id];
        for(Contracted_Products__c cp: cps) {
            cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED; 
            cp.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
            cp.Number_of_Failed_Requests__c = CreateBSSOrderRest.MAX_NR_OF_FAILED_REQUESTS - 1;
            cp.ProductCode__c = 'C106929';
        }
        update cps;


        // verify that there are expected CPs records for batch logic to pick
        // System.assertEquals(2, [SELECT COUNT() FROM
        //     Contracted_Products__c WHERE 
        //     Order__c = :order.Id AND
        //     Delivery_Status__c = :Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED AND 
        //     Billing_Status__c = :Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW AND
        //     CLC__c = :Constants.CONTRACTED_PRODUCT_CLC_ACQ AND
        //     Customer_Asset__c != null AND
        //     Product__r.Billing_Type__c != :Constants.PRODUCT2_BILLINGTYPE_SPECIAL
        // ]);

        // create custom setting with integration data
        External_WebService_Config__c config = new External_WebService_Config__c(
            Name = CreateBSSOrderRest.INTEGRATION_SETTING_NAME,
            URL__c = CreateBSSOrderRest.MOCK_URL, 
            Username__c = 'username',
            Password__c = 'password'
        );
        insert config;
        System.assertNotEquals(null, config.Id);

        // set mock for create BSS order rest service
        setMock(config.URL__c, CreateBSSOrderRest.STATUS_OK,400);
        
        Test.startTest();
        
        // call schedule execute method
        new ScheduleSiasBatch().execute(null);

        Test.stopTest();

        // verify contracted products billing status
        // verify the retries mechanism to set the BOP status to Failed
        for(Contracted_Products__c cp: [SELECT Id, Billing_Status__c, Number_of_Failed_Requests__c FROM Contracted_Products__c WHERE Order__c = :order.Id]) {
            System.assertEquals(CreateBSSOrderRest.BILLING_STATUS_REQUEST_FAILED, cp.Billing_Status__c);
            System.assertEquals(CreateBSSOrderRest.MAX_NR_OF_FAILED_REQUESTS, cp.Number_of_Failed_Requests__c);
        }
        
        // verify the transaction record is created
        System.assertEquals(1, [SELECT COUNT() FROM Order_Billing_Transaction__c]);

        // verify below
        // Methods defined as TestMethod do not support Web service callouts
        // System.assertNotEquals('', [SELECT Id, BOP_export_Errormessage__c FROM Order__c WHERE Id = :order.Id].BOP_export_Errormessage__c);
    }

    @IsTest 
    private static void validateCallScheduler() {
        
        Order__c order = createOrderWithContractedProducts(2, Constants.PRODUCT2_BILLINGTYPE_STANDARD);

        // create custom setting with integration data
        External_WebService_Config__c config = new External_WebService_Config__c(
            Name = CreateBSSOrderRest.INTEGRATION_SETTING_NAME,
            URL__c = CreateBSSOrderRest.MOCK_URL, 
            Username__c = 'username',
            Password__c = 'password'
        );
        insert config;
        System.assertNotEquals(null, config.Id);
        
        setMock(config.URL__c, CreateBSSOrderRest.STATUS_OK, 200);
        
        Test.startTest();
        
        String exceptionType;
        try {
            // As Database.executeBatch is treated as DML, following call would give callout exception
            ScheduleSiasBatch.scheduleInFiveMinutes();
        } catch(Exception objEx) {
        //     exceptionType = objEx.getTypeName();
        }

        Test.stopTest();

        // System.assertEquals('', exceptionType);

        // verify contracted products billing status is unchanged
        for(Contracted_Products__c cp: [SELECT Id, Billing_Status__c FROM Contracted_Products__c WHERE Order__c = :order.Id]) {
            System.assertEquals(Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW, cp.Billing_Status__c);
        }
        
        // verify the transaction record is not created
        System.assertEquals(0, [SELECT COUNT() FROM Order_Billing_Transaction__c]);
    }

    @IsTest 
    private static void validateBatchFinish() {
        Test.startTest();
        
        Database.executeBatch(new SiasBatch(new Set<Id>()), 50);
        
        Test.stopTest();
    }

    @IsTest 
    private static void validateSchedulerAbort() {
        Test.startTest();
        
        try {
            ScheduleSiasBatch.scheduleInFiveMinutes();
            ScheduleSiasBatch.abort();
        } 
        catch(Exception e) {}
        
        Test.stopTest();
    }

    static void setMock(String url, String statusCode, Integer responseCode) {

        Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
        endpoint2TestResp.put(url, new TestUtilMultiRequestMock.SingleRequestMock(responseCode,
                                        'Complete',
                                        '{"status": "' + statusCode + '"}',
                                        null));
        HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);

        Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);

    }

    public static Order__c createOrderWithContractedProducts(Integer totalContractedProducts, String billingType) {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Site__c site = TestUtils.createSite(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
        OrderType__c ot = TestUtils.createOrderType();
        System.assertNotEquals(null, ot.Id);

        TestUtils.autoCommit = false;
        Product2 product = TestUtils.createProduct(new Map<String, Object> {
            'Billing_Type__c' => billingType
        });
        insert product;
        System.assertNotEquals(null, product.Id);

        // create an order with new status
        Order__c order = new Order__c(
            Status__c = 'New',
            Propositions__c = 'Legacy',
            OrderType__c = ot.Id,
            VF_Contract__c = contr.Id,
            Number_of_items__c = 100,
            O2C_Order__c = true
        );
        insert order;
        System.assertNotEquals(null, order.Id);

        List<Order__c> orders = [SELECT Id, Status__c FROM Order__c LIMIT 2];
        System.assertEquals(1, orders.size());

        List<Contracted_Products__c> contractedProducts = new List<Contracted_Products__c>();
        // add contracted products
        for(Integer count = 0; count < totalContractedProducts; count++) {
            contractedProducts.add(new Contracted_Products__c (
                Quantity__c = 1,
                Arpu_Value__c = 100,
                Duration__c = 1,
                Product__c = product.Id,
                VF_Contract__c = contr.Id,
                Site__c = site.Id,
                Order__c = order.Id,
                CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ,
                Gross_List_Price__c = 50,
                ProductCode__c = 'C106929'
            ));
        }
        insert contractedProducts;

        for(Contracted_Products__c cp: contractedProducts) {
            System.assertNotEquals(null, cp.Id);
        }

        return order;
    }

}