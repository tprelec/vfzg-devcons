/**
 * @description         This class schedules the batch processing of the bicc data imports.
 * @author              Gerhard Newman
 */
 global class ScheduleBiccBatches implements Schedulable {

	/**
	 * @description         This method executes the batch job.
	 */
	global void execute(SchedulableContext sc) {
	    BiccGenericBatch biccBatch = new BiccGenericBatch('BICC_Account__c');
		if (!test.isRunningTest()) {  // No need to test that the batches chain
	        biccBatch.chain=true;
	    }
	    // batch size cannot be greated than 100 for BICC_Account (due to callouts)
        Database.executeBatch(biccBatch, 100);
	}
}