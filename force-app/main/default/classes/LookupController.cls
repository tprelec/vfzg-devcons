/*
 * @author Rahul Sharma
 *
 * @description Controller for lookup LWC component
 */

@SuppressWarnings('PMD.ExcessivePublicCount') // because of business logic we have many methods
public without sharing class LookupController {
	@AuraEnabled(cacheable=true)
	public static LightningResponse getRecent(LookupRequest data) {
		try {
			Set<Id> recentIds = new Set<Id>();

			// query the recently viewed ids
			for (RecentlyViewed objRecentlyViewed : [SELECT Id FROM RecentlyViewed WHERE Type = :data.objectApiName LIMIT :data.recordLimit]) {
				recentIds.add(objRecentlyViewed.Id);
			}

			// objects without tabs do not yield results for RecentlyViewed, in that case query default records
			if (recentIds.isEmpty()) {
				return getRecords(data);
			}
			String wherePath = ' WHERE Id IN (\'' + String.join(new List<Id>(recentIds), '\', \'') + '\')';

			LightningResponse recentlyViewedRecordsWithFilter = createQuery(data, wherePath);

			// if there are no recently viewed records matching filter criteria, query default records
			if (!((List<LookupResult>) recentlyViewedRecordsWithFilter.body).isEmpty()) {
				return recentlyViewedRecordsWithFilter;
			} else {
				return getRecords(data);
			}
		} catch (Exception objEx) {
			return new LightningResponse().setError(objEx.getMessage());
		}
	}

	private static LightningResponse createQuery(LookupRequest data, String wherePath) {
		String secondaryField = (String.isNotEmpty(data.secondaryField)) ? ', ' + String.escapeSingleQuotes(data.secondaryField) : '';

		String soql = 'SELECT Id, ' + data.primaryField + secondaryField;
		soql += ' FROM ' + String.escapeSingleQuotes(data.objectApiName);
		soql += wherePath;
		soql += String.isNotEmpty(data.filter) ? ' AND (' + data.filter + ')' : '';
		soql += ' LIMIT ' + data.recordLimit;
		return new LightningResponse().setBody(formSearchResult(soql, data.primaryField, data.secondaryField));
	}

	@AuraEnabled(cacheable=true)
	public static LightningResponse getRecords(LookupRequest data) {
		try {
			String searchKey = '%' + String.escapeSingleQuotes(data.searchKey) + '%';
			String wherePath = '';
			if (data.secondaryField != null) {
				wherePath =
					' WHERE (' +
					data.primaryField +
					' LIKE \'' +
					searchKey +
					'\' OR ' +
					data.secondaryField +
					' LIKE \'' +
					searchKey +
					'\' )';
			} else {
				wherePath = ' WHERE (' + data.primaryField + ' LIKE \'' + searchKey + '\')';
			}

			return createQuery(data, wherePath);
		} catch (Exception objEx) {
			return new LightningResponse().setError(objEx.getMessage());
		}
	}

	@AuraEnabled(cacheable=true)
	public static LightningResponse getRecord(LookupRequest data) {
		try {
			String secondaryField = (String.isNotEmpty(data.secondaryField)) ? ', ' + String.escapeSingleQuotes(data.secondaryField) : '';

			String soqlTemplate = 'SELECT Id, {0}{1} FROM {2} WHERE Id = \'\'{3}\'\'';

			String soql = String.format(soqlTemplate, new List<String>{ data.primaryField, secondaryField, data.objectApiName, data.recordId });

			return new LightningResponse().setBody(formSearchResult(soql, data.primaryField, data.secondaryField));
		} catch (Exception objEx) {
			return new LightningResponse().setError(objEx.getMessage());
		}
	}

	public static List<LookupResult> formSearchResult(String soql, String primaryField, String secondaryField) {
		List<LookupResult> lstResponse = new List<LookupResult>();
		for (SObject sObjectInstance : Database.query(soql)) {
			lstResponse.add(
				new LookupResult(
					sObjectInstance.get('Id'),
					sObjectInstance.get(primaryField),
					String.isNotEmpty(secondaryField) ? sObjectInstance.get(secondaryField) : ''
				)
			);
		}
		return lstResponse;
	}

	public class LookupRequest {
		@AuraEnabled
		public String searchKey { get; set; }
		@AuraEnabled
		public String objectApiName { get; set; }
		@AuraEnabled
		public String primaryField { get; set; }
		@AuraEnabled
		public String secondaryField { get; set; }
		@AuraEnabled
		public String filter { get; set; }
		@AuraEnabled
		public Integer recordLimit { get; set; }

		@AuraEnabled
		public String recordId { get; set; }

		// without empty constructor sometimes LWC doesn't work properly
		@SuppressWarnings('PMD.EmptyStatementBlock')
		public LookupRequest() {
		}

		@SuppressWarnings('PMD.ExcessiveParameterList')
		public LookupRequest(
			String searchKey,
			String objectApiName,
			String primaryField,
			String secondaryField,
			String filter,
			String recordId,
			Integer recordLimit
		) {
			this.searchKey = searchKey;
			this.objectApiName = objectApiName;
			this.primaryField = primaryField;
			this.secondaryField = secondaryField;
			this.filter = filter;
			this.recordId = recordId;
			this.recordLimit = recordLimit;
		}
	}

	public class LookupResult {
		@AuraEnabled
		public Id id { get; set; }
		@AuraEnabled
		public String primaryLabel { get; set; }
		@AuraEnabled
		public String secondaryLabel { get; set; }

		// without empty constructor sometimes LWC doesn't work properly
		@SuppressWarnings('PMD.EmptyStatementBlock')
		public LookupResult() {
		}

		public LookupResult(Object id, Object primaryLabel, Object secondaryLabel) {
			this.id = (Id) id;
			this.primaryLabel = (String) primaryLabel;
			this.secondaryLabel = (String) secondaryLabel;
		}
	}

	public class LookupCreateNewConfiguration {
		@AuraEnabled
		public String name { get; set; }
		@AuraEnabled
		public Object value { get; set; }
		@AuraEnabled
		public Boolean isDisabled { get; set; }
		@AuraEnabled
		public Boolean isRequired { get; set; }

		// without empty constructor sometimes LWC doesn't work properly
		@SuppressWarnings('PMD.EmptyStatementBlock')
		public LookupCreateNewConfiguration() {
		}

		public LookupCreateNewConfiguration setName(String name) {
			this.name = name;
			return this;
		}

		public LookupCreateNewConfiguration setValue(Object value) {
			this.value = value;
			return this;
		}

		public LookupCreateNewConfiguration setDisabled(Boolean isDisabled) {
			this.isDisabled = isDisabled;
			return this;
		}

		public LookupCreateNewConfiguration setRequired(Boolean isRequired) {
			this.isRequired = isRequired;
			return this;
		}
	}
}
