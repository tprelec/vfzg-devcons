@isTest
public with sharing class CST_RemedyUpdateService_Test {
	@TestSetup
	static void makeData() {
		// record type ID dos Incident

		String recordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CST_Incident').getRecordTypeId();
		String usrId = UserInfo.getUserId();
		//Case remedyCase = [select id, casenumber, status, CST_Sub_Status__c, CST_End2End_Owner__c, Reason, OwnerId, CST_Case_is_with_Team__c from Case where id = :caseId];
		Case cs = new Case(
			status = 'In Progress',
			CST_Sub_Status__c = 'Working on it',
			CST_End2End_Owner__c = usrId,
			OwnerId = usrId,
			CST_Case_is_with_Team__c = 'Internal - Advisor'
		);

		insert cs;

		CST_External_Ticket__c remedy = new CST_External_Ticket__c(CST_Case__c = cs.id, CST_IncidentID__c = '565656', CST_Summary__c = 'test case');
		insert remedy;
	}
	@isTest
	static void testInProgress() {
		CST_RemedyUpdateService.RemedyRequestWrapper wrapper = new CST_RemedyUpdateService.RemedyRequestWrapper();
		wrapper.IncidentId = '565656';
		wrapper.Status = 'In Progress';
		wrapper.Assignee = 'Luis';
		wrapper.Priority = 'Low';

		RestRequest request = new RestRequest();

		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

		request.requestUri = baseUrl + '/services/apexrest/RemedyUpdate';
		request.httpMethod = 'PATCH';
		request.requestBody = Blob.valueOf(JSON.serializePretty(wrapper));

		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;
		Case cs = [SELECT id FROM Case];

		Test.startTest();
		// executa os teus testes
		CST_External_Ticket__c remedyTicket = new CST_External_Ticket__c();
		CST_RemedyUpdateService.updateTicket();
		Test.stopTest();
	}
	@isTest
	static void testPending() {
		CST_RemedyUpdateService.RemedyRequestWrapper wrapper = new CST_RemedyUpdateService.RemedyRequestWrapper();
		wrapper.IncidentId = '565656';
		wrapper.Status = 'Pending';
		wrapper.Assignee = 'Luis';
		wrapper.Priority = 'Low';

		RestRequest request = new RestRequest();

		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

		request.requestUri = baseUrl + '/services/apexrest/RemedyUpdate';
		request.httpMethod = 'PATCH';
		request.requestBody = Blob.valueOf(JSON.serializePretty(wrapper));

		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;
		Case cs = [SELECT id FROM Case];

		Test.startTest();
		// executa os teus testes
		CST_External_Ticket__c remedyTicket = new CST_External_Ticket__c();
		CST_RemedyUpdateService.updateTicket();
		Test.stopTest();
	}
	@isTest
	static void testCancelled() {
		CST_RemedyUpdateService.RemedyRequestWrapper wrapper = new CST_RemedyUpdateService.RemedyRequestWrapper();
		wrapper.IncidentId = '565656';
		wrapper.Status = 'Cancelled';
		wrapper.Assignee = 'Luis';
		wrapper.Priority = 'Low';

		RestRequest request = new RestRequest();

		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

		request.requestUri = baseUrl + '/services/apexrest/RemedyUpdate';
		request.httpMethod = 'PATCH';
		request.requestBody = Blob.valueOf(JSON.serializePretty(wrapper));

		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;
		Case cs = [SELECT id FROM Case];

		Test.startTest();
		// executa os teus testes
		CST_External_Ticket__c remedyTicket = new CST_External_Ticket__c();
		CST_RemedyUpdateService.updateTicket();
		Test.stopTest();
	}
	@isTest
	static void testResolved() {
		CST_RemedyUpdateService.RemedyRequestWrapper wrapper = new CST_RemedyUpdateService.RemedyRequestWrapper();
		wrapper.IncidentId = '565656';
		wrapper.Status = 'Resolved';
		wrapper.Assignee = 'Luis';
		wrapper.Priority = 'Low';

		RestRequest request = new RestRequest();

		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

		request.requestUri = baseUrl + '/services/apexrest/RemedyUpdate';
		request.httpMethod = 'PATCH';
		request.requestBody = Blob.valueOf(JSON.serializePretty(wrapper));

		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;
		Case cs = [SELECT id FROM Case];

		Test.startTest();
		// executa os teus testes
		CST_External_Ticket__c remedyTicket = new CST_External_Ticket__c();
		CST_RemedyUpdateService.updateTicket();
		Test.stopTest();
	}
}
