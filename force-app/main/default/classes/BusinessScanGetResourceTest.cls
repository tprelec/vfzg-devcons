/**
 *	@author			mcubias
 *	@created_date	2022.04.11
 *  @last modified : Saurabh 2022.06.10
 *	@description 	provides test and use case coverage for BusinessScanGetResource apex class
 */
@IsTest
public class BusinessScanGetResourceTest {
	@IsTest
	static void getResourceTest() {
		List<ServiceResource> sc = [SELECT Id, Name, RelatedRecordId, ResourceType, External_Id__c FROM ServiceResource];
		System.debug('Resource list' + sc);
		String expectedResponse = '[ { "startTime": "2021-03-18T16:00:00.000+0000", "endTime": "2021-03-18T17:00:00.000+0000", "resources": ["0Hn3O0000008m6wSAA"], "territoryId": "0Hh3O0000008WBNSA2" }, { "startTime": "2021-03-18T19:00:00.000+0000", "endTime": "2021-03-18T20:00:00.000+0000", "resources": ["0Hn3O0000008m6wSAA"], "territoryId": "0Hh3O0000008WBNSA2"} ]';
		lxscheduler.SchedulerResources.setAppointmentCandidatesMock(expectedResponse);
		Test.startTest();
		List<String> availableResource = BusinessScanGetResource.getResource(new List<String>{ '2021-03-18T16:00:00.000' });
		Test.stopTest();
		System.assert(!availableResource.isEmpty(), 'A service resource was returned!');
		String eventOwner = availableResource.get(0);
		List<Event> events = [SELECT Id FROM Event WHERE Subject = 'Business Scan Appointment'];
		System.assert(!events.isEmpty(), 'Event was created!');
	}
}
