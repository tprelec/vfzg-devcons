public without sharing class DoorToDoorBasketService {
	private static final Set<String> CONTACT_FIELDS = new Set<String>{
		'Birthdate',
		'Email',
		'FirstName',
		'LastName',
		'MobilePhone',
		'Phone',
		'Salutation'
	};

	private Id basketId;
	private cscfga__Product_Basket__c basket;
	private Id leadId;
	private Lead basketLead;
	private Id accId;
	private String kvkNumber;
	private Account acc;
	private Id oppId;
	private Opportunity opp;
	private String email;
	private Contact con;
	private Set<Id> conIds;
	private Set<String> conEmails;
	private Map<Id, Contact> cons;
	private Contact adminCon;
	private Contact techCon;
	private cscrm__Address__c premise;
	private cscrm__Site__c site;
	private csconta__Billing_Account__c billingAcc;

	public DoorToDoorBasketService(Id basketId) {
		this.basketId = basketId;
		validateBasket();
		getBasket();
		getRelatedRecords();
	}

	private void validateBasket() {
		if (basketId == null) {
			throw new DoorToDoorBasketException('Invalid Basket: Basket Id is null');
		}
		AggregateResult configCounts = [
			SELECT COUNT(Id) cfgCount
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c = :basketId
		];
		Integer cfgCount = (Integer) configCounts.get('cfgCount');
		if (cfgCount == 0) {
			throw new DoorToDoorBasketException(
				'Invalid Basket: No Product Configurations Attached'
			);
		}
	}

	private void getBasket() {
		String soql =
			'SELECT ' +
			SObjectService.getObjectFieldsAsCSV(cscfga__Product_Basket__c.getSobjectType()) +
			' FROM cscfga__Product_Basket__c WHERE Id = :basketId';
		basket = Database.query(String.escapeSingleQuotes(soql));
	}

	private void getRelatedRecords() {
		// 1. Check Lead
		if (basket.Lead__c != null) {
			leadId = basket.Lead__c;
			getLead();
			kvkNumber = basketLead.KVK_number__c;
			if (basketLead.IsConverted) {
				accId = basketLead.ConvertedAccountId;
				oppId = basketLead.ConvertedOpportunityId;
			}
		}
		// 2. If Account alread provided, take it from Basket
		if (basket.csbb__Account__c != null) {
			accId = basket.csbb__Account__c;
		}
		getAccount();

		// 3. If Opportunity already provided, take it from Basket
		if (basket.cscfga__Opportunity__c != null) {
			oppId = basket.cscfga__Opportunity__c;
		}
		getOpportunity();
		conIds = new Set<Id>();
		conEmails = new Set<String>();
		if (basketLead?.Email != null) {
			email = basketLead.Email.toLowerCase();
			conEmails.add(email);
			getMainContact();
		}
	}

	private void getLead() {
		if (leadId == null) {
			return;
		}
		String soql =
			'SELECT ' +
			SObjectService.getObjectFieldsAsCSV(Lead.getSobjectType()) +
			' FROM Lead WHERE Id = :leadId';
		basketLead = Database.query(String.escapeSingleQuotes(soql));
	}

	private void getAccount() {
		if (accId == null && kvkNumber == null) {
			return;
		}
		String fieldToCheck;
		String valueToCheck;
		if (accId != null) {
			fieldToCheck = 'Id';
			valueToCheck = String.escapeSingleQuotes(accId);
		} else {
			fieldToCheck = 'KVK_Number__c';
			valueToCheck = String.escapeSingleQuotes(kvkNumber);
		}
		String soql =
			'SELECT ' +
			SObjectService.getObjectFieldsAsCSV(Account.getSobjectType()) +
			' FROM Account WHERE ' +
			fieldToCheck +
			' = :valueToCheck';
		List<Account> accs = Database.query(String.escapeSingleQuotes(soql));
		if (!accs.isEmpty()) {
			acc = accs[0];
		}
	}

	private LeadStatus getLeadConvertStatus() {
		return [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted = TRUE LIMIT 1];
	}

	private void getOpportunity() {
		if (oppId == null) {
			return;
		}
		oppId = String.escapeSingleQuotes(oppId);
		String soql =
			'SELECT ' +
			SObjectService.getObjectFieldsAsCSV(Opportunity.getSobjectType()) +
			' FROM Opportunity WHERE Id = :oppId';
		opp = Database.query(String.escapeSingleQuotes(soql));
	}

	private void getMainContact() {
		if (acc == null || email == null) {
			return;
		}
		List<Contact> contacts = [
			SELECT Id
			FROM Contact
			WHERE AccountId = :acc.Id AND Email = :email
		];
		if (!contacts.isEmpty()) {
			con = contacts[0];
		}
	}

	private void getContacts() {
		if (isContactId(basket.LG_AdminContactPicklist__c)) {
			conIds.add((Id) (basket.LG_AdminContactPicklist__c));
		}
		if (String.isNotBlank(basket.LG_AdminContactEmail__c)) {
			conEmails.add(basket.LG_AdminContactEmail__c.toLowerCase());
		}
		if (isContactId(basket.LG_TechnicalContactPicklist__c)) {
			conIds.add((Id) (basket.LG_TechnicalContactPicklist__c));
		}
		if (String.isNotBlank(basket.LG_TechnicalContactEmail__c)) {
			conEmails.add(basket.LG_TechnicalContactEmail__c.toLowerCase());
		}
		String soql =
			'SELECT ' +
			SObjectService.getObjectFieldsAsCSV(Contact.getSobjectType()) +
			' FROM Contact WHERE Id IN :conIds OR (AccountId = :accId AND Email IN :conEmails)';
		cons = new Map<Id, Contact>(
			(List<Contact>) Database.query(String.escapeSingleQuotes(soql))
		);
	}

	private Boolean isContactId(String idString) {
		return String.isNotBlank(idString) &&
			GeneralUtils.isValidId(idString) &&
			Id.valueOf(idString).getSobjectType() == Schema.Contact.SObjectType;
	}

	public void convertLeadAndSyncBasket() {
		Savepoint sp = Database.setSavepoint();
		try {
			convertLead();
			syncBasket();
		} catch (Exception ex) {
			Database.rollback(sp);
			String error = ('D2D Error: ' + ex.getMessage());
			error = error.length() > 255 ? error.substring(0, 255) : error;
			cscfga__Product_Basket__c b = new cscfga__Product_Basket__c(
				Id = basketId,
				Error_Message__c = error
			);
			// Prevent Triggers
			TriggerHandler.preventRecursiveTrigger('ProductBasketTriggerHandler', null, 0);
			update b;
		}
	}

	private void convertLead() {
		// Convert Lead or Create new Opportunity
		if (basketLead != null && !basketLead.IsConverted) {
			// Convert
			LeadStatus convertStatus = getLeadConvertStatus();
			Database.LeadConvert leadConvert = new Database.LeadConvert();
			leadConvert.setOwnerId(UserInfo.getUserId());
			leadConvert.setLeadId(basketLead.Id);
			leadConvert.setConvertedStatus(convertStatus.MasterLabel);
			// Attach to existing Account and Opportunity if it was found
			if (acc != null) {
				leadConvert.setAccountId(acc.Id);
			}
			if (opp != null) {
				leadConvert.setOpportunityId(opp.Id);
			}
			if (con != null) {
				leadConvert.setContactId(con.Id);
			}
			Database.LeadConvertResult leadConvertRes = Database.convertLead(leadConvert, true);
			accId = leadConvertRes.getAccountId();
			oppId = leadConvertRes.getOpportunityId();
			conIds.add(leadConvertRes.getContactId());
		} else if (acc != null) {
			if (opp == null) {
				opp = new Opportunity(AccountId = acc.Id, Name = acc.Name);
			}
		} else {
			return;
		}

		// Refresh Records
		getAccount();
		getOpportunity();
		getContacts();
	}

	private void syncBasket() {
		// Process related records
		handleAccount();
		handleOpportunity();
		handlePremise();
		handleBillingAccount();
		handleContacts();

		// Run DMLs
		upsert opp;
		LG_Util.resolveAndUpsertPremiseDuplicates(new List<cscrm__Address__c>{ premise });
		getPremise();
		site = LG_Util.createNewSite(basket, premise.Id);
		LG_Util.resolveSiteDuplicatesAndUpsert(new List<cscrm__Site__c>{ site });
		if (billingAcc != null) {
			LG_Util.resolveAndUpsertBillingAccountDuplicates(
				new List<csconta__Billing_Account__c>{ billingAcc }
			);
		}
		update acc;
		List<Contact> consForUpsert = new List<Contact>();
		if (adminCon != null) {
			consForUpsert.add(adminCon);
		}
		if (techCon != null && techCon != adminCon) {
			consForUpsert.add(techCon);
		}
		if (!consForUpsert.isEmpty()) {
			upsert consForUpsert;
		}
		updateTehnicalContact();
		createOpportunityTask();
		createOpportunityContactRoles();

		basket.cscfga__Opportunity__c = opp.Id;
		LG_ProductBasketTriggerHandler.populateTheCustomerAccountField(
			new List<cscfga__Product_Basket__c>{ basket }
		);
		LG_ProductBasketTriggerHandler.setDefaultBillingAccount(
			new List<cscfga__Product_Basket__c>{ basket }
		);
		basket.LG_Selected_Billing_Account__c = basket.LG_BillingAccount__c;
		basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
		basket.csbb__Synchronised_With_Opportunity__c = true;
		update basket;

		setBillingAccountOnAttributeFields();
		handlePortingNumbers();
		setAddressAndInstallationWishDate();
		updateOpportunityAttachments();
		Set<String> basketIds = new Set<String>{ basket.Id };
		LG_ProductUtility.deleteHardOLIs(basketIds);
		LG_ProductDetailsUtility.deleteProductDetails(basketIds);
		LG_ProductUtility.createOLIs(basketIds);
		LG_ProductDetailsUtility.createProductDetails(basketIds);
	}

	private void handleAccount() {
		acc.LG_PostalCity__c = getBasketFieldValue('LG_PostalCity__c', 'LG_InstallationCity__c');
		acc.LG_PostalPostalCode__c = getBasketFieldValue(
			'LG_PostalPostalCode__c',
			'LG_InstallationPostalCode__c'
		);
		acc.LG_PostalPostalCode__c = getBasketFieldValue(
			'LG_PostalCity__c',
			'LG_InstallationCity__c'
		);
		acc.LG_PostalPostalCode__c = getBasketFieldValue(
			'LG_PostalCity__c',
			'LG_InstallationCity__c'
		);
		acc.LG_PostalPostalCode__c = getBasketFieldValue(
			'LG_PostalCity__c',
			'LG_InstallationCity__c'
		);
		acc.LG_PostalPostalCode__c = getBasketFieldValue(
			'LG_PostalCity__c',
			'LG_InstallationCity__c'
		);
		acc.LG_PostalPostalCode__c = getBasketFieldValue(
			'LG_PostalCity__c',
			'LG_InstallationCity__c'
		);
		acc.LG_PostalPostalCode__c = getBasketFieldValue(
			'LG_PostalCity__c',
			'LG_InstallationCity__c'
		);

		acc.LG_PostalCountry__c = String.isNotBlank(basket.LG_PostalCountry__c)
			? basket.LG_PostalCountry__c
			: basket.LG_InstallationCountry__c;
		acc.LG_PostalHouseNumber__c = String.isNotBlank(basket.LG_PostalHouseNumber__c)
			? basket.LG_PostalHouseNumber__c
			: basket.LG_InstallationHouseNumber__c;
		acc.LG_PostalStreet__c = String.isNotBlank(basket.LG_PostalStreet__c)
			? basket.LG_PostalStreet__c
			: basket.LG_InstallationStreet__c;
		acc.LG_PostalHouseNumberExtension__c = String.isNotBlank(
				basket.LG_PostalHouseNumberExtension__c
			)
			? basket.LG_PostalHouseNumberExtension__c
			: basket.LG_InstallationHouseNumberExtension__c;
		acc.Type = 'Customer';
		if (
			basket.LG_MobileCompetitorContractEndDate__c !=
			acc.LG_MobileCompetitorContractEndDate__c &&
			basket.LG_MobileCompetitorContractEndDate__c != null &&
			!basket.LG_Mobile_Contract_End_Date_Unknown__c
		) {
			acc.LG_MobileCompetitorContractEndDate__c = basket.LG_MobileCompetitorContractEndDate__c;
		}
		if (basket.LG_Mobile_Contract_End_Date_Unknown__c) {
			acc.LG_MobileCompetitorContractEndDate__c = null;
		}
	}

	private String getBasketFieldValue(String preferredField, String secondaryField) {
		String preferredFieldValue = (String) basket.get(preferredField);
		String secondaryFieldValue = (String) basket.get(secondaryField);
		return String.isNotBlank(preferredFieldValue) ? preferredFieldValue : secondaryFieldValue;
	}

	private void handleOpportunity() {
		// Residental or Soho Migration Data
		opp.LG_ClientNumber__c = basket.LG_ClientNumber__c;
		opp.LG_ExistingSubscription__c = basket.LG_ExistingSubscription__c;
		opp.LG_DTV__c = basket.LG_DTV__c;
		opp.LG_IPAddress__c = basket.LG_IPAddress__c;
		opp.LG_Telephony__c = basket.LG_Telephony__c;
		// Stage Name
		if (basket.LG_ResultofVisit__c == 'Quote Requested') {
			opp.StageName = 'Awareness of interest';
		} else if (basket.LG_ResultofVisit__c == 'Customer Approved') {
			opp.StageName = 'Customer Approved';
		} else if (basket.LG_ResultofVisit__c == 'Customer Rejected') {
			opp.StageName = 'Closed Lost';
			opp.LG_MainReasonLost__c = basket.LG_MainReasonLost__c;
		}
		// Other Fields
		opp.Probability = LG_OpportunityStageCache.DefaultProbability(opp.StageName);
		opp.Description = basket.LG_VisitDescription__c;
		opp.LG_PreferredInstallationDate__c = basket.LG_PreferredInstallationDate__c;
		opp.LG_PreferredInstallationTime__c = basket.LG_PreferredInstallationTime__c;
		opp.LG_OriginatingProductBasketId__c = basket.Id;
		opp.CloseDate = System.today();

		if (basket.LG_CreatedFrom__c == 'Tablet' && opp.StageName == 'Awareness of interest') {
			opp.LG_AutomatedQuoteDelivery__c = 'Quote Requested';
			opp.LG_CreatedFrom__c = 'Tablet';
			opp.LG_SignedQuoteAvailable__c = 'false';
		}
	}

	private void handlePremise() {
		premise = LG_Util.createNewPremise(basket, opp);
	}

	private void getPremise() {
		Id premiseId = premise.Id;
		if (premiseId == null) {
			return;
		}
		String soql =
			'SELECT ' +
			SObjectService.getObjectFieldsAsCSV(cscrm__Address__c.getSobjectType()) +
			' FROM cscrm__Address__c WHERE Id = :premiseId';
		premise = Database.query(String.escapeSingleQuotes(soql));
	}

	private void handleBillingAccount() {
		if (
			String.isNotBlank(basket.LG_BankNumber__c) ||
			String.isNotBlank(basket.LG_BankAccountName__c) ||
			basket.LG_PaymentType__c != null
		) {
			billingAcc = LG_Util.createNewBillingAccount(basket, opp);
		}
	}

	private Map<String, Contact> getConsByEmailMap() {
		Map<String, Contact> consByEmail = new Map<String, Contact>();
		for (Contact c : cons.values()) {
			if (c.Email != null) {
				consByEmail.put(c.Email.toLowerCase(), c);
			}
		}
		return consByEmail;
	}

	private Contact getExistingAdminContact(String email, Map<String, Contact> consByEmail) {
		return getExistingContact('LG_AdminContactPicklist__c', email, consByEmail);
	}

	private Contact getExistingTechContact(String email, Map<String, Contact> consByEmail) {
		return getExistingContact('LG_TechnicalContactPicklist__c', email, consByEmail);
	}

	private Contact getExistingContact(
		String basketContactFieldName,
		String email,
		Map<String, Contact> consByEmail
	) {
		Contact existingCon;
		String contactId = (String) basket.get(basketContactFieldName);
		if (contactId != null && GeneralUtils.isValidId(contactId)) {
			existingCon = cons.get(contactId);
		}
		if (existingCon == null && email != null) {
			existingCon = consByEmail.get(email.toLowerCase());
		}
		return existingCon;
	}

	private void handleContacts() {
		Map<String, Contact> consByEmail = getConsByEmailMap();
		adminCon = createBasketContact(ContactType.ADMIN);
		Contact existingAdminCon = getExistingAdminContact(adminCon.Email, consByEmail);
		if (existingAdminCon != null) {
			copyContactFields(existingAdminCon, adminCon);
			adminCon = existingAdminCon;
		}
		if (!isValidContact(adminCon)) {
			adminCon = null;
		}

		techCon = createBasketContact(ContactType.TECHNICAL);
		if (basket.LG_TechContactSameasBillingContact__c || isSameContact(techCon, adminCon)) {
			techCon = adminCon;
		} else {
			Contact existingTechCon = getExistingTechContact(techCon.Email, consByEmail);
			if (existingTechCon != null) {
				copyContactFields(existingTechCon, techCon);
				techCon = existingTechCon;
			}
			if (!isValidContact(techCon)) {
				techCon = null;
			}
		}
	}

	private Contact createBasketContact(ContactType cType) {
		Contact con = new Contact();
		if (cType == ContactType.ADMIN) {
			con.Birthdate = basket.LG_AdminContactDateofBirth__c;
			con.Email = basket.LG_AdminContactEmail__c;
			con.FirstName = basket.LG_AdminContactFirstName__c;
			con.LastName = basket.LG_AdminContactLastName__c;
			con.MobilePhone = basket.LG_AdminContactMobile__c;
			con.Phone = basket.LG_AdminContactPhone__c;
			con.Salutation = basket.LG_AdminContactSalutation__c;
		} else if (cType == ContactType.TECHNICAL) {
			con.Birthdate = basket.LG_TechnicalContactDateofBirth__c;
			con.Email = basket.LG_TechnicalContactEmail__c;
			con.FirstName = basket.LG_TechnicalContactFirstName__c;
			con.LastName = basket.LG_TechnicalContactLastName__c;
			con.MobilePhone = basket.LG_TechnicalContactMobile__c;
			con.Phone = basket.LG_TechnicalContactPhone__c;
			con.Salutation = basket.LG_TechnicalContactSalutation__c;
		}
		con.AccountId = acc.Id;
		return con;
	}

	private Boolean isValidContact(Contact con) {
		return String.isNotBlank(con.FirstName) &&
			String.isNotBlank(con.LastName) &&
			String.isNotBlank(con.Email);
	}

	private Boolean copyContactFields(Contact originalContact, Contact newContact) {
		Boolean isChanged = false;
		for (String field : CONTACT_FIELDS) {
			if (
				newContact.get(field) != null &&
				originalContact.get(field) != newContact.get(field)
			) {
				originalContact.put(field, newContact.get(field));
				isChanged = true;
			}
		}
		return isChanged;
	}

	private Boolean isSameContact(Contact originalContact, Contact newContact) {
		if (newContact == null) {
			return false;
		}
		for (String field : CONTACT_FIELDS) {
			if (originalContact.get(field) != newContact.get(field)) {
				return false;
			}
		}
		return true;
	}

	private void updateTehnicalContact() {
		if (techCon != null && premise != null) {
			premise.LG_TechnicalContact__c = techCon.Id;
			update premise;
		}
	}

	private void createOpportunityTask() {
		Task tsk = new Task(
			Type = 'Result of D2D Visit',
			Subject = 'Visit',
			ActivityDate = Date.today(),
			Status = 'Completed',
			WhatId = opp.Id
		);
		if (
			opp.StageName == 'Negotiation/Review' ||
			opp.StageName == 'Awareness of interest' ||
			opp.StageName == 'Quotation Delivered'
		) {
			tsk.LG_Result__c = 'Quote';
		} else if (opp.StageName == 'Customer Approved') {
			tsk.LG_Result__c = 'Sales';
		} else {
			tsk.LG_Result__c = 'Not interested';
		}
		insert tsk;
	}

	private void createOpportunityContactRoles() {
		// Delete existing roles if any exist
		List<OpportunityContactRole> existingRoles = [
			SELECT Id, Role, IsPrimary
			FROM OpportunityContactRole
			WHERE OpportunityId = :opp.Id AND (Role = NULL OR Role = '')
		];
		if (!existingRoles.isEmpty()) {
			delete existingRoles;
		}
		// Create new Roles
		List<OpportunityContactRole> newRoles = new List<OpportunityContactRole>();
		if (adminCon != null && adminCon.Id != null) {
			newRoles.add(
				new OpportunityContactRole(
					ContactId = adminCon.Id,
					IsPrimary = true,
					OpportunityId = opp.Id,
					Role = 'Administrative Contact'
				)
			);
		}
		if (techCon != null && techCon.Id != null) {
			newRoles.add(
				new OpportunityContactRole(
					ContactId = techCon.Id,
					IsPrimary = false,
					OpportunityId = opp.Id,
					Role = 'Technical Contact'
				)
			);
		}
		if (!newRoles.isEmpty()) {
			insert newRoles;
		}
	}

	private void setBillingAccountOnAttributeFields() {
		// Retrieve Attribute Fields for line items and Billing Accounts
		List<cscfga__Attribute_Field__c> attributeFields = [
			SELECT
				Id,
				Name,
				cscfga__Value__c,
				cscfga__Attribute__r.Name,
				cscfga__Attribute__r.cscfga__Is_Line_Item__c,
				cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__r.AccountId,
				cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Basket__r.LG_BillingAccount__c
			FROM cscfga__Attribute_Field__c
			WHERE
				Name = 'BillingAccount'
				AND cscfga__Attribute__r.cscfga__Is_Line_Item__c = TRUE
				AND cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basket.Id
		];
		if (!attributeFields.isEmpty()) {
			for (cscfga__Attribute_Field__c a : attributeFields) {
				a.cscfga__Value__c = basket.LG_BillingAccount__c;
			}
			update attributeFields;
		}
	}

	private void handlePortingNumbers() {
		deleteExistingPortingNumbers();
		createNewPortingNumbers();
	}

	private void deleteExistingPortingNumbers() {
		List<LG_PortingNumber__c> existingPortingNumbers = [
			SELECT Id
			FROM LG_PortingNumber__c
			WHERE
				LG_Type__c IN ('Mobile Telephony', 'SOHO Telephony')
				AND LG_Opportunity__c = :basket.cscfga__Opportunity__c
		];
		if (!existingPortingNumbers.isEmpty()) {
			delete existingPortingNumbers;
		}
	}

	private void createNewPortingNumbers() {
		List<LG_PortingNumber__c> newPortingNumbers = new List<LG_PortingNumber__c>();
		List<cscfga__Attribute__c> portingAttributes = [
			SELECT
				Id,
				Name,
				cscfga__Attribute_Definition__r.LG_ProductDetailType__c,
				cscfga__Product_Configuration__c,
				cscfga__Product_Configuration__r.cscfga__Product_Basket__c,
				cscfga__Value__c
			FROM cscfga__Attribute__c c
			WHERE
				cscfga__Attribute_Definition__r.LG_ProductDetailType__c IN (
					'JSON Porting SOHO Telephony',
					'JSON Porting Mobile Telephony'
				)
				AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basket.Id
		];
		for (cscfga__Attribute__c att : portingAttributes) {
			String portingStructureType;
			if (
				att.cscfga__Attribute_Definition__r.LG_ProductDetailType__c ==
				'JSON Porting SOHO Telephony'
			) {
				portingStructureType = 'SOHO Telephony';
			} else {
				portingStructureType = 'Mobile Telephony';
			}
			if (!Test.isRunningTest()) {
				newPortingNumbers.addAll(
					LG_TelephoneUtility.createPortingStructure(
						portingStructureType,
						att.cscfga__Value__c,
						att.cscfga__Product_Configuration__c,
						opp.Id
					)
				);
			}
		}
		if (!newPortingNumbers.isEmpty()) {
			insert newPortingNumbers;
		}
	}

	private void setAddressAndInstallationWishDate() {
		if (basket.LG_CreatedFrom__c != 'Tablet') {
			return;
		}
		// Fetch all pcs from the uploaded basket where address or wish data should be set
		Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>(
			[
				SELECT
					Id,
					LG_Address__c,
					LG_InstallationWishDate__c,
					cscfga__Product_Basket__c,
					cscfga__Product_Basket__r.LG_BasketNumber__c
				FROM cscfga__Product_Configuration__c
				WHERE
					cscfga__Product_Basket__c = :basket.Id
					AND (LG_Address__c = NULL
					OR LG_InstallationWishDate__c = NULL)
				FOR UPDATE
			]
		);
		List<cscfga__Product_Configuration__c> configsForUpdate = new List<cscfga__Product_Configuration__c>();
		List<cscfga__Product_Configuration__c> configsWithNewAddress = new List<cscfga__Product_Configuration__c>();
		for (cscfga__Product_Configuration__c config : configs.values()) {
			Boolean isChanged = false;
			if (premise != null && config.LG_Address__c == null) {
				config.LG_Address__c = premise.Id;
				config.LG_Site__c = site.Id;
				configsWithNewAddress.add(config);
				isChanged = true;
			}
			if (config.LG_InstallationWishDate__c == null) {
				config.LG_InstallationWishDate__c = basket.LG_PreferredInstallationDate__c;
				isChanged = true;
			}
			if (isChanged) {
				configsForUpdate.add(config);
			}
		}
		if (!configsForUpdate.isEmpty()) {
			update configsForUpdate;
		}
		updateConfigRequests(configsWithNewAddress);
		updateAttributeData(configsWithNewAddress);
	}

	private void updateConfigRequests(List<cscfga__Product_Configuration__c> productConfigs) {
		if (productConfigs.isEmpty()) {
			return;
		}
		Set<Id> productConfigIds = GeneralUtils.getIDSetFromList(productConfigs, 'Id');
		Map<Id, cscfga__Product_Configuration__c> configs = new Map<Id, cscfga__Product_Configuration__c>(
			[
				SELECT
					Id,
					LG_Address__r.Id,
					LG_Address__r.cscrm__City__c,
					LG_Address__r.cscrm__Zip_Postal_Code__c,
					LG_Address__r.LG_HouseNumberExtension__c,
					LG_Address__r.LG_HouseNumber__c,
					LG_Address__r.cscrm__Street__c,
					LG_Address__r.LG_AddressID__c,
					cscfga__Product_Basket__c,
					cscfga__Product_Basket__r.csbb__Account__c
				FROM cscfga__Product_Configuration__c
				WHERE Id IN :productConfigIds
			]
		);
		List<csbb__Product_Configuration_Request__c> configReqs = [
			SELECT Id, csbb__Optionals__c, csbb__Product_Configuration__c
			FROM csbb__Product_Configuration_Request__c
			WHERE csbb__Product_Configuration__c IN :productConfigIds
		];
		for (csbb__Product_Configuration_Request__c configReq : configReqs) {
			cscfga__Product_Configuration__c config = configs.get(
				configReq.csbb__Product_Configuration__c
			);
			if (config.LG_Address__r != null) {
				configReq.csbb__Optionals__c = JSON.serialize(
					new LG_AddressResponse.OptionalsJson(config.LG_Address__r)
				);
			}
		}
		update configReqs;
	}

	private void updateAttributeData(List<cscfga__Product_Configuration__c> configsWithNewAddress) {
		Set<Id> configsWithNewAddressIds = GeneralUtils.getIDSetFromList(
			configsWithNewAddress,
			'Id'
		);
		Map<Id, cscfga__Product_Configuration__c> allProductConfigurations = new Map<Id, cscfga__Product_Configuration__c>(
			[
				SELECT
					Id,
					LG_Address__c,
					LG_MarketSegment__c,
					LG_InstallationWishDate__c,
					cscfga__Product_Basket__c,
					cscfga__Product_Basket__r.LG_BasketNumber__c,
					cscfga__Product_Family__c
				FROM cscfga__Product_Configuration__c
				WHERE cscfga__Product_Basket__c = :basket.Id
			]
		);
		Set<String> attributeNames = new Set<String>{
			'Basket Number',
			'Order Number',
			'Premise Id',
			'Premise Number',
			'Site Id'
		};
		List<cscfga__Attribute__c> attributes = [
			SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c
			FROM cscfga__Attribute__c
			WHERE
				cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basket.Id
				AND Name IN :attributeNames
			ORDER BY Name DESC
		];

		List<cscfga__Attribute__c> attributesUpdate = new List<cscfga__Attribute__c>();

		// If there are attributes with empty value, populate them
		for (cscfga__Attribute__c attribute : attributes) {
			if (String.isNotBlank(attribute.cscfga__Value__c)) {
				continue;
			}
			cscfga__Product_Configuration__c pc = allProductConfigurations.get(
				attribute.cscfga__Product_Configuration__c
			);
			Boolean isAddresChanged = configsWithNewAddressIds.contains(pc.Id);
			setAttributeValue(attribute, pc, isAddresChanged);
			attributesUpdate.add(attribute);
		}
		if (!attributesUpdate.isEmpty()) {
			update attributesUpdate;
		}
	}

	private void setAttributeValue(
		cscfga__Attribute__c attribute,
		cscfga__Product_Configuration__c pc,
		Boolean isAddresChanged
	) {
		if (attribute.Name == 'Basket Number') {
			attribute.cscfga__Value__c = pc.cscfga__Product_Basket__r.LG_BasketNumber__c;
		}
		if (!isAddresChanged) {
			return;
		}
		if (attribute.Name == 'Order Number') {
			attribute.cscfga__Value__c =
				pc.cscfga__Product_Basket__r.LG_BasketNumber__c + premise.LG_PremiseNumber__c;
		} else if (attribute.Name == 'Premise Id') {
			attribute.cscfga__Value__c = premise.Id;
		} else if (attribute.Name == 'Premise Number') {
			attribute.cscfga__Value__c = premise.LG_PremiseNumber__c;
		} else if (attribute.Name == 'Site Id') {
			attribute.cscfga__Value__c = site.Id;
		}
	}

	private void updateOpportunityAttachments() {
		if (opp.StageName != 'Customer Approved') {
			return;
		}
		opp.LG_AutomatedQuoteDelivery__c = 'Quote Sent';
		List<Attachment> attachments = [
			SELECT Id, Name, Body, ContentType, ParentId
			FROM Attachment
			WHERE ParentId = :basket.Id
		];
		List<Attachment> newAttachments = new List<Attachment>();
		Attachment signedQuoteAttachment;
		for (Attachment att : attachments) {
			if (att.ContentType == 'application/pdf') {
				Attachment newAtt = new Attachment(
					Name = 'Uw getekende opdrachtbevestiging van Ziggo ' +
						DateTime.now().format('dd-MM-yyyy') +
						'.pdf',
					Body = att.Body,
					ParentId = opp.Id,
					ContentType = att.ContentType,
					Description = (basket.LG_IsQuoteSigned__c) ? 'Signed Quote' : 'Unsigned Quote'
				);
				newAttachments.add(newAtt);
				if (basket.LG_IsQuoteSigned__c) {
					signedQuoteAttachment = newAtt;
				}
			}
		}
		if (!newAttachments.isEmpty()) {
			insert newAttachments;
			if (signedQuoteAttachment != null) {
				opp.LG_SignedQuoteAvailable__c = 'true';
				opp.LG_SignedQuoteAttachmentId__c = signedQuoteAttachment.Id;
			} else {
				opp.LG_SignedQuoteAvailable__c = 'false';
			}
		}
		update opp;
	}

	public class DoorToDoorBasketException extends Exception {
	}

	private enum ContactType {
		ADMIN,
		TECHNICAL
	}
}