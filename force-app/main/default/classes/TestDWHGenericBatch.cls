@isTest
public class TestDWHGenericBatch {
    private static User admin;

    @testSetup
    static void setup() {
        //Data preparation for sync table
        TestUtils.autoCommit = false;

        list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
        fsmlist.add(
            TestUtils.createSync(
                'DWH_Account_SF_Interface__c -> Account',
                'TmeActive__c',
                'Active_Company__c'
            )
        );
        fsmlist.add(TestUtils.createSync('DWH_Lead_dummy__c -> Lead', 'CompanyName__c', 'Company'));
        fsmlist.add(
            TestUtils.createSync(
                'DWH_Asset_SF_Interface__c -> VF_Asset__c',
                'CommercialProductCode__c',
                'Priceplan_Name__c'
            )
        );
        fsmlist.add(
            TestUtils.createSync(
                'DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c',
                'CustomerNumber__c',
                'Customer_Number__c'
            )
        );
        fsmlist.add(
            TestUtils.createSync(
                'DWH_KvkDelta_SF_Interface__c -> VF_Asset__c',
                'Today__c',
                'Retention_Date__c'
            )
        );
        insert fsmlist;

        // Data preparation for destination records
        TestUtils.autoCommit = true;
        admin = TestUtils.createAdministrator();

        TestUtils.autoCommit = false;
        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c = '12345678';
        upsert acc;

        Id ziggoRec = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName()
            .get('IB Ziggo')
            .getRecordTypeId();

        VF_Asset__c ass = new VF_Asset__c(
            RecordTypeId = ziggoRec,
            account__c = acc.id,
            account_KVK__c = '12345678',
            Priceplan_Class__c = 'fZiggo IB'
        );
        insert ass;

        //Data preparation for load table

        list<SObject> loadList = new List<SObject>{
            //existing account
            new DWH_Account_SF_Interface__c(
                TmeCode__c = 'TME_12345678',
                CompanyKvkNumber__c = '12345678',
                CompanyLocationCode__c = '9257RN|18|abcd|',
                TmeNumberOfEmployees__c = 3,
                CompanyNumberOfEmployees__c = 2
            ),
            //lead
            new DWH_Account_SF_Interface__c(
                TmeCode__c = 'TME_12345679',
                CompanyKvkNumber__c = '12345679',
                CompanyLocationCode__c = '9657RN|18||',
                TmeNumberOfEmployees__c = 3
            ),
            //new account
            new DWH_Account_SF_Interface__c(
                TmeCode__c = 'TME_12568789',
                CompanyKvkNumber__c = '01237777',
                CompanyLocationCode__c = '9658RN|18|asdf|',
                CompanyIsActiveCustomer__c = true,
                TmeNumberOfEmployees__c = 7
            ),
            //new delta
            new DWH_KvkDelta_SF_Interface__c(KvkNumber__c = '12345678'),
            //new asset
            new DWH_Asset_SF_Interface__c(
                CommercialProductCode__c = 'CP_IPVPNPRMM5MBPSFBR',
                CustomerNumber__c = '12345678',
                KvkNumber__c = '12345678',
                LocationCode__c = '9257RN|18||',
                Country__c = 'NL',
                City__c = 'Amsterdam',
                Street__c = 'Hoofdstraat'
            ),
            //new relation
            new DWH_Relation_SF_Interface__c(
                MatchStart__c = '01012001',
                CustomerNumber__c = '12345678',
                KvkNumber__c = '12345678'
            )
        };

        insert loadList;
    }

    @isTest
    static void testBatchAccount() {
        TestUtils.autoCommit = false;

        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c = '01234567';
        insert acc;

        DWH_Account_SF_Interface__c dwhAcc = new DWH_Account_SF_Interface__c(
            TmeCode__c = 'TME_01234567',
            CompanyKvkNumber__c = '01234567',
            CompanyLocationCode__c = '9257RN|18|asdf|',
            CompanyNumberOfEmployees__c = 2
        );

        insert dwhAcc;

        Test.startTest();

        DWHGenericBatch controller = new DWHGenericBatch('DWH_Account_SF_Interface__c');
        controller.chain = false;
        Database.executeBatch(controller);

        Test.stopTest();

        DWH_Account_SF_Interface__c accInterfaceLst = [
            SELECT Id, Error__c, Processed__c
            FROM DWH_Account_SF_Interface__c
            WHERE TmeCode__c = 'TME_01234567'
            LIMIT 1
        ];
        Account account = [
            SELECT Id, Target_Market_Entity__c
            FROM Account
            WHERE KVK_number__c = '01234567'
            LIMIT 1
        ];

        System.assertEquals(
            true,
            accInterfaceLst.Processed__c,
            'DWH Account Interface should have been processed.'
        );
        System.assert(
            String.isEmpty(accInterfaceLst.Error__c),
            'DWH Account Interface should have no errors.'
        );
        System.assertEquals(
            'Subsidiary',
            account.Target_Market_Entity__c,
            'Target market entry should be set.'
        );
    }

    @isTest
    static void testBatchAsset() {
        Test.startTest();

        DWHGenericBatch controller = new DWHGenericBatch('DWH_Asset_SF_Interface__c');
        controller.chain = false;
        Database.executeBatch(controller);

        Test.stopTest();

        DWH_Asset_SF_Interface__c aassetInterfaceLst = [
            SELECT Id, Error__c, Processed__c, Target__c
            FROM DWH_Asset_SF_Interface__c
            WHERE CommercialProductCode__c = 'CP_IPVPNPRMM5MBPSFBR'
            LIMIT 1
        ];
        List<VF_Asset__c> asset = [SELECT Id, account_KVK__c, account__c FROM VF_Asset__c];

        System.assertEquals(
            true,
            aassetInterfaceLst.Processed__c,
            'DWH Asset Interface should have been processed.'
        );
        System.assert(
            String.isEmpty(aassetInterfaceLst.Error__c),
            'DWH Asset Interface should have no errors.'
        );
        System.assertEquals(2, asset.size(), 'There should be a new asset created.');
    }

    @isTest
    static void testBatchDelta() {
        TestUtils.autoCommit = false;

        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c = '01234567';
        insert acc;

        DWH_KvkDelta_SF_Interface__c dwhObject = new DWH_KvkDelta_SF_Interface__c(
            KvkNumber__c = '01234567'
        );
        insert dwhObject;

        List<VF_Asset__c> assetPre = [SELECT Id, account_KVK__c, account__c FROM VF_Asset__c];

        Test.startTest();

        DWHGenericBatch controller = new DWHGenericBatch('DWH_KvkDelta_SF_Interface__c');
        controller.chain = false;
        Database.executeBatch(controller);

        Test.stopTest();

        List<VF_Asset__c> assetPost = [SELECT Id, account_KVK__c, account__c FROM VF_Asset__c];

        System.assertEquals(0, assetPost.size(), 'DWH Asset Interface should have been processed.');
        System.assert(
            assetPre.size() > assetPost.size(),
            'DWH Asset Interface should have been processed.'
        );
    }

    @isTest
    static void testBatchRelation() {
        TestUtils.autoCommit = false;

        Test.startTest();

        DWHGenericBatch controller = new DWHGenericBatch('DWH_Relation_SF_Interface__c');
        controller.chain = false;
        Database.executeBatch(controller);

        Test.stopTest();

        List<DWH_Relation_SF_Interface__c> relationInterface = [
            SELECT Id, Processed__c, Error__c
            FROM DWH_Relation_SF_Interface__c
        ];

        System.assertEquals(
            true,
            relationInterface[0].Processed__c,
            'The record should be processed.'
        );
        System.assert(
            String.isEmpty(relationInterface[0].Error__c),
            'The record should be processed.'
        );
    }

    @isTest
    static void testBatchChain() {
        TestUtils.autoCommit = false;

        List<Account> accs = new List<Account>();

        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c = '01234567';
        accs.add(acc);

        Account acc2 = TestUtils.createAccount(admin);
        acc.KVK_number__c = '12345677';
        accs.add(acc2);

        insert accs;

        List<Lead> leads = new List<Lead>();

        Lead lead = new Lead(
            KVK_number__c = '2345678',
            LastName = 'Test',
            Company = 'Test',
            ConvertedAccountId = acc.Id,
            IsConverted = true
        );
        leads.add(lead);

        Lead lead2 = new Lead(
            KVK_number__c = '12345677',
            LastName = 'Test 2',
            Company = 'Test 2',
            ConvertedAccountId = acc.Id,
            IsConverted = true
        );
        leads.add(lead2);

        insert leads;

        List<DWH_Account_SF_Interface__c> dwhAccs = new List<DWH_Account_SF_Interface__c>();

        DWH_Account_SF_Interface__c dwhAcc = new DWH_Account_SF_Interface__c(
            TmeCode__c = 'TME_01234567',
            CompanyKvkNumber__c = '01234567',
            CompanyLocationCode__c = '9257RN|18||',
            TmeNumberOfEmployees__c = 3,
            TmeKvk__c = '02345678'
        );
        dwhAccs.add(dwhAcc);

        DWH_Account_SF_Interface__c dwhAcc2 = new DWH_Account_SF_Interface__c(
            TmeCode__c = 'TME_12345677',
            CompanyKvkNumber__c = '12345679',
            CompanyLocationCode__c = '9257RN|18||',
            TmeNumberOfEmployees__c = 3,
            TmeKvk__c = '12345677'
        );
        dwhAccs.add(dwhAcc2);

        DWH_Account_SF_Interface__c dwhAcc3 = new DWH_Account_SF_Interface__c(
            TmeCode__c = 'TME_02345679',
            CompanyKvkNumber__c = '02345679',
            CompanyLocationCode__c = '9257RN|18|abcd|',
            TmeNumberOfEmployees__c = 3,
            TmeKvk__c = '02345679'
        );
        dwhAccs.add(dwhAcc3);

        insert dwhAccs;

        List<Account> preAccList = [SELECT Id FROM Account];

        Test.startTest();

        DWHGenericBatch controller = new DWHGenericBatch('DWH_Account_SF_Interface__c');
        controller.chain = true;
        Database.executeBatch(controller);

        List<Account> postAccList = [SELECT Id FROM Account];

        Test.stopTest();
        System.assertEquals(
            preAccList.size(),
            postAccList.size(),
            'There should be new acc created.'
        );
    }

    @isTest
    static void testCreateNewSite() {
        TestUtils.autoCommit = false;

        Account acc = TestUtils.createAccount(admin);
        acc.KVK_number__c = '02345678';
        insert acc;

        List<Site__c> sites = [SELECT Id FROM Site__c];

        DWHGenericBatch.AddressInformation ai1 = createAddressInfo('');
        DWHGenericBatch.AddressInformation ai2 = createAddressInfo(' |  |  | ');
        DWHGenericBatch.AddressInformation ai3 = createAddressInfo(' |18||');
        DWHGenericBatch.AddressInformation ai4 = createAddressInfo('9257RN|18||');
        DWHGenericBatch.AddressInformation ai5 = createAddressInfo('9257RN|18|ab|');
        DWHGenericBatch.AddressInformation ai6 = createAddressInfo('9257RN|18|abcdef|');

        DWHGenericBatch controller = new DWHGenericBatch('DWH_Account_SF_Interface__c');
        controller.createNewSite(acc, ai1, 4);
        controller.createNewSite(acc, ai2, 4);
        controller.createNewSite(acc, ai3, 2);
        controller.createNewSite(acc, ai4, 2);
        controller.createNewSite(acc, ai5, 1);
        controller.createNewSite(acc, ai6, 2);

        List<Site__c> newSites = [SELECT Id FROM Site__c];

        System.assertEquals(
            sites.size() + 1,
            newSites.size(),
            'In the six calls one new site should be created.'
        );
    }

    @isTest
    static void testGetScopeKeyDestination() {
        DWHGenericBatch controller = new DWHGenericBatch('DWH_Account_SF_Interface__c');
        String emptyRes = controller.getScopeKeyDestination('');
        String accInter = controller.getScopeKeyDestination('DWH_Account_SF_Interface__c');
        String kvkDeltaInter = controller.getScopeKeyDestination('DWH_KvkDelta_SF_Interface__c');

        System.assertEquals(emptyRes, '', 'Returned string should be empty.');
        System.assertEquals(accInter, 'KVK_number__c', 'Returned string in wrong format.');
        System.assertEquals(kvkDeltaInter, 'Account_KVK__c', 'Returned string in wrong format.');
    }

    private static DWHGenericBatch.AddressInformation createAddressInfo(String locationCode) {
        String street = 'Hoofdstraat';
        String city = 'Amsterdam';
        String country = 'NL';

        DWHGenericBatch.AddressInformation ai = new DWHGenericBatch.AddressInformation();
        ai.locationCode = locationCode;
        ai.street = street;
        ai.city = city;
        ai.country = country;
        return ai;
    }
}