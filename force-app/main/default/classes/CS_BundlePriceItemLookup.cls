global with sharing class CS_BundlePriceItemLookup extends cscfga.ALookupSearch{

public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit){

    final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1; 
    Integer recordOffset = pageOffset * pageLimit;
    
    List<cspmb__Price_Item__c> priceItemsFinal = new List<cspmb__Price_Item__c>();
    Set<Id> priceItemsToRemove = new Set<Id>();
    
    system.debug('++++ searchFields: ' + JSON.serializePretty(searchFields));   
    
    
    String accessType = '%,' + searchFields.get('Access Type') + ',%';
    String region = (searchFields.get('Region') == null ||(searchFields.get('Region') == '')) ? '' : searchFields.get('Region');
    String vendor = searchFields.get('Vendor');
    String resultCheck = searchFields.get('Result Check');
    String cpeSla = searchFields.get('CPE SLA');
    
    Boolean ipvpn = (searchFields.get('IPVPN') == null ||(searchFields.get('IPVPN') == ''))? false : true;
    Boolean managedInternet = (searchFields.get('Managed Internet') == null ||(searchFields.get('Managed Internet') == '')) ? false : true;
    Boolean oneFixed = (searchFields.get('OneFixed') == null ||(searchFields.get('OneFixed') == '')) ? false : true;
    Boolean oneNet = (searchFields.get('OneNet') == null ||(searchFields.get('OneNet') == '')) ? false : true;
    String oneFixedScenario = (searchFields.get('Scenario OneFixed') == null || (searchFields.get('Scenario OneFixed') == ''))? '' : searchFields.get('Scenario OneFixed');
    String oneNetScenario = (searchFields.get('Scenario OneNet') == null || (searchFields.get('Scenario OneNet') == ''))? '' : searchFields.get('Scenario OneNet');
    
    Decimal minBandwidthUpInternet = ((searchFields.get('Min. Upstream Bandwidth Internet') == null) ||(searchFields.get('Min. Upstream Bandwidth Internet') == '')) ? 0 : Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth Internet'));    
    Decimal minBandwidthDownInternet = ((searchFields.get('Min.Downstream Bandwidth Internet') == null)||(searchFields.get('Min.Downstream Bandwidth Internet') == '')) ? 0 : Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth Internet'));
    Decimal minBandwidthDownOneFixed = ((searchFields.get('Min Bandwidth OneFixed') == null)||(searchFields.get('Min Bandwidth OneFixed') == '')) ? 0 : Decimal.valueOf(searchFields.get('Min Bandwidth OneFixed'));
    Decimal minBandwidthUpOneFixed = ((searchFields.get('Min Bandwidth OneFixed') == null) ||(searchFields.get('Min Bandwidth OneFixed') == ''))? 0 : Decimal.valueOf(searchFields.get('Min Bandwidth OneFixed'));
    Decimal availableBandwidthUp = ((searchFields.get('Available bandwidth up') == null) ||(searchFields.get('Available bandwidth up') == ''))? 0 : Decimal.valueOf(searchFields.get('Available bandwidth up'));
    Decimal availableBandwidthDown = ((searchFields.get('Available bandwidth down') == null) ||(searchFields.get('Available bandwidth down') == '')) ? 0 : Decimal.valueOf(searchFields.get('Available bandwidth down'));

    Decimal premiumBandwidthUp = ((searchFields.get('Bandwidth Up Premium') == null) ||(searchFields.get('Bandwidth Up Premium') == ''))? 0 : Decimal.valueOf(searchFields.get('Bandwidth Up Premium'));
    
    Decimal sipChannels = ((searchFields.get('SIP Channels') == null) ||(searchFields.get('SIP Channels') == '')) ? 0 : Decimal.valueOf(searchFields.get('SIP Channels'));
    String codec = ((searchFields.get('Codec') == null) ||(searchFields.get('Codec') == ''))? '' : searchFields.get('Codec');
    String techType = ((searchFields.get('Technology type') == null)||(searchFields.get('Technology type') == '')) ? '' : searchFields.get('Technology type');
    
    Boolean wirelessBackup = (searchFields.get('Wireless backup') == 'No' || searchFields.get('Wireless backup') == 'false')? false : true;
    String proposition = (searchFields.get('Proposition') == null ||(searchFields.get('Proposition') == '')) ? '' : searchFields.get('Proposition');
    String propositionInternet = (searchFields.get('Proposition Internet') == null ||(searchFields.get('Proposition Internet') == '')) ? '' : searchFields.get('Proposition Internet');
    String propositionOneFixed = (searchFields.get('Proposition OneFixed') == null ||(searchFields.get('Proposition OneFixed') == '')) ? '' : searchFields.get('Proposition OneFixed');
    String propositionOneNet = (searchFields.get('Proposition OneNet') == null ||(searchFields.get('Proposition OneNet') == '')) ? '' : searchFields.get('Proposition OneNet');

    Decimal bandwidthUp = 0.00;
    Decimal bandwidthDown = 0.00;
    
    Integer duration = (searchFields.get('Contract Duration') == null ||(searchFields.get('Contract Duration') == '')) ? 0 : Integer.valueOf(searchFields.get('Contract Duration'));

    String promo = (searchFields.get('Promo') == null ||(searchFields.get('Promo') == '')) ? '' : searchFields.get('Promo');
    
    if(managedInternet && !ipvpn & !oneFixed){
        bandwidthUp += minBandwidthUpInternet;
        bandwidthDown += minBandwidthDownInternet;
    }
    
    if(managedInternet && oneFixed && !ipvpn){
        bandwidthUp += minBandwidthUpOneFixed;
        bandwidthDown += minBandwidthDownOneFixed;
    }
    
    system.debug('accessType: ' + accessType);
    system.debug('vendor: ' + vendor);
    system.debug('resultCheck: ' + resultCheck);
    system.debug('region: ' + region);
    system.debug('ipvpn: ' + ipvpn);
    system.debug('managedInternet: ' + managedInternet);
    system.debug('oneFixed: ' + oneFixed);    
    system.debug('oneNet: ' + oneNet); 
    system.debug('oneFixedScenario: ' + oneFixedScenario);   
    system.debug('minBandwidthUpInternet: ' + minBandwidthUpInternet);
    system.debug('minBandwidthDownInternet: ' + minBandwidthDownInternet);
    system.debug('minBandwidthDownOneFixed: ' + minBandwidthDownOneFixed);    
    system.debug('minBandwidthUpOneFixed: ' + minBandwidthUpOneFixed);
    system.debug('bandwidthUp: ' + bandwidthUp);    
    system.debug('bandwidthDown: ' + bandwidthDown);
    system.debug('premiumBandwidthUp: ' + premiumBandwidthUp);    
    system.debug('sipChannels: ' + sipChannels);    
    system.debug('codec: ' + codec);
    system.debug('techType: ' + techType);
    system.debug('propositionOneFixed: ' + propositionOneFixed);
    system.debug('propositionOneNet: ' + propositionOneNet);
    system.debug('****duration: ' + duration);
    system.debug('****cpeSLa: ' + cpeSla);
    system.debug('****promo: ' + promo);
    
    
     Map<Id,cspmb__Price_Item__c> priceItems = new Map<Id,cspmb__Price_Item__c>([SELECT Id, Name, cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Infra_SLA__c, Overbooking__c, Vendor__c, Vendor_lookup__r.Name, Service__c, Duration__c, cspmb__recurring_charge__c, 
                           cspmb__one_off_cost__c, cspmb__recurring_cost__c,Product__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c, Region__c, SLA_Method__c, CPE_SLA__c, Category__c, Category_lookup__r.Name, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, 
                           Number_of_LAN__c, Number_of_LANWAN__c, Number_of_WAN__c, Wireless_compatible__c, Hardware_Toeslag__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c, WAN_LANWAN_M_LAN__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, 
                           One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c, Extra_Product__c, ProductConfigNameText__c, Deal_Type_Text__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, 
                           OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c, Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, 
                           Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c, cspmb__Recurring_Charge_Code__c,Max_Quantity__c,Min_Quantity__c,cspmb__One_Off_Charge_Code__c, Promo__c
                             FROM cspmb__Price_Item__c
                             WHERE cspmb__Is_Active__c = true AND Category_lookup__r.Name = 'Bundle' AND Product__c != ''
                                AND Min_Duration__c <= :duration AND Max_Duration__c > :duration
                                AND Vendor_lookup__r.Name = :vendor AND Access_Type_Text__c LIKE :accessType
                                AND (Result_check__c = :resultCheck OR Result_Check__c = '*')
                                AND Available_bandwidth_up__c <= :availableBandwidthUp AND Available_bandwidth_down__c <= :availableBandwidthDown]);
                                /*
                                AND CPE_SLA__c=:cpeSla
                                AND Available_bandwidth_up__c >= :bandwidthUp AND Available_bandwidth_up__c <= :availableBandwidthUp
                                AND Available_bandwidth_down__c >= :bandwidthDown AND Available_bandwidth_down__c <= :availableBandwidthDown]);
                                */
      
     
     for(cspmb__Price_Item__c pi : priceItems.values()){
         //logic for filtering bundle price items
        
         if(accessType.containsIgnoreCase(',EthernetOverFiber,')){
             if((priceItems.get(pi.Id).Region__c != null && priceItems.get(pi.Id).Region__c != region) || (priceItems.get(pi.Id).Region__c == null && region != '')){               
                if(priceItems.get(pi.Id).Region__c != '*'){
                    priceItemsToRemove.add(pi.Id);
                    system.debug('Remove Fiber Region: ' + pi);
                }
             }
         }
         
        if (accessType != null && accessType.containsIgnoreCase('Coax')) {
            if (priceItems.get(pi.Id).Region__c != region && priceItems.get(pi.Id).Region__c != '*') {
                priceItemsToRemove.add(pi.Id);
            }
        }
         
         if(proposition != '' && ipvpn){
             if(!priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase(proposition)){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove proposition != blank && ipvpn: ' + pi);                 
             }
         }
         
         if(!ipvpn){
             if(priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase(proposition)){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove !ipvpn: ' + pi);                 
             }
         }

         if(oneFixed || oneNet)
         {
             if(priceItems.get(pi.Id).Available_bandwidth_up__c > premiumBandwidthUp )
             {
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove premium bandwidth up unsufficient: ' + pi);  
             }
         }
         
         if(propositionInternet != '' && managedInternet){
             if(!priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase(propositionInternet)){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove propositionInternet != blank && managedInternet: ' + pi);                 
             }
         }
         
         if(!managedInternet){
             if(priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase(propositionInternet)){
                priceItemsToRemove.add(pi.Id);
                system.debug('!managedInternet: ' + pi);                 
             }
         }
         
         if(propositionOneNet != '' && oneNet){
             if(!priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase(propositionOneNet)){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove propositionInternet != blank && managedInternet: ' + pi);                 
             }
         }
         
         if(!oneNet){
             if(priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase(propositionOneNet)){
                priceItemsToRemove.add(pi.Id);
                system.debug('!managedInternet: ' + pi);                 
             }
         }
         
         if(propositionOneFixed != '' && oneFixed){
             if(!priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase(propositionOneFixed)){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove propositionOneFixed != blank && oneFixed: ' + pi);                 
             }
         }
         
         if(!oneFixed){
             if(priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase(propositionOneFixed)){
                priceItemsToRemove.add(pi.Id);
                system.debug('!oneFixed: ' + pi);                 
             }
         }
         /**
          * 
          * Removed as part of requested wireless backup change
          * 
          */
         /*
         if(managedInternet && wirelessBackup){
             if(!priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase('4G')){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove managedInternet && wirelessBackup: ' + pi);                 
             }
         }
         
         if(managedInternet && !wirelessBackup){
             if(priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase('4G')){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove managedInternet && wirelessBackup: ' + pi);                 
             }
         }
         */
         if(oneFixed || oneNet){
             if((priceItems.get(pi.Id).OneFixed_Codec__c != null && priceItems.get(pi.Id).OneFixed_Codec__c != codec) || (priceItems.get(pi.Id).OneFixed_Codec__c == null && codec != '')){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove OneFixed codec: ' + pi);
             }
             
            if((priceItems.get(pi.Id).OneFixed_SIP_Channels__c != null && priceItems.get(pi.Id).OneFixed_SIP_Channels__c != sipChannels) || priceItems.get(pi.Id).OneFixed_SIP_Channels__c == null && sipChannels != 0){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove OneFixed SIP Channels: ' + pi);
             }
             
            if((priceItems.get(pi.Id).OneFixed_Technology_Type__c != null && priceItems.get(pi.Id).OneFixed_Technology_Type__c != techType) || (priceItems.get(pi.Id).OneFixed_Technology_Type__c == null && techType != '')){
                priceItemsToRemove.add(pi.Id);
                system.debug('Remove OneFixed technology type: ' + pi);
             }
         }
         
         //scenario
         if(oneNet && (oneNetScenario!='')){
             if(!priceItems.get(pi.Id).Bundle_Services_Text__c.containsIgnoreCase(oneNetScenario)){
                 priceItemsToRemove.add(pi.Id);
                 system.debug('Removing because OneNet scenario does not match'+pi);
             }
         }

         if(promo != ''){
            if((String.isEmpty(priceItems.get(pi.Id).Promo__c)) || (!priceItems.get(pi.Id).Promo__c.containsIgnoreCase(promo))){
                 priceItemsToRemove.add(pi.Id);
                 system.debug('Removing because Promo__c doesn\'t match'+pi);
             }
         } else {
            if(!(String.isEmpty(priceItems.get(pi.Id).Promo__c))){
                priceItemsToRemove.add(pi.Id);
                system.debug('Removing because Promo__c not important'+pi);
            }
         }
         
         
     }
                             
    priceItemsFinal = [SELECT Id, Name, cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Infra_SLA__c, Overbooking__c, Vendor__c, Vendor_lookup__r.Name, Service__c, Duration__c, cspmb__recurring_charge__c, 
                           cspmb__one_off_cost__c, cspmb__recurring_cost__c,Product__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c, Region__c, SLA_Method__c, CPE_SLA__c, Category__c, Category_lookup__r.Name, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, 
                           Number_of_LAN__c, Number_of_LANWAN__c, Number_of_WAN__c, Wireless_compatible__c, Hardware_Toeslag__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c, WAN_LANWAN_M_LAN__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, 
                           One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c, Extra_Product__c, ProductConfigNameText__c, Deal_Type_Text__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, 
                           Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c, Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c, cspmb__Recurring_Charge_Code__c,Max_Quantity__c,Min_Quantity__c,Promo__c
                             FROM cspmb__Price_Item__c
                             WHERE Id IN :priceItems.keySet() AND Id NOT IN :priceItemsToRemove
                             ORDER BY Available_bandwidth_up__c ASC, Available_bandwidth_down__c ASC NULLS LAST
                             LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];                          

return priceItemsFinal;
    
}

  public override String getRequiredAttributes(){ 
  
      return '["Vendor", "Access Type", "Result Check", "Region", "IPVPN", "Managed Internet", "OneFixed", "Scenario OneFixed", "Min. Upstream Bandwidth Internet", "Min.Downstream Bandwidth Internet",'+ 
      '"Min Bandwidth OneFixed", "Available bandwidth up", "Available bandwidth down", "Proposition", "Proposition Internet", "Proposition OneFixed", "Wireless backup", "SIP Channels", "Codec", "Technology type",'+
      '"Contract Duration", "CPE SLA","Proposition OneNet","OneNet","Scenario OneNet", "Promo", "Bandwidth Up Premium"]';
      
  } 

}