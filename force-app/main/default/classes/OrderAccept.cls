public with sharing class OrderAccept {
	
	public List<Order__c> selectedOrders {get;set;}

	public OrderAccept(ApexPages.StandardSetController controller) {
		controller.addFields(new LIST<String> {'Status__c'});
		selectedOrders = (List<Order__c>)controller.getSelected();
	}

	public pageReference acceptOrders(){
		return acceptOrders(selectedOrders);
	}

	public static pageReference acceptOrders(List<Order__c> orders){

		String error = acceptOrdersError(orders);
		if(error == ''){

			pageReference pr = new PageReference('/'+Order__c.SObjectType.getDescribe().getKeyPrefix());
			pr.setRedirect(true);
			return pr;
		} else {
			ApexPages.AddMessage(New ApexPages.Message(ApexPages.Severity.ERROR,error));
			return null;
		}
	}

	public static String acceptOrdersError(List<Order__c> orders){
		String error = '';
		for(Order__c order : orders){
			if(order.Status__c == 'Assigned to Inside Sales'){
				order.Status__c = 'Accepted by Inside Sales';
				order.Inside_Sales_Owner__c = System.UserInfo.getUserId();
			} else {
				error = 'Only orders with status \'Assigned to Inside Sales\' can be accepted.';
				return error;
			}
		}
		try{
			SharingUtils.updateRecordsWithoutSharing(orders);
			return error;
		} catch (dmlException de){

			system.debug(de);
			error = de.getMessage();
			//ApexPages.AddMessages(de);
			return error;
		}		
	}


	public pageReference claimOrders(){
		return claimOrders(selectedOrders);
	}
	
	public static pageReference claimOrders(List<Order__c> orders){
		for(Order__c order : orders){
			order.Inside_Sales_Owner__c = System.UserInfo.getUserId();
		}
		try{
			SharingUtils.updateRecordsWithoutSharing(orders);
		} catch (dmlException de){
			ApexPages.AddMessages(de);
			return null;
		}

		pageReference pr = new PageReference('/'+Order__c.SObjectType.getDescribe().getKeyPrefix());
		pr.setRedirect(true);
		return pr;
	}	

	public pageReference backToList(){
		pageReference pr = new PageReference('/'+Order__c.SObjectType.getDescribe().getKeyPrefix());
		pr.setRedirect(true);
		return pr;
	}
}