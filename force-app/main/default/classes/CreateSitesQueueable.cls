public with sharing class CreateSitesQueueable implements Queueable, Database.AllowsCallouts {
	@testVisible
	private List<Map<String, String>> siteData;
	@testVisible
	private String accId;
	@testVisible
	private String basketId;
	@testVisible
	public List<Site__c> sites;
	@testVisible
	private static List<Map<String, String>> errors = new List<Map<String, String>>();

	public CreateSitesQueueable(List<Map<String, String>> siteData, String accId, String basketId) {
		this.siteData = siteData;
		this.accId = accId;
		this.basketId = basketId;
	}

	public void execute(QueueableContext context) {
		sites = createSites(siteData, this.accId);

		Sites_Created__e sitesCreated = new Sites_Created__e(
			Related_To_Id__c = this.basketId,
			Errors__c = String.valueOf(errors)
		);
		// Call method to publish events
		Database.SaveResult results = EventBus.publish(sitesCreated);

		// Inspect publishing result for each event
		if (results.isSuccess()) {
			System.debug(LoggingLevel.INFO, 'Successfully published event.');
		} else {
			for (Database.Error err : results.getErrors()) {
				System.debug(
					LoggingLevel.ERROR,
					'Error returned: ' +
					err.getStatusCode() +
					' - ' +
					err.getMessage()
				);
			}
		}
	}

	// Persist the newly created sites
	@testVisible
	private static List<Site__c> createSites(List<Map<String, String>> siteData, Id accId) {
		Account acc = [SELECT Id, KVK_number__c FROM Account WHERE Id = :accId];
		List<Site__c> sitesForInsertion = new List<Site__c>();

		for (Map<String, String> sd : siteData) {
			Site__c newSite = createSiteViaOlbico(acc.Id, acc.KVK_number__c, sd);
			if (newSite.KVK_Number__c != null) {
				sitesForInsertion.add(newSite);
			} else {
				errors.add(sd);
			}
		}
		if (!sitesForInsertion.isEmpty()) {
			insert sitesForInsertion;
		}
		return sitesForInsertion;
	}

	// Site creation via Olbico
	@testVisible
	private static Site__c createSiteViaOlbico(
		Id accId,
		String kvkNumber,
		Map<String, String> siteData
	) {
		// Olbico accepts the zip string as a combination of post code and house number
		String zip = siteData.get(Constants.POSTCODE) + ' ' + siteData.get(Constants.HOUSENUMBER);
		Site__c newSite = new Site__c();

		OlbicoServiceJSON jService = new OlbicoServiceJSON();
		SelectOption sOpt;
		if (Test.isRunningTest()) {
			String val = 'Galjootstraat | 14 |  |  | 1086VE | Amsterdam", label="Galjootstraat | 14 |  |  | 1086VE | Amsterdam';
			String lbl = 'Galjootstraat | 14 |  |  | 1086VE | Amsterdam", label="Galjootstraat | 14 |  |  | 1086VE | Amsterdam';
			sOpt = new SelectOption(val, lbl);
		} else {
			jService.setRequestRestJsonStreet(zip);
			jService.makeRequestRestJsonStreet(zip);
		}

		List<SelectOption> addressOptions = Test.isRunningTest()
			? new List<SelectOption>{ sOpt }
			: jService.AdresInfo();

		for (SelectOption so : addressOptions) {
			// Get the values out of the resulting string
			List<String> address = so.getValue().split('\\|');
			String street = address[0];
			Integer houseNr = Integer.valueof(address[1].Trim());
			String houseNrLetter = address[2];
			String houseNrLetterAddition = address[3];
			String postcode = address[4];
			String cityAddition = address.size() > 6 ? ' - ' + address[6] : '';
			String city = address[5] + cityAddition;
			String houseNrSuffix = houseNrLetter + ' ' + houseNrLetterAddition;
			// If there is no house number sufix than we match the first result which should be the only result
			// If there is a house number sufix we try to match it with the returned result
			if (
				String.isBlank(siteData.get(Constants.HOUSENRSUFFIX)) ||
				siteData.get(Constants.HOUSENRSUFFIX).trim().equalsIgnoreCase(houseNrSuffix.trim())
			) {
				newSite.Site_Account__c = accId;
				newSite.KVK_Number__c = kvkNumber;
				newSite.Site_Street__c = street;
				newSite.Site_House_Number__c = houseNr;
				newSite.Site_House_Number_Suffix__c = houseNrLetter + ' ' + houseNrLetterAddition;
				newSite.Site_Postal_Code__c = postcode;
				newSite.Site_City__c = city;
				newSite.Olbico__c = true;
				newSite.Name = street;
			}
		}

		return newSite;
	}
}