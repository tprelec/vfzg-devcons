/**
 * @description       : Batch Apex Class to process Unica Campiagn Records and create Leads
 * @author            : unknown (SP)
 * @last modified on  : 06-01-2022
 * @last modified by  : Saurabh
 **/
global class UnicaCampaignBatch implements Database.Batchable<sObject>, Database.Stateful {
	global final Map<String, String> fieldMappingCampaign = SyncUtil.fullMapping('Unica_Campaign__c -> Campaign')
		.get('Unica_Campaign__c -> Campaign');

	global final Map<String, String> fieldMappingCampaignMember = SyncUtil.fullMapping('Unica_Campaign__c -> CampaignMember')
		.get('Unica_Campaign__c -> CampaignMember');

	global final Map<String, String> fieldMappingLead = SyncUtil.fullMapping('Unica_Campaign__c -> Lead').get('Unica_Campaign__c -> Lead');

	global final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	global final Schema.SObjectType leadSchema = schemaMap.get('Lead');
	global final Schema.SObjectType campaignSchema = schemaMap.get('Campaign');

	global final Map<String, Schema.SObjectField> leadFieldMap = leadSchema.getDescribe().fields.getMap();

	global final Map<String, Schema.SObjectField> campaignFieldMap = campaignSchema.getDescribe().fields.getMap();

	global Map<String, Id> leadIdsBp = new Map<String, Id>();
	global Map<String, Id> leadIdsItrd = new Map<String, Id>();
	global Map<String, Id> leadIdsZsp = new Map<String, Id>();
	global Map<String, Id> leadIdsBpxuSell = new Map<String, Id>();
	global Map<String, Id> leadIdsBpSaves = new Map<String, Id>();
	global Map<String, Id> leadIdsItrdSaves = new Map<String, Id>();

	global final Schema.SObjectType unicaSchema = schemaMap.get('Unica_Campaign__c');

	global final Map<String, Schema.SObjectField> unicaFieldMap = unicaSchema.getDescribe().fields.getMap();

	// Include the fields that are mapped to lead in the error report
	global final Set<String> queriedFields = SyncUtil.getQueriedFields(fieldMappingLead);
	global final List<String> sortList = SyncUtil.getSortedList(unicaFieldMap);
	global String header = SyncUtil.getHeader(sortList, queriedFields);
	global String dataError = '';

	global Map<String, String> dealerZpOwnerCampaign = new Map<String, String>{ 'ETRECO0020' => 'ETRECO0020' };

	global Database.QueryLocator start(Database.BatchableContext bc) {
		String query = 'SELECT ';

		Set<String> testSet = new Set<String>();
		testSet.addall(fieldMappingCampaign.values());
		testSet.addall(fieldMappingCampaignMember.values());
		testSet.addall(fieldMappingLead.values());

		query += String.join(new List<String>(testSet), ',');

		// Add postcode manually. Needed for allocation but is not mapped to lead
		// Same for campaign channel
		query += ' , Post_Code__c, Campaign_Channel__c FROM Unica_Campaign__c WHERE Processed__c = false';

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext bc, List<Unica_Campaign__c> scope) {
		Map<String, Id> campaignMapParent = new Map<String, Id>();
		Map<String, Id> campaignMapChild = new Map<String, Id>();
		List<Campaign> parentCampaignToInsert = new List<Campaign>();
		List<Campaign> childCampaignToInsert = new List<Campaign>();
		Map<String, Unica_Campaign__c> ucToCreate = new Map<String, Unica_Campaign__c>();
		List<CampaignMember> campaignMemberList = new List<CampaignMember>();
		Map<String, Ban__c> banMap = new Map<String, Ban__c>();
		Map<String, Unica_Campaign__c> cmCreate = new Map<String, Unica_Campaign__c>();
		List<Lead> leadList = new List<Lead>();
		Map<String, Id> leadMap = new Map<String, Id>();
		Set<String> campaignSet = new Set<String>();
		Map<String, Id> campaignMemberMap = new Map<String, Id>();
		Map<Id, Integer> accountMap = new Map<Id, Integer>();
		Map<Id, String> accountMapTemplate = new Map<Id, String>();
		Map<Id, Account> accountFullMapTemplate = new Map<Id, Account>();
		Map<Id, String> banMapTemplate = new Map<Id, String>();
		Map<Id, Id> contactMap = new Map<Id, Id>();
		List<Unica_Campaign__c> ucToBeDeleted = new List<Unica_Campaign__c>();
		List<Unica_Campaign__c> cmList = new List<Unica_Campaign__c>();
		Map<String, Contact> emailMap = new Map<String, Contact>();
		Map<Id, String> profileMap = new Map<Id, String>();

		Id recordType = [SELECT Id FROM RecordType WHERE SobjectType = :'Campaign' AND DeveloperName = :'Unica_Campaign' LIMIT 1].Id;

		//Data preparation
		Map<String, Unica_Campaign__c> scopeMap = new Map<String, Unica_Campaign__c>();

		for (Unica_Campaign__c uc : scope) {
			uc.Processed__c = true;

			if (uc.NTA_Id__c == null) {
				uc.Error__c = 'The NTA ID is not supplied';
				ucToBeDeleted.add(uc);
			} else if (uc.Campaign_Name__c == null) {
				uc.Error__c = 'The Campaign_Name__c is not supplied';
				ucToBeDeleted.add(uc);
			} else {
				if (!cmCreate.containsKey(uc.NTA_Id__c)) {
					cmCreate.put(uc.NTA_Id__c, uc);

					if (uc.Selection_Name__c != null) {
						campaignMapParent.put(uc.Campaign_Name__c, null);
					}

					campaignMapChild.put(uc.Campaign_Name__c + ',' + uc.Selection_Name__c, null);
					ucToCreate.put(uc.Campaign_Name__c + ',' + uc.Selection_Name__c, uc);
					banMap.put(uc.Ban__c, null);
					campaignSet.add(uc.Campaign_Name__c);
					emailMap.put(uc.Ban_Contact_Email__c, null);
					scopeMap.put(uc.ID, uc);
				}
			}
		}

		for (Ban__c ban : [
			SELECT
				Id,
				Account__c,
				Name,
				BAN_Name__c,
				Owner.Email,
				OwnerId,
				Owner.IsActive,
				Owner.Profile.Name,
				Dealer_code__c,
				Owner_Dealercode__c,
				Class_4_Name__c
			FROM Ban__c
			WHERE Name IN :banMap.keySet()
		]) {
			banMap.put(ban.Name, ban);
			banMapTemplate.put(ban.Id, ban.OwnerId);
			accountMap.put(ban.Account__c, null);
			profileMap.put(ban.OwnerId, ban.Owner.Profile.Name);
		}

		// If ban dealercode owner is vodafone or dealercode has no contact/user, replace it by the parent partner account contact user
		Map<String, Id> parentDealerCodeToParentOwner = new Map<String, Id>();
		Map<String, Id> dealerCodeToOwner = new Map<String, Id>();
		Map<String, Id> mobileDealerCodeToOwner = new Map<String, Id>();
		Map<String, String> dealerCodeToDealerName = new Map<String, String>();
		Map<String, String> parentDealerCodeToDealerCode = new Map<String, String>();
		Map<String, String> dealerCodeToParentDealerCode = new Map<String, String>();

		System.debug(LoggingLevel.INFO, banMap);

		for (String banId : banMap.keySet()) {
			if (banId != null) {
				System.debug(LoggingLevel.INFO, '***SP*** banId: ' + banId);

				if (banMap.get(banId) != null) {
					System.debug(
						LoggingLevel.INFO,
						'***SP*** GeneralUtils.stripDealerCode(banMap.get(banId).Dealer_Code__c): ' +
						GeneralUtils.stripDealerCode(banMap.get(banId).Dealer_Code__c)
					);

					dealerCodeToOwner.put(GeneralUtils.stripDealerCode(banMap.get(banId).Dealer_Code__c), null);
					mobileDealerCodeToOwner.put(GeneralUtils.stripDealerCode(banMap.get(banId).Owner_Dealercode__c), null);
				}
			}
		}

		System.debug(LoggingLevel.INFO, dealerCodeToOwner);

		// Collect (active) dealer contact users + additional mappings
		for (Dealer_Information__c di : [
			SELECT Id, Name, Dealer_Code__c, Contact__r.UserId__c, Contact__r.UserId__r.IsActive, Contact__r.UserId__r.Email, Parent_Dealer_Code__c
			FROM Dealer_Information__c
			WHERE Dealer_Code__c IN :dealerCodeToOwner.keySet()
		]) {
			if (di.Contact__r.UserId__c != null && di.Contact__r.UserId__r.IsActive) {
				dealerCodeToOwner.put(di.Dealer_Code__c, di.Contact__r.UserId__c);
			}
			dealerCodeToDealerName.put(di.Dealer_Code__c, di.Name);
			if (di.Parent_Dealer_Code__c != null) {
				parentDealerCodeToDealerCode.put(di.Parent_Dealer_Code__c, di.Dealer_Code__c);
				dealerCodeToParentDealerCode.put(di.Dealer_Code__c, di.Parent_Dealer_Code__c);
			}
		}
		// Collect (active) parent contact users
		for (Dealer_Information__c pdi : [
			SELECT Id, Name, Dealer_Code__c, Contact__r.UserId__c
			FROM Dealer_Information__c
			WHERE Contact__c != NULL AND Contact__r.UserId__r.IsActive = TRUE AND Dealer_Code__c IN :parentDealerCodeToDealerCode.keySet()
		]) {
			parentDealerCodeToParentOwner.put(pdi.Dealer_Code__c, pdi.Contact__r.UserId__c);
		}

		// Collect mobile dealer contact users
		System.debug(LoggingLevel.INFO, '***SP*** searching for mobile dealer');

		for (Dealer_Information__c di : [
			SELECT
				Id,
				Name,
				Dealer_Code__c,
				Contact__r.UserId__c,
				Contact__r.UserId__r.IsActive,
				Contact__r.UserId__r.Email,
				Parent_Dealer_Code__c,
				Contact__r.Account.CVM_Lead_Owner__c
			FROM Dealer_Information__c
			WHERE Dealer_Code__c IN :mobileDealerCodeToOwner.keySet()
		]) {
			if (di.Contact__r.UserId__c != null && di.Contact__r.UserId__r.IsActive) {
				mobileDealerCodeToOwner.put(di.Dealer_Code__c, di.Contact__r.UserId__c);

				System.debug(LoggingLevel.INFO, '***SP*** found mobile dealer');
				System.debug(LoggingLevel.INFO, '***SP*** di ' + di);
				System.debug(LoggingLevel.INFO, '***SP*** di.Contact__r.UserId__c: ' + di.Contact__r.UserId__c);
			}

			dealerCodeToDealerName.put(di.Dealer_Code__c, di.Name);

			if (di.Parent_Dealer_Code__c != null) {
				parentDealerCodeToDealerCode.put(di.Parent_Dealer_Code__c, di.Dealer_Code__c);
				dealerCodeToParentDealerCode.put(di.Dealer_Code__c, di.Parent_Dealer_Code__c);
			}

			// If there is a prefered CVM lead owner, that will be used instead of the dealer contact
			if (di.Contact__r.Account.CVM_Lead_Owner__c != null) {
				mobileDealerCodeToOwner.put(di.Dealer_Code__c, di.Contact__r.Account.CVM_Lead_Owner__c);
			}
		}

		// Fetch channel via owner->contact->account (partner account)
		Map<Id, String> userIdToChannel = new Map<Id, String>();

		for (User u : [SELECT Id, Contact.Account.Channel__c FROM User WHERE Id IN :banMapTemplate.values()]) {
			userIdToChannel.put(u.Id, u.Contact.Account.Channel__c);
		}

		for (Id banId : banMapTemplate.keySet()) {
			String channel = userIdToChannel.get(banMapTemplate.get(banId));
			banMapTemplate.put(banId, channel);
		}

		for (Lead lead : [SELECT Id, NTA_Id__c FROM Lead WHERE NTA_Id__c IN :cmCreate.keySet()]) {
			leadMap.put(lead.NTA_Id__c, lead.Id);
		}

		for (CampaignMember cm : [SELECT Id, NTA_Id__c FROM CampaignMember WHERE NTA_Id__c IN :cmCreate.keySet()]) {
			campaignMemberMap.put(cm.NTA_Id__c, cm.Id);
		}

		for (AggregateResult ar : [
			SELECT AccountId, count(Id)
			FROM Contact
			WHERE AccountId IN :accountMap.keySet()
			GROUP BY AccountId
			HAVING COUNT(Id) = 1
		]) {
			contactMap.put((Id) ar.get('AccountId'), null);
		}

		for (Account acc : [
			SELECT Id, Channel__c, ITW_Team_Member__c, ITW_Indicator__c, ITW_Account_Manager_Overwrite__c
			FROM Account
			WHERE Id IN :accountMap.keySet()
		]) {
			accountMapTemplate.put(acc.Id, acc.Channel__c);
			accountFullMapTemplate.put(acc.Id, acc);
		}

		for (Contact c : [SELECT Id, AccountId FROM Contact WHERE AccountId IN :contactMap.keySet()]) {
			contactMap.put(c.AccountId, c.Id);
		}

		for (Contact c : [SELECT Id, AccountId, Email FROM Contact WHERE Email IN :emailMap.keySet() AND Email != NULL]) {
			emailMap.put(c.Email, c);
		}

		// Parent campaign creation
		for (Campaign c : [SELECT Id, Selection_Name__c, Name FROM Campaign WHERE Name IN :campaignSet]) {
			if (c.Selection_Name__c != null) {
				campaignMapChild.put(c.Name + ',' + c.Selection_Name__c, c.Id);
			} else if (campaignMapParent.containsKey(c.Name)) {
				campaignMapParent.put(c.Name, c.Id);
			} else {
				campaignMapChild.put(c.Name + ',' + c.Selection_Name__c, c.Id);
			}
		}

		for (String s : campaignMapParent.keySet()) {
			if (campaignMapParent.get(s) == null) {
				Campaign c = new Campaign(Name = s, RecordTypeId = recordType);
				parentCampaignToInsert.add(c);
			}
		}

		insert parentCampaignToInsert;

		for (Campaign c : parentCampaignToInsert) {
			campaignMapParent.put(c.Name, c.Id);
		}

		// Child campaign creation
		for (String s : campaignMapChild.keySet()) {
			if (campaignMapChild.get(s) == null) {
				Unica_Campaign__c uc = ucToCreate.get(s);
				Campaign c = new Campaign();

				for (String campaignField : fieldMappingCampaign.keySet()) {
					String unicaField = fieldMappingCampaign.get(campaignField);
					Object unicaValue;
					if (campaignFieldMap.get(campaignField).getDescribe().getType().name() == 'DATE' && uc.get(unicaField) != null) {
						unicaValue = Date.valueOf(String.valueOf(uc.get(unicaField)));
					} else {
						unicaValue = uc.get(unicaField);
					}

					c.put(campaignField, unicaValue);
				}

				c.RecordTypeId = recordType;

				if (s.substringAfter(',') != null) {
					c.ParentId = campaignMapParent.get(s.substringBefore(','));
				}

				childCampaignToInsert.add(c);
			}
		}

		insert childCampaignToInsert;

		for (Campaign c : childCampaignToInsert) {
			campaignMapChild.put(c.Name + ',' + c.Selection_Name__c, c.Id);
		}

		// Lead creation
		Id rtId = [SELECT Id FROM RecordType WHERE DeveloperName = :'Leads_CVM'].Id;

		for (Unica_Campaign__c uc : cmCreate.values()) {
			if (leadMap.get(uc.NTA_Id__c) == null) {
				System.debug(LoggingLevel.INFO, '***SP*** found NTA');

				if (banMap.get(uc.Ban__c) != null) {
					System.debug(LoggingLevel.INFO, '***SP*** found BAN');

					Lead lead = new Lead();

					for (String leadField : fieldMappingLead.keySet()) {
						String ucField = fieldMappingLead.get(leadField);
						Object ucValue;

						if (leadFieldMap.get(leadField).getDescribe().getType().name() == 'BOOLEAN') {
							if (uc.get(ucField) == 'Y') {
								ucValue = Boolean.valueOf('true');
							} else {
								ucValue = Boolean.valueOf('false');
							}
						} else if (leadFieldMap.get(leadField).getDescribe().getType().name() == 'DATE') {
							if (uc.get(ucField) != null && String.valueOf(uc.get(ucField)) != null) {
								ucValue = Date.valueOf(String.valueOf(uc.get(ucField)));
							}
						} else {
							ucValue = uc.get(ucField);
						}

						lead.put(leadField, ucValue);
					}

					lead.Ban__c = banMap.get(uc.Ban__c).Id;

					String companyName = banMap.get(uc.Ban__c).BAN_Name__c;

					if (companyName == null || companyName == '') {
						companyName = 'No Ban Name';
					}

					lead.Company = companyName;
					lead.Account_name__c = banMap.get(uc.Ban__c).Account__c;
					lead.Email = banMap.get(uc.Ban__c).Owner.Email;

					System.debug(LoggingLevel.INFO, uc.Ban__c);
					System.debug(LoggingLevel.INFO, banMap);
					System.debug(LoggingLevel.INFO, banMap.get(uc.Ban__c));

					String dealerCode = GeneralUtils.stripDealerCode(banMap.get(uc.Ban__c).Dealer_Code__c);
					String mobileDealerCode = GeneralUtils.stripDealerCode(banMap.get(uc.Ban__c).Owner_Dealercode__c);

					System.debug(LoggingLevel.INFO, dealerCode);

					Boolean ownerFound = false;

					// First try to assign it to owner dealer code, then if it fails, go to standard process
					System.debug(LoggingLevel.INFO, '***SP*** mobileDealerCodeToOwner: ' + mobileDealerCodeToOwner);
					System.debug(LoggingLevel.INFO, '***SP*** dealerCode: ' + dealerCode);
					System.debug(LoggingLevel.INFO, '***SP*** mobileDealerCodeToOwner.get(dealerCode): ' + mobileDealerCodeToOwner.get(dealerCode));

					Boolean isDealerCampaign = false;

					for (String dealerCamString : dealerZpOwnerCampaign.keySet()) {
						if (!String.isEmpty(uc.Campaign_Name__c)) {
							if (uc.Campaign_Name__c.contains(dealerCamString)) {
								isDealerCampaign = true;
							}
						}
					}
					//SFLO : 330, Trying to Default Zip Code assignment when Zip code is available //isDealerCampaign ||
					if (uc.Post_Code__c != null) {
						System.debug(LoggingLevel.INFO, GeneralUtils.postalCodeToZSPMap);

						if (GeneralUtils.postalCodeToZSPMap.containsKey(uc.Post_Code__c.subString(0, 4))) {
							// Only bans with Class_4_Name like ‘VFW’, ‘Telesales’ or ‘E-Sales’ can be assigned to a ZSP based on their postal code when not having a contact on dealer or parent dealer level.
							Ban__c myBan = banMap.get(uc.Ban__c);
							if (myBan != null) {
								// Moved these lines here so it does not break in test classes, where there is no post code, it would also break if post code is empty
								System.debug(LoggingLevel.INFO, uc.Post_Code__c);
								System.debug(LoggingLevel.INFO, uc.Post_Code__c.subString(0, 4));
								lead.lastName += '  ' + companyName;
								lead.OwnerId = GeneralUtils.postalCodeToZSPMap.get(uc.Post_Code__c.subString(0, 4));
								ownerFound = true; //*/
								System.debug(LoggingLevel.INFO, '***SP*** 0' + 'Lead Name modified' + lead);
							}
						}
					} else {
						// assign the Leads to Default Queue provided by Business
						lead.OwnerId = [SELECT Id FROM Group WHERE DeveloperName = :Label.Unica_Batch_Lead_Queue AND Type = 'Queue'].Id;
						ownerFound = true;
						System.debug(LoggingLevel.INFO, '***SP*** 1a');
					}
					/* Commenting Out Below Code as per current requirement, but not removing in case of Rollback needed or business changes their mind
                    else if (
                    mobileDealerCodeToOwner.containsKey(mobileDealerCode) &&
                    mobileDealerCodeToOwner.get(mobileDealerCode) != null
                    ) {
                    // Base owner on ban mobile dealercode owner
                    lead.OwnerId = mobileDealerCodeToOwner.get(mobileDealerCode); //owner is dealer code contact
                    ownerFound = true;
                    
                    System.debug(LoggingLevel.INFO, '***SP*** 1a');
                    } else if (
                    dealerCodeToOwner.containsKey(dealerCode) &&
                    dealerCodeToOwner.get(dealerCode) != null
                    ) {
                    lead.OwnerId = dealerCodeToOwner.get(dealerCode); //owner is dealer code contact
                    ownerFound = true;
                    
                    System.debug(LoggingLevel.INFO, '***SP*** 1b');
                    } else {
                    // If no ban dealercode owner found, or it is inactive, see if dealer has a parent deal with a contact-user
                    if (dealerCodeToParentDealerCode.containsKey(dealerCode)) {
                    if (
                    parentDealerCodeToParentOwner.containsKey(
                    dealerCodeToParentDealerCode.get(dealerCode)
                    )
                    ) {
                    lead.OwnerId = parentDealerCodeToParentOwner.get(
                    dealerCodeToParentDealerCode.get(dealerCode)
                    );
                    ownerFound = true;
                    
                    System.debug(LoggingLevel.INFO, '***SP*** 2');
                    }
                    }
                    }  */

					System.debug(LoggingLevel.INFO, ownerFound);

					if (!ownerFound) {
						System.debug(LoggingLevel.INFO, GeneralUtils.postalCodeToZSPMap);

						if (uc.Post_Code__c != null && GeneralUtils.postalCodeToZSPMap.containsKey(uc.Post_Code__c.subString(0, 4))) {
							// Only bans with Class_4_Name like ‘VFW’, ‘Telesales’ or ‘E-Sales’ can be assigned to a ZSP based on their postal code when not having a contact on dealer or parent dealer level.
							Ban__c myBan = banMap.get(uc.Ban__c);

							if (
								myBan != null &&
								(myBan.Class_4_Name__c == 'VFW' ||
								myBan.Class_4_Name__c == 'Telesales' ||
								myBan.Class_4_Name__c == 'E-Sales')
							) {
								// Moved these lines here so it does not break in test classes, where there is no post code, it would also break if post code is empty
								System.debug(LoggingLevel.INFO, uc.Post_Code__c);
								System.debug(LoggingLevel.INFO, uc.Post_Code__c.subString(0, 4));

								lead.OwnerId = GeneralUtils.postalCodeToZSPMap.get(uc.Post_Code__c.subString(0, 4));
								ownerFound = true;

								System.debug(LoggingLevel.INFO, '***SP*** 3');
							}
						}
					}

					// If there is an ITW lead owner on account level, take that and assign it to him
					Account acc = accountFullMapTemplate.get(banMap.get(uc.Ban__c).Account__c);

					// If it's ITW campaign and if the account is ITW flagged, assign it to the ITW team member
					if (uc.Campaign_Channel__c == 'ITW') {
						if (acc.ITW_Indicator__c == true && acc.ITW_Account_Manager_Overwrite__c == false && acc.ITW_Team_Member__c != null) {
							lead.OwnerId = acc.ITW_Team_Member__c;
						}
					}

					if (!ownerFound) {
						uc.Error__c = 'No active SFDC user (ban dealercode contactuser or parent dealer contactuser) found to assign Lead ownership';
						ucToBeDeleted.add(uc);
						continue;
					}

					System.debug(LoggingLevel.INFO, lead.OwnerId);

					lead.LeadSource = 'CVM Campaign';
					lead.RecordTypeId = rtId;
					lead.Dealer_Code__c = dealerCode;

					// Also fill Dealer_Name__c field so that distributor can route to the right dealer
					if (dealerCodeToDealerName.containsKey(dealerCode)) {
						lead.Dealer_Name__c = dealerCodeToDealerName.get(dealerCode);
					}

					lead.Status = 'No follow up yet';
					lead.CVM_Participation__c = true;

					if (uc.Selection_Name__c != null) {
						lead.Campaign__c = campaignMapChild.get(uc.Campaign_Name__c + ',' + uc.Selection_Name__c);
					} else if (campaignMapParent.containsKey(uc.Campaign_Name__c)) {
						lead.Campaign__c = campaignMapParent.get(uc.Campaign_Name__c);
					} else {
						lead.Campaign__c = campaignMapChild.get(uc.Campaign_Name__c + ',' + uc.Selection_Name__c);
					}

					System.debug(LoggingLevel.INFO, lead.Campaign__c);

					if (emailMap.containsKey(lead.Contact_Email__c) && emailMap.get(lead.Contact_Email__c) != null) {
						Contact c = emailMap.get(lead.Contact_Email__c);

						if (c.AccountId == lead.Account_name__c) {
							lead.Contact__c = c.Id;
						}
					}

					if (contactMap.containsKey(lead.Account_name__c) && lead.Contact__c == null) {
						lead.Contact__c = contactMap.get(lead.Account_name__c);
					}

					leadList.add(lead);
					cmList.add(uc);
				} else {
					System.debug(LoggingLevel.INFO, '***SP*** not found ban');

					uc.Error__c = 'The BAN doesn\'t exist';
					ucToBeDeleted.add(uc);
				}
			} else {
				System.debug(LoggingLevel.INFO, '***SP*** not found NTA');
			}
		}

		System.debug(LoggingLevel.INFO, ucToBeDeleted);

		Database.insert(leadList, false);

		// Fill maps for initial mail
		Set<Id> leadIds = new Set<Id>();

		for (Lead l : leadList) {
			leadIds.add(l.Id);
		}

		// Re-query to get the related field from campaign
		List<Lead> allLeads = [
			SELECT Id, Lead.Campaign__c, Lead.Campaign__r.Type, BAN__c, NTA_Id__c, OwnerId, Email, Owner.Email
			FROM Lead
			WHERE Id IN :leadIds
		];

		for (Lead lead : allLeads) {
			// Set the email, now owner should be populated
			lead.Email = lead.Owner.Email; // Email should be the lead owner, and not the BAN owner
			leadMap.put(lead.NTA_Id__c, lead.Id);

			if (lead.Campaign__r.Type != null) {
				if (lead.Campaign__r.Type.toLowerCase() == 'recommit') {
					if (banMapTemplate.get(lead.BAN__c) == 'BP') {
						if (!leadIdsBp.containsKey(lead.OwnerId + ',' + lead.Campaign__c)) {
							leadIdsBp.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
						}
					} else if (banMapTemplate.get(lead.Ban__c) == 'Distri') {
						if (!leadIdsItrd.containsKey(lead.OwnerId + ',' + lead.Campaign__c)) {
							leadIdsItrd.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
						}
					} else if (profileMap.get(lead.OwnerId) == 'VF Retail ZS') {
						if (!leadIdsZsp.containsKey(lead.OwnerId + ',' + lead.Campaign__c)) {
							leadIdsZsp.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
						}
					}
				} else if (lead.Campaign__r.Type.toLowerCase() == 'saves') {
					if (banMapTemplate.get(lead.Ban__c) == 'BP') {
						if (!leadIdsBpSaves.containsKey(lead.OwnerId + ',' + lead.Campaign__c)) {
							leadIdsBpSaves.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
						}
					} else if (banMapTemplate.get(lead.Ban__c) == 'Distri') {
						if (!leadIdsItrdSaves.containsKey(lead.OwnerId + ',' + lead.Campaign__c)) {
							leadIdsItrdSaves.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
						}
					}
				} else if (lead.Campaign__r.Type.toLowerCase() == 'xusell') {
					if (banMapTemplate.get(lead.Ban__c) == 'BP') {
						if (!leadIdsBpxuSell.containsKey(lead.OwnerId + ',' + lead.Campaign__c)) {
							leadIdsBpxuSell.put(lead.OwnerId + ',' + lead.Campaign__c, lead.Id);
						}
					}
				}
			}
		}

		update allLeads;

		// Campaign Member creation
		for (Unica_Campaign__c uc : cmCreate.values()) {
			if (campaignMemberMap.get(uc.NTA_Id__c) == null && uc.Error__c == null) {
				if (banMap.get(uc.Ban__c) != null && leadMap.get(uc.NTA_Id__c) != null) {
					CampaignMember cm = new CampaignMember();

					for (String campaignField : fieldMappingCampaignMember.keySet()) {
						String ucField = fieldMappingCampaignMember.get(campaignField);
						Object ucValue = uc.get(ucField);
						cm.put(campaignField, ucValue);
					}

					if (uc.Selection_Name__c != null) {
						cm.CampaignId = campaignMapChild.get(uc.Campaign_Name__c + ',' + uc.Selection_Name__c);
					} else if (campaignMapParent.containsKey(uc.Campaign_Name__c)) {
						cm.CampaignId = campaignMapParent.get(uc.Campaign_Name__c);
					} else {
						cm.CampaignId = campaignMapChild.get(uc.Campaign_Name__c + ',' + uc.Selection_Name__c);
					}

					cm.LeadId = leadMap.get(uc.NTA_Id__c);
					cm.Ban_Number__c = banMap.get(uc.Ban__c).Id;
					campaignMemberList.add(cm);
				}
			}
		}

		List<Database.SaveResult> ur = Database.insert(campaignMemberList, false);

		for (Integer i = 0; i < ur.Size(); i++) {
			Unica_Campaign__c loadRecord = scopeMap.get(String.valueof(campaignMemberList[i].UnicaCampaignID__c));

			if (!ur[i].isSuccess()) {
				loadRecord.Error__c = String.valueof(ur[i].getErrors()).left(255);
				ucToBeDeleted.add(loadRecord);
			}
		}

		for (Unica_Campaign__c uc : ucToBeDeleted) {
			for (String field : sortList) {
				if (queriedFields.contains(field)) {
					dataError += '"' + uc.get(field) + '",';
				}
			}
			dataError += '"' + uc.Error__c + '"\n';
		}

		// Update uploaded info
		for (Unica_Campaign__c uc : scope) {
			uc.Lead__c = leadMap.get(uc.NTA_Id__c);
		}

		update scope;

		//delete ucToBeDeleted;
	}

	global void finish(Database.BatchableContext bc) {
		OrgWideEmailAddress orgWideEmailAddress = EmailService.getDefaultOrgWideEmailAddress();

		AsyncApexJob a = [SELECT Status, NumberOfErrors, JobItemsProcessed, ExtendedStatus FROM AsyncApexJob WHERE Id = :bc.getJobId()];

		// Send emails to tell the campaign is ready
		sendCampaignReadyEmail(leadIdsBp, 'Lead_CVM_BP_Recommit_Initial');
		sendCampaignReadyEmail(leadIdsItrd, 'Lead_CVM_ITR_D_Recommit_Initial');
		sendCampaignReadyEmail(leadIdsZsp, 'Lead_CVM_ZSP_Recommit_Initial');
		sendCampaignReadyEmail(leadIdsBpxuSell, 'Lead_CVM_BP_XUSell_Initial');
		sendCampaignReadyEmail(leadIdsBpSaves, 'Lead_CVM_BP_Saves_Initial');
		sendCampaignReadyEmail(leadIdsItrdSaves, 'Lead_CVM_ITR_D_Saves_Initial');

		// Send an email to the user that processed the batch, notifying of job completion.
		// SPavuna: added custom settings for subject, text, and additional email addresses
		UnicaCampaignSettings__c emailSetting = UnicaCampaignSettings__c.getValues('Main');
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		header = header.subString(0, header.length() - 1) + ',"Error__c"\n';
		Blob b = blob.valueOf(header + dataError);

		Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
		efa.setFileName('UnicaCampaignErrors.csv');
		efa.setBody(b);

		String[] toAddresses = new List<String>{ UserInfo.getUserEmail() };
		String emailSubject = '';
		String emailStatusText = '';

		if (emailSetting != null) {
			if (emailSetting.StatusEmailRecepient1__c != null) {
				toAddresses.add(emailSetting.StatusEmailRecepient1__c);
			}
			if (emailSetting.StatusEmailRecepient2__c != null) {
				toAddresses.add(emailSetting.StatusEmailRecepient2__c);
			}
			if (emailSetting.StatusEmailRecepient3__c != null) {
				toAddresses.add(emailSetting.StatusEmailRecepient3__c);
			}
			if (emailSetting.StatusEmailRecepient4__c != null) {
				toAddresses.add(emailSetting.StatusEmailRecepient4__c);
			}
			if (emailSetting.StatusEmailRecepient5__c != null) {
				toAddresses.add(emailSetting.StatusEmailRecepient5__c);
			}
			if (emailSetting.StatusEmailSubject__c != null) {
				emailSubject = String.format(emailSetting.StatusEmailSubject__c, new List<String>{ a.Status });
			}
			if (emailSetting.StatusEmailText__c != null) {
				emailStatusText = String.format(
					emailSetting.StatusEmailText__c,
					new List<String>{ String.ValueOf(a.JobItemsProcessed), String.ValueOf(a.NumberOfErrors) }
				);
			}
			if (a.NumberOfErrors > 0) {
				if (emailSetting.ErrorTextEmail__c != null) {
					emailStatusText = emailStatusText + String.format(emailSetting.ErrorTextEmail__c, new List<String>{ a.ExtendedStatus });
				}
			}
		} else {
			// Default values if it is not configured in custom settings
			emailSubject = 'Unica Campaign database import ' + a.Status;
			emailStatusText =
				'The Unica Update database import job processed ' +
				a.JobItemsProcessed +
				' records with ' +
				a.NumberOfErrors +
				' failures.';

			if (a.NumberOfErrors > 0) {
				emailStatusText = emailStatusText + ' The following text might help with identifying possible errors: ' + a.ExtendedStatus;
			}
		}

		mail.setToAddresses(toAddresses);
		mail.setSubject(emailSubject);
		mail.setPlainTextBody(emailStatusText);
		mail.setFileAttachments(new List<Messaging.EmailFileAttachment>{ efa });

		if (orgWideEmailAddress != null) {
			mail.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
		}

		if (EmailService.checkDeliverability()) {
			Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ mail });
		}
	}
	@TestVisible
	private static void sendCampaignReadyEmail(Map<String, Id> leadIdsMap, String templateName) {
		if (leadIdsMap.values().size() > 0) {
			Id recommitTemplateId = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :templateName].Id;

			Messaging.MassEmailMessage mails = new Messaging.MassEmailMessage();
			mails.setTargetObjectIds(leadIdsMap.values());
			mails.setTemplateId(recommitTemplateId);

			if (templateName == 'Lead_CVM_BP_Recommit_Initial') {
				mails.setReplyTo('noreply@vodafone.com');
			}

			if (EmailService.checkDeliverability()) {
				Messaging.sendEmail(new List<Messaging.MassEmailMessage>{ mails });
			}
		}
	}
}
