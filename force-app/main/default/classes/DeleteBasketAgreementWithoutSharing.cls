public without sharing class DeleteBasketAgreementWithoutSharing {
	public static Boolean deleteBasketAgreement(Id basketId) {
		List<csclm__Agreement__c> basketAgreements = [
			SELECT Id, Product_Basket__c
			FROM csclm__Agreement__c
			WHERE Product_Basket__c = :basketId
		];

		if (!basketAgreements.isEmpty()) {
			try {
				delete basketAgreements;
				return true;
			} catch (exception e) {
				return false;
			}
		}
		return false;
	}
}