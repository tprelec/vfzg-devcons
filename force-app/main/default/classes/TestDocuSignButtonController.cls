@isTest
private class TestDocuSignButtonController {
	@isTest
	static void prepareOpportunityAndGetUrlBase() {
		TestUtils.createCSProductBasket();

		Test.startTest();
		User accountManagerManager = TestUtils.createAccountManager();
		Opportunity opp = [SELECT Id, AccountId, Amount, Primary_Basket__c, OwnerId FROM Opportunity LIMIT 1];
		opp.Amount = 500001;
		User accountManager = new User(Id = opp.OwnerId, ManagerId = accountManagerManager.Id);
		update accountManager;
		opp.Primary_Basket__c = [SELECT Id, Sign_on_behalf_of_customer__c FROM cscfga__Product_Basket__c].Id;
		update new cscfga__Product_Basket__c(Id = opp.Primary_Basket__c, Sign_on_behalf_of_customer__c = 'both');
		update opp;
		Account acc = [SELECT Id FROM Account WHERE Id = :opp.AccountId];
		List<Contact> signers = getSignersAndPutOnAccount(acc);
		csclm__Agreement__c agreement = CS_DataTest.createAgreement('Test Agreement');
		agreement.csclm__Opportunity__c = opp.Id;
		agreement.recordTypeId = Schema.SObjectType.csclm__Agreement__c.getRecordTypeInfosByDeveloperName().get('Bespoke').getRecordTypeId();
		insert agreement;
		Attachment attachment = new Attachment(
			Name = 'I\'m testing attachments',
			Body = Blob.valueOf('Some body once told me the world is gonna roll me'),
			ParentId = agreement.Id
		);
		insert attachment;

		GenericMock mock = new GenericMock();
		mock.returns.put('attachDocumentsToSObjectForRegularAttachments', null);
		mock.returns.put('attachDocumentsToSObjectForContractConditions', null);
		DocumentService.instance = (DocumentService) Test.createStub(DocumentService.class, mock);

		// The DocuSignButtonController is dependent on a process builder on the Opportunity to
		// fill values on the Opportunity from the Opportunity.Owner.ManagerId, which is tested here.
		opp = [SELECT DS_Manager_Email__c, DS_Manager_Name__c FROM Opportunity WHERE Id = :opp.Id];
		System.assert(
			opp.DS_Manager_Name__c.contains(accountManagerManager.LastName),
			'The Manager name does not correspond with the Account Manager.'
		);
		System.assertEquals(
			accountManagerManager.Email,
			opp.DS_Manager_Email__c,
			'A process builder on the Opportunity should fill the internal signer on the Opportunity ' + 'From the Opportunity.Owner.ManagerId'
		);

		String result = DocuSignButtonController.prepareOpportunityAndCreateEnvelopeURL(opp.Id, false);
		opp = DocuSignButtonController.fetchOpportunity(opp.Id);
		signers = [SELECT Name, Email FROM Contact WHERE Id IN :signers ORDER BY Email];
		// The following assertions test the flow 'Docusign_Signer_Update_Autolaunched' that populates these values
		// on the Opp from the Account.
		System.assertEquals(
			signers[0].Name,
			opp.DS_Authorized_to_sign_1st_Name__c,
			'Contact Name does not correspond with the authorized to sign Name.'
		);
		System.assertEquals(
			signers[0].Email,
			opp.DS_Authorized_to_sign_1st_Mail__c,
			'Contact Email does not correspond with the authorized to sing Email.'
		);
		System.assertEquals(
			signers[1].Name,
			opp.DS_Authorized_to_sign_2nd_Name__c,
			'Contact Name does not correspond with the authorized to sign Name.'
		);
		System.assertEquals(
			signers[1].Email,
			opp.DS_Authorized_to_sign_2nd_Mail__c,
			'Contact Email does not correspond with the authorized to sing Email.'
		);

		System.assertEquals(
			opp.Id,
			mock.args.get('attachDocumentsToSObjectForRegularAttachments')[0],
			'The Id of the Opportunityy does not correspond to the response of the call.'
		);
		List<CS_DocItem> docsInMethodCall = (List<CS_DocItem>) mock.args.get('attachDocumentsToSObjectForRegularAttachments')[1];
		System.assertEquals(attachment.Id, docsInMethodCall[0].docId, 'The Id of the Attachment does not correspond to the Id of the response.');
		System.assertEquals(
			1,
			mock.callCount.get('attachDocumentsToSObjectForRegularAttachments'),
			'The number of retrieved attachments should be 1.'
		);
		System.assertEquals(
			1,
			mock.callCount.get('attachDocumentsToSObjectForContractConditions'),
			'The number of retrieved attachments should be 1.'
		);
		System.assertEquals(
			Constants.DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED,
			opp.Docusign_Attachment_Status__c,
			'The Document Status of the Opportunity should be ' + Constants.DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED
		);
		System.assert(
			new Pagereference(result).getParameters().get('CRL').contains(signers[0].Email),
			'The URL of the Page Reference does not contain signers Email.'
		);
		Test.stopTest();
	}

	@isTest
	static void prepareOpportunityAndGetUrlLegalCase() {
		Account acc = TestUtils.createAccount(null);
		List<Contact> signers = getSignersAndPutOnAccount(acc);
		Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		opp.Amount = 500001;
		update opp;

		TestUtils.autoCommit = false;
		Contact internalSigner2 = TestUtils.createContact(acc);
		internalSigner2.Email = 'internalsigner2@test.com';
		insert internalSigner2;
		Case sourceCase = new Case(
			AccountId = opp.AccountId,
			Subject = 'Test',
			Description = 'Test',
			Opportunity__c = opp.Id,
			DS_Authorized_Internal_Signer_2__c = internalSigner2.Id,
			DS_Sign_on_behalf_of_customer__c = 'both',
			DS_Authorized_to_sign_1st__c = signers[0].Id,
			DS_Authorized_to_sign_2nd__c = signers[1].Id
		);
		sourceCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('Enterprise_Legal').getRecordTypeId();
		insert sourceCase;
		TestUtils.autoCommit = true;

		// Dependency: A Process Builder on the Case populates the Case Id to the Opportunity
		// if the case is Record Type 'Enterprise_Legal' and it looks up to an Opportunity.
		//assert here: opp fields are populated. if not: custom message: flow <developername> not working
		System.assertEquals(
			sourceCase.Id,
			[SELECT Enterprise_Legal_Case__c FROM Opportunity WHERE Id = :opp.Id]
			.Enterprise_Legal_Case__c,
			'The Case Process builder did not populate the Enterprise_Legal_Case__c on the Opportunity'
		);

		GenericMock mock = new GenericMock();
		mock.returns.put('attachDocumentsToSObjectFromSObject', null);
		mock.returns.put('getCaseAttachmentIdsByCaseId', new Set<Id>{ opp.Id }); // any valid Id will do
		DocumentService.instance = (DocumentService) Test.createStub(DocumentService.class, mock);

		Test.startTest();
		DocuSignButtonController.prepareOpportunityAndCreateEnvelopeURL(opp.Id, false);
		opp = DocuSignButtonController.fetchOpportunity(opp.Id);
		signers = [SELECT Name, Email FROM Contact WHERE Id IN :signers ORDER BY Email];

		// The following assertions test the flow 'Docusign_Signer_Update_Autolaunched' that populates these values
		// on the Opp from the Account for legal cases.
		System.assertEquals(
			internalSigner2.Email,
			Opp.DS_Authorized_Internal_Signer_2_Mail__c,
			'The Opportunity signer Email does not correspond with the actual signer Email.'
		);
		System.assertEquals(
			signers[0].Name,
			opp.DS_Authorized_to_sign_1st_Name__c,
			'The Opportunity signer Name does not correspond with the actual signer Name.'
		);
		System.assertEquals(
			signers[0].Email,
			opp.DS_Authorized_to_sign_1st_Mail__c,
			'The Opportunity signer Email does not correspond with the actual signer Email.'
		);
		System.assertEquals(
			signers[1].Name,
			opp.DS_Authorized_to_sign_2nd_Name__c,
			'The Opportunity signer Name does not correspond with the actual signer Name.'
		);
		System.assertEquals(
			signers[1].Email,
			opp.DS_Authorized_to_sign_2nd_Mail__c,
			'The Opportunity signer Email does not correspond with the actual signer Email.'
		);
		System.assertEquals(1, mock.callCount.get('getCaseAttachmentIdsByCaseId'), 'The Attachment retrieved should be equal to 1.');
		System.assertEquals(
			sourceCase.Id,
			mock.args.get('getCaseAttachmentIdsByCaseId')[0],
			'The retrieved Id is not equal to the expected Case Id.'
		);
		System.assertEquals(
			opp.Id,
			mock.args.get('attachDocumentsToSObjectFromSObject')[0],
			'The retrieved Id is not equal to the expected Opportunity Id.'
		);
		System.assertEquals(new Set<Id>{ opp.Id }, mock.args.get('attachDocumentsToSObjectFromSObject')[1], 'The retrieved Id is not the expected.');
		System.assertEquals(1, mock.callCount.get('attachDocumentsToSObjectFromSObject'), 'The retrieved Attachments should be equal to 1.');
		Test.stopTest();
	}

	@isTest
	static void prepareOpportunityAndGetUrlException() {
		AuraHandledException expectedException;
		try {
			DocuSignButtonController.prepareOpportunityAndCreateEnvelopeURL(null, false);
		} catch (AuraHandledException caughtException) {
			expectedException = caughtException;
		}
		System.assert(expectedException != null, 'The Exception should not be NULL.');
	}

	@IsTest
	static void docuSignButtonIsVisibleWhenDocuSignAdminUser() {
		TestUtils.createCompleteOpportunity();
		Opportunity opp = TestUtils.theOpportunity;
		Test.startTest();
		Boolean result = DocuSignButtonController.isDocuSignButtonVisible(opp.Id);
		Test.stopTest();
		User currentUser = [SELECT id, dsfs__DSProSFUsername__c, Profile.Name FROM User WHERE id = :UserInfo.getUserId()];
		Boolean isDocuSignUser = !String.isBlank(currentUser.dsfs__DSProSFUsername__c);
		Boolean isAdmin = String.valueOf(currentUser.Profile.Name).containsIgnoreCase('admin');

		if (isDocuSignUser && isAdmin) {
			System.assertEquals(true, result, 'DocuSign button should be visible to a DocuSign, Admin user.');
		} else {
			System.assertEquals(false, result, 'DocuSign button should not be visible to an non-DocuSign user.');
		}
	}

	@isTest
	static void getDocuSignPagereference() {
		Opportunity opp = TestUtils.createOpportunity(TestUtils.createAccount(null), Test.getStandardPricebookId());
		opp.Amount = 500001;
		update opp;
		opp = DocuSignButtonController.fetchOpportunity(opp.Id);
		opp.DS_Authorized_to_sign_1st_Mail__c = 'sign1@test.com';
		opp.DS_Authorized_to_sign_1st_Name__c = 'Sign1';
		opp.DS_Authorized_to_sign_2nd_Mail__c = 'sign2@test.com';
		opp.DS_Authorized_to_sign_2nd_Name__c = 'Sign2';

		Map<String, String> enveloperParams = DocuSignButtonController.assembleDocuSignEnvelopeParameters(opp);
		Pagereference ref = DocuSignButtonController.createDocuSignEnvelopePageReference(enveloperParams);
		System.assert(
			ref.getUrl().contains('dsfs__DocuSign_CreateEnvelope'),
			'The retrieved Page Reference URL does not contains the required parameter: dsfs__DocuSign_CreateEnvelope.'
		);
		System.assert(ref.getParameters().containsKey('CRL'), 'The retrieved Page Reference URL does not contains the required parameter: CRL.');
		System.assert(
			ref.getParameters().get('CRL').contains(opp.DS_Authorized_to_sign_2nd_Mail__c),
			'The retrieved CRL parameter is not equals to the defined authorized to sign.'
		);
	}

	@isTest
	static void validateEmptyParams() {
		Map<String, String> validMap = new Map<String, String>{ 'What goes in' => 'Must come out' };
		Map<String, String> inValidMap = new Map<String, String>{ 'What goes in' => '' };

		DocuSignButtonController.validateEmptyParams(validMap);

		DocuSignButtonController.DocuSignButtonControllerException expectedException;
		try {
			DocuSignButtonController.validateEmptyParams(inValidMap);
		} catch (DocusignButtonController.DocuSignButtonControllerException caughtException) {
			expectedException = caughtException;
		}
		System.assert(expectedException != null, 'The Exception should not be NULL.');
		System.assert(expectedException.getMessage().contains('What goes in'), 'The Exception message should contains the text: What goes in.');
	}

	@isTest
	static void whenContractSummaryExistsExpectNoException() {
		List<CS_DocItem> csDocItems = new List<CS_DocItem>();
		CS_DocItem csdocitem1 = new CS_DocItem();
		csdocitem1.name = 'First item';
		csdocitem1.isContractSummary = false;
		csDocItems.add(csdocitem1);
		CS_DocItem csdocitem2 = new CS_DocItem();
		csdocitem2.name = 'Second item';
		csdocitem2.isContractSummary = true;
		csDocItems.add(csdocitem2);

		Exception e = null;
		try {
			DocuSignButtonController.validateContractSummaryDocumentExists(csDocItems);
		} catch (Exception myException) {
			e = myException;
		}

		system.assertEquals(null, e, 'No exception should be raised when contract summary document exists');
	}

	@isTest
	static void whenContractSummaryDoesNotExistsExpectException() {
		List<CS_DocItem> csDocItems = new List<CS_DocItem>();
		CS_DocItem csdocitem1 = new CS_DocItem();
		csdocitem1.name = 'First item';
		csdocitem1.isContractSummary = false;
		csDocItems.add(csdocitem1);
		CS_DocItem csdocitem2 = new CS_DocItem();
		csdocitem2.name = 'Second item';
		csdocitem2.isContractSummary = false;
		csDocItems.add(csdocitem2);

		Exception e = null;
		try {
			DocuSignButtonController.validateContractSummaryDocumentExists(csDocItems);
		} catch (Exception myException) {
			e = myException;
		}
		system.assertNotEquals(null, e, 'An exception should have been raised when contract summary document does not exist');
	}

	private class GenericMock implements System.StubProvider {
		private Map<String, Object> returns = new Map<String, Object>();
		private Map<String, Integer> callCount = new Map<String, Integer>();
		private Map<String, List<Object>> args = new Map<String, List<Object>>();

		@SuppressWarnings('PMD.ExcessiveParameterList')
		public Object handleMethodCall(
			Object stubbedObject,
			String stubbedMethodName,
			Type returnType,
			List<Type> listOfParamTypes,
			List<String> listOfParamNames,
			List<Object> listOfArgs
		) {
			if (!callCount.containsKey(stubbedMethodName)) {
				callCount.put(stubbedMethodName, 0);
			}
			callCount.put(stubbedMethodName, callCount.get(stubbedMethodName) + 1);
			if (!args.containsKey(stubbedMethodName)) {
				args.put(stubbedMethodName, new List<Object>());
			}
			args.put(stubbedMethodName, listOfArgs);
			return returns.get(stubbedMethodName);
		}
	}

	private static List<Contact> getSignersAndPutOnAccount(Account acc) {
		TestUtils.autoCommit = false;
		List<Contact> signers = new List<Contact>{ TestUtils.createContact(acc), TestUtils.createContact(acc) };
		signers[0].Email = 'signer1@test.com';
		signers[1].Email = 'signer2@test.com';
		insert signers;
		TestUtils.autoCommit = true;
		acc.Authorized_to_sign_1st__c = signers[0].Id;
		acc.Authorized_to_sign_2nd__c = signers[1].Id;
		update acc;
		return signers;
	}
}
