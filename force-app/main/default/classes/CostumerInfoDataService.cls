public with sharing class CostumerInfoDataService {
	// Entry point to get account information
	public static Account getAccountData(String kvkNumber) {
		Account acc = getExistingAccount(kvkNumber);

		if (acc == null) {
			acc = createNewAccount(kvkNumber);
		}

		return acc;
	}

	// Entry point to get contact information
	public static List<Contact> getContactData(List<Contact> contacts) {
		List<String> emailList = new List<String>();
		List<Contact> fetchedContacts = new List<Contact>();
		List<Contact> finalContactList = new List<Contact>();
		List<Contact> contactsForUpdate = new List<Contact>();

		for (Contact c : contacts) {
			emailList.add(c.Email);
		}
		fetchedContacts = [
			SELECT Id, AccountId, Userid__c, FirstName, LastName, Gender__c, Salutation, MobilePhone, Email, Birthdate, Authorized_to_sign__c
			FROM Contact
			WHERE Email IN :emailList AND AccountId = :contacts[0].AccountId
		];
		if (fetchedContacts.isEmpty()) {
			// If no contact exist create all
			finalContactList = createContacts(contacts);
		} else {
			// If some contacts exist and some not filter out non existing one and create them
			Map<String, Contact> emailContactMap = new Map<String, Contact>();
			List<Contact> nonExistingContact = new List<Contact>();

			for (Contact fc : fetchedContacts) {
				emailContactMap.put(fc.Email, fc);
			}

			checkAndUpdateContacts(contacts, emailContactMap, nonExistingContact);

			// Create contacts that don't exist in the system and save them to final list that will be returned
			if (!nonExistingContact.isEmpty()) {
				finalContactList = createContacts(nonExistingContact);
			}
			// Add the fetched contacts to the final list that will be returned
			finalContactList.addAll(fetchedContacts);
		}
		// Populate Authorize to sign 1st on the account if it is empty
		populateAccountAuthorizedContact(finalContactList);

		return finalContactList;
	}

	@testVisible
	private static Account getExistingAccount(String kvkNumber) {
		List<Account> acc = [
			SELECT Id, Name, csb2c__E_Commerce_Customer_Id__c, Unify_Account_Type__c, Unify_Account_SubType__c, KVK_number__c
			FROM Account
			WHERE KVK_number__c = :kvkNumber
		];

		if (acc.isEmpty()) {
			return null;
		} else {
			return acc[0];
		}
	}

	@testVisible
	private static Account createNewAccount(String kvkNumber) {
		Account acc = null;
		if (Pattern.matches('[0-9]{8}', kvkNumber)) {
			OlbicoServiceJSON jService = new OlbicoServiceJSON();

			jService.setRequestRestJson(kvkNumber);
			jService.makeReqestRestJson(kvkNumber);

			if (jService.getAccId() != null) {
				acc = [
					SELECT Id, Name, csb2c__E_Commerce_Customer_Id__c, Unify_Account_Type__c, Unify_Account_SubType__c, KVK_number__c
					FROM Account
					WHERE Id = :jService.getAccId()
				];
			}
		}
		return acc;
	}

	@future
	public static void checkTypeAndSubType(Id accId) {
		Account acc = [
			SELECT Id, csb2c__E_Commerce_Customer_Id__c, KVK_number__c, Unify_Account_Type__c, Unify_Account_SubType__c
			FROM Account
			WHERE Id = :accId
		];
		acc.csb2c__E_Commerce_Customer_Id__c = 'A' + acc.KVK_number__c;
		if (acc.Unify_Account_Type__c == null) {
			acc.Unify_Account_Type__c = 'B';
			acc.Unify_Account_SubType__c = 'SME-FLEX';
		}
		update acc;
	}

	@testVisible
	private static List<Contact> createContacts(List<Contact> contacts) {
		insert contacts;
		return contacts;
	}

	@testVisible
	private static void checkAndUpdateContacts(List<Contact> contacts, Map<String, Contact> emailContactMap, List<Contact> nonExistingContact) {
		List<String> fields = new List<String>{ 'AccountId', 'FirstName', 'LastName', 'Authorized_to_sign__c', 'Birthdate', 'Gender__c' };
		List<Contact> contactsForUpdate = new List<Contact>();

		for (Contact cnt : contacts) {
			// If contact exists check if it needs to be updated
			if (emailContactMap.containsKey(cnt.Email)) {
				Boolean shouldUpdate = false;
				// Validation prevents from updating Contact if user id is populated
				if (String.isBlank(String.valueOf(emailContactMap.get(cnt.Email).Userid__c))) {
					for (String field : fields) {
						// Check if the values are different
						if (cnt.get(field) != emailContactMap.get(cnt.Email).get(field)) {
							// Change sf record with the received value
							emailContactMap.get(cnt.Email).put(field, cnt.get(field));
							shouldUpdate = true;
						}
					}
				} else {
					// Check if the AccountId is different as it can be updated without triggering the validation
					if (cnt.get('AccountId') != emailContactMap.get(cnt.Email).get('AccountId')) {
						// Change sf record with the received value
						emailContactMap.get(cnt.Email).put('AccountId', cnt.get('AccountId'));
						shouldUpdate = true;
					}
				}
				// Add record to list to be updated later
				if (shouldUpdate) {
					contactsForUpdate.add(emailContactMap.get(cnt.Email));
				}
			} else {
				// If contact doesn't exist add it to list to be created
				nonExistingContact.add(cnt);
			}
		}

		// If there were any changes persist them
		if (!contactsForUpdate.isEmpty()) {
			// If the changes need to be persisted do it here. Currently we don't update the contacts
			// update contactsForUpdate;
		}
	}

	@testVisible
	private static void populateAccountAuthorizedContact(List<Contact> conts) {
		Account acc = [SELECT Id, Authorized_to_sign_1st__c FROM Account WHERE Id = :conts[0].AccountId];

		if (acc.Authorized_to_sign_1st__c == null) {
			for (Contact cnt : conts) {
				if (cnt.Authorized_to_sign__c) {
					acc.Authorized_to_sign_1st__c = cnt.Id;
				}
			}
			update acc;
		}
	}

	// Entry point to get Site__c data
	public static List<Site__c> getSiteData(Id accId, List<Map<String, String>> siteData) {
		List<Site__c> sites = new List<Site__c>();

		// Get and add the existing sites
		sites.addAll(fetchExistingSites(accId, siteData));
		// Create sites for address information that couldn't be matched to an existing site on that account
		List<Map<String, String>> nonExistingSites = new List<Map<String, String>>();
		// If no sites were matched/feched we need to create them all
		if (sites.isEmpty()) {
			nonExistingSites = siteData;
		} else {
			for (Map<String, String> sd : siteData) {
				Boolean match = false;
				// Check if we have already fetched a site
				for (Site__c s : sites) {
					if (
						s.Site_Postal_Code__c == sd.get(CustomerInfoRestService.POSTCODE) &&
						String.valueOf(s.Site_House_Number__c) == sd.get(CustomerInfoRestService.HOUSENUMBER) &&
						(String.isBlank(sd.get(CustomerInfoRestService.HOUSENRSUFFIX)) ||
						s.Site_House_Number_Suffix__c == sd.get(CustomerInfoRestService.HOUSENRSUFFIX))
					) {
						match = true;
					}
				}
				// If we matched a site we don't need to add it to the list to be created
				if (!match) {
					nonExistingSites.add(sd);
				}
			}
		}

		// Get and add newly created sites
		if (!nonExistingSites.isEmpty()) {
			sites.addAll(createSites(accId, nonExistingSites));
		}

		return sites;
	}

	// Get the existing sites based on the address information provided by commerce cloud
	@testVisible
	private static List<Site__c> fetchExistingSites(Id accId, List<Map<String, String>> siteData) {
		List<Site__c> sites = new List<Site__c>();

		if (!siteData.isEmpty()) {
			List<Site__c> accSites = [
				SELECT Id, Site_Street__c, Site_Postal_Code__c, Site_House_Number__c, Site_House_Number_Suffix__c, Site_Account__c, Site_City__c
				FROM Site__c
				WHERE Site_Account__c = :accId
			];

			for (Site__c s : accSites) {
				for (Map<String, String> sd : siteData) {
					if (
						s.Site_Postal_Code__c == sd.get(CustomerInfoRestService.POSTCODE) &&
						s.Site_House_Number__c == Decimal.valueOf(sd.get(CustomerInfoRestService.HOUSENUMBER)) &&
						(String.isBlank(sd.get(CustomerInfoRestService.HOUSENRSUFFIX)) ||
						s.Site_House_Number_Suffix__c == sd.get(CustomerInfoRestService.HOUSENRSUFFIX))
					) {
						sites.add(s);
					}
				}
			}
		}
		return sites;
	}

	// Create the sites based on the address information provided by commerce cloud
	@testVisible
	private static List<Site__c> createSites(Id accId, List<Map<String, String>> siteData) {
		Account acc = [SELECT Id, KVK_number__c FROM Account WHERE Id = :accId];
		List<Site__c> sitesForInsertion = new List<Site__c>();

		for (Map<String, String> sd : siteData) {
			Site__c newSite = createSite(acc.Id, acc.KVK_number__c, sd);
			if (newSite.KVK_Number__c != null) {
				sitesForInsertion.add(newSite);
			}
		}
		if (!sitesForInsertion.isEmpty()) {
			insert sitesForInsertion;
		}
		return sitesForInsertion;
	}

	@testVisible
	private static Site__c createSite(Id accId, String kvkNumber, Map<String, String> siteData) {
		Site__c newSite = new Site__c();
		if (
			String.isBlank(siteData.get(CustomerInfoRestService.HOUSENRSUFFIX)) ||
			siteData.get(CustomerInfoRestService.HOUSENRSUFFIX).trim().equalsIgnoreCase(siteData.get(CustomerInfoRestService.HOUSENRSUFFIX).trim())
		) {
			newSite.Site_Account__c = accId;
			newSite.KVK_Number__c = kvkNumber;
			newSite.Site_Street__c = siteData.get(CustomerInfoRestService.SITESTREET);
			newSite.Site_House_Number__c = Integer.valueof(siteData.get(CustomerInfoRestService.HOUSENUMBER));
			newSite.Site_House_Number_Suffix__c = siteData.get(CustomerInfoRestService.HOUSENRSUFFIX);
			newSite.Site_Postal_Code__c = siteData.get(CustomerInfoRestService.POSTCODE);
			newSite.Site_City__c = siteData.get(CustomerInfoRestService.SITECITY);
			newSite.Olbico__c = false;
			newSite.Name = siteData.get(CustomerInfoRestService.SITESTREET);
		}

		return newSite;
	}
}
