/**
 * @description
 * @author       mcubias@vodafoneziggo.com
 * @since        2022.10.05
 */
@IsTest
public class TestServiceResourceSkillsAction {
	@TestSetup
	static void initData() {
		TestUtils.createAdministrator();
	}

	@IsTest
	static void testGetServiceResourceIds() {
		WorkType workType = new WorkType(Name = 'Test Work Type', DurationType = 'Hours', EstimatedDuration = 1);
		insert workType;

		WorkTypeGroup workTypeGroup = new WorkTypeGroup(Name = 'Test WTG', IsActive = true, GroupType = 'Default');
		insert workTypeGroup;
		// join record
		WorkTypeGroupMember workTypeGroupMember = new WorkTypeGroupMember(WorkTypeId = workType.Id, WorkTypeGroupId = workTypeGroup.Id);
		insert workTypeGroupMember;

		User admin = [SELECT Id FROM User WHERE Email = 'testuser@vodafone.com'];
		ServiceResource businessConsultant = new ServiceResource(
			ResourceType = 'T',
			Name = 'Carlos Ray Norris',
			IsActive = true,
			RelatedRecordId = admin.Id
		);
		insert businessConsultant;

		List<Skill> skills = [SELECT Id, DeveloperName, MasterLabel FROM Skill WHERE DeveloperName = 'Large'];

		ServiceResourceSkill norrisLarge = new ServiceResourceSkill(
			ServiceResourceId = businessConsultant.Id,
			SkillId = skills.get(0).Id,
			EffectiveStartDate = System.today().addDays(-3)
		);
		insert norrisLarge;

		Test.startTest();
		List<String> businessConsultants = ServiceResourceSkillsAction.getServiceResourceIds(new List<String>{ 'Large' });
		Test.stopTest();
		System.assert(!businessConsultants.isEmpty(), 'No Business Consultants have been assigned to Large skill');
		System.assertEquals(businessConsultant.Id, businessConsultants.get(0), 'Business consultant Id not returned in CSV list');
	}
}
