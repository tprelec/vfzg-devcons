@isTest
public with sharing class TestGenerateDocumentQueueable {
	@isTest
	static void testGenDoc() {
		User u = [SELECT Id FROM User LIMIT 1];
		GenerateDocumentQueueable gdq = new GenerateDocumentQueueable(u.Id);
		Test.startTest();
		System.enqueueJob(gdq);
		Test.stopTest();
		System.assert(gdq.parentId == u.Id, 'Parent Id not set.');
	}
}