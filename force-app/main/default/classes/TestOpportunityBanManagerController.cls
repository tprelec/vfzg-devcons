/**
 * @Description:   Test class for OpportunityBanManagerController
 * @Author:        Guy Clairbois
 */
@isTest
private class TestOpportunityBanManagerController
{
    @IsTest
    static void test_method_success()
    {
        // create contract
        TestUtils.createCompleteOpportunity();

        // go to screen
        PageReference pr = new PageReference('/apex/OpportunityBanManager?oppId='+TestUtils.theOpportunity.Id);
        Test.setCurrentPage(pr);

        // open attachmentmgr
        ApexPages.StandardController controller = new ApexPages.Standardcontroller(TestUtils.theOpportunity);
        OpportunityBanManagerController obmc = new OpportunityBanManagerController(controller);

        obmc.updateBan();
        obmc.cancelUpdateBan();
    }
}