public with sharing class OpportunityClosingService implements Queueable {
	private static String macId;
	private Set<Id> opportunityIds;
	private Map<Id, Opportunity> opportunityMap;
	private Map<Id, List<OpportunityLineItem>> oppLineItemsByOpp;
	@TestVisible
	private Map<Id, VF_Contract__c> contractMap;
	Map<String, String> oliFieldMapping;
	Set<String> availabilityIds;

	public OpportunityClosingService(Set<Id> opportunityIds) {
		this.opportunityIds = opportunityIds;
	}

	public void execute(QueueableContext context) {
		this.handleOpportunityClosing();
	}

	public void handleOpportunityClosing() {
		Savepoint svpt = Database.setSavepoint();
		try {
			getMappings();
			getOpportunities();
			getOpportunityLineItems();
			getContracts();
			if (createContractedProducts()) {
				createInitialContractTasks();
				migrateContractAttachments();
			}
			performOrderDecomposition();
			updateClosingCompletedStatus();
		} catch (Exception ex) {
			Database.rollback(svpt);
			updateClosingErrorStatus();
			LoggerService.log(ex, opportunityIds);
			CS_OrderGenerationObserver.firePlatformEvent('', new List<Id>(opportunityIds)[0], false);
		}
	}

	@TestVisible
	private void getOpportunities() {
		opportunityMap = new Map<Id, Opportunity>(
			[
				SELECT
					Id,
					Name,
					Vodafone_Products__c,
					Dummy_Contract__c,
					Contract_VF__c,
					Account.name,
					Closing_Status__c,
					BAN__r.Name,
					BAN__r.Corporate_Id__c,
					Primary_Basket__c
				FROM Opportunity
				WHERE Id IN :opportunityIds
			]
		);
	}

	private void getOpportunityLineItems() {
		String oliQuery = 'SELECT ';
		oliQuery += String.join(oliFieldMapping.values(), ', ');
		oliQuery += ', New_Delivery_Model__c, New_Portfolio__c, Opportunity.RecordTypeId, Site_Availability_List__c, PricebookEntry.Product2.OrderType__c, PricebookEntry.Product2.Role__c, PricebookEntry.Product2.Product_Group__c, PricebookEntry.Product2Id, PricebookEntry.ProductCode, PricebookEntry.Product2.Multiplier_Vodacom__c, OpportunityId FROM OpportunityLineItem WHERE OpportunityId IN :opportunityIds';
		List<OpportunityLineItem> lineItems = Database.query(String.escapeSingleQuotes(oliQuery));
		oppLineItemsByOpp = (Map<Id, List<OpportunityLineItem>>) GeneralUtils.groupByIDField(lineItems, 'OpportunityId');
	}

	private void getContracts() {
		Set<Id> contractIds = GeneralUtils.getIDSetFromListIgnoreNull(opportunityMap.values(), 'Contract_VF__c');
		contractMap = new Map<Id, VF_Contract__c>([SELECT Id, Opportunity__c, Owner__c FROM VF_Contract__c WHERE Id IN :contractIds]);
	}

	private void getMappings() {
		oliFieldMapping = SyncUtil.fullMapping('OpportunityLineItem -> Contracted_Products__c').get('OpportunityLineItem -> Contracted_Products__c');
	}

	private Boolean createContractedProducts() {
		List<Contracted_Products__c> newContractedProducts = new List<Contracted_Products__c>();
		for (Opportunity opp : opportunityMap.values()) {
			List<OpportunityLineItem> olis = oppLineItemsByOpp.get(opp.Id);
			VF_Contract__c contract = contractMap.get(opp.Contract_VF__c);
			if (contract == null) {
				continue;
			}
			for (OpportunityLineItem oli : olis) {
				if (!oli.New_Delivery_Model__c) {
					newContractedProducts.addAll(opportunityLineItemToContractedProducts(oli, contract));
				}
			}
		}
		if (!newContractedProducts.isEmpty()) {
			insert newContractedProducts;
			return true;
		}
		return false;
	}

	private static String getMacId() {
		if (String.isBlank(macId)) {
			macId = [SELECT Id FROM Ordertype__c WHERE Name = 'MAC'].Id;
		}
		return macId;
	}

	private List<Contracted_Products__c> opportunityLineItemToContractedProducts(OpportunityLineItem oli, VF_Contract__c contract) {
		// Create Product from Line Item
		Contracted_Products__c product = new Contracted_Products__c();
		for (String contractProdField : oliFieldMapping.keySet()) {
			String oppProdField = oliFieldMapping.get(contractProdField);
			Object oppProdValue = oli.get(oppProdField);
			if (oppProdValue != null) {
				product.put(contractProdField, oppProdValue);
			}
		}
		product.OpportunityLineItemId__c = oli.Id;
		product.Site_List__c = null;
		product.VF_Contract__c = contract.Id;
		product.Product__c = oli.PricebookEntry.Product2Id;
		product.ProductCode__c = oli.PricebookEntry.ProductCode;
		product.Multiplier_Vodacom__c = oli.PricebookEntry.Product2.Multiplier_Vodacom__c;
		if (product.OrderType__c == null) {
			if (oli.Opportunity.RecordTypeId == GeneralUtils.getRecordTypeIdByName(SObjectType.Opportunity.getName(), 'MAC')) {
				product.OrderType__c = getMacId();
			} else {
				product.OrderType__c = oli.PricebookEntry.Product2.OrderType__c;
			}
		}
		// Create one product for each site
		List<String> sites = getLineItemSites(oli);
		List<String> siteAvailabilityList = getLineItemSitesAvailabilities(oli);
		return createSiteProducts(oli, product, sites, siteAvailabilityList);
	}

	private List<String> getLineItemSites(OpportunityLineItem oli) {
		List<String> sites = new List<String>();
		if (!String.isEmpty(oli.Site_List__c)) {
			String sep = ',';
			if (oli.Site_List__c.contains(';')) {
				sep = ';';
			}
			sites = oli.Site_List__c.split(sep);
		} else {
			sites.add(null);
		}
		return sites;
	}

	private List<String> getLineItemSitesAvailabilities(OpportunityLineItem oli) {
		List<String> siteAvailabilityList = new List<String>();
		if (!String.isEmpty(oli.Site_Availability_List__c)) {
			siteAvailabilityList = oli.Site_Availability_List__c.split(';');
		}
		if (availabilityIds == null) {
			availabilityIds = new Set<String>();
			availabilityIds.addAll(siteAvailabilityList);
		}
		return siteAvailabilityList;
	}

	private List<Contracted_Products__c> createSiteProducts(
		OpportunityLineItem oli,
		Contracted_Products__c product,
		List<String> sites,
		List<String> siteAvailabilityList
	) {
		List<Contracted_Products__c> products = new List<Contracted_Products__c>();
		Integer numberOfSites = sites.size();
		Integer countSites = 0;
		for (String siteId : sites) {
			// Sometimes BM puts the string 'null' here, which has to be caught..
			if (siteId == 'null') {
				siteId = null;
			}
			Contracted_Products__c cp = product.clone();
			cp.Quantity__c = product.Quantity__c.divide(numberOfSites, 0);
			if (siteId != null) {
				cp.Site__c = siteId;
			}
			if (!siteAvailabilityList.isEmpty() && countSites <= siteAvailabilityList.size() - 1) {
				cp.Site_Availability__c = siteAvailabilityList[countSites];
				countSites++;
			}
			// Create a separate ContractedProduct for some types of products if quantity is more than 1
			if (cp.Quantity__c > 1) {
				if (oli.Proposition__c != 'One Mobile') {
					if (
						oli.PricebookEntry.Product2.Product_Group__c == 'Infra' ||
						oli.PricebookEntry.Product2.Product_Group__c == 'SLA' ||
						oli.PricebookEntry.Product2.Product_Group__c == 'CPE' ||
						oli.PricebookEntry.Product2.Role__c == 'Numberporting' ||
						oli.PricebookEntry.Product2.Role__c == 'Switch'
					) {
						Decimal originalQuantity = cp.Quantity__c;
						cp.Subtotal__c = product.Subtotal__c.divide(originalQuantity, 2);
						cp.Quantity__c = 1;
						for (Integer i = 0; i < originalQuantity - 1; i++) {
							Contracted_Products__c cpCopy = cp.clone();
							products.add(cpCopy);
						}
					}
				}
			}
			products.add(cp);
		}
		return products;
	}

	private void createInitialContractTasks() {
		List<Task> newTasks = new List<Task>();
		for (VF_Contract__c contract : contractMap.values()) {
			Opportunity opp = opportunityMap.get(contract.Opportunity__c);
			if (opp.Vodafone_Products__c > 0 && !opp.Dummy_Contract__c) {
				String oppName = Label.Contract_VF_Initial_Task_Subject + ': ' + opp.name;
				String accountName = ' - Account: ' + opp.Account.name;
				String subjectString = oppName + accountName;
				if (subjectString.length() > 255) {
					if (oppName.length() > accountName.length()) {
						oppName = oppName.abbreviate(255 - accountName.length());
						subjectString = (oppName + accountName);
						System.debug(LoggingLevel.INFO, subjectString);
					}
				}
				subjectString = subjectString.abbreviate(255);
				Task newTask = new Task(
					Subject = subjectString,
					Description = Label.Contract_VF_Initial_Task_Description,
					OwnerId = contract.Owner__c,
					WhatId = contract.Id
				);
				newTasks.add(newTask);
			}
		}
		if (!newTasks.isEmpty()) {
			insert newTasks;
		}
	}

	private void migrateContractAttachments() {
		List<Contract_Attachment__c> newContractAttachments = new List<Contract_Attachment__c>();
		Map<Id, Opportunity_Attachment__c> oppAttachments = new Map<Id, Opportunity_Attachment__c>(
			[
				SELECT Id, Name, Attachment_Type__c, Description_Optional__c, Opportunity__c, Signed_Contract__c, ContentDocumentId__c
				FROM Opportunity_Attachment__c
				WHERE Opportunity__c IN :opportunityIds AND Attachment_Type__c != 'HBO Attachment'
			]
		);
		List<ContentDocumentLink> contentDocumentLinkToInsert = new List<ContentDocumentLink>();
		Map<Id, ContentDocumentLink> oaIdToContentDocumentLink = new Map<Id, ContentDocumentLink>();
		List<FeedItem> feedItemsToInsert = new List<FeedItem>();
		Map<FeedItem, Id> feedAttachmentMap = new Map<FeedItem, Id>();
		Map<Id, FeedItem> oaIdToFeedItem = new Map<Id, FeedItem>();
		if (!oppAttachments.isEmpty()) {
			for (ContentDocumentLink cdl : [
				SELECT Id, ContentDocumentId, LinkedEntityId
				FROM ContentDocumentLink
				WHERE LinkedEntityId IN :oppAttachments.keySet()
			]) {
				oaIdToContentDocumentLink.put(cdl.LinkedEntityId, cdl);
			}
			for (FeedItem fi : [SELECT Id, ParentId, Title FROM FeedItem WHERE ParentId IN :oppAttachments.keySet()]) {
				oaIdToFeedItem.put(fi.ParentId, fi);
			}
			for (Opportunity_Attachment__c oa : oppAttachments.values()) {
				Contract_Attachment__c ca = new Contract_Attachment__c(
					Attachment_Type__c = oa.Attachment_Type__c,
					Description_Optional__c = oa.Description_Optional__c,
					Signed_Contract__c = oa.Signed_Contract__c,
					Contract_VF__c = opportunityMap.get(oa.Opportunity__c).Contract_VF__c,
					Source_Opportunity_Attachment_Id__c = oa.Id,
					ContentDocumentId__c = oa.ContentDocumentId__c
				);
				newContractAttachments.add(ca);
			}
		}

		newContractAttachments.addAll(migrateHBOAttachments());
		insert newContractAttachments;

		for (Contract_Attachment__c ca : newContractAttachments) {
			if (oaIdToContentDocumentLink.containsKey(ca.Source_Opportunity_Attachment_Id__c) || ca.Attachment_Type__c == 'HBO Attachment') {
				ContentDocumentLink cdl = new ContentDocumentLink(
					ContentDocumentId = ca.ContentDocumentId__c,
					LinkedEntityId = ca.id,
					ShareType = 'V',
					Visibility = 'Allusers'
				);
				contentDocumentLinkToInsert.add(cdl);
			}
			if (oaIdToFeedItem.containsKey(ca.Source_Opportunity_Attachment_Id__c)) {
				FeedItem oppFi = oaIdToFeedItem.get(ca.Source_Opportunity_Attachment_Id__c);
				FeedItem fi = new FeedItem(ParentId = ca.Id, Title = oppFi.Title);
				feedItemsToInsert.add(fi);
				feedAttachmentMap.put(fi, ca.ContentDocumentId__c);
			}
		}
		delete oppAttachments.values();
		try {
			List<Database.SaveResult> ircontentDocumentLinkToInsert = Database.insert(contentDocumentLinkToInsert, false);
			List<Database.SaveResult> irfeedItemsToInsert = Database.insert(feedItemsToInsert, false);
			List<FeedAttachment> feedAttachmentToInsert = new List<FeedAttachment>();
			for (FeedItem fi : feedAttachmentMap.keySet()) {
				FeedAttachment fia = new FeedAttachment(FeedEntityId = fi.Id, RecordId = feedAttachmentMap.get(fi), Type = 'CONTENT');
				feedAttachmentToInsert.add(fia);
			}
			List<Database.SaveResult> irfeedAttachmentToInsert = Database.insert(feedAttachmentToInsert, false);
		} catch (Exception e) {
		}
	}

	private List<Contract_Attachment__c> migrateHBOAttachments() {
		List<Contract_Attachment__c> oppToContractAttachment = new List<Contract_Attachment__c>();
		if (!availabilityIds.isEmpty()) {
			List<HBO__c> hbos = [
				SELECT Id, HBO_Opportunity__c, (SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title FROM ContentDocumentLinks)
				FROM HBO__c
				WHERE
					HBO_Opportunity__c IN :opportunityIds
					AND HBO_Status__c = 'Approved'
					AND HBO_Postal_Check__c IN (SELECT Source_Check_Entry__c FROM Site_Availability__c WHERE Id IN :availabilityIds)
			];
			for (HBO__c hbo : hbos) {
				if (!hbo.ContentDocumentLinks.isEmpty()) {
					for (ContentDocumentLink cl : hbo.ContentDocumentLinks) {
						Contract_Attachment__c ca = new Contract_Attachment__c();
						ca.Attachment_Type__c = 'HBO Attachment';
						ca.Contract_VF__c = opportunityMap.get(hbo.HBO_Opportunity__c).Contract_VF__c;
						ca.Source_Opportunity_Attachment_Id__c = cl.LinkedEntityId;
						ca.ContentDocumentId__c = cl.ContentDocumentId;
						oppToContractAttachment.add(ca);
					}
				}
			}
		}
		return oppToContractAttachment;
	}

	private void updateClosingCompletedStatus() {
		for (Opportunity opp : opportunityMap.values()) {
			opp.Closing_Status__c = 'Completed';
		}
		update opportunityMap.values();
	}

	@TestVisible
	private void updateClosingErrorStatus() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		if (opportunityMap == null) {
			return;
		}
		for (Opportunity opp : opportunityMap.values()) {
			opp.Closing_Status__c = 'Error';
		}
		update opportunityMap.values();
	}

	/*
	 * @description Performs O&S decomposition of the primary basket
	 * Which Product Configurations will decompose --> there are 2 conditions:
	 * 1) cscfga__Product_Definition__r.csordtelcoa__Product_Type__c != Nothing
	 * 2) csordtelcoa__Ignore_For_Order_Decomposition__c == false
	 * As a result of the decomposition csord__Order__c, csord__Subscription__c and csord__Service__c records are created
	 */
	private void performOrderDecomposition() {
		Boolean hasNewPortfolio = false;
		for (Opportunity opp : opportunityMap.values()) {
			List<OpportunityLineItem> olis = oppLineItemsByOpp.get(opp.Id);
			for (OpportunityLineItem oli : olis) {
				if (oli.New_Portfolio__c) {
					hasNewPortfolio = true;
					break;
				}
			}
		}
		// if all oli's are for legacy Order, then we can skip decomposition to Cloudsense O&S
		if (!hasNewPortfolio) {
			return;
		}
		List<Id> basketIds = new List<Id>();
		for (Opportunity opportunity : opportunityMap.values()) {
			if (opportunity.Primary_Basket__c != null) {
				basketIds.add(opportunity.Primary_Basket__c);
			}
		}
		if (!basketIds.isEmpty()) {
			csordtelcoa.API_V1.generateOrdersFromBaskets(basketIds, false);
		}
	}
}
