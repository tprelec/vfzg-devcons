@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class CS_CustomStepCompleteTaskCancel implements CSPOFA.ExecutionHandler {
	public List<SObject> process(List<SObject> steps) {
		List<SObject> result = new List<SObject>();
		Set<Id> deliveryOrderIds = new Set<Id>();
		Map<Id, CSPOFA__Orchestration_Step__c> processStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
		Map<Id, CSPOFA__Orchestration_Step__c> deliveryOrderStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
		List<CSPOFA__Orchestration_Step__c> stepList = [
			SELECT
				Id,
				Name,
				CSPOFA__Orchestration_Process__c,
				CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c,
				CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c,
				CSPOFA__Status__c,
				CSPOFA__Completed_Date__c,
				CSPOFA__Message__c,
				CSPOFA__Expression_Result__c,
				CSPOFA__Related_Object__c,
				CSPOFA__Related_Object_ID__c,
				COM_Task_Status__c
			FROM CSPOFA__Orchestration_Step__c
			WHERE Id IN :steps
		];

		//try {
		for (CSPOFA__Orchestration_Step__c step : stepList) {
			processStepMap.put(step.CSPOFA__Orchestration_Process__c, step);
			deliveryOrderStepMap.put(step.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c, step);
			deliveryOrderIds.add(step.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c);
		}

		Map<Id, Task> taskMap = new Map<Id, Task>(
			[
				SELECT
					Id,
					Subject,
					RecordType.Name,
					CSPOFA__Orchestration_Step__c,
					CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__c,
					CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c
				FROM Task
				WHERE WhatId IN :deliveryOrderIds AND IsClosed = FALSE AND CSPOFA__Orchestration_Step__c != NULL
			]
		);

		CS_OrchestratorStepUtility.updateTasksAndPauseSteps(taskMap.values(), deliveryOrderStepMap);

		for (CSPOFA__Orchestration_Step__c step : stepList) {
			result.add(CS_OrchestratorStepUtility.setStepToComplete(step));
		}
		/*} catch (Exception ex) {
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				result.add(CS_OrchestratorStepUtility.setStepToError(step, ex));
			}
		}*/
		return result;
	}
}
