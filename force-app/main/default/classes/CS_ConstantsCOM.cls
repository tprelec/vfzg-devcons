public with sharing class CS_ConstantsCOM {
	public static final String CONTRACT_STATUS_IMPLEMENTATION = 'Implementation';
	public static final String CONTRACT_STATUS_AMENDED = 'Amended';

	public static final String PRODUCT_DEFINITION_ACCESS = 'Access';
	public static final String PRODUCT_DEFINITION_INTERNET = 'Internet';
	public static final String PRODUCT_DEFINITION_BUSINESS_INTERNET = 'Business Internet';

	public static final String SOLUTION_DEFINITION_TYPE_MAIN = 'Main';
	public static final String SOLUTION_DEFINITION_TYPE_COMPONENT = 'Component';

	public static final String SOLUTION_DEFINITION_NAME_BIMO = 'Business Internet Modem Only';

	public static final String OLI_ACTIVITY_TYPE_ADD = 'Add';
	public static final String OLI_ACTIVITY_TYPE_CEASE = 'Cease';

	public static final String BASKET_CHANGE_TYPE_CHANGE_SOLUTION = 'Change Solution';

	public static final String DM_TYPE_PERCENTAGE = 'percentage';
	public static final String DM_TYPE_ABSOLUTE = 'absolute';
}
