@isTest
public with sharing class TestTaxonomyTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Taxonomy';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'TAX-07-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		Taxonomy__c testTaxonomy = new Taxonomy__c();
		testTaxonomy.Name = 'Test';
		insert testTaxonomy;

		Taxonomy__c queryRecord = [SELECT Id, ExternalID__c FROM Taxonomy__c LIMIT 1];
		System.assert(queryRecord.ExternalID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
