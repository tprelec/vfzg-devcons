/**
 * @author: Rahul Sharma (07-09-2021)
 * @description: Queueable class for ISW Site Check
 *      TestSitePostalCheckEventTriggerHandler covers unit test of this class
 *      Jira ticket reference: COM-1960
 */

public with sharing class ISWSiteCheckQueueable implements Queueable, Database.AllowsCallouts {
    private List<Id> lstSiteId;
    public ISWSiteCheckQueueable(List<Id> lstSiteId) {
        this.lstSiteId = lstSiteId;
    }

    public void execute(QueueableContext context) {
        // System.debug('@@@@lstSiteId: ' + this.lstSiteId);
        if (!this.lstSiteId.isEmpty()) {
            ISWSiteCheckService.performISWSiteCheck(lstSiteId[0]);
            this.lstSiteId.remove(0);
            enqueueNextJob();
        }
    }

    // System.enqueueJob is only allowed once in a unit test, hence this is marked as @testVisible and tested explicitly
    @TestVisible
    private void enqueueNextJob() {
        if (!this.lstSiteId.isEmpty()) {
            Id jobId = System.enqueueJob(
                new ISWSiteCheckQueueable(this.lstSiteId)
            );
        }
    }
}