public with sharing class CleanOrderCriteriaHelper {
	public static String validateFieldsOnOpportunity(Id basketId) {
		try {
			checkIfInstallationFieldsArePopulated(basketId);
			return null;
		} catch (CleanOrderCriteriaHelperException cleanOrderException) {
			return cleanOrderException.getMessage();
		} catch (Exception objEx) {
			LoggerService.log(objEx);
			return System.Label.InstallationInformation_GenericExceptionMessage;
		}
	}

	public static void checkIfInstallationFieldsArePopulated(Id basketId) {
		for (cscfga__Product_Configuration__c productConfiguration : getProductConfigurations(basketId)) {
			if (!productConfiguration.Is_Installation_Information_Complete__c && productConfiguration.cscfga__Parent_Configuration__c == null) {
				throw new CleanOrderCriteriaHelperException(System.Label.InstallationInformation_FieldsAreRequired);
			}
		}
	}

	private static List<cscfga__Product_Configuration__c> getProductConfigurations(Id basketId) {
		return [
			SELECT Id, Is_Installation_Information_Complete__c, cscfga__Parent_Configuration__c
			FROM cscfga__Product_Configuration__c
			WHERE Site__c != NULL AND cscfga__Product_Basket__c = :basketId
		];
	}

	public class CleanOrderCriteriaHelperException extends Exception {
	}
}
