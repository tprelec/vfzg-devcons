/**
 * @description         This class schedules the batch processing of Ban export.
 * @author              Guy Clairbois
 */
global class ScheduleBanExportBatch implements Schedulable {
    
    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){
        
		BanExportBatch banBatch = new BanExportBatch();
		Id batchprocessId = Database.executeBatch(banBatch,100);
    }
    
}