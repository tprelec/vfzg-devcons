public with sharing class ClickApproveEmailHandler implements IEmailHandler {
	public Messaging.SingleEmailMessage handleEmail(EmailService.EmailRequest request) {
		CSCAP__Customer_Approval__c approval = [
			SELECT Id, CSCAP__Click_Approve_Setting__r.CSCAP__From_Email_Address__c, CSCAP__Key__c
			FROM CSCAP__Customer_Approval__c
			WHERE Id = :request.relatedToId
		];
		CSCAP__ClickApprove_Approver__c approver = [
			SELECT Id, CSCAP__Contact__c
			FROM CSCAP__ClickApprove_Approver__c
			WHERE CSCAP__Customer_Approval__c = :approval.Id AND CSCAP__Contact__c = :request.recipientId
		];
		List<Id> attachmentIds = new List<Id>();
		for (Attachment att : [SELECT Id FROM Attachment WHERE ParentId = :approval.Id]) {
			attachmentIds.add(att.Id);
		}
		Messaging.SingleEmailMessage mail = Messaging.renderStoredEmailTemplate(request.templateId, approver.CSCAP__Contact__c, approval.Id);
		LoggerService.log(mail.getHtmlBody());
		mail.setHtmlBody(mail.getHtmlBody().replace('Approvelink', getApprovalLink(approval, approver)));
		LoggerService.log(mail.getHtmlBody());
		mail.setSaveAsActivity(true);
		mail.setEntityAttachments(attachmentIds);
		String orgWideEmail = approval.CSCAP__Click_Approve_Setting__r.CSCAP__From_Email_Address__c;
		if (String.isNotBlank(orgWideEmail)) {
			mail.setOrgWideEmailAddressId(EmailService.getOrgWideEmailAddress(orgWideEmail).Id);
		}
		return mail;
	}

	private String getApprovalLink(CSCAP__Customer_Approval__c approval, CSCAP__ClickApprove_Approver__c approver) {
		return String.format(
			'{0}?key={1}&aid={2}',
			new List<String>{ CSCAP__ClickApprove_Constants__c.getInstance().CSCAP__Site_Address__c, approval.CSCAP__Key__c, approver.Id }
		);
	}
}
