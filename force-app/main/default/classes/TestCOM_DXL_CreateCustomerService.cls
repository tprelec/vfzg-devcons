@isTest
private class TestCOM_DXL_CreateCustomerService {
	@testSetup
	static void setup() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		TestUtils.createOrderType();

		Integer nrOfRows = 2;
		User owner = TestUtils.createAdministrator();

		Account acct = TestUtils.createAccount(owner);
		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test', Email = 'contact@test.com', Phone = '+31 111111789');
		insert c;

		TestUtils.autoCommit = false;
		List<Site__c> sites = new List<Site__c>();
		sites.add(TestUtils.createSite(acct));
		sites.add(TestUtils.createSite(acct));
		insert sites;

		Opportunity testOpportunity = CS_DataTest.createOpportunity(acct, 'Test Opportunity', owner.Id);
		testOpportunity.Main_Contact_Person__c = c.Id;
		insert testOpportunity;

		List<Product2> products = new List<Product2>();
		for (Integer i = 0; i < nrOfRows; i++) {
			products.add(TestUtils.createProduct());
		}
		insert products;

		List<PricebookEntry> pbEntries = new List<PricebookEntry>();
		for (Integer i = 0; i < nrOfRows; i++) {
			pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i)));
		}
		insert pbEntries;

		List<Opportunitylineitem> olis = new List<Opportunitylineitem>();

		for (Integer i = 0; i < nrOfRows; i++) {
			OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(testOpportunity, pbEntries.get(0));
			oppLineItem.Location__c = sites[i].Id;
			olis.add(oppLineItem);
		}

		insert olis;

		External_account__c externalaccountcObj = new External_account__c(
			Account__c = acct.Id,
			External_Account_Id__c = 'test value',
			External_Source__c = 'Unify'
		);
		insert externalaccountcObj;

		External_contact__c externalcontactcObj = new External_contact__c(
			Contact__c = c.Id,
			External_Contact_Id__c = 'test value',
			External_Source__c = 'Unify',
			ExternalId__c = 'test value'
		);
		insert externalcontactcObj;

		External_site__c externalsitecObj = new External_site__c(
			External_Site_Id__c = 'UNIFY123654789',
			External_Source__c = 'Unify',
			Site__c = sites[0].Id
		);
		insert externalsitecObj;

		List<Contact_Role__c> contactRoleList = new List<Contact_Role__c>();
		Contact_Role__c contactRole1 = new Contact_Role__c();
		contactRole1.Type__c = 'main';
		contactRole1.Account__c = acct.Id;
		contactRole1.Contact__c = c.Id;

		contactRoleList.add(contactRole1);

		insert contactRoleList;

		csord__Order__c ord = CS_DataTest.createOrder();
		ord.csord__Status2__c = 'Created';
		insert ord;
	}

	@isTest
	static void testMethod1() {
		List<Opportunity> oppList = [SELECT Id FROM Opportunity];
		List<csord__Order__c> ordList = [SELECT Id FROM csord__Order__c];
		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(oppList[0].Id, ordList[0].Id);
		Set<Id> siteIds = customerService.getSiteIds(oppList[0].Id);
		List<Site__c> sites = customerService.getSites(siteIds);

		System.assertEquals(2, siteIds.size(), 'Expected number of sitesIds: 2 -> Actual: ' + siteIds.size());
		System.assertEquals(2, sites.size(), 'Expected number of sites: 2 -> Actual: ' + sites.size());
	}

	@isTest
	static void testMethod2() {
		List<Opportunity> oppList = [SELECT Id FROM Opportunity];
		List<Account> accounts = [SELECT Id, Name FROM Account];
		Set<Id> accountIds = new Set<Id>();
		for (Account acc : accounts) {
			accountIds.add(acc.Id);
		}

		List<csord__Order__c> ordList = [SELECT Id FROM csord__Order__c];
		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(oppList[0].Id, ordList[0].Id);

		List<External_Account__c> externalAccounts = customerService.getExternalAccounts(accountIds);
		Map<Id, List<External_Account__c>> accountWithExternalAccounts = customerService.getAccountExternalAccounts(externalAccounts);

		System.assertEquals(
			1,
			accountWithExternalAccounts.keySet().size(),
			'Expected number of Accounts: 1 -> Actual: ' + accountWithExternalAccounts.keySet().size()
		);
	}

	@isTest
	static void testMethod3() {
		List<Opportunity> oppList = [SELECT Id FROM Opportunity];
		List<Site__c> sites = [SELECT Id, Name FROM Site__c];
		Set<Id> siteIds = new Set<Id>();
		for (Site__c site : sites) {
			siteIds.add(site.Id);
		}

		List<csord__Order__c> ordList = [SELECT Id FROM csord__Order__c];
		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(oppList[0].Id, ordList[0].Id);
		List<External_Site__c> externalSites = customerService.getExternalSites(siteIds);
		Map<Id, List<External_Site__c>> siteWithExternalSites = customerService.getSiteExternalSites(externalSites);

		System.assertEquals(
			1,
			siteWithExternalSites.keySet().size(),
			'Expected number of Accounts: 1 -> Actual: ' + siteWithExternalSites.keySet().size()
		);
	}

	@isTest
	static void testMethod4() {
		List<Opportunity> oppList = [SELECT Id FROM Opportunity];
		List<Contact> contacts = [SELECT Id, Name FROM Contact];
		Set<Id> contactIds = new Set<Id>();
		for (Contact cnt : contacts) {
			contactIds.add(cnt.Id);
		}

		List<csord__Order__c> ordList = [SELECT Id FROM csord__Order__c];
		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(oppList[0].Id, ordList[0].Id);
		List<External_Contact__c> externalContacts = customerService.getExternalContacts(contactIds);
		Map<Id, List<External_Contact__c>> contactWithExternalContacts = customerService.getContactExternalContacts(externalContacts);

		System.assertEquals(
			1,
			contactWithExternalContacts.keySet().size(),
			'Expected number of Accounts: 1 -> Actual: ' + contactWithExternalContacts.keySet().size()
		);
	}
	@isTest
	static void testMethod5() {
		List<Opportunity> testOpps = [SELECT ID FROM Opportunity];
		List<csord__Order__c> ordList = [SELECT Id FROM csord__Order__c];
		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(testOpps[0].Id, ordList[0].Id);

		List<Site__c> sites = [
			SELECT
				Id,
				Name,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Site_Postal_Code__c,
				Site_City__c,
				Country__c,
				Location_Alias__c,
				Unify_Site_Name__c,
				Canvas_Street__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_City__c,
				SLA__r.BOP_Code__c,
				Location_Type__c,
				Site_Phone__c,
				Building__c,
				Site_Account__c
			FROM Site__c
		];
		Set<Id> siteIds = new Set<Id>();
		for (Site__c site : sites) {
			siteIds.add(site.Id);
		}
		List<External_Site__c> externalSites = customerService.getExternalSites(siteIds);
		Map<Id, List<External_Site__c>> sitesWithExternalSites = customerService.getSiteExternalSites(externalSites);
		System.assertEquals(1, sitesWithExternalSites.size(), 'Result String contains JSON: ' + sitesWithExternalSites.size());

		List<Contact> contacts = [SELECT Id, Name FROM Contact];
		Set<Id> contactIds = new Set<Id>();
		for (Contact cnt : contacts) {
			contactIds.add(cnt.Id);
		}

		List<External_Contact__c> externalContacts = customerService.getExternalContacts(contactIds);
		Map<Id, List<External_Contact__c>> contactsWithExternalContacts = customerService.getContactExternalContacts(externalContacts);
		System.assertEquals(1, contactsWithExternalContacts.size(), 'Result String contains JSON: ' + contactsWithExternalContacts.size());

		List<Account> accounts = [SELECT Id, Name FROM Account];
		Set<Id> accountIds = new Set<Id>();
		for (Account acc : accounts) {
			accountIds.add(acc.Id);
		}

		List<External_Account__c> externalAccounts = customerService.getExternalAccounts(accountIds);
		Map<Id, List<External_Account__c>> accountsWithExternalAccounts = customerService.getAccountExternalAccounts(externalAccounts);
		System.assertEquals(1, accountsWithExternalAccounts.size(), 'Result String contains JSON: ' + accountsWithExternalAccounts.size());

		String result = customerService.generateDxlCreateCustomerPayload();

		System.assertEquals(true, !String.isEmpty(result), 'Result String contains JSON: ' + result.substring(0, 20));
	}

	@isTest
	static void testMethod6() {
		List<Opportunity> oppData = [
			SELECT Id, Account.Authorized_to_Sign_1st__c, Contact__c, Financial_Account__r.Financial_Contact__c
			FROM Opportunity
		];

		List<Contact_Role__c> contactRoles = [
			SELECT
				Id,
				Name,
				Contact__c,
				Contact__r.Email,
				Type__c,
				Contact__r.MobilePhone,
				Account__c,
				Contact__r.Account.Visiting_Street__c,
				Contact__r.Account.Visiting_Housenumber1__c,
				Contact__r.Account.Visiting_Housenumber_Suffix__c,
				Contact__r.Account.Visiting_Postal_Code__c,
				Contact__r.Account.Visiting_City__c,
				Contact__r.Account.Visiting_Country__c,
				Contact__r.Gender__c,
				Contact__r.Birthdate,
				Contact__r.FirstName,
				Contact__r.LastName,
				Contact__r.Phone,
				Contact__r.Document_Type__c,
				Contact__r.Document_Number__c,
				Contact__r.Jurisdiction_Type__c
			FROM Contact_Role__c
			WHERE Account__c = :oppData[0].AccountId
		];

		List<csord__Order__c> ordList = [SELECT Id FROM csord__Order__c];
		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(oppData[0].Id, ordList[0].Id);
		Set<Id> contactIds = customerService.getOpportunityContacts(oppData[0], contactRoles);

		System.assertEquals(2, contactIds.size(), 'Expected number of Contacts: 2 -> Actual: ' + contactIds.size());
	}

	@isTest
	static void testMethod7() {
		List<Opportunity> oppData = [SELECT Id FROM Opportunity];
		List<csord__Order__c> ordList = [SELECT Id FROM csord__Order__c];
		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(oppData[0].Id, ordList[0].Id);

		String attachmentData = 'test data';
		customerService.createRequestAttachment(oppData[0].Id, attachmentData);

		List<Attachment> attList = [SELECT Id, Body FROM Attachment WHERE ParentId = :oppData[0].Id];

		System.assertEquals('test data', attList[0].Body.toString(), 'Expected content: "test data" -> Actual: ' + attList[0].Body.toString());
	}

	@isTest
	static void testMethod8() {
		List<Opportunity> oppData = [SELECT Id FROM Opportunity];
		List<csord__Order__c> ordList = [SELECT Id FROM csord__Order__c];
		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(oppData[0].Id, ordList[0].Id);

		COM_DXL_settings__mdt businessTransactionIdSetting = COM_DXL_settings__mdt.getInstance(
			COM_DXL_Constants.CREATECUSTOMER_SYNC_RESPONSE_TRANSACTION_ID
		);

		String businessTransactionIdKey = businessTransactionIdSetting != null
			? businessTransactionIdSetting.Value__c
			: COM_DXL_Constants.BUSINESS_TRANSACTION_ID;

		HttpResponse dxlSyncResponse = new HttpResponse();
		dxlSyncResponse.setHeader('transactionId', 'TR-' + Datetime.now().getTime());
		dxlSyncResponse.setStatusCode(400);
		dxlSyncResponse.setBody('{"statusCode": 400,"statusMessage": "Error"}');
		customerService.createTransactionLog(dxlSyncResponse, oppData[0].Id, businessTransactionIdKey);

		List<COM_DXL_Integration_Log__c> transactionLogs = [SELECT Id FROM COM_DXL_Integration_Log__c WHERE Opportunity__c = :oppData[0].Id];

		System.assertEquals(false, transactionLogs.isEmpty(), 'Expected: true Actual: ' + String.valueOf(transactionLogs.isEmpty()));
	}

	@isTest
	static void testMethod9() {
		String keyName = '01';
		String expected = 'sole proprietorship';
		String result = COM_DXL_CreateCustomerService.getDXLMapping(COM_DXL_Constants.COM_DXL_LEGAL_ENTITY_MAPPING, keyName);

		System.assertEquals(expected, result, 'Expected: ' + expected + ' Actual: ' + result);
	}
}
