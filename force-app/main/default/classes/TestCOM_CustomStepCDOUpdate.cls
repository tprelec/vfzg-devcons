@isTest
private class TestCOM_CustomStepCDOUpdate {
	@isTest
	private static void testProcessMethod() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, false);
		delOrd.CDO_Integration_Status__c = COM_Constants.CDO_INTEGRATION_STATUS_COMPLETE;
		delOrd.MACD_Type__c = COM_Constants.DEFAULT_MACD_TYPE;
		insert delOrd;

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c service = new csord__Service__c();
		service.csord__Identification__c = 'TestService';
		service.Name = 'TestService';
		service.csord__Subscription__c = subscription.Id;
		service.Installation_Required__c = true;
		service.Provisioning_Required__c = true;
		service.COM_Delivery_Order__c = delOrd.Id;
		service.Has_Delivery_Components__c = true;
		service.Delivery_Components__c = 'L2';
		insert service;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
		CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', false);
		insert step1Template;

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = delOrd.Id;
		testProcess.Name = COM_Constants.CDO_PROVISION_PROCESS_NAME;
		insert testProcess;
		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);

		List<CSPOFA__Orchestration_Step__c> steps = CS_OrchestratorStepUtility.getStepList(new List<CSPOFA__Orchestration_Step__c>{ step1 });

		Test.startTest();
		COM_CustomStepCDOUpdate cdoCall = new COM_CustomStepCDOUpdate();
		List<SObject> result = cdoCall.process(steps);
		Test.stopTest();

		System.assertEquals(
			Constants.ORCHESTRATOR_STEP_COMPLETE,
			((CSPOFA__Orchestration_Step__c) result[0]).CSPOFA__Status__c,
			'Status has a wrong value.'
		);

		csord__Service__c updatedService = [
			SELECT CDO_Umbrella_Service_Id__c, csord__Status__c, OSS10_Service_Id__c
			FROM csord__Service__c
			WHERE Id = :service.Id
		];
		System.assertEquals(COM_Constants.CDO_STATUS_PROVISIONED, updatedService.csord__Status__c, 'Status has a wrong value.');
	}

	@isTest
	private static void testUpdateDeliveryOrdersForCDOComplete() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, false);
		delOrd.CDO_Integration_Status__c = COM_Constants.CDO_INTEGRATION_STATUS_COMPLETE;
		delOrd.MACD_Type__c = COM_Constants.DEFAULT_MACD_TYPE;
		insert delOrd;

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c service = new csord__Service__c();
		service.csord__Identification__c = 'TestService';
		service.Name = 'TestService';
		service.csord__Subscription__c = subscription.Id;
		service.Installation_Required__c = true;
		service.Provisioning_Required__c = true;
		service.COM_Delivery_Order__c = delOrd.Id;
		service.Has_Delivery_Components__c = true;
		service.Delivery_Components__c = 'L2';
		insert service;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
		CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', false);
		insert step1Template;

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = delOrd.Id;
		testProcess.Name = COM_Constants.CDO_PROVISION_PROCESS_NAME;
		insert testProcess;
		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);

		List<CSPOFA__Orchestration_Step__c> steps = CS_OrchestratorStepUtility.getStepList(new List<CSPOFA__Orchestration_Step__c>{ step1 });

		Test.startTest();
		COM_CustomStepCDOUpdate.updateDeliveryOrdersForCDO(steps);
		Test.stopTest();

		csord__Service__c updatedService = [
			SELECT CDO_Umbrella_Service_Id__c, csord__Status__c, OSS10_Service_Id__c
			FROM csord__Service__c
			WHERE Id = :service.Id
		];
		System.assertEquals(COM_Constants.CDO_STATUS_PROVISIONED, updatedService.csord__Status__c, 'Status has a wrong value.');
	}

	@isTest
	private static void testUpdateDeliveryOrdersForCDOFail() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, false);
		delOrd.CDO_Integration_Status__c = COM_Constants.CDO_INTEGRATION_STATUS_FAILURE;
		insert delOrd;

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c service = new csord__Service__c();
		service.csord__Identification__c = 'TestService';
		service.Name = 'TestService';
		service.csord__Subscription__c = subscription.Id;
		service.Installation_Required__c = true;
		service.Provisioning_Required__c = true;
		service.COM_Delivery_Order__c = delOrd.Id;
		service.Has_Delivery_Components__c = true;
		insert service;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
		CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', false);
		insert step1Template;

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = delOrd.Id;
		testProcess.Name = COM_Constants.CDO_PROVISION_PROCESS_NAME;
		insert testProcess;
		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);

		List<CSPOFA__Orchestration_Step__c> steps = CS_OrchestratorStepUtility.getStepList(new List<CSPOFA__Orchestration_Step__c>{ step1 });

		Test.startTest();
		COM_CustomStepCDOUpdate.updateDeliveryOrdersForCDO(steps);
		Test.stopTest();

		csord__Service__c updatedService = [SELECT CDO_Umbrella_Service_Id__c, csord__Status__c FROM csord__Service__c WHERE Id = :service.Id];
		System.assertNotEquals(COM_Constants.CDO_STATUS_PROVISIONED, updatedService.csord__Status__c, 'Status has a wrong value.');
	}

	@isTest
	private static void testUpdateDeliveryComponentsSuspend() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, true);
		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c service = new csord__Service__c();
		service.csord__Identification__c = 'TestService';
		service.Name = 'TestService';
		service.csord__Subscription__c = subscription.Id;
		service.Installation_Required__c = true;
		service.Provisioning_Required__c = true;
		service.COM_Delivery_Order__c = delOrd.Id;
		service.Has_Delivery_Components__c = true;
		insert service;

		Test.startTest();
		COM_CustomStepCDOUpdate.updateServices(new Set<Id>{ delOrd.Id }, new Map<Id, String>{ delOrd.Id => COM_Constants.CDO_SUSPEND_PROCESS_NAME });
		Test.stopTest();

		csord__Service__c updatedService = [SELECT CDO_Umbrella_Service_Id__c, csord__Status__c FROM csord__Service__c WHERE Id = :service.Id];
		System.assertEquals(COM_Constants.CDO_STATUS_SUSPENDED, updatedService.csord__Status__c, 'Status has a wrong value.');
	}

	@isTest
	private static void testUpdateDeliveryComponentsDeprovision() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, false);
		delOrd.Termination_Date__c = System.today();
		delOrd.MACD_Type__c = 'Termination';
		insert delOrd;

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c service = new csord__Service__c();
		service.csord__Identification__c = 'TestService';
		service.Name = 'TestService';
		service.csord__Subscription__c = subscription.Id;
		service.Installation_Required__c = true;
		service.Provisioning_Required__c = true;
		service.COM_Delivery_Order__c = delOrd.Id;
		service.Has_Delivery_Components__c = true;
		service.csordtelcoa__Cancelled_By_Change_Process__c = true;
		insert service;

		Test.startTest();
		COM_CustomStepCDOUpdate.updateServices(
			new Set<Id>{ delOrd.Id },
			new Map<Id, String>{ delOrd.Id => COM_Constants.CDO_DEPROVISION_PROCESS_NAME }
		);
		Test.stopTest();

		csord__Service__c updatedService = [SELECT Deprovisioned_Date__c, csord__Status__c FROM csord__Service__c WHERE Id = :service.Id];
		System.assertEquals(COM_Constants.CDO_STATUS_DEPROVISIONED, updatedService.csord__Status__c, 'Status has a wrong value.');
		System.assertNotEquals(null, updatedService.Deprovisioned_Date__c, 'Date is empty.');
	}

	@isTest
	private static void testUpdateDeliveryComponentsProvisioningNoInstallationRequired() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, false);
		delOrd.CDO_Integration_Status__c = COM_Constants.CDO_INTEGRATION_STATUS_COMPLETE;
		delOrd.MACD_Type__c = COM_Constants.DEFAULT_MACD_TYPE;
		insert delOrd;

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c service = new csord__Service__c();
		service.csord__Identification__c = 'TestService';
		service.Name = 'TestService';
		service.csord__Subscription__c = subscription.Id;
		service.Installation_Required__c = false;
		service.Provisioning_Required__c = true;
		service.COM_Delivery_Order__c = delOrd.Id;
		service.Has_Delivery_Components__c = true;
		insert service;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
		CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', false);
		insert step1Template;

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = delOrd.Id;
		testProcess.Name = COM_Constants.CDO_PROVISION_PROCESS_NAME;
		insert testProcess;
		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);

		List<CSPOFA__Orchestration_Step__c> steps = CS_OrchestratorStepUtility.getStepList(new List<CSPOFA__Orchestration_Step__c>{ step1 });

		Test.startTest();
		COM_CustomStepCDOUpdate.updateDeliveryOrdersForCDO(steps);
		Test.stopTest();

		csord__Service__c updatedService = [
			SELECT CDO_Umbrella_Service_Id__c, csord__Status__c, Implemented_Date__c
			FROM csord__Service__c
			WHERE Id = :service.Id
		];
		System.assertNotEquals(null, updatedService.Implemented_Date__c, 'Date is empty.');
		System.assertEquals(COM_Constants.CDO_STATUS_IMPLEMENTED, updatedService.csord__Status__c, 'Status has a wrong state.');
	}
}
