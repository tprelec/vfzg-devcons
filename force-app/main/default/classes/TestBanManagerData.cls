@isTest
public with sharing class TestBanManagerData {

    @isTest
    public static void testCreateBanManagerData() {
        Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
        BAN__c ban = TestUtils.createBan(acc);

        BanManagerData bmd = new BanManagerData(new Set<Id>{acc.Id}, ban.Id, true, true, true, true);
        List<SelectOption> options;

        Test.startTest();
            options = bmd.bans;
        Test.stopTest();

        System.assert(!options.isEmpty());
    }

    @isTest
    public static void testInsertBan() {
        Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
        BAN__c ban = TestUtils.createBan(acc);

        BanManagerData bmd = new BanManagerData(new Set<Id>{acc.Id}, ban.Id, false, false, false, false);
        bmd.newBan = '599999999';
        BAN__c newBan;

        Test.startTest();
            newBan = bmd.insertNewBan(acc.Id);
        Test.stopTest();

        System.assert(newBan.name == '599999999');
    }

    @isTest
    public static void testGetBanRequestNew() {
        Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
        BAN__c ban = TestUtils.createBan(acc);

        BanManagerData bmd = new BanManagerData(new Set<Id>{acc.Id}, ban.Id, false, false, false, false);
        bmd.newBan = ban.Name;
        bmd.banSelected = 'Request New Unify BAN';
        BAN__c newBan;
        Test.startTest();
            newBan = bmd.getBan(acc.Id);
        Test.stopTest();

        System.assert(newBan.name.contains('Requested New Unify BAN'));
    }

    @isTest
    public static void testGetBanNew() {
        Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
        //BAN__c ban = TestUtils.createBan(acc);

        BanManagerData bmd = new BanManagerData(new Set<Id>{acc.Id}, null, false, false, false, false);
        bmd.newBan = '599999999';
        bmd.banSelected = 'New';
        BAN__c newBan;

        Test.startTest();
            newBan = bmd.getBan(acc.Id);
        Test.stopTest();

        System.assert(newBan.name == '599999999');
    }

    @isTest
    public static void testGetBanRequestException() {
        Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
        BAN__c ban = TestUtils.createBan(acc);

        BanManagerData bmd = new BanManagerData(new Set<Id>{acc.Id}, ban.Id, false, false, false, false);
        bmd.newBan = ban.Name;
        bmd.banSelected = 'new';
        String result;
        Test.startTest();
            try {
                bmd.getBan(acc.Id);
            } catch (Exception e) {
                result = e.getMessage();
            }

        Test.stopTest();

        System.assert(result.contains('BAN already exists for this account. Please select from the list.'));
    }

    @isTest
    public static void testGetBanPartner() {
        Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
        Account partnerAcc = TestUtils.createPartnerAccount();
        User portalUser = TestUtils.createPortalUser(partnerAcc);

        BanManagerData bmd = new BanManagerData(new Set<Id>{acc.Id}, null, false, false, false, false);
        bmd.newBan = '599999999';
        BAN__c newBan;

        System.runAs(portalUser){
            newBan = bmd.getBan(acc.Id);
        }
    }

    @isTest
    public static void testSharingInsert(){
        Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
        BAN__c ban = TestUtils.createBan(acc);
        Account partnerAcc = TestUtils.createPartnerAccount();
        User portalUser = TestUtils.createPortalUser(partnerAcc);

        Test.startTest();
            BanManagerData.createAccountSharing(acc.Id, portalUser.Id);
            BanManagerData.createBanSharing(ban, UserInfo.getUserId());
        Test.stopTest();

        List<AccountShare> accShares = [SELECT Id FROM AccountShare];
        LIST<Ban__Share> banShares = [SELECT Id FROM Ban__Share];
        System.assert(!accShares.isEmpty() && !banShares.isEmpty());
    }
}