@isTest
private class CS_ContractPDFControllerTest {

	private static testMethod void test() {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            Test.startTest();
            
            List<Contract_Custom_Settings__mdt> contractSettings = [SELECT Product_Name__c, Product_Code__c, Document_template__c, English_contract__c FROM Contract_Custom_Settings__mdt];
            
            No_Triggers__c notriggers = new No_Triggers__c();
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;
            
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;
            
            Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
            insert tmpOpp;
            
            cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);  
            insert tmpProductBasket;
            
            csclm__Document_Definition__c testDocumentDefinition = CS_DataTest.createDocumentDefinition();
            testDocumentDefinition.ExternalID__c = 'SomeId3';
            insert testDocumentDefinition;
            
            csclm__Document_Template__c docTemplate = CS_DataTest.createDocumentTemplate('TestDocumentTemplate');
            docTemplate.csclm__Document_Definition__c = testDocumentDefinition.Id;
            docTemplate.ExternalID__c = 'SomeId3';
            insert docTemplate;
            
            csclm__Agreement__c testAgreement = CS_DataTest.createAgreement('TestAgreement');
            testAgreement.csclm__Document_Template__c = docTemplate.Id;
            testAgreement.Product_Basket__c = tmpProductBasket.Id;
            insert testAgreement;
            
            cscfga__Product_Definition__c tmpProductDef = CS_DataTest.createProductDefinition('Test Product Definition');
            tmpProductDef.Product_Type__c = 'Fixed';
            insert tmpProductDef;

            cscfga__Product_Definition__c tmpProductDef2 = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
            tmpProductDef2.Product_Type__c = 'Mobile';
            insert tmpProductDef2;
            
            cscfga__Product_Definition__c parentProductDef = CS_DataTest.createProductDefinition('Test Parent Product Definition');
            parentProductDef.Product_Type__c = 'Fixed';
            insert parentProductDef;
            
            cscfga__Product_Configuration__c parentProdConfig = CS_DataTest.createProductConfiguration(parentProductDef.Id, 'Parent Product Configuration', tmpProductBasket.Id);
            insert parentProdConfig;
            
            Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112',tmpAcc, '1111AA', 'Breestraat','LEIDEN', 112); 
            insert site;
            
            Site_Postal_Check__c spc = CS_DataTest.createSite(site);
            insert spc;
            
            Site_Availability__c siteAvailability = CS_DataTest.createSiteAvailability('LEIDEN, Breestraat 112', site,spc); 
            insert siteAvailability;
      
            List<cscfga__Product_Configuration__c> prodConfigList = new List<cscfga__Product_Configuration__c>();
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Access Infrastructure', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'One Fixed', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'One Net', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Managed Internet', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'IPVPN', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Business Managed Services', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Mobile CTN profile', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Mobile CTN profile', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Mobile CTN profile', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Skype for Business', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Mobile CTN profile', tmpProductBasket.Id));
            prodConfigList.add(CS_DataTest.createProductConfiguration(tmpProductDef.Id, 'Mobile CTN profile', tmpProductBasket.Id));
            insert prodConfigList;
            
            prodConfigList[0].Site_Availability_Id__c = siteAvailability.Id;
            prodConfigList[0].OneNet__c = prodConfigList[2].Id;
            prodConfigList[0].ManagedInternet__c = prodConfigList[3].Id;
            prodConfigList[0].IPVPN__c = prodConfigList[4].Id;
            prodConfigList[0].OneFixed__c = prodConfigList[1].Id;
            prodConfigList[0].OneNet_Scenario__c = 'One Net Enterprise';
            prodConfigList[0].Fixed_Scenario__c = 'One Fixed Enterprise';
            prodConfigList[6].Mobile_Scenario__c = 'OneBusiness';
            prodConfigList[7].Mobile_Scenario__c = 'Data only';
            prodConfigList[8].Mobile_Scenario__c = 'RedPro';
            prodConfigList[10].Mobile_Scenario__c = 'OneBusiness IOT';
            prodConfigList[11].Mobile_Scenario__c = 'OneMobile';
            update prodConfigList;
            
            List<CS_Basket_Snapshot_Transactional__c> basketSnapList = new List<CS_Basket_Snapshot_Transactional__c>();
            basketSnapList.add(CS_DataTest.createBasketSnapshotTransactional('Basket Snap 1', tmpProductBasket.Id, prodConfigList[0].Id, 'Product 1', 'PG1'));
            basketSnapList.add(CS_DataTest.createBasketSnapshotTransactional('Basket Snap 2', tmpProductBasket.Id, prodConfigList[0].Id, 'Product 2', 'PG2'));
            basketSnapList.add(CS_DataTest.createBasketSnapshotTransactional('Basket Snap 3', tmpProductBasket.Id, prodConfigList[5].Id, 'Product 3', 'PG3'));
            basketSnapList.add(CS_DataTest.createBasketSnapshotTransactional('Basket Snap 4', tmpProductBasket.Id, prodConfigList[6].Id, 'Product 4', 'PG4'));
            basketSnapList.add(CS_DataTest.createBasketSnapshotTransactional('Basket Snap 4', tmpProductBasket.Id, prodConfigList[7].Id, 'Product 5', 'PG5'));
            basketSnapList.add(CS_DataTest.createBasketSnapshotTransactional('Basket Snap 4', tmpProductBasket.Id, prodConfigList[8].Id, 'Product 6', 'PG6'));
            basketSnapList.add(CS_DataTest.createBasketSnapshotTransactional('Basket Snap 4', tmpProductBasket.Id, prodConfigList[9].Id, 'Product 9', 'PG9'));
            basketSnapList.add(CS_DataTest.createBasketSnapshotTransactional('Basket Snap 4', tmpProductBasket.Id, prodConfigList[10].Id, 'Product 10', 'PG10'));
            basketSnapList.add(CS_DataTest.createBasketSnapshotTransactional('Basket Snap 4', tmpProductBasket.Id, prodConfigList[11].Id, 'Product 11', 'PG11'));
            basketSnapList[8].Product_Definition__c = tmpProductDef2.Id;
            insert basketSnapList;
            
            csclm__Transactional_Document__c transactionalDocument = new csclm__Transactional_Document__c();
            transactionalDocument.csclm__Agreement__c = testAgreement.Id;
            insert transactionalDocument;
            
            csclm__Transactional_Section__c transactionalSection = new csclm__Transactional_Section__c();
            transactionalSection.csclm__Transactional_Document__c = transactionalDocument.Id;
            insert transactionalSection;
            
            csclm__Transactional_Clause__c transactionalClause = new csclm__Transactional_Clause__c();
            transactionalClause.csclm__Transactional_Section__c = transactionalSection.Id;
            transactionalClause.csclm__Final_Rich_Text__c = '<div class="centerMe">One Business IoT</div>';
            insert transactionalClause;
        
            
            ApexPages.StandardController stdController = new ApexPages.StandardController(testAgreement);
            CS_ContractPDFController contractPDFController = new CS_ContractPDFController(stdController);

            CS_Future_Controller futureController;

            String newUrl = '/apex/c__CS_Future_Waiter_Page?basketId=' + tmpProductBasket.Id + '&parentProcessId=abc';
            futureController = new CS_Future_Controller(tmpProductBasket.Id, 'Parent process for preparing Contract Data.', newUrl, newUrl);
 
            Id newFutureId = futureController.defineNewFutureJob('Collect contract data');
            CS_ContractPDFController.populateDataAsync(tmpProductBasket.Id, newFutureId);

            String res = contractPDFController.CalculateAddendumNumber(testAgreement, 'Business Managed Services', contractSettings);
            System.assertEquals(true, res.length() > 0);
            res = contractPDFController.CalculateAddendumNumber(testAgreement, 'Vodafone Managed Netwerk Services', contractSettings);
            System.assertEquals(true, res.length() > 0);
            String res2 = contractPDFController.CalculateAddendumNumber(testAgreement, 'Vodafone Mobiel Internet Dienst', contractSettings);
            System.assertEquals(true, res != res2);
            String res3 = contractPDFController.CalculateAddendumNumber(testAgreement, 'Vodafone One Business Dienst', contractSettings);
            System.assertEquals(true, res2 != res3);
            String res4 = contractPDFController.CalculateAddendumNumber(testAgreement, 'Addendum Vodafone ONE Business IoT Dienst', contractSettings);
            System.assertEquals(true, res3 != res4);
                        
            contractPDFController.addHeaders(testAgreement, contractSettings);
            System.assertEquals(true, contractPDFController.pageBreaks.size() > 1);
            
            CS_SnapshotHelper.getConfigPerId(prodConfigList[0].Id, prodConfigList);

            CS_ContractsTableHelper.getDiscountEndRowsOneNet(3,'Product');
            CS_ContractsTableHelper.addTrForFieldNames(basketSnapList[0], new List<String>{'Name'});

            CS_ContractsTableHelper.getMNSBijlage(prodConfigList);
            CS_ContractsTableHelper.createTableRowsProductList(basketSnapList,new List<String>{'Name'});

            CS_ContractsTableHelper.createTableRowsWithValues(new List<String>{'Name'});
            
            Test.stopTest();
        }
	}

}