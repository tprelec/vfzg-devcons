global with sharing class TB_PreInflightChangeImplementation implements csordcb.ObserverApi.IObserver{
    global TB_PreInflightChangeImplementation(){
        
        
    }
    
    global void execute(csordcb.ObserverApi.Observable o,Object arg){
        csordtelcoa.InflightObservable observable = (csordtelcoa.InflightObservable)o; 
        Map<Id,Id> ordIdByBasketId = observable.getOrderIdByBasketIdMap();
        
        list<csord__Service__c> servicesToUpdate = [select id, tb_pre_in_flight_hash__c , tb_post_in_flight_hash__c  from csord__service__c where csord__order__c in :ordIdByBasketId.values()];
        
        for (csord__service__c srv : servicesToUpdate){
            srv.tb_pre_in_flight_hash__c = srv.tb_post_in_flight_hash__c;
        }
        
        update servicesToUpdate;
    }
}