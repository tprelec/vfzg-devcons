@isTest
public class CS_CPEPriceItemLookupTest{
    private static testMethod void testRequiredAttributes(){
        CS_CPEPriceItemLookup caLookup = new CS_CPEPriceItemLookup();
        caLookup.getRequiredAttributes();
    }
  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
     System.runAs (simpleUser) { 
      
     
      Framework__c frameworkSetting = new Framework__c();
      frameworkSetting.Framework_Sequence_Number__c = 2;
      insert frameworkSetting;
      
      PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
     insert priceResetSetting;
      Account testAccount = CS_DataTest.createAccount('Test Account');
      insert testAccount;
      
      Sales_Settings__c ssettings = new Sales_Settings__c();
      ssettings.Postalcode_check_validity_days__c = 2;
      ssettings.Max_Daily_Postalcode_Checks__c = 2;
      ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
      ssettings.Postalcode_check_block_period_days__c = 2;
      ssettings.Max_weekly_postalcode_checks__c = 15;
      insert ssettings;
      
      
      //price items
      OrderType__c orderType = CS_DataTest.createOrderType();
      insert orderType;
      Product2 productCpeSla2 = CS_DataTest.createProduct('CPE1', orderType);
      insert productCpeSla2;
      
      Product2 productCpeSla1 = CS_DataTest.createProduct('CPE2', orderType);
      insert productCpeSla1;
      
      Category__c cpeSlaCategory = CS_DataTest.createCategory('CPE');
      insert cpeSlaCategory;
      
      Vendor__c vendor1 = CS_DataTest.createVendor('KPNWEAS');
      insert vendor1;
    
        
    
        cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(productCpeSla2, cpeSlaCategory, 20480.0, 20480.0, vendor1, 'ONNET', 'OfficeTTR2BD');
        priceItem1.Bundle_Services__c = 'Internet';
        priceItem1.Access_Type_Multi__c = 'EthernetOverFiber';
        priceItem1.OneFixed_Technology_Type__c = 'ISDN30';
        priceItem1.Wireless_compatible__c = true;
        priceItem1.SFP_Slots__c = 4;
        priceItem1.OneFixed_SIP_Channels__c = 4;
        priceItem1.Min_Duration__c = 1;
        priceItem1.Max_Duration__c = 99;
        
        insert priceItem1;
      
      
      
      Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
      insert testOpp;
      
      
      cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        insert basket;
        
       
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        Id packageDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Package Definition').getRecordTypeId(); 
        
        
        
        cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        accessInfraDef.Product_Type__c = 'Fixed';
        accessInfraDef.RecordTypeId = productDefinitionRecordType;
        insert accessInfraDef;
        
        
        
        cscfga__Product_Definition__c managedInternetDef = CS_DataTest.createProductDefinition('Managed Internet');
        managedInternetDef.Product_Type__c = 'Fixed';
        managedInternetDef.RecordTypeId = productDefinitionRecordType;
        insert managedInternetDef;
        
        cscfga__Product_Configuration__c managedInternetConf = CS_DataTest.createProductConfiguration(managedInternetDef.Id, 'Managed Internet',basket.Id);
        managedInternetConf.cscfga__Root_Configuration__c = null;
        managedInternetConf.cscfga__Parent_Configuration__c = null;
        insert managedInternetConf;
        
        cscfga__Product_Definition__c ipvpnDef = CS_DataTest.createProductDefinition('IPVPN');
        ipvpnDef.Product_Type__c = 'Fixed';
         ipvpnDef.RecordTypeId = productDefinitionRecordType;
        
        insert ipvpnDef;
        cscfga__Product_Configuration__c ipvpnConf = CS_DataTest.createProductConfiguration(ipvpnDef.Id, 'IPVPN',basket.Id);
        ipvpnConf.cscfga__Root_Configuration__c = null;
        ipvpnConf.cscfga__Parent_Configuration__c = null;
        
        insert ipvpnConf;
        
        cscfga__Product_Definition__c ipvpnPackageDef = CS_DataTest.createProductPackageDefinition('IPVPN package');
        ipvpnDef.RecordTypeId = packageDefinitionRecordType;
        ipvpnPackageDef.Product_Type__c = 'Fixed';
        insert ipvpnPackageDef;
        
        cscfga__Product_Configuration__c ipvpnPackageConf = CS_DataTest.createProductConfiguration(ipvpnPackageDef.Id, 'IPVPN package',basket.Id);
        insert ipvpnPackageConf;
        
        cscfga__Attribute_Definition__c accessPackageAttributeDef = new cscfga__Attribute_Definition__c();
        accessPackageAttributeDef.cscfga__Type__c = 'Package Slot';
        accessPackageAttributeDef.cscfga__Data_Type__c = 'String';
        accessPackageAttributeDef.Name = 'Access Infrastructure';
        accessPackageAttributeDef.cscfga__Product_Definition__c = ipvpnPackageDef.Id;
        insert accessPackageAttributeDef;
        
        cscfga__Attribute__c accessPackageAttribute = new cscfga__Attribute__c();
        accessPackageAttribute.Name = 'Access Infrastructure';
        accessPackageAttribute.cscfga__Product_Configuration__c = ipvpnPackageConf.Id;
        insert accessPackageAttribute;
        
        cscfga__Product_Configuration__c accessInfraConf = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        accessInfraConf.Site_Name__c = 'ESP 130';
        accessInfraConf.cscfga__Root_Configuration__c = null;
        accessInfraConf.cscfga__Parent_Configuration__c = null;
        accessInfraConf.cscfga__package_slot__c = accessPackageAttribute.Id;
        
        insert accessInfraConf;
        
        cscfga__Product_Configuration__c accessInfraConf2 = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        accessInfraConf2.cscfga__Root_Configuration__c = null;
        accessInfraConf2.cscfga__Parent_Configuration__c = null;
        accessInfraConf2.cscfga__package_slot__c = accessPackageAttribute.Id;
        accessInfraConf2.cscfga__package_slot__c = accessPackageAttribute.Id;
        insert accessInfraConf2;
        
        
       
        csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(ipvpnPackageConf);
        pcr.csbb__Product_Configuration__c = ipvpnPackageConf.Id;
        insert pcr;
        
        
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('Access Type', 'EthernetOverFiber');
        searchFields.put('Quality of Service', 'Yes');
        searchFields.put('Available bandwidth up', '20480.0');
        searchFields.put('Available bandwidth down', '20480.0');
        searchFields.put('Num WANLAN', '2');
        searchFields.put('Num WAN', '1');
        searchFields.put('Num LAN', '1');
        searchFields.put('CPE SLA', 'OfficeTTR2BD');
        searchFields.put('Contract Duration', '36');
        searchFields.put('Technology type', 'ISDN30');
        searchFields.put('Codec', 'G711');
        searchFields.put('SIP Channels', '4');
        searchFields.put('Wireless backup', 'Yes');
        searchFields.put('Overbooking Type', 'IAD');
        searchFields.put('Num SFP','4');
        
        
        
        CS_CPEPriceItemLookup caLookup = new CS_CPEPriceItemLookup();
        caLookup.doLookupSearch(searchFields, String.valueOf(accessInfraDef.Id),null, 0, 0);
     
         searchFields.put('Overbooking Type', 'NOT CUBE');
         caLookup.doLookupSearch(searchFields, String.valueOf(accessInfraDef.Id),null, 0, 0);
     
        searchFields.put('Quality of Service', 'No');
         caLookup.doLookupSearch(searchFields, String.valueOf(accessInfraDef.Id),null, 0, 0);
     
     }
  }

}