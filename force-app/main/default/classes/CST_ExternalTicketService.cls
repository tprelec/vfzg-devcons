/**
 * @description Manages External Tickets in Salesforce
 */
@RestResource(urlMapping='/cst/external-tickets/*')
global with sharing class CST_ExternalTicketService {
	public static final String CST_NOTIFICATION_EXTERNAL_TICKET = 'CST_Notification_External_Ticket';
	public static final String CST_TASK_RECORD_TYPE = 'CST_New_Task';
	private static final Integer TASK_DUE_DATE_DAY_OFFSET = 2;

	public static Set<String> allowedExternalTicketStatuses = new Set<String>{
		'Assigned',
		'In Progress',
		'Pending',
		'Resolved',
		'Cancelled',
		'Closed'
	};

	public class ExternalTicketRequest {
		public String incidentId { get; set; }
		public String status { get; set; }
		public String assignee { get; set; }
		public String priority { get; set; }
	}

	/**
	 * @description Updates External Ticket data and propagates changes to the related Case record,
	 * depending on the new status creates Task or notification for the Case Advisor.
	 */
	@HttpPatch
	global static void doPatch() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		res.addHeader('Content-Type', 'application/json');

		String reqBody = req.requestBody.toString();
		ExternalTicketRequest externalTicketReq = (ExternalTicketRequest) System.JSON.deserialize(reqBody, ExternalTicketRequest.class);

		if (!allowedExternalTicketStatuses.contains(externalTicketReq.status)) {
			String errorMsg =
				'Invalid status transition status=' +
				externalTicketReq.status +
				'. Allowed statuses: ' +
				String.join(new List<String>(allowedExternalTicketStatuses), ', ');
			LoggerService.log(errorMsg);

			res.responseBody = Blob.valueOf(errorMsg);
			res.statusCode = 409;

			return;
		}

		List<CST_External_Ticket__c> externalTickets = [
			SELECT Id, CST_Status__c, CST_Case__c, CST_IncidentId__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			WHERE CST_IncidentId__c = :externalTicketReq.incidentId
			LIMIT 1
		];

		if (externalTickets.size() == 0) {
			String errorMsg = 'External Ticket with incidentId=' + externalTicketReq.incidentId + ' not found';
			LoggerService.log(errorMsg);

			res.responseBody = Blob.valueOf(errorMsg);
			res.statusCode = 404;

			return;
		}

		CST_External_Ticket__c externalTicket = externalTickets[0];

		Case cstCase = [
			SELECT Id, CaseNumber, Status, CST_Sub_Status__c, CST_End2End_Owner__c, Reason, OwnerId, CST_Case_is_with_Team__c
			FROM Case
			WHERE Id = :externalTicket.CST_Case__c
		];

		if (cstCase.status == 'Closed' || cstCase.Status == 'Cancelled') {
			String errorMsg = 'Not allowed to update external tickets related to closed or cancelled cases';
			LoggerService.log(errorMsg);

			res.responseBody = Blob.valueOf(errorMsg);
			res.statusCode = 409;

			return;
		}

		String oldExternalTicketStatus = externalTicket.CST_Status__c;
		updateExternalTicket(externalTicket, externalTicketReq);

		updateCSTCase(cstCase, externalTicket, oldExternalTicketStatus);

		res.statusCode = 200;
	}

	private static void updateExternalTicket(CST_External_Ticket__c externalTicket, ExternalTicketRequest externalTicketReq) {
		externalTicket.CST_Status__c = externalTicketReq.status;
		externalTicket.CST_Assignee__c = externalTicketReq.assignee;
		externalTicket.CST_Ticket_Priority__c = externalTicketReq.priority;
		if (externalTicket.CST_Status__c == 'Pending') {
			externalTicket.CST_Status_Reason__c = 'Client Action Required';
		}

		update externalTicket;
	}

	private static void updateCSTCase(Case cstCase, CST_External_Ticket__c externalTicket, String oldExternalTicketStatus) {
		Case caseToUpdate = null;

		if (oldExternalTicketStatus != externalTicket.CST_Status__c) {
			switch on externalTicket.CST_Status__c {
				when 'Assigned' {
					sendNotificationToAdvisor(cstCase, externalTicket, true);
				}
				when 'In Progress' {
					cstCase.Status = 'In Progress';
					cstCase.CST_Sub_Status__c = 'Working on it';
					caseToUpdate = cstCase;

					sendNotificationToAdvisor(cstCase, externalTicket, true);
				}
				when 'Pending' {
					cstCase.Status = 'In Progress';
					cstCase.CST_Sub_Status__c = 'Working on it';
					cstCase.CST_Case_Is_With_Team__c = 'Internal - Advisor';
					cstCase.OwnerId = cstCase.CST_End2End_Owner__c;
					caseToUpdate = cstCase;

					createTaskForAdvisor(cstCase, externalTicket);
				}
				when 'Resolved' {
					cstCase.Status = 'Resolved';
					cstCase.CST_Sub_Status__c = '';
					cstCase.CST_Case_Is_With_Team__c = '';
					cstCase.ownerId = cstCase.CST_End2End_Owner__c;
					caseToUpdate = cstCase;
				}
				when 'Cancelled' {
					cstCase.Status = 'In Progress';
					cstCase.CST_Sub_Status__c = 'Working on it';
					cstCase.CST_Case_Is_With_Team__c = 'Internal - Advisor';
					cstCase.OwnerId = cstCase.CST_End2End_Owner__c;
					caseToUpdate = cstCase;

					createTaskForAdvisor(cstCase, externalTicket);
				}
				when 'Closed' {
					createTaskForAdvisor(cstCase, externalTicket);
				}
			}
		} else {
			sendNotificationToAdvisor(cstCase, externalTicket, false);
		}

		if (caseToUpdate != null) {
			update caseToUpdate;
		}
	}

	private static void sendNotificationToAdvisor(Case cstCase, CST_External_Ticket__c externalTicket, Boolean includeStatus) {
		CustomNotificationType notificationType = [
			SELECT Id, DeveloperName
			FROM CustomNotificationType
			WHERE DeveloperName = :CST_NOTIFICATION_EXTERNAL_TICKET
		];

		String msgTitle = 'Remedy Ticket updated' + (includeStatus ? ' to ' + externalTicket.CST_Status__c : '');
		String msgBody =
			'Incident ID: ' +
			externalTicket.CST_IncidentId__c +
			'\nCase Number: ' +
			cstCase.CaseNumber +
			(includeStatus ? '\nStatus: ' + externalTicket.CST_Status__c : '');

		Messaging.CustomNotification notification = new Messaging.CustomNotification();
		notification.setTitle(msgTitle);
		notification.setBody(msgBody);
		notification.setNotificationTypeId(notificationType.Id);
		notification.setTargetId(cstCase.Id);

		Set<String> addressee = new Set<String>{ cstCase.CST_End2End_Owner__c };
		notification.send(addressee);

		LoggerService.log('Sent notification for externalTicket.Id=' + externalTicket.Id);
	}

	private static void createTaskForAdvisor(Case cstCase, CST_External_Ticket__c externalTicket) {
		RecordType taskRecordType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = :CST_TASK_RECORD_TYPE LIMIT 1];

		Task newTask = new Task();
		newTask.Subject = 'Remedy Ticket updated to ' + externalTicket.CST_Status__c;
		newTask.OwnerId = cstCase.CST_End2End_Owner__c;
		newTask.WhatId = cstCase.Id;
		newTask.Status = 'Open';
		newTask.Type = 'Other';
		newTask.RecordTypeId = taskRecordType.Id;
		newTask.IsReminderSet = true;
		newTask.ReminderDateTime = System.now();
		newTask.ActivityDate = getTaskDueDate();

		insert newTask;
	}

	/**
	 * @return next working day wrt Business Hours
	 */
	private static Date getTaskDueDate() {
		BusinessHours bh = [SELECT Id, Name, IsDefault, IsActive FROM BusinessHours WHERE IsDefault = TRUE LIMIT 1];

		Date newDate = System.today().addDays(TASK_DUE_DATE_DAY_OFFSET);
		DateTime dt = DateTime.newInstance(newDate.year(), newDate.month(), newDate.day(), 0, 0, 0);
		dt = BusinessHours.nextStartDate(bh.Id, dt);

		return dt.date();
	}
}
