/**
 * @description         This is the trigger handler for the cspmb__Price_Item__c sObject.
 * @author              Guy Clairbois
 */
public with sharing class PriceItemTriggerhandler extends TriggerHandler {
	/**
	 * @description         This handles the before insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void BeforeInsert() {
		List<cspmb__Price_Item__c> newPriceitems = (List<cspmb__Price_Item__c>) this.newList;
		updateProductLink(newPriceitems, null);
		updateLookupFields(newPriceitems, null);
		updatePriceItemFields(newPriceitems);
		//setIsActive(newPriceItems);
		setExternalIds(newPriceItems);
	}

	/**
	 * @description         This handles the after insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void AfterInsert() {
		List<cspmb__Price_Item__c> newPriceitems = (List<cspmb__Price_Item__c>) this.newList;
	}

	/**
	 * @description         This handles the before update trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void BeforeUpdate() {
		List<cspmb__Price_Item__c> newPriceitems = (List<cspmb__Price_Item__c>) this.newList;
		Map<Id, cspmb__Price_Item__c> oldPriceitemsMap = (Map<Id, cspmb__Price_Item__c>) this.oldMap;

		updateProductLink(newPriceitems, oldPriceitemsMap);
		updateLookupFields(newPriceitems, oldPriceitemsMap);
		updatePriceItemFields(newPriceitems);
		//setIsActive(newPriceItems);
	}

	/**
	 * @description         This handles the after update trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void AfterUpdate() {
		List<cspmb__Price_Item__c> newPriceitems = (List<cspmb__Price_Item__c>) this.newList;
		Map<Id, cspmb__Price_Item__c> oldPriceitemsMap = (Map<Id, cspmb__Price_Item__c>) this.oldMap;
	}

	/**
	 * @description         This method updates the prduct2 lookup fields by taking the productcodes from the external id fields
	 */
	private void updateProductLink(List<cspmb__Price_Item__c> newPriceitems, Map<Id, cspmb__Price_Item__c> oldPriceitemsMap) {
		// check if there are any priceitems without a product__c but with cspmb__One_Off_Charge_External_Id__c or cspmb__Recurring_Charge_External_Id__c
		// if so, store the id's for lookup
		Map<String, Id> productCodeToId = new Map<String, Id>();
		for (cspmb__Price_Item__c pi : newPriceitems) {
			if (oldPriceitemsMap == null) {
				// insert
				if (pi.cspmb__One_Off_Charge_External_Id__c != null || pi.cspmb__Recurring_Charge_External_Id__c != null) {
					productCodeToId.put(pi.cspmb__One_Off_Charge_External_Id__c, null);
					productCodeToId.put(pi.cspmb__Recurring_Charge_External_Id__c, null);
				}
			} else {
				// update
				if (
					pi.cspmb__One_Off_Charge_External_Id__c != oldPriceitemsMap.get(pi.Id).cspmb__One_Off_Charge_External_Id__c ||
					pi.cspmb__Recurring_Charge_External_Id__c != oldPriceitemsMap.get(pi.Id).cspmb__Recurring_Charge_External_Id__c
				) {
					productCodeToId.put(pi.cspmb__One_Off_Charge_External_Id__c, null);
					productCodeToId.put(pi.cspmb__Recurring_Charge_External_Id__c, null);
				}
			}
		}
		if (!productCodeToId.isEmpty()) {
			for (Product2 p : [
				SELECT Id, ProductCode
				FROM Product2
				WHERE ProductCode IN :productCodeToId.keySet() AND CLC__c = NULL AND IsActive = TRUE
			]) {
				productCodeToId.put(p.ProductCode, p.Id);
			}
			system.debug(productCodeToId);

			for (cspmb__Price_Item__c pi : newPriceitems) {
				if (!pi.cspmb__Is_Active__c) {
					continue;
				}
				if (pi.cspmb__One_Off_Charge_External_Id__c != null && pi.cspmb__One_Off_Charge_External_Id__c != '') {
					if (
						productCodeToId.containsKey(pi.cspmb__One_Off_Charge_External_Id__c) &&
						productCodeToId.get(pi.cspmb__One_Off_Charge_External_Id__c) != null
					) {
						pi.One_Off_Charge_Product__c = productCodeToId.get(pi.cspmb__One_Off_Charge_External_Id__c);
					} else {
						pi.cspmb__One_Off_Charge_External_Id__c.addError('Productcode does not exist or product is not active.');
					}
				} else {
					pi.One_Off_Charge_Product__c = null;
				}
				if (pi.cspmb__Recurring_Charge_External_Id__c != null && pi.cspmb__Recurring_Charge_External_Id__c != '') {
					if (
						productCodeToId.containsKey(pi.cspmb__Recurring_Charge_External_Id__c) &&
						productCodeToId.get(pi.cspmb__Recurring_Charge_External_Id__c) != null
					) {
						pi.Recurring_Charge_Product__c = productCodeToId.get(pi.cspmb__Recurring_Charge_External_Id__c);
					} else {
						pi.cspmb__Recurring_Charge_External_Id__c.addError('Productcode does not exist or product is not active.');
					}
				} else {
					pi.Recurring_Charge_Product__c = null;
				}
			}
		}
	}

	/**
	 * @description         This method updates the category and vendor lookup fields by taking the names from the text fields
	 */
	private void updateLookupFields(List<cspmb__Price_Item__c> newPriceitems, Map<Id, cspmb__Price_Item__c> oldPriceitemsMap) {
		Map<String, Id> categoryNameToId = new Map<String, Id>();
		Map<String, Id> vendorNameToId = new Map<String, Id>();

		for (cspmb__Price_Item__c pi : newPriceitems) {
			if (oldPriceitemsMap == null) {
				// insert
				if (pi.Category__c != null) {
					categoryNameToId.put(pi.Category__c, null);
				}
				if (pi.Vendor__c != null) {
					vendorNameToId.put(pi.Vendor__c, null);
				}
			} else {
				// update
				if (pi.Category__c != oldPriceitemsMap.get(pi.Id).Category__c) {
					categoryNameToId.put(pi.Category__c, null);
				}
				if (pi.Vendor__c != oldPriceitemsMap.get(pi.Id).Vendor__c) {
					vendorNameToId.put(pi.Vendor__c, null);
				}
			}
		}
		system.debug(categoryNameToId);
		system.debug(vendorNameToId);
		if (!categoryNameToId.isEmpty()) {
			for (Category__c c : [SELECT Id, Name FROM Category__c WHERE Name IN :categoryNameToId.keySet()]) {
				categoryNameToId.put(c.Name, c.Id);
			}
			for (cspmb__Price_Item__c pi : newPriceitems) {
				if (pi.Category__c != null && pi.Category__c != '') {
					if (categoryNameToId.get(pi.Category__c) != null) {
						pi.Category_Lookup__c = categoryNameToId.get(pi.Category__c);
					} else {
						pi.Category__c.addError(
							'Category ' +
							pi.Category__c +
							' not found in Category table. Please look up the correct category or contact your adminstrator to have it created.'
						);
					}
				} else {
					pi.Category_Lookup__c = null;
				}
			}
		}
		if (!vendorNameToId.isEmpty()) {
			for (Vendor__c c : [SELECT Id, Name FROM Vendor__c WHERE Name IN :vendorNameToId.keySet()]) {
				vendorNameToId.put(c.Name, c.Id);
			}
			for (cspmb__Price_Item__c pi : newPriceitems) {
				if (pi.Vendor__c != null && pi.Vendor__c != '') {
					system.debug('1');
					if (vendorNameToId.get(pi.Vendor__c) != null) {
						system.debug('2');
						pi.Vendor_lookup__c = vendorNameToId.get(pi.Vendor__c);
					} else {
						pi.Vendor__c.addError(
							'Vendor ' +
							pi.Vendor__c +
							' not found in Vendor table. Please look up the correct category or contact your adminstrator to have it created.'
						);
					}
				} else {
					system.debug('3');
					pi.Vendor_lookup__c = null;
				}
			}
		}
	}

	/**
	 * @description         This updates Price Item fields.
	 */
	private void updatePriceItemFields(List<cspmb__Price_Item__c> newPriceitems) {
		for (cspmb__Price_Item__c pi : newPriceitems) {
			if (pi.Access_Type_Multi__c != null) {
				String access = pi.Access_Type_Multi__c;
				if (access.length() > 255) {
					pi.Access_Type_Multi__c.addError('Selection is limited to 255 characters.');
				} else {
					pi.Access_Type_Text__c = ',' + access.replace(';', ',') + ',';
				}
			} else {
				pi.Access_Type_Text__c = '';
			}
			if (pi.Deal_Type_Multi__c != null) {
				String deal = pi.Deal_Type_Multi__c;
				if (deal.length() > 255) {
					pi.Deal_Type_Multi__c.addError('Selection is limited to 255 characters.');
				} else {
					pi.Deal_Type_Text__c = ',' + deal.replace(';', ',') + ',';
				}
			} else {
				pi.Deal_Type_Text__c = '';
			}
			if (pi.Bundle_Services__c != null) {
				String bserv = pi.Bundle_Services__c;
				if (bserv.length() > 255) {
					pi.Bundle_Services__c.addError('Selection is limited to 255 characters.');
				} else {
					pi.Bundle_Services_Text__c = ',' + bserv.replace(';', ',') + ',';
				}
			} else {
				pi.Bundle_Services_Text__c = '';
			}
			if (pi.Mobile_Scenario__c != null) {
				String mscen = pi.Mobile_Scenario__c;
				if (mscen.length() > 255) {
					pi.Mobile_Scenario__c.addError('Selection is limited to 255 characters.');
				} else {
					pi.Mobile_Scenario_Text__c = ',' + mscen.replace(';', ',') + ',';
				}
			} else {
				pi.Mobile_Scenario_Text__c = '';
			}
			if (pi.Mobile_Subscription__c != null) {
				String msub = pi.Mobile_Subscription__c;
				if (msub.length() > 255) {
					pi.Mobile_Subscription__c.addError('Selection is limited to 255 characters.');
				} else {
					pi.Mobile_Subscription_Text__c = ',' + msub.replace(';', ',') + ',';
				}
			} else {
				pi.Mobile_Subscription_Text__c = '';
			}
		}
	}

	/*
	private void setIsActive(List<cspmb__Price_Item__c> newPriceItems) {
		for (cspmb__Price_Item__c pi : newPriceitems) {
			if (
				Date.today() >= pi.cspmb__Effective_Start_Date__c &&
				Date.today() < pi.cspmb__Effective_End_Date__c
			) {
				pi.cspmb__Is_Active__c = true;
			} else if (
				Date.today() >= pi.cspmb__Effective_Start_Date__c &&
				pi.cspmb__Effective_End_Date__c == null
			) {
				pi.cspmb__Is_Active__c = true;
			} else {
				pi.cspmb__Is_Active__c = false;
			}
		}
	}
	*/

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 * @param List<cspmb__Price_Item__c> lstPriceItems
	 **/
	private void setExternalIds(List<cspmb__Price_Item__c> lstPriceItems) {
		Sequence objSequence = new Sequence('Commercial Product');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (cspmb__Price_Item__c objPriceItem : lstPriceItems) {
				objPriceItem.External_ID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
