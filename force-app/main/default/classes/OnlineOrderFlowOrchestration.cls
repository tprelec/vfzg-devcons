public with sharing class OnlineOrderFlowOrchestration {
	@testVisible
	private static CustomOrderData data;
	@testVisible
	private static String accId;
	@testVisible
	private static String pbId;
	@testVisible
	private static cscfga__Product_Basket__c basket;

	@SuppressWarnings('PMD.EmptyStatementBlock')
	public OnlineOrderFlowOrchestration() {
	}

	public OnlineOrderFlowOrchestration(String jsonStr, String accountId, String productBasketId) {
		data = (CustomOrderData) JSON.deserialize(jsonStr, CustomOrderData.class);
		if (accountId == null) {
			accId = getAccountFromPurchaseContact(data.customer.identification.purchaseContactId);
		} else {
			accId = accountId;
		}

		pbId = productBasketId;
	}

	// If the account id is not populated on the poduct basket we can fetch it using the purchase contact
	@testVisible
	private static Id getAccountFromPurchaseContact(Id purchaseContactId) {
		return [SELECT AccountId FROM Contact WHERE Id = :purchaseContactId].AccountId;
	}

	@testVisible
	private static List<Site__c> sites = new List<Site__c>();
	public void handleSites() {
		List<Map<String, String>> siteData = prepareSiteData(data.locations);
		// Get and add the existing sites
		sites.addAll(fetchExistingSites(siteData));
		// Create sites for address information that couldn't be matched to an existing site on that account
		List<Map<String, String>> nonExistingSites = new List<Map<String, String>>();
		// If no sites were matched/feched we need to create them all
		if (sites.isEmpty()) {
			nonExistingSites = siteData;
		} else {
			for (Map<String, String> sd : siteData) {
				Boolean match = false;
				// Check if we have already fetched a site
				for (Site__c s : sites) {
					if (
						s.Site_Postal_Code__c == sd.get(Constants.POSTCODE) &&
						String.valueOf(s.Site_House_Number__c) == sd.get(Constants.HOUSENUMBER) &&
						(String.isBlank(sd.get(Constants.HOUSENRSUFFIX)) ||
						s.Site_House_Number_Suffix__c == sd.get(Constants.HOUSENRSUFFIX))
					) {
						match = true;
					}
				}
				// If we matched a site we don't need to add it to the list to be created
				if (!match) {
					nonExistingSites.add(sd);
				}
			}
		}

		// Check if sites need to be created
		if (!nonExistingSites.isEmpty()) {
			// Call the queable method to create sites
			CreateSitesQueueable csq = new CreateSitesQueueable(nonExistingSites, accId, pbId);
			System.enqueueJob(csq);
		} else {
			// If no sites need to be created continue the process
			createObjects();
		}
	}

	// Helper method to prepare site data for service
	@testVisible
	private static List<Map<String, String>> prepareSiteData(List<Location> locations) {
		List<Map<String, String>> sitesData = new List<Map<String, String>>();

		for (Location l : locations) {
			Map<String, String> sd = new Map<String, String>();
			sd.put(Constants.POSTCODE, l.address.postcode);
			sd.put(Constants.HOUSENUMBER, l.address.houseNumber);
			sd.put(Constants.HOUSENRSUFFIX, l.address.houseNumberAddition);

			sitesData.add(sd);
		}
		return sitesData;
	}

	// Get the existing sites based on the address information provided by commerce cloud
	@testVisible
	private static List<Site__c> fetchExistingSites(List<Map<String, String>> siteData) {
		List<Site__c> sites = new List<Site__c>();

		if (!siteData.isEmpty()) {
			List<Site__c> accSites = [
				SELECT
					Id,
					Site_Street__c,
					Site_Postal_Code__c,
					Site_House_Number__c,
					Site_House_Number_Suffix__c,
					Site_Account__c
				FROM Site__c
				WHERE Site_Account__c = :accId
			];
			for (Site__c s : accSites) {
				for (Map<String, String> sd : siteData) {
					if (
						s.Site_Postal_Code__c == sd.get(Constants.POSTCODE) &&
						s.Site_House_Number__c == Decimal.valueOf(sd.get(Constants.HOUSENUMBER)) &&
						(String.isBlank(sd.get(Constants.HOUSENRSUFFIX)) ||
						s.Site_House_Number_Suffix__c == sd.get(Constants.HOUSENRSUFFIX))
					) {
						sites.add(s);
					}
				}
			}
		}
		return sites;
	}

	// Point of entry after we create sites
	public void fetchSitesAndCreateObjects() {
		sites.addAll(fetchExistingSites(prepareSiteData(data.locations)));
		createObjects();
	}

	@testVisible
	private static Account acc;
	@testVisible
	private static void createObjects() {
		if (String.isNotBlank(accId)) {
			acc = [
				SELECT
					Id,
					Name,
					Frame_Work_Agreement__c,
					Create_Framework_Agreement__c,
					Visiting_City__c,
					Visiting_Housenumber1__c,
					Visiting_Housenumber_Suffix__c,
					Visiting_Postal_Code__c,
					Visiting_street__c,
					Authorized_to_sign_1st__c
				FROM Account
				WHERE Id = :accId
			];

			basket = [
				SELECT
					Id,
					CustomData__c,
					cscfga__Opportunity__r.Direct_Indirect__c,
					Sign_on_behalf_of_customer__c,
					Contract_Language__c,
					cscfga__Opportunity__c,
					Name
				FROM cscfga__Product_Basket__c
				WHERE Id = :pbId
			];

			List<Contact> techContacts = createTechnicalContacts();

			// Sort corresponding site with technical contact and update the ids in CustomData JSON
			mapSitesAndContacts(sites, techContacts);

			createBan();
			createFinancialAccount();
			createBillingArrangment();

			if (String.isBlank(acc.Frame_Work_Agreement__c)) {
				AccountGenerateFrameworkButtonController.generateFrameworkForVodafoneAccount(
					acc.Id
				);
			}

			createOpporutnity();
			connectProductBasketWithOpportunity();

			contractAtt = createOpporutnityAttachment('Contract (Signed)');
			copyKvkAtt = createOpporutnityAttachment('Copy KvK');
			numberlistAtt = createOpporutnityAttachment('Number List');

			syncOppAndCreateContract(pbId);
		}
	}

	private static Map<String, Contact> contactMap;
	// Get existing contacts
	@testVisible
	private static Map<String, Contact> getExistingContacts() {
		contactMap = new Map<String, Contact>();
		for (Contact c : [
			SELECT Id, MiddleName, LastName, Email, Phone, Comments_on_Contact__c
			FROM Contact
			WHERE AccountId = :accId
		]) {
			contactMap.put(c.Email, c);
		}
		return contactMap;
	}

	// Create technical contacts
	@testVisible
	private static List<Contact> createTechnicalContacts() {
		getExistingContacts();
		List<Contact> techContacts = new List<Contact>();
		for (Location loc : data.locations) {
			if (!contactMap.containsKey(loc.contact.email)) {
				Contact tc = new Contact(
					AccountId = accId,
					MiddleName = loc.contact.middleName,
					LastName = loc.contact.lastName,
					Email = loc.contact.email,
					Phone = loc.contact.phone,
					Comments_on_Contact__c = loc.contact.comment
				);
				techContacts.add(tc);
			} else {
				techContacts.add(contactMap.get(loc.contact.email));
			}
		}
		upsert techContacts;
		return techContacts;
	}

	// Sort corresponding site with technical contact and update the ids in CustomData JSON
	@testVisible
	private static void mapSitesAndContacts(List<Site__c> sites, List<Contact> techContacts) {
		// Holds the postcode+housenumber+ext and id pair
		Map<String, String> siteMap = new Map<String, String>();
		for (Site__c s : sites) {
			String key = generateKey(
				s.Site_Postal_Code__c,
				String.valueOf(s.Site_House_Number__c),
				s.Site_House_Number_Suffix__c
			);
			siteMap.put(key, s.Id);
		}
		// Holds the email and id pair
		Map<String, String> contactMap = new Map<String, String>();
		for (Contact c : techContacts) {
			contactMap.put(c.Email, c.Id);
		}

		for (Location loc : data.locations) {
			String key = generateKey(
				loc.address.postcode,
				loc.address.houseNumber,
				loc.address.houseNumberAddition
			);
			loc.address.id = siteMap.get(key);
			loc.contact.id = contactMap.get(loc.contact.email);
		}

		system.debug('*** veka siteMap ' + siteMap);
		system.debug('*** veka contactMap ' + contactMap);
		system.debug('*** veka data ' + data);
		system.debug('*** veka data.locations ' + data.locations);

		basket.CustomData__c = JSON.serialize(data);
	}

	private static String generateKey(String postCode, String houseNr, String ext) {
		String key = '';
		key += String.isNotBlank(postCode) ? postCode : '';
		key += String.isNotBlank(houseNr) ? houseNr : '';
		key += String.isNotBlank(ext) ? ext : '';
		return key;
	}

	@future(callout=true)
	private static void syncOppAndCreateContract(String pbId) {
		Map<Id, CCAQDProcessor.CCAQDStructure> pcData = CCAQDProcessor.extractPCAqdData(
			new List<Id>{ pbId }
		);

		CustomButtonSynchronizeWithOpportunity.syncWithOpportunityOnlineScenario(pbId, pcData);
		generateClmContract(pbId);
	}

	@testVisible
	private static void generateClmContract(String basketId) {
		cscfga__Product_Basket__c pBasket = [
			SELECT
				Id,
				cscfga__Opportunity__r.Direct_Indirect__c,
				Sign_on_behalf_of_customer__c,
				Contract_Language__c,
				cscfga__Opportunity__c,
				Name
			FROM cscfga__Product_Basket__c
			WHERE Id = :basketId
		];

		csclm__Agreement__c agreement = ContractRelatedQuestionsController.createBPAIfNeeded(
			pBasket
		);

		String template = 'Indirect';
		if (pBasket.cscfga__Opportunity__r.Direct_Indirect__c == 'Direct') {
			template = 'Direct 2';
		}

		csclm__Agreement__c agreement2 = ContractRelatedQuestionsController.createAgreement2(
			pBasket,
			template
		);
	}

	private static Opportunity opp;
	// Create Opportunity
	@testVisible
	private static Opportunity createOpporutnity() {
		Account acc = [SELECT Id, Name FROM Account WHERE Id = :accId];
		COM_One_Mobile_Online_Channel_Setting__mdt settings = [
			SELECT Online_Chanel_Opportunity_Owner__c
			FROM COM_One_Mobile_Online_Channel_Setting__mdt
			WHERE DeveloperName = 'SFCC_Integration_User'
			LIMIT 1
		];
		User b2bDefaultOwner = [
			SELECT Id, Name
			FROM User
			WHERE Username = :settings.Online_Chanel_Opportunity_Owner__c
			LIMIT 1
		];
		Id defaultRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName()
			.get('Default')
			.getRecordTypeId();

		opp = new Opportunity(
			AccountId = accId,
			RecordTypeId = defaultRecordTypeId,
			Name = acc.Name + ' Acq + Proposition',
			CloseDate = Date.today(),
			Type_of_Service__c = 'Mobile',
			StageName = 'Closing',
			Type = 'New Business',
			Deal_Type__c = 'Acquisition',
			OwnerId = b2bDefaultOwner.Id,
			BAN__c = ban.Id,
			Document_Type__c = data.customer.identification.identificationType,
			Document_Number__c = data.customer.identification.identificationNumber
		);

		insert opp;
		return opp;
	}

	// Connect the product basket with the created opportunity
	@testVisible
	private static void connectProductBasketWithOpportunity() {
		basket.cscfga__Opportunity__c = opp.Id;
		basket.Sign_on_behalf_of_customer__c = 'first';
		basket.Contract_Language__c = 'Dutch';
		update basket;
	}

	private static Opportunity_Attachment__c contractAtt;
	private static Opportunity_Attachment__c copyKvkAtt;
	private static Opportunity_Attachment__c numberlistAtt;
	// Create Opportunity Attachment
	@testVisible
	private static Opportunity_Attachment__c createOpporutnityAttachment(String attType) {
		Opportunity_Attachment__c oppAtt = new Opportunity_Attachment__c(
			Attachment_Type__c = attType,
			Opportunity__c = opp.Id
		);

		insert oppAtt;
		return oppAtt;
	}

	private static Ban__c ban;
	// Create BAN
	@testVisible
	private static Ban__c createBan() {
		// Create correct ban name preserving the format of system now and adjusting the time zone difference
		String banName = 'Requested New Unify BAN ';
		banName += String.valueOf(Date.Today().Year());
		banName += '-';
		if (Date.Today().Month() < 10) {
			banName += '0';
		}
		banName += String.valueOf(Date.Today().Month());
		banName += '-';
		if (Date.Today().Day() < 10) {
			banName += '0';
		}
		banName += String.valueOf(Date.Today().Day());
		banName += ' ';
		Datetime dt = Datetime.now();
		if (dt.Hour() < 10) {
			banName += '0';
		}
		banName += String.valueOf(dt.Hour());
		banName += ':';
		if (dt.Minute() < 10) {
			banName += '0';
		}
		banName += String.valueOf(dt.Minute());
		banName += ':';
		if (dt.Second() < 10) {
			banName += '0';
		}
		banName += String.valueOf(dt.Second());

		ban = new Ban__c(
			Account__c = accId,
			Name = banName,
			BAN_Status__c = 'Requested',
			Unify_Customer_Type__c = 'B',
			Unify_Customer_SubType__c = 'BU'
		);

		insert ban;
		return ban;
	}

	private static Financial_Account__c finAcc;
	// Create Financial Account
	@testVisible
	private static Financial_Account__c createFinancialAccount() {
		finAcc = new Financial_Account__c(
			BAN__c = ban.Id,
			Bank_Account_Number__c = data.order.payment.IBAN,
			Payment_method__c = data.order.payment.paymentMethod,
			Financial_Contact__c = acc.Authorized_to_sign_1st__c
		);

		insert finAcc;
		return finAcc;
	}

	private static Billing_Arrangement__c billingArr;
	// Create Billing Arrangement
	@testVisible
	private static Billing_Arrangement__c createBillingArrangment() {
		billingArr = new Billing_Arrangement__c(
			Financial_Account__c = finAcc.Id,
			Payment_method__c = data.order.payment.paymentMethod,
			Bank_Account_Number__c = data.order.payment.IBAN,
			Bank_Account_Name__c = acc.Name,
			Billing_Arrangement_Alias__c = StringUtils.truncate(acc.Name, 50),
			Bill_Format__c = 'EB',
			Billing_City__c = acc.Visiting_City__c,
			Billing_Housenumber__c = String.valueOf(acc.Visiting_Housenumber1__c),
			Billing_Housenumber_Suffix__c = acc.Visiting_Housenumber_Suffix__c,
			Billing_Postal_Code__c = acc.Visiting_Postal_Code__c,
			Billing_Street__c = acc.Visiting_street__c
		);

		insert billingArr;
		return billingArr;
	}

	// Classes to deserialize JSON
	public class CustomOrderData {
		public Order order;
		public Customer customer;
		public List<Location> locations;
	}

	public class Order {
		public Payment payment;
	}
	@SuppressWarnings('PMD.FieldNamingConventions')
	public class Payment {
		public String paymentMethod;
		public String IBAN;
	}

	public class Customer {
		public Identification identification;
	}

	public class Identification {
		public string purchaseContactId;
		public String nationality;
		public String identificationType;
		public String identificationNumber;
		public String identificationExpirationdate;
	}

	public class Location {
		public String id;
		public Address address;
		public TechnicalContact contact;
	}

	public class Address {
		public String id;
		public String street;
		public String houseNumber;
		public String houseNumberAddition;
		public String room;
		public String postcode;
		public String city;
	}
	public class TechnicalContact {
		public String id;
		public String initials;
		public String middleName;
		public String lastName;
		public String email;
		public String phone;
		public String comment;
	}
}