/**
 * @description:    BICC Dealer_Information__c builder
 * @testClass:      TestBICC_DealerInformationBuilder
 **/
public inherited sharing class BICC_DealerInformationBuilder extends BICC_DealerSObjectBuilderAbstract {
	private static final Integer DEALER_CODE_WITHOUT_PREFIX_LENGTH = 6;
	@TestVisible
	private static final String ERROR_MESSAGE_DEALER_CONTACT_NOT_FOUND = 'No matching Contact found for Email = {0}';
	@TestVisible
	private static final String ERROR_MESSAGE_EXPIRED_STATUS_NOT_IMPLEMENTED = 'Expired status dealers need to be handled manually';
	@TestVisible
	private static final String ERROR_MESSAGE_UPDATE_OF_EXISTING_DEALER_NOT_IMPLEMENTED = 'Update of existing dealer needs to be handled manually';
	@TestVisible
	private static final String ERROR_MESSAGE_NOT_ALLOWED_SALES_CHANNEL = 'Not allowed sales channel: {0}. Allowed sales channels are: {1}';

	@TestVisible
	private static final List<String> ALLOWED_SALES_CHANNELS = new List<String>{ Constants.DEALER_INFORMATION_SALES_CHANNEL_INDIRECT };

	/**
	 * @description:    Executes logic to build Dealer_Information__c
	 * @return          BICC_DealerSObjectBuilderAbstract - instance of this
	 **/
	public override BICC_DealerSObjectBuilderAbstract build() {
		List<String> dealerCodes = new List<String>();
		List<String> dealerContactEmails = new List<String>();

		for (BICC_Dealer_Information__c biccDealerInformation : (List<BICC_Dealer_Information__c>) this.biccSObjectRecords) {
			if (String.isNotBlank(biccDealerInformation?.Dealer_Code__c)) {
				String dealerCode = biccDealerInformation.Dealer_Code__c.subString(this.getDealerPrefixIndex(biccDealerInformation.Dealer_Code__c));

				dealerCodes.add(dealerCode);
			}

			if (String.isNotBlank(biccDealerInformation?.Dealer_Contact_Email__c)) {
				dealerContactEmails.add(biccDealerInformation.Dealer_Contact_Email__c);
			}
		}

		Map<String, Dealer_Information__c> existingDealerInformationByDealerCode = this.getExistingDealerInformationByDealerCode(dealerCodes);

		Map<String, Contact> existingDealerContactsByEmail = this.getExistingDealerContactsByEmail(dealerContactEmails);

		for (BICC_Dealer_Information__c biccDealerInformation : (List<BICC_Dealer_Information__c>) this.biccSObjectRecords) {
			Integer dealerCodePrefixIndex = this.getDealerPrefixIndex(biccDealerInformation.Dealer_Code__c);

			String biccDealerInformationDealerCode = biccDealerInformation.Dealer_Code__c.subString(dealerCodePrefixIndex);

			Contact dealerContact = existingDealerContactsByEmail.get(biccDealerInformation.Dealer_Contact_Email__c);

			Boolean hasExistingDealerInformation = existingDealerInformationByDealerCode.containsKey(biccDealerInformationDealerCode);

			Boolean dealerInformationHasErrors = this.checkForErrors(biccDealerInformation, dealerContact, hasExistingDealerInformation);

			if (!dealerInformationHasErrors) {
				Dealer_Information__c dealerInformation = (Dealer_Information__c) SyncUtil.populateFields(
					new Dealer_Information__c(),
					biccDealerInformation,
					this.biccFieldSyncMappingForDestinationField,
					this.sObjectFieldsPerFieldName,
					hasExistingDealerInformation
				);

				dealerInformation.Status__c = Constants.DEALER_INFORMATION_STATUS_ACTIVE;
				dealerInformation.Contact__c = dealerContact?.Id;
				dealerInformation.Dealer_Code__c = biccDealerInformationDealerCode;
				dealerInformation.Dealer_Code_Prefix__c = biccDealerInformation.Dealer_Code__c.subString(0, dealerCodePrefixIndex);

				if (biccDealerInformation?.Parent_Dealer_Code__c != null) {
					Integer parentDealerCodePrefixIndex = this.getDealerPrefixIndex(biccDealerInformation.Parent_Dealer_Code__c);

					String biccDealerInformationParentDealerCode = biccDealerInformation.Parent_Dealer_Code__c.subString(parentDealerCodePrefixIndex);

					dealerInformation.Parent_Dealer_Code__c = biccDealerInformationParentDealerCode;
					dealerInformation.Parent_Dealer_Code_Prefix__c = biccDealerInformation.Parent_Dealer_Code__c.subString(
						0,
						parentDealerCodePrefixIndex
					);
				}

				this.generatedRecordPerBICCSobjectId.put(biccDealerInformation.Id, dealerInformation);
			}

			biccDealerInformation.Processed__c = true;
		}

		return this;
	}

	/**
	 * @description:    Retrieves existing Dealer_Information__c records by dealer code
	 * @param           dealerCodes - list of dealer codes
	 * @return          Map<String, Dealer_Information__c> - map of existing Dealer_Information__c
	 * 					records by dealer code
	 **/
	private Map<String, Dealer_Information__c> getExistingDealerInformationByDealerCode(List<String> dealerCodes) {
		Map<String, Dealer_Information__c> existingDealerInformationByDealerCode = new Map<String, Dealer_Information__c>();

		List<Dealer_Information__c> existingDealerInformations = [
			SELECT Id, Contact__c, Dealer_Code__c, Parent_Dealer_Code__c, Status__c, Sales_Channel__c, Name
			FROM Dealer_Information__c
			WHERE Dealer_Code__c IN :dealerCodes
		];

		for (Dealer_Information__c existingDealerInformation : existingDealerInformations) {
			existingDealerInformationByDealerCode.put(existingDealerInformation.Dealer_Code__c, existingDealerInformation);
		}

		return existingDealerInformationByDealerCode;
	}

	/**
	 * @description:    Retrieves existing dealer Contacts by email
	 * @param           dealerContactEmails - list of dealer contact emails
	 * @return          Map<String, Contact> - map of existing dealer Contacts
	 * 					by email
	 **/
	private Map<String, Contact> getExistingDealerContactsByEmail(List<String> dealerContactEmails) {
		Map<String, Contact> existingDealerContactsByEmail = new Map<String, Contact>();

		List<Contact> existingContacts = [SELECT Id, Email, AccountId FROM Contact WHERE Email IN :dealerContactEmails];

		for (Contact existingContact : existingContacts) {
			if (String.isNotBlank(existingContact.Email)) {
				existingDealerContactsByEmail.put(existingContact.Email, existingContact);
			}
		}

		return existingDealerContactsByEmail;
	}

	/**
	 * @description:    Checks BICC Dealer Information record for errors
	 * @param           biccDealerInformation - BICC Dealer Information record
	 * @param           dealerContact - dealer ontact record
	 * @param           hasExistingDealerInformation - dealer Information record exists
	 * @return			Boolean - true if errors happened, false otherwise
	 **/
	private Boolean checkForErrors(BICC_Dealer_Information__c biccDealerInformation, Contact dealerContact, Boolean hasExistingDealerInformation) {
		Boolean hasErrors = false;

		if (dealerContact == null) {
			biccDealerInformation.Error__c = String.format(
				ERROR_MESSAGE_DEALER_CONTACT_NOT_FOUND,
				new List<String>{ biccDealerInformation.Dealer_Contact_Email__c }
			);
			hasErrors = true;
		} else if (hasExistingDealerInformation) {
			biccDealerInformation.Error__c = ERROR_MESSAGE_UPDATE_OF_EXISTING_DEALER_NOT_IMPLEMENTED;
			hasErrors = true;
		} else if (biccDealerInformation?.Status__c == Constants.BICC_DEALER_INFORMATION_STATUS_EXPIRED) {
			biccDealerInformation.Error__c = ERROR_MESSAGE_EXPIRED_STATUS_NOT_IMPLEMENTED;
			hasErrors = true;
		} else if (!ALLOWED_SALES_CHANNELS.contains(biccDealerInformation?.Sales_Channel__c)) {
			biccDealerInformation.Error__c = String.format(
				ERROR_MESSAGE_NOT_ALLOWED_SALES_CHANNEL,
				new List<String>{ biccDealerInformation?.Sales_Channel__c, String.join(ALLOWED_SALES_CHANNELS, ', ') }
			);
			hasErrors = true;
		}

		return hasErrors;
	}

	/**
	 * @description:    Retrieves proper dealer code prefix index
	 * @param           dealerCode - dealer code
	 * @return			Integer - dealer code prefix index
	 **/
	private Integer getDealerPrefixIndex(String dealerCode) {
		Integer dealerCodePrefixIndex = 0;

		if (dealerCode.length() > DEALER_CODE_WITHOUT_PREFIX_LENGTH) {
			dealerCodePrefixIndex = dealerCode.length() - DEALER_CODE_WITHOUT_PREFIX_LENGTH;
		}

		return dealerCodePrefixIndex;
	}
}
