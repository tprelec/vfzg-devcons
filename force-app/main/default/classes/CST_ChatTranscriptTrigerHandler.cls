public without sharing class CST_ChatTranscriptTrigerHandler extends TriggerHandler {
	Set<Id> contactTranscriptIds = new Set<Id>();
	Map<String, Id> contactAccounts = new Map<String, Id>();

	private List<LiveChatTranscript> newChatTranscripts = new List<LiveChatTranscript>();
	private Map<Id, LiveChatTranscript> newTranscriptMap = new Map<Id, LiveChatTranscript>();
	private Map<Id, LiveChatTranscript> oldTranscriptMap = new Map<Id, LiveChatTranscript>();

	private void init() {
		newChatTranscripts = (List<LiveChatTranscript>) this.newList;
		newTranscriptMap = (Map<Id, LiveChatTranscript>) this.newMap;
		oldTranscriptMap = (Map<Id, LiveChatTranscript>) this.oldMap;
	}

	public override void beforeUpdate() {
		System.debug('beforeUpdate: ' + newChatTranscripts);
		init();
		updateChat();
	}

	private void updateChat() {
		for (LiveChatTranscript chat : newChatTranscripts) {
			if (chat.ContactId != null && chat.status != 'Waiting' && chat.accountId == null) {
				contactTranscriptIds.add(chat.ContactId);
			}
		}
		System.debug('contactTranscriptIds: ' + contactTranscriptIds);

		if (!contactTranscriptIds.isEmpty()) {
			Map<Id, Contact> mapContacts = new Map<Id, Contact>(
				[SELECT AccountId, name, Id FROM Contact WHERE id IN :contactTranscriptIds AND AccountId != NULL]
			);

			System.debug('mapContacts: ' + mapContacts);

			for (LiveChatTranscript chat : newChatTranscripts) {
				if (chat.contactId != null && mapContacts.containsKey(chat.contactId)) {
					chat.AccountId = mapContacts.get(chat.contactId).AccountId;
					System.debug('update chat: ' + chat.AccountId);
				}

				System.debug('updated chats: ' + newChatTranscripts);
			}
		}
	}
}
