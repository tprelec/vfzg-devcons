@isTest
private class TestBdsRestCreateOrder {
    @isTest
    static void runCloseOpp() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0623456789';
        conClass.mobilephone = '0623456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);


        Test.startTest();

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN', '1', '');

        BdsRestCreateOrder.doPost(retClass.opportunityId);
        Test.stopTest();
    }

    @isTest
    static void runCloseOppMissingOppId() {
        Test.startTest();

        BdsRestCreateOrder.doPost('');
        Test.stopTest();
    }

    @isTest
    static void runValidateMissingOppId() {
        Test.startTest();

        BdsResponseClasses.OpportunityStatusClass errClass = BdsRestCreateOrder.validateValues('');
        System.assertEquals(false, errClass.isSuccess);
        System.assertEquals('No Opportunity ID provided', errClass.errorMessage);

        Test.stopTest();
    }

    @isTest
    static void runValidateInvalidId() {
        Test.startTest();

        BdsResponseClasses.OpportunityStatusClass errClass = BdsRestCreateOrder.validateValues('12345x1234');
        System.assertEquals(false, errClass.isSuccess);
        System.assertEquals('No valid Opportunity ID provided', errClass.errorMessage);

        Test.stopTest();
    }

    @isTest
    static void runValidateNoOppWithThisId() {
        Test.startTest();

        BdsResponseClasses.OpportunityStatusClass errClass = BdsRestCreateOrder.validateValues(UserInfo.getUserId());
        System.assertEquals(false, errClass.isSuccess);
        System.assertEquals('No opportunity found with this ID', errClass.errorMessage);

        Test.stopTest();
    }


}