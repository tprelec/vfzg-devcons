@isTest
@SuppressWarnings('PMD.ExcessiveParameterList')
public with sharing class ECSSOAPUserMocks {
	public class CreateUpdateUserResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPUser.userResponseType[] userResX = new List<ECSSOAPUser.userResponseType>{};
			ECSSOAPUser.userResponseType myResponse = new ECSSOAPUser.userResponseType();
			myResponse.ban = '12345';
			myResponse.corporateId = '000randomId';
			myResponse.bopCode = 'ABC';
			myResponse.companyReferenceId = 'ranIdCompany';
			myResponse.email = 'test@gmail.com';
			myResponse.userReferenceId = 'DFG';
			myResponse.referenceId = 'XYZ';
			ECSSOAPUser.userErrorListType clsErrors = new ECSSOAPUser.userErrorListType();
			List<ECSSOAPUser.userErrorType> lstError = new List<ECSSOAPUser.userErrorType>();
			ECSSOAPUser.userErrorType objError = new ECSSOAPUser.userErrorType();
			objError.code = '0';
			objError.message = 'mock';
			lstError.add(objError);
			clsErrors.error = lstError;
			myResponse.errors = clsErrors;
			userResX.add(myResponse);
			ECSSOAPUser.usersResponse_element responseX = new ECSSOAPUser.usersResponse_element();
			responseX.user_x = userResX;
			response.put('response_x', responseX);
		}
	}
	public class CreateContactResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPUser.contactResponseType[] contactResX = new List<ECSSOAPUser.contactResponseType>{};
			ECSSOAPUser.contactResponseType myResponse = new ECSSOAPUser.contactResponseType();
			myResponse.type_x = 'test';
			myResponse.service = 'create';

			ECSSOAPUser.userRefType objUserx = new ECSSOAPUser.userRefType();
			ECSSOAPUser.companyRefType objUserxCompany = new ECSSOAPUser.companyRefType();
			objUserxCompany.accountId = '001Rand';
			objUserxCompany.banNumber = '300123456';
			objUserxCompany.bopCode = 'NPI';
			objUserxCompany.corporateId = 'CorpRanId';
			objUserxCompany.externalSystem = 'extRandId';
			objUserxCompany.referenceId = 'refIdRand';
			objUserx.company = objUserxCompany;
			objUserx.email = 'abc@abc.com';
			objUserx.referenceId = 'UserXRefId';
			myResponse.user_x = objUserx;

			ECSSOAPUser.companyRefType objCompany = new ECSSOAPUser.companyRefType();
			objCompany.accountId = '001Rand2';
			objCompany.banNumber = '3001234567';
			objCompany.bopCode = 'NGI';
			objCompany.corporateId = 'CorpRanId2';
			objCompany.externalSystem = 'extRandId2';
			objCompany.referenceId = 'refIdRand2';
			myResponse.company = objCompany;

			ECSSOAPUser.locationRefType objlocation = new ECSSOAPUser.locationRefType();
			ECSSOAPUser.companyRefType objCompanyLocation = new ECSSOAPUser.companyRefType();
			objCompanyLocation.accountId = '001RandLoc';
			objCompanyLocation.banNumber = '3001234568';
			objCompanyLocation.bopCode = 'NGL';
			objCompanyLocation.corporateId = 'CorpRanIdLoc';
			objCompanyLocation.externalSystem = 'extRandIdLoc';
			objCompanyLocation.referenceId = 'refIdRandLoc';
			objlocation.company = objCompanyLocation;
			objlocation.housenumber = '2';
			objlocation.housenumberExt = 'A';
			objlocation.locationReferenceId = 'LocRefId';
			objlocation.zipcode = '1234AB';
			myResponse.location = objlocation;

			ECSSOAPUser.userErrorListType clsErrors = new ECSSOAPUser.userErrorListType();
			List<ECSSOAPUser.userErrorType> lstError = new List<ECSSOAPUser.userErrorType>();
			ECSSOAPUser.userErrorType objError = new ECSSOAPUser.userErrorType();
			objError.code = '0';
			objError.message = 'mock';
			lstError.add(objError);
			clsErrors.error = lstError;
			myResponse.errors = clsErrors;

			contactResX.add(myResponse);
			ECSSOAPUser.createContactsResponse_element responseX = new ECSSOAPUser.createContactsResponse_element();
			responseX.contact = contactResX;
			response.put('response_x', responseX);
		}
	}

	public class DeleteContactResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPUser.contactResponseType[] contactResX = new List<ECSSOAPUser.contactResponseType>{};
			ECSSOAPUser.contactResponseType myResponse = new ECSSOAPUser.contactResponseType();
			myResponse.type_x = 'test';
			myResponse.service = 'delete';

			ECSSOAPUser.userRefType objUserx = new ECSSOAPUser.userRefType();
			ECSSOAPUser.companyRefType objUserxCompany = new ECSSOAPUser.companyRefType();
			objUserxCompany.accountId = '001Rand';
			objUserxCompany.banNumber = '300123456';
			objUserxCompany.bopCode = 'NPI';
			objUserxCompany.corporateId = 'CorpRanId';
			objUserxCompany.externalSystem = 'extRandId';
			objUserxCompany.referenceId = 'refIdRand';
			objUserx.company = objUserxCompany;
			objUserx.email = 'abc@abc.com';
			objUserx.referenceId = 'UserXRefId';
			myResponse.user_x = objUserx;

			ECSSOAPUser.companyRefType objCompany = new ECSSOAPUser.companyRefType();
			objCompany.accountId = '001Rand2';
			objCompany.banNumber = '3001234567';
			objCompany.bopCode = 'NGI';
			objCompany.corporateId = 'CorpRanId2';
			objCompany.externalSystem = 'extRandId2';
			objCompany.referenceId = 'refIdRand2';
			myResponse.company = objCompany;
			myResponse.location = null;

			ECSSOAPUser.userErrorListType clsErrors = new ECSSOAPUser.userErrorListType();
			List<ECSSOAPUser.userErrorType> lstError = new List<ECSSOAPUser.userErrorType>();
			ECSSOAPUser.userErrorType objError = new ECSSOAPUser.userErrorType();
			objError.code = '0';
			objError.message = 'mock';
			lstError.add(objError);
			clsErrors.error = lstError;
			myResponse.errors = clsErrors;

			contactResX.add(myResponse);
			ECSSOAPUser.deleteContactsResponse_element responseX = new ECSSOAPUser.deleteContactsResponse_element();
			responseX.contact = contactResX;
			response.put('response_x', responseX);
		}
	}
}
