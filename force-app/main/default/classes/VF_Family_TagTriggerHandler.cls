public with sharing class VF_Family_TagTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<VF_Family_Tag__c> newProducts = (List<VF_Family_Tag__c>) this.newList;
		Sequence seq = new Sequence('VF Family Tag');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (VF_Family_Tag__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
