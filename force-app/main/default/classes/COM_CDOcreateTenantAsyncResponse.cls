@RestResource(urlMapping='/testCDOCreateTenant/*')
global with sharing class COM_CDOcreateTenantAsyncResponse {
	/*
    @HttpPost
    global static void updateTenantId() {
        RestRequest req = RestContext.request;

        String reqBody = COM_CDOcreateTenantService.replaceJsonKeysToFitDeserializeClass(
            req.requestbody.tostring()
        );
        try {
            System.debug('Deserialiting');
            CDOCreateTenantAsyncResponse response = (CDOCreateTenantAsyncResponse) JSON.deserializeStrict(
                reqBody,
                CDOCreateTenantAsyncResponse.class
            );

            System.debug('Deserialiting done 1');
            String umbrellaService = COM_CDOcreateTenant.getUmbrellaService(
                response.event.serviceOrder
            );
            System.debug('Deserialiting done 2');

            System.debug(response);

            Id accountId = COM_CDOcreateTenant.getAccountIdByUmbrellaService(umbrellaService);
            System.debug('Deserialiting done 3');
            String tenantId = COM_CDOcreateTenant.getTenantId(response.event.serviceOrder);
            System.debug('Deserialiting done 4');
            COM_CDOcreateTenant.CreateTenantAccountUpdateData accountUpdate = new COM_CDOcreateTenant.CreateTenantAccountUpdateData();
            accountUpdate.accountId = accountId;
            accountUpdate.successful = true;
            accountUpdate.tenantId = tenantId;
            accountUpdate.umbrellaService = umbrellaService;
            accountUpdate.errorMessage = null;

            System.debug('Will try to update account');
            COM_CDOcreateTenant.updateAccount(accountUpdate);
        } catch (Exception ex) {
            try {
                CDOCreateTenantAsyncFailedResponse response = (CDOCreateTenantAsyncFailedResponse) JSON.deserializeStrict(
                    reqBody,
                    CDOCreateTenantAsyncFailedResponse.class
                );
                String umbrellaService = COM_CDOcreateTenant.getUmbrellaService(
                    response.event.serviceOrder
                );
                Id accountId = COM_CDOcreateTenant.getAccountIdByUmbrellaService(umbrellaService);
                String generateEventErrorMessageText = generateEventErrorMessage(
                    response.event.errorEvents
                );

                String generateEventErrorMessageFormatted = generateEventErrorMessageText.length() >
                    131072
                    ? generateEventErrorMessageText.substring(0, 131071)
                    : generateEventErrorMessageText;

                COM_CDOcreateTenant.CreateTenantAccountUpdateData accountUpdate = new COM_CDOcreateTenant.CreateTenantAccountUpdateData();
                accountUpdate.accountId = accountId;
                accountUpdate.successful = false;
                accountUpdate.tenantId = null;
                accountUpdate.umbrellaService = umbrellaService;
                accountUpdate.errorMessage = generateEventErrorMessageFormatted;

                COM_CDOcreateTenant.updateAccount(accountUpdate);
            } catch (Exception eex) {
                System.debug(LoggingLevel.Error, '***Failure');
                System.debug(LoggingLevel.Error, eex.getMessage());
            }
        }
    }
    private static String generateEventErrorMessage(ErrorEvents[] errorEvents) {
        String result = '';
        if (errorEvents != null && errorEvents.size() > 0) {
            for (ErrorEvents ef : errorEvents) {
                result += ef.message;
            }
        }
        return result;
    }
    global class CDOCreateTenantAsyncResponse {
        public String eventId; //deff54ee-ba37-4dba-b1c9-1fe1e554a4e1
        public String eventTime; //2022-02-08T11:30:15.97221Z
        public String eventType; //ServiceOrderStateChangeNotification
        public Event event;
    }

    global class Event {
        public COM_CDOcreateTenantService serviceOrder;
    }

    global class CDOCreateTenantAsyncFailedResponse {
        public String eventId; //9577add4-5200-47cb-b787-f6e617b396d9
        public String eventTime; //2022-02-08T10:45:15.97221Z
        public String eventType; //ServiceOrderStateChangeNotification
        public EventFailed event;
    }

    global class EventFailed {
        public COM_CDOcreateTenantService serviceOrder;
        public ErrorEvents[] errorEvents;
    }

    global class ErrorEvents {
        public Integer code; //510105000
        public String reason; //EXTERNAL_SYSTEM_ERROR
        public String message; //A fulfillment activity failed with error-code '5000', error-message 'some error message'.
    }
    */
}
