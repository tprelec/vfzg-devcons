public with sharing class CS_MFStatusUpdateStartupTime {

    private static final String CASE_RECORD_TYPE_CS_MF_ORDER_CLEANING = 'CS_MF_Order_Cleaning';
    private static final BusinessHours DEFAULTBH = [SELECT Id From BusinessHours WHERE IsDefault = true];

    @InvocableMethod(label = 'Calc KPI Startup Time')
    public static void calculateStartUpTime2(List<Id> caseSet) {
        if (!caseSet.isEmpty()) {
            Set<Id> contractIdsSet = new Set<Id>();
            for (Case objCase: [SELECT Contract_VF__c FROM Case WHERE Id IN :caseSet AND Contract_VF__c != null]) {
                contractIdsSet.add(objCase.Contract_VF__c);
            }
            if (!contractIdsSet.isEmpty()) {
                Datetime orderCleaningCloseDate;
                Datetime productIntakeOwnershipDate = Datetime.now();
                List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
                Id tempId;

                for (Case objCase : [
                SELECT Id, Case_Start_Date__c, ClosedDate, RecordType.DeveloperName, Contract_VF__c
                FROM Case
                WHERE Contract_VF__c IN :contractIdsSet
                AND RecordType.DeveloperName = :CASE_RECORD_TYPE_CS_MF_ORDER_CLEANING
                AND ClosedDate < TOMORROW
                ORDER BY Contract_VF__c]) {

                    if (objCase.Contract_VF__c != tempId) {
                        orderCleaningCloseDate = objCase.ClosedDate;
                        Decimal businessDayDifference = getWorkdayDifference(orderCleaningCloseDate, productIntakeOwnershipDate);
                        contractsToUpdate.add(new VF_Contract__c(Id = objCase.Contract_VF__c, Startup_Time__c = businessDayDifference, Start_Up_Time_Start__c = Date.today()));
                    }
                    tempId = objCase.Contract_VF__c;
                }
                update contractsToUpdate;
            }
        }
    }

    private static Decimal getWorkdayDifference(Datetime startDate, Datetime endDate) {
        Long inputMillisecs =  BusinessHours.diff(DEFAULTBH.Id, startDate, endDate);
        Long hours = Math.abs(BusinessHours.diff(DEFAULTBH.Id, startDate, endDate)/1000/60/60);
        Decimal totalDurationInDays = hours / 8.0;
        return totalDurationInDays.round(System.RoundingMode.CEILING);
    }
}