/**
 * After save discount observer.
 */
global class CS_DiscountObserverAfterSave implements csdiscounts.IDiscountsObserver {
	/**
	 * Default constructor.
	 */
	public CS_DiscountObserverAfterSave() {
	}

	/**
	 * Requirement - discount allowed only on certain products
	 */
	public Boolean CS_DiscountObserverAfterSave(List<Id> pcIds) {
		Boolean isAllowed = false;

		List<cscfga__Product_Configuration__c> configs = [SELECT Id FROM cscfga__Product_Configuration__c WHERE Id IN :pcIds];

		//find
		return isAllowed;
	}

	/**
	 * Requirement was that mobile discount percentage should be rounded to Integer.
	 */
	public Integer roundDecimalNumber(Decimal decimalNumber) {
		return Integer.valueOf(Math.Round(decimalNumber));
	}

	/**
	 * Requirements was that fixed discount percentage should be rounded to 4 decimal places.
	 */
	public Decimal roundToFourDecimalPlaces(Decimal decimalNumber) {
		Decimal val = decimalNumber.setScale(4);
		if (decimalNumber == 100.00) {
			return decimalNumber.setScale(2);
		}
		return val.setScale(4);
	}

	/**
	 * Requirement was that if percentage was entered as amount that it should be caclulated into percentage.
	 */
	public Decimal calculatePercentage(Decimal amount, Decimal price) {
		Decimal percentage = 0.00;
		if (amount == price + amount && amount > 0) {
			return 100;
		} else if (amount > 0 && price > 0) {
			percentage = amount / (price + amount) * 100;
		}

		return roundToFourDecimalPlaces(percentage);
	}

	/**
	 * TODO this method can be changed to check categories and not name of product definitions.
	 */
	public Boolean isMobile(cscfga__Product_Configuration__c pc) {
		System.debug('CSDOAS pc ' + pc);
		System.debug('CSDOAS pd ' + pc.cscfga__Product_Definition__c);
		System.debug('CSDOAS pd ' + pc.cscfga__Product_Definition__r);
		System.debug('CSDOAS name ' + pc.cscfga__Product_Definition__r.Name);
		if (pc.cscfga__Parent_Configuration__c == null) {
			if (pc.cscfga__Product_Definition__r.Name.indexOf('Mobile') != -1) {
				return true;
			}
		} else {
			if (pc.cscfga__Parent_Configuration__r.cscfga__Product_Definition__r.Name.indexOf('Mobile') != -1) {
				return true;
			}
		}
		return false;
	}

	/**Fixed Products
	 *
	 * Basket In Net Profit ->all discounts allowed
	 * Basket NOT in net profit --> only Discount allowed
	 *
	 */
	public Boolean basketInNetProfit(cscfga__Product_Basket__c basket) {
		Boolean result = false;
		if (basket != null && basket.Basket_qualification__c != null) {
			//Abdul said to change - 5/04/2019 -> Promo is SAC SRC
			if (basket.Basket_qualification__c == 'Net profit') {
				result = true;
			}
		}
		return result;
	}

	/**
	 * Method that is automatically called when observer implementation s called. Each product configuration will have Id and csdiscounts__manual_discounts__c  fields populated.
	 */
	public String execute(List<cscfga__Product_Configuration__c> productConfigurations) {
		/**
		 * Message string that is going to be shown to the user after Observer execute method.
		 */
		String message = '';

		/**
		 * Field for basket ID which is needed in Discount recalculation.
		 */
		Id basketId;
		cscfga.ProductConfiguration.Discount discountObjectTmp = new cscfga.ProductConfiguration.Discount();

		cscfga__Product_Basket__c basket;
		Boolean basketInNetProfit = false;
		//include configurations with GroupDiscount__c not empty
		productConfigurations = [
			SELECT
				id,
				Commercial_Product__c,
				Add_On_Product__c,
				GroupDiscount__c,
				cscfga__Product_Basket__c,
				cscfga__Product_Basket__r.DirectIndirect__c,
				Name,
				cscfga__Root_Configuration__c,
				cscfga__one_off_charge_product_discount_value__c,
				cscfga__recurring_charge_product_discount_value__c,
				Discount_allowed__c,
				cscfga__Recurring_Charge__c,
				cscfga__One_Off_Charge__c,
				cscfga__discounts__c,
				csdiscounts__manual_discounts__c,
				cscfga__Parent_Configuration__c,
				cscfga__Parent_Configuration__r.cscfga__Product_Definition__r.Name,
				cscfga__Product_Definition__r.Name,
				Proposition__c,
				cscfga__Quantity__c,
				Mobile_Scenario__c,
				cscfga__Product_Definition__r.cscfga__Product_Category__r.Name,
				New_Portfolio__c
			FROM cscfga__Product_Configuration__c
			WHERE id IN :productConfigurations
		];

		/*Set<Id> relevantProducts = new Set<Id>();
        
        for(cscfga__Product_Configuration__c pc : productConfigurations){
            if(pc.Commercial_Product__c != null){
                relevantProducts.add(pc.Commercial_Product__c);
            }
        }
        
        
        Set<Id> relevantAddonProducts = new Set<Id>();
        
        for(cscfga__Product_Configuration__c pc : productConfigurations){
            if(pc.Add_On_Product__c != null){
                relevantProducts.add(pc.Add_On_Product__c);
            }
        }*/

		//new discount feature
		//List<cspmb__Price_Item__c> commercialProductsForValidation = [SELECT Id, Name,OneOff_Discount_Sales_Channel__c,Recurring_Discount_Sales_Channel__c,Recurring_Product_Discount_Allowed__c,One_Off_Product_Discount_Allowed__c from cspmb__Price_Item__c where Id in :relevantProducts];
		//Map<Id, cspmb__Price_Item__c> commercialProductMap = new Map<Id, cspmb__Price_Item__c>(commercialProductsForValidation);

		//List<cspmb__Add_On_Price_Item__c> addonProductsForValidation = [SELECT Id, Name,OneOff_Discount_Sales_Channel__c,Recurring_Discount_Sales_Channel__c,Recurring_Product_Discount_Allowed__c,One_Off_Product_Discount_Allowed__c from cspmb__Add_On_Price_Item__c where Id in :relevantProducts];
		//Map<Id, cspmb__Add_On_Price_Item__c> addonProductMap = new Map<Id, cspmb__Add_On_Price_Item__c>(addonProductsForValidation);

		List<cscfga__Product_Configuration__c> basketConfigsAll = [
			SELECT
				id,
				Commercial_Product__c,
				Add_On_Product__c,
				GroupDiscount__c,
				cscfga__Product_Basket__r.DirectIndirect__c,
				cscfga__Product_Basket__c,
				Name,
				cscfga__Root_Configuration__c,
				cscfga__one_off_charge_product_discount_value__c,
				cscfga__recurring_charge_product_discount_value__c,
				Discount_allowed__c,
				cscfga__Recurring_Charge__c,
				cscfga__One_Off_Charge__c,
				cscfga__discounts__c,
				csdiscounts__manual_discounts__c,
				cscfga__Parent_Configuration__c,
				cscfga__Contract_Term__c,
				cscfga__Parent_Configuration__r.cscfga__Product_Definition__r.Name,
				cscfga__Product_Definition__r.Name,
				Proposition__c,
				cscfga__Quantity__c,
				Mobile_Scenario__c,
				cscfga__Product_Definition__r.cscfga__Product_Category__r.Name,
				New_Portfolio__c
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c = :productConfigurations[0].cscfga__Product_Basket__c
		];

		//add configs with group discount in product configurations
		Map<Id, cscfga__Product_Configuration__c> productConfigurationMap = new Map<Id, cscfga__Product_Configuration__c>(productConfigurations);
		Map<Id, cscfga__Product_Configuration__c> basketConfigsAllMap = new Map<Id, cscfga__Product_Configuration__c>(basketConfigsAll);

		for (cscfga__Product_Configuration__c config : basketConfigsAll) {
			if (config.GroupDiscount__c != null) {
				if (!productConfigurationMap.containsKey(config.Id)) {
					productConfigurations.add(config);
				}
			}
		}

		if (productConfigurations.size() > 0) {
			basketId = productConfigurations[0].cscfga__Product_Basket__c;
			basket = [
				SELECT
					id,
					Basket_qualification__c,
					Current_User_Channel__c,
					Minimum_Volume_Mobile__c,
					Contract_duration_Mobile__c,
					Minimum_Value_Mobile__c,
					Contract_duration_Fixed__c,
					Minimum_Value_Fixed__c,
					Minimum_Volume_Fixed__c,
					Number_of_SIP__c,
					Maximum_Usage_Mobile__c,
					TotalCLFVMinutes__c
				FROM cscfga__Product_Basket__c
				WHERE id = :basketId
			];
			basketInNetProfit = basketInNetProfit(basket);
		} else {
			message = 'Product configuration list is empty.';
			return message;
		}

		/*
    NC - 05.09.2019.
    moved from lines 115 - 130
    commercialProductMap didn't take the products from all conigs in basket but only the ones for the config being saved
    this was causing errors when calling getNotAllowedDiscounts(...) after the user selected Save discount on config row (it was working fine when pressing the "master" Save discounts)
    */
		Set<Id> relevantProducts = new Set<Id>();

		for (cscfga__Product_Configuration__c pc : productConfigurations) {
			if (pc.Commercial_Product__c != null) {
				relevantProducts.add(pc.Commercial_Product__c);
			}
		}

		Set<Id> relevantAddonProducts = new Set<Id>();

		for (cscfga__Product_Configuration__c pc : productConfigurations) {
			if (pc.Add_On_Product__c != null) {
				relevantProducts.add(pc.Add_On_Product__c);
			}
		}

		//NC - 05.09.2019. -> moved from lines 135 and 136, 138 ,139
		List<cspmb__Price_Item__c> commercialProductsForValidation = [
			SELECT
				Id,
				Name,
				OneOff_Discount_Sales_Channel__c,
				Recurring_Discount_Sales_Channel__c,
				Recurring_Product_Discount_Allowed__c,
				One_Off_Product_Discount_Allowed__c
			FROM cspmb__Price_Item__c
			WHERE Id IN :relevantProducts
		];
		Map<Id, cspmb__Price_Item__c> commercialProductMap = new Map<Id, cspmb__Price_Item__c>(commercialProductsForValidation);

		List<cspmb__Add_On_Price_Item__c> addonProductsForValidation = [
			SELECT
				Id,
				Name,
				OneOff_Discount_Sales_Channel__c,
				Recurring_Discount_Sales_Channel__c,
				Recurring_Product_Discount_Allowed__c,
				One_Off_Product_Discount_Allowed__c
			FROM cspmb__Add_On_Price_Item__c
			WHERE Id IN :relevantProducts
		];
		Map<Id, cspmb__Add_On_Price_Item__c> addonProductMap = new Map<Id, cspmb__Add_On_Price_Item__c>(addonProductsForValidation);

		for (cscfga__Product_Configuration__c pc : productConfigurations) {
			//SYNC
			List<cscfga.ProductConfiguration.Discount> discountList = new List<cscfga.ProductConfiguration.Discount>();
			if (pc.GroupDiscount__c != null) {
				System.debug('Will apply group discount for A ' + pc.Id);
				if (pc.GroupDiscount__c == '{"discounts":[]}') {
					pc.csdiscounts__manual_discounts__c = '{"discounts":[]}';
				}
				discountList = getDiscountObjectsOOTBStructure(pc.GroupDiscount__c, discountObjectTmp);
				System.debug('dlist=' + discountList);
				pc.GroupDiscount__c = null;
			} else {
				discountList = getDiscountObjectsOOTBStructure(pc.csdiscounts__manual_discounts__c, discountObjectTmp);
			}

			if (discountList == null || (discountList.size() == 0)) {
				pc.csdiscounts__manual_discounts__c = '{"discounts":[]}';
			}
			//END SYNC
			system.debug('discountObjectTmp ----- ' + discountObjectTmp);

			List<cscfga.ProductConfiguration.Discount> discountListReversed = new List<cscfga.ProductConfiguration.Discount>();

			for (Integer i = discountList.size() - 1; i >= 0; i--) {
				discountListReversed.add(discountList[i]);
			}

			system.debug(
				'**** basketInNetProfit: ' +
				basketInNetProfit +
				' => pc.Id:' +
				pc.Id +
				' => pc.Commercial_Product__c: ' +
				pc.Commercial_Product__c
			);
			if ((!basketInNetProfit) && pc.Commercial_Product__c != null) {
				if (Test.isRunningTest()) {
					List<cscfga.ProductConfiguration.Discount> notAllowedDiscounts = getNotAllowedDiscounts(
						'Direct',
						discountList,
						commercialProductMap.get(pc.Commercial_Product__c)
					);
					if (notAllowedDiscounts != null && notAllowedDiscounts.size() > 0) {
						for (cscfga.ProductConfiguration.Discount discount : notAllowedDiscounts) {
							message +=
								'<br>Discount ' +
								discount.type +
								'=' +
								discount.amount +
								' is NOT ALLOWED for ' +
								pc.Name +
								' and it has been removed!';
						}

						discountList = removeNotAllowedDiscounts(discountList, notAllowedDiscounts);

						if (discountList == null || (discountList.size() == 0)) {
							pc.csdiscounts__manual_discounts__c = '{"discounts":[]}';
						}
					}
				} else {
					List<cscfga.ProductConfiguration.Discount> notAllowedDiscounts = getNotAllowedDiscounts(
						basket.Current_User_Channel__c,
						discountList,
						commercialProductMap.get(pc.Commercial_Product__c)
					);
					if (notAllowedDiscounts != null && notAllowedDiscounts.size() > 0) {
						for (cscfga.ProductConfiguration.Discount discount : notAllowedDiscounts) {
							message +=
								'<br>Discount ' +
								discount.type +
								'=' +
								discount.amount +
								' is NOT ALLOWED for ' +
								pc.Name +
								' and it has been removed!';
						}

						discountList = removeNotAllowedDiscounts(discountList, notAllowedDiscounts);

						if (discountList == null || (discountList.size() == 0)) {
							pc.csdiscounts__manual_discounts__c = '{"discounts":[]}';
						}
					}
				}
			}
			if ((!basketInNetProfit) && pc.Add_On_Product__c != null) {
				if (Test.isRunningTest()) {
					List<cscfga.ProductConfiguration.Discount> notAllowedDiscounts = getNotAllowedDiscountsAddon(
						'Direct',
						discountList,
						addonProductMap.get(pc.Add_On_Product__c)
					);
					if (notAllowedDiscounts != null && notAllowedDiscounts.size() > 0) {
						for (cscfga.ProductConfiguration.Discount discount : notAllowedDiscounts) {
							message +=
								'<br>Discount ' +
								discount.type +
								'=' +
								discount.amount +
								' is NOT ALLOWED for ' +
								pc.Name +
								' and it has been removed!';
						}

						discountList = removeNotAllowedDiscounts(discountList, notAllowedDiscounts);

						if (discountList == null || (discountList.size() == 0)) {
							pc.csdiscounts__manual_discounts__c = '{"discounts":[]}';
						}
					}
				} else {
					List<cscfga.ProductConfiguration.Discount> notAllowedDiscounts = getNotAllowedDiscountsAddon(
						basket.Current_User_Channel__c,
						discountList,
						addonProductMap.get(pc.Add_On_Product__c)
					);
					if (notAllowedDiscounts != null && notAllowedDiscounts.size() > 0) {
						for (cscfga.ProductConfiguration.Discount discount : notAllowedDiscounts) {
							message +=
								'<br>Discount ' +
								discount.type +
								'=' +
								discount.amount +
								' is NOT ALLOWED for ' +
								pc.Name +
								' and it has been removed!';
						}

						discountList = removeNotAllowedDiscounts(discountList, notAllowedDiscounts);

						if (discountList == null || (discountList.size() == 0)) {
							pc.csdiscounts__manual_discounts__c = '{"discounts":[]}';
						}
					}
				}
			}

			Decimal oneOffValue = pc.cscfga__one_off_charge_product_discount_value__c + pc.cscfga__One_Off_Charge__c;
			Decimal recurringValue = pc.cscfga__recurring_charge_product_discount_value__c + pc.cscfga__Recurring_Charge__c;

			Map<cscfga.ProductConfiguration.Discount, Decimal> discountValuesMap = new Map<cscfga.ProductConfiguration.Discount, Decimal>();

			for (cscfga.ProductConfiguration.Discount discountElement : discountList) {
				if (discountElement.type == 'percentage') {
					if (discountElement.chargeType == 'oneOff') {
						oneOffValue = oneOffValue * ((100 - discountElement.amount) / 100);
						discountValuesMap.put(discountElement, oneOffValue);
					} else if (discountElement.chargeType == 'recurring') {
						recurringValue = recurringValue * ((100 - discountElement.amount) / 100);
						discountValuesMap.put(discountElement, recurringValue);
					}
				} else if (discountElement.type == 'absolute') {
					if (discountElement.chargeType == 'oneOff') {
						oneOffValue = oneOffValue - discountElement.amount;
						discountValuesMap.put(discountElement, oneOffValue);
					} else if (discountElement.chargeType == 'recurring') {
						recurringValue = recurringValue - discountElement.amount;
						discountValuesMap.put(discountElement, recurringValue);
					}
				}
			}

			if (String.isEmpty(pc.cscfga__Root_Configuration__c) && !pc.New_Portfolio__c) {
				if (discountList.size() > 0) {
					System.debug('HERE WE ARE!');
					message += '<br>Discount is NOT ALLOWED for ' + pc.Name + ' and it has been removed!';
					pc.csdiscounts__manual_discounts__c = '{"discounts":[]}';
				}
			} else {
				for (cscfga.ProductConfiguration.Discount discountElement : discountList) {
					System.debug('discountElement for loop 2 - ' + discountElement);

					if (discountElement.type == 'percentage') {
						// TP - removed "decimal places ban" for mobile products as per request in W-004273
						discountElement.amount = roundToFourDecimalPlaces(discountElement.amount);
					} else if (discountElement.type == 'absolute') {
						if (isMobile(pc) == false && !pc.New_Portfolio__c) {
							Decimal decimalValue = discountElement.amount;

							discountElement.type = 'percentage';

							if (discountElement.chargeType == 'oneOff') {
								if ((pc.cscfga__One_Off_Charge__c == 0) || (pc.cscfga__One_Off_Charge__c == 0.00)) {
									message += '<br>' + pc.Name + '--100% discount applied on one off charge';
									discountElement.amount = 100.00;
									//return message;
								} else {
									message +=
										'<br>Applying discount amount of: ' +
										discountElement.amount +
										' on one off charge: ' +
										pc.cscfga__One_Off_Charge__c;
									discountElement.amount = pc.cscfga__One_Off_Charge__c != 0.00
										? calculatePercentage(decimalValue, discountValuesMap.get(discountElement))
										: 0.00;
								}
							} else if (discountElement.chargeType == 'recurring') {
								if ((pc.cscfga__Recurring_Charge__c == 0) || (pc.cscfga__Recurring_Charge__c == 0.00)) {
									message += '<br>' + pc.Name + '--100% discount applied on recurring charge';
									discountElement.amount = 100.00;
									//return message;
								} else {
									message +=
										'<br>' +
										pc.Name +
										'--Applying discount amount of: ' +
										discountElement.amount +
										' on recurring charge: ' +
										pc.cscfga__Recurring_Charge__c;
									discountElement.amount = pc.cscfga__Recurring_Charge__c != 0.00
										? calculatePercentage(decimalValue, discountValuesMap.get(discountElement))
										: 0.00;
								}
							}
						}
					}
				}

				System.debug('discountObjectTmp END - ' + discountObjectTmp);

				if (discountObjectTmp.version == null) {
					pc.csdiscounts__manual_discounts__c = '{"discounts":[]}';
				} else {
					List<CS_DiscountJsonElement> memberDiscounts = new List<CS_DiscountJsonElement>();

					for (cscfga.ProductConfiguration.Discount discountElement : discountList) {
						CS_DiscountJsonElement discountJsonElement = new CS_DiscountJsonElement(
							discountElement.duration,
							discountElement.type,
							discountElement.source,
							discountElement.discountCharge,
							discountElement.description,
							discountElement.customData
						);
						discountJsonElement.amount = discountElement.amount;
						discountJsonElement.chargeType = discountElement.chargeType;
						discountJsonElement.recordType = discountElement.recordType;
						discountJsonElement.version = discountElement.version;
						memberDiscounts.add(discountJsonElement);
					}

					if (memberDiscounts.size() > 0) {
						CS_DiscountJsonElement.CS_DiscountJsonElementRoot discountsRoot = new CS_DiscountJsonElement.CS_DiscountJsonElementRoot();
						discountsRoot.discounts = new List<CS_DiscountJsonElement.CS_DiscountJsonElementNew>();

						CS_DiscountJsonElement.CS_DiscountJsonElementNew memberDiscountsData = new CS_DiscountJsonElement.CS_DiscountJsonElementNew();
						memberDiscountsData.memberDiscounts = memberDiscounts;
						memberDiscountsData.version = discountObjectTmp.version;
						memberDiscountsData.recordType = discountObjectTmp.recordType;
						memberDiscountsData.evaluationOrder = discountObjectTmp.evaluationOrder;

						List<CS_DiscountJsonElement.CS_DiscountJsonElementNew> memberDiscountsDataList = new List<CS_DiscountJsonElement.CS_DiscountJsonElementNew>();
						memberDiscountsDataList.add(memberDiscountsData);

						discountsRoot.discounts = memberDiscountsDataList;

						System.debug('discountsRoot before serialize: ' + discountsRoot);
						String serializedDiscount = JSON.serialize(discountsRoot, true);
						System.debug('serializedDiscount ----- ' + serializedDiscount);

						pc.cscfga__discounts__c = serializedDiscount;
						pc.csdiscounts__manual_discounts__c = serializedDiscount;
					} else {
						pc.csdiscounts__manual_discounts__c = '{"discounts":[]}';
					}
				}
			}
		}

		try {
			update productConfigurations;
			cscfga.ProductConfigurationBulkActions.calculateTotals(new Set<Id>{ basketId });
		} catch (Exception e) {
			message += '<br/>' + e.getMessage();
			System.debug('***Discount SAVE FAILED -->' + e.getMessage());
		}

		CS_PCPackageSubscriber.calculateFairUsageBasketCalculations(basketConfigsAll, basketId, basket);

		return message;
	}

	/**
	 *
	 * Example JSON: {"discounts":[{"duration":null,"type":"absolute","source":"Bulk","discountCharge":"oneOff","description":"oneForAll","customData":{"key":"nisVw"},"amount":10},{"duration":null,"type":"absolute","source":"Bulk","discountCharge":"oneOff","description":"justFor5","customData":{"key":"FqWGg"},"amount":5},{"duration":null,"type":"absolute","source":"Bulk","discountCharge":"oneOff","description":"newOne","customData":{"key":"0lyPz"},"amount":1},{"duration":null,"type":"absolute","source":"Bulk","discountCharge":"oneOff","description":"testw","customData":{"key":"3o0Rf"},"amount":20},{"duration":null,"type":"absolute","source":"Bulk","discountCharge":"oneOff","description":"just parent","customData":{"key":"8TZGY"},"amount":0.1},{"duration":null,"customData":{"key":"OdHHl"},"source":"Bulk","amount":0,"description":"newThing","discountCharge":"oneOff","type":"absolute"}]}
	 */
	// public static List<CS_DiscountJsonElement> getDiscountObjects(String jsonStr) {
	// List<CS_DiscountJsonElement> discountList = new List<CS_DiscountJsonElement>();
	//     // Parse entire JSON response.
	//     JSONParser parser = JSON.createParser(jsonStr);
	//     while (parser.nextToken() != null) {
	//         // Start at the array of invoices.
	//         if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
	//             while (parser.nextToken() != null) {
	//                 // Advance to the start object marker to
	//                 //  find next invoice statement object.
	//                 if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
	//                     // Read entire invoice object, including its array of line items.
	//                     CS_DiscountJsonElement discountElement = (CS_DiscountJsonElement)parser.readValueAs(CS_DiscountJsonElement.class);
	//                     discountList.add(discountElement);

	//                     // For debugging purposes, serialize again to verify what was parsed.

	//                     // Skip the child start array and start object markers.
	//                     parser.skipChildren();
	//                 }
	//             }
	//         }
	//     }
	//     return discountList;
	// }

	public static List<cscfga.ProductConfiguration.Discount> getDiscountObjectsOOTBStructure(
		String jsonStr,
		cscfga.ProductConfiguration.Discount discountObjectTmp
	) {
		List<cscfga.ProductConfiguration.Discount> discountList = new List<cscfga.ProductConfiguration.Discount>();
		// Parse entire JSON response.
		System.debug('jsonStr getDiscountObjectsOOTBStructure - ' + jsonStr);
		if (jsonStr == null) {
			return null;
		}
		cscfga.ProductConfiguration.ProductDiscount discountData = (cscfga.ProductConfiguration.ProductDiscount) JSON.deserializeStrict(
			jsonStr,
			cscfga.ProductConfiguration.ProductDiscount.class
		);
		system.debug('dscdata- ' + discountData);

		if (discountData != null && discountData.discounts != null && discountData.discounts.size() > 0) {
			for (cscfga.ProductConfiguration.Discount item : discountData.discounts) {
				discountObjectTmp.evaluationOrder = item.evaluationOrder;
				discountObjectTmp.recordType = item.recordType;
				discountObjectTmp.version = item.version;

				if (item.recordType == 'single') {
					discountList.add(item);
				} else {
					if (item.memberDiscounts != null)
						discountList.addAll(item.memberDiscounts);
				}
			}

			System.debug('discountObjectTmp pop: ' + discountObjectTmp);
		}
		System.debug('Discount List: ' + discountList);
		return discountList;
	}

	public static Boolean DiscountIsValid(cscfga__Product_Configuration__c pc) {
		/* discount is valid if Basket is in Net Profit - TO BE DEFINED 
        or if Discount allowed flag is set on Commercial product
        Since 'Net Profit' basket status isn't defined yet, this covers only Discount allowed condition
        */

		Boolean result = false;

		if (pc.Discount_allowed__c) {
			result = true;
		}
		return result;
	}

	public static List<cscfga.ProductConfiguration.Discount> getNotAllowedDiscounts(
		String currentUserChannel,
		List<cscfga.ProductConfiguration.Discount> discountList,
		cspmb__Price_Item__c commercialProduct
	) {
		system.debug('++++ getNotAllowedDiscounts: ');
		system.debug('++++ getNotAllowedDiscounts commercialProduct: ' + commercialProduct);
		system.debug('++++ getNotAllowedDiscounts currentUserChannel: ' + currentUserChannel);
		List<cscfga.ProductConfiguration.Discount> notAllowedElements = new List<cscfga.ProductConfiguration.Discount>();
		Map<String, Boolean> discountsAllowed = discountTypeAllowed(currentUserChannel, commercialProduct);
		system.debug('++++ discountsAllowed ' + discountsAllowed);
		system.debug('++++ discountList ' + discountList);
		for (cscfga.ProductConfiguration.Discount element : discountList) {
			system.debug('++++ getNotAllowedDiscounts element: ' + element);
			system.debug('++++ discountsAllowed.containsKey(element.chargeType): ' + discountsAllowed.containsKey(element.chargeType));
			if (discountsAllowed.containsKey(element.chargeType) && !discountsAllowed.get(element.chargeType)) {
				system.debug('++++ getNotAllowedDiscounts element: entered if in for loop ');
				notAllowedElements.add(element);
			}
		}
		return notAllowedElements;
	}

	public static List<cscfga.ProductConfiguration.Discount> getNotAllowedDiscountsAddon(
		String currentUserChannel,
		List<cscfga.ProductConfiguration.Discount> discountList,
		cspmb__Add_On_Price_Item__c addon
	) {
		system.debug('++++ getNotAllowedDiscountsAddon: ');
		system.debug('++++ getNotAllowedDiscountsAddon addon: ' + addon.Id);
		system.debug('++++ getNotAllowedDiscountsAddon currentUserChannel: ' + currentUserChannel);
		List<cscfga.ProductConfiguration.Discount> notAllowedElements = new List<cscfga.ProductConfiguration.Discount>();
		Map<String, Boolean> discountsAllowed = discountTypeAllowedAddon(currentUserChannel, addon);
		for (cscfga.ProductConfiguration.Discount element : discountList) {
			system.debug('++++ getNotAllowedDiscountsAddon element: ' + element);
			if (discountsAllowed.containsKey(element.chargeType) && !discountsAllowed.get(element.chargeType)) {
				notAllowedElements.add(element);
			}
		}
		return notAllowedElements;
	}

	/**
	 * ONEOFF = oneOff
	 * RECCURRING = recurring
	 *
	 */
	public static Map<String, Boolean> discountTypeAllowed(String basketSalesChannel, cspmb__Price_Item__c commercialProduct) {
		Map<String, Boolean> result = new Map<String, Boolean>();
		Set<String> oneOffSalesChannels = new Set<String>();
		Set<String> recurringSalesChannels = new Set<String>();

		if (commercialProduct.OneOff_Discount_Sales_Channel__c != null) {
			oneOffSalesChannels = new Set<String>(commercialProduct.OneOff_Discount_Sales_Channel__c.split(';'));
		}
		if (commercialProduct.Recurring_Discount_Sales_Channel__c != null) {
			recurringSalesChannels = new Set<String>(commercialProduct.Recurring_Discount_Sales_Channel__c.split(';'));
		}
		//typeOfDiscount == 'oneOff
		Boolean oneOffDiscountAllowed = false;
		if (oneOffSalesChannels != null && oneOffSalesChannels.contains(basketSalesChannel)) {
			if (commercialProduct.One_Off_Product_Discount_Allowed__c) {
				oneOffDiscountAllowed = true;
			}
		}

		result.put('oneOff', oneOffDiscountAllowed);

		//typeOfDiscount == 'recurring'
		Boolean recurringDiscountAllowed = false;
		if (recurringSalesChannels != null && recurringSalesChannels.contains(basketSalesChannel)) {
			if (commercialProduct.Recurring_Product_Discount_Allowed__c) {
				recurringDiscountAllowed = true;
			}
		}
		result.put('recurring', recurringDiscountAllowed);
		return result;
	}

	public static Map<String, Boolean> discountTypeAllowedAddon(String basketSalesChannel, cspmb__Add_On_Price_Item__c addon) {
		Map<String, Boolean> result = new Map<String, Boolean>();
		Set<String> oneOffSalesChannels = new Set<String>();
		Set<String> recurringSalesChannels = new Set<String>();

		if (addon.OneOff_Discount_Sales_Channel__c != null) {
			oneOffSalesChannels = new Set<String>(addon.OneOff_Discount_Sales_Channel__c.split(';'));
		}
		if (addon.Recurring_Discount_Sales_Channel__c != null) {
			recurringSalesChannels = new Set<String>(addon.Recurring_Discount_Sales_Channel__c.split(';'));
		}
		//typeOfDiscount == 'oneOff
		Boolean oneOffDiscountAllowed = false;
		if (oneOffSalesChannels != null && oneOffSalesChannels.contains(basketSalesChannel)) {
			if (addon.One_Off_Product_Discount_Allowed__c) {
				oneOffDiscountAllowed = true;
			}
		}

		result.put('oneOff', oneOffDiscountAllowed);

		//typeOfDiscount == 'recurring'
		Boolean recurringDiscountAllowed = false;
		if (recurringSalesChannels != null && recurringSalesChannels.contains(basketSalesChannel)) {
			if (addon.Recurring_Product_Discount_Allowed__c) {
				recurringDiscountAllowed = true;
			}
		}
		result.put('recurring', recurringDiscountAllowed);
		return result;
	}

	public static List<cscfga.ProductConfiguration.Discount> removeNotAllowedDiscounts(
		List<cscfga.ProductConfiguration.Discount> discountList,
		List<cscfga.ProductConfiguration.Discount> notAllowedDiscounts
	) {
		if (notAllowedDiscounts != null && notAllowedDiscounts.size() > 0) {
			List<cscfga.ProductConfiguration.Discount> resultList = new List<cscfga.ProductConfiguration.Discount>();

			for (cscfga.ProductConfiguration.Discount discountOrigin : discountList) {
				Boolean contains = false;
				for (cscfga.ProductConfiguration.Discount notAllowed : notAllowedDiscounts) {
					if ((notAllowed.chargeType == discountOrigin.chargeType) && (notAllowed.amount == discountOrigin.amount)) {
						contains = true;
					}
				}

				if (!contains) {
					resultList.add(discountOrigin);
				}
			}
			return resultList;
		} else {
			return discountList;
		}
	}
}
