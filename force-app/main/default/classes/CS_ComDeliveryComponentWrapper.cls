public with sharing class CS_ComDeliveryComponentWrapper {
    public CS_ComDeliveryComponentWrapper() {

    }
    @AuraEnabled
    public String deliveryComponentId;

    @AuraEnabled
    public String productName;

    @AuraEnabled
    public String componentName;

    @AuraEnabled
    public String articleName;

    @AuraEnabled
    public String articleCode;

    @AuraEnabled
    public String replacedArticleCode;

    @AuraEnabled
    public String macdAction;

    @AuraEnabled
    public String serviceId;

    @AuraEnabled
    public Boolean installed;

    @AuraEnabled
    public Datetime installedDate;

    @AuraEnabled
    public Boolean tested;

    @AuraEnabled
    public Datetime testedDate;

    @AuraEnabled
    public Boolean implemented;

    @AuraEnabled
    public Datetime implementedDate;

    @AuraEnabled
    public Boolean activated;

    @AuraEnabled
    public Datetime activatedDate;
    
    @AuraEnabled
    public Boolean editable;

    @AuraEnabled
    public String externalServiceId;
    
    @AuraEnabled
    public String cdoServiceId;

    @AuraEnabled
    public List<CS_ComDeliveryComponentWrapperDetail> details;

    @AuraEnabled
    public List<CS_ComDeliveryComponentWrapperDetail> replacedDetails;

    @AuraEnabled
    public List<CS_ComPortingDetailsWrapper> portingDetails;

    @AuraEnabled
    public List<CS_ComDependantDataWrapper> dependantDataDetails;
}