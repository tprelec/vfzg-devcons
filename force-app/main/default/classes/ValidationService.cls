public with sharing class ValidationService {
	@InvocableMethod(
		label='Validate records'
		description='Validates records for a specific context.'
		category='Validation'
	)
	public static List<ValidationResponse> validate(List<ValidationRequest> requests) {
		List<ValidationResponse> responses = new List<ValidationResponse>();
		for (ValidationRequest request : requests) {
			responses.add(validate(request));
		}
		return responses;
	}

	/**
	 * Validate record for a specific context.
	 * @param  request Validation Request.
	 * @return         Validation Response.
	 */
	public static ValidationResponse validate(ValidationRequest request) {
		ValidationResponse response;
		String objectName = request.recordId.getSobjectType().getDescribe().getName().toLowerCase();
		if (objectName == 'opportunity') {
			if (request.context == 'ORDER') {
				String error = OrderValidation.checkCompleteOpportunities(
					null,
					new Set<Id>{ request.recordId }
				);
				if (String.isBlank(error)) {
					response = new ValidationResponse(true);
				} else {
					response = new ValidationResponse(false, new List<String>{ error });
				}
			}
		}
		return response;
	}

	/**
	 * Represents Validation Request.
	 * Consists of the Context in which Validation will be executed
	 * (e.g. Order) and Record that is validated.
	 */
	public class ValidationRequest {
		@InvocableVariable(
			label='Validation Context'
			description='Context in which record will be validated.'
			required=true
		)
		public String context;

		@InvocableVariable(
			label='Record ID'
			description='ID of the record that will be validated.'
			required=true
		)
		public Id recordId;

		public ValidationRequest() {
		}

		public ValidationRequest(String context, Id recordId) {
			this.context = context;
			this.recordId = recordId;
		}
	}

	/**
	 * Represents Validation Response.
	 */
	public class ValidationResponse {
		@InvocableVariable(label='Is Valid' description='Defines if record is valid' required=true)
		public Boolean isValid;
		@InvocableVariable(label='Errors' description='List of validation errors' required=false)
		public List<String> errors;

		public ValidationResponse(Boolean isValid) {
			this(isValid, new List<String>());
		}

		public ValidationResponse(Boolean isValid, List<String> errors) {
			this.isValid = isValid;
			this.errors = errors;
		}
	}
}