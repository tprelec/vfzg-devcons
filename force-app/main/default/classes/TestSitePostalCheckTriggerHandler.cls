/**
 * @description			This class is responsible for testing the postcodecheck triggerhandler
 * @author				Guy Clairbois
 */
@isTest
private class TestSitePostalCheckTriggerHandler {
	static testMethod void TestCreateNewPostalCheck(){
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);		

		// create mappings
		TestUtils.createPortFolioVendorMappings();
		TestUtils.createPreferredSuppliers();			
        TestUtils.createSalesSettings();
		TestUtils.createInfrastructureMapping();

        Preferred_Supplier__c ps = [Select Id, Vendor__c, Technology_Type__c, Order__c From Preferred_Supplier__c WHERE Technology_Type__c = 'ADSL' LIMIT 1];
		system.debug(ps);
        Test.startTest();

		// create default site
		Site__c s = TestUtils.createSite(acct);	

		Site_Postal_Check__c spc = new Site_Postal_Check__c();
		spc.Access_Site_ID__c = s.Id;
		spc.Technology_Type__c = ps.Technology_Type__c;
		spc.Access_Vendor__c = ps.Vendor__c;
		insert spc;

		spc.Access_Max_Down_Speed__c = 1000;
		spc.Access_Max_Up_Speed__c = 100;
		update spc;
		
		Test.stopTest();
		List<Site_Postal_Check__c> pcs = [Select Id,Order__c,Preferred_Supplier_Reference__c,Access_Infrastructure__c from Site_Postal_Check__c where Id = :spc.Id];
		// check if the right order is assigned via the preferred supllier
		system.debug(pcs);
		System.assertEquals(pcs[0].Order__c,ps.Order__c);	
	}
}