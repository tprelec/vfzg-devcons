public with sharing class OrderEntryDiscountTriggerHandler extends TriggerHandler {
	private List<OE_Discount__c> newDiscounts;
	private Map<Id, OE_Discount__c> newDiscountsMap;
	private Map<Id, OE_Discount__c> oldDiscountsMap;

	private void init() {
		newDiscounts = (List<OE_Discount__c>) this.newList;
		newDiscountsMap = (Map<Id, OE_Discount__c>) this.newMap;
		oldDiscountsMap = (Map<Id, OE_Discount__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	public void setExternalIds() {
		Sequence seq = new Sequence('OE_Discount');

		if (seq.canBeUsed()) {
			seq.startMutex();
			for (OE_Discount__c d : newDiscounts) {
				d.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}