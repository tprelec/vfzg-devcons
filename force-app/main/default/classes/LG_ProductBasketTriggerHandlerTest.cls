@isTest
private class LG_ProductBasketTriggerHandlerTest {

	private static String accWithBillAccName = 'SFDT-59 W-Def';
	private static String accWithoutBillAccName = 'SFDT-59 Wo-Def';
	private static String defaultBillingAccountName = 'SFDT-59 Bill1';

	@testsetup
	private static void setupTestData() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		Framework__c frameworkSetting = new Framework__c();
	    frameworkSetting.Framework_Sequence_Number__c = 2;
	    insert frameworkSetting;

		List<Account> accounts = new List<Account>();
		Account accWithDefaultBill = LG_GeneralTest.CreateAccount(accWithBillAccName, '12345678', 'Ziggo', false);
		Account accWithoutDefaultBill = LG_GeneralTest.CreateAccount(accWithoutBillAccName, '12345679', 'Ziggo', false);
		accounts.add(accWithoutDefaultBill);
		accounts.add(accWithDefaultBill);
		insert accounts;

		List<csconta__Billing_Account__c> billAccounts = new List<csconta__Billing_Account__c>();
		billAccounts.add(LG_GeneralTest.createBillingAccount(defaultBillingAccountName, accWithDefaultBill.Id, true, false));
		billAccounts.add(LG_GeneralTest.createBillingAccount('SFDT-59 Bill2', accWithDefaultBill.Id, false, false));
		billAccounts.add(LG_GeneralTest.createBillingAccount('SFDT-59 Bill3', accWithoutDefaultBill.Id, false, false));
		insert billAccounts;

		List<Opportunity> opps = new List<Opportunity>();
		Opportunity oppWDefault = LG_GeneralTest.CreateOpportunity(accWithDefaultBill, false);
		Opportunity oppWoDefault = LG_GeneralTest.CreateOpportunity(accWithoutDefaultBill, false);
		opps.add(oppWDefault);
		opps.add(oppWoDefault);
		insert opps;

		// vodafone Oppty
		Account account = LG_GeneralTest.CreateAccount('Account Vodafone', '28289200', 'Vodafone', true);
		Account accountZiggo = LG_GeneralTest.CreateAccount('Account Ziggo', '28289201', 'Ziggo', true);

		Opportunity opp = new Opportunity();
		opp.Name = 'Vodafone Oppty 1';
		opp.Account = account;
		opp.AccountId = account.Id;
		opp.StageName = 'Qualification';
		opp.CloseDate = system.today();
		opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('MAC').getRecordTypeId();
		insert opp;

		Opportunity oppZiggo = new Opportunity();
		oppZiggo.Name = 'Vodafone Oppty 1';
		oppZiggo.Account = accountZiggo;
		oppZiggo.AccountId = accountZiggo.Id;
		oppZiggo.StageName = 'Qualification';
		oppZiggo.CloseDate = system.today();
		oppZiggo.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ziggo').getRecordTypeId();
		insert oppZiggo;


		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('TestBasket', accountZiggo, null, oppZiggo, false);
		basket.csordtelcoa__Change_Type__c = 'Terminate';
		//basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
		insert basket;

		cscfga__Product_Category__c prodCategory = LG_GeneralTest.createProductCategory('Termination', true);

		// Prepare Product Configurations with Total Price
		cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('Penalty Fee', false);
		prodDef.cscfga__Product_Category__c = prodCategory.Id;
		insert prodDef;

		List<cscfga__Product_Configuration__c> prodConfs = new List<cscfga__Product_Configuration__c>();
		cscfga__Product_Configuration__c pc = LG_GeneralTest.createProductConfiguration('ProdConf58', 3, basket, prodDef, false);
		pc.cscfga__total_one_off_charge__c = 1000;
		pc.cscfga__Total_Price__c = 1000;
		pc.cscfga__Unit_Price__c = 1000;

		//prodConfs.add(pc);
		insert pc;

		cscfga__Attribute_Definition__c attributeDefinition = LG_GeneralTest.createAttributeDefinition('Test definition', prodDef, 'Calculation', 'Decimal', '', 'Main OLI', '');
		cscfga__Attribute_Definition__c attributeDefinitionOp = LG_GeneralTest.createAttributeDefinition('Overridden Price', prodDef, 'Calculation', 'Decimal', '', 'Main OLI', '');
		cscfga__Attribute_Definition__c attributeDefinitionCp= LG_GeneralTest.createAttributeDefinition('Calculated Price', prodDef, 'Calculation', 'Decimal', '', 'Main OLI', '');

		cscfga__Attribute__c a = LG_GeneralTest.createAttribute('Overridden Price', attributeDefinition, true, 0, pc, false, '2000', false);
		cscfga__Attribute__c c = LG_GeneralTest.createAttribute('Calculated Price', attributeDefinition, true, 0, pc, false, '2000', false);

		a.cscfga__Price__c = 2000;
		c.cscfga__Price__c = 2000;

		List<cscfga__Attribute__c> attrs = new List<cscfga__Attribute__c> { a, c };
		insert attrs;

		OrderType__c newOrderType = CS_DataTest.createOrderType();
		newOrderType.Name = 'Ziggo';
		insert newOrderType;

		noTriggers.Flag__c = false;
		upsert noTriggers;
	}

	/*private static testmethod void testVodafoneOppty() {

		Account acct = [SELECT Id, Name FROM Account WHERE Name = 'Account Vodafone'];
		Opportunity oppWith = [SELECT Id, AccountId FROM Opportunity WHERE AccountId = :acct.Id];

		Test.startTest();

		cscfga__Product_Basket__c basket2 = LG_GeneralTest.createProductBasket('Basket2', acct, null, oppWith, false);
		insert basket2;

		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', acct, null, oppWith, false);
		basket.Block_Expiration__c = false;
		basket.Contract_duration_Mobile__c = '12';
		basket.Contract_duration_Fixed__c = 12;
		basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 6, 16);
		basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 6, 16);
		basket.Block_Expiration__c = true;
		basket.cscfga__Basket_Status__c = 'Pending approval';
		basket.Basket_Approval_Status__c = 'Draft';
		basket.NetProfit_Approval_Status__c = 'Draft';
		basket.KPI_JSON_Values__c = '[{"taxonomy":null,"salesMarginDivNetRevenue":2.00,"paybackPeriod":0.00,"NPV":0.00,"netIncrementalBilledRevenue":0.00,"freeCashFlowDivNetRevenue":2.00,"ebitdaDivNetRevenue":2.00,"category":"Total"},{"taxonomy":null,"salesMarginDivNetRevenue":0.00,"paybackPeriod":0.00,"NPV":0.00,"netIncrementalBilledRevenue":0.00,"freeCashFlowDivNetRevenue":0.00,"ebitdaDivNetRevenue":0.00,"category":"Mobile Voice Usage"},{"taxonomy":null,"salesMarginDivNetRevenue":2.00,"paybackPeriod":0.00,"NPV":0.00,"netIncrementalBilledRevenue":0.00,"freeCashFlowDivNetRevenue":2.00,"ebitdaDivNetRevenue":2.00,"category":"RedPro"}]';
		insert basket;

		basket.Block_Expiration__c = true;
		basket.Contract_duration_Mobile__c = '24';
		basket.Basket_Approval_Status__c = 'Pending';
		basket.NetProfit_Approval_Status__c = 'Pending';
		basket.cscfga__Basket_Status__c = 'Valid';
		basket.KPI_JSON_Values__c = '[{"taxonomy":null,"salesMarginDivNetRevenue":2.00,"paybackPeriod":0.00,"NPV":1.00,"netIncrementalBilledRevenue":0.00,"freeCashFlowDivNetRevenue":2.00,"ebitdaDivNetRevenue":2.00,"category":"Total"},{"taxonomy":null,"salesMarginDivNetRevenue":1.00,"paybackPeriod":0.00,"NPV":0.00,"netIncrementalBilledRevenue":0.00,"freeCashFlowDivNetRevenue":0.00,"ebitdaDivNetRevenue":0.00,"category":"Mobile Voice Usage"},{"taxonomy":null,"salesMarginDivNetRevenue":2.00,"paybackPeriod":0.00,"NPV":0.00,"netIncrementalBilledRevenue":1.00,"freeCashFlowDivNetRevenue":2.00,"ebitdaDivNetRevenue":3.00,"category":"RedPro"}]';
		update basket;
		Test.stopTest();
	}*/

	/*private static testmethod void testApprovalStatusChangesApproved() {
		Account acct = [SELECT Id, Name FROM Account WHERE Name = 'Account Vodafone'];
		Opportunity oppWith = [SELECT Id, AccountId FROM Opportunity WHERE AccountId = :acct.Id];

		Test.startTest();
		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', acct, null, oppWith, false);
		basket.Block_Expiration__c = false;
		basket.Contract_duration_Mobile__c = '12';
		basket.Contract_duration_Fixed__c = 12;
		basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 6, 16);
		basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 6, 16);
		basket.Block_Expiration__c = true;
		basket.Basket_Approval_Status__c = 'Draft';
		basket.NetProfit_Approval_Status__c = 'Draft';
		insert basket;

		Product_Basket_Attachment__c pba = new Product_Basket_Attachment__c();
		pba.Attachment_Type__c = 'Customer email';
		pba.ContentDocumentID__c = '';
		pba.Description_Optional__c = 'this is optional';
		pba.Product_Basket__c = basket.Id;
		insert pba;

		basket.Block_Expiration__c = true;
		basket.Contract_duration_Mobile__c = '24';
		basket.Basket_Approval_Status__c = 'Approved';
		basket.NetProfit_Approval_Status__c = 'Approved';
		update basket;
		Test.stopTest();
	}*/

	private static testmethod void testApprovalStatusChangesRejected() {
		Account acct = [SELECT Id, Name FROM Account WHERE Name = 'Account Vodafone'];
		Opportunity oppWith = [SELECT Id, AccountId FROM Opportunity WHERE AccountId = :acct.Id];

		Test.startTest();
		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', acct, null, oppWith, false);
		basket.Block_Expiration__c = false;
		basket.Contract_duration_Mobile__c = '12';
		basket.Contract_duration_Fixed__c = 12;
		basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 6, 16);
		basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 6, 16);
		basket.Block_Expiration__c = true;
		basket.Basket_Approval_Status__c = 'Draft';
		basket.NetProfit_Approval_Status__c = 'Draft';
		insert basket;
		basket.Block_Expiration__c = true;
		basket.Contract_duration_Mobile__c = '24';
		basket.Basket_Approval_Status__c = 'Rejected';
		basket.NetProfit_Approval_Status__c = 'Rejected';
		update basket;
		Test.stopTest();
	}

	private static testmethod void testApprovalStatusChangesRecalled() {
		Account acct = [SELECT Id, Name FROM Account WHERE Name = 'Account Vodafone'];
		Opportunity oppWith = [SELECT Id, AccountId FROM Opportunity WHERE AccountId = :acct.Id];

		Test.startTest();
		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', acct, null, oppWith, false);
		basket.Block_Expiration__c = false;
		basket.Contract_duration_Mobile__c = '12';
		basket.Contract_duration_Fixed__c = 12;
		basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 6, 16);
		basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 6, 16);
		basket.Block_Expiration__c = true;
		basket.Basket_Approval_Status__c = 'Draft';
		basket.NetProfit_Approval_Status__c = 'Draft';
		insert basket;
		basket.Block_Expiration__c = true;
		basket.Contract_duration_Mobile__c = '24';
		basket.Basket_Approval_Status__c = 'Recalled';
		basket.NetProfit_Approval_Status__c = 'Recalled';
		update basket;
		Test.stopTest();
	}

	private static testmethod void testDeleteBasketZiggo() {
		Account acct = [SELECT Id, Name FROM Account WHERE Name = 'Account Vodafone'];
		Opportunity oppWith = [SELECT Id, AccountId FROM Opportunity WHERE AccountId = :acct.Id];

		Test.startTest();
		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', acct, null, oppWith, false);
		insert basket;
		delete basket;
		Test.stopTest();
	}

	private static testmethod void testUpdateBasketZiggo() {
		Account acct = [SELECT Id, Name FROM Account WHERE Name = 'Account Ziggo'];
		Opportunity oppWith = [SELECT Id, AccountId FROM Opportunity WHERE AccountId = :acct.Id];

		Test.startTest();
		cscfga__Product_Basket__c basket2 = LG_GeneralTest.createProductBasket('Basket2', acct, null, oppWith, false);
		insert basket2;
		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', acct, null, oppWith, false);
		basket.Block_Expiration__c = false;
		insert basket;
		basket.Name = 'Basket Ziggo';
		basket.Block_Expiration__c = true;
		update basket;
		Test.stopTest();
	}

	private static testmethod void testPopulateTheCustomerAccountField() {
		Account accWithBillAcc = [SELECT Id, Name FROM Account WHERE Name = :accWithBillAccName];
		Opportunity oppWith = [SELECT Id, AccountId FROM Opportunity WHERE AccountId = :accWithBillAcc.Id];

		Test.startTest();

		cscfga__Product_Basket__c basketWithDefaultBillAcc = LG_GeneralTest.createProductBasket('WithBillAcc', accWithBillAcc, null, oppWith, true);

		Test.stopTest();

		basketWithDefaultBillAcc = [SELECT Id, csbb__Account__r.Id FROM cscfga__Product_Basket__c WHERE Id = :basketWithDefaultBillAcc.Id];

		System.assertEquals(accWithBillAcc.Id, basketWithDefaultBillAcc.csbb__Account__r.Id, 'Basket should have the customer account field populated');
	}

	private static testmethod void testStDefaultBillingAccount() {
		List<Account> accounts = [SELECT Id, Name FROM Account WHERE Name = :accWithoutBillAccName OR Name = :accWithBillAccName];
		Account accWithBillAcc;
		Account accWithoutBillAcc;

		for (Account account : accounts) {
			if (account.Name.equals(accWithBillAccName)) {
				accWithBillAcc = account;
			} else if (account.Name.equals(accWithoutBillAccName)) {
				accWithoutBillAcc = account;
			}
		}

		List<Opportunity> opps = [SELECT Id, AccountId FROM Opportunity WHERE AccountId = :accWithBillAcc.Id OR AccountId = :accWithoutBillAcc.Id];
		Opportunity oppWith;
		Opportunity oppWo;

		for (Opportunity opp : opps) {
			if (opp.AccountId.equals(accWithBillAcc.Id)) {
				oppWith = opp;
			} else if (opp.AccountId.equals(accWithoutBillAcc.Id)) {
				oppWo = opp;
			}
		}

		Test.startTest();

		List<cscfga__Product_Basket__c> baskets = new List<cscfga__Product_Basket__c>();
		cscfga__Product_Basket__c basketWithDefaultBillAcc = LG_GeneralTest.createProductBasket('WithBillAcc', accWithBillAcc, null, oppWith, false);
		cscfga__Product_Basket__c basketWoDefaultBillAcc = LG_GeneralTest.createProductBasket('WithoutBillAcc', accWithoutBillAcc, null, oppWo, false);
		insert baskets;

		Test.stopTest();

		baskets = [SELECT Id, LG_BillingAccount__c, LG_BillingAccount__r.csconta__Financial_Account_Number__c FROM cscfga__Product_Basket__c WHERE Id IN :baskets];
		for (cscfga__Product_Basket__c basket : baskets) {
			if (basket.Name.equals('WithBillAcc')) {
				System.assert(basket.LG_BillingAccount__c != null, 'Billing Account should be populated');
				System.assertEquals(defaultBillingAccountName, basket.LG_BillingAccount__r.csconta__Financial_Account_Number__c, 'Billing Account should be SFDT-59 Bill1');
			} else if (basket.Name.equals('WithoutBillAcc')) {
				System.assert(basket.LG_BillingAccount__c == null, 'Billing Account should not be populated');
			}
		}
	}

	private static testmethod void testCalculateCreatePenaltyFee() {

		cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Name = 'TestBasket'];

		Test.startTest();
		basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
		update basket;
		Test.stopTest();

	}
}