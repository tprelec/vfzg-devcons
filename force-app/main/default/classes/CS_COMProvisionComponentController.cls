public with sharing class CS_COMProvisionComponentController {
    @AuraEnabled
    public static String closeTask(Id recordId) {
        String result = '';
        List<Task> tasksToUpdate = new List<Task>();

        tasksToUpdate.add(new Task(
            Id = recordId,
            Status = 'Completed',
            Delivery_Task_Finished__c = true
        ));

        try {
            Database.SaveResult[] updateResult = Database.update(tasksToUpdate);
            result = 'Task Closed';
        } catch (Exception e) {
            return e.getMessage();
        }

        return result;
    }
}