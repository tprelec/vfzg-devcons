@isTest
private class TestCOM_DXL_BSSListenerService {
	@testSetup
	static void setup() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);

		Com_dxl_integration_log__c comdxlintegrationlogcObj = new Com_dxl_integration_log__c(
			Transaction_Id__c = 'TR-123456',
			Type__c = 'createCustomer',
			Status__c = 'Success',
			Affected_Objects__c = 'Test Value',
			Error__c = 'Test Value'
		);
		insert comdxlintegrationlogcObj;

		Com_dxl_integration_log__c integrationLogBSSOrder = new Com_dxl_integration_log__c(
			Transaction_Id__c = 'TR-123456',
			Type__c = 'createBSSOrder',
			Status__c = 'Success',
			Affected_Objects__c = 'Test Value',
			Error__c = 'Test Value'
		);
		insert integrationLogBSSOrder;

		List<External_Account__c> externalAccounts = new List<External_Account__c>();

		External_account__c externalaccountcObj1 = new External_account__c(Account__c = acct.Id, External_Source__c = 'Unify');

		External_account__c externalaccountcObj2 = new External_account__c(Account__c = acct.Id, External_Source__c = 'BOP');

		externalAccounts.add(externalaccountcObj1);
		externalAccounts.add(externalaccountcObj2);

		insert externalAccounts;

		Opportunity testOpp = CS_DataTest.createOpportunity(acct, 'Test Opp', UserInfo.getUserId());
		testOpp.csordtelcoa__Change_Type__c = 'Change';
		testOpp.Name = 'testOpp';
		testOpp.StageName = 'Ready for Order';
		testOpp.CloseDate = System.today();
		testOpp.AccountId = acct.Id;
		insert testOpp;

		cscfga__Product_Basket__c testBasket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
		testBasket.Name = 'testBasket';
		testBasket.cscfga__Opportunity__c = testOpp.Id;
		insert testBasket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c businessInternetDef = CS_DataTest.createProductDefinition('Business Internet');
		businessInternetDef.RecordTypeId = productDefinitionRecordType;
		businessInternetDef.Product_Type__c = 'Fixed';
		insert businessInternetDef;

		cscfga__Product_Configuration__c businessInternetConf = CS_DataTest.createProductConfiguration(
			businessInternetDef.Id,
			'Business Internet',
			testBasket.Id
		);
		insert businessInternetConf;

		csord__Order__c order = new csord__Order__c();
		order.csord__Identification__c = 'Order_' + CS_DataTest.generateRandomString(6);
		insert order;

		Site__c site = CS_DataTest.createSite('Utrecht Lestraat 12', acct, '3572RE', 'Lestraat', 'Utrecht', 12);
		site.Footprint__c = null;
		site.Site_House_Number_Suffix__c = 'A';
		site.Country__c = 'NL';
		insert site;

		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', order.Id, false);
		insert deliveryOrder;

		csord__Subscription__c businessInternetSub = CS_DataTest.createSubscription(businessInternetConf.Id);
		businessInternetSub.csord__Identification__c = 'Business_Internet_Sub_' + CS_DataTest.generateRandomString(6);
		businessInternetSub.csord__Status__c = 'Subscription created';
		businessInternetSub.csord__Order__c = order.Id;
		insert businessInternetSub;

		csord__Service__c businessInternetService = CS_DataTest.createService(
			order.Id,
			deliveryOrder.Id,
			'Business Internet',
			testBasket.Id,
			businessInternetSub.Id,
			site.Id,
			null,
			businessInternetConf.Id
		);
		businessInternetService.csord__Status__c = 'Active';
		insert businessInternetService;

		csord__Service_Line_Item__c businessInternetSLIOneOff = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet One Off SLI',
			businessInternetService.Id,
			false,
			0,
			'Charge',
			'Internet SLI OF'
		);
		insert businessInternetSLIOneOff;
		csord__Service_Line_Item__c businessInternetSLIRec = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet Recurring SLI',
			businessInternetService.Id,
			true,
			0,
			'Charge',
			'Internet SLI REC'
		);
		insert businessInternetSLIRec;
	}

	@isTest
	static void testgetIntegrationLog() {
		String logId = 'TR-123456';
		List<Com_dxl_integration_log__c> integrationLogRecordList = COM_DXL_BSSListenerService.getIntegrationLog(logId);

		System.assertEquals(
			logId,
			integrationLogRecordList[0].Transaction_Id__c,
			'Expected: ' +
			logId +
			' Actual: ' +
			integrationLogRecordList[0].Transaction_Id__c
		);
	}

	@isTest
	static void testgetLatestIntegrationLog() {
		Com_dxl_integration_log__c integrationLogRecord = COM_DXL_BSSListenerService.getLatestIntegrationLog('createBSSOrder');

		System.assertEquals(
			'createBSSOrder',
			integrationLogRecord.Type__c,
			'Expected: createBSSOrder ' +
			' Actual: ' +
			integrationLogRecord.Transaction_Id__c
		);
	}

	@isTest
	static void testorderLineStatusSuccess() {
		String payload = COM_DXLNotificationListenerService.generateAsyncReponse('COM_DXL_CREATE_BSS_ORDER_ASYNC_RESPONSE');
		COM_DXLCreateBSSOrderAsyncResponse response = (COM_DXLCreateBSSOrderAsyncResponse) JSON.deserialize(
			payload,
			COM_DXLCreateBSSOrderAsyncResponse.class
		);

		Boolean result = COM_DXL_BSSListenerService.orderLineStatusSuccess(response.payload.orderLines[0]);

		System.assertEquals(true, result, 'Expected: true ' + ' Actual: ' + result);
	}
}
