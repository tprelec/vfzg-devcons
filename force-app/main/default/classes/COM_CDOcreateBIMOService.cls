public class COM_CDOcreateBIMOService {
	public String externalId; //COM
	public String priority; //1
	public String description; //Internet Modem Only Service Order
	public String category; //Internet Modem Only Service
	public String type; //standard
	public OrderItem[] orderItem;
	public class OrderItem {
		public String id; //1
		public String action; //add
		public String type; //standard
		public Service service;
	}
	class Service {
		public String type; //Service
		public ServiceSpecification serviceSpecification;
		public ServiceCharacteristic[] serviceCharacteristic;
	}
	class ServiceSpecification {
		public String id; //bdab731e-d5de-4135-8849-5c724ea01041
		public String invariantID; //930d7161-0eb8-444f-8d11-5971812246d5
		public String version; //1.0.0
		public String name; //Internet Modem Only Service
		public String type; //ServiceSpecification
	}
	class ServiceCharacteristic {
		public String name; //sourcesystem
		public String valueType; //string
		public GenValue value;
	}
	class GenValue {
		public String type; //string
		public String valueOf; //SF
	}
	public static COM_CDOcreateBIMOService parse(String json) {
		return (COM_CDOcreateBIMOService) System.JSON.deserialize(json, COM_CDOcreateBIMOService.class);
	}
}
