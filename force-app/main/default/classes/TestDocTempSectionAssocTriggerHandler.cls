/**
 * @description       : Apex Test Class for the DocTempSectionAssocTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   20-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestDocTempSectionAssocTriggerHandler {
	@testSetup
	static void setup() {
		List<ExternalIDNumber__c> lstExternalIDNumbers = new List<ExternalIDNumber__c>();

		ExternalIDNumber__c objExternalIDNumberDTSA = new ExternalIDNumber__c(
			Name = 'Document Template/Section Ass',
			External_Number__c = 1,
			Object_Prefix__c = 'DTSA-15-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		lstExternalIDNumbers.add(objExternalIDNumberDTSA);

		ExternalIDNumber__c objExternalIDNumberSD = new ExternalIDNumber__c(
			Name = 'Section Definition',
			External_Number__c = 1,
			Object_Prefix__c = 'SD-20-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		lstExternalIDNumbers.add(objExternalIDNumberSD);

		insert lstExternalIDNumbers;

		csclm__Document_Definition__c objDocumentDefinition = CS_DataTest.createDocumentDefinition();
		insert objDocumentDefinition;

		csclm__Document_Template__c objDocumentTemplate = CS_DataTest.createDocumentTemplate('Test');
		objDocumentTemplate.csclm__Document_Definition__c = objDocumentDefinition.Id;
		objDocumentTemplate.csclm__Valid__c = true;
		objDocumentTemplate.csclm__Active__c = true;
		insert objDocumentTemplate;

		csclm__Section_Definition__c objSectionDefinition = new csclm__Section_Definition__c();
		objSectionDefinition.csclm__Section_Name__c = 'Test';
		objSectionDefinition.Start_on_new_page__c = true;
		insert objSectionDefinition;
	}

	@isTest
	static void testSetExternalIds() {
		csclm__Document_Template__c objDocumentTemplate = [SELECT Id FROM csclm__Document_Template__c LIMIT 1];
		csclm__Section_Definition__c objSectionDefinition = [SELECT Id FROM csclm__Section_Definition__c LIMIT 1];

		csclm__Document_Template_Section_Association__c objDocumentTemplateSectionAssociation = new csclm__Document_Template_Section_Association__c();
		objDocumentTemplateSectionAssociation.csclm__Document_Template__c = objDocumentTemplate.Id;
		objDocumentTemplateSectionAssociation.csclm__Section_Definition__c = objSectionDefinition.Id;

		Test.startTest();
		insert objDocumentTemplateSectionAssociation;
		Test.stopTest();

		csclm__Document_Template_Section_Association__c objDocumentTemplateSectionAssociationUpdated = [
			SELECT Id, ExternalID__c
			FROM csclm__Document_Template_Section_Association__c
			WHERE Id = :objDocumentTemplateSectionAssociation.Id
			LIMIT 1
		];

		System.assertEquals(
			'DTSA-15-000002',
			objDocumentTemplateSectionAssociationUpdated.ExternalID__c,
			'The External ID on Document Template / Section Association was not set correctly.'
		);
	}
}
