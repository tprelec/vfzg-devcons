@isTest
private class TestSiteManagerController {
	
	@isTest static void test_method_one() {
		Account a = TestUtils.createAccount(null);
		Site__c s = TestUtils.createSite(a);
		
		List<PBX_Type__c> pbxs = new List<PBX_Type__c>();
		
		for (Integer i=0; i<5; i++) {
			PBX_Type__c pbx = new PBX_Type__c(Office_Voice_approved__c='G.711',Vendor__c='testVendor',Product_Name__c='testProduct'+i,Software_Version__c='1.'+i);	
			pbxs.add(pbx);
		}
		insert pbxs;

		Competitor_Asset__c ca = new Competitor_Asset__c();
		ca = PBXSelectionWizardController.createAsset(s,ca,pbxs[0].id);
		insert ca;		

		TestUtils.createSalesSettings();

		Test.startTest();

        PageReference pageRef = Page.SiteManager;
        Test.setCurrentPage(pageRef);
    	System.currentPageReference().getParameters().put('id',a.Id);	        
		        
        ApexPages.StandardSetController stdController = new ApexPages.standardSetController(new list<Account>());             
        SiteManagerController controller = new SiteManagerController ( stdController );

		controller.backToBasket();
		controller.goToImport();
		controller.getshowRemove();

		controller.fiberChecksAllowed(12);
		controller.scheduleDsl();
		controller.scheduleFiber();
		controller.scheduleBoth();

		String blobCreator = 'POSTALCODE,HOUSENUMBER,HOUSENUMBERSUFFIX,PHONE' + '\r\n'
									+ '1066NC,88,,0653456787';
		controller.csvBody = blob.valueOf(blobCreator);
		controller.handleCSV();

        controller.siteList[1].selected = true;
        try {controller.bulkSearch();}catch(Exception e){}
        controller.siteList[1].selected = false;
        
		// Select the sites for saving
		for (SiteManagerController.SiteWrapper site:controller.siteList) {
			site.selected = true;
			site.selectedAddress = 'Jan Rebelstraat | 14 | C | | 1069CC | Amsterdam | Netherlands';
			site.searchResult = new List<SelectOption>();
			site.hasValidAddressInput = true;			
		}
		controller.bulkSave();		
		controller.manageInfra();
		controller.addSite();
		controller.remove();		
		controller.addPBX();
		controller.failedRows = new List<String>();
		controller.downloadExample();
		controller.getSeparators();
		controller.csvName = 'myCSV.csv';
		controller.backToAccount();
		controller.searchPostalCode = '1066NC';
		controller.searchHouseNumber = '88';

        Test.stopTest();
	}
	
    public static testMethod void test_method_two() { 
        Account a = TestUtils.createAccount(null);
		Site__c s = TestUtils.createSite(a);
		
		List<PBX_Type__c> pbxs = new List<PBX_Type__c>();
		
		for(Integer i=0; i<5; i++) {
			PBX_Type__c pbx = new PBX_Type__c(Office_Voice_approved__c='G.711',Vendor__c='testVendor',Product_Name__c='testProduct'+i,Software_Version__c='1.'+i);	
			pbxs.add(pbx);
		}
		insert pbxs;

		Competitor_Asset__c ca = new Competitor_Asset__c();
		ca = PBXSelectionWizardController.createAsset(s,ca,pbxs[0].id);
		insert ca;		

		TestUtils.createSalesSettings();

		Test.startTest();

        PageReference pageRef = Page.SiteManager;
        Test.setCurrentPage(pageRef);
    	System.currentPageReference().getParameters().put('id',a.Id);	        
		        
        ApexPages.StandardSetController stdController = new ApexPages.standardSetController(new list<Account>());             
        SiteManagerController controller = new SiteManagerController ( stdController );
        
        controller.previous();
        controller.next();
        controller.refresh();
        controller.orderByColumn = 'Name';
        controller.sort();
        controller.sort();
        
        controller.siteList[0].selected = true;
        controller.activateSites();
        controller.listView = 'Active';
		controller.filterSites();
		System.assertEquals(1, controller.siteList.size());
        controller.siteList[0].selected = true;
        controller.deactivateSites();
        controller.listView = 'Inactive';
		controller.filterSites();
		System.assertEquals(1, controller.siteList.size());
        controller.listView = 'All';
		controller.filterSites();
		
        controller.manageInfras();
        controller.siteList[0].selected = true;
        controller.manageInfras();
        
		Test.stopTest();
    }
}