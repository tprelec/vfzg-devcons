public with sharing class OrderEntryProductAddOnTriggerHandler extends TriggerHandler {
	private List<OE_Product_Add_On__c> newProductAddOns;
	private Map<Id, OE_Product_Add_On__c> newProductAddOnsMap;
	private Map<Id, OE_Product_Add_On__c> oldProductAddOnsMap;

	private void init() {
		newProductAddOns = (List<OE_Product_Add_On__c>) this.newList;
		newProductAddOnsMap = (Map<Id, OE_Product_Add_On__c>) this.newMap;
		oldProductAddOnsMap = (Map<Id, OE_Product_Add_On__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	public void setExternalIds() {
		Sequence seq = new Sequence('OE_Product_Add_On');

		if (seq.canBeUsed()) {
			seq.startMutex();
			for (OE_Product_Add_On__c pao : newProductAddOns) {
				pao.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}