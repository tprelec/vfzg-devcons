global with sharing class CustomButtonPandLLIG extends csbb.CustomButtonExt  
{
    public String performAction (String basketId) 
    { 
        createSnapshotRecords(basketId);
        String newUrl = '/apex/cspl__PLReport?basketId=' + basketId;
        return '{"status":"ok","redirectURL":"' + newUrl + '"}';
    }
    
    private void createSnapshotRecords(Id basketId){
        /*
        List<CS_Basket_Snapshot_Transactional__c> oldData = [SELECT Id from CS_Basket_Snapshot_Transactional__c where Product_Basket__c = :basket.Id];
        if(oldData.size() > 0){
            SharingUtils.deleteRecordsWithoutSharing(oldData);
            
        }
        */
        cscfga__Product_Basket__c basket = [SELECT id, Used_Snapshot_Objects__c FROM cscfga__Product_Basket__c WHERE id = :basketId];
        
        if((basket.Used_Snapshot_Objects__c == '') ||(basket.Used_Snapshot_Objects__c == null)||(basket.Used_Snapshot_Objects__c != '[CS_Basket_Snapshot_Transactional__c]')){
            basket.Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]';
            update basket;
        }
        CS_BasketSnapshotManager.TakeBasketSnapshot(basket, true);
    }
}