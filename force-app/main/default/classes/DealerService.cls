public without sharing class DealerService {
	/**
	 * Initializes Dealer hierarchy based on Delaer Codes.
	 * @param  dealerCodes 	Dealer Codes
	 * @return              Map of Dealer wrappers, with Dealer Code as key
	 */
	public Map<String, Dealer> getDealersByCode(Set<String> dealerCodes) {
		List<Dealer_Information__c> dealerInfos = getDealerInfosByCode(dealerCodes);
		Map<Id, Dealer> dealers = getDealers(dealerInfos);
		Map<String, Dealer> dealersByCode = new Map<String, Dealer>();
		for (Dealer dealer : dealers.values()) {
			dealersByCode.put(dealer.dealerInfo.Dealer_Code__c, dealer);
		}
		return dealersByCode;
	}

	/**
	 * Initializes Dealer hierarchy based on Delaer Info IDs.
	 * @param  dealerInfoIds Dealer Info IDs.
	 * @return               Map of Dealer wrappers, with Dealer Info as key
	 */
	public Map<Id, Dealer> getDealersById(Set<Id> dealerInfoIds) {
		List<Dealer_Information__c> dealerInfos = getDealerInfosById(dealerInfoIds);
		return getDealers(dealerInfos);
	}

	/**
	 * Initializes Dealer hierarchy based on User IDs.
	 * @param  userIds Dealer Info IDs.
	 * @return         Map of Dealer wrappers, with User ID as key
	 */
	public Map<Id, Dealer> getDealersByUserIds(Set<Id> userIds) {
		Map<Id, Dealer> dealersByUserId = new Map<Id, Dealer>();
		// Get Users
		List<User> users = [SELECT Id, AccountId, UserRoleId, UserRole.Name, UserType FROM User WHERE Id IN :userIds AND AccountId != NULL];
		if (users.isEmpty()) {
			return dealersByUserId;
		}

		// Get related Dealer Infos
		Set<Id> accountIds = GeneralUtils.getIDSetFromList(users, 'AccountId');
		List<Dealer_Information__c> dealerInfos = getDealerInfosByAccountId(accountIds);

		// Map Dealer Infos by Account
		Map<Id, Dealer_Information__c> dealerInfosByAccountId = new Map<Id, Dealer_Information__c>();
		for (Dealer_Information__c dealerInfo : dealerInfos) {
			dealerInfosByAccountId.put(dealerInfo.Contact__r.AccountId, dealerInfo);
		}

		// Get Dealers and build result
		Map<Id, Dealer> dealers = getDealers(dealerInfos);
		for (User u : users) {
			Dealer_Information__c dealerInfo = dealerInfosByAccountId.get(u.AccountId);
			dealersByUserId.put(u.Id, dealers.get(dealerInfo?.Id));
		}
		return dealersByUserId;
	}

	/**
	 * Initializes Dealer hierarchy based on Dealer Infos.
	 * @param  dealerInfos Dealer Infos IDs.
	 * @return             Map of Dealer wrappers with Dealer Info as a key
	 */
	private Map<Id, Dealer> getDealers(List<Dealer_Information__c> dealerInfos) {
		// Get Parent Dealers
		Set<String> parentDealerCodes = new Set<String>();
		for (Dealer_Information__c dealerInfo : dealerInfos) {
			if (dealerInfo.HasParent__c) {
				parentDealerCodes.add(dealerInfo.Parent_Dealer_Code__c);
			}
		}
		Map<String, Dealer> parentDealersByCode = new Map<String, Dealer>();
		if (!parentDealerCodes.isEmpty()) {
			DealerService dealerSvc = new DealerService();
			parentDealersByCode = dealerSvc.getDealersByCode(parentDealerCodes);
		}
		// Init
		Map<Id, Dealer> dealers = new Map<Id, Dealer>();
		for (Dealer_Information__c dealerInfo : dealerInfos) {
			Dealer parentDealer = dealerInfo.HasParent__c ? parentDealersByCode.get(dealerInfo.Parent_Dealer_Code__c) : null;
			dealers.put(dealerInfo.Id, new Dealer(dealerInfo, parentDealer));
		}
		initDealerGroups(dealers.values());
		return dealers;
	}

	/**
	 * Sets corresponding Group for each Dealer wrapper
	 * @param  dealers Dealer wrappers
	 */
	private void initDealerGroups(List<Dealer> dealers) {
		Set<String> groupNames = new Set<String>();
		Set<Id> userIds = new Set<Id>();
		for (Dealer dealer : dealers) {
			groupNames.add(dealer.getDealerGroupName());
			userIds.add(dealer.dealerInfo.ContactUserId__c);
		}
		// Get Groups by Dealer Name
		Map<String, SObject> groupsByName = GeneralUtils.getMapByUniqueKey('Name', [SELECT Id, Name FROM Group WHERE Name IN :groupNames]);
		// Get Groups by Dealer User Role
		Map<Id, User> usersMap = new Map<Id, User>([SELECT Id, IsActive, UserRole.DeveloperName FROM User WHERE Id IN :userIds]);
		Set<String> roleNames = new Set<String>();
		for (User u : usersMap.values()) {
			roleNames.add(u.UserRole.DeveloperName);
		}
		Map<String, SObject> groupsByRoleName = GeneralUtils.getMapByUniqueKey(
			'DeveloperName',
			[SELECT Id, Name, DeveloperName FROM Group WHERE DeveloperName IN :roleNames AND Type = 'RoleAndSubordinates']
		);
		// First try to match group by name, then by user role
		for (Dealer dealer : dealers) {
			dealer.dealerUser = usersMap.get(dealer.dealerInfo.ContactUserId__c);
			if (groupsByName.containsKey(dealer.getDealerGroupName())) {
				dealer.dealerGroup = (Group) groupsByName.get(dealer.getDealerGroupName());
			} else if (groupsByRoleName.containsKey(dealer.dealerUser?.UserRole?.DeveloperName)) {
				dealer.dealerGroup = (Group) groupsByRoleName.get(dealer.dealerUser.UserRole.DeveloperName);
			}
		}
	}

	/**
	 * Gets Dealer Infos with all needed fields based on Dealer Info IDs
	 * @param  dealerInfoIds  Dealer Info IDs
	 * @return                List of Dealer Infos
	 */
	private List<Dealer_Information__c> getDealerInfosById(Set<Id> dealerInfoIds) {
		if (dealerInfoIds.isEmpty()) {
			return new List<Dealer_Information__c>();
		}
		return [
			SELECT Name, HasParent__c, Dealer_Code__c, Parent_Dealer_Code__c, ContactUserId__c, Contact__r.AccountId
			FROM Dealer_Information__c
			WHERE Id IN :dealerInfoIds
		];
	}

	/**
	 * Gets Dealer Infos with all needed fields based on Dealer Codes
	 * @param  dealerCodes  Dealer Codes
	 * @return              List of Dealer Infos
	 */
	private List<Dealer_Information__c> getDealerInfosByCode(Set<String> dealerCodes) {
		if (dealerCodes.isEmpty()) {
			return new List<Dealer_Information__c>();
		}
		return [
			SELECT Name, HasParent__c, Dealer_Code__c, Parent_Dealer_Code__c, ContactUserId__c, Contact__r.AccountId
			FROM Dealer_Information__c
			WHERE Dealer_Code__c IN :dealerCodes
		];
	}

	/**
	 * Gets Dealer Infos with all needed fields based on Account IDs
	 * @param  accountIds  Account IDs
	 * @return             List of Dealer Infos
	 */
	private List<Dealer_Information__c> getDealerInfosByAccountId(Set<Id> accountIds) {
		if (accountIds.isEmpty()) {
			return new List<Dealer_Information__c>();
		}
		return [
			SELECT Name, HasParent__c, Dealer_Code__c, Parent_Dealer_Code__c, ContactUserId__c, Contact__r.AccountId
			FROM Dealer_Information__c
			WHERE Contact__r.AccountId IN :accountIds
		];
	}

	/**
	 * @description Executes a SOQL query to Counts VF_Asset__c records against the account.
	 * @param accountId an ID of an account whose assets need to be counted.
	 * @param dealerCodes an optional filter on the asset count. Provide a set of dealer codes
	 *  if you want only assets from specific dealers counted. Null or empty otherwise.
	 * @return Integer a count of VF_Asset__c records against the account.
	 **/
	public static Integer fetchCountOfMobileDealerAssets(Id accountId, Set<String> dealerCodes) {
		List<String> pricePlanFilter = new List<String>{
			Constants.VF_ASSET_PRICEPLAN_DATA_ONLY,
			Constants.VF_ASSET_PRICEPLAN_DATAONLY,
			Constants.VF_ASSET_PRICEPLAN_MOBILE_ONLY
		};

		Set<String> dealerCodesFilter = new Set<String>();
		String soqlQuery =
			'SELECT COUNT() FROM VF_Asset__C WHERE ' +
			'Account__c = \'' +
			accountId +
			'\' ' +
			'AND CTN_Status__c = \'' +
			Constants.VF_ASSET_CTN_STATUS_ACTIVE +
			'\' ' +
			'AND (Initial_dealer_code__c != null OR Retention_Dealer_code__c != null) ' +
			'AND Priceplan_Class__c in :pricePlanFilter ';
		if (dealerCodes != null && !dealerCodes.isEmpty()) {
			for (String dealerCode : dealerCodes) {
				dealerCodesFilter.add('%' + dealerCode);
			}
			soqlQuery +=
				'AND (Retention_Dealer_code__c LIKE :dealerCodesFilter' +
				' OR (Retention_Dealer_code__c = NULL AND Initial_dealer_code__c LIKE :dealerCodesFilter))';
		}
		return Database.countQuery(soqlQuery);
	}

	/**
	 * Dealer Code should be 6 digits. Cleans up unneeded characters.
	 * @param  dealerCode Dealer Code to be cleaned up.
	 * @return            Cleaned up dealer code.
	 */
	public static String cleanDealerCode(String dealerCode) {
		String result = dealerCode;
		if (result != null && result.length() != 6) {
			if (result.length() > 6) {
				result = result.right(6);
			} else {
				result = result.leftPad(6, '0');
			}
		}
		return result;
	}

	public class Dealer {
		public Dealer_Information__c dealerInfo;
		public Dealer parentDealer;
		public Group dealerGroup;
		public User dealerUser;

		public Dealer(Dealer_Information__c dealerInfo) {
			this.dealerInfo = dealerInfo;
		}

		public Dealer(Dealer_Information__c dealerInfo, Dealer parentDealer) {
			this(dealerInfo);
			this.parentDealer = parentDealer;
		}

		public String getDealerGroupName() {
			if (parentDealer != null) {
				return parentDealer.getDealerGroupName();
			} else {
				return 'PMs:' + this.dealerInfo.Name.left(36);
			}
		}

		public Id getGroupOrUserId() {
			if (dealerGroup != null) {
				return dealerGroup.Id;
			} else if (dealerUser != null && dealerUser.IsActive) {
				return dealerUser.Id;
			}
			return null;
		}
	}
}
