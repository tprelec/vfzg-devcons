@SuppressWarnings('PMD')
public with sharing class PP_JourneyController {
	private static final Integer QUERY_LIMIT = 15;
	private static final Integer QUERY_LIMIT_BASKET = 2000;

	private static String opportunityFields =
		'Name,Amount,Probability,StageName,CloseDate,' +
		'ContractId,Contract_VF__c,VF_Contract_Status__c,Contract_VF__r.Name,Contract_VF__r.Signed_by_Customer__c,' +
		'Contract_VF__r.Signed_by_Customer__r.Name,Contract_VF__r.Contract_Sign_Date__c,Contract_VF__r.Contract_Status__c,' +
		'CreatedDate,Customer_Reference__c,Deal_Type__c,Department__c,IsClosed,' +
		'AccountId,Account.BAN_Number__c,Account.LG_VisitAddress__c,Account.BillingAddress,Account.BillingCity,Account.BillingCountry,' +
		'Account.BillingPostalCode,Account.BillingStreet,Account.KVK_number__c,Account.Name,Account.Phone,Account.Website,' +
		'Primary_Quote__c,Primary_Quote__r.Name,Primary_Quote__r.Approval_Date__c,Primary_Quote__r.Submitted_Date__c,' +
		'Primary_Quote__r.BigMachines__Status__c,Primary_Quote__r.Quote_Type__c,' +
		'RecordTypeId,Record_Type_is_MAC__c, LastModifiedDate, Owner.Name,' +
		'Select_Journey__c,BAN__r.BAN_Status__c,BAN__r.Name';

	private static String orderFields = 'Name, Id, Ready_for_Inside_Sales__c';

	//SF-320 had to update the query not to display hidden attachments on partner portal journey on right side (the same query is used in OpportunityAttachmentManagerController.cls)
	@AuraEnabled
	public static List<Opportunity_Attachment__c> getOpportunityAttachments(String opportunityId) {
		List<Opportunity_Attachment__c> returnList = new List<Opportunity_Attachment__c>();
		List<Opportunity_Attachment__c> attachments = [
			SELECT
				Id,
				Name,
				Attachment_ID__c,
				Attachment_Type__c,
				Description_Optional__c,
				Opportunity__c,
				Signed_Contract__c,
				View_Attachment__c,
				CreatedById
			FROM Opportunity_Attachment__c
			WHERE Opportunity__c = :opportunityId
		];

		for (Opportunity_Attachment__c oa : attachments) {
			if (!GeneralUtils.currentUserIsPartnerUser() || !getHiddenContractTypes().contains(oa.Attachment_Type__c)) {
				returnList.add(oa);
			}
		}
		return returnList;
	}
	//SF-320
	private static Set<String> getHiddenContractTypes() {
		Set<String> returnSet = new Set<String>();
		for (Attachment_Setting__mdt atts : [
			SELECT DeveloperName, Label, Attachment_types_hidden_from_partners__c
			FROM Attachment_Setting__mdt
			WHERE Label = 'default'
		]) {
			for (String s : atts.Attachment_types_hidden_from_partners__c.split(';')) {
				returnSet.add(s);
			}
		}
		return returnSet;
	}

	@AuraEnabled
	public static List<Order__c> getOrders(String contractId) {
		List<Order__c> orders = [SELECT Id, Name, Status__c FROM Order__c WHERE VF_Contract__c = :contractId];

		return orders;
	}

	@AuraEnabled
	public static Opportunity getOpportunity(String opportunityId) {
		Opportunity oppty = Database.query('SELECT ' + opportunityFields + ' FROM Opportunity WHERE Id = :opportunityId');
		return oppty;
	}

	@AuraEnabled
	public static Account getAccount(String accountId) {
		Account account = Database.query('SELECT ' + PP_SearchAccountController.account_fields + ' FROM Account WHERE Id = :accountId');
		return account;
	}

	public class CustomOpportunityClass implements Comparable {
		public Boolean isCompletedOpp = false;

		@AuraEnabled
		public DateTime lastOrderModifiedDate;

		@AuraEnabled
		public String phase;

		@AuraEnabled
		public Opportunity opp;

		@AuraEnabled
		public String bopSalesOrderId;

		@AuraEnabled
		public cscfga__Product_Basket__c basket;

		@AuraEnabled
		public Boolean isBasket {
			get {
				if (basket != null) {
					isBasket = true;
				} else {
					isBasket = false;
				}

				return isBasket;
			}
			private set;
		}

		@AuraEnabled
		public String basketName {
			get {
				if (basketName == null) {
					if (basket != null) {
						basketName = basket.Name;
					} else {
						if (opp.Primary_Quote__c != null) {
							basketName = opp.Primary_Quote__r.Name;
						}
					}
				}
				return basketName;
			}
			private set;
		}

		@AuraEnabled
		public DateTime lastOrderModifiedDt {
			get {
				if (lastOrderModifiedDate != null) {
					lastOrderModifiedDt = date.newinstance(lastOrderModifiedDate.year(), lastOrderModifiedDate.month(), lastOrderModifiedDate.day());
				}
				return lastOrderModifiedDt;
			}
			set;
		}

		public Integer phaseNumber {
			get {
				if (phase == Label.pp_Phase_Order) {
					return 1;
				} else {
					return 0;
				}
			}
			set;
		}

		public Integer compareTo(Object compareTo) {
			CustomOpportunityClass compareToEmp = (CustomOpportunityClass) compareTo;
			if (basket != null) {
				if (lastOrderModifiedDate <= compareToEmp.lastOrderModifiedDate) {
					return 1;
				}

				if (lastOrderModifiedDate > compareToEmp.lastOrderModifiedDate) {
					return 0;
				}
			} else {
				if (isCompletedOpp) {
					if (lastOrderModifiedDt <= compareToEmp.lastOrderModifiedDt) {
						return 1;
					}

					if (lastOrderModifiedDt > compareToEmp.lastOrderModifiedDt) {
						return 0;
					}
				} else {
					if (phaseNumber == compareToEmp.phaseNumber) {
						return 0;
					}

					if (phaseNumber > compareToEmp.phaseNumber) {
						return 1;
					}
				}
			}
			return -1;
		}
	}

	public static List<CustomOpportunityClass> incompleteOrders(String pageName, String viewAccessValue, String filterName, String viewListValue) {
		List<CustomOpportunityClass> returnOpp = new List<CustomOpportunityClass>();

		List<Opportunity> oppList = Database.query(getIncompleteOrdersQuery2(viewAccessValue, viewListValue));
		Set<Id> oppIdSet = new Set<Id>();
		Map<Id, String> oppBopSalesIdMap = new Map<Id, String>();
		Map<Id, List<Order__c>> oppOrderMap = new Map<Id, List<Order__c>>();
		String bopSalesOrderId = '';
		Map<Id, List<cscfga__Product_Basket__c>> basketMap = new Map<Id, List<cscfga__Product_Basket__c>>();
		for (Opportunity opp : oppList) {
			basketMap.put(opp.Id, new List<cscfga__Product_Basket__c>());
			if (opp.StageName == 'Closed Won' || opp.StageName == 'Closing') {
				oppIdSet.add(opp.Id);
				oppBopSalesIdMap.put(opp.Id, '');
				oppOrderMap.put(opp.Id, new List<Order__c>());
			}
		}
		if (oppIdSet.size() > 0) {
			List<Order__c> orderList = [
				SELECT Id, Name, VF_Contract__r.Opportunity__c, Ready_for_Inside_Sales__c, Sales_Order_Id__c, Status__c
				FROM Order__c
				WHERE VF_Contract__r.Opportunity__c IN :oppIdSet
			];
			for (Order__c order : orderList) {
				List<Order__c> orderTmpList = oppOrderMap.get(order.VF_Contract__r.Opportunity__c);
				orderTmpList.add(order);
				oppOrderMap.put(order.VF_Contract__r.Opportunity__c, orderTmpList);
				if (!String.isEmpty(order.Sales_Order_Id__c)) {
					String bobSalesStringTmp = oppBopSalesIdMap.get(order.VF_Contract__r.Opportunity__c);
					if (!String.isEmpty(bobSalesStringTmp)) {
						bobSalesStringTmp += ' + ';
					}
					bobSalesStringTmp += order.Sales_Order_Id__c;
					oppBopSalesIdMap.put(order.VF_Contract__r.Opportunity__c, bobSalesStringTmp);
				}
			}
		}
		List<cscfga__Product_Basket__c> basketList = [
			SELECT Id, Name, Basket_Description__c, cscfga__Opportunity__c, LastModifiedDate, OwnerName__c, cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c IN :basketMap.keySet()
		];

		for (cscfga__Product_Basket__c basket : basketList) {
			if (basketMap.containsKey(basket.cscfga__Opportunity__c)) {
				List<cscfga__Product_Basket__c> tmpList = basketMap.get(basket.cscfga__Opportunity__c);
				tmpList.add(basket);
				basketMap.put(basket.cscfga__Opportunity__c, tmpList);
			} else {
				List<cscfga__Product_Basket__c> tmpList = new List<cscfga__Product_Basket__c>();
				tmpList.add(basket);
				basketMap.put(basket.cscfga__Opportunity__c, tmpList);
			}
		}
		for (Opportunity opp : oppList) {
			Boolean addOpp = false;
			if (opp.StageName == 'Closed Won') {
				List<Order__c> orderTmpList = oppOrderMap.get(opp.Id);
				if (orderTmpList.size() > 0) {
					Integer ordersAssignedToSag = 0;
					for (Order__c order : orderTmpList) {
						if (order.Status__c == 'Accepted' || order.Status__c == 'Processed manually') {
							ordersAssignedToSag += 1;
						}
					}

					if (ordersAssignedToSag < orderTmpList.size()) {
						addOpp = true;
					}
				}
			} else if (opp.StageName == 'Closing') {
				List<Order__c> orderTmpList = oppOrderMap.get(opp.Id);
				if (orderTmpList.size() == 0) {
					addOpp = true;
				}
			} else {
				addOpp = true;
			}
			if (addOpp) {
				if (viewListValue == 'basket') {
					List<cscfga__Product_Basket__c> basketTmpList = basketMap.get(opp.Id);
					if (basketTmpList != null) {
						for (cscfga__Product_Basket__c basket : basketTmpList) {
							CustomOpportunityClass coc = new CustomOpportunityClass();
							coc.opp = opp;
							coc.basket = basket;
							coc.bopSalesOrderId = oppBopSalesIdMap.get(opp.Id);
							coc.phase = (opp.StageName == 'Closed Won') ? Label.pp_Phase_Order : Label.pp_Phase_Basket;
							coc.lastOrderModifiedDate = coc.basket.LastModifiedDate;

							returnOpp.add(coc);
						}
					}
					if (opp.Primary_Quote__c != null && !String.isEmpty(filterName)) {
						CustomOpportunityClass coc = new CustomOpportunityClass();
						coc.opp = opp;
						coc.bopSalesOrderId = oppBopSalesIdMap.get(opp.Id);
						coc.phase = (opp.StageName == 'Closed Won') ? Label.pp_Phase_Order : Label.pp_Phase_Basket;
						coc.lastOrderModifiedDate = coc.basket.LastModifiedDate;

						returnOpp.add(coc);
					}
				} else {
					CustomOpportunityClass coc = new CustomOpportunityClass();
					coc.opp = opp;
					coc.bopSalesOrderId = oppBopSalesIdMap.get(opp.Id);
					coc.phase = (opp.StageName == 'Closed Won') ? Label.pp_Phase_Order : Label.pp_Phase_Basket;

					returnOpp.add(coc);
				}
			}
		}

		return returnOpp;
	}

	public static List<CustomOpportunityClass> ordersPendingSag(String pageName, String viewAccessValue, String filterName, String viewListValue) {
		List<CustomOpportunityClass> returnOpp = new List<CustomOpportunityClass>();

		List<Opportunity> oppList = Database.query(getSagOrdersQuery2(viewAccessValue));

		Set<Id> oppIdSet = new Set<Id>();
		Map<Id, String> oppBopSalesIdMap = new Map<Id, String>();
		Map<Id, List<Order__c>> oppOrderMap = new Map<Id, List<Order__c>>();
		String bopSalesOrderId = '';
		Map<Id, List<cscfga__Product_Basket__c>> basketMap = new Map<Id, List<cscfga__Product_Basket__c>>();
		for (Opportunity opp : oppList) {
			basketMap.put(opp.Id, new List<cscfga__Product_Basket__c>());
			if (opp.StageName == 'Closed Won' || opp.StageName == 'Closing by SAG') {
				oppIdSet.add(opp.Id);
				oppBopSalesIdMap.put(opp.Id, '');
				oppOrderMap.put(opp.Id, new List<Order__c>());
			}
		}
		if (oppIdSet.size() > 0) {
			List<Order__c> orderList = [
				SELECT Id, Status__c, VF_Contract__r.Opportunity__c, Ready_for_Inside_Sales__c, Sales_Order_Id__c
				FROM Order__c
				WHERE VF_Contract__r.Opportunity__c IN :oppIdSet
			];
			for (Order__c order : orderList) {
				List<Order__c> orderTmpList = oppOrderMap.get(order.VF_Contract__r.Opportunity__c);
				orderTmpList.add(order);
				oppOrderMap.put(order.VF_Contract__r.Opportunity__c, orderTmpList);
				if (!String.isEmpty(order.Sales_Order_Id__c)) {
					String bobSalesStringTmp = oppBopSalesIdMap.get(order.VF_Contract__r.Opportunity__c);
					if (!String.isEmpty(bobSalesStringTmp)) {
						bobSalesStringTmp += ' + ';
					}
					bobSalesStringTmp += order.Sales_Order_Id__c;
					oppBopSalesIdMap.put(order.VF_Contract__r.Opportunity__c, bobSalesStringTmp);
				}
			}
		}
		List<cscfga__Product_Basket__c> basketList = [
			SELECT Id, Name, cscfga__Opportunity__c, LastModifiedDate, OwnerName__c, cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c IN :basketMap.keySet()
		];
		for (cscfga__Product_Basket__c basket : basketList) {
			if (basketMap.containsKey(basket.cscfga__Opportunity__c)) {
				List<cscfga__Product_Basket__c> tmpList = basketMap.get(basket.cscfga__Opportunity__c);
				tmpList.add(basket);
				basketMap.put(basket.cscfga__Opportunity__c, tmpList);
			} else {
				List<cscfga__Product_Basket__c> tmpList = new List<cscfga__Product_Basket__c>();
				tmpList.add(basket);
				basketMap.put(basket.cscfga__Opportunity__c, tmpList);
			}
		}
		for (Opportunity opp : oppList) {
			CustomOpportunityClass coc = new CustomOpportunityClass();
			Boolean addOpp = false;
			if (opp.StageName == 'Closed Won') {
				List<Order__c> orderTmpList = oppOrderMap.get(opp.Id);
				if (orderTmpList.size() > 0) {
					Integer ordersAssignedToSag = 0;
					Boolean ordersNotFinal = false;
					for (Order__c order : orderTmpList) {
						if (order.Ready_for_Inside_Sales__c) {
							ordersAssignedToSag += 1;
						}
						if (order.Status__c != 'Accepted' && order.Status__c != 'Processed manually') {
							ordersNotFinal = true;
						}
					}
					if (ordersAssignedToSag == orderTmpList.size() && ordersNotFinal) {
						addOpp = true;
						coc.phase = Label.pp_Phase_Order;
					}
				}
			} else if (opp.StageName == 'Closing by SAG') {
				List<Order__c> orderTmpList = oppOrderMap.get(opp.Id);
				if (orderTmpList.size() == 0) {
					addOpp = true;
					coc.phase = Label.pp_Phase_Basket;
				}
			} else {
				addOpp = true;
				coc.phase = Label.pp_Phase_Order;
			}
			if (addOpp) {
				if (viewListValue == 'basket') {
					List<cscfga__Product_Basket__c> basketTmpList = basketMap.get(opp.Id);
					if (basketTmpList != null) {
						for (cscfga__Product_Basket__c basket : basketTmpList) {
							CustomOpportunityClass tmpCoc = new CustomOpportunityClass();
							tmpCoc.opp = opp;
							tmpCoc.basket = basket;
							tmpCoc.lastOrderModifiedDate = tmpCoc.basket.LastModifiedDate;
							tmpCoc.phase = coc.phase;
							returnOpp.add(tmpCoc);
						}
					}
					if (opp.Primary_Quote__c != null && !String.isEmpty(filterName)) {
						CustomOpportunityClass tmpCoc = new CustomOpportunityClass();
						tmpCoc.opp = opp;
						tmpCoc.lastOrderModifiedDate = tmpCoc.basket.LastModifiedDate;
						tmpCoc.phase = coc.phase;
						returnOpp.add(tmpCoc);
					}
				} else {
					coc.opp = opp;
					coc.bopSalesOrderId = oppBopSalesIdMap.get(opp.Id);
					returnOpp.add(coc);
				}
			}
		}

		return returnOpp;
	}

	public static List<CustomOpportunityClass> completedOrders(String pageName, String viewAccessValue, String filterName, String viewListValue) {
		List<CustomOpportunityClass> returnOpp = new List<CustomOpportunityClass>();

		List<Opportunity> oppList = Database.query(getCompleteOrdersQuery2(viewAccessValue));
		Set<Id> oppIdSet = new Set<Id>();
		String bopSalesOrderId = '';
		Map<Id, String> oppBopSalesIdMap = new Map<Id, String>();
		Map<Id, DateTime> oppLastOrderDateMap = new Map<Id, DateTime>();
		Map<Id, List<Order__c>> oppOrderMap = new Map<Id, List<Order__c>>();
		Map<Id, List<cscfga__Product_Basket__c>> basketMap = new Map<Id, List<cscfga__Product_Basket__c>>();
		for (Opportunity opp : oppList) {
			basketMap.put(opp.Id, new List<cscfga__Product_Basket__c>());
			oppIdSet.add(opp.Id);
			oppBopSalesIdMap.put(opp.Id, '');
			oppOrderMap.put(opp.Id, new List<Order__c>());
			oppLastOrderDateMap.put(opp.Id, opp.LastModifiedDate);
		}
		if (oppIdSet.size() > 0) {
			List<Order__c> orderList = [
				SELECT Id, LastModifiedDate, Status__c, VF_Contract__r.Opportunity__c, Ready_for_Inside_Sales__c, Sales_Order_Id__c
				FROM Order__c
				WHERE VF_Contract__r.Opportunity__c IN :oppIdSet
			];
			for (Order__c order : orderList) {
				List<Order__c> orderTmpList = oppOrderMap.get(order.VF_Contract__r.Opportunity__c);
				orderTmpList.add(order);
				oppOrderMap.put(order.VF_Contract__r.Opportunity__c, orderTmpList);
				if (order.VF_Contract__r.Opportunity__c != null && !String.isEmpty(order.Sales_Order_Id__c)) {
					String bobSalesStringTmp = oppBopSalesIdMap.get(order.VF_Contract__r.Opportunity__c);
					if (!String.isEmpty(bobSalesStringTmp)) {
						bobSalesStringTmp += ' + ';
					}
					bobSalesStringTmp += order.Sales_Order_Id__c;
					oppBopSalesIdMap.put(order.VF_Contract__r.Opportunity__c, bobSalesStringTmp);
				}
				if (oppLastOrderDateMap.containsKey(order.VF_Contract__r.Opportunity__c)) {
					DateTime tmpLastModDate = oppLastOrderDateMap.get(order.VF_Contract__r.Opportunity__c);
					if (tmpLastModDate < order.LastModifiedDate) {
						oppLastOrderDateMap.put(order.VF_Contract__r.Opportunity__c, order.LastModifiedDate);
					}
				} else {
					oppLastOrderDateMap.put(order.VF_Contract__r.Opportunity__c, order.LastModifiedDate);
				}
			}
		}
		List<cscfga__Product_Basket__c> basketList = [
			SELECT Id, Name, cscfga__Opportunity__c, LastModifiedDate, OwnerName__c, cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c IN :basketMap.keySet()
		];
		for (cscfga__Product_Basket__c basket : basketList) {
			if (basketMap.containsKey(basket.cscfga__Opportunity__c)) {
				List<cscfga__Product_Basket__c> tmpList = basketMap.get(basket.cscfga__Opportunity__c);
				tmpList.add(basket);
				basketMap.put(basket.cscfga__Opportunity__c, tmpList);
			} else {
				List<cscfga__Product_Basket__c> tmpList = new List<cscfga__Product_Basket__c>();
				tmpList.add(basket);
				basketMap.put(basket.cscfga__Opportunity__c, tmpList);
			}
		}
		for (Opportunity opp : oppList) {
			if (opp.StageName == 'Closed Won') {
				Boolean addToCoc = false;
				List<Order__c> orderTmpList = oppOrderMap.get(opp.Id);
				if (orderTmpList.size() > 0) {
					Integer acceptedOrders = 0;
					for (Order__c order : orderTmpList) {
						if (order.Status__c == 'Accepted' || order.Status__c == 'Processed manually') {
							acceptedOrders += 1;
						}
					}
					if (acceptedOrders == orderTmpList.size()) {
						addToCoc = true;
					}
				}
				if (addToCoc) {
					if (viewListValue == 'basket') {
						List<cscfga__Product_Basket__c> basketTmpList = basketMap.get(opp.Id);
						if (basketTmpList != null) {
							for (cscfga__Product_Basket__c basket : basketTmpList) {
								CustomOpportunityClass coc = new CustomOpportunityClass();
								coc.opp = opp;
								coc.basket = basket;
								coc.bopSalesOrderId = oppBopSalesIdMap.get(opp.Id);
								coc.lastOrderModifiedDate = coc.basket.LastModifiedDate;
								coc.phase = Label.pp_Phase_ClosedWon;
								returnOpp.add(coc);
							}
						}
					} else {
						CustomOpportunityClass coc = new CustomOpportunityClass();
						coc.opp = opp;
						coc.isCompletedOpp = true;
						coc.lastOrderModifiedDate = oppLastOrderDateMap.get(opp.Id);
						coc.bopSalesOrderId = oppBopSalesIdMap.get(opp.Id);
						coc.phase = Label.pp_Phase_ClosedWon;
						returnOpp.add(coc);
					}
				}
			} else {
				if (viewListValue == 'basket') {
					List<cscfga__Product_Basket__c> basketTmpList = basketMap.get(opp.Id);
					if (basketTmpList != null) {
						for (cscfga__Product_Basket__c basket : basketTmpList) {
							CustomOpportunityClass coc = new CustomOpportunityClass();
							coc.opp = opp;
							coc.basket = basket;
							coc.bopSalesOrderId = oppBopSalesIdMap.get(opp.Id);
							coc.lastOrderModifiedDate = coc.basket.LastModifiedDate;
							coc.phase = Label.pp_Phase_ClosedLost;
							returnOpp.add(coc);
						}
					}
					if (opp.Primary_Quote__c != null && !String.isEmpty(filterName)) {
						CustomOpportunityClass coc = new CustomOpportunityClass();
						coc.opp = opp;
						coc.bopSalesOrderId = oppBopSalesIdMap.get(opp.Id);
						coc.lastOrderModifiedDate = coc.basket.LastModifiedDate;
						coc.phase = Label.pp_Phase_ClosedLost;
						returnOpp.add(coc);
					}
				} else {
					CustomOpportunityClass coc = new CustomOpportunityClass();
					coc.opp = opp;
					coc.isCompletedOpp = true;
					coc.bopSalesOrderId = oppBopSalesIdMap.get(opp.Id);
					coc.lastOrderModifiedDate = oppLastOrderDateMap.get(opp.Id);
					coc.phase = Label.pp_Phase_ClosedLost;
					returnOpp.add(coc);
				}
			}
		}

		return returnOpp;
	}

	public static List<CustomOpportunityClass> filterOptions(List<CustomOpportunityClass> returnOpp, String filterName) {
		if (!String.isEmpty(filterName)) {
			/** make it all case-insensative */
			filterName = filterName.toLowerCase();
			if (returnOpp.size() > 0) {
				List<CustomOpportunityClass> tmpCusClassLst = new List<CustomOpportunityClass>();
				for (CustomOpportunityClass cc : returnOpp) {
					Boolean addToFinal = false;
					if (cc.basket != null) {
						String basketName = cc.basket.Name;
						basketName = basketName.toLowerCase();
						addToFinal = basketName.contains(filterName);
					}
					/** Check old VF Quotes */
					if (cc.opp.Primary_Quote__c != null) {
						String quoteName = cc.opp.Primary_Quote__r.Name;
						quoteName = quoteName.toLowerCase();
						addToFinal = quoteName.contains(filterName);
					}
					String oppName = cc.opp.Name;
					oppName = oppName.toLowerCase();
					if (oppName.contains(filterName)) {
						addToFinal = true;
					}
					String accName = cc.opp.Account.Name;
					accName = accName.toLowerCase();
					if (accName.contains(filterName)) {
						addToFinal = true;
					}

					if (addToFinal) {
						tmpCusClassLst.add(cc);
					}
				}
				returnOpp = tmpCusClassLst;
			}
		}

		return returnOpp;
	}

	@AuraEnabled
	public static List<CustomOpportunityClass> retrieveOrdersByView(
		String offset,
		String pageName,
		String viewAccessValue,
		String viewListValue,
		String filterName
	) {
		Integer offsetIntValue = 0;
		try {
			Integer parsedValue = Integer.valueOf(offset);
			if (parsedValue >= 0) {
				offsetIntValue = parsedValue;
			}
		} catch (Exception exc) {
			System.debug(LoggingLevel.ERROR, exc.getMessage());
		}
		List<CustomOpportunityClass> returnOpp = new List<CustomOpportunityClass>();

		String tmpFilterName = filterName;
		if (viewListValue == 'basket') {
			tmpFilterName = null;
		}
		/** Incomplete orders */
		if (pageName == 'incomplete-orders') {
			for (CustomOpportunityClass cOppClass : incompleteOrders(pageName, viewAccessValue, filterName, viewListValue)) {
				returnOpp.add(cOppClass);
			}
		} else if (pageName == 'orders-pending-sag' || pageName == 'openstaande-bestelling-bij-tes') {
			for (CustomOpportunityClass cOppClass : ordersPendingSag(pageName, viewAccessValue, filterName, viewListValue)) {
				returnOpp.add(cOppClass);
			}
		} else if (pageName == 'completed-orders') {
			for (CustomOpportunityClass cOppClass : completedOrders(pageName, viewAccessValue, filterName, viewListValue)) {
				returnOpp.add(cOppClass);
			}
		}
		/** filter options go here */
		returnOpp = filterOptions(returnOpp, filterName);

		returnOpp.sort();
		List<CustomOpportunityClass> returnOppFinal = new List<CustomOpportunityClass>();
		Integer counter = 0;

		for (CustomOpportunityClass coc : returnOpp) {
			if (counter >= offsetIntValue && returnOppFinal.size() < QUERY_LIMIT) {
				returnOppFinal.add(coc);
			}
			counter += 1;
		}

		returnOpp = returnOppFinal;
		returnOpp.sort();
		return returnOpp;
	}

	@AuraEnabled
	public static List<Opportunity> getRecentOpportunities(String offset) {
		Integer offsetIntValue = 0;
		try {
			Integer parsedValue = Integer.valueOf(offset);
			if (parsedValue >= 0) {
				offsetIntValue = parsedValue;
			}
		} catch (Exception exc) {
			System.debug(LoggingLevel.ERROR, exc.getMessage());
		}

		List<Opportunity> recentOpportunities = Database.query(getRecentOpportunitiesQuery(offsetIntValue));
		return recentOpportunities;
	}

	@AuraEnabled
	public static String getContactGeneralRecordTypeId() {
		Id generalRecordTypeId = Schema.SObjectType.Contact.getRecordTypeInfosByName().get('General').getRecordTypeId();
		return generalRecordTypeId;
	}

	@AuraEnabled
	public static String getOpportunityDefaultRecordTypeId() {
		Id defaultRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Default').getRecordTypeId();
		return defaultRecordTypeId;
	}

	@AuraEnabled
	public static Boolean opptyRecordTypeIsMac(String opptyId) {
		try {
			Id macRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('MAC').getRecordTypeId();
			Opportunity oppty = [SELECT Id, RecordTypeId FROM Opportunity WHERE Id = :opptyId];

			return oppty.RecordTypeId == macRecordTypeId;
		} catch (Exception exc) {
			System.debug(LoggingLevel.ERROR, exc.getMessage());
		}

		return null;
	}

	public static String getRecentOpportunitiesQuery(Integer offset) {
		String query =
			'SELECT ' +
			opportunityFields +
			' FROM Opportunity where OwnerId = \'' +
			UserInfo.getUserId() +
			'\' ' +
			' AND IsClosed = false ' +
			' ORDER BY LastModifiedDate DESC ' +
			' LIMIT ' +
			QUERY_LIMIT +
			' ' +
			' OFFSET ' +
			offset;

		return query;
	}

	public static String getIncompleteOrdersQuery2(String viewAccessValue, String viewListValue) {
		String query = 'SELECT ' + opportunityFields;

		query += ' FROM Opportunity where';
		if (!String.isEmpty(viewAccessValue)) {
			if (viewAccessValue == 'mine') {
				query += ' OwnerId = \'' + UserInfo.getUserId() + '\' AND ';
			}
		}

		List<String> incompleteOrderStages = new List<String>{ 'Prospect', 'Qualify', 'Offer', 'Closing', 'Closed Won', 'Lead' };

		String incompleteOrderStagesStr = String.join(incompleteOrderStages, '\',\'');
		query += ' StageName in (\'' + incompleteOrderStagesStr + '\')';

		query += ' ORDER BY LastModifiedDate DESC ';
		if (!String.isEmpty(viewListValue) && viewListValue == 'basket' && !String.isEmpty(viewAccessValue) && viewAccessValue == 'all') {
			query += ' LIMIT ' + QUERY_LIMIT_BASKET + ' ';
		}
		system.debug('getIncompleteOrdersQuery2 - query: ' + query);
		return query;
	}

	public static String getSagOrdersQuery2(String viewAccessValue) {
		String query = 'SELECT ' + opportunityFields;

		query += ' FROM Opportunity where';
		if (!String.isEmpty(viewAccessValue)) {
			if (viewAccessValue == 'mine') {
				query += ' OwnerId = \'' + UserInfo.getUserId() + '\' AND ';
			}
		}

		query += ' (' + ' StageName = \'Closed Won\'' + ' OR' + ' StageName = \'Closing by SAG\'' + ' )';

		query += ' ORDER BY LastModifiedDate DESC ';

		return query;
	}

	public static String getCompleteOrdersQuery2(String viewAccessValue) {
		String query = 'SELECT ' + opportunityFields;
		query += ' FROM Opportunity where';

		if (!String.isEmpty(viewAccessValue)) {
			if (viewAccessValue == 'mine') {
				query += ' OwnerId = \'' + UserInfo.getUserId() + '\' AND ';
			}
		}

		query += ' (' + ' StageName = \'Closed Won\'' + ' OR' + ' StageName = \'Closed Lost\'' + ' )';

		query += ' ORDER BY LastModifiedDate DESC ';

		return query;
	}

	public static List<AuraSelectOption> getFieldSelectOptions(Schema.SObjectField sObjectField) {
		List<AuraSelectOption> options = new List<AuraSelectOption>();

		Schema.DescribeFieldResult fieldResult = sObjectField.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

		for (Schema.PicklistEntry f : ple) {
			if (f.isActive()) {
				options.add(new AuraSelectOption(f.getValue(), f.getLabel(), f.isActive(), f.isDefaultValue()));
			}
		}
		return options;
	}

	public static String getFieldSelectDefaultValue(Schema.SObjectField sObjectField) {
		Schema.DescribeFieldResult fieldResult = sObjectField.getDescribe();
		return String.valueOf(fieldResult.getDefaultValue());
	}

	@AuraEnabled
	public static List<AuraField> getContactFields(String contactId) {
		try {
			Boolean existing = String.isNotBlank(contactId);
			Contact cont = null;
			if (existing) {
				List<Contact> contacts = [SELECT Id, Salutation, Gender__c, RecordTypeId FROM Contact WHERE Id = :contactId];
				cont = contacts.get(0);
			}

			List<AuraField> fields = new List<AuraField>();

			SObjectField field = Contact.Gender__c.getDescribe().getSobjectField();
			fields.add(
				new AuraField(
					field.getDescribe().getName(),
					existing ? cont.Gender__c : getFieldSelectDefaultValue(field),
					field.getDescribe().getLabel(),
					AuraFieldType.INPUT_SELECT,
					getFieldSelectOptions(field)
				)
			);

			field = Contact.Salutation.getDescribe().getSobjectField();
			fields.add(
				new AuraField(
					field.getDescribe().getName(),
					existing ? cont.Salutation : getFieldSelectDefaultValue(field),
					field.getDescribe().getLabel(),
					AuraFieldType.INPUT_SELECT,
					getFieldSelectOptions(field)
				)
			);

			fields.add(new AuraField('RecordTypeId', existing ? cont.RecordTypeId : getContactGeneralRecordTypeId()));

			return fields;
		} catch (Exception exc) {
			throw new AuraHandledException('Failed trying to get contact fields for Id = ' + contactId);
		}
	}

	@AuraEnabled
	public static boolean getJourneyLock(Id opportunityId) {
		boolean lockJourney = false;
		integer numberOfBaskets = [SELECT COUNT() FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :opportunityId];
		integer numberOfOppLineItems = [SELECT COUNT() FROM OpportunityLineItem WHERE OpportunityId = :opportunityId];
		if (numberOfBaskets > 0 || numberOfOppLineItems > 0) {
			lockJourney = true;
		}
		return lockJourney;
	}

	@AuraEnabled
	public static List<OpportunityLineItem> getOpportunityLineItems(String opportunityId) {
		List<OpportunityLineItem> opportunityLineItems = [
			SELECT
				Id,
				Name,
				Framework_Agreement_Id__c,
				Template_Id__c,
				Quantity,
				OpportunityId,
				Assigned_Product_Id__c,
				Deal_Type__c,
				Description,
				CTN_number__c
			FROM OpportunityLineItem
			WHERE OpportunityId = :opportunityId
		];
		return opportunityLineItems;
	}

	@AuraEnabled
	public static Opportunity getBANforAddMobileJourney(String opportunityId) {
		Opportunity opptyAddMobile = [SELECT BAN__r.Name FROM Opportunity WHERE Id = :opportunityId];
		return opptyAddMobile;
	}

	@AuraEnabled
	public static Id createDefaultBppOpportunity(String accountId) {
		if (String.isEmpty(accountId)) {
			return null;
		}

		Account account = [SELECT Id, Name FROM Account WHERE Id = :accountId];

		Opportunity opportunity = new Opportunity();
		opportunity.AccountId = accountId;
		opportunity.Name = account.Name + ' / ' + Label.pp_Enter_Short_Description;
		opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Default').getRecordTypeId();
		opportunity.CloseDate = Date.today().addMonths(1);
		opportunity.StageName = 'Offer';
		opportunity.Select_Journey__c = 'Sales';

		try {
			insert opportunity;
		} catch (Exception e) {
			System.debug(e.getStackTraceString());
			return null;
		}

		return opportunity.Id;
	}
}
