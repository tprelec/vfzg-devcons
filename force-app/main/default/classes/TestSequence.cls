@isTest
public with sharing class TestSequence {
	@TestSetup
	static void setupData() {
		ExternalIDNumber__c custSetting = new ExternalIDNumber__c(
			Name = 'OE_Validity_Period',
			External_Number__c = 1,
			Object_Prefix__c = 'OEVP-57-',
			blocked__c = false,
			runningOrg__c = '00D1l0000008i1vEAA'
		);
		insert custSetting;
	}

	@isTest
	public static void testUtilities() {
		Sequence seq = new Sequence('OE_Validity_Period');

		Test.startTest();
		Integer show = seq.show();
		Boolean isBlocked = seq.isBlocked();
		Test.stopTest();

		System.assertEquals(1, show, 'Sequence should start at 1');
		System.assertEquals(isBlocked, false, 'The sequence shouldn\'t be blocked');
	}

	@isTest
	public static void testAutoNumbering() {
		String orgId = UserInfo.getOrganizationId();
		OE_Product__c product = OrderEntryTestFactory.createOEProduct('Main', true);

		Test.startTest();
		OrderEntryTestFactory.createOEValidityPeriod(
			Date.today(),
			Date.today().addDays(7),
			product,
			true
		);
		Test.stopTest();

		OE_Validity_Period__c result = [SELECT Id, External_Id__c FROM OE_Validity_Period__c];
		ExternalIDNumber__c custSetting = [
			SELECT Id, runningOrg__c
			FROM ExternalIDNumber__c
			WHERE Name = 'OE_Validity_Period'
		];
		if (custSetting.runningOrg__c == orgId) {
			System.assertEquals(
				'OEVP-57-000002',
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		} else {
			System.assertEquals(
				null,
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		}
	}
}