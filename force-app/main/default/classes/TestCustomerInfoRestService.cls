@IsTest
public class TestCustomerInfoRestService {
	@TestSetup
	static void makeData() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.KVK_number__c = '12345678';
		update acc;
		Contact con = TestUtils.createContact(acc);
		con.Email = 'mostUniqueEmailInTheWorld@vodafoneziggo.com';
		update con;
	}

	@IsTest
	static void testGetOrGenerateContactData() {
		Account acc = [SELECT Id, KVK_number__c FROM Account];
		Contact con = [SELECT Id, FirstName, LastName, Email FROM Contact];

		String newContactEmail = 'newMostUniqueEmailInTheWorld@vodafoneziggo.com';
		CustomerInfoRestService.Contacts cont1 = new CustomerInfoRestService.Contacts();
		cont1.FirstName = 'Art';
		cont1.MiddleName = 'van de';
		cont1.LastName = 'Vandelay';
		cont1.Gender_c = 'male';
		cont1.MobilePhone = '06-89894565';
		cont1.Email = newContactEmail;
		cont1.DateOfBirth = '01-01-2000';
		cont1.Authorized_to_sign_c = 'true';
		CustomerInfoRestService.Contacts cont2 = new CustomerInfoRestService.Contacts();
		cont2.FirstName = con.FirstName;
		cont2.LastName = con.LastName;
		cont2.Gender_c = 'female';
		cont2.MobilePhone = '06-89894565';
		cont2.Email = con.Email;
		cont2.DateOfBirth = '01-01-2000';
		cont2.Authorized_to_sign_c = 'true';
		CustomerInfoRestService.Site site = new CustomerInfoRestService.Site();
		site.Postcode = '1234AB';
		site.HouseNumber = '13';
		site.HouseNrSuffix = 'x';
		site.City = 'Amsterdam';
		site.Street = 'Test Street';
		CustomerInfoRestService.CustomerInfo ci = new CustomerInfoRestService.CustomerInfo();
		ci.KVK_number_c = acc.KVK_number__c;
		ci.csb2c_E_Commerce_Customer_Id_c = 'A00000002';
		ci.contacts = new List<CustomerInfoRestService.Contacts>{ cont1, cont2 };
		ci.sites = new List<CustomerInfoRestService.Site>{ site };

		Test.startTest();
		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/customer';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(JSON.serialize(ci));
		RestContext.request = request;

		CustomerInfoRestService.getOrGenerateContactData();
		Test.stopTest();

		List<Contact> contacts = [SELECT Id FROM Contact];
		List<Contact> co = [SELECT Id, FirstName, LastName FROM Contact WHERE email = :newContactEmail];

		System.assertEquals(200, RestContext.response.statusCode, 'Response code must be 200.');
		System.assert(contacts.size() == 2, 'There should be one new contact created.');
		System.assertEquals('Art', co[0].FirstName, 'New contact first name should match the name from the request.');
		System.assertEquals('van de Vandelay', co[0].LastName, 'New contact last name should match the last name from the request.');
	}

	@IsTest
	static void testWrongKVKFormat() {
		Account acc = [SELECT Id, KVK_number__c FROM Account];
		Contact con = [SELECT Id, FirstName, LastName, Email FROM Contact];

		String reqBody =
			'{"KVK_number_c": "123",  "csb2c_E_Commerce_Customer_Id_c": "A00000002", "contacts": [ ' +
			'{ "FirstName": "Art", "LastName": "Vandelay", "Gender_c": "male", "MobilePhone": "06-89894565", "Email": "not.important@vodafoneziggo.com", "DateOfBirth": "01-01-2000", "Authorized_to_sign_c": "true" }],' +
			'"sites": [ {"Postcode": "1234AB", "HouseNumber": 13, "HouseNrSuffix": "x", "City": "Amsterdam", "Street":"Test Street"}] }';

		Test.startTest();
		RestRequest request = new RestRequest();
		request.requestUri = '/services/apexrest/customer';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(reqBody);
		RestContext.request = request;

		CustomerInfoRestService.getOrGenerateContactData();
		Test.stopTest();

		System.assertEquals(500, RestContext.response.statusCode, 'Response code should be 500.');
		System.assert(RestContext.response.responseBody.tostring().contains('Invalid KVK number format.'), 'Invalid kvk error should be thrown.');
	}
}
