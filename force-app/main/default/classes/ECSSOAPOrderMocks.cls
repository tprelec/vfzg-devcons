@isTest
public with sharing class ECSSOAPOrderMocks {
	public class CreateOrdersResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			// start - specify the response you want to send
			ECSSOAPOrder.createOrdersResponse_element response_x = new ECSSOAPOrder.createOrdersResponse_element();
			ECSSOAPOrder.createOrderResponseType[] orders = new List<ECSSOAPOrder.createOrderResponseType>{};
			response_x.order = orders;

			for (Integer i = 0; i < 1; i++) {
				ECSSOAPOrder.createOrderResponseType myResponse = new ECSSOAPOrder.createOrderResponseType();
				ECSSOAPOrder.errorType error = new ECSSOAPOrder.errorType();
				ECSSOAPOrder.additionalInfoType additonalInfo = new ECSSOAPOrder.additionalInfoType();

				myResponse.referenceId = String.valueOf(i + 1);
				myResponse.orderId = i + 1;
				myResponse.salesorderNumber = String.valueOf(i + 1);

				additonalInfo.orderCreationTime = Datetime.now();

				error.code = 'SIAS000';
				error.message = 'OK';
				error.additionalInfo = additonalInfo;

				myResponse.error = error;
				response_x.order.add(myResponse);
			}

			response.put('response_x', response_x);
		}
	}

	public class CancelOrdersResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			// start - specify the response you want to send
			ECSSOAPOrder.cancelOrdersResponse_element response_x = new ECSSOAPOrder.cancelOrdersResponse_element();
			ECSSOAPOrder.cancelOrderResponseType[] orders = new List<ECSSOAPOrder.cancelOrderResponseType>();
			response_x.order = orders;

			for (Integer i = 0; i < 1; i++) {
				ECSSOAPOrder.cancelOrderResponseType myResponse = new ECSSOAPOrder.cancelOrderResponseType();
				ECSSOAPOrder.errorType error = new ECSSOAPOrder.errorType();
				ECSSOAPOrder.additionalInfoType additonalInfo = new ECSSOAPOrder.additionalInfoType();

				myResponse.referenceId = String.valueOf(i + 1);
				myResponse.orderId = i + 1;
				//myResponse.salesorderNumber = String.valueOf(i + 1);

				additonalInfo.orderCreationTime = Datetime.now();

				error.code = '0';
				error.message = 'OK';
				error.additionalInfo = additonalInfo;

				myResponse.error = error;
				response_x.order.add(myResponse);
			}

			response.put('response_x', response_x);
		}
	}

	public class LegacyOrdersResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			// start - specify the response you want to send
			ECSSOAPOrder.legacyOrdersResponse_element response_x = new ECSSOAPOrder.legacyOrdersResponse_element();
			ECSSOAPOrder.legacyOrderResponseType[] orders = new List<ECSSOAPOrder.legacyOrderResponseType>();
			response_x.order = orders;

			for (Integer i = 0; i < 1; i++) {
				ECSSOAPOrder.legacyOrderResponseType myResponse = new ECSSOAPOrder.legacyOrderResponseType();
				ECSSOAPOrder.errorType error = new ECSSOAPOrder.errorType();
				ECSSOAPOrder.additionalInfoType additonalInfo = new ECSSOAPOrder.additionalInfoType();

				myResponse.referenceId = String.valueOf(i + 1);
				myResponse.salesorderNumber = String.valueOf(i + 1);

				additonalInfo.orderCreationTime = Datetime.now();

				error.code = '0';
				error.message = 'OK';
				error.additionalInfo = additonalInfo;

				myResponse.error = error;
				response_x.order.add(myResponse);
			}

			response.put('response_x', response_x);
		}
	}

	public class CatchExceptionResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			// start - specify the response you want to send

			response.put('response_x', null);
		}
	}
}