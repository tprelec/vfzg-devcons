public without sharing class CST_Customer_ButtonsController {
	@AuraEnabled
	public static void updateCaseToInProgress(Id recordId) {
		Case mycase = [
			SELECT Id, CaseNumber, Contact.Name, Status, CST_Sub_Status__c, CST_Case_is_with_Team__c, Reason, CST_End2End_Owner__c
			FROM Case
			WHERE id = :recordId
		];
		mycase.Status = 'In Progress';
		mycase.CST_Sub_Status__c = 'Assigned';
		mycase.CST_Case_is_with_Team__c = 'Internal - Advisor';
		mycase.Reason = 'Not Resolved';

		update mycase;
		fireNotification(
			'Case ' +
			mycase.CaseNumber +
			': Solution not validated!',
			'Customer ' +
			mycase.Contact.Name +
			' did not validate the provided solution. Please review the case.',
			mycase.Id,
			mycase.CST_End2End_Owner__c
		);
	}

	@AuraEnabled
	public static void updateCloseCase(Id recordId) {
		Case mycase = [
			SELECT Id, CaseNumber, Contact.Name, Status, Reason, CST_End2End_Owner__c, CST_ActiveExternalTickets__c
			FROM Case
			WHERE id = :recordId
		];
		if (mycase.CST_ActiveExternalTickets__c > 0) {
			CST_External_Ticket__c remedy = [
				SELECT id, CST_Status__c, CST_Case__c, CST_IncidentID__c
				FROM CST_External_Ticket__c
				WHERE CST_Case__c = :mycase.id
			];
			remedy.CST_Status__c = 'Closed';
			update remedy;
		}
		mycase.Status = 'Closed';
		mycase.Reason = 'Customer Confirmed Solution';
		update mycase;

		fireNotification(
			'Case ' +
			mycase.CaseNumber +
			': Solution validated!',
			'Customer ' +
			mycase.Contact.Name +
			' validated the provided solution. Case is now closed.',
			mycase.Id,
			mycase.CST_End2End_Owner__c
		);
	}

	@AuraEnabled
	public static void caseClosed(Id recordId, String caseReason) {
		System.debug('caseClosed!');
		Case mycase = [
			SELECT
				Id,
				CaseNumber,
				Contact.Name,
				Status,
				Reason,
				CST_End2End_Owner__c,
				CST_ActiveExternalTickets__c,
				CST_Is_Dealer_Portal_Case__c,
				CST_Dealer_Contact__r.Name
			FROM Case
			WHERE id = :recordId
		];
		if (mycase.CST_ActiveExternalTickets__c > 0) {
			CST_External_Ticket__c remedy = [
				SELECT id, CST_Status__c, CST_Case__c, CST_IncidentID__c
				FROM CST_External_Ticket__c
				WHERE CST_Case__c = :mycase.id
			];
			remedy.CST_Status__c = 'Closed';
			update remedy;
		}
		mycase.Status = 'Closed';
		String cr = caseReason;
		mycase.Reason = cr;
		System.debug('caseClosed!!' + mycase);
		//mycase.Reason = 'Customer Confirmed Solution';
		update mycase;

		if (mycase.CST_End2End_Owner__c != null) {
			if (mycase.CST_Is_Dealer_Portal_Case__c != true) {
				System.debug('is portal?' + mycase.CST_Is_Dealer_Portal_Case__c);
				fireNotification(
					'Case ' +
					mycase.CaseNumber +
					': Case Closed',
					mycase.Contact.Name + ' closed the case.',
					mycase.Id,
					mycase.CST_End2End_Owner__c
				);
			} else {
				//fireNotification('Case '+mycase.CaseNumber+': Case Closed', mycase.CST_Dealer_Contact__c.Name +' closed the case.', mycase.Id, mycase.CST_End2End_Owner__c);
				System.debug('dealer name' + mycase.CST_Dealer_Contact__c);
				fireNotification(
					'Case ' +
					mycase.CaseNumber +
					': Case Closed',
					mycase.CST_Dealer_Contact__r.Name + ' closed the case.',
					mycase.Id,
					mycase.CST_End2End_Owner__c
				);
			}
		}
	}

	@AuraEnabled
	public static String showComponent(Id recordId) {
		Case mycase = [SELECT Id, Status FROM Case WHERE id = :recordId LIMIT 1];
		//return myCase.Status == 'Waiting for Customer Validation';
		return myCase.Status;
	}

	public static void fireNotification(String title, String message, String caseId, String end2endUserId) {
		Id cstNotificationType = [SELECT Id FROM CUstomNotificationType WHERE DeveloperName = 'CST_Notification'].Id;
		Messaging.CustomNotification notification = new Messaging.CustomNotification();
		notification.setNotificationTypeId(cstNotificationType);
		notification.setTitle(title);
		notification.setBody(message);
		notification.setTargetId(caseId);
		notification.setSenderId(Userinfo.getUserId());
		notification.send(new Set<String>{ end2endUserId });
	}
}
