@isTest
public class CCOrderRequestProcessingTest {
	private static testMethod void testExecute() {
		Framework__c frameworkSetting = new Framework__c();
		frameworkSetting.Framework_Sequence_Number__c = 2;
		insert frameworkSetting;

		PriceReset__c priceResetSetting = new PriceReset__c();

		priceResetSetting.MaxRecurringPrice__c = 200.00;
		priceResetSetting.ConfigurationName__c = 'IP Pin';

		insert priceResetSetting;

		Sales_Settings__c ssettings = new Sales_Settings__c();
		ssettings.Postalcode_check_validity_days__c = 2;
		ssettings.Max_Daily_Postalcode_Checks__c = 2;
		ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
		ssettings.Postalcode_check_block_period_days__c = 2;
		ssettings.Max_weekly_postalcode_checks__c = 15;
		insert ssettings;

		Account account = new Account(OwnerId = UserInfo.getUserId(), Name = 'Account', Type = 'End Customer');
		insert account;

		Contact contact = new Contact(
			AccountId = account.id,
			LastName = 'Last',
			FirstName = 'First',
			Contact_Role__c = 'Consultant',
			Email = 'test@vf.com'
		);
		insert contact;

		Opportunity opportunity = new Opportunity(
			Name = 'New Opportunity',
			OwnerId = UserInfo.getUserId(),
			StageName = 'Qualification',
			Probability = 0,
			CloseDate = system.today(),
			AccountId = account.id,
			Segment__c = 'SoHo',
			Type_of_service__c = 'Mobile'
		);
		insert opportunity;

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
			Name = 'New Basket 1',
			Primary__c = false,
			OwnerId = UserInfo.getUserId(),
			cscfga__Opportunity__c = opportunity.Id,
			Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
		);
		insert basket;

		cscfga__Product_Definition__c def = CS_DataTest.createProductDefinition('Business Mobile');
		def.Product_Type__c = 'Mobile';
		insert def;

		cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
		config.cscfga__Product_Definition__c = def.Id;
		insert config;

		cscfga__Attribute__c attr = new cscfga__Attribute__c(
			Name = 'contractDuration',
			cscfga__Product_Configuration__c = config.Id,
			cscfga__Value__c = '24'
		);
		insert attr;

		String body = '{"connectionTypes": [{ "type": "New", "quantity": 50 }, { "type": "Port-in", "quantity": 50 }], "charges":[{"source":"recurring","salesPrice":34,"listPrice":34,"quantity":1,"name":"recurring","description":"Override charge value forrecurring","chargeType":"recurring","isProductLevelPricing":true}],"discounts":[{"type":"PER","amount":5,"chargeType":"recurring","discountPrice":"sales","version":"2-0-0","recordType":"single","discountCharge":"recurring","source":"promo-test;promotion"}],"customData":{},"OE":[],"CS_QuantityStrategy":"Multiplier","csQuantity":1}';

		Attachment att = new Attachment(Name = 'AdditionalQualificationData.json', Body = Blob.valueOf(body), ParentId = config.Id);
		insert att;

		Test.startTest();

		List<Id> basketIdList = new List<Id>();
		basketIdList.add(basket.Id);
		CCOrderRequestProcessing processingObj = new CCOrderRequestProcessing();
		processingObj.execute(basketIdList);

		Map<ID, CCAQDProcessor.CCAQDStructure> pcsWithAQD = CCAQDProcessor.extractPCAqdData(basketIdList);

		CCAQDProcessor.CCAQDStructure workingAQD = pcsWithAQD.get(config.Id);

		ChargesProcessor.processCharges(
			config,
			new cscfga.ProductConfiguration.ProductDiscount(),
			workingAQD.charges,
			workingAQD.CS_QuantityStrategy,
			1
		);

		Test.stopTest();
	}

	private static testMethod void testExecute2() {
		Framework__c frameworkSetting = new Framework__c();
		frameworkSetting.Framework_Sequence_Number__c = 2;
		insert frameworkSetting;

		PriceReset__c priceResetSetting = new PriceReset__c();

		priceResetSetting.MaxRecurringPrice__c = 200.00;
		priceResetSetting.ConfigurationName__c = 'IP Pin';

		insert priceResetSetting;

		Sales_Settings__c ssettings = new Sales_Settings__c();
		ssettings.Postalcode_check_validity_days__c = 2;
		ssettings.Max_Daily_Postalcode_Checks__c = 2;
		ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
		ssettings.Postalcode_check_block_period_days__c = 2;
		ssettings.Max_weekly_postalcode_checks__c = 15;
		insert ssettings;

		Account account = new Account(OwnerId = UserInfo.getUserId(), Name = 'Account', Type = 'End Customer');
		insert account;

		Contact contact = new Contact(
			AccountId = account.id,
			LastName = 'Last',
			FirstName = 'First',
			Contact_Role__c = 'Consultant',
			Email = 'test@vf.com'
		);
		insert contact;

		Opportunity opportunity = new Opportunity(
			Name = 'New Opportunity',
			OwnerId = UserInfo.getUserId(),
			StageName = 'Qualification',
			Probability = 0,
			CloseDate = system.today(),
			AccountId = account.id,
			Segment__c = 'SoHo',
			Type_of_service__c = 'Mobile'
		);
		insert opportunity;

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
			Name = 'New Basket',
			Primary__c = false,
			OwnerId = UserInfo.getUserId(),
			cscfga__Opportunity__c = opportunity.Id,
			Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
		);
		insert basket;

		cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
		insert config;

		cscfga__Attribute__c attr = new cscfga__Attribute__c(
			Name = 'contractDuration',
			cscfga__Product_Configuration__c = config.Id,
			cscfga__Value__c = '24'
		);
		insert attr;

		String body = '{"connectionTypes": [{ "type": "New", "quantity": 50 }, { "type": "Port-in", "quantity": 50 }], "charges":[{"source":"recurring","salesPrice":34,"listPrice":34,"quantity":1,"name":"recurring","description":"Override charge value forrecurring","chargeType":"recurring","isProductLevelPricing":true}],"discounts":[{"type":"PER","amount":5,"chargeType":"recurring","discountPrice":"sales","version":"2-0-0","recordType":"single","discountCharge":"recurring","source":"promo-test;promotion"}],"customData":{},"OE":[],"CS_QuantityStrategy":"Multiplier","csQuantity":1}';

		Attachment att = new Attachment(Name = 'AdditionalQualificationData.json', Body = Blob.valueOf(body), ParentId = config.Id);
		insert att;

		Test.startTest();

		List<Id> basketIdList = new List<Id>();
		basketIdList.add(basket.Id);
		List<cscfga__Product_Basket__c> baskets = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
		CCOrderRequestProcessing.generateCtnRecordsOnlineScenario(baskets);
		Test.stopTest();
	}

	// for now we don't have external pricing
	// leaving for future reference
	/*
	@isTest
	private static void testExternalPricing() {
		List<Profile> pList = [
			SELECT Id, Name
			FROM Profile
			WHERE Name = 'System Administrator'
			LIMIT 1
		];
		List<UserRole> roleList = [
			SELECT Id, Name, DeveloperName
			FROM UserRole u
			WHERE ParentRoleId = NULL
		];

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;
		System.runAs(simpleUser) {

			csutil__Json_Settings__c psContribution = new csutil__Json_Settings__c();
			psContribution.csutil__json_configuration__c = '"version":"1-0-0", "cartFields":[],"cartItemFields":["customData.customFields!ps_contribution__c"]';
			psContribution.Name = 'PSI/PricingAggregator/ps_contribution';
			insert psContribution;
			// csutil__Json_Metadata__mdt psContribution = new csutil__Json_Metadata__mdt();
			// psContribution.csutil__active__c = true;
			// psContribution.csutil__name__c = 'PSI/PricingAggregator/ps_contribution';
			// psContribution.csutil__json_configuration__c = '"version":"1-0-0", "cartFields":[],"cartItemFields":["customData.customFields!ps_contribution__c"]';

			csutil__Json_Settings__c sfccContribution = new csutil__Json_Settings__c();
			sfccContribution.csutil__json_configuration__c = '"version":"1-0-0", "cartFields":["customData.customFields!sfcc_contribution__c"],"cartItemFields":["customData.customFields!sfcc_contribution__c"]';
			sfccContribution.Name = 'PSI/PricingAggregator/sfcc_contrib';
			insert sfccContribution;

			// csutil__Json_Metadata__mdt sfccContribution = new csutil__Json_Metadata__mdt();
			// sfccContribution.csutil__active__c = true;
			// sfccContribution.csutil__name__c = 'PSI/PricingAggregator/sfcc_contribution';
			// sfccContribution.csutil__json_configuration__c = '"version":"1-0-0", "cartFields":["customData.customFields!sfcc_contribution__c"],"cartItemFields":["customData.customFields!sfcc_contribution__c"]';

			Account testAccount = CS_DataTest.createAccount('SFCC');
			insert testAccount;

			Opportunity testOpportunity = CS_DataTest.createOpportunity(
				testAccount,
				'SFCC Oppty',
				simpleUser.Id
			);

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(
				testOpportunity,
				'B1'
			);
			insert basket;

			cscfga__Product_Definition__c productDefinition = CS_DataTest.createProductDefinitionRegular(
				'Business Mobile'
			);
			insert productDefinition;

			cscfga__Product_Configuration__c config = CS_DataTest.createProductConfiguration(
				productDefinition.Id,
				'CFG 1',
				basket.Id
			);
			insert config;

			Test.startTest();
			List<SObject> returnList = ChargesProcessor.processPricing(
				config,
				new List<CCAQDProcessor.CCCharge>(),
				new List<CCAQDProcessor.CCDiscount>(),
				ChargesProcessorUtils.QUANTITY_STRATEGY_DECOMPOSITION,
				1,
				true
			);
			Test.stopTest();

			System.assertEquals(1, returnList.size(), 'One item should be processed');
		}
	}
	*/

	private static testMethod void testFailure() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];

		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Framework__c frameworkSetting = new Framework__c();
			frameworkSetting.Framework_Sequence_Number__c = 2;
			insert frameworkSetting;

			PriceReset__c priceResetSetting = new PriceReset__c();

			priceResetSetting.MaxRecurringPrice__c = 200.00;
			priceResetSetting.ConfigurationName__c = 'IP Pin';

			insert priceResetSetting;

			Sales_Settings__c ssettings = new Sales_Settings__c();
			ssettings.Postalcode_check_validity_days__c = 2;
			ssettings.Max_Daily_Postalcode_Checks__c = 2;
			ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
			ssettings.Postalcode_check_block_period_days__c = 2;
			ssettings.Max_weekly_postalcode_checks__c = 15;
			insert ssettings;

			Account account = new Account(OwnerId = UserInfo.getUserId(), Name = 'Account', Type = 'End Customer');
			insert account;

			Contact contact = new Contact(
				AccountId = account.id,
				LastName = 'Last',
				FirstName = 'First',
				Contact_Role__c = 'Consultant',
				Email = 'test@vf.com'
			);
			insert contact;

			Opportunity opportunity = new Opportunity(
				Name = 'New Opportunity',
				OwnerId = UserInfo.getUserId(),
				StageName = 'Qualification',
				Probability = 0,
				CloseDate = system.today(),
				AccountId = account.id
			);
			insert opportunity;

			cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
				Name = 'New Basket',
				Primary__c = false,
				OwnerId = UserInfo.getUserId(),
				cscfga__Opportunity__c = opportunity.Id,
				Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
			);
			insert basket;

			cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
			insert config;

			String body = '{"charges":[{"source":"recurring","salesPrice":34,"listPrice":34,"quantity":1,"name":"recurring","description":"Override charge value forrecurring","chargeType":"recurring","isProductLevelPricing":true}],"discounts":[],"customData":{},"OE":[],"CS_QuantityStrategy":"Multiplier","csQuantity":1}';

			Attachment att = new Attachment(Name = 'AdditionalQualificationData.json', Body = Blob.valueOf(body), ParentId = config.Id);
			insert att;

			Test.startTest();

			List<Id> basketIdList = new List<Id>();
			basketIdList.add(basket.Id);
			CCOrderRequestProcessing processingObj = new CCOrderRequestProcessing();
			csb2c.ProductBasketObservable observable = (csb2c.ProductBasketObservable) JSON.deserialize('{}', csb2c.ProductBasketObservable.class);
			processingObj.execute(observable, null);

			Test.stopTest();
		}
	}
}
