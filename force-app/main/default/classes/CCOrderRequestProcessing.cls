/**
 * Created by bojan.zunko on 30/06/2020.
 */

global with sharing class CCOrderRequestProcessing implements csam.ObserverApi.IObserver {
	global CCOrderRequestProcessing() {
	}

	global void execute(List<Id> basketIds) {
		if (basketIds.size() > 0) {
			Id basketId = basketIds[0];
			Boolean isExternalPricingEnabled = [SELECT cc_enable_external_pricing__c FROM cscfga__Product_Basket__c WHERE Id = :basketId]
			.cc_enable_external_pricing__c;

			Map<ID, CCAQDProcessor.CCAQDStructure> pcsWithAQD = CCAQDProcessor.extractPCAqdData(basketIds);

			List<SObject> bulkCustomDataStore = new List<SObject>();
			List<ID> pcsIdsToTerminate = new List<ID>();

			List<Id> rootConfigs = new List<Id>();
			Map<Id, Id> rootPcIdBasketId = new Map<Id, Id>();
			Map<Id, Integer> rootConfigIdQuantity = new Map<Id, Integer>();
			Map<Id, Decimal> basketIdNmbOfDisconnects = new Map<Id, Decimal>();
			Boolean hasMobileConfigurations = false;

			for (cscfga__Product_Configuration__c targetPC : [
				SELECT
					Id,
					cscfga__discounts__c,
					cscfga__Root_Configuration__c,
					cscfga__Product_Basket__c,
					Downgrade_allowance__c,
					Group__c,
					cscfga__Quantity__c,
					cscfga__Product_Definition__r.Name,
					cscfga__Contract_Term__c
				FROM cscfga__Product_Configuration__c
				WHERE Id IN :pcsWithAQD.keySet()
			]) {
				if (
					targetPC.cscfga__Root_Configuration__c == null ||
					targetPC.cscfga__Product_Definition__r.Name.containsIgnoreCase('Business Mobile')
				) {
					rootConfigs.add(targetPC.Id);
					rootPcIdBasketId.put(targetPC.Id, targetPC.cscfga__Product_Basket__c);
					rootConfigIdQuantity.put(targetPC.Id, 0);
				}

				if (targetPC.cscfga__Product_Definition__r.Name.containsIgnoreCase('Mobile')) {
					hasMobileConfigurations = true;
				}

				CCAQDProcessor.CCAQDStructure workingAQD = pcsWithAQD.get(targetPC.Id);

				/*if (rootConfigIdQuantity.containsKey(targetPC.cscfga__Root_Configuration__c)) {
                    rootConfigIdQuantity.put(targetPC.cscfga__Root_Configuration__c, workingAQD.csQuantity);
                }*/

				//List<SObject> customDataAttach = CustomDataProcessor.processCustomData(targetPC.Id, workingAQD.customData);
				//bulkCustomDataStore.addAll(customDataAttach);

				if (workingAQD.charges == null)
					workingAQD.charges = new List<CCAQDProcessor.CCCharge>();

				if (workingAQD.discounts == null)
					workingAQD.discounts = new List<CCAQDProcessor.CCDiscount>();

				String CS_QuantityStrategy = workingAQD.CS_QuantityStrategy;
				// processing Addon - quantity is always what it is on root - if multiplier then we need to adjust discount and price amount to match
				if (targetPC.cscfga__Root_Configuration__c != null) {
					CS_QuantityStrategy = pcsWithAQD.get(targetPC.cscfga__Root_Configuration__c).CS_QuantityStrategy;
					if (String.isBlank(CS_QuantityStrategy)) {
						CS_QuantityStrategy = ChargesProcessorUtils.QUANTITY_STRATEGY_MULTIPLIER;
					}
				} else {
					if (String.isBlank(CS_QuantityStrategy)) {
						CS_QuantityStrategy = ChargesProcessorUtils.QUANTITY_STRATEGY_DECOMPOSITION;
					}
				}

				Integer csQuantity = workingAQD.csQuantity;
				if (csQuantity == null) {
					csQuantity = 1;
				}

				// CUSTOMIZED, TO BE DETERMINED
				if (workingAQD.CS_QuantityStrategy != null && workingAQD.CS_QuantityStrategy == ChargesProcessorUtils.QUANTITY_STRATEGY_MULTIPLIER) {
					targetPC.cscfga__Quantity__c = workingAQD.csQuantity;
				}

				workingAQD.CS_QuantityStrategy = ChargesProcessorUtils.QUANTITY_STRATEGY_DECOMPOSITION;
				// END CUSTOMIZED

				// marking for softDelete based on AQD data
				if (workingAQD.csSoftDelete == true) {
					pcsIdsToTerminate.add(targetPC.Id);
				}

				// List<SObject> customDataAttach = CustomDataProcessor.processCustomData(targetPC.Id, workingAQD.customData);
				// bulkCustomDataStore.addAll(customDataAttach);

				List<SObject> pricingInformation = ChargesProcessor.processPricing(
					targetPC,
					workingAQD.charges,
					workingAQD.discounts,
					CS_QuantityStrategy,
					csQuantity,
					isExternalPricingEnabled
				);
				bulkCustomDataStore.addAll(pricingInformation);
			}

			// not needed now - 11.05.2021 written
			//List<SObject> baskets = BasketChargesProcessor.processBasketPricing(basketIds);
			//bulkCustomDataStore.addAll(baskets);

			List<cscfga__Attribute__c> rootConfigAttributes = [
				SELECT Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c, cscfga__Product_Configuration__r.cscfga__Product_Basket__c
				FROM cscfga__Attribute__c
				WHERE cscfga__Product_Configuration__c IN :rootConfigs
			];

			Map<Id, String> basketIdContractDurationMobile = new Map<Id, String>();
			Map<Id, Decimal> basketIdConnectionTypesQuantity = new Map<Id, Decimal>(); //Number_of_ctn__c

			if (rootConfigs != null && rootConfigs.size() > 0) {
				for (Id rootConfigId : rootConfigs) {
					if (pcsWithAQD.get(rootConfigId) != null) {
						if (pcsWithAQD.get(rootConfigId).connectionTypes != null) {
							for (CCAQDProcessor.ConnectionType connTypeItem : pcsWithAQD.get(rootConfigId).connectionTypes) {
								Decimal quantityTmp = Decimal.valueOf(connTypeItem.quantity);
								Integer tmpQuantity = rootConfigIdQuantity.get(rootConfigId);
								rootConfigIdQuantity.put(rootConfigId, (Integer) tmpQuantity + (Integer) quantityTmp);

								if (basketIdConnectionTypesQuantity.containsKey(rootPcIdBasketId.get(rootConfigId))) {
									Decimal quantityMapValue = basketIdConnectionTypesQuantity.get(rootPcIdBasketId.get(rootConfigId));
									quantityMapValue += quantityTmp;
									basketIdConnectionTypesQuantity.put(rootPcIdBasketId.get(rootConfigId), quantityMapValue);
								} else {
									basketIdConnectionTypesQuantity.put(rootPcIdBasketId.get(rootConfigId), quantityTmp);
								}
							}
						}
					}
				}
			}

			if (rootConfigAttributes != null && rootConfigAttributes.size() > 0) {
				for (cscfga__Attribute__c attrItem : rootConfigAttributes) {
					if (attrItem.Name == 'contractDuration') {
						basketIdContractDurationMobile.put(
							attrItem.cscfga__Product_Configuration__r.cscfga__Product_Basket__c,
							attrItem.cscfga__Value__c
						);
					}
				}
			}

			List<SObject> baskets = [
				SELECT
					Id,
					Number_of_ctn__c,
					Contract_duration_Mobile__c,
					Expected_delivery_date_for_Mobile__c,
					cscfga__Basket_Status__c,
					cscfga__Opportunity__c
				FROM cscfga__Product_Basket__c
				WHERE Id IN :basketIds
			];
			for (SObject basket : baskets) {
				Integer basketContractDuration = 24;
				basket.put('Primary__c', true);
				basket.put('Is_Online_Basket__c', true);
				basket.put('Expected_delivery_date_for_Mobile__c', Date.today());
				basket.put('cscfga__Basket_Status__c', 'Contract created');

				if (hasMobileConfigurations) {
					basket.put('Data_Capping__c', '50');
				}

				if (basketIdContractDurationMobile.containsKey(basket.Id)) {
					basketContractDuration = Integer.valueOf(basketIdContractDurationMobile.get(basket.Id));
					basket.put('Contract_duration_Mobile__c', String.valueOf(basketContractDuration));
				}

				if (basketContractDuration >= 24) {
					for (Id rootPCId : rootConfigIdQuantity.keySet()) {
						Integer tmpQuantity = rootConfigIdQuantity.get(rootPCId);
						Id tmpBasketId = rootPcIdBasketId.get(rootPCId);

						if (basketIdNmbOfDisconnects.containsKey(tmpBasketId)) {
							Decimal tmpNmbDisconnects = basketIdNmbOfDisconnects.get(tmpBasketId);
							Decimal tmpCurrentCalculation = tmpQuantity * 0.1;
							System.debug('tmpCurrentCalculation - ' + tmpCurrentCalculation);
							tmpNmbDisconnects += tmpCurrentCalculation.round(System.RoundingMode.HALF_UP);
							System.debug('tmpNmbDisconnects - ' + tmpNmbDisconnects);
							basketIdNmbOfDisconnects.put(tmpBasketId, tmpNmbDisconnects);
						} else {
							Decimal tmpCurrentCalculation = tmpQuantity * 0.1;
							System.debug('tmpCurrentCalculation - ' + tmpCurrentCalculation);
							Decimal tmpNmbDisconnects = tmpCurrentCalculation.round(System.RoundingMode.HALF_UP);
							System.debug('tmpNmbDisconnects - ' + tmpNmbDisconnects);
							basketIdNmbOfDisconnects.put(tmpBasketId, tmpNmbDisconnects);
						}
					}

					System.debug('basketIdNmbOfDisconnects - ' + basketIdNmbOfDisconnects);
				}

				if (basketIdConnectionTypesQuantity.containsKey(basket.Id)) {
					Decimal tmpDisconnect = basketIdNmbOfDisconnects.get(basket.Id) == null ? 0 : basketIdNmbOfDisconnects.get(basket.Id);
					basket.put('Number_of_ctn__c', basketIdConnectionTypesQuantity.get(basket.Id) - tmpDisconnect);
					basket.put('Number_of_CTN_Disconnect__c', tmpDisconnect);
				}
			}

			Map<String, Object> args = new Map<String, Object>{ 'pcIds' => pcsIdsToTerminate, 'basketId' => basketId };

			Callable telcoAPICallable = new csordtelcoa.API_V1();
			telcoAPICallable.call('softDeleteConfiguration', args);

			bulkCustomDataStore.addAll(baskets);

			Map<Id, cscfga__Product_Configuration__c> rootPCsForUpdate = new Map<Id, cscfga__Product_Configuration__c>();
			for (Id rootPCId : rootConfigIdQuantity.keySet()) {
				Decimal tmpQuantity = rootConfigIdQuantity.get(rootPCId);
				if (tmpQuantity == 0)
					tmpQuantity = 1;
				cscfga__Product_Configuration__c pcSObject = new cscfga__Product_Configuration__c(
					Id = rootPCId,
					cscfga__Quantity__c = rootConfigIdQuantity.get(rootPCId)
				);
				rootPCsForUpdate.put(rootPCId, pcSObject);
			}
			//List<SObject> sobjlist = new List<SObject>(rootPCsForUpdate);
			//bulkCustomDataStore.addAll(sobjlist);

			List<SObject> insertList = new List<SObject>();
			List<SObject> updateList = new List<SObject>();

			for (SObject iter : bulkCustomDataStore) {
				if (iter.Id == null) {
					insertList.add(iter);
				} else {
					updateList.add(iter);
				}
			}

			for (Id rootPcId : rootPCsForUpdate.keySet()) {
				Boolean itemFound = false;
				for (SObject iter : updateList) {
					if (iter.Id == rootPcId) {
						itemFound = true;
						iter.put('cscfga__Quantity__c', rootPCsForUpdate.get(rootPcId).cscfga__Quantity__c);
					}
				}

				if (!itemFound) {
					updateList.add(rootPCsForUpdate.get(rootPcId));
				}
			}

			insert insertList;
			update updateList;
			Set<Id> basketIdsSet = new Set<Id>();
			basketIdsSet.addAll(basketIds);
			cscfga.ProductConfigurationBulkActions.calculateTotals(basketIdsSet);
		}
	}

	global static void generateCtnRecordsOnlineScenario(List<cscfga__Product_Basket__c> baskets) {
		List<NetProfit_Information__c> npInfoToInsert = new List<NetProfit_Information__c>();
		List<NetProfit_CTN__c> npCtnToInsert = new List<NetProfit_CTN__c>();
		Map<Id, Opportunity> oppsById = new Map<Id, Opportunity>();
		Map<Id, NetProfit_Information__c> netProfitInfoByOppId = new Map<Id, NetProfit_Information__c>();

		for (cscfga__Product_Basket__c basket : baskets) {
			oppsById.put(basket.cscfga__Opportunity__c, null);
		}

		for (NetProfit_Information__c existingNpInfo : [
			SELECT Id, Opportunity__c
			FROM NetProfit_Information__c
			WHERE Opportunity__c IN :oppsById.keySet()
		]) {
			netProfitInfoByOppId.put(existingNpInfo.Opportunity__c, existingNpInfo);
		}

		oppsById = new Map<Id, Opportunity>([SELECT Id, Segment__c, Type_of_service__c FROM Opportunity WHERE Id IN :oppsById.keySet()]);

		for (Opportunity opp : oppsById.values()) {
			if (opp.Segment__c == 'SoHo' && opp.Type_of_service__c == 'Mobile' && !netProfitInfoByOppId.containsKey(opp.Id)) {
				NetProfit_Information__c npInfo = new NetProfit_Information__c(Opportunity__c = opp.Id, Status__c = 'New');
				npInfoToInsert.add(npInfo);
			}
		}
		System.debug('npInfoToInsert -> ' + npInfoToInsert);
		insert npInfoToInsert;

		for (NetProfit_Information__c npInfo : npInfoToInsert) {
			NetProfitCTNGenerator generator = new NetProfitCTNGenerator(npInfo.Opportunity__c);
			generator.npInfoId = npInfo.Id;
			npCtnToInsert.addAll(generator.generateCTNInfo());
		}

		System.debug('npCtnToInsert -> ' + npCtnToInsert);
		insert npCtnToInsert;
	}

	global void execute(csam.ObserverApi.Observable o, Object arg) {
		String debug = 'CC ORDER Observer execute, observable: ' + o + ', args: ' + arg;
		System.debug(debug);

		if (o instanceof csb2c.ProductBasketObservable) {
			try {
				csb2c.ProductBasketObservable observable = (csb2c.ProductBasketObservable) o;
				execute(observable.getBasketIds());
			} catch (Exception e) {
				System.debug(e.getMessage() + ' | ' + e.getStackTraceString());
				Attachment errorOut = new Attachment();
				errorOut.Name = 'Error during observer';
				csb2c.ProductBasketObservable observable = (csb2c.ProductBasketObservable) o;

				if (observable.getBasketIds() != null)
					errorOut.ParentId = observable.getBasketIds()[0];

				errorOut.Body = Blob.valueOf(e.getMessage() + ' | ' + e.getStackTraceString());

				if (errorOut.ParentId != null)
					insert errorOut;
			}
		}
	}
}
