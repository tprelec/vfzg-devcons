@isTest
private class TestBillingArrangementTriggerHandler {
        
        @isTest()
        static void testUpdateBAR() {

			User owner = TestUtils.createAdministrator();
   			Account acct1 = TestUtils.createAccount(owner);
   			Contact cont1 = TestUtils.createContact(acct1);
   			Ban__c ban1 = TestUtils.createBan(acct1);
   			Financial_Account__c fa1 = TestUtils.createFinancialAccount(ban1,cont1.Id);

   			Billing_Arrangement__c bar1 = TestUtils.createBillingArrangement(fa1);

	        Test.startTest();

	        bar1.Payment_Method__c = 'Creditcard';
	        update bar1;

	        Test.stopTest();

	        fa1 = [Select Id, Payment_Method__c From Financial_Account__c Where Id = :fa1.Id];
	        System.assertEquals('Creditcard', fa1.Payment_method__c);

        }       

        
}