@IsTest
private class TestAccountMassSharingController {

    @testsetup
    private static void setupTestData() {

        TestUtils.createAccount(GeneralUtils.currentUser);
    }

    @IsTest
    private static void test_MassShare_NoUserSharingRecord() {

        Account account = [SELECT Id, Name, OwnerId FROM Account LIMIT 1];

        Test.startTest();

        System.RunAs(getUserInstance()) {

            List<Account> lstAccount = new List<Account> { account };

            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(lstAccount);

            controller.setSelected(lstAccount);
            AccountMassSharingController classInstance = new AccountMassSharingController(controller);

            classInstance.objQueueUserSharing.ownerId = UserInfo.getUserId();

            classInstance.save();

            verifyNoPageMessages();
        }

        Test.stopTest();

        List<Queue_User_Sharing__c> lstQueueUserSharing = [SELECT Id FROM Queue_User_Sharing__c LIMIT 10];

        System.assertEquals(1, lstQueueUserSharing.size());

        System.assertEquals(lstQueueUserSharing[0].Id,
                [SELECT Id, Queue_User_Assigned__c FROM Account LIMIT 1].Queue_User_Assigned__c);
    }

    @IsTest
    private static void test_MassShare_WithUserSharingRecord() {

        Account account = [SELECT Id, Name, OwnerId FROM Account LIMIT 1];

        Test.startTest();

        System.RunAs(getUserInstance()) {

            Queue_User_Sharing__c objQueueUserSharing = new Queue_User_Sharing__c(OwnerId = UserInfo.getUserId());
            insert objQueueUserSharing;
            System.assertNotEquals(null, objQueueUserSharing.Id);

            List<Account> lstAccount = new List<Account> { account };

            ApexPages.StandardSetController controller = new ApexPages.StandardSetController(lstAccount);

            controller.setSelected(lstAccount);
            AccountMassSharingController classInstance = new AccountMassSharingController(controller);

            classInstance.objQueueUserSharing.ownerId = UserInfo.getUserId();

            classInstance.save();

            verifyNoPageMessages();

        }

        Test.stopTest();

        List<Queue_User_Sharing__c> lstQueueUserSharing = [SELECT Id FROM Queue_User_Sharing__c LIMIT 10];

        System.assertEquals(1, lstQueueUserSharing.size());

        System.assertEquals(lstQueueUserSharing[0].Id,
                [SELECT Id, Queue_User_Assigned__c FROM Account LIMIT 1].Queue_User_Assigned__c);
    }

    @IsTest
    private static void test_MassShare_NoSelectedOwner() {

        Account account = [SELECT Id, Name, OwnerId FROM Account LIMIT 1];

        System.assertEquals(0, [SELECT COUNT() FROM Queue_User_Sharing__c LIMIT 1]);

        Test.startTest();

        List<Account> lstAccount = new List<Account> { account };

        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(lstAccount);

        controller.setSelected(lstAccount);
        AccountMassSharingController classInstance = new AccountMassSharingController(controller);

        classInstance.save();

        verifyNoPageMessages();

        Test.stopTest();

        // verify no records for queue user sharing
        System.assertEquals(0, [SELECT COUNT() FROM Queue_User_Sharing__c LIMIT 1]);

        System.assertEquals(null,
                [SELECT Id, Queue_User_Assigned__c FROM Account LIMIT 1].Queue_User_Assigned__c);
    }

    // below method covers the handleException private method to increase code coverage
    @IsTest
    private static void test_MassShare_CoverException() {

        ApexPages.StandardSetController controller = new ApexPages.StandardSetController(new List<Account>());
        CustomException objEx = new CustomException('some exception');
        System.Savepoint savepoint = Database.setSavepoint();
        new AccountMassSharingController(controller).handleException(objEx, savepoint);


        List<ApexPages.Message> pageMessages = ApexPages.getMessages();
        System.assertEquals(1, pageMessages.size(), 'There must be one exception message');
        System.assertEquals(pageMessages[0].getDetail(), objEx.getMessage());
    }

    private class CustomException extends Exception {}

    private static User getUserInstance() {
        return TestUtils.generateTestUser('FirstName', 'LastName', 'System Administrator');
    }

    private static void verifyNoPageMessages() {
        List<ApexPages.Message> pageMessages = ApexPages.getMessages();
        System.assertEquals(0, pageMessages.size(), 'Unwanted page message found: ' + pageMessages);
    }
}