@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class OrchGetCPDExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
	private Map<Id, String> mapOrderId;
	private Map<Id, String> mapBillingCustomer;
	private Map<Id, String> mapContractId;
	private Map<Id, EMP_CustomerProductDetails.Response> calloutResultsMap = new Map<Id, EMP_CustomerProductDetails.Response>();

	public Boolean performCallouts(List<SObject> data) {
		Boolean calloutsPerformed = false;

		try {
			List<CSPOFA__Orchestration_Step__c> steps = (List<CSPOFA__Orchestration_Step__c>) data;
			Set<Id> resultIds = GeneralUtils.getIDSetFromList(
				steps,
				'CSPOFA__Orchestration_Process__c'
			);
			OrchUtils.getProcessDetails(resultIds);

			mapOrderId = OrchUtils.mapOrderId;
			mapBillingCustomer = OrchUtils.mapBillingCustomer;
			mapContractId = OrchUtils.mapContractId;

			for (CSPOFA__Orchestration_Step__c step : steps) {
				Id processId = step.CSPOFA__Orchestration_Process__c;
				String orderId = mapOrderId.get(processId);
				String billingCustomerId = mapBillingCustomer.get(processId);

				calloutResultsMap.put(
					step.Id,
					EMP_BSLintegration.getCustomerProductDetails(orderId, billingCustomerId)
				);
				calloutsPerformed = true;
			}
		} catch (Exception e) {
			System.debug(
				LoggingLevel.DEBUG,
				'Orchestartor step error (Get Customer Product Details): ' +
				getFormateddErrorMessage(e)
			);
		}

		return calloutsPerformed;
	}

	public List<sObject> process(List<SObject> data) {
		List<sObject> results = new List<sObject>();
		List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
		List<CSPOFA__Orchestration_Step__c> steps = (List<CSPOFA__Orchestration_Step__c>) data;

		for (CSPOFA__Orchestration_Step__c step : steps) {
			if (!calloutResultsMap.containsKey(step.Id)) {
				step = OrchUtils.setStepRecord(
					step,
					true,
					'Error occurred: Callout results not received.'
				);
				results.add(step);
				continue;
			}

			EMP_CustomerProductDetails.Response calloutResult = calloutResultsMap.get(step.Id);
			step.Request__c = calloutResult.httpRequest;
			step.Response__c = calloutResult.httpResponse;

			if (calloutResult == null || !calloutResult.error.isEmpty()) {
				step = OrchUtils.setStepRecord(
					step,
					true,
					'Error occurred: ' + calloutResult.getErrorMessages()
				);
				results.add(step);
				continue;
			}

			step = OrchUtils.setStepRecord(
				step,
				false,
				'Get Customer Product Details step completed'
			);
			String contractId = mapContractId.get(step.CSPOFA__Orchestration_Process__c);

			calloutResult.httpRequest = null;
			calloutResult.httpResponse = null;

			contractsToUpdate.add(
				new VF_Contract__c(
					Id = contractId,
					Customer_Product_Details__c = JSON.serialize(calloutResult, false)
				)
			);

			results.add(step);
		}

		return contractsToUpdate.isEmpty()
			? results
			: updateVfContracts(steps, results, contractsToUpdate);
	}

	@TestVisible
	private List<SObject> updateVfContracts(
		List<CSPOFA__Orchestration_Step__c> steps,
		List<SObject> results,
		List<VF_Contract__c> contractsToUpdate
	) {
		try {
			Map<Id, String> failedContractUpdateMap = new Map<Id, String>();
			List<Database.SaveResult> updateResults = Database.update(contractsToUpdate, false);

			for (Integer i = 0; i < updateResults.size(); i++) {
				Database.SaveResult updateResult = updateResults.get(i);

				if (!updateResult.isSuccess()) {
					failedContractUpdateMap.put(
						contractsToUpdate.get(i).Id,
						updateResult.getErrors().get(0).getMessage()
					);
				}
			}

			if (failedContractUpdateMap.isEmpty()) {
				return results;
			}

			for (CSPOFA__Orchestration_Step__c step : steps) {
				String contractId = mapContractId.get(step.CSPOFA__Orchestration_Process__c);

				if (failedContractUpdateMap.containsKey(contractId)) {
					String error =
						'Error occurred: Contract update failed with error: ' +
						failedContractUpdateMap.get(contractId);
					results.add(OrchUtils.setStepRecord(step, true, error));
				}
			}
		} catch (Exception e) {
			for (CSPOFA__Orchestration_Step__c step : steps) {
				String errorInfo =
					'Error occurred (exception thrown): ' + getFormateddErrorMessage(e);
				results.add(OrchUtils.setStepRecord(step, true, errorInfo.abbreviate(255)));
			}
		}
		return results;
	}

	private String getFormateddErrorMessage(Exception e) {
		return e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString();
	}
}