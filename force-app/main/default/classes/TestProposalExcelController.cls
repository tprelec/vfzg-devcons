@isTest
private class TestProposalExcelController {
	@isTest
	static void testCheckAccount() {
		PageReference pageRef = Page.ProposalPage;
		Test.setCurrentPage(pageRef);
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
		basket.cscfga__Opportunity__c = TestUtils.theOpportunity.id;
		basket.Name = 'New Basket ' + system.now();
		insert basket;

		Test.startTest();
		pageRef.getParameters().put('id', basket.Id);
		//Apexpages.StandardController sc = new Apexpages.standardController(TestUtils.theOpportunity);
		ProposalExcelController controller = new ProposalExcelController();

		ProposalExcelController.AccountDetails ad = controller.acc;
		Test.stopTest();
	}

	@isTest
	static void testCheckBasket() {
		PageReference pageRef = Page.ProposalPage;
		Test.setCurrentPage(pageRef);
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		Opportunity opp = TestUtils.theOpportunity;

		insert new PriceReset__c(ConfigurationName__c = 'Test');

		Test.startTest();

		/** Create Basket */
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
		basket.csbb__Account__c = opp.AccountId;
		basket.cscfga__Opportunity__c = opp.Id;
		basket.cscfga__Basket_Status__c = 'Incomplete';
		basket.Primary__c = true;
		insert basket;

		cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c();
		pc.cscfga__Product_Basket__c = basket.Id;
		pc.Name = 'Mobile CTN profile';
		insert pc;

		cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c();
		pc2.cscfga__Product_Basket__c = basket.Id;
		pc2.Name = 'Mobile CTN profile2';
		pc2.Site_Check_SiteID__c = '0x0';
		insert pc2;

		pageRef.getParameters().put('id', basket.Id);
		ProposalExcelController controller = new ProposalExcelController();
		List<ProposalExcelController.BasketDetail> basketList = controller.basketDetailList;

		Test.stopTest();
	}

	/**
	 * Needs to be changed to run through the actual products, now only for code coverage
	 */
	@isTest
	static void testCompleteBasket() {
		PageReference pageRef = Page.ProposalPage;
		Test.setCurrentPage(pageRef);

		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		ID rId = roleList[0].Id;
		ID pId = pList[0].Id;

		User simpleUser = new User(
			UserRoleId = rId,
			ProfileId = pId,
			Alias = 'standard',
			Email = 'standarduser@testorg.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'Testing',
			LanguageLocaleKey = 'nl_NL',
			LocaleSidKey = 'nl_NL',
			TimeZoneSidKey = 'Europe/Amsterdam',
			UserName = 'testUserAAAA1234@testorganise.com'
		);

		insert simpleUser;
		System.runAs(simpleUser) {
			Test.startTest();

			No_Triggers__c notriggers = new No_Triggers__c();
			notriggers.SetupOwnerId = simpleUser.Id;
			notriggers.Flag__c = true;
			insert notriggers;

			PriceReset__c priceResetSettings = new PriceReset__c();
			priceResetSettings.ConfigurationName__c = 'Child Config';
			priceResetSettings.ErrorMessage__c = 'Error message 1';
			priceResetSettings.ErrorMessage2__c = 'Error message 2';
			priceResetSettings.MaxRecurringPrice__c = 1;
			priceResetSettings.RecurringAttributeName__c = 'Recurring Attribute';
			insert priceResetSettings;

			DealTypeSettings__c dealTypeSettings = new DealTypeSettings__c();
			dealTypeSettings.Location_Configuration_Name__c = 'Location Config Name';
			dealTypeSettings.Deal_Type_Attribute_Name__c = 'ConfigName';
			insert dealTypeSettings;

			Account tmpAcc = CS_DataTest.createAccount('Test Account');
			insert tmpAcc;

			Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', simpleUser.Id);
			insert tmpOpp;

			cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
			tmpProductBasket.Error_message__c = 'super errrrrror message';
			tmpProductBasket.Number_of_SIP__c = 20;
			tmpProductBasket.Fixed_Scenario__c = 'One Fixed Enterprise';
			tmpProductBasket.OneNet_Scenario__c = 'One Net Enterprise';
			insert tmpProductBasket;

			cscfga__Product_Definition__c pd = CS_DataTest.createProductDefinition('Access Infrastructure');
			pd.Product_Type__c = 'Fixed';
			insert pd;

			cscfga__Product_Configuration__c pc = CS_DataTest.createProductConfiguration(pd.Id, 'Access Infrastructure', tmpProductBasket.Id);
			pc.cscfga__Contract_Term__c = 24;
			pc.cscfga__total_one_off_charge__c = 100;
			pc.cscfga__Total_Price__c = 20;
			pc.cspl__Type__c = 'Mobile Voice Services';
			pc.cscfga__Contract_Term_Period__c = 12;
			pc.cscfga__Configuration_Status__c = 'Valid';
			pc.cscfga__Quantity__c = 1;
			pc.cscfga__total_recurring_charge__c = 22;
			pc.RC_Cost__c = 0;
			pc.NRC_Cost__c = 0;
			pc.cscfga__one_off_charge_product_discount_value__c = 0;
			pc.cscfga__recurring_charge_product_discount_value__c = 0;
			pc.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
			insert pc;

			cscfga__Product_Configuration__c childPc = CS_DataTest.createProductConfiguration(pd.Id, 'Child Config', tmpProductBasket.Id);
			childPc.cscfga__Parent_Configuration__c = pc.Id;
			insert childPc;

			cscfga__Attribute_Definition__c adRecurringAttribute = new cscfga__Attribute_Definition__c(
				Name = 'Recurring Attribute',
				cscfga__Product_Definition__c = pd.Id,
				cscfga__Data_Type__c = 'String'
			);
			insert adRecurringAttribute;

			cscfga__Attribute__c recurringAttribute = new cscfga__Attribute__c(
				Name = 'Recurring Attribute',
				cscfga__Attribute_Definition__c = adRecurringAttribute.Id,
				cscfga__Product_Configuration__c = childPc.Id,
				cscfga__is_active__c = true,
				cscfga__Value__c = '50',
				cscfga__Price__c = 50
			);
			insert recurringAttribute;

			cscfga__Attribute_Definition__c locationPD = new cscfga__Attribute_Definition__c(
				Name = 'IPVPN',
				cscfga__Product_Definition__c = pd.Id,
				cscfga__Data_Type__c = 'String'
			);
			insert locationPD;

			cscfga__Attribute__c locationAttribute = new cscfga__Attribute__c(
				Name = 'IPVPN',
				cscfga__Attribute_Definition__c = locationPD.Id,
				cscfga__Product_Configuration__c = pc.Id,
				cscfga__is_active__c = true,
				cscfga__Value__c = pc.Id,
				cscfga__Price__c = 50
			);
			insert locationAttribute;

			cscfga__Attribute_Definition__c configPD = new cscfga__Attribute_Definition__c(
				Name = 'ConfigName',
				cscfga__Product_Definition__c = pd.Id,
				cscfga__Data_Type__c = 'String'
			);
			insert configPD;

			cscfga__Attribute__c configAttribute = new cscfga__Attribute__c(
				Name = 'ConfigName',
				cscfga__Attribute_Definition__c = configPD.Id,
				cscfga__Product_Configuration__c = pc.Id,
				cscfga__is_active__c = true,
				cscfga__Value__c = 'Location Config Name',
				cscfga__Price__c = 50
			);
			insert configAttribute;

			cscfga__Attribute_Definition__c secondaryPD = new cscfga__Attribute_Definition__c(
				Name = 'Secondary',
				cscfga__Product_Definition__c = pd.Id,
				cscfga__Data_Type__c = 'String'
			);
			insert secondaryPD;

			cscfga__Attribute__c secondaryAttribute = new cscfga__Attribute__c(
				Name = 'Secondary',
				cscfga__Attribute_Definition__c = secondaryPD.Id,
				cscfga__Product_Configuration__c = pc.Id,
				cscfga__is_active__c = true,
				cscfga__Value__c = 'No',
				cscfga__Price__c = 50
			);
			insert secondaryAttribute;

			ApexPages.StandardController stdController = new ApexPages.StandardController(tmpProductBasket);
			CS_CustomPostConfiguringController customPost = new CS_CustomPostConfiguringController(stdController);

			CS_CustomPostConfiguringController.resetPrices(tmpProductBasket.Id);
			CS_CustomPostConfiguringController.containsConfigPerName(
				tmpProductBasket.Id,
				'Access Infrastructure',
				new List<cscfga__Product_Configuration__c>{ pc, childPc }
			);
			CS_CustomPostConfiguringController.getSumPerBasket(
				tmpProductBasket.Id,
				new List<cscfga__Attribute__c>{ recurringAttribute },
				new List<cscfga__Product_Configuration__c>{ pc, childPc }
			);

			system.debug('****pc.Id: ' + pc.Id);
			CS_CustomPostConfiguringController.propagateLocationDealType(tmpProductBasket.Id, pc.Id);

			pageRef = Page.ProposalPage;
			Test.setCurrentPage(pageRef);
			pageRef.getParameters().put('id', tmpProductBasket.Id);
			ProposalExcelController controller = new ProposalExcelController();

			Opportunity opp = controller.opp;
			List<ProposalExcelController.BasketDetail> basketList = controller.basketDetailList;

			/** basketListCounter */
			String basketListCounter = controller.basketListCounter;
			system.assertEquals('4', basketListCounter);

			/** sumProductTotalPrice */
			Decimal sumProductTotalPrice = controller.sumProductTotalPrice;
			system.assertEquals(0, sumProductTotalPrice);

			/** sumOneOffPrice */
			Decimal sumOneOffPrice = controller.sumOneOffPrice;
			system.assertEquals(0, sumOneOffPrice);

			/** sumRecurringPrice */
			Decimal sumRecurringPrice = controller.sumRecurringPrice;
			system.assertEquals(0, sumRecurringPrice);

			/** totalPrice */
			Decimal totalPrice = controller.totalPrice;
			system.assertEquals(0, totalPrice);

			Test.stopTest();
		}
	}

	/**
	 * Needs to be changed to run through the actual products, now only for code coverage
	 */
	@isTest
	static void testRunThroughmethodsAndVariables() {
		PageReference pageRef = Page.ProposalPage;
		Test.setCurrentPage(pageRef);

		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		TestUtils.createCompleteContract();

		Test.startTest();
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
		basket.cscfga__Opportunity__c = TestUtils.theOpportunity.id;
		basket.Name = 'New Basket ' + system.now();
		insert basket;

		pageRef.getParameters().put('id', basket.Id);
		ProposalExcelController controller = new ProposalExcelController();

		String xmlHeader = controller.xmlheader;
		List<ProposalExcelController.BasketDetail> bdList = new List<ProposalExcelController.BasketDetail>();
		ProposalExcelController.BasketDetail bd = new ProposalExcelController.BasketDetail();
		bd.siteId = '0x0';
		bd.locationnumber = 1;
		bd.siteName = '0x0';
		bd.durationMobile = '12';
		bd.durationFixed = '12';
		bd.totalLocations = 3;
		ProposalExcelController.ProductItem pi = new ProposalExcelController.ProductItem();
		pi.productName = 'test Product';
		pi.productUnitPrice = 2;
		pi.recurringDiscount = 1;
		pi.recurring = true;
		pi.recurringAnnualy = 2;
		pi.oneOffPrice = 2;
		pi.productTotalPrice = 2;
		pi.oneOffDiscount = 1;
		pi.recurringPrice = 1;
		pi.quantity = 1;
		pi.recurringTotalPrice = 1;

		bd.prodItems.add(pi);
		bdList.add(bd);
		bdList.sort();

		/** basketListCounter */
		String basketListCounter = controller.basketListCounter;
		system.assertEquals('3', basketListCounter);

		/** sumBrutRecurringTotalPrice */
		Decimal sumBrutRecurringTotalPrice = controller.sumBrutRecurringTotalPrice;
		system.assertEquals(0, sumBrutRecurringTotalPrice);

		/** sumBrutProductTotalPrice */
		Decimal sumBrutProductTotalPrice = controller.sumBrutProductTotalPrice;
		system.assertEquals(0, sumBrutProductTotalPrice);

		Decimal sumOneOffDiscount = controller.sumOneOffDiscount;
		system.assertEquals(0, sumOneOffDiscount);

		Decimal sumRecurringTotalPrice = controller.sumRecurringTotalPrice;
		system.assertEquals(0, sumRecurringTotalPrice);

		Decimal sumRecurringDiscount = controller.sumRecurringDiscount;
		system.assertEquals(0, sumRecurringDiscount);

		/** sumProductTotalPrice */
		Decimal sumProductTotalPrice = controller.sumProductTotalPrice;
		system.assertEquals(0, sumProductTotalPrice);

		/** sumOneOffPrice */
		Decimal sumOneOffPrice = controller.sumOneOffPrice;
		system.assertEquals(0, sumOneOffPrice);

		/** sumRecurringPrice */
		Decimal sumRecurringPrice = controller.sumRecurringPrice;
		system.assertEquals(0, sumRecurringPrice);

		/** totalPrice */
		Decimal totalPrice = controller.totalPrice;
		system.assertEquals(0, totalPrice);

		System.assertEquals(bd.compareTo(bd), 1);
		Decimal totalLocationrecurringDiscount = bd.totalLocationrecurringDiscount;
		Decimal totalLocationoneproductTotalPrice = bd.totalLocationoneproductTotalPrice;
		Decimal totalLocationoneproductTotalPriceNet = bd.totalLocationoneproductTotalPriceNet;
		Decimal totalLocationonerecurringTotalPrice = bd.totalLocationonerecurringTotalPrice;
		Decimal totalLocationonerecurringTotalPriceNet = bd.totalLocationonerecurringTotalPriceNet;
		System.assertEquals(bd.totalLocationrecurringDiscount, 1);
		System.assertEquals(bd.totalLocationoneproductTotalPrice, 3);
		System.assertEquals(bd.totalLocationoneproductTotalPriceNet, 2);
		System.assertEquals(bd.totalLocationonerecurringTotalPrice, 2);
		System.assertEquals(bd.totalLocationonerecurringTotalPriceNet, 1);

		Test.stopTest();
	}
}
