@IsTest
public with sharing class TestManageExistingInfrasController {
	@TestSetup
	static void makeData() {
		Account a = TestUtils.createAccount(null);
		Site__c s = TestUtils.createSite(a);

		Site_Postal_Check__c spc = new Site_Postal_Check__c();
		spc.Access_Site_ID__c = s.Id;
		spc.Access_Active__c = true;
		spc.Access_Vendor__c = 'IPVPN-FLEX';
		spc.Technology_Type__c = 'Fiber';
		spc.Access_Max_Down_Speed__c = 100;
		spc.Access_Max_Up_Speed__c = 100;
		spc.Existing_Infra__c = true;

		insert spc;
	}

	@IsTest
	public static void testSaveInfras() {
		Account acc = [SELECT Id FROM Account LIMIT 1];

		Site__c site = [SELECT Id, Active__c FROM Site__c WHERE Site_Account__c = :acc.Id];

		Site_Postal_Check__c spc = [SELECT Id, Access_Vendor__c FROM Site_Postal_Check__c];

		spc.Access_Vendor__c = 'IPVPN-VF';

		List<Site_Postal_Check__c> spcs = new List<Site_Postal_Check__c>();
		spcs.add(spc);
		Test.startTest();

		String result = ManageExistingInfrasController.saveInfras(spcs);

		Site_Postal_Check__c spcUpdated = [SELECT Id, Access_Vendor__c FROM Site_Postal_Check__c];
		System.assertEquals('IPVPN-VF', spcUpdated.Access_Vendor__c);
		Test.stopTest();
	}

	@IsTest
	public static void testSaveNewInfras() {
		Account acc = [SELECT Id FROM Account LIMIT 1];

		Site__c site = [SELECT Id, Active__c FROM Site__c WHERE Site_Account__c = :acc.Id];

		Site_Postal_Check__c spc = new Site_Postal_Check__c();
		spc.Access_Site_ID__c = site.Id;
		spc.Access_Active__c = true;
		spc.Access_Vendor__c = 'IPVPN-VF';
		spc.Technology_Type__c = 'Fiber';
		spc.Access_Max_Down_Speed__c = 100;
		spc.Access_Max_Up_Speed__c = 100;
		spc.Existing_Infra__c = true;

		List<Site_Postal_Check__c> spcs = new List<Site_Postal_Check__c>();
		spcs.add(spc);
		Test.startTest();

		List<Site_Postal_Check__c> newSpcs = ManageExistingInfrasController.saveNewInfras(spcs);

		List<Site_Postal_Check__c> spcsAfterInsert = [SELECT Id FROM Site_Postal_Check__c];
		System.assertEquals(2, spcsAfterInsert.size());
		Test.stopTest();
	}

	@IsTest
	public static void testDeleteInfras() {
		List<Site_Postal_Check__c> spc = [SELECT Id FROM Site_Postal_Check__c];

		Test.startTest();
		System.assertEquals(1, spc.size());

		String result = ManageExistingInfrasController.deleteInfras(spc);
		List<Site_Postal_Check__c> spcAfterDelete = [SELECT Id FROM Site_Postal_Check__c];

		System.assertEquals(0, spcAfterDelete.size());

		Test.stopTest();
	}
}
