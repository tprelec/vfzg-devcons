/**
 * 	@description	This class contains unit tests for the ContactTriggerHandler class
 *	@Author			Jefferson Absalon
 */
@isTest
private class TestCreditNoteTriggerHandler {
	@TestSetup
	static void makeData() {
		Contact ebu = new Contact(LastName = 'EBU', Email = 'ebusalesforce@vodafoneziggo.com');
		insert ebu;
	}

	@isTest
	static void testCreditNoteDirect() {
		//Create User with Manager
		User managedUser = TestUtils.createAdministrator();

		//to allow us to modify before insert
		TestUtils.autoCommit = false;

		//Create the Partner and Dealer accounts
		List<Account> testAccounts = new List<Account>();
		testAccounts.add(TestUtils.createAccount(managedUser));
		testAccounts.add(TestUtils.createPartnerAccount());
		insert testAccounts;

		// Create Partner and Dealer Contacts
		List<Contact> testContacts = new List<Contact>();
		testContacts.add(TestUtils.createContact(testAccounts[0]));
		testContacts.add(TestUtils.portalContact(testAccounts[1]));
		testContacts[1].Userid__c = managedUser.Id;
		insert testContacts;

		// Create Dealer Information
		Dealer_Information__c testDealer = TestUtils.createDealerInformation(testContacts[1].Id);
		insert testDealer;

		//Create customer account
		Account customerAccount = TestUtils.createAccount(managedUser, testDealer);
		insert customerAccount;
		customerAccount.Fixed_Credit_Approver__c = testDealer.Id;
		update customerAccount;

		Test.startTest();
		List<VF_Asset__c> newAssets = new List<VF_Asset__c>();
		newAssets.add(createAsset(customerAccount.Id, testDealer.Dealer_Code__c, false));
		insert newAssets;

		User myAccountManager = TestUtils.createAccountManager();
		User mysalesManager = TestUtils.createManager();
		CreditNote_Approvals__c newDirector = new CreditNote_Approvals__c();
		newDirector.Name = 'Corperate';
		newDirector.User__c = myAccountManager.Id;
		newDirector.Is_Director__c = true;
		insert newDirector;

		List<VF_Contract__c> contractsVF = [SELECT Id, Name, Account__c, BAN__c, Contract_Number__c, No_of_CTN_Assets__c FROM VF_Contract__c];
		contractsVF[0].Dealer_Information__c = testDealer.Id;
		contractsVF[0].No_of_CTN_Assets__c = 1;
		update contractsVF[0];
		List<VF_Contract__c> contractsList = [
			SELECT
				Id,
				OwnerId,
				Dealer_Information__c,
				Dealer_Information__r.ContactUserid__c,
				Dealer_Information__r.Contact__r.Userid__c,
				Dealer_Information__r.Status__c,
				Dealer_Information__r.Contact__r.Userid__r.ManagerId,
				Dealer_Information__r.Contact__r.Userid__r.Manager.ManagerId,
				Dealer_Information__r.Contact__r.Userid__r.Manager.Manager.Email,
				Dealer_Information__r.Contact__r.Userid__r.Non_Personal_User__c,
				Dealer_Information__r.Contact__r.Account.Owner.Email,
				Dealer_Information__r.Is_Dealer_in_SFDC__c
			FROM VF_Contract__c
		];

		Map<Id, Dealer_Information__c> dealerInformationMap = new Map<Id, Dealer_Information__c>();
		dealerInformationMap.put(contractsList[0].Id, contractsList[0].Dealer_Information__r);

		CreditNoteTriggerHandler.sendDealerInfoNotification(dealerInformationMap);
		List<Credit_Note__c> testCNotes = new List<Credit_Note__c>();
		//Direct
		Credit_Note__c testCNote = new Credit_Note__c();
		testCNote.RecordTypeId = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByName().get('Credit Note PH&S').getRecordTypeId();
		testCNote.Account__c = contractsVF[0].Account__c;
		testCNote.Contract_VF__c = contractsVF[0].Id;
		testCNote.Subscription__c = 'Mobile';
		testCNote.Credit_Type__c = 'Coulance';
		testCNote.Description__c = 'Creditnote';
		testCNote.Portfolio__c = 'Site_Connectivity';
		testCNote.Product_Family__c = 'Fixed_Connectivity';
		testCNote.Product_Category__c = 'Corporate Internet';
		testCNotes.add(testCNote);

		insert testCNotes;

		testCNote.Sales_Manager__c = mysalesManager.Id;
		testCNote.Account_Owner__c = myAccountManager.Id;
		testCNote.Credit_Amount__c = 100;
		testCNote.Status__c = 'Draft';
		update testCNote;

		new CreditNoteTriggerHandler();
		Test.stopTest();
	}

	@isTest
	static void testCreditNoteIndirect() {
		Account dealerAcc = new Account();
		Account clientAccount = new Account();
		User userWithManager = TestUtils.createAdministrator();
		User dealerUser = new User();
		System.runAs(userWithManager) {
			dealerAcc = TestUtils.createPartnerAccount();
			dealerUser = TestUtils.createPortalUser(dealerAcc); //VF Business Partner Manager
			update new Contact(Id = dealerUser.ContactId, UserId__c = dealerUser.Id);
			Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(dealerUser.ContactId, '999998');
			TestUtils.autoCommit = false;
			clientAccount = TestUtils.createAccount(userWithManager, dealerInfo);
			clientAccount.Fixed_Credit_Approver__c = dealerInfo.Id;
			clientAccount.Mobile_Dealer__c = dealerInfo.Id;
			insert clientAccount;
			dealerAcc.Mobile_Dealer__c = dealerInfo.Id;
			update dealerAcc;
			TestUtils.autoCommit = true;
			BAN__c clientBAN = TestUtils.createBan(clientAccount);
			Test.startTest();
			Opportunity theOpp = TestUtils.createOpportunityWithBan(clientAccount, Test.getStandardPricebookId(), clientBAN, dealerUser);
			TestUtils.autoCommit = false;
			VF_Contract__c theContract = TestUtils.createVFContract(clientAccount, theOpp);
			theContract.Dealer_Information__c = dealerInfo.Id;
			insert theContract;
			TestUtils.autoCommit = true;
		}

		CreditNote_Approvals__c newDirector = new CreditNote_Approvals__c();
		newDirector.Name = 'Corperate';
		newDirector.User__c = userWithManager.Id;
		newDirector.Is_Director__c = true;
		insert newDirector;
		CreditNote_Approvals__c newBP = new CreditNote_Approvals__c();
		newBP.Name = '(InDirect) Business Partners';
		newBP.User__c = userWithManager.Id;
		newBP.Is_Director__c = true;
		insert newBP;

		VF_Contract__c contractsVF = [
			SELECT
				Id,
				Name,
				Account__c,
				BAN__c,
				No_of_CTN_Assets__c,
				contract_number__c,
				Opportunity__r.BAN__r.Id,
				Dealer_Information__r.Is_ZSP_in_SFDC__c
			FROM VF_Contract__c
			LIMIT 1
		];
		contractsVF.No_of_CTN_Assets__c = 2;
		update contractsVF;

		List<Credit_Note__c> testCNotes = new List<Credit_Note__c>();
		//Indirect
		Credit_Note__c testCNote = new Credit_Note__c();
		testCNote.RecordTypeId = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByName()
			.get('Credit Note Business Partner Services')
			.getRecordTypeId();
		testCNote.Contract_VF__c = contractsVF.Id;
		testCNote.Subscription__c = 'Mobile';
		testCNote.Credit_Type__c = 'Coulance';
		testCNote.Account__c = clientAccount.Id;
		testCNote.Partner__c = dealerAcc.Id;
		testCNote.Description__c = 'Creditnote Indirect';
		testCNote.Portfolio__c = 'Site_Connectivity';
		testCNote.Product_Family__c = 'Fixed_Connectivity';
		testCNote.Product_Category__c = 'Corporate Internet';
		testCNote.Contract_VF__c = contractsVF.Id;
		testCNotes.add(testCNote);
		insert testCNotes;

		Test.stopTest();
	}

	@isTest
	static void testCreditNotePartner() {
		Account dealerAcc = new Account();
		Account clientAccount = new Account();
		User userWithManager = TestUtils.createAdministrator();
		User dealerUser = new User();
		Dealer_Information__c dealerInfo = new Dealer_Information__c();

		System.runAs(userWithManager) {
			dealerAcc = TestUtils.createPartnerAccount();
			dealerUser = TestUtils.createPortalUser(dealerAcc);
			update new Contact(Id = dealerUser.ContactId, UserId__c = dealerUser.Id);
			dealerInfo = TestUtils.createDealerInformation(dealerUser.ContactId, '999998');
		}
		System.runAs(dealerUser) {
			clientAccount = TestUtils.createAccount(dealerUser, dealerInfo);
			BAN__c clientBAN = TestUtils.createBan(clientAccount);
			Test.startTest();
			Opportunity theOpp = TestUtils.createOpportunityWithBan(clientAccount, Test.getStandardPricebookId(), clientBAN, dealerUser);
			TestUtils.autoCommit = false;
			VF_Contract__c theContract = TestUtils.createVFContract(clientAccount, theOpp);
			theContract.Dealer_Information__c = dealerInfo.Id;
			insert theContract;
			TestUtils.autoCommit = true;
		}

		CreditNote_Approvals__c newDirector = new CreditNote_Approvals__c();
		newDirector.Name = 'Corperate';
		newDirector.User__c = userWithManager.Id;
		newDirector.Is_Director__c = true;
		insert newDirector;

		System.runAs(dealerUser) {
			VF_Contract__c contractsVFTest = [
				SELECT
					Id,
					Name,
					Account__c,
					BAN__c,
					No_of_CTN_Assets__c,
					contract_number__c,
					Opportunity__r.BAN__r.Id,
					Dealer_Information__r.Is_ZSP_in_SFDC__c
				FROM VF_Contract__c
			];
			contractsVFTest.No_of_CTN_Assets__c = 2;
			update contractsVFTest;

			List<Credit_Note__c> testCNotes = new List<Credit_Note__c>();
			//Partner
			Credit_Note__c testCNote = new Credit_Note__c();
			testCNote.RecordTypeId = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByName()
				.get('Credit Note Business Partner Services Portal')
				.getRecordTypeId();
			testCNote.Contract_VF__c = contractsVFTest.Id;
			testCNote.Subscription__c = 'Mobile';
			testCNote.Credit_Type__c = 'Coulance';
			testCNote.Account__c = clientAccount.Id;
			testCNote.Partner__c = dealerAcc.Id;
			testCNote.Description__c = 'Creditnote Indirect';
			testCNote.Portfolio__c = 'Site_Connectivity';
			testCNote.Product_Family__c = 'Fixed_Connectivity';
			testCNote.Product_Category__c = 'Corporate Internet';
			testCNotes.add(testCNote);
			insert testCNotes;
		}
		Test.stopTest();
	}

	public static VF_Asset__c createAsset(Id accId, String dealerCode, Boolean isRetention) {
		VF_Asset__c newAsset = new VF_Asset__c();
		newAsset.Account__c = accId;
		newAsset.Dealer_Code__c = dealerCode;
		newAsset.Contract_Number__c = '1';
		newAsset.CTN_Status__c = 'Active';
		newAsset.BAN_Number__c = '399999932';
		if (isRetention) {
			newAsset.Retention_Date__c = system.today();
			newAsset.Retention_Dealer_code__c = dealerCode;
			newAsset.Contract_Start_Date__c = system.today();
			newAsset.Contract_End_Date__c = newAsset.Contract_Start_Date__c.addMonths(12);
		}
		return newAsset;
	}
}
