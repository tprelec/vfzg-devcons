@IsTest
public with sharing class TestCCAQDProcessor {
	@IsTest
	static void testExtractPCAqdData() {
		List<Profile> pList = [
			SELECT Id, Name
			FROM Profile
			WHERE Name = 'System Administrator'
			LIMIT 1
		];
		List<UserRole> roleList = [
			SELECT Id, Name, DeveloperName
			FROM UserRole u
			WHERE ParentRoleId = NULL
		];

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Framework__c frameworkSetting = new Framework__c();
			frameworkSetting.Framework_Sequence_Number__c = 2;
			insert frameworkSetting;

			PriceReset__c priceResetSetting = new PriceReset__c();

			priceResetSetting.MaxRecurringPrice__c = 200.00;
			priceResetSetting.ConfigurationName__c = 'IP Pin';

			insert priceResetSetting;

			Sales_Settings__c ssettings = new Sales_Settings__c();
			ssettings.Postalcode_check_validity_days__c = 2;
			ssettings.Max_Daily_Postalcode_Checks__c = 2;
			ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
			ssettings.Postalcode_check_block_period_days__c = 2;
			ssettings.Max_weekly_postalcode_checks__c = 15;
			insert ssettings;

			Account account = new Account(
				OwnerId = UserInfo.getUserId(),
				Name = 'Account',
				Type = 'End Customer'
			);
			insert account;

			Contact contact = new Contact(
				AccountId = account.id,
				LastName = 'Last',
				FirstName = 'First',
				Contact_Role__c = 'Consultant',
				Email = 'test@vf.com'
			);
			insert contact;

			Opportunity opportunity = new Opportunity(
				Name = 'New Opportunity',
				OwnerId = UserInfo.getUserId(),
				StageName = 'Qualification',
				Probability = 0,
				CloseDate = system.today(),
				AccountId = account.id
			);
			insert opportunity;

			cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
				Name = 'New Basket',
				Primary__c = false,
				OwnerId = UserInfo.getUserId(),
				cscfga__Opportunity__c = opportunity.Id,
				Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
			);
			insert basket;

			cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
				cscfga__Product_Basket__c = basket.Id
			);
			insert config;

			String body = '{"charges":[{"source":"recurring","salesPrice":34,"listPrice":34,"quantity":1,"name":"recurring","description":"Override charge value forrecurring","chargeType":"recurring","isProductLevelPricing":true}],"discounts":[],"customData":{},"OE":[],"CS_QuantityStrategy":"Multiplier","csQuantity":1}';

			Attachment att = new Attachment(
				Name = 'AdditionalQualificationData.json',
				Body = Blob.valueOf(body),
				ParentId = config.Id
			);
			insert att;
		}

		cscfga__Product_Basket__c pb = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];

		Map<Id, CCAQDProcessor.CCAQDStructure> data = CCAQDProcessor.extractPCAqdData(
			new List<Id>{ pb.Id }
		);

		System.assertEquals('recurring', data.values()[0].charges[0].source, 'Data missmatch.');
		System.assertEquals(34, data.values()[0].charges[0].salesPrice, 'Data missmatch.');
		System.assertEquals('Multiplier', data.values()[0].CS_QuantityStrategy, 'Data missmatch.');
	}
}