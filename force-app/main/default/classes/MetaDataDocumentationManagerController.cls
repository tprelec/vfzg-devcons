public with sharing class MetaDataDocumentationManagerController {
  
  public MetaDataDocumentationManagerController(){

    objectSelected = ApexPages.currentPage().getParameters().get('object');

    if(objectSelected == null) objectSelected = objectsList[0].getValue();
    loadFieldList();
  }


  public String objectSelected {get;set;}
  public List<fieldWrapper> fieldList {get;set;}

  public void loadFieldList(){
    // load list of fields

    fieldList = new List<fieldWrapper>();  
  
    MetadataService.MetadataPort service = createService();
    system.debug(service.readMetadata('CustomObject', new String[] { objectSelected }).getRecords()[0]);
    MetadataService.CustomObject customObject = (MetadataService.CustomObject) service.readMetadata('CustomObject', new String[] { objectSelected }).getRecords()[0];
    
    for(MetadataService.CustomField field : customObject.fields){
    //for(MetadataService.CustomField field : (service.readMetadata('CustomObject', new String[] { objectSelected }).getRecords()[0]).fields){
      fieldList.add(new FieldWrapper(field));  
      system.debug(field);
    }
  }

  public List<SelectOption> objectsList{
    get{
      if(objectsList == null){
        objectsList = new List<SelectOption>();
      
        // load list of objects
        MetadataService.MetadataPort service = createService();    
        List<MetadataService.ListMetadataQuery> queries = new List<MetadataService.ListMetadataQuery>();    
        MetadataService.ListMetadataQuery query = new MetadataService.ListMetadataQuery();
        query.type_x = 'CustomObject';
        queries.add(query);    
        MetadataService.FileProperties[] fileProperties = service.listMetadata(queries, 31);

        fileProperties.sort();
        for(MetadataService.FileProperties fileProperty : fileProperties){
          system.debug(fileProperty.fullName);
          objectsList.add(new SelectOption(fileProperty.fullName,fileProperty.fullName));
        }  
      }
      system.debug(objectsList);
      return objectsList;    

    }
    private set;
  }

  public class fieldWrapper {
    public MetadataService.CustomField theField {get;set;}
    public String theFullName{get;set;}
    public String theLabel{get;set;}
    public String theDescription{get;set;}
    public String theInlineHelpText{get;set;}
    public Boolean isStandardField{get;set;}

    public fieldWrapper(MetadataService.CustomField field){
      theField = field;
      theFullName = field.fullName;
      theLabel = field.label;
      theDescription = field.description;
      theInlineHelpText = field.inlineHelpText;
      if(theFullName.endsWith('__c')) 
        isStandardField = false;
      else
        isStandardField = true;
    }
  }


  public  pageReference updateAll(){
    List<MetadataService.CustomField> allCustomFieldsToUpdate = new List<MetadataService.CustomField>();

    MetadataService.MetadataPort service = createService();

    Integer updateCounter = 0;

    Map<String,FieldWrapper> changedFieldsToWrapper = new Map<String,FieldWrapper>();

    for(fieldWrapper fw : fieldList){
      if(fw.theFullName != fw.theField.fullName){
        Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,'Wrong object, or fields are not in the same order as the export. Please correct and try again.'));
        return null;
      }
      if((
        fw.theLabel != fw.theField.Label 
          && ((fw.theLabel != null && fw.theLabel != '') || (fw.theField.Label != null && fw.theField.Label != ''))
        ) || (
        fw.theDescription != fw.theField.Description 
          && ((fw.theDescription != null && fw.theDescription != '') || (fw.theField.Description != null && fw.theField.Description != ''))
        ) || (
        fw.theInlineHelpText != fw.theField.inlineHelpText 
          && ((fw.theInlineHelpText != null && fw.theInlineHelpText != '') || (fw.theField.inlineHelpText != null && fw.theField.inlineHelpText != ''))
        )
      ){
        changedFieldsToWrapper.put(objectSelected+'.'+fw.theField.fullName,fw);
        system.debug('adding: '+fw);
      }
    }
  
    system.debug(changedFieldsToWrapper);
    
    for(MetadataService.Metadata metaData : service.readMetadata('CustomField', New List<String>(changedFieldsToWrapper.keySet())).getRecords() ){
      system.debug(metaData);
      MetadataService.CustomField customField = (MetadataService.CustomField) metaData;
      system.debug(customField.fullName);
      system.debug(customField);          
      fieldWrapper fw = changedFieldsToWrapper.get(customField.fullName);
      
      // skip these fields for standard fields
      if(customField.fullName.endsWith('__c')){
          customField.label = fw.theLabel;
          customField.description = fw.theDescription;
      }

        customField.inlineHelptext = fw.theInlineHelpText;
        system.debug(customField);        
        allCustomFieldsToUpdate.add(customField);
    }  

      // do max 100 updates
      if(allCustomFieldsToUpdate.size() > 100){
            Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,'You can only change 100 fields at a time. Please split your update into parts of under 100 fields.'));
            return null;
      }

    List<MetadataService.CustomField> customFieldsToUpdate = new List<MetadataService.CustomField>();

      for(MetadataService.CustomField customField : allCustomFieldsToUpdate){
        customFieldsToUpdate.add(customField);

        // do a save for each 10 updates
        system.debug(updateCounter);
        if(Math.mod(updateCounter,10) == 9){
          system.debug(customFieldsToUpdate);
          List<MetadataService.SaveResult> results = service.updateMetadata(customFieldsToUpdate);    
          handleSaveResults(results);  

          customFieldsToUpdate = new List<MetadataService.CustomField>();
        }
        updateCounter++;

    }

    if(!customFieldsToUpdate.isEmpty()) {
      system.debug(customFieldsToUpdate);
      List<MetadataService.SaveResult> results = service.updateMetadata(customFieldsToUpdate);    
      handleSaveResults(results);  
    
    }  

    return null;
  }


  public String csvFileName {get;set;}
  public String csvOutput {get;set;}


  // export fields to csv
  public pageReference exportToCsv(){
    csvFileName = objectSelected+'FieldDocumentation.csv';
    csvOutput = 'API FieldName,Field Label,Description,Helptext\n';
    for(fieldWrapper fw : fieldList){
      csvOutput += fw.theFullName + ',';
      csvOutput += fw.theLabel + ',';
      csvOutput += '"'+fw.theDescription +'"'+ ',';
      csvOutput += '"'+fw.theInlineHelptext + '"';
      csvOutput += '\n';
    }
    return Page.MetaDataDocumentationManagerCSV;
  }


  public Blob importCsvBody {get;set;}
  public String importCsvName {get;set;}

  // import fields from csv
  public PageReference importFromCsv(){
    // Ensure that we are working with a file
    if(importCsvBody == null){
      Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,'File missing.'));
      return null;
    }
    
    // Ensure the file ends with a line feed and it is a text file
    String fileString;
      
    try {
      fileString = importCsvBody.toString();
    } catch(Exception ex){
      Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,'No CSV File found.'));
      return null;
    }
    
    List<String> cellValues = new List<String>();
    List<String> csvRows = fileString.split( '\n|\r' );
    Integer index = 0;
    String delimiter = ',';

    // assume that each row in the csv is a row in the fieldList (in the same order!)        
    for(String csvRow : csvRows){
      system.debug(csvRow);
      //skip first row
      if(index!=0) {
        csvRow = csvRow.replaceAll('\n','');
        system.debug(csvRow);
        //csvRow = csvRow.replaceAll('\r','');

        // remove double quotes!
        csvRow = csvRow.replaceAll('"','');
        system.debug(csvRow);
        cellValues = csvRow.split('\\,',-1);
        system.debug(cellValues);

        if(cellValues.size() == 1 && cellValues[0] == '') {
          // this is for if the last row is an empty line
          break;
        } else {
          system.debug(cellValues);
          system.debug(cellValues[0]);
          system.debug(cellValues[1]);
          system.debug(cellValues[2]);
          system.debug(cellValues[3]);          
          if (cellValues[0] != null) fieldList[index-1].theFullName = cellValues[0];
          if (cellValues[1] != null) fieldList[index-1].theLabel = cellValues[1];
          if (cellValues[2] != null) fieldList[index-1].theDescription = cellValues[2];
          if (cellValues[3] != null) fieldList[index-1].theInlineHelptext = cellValues[3];
        }
        system.debug(fieldList[index-1]);
      }
      index++;

    }
      
    system.debug(fieldList);
    return null;
    //return updateAll();
  }


  

  public class MetadataServiceExamplesException extends Exception { }

  public static MetadataService.MetadataPort createService()
  { 
    MetadataService.MetadataPort service = new MetadataService.MetadataPort();
    service.SessionHeader = new MetadataService.SessionHeader_element();
    service.SessionHeader.sessionId = UserInfo.getSessionId();
    return service;    
  }

  /**
   * Example helper method to interpret a SaveResult, throws an exception if errors are found
   **/
  private static void handleSaveResults(List<MetadataService.SaveResult> saveResults)
  {
    for(MetadataService.SaveResult saveResult : saveResults){
      // Nothing to see?
      if(saveResult==null || saveResult.success)
        return;
      // Construct error message and throw an exception
      List<String> messages = new List<String>();
      messages.add(
        (saveResult.errors.size()==1 ? 'Error ' : 'Errors ') + 
          'occured processing component ' + saveResult.fullName + '.');
      for(MetadataService.Error error : saveResult.errors)
        messages.add(
          error.message + ' (' + error.statusCode + ').' + 
          ( error.fields!=null && error.fields.size()>0 ? 
            ' Fields ' + String.join(error.fields, ',') + '.' : '' ) );
      if(messages.size()>0)
        Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,String.join(messages, ' ')));
    }
  }  


}