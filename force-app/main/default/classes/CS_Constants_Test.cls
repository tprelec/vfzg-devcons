@isTest
private class CS_Constants_Test {
	@isTest
	private static void test() {
		Test.startTest();
		System.assertEquals(true, CS_Constants.LOOKOUT_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.MANAGED_SERVICE_1_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.MANAGED_SERVICE_2_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.MANAGED_SERVICE_EXPRESS_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.MANAGED_SERVICE_OPTIE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.MICROSOFT_EMS_INTUNE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.MOBILE_IRON_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.PROFESSIONAL_SERVICES_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.SERVICE_MANAGEMENT_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.TREND_MICRO_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.VERVANGENDE_TOESTEL_SERVICE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.VSDM_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.WANDERA_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.CISCO_UMBRELLA_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.CYBER_EXPOSURE_DIAGNOSTIC_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.PHISHING_AWARENESS_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.VULNERABILITY_ASSESSMENT_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.PENETRATION_TESTING_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.MANAGED_DETECTION_RESPONSE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.BRECH_RESPONSE_FORENSICS_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.DEAL_TYPES.keySet().size() > 0);
		System.assertEquals(true, CS_Constants.ACCESS_INFRASTRUCTURE.length() > 0);
		System.assertEquals(true, CS_Constants.ONE_NET_SEAT_LICENCES.size() > 0);
		System.assertEquals(true, CS_Constants.TRIGGER_LICENCE_NAMES.size() > 0);
		System.assertEquals(true, CS_Constants.ONSITE_PROJECT_MANAGEMENT.length() > 0);
		System.assertEquals(true, CS_Constants.ONE_NET_PRODUCT_IN_BASKET.length() > 0);
		System.assertEquals(true, CS_Constants.ONE_CALL_CENTRE_LICENTIE.length() > 0);
		System.assertEquals(true, CS_Constants.IVR_KEUZEMENU.length() > 0);
		System.assertEquals(true, CS_Constants.ONE_INTEGRATE_PREMIUM.length() > 0);
		System.assertEquals(true, CS_Constants.ONE_NET_BASIS_PROJECTCOORDINATIE.length() > 0);
		System.assertEquals(true, CS_Constants.ONE_NET_STANDAARD_PM.length() > 0);
		System.assertEquals(true, CS_Constants.PRIVATE_COMPANY_NL.length() > 0);
		System.assertEquals(true, CS_Constants.PUBLIC_COMPANY_NL.length() > 0);
		System.assertEquals(true, CS_Constants.ITS_NL.length() > 0);
		System.assertEquals(true, CS_Constants.AND_NL.length() > 0);
		System.assertEquals(true, CS_Constants.PRIVATE_COMPANY_EN.length() > 0);
		System.assertEquals(true, CS_Constants.PUBLIC_COMPANY_EN.length() > 0);
		System.assertEquals(true, CS_Constants.ITS_EN.length() > 0);
		System.assertEquals(true, CS_Constants.AND_EN.length() > 0);
		System.assertEquals(true, CS_Constants.ONE_NET_ENTERPRISE.length() > 0);
		System.assertEquals(true, CS_Constants.ONE_NET_FLEX_PRODUCT_IN_BASKET.length() > 0);
		System.assertEquals(true, CS_Constants.LICENCE_ONE_MOBIEL.length() > 0);
		System.assertEquals(true, CS_Constants.VODAFONE_CALLING.length() > 0);
		System.assertEquals(true, CS_Constants.MOBILE_CTN_PROFILE_IN_BASKET.length() > 0);
		System.assertEquals(true, CS_Constants.MOBILE_BAN_PROFILE_IN_BASKET.length() > 0);
		System.assertEquals(true, CS_Constants.CUSTOMER_SERVICES_IN_BASKET.length() > 0);
		System.assertEquals(true, CS_Constants.GDSP_IN_BASKET.length() > 0);
		System.assertEquals(true, CS_Constants.BM_ABBONNEMENTEN.size() > 0);
		System.assertEquals(true, CS_Constants.MICROSOFT_BUSINESS_MARKETPLACE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.TREND_MICRO_MARKETPLACE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.IVANTI_MOBILEIRON_MARKETPLACE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.VODAFONE_SECURE_DEVICE_MANAGER_MARKETPLACE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.LOOKOUT_MARKETPLACE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.VODAFONE_CLOUD_BACKUP_MARKETPLACE_GROUP.length() > 0);
		System.assertEquals(true, CS_Constants.SALUTATION_EN_MR.length() > 0);
		System.assertEquals(true, CS_Constants.SALUTATION_EN_MRS.length() > 0);
		System.assertEquals(true, CS_Constants.SALUTATION_NL_MR.length() > 0);
		System.assertEquals(true, CS_Constants.SALUTATION_NL_MRS.length() > 0);

		Test.stopTest();
	}
}
