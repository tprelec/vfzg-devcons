global class BigMachinesUserSyncBatch implements Database.Batchable<sObject> {

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('SELECT Id,BigMachines__Provisioned__c,FirstName,LastName FROM User Where BigMachines__Provisioned__c = false AND AccountId != null AND IsActive = true limit 100');
	}

	global void execute(Database.BatchableContext BC, List<User> scope) {
		List<User> usersToUpdate = new List<User>();
		for(User u : (List<User>) scope){
			u.BigMachines__Provisioned__c = true;
			usersToUpdate.add(u);
		}
		update usersToUpdate;
	}

	global void finish(Database.BatchableContext BC) {
		//ExportUtils.startNextJob('BMUserSync');
	}
}