@isTest
private with sharing class TestCS_ContractNumberGenerator {
	@isTest
	private static void testGenerateAcquisitionContractNumbers() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Test.startTest();

			ContractNumberGeneration__c newSetting = new ContractNumberGeneration__c();
			newSetting.Contract_Number__c = 1;
			insert newSetting;

			Account tmpAcc = CS_DataTest.createAccount('Test Account');
			insert tmpAcc;

			Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', simpleUser.Id);
			insert tmpOpp;

			cscfga__Product_Definition__c tmpProductDef = CS_DataTest.createProductDefinition('Internet');
			tmpProductDef.Product_Type__c = 'Fixed';
			insert tmpProductDef;

			cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);

			cscfga__Product_Configuration__c fixedCfg = CS_DataTest.createProductConfiguration(
				tmpProductDef.Id,
				'Fixed Configuration',
				tmpProductBasket.Id
			);
			fixedCfg.Contract_Number_Group__c = 'BI';
			fixedCfg.cscfga__Contract_Term__c = 1;
			insert fixedCfg;

			cscfga__Product_Configuration__c mobileCfg = CS_DataTest.createProductConfiguration(
				tmpProductDef.Id,
				'Mobile Configuration',
				tmpProductBasket.Id
			);
			mobileCfg.Contract_Number_Group__c = 'OM';
			mobileCfg.ContractNumber_JSON__c = '{"year":2021,"minorVersion":1,"majorVersion":2,"incrementNumber":3,"format":"CS-#YYYY#-VZ-#GROUP#-#NUMBER# #MAJOR#.#MINOR# d.d. #DATE#","contractGroup":"OM","contractDate":"2021-12-09"}';
			mobileCfg.cscfga__Contract_Term__c = 1;
			insert mobileCfg;

			CS_ContractNumberGenerator.generateAcquisitionContractNumbers(new List<cscfga__Product_Configuration__c>{ fixedCfg, mobileCfg });

			Test.stopTest();

			cscfga__Product_Configuration__c mobileCfgUpdated = [
				SELECT ContractNumber_JSON__c
				FROM cscfga__Product_Configuration__c
				WHERE Id = :mobileCfg.Id
			];
			CS_ContractNumber numberContract = (CS_ContractNumber) JSON.deserialize(mobileCfgUpdated.ContractNumber_JSON__c, CS_ContractNumber.class);
			System.assertEquals(false, numberContract.isMacd());
		}
	}

	@isTest
	private static void testIncrementMacdContractNumber() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Test.startTest();

			ContractNumberGeneration__c newSetting = new ContractNumberGeneration__c();
			newSetting.Contract_Number__c = 1;
			insert newSetting;

			Account tmpAcc = CS_DataTest.createAccount('Test Account');
			insert tmpAcc;

			Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', simpleUser.Id);
			insert tmpOpp;

			cscfga__Product_Definition__c tmpProductDef = CS_DataTest.createProductDefinition('Internet');
			tmpProductDef.Product_Type__c = 'Fixed';
			insert tmpProductDef;

			cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);

			cscfga__Product_Configuration__c mobileCfg = CS_DataTest.createProductConfiguration(
				tmpProductDef.Id,
				'Mobile Configuration',
				tmpProductBasket.Id
			);
			mobileCfg.Contract_Number_Group__c = 'OM';
			mobileCfg.ContractNumber_JSON__c = '{"year":2021,"minorVersion":1,"majorVersion":2,"incrementNumber":3,"format":"CS-#YYYY#-VZ-#GROUP#-#NUMBER# #MAJOR#.#MINOR# d.d. #DATE#","contractGroup":"OM","contractDate":"2021-12-09"}';
			mobileCfg.cscfga__Contract_Term__c = 1;
			insert mobileCfg;

			CS_ContractNumberGenerator.incrementMacdContractNumber(new List<cscfga__Product_Configuration__c>{ mobileCfg });

			Test.stopTest();

			cscfga__Product_Configuration__c mobileCfgUpdated = [
				SELECT ContractNumber_JSON__c
				FROM cscfga__Product_Configuration__c
				WHERE Id = :mobileCfg.Id
			];
			CS_ContractNumber numberContract = (CS_ContractNumber) JSON.deserialize(mobileCfgUpdated.ContractNumber_JSON__c, CS_ContractNumber.class);
			System.assertEquals(true, numberContract.isMacd());
		}
	}

	@isTest
	private static void testGenerateContractNumbers() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Test.startTest();

			ContractNumberGeneration__c newSetting = new ContractNumberGeneration__c();
			newSetting.Contract_Number__c = 1;
			insert newSetting;

			Account tmpAcc = CS_DataTest.createAccount('Test Account');
			insert tmpAcc;

			Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', simpleUser.Id);
			insert tmpOpp;

			cscfga__Product_Definition__c tmpProductDef = CS_DataTest.createProductDefinition('Internet');
			tmpProductDef.Product_Type__c = 'Fixed';
			insert tmpProductDef;

			cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
			tmpProductBasket.csordtelcoa__Change_Type__c = 'Change Solution';
			insert tmpProductBasket;

			cscfga__Product_Configuration__c originalMobileCfg = CS_DataTest.createProductConfiguration(
				tmpProductDef.Id,
				'Mobile Configuration',
				tmpProductBasket.Id
			);
			originalMobileCfg.Contract_Number_Group__c = 'OM';
			originalMobileCfg.ContractNumber_JSON__c = '{"year":2021,"minorVersion":1,"majorVersion":2,"incrementNumber":3,"format":"CS-#YYYY#-VZ-#GROUP#-#NUMBER# #MAJOR#.#MINOR# d.d. #DATE#","contractGroup":"OM","contractDate":"2021-12-09"}';
			originalMobileCfg.cscfga__Contract_Term__c = 1;
			insert originalMobileCfg;

			cscfga__Product_Configuration__c mobileCfg = CS_DataTest.createProductConfiguration(
				tmpProductDef.Id,
				'Mobile Configuration',
				tmpProductBasket.Id
			);
			mobileCfg.Contract_Number_Group__c = 'OM';
			mobileCfg.ContractNumber_JSON__c = '{"year":2021,"minorVersion":1,"majorVersion":2,"incrementNumber":3,"format":"CS-#YYYY#-VZ-#GROUP#-#NUMBER# #MAJOR#.#MINOR# d.d. #DATE#","contractGroup":"OM","contractDate":"2021-12-09"}';
			mobileCfg.cscfga__Contract_Term__c = 1;
			mobileCfg.csordtelcoa__Replaced_Product_Configuration__c = originalMobileCfg.Id;
			insert mobileCfg;

			cscfga__Product_Configuration__c fixedCfg = CS_DataTest.createProductConfiguration(
				tmpProductDef.Id,
				'Fixed Configuration',
				tmpProductBasket.Id
			);
			fixedCfg.Contract_Number_Group__c = 'BI';
			fixedCfg.cscfga__Contract_Term__c = 1;
			insert fixedCfg;

			List<cscfga__Product_Configuration__c> inputConfigs = [
				SELECT
					Id,
					Name,
					cscfga__Product_Basket__r.csordtelcoa__Change_Type__c,
					csordtelcoa__Replaced_Product_Configuration__c,
					ContractNumber_JSON__c,
					Contract_Number__c,
					Contract_Number_Group__c,
					cscfga__Parent_Configuration__c,
					cscfga__Parent_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,
					cscfga__Parent_Configuration__r.ContractNumber_JSON__c,
					cscfga__Parent_Configuration__r.Contract_Number__c
				FROM cscfga__Product_Configuration__c
				WHERE cscfga__Product_Basket__c = :tmpProductBasket.Id
			];

			CS_ContractNumberGenerator.generateContractNumbers(inputConfigs);

			cscfga__Product_Configuration__c mobileCfgUpdated = [
				SELECT ContractNumber_JSON__c
				FROM cscfga__Product_Configuration__c
				WHERE Id = :mobileCfg.Id
			];
			CS_ContractNumber numberContract = (CS_ContractNumber) JSON.deserialize(mobileCfgUpdated.ContractNumber_JSON__c, CS_ContractNumber.class);
			System.assertEquals(true, numberContract.isMacd());

			Test.stopTest();
		}

	}
}
