global class COM_DXLCreateBSSOrderAsyncResponse {
	public ProvideInvoicingInforOrderLines payload;

	global class ProvideInvoicingInforOrderLines {
		public List<OrderLines> orderLines;
	}

	global class OrderLines {
		public String componentAPID;
		public String externalReferenceId;
		public String installedBaseId;
		public String billingorderId;
		public OrderLineStatus orderLineStatus;
		public List<Error> orderLineError;
	}
	global class Error {
		public String errorCode;
		public String errorMessage;
	}
	global class OrderLineStatus {
		public String statusCode;
		public String statusMessage;
	}
}
