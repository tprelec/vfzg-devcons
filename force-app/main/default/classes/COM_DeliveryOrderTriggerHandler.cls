public with sharing class COM_DeliveryOrderTriggerHandler extends TriggerHandler {
	public override void afterUpdate() {
		Set<Id> changedOrders = new Set<Id>();
		Map<Id, COM_Delivery_Order__c> newMap = (Map<Id, COM_Delivery_Order__c>) Trigger.newMap;
		Map<Id, COM_Delivery_Order__c> oldMap = (Map<Id, COM_Delivery_Order__c>) Trigger.oldMap;

		for (Id deliveryOrderId : newMap.keySet()) {
			if (
				(newMap.get(deliveryOrderId).Confirmed_Installation__c && !oldMap.get(deliveryOrderId).Confirmed_Installation__c) ||
				(newMap.get(deliveryOrderId).Appointment_Changed__c && !oldMap.get(deliveryOrderId).Appointment_Changed__c) ||
				((newMap.get(deliveryOrderId).Status__c.equalsIgnoreCase('Cancellation Requested') &&
				!oldMap.get(deliveryOrderId).Status__c.equalsIgnoreCase('Cancellation Requested'))) ||
				((newMap.get(deliveryOrderId).Status__c == 'Decomposed' &&
				oldMap.get(deliveryOrderId).Status__c != 'Decomposed')) ||
				(newMap.get(deliveryOrderId).CDO_Integration_Status__c != oldMap.get(deliveryOrderId).CDO_Integration_Status__c) ||
				(newMap.get(deliveryOrderId).CDO_Service_Order_Request_Id__c != oldMap.get(deliveryOrderId).CDO_Service_Order_Request_Id__c) ||
				(newMap.get(deliveryOrderId).Provisioned__c != oldMap.get(deliveryOrderId).Provisioned__c) ||
				(newMap.get(deliveryOrderId).Inflight_Change_Applied__c != oldMap.get(deliveryOrderId).Inflight_Change_Applied__c)
			) {
				changedOrders.add(deliveryOrderId);
			}
		}

		if (changedOrders.size() > 0) {
			CSPOFA.Events.emit('update', changedOrders);
		}
	}
}
