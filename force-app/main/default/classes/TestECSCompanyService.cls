@isTest
public class TestECSCompanyService {
	@TestSetup
	static void makeData() {
		TestUtils.autoCommit = true;
		User u = TestUtils.createManager();
		TestUtils.autoCommit = false;
		//create Reseller Account with Custom Name so the mock can get the ID.
		Account accCust = TestUtils.createAccount(u);
		//set name to query as referenceId in the mock callout
		accCust.Name = 'TestAccount Customer';
		insert accCust;
		//create Reseller Account with Custom Name so the mock can get the ID.
		Account accReseller = TestUtils.createAccount(u);
		accReseller.Name = 'TestAccount Reseller';
		insert accReseller;

		Ban__c banana = new Ban__c(Name = '399999999', Ban_Number__c = '399999999', BAN_Status__c = 'Open', Account__c = accCust.Id);
		insert banana;

		External_Account__c extAcct = new External_Account__c(Account__c = accCust.Id, External_Account_Id__c = 'ACD', External_Source__c = 'BOP');
		insert extAcct;
	}

	@isTest
	public static void testMakeCreateRequest() {
		String operation = 'createCustomer';
		Account customerAccount = [
			SELECT
				Id,
				Name,
				Phone,
				Fax,
				KVK_number__c,
				Website,
				BillingStreet,
				BillingPostalCode,
				BillingCity,
				BillingCountry,
				Owner.UserType,
				Dealer_code__c,
				BAN_Number__c,
				BOPCode__c,
				BOP_export_datetime__c,
				BOP_export_Errormessage__c,
				Owner.Name,
				Parent.BAN_Number__c,
				parent.BOPCode__c
			FROM Account
			WHERE Name = 'TestAccount Customer'
			LIMIT 1
		];

		ECSCompanyService service = new ECSCompanyService();
		List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
		ECSCompanyService.request req = new ECSCompanyService.request();

		req.banNumber = customerAccount.BAN_Number__c;
		req.corporateId = null;
		req.bopCode = null;
		req.referenceId = customerAccount.Id;
		req.name = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'; //replaced with a long Name
		req.accountManagerEbu = customerAccount.Owner.Name;
		req.phone = customerAccount.Phone;
		req.fax = customerAccount.Fax;
		req.countryId = null;
		req.kvkNumber = customerAccount.KVK_number__c;
		req.website = customerAccount.Website;
		req.billingStreet = customerAccount.BillingStreet;
		req.billingPostalCode = customerAccount.BillingPostalCode;
		req.billingCity = customerAccount.BillingCity;
		req.billingCountry = customerAccount.BillingCountry;
		req.dealerCode = customerAccount.Dealer_code__c;
		req.resellerBanNumber = '399999999';
		req.resellerCorporateId = null;
		req.resellerBopCode = 'XYZ';
		req.resellerReferenceId = '001Random';
		req.parentCompanyBanNumber = '399999993';
		req.parentCompanyCorporateId = null;
		req.parentCompanyBopCode = 'SON';
		req.parentCompanyReferenceId = '001IdRandom';
		req.internalInvoicing = null;
		requests.add(req);

		Test.setMock(WebServiceMock.class, new ECSSOAPCompanyMocks.CreateCustomersResponse());
		Test.startTest();
		service.setRequest(requests);
		service.makeRequest(operation);
		List<ECSCompanyService.response> responseList = service.getResponse();
		System.assertEquals('TES', responseList[0].bopCode, 'Bop Code returned: ' + responseList[0].bopCode);
		Test.stopTest();
	}

	@isTest
	public static void testMakeUpdateRequest() {
		String operation = 'updateCustomer';
		Account customerAccount = [
			SELECT
				Id,
				Name,
				Phone,
				Fax,
				KVK_number__c,
				Website,
				BillingStreet,
				BillingPostalCode,
				BillingCity,
				BillingCountry,
				Owner.UserType,
				Dealer_code__c,
				BAN_Number__c,
				BOPCode__c,
				BOP_export_datetime__c,
				BOP_export_Errormessage__c,
				Owner.Name,
				Parent.BAN_Number__c,
				parent.BOPCode__c
			FROM Account
			WHERE Name = 'TestAccount Customer'
			LIMIT 1
		];

		ECSCompanyService service = new ECSCompanyService();
		List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
		ECSCompanyService.request req = new ECSCompanyService.request();

		req.banNumber = customerAccount.BAN_Number__c;
		req.corporateId = null;
		req.bopCode = 'TES';
		req.referenceId = customerAccount.Id;
		req.name = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.'; //replaced with a long Name
		req.accountManagerEbu = customerAccount.Owner.Name;
		req.phone = customerAccount.Phone;
		req.fax = customerAccount.Fax;
		req.countryId = null;
		req.kvkNumber = customerAccount.KVK_number__c;
		req.website = customerAccount.Website;
		req.billingStreet = customerAccount.BillingStreet;
		req.billingPostalCode = customerAccount.BillingPostalCode;
		req.billingCity = customerAccount.BillingCity;
		req.billingCountry = customerAccount.BillingCountry;
		req.dealerCode = customerAccount.Dealer_code__c;
		req.resellerBanNumber = '399999999';
		req.resellerCorporateId = null;
		req.resellerBopCode = 'XYZ';
		req.resellerReferenceId = '001Random';
		req.parentCompanyBanNumber = '399999993';
		req.parentCompanyCorporateId = null;
		req.parentCompanyBopCode = 'SON';
		req.parentCompanyReferenceId = '001IdRandom';
		req.internalInvoicing = null;
		requests.add(req);

		Test.setMock(WebServiceMock.class, new ECSSOAPCompanyMocks.UpdateCustomersResponse());
		Test.startTest();
		service.setRequest(requests);
		service.makeRequest(operation);
		List<ECSCompanyService.response> responseList = service.getResponse();
		System.assertEquals('TES', responseList[0].bopCode, 'Bop Code returned: ' + responseList[0].bopCode);
		Test.stopTest();
	}

	@isTest
	public static void testMakeResellerCreateRequest() {
		String operation = 'createReseller';
		Account customerAccount = [
			SELECT
				Id,
				Name,
				Phone,
				Fax,
				KVK_number__c,
				Website,
				BillingStreet,
				BillingPostalCode,
				BillingCity,
				BillingCountry,
				Owner.UserType,
				Dealer_code__c,
				BAN_Number__c,
				BOPCode__c,
				BOP_export_datetime__c,
				BOP_export_Errormessage__c,
				Owner.Name,
				Parent.BAN_Number__c,
				parent.BOPCode__c
			FROM Account
			WHERE Name = 'TestAccount Reseller'
			LIMIT 1
		];

		ECSCompanyService service = new ECSCompanyService();
		List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
		ECSCompanyService.request req = new ECSCompanyService.request();

		req.banNumber = customerAccount.BAN_Number__c;
		req.corporateId = null;
		req.bopCode = 'TES';
		req.referenceId = customerAccount.Id;
		req.name = customerAccount.Name;
		req.accountManagerEbu = customerAccount.Owner.Name;
		req.phone = customerAccount.Phone;
		req.fax = customerAccount.Fax;
		req.countryId = null;
		req.kvkNumber = customerAccount.KVK_number__c;
		req.website = customerAccount.Website;
		req.dealerCode = customerAccount.Dealer_code__c;
		requests.add(req);

		Test.setMock(WebServiceMock.class, new ECSSOAPCompanyMocks.CreateResellersResponse());
		Test.startTest();
		service.setRequest(requests);
		service.makeRequest(operation);
		List<ECSCompanyService.response> responseList = service.getResponse();
		System.assertEquals('WXY', responseList[0].bopCode, 'Bop Code returned: ' + responseList[0].bopCode);
		Test.stopTest();
	}

	@isTest
	public static void testMakeResellerUpdateRequest() {
		String operation = 'updateReseller';
		Account customerAccount = [
			SELECT
				Id,
				Name,
				Phone,
				Fax,
				KVK_number__c,
				Website,
				BillingStreet,
				BillingPostalCode,
				BillingCity,
				BillingCountry,
				Owner.UserType,
				Dealer_code__c,
				BAN_Number__c,
				BOPCode__c,
				BOP_export_datetime__c,
				BOP_export_Errormessage__c,
				Owner.Name,
				Parent.BAN_Number__c,
				parent.BOPCode__c
			FROM Account
			WHERE Name = 'TestAccount Reseller'
			LIMIT 1
		];

		ECSCompanyService service = new ECSCompanyService();
		List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
		ECSCompanyService.request req = new ECSCompanyService.request();

		req.banNumber = customerAccount.BAN_Number__c;
		req.corporateId = null;
		req.bopCode = 'TES';
		req.referenceId = customerAccount.Id;
		req.name = customerAccount.Name;
		req.accountManagerEbu = customerAccount.Owner.Name;
		req.phone = customerAccount.Phone;
		req.fax = customerAccount.Fax;
		req.countryId = null;
		req.kvkNumber = customerAccount.KVK_number__c;
		req.website = customerAccount.Website;
		req.dealerCode = customerAccount.Dealer_code__c;
		requests.add(req);

		Test.setMock(WebServiceMock.class, new ECSSOAPCompanyMocks.UpdateResellersResponse());
		Test.startTest();
		service.setRequest(requests);
		service.makeRequest(operation);
		List<ECSCompanyService.response> responseList = service.getResponse();
		System.assertEquals('XYZ', responseList[0].bopCode, 'Bop Code returned: ' + responseList[0].bopCode);
		Test.stopTest();
	}

	@isTest
	public static void testLooseMethods() {
		Test.startTest();
		ECSCompanyService service = new ECSCompanyService();
		List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
		ECSCompanyService.request req = new ECSCompanyService.request();
		requests.add(req);
		service.setRequest(requests);
		service.genericRequest();
		try {
			//remove items from list
			requests.clear();
			service.setRequest(requests);
			service.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		try {
			//remove items from list
			requests = null;
			service.setRequest(requests);
			service.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}

		Test.stopTest();
	}

	@isTest
	public static void testCalloutErrors() {
		Account customerAccount = [
			SELECT
				Id,
				Name,
				Phone,
				Fax,
				KVK_number__c,
				Website,
				BillingStreet,
				BillingPostalCode,
				BillingCity,
				BillingCountry,
				Owner.UserType,
				Dealer_code__c,
				BAN_Number__c,
				BOPCode__c,
				BOP_export_datetime__c,
				BOP_export_Errormessage__c,
				Owner.Name,
				Parent.BAN_Number__c,
				parent.BOPCode__c
			FROM Account
			WHERE Name = 'TestAccount Customer'
			LIMIT 1
		];

		ECSCompanyService service = new ECSCompanyService();
		List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
		ECSCompanyService.request req = new ECSCompanyService.request();

		req.banNumber = customerAccount.BAN_Number__c;
		req.corporateId = null;
		req.bopCode = null;
		req.referenceId = customerAccount.Id;
		req.name = customerAccount.Name;
		req.accountManagerEbu = customerAccount.Owner.Name;
		req.phone = customerAccount.Phone;
		req.fax = customerAccount.Fax;
		req.countryId = null;
		req.kvkNumber = customerAccount.KVK_number__c;
		req.website = customerAccount.Website;
		req.billingStreet = customerAccount.BillingStreet;
		req.billingPostalCode = customerAccount.BillingPostalCode;
		req.billingCity = customerAccount.BillingCity;
		req.billingCountry = customerAccount.BillingCountry;
		req.dealerCode = customerAccount.Dealer_code__c;
		req.resellerBanNumber = '399999999';
		req.resellerCorporateId = null;
		req.resellerBopCode = 'XYZ';
		req.resellerReferenceId = '001Random';
		req.parentCompanyBanNumber = '399999993';
		req.parentCompanyCorporateId = null;
		req.parentCompanyBopCode = 'SON';
		req.parentCompanyReferenceId = '001IdRandom';
		req.internalInvoicing = null;
		requests.add(req);

		Test.startTest();
		service.setRequest(requests);
		try {
			service.makeRequest('createCustomer');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to create the customer(s): '),
				'when there is no mock Create'
			);
		}
		try {
			service.makeRequest('updateCustomer');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to update the customer(s): '),
				'when there is no mock Update'
			);
		}
		try {
			service.makeRequest('createReseller');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to create the customer(s): '),
				'when there is no mock Create Reseller'
			);
		}
		try {
			service.makeRequest('updateReseller');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to update the customer(s): '),
				'when there is no mock Update Reseller'
			);
		}
		Test.stopTest();
	}
}
