@isTest
public with sharing class TestOpportunityClosedWonComponentCtr {
    @TestSetup
    private static void testSetup() {
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;
        Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opportunity', Userinfo.getUserId());
        insert testOpp;
        List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>{CS_DataTest.createProductBasket(testOpp, 'Test Basket')};
        basketList[0].cscfga__Basket_Status__c = 'Contract Created';
        insert basketList;
        testOpp.Primary_Basket__c = basketList[0].Id;   
        update testOpp;
        csord__Order__c testOrder = CS_DataTest.createOrder();
        insert testOrder;
        TestUtils.createOpportunityFieldMappings();
    }

    @isTest
    private static void opportunityIsNotNewPortfolio() {
        Opportunity opp = [
			SELECT 
				Id, 
				Primary_Basket__r.New_Portfolio__c
			FROM Opportunity
		];
        Test.startTest();
        Boolean result = OpportunityClosedWonComponentController.getProductBasketPortfolioStatus(opp.Id);
        Test.stopTest();
        System.assert(result == false);
    }
    
    @isTest
    private static void opportunityIsNewPortfolio() {
        Opportunity opp = [
			SELECT 
				Id, 
				Primary_Basket__r.New_Portfolio__c
			FROM Opportunity
		];
        cscfga__Product_Basket__c basket = [
			SELECT 
				Id, 
				New_Portfolio__c
			FROM cscfga__Product_Basket__c
		];
        basket.New_Portfolio__c = true;
        update basket;
        Test.startTest();
        Boolean result = OpportunityClosedWonComponentController.getProductBasketPortfolioStatus(opp.Id);
        Test.stopTest();
        System.assert(result == true);
    } 

    @isTest
    private static void opportunityNotFound() {
        Test.startTest();
        Boolean result = OpportunityClosedWonComponentController.getProductBasketPortfolioStatus(null);
        Test.stopTest();
        System.assert(result == null);
    } 

    @isTest
    private static void orderRelatedToOpportunityFound() {
        Opportunity opp = [
			SELECT 
				Id
			FROM Opportunity
		];
        csord__Order__c order = [
			SELECT 
				Id
			FROM csord__Order__c
            LIMIT 1
		];
        order.csordtelcoa__Opportunity__c = opp.Id;
        update order;
        Test.startTest();
        String result = OpportunityClosedWonComponentController.getOrder(opp.Id);
        Test.stopTest();
        System.assert(result != null);
        System.assert(Id.valueOf(result) == order.Id);
    }

    @isTest
    private static void orderRelatedToOpportunityNotFound() {
        Opportunity opp = [
			SELECT 
				Id
			FROM Opportunity
		];
        Test.startTest();
        String result = OpportunityClosedWonComponentController.getOrder(opp.Id);
        Test.stopTest();
        System.assert(result == null);
    }

    @isTest
    private static void opportunityCloseDateUpdated() {
        Opportunity opp = [
			SELECT 
				Id
			FROM Opportunity
		];
        Test.startTest();
        Opportunity result = OpportunityClosedWonComponentController.updateOppCloseDate(opp.Id, System.today());
        Test.stopTest();
        System.assert(result != null);
        System.assertEquals(System.today(), result.CloseDate);
    }  

    @isTest
    private static void opportunityClosedWon(){
        Opportunity opp = [
			SELECT 
				Id, StageName, Primary_Basket__c
			FROM Opportunity
		];
        opp.StageName = 'Closed Won';
        update opp;
        Test.startTest();
        Boolean result = OpportunityClosedWonComponentController.isOpportunityClosedWon(opp.Id);
        Test.stopTest();
        System.assert(result == true);
    }

    @isTest
    private static void opportunityNotClosedWon(){
        Opportunity opp = [
			SELECT 
				Id, StageName
			FROM Opportunity
		];
        Test.startTest();
        Boolean result = OpportunityClosedWonComponentController.isOpportunityClosedWon(opp.Id);
        Test.stopTest();
        System.assert(result == false);
    }
}