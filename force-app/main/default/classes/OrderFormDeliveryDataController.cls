/**
 * @description			This is the controller class for the Order Form Delivery Data.
 * @author				Guy Clairbois
 */
public with sharing class OrderFormDeliveryDataController {

	/**
	 *		Variables
	 */

    public VF_Contract__c contract {get;set;}
    public String contractId{get; private set;}
    public String orderId{get; private set;}

	public Boolean errorFound{get;set;}
    public OrderWrapper theOrderWrapper {get;set;}
	public Boolean allDeliveriesReady {get;set;}
	public Boolean locked {get;set;}

	/**
	 *		Controllers
	 */

 	public OrderFormDeliveryDataController getApexController() {
 		// used for passing the controller between form components
        return this;
    }
    
    public OrderFormDeliveryDataController() {
    		
    	contractId = ApexPages.currentPage().getParameters().get('contractId');
    	orderId = ApexPages.currentPage().getParameters().get('orderId');
	
    	errorFound = false;
	    	
    	if(contractId == null ){
    		ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'No ContractId provided.'));
    		errorFound = true;
    	}
	    	
    	if(contractId != null){
	    	// only continue if no errors were found earlier on
	    	if(!errorFound){
    			// fetch the contract details if the order is based on a contract	    		
	    		errorFound = retrieveContract();

                theOrderWrapper = OrderUtils.retrieveOrderDetails(orderId,null);

	    		initializeSiteMap(theOrderWrapper);
	    		system.debug(theOrderWrapper);
	    		
				// make sure the first site is loaded upon page load
				if(this.siteSelectedString == null){
					this.siteSelectedString = theOrderWrapper.SiteMap.values()[0].site.Id;
				}
				if(this.siteSelected == null){
					this.siteSelected = theOrderWrapper.SiteMap.values()[0];
				}
			}
    	}
    	locked = OrderUtils.getLocked(theOrderWrapper.theOrder);
    }


	public void initializeSiteMap(OrderWrapper ow){
		ow.SiteMap = OrderUtils.retrieveOrderIdToLocationIdToLocation(new set<id>{}, new Set<Id>{theOrderWrapper.theOrder.Id}).get(theOrderWrapper.theOrder.Id);
		
		// check if any site is ready for order
		for(DeliveryWrapper dw : ow.SiteMap.values()){
			OrderValidation.checkCompleteDeliveryWrapper(dw,ow.theOrder);
		}
		
		ow.locations = ow.SiteMap.values();
		system.debug(ow.locations);				
	}

	/**
	 *		Page References
	 */
	     
	public pageReference saveSite(){
		try{	
			// if record is locked, prevent updating
			if(theOrderWrapper.theOrder.Record_Locked__c){
				Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_Order_Locked));
				return null;
			}

			siteSelected.updateDelivery();

			// reload sites and check completeness
			updateDeliveryStatus();

			siteSelected = theOrderWrapper.siteMap.get(this.siteSelectedString);
			
		} catch (dmlException e){
			Apexpages.addMessages(e);
		}
		return null;
		
	}   

	public void updateDeliveryStatus(){
		// refill sitemap to ensure formula gets updated
		initializeSiteMap(theOrderWrapper);

		// check if all deliveries are ready to be submitted (if so, update cache)
		allDeliveriesReady = true;
		for(DeliveryWrapper dw : theOrderWrapper.locations){
			system.debug(dw);
			if(dw.readyForOrder == false){
				allDeliveriesReady = false;
				break;
			}

		}
		system.debug(theOrderWrapper.theOrder.DeliveryReady__c);
		system.debug(allDeliveriesReady);
		if(theOrderWrapper.theOrder.DeliveryReady__c != allDeliveriesReady){
			theOrderWrapper.theOrder.DeliveryReady__c = allDeliveriesReady;			
			SharingUtils.updateRecordsWithoutSharing(theOrderWrapper.theOrder);
		}

		// recheck if this order is now clean 
		OrderUtils.updateOrderStatus(theOrderWrapper.theOrder,true);

	} 

	/* site selection controllers */
    
    public DeliveryWrapper siteSelected {get;set;}
	
	public String siteSelectedString {
		get;
		set{
			this.siteSelectedString = value;
			if(theOrderWrapper != null)
				if(theOrderWrapper.siteMap != null)
					if(this.siteSelectedString != null)
						this.siteSelected = theOrderWrapper.siteMap.get(this.siteSelectedString);
			system.debug(this.siteSelectedString);
			system.debug(this.siteSelected);
		}
	}
	
	public String siteContactSelected {
		get{
			if(theOrderWrapper != null){
				return siteSelected.Site.Site_Contact__c;
			} else {
				return null;
			}					
		}
		set{
			if(value != ''){
				siteContactSelected = value;
				siteSelected.Site.Site_Contact__c = siteContactSelected;
			}
		}
	}
	
	public List<SelectOption> getOrderContacts() {
		return OrderUtils.getOrderContacts(contract.Account__c,contract.Owner__r.Contact.AccountId,true);
    }
    
    /**
     * @description			Retrieve the contract that the orders should be created/updated for
     */
	private Boolean retrieveContract(){
		Boolean error = false;
    	contract = OrderUtils.getContractRecordById(contractId);

    	return error;
	}

    public static List<SelectOption> slaSelectOptions{
        get{
            if(slaSelectOptions == null){
                // retrieve all contractedproduct sla's
                slaSelectOptions = new List<SelectOption>();
                slaSelectOptions.add(new SelectOption('','--None selected--'));
                for(SLA__c sla : [Select Id, Name__c, BOP_Code__c, Reactive_Proactive__c, Reaction_time__c, SLA_type__c, Repair_time__c, Replace_by__c From SLA__c Where Object__c = 'ContractedProducts' AND Active__c = true ORDER BY Name__c ASC]){
                    slaSelectOptions.add(new SelectOption(sla.Id,sla.Name__c+' '+sla.Reactive_Proactive__c+' '+sla.Reaction_time__c+'mins'+' '+sla.SLA_type__c+' '+sla.Repair_time__c+' '+sla.Replace_by__c));
                }
            }
            return slaSelectOptions;
        }
        private set;        
                    
    }       	
 
}