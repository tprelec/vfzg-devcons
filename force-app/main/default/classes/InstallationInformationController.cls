@SuppressWarnings('PMD.ExcessivePublicCount')
public with sharing class InstallationInformationController {
	private static final String MOVE_TERMINATION_REASON = 'Move';
	private static final String MOVE_TERMINATION_SUB_REASON = 'Move within VFZ footprint';

	@AuraEnabled
	public static LightningResponse getSites(Id recordId) {
		List<InstallationSite> installationSites = new List<InstallationSite>();
		Set<Id> movedProductConfigurations = new Set<Id>();
		Set<Id> movedServices = new Set<Id>();
		LightningResponse response;
		String objectType;
		InstallationSite installation;
		if (recordId == null) {
			return new LightningResponse().setError(System.Label.InstallationInformation_UnexpectedComponentBehavior);
		}
		objectType = getObjectType(recordId);
		if (objectType == 'cscfga__Product_Basket__c') {
			for (cscfga__Product_Configuration__c productConfiguration : getProductConfigurations(recordId, null)) {
				installationSites.add(new installationSite(productConfiguration));

				if (
					productConfiguration.csordtelcoa__Replaced_Product_Configuration__c != null &&
					productConfiguration.Site__c != productConfiguration.csordtelcoa__Replaced_Product_Configuration__r.Site__c
				) {
					movedProductConfigurations.add(productConfiguration.csordtelcoa__Replaced_Product_Configuration__c);
				}
			}
			for (cscfga__Product_Configuration__c productConfiguration : getProductConfigurations(null, movedProductConfigurations)) {
				installation = new installationSite(productConfiguration);
				installation.moveSite = true;
				installation.terminationReason = MOVE_TERMINATION_REASON;
				installation.terminationSubReason = MOVE_TERMINATION_SUB_REASON;
				installationSites.add(installation);
			}
		} else if (objectType == 'csord__Order__c') {
			for (csord__Service__c service : getServices(recordId, null)) {
				installationSites.add(new installationSite(service));

				if (service.csordtelcoa__Replaced_Service__c != null && service.Site__c != service.csordtelcoa__Replaced_Service__r.Site__c) {
					movedServices.add(service.csordtelcoa__Replaced_Service__c);
				}
			}
			for (csord__Service__c service : getServices(null, movedServices)) {
				installation = new installationSite(service);
				installation.moveSite = true;
				installation.terminationReason = MOVE_TERMINATION_REASON;
				installation.terminationSubReason = MOVE_TERMINATION_SUB_REASON;
				installationSites.add(installation);
			}
		} else {
			return new LightningResponse().setError(System.Label.InstallationInformation_UnexpectedComponentBehavior);
		}
		if (!installationSites.isEmpty()) {
			response = new LightningResponse().setBody(installationSites);
		} else {
			response = new LightningResponse().setBody(null);
		}
		return response;
	}

	@AuraEnabled
	public static LightningResponse save(Id recordId, String installationSitesInput) {
		Savepoint savepoint = Database.setSavePoint();
		String objectType = getObjectType(recordId);
		Opportunity opportunityToUpdate;
		Set<Id> sitesToUpdateId = new Set<Id>();
		Set<Id> basketsToUpdateId = new Set<Id>();
		List<Site__c> sitesToUpdate = new List<Site__c>();
		List<cscfga__Product_Configuration__c> productConfigurationsToUpdate = new List<cscfga__Product_Configuration__c>();
		List<csord__Service__c> serviceToUpdate = new List<csord__Service__c>();
		List<cscfga__Product_Basket__c> productBasketToUpdate = new List<cscfga__Product_Basket__c>();
		Map<Id, List<cscfga__Product_Configuration__c>> mapChildProductsConfigurations = new Map<Id, List<cscfga__Product_Configuration__c>>();
		Map<Id, Id> mapServiceByConfigurations = new Map<Id, Id>();
		Map<Id, csord__Service__c> mapChildServicesByParent;
		try {
			Map<String, installationSite> installationSitesMap = (Map<String, installationSite>) JSON.deserialize(
				installationSitesInput,
				Map<String, installationSite>.class
			);
			if (objectType == 'cscfga__Product_Basket__c') {
				for (cscfga__Product_Configuration__c productConfiguration : getProductConfigurations(recordId, null)) {
					if (!productConfiguration.cscfga__Related_Configurations__r.isEmpty()) {
						mapChildProductsConfigurations.put(productConfiguration.Id, productConfiguration.cscfga__Related_Configurations__r);
					}
				}
			}
			for (installationSite installationSiteObject : installationSitesMap.values()) {
				if (installationSiteObject.moveSite != null && installationSiteObject.moveSite) {
					mapServiceByConfigurations.put(installationSiteObject.id, installationSiteObject.serviceId);
				}
			}
			mapChildServicesByParent = new Map<Id, csord__Service__c>(getServices(null, new Set<Id>(mapServiceByConfigurations.values())));
			for (installationSite installationSiteObject : installationSitesMap.values()) {
				if (!sitesToUpdateId.contains(installationSiteObject.siteId)) {
					sitesToUpdateId.add(installationSiteObject.siteId);
					sitesToUpdate.add(
						new Site__c(Id = installationSiteObject.siteId, Technical_Contact__c = installationSiteObject.technicalContact)
					);
				}
				if (objectType == 'cscfga__Product_Basket__c') {
					if (
						opportunityToUpdate == null &&
						installationSiteObject.opportunityId != null &&
						installationSiteObject.terminationFeeTotal != null &&
						installationSiteObject.terminationDate != null
					) {
						opportunityToUpdate = new Opportunity(
							Id = installationSiteObject.opportunityId,
							Indicative_Termination_Fee__c = installationSiteObject.terminationFeeTotal,
							Termination_Wish_Date__c = installationSiteObject.terminationDate
						);
					}
					if (!basketsToUpdateId.contains(installationSiteObject.productBasketId)) {
						basketsToUpdateId.add(installationSiteObject.productBasketId);
						productBasketToUpdate.add(
							new cscfga__Product_Basket__c(
								Id = installationSiteObject.productBasketId,
								Discount_Approver__c = installationSiteObject.discountApprover
							)
						);
					}
					productConfigurationsToUpdate.add(
						new cscfga__Product_Configuration__c(
							Id = installationSiteObject.id,
							Installation_Wish_Date__c = installationSiteObject.wishDate,
							Termination_Wish_Date__c = installationSiteObject.terminationDate,
							Termination_Reason__c = installationSiteObject.terminationReason,
							Termination_Sub_Reason__c = installationSiteObject.terminationSubReason,
							Termination_Fee_Discount__c = installationSiteObject.terminationDiscount,
							Termination_Fee_Discount_Type__c = installationSiteObject.terminationDiscountType,
							Termination_Fee_Calculation__c = installationSiteObject.terminationFee
						)
					);
					if (installationSiteObject.moveSite != null && installationSiteObject.moveSite) {
						serviceToUpdate.add(
							new csord__Service__c(
								Id = installationSiteObject.serviceId,
								Termination_Wish_Date__c = installationSiteObject.terminationDate
							)
						);
						if (mapChildServicesByParent != null && mapChildServicesByParent.containsKey(Id.valueOf(installationSiteObject.serviceId))) {
							for (
								csord__Service__c childService : mapChildServicesByParent.get(Id.valueOf(installationSiteObject.serviceId))
									.csord__services__r
							) {
								serviceToUpdate.add(
									new csord__Service__c(Id = childService.Id, Termination_Wish_Date__c = installationSiteObject.terminationDate)
								);
							}
						}
					}
					if (mapChildProductsConfigurations.containsKey(installationSiteObject.id)) {
						for (cscfga__Product_Configuration__c childConfiguration : mapChildProductsConfigurations.get(installationSiteObject.id)) {
							childConfiguration.Termination_Wish_Date__c = installationSiteObject.terminationDate;
							productConfigurationsToUpdate.add(childConfiguration);
						}
					}
				} else if (objectType == 'csord__Order__c') {
					serviceToUpdate.add(
						new csord__Service__c(Id = installationSiteObject.id, Installation_Wishdate__c = installationSiteObject.wishDate)
					);
				}
			}
			update productBasketToUpdate;
			update sitesToUpdate;
			update productConfigurationsToUpdate;
			update serviceToUpdate;
			if (opportunityToUpdate != null) {
				update opportunityToUpdate;
			}
			return new LightningResponse().setSuccess(System.Label.InstallationInformation_InformationSuccessfullyUpdated);
		} catch (DMLException objEx) {
			return setError(savepoint, objEx.getDmlMessage(0));
		} catch (Exception objEx) {
			return setError(savepoint, objEx.getMessage());
		}
	}

	private static LightningResponse setError(Savepoint savepoint, String message) {
		if (savepoint != null) {
			Database.rollback(savepoint);
		}
		return new LightningResponse().setError(message);
	}

	private static string getObjectType(Id recordId) {
		return recordId.getSObjectType().getDescribe().getName();
	}

	private static List<cscfga__Product_Configuration__c> getProductConfigurations(Id basketId, Set<Id> productConfigurationsId) {
		return [
			SELECT
				Id,
				Site__c,
				Site__r.Id,
				Site__r.Name,
				Site__r.Technical_Contact__c,
				Site__r.Unify_Site_Name__c,
				cscfga__Product_Basket__c,
				cscfga__Product_Basket__r.Discount_Approver__c,
				cscfga__Product_Basket__r.cscfga__Opportunity__c,
				cscfga__Description__c,
				csordtelcoa__Replaced_Product_Configuration__c,
				csordtelcoa__Replaced_Product_Configuration__r.Site__c,
				csordtelcoa__Replaced_Service__c,
				csordtelcoa__Replaced_Service__r.Contract_End_Date__c,
				csordtelcoa__Replaced_Service__r.csord__Activation_Date__c,
				Installation_Wish_Date__c,
				Termination_Wish_Date__c,
				Termination_Reason__c,
				Termination_Sub_Reason__c,
				Termination_Fee_Discount_Type__c,
				Termination_Fee_Discount__c,
				Termination_Fee_Calculation__c,
				cscfga__Contract_Term__c,
				cscfga__total_recurring_charge__c,
				csordtelcoa__cancelled_by_change_process__c,
				cscfga__Parent_Configuration__c,
				(SELECT Id FROM cscfga__Related_Configurations__r),
				(SELECT Id, csord__Activation_Date__c FROM csordtelcoa__Services__r WHERE csord__Service__c = NULL)
			FROM cscfga__Product_Configuration__c
			WHERE
				Site__c != NULL
				AND cscfga__Parent_Configuration__c = NULL
				AND cscfga__Product_Basket__c != NULL
				AND (cscfga__Product_Basket__c = :basketId
				OR Id IN :productConfigurationsId)
			ORDER BY Site__r.Unify_Site_Name__c
		];
	}

	private static List<csord__Service__c> getServices(Id orderId, Set<Id> servicesId) {
		return [
			SELECT
				Id,
				Site__c,
				Site__r.Id,
				Site__r.Name,
				Site__r.Technical_Contact__c,
				Site__r.Unify_Site_Name__c,
				Installation_Wishdate__c,
				csord__Identification__c,
				VFZ_Commercial_Article_Name__c,
				csord__Service__c,
				Termination_Wish_Date__c,
				Contract_End_Date__c,
				csordtelcoa__Cancelled_By_Change_Process__c,
				csordtelcoa__Product_Configuration__r.Termination_Wish_Date__c,
				csordtelcoa__Product_Configuration__r.Termination_Reason__c,
				csordtelcoa__Product_Configuration__r.Termination_Sub_Reason__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Discount_Type__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Discount__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Calculation__c,
				csordtelcoa__Product_Configuration__r.cscfga__Contract_Term__c,
				csordtelcoa__Product_Configuration__r.cscfga__total_recurring_charge__c,
				csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__c,
				csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__r.Discount_Approver__c,
				csordtelcoa__Replaced_Service__c,
				csordtelcoa__Replaced_Service__r.Site__c,
				(SELECT Id FROM csord__services__r)
			FROM csord__Service__c
			WHERE Site__c != NULL AND csord__Service__c = NULL AND csord__Order__c != NULL AND (csord__Order__c = :orderId OR Id IN :servicesId)
			ORDER BY Site__r.Unify_Site_Name__c
		];
	}

	@SuppressWarnings('PMD.TooManyFields, PMD.ExcessivePublicCount')
	public class InstallationSite {
		@AuraEnabled
		public String id;
		@AuraEnabled
		public String name;
		@AuraEnabled
		public String siteId;
		@AuraEnabled
		public String parentConfiguration;
		@AuraEnabled
		public String description;
		@AuraEnabled
		public String technicalContact;
		@AuraEnabled
		public Date wishDate;
		@AuraEnabled
		public Boolean cancelledByChange;
		@AuraEnabled
		public Date terminationDate;
		@AuraEnabled
		public String terminationReason;
		@AuraEnabled
		public String terminationSubReason;
		@AuraEnabled
		public Decimal terminationDiscount;
		@AuraEnabled
		public String terminationDiscountType;
		@AuraEnabled
		public Decimal terminationFee;
		@AuraEnabled
		public Decimal terminationFeeTotal;
		@AuraEnabled
		public Decimal contractTermPeriod;
		@AuraEnabled
		public Decimal totalRecurringCharge;
		@AuraEnabled
		public Date contractEndDate;
		@AuraEnabled
		public String discountApprover;
		@AuraEnabled
		public String productBasketId;
		@AuraEnabled
		public String opportunityId;
		@AuraEnabled
		public Boolean moveSite;
		@AuraEnabled
		public String serviceId;
		@AuraEnabled
		public Date contractStartDate;

		public installationSite(cscfga__Product_Configuration__c productConfiguration) {
			Site__c site = productConfiguration.Site__r;
			cscfga__Product_Basket__c basket = productConfiguration.cscfga__Product_Basket__r;
			if (site != null) {
				this.name = site.Unify_Site_Name__c;
				this.siteId = site.Id;
				this.technicalContact = site.Technical_Contact__c;
			}
			if (basket != null) {
				this.discountApprover = basket.Discount_Approver__c;
				this.productBasketId = basket.Id;
				this.opportunityId = basket.cscfga__Opportunity__c;
			}
			this.id = productConfiguration.Id;
			this.description = productConfiguration.cscfga__Description__c;
			this.parentConfiguration = productConfiguration.cscfga__Parent_Configuration__c;
			this.wishDate = productConfiguration.Installation_Wish_Date__c;
			this.cancelledByChange = productConfiguration.csordtelcoa__cancelled_by_change_process__c;
			this.terminationDate = productConfiguration.Termination_Wish_Date__c;
			this.terminationReason = productConfiguration.Termination_Reason__c;
			this.terminationSubReason = productConfiguration.Termination_Sub_Reason__c;
			this.terminationDiscount = productConfiguration.Termination_Fee_Discount__c;
			this.terminationDiscountType = productConfiguration.Termination_Fee_Discount_Type__c;
			this.terminationFee = productConfiguration.Termination_Fee_Calculation__c;
			this.contractTermPeriod = productConfiguration.cscfga__Contract_Term__c;
			this.totalRecurringCharge = productConfiguration.cscfga__total_recurring_charge__c;
			this.contractEndDate = productConfiguration.csordtelcoa__Replaced_Service__c != null
				? productConfiguration.csordtelcoa__Replaced_Service__r.Contract_End_Date__c
				: null;
			this.serviceId = productConfiguration.csordtelcoa__Services__r != null && !productConfiguration.csordtelcoa__Services__r.isEmpty()
				? productConfiguration.csordtelcoa__Services__r[0].Id
				: null;
			if (productConfiguration.csordtelcoa__Replaced_Service__c != null) {
				this.contractStartDate = productConfiguration.csordtelcoa__Replaced_Service__r.csord__Activation_Date__c;
			} else if (productConfiguration.csordtelcoa__Services__r != null && !productConfiguration.csordtelcoa__Services__r.isEmpty()) {
				this.contractStartDate = productConfiguration.csordtelcoa__Services__r[0].csord__Activation_Date__c;
			}
		}

		public installationSite(csord__Service__c service) {
			Site__c site = service.Site__r;
			cscfga__Product_Basket__c basket = service.csordtelcoa__Product_Configuration__c != null &&
				service.csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__c != null
				? service.csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__r
				: null;
			if (site != null) {
				this.name = site.Unify_Site_Name__c;
				this.siteId = site.Id;
				this.technicalContact = site.Technical_Contact__c;
			}
			if (basket != null) {
				this.discountApprover = basket.Discount_Approver__c;
			}
			this.id = service.Id;
			this.description = service.VFZ_Commercial_Article_Name__c;
			this.parentConfiguration = service.csord__Service__c;
			this.wishDate = service.Installation_Wishdate__c;
			this.cancelledByChange = service.csordtelcoa__Cancelled_By_Change_Process__c;
			this.terminationDate = service.csordtelcoa__Product_Configuration__r.Termination_Wish_Date__c;
			this.terminationReason = service.csordtelcoa__Product_Configuration__r.Termination_Reason__c;
			this.terminationSubReason = service.csordtelcoa__Product_Configuration__r.Termination_Sub_Reason__c;
			this.terminationDiscount = service.csordtelcoa__Product_Configuration__r.Termination_Fee_Discount__c;
			this.terminationDiscountType = service.csordtelcoa__Product_Configuration__r.Termination_Fee_Discount_Type__c;
			this.terminationFee = service.csordtelcoa__Product_Configuration__r.Termination_Fee_Calculation__c;
			this.contractTermPeriod = service.csordtelcoa__Product_Configuration__r.cscfga__Contract_Term__c;
			this.totalRecurringCharge = service.csordtelcoa__Product_Configuration__r.cscfga__total_recurring_charge__c;
			this.contractEndDate = service.Contract_End_Date__c;
		}
	}
}
