/**
 * @description         This class is responsible for processing the acocunts put in the temp table
 * @author              Marcel Vreuls
 * @History             dec 2018: changes for W-0010002
 * 
 * @Defaults            
 *
 *
 */
global class AccountAssetProcessBatch implements Database.Batchable<sObject>, Database.AllowsCallouts {
    
   public  List<Account> accountsToUpdate = new List<Account>();

    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('Select Account__c from AccountToUpdate__c');
    }

   global void execute(Database.BatchableContext BC, List<AccountToUpdate__c> scope) {


    Integer retainable=0;    //teller to count the retainable assest
    Integer active=0;         //teller to count the Active assets
    
    for(AccountToUpdate__c xx : scope){        
        // Added limit 50000 to prevent SOQL error
        // Accounts that have more than 50000 are not valid anyway so the counts are not important for those
        List<VF_Asset__c> allVFAsset = [Select Id, Retainable__c, CTN_Status__c from VF_Asset__c WHERE Account__c = : xx.AccountId__c limit 49000];
        if(!allVFAsset.isEmpty()){
            //system.debug('vreuls-execute:' + atu.Id;
            for(VF_Asset__c x : allVFAsset){
                //System.Debug('Retainable__c' + x.Retainable__c);
                if(x.Retainable__c == true && x.CTN_Status__c == 'Active'){
                    retainable++;
                }
                //System.Debug('CTN_Status__c' + x.CTN_Status__c);
                if (x.CTN_Status__c == 'Active' && x.Priceplan_Class__c == 'Voice'){
                    active++;
                }
            }              
            
            Account a = new Account();
            a.Id = xx.AccountId__c;
            a.Count_Retainable_Assets__c = retainable;
            a.Count_Active_Assets__c = active;
            accountsToUpdate.add(a);
            
        }
        retainable=0; //zet teller op 0, voor nieuwe Account
        active=0;//zet teller op 0, voor nieuwe Account            
    }

    try{                    
        Database.update(accountsToUpdate);
     } catch (DmlException e) {
        System.debug(e.getMessage());
    }

}
    
    global void finish(Database.BatchableContext BC) {

        //Get the batch job for reference in the email.
        AsyncApexJob a = [SELECT
                            Status,
                            NumberOfErrors,
                            TotalJobItems
                          FROM
                            AsyncApexJob
                          WHERE
                            Id =: BC.getJobId()];

        // Send an email to the running user notifying of job completion.
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

        String[] toAddresses = new String[] {UserInfo.getUserEmail()};
        mail.setToAddresses(toAddresses);
        mail.setSubject('AccountAssetProcessBatch ' + a.Status);
        string Processed = 'Accounts processed: \n';
        for(Account ac : accountsToUpdate)
        {
             Processed = Processed + ac.Name  +'\n';
        }
        mail.setPlainTextBody('The Account Olbico Update job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures. ' + Processed );
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        
    }
    
}