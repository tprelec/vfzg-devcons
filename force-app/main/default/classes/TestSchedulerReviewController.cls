/**
 *
 * @author      miguel.cubias@vodafoneziggo.com
 * @description provides use and code coverage to SchedulerReviewController apex class
 * @group       leads united
 * @since       2022.11.08
 */
@IsTest
public class TestSchedulerReviewController {
	@IsTest
	static void getServiceAppointmentLabelWhenOptionExists() {
		String appointmentTypeLabel = SchedulerReviewController.getServiceAppointmentLabel('Call');
		System.assert(String.isNotBlank(appointmentTypeLabel), 'Label was not returned');
	}

	@IsTest
	static void getServiceAppointmentLabelWhenOptionNotExists() {
		Boolean hasException = false;
		try {
			SchedulerReviewController.getServiceAppointmentLabel('Unknown');
		} catch (AuraHandledException e) {
			hasException = true;
		}
		System.assert(hasException, 'Exception was not thrown for unknown appointment type option');
	}
}
