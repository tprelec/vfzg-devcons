@SuppressWarnings('PMD.ApexAssertionsShouldIncludeMessage')
@isTest
private class TestBanVerifierController {
	@isTest
	static void getBanStatusPossitive() {
		List<Account> acctList = createAccountList();

		External_Account__c extAcct0 = new External_Account__c();
		extAcct0.Account__c = acctList[1].Id;
		extAcct0.External_Source__c = 'Unify';
		extAcct0.External_Account_Id__c = '232323';
		insert extAcct0;

		External_Account__c extAcct = new External_Account__c();
		extAcct.Account__c = acctList[0].Id;
		extAcct.External_Source__c = 'BOP';
		extAcct.External_Account_Id__c = '121212';
		extAcct.Related_External_Account__c = extAcct0.Id;
		insert extAcct;

		TestUtils.autoCommit = false;
		Ban__c banNumm = TestUtils.createBan(acctList[0]);
		banNumm.ExternalAccount__c = extAcct.Id; // ExternalAccount__c object
		banNumm.BAN_Status__c = BanVerifierController.STAT_CLOSED;
		insert banNumm;

		Test.startTest();
		Map<String, String> mpResult = BanVerifierController.getBanStatus(banNumm.Id);
		Test.stopTest();
		System.AssertEquals(BanVerifierController.STAT_CLOSED, mpResult.get(BanVerifierController.STATUS_STATUS));
		System.AssertEquals('true', mpResult.get(BanVerifierController.STATUS_AVAILABLE));
		System.AssertEquals('121212', mpResult.get(BanVerifierController.STATUS_EXTERNAL_ACCT_ID));
		System.AssertEquals('232323', mpResult.get(BanVerifierController.STATUS_REL_EXTERNAL_ACCT_ID));
		System.AssertEquals('Y', mpResult.get(BanVerifierController.STATUS_HAS_FIXED));
	}

	@isTest
	static void getBanStatusNoExternalPossitive() {
		List<Account> acctList = createAccountList();

		TestUtils.autoCommit = false;
		Ban__c banNumm = TestUtils.createBan(acctList[0]);
		banNumm.BAN_Status__c = BanVerifierController.STAT_CLOSED;
		insert banNumm;

		Test.startTest();
		Map<String, String> mpResult = BanVerifierController.getBanStatus(banNumm.Id);
		Test.stopTest();
		System.AssertEquals(BanVerifierController.STAT_CLOSED, mpResult.get(BanVerifierController.STATUS_STATUS));
		System.AssertEquals('true', mpResult.get(BanVerifierController.STATUS_AVAILABLE));
		System.AssertEquals('Y', mpResult.get(BanVerifierController.STATUS_HAS_FIXED));
	}

	@isTest
	static void getAssociatedAttributesPossitive() {
		List<Account> acctList = createAccountList();
		TestUtils.autoCommit = false;
		Ban__c banNumm = TestUtils.createBan(acctList[0]);
		banNumm.BAN_Status__c = BanVerifierController.STAT_CLOSED;
		insert banNumm;

		TestUtils.autoCommit = true;
		Financial_Account__c fa = TestUtils.createFinancialAccount(banNumm, TestUtils.createContact(acctList[0]).Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity oppty = new Opportunity();
		oppty.Name = 'Buy groserias';
		oppty.StageName = 'Closing';
		oppty.CloseDate = Date.today();
		oppty.AccountId = acctList[0].Id;
		oppty.Ban__c = banNumm.Id;
		oppty.Financial_Account__c = fa.Id;
		oppty.Billing_Arrangement__c = bar.Id;
		insert oppty;

		Map<String, String> mpResult = banVerifierController.getAssociatedAttributes(String.valueOf(oppty.Id), 'BAN__c', 'AccountId');
		System.AssertEquals(String.valueOf(banNumm.Id), mpResult.get('banFieldValue'));
		System.AssertEquals(String.valueOf(acctList[0].Id), mpResult.get('acctFieldValue'));
		System.AssertEquals('false', mpResult.get('disabled'));
	}

	@isTest
	static void requestNewPossitive() {
		TestUtils.autoCommit = true;
		List<Account> acctList = createAccountList();
		Opportunity oppty = TestUtils.createOpportunity(acctList[0], Test.getStandardPricebookId());

		Map<String, String> mapResponse = BanVerifierController.requestNew(oppty.Id, 'BAN__c', acctList[0].Id);
		System.assert(mapResponse.get('Name').startsWith('Request'));
		System.assert(mapResponse.get('Id') != null);
	}

	@isTest
	static void savePossitive() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		List<Account> acctList = createAccountList();

		External_Account__c extAcct0 = new External_Account__c();
		extAcct0.Account__c = acctList[1].Id;
		extAcct0.External_Source__c = 'Unify';
		extAcct0.External_Account_Id__c = '232323';
		insert extAcct0;

		External_Account__c extAcct = new External_Account__c();
		extAcct.Account__c = acctList[0].Id;
		extAcct.External_Source__c = 'BOP';
		extAcct.External_Account_Id__c = '121212';
		extAcct.Related_External_Account__c = extAcct0.Id;
		insert extAcct;

		TestUtils.autoCommit = false;
		Ban__c banNumm = TestUtils.createBan(acctList[0]);
		banNumm.ExternalAccount__c = extAcct.Id; // ExternalAccount__c object
		banNumm.BAN_Status__c = BanVerifierController.STAT_CLOSED;
		insert banNumm;

		TestUtils.autoCommit = true;
		Opportunity oppty = TestUtils.createOpportunity(acctList[0], Test.getStandardPricebookId());
		oppty.OwnerId = UserInfo.getUserId();
		//insert oppty;
		update oppty;

		BanVerifierController.save(oppty.Id, banNumm.Id, 'BAN__c', acctList[0].Id);

		Opportunity oppSv = [SELECT Id, BAN__c FROM Opportunity WHERE Id = :oppty.Id LIMIT 1];
		System.assertEquals(banNumm.Id, oppSv.BAN__c);
	}

	@isTest
	public static void testSharingInsert() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		// BAN__c ban = TestUtils.createBan(acc);
		Account partnerAcc = TestUtils.createPartnerAccount();
		User portalUser = TestUtils.createPortalUser(partnerAcc);

		Test.startTest();
		BanVerifierController.createAccountSharing(acc.Id, portalUser.Id);
		//BanManagerData.createBanSharing(ban, UserInfo.getUserId());
		Test.stopTest();

		List<AccountShare> accShares = [SELECT Id FROM AccountShare];
		System.assert(!accShares.isEmpty());
	}

	private static List<Account> createAccountList() {
		List<Account> acctList = new List<Account>();
		acctList.add(new Account(Name = 'The Hollywood Company', VZ_Framework_Agreement__c = 'VZ-001', Frame_Work_Agreement__c = 'VF-001'));
		acctList.add(new Account(Name = 'The Schoonmaakt bedrijf', VZ_Framework_Agreement__c = 'VZ-002', Frame_Work_Agreement__c = 'VF-001'));
		acctList.add(new Account(Name = 'Miscellaneous bedrijft', VZ_Framework_Agreement__c = 'VZ-003', Frame_Work_Agreement__c = 'VF-001'));
		insert acctList;
		return acctList;
	}

	@TestSetup
	static void makeData() {
		Special_Authorizations__c recSetting = new Special_Authorizations__c();
		recSetting.Close_NonStandard_Opportunities__c = true;
		recSetting.SetupOwnerId = UserInfo.getUserId();
		upsert recSetting;
	}
}
