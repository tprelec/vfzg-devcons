public with sharing class DealSummaryTotalOverviewData {
    @AuraEnabled
    public String basketStatus;
    
    @AuraEnabled
    public String basketChannel;
    
    @AuraEnabled
    public Decimal totalNetOneOff;
    
    @AuraEnabled
    public Decimal totalNetRecurring;
    
    @AuraEnabled
    public Decimal totalOneOffDiscount;
    
    @AuraEnabled
    public Decimal totalRecurringDiscount;

    @AuraEnabled
    public Decimal contributionMargin;    
    
    @AuraEnabled
    public Decimal contributionMarginMin;
    
    @AuraEnabled
    public Decimal contributionMarginMax;
    
    public DealSummaryTotalOverviewData(List<CS_Basket_Snapshot_Transactional__c> snapshots, cscfga__Product_Basket__c basket) {  

        List<CS_DealSummaryContributionMargin> dscm = getDealSummaryConfig('','contributionMarginLimits');
            
        Decimal totalNetOneOff = 0.00;
        Decimal totalNetRecurring = 0.00;
        Decimal totalOneOffDiscount = 0.00;
        Decimal totalRecurringDiscount = 0.00;
        
        String profileName = CS_DealSummaryController.getProfileName();
        List<String> profilesForDisconnects = Constants.DEAL_SUMMARY_PROFILES_FOR_DISCONNECTS;
        
        for (CS_Basket_Snapshot_Transactional__c bst : snapshots) {
            if (bst.Product_Configuration__c != null && bst.Product_Configuration__r.Deal_type__c == 'Disconnect' && !(profilesForDisconnects.contains(profileName))) {
                continue;
            }

            if (bst.NetPriceOneOff__c != null) {
                totalNetOneOff += bst.FinalPriceOneOff__c;
            }
            
            if (bst.NetPriceRecurring__c != null) {
                totalNetRecurring += bst.FinalPriceRecurring__c;
            }
            
            if (bst.DiscountOneOff__c != null) {
                totalOneOffDiscount += bst.DiscountOneOff__c;
            }
            
            if (bst.DiscountRecurring__c != null) {
                totalRecurringDiscount += bst.DiscountRecurring__c;
            }
        }
        
        this.totalNetOneOff = totalNetOneOff;
        this.totalNetRecurring = totalNetRecurring;
        this.totalOneOffDiscount = totalOneOffDiscount;
        this.totalRecurringDiscount = totalRecurringDiscount;
        
        this.basketStatus = basket.cscfga__Basket_Status__c;
        this.basketChannel = basket.DirectIndirect__c;
        if (basket.Contribution_Margin__c != null) {
            this.contributionMargin = basket.Contribution_Margin__c/100;
         }
        if (dscm != null && dscm.size() > 0) {
            this.contributionMarginMin = dscm[0].minValue/100;
            this.contributionMarginMax = dscm[0].maxValue/100;
        }

    }

    private List<CS_DealSummaryContributionMargin> getDealSummaryConfig(String profileName, String configName) {
        List<CSDealSummary_Configuration_Association__c> dealSummaryConfField = [
            SELECT name, CS_Deal_Summary_Configuration__c,CS_Deal_Summary_Configuration__r.Value__c
            FROM CSDealSummary_Configuration_Association__c
            WHERE name = :configName AND Profile_Name__c = :profileName
        ];

        List<CS_DealSummaryContributionMargin> result = new List<CS_DealSummaryContributionMargin>();

        if (!dealSummaryConfField.isEmpty()) {
            List<CS_DealSummaryContributionMargin> objectFields = (List<CS_DealSummaryContributionMargin>)JSON.deserialize(dealSummaryConfField[0].CS_Deal_Summary_Configuration__r.Value__c, List<CS_DealSummaryContributionMargin>.class);
    
            for (CS_DealSummaryContributionMargin fv :objectFields) {
                 CS_DealSummaryContributionMargin fa =  new CS_DealSummaryContributionMargin(fv.minValue,fv.maxValue,fv.type);
                result.add(fa);
            }
        }

        return result;
    }
}