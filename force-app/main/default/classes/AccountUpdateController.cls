/**
 * @description			This class is responsible for updating the Account Object
 * @author				Marcel Vreuls
 * @History				Okt 2017: adjustments for W-000269..
 * 
 * @Defaults   			
 *
 *
 */
public with sharing class AccountUpdateController {

	public Account acc{get;set;}
	ApexPages.Standardcontroller controller;

	public AccountUpdateController(ApexPages.StandardController stdController) {
		controller = stdController;
		controller.addFields(New List<String>{'KVK_number__c'});
		acc = (Account)stdController.getRecord();
	}

	public AccountUpdateController(ApexPages.StandardSetController stdController) {
	}	

	public PageReference updateAccountsBatch(){
		
		AccountOlbicoUpdateBatch olbicoBatch = new AccountOlbicoUpdateBatch();

		Id batchprocessId = Database.executeBatch(olbicoBatch,1);	
		   
 		ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Batch started.'));		
   		return null;
	}

	public PageReference backToAccountList(){
		return new PageReference('/001');
	}


	public PageReference updateAccount(){

		if(acc.KVK_number__c != null){

			OlbicoServiceJSON jService = new OlbicoServiceJSON();

			jService.setRequestRestJson(acc.KVK_number__c);
			jService.makeReqestRestJson(acc.KVK_number__c);

			//if(jService.getAcc() != null){
				try{					
					Database.upsert(jService.getAcc(), Account.Fields.KVK_number__c, false);
				} catch (Exception e){

				}
			//}
		}

		return new pageReference('/' + acc.Id);
	}
}