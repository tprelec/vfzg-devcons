public with sharing class DocumentInvocableService {
	@InvocableMethod(label='Get Files from Agreements' description='Get Files from Agreements')
	public static List<List<String>> getFilesFromAgreements(List<RequestParamater> requests) {
		List<String> setFiles = new List<String>();
		List<String> setContentDocumentIds = new List<String>();
		List<String> setOpportunitiy = new List<String>();
		for (RequestParamater request : requests) {
			setOpportunitiy.add(request.opportunityId);
		}

		Opportunity opp = [
			SELECT Id, Docusign_Attachment_Status__c, OwnerId, DocuSign_On_Click_Option__c
			FROM Opportunity
			WHERE Id IN :setOpportunitiy
		];

		if (String.isBlank(opp.Docusign_Attachment_Status__c) || opp.Docusign_Attachment_Status__c == Constants.DOCUSIGN_ATTACHMENT_STATUS_DEFAULT) {
			List<CS_DocItem> documents = CS_OppDocController.getDocuments(opp.Id);
			DocumentService.getInstance().attachDocumentsToSObjectForRegularAttachments(opp.Id, documents);
			DocumentService.getInstance().attachDocumentsToSObjectForContractConditions(opp.Id, documents);
			opp.Docusign_Attachment_Status__c = Constants.DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED;
			update opp;

			List<ContentDocumentLink> documentLink = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :opp.Id];

			for (ContentDocumentLink doc : documentLink) {
				setContentDocumentIds.add(doc.ContentDocumentId);
			}

			List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion WHERE ContentDocumentId IN :setContentDocumentIds];

			for (ContentVersion content : contentVersions) {
				setFiles.add(content.Id);
			}
		}
		return new List<List<String>>{ setFiles };
	}

	public class RequestParamater {
		@InvocableVariable(label='Opportunity Id')
		public String opportunityId;
	}
}
