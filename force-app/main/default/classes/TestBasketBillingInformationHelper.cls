/*
 * @author Rahul Sharma
 * @date 05-05-2022
 *
 * @description Test class for BasketBillingInformationHelper
 */
@IsTest
public with sharing class TestBasketBillingInformationHelper {
	@IsTest
	private static void testValidateFieldsOnOpportunityWithEmptyFields() {
		// insert the configuration validation fields
		TestUtils.createOrderValidationBilling();
		// insert the configuration validations
		TestUtils.createOrderValidations();

		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Contact contact = TestUtils.createContact(account);
		Ban__c ban = TestUtils.createBan(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);

		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.BAN__c = ban.Id;
		opportunity.Financial_Account__c = fa.Id;
		opportunity.Billing_Arrangement__c = null;
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;

		Test.startTest();

		String billingInformationValidationmessage = BasketBillingInformationHelper.validateFieldsOnOpportunity(opportunity);

		Test.stopTest();

		System.assertEquals(
			System.Label.BasketBillingInformation_FieldsAreRequired,
			billingInformationValidationmessage,
			'we must get required field message'
		);
	}

	@IsTest
	private static void testValidateFieldsOnOpportunityWithFieldsPopulatedAndInvalid() {
		// insert the configuration validation fields
		TestUtils.createOrderValidationBilling();
		// insert the configuration validations
		TestUtils.createOrderValidations();

		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Contact contact = TestUtils.createContact(account);
		Ban__c ban = TestUtils.createBan(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);

		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.BAN__c = ban.Id;
		opportunity.Financial_Account__c = fa.Id;
		opportunity.Billing_Arrangement__c = ba.Id;
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;

		Test.startTest();

		String billingInformationValidationmessage = BasketBillingInformationHelper.validateFieldsOnOpportunity(opportunity);

		Test.stopTest();

		System.assertEquals(
			System.Label.BasketBillingInformation_FieldsMustBeValid,
			billingInformationValidationmessage,
			'we must get fields must be valid message'
		);
	}

	@IsTest
	private static void testValidateFieldsOnOpportunityWithFieldsPopulatedAndValid() {
		// insert the configuration validation fields
		TestUtils.createOrderValidationBilling();

		// insert the configuration validations
		List<Order_Validation__c> billingValidationConfiguration = new List<Order_Validation__c>{
			new Order_Validation__c(
				Active__c = true,
				Proposition__c = 'One Fixed;One Fixed Express;Legacy',
				Object__c = 'Ban__c',
				API_Fieldname__c = 'Name',
				Error_message__c = 'test',
				Export_System__c = 'SIAS',
				Operator__c = 'isblank',
				Who__c = 'Inside Sales;'
			),
			new Order_Validation__c(
				Active__c = true,
				Proposition__c = 'One Fixed;One Fixed Express;Legacy',
				Object__c = 'Financial_Account__c',
				API_Fieldname__c = 'Name',
				Error_message__c = 'test',
				Export_System__c = 'SIAS',
				Operator__c = 'isblank',
				Who__c = 'Inside Sales;'
			),
			new Order_Validation__c(
				Active__c = true,
				Proposition__c = 'One Fixed;One Fixed Express;Legacy',
				Object__c = 'Billing_Arrangement__c',
				API_Fieldname__c = 'Name',
				Error_message__c = 'test',
				Export_System__c = 'SIAS',
				Operator__c = 'isblank',
				Who__c = 'Inside Sales;'
			)
		};
		insert billingValidationConfiguration;

		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Contact contact = TestUtils.createContact(account);
		Ban__c ban = new Ban__c(Account__c = account.Id, Unify_Customer_Type__c = 'C', Unify_Customer_SubType__c = 'B', Name = '388888856');
		insert ban;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);

		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.BAN__c = ban.Id;
		opportunity.Financial_Account__c = fa.Id;
		opportunity.Billing_Arrangement__c = ba.Id;
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;

		Test.startTest();

		String billingInformationValidationmessage = BasketBillingInformationHelper.validateFieldsOnOpportunity(opportunity);

		Test.stopTest();

		System.assertEquals(null, billingInformationValidationmessage, 'There must not be any billing errors');
	}

	@IsTest
	private static void testValidateFieldsOnOpportunityCoverGenericException() {
		// insert the configuration validation fields
		TestUtils.createOrderValidationBilling();

		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Contact contact = TestUtils.createContact(account);
		Ban__c ban = new Ban__c(Account__c = account.Id, Unify_Customer_Type__c = 'C', Unify_Customer_SubType__c = 'B', Name = '388888856');
		insert ban;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);

		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.BAN__c = ban.Id;
		opportunity.Financial_Account__c = fa.Id;
		opportunity.Billing_Arrangement__c = ba.Id;
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;

		Test.startTest();

		String billingInformationValidationmessage = BasketBillingInformationHelper.validateFieldsOnOpportunity(opportunity);

		Test.stopTest();

		System.assertEquals(
			System.Label.BasketBillingInformation_GenericExceptionMessage,
			billingInformationValidationmessage,
			'Generic exception should have been caught here'
		);
	}

	@IsTest
	private static void testCopyAllFieldsFromOpportunityToPCs() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Contact contact = TestUtils.createContact(account);
		Ban__c ban = TestUtils.createBan(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);

		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);

		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.BAN__c = ban.Id;
		opportunity.Financial_Account__c = fa.Id;
		opportunity.Billing_Arrangement__c = ba.Id;
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opportunity.id, csbb__Account__c = account.Id);
		insert basket;

		List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>{
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id),
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id)
		};
		insert configs;

		Test.startTest();

		BasketBillingInformationHelper.copyFieldsFromOpportunityToPCs(basket.Id, configs);

		Test.stopTest();

		configs = [
			SELECT BAN_Information__c, Financial_Account__c, Billing_Arrangement__c
			FROM cscfga__Product_Configuration__c
			WHERE Id IN :configs
		];

		for (cscfga__Product_Configuration__c config : configs) {
			System.assertEquals(ban.Id, config.BAN_Information__c, 'BAN must be populated');
			System.assertEquals(fa.Id, config.Financial_Account__c, 'FA must be populated');
			System.assertEquals(ba.Id, config.Billing_Arrangement__c, 'BA must be populated');
		}

		System.assertEquals(
			2,
			[
				SELECT COUNT()
				FROM cscfga__Product_Configuration__c
				WHERE Id IN :configs AND BAN_Information__c = :ban.Id AND Financial_Account__c = :fa.Id AND Billing_Arrangement__c = :ba.Id
			],
			'BAN, FA and BA must be populated'
		);
	}

	@IsTest
	private static void testCopyFinancialAccountFromOpportunityToPCs() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Contact contact = TestUtils.createContact(account);
		Ban__c ban = TestUtils.createBan(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);

		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.BAN__c = null;
		opportunity.Financial_Account__c = fa.Id;
		opportunity.Billing_Arrangement__c = null;
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opportunity.id, csbb__Account__c = account.Id);
		insert basket;

		List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>{
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id),
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id)
		};
		insert configs;

		Test.startTest();

		BasketBillingInformationHelper.copyFieldsFromOpportunityToPCs(basket.Id, configs);

		Test.stopTest();

		configs = [
			SELECT BAN_Information__c, Financial_Account__c, Billing_Arrangement__c
			FROM cscfga__Product_Configuration__c
			WHERE Id IN :configs
		];

		for (cscfga__Product_Configuration__c config : configs) {
			System.assertEquals(null, config.BAN_Information__c, 'BAN must be empty');
			System.assertEquals(fa.Id, config.Financial_Account__c, 'FA must be populated');
			System.assertEquals(null, config.Billing_Arrangement__c, 'BA must be empty');
		}

		System.assertEquals(
			2,
			[
				SELECT COUNT()
				FROM cscfga__Product_Configuration__c
				WHERE Id IN :configs AND BAN_Information__c = NULL AND Financial_Account__c = :fa.Id AND Billing_Arrangement__c = NULL
			],
			'FA must be populated'
		);
	}

	@IsTest
	private static void testCopyBillingArrangementFromOpportunityToPCs() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Contact contact = TestUtils.createContact(account);
		Ban__c ban = TestUtils.createBan(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);

		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);

		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.BAN__c = null;
		opportunity.Financial_Account__c = null;
		opportunity.Billing_Arrangement__c = ba.Id;
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opportunity.id, csbb__Account__c = account.Id);
		insert basket;

		List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>{
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id),
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id)
		};
		insert configs;

		Test.startTest();

		BasketBillingInformationHelper.copyFieldsFromOpportunityToPCs(basket.Id, configs);

		Test.stopTest();

		configs = [
			SELECT BAN_Information__c, Financial_Account__c, Billing_Arrangement__c
			FROM cscfga__Product_Configuration__c
			WHERE Id IN :configs
		];

		for (cscfga__Product_Configuration__c config : configs) {
			System.assertEquals(null, config.BAN_Information__c, 'BAN must be empty');
			System.assertEquals(null, config.Financial_Account__c, 'FA must be empty');
			System.assertEquals(ba.Id, config.Billing_Arrangement__c, 'BA must be populated');
		}

		System.assertEquals(
			2,
			[
				SELECT COUNT()
				FROM cscfga__Product_Configuration__c
				WHERE Id IN :configs AND BAN_Information__c = NULL AND Financial_Account__c = NULL AND Billing_Arrangement__c = :ba.Id
			],
			'BA must be populated'
		);
	}

	@IsTest
	private static void testCopyEmptyFieldsFromOpportunityToPCs() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);

		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		opportunity.BAN__c = null;
		opportunity.Financial_Account__c = null;
		opportunity.Billing_Arrangement__c = null;
		update opportunity;
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opportunity.id, csbb__Account__c = account.Id);
		insert basket;

		List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>{
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id),
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id)
		};
		insert configs;

		Test.startTest();

		BasketBillingInformationHelper.copyFieldsFromOpportunityToPCs(basket.Id, configs);

		Test.stopTest();

		configs = [
			SELECT BAN_Information__c, Financial_Account__c, Billing_Arrangement__c
			FROM cscfga__Product_Configuration__c
			WHERE Id IN :configs
		];

		for (cscfga__Product_Configuration__c config : configs) {
			System.assertEquals(null, config.BAN_Information__c, 'BAN must be empty');
			System.assertEquals(null, config.Financial_Account__c, 'FA must be empty');
			System.assertEquals(null, config.Billing_Arrangement__c, 'BA must be empty');
		}

		System.assertEquals(
			2,
			[
				SELECT COUNT()
				FROM cscfga__Product_Configuration__c
				WHERE Id IN :configs AND BAN_Information__c = NULL AND Financial_Account__c = NULL AND Billing_Arrangement__c = NULL
			],
			'BAN, FA and BA must be empty'
		);
	}
}
