/**
 * @description: This class is responsible for invoking the OrderPABX REST service
 * @author: Jurgen van Westreenen
 */
@SuppressWarnings('PMD')
public class OrderPABXRest {
    @TestVisible
    private static final String INTEGRATION_SETTING_NAME = 'SIASRESTOrderPABX';
    @TestVisible
    private static final String MOCK_URL = 'http://example.com/OrderPABXRest';

    public static void createOrderPABX(String contProdId) {
        /*
        // rahul: used a generic method to fetch the integration details
        IWebServiceConfig webServiceConfig;
        if(!Test.isRunningTest()) {
            webServiceConfig = WebServiceConfigLocator.getConfig(INTEGRATION_SETTING_NAME);
        } else {
            webServiceConfig = WebServiceConfigLocator.createConfig();
            webServiceConfig.setEndpoint(MOCK_URL);
        }

        string endpointURL = webServiceConfig.getEndpoint();
        string authorizationHeader = 'Bearer ' + webServiceConfig.getAuthorizationHeader();

        if(String.isNotEmpty(webServiceConfig.getCertificateName())) {
            reqData.setClientCertificateName(webServiceConfig.getCertificateName());
        }
*/

        // Retrieve the credentials from the custom setting
        External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(
            INTEGRATION_SETTING_NAME
        );
        string endpointURL = webServiceConfig.URL__c;
        // String authorizationHeaderString = webServiceConfig.Authorization_Header__c;
        // string authorizationHeader = authorizationHeaderString; // 'Bearer ' + authorizationHeaderString;
        String authorizationHeaderString =
            webServiceConfig.Username__c +
            ':' +
            webServiceConfig.Password__c;
        String authorizationHeader =
            'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(authorizationHeaderString));
        System.debug('authorizationHeader: ' + authorizationHeader);

        HttpRequest reqData = new HttpRequest();
        Http http = new Http();

        reqData.setHeader('Content-Type', 'application/json');
        reqData.setHeader('Connection', 'keep-alive');
        reqData.setHeader('Content-Length', '0');

        reqData.setHeader('Authorization', authorizationHeader);
        // rahul: increased the timeout
        reqData.setTimeout(120000);
        reqData.setEndpoint(endpointURL);

        if (String.isNotEmpty(webServiceConfig.Certificate_Name__c)) {
            reqData.setClientCertificateName(webServiceConfig.Certificate_Name__c);
        }

        try {
            // Retrieve the necessary product data based on passed Contracted Product Id
            List<Contracted_Products__c> prodList = [
                SELECT
                    Id,
                    Order__c,
                    Site__r.Assigned_Product_Id__c,
                    Customer_Asset__r.Installed_Base_Id__c,
                    Customer_Asset__r.Billing_Arrangement__c,
                    PBX__r.Billing_Arrangement__r.Unify_Ref_Id__c,
                    PBX__r.Billing_Arrangement__r.Financial_Account__r.BAN__r.Unify_Ref_Id__c,
                    PBX__r.Name,
                    Unify_Product_Family__c,
                    Unify_Billing_Offer__c,
                    PBX_Error_Info__c,
                    Order__r.Billing_Arrangement__c,
                    Order__r.Billing_Arrangement__r.Financial_Account__c,
                    Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__c,
                    Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.ExternalAccount__c
                FROM Contracted_Products__c
                WHERE PBX__c != NULL AND PBX__r.PABX_Id__c = NULL AND Id = :contProdId
                LIMIT 1
            ];
            if (prodList.size() > 0) {
                Contracted_Products__c contProd = prodList[0];
                // key ring
                String siteId = contProd.Site__c;
                String extAccId = contProd.Order__r
                    ?.Billing_Arrangement__r
                    ?.Financial_Account__r
                    ?.Ban__r
                    ?.ExternalAccount__c;
                String slsApId;
                String extSource = 'Unify';
                List<External_Site__c> extSiteList = KeyRingService.getExternalsites(
                    siteId,
                    extAccId,
                    extSource
                );
                if (extSiteList.size() > 0) {
                    slsApId = extSiteList[0].SLSAPID__c;
                }
                // Generate the request JSON message
                JSONGenerator generator = JSON.createGenerator(false);
                generator.writeStartObject();
                generator.writeFieldName('orderPABXRequest');
                generator.writeStartObject();
                generator.writeStringField(
                    'SFOrderID',
                    contProd.Order__c != null ? string.valueOf(contProd.Order__c) : ''
                );
                //generator.writeStringField('SLSAPID', contProd.Site__r.Assigned_Product_Id__c != null ? string.valueOf(contProd.Site__r.Assigned_Product_Id__c) : '' );
                // key ring
                generator.writeStringField(
                    'SLSAPID',
                    slsApId != null ? string.valueOf(slsApId) : ''
                );
                generator.writeStringField(
                    'SFIBID',
                    contProd.Customer_Asset__r.Installed_Base_Id__c != null
                        ? string.valueOf(contProd.Customer_Asset__r.Installed_Base_Id__c)
                        : ''
                );
                generator.writeStringField(
                    'BAID',
                    contProd.PBX__r.Billing_Arrangement__r.Unify_Ref_Id__c != null
                        ? string.valueOf(contProd.PBX__r.Billing_Arrangement__r.Unify_Ref_Id__c)
                        : ''
                );
                generator.writeStringField(
                    'BCID',
                    contProd.PBX__r.Billing_Arrangement__r.Financial_Account__r.BAN__r.Unify_Ref_Id__c !=
                        null
                        ? string.valueOf(
                                contProd.PBX__r.Billing_Arrangement__r.Financial_Account__r.BAN__r.Unify_Ref_Id__c
                          )
                        : ''
                );
                generator.writeStringField(
                    'PBXLabel',
                    contProd.PBX__r.Name != null ? string.valueOf(contProd.PBX__r.Name) : ''
                );
                generator.writeStringField(
                    'unifyFamilyTag',
                    contProd.Unify_Product_Family__c != null
                        ? string.valueOf(contProd.Unify_Product_Family__c)
                        : ''
                );
                generator.writeStringField(
                    'unifyBillingOfferID',
                    contProd.Unify_Billing_Offer__c != null
                        ? string.valueOf(contProd.Unify_Billing_Offer__c)
                        : ''
                );
                generator.writeEndObject();
                generator.writeEndObject();
                system.debug('### Request body: ' + generator.getAsString());

                reqData.setBody(generator.getAsString());
                reqData.setMethod('POST');

                HTTPResponse response = http.send(reqData);
                system.debug('### Response: ' + response);
                system.debug('### Response body: ' + response.getBody());
                // If the request is successful, parse the JSON response.
                if (response.getStatusCode() == 200) {
                    // Deserializes the JSON string.
                    OrderPABXSuccessResponse resultSuccess = (OrderPABXSuccessResponse) JSON.deserializeStrict(
                        response.getBody(),
                        OrderPABXSuccessResponse.class
                    );
                    OrderPABXRespData result = resultSuccess.orderPABXResponse;
                    if (
                        contProd.Order__c == result.SFOrderID &&
                        contProd.Customer_Asset__r.Installed_Base_Id__c == result.SFIBID
                    ) {
                        String compAssetId = contProd.PBX__c;
                        List<Competitor_Asset__c> compAsset = [
                            SELECT Id, Assigned_Product_Id__c, PABX_Id__c
                            FROM Competitor_Asset__c
                            WHERE Id = :compAssetId
                            LIMIT 1
                        ];
                        // Update the corresponding Competitor Asset
                        if (compAsset.size() > 0) {
                            Competitor_Asset__c ca = compAsset[0];
                            ca.Assigned_Product_Id__c = result.APID;
                            ca.PABX_Id__c = result.PABXID;
                            update ca;
                        }
                    }
                } else {
                    // Otherwise write the error response to the Contracted Product
                    //OrderPABXErrorResponse result = (OrderPABXErrorResponse) JSON.deserializeStrict(response.getBody(), OrderPABXErrorResponse.class);
                    // String result = (String) JSON.deserialize(response.getBody(), String.class);
                    String result = response.getBody();
                    if (String.isEmpty(contProd.PBX_Error_Info__c)) {
                        contProd.PBX_Error_Info__c = '';
                    }
                    contProd.PBX_Error_Info__c = result.replace('&quot;', '"').left(255);
                    update contProd;
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            // Nothing to do here
        }
    }
    public class OrderPABXSuccessResponse {
        public OrderPABXRespData orderPABXResponse;
        public OrderPABXSuccessResponse(OrderPABXRespData orderPABXResponse) {
            this.orderPABXResponse = orderPABXResponse;
        }
    }
    public class OrderPABXRespData {
        public String APID;
        public String SFOrderID;
        public String PABXID;
        public String SFIBID;
        public OrderPABXRespData(String APID, String SFOrderID, String PABXID, String SFIBID) {
            this.APID = APID;
            this.SFOrderID = SFOrderID;
            this.PABXID = PABXID;
            this.SFIBID = SFIBID;
        }
    }
    /*  private class OrderPABXRequest {
        public String SFOrderID;
        public String SLSAPID;
        public String SFIBID;
        public String BAID;
        public String BCID;
        public String PBXLabel;
        public String unifyFamilyTag;
        public String unifyBillingOfferID;
    } 
    private class OrderPABXErrorResponse {
        public ErrorInfo errorInfo;  
    }
    private class ErrorInfo {
        public String errorCode;
        public String errorDescription;
        public String targetSystemName;
        public String targetServiceName;
    } */
}