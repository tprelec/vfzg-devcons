/*
    Author: Juan Cardona/Rahul Sharma
    Description: Order Status API, Salesforce commerce cloud uses this api to retrieve the order status
    Jira ticket: COM-1542
*/

@RestResource(urlMapping='/orders/getStatus')
global with sharing class SFCC_OrderApi {
    @TestVisible
    private static final Integer ORDER_API_REQUEST_JSON_PARSER_ERROR_CODE = 400;
    @TestVisible
    private static final String ORDER_API_REQUEST_JSON_PARSER_ERROR = 'JSON_PARSER_ERROR Unexpected character.';
    @TestVisible
    private static final Integer ORDER_API_REQUEST_SUCCESS_CODE = 200;
    @TestVisible
    private static final String ORDER_API_REQUEST_JSON_EMPTY_ATTRIBUTES = 'JSON_EMPTY_ATTRIBUTES Some attributes of the JSON are empty.';
    @TestVisible
    private static final String ORDER_API_REQUEST_ORDER_NOT_FOUND = 'ORDER_NOT_FOUND Order not found in SFDC.';
    @TestVisible
    private static final String ORDER_API_REQUEST_ORDER_STATUS_NOT_FOUND = 'ORDER_API_REQUEST_ORDER_STATUS_NOT_FOUND There is not Order Status related to this Order.';
    @TestVisible
    private static List<COM_B2B_Online_Order_Status__mdt> orderStatusMetaDataList;

    @HttpPost
    global static void getOrderStatus() {
        RestResponse response = RestContext.response;
        List<RequestWrapper> requestWrapperList;
        List<ResponseWrapper> responseWrapperList;
        Map<String, RequestWrapper> requestWrappersByOrderId;
        try {
            String requestJsonString = RestContext.request.requestBody.toString();
            requestWrapperList = (List<RequestWrapper>) JSON.deserialize(requestJsonString, List<RequestWrapper>.class);
        } catch (Exception e) {
            response.statusCode =  ORDER_API_REQUEST_JSON_PARSER_ERROR_CODE;
            response.responseBody = Blob.valueOf(ORDER_API_REQUEST_JSON_PARSER_ERROR);
            return;
        }
        requestWrappersByOrderId = new Map<String, RequestWrapper>();
        responseWrapperList = new List<ResponseWrapper>();
        for (RequestWrapper requestObj :requestWrapperList) {
            if (String.isNotBlank(requestObj.orderNumber) && String.isNotBlank(requestObj.accountId)) {
                requestWrappersByOrderId.put(requestObj.orderNumber, requestObj);
            } else {
                responseWrapperList.add(getResponse(false, ORDER_API_REQUEST_JSON_EMPTY_ATTRIBUTES, requestObj, null, null));
            }
        }
        orderStatusMetaDataList = getStatusCustomMetadataTypes();
        Map<Id, csb2c__Inbound_Ecommerce_Order_Request__c> orderNumberByOpportunityIds = new Map<Id, csb2c__Inbound_Ecommerce_Order_Request__c>();
        for (csb2c__Inbound_Ecommerce_Order_Request__c eCommerceRequest :[SELECT    csb2c__Product_Basket__r.cscfga__Opportunity__c,
                                                                                    csb2c__Product_Basket__r.cscfga__Opportunity__r.IsClosed,
                                                                                    csb2c__Product_Basket__r.cscfga__Opportunity__r.StageName,
                                                                                    csb2c__Product_Basket__r.cscfga__Opportunity__r.IsWon,
                                                                                    csb2c__API_Order_Reference__c
                                                                                    FROM csb2c__Inbound_Ecommerce_Order_Request__c
                                                                                    WHERE csb2c__API_Order_Reference__c IN :requestWrappersByOrderId.keySet()
                                                                                    AND csb2c__Product_Basket__c != NULL
                                                                                    AND csb2c__Product_Basket__r.cscfga__Opportunity__c != NULL]) {
            orderNumberByOpportunityIds.put(eCommerceRequest.csb2c__Product_Basket__r.cscfga__Opportunity__c, eCommerceRequest);
        }
        Map<Id, Order__c> ordersByOppIds = new Map<Id, Order__c>();
        for (Order__c order :[SELECT    Id,
                                        Name,
                                        ProvisioningProcessStatus__c,
                                        VF_Contract__c,
                                        VF_Contract__r.Opportunity__c,
                                        VF_Contract__r.Opportunity__r.Id,
                                        VF_Contract__r.Opportunity__r.IsClosed,
                                        VF_Contract__r.Opportunity__r.StageName,
                                        VF_Contract__r.Opportunity__r.IsWon,
                                        VF_Contract__r.Opportunity__r.Owner.Username,
                                        VF_Contract__r.Opportunity__r.Owner.FirstName,
                                        VF_Contract__r.Opportunity__r.Owner.LastName,
                                        VF_Contract__r.Opportunity__r.Owner.Email
                                        FROM Order__c
                                        WHERE VF_Contract__r.Opportunity__c IN :orderNumberByOpportunityIds.keySet()]) {
            ordersByOppIds.put(order?.VF_Contract__r?.Opportunity__r?.Id, order);
        }
        Map<String, Order__c> orderByOrderNumber = new Map<String, Order__c>();
        Map<String, csb2c__Inbound_Ecommerce_Order_Request__c> orderRequestByOrderNumber = new Map<String, csb2c__Inbound_Ecommerce_Order_Request__c>();
        for (String opportunityId :orderNumberByOpportunityIds.keySet()) {
            if (ordersByOppIds.containsKey(opportunityId)) {
                orderByOrderNumber.put(orderNumberByOpportunityIds.get(opportunityId).csb2c__API_Order_Reference__c, ordersByOppIds.get(opportunityId));
            }
            orderRequestByOrderNumber.put(orderNumberByOpportunityIds.get(opportunityId).csb2c__API_Order_Reference__c, orderNumberByOpportunityIds.get(opportunityId));
        }
        Sobject order;
        CustomIterableUtils iterator;
        csb2c__Inbound_Ecommerce_Order_Request__c orderRequest;
        COM_B2B_Online_Order_Status__mdt orderStatusMetaData;
        for (RequestWrapper requestObj :requestWrappersByOrderId.values()) {
            if ((orderByOrderNumber.containsKey(requestObj.orderNumber) &&
                orderByOrderNumber.get(requestObj.orderNumber).VF_Contract__r?.Opportunity__c != null) ||
                orderRequestByOrderNumber.containsKey(requestObj.orderNumber)) {
                order = orderByOrderNumber.containsKey(requestObj.orderNumber) ? orderByOrderNumber.get(requestObj.orderNumber) : null;
                orderRequest = orderRequestByOrderNumber.containsKey(requestObj.orderNumber) ? orderRequestByOrderNumber.get(requestObj.orderNumber) : null;
                iterator = new CustomIterableUtils(orderStatusMetaDataList);
                while (iterator.hasNext()) {
                    orderStatusMetaData = (COM_B2B_Online_Order_Status__mdt) iterator.next();
                    if (((SObject) Type.forName(orderStatusMetaData.Source_Object__c).newInstance()).getSObjectType() == Schema.Order__c.getSObjectType() &&
                        String.valueOf(order?.get(orderStatusMetaData.Source_Field__c)) == orderStatusMetaData.Value__c &&
                        order?.getSobject('VF_Contract__r')?.getSobject('Opportunity__r')?.get('IsWon') == true) {
                            responseWrapperList.add(getResponse(true, null, requestObj, (Order__c) order, orderStatusMetaData.Commerce_Cloud_Status__c));
                            break;
                    } else if (((SObject) Type.forName(orderStatusMetaData.Source_Object__c).newInstance()).getSObjectType() == Schema.Opportunity.getSObjectType() &&
                        String.valueOf(orderRequest?.getSobject('csb2c__Product_Basket__r')?.getSobject('cscfga__Opportunity__r')?.get(orderStatusMetaData.Source_Field__c)) == orderStatusMetaData.Value__c) {
                            responseWrapperList.add(getResponse(true, null, requestObj, (Order__c) order, orderStatusMetaData.Commerce_Cloud_Status__c));
                            break;
                    } else if (!iterator.hasNext()) {
                        responseWrapperList.add(getResponse(false, ORDER_API_REQUEST_ORDER_STATUS_NOT_FOUND, requestObj, (Order__c) order, null));
                    }
                }
            } else if (!orderByOrderNumber.containsKey(requestObj.orderNumber)) {
                responseWrapperList.add(getResponse(false, ORDER_API_REQUEST_ORDER_NOT_FOUND, requestObj, null, null));
            }
        }
        response.statusCode =  ORDER_API_REQUEST_SUCCESS_CODE;
        response.responseBody = Blob.valueOf( JSON.serialize(responseWrapperList));
    }

    private static List<COM_B2B_Online_Order_Status__mdt> getStatusCustomMetadataTypes() {
        if(orderStatusMetaDataList == null) {
            return COM_B2B_Online_Order_Status__mdt.getAll().values();
        }
        return orderStatusMetaDataList;
    }

    private static ResponseWrapper getResponse(Boolean isSuccess, String message, RequestWrapper request, Order__c order, String orderStatus) {
        OwnerWrapper owner;
        if(onlineChannelUser?.Online_Chanel_Opportunity_Owner__c != order?.VF_Contract__r?.Opportunity__r?.Owner?.Username) {
            owner = new OwnerWrapper(order?.VF_Contract__r?.Opportunity__r?.Owner?.FirstName, order?.VF_Contract__r?.Opportunity__r?.Owner?.LastName, order?.VF_Contract__r?.Opportunity__r?.Owner?.Email);
        }
        ResultWrapper result = new ResultWrapper(isSuccess, message);
        return new ResponseWrapper(request.orderNumber, request.accountId, orderStatus, owner, result);
    }

    private static COM_One_Mobile_Online_Channel_Setting__mdt onlineChannelUser {
        get {
            if (onlineChannelUser == null ) onlineChannelUser = COM_One_Mobile_Online_Channel_Setting__mdt.getInstance('SFCC_Integration_User');
            return onlineChannelUser;
        }
        set;
    }

    global class RequestWrapper {
        public String orderNumber;
        public String accountId;
        global RequestWrapper(String orderNumber, String accountId) {
            this.orderNumber = orderNumber;
            this.accountId = accountId;
        }
    }

    global class ResponseWrapper {
        public String orderNumber;
        public String accountId;
        public String orderStatus;
        public OwnerWrapper owner;
        public ResultWrapper result;
        public ResponseWrapper(String orderNumber, String accountId, String orderStatus, OwnerWrapper owner, ResultWrapper result) {
            this.orderNumber = orderNumber;
            this.accountId = accountId;
            this.orderStatus = orderStatus;
            this.owner = owner;
            this.result = result;
        }
    }

    global class OwnerWrapper {
        public String firstName;
        public String lastName;
        public String email;
        public OwnerWrapper(String firstName, String lastName, String email) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
        }
    }

    global class ResultWrapper {
        public Boolean isSuccess;
        public String message;
        public ResultWrapper(Boolean isSuccess, String message) {
            this.isSuccess = isSuccess;
            this.message = message;
        }
    }
}