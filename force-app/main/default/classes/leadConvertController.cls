/*
    This is the controller for the Visual Force page leadConvertPage.
*/

public with sharing class leadConvertController extends PageControllerBase {
    
    // This is the lead that is to be converted
    public Lead leadToConvert {get; set;}
    
    // Constructor for this controller
    public leadConvertController(ApexPages.StandardController stdController) {
        
        //get the ID to query for the Lead fields
        Id leadId = stdController.getId();
        leadToConvert = [SELECT Id, LeadSource, Status, OwnerId, Name, Company, Street__c, Town__c, Visiting_postalcode__c, Country, Email, FirstName, LastName, Title, DUNS_Number__c, KVK_Number__c, Visiting_Housenumber1__c FROM Lead WHERE Id = :leadId];
    }

    /*
    These are instances of the components' controllers which this class will access.
    
    If you add new custom components, add an instance of the class here
    */
    public leadConvertCoreComponentController myComponentController { get; set; }
    public leadConvertTaskInfoComponentController myTaskComponentController { get; set; }
    public leadConvertTaskDescComponentController myDescriptionComponentController { get; set; }
    
    /*
        These are the set methods which override the methods in PageControllerBase. 
        These methods will be called by the ComponentControllerBase class.
        
        If you add new custom components, a new overridden set method must be added here.
    */
    public override void setComponentController(ComponentControllerBase compController) {
        myComponentController = (leadConvertCoreComponentController)compController;
    }
   
    public override void setTaskComponentController(ComponentControllerBase compController) {
        myTaskComponentController = (leadConvertTaskInfoComponentController)compController;
    }
  
    public override void setDescriptionComponentController(ComponentControllerBase compController) {
        myDescriptionComponentController = (leadConvertTaskDescComponentController)compController;
    } 

    /*
        These are the get methods which override the methods in PageControllerBase.
        
        If you add new custom components, a new overridden get method must be added here.
    */
    public override ComponentControllerBase getMyComponentController() {
        return myComponentController;
    }

    public override ComponentControllerBase getmyTaskComponentController() {
        return myTaskComponentController;
    }   
  
    public override ComponentControllerBase getmyDescriptionComponentController() {
        return myDescriptionComponentController;
    }
    
    //Olbico custom lead convert
    public PageReference olbicoConvertLead() {
        //do checks
        
        // if a due date is set but the subject is not, then show an error 
        if (myTaskComponentController != null && myTaskComponentController.taskID.ActivityDate != null && string.isBlank(myTaskComponentController.taskID.Subject)){
            PrintError('You must enter a Subject if a Due Date is set..');
            return null;
        }
        
        // also other way around
        if (myTaskComponentController != null && myTaskComponentController.taskID.ActivityDate == null && !string.isBlank(myTaskComponentController.taskID.Subject)){
            PrintError('You must enter a Due  Date if a Subject is set..');
            return null;
        }        
            
        //if the main lead convert component is not set then return
        if (myComponentController == NULL) return null;
        
        // if Lead Status is not entered show an error  
        if (myComponentController.leadConvert.Status == 'NONE'){
            PrintError('Please select a Lead Status.');
            return null;
        }
         // if Lead is converted without opportunity creation show an error
        if (myComponentController.doNotCreateOppty == true){
            PrintError('All converted leads must have an opportunity. Please uncheck the "Do not create a new opportunity box" and try again.');
            return null;
        }

        //before converting the lead, cancel any running time-based workflows on the lead
        //we have to temporarily reset the status because the page sets it to 'converted' (that will later be overwritten anyway)
        String currentStatus = LeadToConvert.Status; // this is the converted status
        String previousStatus = [SELECT Id, Status FROM Lead WHERE Id = :LeadToConvert.Id].Status; // this is the actual status before going to the convert screen
        leadToConvert.Status = previousStatus;
        leadToConvert.Workflow_Cancel__c = true;  
        update LeadToConvert;      
        LeadToConvert.Status = currentStatus;

        /**
         * @description         When the Lead Source == 'New KVK' allow standard salesforce lead conversion
         *                      instead of redirecting to the custom OlbicoCDPConvertLead oage
         *                      This is required to permit the conversion of leads with a KVK
         * @author              Gerhard Newman
         */
        PageReference pageRef;
        PageReference pageRef1;
        if (LeadToConvert.LeadSource=='New KVK') {
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(LeadToConvert.id);

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            try {
                Database.LeadConvertResult lcr = Database.convertLead(lc);
                
            } catch(System.DmlException ex) {
                if (ex.getDmlType(0)==StatusCode.DUPLICATE_VALUE) {
                    PrintError(Label.ERROR_Lead_Duplicate);
                } else {
                    PrintError(ex.getDmlMessage(0));
                }
                return null;                
            } 
            ID accountID = [Select ConvertedAccountId from lead where id=:LeadToConvert.id].ConvertedAccountId;    
            pageRef = new PageReference('/'+accountID);

            pageRef.setRedirect(true);
            return pageRef;             
        }
        
        //collect params
        pageRef1 = new PageReference('/apex/OlbicoCdpConvertLead');
        
        //lead data
        pageRef1.getParameters().put('leadId', leadToConvert.Id);
        
        pageRef1.getParameters().put('leadCompany', leadToConvert.Company);
        pageRef1.getParameters().put('leadStreet', leadToConvert.Street__c);
        pageRef1.getParameters().put('leadHousenumber', string.valueof(leadToConvert.Visiting_Housenumber1__c ));
        pageRef1.getParameters().put('leadCity', leadToConvert.Town__c);
        pageRef1.getParameters().put('leadPostalCode', leadToConvert.Visiting_postalcode__c);
        pageRef1.getParameters().put('leadCountry', leadToConvert.Country);

        //pageRef.getParameters().put('accid', myComponentController.selectedAccount);
        pageRef1.getParameters().put('leadTitle', leadToConvert.Title);
        pageRef1.getParameters().put('leadFirstName', leadToConvert.FirstName);
        pageRef1.getParameters().put('leadLastName', leadToConvert.LastName);
        pageRef1.getParameters().put('leadEmail', leadToConvert.Email);
        
        //custom vodafone fields
        pageRef1.getParameters().put('leadDuns', leadToConvert.DUNS_Number__c);
        pageRef1.getParameters().put('leadKvk', leadToConvert.KVK_Number__c);
        
        //lead convert
        pageRef1.getParameters().put('ownerID', myComponentController.contactId.ownerID);
        pageRef1.getParameters().put('sendOwnerEmail', myComponentController.sendOwnerEmail ? 'true' : 'false');
        //pageRef.getParameters().put('selectedAccount', myComponentController.selectedAccount);
        pageRef1.getParameters().put('convertedStatus', myComponentController.leadConvert.Status);
        pageRef1.getParameters().put('doNotCreateOppty', myComponentController.doNotCreateOppty ? 'true' : 'false');
        if (!myComponentController.doNotCreateOppty)
        {
            pageRef1.getParameters().put('opptyName',myComponentController.opportunityID.Name);
        }
        
        //task
        if (myTaskComponentController != null && 
            myDescriptionComponentController != null &&
            myTaskComponentController.taskID.subject != null)
        {
            pageRef1.getParameters().put('taskSubject', myTaskComponentController.taskID.subject);
            pageRef1.getParameters().put('taskIsReminderSet', myTaskComponentController.remCon.taskID.IsReminderSet ? 'true' : 'false');
            pageRef1.getParameters().put('taskActivityDate', convertDateToString(myTaskComponentController.taskID.ActivityDate));
            
            if (myTaskComponentController.remCon.taskID.IsReminderSet)
            {
                //YYYY-MM-DDThh:mm:ss
                DateTime reminderDateTime = convertToDatetime(myTaskComponentController.remCon.taskID.ActivityDate, myTaskComponentController.remCon.reminderTime);
                pageRef1.getParameters().put('taskReminderDateTime', reminderDateTime.format('yyyy-MM-dd\'T\'hh:mm:ss') );
            }
            pageRef1.getParameters().put('taskStatus', myTaskComponentController.taskID.Status);
            pageRef1.getParameters().put('taskPriority', myTaskComponentController.taskID.Priority);
            
            pageRef1.getParameters().put('taskDescription', myDescriptionComponentController.taskID.Description);
            pageRef1.getParameters().put('taskSendNotificationEmail', myDescriptionComponentController.sendNotificationEmail ? 'true' : 'false');
        }
        
        //start new account page
        pageRef1.setRedirect(true);
        return pageRef1; 
    }
    
    // This method is called when the user clicks the Convert button on the VF Page
    public PageReference convertLead() {
        return null;
    }
    
    //this method will take database errors and print them to teh PageMessages 
    public void PrintErrors(Database.Error[] errors)
    {
        for(Database.Error error : errors)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, error.message);
            ApexPages.addMessage(msg);
        }
    }
    
    //This method will put an error into the PageMessages on the page
    public void PrintError(string error) {
        ApexPages.Message msg = new 
            ApexPages.Message(ApexPages.Severity.ERROR, error);
        ApexPages.addMessage(msg);
    } 
    
    //Convert a date to yyyy-mm-dd string
    public static string convertDateToString(Date d) {
        DateTime dt = Datetime.newInstance(d.year(), d.month(),d.day(), 0, 0, 0);
        return dt.format('yyyy-MM-dd');
    }
    
    //Given a date and time, where time is a string this method will return a DateTime
    public static DateTime convertToDatetime(Date d, String t) {
        string AM_PM;
        String timeFormat = DateTimeUtility.LocaleToTimeFormatMap().get(UserInfo.getLocale());
 
        //if the local of the user uses AM/PM 
        //if (timeFormat != null && timeFormat.endsWith('a')) {
        if (t.length() > 5){
            
            //split the time into 2 strings 1 time and 1 am r pm
            string [] reminderTime = t.split(' ');
            
            //split the time into hour and minute
            string hour = reminderTime[0].split(':')[0];
            string min = reminderTime[0].split(':')[1];
            
            //get the am or pm
                if(t.length() > 5){
                AM_PM = reminderTime[1];
                }
 
            //turn the hour into an integer
            Integer hr = Integer.valueOf(hour);
            
            //if the am/pm part of the string is PM then add 12 hours
               
            if (AM_PM.equalsIgnoreCase('PM')) hr += 12;
            
            //return a new DateTime based on the above information
            return (
                DateTime.newInstance(
                    d, 
                    Time.newInstance(
                        hr, 
                        Integer.valueOf(min), 
                        0,
                        0
                    )
                )
            ); 
        }
        //If the user's local does not use AM/PM and uses 24 hour time
        else {
            
            //split the time by a : to get hour and minute
            string hour = t.split(':')[0];
            string min = t.split(':')[1];
            
            //turn the hour into an integer
            Integer hr = Integer.valueOf(hour);
            
            //return a new DateTime based on the above information
            return (
                DateTime.newInstance(
                    d, 
                    Time.newInstance(
                        hr, 
                        Integer.valueOf(min), 
                        0,
                        0
                    )
                )
            ); 
        }
    }
}