/**
 * @description         The function of this class is to expose the DataFactory as a webservice
                        so that Tosca can use it to setup it's Data used during regression tests
                        on UAT, this class is blocked and should never be used on production
 *                      
 * @author              Chris Appels
 */

@RestResource(urlMapping='/DataFactory/*')
global with sharing class DataFactoryWebService {

    //https://vodafoneziggo--uat.lightning.force.com//services/apexrest/DataFactory

    @Httpget
    global static DataFactoryWebServiceResponse setupTestAccount() {
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        res.addHeader('Content-Type', 'application/json');

        String toReturn;
        DataFactoryWebServiceResponse response = new DataFactoryWebServiceResponse();

         try { 
            res.statusCode = 200;
            response.results.put('AccountId', DataFactory.setupTestAccounts(1));
            //res.responseBody = Blob.valueOf(response);
        } catch (Exception e) {
            res.statuscode = 400;
            response.results.put('Error', e.getMessage());
            //res.responseBody = Blob.valueOf(response);

        }
        return response;
    }

    global class DataFactoryWebServiceResponse{
        public Map<String, String> results;

        global DataFactoryWebServiceResponse(){
            results = new Map<String, String>();
        }
    }
}