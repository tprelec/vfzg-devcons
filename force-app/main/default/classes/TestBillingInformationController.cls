/*
 * @author Rahul Sharma
 *
 * @description Test class for BillingInformationController
 */

@IsTest
private class TestBillingInformationController {
	@IsTest
	static void testGetDataWithNoRecordId() {
		Test.startTest();
		LightningResponse response = BillingInformationController.getData(null);
		Test.stopTest();

		System.assertEquals(null, response.body, 'Body is expected');
		System.assertNotEquals(null, response.message, 'message is expected');
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant, 'variant is required');
	}
	@IsTest
	static void testGetDataFromBasket() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opportunity.id, csbb__Account__c = account.Id);
		insert basket;

		Test.startTest();
		LightningResponse response = BillingInformationController.getData(basket.Id);
		Test.stopTest();

		System.assertNotEquals(null, response.body, 'Body is expected');
		System.assertEquals(null, response.message, 'message is expected');
	}

	@IsTest
	static void testSaveDataFromBasket() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opportunity.id, csbb__Account__c = account.Id);
		insert basket;

		List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>{
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id),
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id)
		};
		insert configs;

		Test.startTest();

		LightningResponse response = BillingInformationController.save(
			new BillingInformationController.SaveBillingInformation(
				basket.Id,
				opportunity.BAN__c,
				new Map<String, Object>{ 'BAN_Name__c' => 'test' },
				new Map<String, Object>{ 'Billing_Arrangement_Alias__c' => 'test' },
				opportunity.Financial_Account__c,
				opportunity.Billing_Arrangement__c
			)
		);

		Test.stopTest();

		System.assertEquals(null, response.body, 'Body is expected');
		System.assertNotEquals(null, response.message, 'message is expected');
		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant, 'variant is required');

		basket = [
			SELECT
				Id,
				csbb__Account__c,
				cscfga__Opportunity__r.BAN__c,
				cscfga__Opportunity__r.BAN__r.Name,
				cscfga__Opportunity__r.BAN__r.Account__c,
				cscfga__Opportunity__r.Financial_Account__c,
				cscfga__Opportunity__r.Financial_Account__r.Name,
				cscfga__Opportunity__r.Billing_Arrangement__c,
				cscfga__Opportunity__r.Billing_Arrangement__r.Name
			FROM cscfga__Product_Basket__c
			WHERE Id = :basket.Id
		];
		System.assertEquals(opportunity.BAN__c, basket.cscfga__Opportunity__r.BAN__c, 'Ban must match');
		System.assertEquals(opportunity.Financial_Account__c, basket.cscfga__Opportunity__r.Financial_Account__c, 'FA must match');
		System.assertEquals(opportunity.Billing_Arrangement__c, basket.cscfga__Opportunity__r.Billing_Arrangement__c, 'BAR must match');

		List<cscfga__Product_Configuration__c> productConfigurations = [
			SELECT Id, BAN_Information__c, Financial_Account__c, Billing_Arrangement__c
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c = :basket.Id
		];
		System.assertEquals(2, productConfigurations.size(), 'we expect 2 pcs');

		for (cscfga__Product_Configuration__c pc : productConfigurations) {
			System.assertEquals(opportunity.BAN__c, pc.BAN_Information__c, 'Ban must match');
			System.assertEquals(opportunity.Financial_Account__c, pc.Financial_Account__c, 'FA must match');
			System.assertEquals(opportunity.Billing_Arrangement__c, pc.Billing_Arrangement__c, 'BAR must match');
		}
	}

	@IsTest
	static void testGetDataFromOrder() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Contact contact = TestUtils.createContact(account);
		Ban__c ban = TestUtils.createBan(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);

		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		csord__Order__c order = new csord__Order__c(
			Name = 'Order 1',
			csord__Identification__c = 'ID_4568978',
			csord__Account__c = account.Id,
			BAN_Information__c = ban.Id,
			Financial_Account__c = fa.Id,
			Billing_Arrangement__c = bar.Id
		);
		insert order;

		Test.startTest();
		LightningResponse response = BillingInformationController.getData(order.Id);
		Test.stopTest();

		System.assertNotEquals(null, response.body, 'Body is expected');
		System.assertEquals(null, response.message, 'message is expected');
	}

	@IsTest
	static void testSaveDataFromOrder() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Contact contact = TestUtils.createContact(account);
		Ban__c ban = TestUtils.createBan(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);

		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		csord__Order__c order = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978', csord__Account__c = account.Id);
		insert order;

		Test.startTest();

		LightningResponse response = BillingInformationController.save(
			new BillingInformationController.SaveBillingInformation(order.Id, ban.Id, null, null, fa.Id, bar.Id)
		);

		Test.stopTest();

		System.assertEquals(null, response.body, 'Body is expected');
		System.assertNotEquals(null, response.message, 'message is expected');
		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant, 'variant is required');

		order = [SELECT Id, BAN_Information__c, Financial_Account__c, Billing_Arrangement__c FROM csord__Order__c WHERE Id = :order.Id];
		System.assertEquals(ban.Id, order.BAN_Information__c, 'Ban must match');
		System.assertEquals(fa.Id, order.Financial_Account__c, 'FA must match');
		System.assertEquals(bar.Id, order.Billing_Arrangement__c, 'BAR must match');
	}

	@IsTest
	static void testGetDataFromOpportunity() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;

		Test.startTest();

		LightningResponse response = BillingInformationController.getData(opportunity.Id);

		Test.stopTest();

		System.assertNotEquals(null, response.body, 'Body is expected');
		System.assertEquals(null, response.message, 'message is expected');
	}

	@IsTest
	static void testSaveDataFromOpportunity() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;

		Test.startTest();

		LightningResponse response = BillingInformationController.save(
			new BillingInformationController.SaveBillingInformation(
				opportunity.Id,
				opportunity.ban__c,
				new Map<String, Object>{ 'BAN_Name__c' => 'test' },
				new Map<String, Object>{ 'Billing_Arrangement_Alias__c' => 'test' },
				opportunity.Financial_Account__c,
				opportunity.Billing_Arrangement__c
			)
		);

		Test.stopTest();

		System.assertEquals(null, response.body, 'Body is expected');
		System.assertNotEquals(null, response.message, 'message is expected');
		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant, 'variant is required');

		opportunity = [SELECT Id, AccountId, BAN__c, Financial_Account__c, Billing_Arrangement__c FROM Opportunity WHERE Id = :opportunity.Id];
		System.assertNotEquals(null, opportunity.BAN__c, 'Ban must match');
		System.assertNotEquals(null, opportunity.Financial_Account__c, 'FA must match');
		System.assertNotEquals(null, opportunity.Billing_Arrangement__c, 'BAR must match');
	}

	@IsTest
	public static void testGetBanForAccountId() {
		User owner = GeneralUtils.currentUser;
		Account account = TestUtils.createAccount(owner);
		TestUtils.createBan(account);

		List<Ban__c> bans = BillingInformationController.getBanForAccountId(account.Id);

		System.assertEquals(bans.size(), 1, 'Controller return one BAN!');
	}

	@IsTest
	public static void testGetFAForBanId() {
		User owner = GeneralUtils.currentUser;
		Account account = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(account);
		Contact contact = TestUtils.createContact(account);
		TestUtils.createFinancialAccount(ban, contact.Id);

		List<Financial_Account__c> financialAccounts = BillingInformationController.getFAForBanId(ban.Id);

		System.assertEquals(financialAccounts.size(), 1, 'Controller return one Financial Account!');
	}

	@IsTest
	public static void testGetBillingArrangementForFinancialAccountId() {
		User owner = GeneralUtils.currentUser;
		Account account = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(account);
		Contact contact = TestUtils.createContact(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);

		TestUtils.createBillingArrangement(fa);

		List<Billing_Arrangement__c> bars = BillingInformationController.getBillingArrangementForFinancialAccountId(fa.Id);

		System.assertEquals(bars.size(), 1, 'Controller return one Billing Arrangement!');
	}

	@IsTest
	public static void testGetBillingArrangementByFinancialAccountId() {
		User owner = GeneralUtils.currentUser;
		Account account = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(account);
		Contact contact = TestUtils.createContact(account);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, contact.Id);

		TestUtils.createBillingArrangement(fa);

		List<Billing_Arrangement__c> bars = BillingInformationController.getBillingArrangementForBanId(ban.Id);

		System.assertEquals(bars.size(), 1, 'Controller return one Billing Arrangement!');
	}

	@IsTest
	public static void testGetActivePicklistValues() {
		Map<String, String> piclistValues = BillingInformationController.getActivePicklistValues('Ban__c.Unify_Customer_SubType__c');
		System.assertNotEquals(1, piclistValues.size(), 'Map should have more than 1 value');
	}

	@IsTest
	public static void testGetDependentPicklistValues() {
		Map<String, List<String>> piclistValues = BillingInformationController.getDependentPicklistValues('Ban__c.Unify_Customer_SubType__c');
		System.assertNotEquals(1, piclistValues.size(), 'Map should have more than 1 value');
	}
}
