/**
 * Created by miaaugustinovic on 09/04/2021.
 */
@isTest
public with sharing class CustomButtonSolManTest {

    private static testMethod void test() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs (simpleUser) {
            Test.startTest();
            No_Triggers__c notriggers = new No_Triggers__c();
            notriggers.SetupOwnerId = simpleUser.Id;
            notriggers.Flag__c = true;
            insert notriggers;

            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;

            Opportunity opp = CS_DataTest.createOpportunity(tmpAcc, 'Contract opportunity',simpleUser.id);
            insert opp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'OneMobile Basket');
            basket.Name = 'OneMobile Basket';
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = tmpAcc.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 0;
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2022, 02, 02);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2022, 02, 02);
            insert basket;

            cscfga__Product_Definition__c oneMobDef = CS_DataTest.createProductDefinition('OneMobile');
            oneMobDef.Product_Type__c = 'Mobile';

            cscfga__Product_Definition__c oneMobSolDef = CS_DataTest.createProductDefinition('OneMobile Solution');
            oneMobSolDef.Product_Type__c = 'Mobile';

            List<cscfga__Product_Definition__c> prodDefList = new List<cscfga__Product_Definition__c>{oneMobDef,oneMobSolDef};
            insert prodDefList;

            csutil__JSON_Data__c schemaMobSol = new csutil__JSON_Data__c();
            schemaMobSol.Name = 'Mobile Solution';
            schemaMobSol.csutil__value__c = '{"attributes":[{"type":"String","name":"GUID"},{"type":"String","name":"SolutionId"},{"required":false,"showInUI":true,"readOnly":false,"name":"SolutionName","other":"__ConfigName__","label":"Solution Name","options":[],"type":"String","customCSSClasses":"","referencedAttributes":""},{"required":true,"showInUI":true,"readOnly":false,"name":"ContractDuration","label":"Contract Duration","options":[{"value":"1","label":"1"},{"value":"12","label":"12"},{"value":"24","label":"24"},{"value":"36","label":"36"}],"type":"Picklist","customCSSClasses":"","referencedAttributes":""},{"required":false,"showInUI":false,"readOnly":false,"name":"isMainComponent","label":"isMainComponent","value":true,"options":[],"type":"Boolean","customCSSClasses":"","referencedAttributes":""},{"required":false,"showInUI":false,"readOnly":false,"name":"Today","label":"Today","type":"String","helpText":" Field used to capture the current date as a string. Value set using JS.","customCSSClasses":"","referencedAttributes":""},{"required":false,"showInUI":false,"readOnly":false,"name":"SalesChannel","label":"Sales Channel","type":"String","customCSSClasses":"","referencedAttributes":""}],"attributeDefinitions":[],"visible":true,"disabled":false,"name":"Mobile Solution","buttonLabel":"Mobile"}';
            insert schemaMobSol;

            cssdm__Solution_Definition__c solDef = new cssdm__Solution_Definition__c();
            solDef.Name = 'Mobile';
            solDef.cssdm__component_type__c = 'Mobile Solution';
            solDef.cssdm__create_pcr__c = false;
            solDef.cssdm__max__c = 1;
            solDef.cssdm__min__c = 1;
            solDef.cssdm__product_definition__c = oneMobSolDef.Id;
            solDef.cssdm__schema__c = schemaMobSol.Id;
            solDef.cssdm__type__c = 'Main';
            insert solDef;

            csord__Solution__c solution = new csord__Solution__c();
            solution.csord__Status__c = 'Completed';
            solution.cssdm__product_basket__c = basket.Id;
            solution.Name = 'OneMobile Solution';
            solution.cssdm__solution_definition__c = solDef.Id;
            solution.csord__Identification__c = 'Solution Console 2021-04-09 09:04:00';
            insert solution;

            cscfga__Product_Configuration__c pcOneMobileSol = CS_DataTest.createProductConfiguration(oneMobSolDef.Id, 'OneMobile Solution',basket.Id);
            pcOneMobileSol.cscfga__Configuration_Status__c = 'Valid';
            pcOneMobileSol.cscfga__Contract_Term_Period__c = 12;
            pcOneMobileSol.cscfga__Quantity__c = 1;

            cscfga__Product_Configuration__c pcOneMobile = CS_DataTest.createProductConfiguration(oneMobDef.Id, 'OneMobile',basket.Id);
            pcOneMobile.cscfga__Contract_Term__c = 24;
            pcOneMobile.cscfga__total_one_off_charge__c = 0;
            pcOneMobile.cscfga__Total_Price__c = 288;
            pcOneMobile.cscfga__Parent_Configuration__c = pcOneMobileSol.Id;
            pcOneMobile.cscfga__Contract_Term_Period__c = 12;
            pcOneMobile.cscfga__Configuration_Status__c = 'Valid';
            pcOneMobile.cscfga__Quantity__c = 1;
            pcOneMobile.cscfga__total_recurring_charge__c = 24;
            pcOneMobile.RC_Cost__c = 10;
            pcOneMobile.cscfga__one_off_charge_product_discount_value__c = 0;
            pcOneMobile.cscfga__recurring_charge_product_discount_value__c = 0;
            pcOneMobile.Mobile_Scenario__c = 'OneMobile4';
            pcOneMobile.cssdm__solution_association__c = solution.Id;

            List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>{pcOneMobileSol, pcOneMobile};
            insert pcList;

            csbb__product_configuration_request__c pcrTmp = CS_DataTest.createPCR(pcOneMobile);
            insert pcrTmp;

            CustomButtonSolutionManagement solutionManagement = new CustomButtonSolutionManagement();
            solutionManagement.performAction(basket.Id, '[]');

            solutionManagement.performAction(basket.Id, '["'+ pcrTmp.Id+ '"]');

            Test.stopTest();
        }
    }
}