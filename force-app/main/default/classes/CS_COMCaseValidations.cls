public with sharing class CS_COMCaseValidations {

    private static final String installSolutionRecordType = 'CS_COM_Install_Solution';
    private static final String missingAttachmentErrorMessageForSolution = 'Solution should have at least one Attachment!';
    private Map<Id,Case> oldCaseMap = new Map<Id,Case>();
    private Map<Id,Case> newCaseMap = new Map<Id,Case>();

    public CS_COMCaseValidations(Map<Id,Case> oldMap, Map<Id,Case> newMap){
        this.oldCaseMap = oldMap;
        this.newCaseMap = newMap;
    }

    public void run(){
        //checkNumberOfAttachments(this.oldCaseMap,this.newCaseMap);
        //MOVED TO CS_COMCloseCaseController
    }

    //if Solution Installation is successfull then we need to have at least one attachment on Solution level
    /*private void checkNumberOfAttachments(Map<Id,Case> oldCaseMap, Map<Id,Case> newCaseMap){
        Map<Id,csord__Solution__c> solutionsWithAttachmentsMap;
        Set<Id> solutionIds = new Set<Id>();
        List<Case> installSolutionCases = new List<Case>();

        for(Id c :newCaseMap.keySet()){
            if(newCaseMap.get(c).Case_Record_Type_Text__c.equals(installSolutionRecordType) &&
                    (newCaseMap.get(c).Status == 'Closed' && oldCaseMap.get(c).Status != 'Closed')){
                solutionIds.add(newCaseMap.get(c).CS_Solution__c);
                installSolutionCases.add(newCaseMap.get(c));
            }
        }

        if(solutionIds.size() > 0){
            solutionsWithAttachmentsMap = new Map<Id,csord__Solution__c>(
                                            [SELECT Id, Implemented__c, (SELECT Id FROM CombinedAttachments)
                                            FROM csord__Solution__c
                                            WHERE Id IN :solutionIds]
            );

            for(Case c :installSolutionCases){
                if(solutionsWithAttachmentsMap.get(c.CS_Solution__c) != null && solutionsWithAttachmentsMap.get(c.CS_Solution__c).Implemented__c && solutionsWithAttachmentsMap.get(c.CS_Solution__c).CombinedAttachments.size() == 0){
                        c.addError(missingAttachmentErrorMessageForSolution);
                }
            }
        }

    }*/
}