@IsTest
private class TestBanTriggerHandler {
	static final String ACCOUNT_NAME_1 = 'Test Account 1';
	static final String ACCOUNT_NAME_2 = 'Test Account 2';
	static final String FIRST_DEALER_CODE = '990000';
	static final String SECOND_DEALER_CODE = '880000';
	static final String PARENT_DEALER_CODE = '999999';

	@TestSetup
	static void makeData() {
		TestUtils.theAdministrator = new User(Id = UserInfo.getUserId());
		User owner = TestUtils.theAdministrator;

		TestUtils.autoCommit = false;

		// Create Dealer Data

		System.runAs(owner) {
			// Dealer Accounts
			Map<String, Account> dealerAccounts = createDealerAccounts(
				new List<String>{ FIRST_DEALER_CODE, SECOND_DEALER_CODE, PARENT_DEALER_CODE }
			);

			// Dealer Contacts
			Map<String, Contact> dealerContacts = createDealerContacts(dealerAccounts);

			// Dealer Users
			Map<String, User> dealerUsers = createDealerUsers(dealerAccounts, dealerContacts);

			// Dealer Infos
			Dealer_Information__c firstDealerInfo = TestUtils.createDealerInformation(
				dealerContacts.get(FIRST_DEALER_CODE).Id,
				FIRST_DEALER_CODE,
				PARENT_DEALER_CODE
			);
			firstDealerInfo.Name = FIRST_DEALER_CODE;
			Dealer_Information__c secondDealerInfo = TestUtils.createDealerInformation(
				dealerContacts.get(SECOND_DEALER_CODE).Id,
				SECOND_DEALER_CODE,
				SECOND_DEALER_CODE
			);
			secondDealerInfo.Name = SECOND_DEALER_CODE;
			Dealer_Information__c parentDealerInfo = TestUtils.createDealerInformation(
				dealerContacts.get(PARENT_DEALER_CODE).Id,
				PARENT_DEALER_CODE,
				PARENT_DEALER_CODE
			);
			parentDealerInfo.Name = PARENT_DEALER_CODE;
			insert new List<Dealer_Information__c>{
				firstDealerInfo,
				secondDealerInfo,
				parentDealerInfo
			};

			// Create Accounts
			Account acc1 = TestUtils.createAccount(owner);
			acc1.Name = ACCOUNT_NAME_1;
			acc1.Fixed_Dealer__c = firstDealerInfo.Id;

			Account acc2 = TestUtils.createAccount(owner);
			acc2.Name = ACCOUNT_NAME_2;
			acc2.Fixed_Dealer__c = secondDealerInfo.Id;

			List<Account> accs = new List<Account>{ acc1, acc2 };
			insert accs;
		}
	}

	static Map<String, Account> createDealerAccounts(List<String> dealerCodes) {
		Map<String, Account> result = new Map<String, Account>();
		for (String dealerCode : dealerCodes) {
			Account dealerAcc = TestUtils.createPartnerAccount();
			dealerAcc.Name = dealerCode;
			dealerAcc.Dealer_code__c = dealerCode;
			dealerAcc.BOPCode__c = dealerCode.left(3);
			dealerAcc.Type = 'Dealer';
			dealerAcc.BAN_Number__c = dealerCode;
			result.put(dealerCode, dealerAcc);
		}
		insert result.values();
		return result;
	}

	static Map<String, Contact> createDealerContacts(Map<String, Account> dealerAccounts) {
		Map<String, Contact> result = new Map<String, Contact>();
		for (String dealerCode : dealerAccounts.keySet()) {
			Account dealerAcc = dealerAccounts.get(dealerCode);
			Contact dealerCon = TestUtils.createContact(dealerAcc);
			dealerCon.Email = dealerCode + '@vfztest.com';
			result.put(dealerCode, dealerCon);
		}
		insert result.values();
		return result;
	}

	static Map<String, User> createDealerUsers(
		Map<String, Account> dealerAccounts,
		Map<String, Contact> dealerContacts
	) {
		Map<String, User> result = new Map<String, User>();
		for (String dealerCode : dealerAccounts.keySet()) {
			Account dealerAcc = dealerAccounts.get(dealerCode);
			Contact dealerCon = dealerContacts.get(dealerCode);
			User dealerUser = TestUtils.createPortalUser(dealerAcc);
			dealerUser.Username = dealerCon.Email;
			dealerUser.CommunityNickname = 'TestDealer' + dealerCode;
			dealerUser.Dealercode__c = dealerCode;
			dealerUser.ContactId = dealerCon.Id;
			result.put(dealerCode, dealerUser);
		}
		insert result.values();
		for (String dealerCode : dealerContacts.keySet()) {
			Contact dealerCon = dealerContacts.get(dealerCode);
			User dealerUser = result.get(dealerCode);
			dealerCon.Userid__c = dealerUser.Id;
		}
		update dealerContacts.values();
		return result;
	}

	@IsTest
	static void testAccountBanCount() {
		Account acc = getAccount();

		System.debug(System.Limits.getQueries());

		Test.startTest();
		Ban__c ban = TestUtils.createBan(acc);
		ban.BAN_Status__c = 'Opened';
		update ban;

		acc = getAccount();

		System.assertEquals(1, acc.Active_Ban_Count__c, 'Account BANs should be counted.');

		delete ban;
		Test.stopTest();

		acc = getAccount();

		System.assertEquals(0, acc.Active_Ban_Count__c, 'Account BANs should be counted.');
	}

	@IsTest
	static void testUpdateAccountToCustomer() {
		Account acc = getAccount();

		Test.startTest();
		Ban__c ban = TestUtils.createBan(acc);
		Test.stopTest();

		acc = getAccount();

		System.assertEquals(
			'Customer',
			acc.Type,
			'Account Type should be Customer if it has related BANs.'
		);
	}

	@IsTest
	static void testCreateSharingRules() {
		Account acc = getAccount();

		Test.startTest();
		TestUtils.autoCommit = false;
		Ban__c ban = TestUtils.createBan(acc);
		ban.OwnerId = getUser(FIRST_DEALER_CODE).Id;
		insert ban;
		Test.stopTest();

		// Fixed Owner Share Check
		Group parentDealerGroup = getParentDealerGroup();
		List<Ban__Share> banShares = getBanShares(ban, parentDealerGroup.Id);
		System.assert(banShares.size() > 0, 'BAN shares should be created for Fixed Owner.');

		// Hierarchy Share Check
		User parentDealer = getUser(PARENT_DEALER_CODE);
		banShares = getBanShares(ban, parentDealer.Id);
		System.assert(banShares.size() > 0, 'BAN shares should be created for Dealer Hierarchy.');
	}

	@IsTest
	static void testUpdateSharingRules() {
		Account acc = getAccount();

		Test.startTest();
		Ban__c ban = TestUtils.createBan(acc);
		Account acc2 = getAccount(ACCOUNT_NAME_2);
		ban.Account__c = acc2.Id;
		update ban;
		Test.stopTest();

		// There should be no Shares related to child Dealer
		Group parentDealerGroup = getParentDealerGroup();
		List<Ban__Share> banShares = getBanShares(ban, parentDealerGroup.Id);
		System.assert(banShares.size() == 0, 'BAN shares should not exist for old Fixed Dealer.');

		// There should be Shares related to the parent dealer
		Group dealerGroup = getDealerGroup(SECOND_DEALER_CODE);
		banShares = getBanShares(ban, dealerGroup.Id);
		System.assert(banShares.size() > 0, 'BAN shares should be created for new Fixed Dealer.');
	}

	private static Account getAccount() {
		return getAccount(ACCOUNT_NAME_1);
	}

	private static Account getAccount(String name) {
		return [SELECT Id, Type, Active_Ban_Count__c FROM Account WHERE Name = :name];
	}

	private static User getUser(String dealerCode) {
		String email = dealerCode + '@vfztest.com';
		return [SELECT Id, Name, UserRole.DeveloperName FROM User WHERE Username = :email];
	}

	private static Group getDealerGroup(String dealerCode) {
		User dealerUser = getUser(dealerCode);
		return [
			SELECT Id, Name
			FROM Group
			WHERE
				DeveloperName = :dealerUser.UserRole.DeveloperName
				AND Type = 'RoleAndSubordinates'
		];
	}

	private static Group getParentDealerGroup() {
		String groupName = 'PMs:' + PARENT_DEALER_CODE;
		return [SELECT Id, Name FROM Group WHERE Name = :groupName];
	}

	private static Dealer_Information__c getDealerInfo(String dealerCode) {
		return [SELECT Id FROM Dealer_Information__c WHERE Dealer_Code__c = :dealerCode];
	}

	private static List<Ban__Share> getBanShares(Ban__c ban, Id userOrGroupId) {
		return [
			SELECT Id, UserOrGroupId, UserOrGroup.Name, RowCause
			FROM Ban__Share
			WHERE
				ParentId = :ban.Id
				AND RowCause = :Schema.Ban__share.RowCause.Dealer_Hierarchy__c
				AND UserOrGroupId = :userOrGroupId
		];
	}
}