/**
 * @description:    Credit note approval manager class
 * @testClass:      TestCreditNoteApprovalManager
 **/
public without sharing class CreditNoteApprovalManager {
	private static final String CREDIT_NOTE_STATUS_APPROVED = Constants.CREDIT_NOTE_STATUS_APPROVED;
	private static final Id CREDIT_NOTE_RECORD_TYPE_ID_FZIGGO = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
		Credit_Note__c.SObjectType,
		Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_FZIGGO
	);

	private CreditNoteApprovalMatrixFilter creditNoteApprovalMatrixFilter;

	/**
	 * @description:    Constructor - initializes approval matrix filter
	 **/
	public CreditNoteApprovalManager() {
		creditNoteApprovalMatrixFilter = new CreditNoteApprovalMatrixFilter(CreditNoteApprovalMatrixAccessor.getAllCreditNoteApprovalMatrices());
	}

	/**
	 * @description:    Filters records before insert
	 * @param           creditNotesBeforeInsert - list of Credit_Note__c records before insert
	 * @return          List<Credit_Note__c> - filtered Credit_Note__c records
	 **/
	public List<Credit_Note__c> filterRecordsBeforeInsert(List<Credit_Note__c> creditNotesBeforeInsert) {
		List<Credit_Note__c> filteredCreditNotesForApprovalFieldsChangeBeforeInsert = new List<Credit_Note__c>();

		for (Credit_Note__c creditNoteBeforeInsert : creditNotesBeforeInsert) {
			if (creditNoteBeforeInsert.RecordTypeId != CREDIT_NOTE_RECORD_TYPE_ID_FZIGGO) {
				filteredCreditNotesForApprovalFieldsChangeBeforeInsert.add(creditNoteBeforeInsert);
			}
		}

		return filteredCreditNotesForApprovalFieldsChangeBeforeInsert;
	}

	/**
	 * @description:    Filters records before update
	 * @param           creditNotesBeforeUpdate - list of Credit_Note__c records before update
	 * @param			oldCreditNotes - map of old Credit_Note__c records per id
	 * @return          List<Credit_Note__c> - filtered Credit_Note__c records
	 **/
	public List<Credit_Note__c> filterRecordsBeforeUpdate(List<Credit_Note__c> creditNotesBeforeUpdate, Map<Id, Credit_Note__c> oldCreditNotes) {
		List<Credit_Note__c> filteredCreditNotesForApprovalFieldsChangeBeforeUpdate = new List<Credit_Note__c>();

		for (Credit_Note__c creditNoteBeforeUpdate : creditNotesBeforeUpdate) {
			Credit_Note__c oldCreditNote = oldCreditNotes.get(creditNoteBeforeUpdate.Id);

			if (
				creditNoteBeforeUpdate.RecordTypeId != CREDIT_NOTE_RECORD_TYPE_ID_FZIGGO &&
				creditNoteBeforeUpdate.Is_Matrix_Approval_Process__c &&
				!creditNoteBeforeUpdate.Is_Final_Approver__c &&
				this.isCreditNoteApprovedInThisStep(creditNoteBeforeUpdate, oldCreditNote)
			) {
				filteredCreditNotesForApprovalFieldsChangeBeforeUpdate.add(creditNoteBeforeUpdate);
			}
		}

		return filteredCreditNotesForApprovalFieldsChangeBeforeUpdate;
	}

	/**
	 * @description:    Filters records after update
	 * @param           creditNotesBeforeUpdate - list of Credit_Note__c records after update
	 * @param			oldCreditNotes - map of old Credit_Note__c records per id
	 * @return          List<Credit_Note__c> - filtered Credit_Note__c records
	 **/
	public List<Credit_Note__c> filterRecordsAfterUpdate(List<Credit_Note__c> creditNotesBeforeUpdate, Map<Id, Credit_Note__c> oldCreditNotes) {
		List<Credit_Note__c> filteredCreditNotesForAutomaticApprovalSubmission = new List<Credit_Note__c>();

		for (Credit_Note__c creditNote : creditNotesBeforeUpdate) {
			Credit_Note__c oldCreditNote = oldCreditNotes.get(creditNote.Id);

			if (
				oldCreditNote != null &&
				creditNote.RecordTypeId != CREDIT_NOTE_RECORD_TYPE_ID_FZIGGO &&
				creditNote.Is_Matrix_Approval_Process__c &&
				(!creditNote.Is_Final_Approver__c || (creditNote.Is_Final_Approver__c && !oldCreditNote.Is_Final_Approver__c)) &&
				this.isCreditNoteApprovedInThisStep(creditNote, oldCreditNote)
			) {
				filteredCreditNotesForAutomaticApprovalSubmission.add(creditNote);
			}
		}

		return filteredCreditNotesForAutomaticApprovalSubmission;
	}

	/**
	 * @description:    Maps approval fields from matrix before insert
	 * @param           filteredCreditNotesForApprovalFieldsChangeBeforeInsert
	 * 					- list of Credit_Note__c records for approval fields change
	 * 					before insert
	 **/
	public void mapApprovalFieldsFromMatrixBeforeInsert(List<Credit_Note__c> filteredCreditNotesForApprovalFieldsChangeBeforeInsert) {
		for (Credit_Note__c creditNote : filteredCreditNotesForApprovalFieldsChangeBeforeInsert) {
			Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredCreditNoteApprovalMatricesPerLevel = this.getCreditNoteApprovalMatricesPerLevel(
				creditNote
			);

			if (!filteredCreditNoteApprovalMatricesPerLevel.isEmpty()) {
				new CreditNoteApprovalFieldMapper(creditNote, filteredCreditNoteApprovalMatricesPerLevel)
					.resetCreditNoteCurrentApprovalFields()
					.resetCreditNoteNextApprovalFields()
					.mapCurrentApproverMatrixValues()
					.mapNextApproverMatrixValues();
			}
		}
	}

	/**
	 * @description:    Maps approval fields from matrix before update
	 * @param           filteredCreditNotesForApprovalFieldsChangeBeforeUpdate
	 * 					- list of Credit_Note__c records for approval fields change
	 * 					before update
	 **/
	public void mapApprovalFieldsFromMatrixBeforeUpdate(List<Credit_Note__c> filteredCreditNotesForApprovalFieldsChangeBeforeUpdate) {
		for (Credit_Note__c creditNote : filteredCreditNotesForApprovalFieldsChangeBeforeUpdate) {
			if (new CreditNoteApprovalValidator(creditNote).validate()) {
				Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredCreditNoteApprovalMatricesPerLevel = this.getCreditNoteApprovalMatricesPerLevel(
					creditNote
				);

				new CreditNoteApprovalFieldMapper(creditNote, filteredCreditNoteApprovalMatricesPerLevel)
					.copyNextApproverToCurrentApprover()
					.resetCreditNoteNextApprovalFields()
					.mapNextApproverMatrixValues();
			}
		}
	}

	/**
	 * @description:    Resubmits credit note for approval
	 * @param           filteredCreditNotesForAutomaticApprovalSubmission
	 * 					- list of Credit_Note__c records for approval process
	 * 					resubmission
	 **/
	public void reSubmitMatrixRecordsForApproval(List<Credit_Note__c> filteredCreditNotesForAutomaticApprovalSubmission) {
		for (Credit_Note__c creditNote : filteredCreditNotesForAutomaticApprovalSubmission) {
			Approval.ProcessSubmitRequest processSubmitRequest = new Approval.ProcessSubmitRequest();
			processSubmitRequest.setObjectId(creditNote.id);

			Approval.process(processSubmitRequest);
		}
	}

	/**
	 * @description:    Retrieves credit note approval matrices per level
	 * @param           creditNote - Credit_Note__c record
	 * @return          Map<Decimal, List<CreditNote_Approval_Matrix__c>> - credit note
	 * 					approval matrices per level
	 **/
	private Map<Decimal, List<CreditNote_Approval_Matrix__c>> getCreditNoteApprovalMatricesPerLevel(Credit_Note__c creditNote) {
		return this.creditNoteApprovalMatrixFilter
			.withCreditNoteRecordType(RecordTypeAccessor.getRecordTypeDeveloperNameForId(creditNote.RecordTypeId))
			.withCreditNoteCreditType(creditNote.Credit_Type__c)
			.withCreditNoteSubscription(creditNote.Subscription__c)
			.withAmount(creditNote.Credit_Amount__c)
			.getFilteredApprovalMatricesPerLevel();
	}

	/**
	 * @description:    Checks if credit note is approved in this step
	 * @param           currentCreditNote - current Credit_Note__c record
	 * @param           oldCreditNote - old Credit_Note__c record
	 * @return          Boolean - true if credit note is approved in this step
	 * 					, false else
	 **/
	private Boolean isCreditNoteApprovedInThisStep(Credit_Note__c currentCreditNote, Credit_Note__c oldCreditNote) {
		return currentCreditNote.Status__c == CREDIT_NOTE_STATUS_APPROVED && currentCreditNote.Status__c != oldCreditNote.Status__c;
	}
}
