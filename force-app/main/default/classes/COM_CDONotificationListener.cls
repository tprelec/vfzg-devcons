@RestResource(urlMapping='/notificationListener')
global with sharing class COM_CDONotificationListener {
	@HttpPost
	global static void processNotification() {
		RestRequest req = RestContext.request;
		String reqBody = req.requestbody.tostring();
		reqBody = COM_CDOServiceCharacteristicHelper.modifyRequestBody(reqBody);

		try {
			COM_CDO_DataPropagation dataPropagation = (COM_CDO_DataPropagation) JSON.deserialize(reqBody, COM_CDO_DataPropagation.class);
			processDataPropagation(dataPropagation);
		} catch (Exception ex) {
			try {
				CDOServiceAsyncResponse response = (CDOServiceAsyncResponse) JSON.deserialize(reqBody, CDOServiceAsyncResponse.class);
				processServiceOrder(response.event.serviceOrder, response.event.errorEvents);
			} catch (Exception exx) {
				LoggerService.log(LoggingLevel.ERROR, 'Error in CDO Notification listener processing: ' + exx.getMessage());
			}
		}
	}

	private static void processDataPropagation(COM_CDO_DataPropagation dataPropagationEntity) {
		COM_CDOServiceCharacteristicHelper.parseBIMOCharacteristics(dataPropagationEntity.event.service);
	}

	private static Boolean processServiceOrder(COM_CDO_ServiceOrder serviceOrder, CDOServiceErrorEvents[] errorEvents) {
		Boolean result = false;
		String errorMessage = errorEvents != null ? getErrorMessage(errorEvents) : '';

		Boolean serviceIsTenant = false;
		Boolean serviceDeletion = false;
		Boolean provisionService = false;
		for (COM_CDO_ServiceOrder.OrderItem orderItem : serviceOrder.orderItem) {
			COM_CDO_ServiceOrder.Service service = orderItem.service;
			serviceIsTenant = processGenericTenantService(service, errorMessage);
			if (!serviceIsTenant) {
				serviceDeletion = processBIMODelete(serviceOrder, orderItem, errorMessage);

				if (!serviceDeletion) {
					provisionService = processBIMOProvision(serviceOrder, orderItem, errorMessage);
				}
			}
		}
		result = serviceIsTenant || serviceDeletion || provisionService;
		return result;
	}

	private static Boolean processBIMODelete(COM_CDO_ServiceOrder serviceOrder, COM_CDO_ServiceOrder.OrderItem orderItem, String errorMessage) {
		Boolean result = false;
		COM_CDO_integration_settings__mdt deprovisionSpecificationServiceSettings = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.CDO_DEPROVISION_BIMO_SETTING_NAME
		);

		COM_Integration_setting__mdt deprovisionIntegrationSettings = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.CDO_DEPROVISION_BIMO_SETTING_NAME
		);

		if (
			orderItem.action == COM_CDO_Constants.COM_CDO_ACTION_DELETE &&
			isTypeOfService(deprovisionSpecificationServiceSettings, orderItem.service.serviceSpecification)
		) {
			result = true;
			if (orderItem.service.serviceState == deprovisionIntegrationSettings.Async_success_status__c) {
				processBIMOServiceSuccess(serviceOrder);
			} else {
				processBIMOServiceFailed(serviceOrder, errorMessage);
			}
		}

		return result;
	}

	private static Boolean processBIMOProvision(COM_CDO_ServiceOrder serviceOrder, COM_CDO_ServiceOrder.OrderItem orderItem, String errorMessage) {
		Boolean result = false;

		COM_CDO_integration_settings__mdt provisionSpecificationServiceSettings = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.CDO_PROVISION_BIMO_SETTING_NAME
		);

		COM_Integration_setting__mdt provisionIntegrationServiceSettings = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.CDO_PROVISION_BIMO_SETTING_NAME
		);

		if (isTypeOfService(provisionSpecificationServiceSettings, orderItem.service.serviceSpecification)) {
			if (orderItem.service.serviceState == provisionIntegrationServiceSettings.Async_success_status__c) {
				result = true;
				processBIMOServiceSuccess(serviceOrder);
			} else {
				processBIMOServiceFailed(serviceOrder, errorMessage);
			}
		}
		return result;
	}
	private static void processBIMOServiceSuccess(COM_CDO_ServiceOrder serviceOrder) {
		COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
		updates.deliveryOrderId = null;
		updates.status = COM_Constants.CDO_INTEGRATION_STATUS_COMPLETE;
		updates.errorMessage = '';
		updates.success = true;

		COM_CDOHelper.performOrderUpdatesAfterResponse(updates, serviceOrder);
	}

	private static void processBIMOServiceFailed(COM_CDO_ServiceOrder serviceOrder, String errorMessage) {
		COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
		updates.deliveryOrderId = null;
		updates.status = COM_Constants.CDO_INTEGRATION_STATUS_FAILURE;
		updates.errorMessage = errorMessage;
		updates.success = false;

		COM_CDOHelper.performOrderUpdatesAfterResponse(updates, serviceOrder);
	}

	private static Boolean processGenericTenantService(COM_CDO_ServiceOrder.Service service, String errorMessage) {
		COM_CDO_integration_settings__mdt tenantSpecificationSetting = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);
		COM_Integration_setting__mdt tenantIntegrationSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);
		Boolean result = false;
		if (isTypeOfService(tenantSpecificationSetting, service.serviceSpecification)) {
			result = true;
			if (service.serviceState == tenantIntegrationSetting.Async_success_status__c) {
				genericTenantServiceSuccess(service);
			} else {
				genericTenantServiceFailure(service, errorMessage);
			}
		}
		return result;
	}

	private static void genericTenantServiceSuccess(COM_CDO_ServiceOrder.Service service) {
		String serviceId = service.Id;
		Id accountId = COM_CDOcreateTenant.getAccountIdByUmbrellaService(serviceId);
		COM_CDOcreateTenant.CreateTenantAccountUpdateData accountUpdate = new COM_CDOcreateTenant.CreateTenantAccountUpdateData();
		accountUpdate.accountId = accountId;
		accountUpdate.successful = true;
		accountUpdate.tenantId = serviceId;
		accountUpdate.umbrellaService = service.Id;
		accountUpdate.errorMessage = null;
		COM_CDOcreateTenant.updateAccount(accountUpdate);
	}

	private static void genericTenantServiceFailure(COM_CDO_ServiceOrder.Service service, String errorMessage) {
		String serviceId = service.Id;
		Id accountId = COM_CDOcreateTenant.getAccountIdByUmbrellaService(serviceId);
		COM_CDOcreateTenant.CreateTenantAccountUpdateData accountUpdate = new COM_CDOcreateTenant.CreateTenantAccountUpdateData();
		accountUpdate.accountId = accountId;
		accountUpdate.successful = false;
		accountUpdate.tenantId = null;
		accountUpdate.umbrellaService = service.Id;
		accountUpdate.errorMessage = errorMessage;
		COM_CDOcreateTenant.updateAccount(accountUpdate);
	}

	private static Boolean isTypeOfService(
		COM_CDO_integration_settings__mdt setting,
		COM_CDO_ServiceOrder.ServiceSpecification serviceSpecification
	) {
		Boolean result = false;
		if ((serviceSpecification != null) && (serviceSpecification.id == setting.serviceSpecificationId__c)) {
			result = true;
		}
		return result;
	}
	private static String getErrorMessage(CDOServiceErrorEvents[] errorEvents) {
		String result = '';
		if (errorEvents != null && errorEvents.size() > 0) {
			for (CDOServiceErrorEvents ef : errorEvents) {
				result += ef.message;
			}
		}
		return result;
	}
	global class CDOServiceAsyncResponse {
		public String eventId;
		public String eventTime;
		public String eventType;
		public Event event;
	}

	global class Event {
		public COM_CDO_ServiceOrder serviceOrder;
		public CDOServiceErrorEvents[] errorEvents;
	}

	global class CDOServiceAsyncFailedResponse {
		public String eventId;
		public String eventTime;
		public String eventType;
		public CDOServiceEventFailed event;
	}

	global class CDOServiceEventFailed {
		public COM_CDO_ServiceOrder serviceOrder;
		public CDOServiceErrorEvents[] errorEvents;
	}

	global class CDOServiceErrorEvents {
		public Integer code;
		public String reason;
		public String message;
	}
}
