public with sharing class BanTriggerHandler extends TriggerHandler {
	private List<Ban__c> oldBans;
	private List<Ban__c> newBans;
	private Map<Id, Ban__c> oldBanMap;
	private Map<Id, Ban__c> newBanMap;

	private void init() {
		oldBans = (List<Ban__c>) this.oldList;
		newBans = (List<Ban__c>) this.newList;
		oldBanMap = (Map<Id, Ban__c>) this.oldMap;
		newBanMap = (Map<Id, Ban__c>) this.newMap;
	}

	public override void afterInsert() {
		init();
		updateAccountToCustomer();
		shareBans();
		updateAccountBanCount();
	}

	public override void afterUpdate() {
		init();
		shareBans();
		updateAccountBanCount();
	}

	public override void afterDelete() {
		init();
		updateAccountBanCount();
	}

	// If this is the first ban for an account, possibly change the account type to 'customer'
	private void updateAccountToCustomer() {
		Set<Id> accountIds = GeneralUtils.getIDSetFromList(newBans, 'Account__c');
		AccountService.getInstance().updateAccountsToCustomer(accountIds);
	}

	// Updates BAN count on Parent Account
	private void updateAccountBanCount() {
		List<Ban__c> bans = newBans;
		Map<Id, Ban__c> bansMap = oldBanMap;
		if (newBans == null) {
			bans = oldBans;
			bansMap = newBanMap;
		}
		List<Ban__c> changedBans = GeneralUtils.filterChangedRecords(
			bans,
			bansMap,
			new List<String>{ 'Account__c', 'BAN_Status__c' },
			false
		);
		Set<Id> accountIds = GeneralUtils.getIDSetFromList(changedBans, 'Account__c');

		Map<Id, Account> accountsToUpdate = new Map<Id, Account>();

		AggregateResult[] results = [
			SELECT Account__c accountId, Count(Id) cnt, Max(Account__r.Active_Ban_Count__c) max
			FROM Ban__c
			WHERE Account__c IN :accountIds AND BAN_Status__c = 'Opened'
			GROUP BY Account__c
		];

		for (AggregateResult ar : results) {
			Integer banCount = (Integer) ar.get('cnt');
			Decimal currentBanCount = (Decimal) ar.get('max');

			// remove from set so we can determine left-over accounts later
			Id accId = (Id) ar.get('accountId');
			accountIds.remove(accId);

			if (banCount != currentBanCount) {
				accountsToUpdate.put(
					accId,
					new Account(Id = accId, Active_Ban_Count__c = banCount)
				);
			}
		}

		// if no opened bans remain, set to 0
		for (Id acctID : accountIds) {
			accountsToUpdate.put(acctID, new Account(Id = acctID, Active_Ban_Count__c = 0));
		}

		if (!accountsToUpdate.isEmpty()) {
			update accountsToUpdate.values();
		}
	}

	// Creates new Sharing Rules for inserted and updated BANs
	// BAN is shared to Fixed Owner of the Account
	private void shareBans() {
		BanService banSvc = new BanService(newBans, oldBanMap);
		banSvc.shareBans();
	}

}