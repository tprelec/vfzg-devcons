/*******************************************************************************************************************************************
* File Name     :  LG_SalesforceOneLeadActionsCtrlTest
* Description   :  Test class for LG_SalesforceOneLeadActionsController

* @author       :   Shreyas
* Modification Log
===================================================================================================
* Ver.    Date          Author              Modification
---------------------------------------------------------------------------------------------------
* 1.0     9th-Aug-16    Shreyas             Created the class for release R1.5
********************************************************************************************************************************************/

@isTest
private class LG_SalesforceOneLeadActionsCtrlTest {
	@TestSetup
	static void makeData() {
		TestUtils.createLead();
	}

	@IsTest
	static void followUpLeadTest() {
		//coverage for the vf page: LG_UpdateLeadStatustoContactedFollowUp
		Lead l = [SELECT Id FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];
		Test.startTest();
		ApexPages.StandardController stdController = new ApexPages.StandardController(l);
		PageReference pageRefFollowUp = Page.LG_UpdateLeadStatustoFollowUp;
		Test.setCurrentPage(pageRefFollowUp);
		ApexPages.currentPage().getParameters().put('Id', l.Id);
		LG_SalesforceOneLeadActionsController obj1 = new LG_SalesforceOneLeadActionsController(stdController);
		LG_SalesforceOneLeadActionsController.updateLeadStatusToFollowUp_OnLoadAction();
		LG_SalesforceOneLeadActionsController.updateLeadStatusToFollowUp_Remote(
			l.Id,
			'Follow Up',
			'2021-10-10',
			'10:00',
			'test_description',
			'competitor name test',
			'2021-10-10',
			'068000001',
			'2021-10-10',
			'test@vodazoneziggo.com',
			'true',
			''
		);
		obj1.updateLeadStatusToFollowUp();
		Test.stopTest();
		Lead updatedLead = [SELECT Id, Status FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];
		System.assertEquals('Follow Up', updatedLead.Status, 'Lead status was not properly updated.');
		List<Event> events = [SELECT Id FROM Event WHERE WhoId = :l.Id];
		System.assert(!events.isEmpty(), 'Calendar Event was not created for lead record');
	}

	@IsTest
	static void notAvailableLeadTest() {
		//coverage for the vf page: LG_UpdateLeadStatustoNotAvailable
		Lead l = [SELECT Id FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];
		Test.startTest();
		ApexPages.StandardController stdController = new ApexPages.StandardController(l);

		PageReference pageRefNotAvailable = Page.LG_UpdateLeadStatustoNotAvailable;
		Test.setCurrentPage(pageRefNotAvailable);
		ApexPages.currentPage().getParameters().put('Id', l.Id);
		new LG_SalesforceOneLeadActionsController(stdController);
		//obj1.updateLeadStatusToNotAvailabale(l.Id);
		LG_SalesforceOneLeadActionsController.updateLeadStatusToNotAvailable(l.Id);
		Test.stopTest();
		Lead updatedLead = [SELECT Id, Status FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];
		System.assertEquals('Not Available', updatedLead.Status, 'Lead status was not updated');
		List<Task> tasks = [SELECT Id FROM Task WHERE WhoId = :l.Id];
		System.assert(!tasks.isEmpty(), 'D2D Task was not logged in');
	}

	@IsTest
	static void futureInterestLeadTest() {
		//coverage for the vf page: LG_UpdateLeadStatustoFutureInterest
		Lead l = [SELECT Id FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];
		Test.startTest();
		ApexPages.StandardController stdController = new ApexPages.StandardController(l);

		PageReference pageRefFutureInterest = Page.LG_UpdateLeadStatustoFutureInterest;
		Test.setCurrentPage(pageRefFutureInterest);
		ApexPages.currentPage().getParameters().put('Id', l.Id);
		new LG_SalesforceOneLeadActionsController(stdController);
		//obj1.updateLeadStatusToFutureInterest(l.Id);
		LG_SalesforceOneLeadActionsController.updateLeadStatusToFutureInterest(l.Id);
		Test.stopTest();
		Lead updatedLead = [SELECT Id, Status FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];
		System.assertEquals('Future Interest', updatedLead.Status, 'Lead status was not updated');
	}
	/*
        Name: TestMethod1_SalesforceOneActions
        Purpose: Test method for testing the logic for salesforce1 lead actions
        Argument: none
        Return type: none
     */
	@IsTest
	static void testMethod1SalesforceOneActions() {
		Lead l = [SELECT Id FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];

		ApexPages.StandardController stdController = new ApexPages.StandardController(l);
		LG_SalesforceOneLeadActionsController obj1;

		//coverage for the vf page: LG_UpdateLeadStatustoQualified
		PageReference pageRefQualified = Page.LG_UpdateLeadStatustoQualified;
		Test.setCurrentPage(pageRefQualified);
		ApexPages.currentPage().getParameters().put('Id', l.Id);
		string leadId = l.Id;
		obj1 = new LG_SalesforceOneLeadActionsController(stdController);
		LG_SalesforceOneLeadActionsController.updateLeadStatusToQualified(leadId);

		//coverage for the vf page: LG_UpdateLeadStatustoDisqualified
		PageReference pageRefDisqualified = Page.LG_UpdateLeadStatustoDisqualified;
		Test.setCurrentPage(pageRefDisqualified);
		ApexPages.currentPage().getParameters().put('Id', l.Id);
		obj1 = new LG_SalesforceOneLeadActionsController(stdController);
		LG_SalesforceOneLeadActionsController.updateLeadStatusToDisqualified_OnLoadAction();
		LG_SalesforceOneLeadActionsController.updateLeadStatusToDisqualified_Remote(l.Id, 'Reject', 'Bad data quality', 'CED Incorrect');
		obj1.updateLeadStatusToDisqualified();

		//coverage for the vf page: LG_AcceptLead
		PageReference pageRefAccept = Page.LG_AcceptLead;
		Test.setCurrentPage(pageRefAccept);
		ApexPages.currentPage().getParameters().put('Id', l.Id);
		obj1 = new LG_SalesforceOneLeadActionsController(stdController);
		//obj1.acceptLead(l.Id);
		LG_SalesforceOneLeadActionsController.acceptLead(leadId);

		Lead updatedLead = [SELECT Id, OwnerId FROM Lead WHERE Id = :leadId];
		System.assertEquals(userInfo.getUserId(), updatedLead.OwnerId, 'User was not assigned as Lead Owner');
	}

	/*
        Name: notReachCustomActionTest
        Purpose: Test method for testing the logic for salesforce1 lead action Not Reached: https://jiranl.vodafoneziggo.com/browse/SFLO-324
        Argument: none
        Return type: none
     */
	@IsTest
	static void notReachCustomActionTest() {
		//coverage for the vf page: LG_UpdateLeadStatustoNotReached
		Id leadId = [SELECT Id FROM Lead WHERE LastName = 'Test Lead'][0].Id;
		String resultNotReached = LG_SalesforceOneLeadActionsController.updateLeadStatusToNotReached(leadId);
		System.assertEquals('Success', resultNotReached, 'Operation failed');
		Lead notReachedLead = [SELECT Id, Status, Reject_Reason__c, Reject_Sub_Reason__c FROM Lead WHERE Id = :leadId];
		System.assertEquals('Not Reached', notReachedLead.Status, 'Lead status was not updated to Not Reached!');
		System.assertEquals('Not reached', notReachedLead.Reject_Reason__c, 'Lead Reject Reason was not updated!');
		System.assertEquals('1st Not Reached', notReachedLead.Reject_Sub_Reason__c, 'Lead Reject Sub-Reason was not updated!');
	}

	/*
        Name: secondNotReachedCustomActionTest
        Purpose: Test method for testing the logic for salesforce1 lead action Not Reached: https://jiranl.vodafoneziggo.com/browse/SFLO-324
        Argument: none
        Return type: none
    */
	@IsTest
	static void secondNotReachedCustomActionTest() {
		//coverage for the vf page: LG_UpdateLeadStatustoNotReached
		Lead l = [SELECT Id FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];
		l.Status = 'Not Reached';
		l.Reject_Reason__c = 'Not reached';
		l.Reject_Sub_Reason__c = '1st Not Reached';
		update l;
		Test.startTest();
		String resultNotReached = LG_SalesforceOneLeadActionsController.updateLeadStatusToNotReached(l.Id);
		Test.stopTest();
		System.assertEquals('Success', resultNotReached, 'Operation failed');
		Lead secondNotReachedLead = [SELECT Id, Reject_Sub_Reason__c FROM Lead WHERE Id = :l.Id];
		System.assertEquals('2nd Not Reached', secondNotReachedLead.Reject_Sub_Reason__c, 'Lead Reject Sub-Reason was not updated!');
	}

	/*
        Name: thirdNotReachCutomActionTest
        Purpose: Test method for testing the logic for salesforce1 lead action Not Reached: https://jiranl.vodafoneziggo.com/browse/SFLO-324
        Argument: none
        Return type: none
    */
	@IsTest
	static void thirdNotReachCutomActionTest() {
		//coverage for the vf page: LG_UpdateLeadStatustoNotReached
		Lead l = [SELECT Id FROM Lead WHERE LastName = 'Test Lead' LIMIT 1];
		l.Status = 'Not Reached';
		l.Reject_Reason__c = 'Not reached';
		l.Reject_Sub_Reason__c = '2nd Not Reached';
		update l;
		Test.startTest();
		String resultNotReached = LG_SalesforceOneLeadActionsController.updateLeadStatusToNotReached(l.Id);
		Test.stopTest();
		System.assertEquals('Success', resultNotReached, 'Operation failed');
		Lead thirdNotReachedLead = [SELECT Id, Reject_Sub_Reason__c FROM Lead WHERE Id = :l.Id];
		System.assertEquals('3rd Not Reached', thirdNotReachedLead.Reject_Sub_Reason__c, 'Lead Reject Sub-Reason was not updated!');
	}
}
