@isTest
public with sharing class TestOrderEntryAddOnTriggerHandler {
	@isTest
	public static void testAutoNumbering() {
		ExternalIDNumber__c custSetting = new ExternalIDNumber__c(
			Name = 'OE_Add_On',
			External_Number__c = 1,
			Object_Prefix__c = 'OEAO-49-',
			blocked__c = false,
			runningOrg__c = '00D1l0000008i1vEAA'
		);
		insert custSetting;

		String orgId = UserInfo.getOrganizationId();

		Test.startTest();
		OrderEntryTestFactory.createOEAddOn('Internet Additional', '1111', true);
		Test.stopTest();

		OE_Add_On__c result = [SELECT Id, External_Id__c FROM OE_Add_On__c];
		if (custSetting.runningOrg__c == orgId) {
			System.assertEquals(
				'OEAO-49-000002',
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		} else {
			System.assertEquals(
				null,
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		}
	}
}