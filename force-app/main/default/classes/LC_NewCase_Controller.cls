public with sharing class LC_NewCase_Controller {
	public static Map<Id, String> recordtypemap;

	@AuraEnabled
	public static List<LC_NewCase_Controller.RecordTypeRadio> fetchRecordTypeValues(
		String objectName
	) {
		List<LC_NewCase_Controller.RecordTypeRadio> returnList = new List<LC_NewCase_Controller.RecordTypeRadio>();
		List<Schema.RecordTypeInfo> recordtypes = Schema.getGlobalDescribe()
			.get(objectName)
			.getDescribe()
			.getRecordTypeInfos();
		recordtypemap = new Map<Id, String>();
		for (RecordTypeInfo rt : recordtypes) {
			if (LC_NewCase_Controller.isRecordTypeAvailable(rt)) {
				if (rt.getName() != 'Master' && rt.getName().trim() != '') {
					LC_NewCase_Controller.RecordTypeRadio rtRadio = new LC_NewCase_Controller.RecordTypeRadio();
					rtRadio.label = rt.getName();
					rtRadio.value = rt.getRecordTypeId();
					returnList.add(rtRadio);
				}
			}
		}
		return returnList;
	}

	public static Boolean isRecordTypeAvailable(RecordTypeInfo info) {
		return info.isMaster() && !info.isDefaultRecordTypeMapping() ? false : info.isAvailable();
	}

	@AuraEnabled
	public static Opportunity fetchOpportunityDetails(String oppId) {
		Opportunity opp;
		if (!String.isEmpty(oppId)) {
			opp = [
				SELECT AccountId, Contract_VF__c, Primary_Basket__c, Primary_Quote__c
				FROM Opportunity
				WHERE Id = :oppId
			];
		}
		return opp;
	}

	@AuraEnabled
	public static Credit_Note__c fetchCreditNoteDetails(String cnId) {
		Credit_Note__c cn;
		if (!String.isEmpty(cnId)) {
			cn = [SELECT Account__c FROM Credit_Note__c WHERE Id = :cnId];
		}
		return cn;
	}

	@AuraEnabled
	public static HBO__c fetchHBODetails(String hboId) {
		HBO__c hbo;
		if (!String.isEmpty(HBOId)) {
			hbo = [SELECT hbo_account__c FROM HBO__c WHERE Id = :hboId];
		}
		return hbo;
	}

	@AuraEnabled
	public static Order__c fetchOrderDetails(String orderId) {
		Order__c order;
		if (!String.isEmpty(orderId)) {
			order = [
				SELECT VF_Contract__c, Account__c, VF_Contract__r.Opportunity__r.Id
				FROM Order__c
				WHERE Id = :orderId
			];
		}
		return order;
	}

	@AuraEnabled
	public static VF_Contract__c fetchContractVFDetails(String contractId) {
		VF_Contract__c contract;
		if (!String.isEmpty(contractId)) {
			contract = [
				SELECT Account__c, Opportunity__c
				FROM VF_Contract__c
				WHERE Id = :contractId
			];
		}
		return contract;
	}

	@AuraEnabled
	public static cscfga__Product_Basket__c basket(String basketId) {
		cscfga__Product_Basket__c basket;
		if (!String.isEmpty(basketId)) {
			basket = [
				SELECT cscfga__Opportunity__c, csbb__Account__c
				FROM cscfga__Product_Basket__c
				WHERE Id = :basketId
			];
		}
		return basket;
	}

	@AuraEnabled
	public static User validateChatterUser(){
		return [SELECT User_License__c FROM User WHERE Id = :Userinfo.getUserId()];
		
	}

	public class RecordTypeRadio {
		@AuraEnabled
		public String label;
		@AuraEnabled
		public String value;
	}
}