@IsTest
public with sharing class TestAddPbxController {
	@TestSetup
	static void makeData() {
		Account a = TestUtils.createAccount(null);
		Site__c s = TestUtils.createSite(a);
		TestUtils.createPBXTypes(1);
	}

	@IsTest
	public static void testGetPbx() {
		Test.startTest();
		List<PBX_Type__c> pbxTypes = AddPbxController.getPbx();
		Test.stopTest();

		System.assertEquals(1, pbxTypes.size());
	}

	@IsTest
	public static void testSavePbx() {
		Account acc = [SELECT Id FROM Account];
		Site__c site = [SELECT Id FROM Site__c];
		PBX_Type__c pbxType = [SELECT Id FROM PBX_Type__c];
		Competitor_Asset__c pbx = new Competitor_Asset__c();
		pbx.Account__c = acc.Id;
		pbx.Site__c = site.Id;
		pbx.PBX_type__c = pbxType.Id;

		Test.startTest();
		AddPbxController.savePbx(pbx);
		Test.stopTest();

		List<Competitor_Asset__c> savedPbx = [SELECT Id FROM Competitor_Asset__c];

		System.assertEquals(1, savedPbx.size());
	}
}
