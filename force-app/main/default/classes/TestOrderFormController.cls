@isTest
private class TestOrderFormController {
	@isTest
	static void prepareDataWithoutContractId() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();

		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;

		Test.startTest();

		PageReference pageRef = Page.OrderFormPreload;
		Test.setCurrentPage(pageRef);

		OrderFormController controller = new OrderFormController(null);
		try {
			controller.prepareData();
		} catch (Exception e) {
			Boolean expectedExceptionThrown = (e.getMessage().contains(System.Label.ERROR_OrderForm_No_ContractId)) ? true : false;
			System.AssertEquals(true, expectedExceptionThrown, e.getMessage());
		}
		System.AssertEquals(true, controller.errorFound, 'Error not found');

		Test.stopTest();
	}

	@isTest
	static void prepareData() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();

		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;
		Order__c order = new Order__c();

		Test.startTest();

		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', TestUtils.theContract.Id);
		Test.setCurrentPage(pageRef);

		OrderFormController controller = new OrderFormController(null);

		try {
			controller.prepareData();

			Order__c order2 = [SELECT Id FROM Order__c ORDER BY CreatedDate DESC LIMIT 1];
			PageReference pageRef2 = Page.OrderFormCustomerDetails;
			pageRef.getParameters().put('contractId', TestUtils.theContract.Id);
			pageRef.getParameters().put('orderId', order2.Id);
			ApexPages.StandardController sc2 = new ApexPages.StandardController(order2);
			OrderFormCustomerDataController controller2 = new OrderFormCustomerDataController();
			controller2.refresh();

			/** Change to MAC */
			controller.changeToMAC();
		} catch (Exception e) {
			system.debug(LoggingLevel.INFO, '##e.getMessage(): ' + e.getMessage());
		}

		Test.stopTest();
		System.assertNotEquals(null, order, 'Order Not created');
	}

	@isTest
	static void testSomeMethods() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();

		TestUtils.createDefaultOnenetPBXType();
		TestUtils.createCompleteContract();
		VF_Contract__c contr = TestUtils.TheContract;
		contr.Services__c = 'IPC;ALARM;IPVPN;VOVOF;ONET;SIW';
		update contr;

		TestUtils.autoCommit = true;
		Order__c order = new Order__c();

		Test.startTest();

		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', contr.Id);
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		Test.setCurrentPage(pageRef);

		OrderFormController controller = new OrderFormController(sc);

		/** BackTo */
		PageReference backToHome = controller.backTo();
		System.AssertEquals('/home/home.jsp', backToHome.getURL(), 'The Url is not correct');
		createCPs(TestUtils.theContract);
		controller.prepareData();

		Product2 p = TestUtils.createProductWithCode('C108874');
		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Quantity__c = 1;
		cp.UnitPrice__c = 10;
		cp.Arpu_Value__c = 10;
		cp.Duration__c = 1;
		cp.Product__c = p.Id;
		cp.VF_Contract__c = TestUtils.theContract.Id;
		cp.Fixed_Mobile__c = 'Fixed';
		cp.Proposition__c = 'Skype for Business Express';
		cp.CLC__c = 'Acq';
		insert cp;
		controller.prepareData();

		/** Cancel doesn't really do anything here */
		controller.cancel();

		/** BackTo */
		PageReference backToContractId = controller.backTo();
		System.AssertEquals('/' + contr.Id, backToContractId.getURL(), 'The URL with Contracted id is not correct');

		Map<String, Product2> propositionToDummyProduct = controller.propositionToDummyProduct;
		OrderFormController.createPBX(contr.Account__c, GeneralUtils.defaultOnenetPBXType);

		Test.stopTest();
	}

	@isTest
	static void changeToOnenetNoOneNetSelected() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();

		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;
		Order__c order = new Order__c();

		Test.startTest();

		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', TestUtils.theContract.Id);
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		Test.setCurrentPage(pageRef);

		OrderFormController controller = new OrderFormController(sc);
		controller.prepareData();

		Order__c order2 = [SELECT Id FROM Order__c ORDER BY CreatedDate DESC LIMIT 1];
		PageReference pageRef2 = Page.OrderFormCustomerDetails;
		pageRef.getParameters().put('contractId', TestUtils.theContract.Id);
		pageRef.getParameters().put('orderId', order2.Id);

		ApexPages.StandardController sc2 = new ApexPages.StandardController(order2);
		OrderFormCustomerDataController controller2 = new OrderFormCustomerDataController();
		controller2.refresh();

		/** Change to changeToOnenet */
		PageReference pageRef3 = controller.changeToOnenet();

		Test.stopTest();

		System.AssertEquals(null, pageRef3, 'The page reference is not null');
	}

	@isTest
	static void changeToOnenet() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();

		TestUtils.createOrderTypeFixedOneNet();
		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;
		Order__c order = new Order__c();

		Test.startTest();
		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', TestUtils.theContract.Id);
		pageRef.getParameters().put('onenetTypeSelected', 'ONENETX2014');
		pageRef.getParameters().put('forceProposition', 'onenet');
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		Test.setCurrentPage(pageRef);

		OrderFormController controller = new OrderFormController(sc);

		try {
			controller.prepareData();

			Order__c order2 = [SELECT Id FROM Order__c ORDER BY CreatedDate DESC LIMIT 1];
			PageReference pageRef2 = Page.OrderFormCustomerDetails;
			pageRef.getParameters().put('contractId', TestUtils.theContract.Id);
			pageRef.getParameters().put('orderId', order2.Id);
			controller.prepareData();

			/** Change to changeToOnenet */
			controller.onenetTypeSelected = 'ONENETX2014';
			controller.changeToOnenet();
		} catch (Exception e) {
			system.debug(LoggingLevel.INFO, '##e.getMessage(): ' + e.getMessage());
		}

		Test.stopTest();
		System.assertNotEquals(null, order, 'The order is null');
	}

	@isTest
	static void forceProposition() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOrderValidationSite();

		TestUtils.createOrderTypeFixedOneNet();
		TestUtils.createCompleteContract();
		createCPs(TestUtils.theContract);
		TestUtils.autoCommit = true;
		Order__c order = new Order__c();

		Test.startTest();

		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', TestUtils.theContract.Id);
		pageRef.getParameters().put('onenetTypeSelected', 'ONENETX2014');
		pageRef.getParameters().put('forceProposition', 'legacy');
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		Test.setCurrentPage(pageRef);
		OrderFormController controller = new OrderFormController(sc);

		try {
			controller.prepareData();
		} catch (Exception e) {
			system.debug(LoggingLevel.INFO, '##e.getMessage(): ' + e.getMessage());
		}

		Test.stopTest();
		System.assertNotEquals(null, order, 'The order is null');
	}

	@isTest
	static void testCreateNumberportings() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createOrderValidationBilling();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationOpportunity();

		TestUtils.autoCommit = false;
		Ordertype__c ot = TestUtils.createOrderTypeFixedOneNet();
		ot.ExportSystem__c = Constants.ORDERTYPE_EXPORT_SYSTEM_BOP;
		TestUtils.autoCommit = true;
		TestUtils.createCompleteContract();
		TestUtils.createDefaultOnenetPBXType();
		VF_Contract__c contr = TestUtils.theContract;
		Opportunity opp = new Opportunity();
		opp.Id = contr.Opportunity__c;
		Site__c site = TestUtils.theSite;
		TestUtils.autoCommit = false;
		Order__c order = new Order__c();

		Test.startTest();

		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', contr.Id);
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		Test.setCurrentPage(pageRef);
		List<String> productCodes = new List<String>{
			'C105230',
			'C105231',
			'C105232',
			'C105233',
			'C105234',
			'C105235',
			'C105236',
			'C105237',
			'C108872',
			'C108869'
		};
		List<Product2> products = new List<Product2>();
		for (Integer i = 0; i < productCodes.size(); i++) {
			Product2 p = TestUtils.createProductWithCode(productCodes[i]);
			products.add(p);
		}
		insert products;

		List<PricebookEntry> pbEntries = new List<PricebookEntry>();
		for (Integer i = 0; i < products.size(); i++) {
			pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i)));
		}
		insert pbEntries;

		Decimal totalAmount = 0.0;
		List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
		for (Integer i = 0; i < products.size(); i++) {
			OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp, pbEntries.get(i));
			oppLineItem.Site_List__c = site.Id;
			oppLineItems.add(oppLineItem);
			totalAmount += oppLineItem.Product_Arpu_Value__c * oppLineItem.Duration__c * oppLineItem.Quantity;
		}

		//Decimal value = 10;
		Integer quantity = 1;
		List<Contracted_Products__c> cpInsertList = new List<Contracted_Products__c>();
		for (Integer i = 0; i < products.size(); i++) {
			Contracted_Products__c cp = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[i].Id,
				VF_Contract__c = contr.Id,
				Site__c = site.Id
			);
			cpInsertList.add(cp);
		}
		insert cpInsertList;

		List<Contracted_Products__c> cpList = [
			SELECT Id, Product__r.ProductCode, Quantity__c, Product__r.Role__c, Site__r.PBX__c, Proposition__c, Site__c
			FROM Contracted_Products__c
			WHERE VF_Contract__c = :contr.Id
		];
		OrderFormController controller = new OrderFormController(sc);
		OrderFormController.createNumberportings(cpList, contr.Account__c);

		List<Contracted_Products__c> cpExtraInsertList = new List<Contracted_Products__c>();
		for (Integer i = 0; i < products.size(); i++) {
			Contracted_Products__c cp = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[i].Id,
				VF_Contract__c = contr.Id,
				Proposition_Type__c = 'FIXED'
			);
			cpExtraInsertList.add(cp);
		}
		insert cpExtraInsertList;

		try {
			controller.prepareData();
		} catch (Exception e) {
			system.debug(LoggingLevel.INFO, '##e.getMessage(): ' + e.getMessage());
		}

		Test.stopTest();
		System.assertNotEquals(null, order, 'The order is null');
	}

	@isTest
	public static void addNumberporting() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOrderValidationSite();
		TestUtils.createOrderTypeFixedOneNet();
		TestUtils.createDefaultOnenetPBXType();

		TestUtils.createCompleteContract();
		Order__c order = new Order__c(Propositions__c = 'Internet');
		TestUtils.theContract.Services__c = 'IPC;ALARM;IPVPN;VOVOF;ONET;SIW';
		TestUtils.theContract.Deal_type__c = 'New';
		TestUtils.theContract.Owner__c = TestUtils.theAdministrator.Id;
		update TestUtils.theContract;
		createCPs(TestUtils.theContract);

		Test.startTest();

		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', TestUtils.theContract.Id);
		pageRef.getParameters().put('onenetTypeSelected', 'ONENETX2014');
		pageRef.getParameters().put('forceProposition', 'onenet');
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		Test.setCurrentPage(pageRef);

		OrderFormController controller = new OrderFormController(sc);
		controller.prepareData();
		order = [SELECT Id, Name, Account__c FROM Order__c ORDER BY CreatedDate DESC LIMIT 1];

		List<NumberportingRowWrapper> portingRows = new List<NumberportingRowWrapper>();
		OrderFormController.addDefaultNumberportings(order, portingRows);

		delete [SELECT Id FROM Contracted_Products__c WHERE VF_Contract__c = :TestUtils.theContract.Id];
		List<NumberportingRowWrapper> portingRows2 = new List<NumberportingRowWrapper>();
		OrderFormController.addDefaultNumberportings(order, portingRows2);
		controller.prepareData();

		Test.stopTest();
		System.assertNotEquals(null, order, 'The order is null');
	}

	public static void createCPs(VF_Contract__c theContract) {
		TestUtils.autoCommit = false;
		List<Product2> products = createProductsAndPricebookEntries();

		List<Contracted_Products__c> cpInsertList = new List<Contracted_Products__c>();
		List<String> fixedMobile = new List<String>{
			'Fixed',
			'Fixed',
			'Fixed',
			'Fixed',
			'Fixed',
			'Mobile',
			'Fixed',
			'Mobile',
			'Fixed',
			'Mobile',
			'Fixed',
			'Test',
			'Fixed',
			'Fixed'
		};
		List<String> prepositions = new List<String>{
			'Internet',
			'One Fixed',
			'One Fixed Enterprise',
			'One Fixed Express',
			'One Fixed Express',
			'IPVPN',
			'IPVPN+Internet',
			'IPVPN+Internet',
			'IPVPN+One Fixed',
			'IPVPN+One Fixed',
			'IPVPN+One Fixed+Internet',
			'IPVPN+One Fixed+Internet',
			'IPVPN+One Fixed+Internet',
			'One Net Enterprise'
		};
		List<String> prepositionsComponents = new List<String>{
			'Internet',
			'Voice',
			'Internet',
			'Voice',
			'Internet',
			'IPVPN',
			'Internet',
			'IPVPN',
			'IPVPN',
			'Voice',
			'IPVPN',
			'Voice',
			'Internet',
			'Internet'
		};
		for (Integer i = 0; i < products.size(); i++) {
			Contracted_Products__c cp = new Contracted_Products__c();
			cp.Quantity__c = 1;
			cp.UnitPrice__c = 10;
			cp.Arpu_Value__c = 10;
			cp.Duration__c = 1;
			cp.Product__c = products[i].Id;
			cp.VF_Contract__c = theContract.Id;
			cp.Fixed_Mobile__c = fixedMobile[i];
			cp.Proposition__c = prepositions[i];
			cp.Proposition_Component__c = prepositionsComponents[i];
			if (products[i].Role__c == 'Link') {
				cp.Interface_Type__c = 'ISDN2';
			}
			cpInsertList.add(cp);
		}
		insert cpInsertList;
		TestUtils.autoCommit = false;
	}

	public static List<Product2> createProductsAndPricebookEntries() {
		List<String> productRoles = new List<String>{
			'Link',
			'Carrier',
			'Numberporting',
			'Phone',
			'ERS',
			'Dark Fiber',
			'Router',
			'Numberporting',
			'Link (Internet)',
			'Phone',
			'ERS',
			'Dark Fiber',
			'Router',
			'Carrier'
		};
		List<String> productCodes = new List<String>{
			'C105230',
			'C105231',
			'C105232',
			'C105233',
			'C105234',
			'C105235',
			'C105236',
			'C105237',
			'C105238',
			'C108869',
			'C108870',
			'C108871',
			'C108872',
			'C108873'
		};
		List<String> linkTypesProducts = new List<String>{
			'ADSL',
			'EOC',
			'FIBER',
			'SDSL',
			'VDSL',
			'WEAS',
			'WEAS',
			'ADSL',
			'ADSL',
			'EOC',
			'FIBER',
			'SDSL',
			'VDSL',
			'WEAS'
		};
		List<Product2> products = new List<Product2>();
		for (Integer i = 0; i < productCodes.size(); i++) {
			Product2 p = TestUtils.createProductWithCode(productCodes[i]);
			p.Role__c = productRoles[i];
			if (p.Role__c == 'Router') {
				p.Brand__c = 'TestBrand';
				p.Model__c = 'TestModel';
			}
			p.Link_type__c = linkTypesProducts[i];
			products.add(p);
		}
		insert products;

		List<PricebookEntry> pbEntries = new List<PricebookEntry>();
		for (Integer i = 0; i < products.size(); i++) {
			pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i)));
		}
		insert pbEntries;
		return products;
	}

	@isTest
	static void testPrepareWithoutNoSites() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();

		TestUtils.createDefaultOnenetPBXType();
		TestUtils.createCompleteContract();
		VF_Contract__c contr = TestUtils.TheContract;
		contr.Services__c = 'IPC;ALARM;IPVPN;VOVOF;ONET;SIW';
		update contr;
		List<Contracted_Products__c> cProducts = [SELECT Id, Site__c FROM Contracted_Products__c];
		for (Contracted_Products__c cp : cProducts) {
			cp.Site__c = null;
		}
		update cProducts;
		TestUtils.autoCommit = true;
		Order__c order = new Order__c();

		Test.startTest();

		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', contr.Id);
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		Test.setCurrentPage(pageRef);

		OrderFormController controller = new OrderFormController(sc);

		/** BackTo */
		PageReference backToHome = controller.backTo();
		System.AssertEquals('/home/home.jsp', backToHome.getURL(), 'The URL Is not home');
		createCPs(TestUtils.theContract);
		controller.prepareData();

		Product2 p = TestUtils.createProductWithCode('C108874');
		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Quantity__c = 1;
		cp.UnitPrice__c = 10;
		cp.Arpu_Value__c = 10;
		cp.Duration__c = 1;
		cp.Product__c = p.Id;
		cp.VF_Contract__c = TestUtils.theContract.Id;
		cp.Fixed_Mobile__c = 'Fixed';
		cp.Proposition__c = 'Skype for Business Express';
		cp.Site__c = null;
		cp.CLC__c = 'Acq';
		insert cp;
		controller.prepareData();

		/** Cancel doesn't really do anything here */
		controller.cancel();

		/** BackTo */
		PageReference backToContractId = controller.backTo();
		System.AssertEquals('/' + contr.Id, backToContractId.getURL(), 'The Contract Id of the URL not correct');

		Map<String, Product2> propositionToDummyProduct = controller.propositionToDummyProduct;
		OrderFormController.createPBX(contr.Account__c, GeneralUtils.defaultOnenetPBXType);

		Test.stopTest();
	}

	@isTest
	static void testMobile() {
		TestUtils.disableDlrs = true;
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOrderType();

		TestUtils.createCompleteMobileContract();

		VF_Contract__c contr = TestUtils.TheContract;
		TestUtils.autoCommit = true;
		Order__c order = new Order__c();

		Test.startTest();

		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', contr.Id);
		ApexPages.StandardController sc = new ApexPages.StandardController(order);
		Test.setCurrentPage(pageRef);

		OrderFormController controller = new OrderFormController(sc);

		controller.prepareData();

		Order__c orderMobile = [SELECT Id, Mobile_Fixed__c FROM Order__c WHERE VF_Contract__c = :contr.Id].get(0);

		System.assertNotEquals(null, orderMobile, 'Mobile Order not generated');

		Test.stopTest();
	}
}
