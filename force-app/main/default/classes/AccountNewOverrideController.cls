public with sharing class AccountNewOverrideController {

    public AccountNewOverrideController(ApexPages.StandardController ignored) {
        
    }   

    public PageReference url() {
        if(System.Userinfo.getUserType() == 'PowerPartner'){
            // if powerpartner, then return custom search page. From there user will be taken to 'real' account page if needed
            return Page.AccountCustomSearch;
        } else {
            // if recordtype is dealer, then forward to dealer edit page
            String recordType = Apexpages.currentPage().getParameters().get('RecordType');
            if(recordType  != null && recordType == GeneralUtils.recordTypeMap.get('Account').get('Dealer_Accounts')){
                String retURL = Apexpages.currentPage().getParameters().get('retURL');
                String pr = '/001/e?nooverride=1&RecordType=' + recordType + '&retURL=' + retURL;
                return new PageReference(pr);             
            } else {
            // else go to olbico account search page    
                return Page.OlbicoCdpNewAccount;
            }
        }           
        
    } 

}