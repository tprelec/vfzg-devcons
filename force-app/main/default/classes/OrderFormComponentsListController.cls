public with sharing class OrderFormComponentsListController {


  public Order__c theCurrentOrder {
    get;
    set{
      theCurrentOrder = value;
      createOrderFormComponentsMap(theCurrentOrder);
    }
  }

  public OrderFormComponentsListController(){

  }


  /* OrderFormComponent methods */

  public List<orderFormComponent> otherOrderFormComponentList {
    get;
    set;
  }

  public orderFormComponent customerComponent{
    get{
      customerComponent.ready = theCurrentOrder.CustomerReady__c;
      return customerComponent;
    }
    set;
  }
  public orderFormComponent billingComponent{
    get{
      billingComponent.ready = theCurrentOrder.BillingReady__c;
      return billingComponent;
    }
    set;
  }
  public orderFormComponent orderComponent{
    get{
      system.debug('GC current order: '+theCurrentOrder);
      orderComponent.ready = theCurrentOrder.OrderReady__c;
      return orderComponent;
    }
    set;
  }
  public static orderFormComponent deliveryComponent{
    get;
    set;
  }
  public orderFormComponent ctnComponent{
    get{
      if(ctnComponent != null) ctnComponent.ready = theCurrentOrder.CTNReady__c;
      return ctnComponent;
    }
    set;
  }
  public orderFormComponent numberportingComponent{
    get{
      if(numberportingComponent != null) numberportingComponent.ready = theCurrentOrder.NumberportingReady__c;
      return numberportingComponent;
    }
    set;
  }
  public orderFormComponent phonebookRegistrationComponent{
    get{
      if(phonebookRegistrationComponent != null) phonebookRegistrationComponent.ready = theCurrentOrder.PhonebookregistrationReady__c;
      return phonebookRegistrationComponent;
    }
    set;
  }
  public orderFormComponent orderProgressComponent{
    get{
      system.debug(theCurrentOrder);
      orderProgressComponent.ready = (theCurrentOrder.Status__c=='Accepted'?true:false);
      return orderProgressComponent;
    }
    set;
  }
  // JvW 11-12-2019
  public orderFormComponent contractedProductsComponent{
    get{
      system.debug(theCurrentOrder);
      contractedProductsComponent.ready = theCurrentOrder.ContractedproductsReady__c;
      return contractedProductsComponent;
    }
    set;
  }

   public void createOrderFormComponentsMap(Order__c theCurrentOrder){
     otherOrderFormComponentList = new List<OrderFormComponent>();
     List<String> componentsList = formComponentsList(theCurrentOrder);
    for (String thisComponent : componentsList){
      OrderFormComponent ofc = new OrderFormComponent();

      ofc.url = '/apex/'+thisComponent+'?contractId='+theCurrentOrder.VF_contract__c+'&orderId='+theCurrentOrder.Id;
      ofc.ready = false;
      ofc.pageName = thisComponent;



      // map special form components to their dedicated placeholder on the screen
      if(thisComponent.toLowerCase()=='orderformcustomerdetails'){
        ofc.componentType = 'Customer';
        ofc.Name = 'Customer/Contract';
        customerComponent = ofc;
      } else if(thisComponent.toLowerCase() == 'orderformbillingdetails'){
        ofc.componentType = 'Billing';
        ofc.Name = 'Billing';
        billingComponent = ofc;
      } else if(thisComponent.toLowerCase() == 'orderformorderdetails'){
        ofc.componentType = 'Order';
        ofc.Name = 'Order';
        orderComponent = ofc;
      } else if(thisComponent.toLowerCase() == 'orderformdeliverydetails'){
        ofc.componentType = 'Delivery';
        ofc.Name = 'Delivery';
        ofc.Ready = theCurrentOrder.DeliveryReady__c;
        deliveryComponent = ofc;
      } else if(thisComponent.toLowerCase() == 'orderformctndetails'){
        ofc.componentType = 'CTN';
        ofc.Name = 'CTN';
        ctnComponent = ofc;
      } else if(thisComponent.toLowerCase() == 'orderformnumberportingdetails'){
        ofc.componentType = 'Numberblocks';
        ofc.Name = 'Numberblocks';
        numberportingComponent = ofc;
      } else if(thisComponent.toLowerCase() == 'orderformphonebookregistrationdetails'){
        ofc.componentType = 'PhonebookRegistration';
        ofc.Name = 'PhonebookRegistration';
        phonebookRegistrationComponent = ofc;
      } else if(thisComponent.toLowerCase() == 'orderformorderprogress'){
        ofc.componentType = 'Progress';
        ofc.Name = 'Customer & Order Creation';
        orderProgressComponent = ofc;
      } else if(thisComponent.toLowerCase() == 'orderformcontractedproducts'){ // JvW 11-12-2019
        ofc.componentType = 'ContractedProducts';
        ofc.Name = 'Delivery & Billing';
        contractedProductsComponent = ofc;
      } else {
        ofc.componentType = 'Other';
        ofc.Name = thisComponent.replace('OrderForm','');
        ofc.ready = true;
        otherOrderFormComponentList.add(ofc);
      }

      system.debug(ofc);
    }

   }

  public class OrderFormComponent{
    public String name {get;set;}
    public String pageName {get;set;}
    public String componentType {get;set;}
    public String url {get;set;}
    public String imgUrl {get;set;}
    public Boolean ready {
      get;
      set{
        ready = value;
        if(ready){
          imgUrl = '/img/samples/flag_green.gif';
        } else {
          imgUrl = '/img/samples/flag_red.gif';
        }
      }
    }

  }

    private List<String> formComponentsList(Order__c theCurrentOrder) {

    List<String> returnList = new List<String>();


    returnList.add('OrderFormCustomerDetails');
    returnList.add('OrderFormBillingDetails');
    returnList.add('OrderFormOrderDetails');
    if(theCurrentOrder.propositions__c.contains('One Fixed')
      || theCurrentOrder.propositions__c.contains('One Net')
      || theCurrentOrder.propositions__c.contains('IPVPN')
      || theCurrentOrder.propositions__c.contains('Legacy')
      || theCurrentOrder.propositions__c.contains('Internet')
    ){
      returnList.add('OrderFormDeliveryDetails');
    }

    // Orders owned by Partner users always get the CTN tab if the order is mobile
    // Other mobile orders have to be an escalation to get the CTN tab
    System.debug(theCurrentOrder.VF_Contract__r.Opportunity__r.Department__c == 'SoHo');
    System.debug(theCurrentOrder.Mobile_Fixed__c == 'Mobile');
    System.debug(theCurrentOrder.VF_Contract__r.Opportunity__r.SmallBusiness_Onderstroom__c);
    if ((theCurrentOrder.VF_Contract__r.Opportunity__r.Owner.UserType=='PowerPartner' && theCurrentOrder.Mobile_Fixed__c == 'Mobile')
      || (theCurrentOrder.VF_Contract__r.Opportunity__r.Escalation__c && theCurrentOrder.Mobile_Fixed__c == 'Mobile')
      || (theCurrentOrder.VF_Contract__r.Opportunity__r.Segment__c == 'SoHo' && theCurrentOrder.Mobile_Fixed__c == 'Mobile' /**&& theCurrentOrder.VF_Contract__r.Opportunity__r.SmallBusiness_Onderstroom__c == true**/)) {
      returnList.add('OrderFormCTNDetails');
    }

    if(OrderWrapper.getHasFixedVoiceFromPropositions(theCurrentOrder.Propositions__c)) {
      returnList.add('OrderFormNumberPortingDetails');
      returnList.add('OrderFormPhoneBookRegistrationDetails');
    }
    returnList.add('OrderFormOrderProgress');
    returnList.add('OrderFormContractedProducts'); // JvW 11-12-2019

    system.debug(returnList);
    return returnList;

    }


}