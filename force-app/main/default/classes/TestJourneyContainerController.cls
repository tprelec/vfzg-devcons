@isTest
public with sharing class TestJourneyContainerController {
	@TestSetup
	static void makeData() {
		User owner = GeneralUtils.currentUser;
		Account acct = TestUtils.createAccount(owner);
		TestUtils.autoCommit = true;
		TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), null, owner);
	}

	@isTest
	public static void testGetJourneySteps() {
		List<Opportunity> oppList = [SELECT Id FROM Opportunity];

		Test.startTest();
		String steps = JourneyContainerController.getJourneySteps(oppList[0].Id);
		System.assertEquals(JSON.serialize(AddMobileProductsJourneyHandler.getInitialJourneySteps()), steps, 'Journey Step is returned!');
		Test.stopTest();
	}
	@isTest
	public static void testUpdateJorneySteps() {
		List<Opportunity> oppList = [SELECT Id FROM Opportunity];

		Test.startTest();
		Boolean flag = JourneyContainerController.updateJorneySteps(
			oppList[0].Id,
			JSON.serialize(AddMobileProductsJourneyHandler.getInitialJourneySteps())
		);
		System.assertEquals(true, flag, 'Journey Step is updated!');
		Test.stopTest();
	}
}
