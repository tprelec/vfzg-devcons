@IsTest
private class PP_SearchContactsControllerTest {
	@TestSetup
	static void setup() {
		User portalUser = PP_TestUtils.createPortalUser();

		TestUtils.autoCommit = false;
		Account acct = TestUtils.createAccount(portalUser);
		acct.Name = 'CustomerTest';
		insert acct;

		List<Contact> contacts = new List<Contact>{
			new Contact(AccountId = acct.Id, LastName = 'Last', FirstName = 'First'),
			new Contact(AccountId = acct.Id, LastName = 'Last2', FirstName = 'First2')
		};
		insert contacts;
	}

	@IsTest
	static void testGetContact() {
		User portalUser = PP_TestUtils.getPortalUser();
		Contact cont = [SELECT Id, LastName FROM Contact WHERE LastName = 'Last'];

		System.runAs(portalUser) {
			Contact result = PP_SearchContactsController.getContact(cont.Id);
			System.assertEquals(cont.LastName, result.LastName, 'Last Name should be Last');
		}
	}

	@IsTest
	static void testGetContacts() {
		User portalUser = PP_TestUtils.getPortalUser();
		Account acct = [SELECT Id FROM Account WHERE Name = 'CustomerTest'];

		System.runAs(portalUser) {
			Test.startTest();
			List<Contact> result = PP_SearchContactsController.getContacts(acct.Id, 'abracadabra');
			System.assertEquals(2, result.size(), 'There should be two contacts');

			result = PP_SearchContactsController.getContacts(acct.Id, '1');
			System.assertEquals(1, result.size(), 'There should be one contact');
			Test.stopTest();
		}
	}

	@IsTest
	static void testSaveContacts() {
		User portalUser = PP_TestUtils.getPortalUser();
		Account acct = [SELECT Id FROM Account WHERE Name = 'CustomerTest'];
		List<Contact> contacts = [SELECT Id FROM Contact WHERE AccountId = :acct.Id];

		System.runAs(portalUser) {
			Test.startTest();
			String result = PP_SearchContactsController.saveContacts(contacts);
			Test.stopTest();
			System.assertEquals('SUCCESS', result, 'Contacts should be updated/saved');
		}
	}

	@IsTest
	static void testFailToSaveContacts() {
		User portalUser = PP_TestUtils.getPortalUser();
		List<Contact> contacts = [SELECT Id FROM Contact];

		System.runAs(portalUser) {
			Test.startTest();
			String result = PP_SearchContactsController.saveContacts(contacts);
			Test.stopTest();
			System.assertEquals(result != 'SUCCESS', true, 'Exception should be thrown');
		}
	}

	@IsTest
	static void testAuthorizeToSignFirst() {
		User portalUser = PP_TestUtils.getPortalUser();
		Account acct = [
			SELECT Id, Authorized_to_sign_1st__c
			FROM Account
			WHERE Name = 'CustomerTest'
		];
		System.assertEquals(
			null,
			acct.Authorized_to_sign_1st__c,
			'Authorized_to_sign_1st__c should not be set yet'
		);

		System.runAs(portalUser) {
			Test.startTest();
			List<Contact> contacts = PP_SearchContactsController.getContacts(acct.Id, '0');
			AuraActionResult result = PP_SearchContactsController.authorizeToSignFirst(
				acct.Id,
				contacts.get(0).Id
			);
			System.assertEquals(true, result.success, 'Call to server should be successful');

			acct = [SELECT Id, Authorized_to_sign_1st__c FROM Account WHERE Name = 'CustomerTest'];
			System.assertEquals(
				contacts.get(0).Id,
				acct.Authorized_to_sign_1st__c,
				'Authorized_to_sign_1st__c should be updated'
			);

			result = PP_SearchContactsController.authorizeToSignFirst(
				'invalid id',
				contacts.get(0).Id
			);
			System.assertEquals(false, result.success, 'Call to server should not be successful');
			Test.stopTest();
		}
	}

	@IsTest
	static void testAuthorizeToSignSecond() {
		User portalUser = PP_TestUtils.getPortalUser();
		Account acct = [
			SELECT Id, Authorized_to_sign_2nd__c
			FROM Account
			WHERE Name = 'CustomerTest'
		];
		System.assertEquals(
			null,
			acct.Authorized_to_sign_2nd__c,
			'Authorized_to_sign_2nd__c should not be set yet'
		);

		System.runAs(portalUser) {
			Test.startTest();
			List<Contact> contacts = PP_SearchContactsController.getContacts(acct.Id, '0');
			AuraActionResult result = PP_SearchContactsController.authorizeToSignSecond(
				acct.Id,
				contacts.get(0).Id
			);
			System.assertEquals(true, result.success, 'Call to server should be successful');

			acct = [SELECT Id, Authorized_to_sign_2nd__c FROM Account WHERE Name = 'CustomerTest'];
			System.assertEquals(
				contacts.get(0).Id,
				acct.Authorized_to_sign_2nd__c,
				'Authorized_to_sign_2nd__c should be updated'
			);

			result = PP_SearchContactsController.authorizeToSignSecond(
				'invalid id',
				contacts.get(0).Id
			);
			System.assertEquals(false, result.success, 'Call to server should not be successful');
			Test.stopTest();
		}
	}

	@IsTest
	static void testAuthorizeToSignSwitch() {
		User portalUser = PP_TestUtils.getPortalUser();
		Account acct = [
			SELECT Id, Authorized_to_sign_1st__c, Authorized_to_sign_2nd__c
			FROM Account
			WHERE Name = 'CustomerTest'
		];
		System.assertEquals(
			null,
			acct.Authorized_to_sign_1st__c,
			'Authorized_to_sign_1st__c should not be set yet'
		);

		System.runAs(portalUser) {
			Test.startTest();
			List<Contact> contacts = PP_SearchContactsController.getContacts(acct.Id, '0');
			AuraActionResult result = PP_SearchContactsController.authorizeToSignFirst(
				acct.Id,
				contacts.get(0).Id
			);
			System.assertEquals(true, result.success, 'Call to server should be successful');

			acct = [
				SELECT Id, Authorized_to_sign_1st__c, Authorized_to_sign_2nd__c
				FROM Account
				WHERE Name = 'CustomerTest'
			];
			System.assertEquals(
				contacts.get(0).Id,
				acct.Authorized_to_sign_1st__c,
				'Authorized_to_sign_1st__c should be updated'
			);

			result = PP_SearchContactsController.authorizeToSignSecond(acct.Id, contacts.get(0).Id);
			System.assertEquals(true, result.success, 'Call to server should be successful');

			acct = [
				SELECT Id, Authorized_to_sign_1st__c, Authorized_to_sign_2nd__c
				FROM Account
				WHERE Name = 'CustomerTest'
			];
			System.assertEquals(
				contacts.get(0).Id,
				acct.Authorized_to_sign_2nd__c,
				'Authorized_to_sign_2nd__c should be updated'
			);

			System.assertEquals(
				null,
				acct.Authorized_to_sign_1st__c,
				'Authorized_to_sign_1st__c should be removed'
			);

			result = PP_SearchContactsController.authorizeToSignFirst(acct.Id, contacts.get(0).Id);
			System.assertEquals(true, result.success, 'Call to server should be successful');

			acct = [
				SELECT Id, Authorized_to_sign_1st__c, Authorized_to_sign_2nd__c
				FROM Account
				WHERE Name = 'CustomerTest'
			];
			System.assertEquals(
				contacts.get(0).Id,
				acct.Authorized_to_sign_1st__c,
				'Authorized_to_sign_1st__c should be updated'
			);

			System.assertEquals(
				null,
				acct.Authorized_to_sign_2nd__c,
				'Authorized_to_sign_2nd__c should be removed'
			);
			Test.stopTest();
		}
	}

	@IsTest
	static void testRemoveAuthorizeToSign() {
		User portalUser = PP_TestUtils.getPortalUser();
		Account acct = [
			SELECT Id, Authorized_to_sign_2nd__c
			FROM Account
			WHERE Name = 'CustomerTest'
		];
		System.assertEquals(
			null,
			acct.Authorized_to_sign_2nd__c,
			'Authorized_to_sign_2nd__c should not be set yet'
		);

		System.runAs(portalUser) {
			Test.startTest();
			List<Contact> contacts = PP_SearchContactsController.getContacts(acct.Id, '0');
			AuraActionResult result = PP_SearchContactsController.authorizeToSignFirst(
				acct.Id,
				contacts.get(0).Id
			);
			System.assertEquals(true, result.success, 'Call to server should be successful');

			acct = [SELECT Id, Authorized_to_sign_1st__c FROM Account WHERE Name = 'CustomerTest'];
			System.assertEquals(
				contacts.get(0).Id,
				acct.Authorized_to_sign_1st__c,
				'Authorized_to_sign_1st__c should be updated'
			);

			result = PP_SearchContactsController.removeAuthorizeToSign(acct.Id, contacts.get(0).Id);
			System.assertEquals(true, result.success, 'Call to server should be successful');

			acct = [SELECT Id, Authorized_to_sign_1st__c FROM Account WHERE Name = 'CustomerTest'];
			System.assertEquals(
				null,
				acct.Authorized_to_sign_1st__c,
				'Authorized_to_sign_1st__c should be removed'
			);

			result = PP_SearchContactsController.authorizeToSignSecond(acct.Id, contacts.get(0).Id);
			System.assertEquals(true, result.success, 'Call to server should be successful');

			acct = [SELECT Id, Authorized_to_sign_2nd__c FROM Account WHERE Name = 'CustomerTest'];
			System.assertEquals(
				contacts.get(0).Id,
				acct.Authorized_to_sign_2nd__c,
				'Authorized_to_sign_2nd__c should be updated'
			);

			result = PP_SearchContactsController.removeAuthorizeToSign(acct.Id, contacts.get(0).Id);
			System.assertEquals(true, result.success, 'Call to server should be successful');

			acct = [SELECT Id, Authorized_to_sign_2nd__c FROM Account WHERE Name = 'CustomerTest'];
			System.assertEquals(
				null,
				acct.Authorized_to_sign_2nd__c,
				'Authorized_to_sign_2nd__c should be removed'
			);

			result = PP_SearchContactsController.removeAuthorizeToSign(
				'invalid id',
				contacts.get(0).Id
			);
			System.assertEquals(false, result.success, 'Call to server should not be successful');
			Test.stopTest();
		}
	}
}