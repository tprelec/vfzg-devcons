/*
    All SOQLs required onto cfg
*/
public with sharing class DBHelp{

    public static final String TELCO_OPTIONS_OBJECT = 'csordtelcoa__orders_subscriptions_options__c';

    public static Schema.SObjectType getTelcoOptionsOtype() {
        return Schema.getGlobalDescribe().get(TELCO_OPTIONS_OBJECT);
    }

}