@isTest
private class TestVendorTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Vendor';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'VEN-09-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		Vendor__c testVendor = new Vendor__c();
		testVendor.Name = 'Test';
		insert testVendor;

		Vendor__c queryRecord = [SELECT Id, ExternalID__c FROM Vendor__c LIMIT 1];
		System.assert(queryRecord.ExternalID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
