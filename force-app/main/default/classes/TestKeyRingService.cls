@isTest
public class TestKeyRingService {
	@isTest
	static void keyRingServiceMethods() {
		Sales_Settings__c ss = new Sales_Settings__c(
			Max_weekly_postalcode_checks__c = 0,
			Max_Daily_Postalcode_Checks__c = 1,
			Postalcode_check_validity_days__c = 3
		);
		insert ss;

		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'AFD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		External_Account__c externalAcc1 = new External_Account__c();
		externalAcc1.Account__c = acct.Id;
		externalAcc1.External_Account_Id__c = 'ABC';
		externalAcc1.External_Source__c = 'Unify';
		externalAcc1.Related_External_Account__c = externalAcc.Id;
		insert externalAcc1;
		Ban__c ban = TestUtils.createBan(acct);
		ban.BOP_export_datetime__c = System.now();
		update ban;
		TestUtils.autoCommit = false;
		Site__c s1 = TestUtils.createSite(acct);
		s1.Last_dsl_check__c = System.today().addDays(-1);
		insert s1;
		TestUtils.autoCommit = true;

		Test.startTest();
		ss.Max_weekly_postalcode_checks__c = 2;
		update ss;
		ban.BOPCode__c = 'ABC';
		update ban;
		Set<Id> banIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();
		Set<String> accIds = new Set<String>();
		Set<Id> siteIds = new Set<Id>();
		String accountId = acct.Id;
		String siteId = s1.Id;
		banIds.add(ban.Id);
		accountIds.add(acct.Id);
		accIds.add(acct.Id);

		siteIds.add(s1.Id);
		//Site__c s = TestUtils.createSite(acct);
		Set<Id> accountIdsWithBopCode = KeyRingService.externalBopAccounts(accountIds);
		System.assert(!accountIdsWithBopCode.isEmpty(), 'test List');
		List<External_Account__c> exAccList = KeyRingService.externalUnifyAccounts(accountId);
		System.assertEquals(1, exAccList.size(), 'test List');
		Map<string, string> exAccMap = KeyRingService.getexternalBopAccount(accountIds);
		System.assert(!exAccMap.isEmpty(), 'test List');
		Map<Id, List<External_Account__c>> mapExaccounts = KeyRingService.getExternalAccount(accIds);
		System.assert(mapExaccounts.isEmpty(), 'test List');
		Map<String, List<External_Account__c>> mapExaccount = KeyRingService.buildBopCodeMap(accountIds);
		System.assert(!mapExaccount.isEmpty(), 'test List');
		String acc = KeyRingService.getExternalBopCode(accountId);
		System.assert(acc != null, 'test List');
		List<External_Account__c> exAccsList = KeyRingService.externalAccountsForBop(accountId);
		System.assertEquals(1, exAccsList.size(), 'test List');
		String extSource = 'Unify';
		List<External_Site__c> exSiteList = KeyRingService.getExternalsitesList(accountIds, extSource);
		System.assertEquals(0, exSiteList.size(), 'test List');
		List<External_Site__c> exSitesList = KeyRingService.getExternalsites(siteId, accountId, extSource);
		System.assertEquals(0, exSitesList.size(), 'test List');

		Test.stopTest();
	}

	@isTest
	static void keyRingServiceMethodsOne() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('SiteTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('BanTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		String extAccountId = 'ABC';
		Map<Id, List<External_Account__c>> extAcctMap = new Map<Id, List<External_Account__c>>();
		Map<Id, List<External_Account__c>> extUnifyAcctMap = new Map<Id, List<External_Account__c>>();
		Map<Id, List<External_Site__c>> extSiteMap = new Map<Id, List<External_Site__c>>();
		Map<Id, List<External_Contact__c>> extConMap = new Map<Id, List<External_Contact__c>>();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact cont = TestUtils.createContact(acct);
		Site__c site = TestUtils.createSite(acct);
		Ban__c ban = TestUtils.createBan(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban);
		opp.Ban__c = ban.Id;
		update opp;
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		//VF_Contract__c contr = new VF_Contract__c(Account__c = acct.Id, Opportunity__c = opp.Id, Net_Revenues__c = 12345);

		Order__c ord = TestUtils.createOrder(contr);

		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'AFD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		External_Account__c externalAcc1 = new External_Account__c();
		externalAcc1.Account__c = acct.Id;
		externalAcc1.External_Account_Id__c = extAccountId;
		externalAcc1.External_Source__c = 'Unify';
		externalAcc1.Related_External_Account__c = externalAcc.Id;
		insert externalAcc1;

		ban.ExternalAccount__c = externalAcc1.Id;
		update ban;

		extUnifyAcctMap.put(externalAcc.Account__c, new List<External_Account__c>{ externalAcc1 });
		extAcctMap.put(externalAcc.Account__c, new List<External_Account__c>{ externalAcc });
		External_Site__c exsite = new External_Site__c();
		exsite.External_Account__c = externalAcc1.Id;
		exsite.Site__c = site.Id;
		exsite.External_Site_Id__c = 'Test';
		exsite.External_Source__c = 'Unify';
		insert exsite;
		extSiteMap.put(exsite.Site__c, new List<External_Site__c>{ exsite });
		External_Contact__c exCon = new External_Contact__c();
		exCon.Contact__c = cont.Id;
		exCon.ExternalId__c = extAccountId + '-Test1';
		exCon.External_Account__c = externalAcc1.Id;
		exCon.External_Contact_Id__c = 'Test1';
		exCon.External_Source__c = 'Unify';
		insert exCon;
		extConMap.put(exCon.Contact__c, new List<External_Contact__c>{ exCon });

		Test.startTest();
		Order_Response__c or11 = new Order_Response__c();
		or11.Order__c = ord.Id;
		or11.Type__c = 'BopOrderCreation';
		or11.Unify_Error_Number__c = '0';
		insert or11;

		Order_Response__c or12 = new Order_Response__c();
		or12.Order__c = ord.Id;
		or12.Type__c = 'BopOrderCreation';
		or12.Unify_Error_Number__c = 'SIAS102';
		or12.Unify_Error_Description__c = 'WRONG!';
		insert or12;

		Order_Response__c or13 = new Order_Response__c();
		or13.Order__c = ord.Id;
		or13.Type__c = 'BopOrderCreation';
		or13.Unify_Error_Number__c = 'SIAS001';
		insert or13;

		Order_Response__c or14 = new Order_Response__c();
		or14.Order__c = ord.Id;
		or14.Type__c = 'UnifyOrderCreation';
		or14.Unify_Error_Number__c = 'SIAS000';
		insert or14;

		Order_Response__c or15 = new Order_Response__c();
		or15.Order__c = ord.Id;
		or15.Type__c = 'UnifyOrderCreation';
		or15.Unify_Account_Id__c = extAccountId;
		or15.Unify_Error_Number__c = 'SIAS001';
		insert or15;

		Order_Response__c or16 = new Order_Response__c();
		or16.Order__c = ord.Id;
		or16.Type__c = 'SendMappingObject';
		or16.Unify_Error_Number__c = 'SIAS000';
		insert or16;

		Order_Response__c or17 = new Order_Response__c();
		or17.Order__c = ord.Id;
		or17.Type__c = 'SendMappingObject';
		or17.Unify_Error_Number__c = 'SIAS001';
		insert or17;

		Map<String, List<External_Account__c>> extAccntToProcessMap = KeyRingService.getExternalBOPAccountsList(or12, extAcctMap);

		List<External_Account__c> extAccntsToInsert = extAccntToProcessMap.get('insert');

		List<External_Account__c> extAccntsToUpdate = extAccntToProcessMap.get('update');

		System.assertEquals(0, extAccntsToInsert.size(), 'test List');
		List<External_Account__c> exUnifyAccList = KeyRingService.getExternalAccountsList(or17, extUnifyAcctMap);
		System.assertEquals(1, exUnifyAccList.size(), 'test List');
		List<External_Site__c> exSiteList = KeyRingService.getExternalSitesList(or16, extSiteMap);
		System.assertEquals(1, exSiteList.size(), 'test List');
		List<External_Contact__c> exConList = KeyRingService.getExternalContactsList(or15, extConMap);
		System.assertEquals(1, exConList.size(), 'test List');

		Set<Order__c> ordersSet = loadOrdersSet();
		Map<Id, String> banCodeMap = KeyRingService.retrieveBANCodeFromOrders(ordersSet);
		System.assertNotEquals(0, banCodeMap.size(), 'The BAN set is empty');
		Map<Id, List<External_Contact__c>> extConMap2 = KeyRingService.getExternalcontact(new Set<String>{ (String) cont.Id });
		System.assertNotEquals(0, extConMap2.size(), 'The Extcontacts set is empty');

		Test.stopTest();
	}

	static Set<Order__c> loadOrdersSet() {
		List<Order__c> resultingOrdersSet = [
			SELECT Id, VF_Contract__r.Opportunity__r.BAN__r.ExternalAccount__r.Related_External_Account__r.External_Account_Id__c
			FROM Order__c
		];
		return new Set<Order__c>(resultingOrdersSet);
	}
}
