/**
 * @description       : Apex Class Handler of the D2D Compensation Trigger
 * @author            : Filip Vucic (filip.vucic@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 03-11-2022
 * @last modified by  : Filip Vucic (filip.vucic@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                        Modification
 * 1.0   03-11-2022   Filip Vucic (filip.vucic@vodafoneziggo.com)   Initial Version
 **/
public class D2DCompensationTriggerHandler extends TriggerHandler {
	private List<D2D_Compensation__c> newD2DCompensations;

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Filip Vucic (filip.vucic@vodafoneziggo.com) | 03-11-2022
	 **/
	public override void beforeInsert() {
		newD2DCompensations = (List<D2D_Compensation__c>) this.newList;
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Filip Vucic (filip.vucic@vodafoneziggo.com) | 03-11-2022
	 **/
	private void setExternalIds() {
		Sequence objSequence = new Sequence('D2D Compensation');
		if (objSequence.canBeUsed()) {
			objSequence.startMutex();
			for (D2D_Compensation__c objD2DCompensation : newD2DCompensations) {
				objD2DCompensation.ExternalID__c = objSequence.incr(1, 8, false);
			}
			objSequence.endMutex();
		}
	}
}
