@isTest
private class TestCOM_DXLNotificationListener {
	private static final String COM_DXL_AFFECTED_OBJECTS = '[{"type":"Account","sfdcId":""},{"type":"BAN__c","sfdcId":""},{"type":"Financial_Account__c","sfdcId":""},{"type":"Billing_Arrangement__c","sfdcId":""},{"type":"Site__c","sfdcId":""},{"type":"Contact","sfdcId":""},{"type":"Contact","sfdcId":""}]';
	@testSetup
	static void setup() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		List<External_Account__c> externalAccounts = new List<External_Account__c>();

		External_account__c externalaccountcObj1 = new External_account__c(Account__c = acct.Id, External_Source__c = 'Unify');

		External_account__c externalaccountcObj2 = new External_account__c(Account__c = acct.Id, External_Source__c = 'BOP');

		externalAccounts.add(externalaccountcObj1);
		externalAccounts.add(externalaccountcObj2);

		insert externalAccounts;

		Contact testContact = new Contact(AccountId = acct.Id, Phone = '+31 111111789', LastName = 'Fixed', Userid__c = owner.Id);
		insert testContact;

		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, testContact.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);
		TestUtils.createSite(acct);

		Opportunity testOpp = CS_DataTest.createOpportunity(acct, 'Test Opp', UserInfo.getUserId());
		testOpp.csordtelcoa__Change_Type__c = 'Change';
		testOpp.Name = 'testOpp';
		testOpp.StageName = 'Ready for Order';
		testOpp.CloseDate = System.today();
		testOpp.AccountId = acct.Id;
		insert testOpp;

		cscfga__Product_Basket__c testBasket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
		testBasket.Name = 'testBasket';
		testBasket.cscfga__Opportunity__c = testOpp.Id;
		insert testBasket;

		Com_dxl_integration_log__c comdxlintegrationlogcObj = new Com_dxl_integration_log__c(
			Transaction_Id__c = 'TR-123456',
			Type__c = 'createCustomer',
			Status__c = 'Success',
			Affected_Objects__c = updateAffectedObjects(COM_DXL_AFFECTED_OBJECTS, acct, ban, fa, bar, testContact),
			Error__c = 'Test Value'
		);
		insert comdxlintegrationlogcObj;
	}

	private static String updateAffectedObjects(
		String affectedObjectsTemplate,
		Account acct,
		Ban__c ban,
		Financial_Account__c fa,
		Billing_Arrangement__c bar,
		Contact cnt
	) {
		List<COM_DxlAffectedObjects> affectedObjects = (List<COM_DxlAffectedObjects>) JSON.deserializeStrict(
			affectedObjectsTemplate,
			List<COM_DxlAffectedObjects>.class
		);
		for (COM_DxlAffectedObjects affectedObject : affectedObjects) {
			if (affectedObject.type == 'Account') {
				affectedObject.sfdcId = acct.Id;
			} else if (affectedObject.type == 'BAN__c') {
				affectedObject.sfdcId = ban.Id;
			} else if (affectedObject.type == 'Financial_Account__c') {
				affectedObject.sfdcId = fa.Id;
			} else if (affectedObject.type == 'Billing_Arrangement__c') {
				affectedObject.sfdcId = bar.Id;
			} else if (affectedObject.type == 'Contact') {
				affectedObject.sfdcId = cnt.Id;
			}
		}

		return JSON.serialize(affectedObjects);
	}

	private static String updateAsyncResponse(String asyncResponseTemplate, Account acct, Contact cnt, Site__c site) {
		COM_DXLServiceAsyncResponse asyncResponse = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			asyncResponseTemplate,
			COM_DXLServiceAsyncResponse.class
		);

		for (COM_DXLServiceAsyncResponse.Contact responseContact : asyncResponse.payload.contacts) {
			responseContact.salesforceAccountId = acct.Id;
			responseContact.salesforceContactId = cnt.Id;
		}

		for (COM_DXLServiceAsyncResponse.Site responseSite : asyncResponse.payload.sites) {
			responseSite.salesforceAccountId = acct.Id;
			responseSite.salesforceSiteId = site.Id;
		}

		return JSON.serialize(asyncResponse);
	}

	@isTest
	static void testHandleCreateCustomerValidPayload() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/dxlnotificationListener';
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf(
			updateAsyncResponse(
				COM_DXLNotificationListenerService.generateAsyncReponse('COM_DXL_CREATE_CUSTOMER_ASYNC_RESPONSE'),
				acctList[0],
				contactList[0],
				siteList[0]
			)
		);

		RestContext.request = req;
		RestContext.response = res;

		COM_DXLNotificationListener.processNotification();

		Integer expected = 500;
		String msg = 'Expected:' + expected + ' Actual: ' + res.statusCode + ' Error msg: ' + res.responseBody.toString();
		System.assertEquals(expected, res.statusCode, msg);
	}

	@isTest
	static void testHandleCreateCustomerInvalidPayload() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];

		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/dxlnotificationListener';
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf(
			updateAsyncResponse(
					COM_DXLNotificationListenerService.generateAsyncReponse('COM_DXL_CREATE_CUSTOMER_ASYNC_RESPONSE'),
					acctList[0],
					contactList[0],
					siteList[0]
				)
				.remove('"transactionId":"TR-1657110353379",')
		);

		RestContext.request = req;
		RestContext.response = res;

		COM_DXLNotificationListener.processNotification();

		Integer expected = 400;
		String msg = 'Expected:' + expected + ' Actual: ' + res.statusCode + ' Error msg: ' + res.responseBody.toString();
		System.assertEquals(expected, res.statusCode, msg);
	}

	@isTest
	static void testHandleCreateBSSOrderValidPayload() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/dxlnotificationListener';
		req.httpMethod = 'POST';
		req.headers.put('transactionId', 'TR-123456');
		req.requestBody = Blob.valueOf(COM_DXLNotificationListenerService.generateAsyncReponse('COM_DXL_CREATE_BSS_ORDER_ASYNC_RESPONSE'));

		RestContext.request = req;
		RestContext.response = res;

		COM_DXLNotificationListener.processNotification();

		Integer expected = 500;
		String msg = 'Expected:' + expected + ' Actual: ' + res.statusCode + ' Error msg: ' + res.responseBody.toString();
		System.assertEquals(expected, res.statusCode, msg);
	}

	@isTest
	static void testHandleCreateBSSOrderInvalidPayload() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/dxlnotificationListener';
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf(COM_DXLNotificationListenerService.generateAsyncReponse('COM_DXL_CREATE_BSS_ORDER_ASYNC_RESPONSE'));

		RestContext.request = req;
		RestContext.response = res;

		COM_DXLNotificationListener.processNotification();

		Integer expected = 400;
		String msg = 'Expected:' + expected + ' Actual: ' + res.statusCode + ' Error msg: ' + res.responseBody.toString();
		System.assertEquals(expected, res.statusCode, msg);
	}

	@isTest
	static void testMockCreateCustomer() {
		String payload = COM_DXLNotificationListenerService.generateAsyncReponse('COM_DXL_CREATE_CUSTOMER_ASYNC_RESPONSE');
		COM_DXLServiceAsyncResponse asyncResponse = (COM_DXLServiceAsyncResponse) JSON.deserialize(payload, COM_DXLServiceAsyncResponse.class);

		RestRequest req = new RestRequest();
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf(JSON.serialize(asyncResponse));
		req.headers.put('mockType', 'createCustomer');
		RestContext.request = req;

		String result = COM_DXLNotificationListener.getRequestBody(req);

		COM_DXLServiceAsyncResponse asyncResponseResult = (COM_DXLServiceAsyncResponse) JSON.deserialize(result, COM_DXLServiceAsyncResponse.class);

		String msg = 'Expected:' + asyncResponse.payload.transactionId + ' Actual: ' + asyncResponseResult.payload.transactionId;
		System.assertEquals(asyncResponse.payload.transactionId, asyncResponseResult.payload.transactionId, msg);
	}

	@isTest
	static void testMockCreateBSSOrder() {
		String payload = COM_DXLNotificationListenerService.generateAsyncReponse('COM_DXL_CREATE_BSS_ORDER_ASYNC_RESPONSE');
		COM_DXLCreateBSSOrderAsyncResponse asyncResponse = (COM_DXLCreateBSSOrderAsyncResponse) JSON.deserialize(
			payload,
			COM_DXLCreateBSSOrderAsyncResponse.class
		);

		RestRequest req = new RestRequest();
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf(JSON.serialize(asyncResponse));
		req.headers.put('mockType', 'createBSSOrder');
		RestContext.request = req;

		String result = COM_DXLNotificationListener.getRequestBody(req);

		COM_DXLCreateBSSOrderAsyncResponse asyncResponseResult = (COM_DXLCreateBSSOrderAsyncResponse) JSON.deserialize(
			result,
			COM_DXLCreateBSSOrderAsyncResponse.class
		);

		String msg =
			'Expected:' +
			asyncResponse.payload.orderLines[0].componentAPID +
			' Actual: ' +
			asyncResponseResult.payload.orderLines[0].componentAPID;
		System.assertEquals(asyncResponse.payload.orderLines[0].componentAPID, asyncResponseResult.payload.orderLines[0].componentAPID, msg);
	}
}
