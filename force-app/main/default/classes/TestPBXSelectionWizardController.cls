/**
 * @description		This is the test class for TestPBXSelectionWizardController class. 
 * @author        	Guy Clairbois
 */
@isTest
private class TestPBXSelectionWizardController {

    static testMethod void TestPBXSelection(){
		
		Account a = TestUtils.createAccount(null);
		Site__c s = TestUtils.createSite(a);
		
		List<PBX_Type__c> pbxs = new List<PBX_Type__c>();
		
		for(Integer i=0;i<5;i++){
			PBX_Type__c pbx = new PBX_Type__c(Vendor__c='testVendor',Product_Name__c='testProduct'+i,Software_Version__c='1.'+i);	
			pbxs.add(pbx);
		}
		insert pbxs;
		
		Test.startTest();
		
		PageReference pageRef = Page.PBXSelectionWizard;
		Test.setCurrentPage(pageRef);

		//instantiate the standard controller
		Apexpages.StandardController sc = new Apexpages.standardController(s);
		
		//Instantiate and construct the controller class and pass the standard controller to the extension.   
		PBXSelectionWizardController controller = new PBXSelectionWizardController(sc);

		controller.getSite();
		controller.getVendors();
		controller.getProductNames();
		controller.getSoftwareVersions();
		controller.setVendorSelected('testVendor');
		controller.setProductNameSelected('testProduct0');
		controller.setSoftwareVersionSelected('1.0');
		controller.getCurrentPBX();
		
		PageReference nextPage = controller.doSave();
		
		test.stopTest();
		
    }

}