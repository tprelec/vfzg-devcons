@isTest
public with sharing class TestOrderEntryDiscountTriggerHandler {
	@isTest
	public static void testAutoNumbering() {
		ExternalIDNumber__c custSetting = new ExternalIDNumber__c(
			Name = 'OE_Discount',
			External_Number__c = 1,
			Object_Prefix__c = 'OED-50-',
			blocked__c = false,
			runningOrg__c = '00D1l0000008i1vEAA'
		);
		insert custSetting;

		String orgId = UserInfo.getOrganizationId();

		Test.startTest();
		OrderEntryTestFactory.createOEDiscount('De eerste maand Safe Online XL gratis', 'Percentage', 100, 'Bundle', true);
		Test.stopTest();

		OE_Discount__c result = [SELECT Id, External_Id__c FROM OE_Discount__c];
		if (custSetting.runningOrg__c == orgId) {
			System.assertEquals(
				'OED-50-000002',
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		} else {
			System.assertEquals(
				null,
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		}
	}
}