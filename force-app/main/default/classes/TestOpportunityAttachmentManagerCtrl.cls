/*
 *  @Description:   Test class for OpportunityAttachmentManagerController
 *  @Author:        Guy Clairbois
 */
@isTest
private class TestOpportunityAttachmentManagerCtrl {

    static void test_method_no_opp() {

        // go to screen
        PageReference pr = new PageReference('/apex/OpportunityAttachmentManager');
        Test.setCurrentPage(pr);

        // open attachmentmgr
        OpportunityAttachmentManagerController vfcamc = new OpportunityAttachmentManagerController();
    }
    
    @isTest 
    static void test_method_success() {
        // create contract
        TestUtils.createCompleteOpportunity();

        // add existing attachment
        Opportunity_Attachment__c oa = new Opportunity_Attachment__c();
        oa.Attachment_Type__c = 'Contract (Signed)';
        oa.Opportunity__c = TestUtils.theOpportunity.Id;
        insert oa;

        Blob xmlBlob = Blob.valueOf('Hello World');
        ContentVersion cv = new ContentVersion();
        cv.Title = 'Provisining XML';
        cv.PathOnClient = 'Provisining_XML.xml';
        cv.VersionData = xmlBlob;
        insert cv;      

        Test.startTest();
        // go to screen
        PageReference pr = new PageReference('/apex/OpportunityAttachmentManager?oppId='+TestUtils.theOpportunity.Id);
        Test.setCurrentPage(pr);

        // open attachmentmgr with standardcontroller
        Apexpages.StandardController sc = new Apexpages.standardController(testUtils.theOpportunity);
        OpportunityAttachmentManagerController vfcamc = new OpportunityAttachmentManagerController(sc);
        

        // add more attachments on screen
        vfcamc.addMore();

        // add attachment
        OpportunityAttachmentManagerController.AttachmentWrapper aw = vfcamc.newOpportunityAttachments[0];
        vfcamc.opportunityAttachments = new List<OpportunityAttachmentManagerController.AttachmentWrapper>();
        aw.oa = oa.clone();
        aw.file = cv.clone();

        // save
        vfcamc.saveAttachments();

        // load options dropdown
        List<SelectOption> options = vfcamc.signedContractOptions;

        // delete attachment
        Id caToDelete = [Select Id From Opportunity_Attachment__c Where Opportunity__c = :testUtils.theOpportunity.Id Limit 1][0].Id;
        vfcamc.SelectedOaId = caToDelete;
        vfcamc.DeleteOpportunityAttachment();

        // return to opportunity
        vfcamc.backToOpportunity();

        // verify that 1 attachment remains
        List<Opportunity_Attachment__c> caRemaining = [Select Id From Opportunity_Attachment__c Where Opportunity__c = :testUtils.theOpportunity.Id];

        system.AssertEquals(1,caRemaining.size());

        FeedItem fi = aw.getHiddenFeedItem();
        pr = vfcamc.manageAttachments();
        pr = vfcamc.nothing();
        Test.stopTest();
    }
    
    
}