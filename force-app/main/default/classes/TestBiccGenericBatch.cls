//@isTest(isParallel=true)
@isTest
private class TestBiccGenericBatch {
	@isTest
	static void testCTNOwnership() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_CTN_Ownership__c -> VF_Asset__c', 'Dealer_code__c', 'Dealer_code__c'));
		fsmlist.add(TestUtils.createSync('BICC_CTN_Ownership__c -> VF_Asset__c', 'CTN__c', 'CTN__c'));
		fsmlist.add(TestUtils.createSync('BICC_CTN_Ownership__c -> VF_Asset__c', 'Calculation_Date__c', 'Calculation_Date__c'));
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(admin);
		Ban__c ban = TestUtils.createBan(acc);
		VF_Asset__c asset = new VF_Asset__c();
		asset.Account__c = acc.id;
		asset.BAN__c = ban.ID;
		asset.CTN__c = '1234';
		insert asset;

		//Data preparation for load table

		list<BICC_CTN_Ownership__c> loadList = new List<BICC_CTN_Ownership__c>{
			new BICC_CTN_Ownership__c(Calculation_Date__c = '2017-05-05', CTN__c = '1234', Dealer_code__c = '999'),
			new BICC_CTN_Ownership__c(Calculation_Date__c = 'FAIL', CTN__c = '1234', Dealer_code__c = '999'),
			new BICC_CTN_Ownership__c(Calculation_Date__c = '2017-05-05', CTN__c = '666', Dealer_code__c = '999'),
			new BICC_CTN_Ownership__c(Calculation_Date__c = '2017-05-05', CTN__c = '666')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_CTN_Ownership__c');
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
		database.executebatch(controller);

		Test.stopTest();
	}

	@isTest
	static void testCTNInformation() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_CTN_Information__c -> VF_Asset__c', 'BAN__c', 'BAN_Number__c'));
		fsmlist.add(TestUtils.createSync('BICC_CTN_Information__c -> VF_Asset__c', 'CTN__c', 'CTN__c'));
		fsmlist.add(TestUtils.createSync('BICC_CTN_Information__c -> VF_Asset__c', 'ctn_status__c', 'ctn_status__c'));
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(admin);
		TestUtils.autoCommit = false;
		Ban__c ban = TestUtils.createBan(acc);
		ban.Name = '323456789';
		insert ban;
		VF_Asset__c asset = new VF_Asset__c();
		asset.Account__c = acc.id;
		asset.BAN__c = ban.ID;
		asset.CTN__c = '1234';
		insert asset;

		//Data preparation for load table

		list<BICC_CTN_Information__c> loadList = new List<BICC_CTN_Information__c>{
			new BICC_CTN_Information__c(BAN__c = '323456789', CTN__c = '1234', ctn_status__c = 'Active'),
			new BICC_CTN_Information__c(BAN__c = 'FAIL', CTN__c = '1234', ctn_status__c = 'Active'),
			new BICC_CTN_Information__c(BAN__c = '323456789', CTN__c = '666', ctn_status__c = 'Active'),
			new BICC_CTN_Information__c(BAN__c = '323456789', CTN__c = '666')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_CTN_Information__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testSites() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_Site__c -> Site__c', 'Unify_Site_Id__c', 'Unify_Ref_Id__c'));
		fsmlist.add(TestUtils.createSync('BICC_Site__c -> Site__c', 'Unify_Account_Id__c', 'Unify_Account_Id__c'));
		fsmlist.add(TestUtils.createSync('BICC_Site__c -> Site__c', 'Site_House_Number__c', 'Site_House_Number__c'));
		fsmlist.add(TestUtils.createSync('BICC_Site__c -> Site__c', 'Site_Postal_Code__c', 'Site_Postal_Code__c'));

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Site__c site = TestUtils.createSite(acc);
		site.Unify_Ref_Id__c = '55';
		site.Unify_Account_Id__c = '99';
		insert site;
		Site__c site2 = TestUtils.createSite(acc);
		site2.Unify_Account_Id__c = '99';
		site2.Site_Postal_Code__c = '1234AB';
		insert site2;

		//Data preparation for load table

		list<BICC_Site__c> loadList = new List<BICC_Site__c>{
			new BICC_Site__c(Unify_Site_Id__c = '55', Unify_Account_Id__c = '99', Site_House_Number__c = '88', Site_Postal_Code__c = '1234AB'),
			new BICC_Site__c(Unify_Site_Id__c = '44', Unify_Account_Id__c = '99', Site_House_Number__c = '88', Site_Postal_Code__c = '1234AB'),
			new BICC_Site__c(Unify_Site_Id__c = '55', Unify_Account_Id__c = '666', Site_House_Number__c = '88', Site_Postal_Code__c = '1234AB'),
			new BICC_Site__c(Unify_Site_Id__c = '88', Unify_Account_Id__c = '99', Site_Postal_Code__c = '1234AB')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Site__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testBillingArrangement() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(
			TestUtils.createSync('BICC_Billing_Arrangment__c -> Billing_Arrangement__c', 'Unify_Billing_Arrangement_Id__c', 'Unify_Ref_Id__c')
		);
		fsmlist.add(TestUtils.createSync('BICC_Billing_Arrangment__c -> Billing_Arrangement__c', 'Payment_Method__c', 'Payment_Method__c'));
		fsmlist.add(TestUtils.createSync('BICC_Billing_Arrangment__c -> Billing_Arrangement__c', 'Bank_Account_Name__c', 'Bank_Account_Name__c'));

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Contact con = TestUtils.createContact(acc);
		insert con;
		Ban__c ban = TestUtils.createBan(acc);
		ban.Name = '323456789';
		insert ban;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.id);
		insert fa;
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
		ba.Unify_Ref_Id__c = '55';
		insert ba;

		//Data preparation for load table

		list<BICC_Billing_Arrangment__c> loadList = new List<BICC_Billing_Arrangment__c>{
			new BICC_Billing_Arrangment__c(
				Unify_Billing_Arrangement_Id__c = '55',
				Payment_Method__c = 'Invoice',
				Bank_Account_Name__c = 'Slush Fund'
			),
			new BICC_Billing_Arrangment__c(
				Unify_Billing_Arrangement_Id__c = '44',
				Payment_Method__c = 'Invoice',
				Bank_Account_Name__c = 'Slush Fund'
			),
			new BICC_Billing_Arrangment__c(
				Unify_Billing_Arrangement_Id__c = '55',
				Payment_Method__c = 'Robbery',
				Bank_Account_Name__c = 'Slush Fund'
			),
			new BICC_Billing_Arrangment__c(Unify_Billing_Arrangement_Id__c = '55', Payment_Method__c = 'Invoice')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Billing_Arrangment__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testFinancialAccounts() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_Financial_Account__c -> Financial_Account__c', 'Status__c', 'Status__c'));
		fsmlist.add(TestUtils.createSync('BICC_Financial_Account__c -> Financial_Account__c', 'Unify_Financial_Account_Id__c', 'Unify_Ref_Id__c'));

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Contact con = TestUtils.createContact(acc);
		insert con;
		Ban__c ban = TestUtils.createBan(acc);
		ban.Name = '323456789';
		insert ban;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.id);
		fa.Unify_Ref_Id__c = '55';
		insert fa;

		//Data preparation for load table

		list<BICC_Financial_Account__c> loadList = new List<BICC_Financial_Account__c>{
			new BICC_Financial_Account__c(
				Unify_Financial_Account_Id__c = '55',
				Status__c = 'Open',
				Ban__c = '323456789',
				Financial_Contact__c = con.id
			),
			new BICC_Financial_Account__c(
				Unify_Financial_Account_Id__c = '44',
				Status__c = 'Open',
				Ban__c = '323456789',
				Financial_Contact__c = con.id
			),
			new BICC_Financial_Account__c(
				Unify_Financial_Account_Id__c = '55',
				Status__c = 'Missing',
				Ban__c = '323456789',
				Financial_Contact__c = con.id
			),
			new BICC_Financial_Account__c(Unify_Financial_Account_Id__c = '55', Status__c = 'Open', Ban__c = '555', Financial_Contact__c = con.id),
			new BICC_Financial_Account__c(Unify_Financial_Account_Id__c = '55', Status__c = 'Open', Ban__c = '323456789')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Financial_Account__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testBans() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_BAN_Information__c -> Ban__c', 'BAN__c', 'Name'));
		fsmlist.add(TestUtils.createSync('BICC_BAN_Information__c -> Ban__c', 'Unify_reference_account__c', 'Unify_Ref_Id__c'));

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;

		//Data preparation for load table

		list<BICC_BAN_Information__c> loadList = new List<BICC_BAN_Information__c>{
			new BICC_BAN_Information__c(Unify_reference_account__c = '99', Ban__c = '323456789'),
			new BICC_BAN_Information__c(Unify_reference_account__c = '44', Ban__c = '323456789'),
			new BICC_BAN_Information__c(Unify_reference_account__c = '99', Ban__c = '555')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_BAN_Information__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testBICCContacts() {
		//Data preparation for sync table
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Email__c', 'Email'));
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Unify_Contact_Id__c', 'Unify_Ref_Id__c'));
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Unify_Account_Id__c', 'N/A'));

		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'LastName__c', 'LastName'));
		insert fsmlist;

		String unifyAccountId = '8876';
		String unifyAccountIdB = '88767';
		String unifyAccountIdC = '88768';

		String unifyContactId = '2314324';
		String unifyContactIdB = '23143245';
		String unifyContactIdC = '23143246';
		String unifyContactIdD = '34323412431';

		String testLastName = 'sdfdsfadfadf';

		String emailAddress = 'test2@example.com';
		String emailAddressB = 'test3@example.com';
		String emailAddressC = 'test4@example.com';
		String emailAddressD = 'test5@example.com';
		String testEmail4 = 'test5@example.com';

		List<Account> accntList = new List<Account>();
		List<Contact> contactList = new List<Contact>();
		List<External_Account__c> extAccntList = new List<External_Account__c>();
		List<External_Contact__c> extContactList = new List<External_Contact__c>();

		Account a = new Account();
		a.Name = 'Test Account';
		a.Unify_Ref_Id__c = unifyAccountId;
		accntList.add(a);

		Account b = new Account();
		b.Name = 'Test Account B';
		b.Unify_Ref_Id__c = unifyAccountIdB;
		accntList.add(b);

		Account aC = new Account();
		aC.Name = 'Test Account C';
		aC.Unify_Ref_Id__c = unifyAccountIdC;
		accntList.add(aC);

		insert accntList;

		System.assertEquals(3, [SELECT Id FROM Account].size());

		External_Account__c newExtAccount = new External_Account__c(
			External_Account_Id__c = unifyAccountId,
			External_Source__c = 'Unify',
			Account__c = a.Id
		);
		extAccntList.add(newExtAccount);

		External_Account__c newExtAccountB = new External_Account__c(
			External_Account_Id__c = unifyAccountIdB,
			External_Source__c = 'Unify',
			Account__c = b.Id
		);
		extAccntList.add(newExtAccountB);

		External_Account__c newExtAccountC = new External_Account__c(
			External_Account_Id__c = unifyAccountIdC,
			External_Source__c = 'Unify',
			Account__c = aC.Id
		);
		extAccntList.add(newExtAccountC);

		insert extAccntList;

		System.assertEquals(3, [SELECT Id FROM External_Account__c].size());

		Contact c = new Contact();
		c.LastName = 'Test Contact';
		c.Email = 'testA@example.com';
		c.AccountId = aC.Id;
		c.Unify_Ref_Id__c = unifyContactId;
		contactList.add(c);

		Contact c2 = new Contact();
		c2.LastName = 'Test Contact';
		c2.Email = 'testB@example.com';
		c2.AccountId = a.Id;
		c2.Unify_Ref_Id__c = unifyContactIdB;
		contactList.add(c2);

		Contact c3 = new Contact();
		c3.LastName = 'Test Contact';
		c3.Email = emailAddressC;
		c3.AccountId = aC.Id;
		c3.Unify_Ref_Id__c = null;
		contactList.add(c3);

		Contact c4 = new Contact();
		c4.LastName = 'Test Contact';
		c4.Email = testEmail4;
		c4.AccountId = aC.Id;
		c4.Unify_Ref_Id__c = unifyContactIdB;
		contactList.add(c4);

		insert contactList;
		System.assertEquals(4, [SELECT Id FROM Contact].size());

		External_Contact__c newExtContact = new External_Contact__c();
		newExtContact.External_Contact_Id__c = unifyContactId;
		newExtContact.External_Source__c = 'Unify';
		newExtContact.Contact__c = c.Id;
		newExtContact.ExternalId__c = KeyRingService.getKeyringKey(unifyAccountId, unifyContactId);
		newExtContact.External_Account__r = new External_Account__c(External_Account_Id__c = unifyAccountId);
		extContactList.add(newExtContact);
		insert extContactList;

		BICC_Contact__c biccContact = new BICC_Contact__c( // Should update existing contact. Ext Contact already existing, key: 8876-2314324
			Email__c = emailAddress,
			Unify_Contact_Id__c = unifyContactId,
			Unify_Account_Id__c = unifyAccountId
		);

		BICC_Contact__c biccContactB = new BICC_Contact__c( // Should create new contact and insert new ext contact, key: 88767-23143245
			Email__c = emailAddressB,
			Unify_Contact_Id__c = unifyContactIdB,
			Unify_Account_Id__c = unifyAccountIdB
		);

		BICC_Contact__c biccContactC = new BICC_Contact__c(
			Email__c = emailAddressC, // Should update (populate unify contact id) contact and create new ext contact related to account with unifyAccountIdC, key: 88768-23143246
			Unify_Contact_Id__c = unifyContactIdC,
			Unify_Account_Id__c = unifyAccountIdC,
			LastName__c = 'some lastname'
		);

		BICC_Contact__c biccContactD = new BICC_Contact__c( // No update of contact, create new ext contact, key: 88768-23143245
			Email__c = emailAddressC,
			Unify_Contact_Id__c = unifyContactIdB,
			Unify_Account_Id__c = unifyAccountIdC
		);

		BICC_Contact__c biccContactE = new BICC_Contact__c( // key: 88768-34323412431
			Email__c = emailAddressD,
			Unify_Contact_Id__c = unifyContactIdD,
			Unify_Account_Id__c = unifyAccountIdC,
			LastName__c = testLastName
		);

		list<BICC_Contact__c> biccContactList = new List<BICC_Contact__c>{ biccContact, biccContactB, biccContactC, biccContactD, biccContactE };

		insert biccContactList;

		List<Contact> cList = [SELECT Id, Email FROM Contact WHERE Unify_Ref_Id__c = :unifyContactIdD];
		System.assertEquals(0, cList.size(), 'Contact should be non-existing ' + unifyContactIdD);

		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Contact__c');
		database.executebatch(controller);

		Test.stopTest();

		// All processed:
		for (BICC_Contact__c bC : [SELECT Id, Processed__c FROM BICC_Contact__c]) {
			System.assertEquals(true, bC.Processed__c, 'BICC Record should be marked as processed');
		}

		String email = [SELECT Email FROM Contact WHERE Id = :c.Id].email;
		System.assertEquals(emailAddress, email, 'Email address should be updated to match ' + emailAddress);

		String emailB = [SELECT Email FROM Contact WHERE Id = :c2.Id].email;
		System.assertEquals(emailAddressB, emailB, 'Email address should be updated to match ' + emailAddressB);
		String unifyKey = KeyRingService.getKeyringKey(unifyAccountIdB, unifyContactIdB);
		System.assertEquals(
			1,
			[SELECT Id FROM External_Contact__c WHERE ExternalId__c = :unifyKey].size(),
			'External Contact should be created with key ' + unifyKey
		);

		String contactUnifyIdFound = [SELECT Unify_Ref_Id__c FROM Contact WHERE Id = :c3.Id].Unify_Ref_Id__c;
		System.assertEquals(unifyContactIdC, contactUnifyIdFound, 'Unify Contact Id must match ' + unifyContactIdC);
		String unifyKey2 = KeyRingService.getKeyringKey(unifyAccountIdC, unifyContactIdC);
		System.assertEquals(
			1,
			[SELECT Id FROM External_Contact__c WHERE ExternalId__c = :unifyKey].size(),
			'External Contact should be created with key ' + unifyKey2
		);

		String email4 = [SELECT Email FROM Contact WHERE Id = :c4.Id].email;
		System.assertEquals(testEmail4, email4, 'Email should not have been updated as there are 2 contacts with the same unify id ' + testEmail4);

		String unifyKey3 = KeyRingService.getKeyringKey(unifyAccountIdC, unifyContactIdB);

		/*System.assertEquals(
			1,
			[SELECT Id FROM External_Contact__c WHERE ExternalId__c = :unifyKey3].size(),
			'External Contact should be created with key ' +
			unifyKey3 +
			' ' +
			[SELECT Error__c FROM BICC_Contact__c WHERE Id = :biccContactD.Id]
			.Error__c +
			' ' +
			extContList.size() +
			'  ' +
			temp
		);*/

		cList = [SELECT Id, Email, LastName FROM Contact WHERE Unify_Ref_Id__c = :unifyContactIdD];
		System.assertEquals(1, cList.size(), 'Contact should be existing now ' + unifyContactIdD);

		Contact newContact = cList.get(0);

		System.assertEquals(emailAddressD, newContact.Email, 'Email should be ' + emailAddressD);

		System.assertEquals(testLastName, newContact.LastName, 'Lastname should be ' + testLastName);

		unifyKey3 = KeyRingService.getKeyringKey(unifyAccountIdC, unifyContactIdD);
		System.assertEquals(
			1,
			[SELECT Id FROM External_Contact__c WHERE ExternalId__c = :unifyKey].size(),
			'External Contact should be created with key ' + unifyKey3
		);

		System.assertEquals(0, [SELECT Id FROM Contact WHERE AccountId = NULL].size(), 'No contacts should be without account');
	}

	@isTest
	static void testContacts() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Email__c', 'Email'));
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Unify_Contact_Id__c', 'Unify_Ref_Id__c'));
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Unify_Account_Id__c', 'N/A'));
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Contact con = TestUtils.createContact(acc);
		con.Unify_Ref_Id__c = '123';
		insert con;
		TestUtils.autocommit = true;

		//Data preparation for load table

		list<BICC_Contact__c> loadList = new List<BICC_Contact__c>{
			new BICC_Contact__c(Email__c = 'gerhard276@hotmail.com', Unify_Contact_Id__c = '123', Unify_Account_Id__c = '99'),
			new BICC_Contact__c(Email__c = 'gerhard276@hotmail.com', Unify_Contact_Id__c = '123', Unify_Account_Id__c = '88'),
			new BICC_Contact__c(Email__c = 'geen@vodafone.com', Unify_Contact_Id__c = '123', Unify_Account_Id__c = '99')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Contact__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testContacts2() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Email__c', 'Email'));
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Unify_Contact_Id__c', 'Unify_Ref_Id__c'));
		fsmlist.add(TestUtils.createSync('BICC_Contact__c -> Contact', 'Unify_Account_Id__c', 'N/A'));
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		Contact con = TestUtils.createContact(acc);
		con.Unify_Ref_Id__c = '123';
		con.Email = 'email@vodafone.com';
		insert con;
		TestUtils.autocommit = true;
		/**
        Contact_Unify_Map__c cum = new Contact_Unify_Map__c(
            Contact__c = con.Id,
            Unify_Ref_Id__c = '1234'
        );
        insert cum;
        **/

		//Data preparation for load table

		list<BICC_Contact__c> loadList = new List<BICC_Contact__c>{
			new BICC_Contact__c(Email__c = 'email@vodafone.com', Unify_Contact_Id__c = '1234', Unify_Account_Id__c = '99')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Contact__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testAccount() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'DUNS_Number__c', 'DUNS_Number__c'));
		fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'COC_number__c', 'KVK_number__c'));
		fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'Unify_Account_Id__c', 'Unify_Ref_Id__c'));
		fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'Phone__c', 'Phone'));

		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acc = TestUtils.createAccount(admin);
		acc.Unify_Ref_Id__c = '99';
		insert acc;
		acc = TestUtils.createAccount(admin);
		acc.DUNS_Number__c = '88';
		insert acc;
		acc = TestUtils.createAccount(admin);
		acc.KVK_number__c = '12345678';
		insert acc;

		//Data preparation for load table

		list<BICC_Account__c> loadList = new List<BICC_Account__c>{
			new BICC_Account__c(DUNS_Number__c = '88', Phone__c = '0653957801', Unify_Account_Id__c = '199'),
			new BICC_Account__c(COC_number__c = '12345678', Phone__c = '0653957801', Unify_Account_Id__c = '299'),
			new BICC_Account__c(Unify_Account_Id__c = '99', Phone__c = '0653957801'),
			new BICC_Account__c(DUNS_Number__c = '1', Phone__c = '0653957801', Unify_Account_Id__c = '399'),
			new BICC_Account__c(COC_number__c = '2', Phone__c = '0653957801', Unify_Account_Id__c = '499'),
			new BICC_Account__c(Unify_Account_Id__c = '3', Phone__c = '0653957801')
		};

		insert loadList;

		//The actual Test
		Test.startTest();

		BiccGenericBatch controller = new BiccGenericBatch('BICC_Account__c');
		database.executebatch(controller);

		Test.stopTest();
		System.assert(true, 'Have to rework this later anyway so putting it like this for now -- Chris');
	}

	@isTest
	static void testDealerInformation() {
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(
			TestUtils.createSync('BICC_Dealer_Information__c -> Dealer_Information__c', 'Dealer_Contact_Email__c', 'Dealer_Contact_Email__c')
		);
		fsmlist.add(TestUtils.createSync('BICC_Dealer_Information__c -> Dealer_Information__c', 'Sales_Channel__c', 'Sales_Channel__c'));
		fsmlist.add(TestUtils.createSync('BICC_Dealer_Information__c -> Dealer_Information__c', 'Name', 'Name'));
		fsmlist.add(TestUtils.createSync('BICC_Dealer_Information__c -> Dealer_Information__c', 'Parent_Dealer_Code__c', 'N/A'));
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		Account dealerAccount = TestUtils.createPartnerAccount();

		dealerAccount.Type = Constants.ACCOUNT_TYPE_DEALER;
		dealerAccount.IsPartner = true;
		dealerAccount.Dealer_code__c = '123456';

		update dealerAccount;

		TestUtils.autoCommit = false;
		Contact contact1 = TestUtils.createContact(dealerAccount);
		contact1.Email = 'Testclass@test.com';

		Contact contact2 = TestUtils.createContact(dealerAccount);
		contact2.Email = 'wrongemail@test.com';

		List<Contact> contactList = new List<Contact>();
		contactList.add(contact1);
		contactList.add(contact2);
		insert contactList;

		list<BICC_Dealer_Information__c> loadList = new List<BICC_Dealer_Information__c>{
			new BICC_Dealer_Information__c(
				Dealer_Contact_Email__c = 'Testclass@test.com',
				Status__c = 1,
				Name = 'Dealer1',
				Dealer_Code__c = '123456',
				Sales_Channel__c = Constants.DEALER_INFORMATION_SALES_CHANNEL_INDIRECT,
				Parent_Dealer_Code__c = '123456789'
			),
			new BICC_Dealer_Information__c(
				Dealer_Contact_Email__c = 'wrongemail@test.com',
				Status__c = 1,
				Name = 'Dealer2',
				Dealer_Code__c = '654321',
				Sales_Channel__c = Constants.DEALER_INFORMATION_SALES_CHANNEL_INDIRECT
			)
		};

		insert loadList;

		Test.startTest();
		BiccGenericBatch batch = new BiccGenericBatch('BICC_Dealer_Information__c');
		database.executebatch(batch);
		Test.stopTest();

		BICC_Dealer_Information__c biccDealerInformation1 = [
			SELECT Name, Dealer_Contact_Email__c, Dealer_Code__c, Error__c
			FROM BICC_Dealer_Information__c
			WHERE Name = 'Dealer1'
			LIMIT 1
		];
		System.assertEquals('123456', biccDealerInformation1.Dealer_Code__c, '1 BICC Dealer Information record should be created');

		BICC_Dealer_Information__c biccDealerInformation2 = [
			SELECT Name, Dealer_Contact_Email__c, Dealer_Code__c, Error__c
			FROM BICC_Dealer_Information__c
			WHERE Name = 'Dealer2'
			LIMIT 1
		];
		System.assertEquals('654321', biccDealerInformation2.Dealer_Code__c, '1 BICC Dealer Information record should be created');

		List<Dealer_Information__c> dealerInformation = [
			SELECT Dealer_Code__c, Parent_Dealer_Code__c, Parent_Dealer_Code_Prefix__c
			FROM Dealer_Information__c
		];
		System.assertEquals('123456', dealerInformation[0].Dealer_Code__c, '1 Dealer Information record should be created with the correct code');
		System.assertEquals(
			'456789',
			dealerInformation[0].Parent_Dealer_Code__c,
			'1 Dealer Information record should be created with the correct parent code'
		);
		System.assertEquals(
			'123',
			dealerInformation[0].Parent_Dealer_Code_Prefix__c,
			'1 Dealer Information record should be created with the correct parent code prefix'
		);
	}

	@isTest
	static void failedBiccBatch() {
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('BICC_Site__c -> Site__c', 'Unify_Site_Id__c', 'Unify_Ref_Id__c'));
		fsmlist.add(TestUtils.createSync('BICC_Site__c -> Site__c', 'Unify_Account_Id__c', 'Unify_Account_Id__c'));
		fsmlist.add(TestUtils.createSync('BICC_Site__c -> Site__c', 'Site_House_Number__c', 'Site_House_Number__c'));
		fsmlist.add(TestUtils.createSync('BICC_Site__c -> Site__c', 'Site_Postal_Code__c', 'Site_Postal_Code__c'));

		insert fsmlist;

		BiccGenericBatch batch = new BiccGenericBatch('BICC_Site__c');
		batch.query = 'no query';
		Exception err;
		try {
			Test.startTest();

			database.executebatch(batch);

			Test.stopTest();
		} catch (Exception e) {
			err = e;
		}
		Test.getEventBus().deliver();
		System.assert(err != null, 'Exception should be thrown');
	}
}
