@isTest
public with sharing class TestCOM_OrderFinishObserver {
    @testSetup 
    static void setup() {

        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;

        Account testAccount = new Account();
        testAccount.Name = 'testAccount';
        testAccount.LG_ExternalID__c  = '1234456';
        insert testAccount;
            
        csconta__Billing_Account__c billingAcc = new csconta__Billing_Account__c();
        billingAcc.csconta__Account__c = testAccount.Id;
        billingAcc.LG_PaymentType__c = 'Bank Transfer';
        billingAcc.LG_HouseNumber__c = '12';
        billingAcc.LG_BillingAccountName__c = 'Test Billing Account';
        //billingAcc.Mandate_Reference__c = mandate.Id;
        billingAcc.LG_MandateStartDate__c = System.today();
        insert billingAcc;
            
        Opportunity testOpp = new Opportunity();
        testOpp.Name = 'testOpp';
        testOpp.StageName = 'Ready for Order';
        testOpp.CloseDate = System.today();
        testOpp.AccountId = testAccount.Id;
        insert testOpp;
            
        cscfga__Product_Basket__c testBasket = new cscfga__Product_Basket__c();
        testBasket.Name = 'testBasket';
        testBasket.cscfga__Opportunity__c = testOpp.Id;
        testBasket.LG_Selected_Billing_Account__c = billingAcc.id;
        insert testBasket;

        cscfga__Product_Definition__c businessInternetDef = new cscfga__Product_Definition__c();
        businessInternetDef.Name = 'Business Internet';
        businessInternetDef.cscfga__Description__c = 'Business Internet';
        businessInternetDef.Product_Type__c = 'Fixed';
        insert businessInternetDef;
        
        
        cscfga__Product_Configuration__c businessInternetConf = new cscfga__Product_Configuration__c();
        businessInternetConf.cscfga__Product_Definition__c = businessInternetDef.Id;
        businessInternetConf.cscfga__Product_Basket__c = testBasket.Id;
        insert businessInternetConf;
        
        csord__Order__c order = new csord__Order__c();
        order.csord__Identification__c = 'Order_'+String.valueOf(testBasket.Id);
        insert order;
           
        csord__Subscription__c businessInternetSub = new csord__Subscription__c();
        businessInternetSub.csord__Order__c = order.Id;
        businessInternetSub.csordtelcoa__Product_Configuration__c = businessInternetConf.Id;
        businessInternetSub.csord__Identification__c = 'Business_Internet_Sub_'+String.valueOf(testBasket.Id);
        businessInternetSub.csord__Status__c = 'Subscription created';
        insert businessInternetSub;    
    }
    
    static testMethod void testObserver() {

        List<csord__Subscription__c> subscriptions = [select Id from csord__Subscription__c];
        List<Id> subIds = new List<Id>();

        for(csord__Subscription__c subscription : subscriptions) {
            subIds.add(subscription.Id);
        }
        
        Test.startTest();
        COM_OrderFinishObserver.generateServiceSpecifications(subIds);
        Test.stopTest();
        List<Attachment> att = [Select Id from Attachment];

        System.assertEquals(1, att.size());

    }
}