public without sharing class CS_OppDocController {
	private static List<CS_DocItem> returnList = new List<CS_DocItem>();
	private static final String VAL_SEPARATOR = '|';
	private static final String DEFAULT_LANGUAGE = 'Dutch';

	@AuraEnabled
	public static List<CS_DocItem> getDocuments(Id objId) {
		String oppId = objId;
		if (objId == null) {
			throw new CS_OppDocControllerException('No Object Id is defined');
		}
		String sobjectType = String.valueOf(objId.getsobjecttype());
		if (sobjectType == 'Opportunity') {
			oppId = objId;
		}
		if (sobjectType == 'VF_Contract__c') {
			oppId = [SELECT Opportunity__c FROM VF_Contract__c WHERE Id = :objId].Opportunity__c;
			if (oppId == null) {
				return returnList; //W-3998: throw error and handle it in component
			}
		}
		Set<Id> agreementSet = new Set<Id>();
		Set<Id> applicableContractConditions = new Set<Id>();

		try {
			Opportunity opp = [
				SELECT
					Id,
					CloseDate,
					Direct_Indirect__c,
					Primary_Basket__c,
					Contract_Summary_Attachment_Id__c,
					(SELECT Id, Proposition__c, Category__c, Product2Id FROM OpportunityLineItems)
				FROM Opportunity
				WHERE Id = :oppId
			];
			Date validDateForContractConditions = opp.CloseDate != null ? opp.CloseDate : Date.today();
			String directIndrect = String.isNotEmpty(opp.Direct_Indirect__c) ? opp.Direct_Indirect__c : '';
			// W-3730/W-3954
			Id primaryBasket = opp.Primary_Basket__c;
			String docLanguage = DEFAULT_LANGUAGE;
			if (primaryBasket != null) {
				docLanguage = [SELECT Contract_Language__c FROM cscfga__Product_Basket__c WHERE Id = :primaryBasket].Contract_Language__c;
			}

			Set<String> propSet = new Set<String>();
			Set<String> categorySetSet = new Set<String>();
			Set<Id> productSet = new Set<Id>();
			for (OpportunityLineItem opli : opp.OpportunityLineItems) {
				if (String.isNotEmpty(opli.Proposition__c)) {
					propSet.add(opli.Proposition__c);
				}
				if (String.isNotEmpty(opli.Category__c)) {
					categorySetSet.add(opli.Category__c);
				}
				productSet.add(opli.Product2Id);
			}
			Set<Id> applibleContractConditionsFromProductAssociations = getContractConditionsFromProducts(productSet, validDateForContractConditions);

			List<csclm__Agreement__c> agreementList = [SELECT Id FROM csclm__Agreement__c WHERE csclm__Opportunity__c = :oppId];
			for (csclm__Agreement__c agree : agreementList) {
				agreementSet.add(agree.Id);
			}
			if (!agreementSet.isEmpty()) {
				List<Attachment> attachmentList = [
					SELECT Id, Name, CreatedDate
					FROM Attachment
					WHERE ParentId IN :agreementSet
					ORDER BY CreatedDate DESC
				];
				for (Attachment attach : attachmentList) {
					CS_DocItem di = new CS_DocItem();
					di.name = attach.Name;
					di.isContractSummary = false;
					di.isServiceDescription = false;
					di.isTariffDescription = false;
					di.docId = attach.Id;
					returnList.add(di);
				}
			}

			if (opp.Contract_Summary_Attachment_Id__c != null) {
				Attachment contractSummaryAtt = [SELECT Id, Name FROM Attachment WHERE Id = :opp.Contract_Summary_Attachment_Id__c LIMIT 1];

				if (contractSummaryAtt != null) {
					CS_DocItem di = new CS_DocItem();
					di.name = contractSummaryAtt.Name;
					di.isContractSummary = true;
					di.isServiceDescription = false;
					di.isTariffDescription = false;
					di.docId = opp.Contract_Summary_Attachment_Id__c;
					di.contractSummaryUrl = Site.getName() != null
						? '/partnerportal/servlet/servlet.FileDownload?file=' + opp.Contract_Summary_Attachment_Id__c + '&operationContext=S1'
						: '/servlet/servlet.FileDownload?file=' + opp.Contract_Summary_Attachment_Id__c + '&operationContext=S1';
					returnList.add(di);
				}
			}

			List<CS_Basket_Snapshot_Transactional__c> snapShotList = [
				SELECT Id, cs_Proposition_Scenario_Matrix__c
				FROM CS_Basket_Snapshot_Transactional__c
				WHERE Product_Basket__r.cscfga__Opportunity__c = :oppId AND Product_Basket__r.Primary__c = TRUE
			];

			List<Contract_Conditions__c> ccList = [
				SELECT Name, Channel__c, Proposition1__c, Type__c, URL_Link__c, Version_Number__c, Category__c, Proposition__c, Language__c
				FROM Contract_Conditions__c
				WHERE
					Start_Date__c <= :validDateForContractConditions
					AND End_Date__c >= :validDateForContractConditions
					AND (Channel__c = 'Direct/Indirect'
					OR Channel__c = :directIndrect)
					AND (Id IN :applibleContractConditionsFromProductAssociations
					OR Type__c = 'AVW'
					OR Proposition1__c IN :propSet
					OR Category__c IN :categorySetSet)
			];

			// W-3730/W-3954 TEMPORARY PATCH ...
			Map<String, String> ccCheckInMap = new Map<String, String>();
			Map<Id, CS_DocItem> docItemsMap = new Map<Id, CS_DocItem>();
			Map<String, Set<Id>> conflictingDocsMap = new Map<String, Set<Id>>();

			for (Contract_Conditions__c cc : ccList) {
				Boolean addRecord = false;
				if (cc.Type__c == 'AVW') {
					if (String.isEmpty(cc.Proposition1__c)) {
						addRecord = true;
					} else if (propSet.contains(cc.Proposition1__c)) {
						addRecord = true;
					}
				}

				if (propSet.contains(cc.Proposition1__c)) {
					/** Category check */
					if (String.isEmpty(cc.Category__c) || categorySetSet.contains(cc.Category__c)) {
						addRecord = true;
					}
				}

				if (applibleContractConditionsFromProductAssociations.contains(cc.Id)) {
					addRecord = true;
				}

				for (CS_Basket_Snapshot_Transactional__c snap : snapShotList) {
					if (!String.isEmpty(snap.cs_Proposition_Scenario_Matrix__c) && !String.isEmpty(cc.Proposition1__c)) {
						String matrix = snap.cs_Proposition_Scenario_Matrix__c.toLowerCase().deleteWhitespace();
						String prop = cc.Proposition1__c.toLowerCase().deleteWhitespace();
						if (matrix.contains(prop)) {
							addRecord = true;
						}
					}
				}

				// W-3730/W-3954 TEMPORARY PATCH ...
				if (cc.Language__c != DEFAULT_LANGUAGE && cc.Language__c != docLanguage && String.isNotEmpty(cc.Language__c)) {
					addRecord = false;
				}
				String mapKey = cc.Type__c + VAL_SEPARATOR + cc.Category__c + VAL_SEPARATOR + cc.Proposition__c + VAL_SEPARATOR + cc.Proposition1__c;
				Id mapNewValue = cc.Id;

				if (addRecord == true) {
					if (ccCheckInMap.containsKey(mapKey)) {
						Id mapOldValue = ccCheckInMap.get(mapKey);

						// If it is repeated, add it in the conflictingDocsMap in its corresponding conflictKey (=mapKey)
						// to carry a list of those that are potentially the same and then define which ones remain and which ones are not.
						Set<Id> idSet;
						if (!conflictingDocsMap.containsKey(mapKey)) {
							idSet = new Set<Id>();
						} else {
							idSet = conflictingDocsMap.get(mapKey);
						}

						idSet.add(mapOldValue);
						idSet.add(mapNewValue);
						conflictingDocsMap.put(mapKey, idSet);
					} else {
						ccCheckInMap.put(mapKey, mapNewValue);
					}
				}

				if (addRecord) {
					applicableContractConditions.add(cc.Id);
					CS_DocItem di = new CS_DocItem();
					di.contractConditionId = cc.Id;
					di.isContractSummary = false;
					di.isTariffDescription = false;
					di.isServiceDescription = false;
					di.name = cc.Name;
					di.language = cc.Language__c;
					switch on cc.Type__c {
						when 'Tarievenoverzicht' {
							di.isTariffDescription = true;
						}
						when else {
							di.isServiceDescription = true;
						}
					}
					di.serviceDescriptionUrl = cc.URL_Link__c;
					docItemsMap.put(cc.Id, di);
				}
			}
			ccCheckInMap.clear();
			if (docLanguage != DEFAULT_LANGUAGE) {
				List<CS_DocItem> filterResultList = filterConflictingDocs(docLanguage, conflictingDocsMap, docItemsMap);
				returnList.addAll(filterResultList);
			} else {
				returnList.addAll(docItemsMap.values());
			}
			// END ... W-3730/W-3954
			linkDocumentsToDocumentItems(applicableContractConditions);
		} catch (Exception e) {
			System.debug(LoggingLevel.DEBUG, 'Exception: ' + e.getMessage() + e.getStackTraceString());
		}
		return returnList;
	}

	// Take the conflicting docs, decides whether they should be on the list or not based on the products they represent and the language they have.
	@testVisible
	private static List<CS_DocItem> filterConflictingDocs(
		String ctrLanguage,
		Map<String, Set<Id>> conflictingDocSetMap,
		Map<Id, CS_DocItem> docItems
	) {
		Set<Id> contrCondIdSet = new Set<Id>();
		for (Set<Id> sCurrSet : conflictingDocSetMap.values()) {
			contrCondIdSet.addAll(sCurrSet);
		}
		Map<Id, Set<Id>> contractProductMap = getProductsFromContractConditions(contrCondIdSet);

		Map<String, Id> oldKeyPlusProdIdMap = new Map<String, Id>();
		for (String curConflictSetKey : conflictingDocSetMap.keySet()) {
			// Iterate over the Set of the curConflictSetKey value, take contractProduct map from the value of curConflictSetKey;
			// add to curConflictSetKey the products2 separated by ','; If it exists, determine if should be replaced or not.
			// If so, delete the right value in docItems.
			Set<Id> idIteratorSet = conflictingDocSetMap.get(curConflictSetKey);
			String keySuffix;
			for (Id currContractCondId : idIteratorSet) {
				if (contractProductMap.containsKey(currContractCondId)) {
					keySuffix = String.join((Iterable<String>) contractProductMap.get(currContractCondId), ',');
				} else {
					keySuffix = '';
				}
				String completeKey = curConflictSetKey + VAL_SEPARATOR; // + keySuffix;
				if (!oldKeyPlusProdIdMap.containsKey(completeKey)) {
					oldKeyPlusProdIdMap.put(completeKey, currContractCondId);
				} else {
					// Determine if the current value should be replaced or not.
					Id oldDocId = oldKeyPlusProdIdMap.get(completeKey);
					CS_DocItem oldDocItem = docItems.get(oldDocId);
					// Delete in docItems the wrong value.
					if (oldDocItem.language == ctrLanguage) {
						docItems.remove(currContractCondId);
					} else {
						CS_DocItem currDocItem = docItems.get(currContractCondId);
						if (currDocItem.language == ctrLanguage) {
							oldKeyPlusProdIdMap.put(completeKey, currContractCondId);
							docItems.remove(oldDocItem.contractConditionId);
						}
					}
				}
			}
		}
		return docItems.values();
	}

	private static Map<Id, Set<Id>> getProductsFromContractConditions(Set<Id> contractConditionsIdSet) {
		Map<Id, Set<Id>> contProdMap = new Map<Id, Set<Id>>();
		for (Contract_Condition_Product_Association__c currRec : [
			SELECT Contract_Conditions__c, Product2__c
			FROM Contract_Condition_Product_Association__c
			WHERE Contract_Conditions__c IN :contractConditionsIdSet
		]) {
			Set<Id> crProductSet;
			if (!contProdMap.containsKey(currRec.Contract_Conditions__c)) {
				crProductSet = new Set<Id>();
			} else {
				crProductSet = contProdMap.get(currRec.Contract_Conditions__c);
			}
			crProductSet.add(currRec.Product2__c);
			contProdMap.put(currRec.Contract_Conditions__c, crProductSet);
		}
		return contProdMap;
	}

	@testVisible
	private static Set<Id> getContractConditionsFromProducts(Set<Id> products, Date validDateForContractConditions) {
		List<Contract_Condition_Product_Association__c> contractConditionProductAssociations = [
			SELECT Contract_Conditions__c
			FROM Contract_Condition_Product_Association__c
			WHERE
				Product2__c IN :products
				AND Contract_Conditions__r.Start_Date__c <= :validDateForContractConditions
				AND Contract_Conditions__r.End_Date__c >= :validDateForContractConditions
		];

		Set<Id> applicableContractConditions = new Set<Id>();
		for (Contract_Condition_Product_Association__c junctionObject : contractConditionProductAssociations) {
			applicableContractConditions.add(junctionObject.Contract_Conditions__c);
		}

		return applicableContractConditions;
	}

	@testVisible
	private static void linkDocumentsToDocumentItems(Set<Id> applicableContractConditions) {
		Map<Id, List<Id>> applicableContractConditionsToContentVersions = new Map<Id, List<Id>>();
		for (ContentDocumentLink cdl : [
			SELECT
				LinkedEntityId,
				ContentDocument.LatestPublishedVersionId,
				ContentDocument.FileExtension,
				ContentDocument.LatestPublishedVersion.VersionNumber
			FROM ContentDocumentLink
			WHERE LinkedEntityId IN :applicableContractConditions AND ContentDocument.FileExtension = 'pdf'
			ORDER BY ContentDocument.LatestPublishedVersion.VersionNumber DESC
		]) {
			if (!applicableContractConditionsToContentVersions.containsKey(cdl.LinkedEntityId)) {
				applicableContractConditionsToContentVersions.put(cdl.LinkedEntityId, new List<Id>{ cdl.ContentDocument.LatestPublishedVersionId });
			}
		}

		List<CS_DocItem> toAdd = new List<CS_DocItem>();
		for (CS_DocItem docItem : returnList) {
			if (docItem.contractConditionId == null) {
				continue;
			}
			List<Id> contentVersionsToApply = applicableContractConditionsToContentVersions.get(docItem.contractConditionId);

			if (contentVersionsToApply == null || contentVersionsToApply.isEmpty()) {
				docItem.serviceDescriptionUrl = ''; // TODO: W-3998: throw error and handle it in component
				continue;
			} else {
				docItem.serviceDescriptionUrl = createContentVersionDownloadLink(contentVersionsToApply[0]);
				continue;
			}
		}
	}

	@testVisible
	private static String createContentVersionDownloadLink(Id contentVersion) {
		String baseURL = System.URL.getSalesforcebaseUrl().toExternalForm();
		String pathToContentVersionDownload = '/sfc/servlet.shepherd/version/download/' + contentVersion + '?operationContext=S1';
		if (!GeneralUtils.currentUserIsPartnerUser()) {
			return baseURL + pathToContentVersionDownload;
		}
		String pathFromCurrentURL = System.URL.getCurrentRequestUrl().getPath();
		String communityName = String.isNotBlank(pathFromCurrentURL) ? '/' + pathFromCurrentURL.split('/')[1] : '';
		return baseURL + communityName + pathToContentVersionDownload;
	}

	public class CS_OppDocControllerException extends Exception {
	}
}
