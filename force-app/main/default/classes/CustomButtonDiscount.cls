global with sharing class CustomButtonDiscount extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		try {
			String newUrl = CustomButtonDiscount.redirectToDiscount(basketId);
			return '{"status":"ok","redirectURL":"' + newUrl + '"}';
		} catch (System.CalloutException e) {
			System.debug('ERROR:' + e);
			return '{"status":"error"}';
		}
	}

	// Set url for redirect after action
	public static String redirectToDiscount(String basketId) {
		PageReference editPage = new PageReference('/apex/csdiscounts__DiscountPage?basketId=' + basketId);

		Id profileId = UserInfo.getProfileId();
		String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;
		if (profileName == 'VF Partner Portal User') {
			editPage = new PageReference('/partnerportal/apex/csdiscounts__DiscountPage?basketId=' + basketId);
		}

		return editPage.getUrl();
	}
}
