public with sharing class COM_DXL_CustomerDetails {
	JSONGenerator generator;
	List<Opportunity> opportunityData;
	Map<Id, List<External_Account__c>> accountsWithExternalAccounts;

	public COM_DXL_CustomerDetails(
		JSONGenerator generator,
		List<Opportunity> opportunityData,
		Map<Id, List<External_Account__c>> accountsWithExternalAccounts
	) {
		this.generator = generator;
		this.opportunityData = opportunityData;
		this.accountsWithExternalAccounts = accountsWithExternalAccounts;
	}

	public void generateCustomerDetails() {
		generator.writeFieldName('customerDetails');
		generator.writeStartObject();

		generateCompanyDetails();
		generateBillingCustomer();
		generateCompanyAddress();
		generateAccountManager();

		generator.writeEndObject();
	}

	private void generateCompanyDetails() {
		generator.writeFieldName('companyDetails');
		generator.writeStartObject();
		generator.writeStringField(
			'unifyAccountRefId',
			getExternalAccountExternalId(accountsWithExternalAccounts.get(opportunityData[0].AccountId), COM_DXL_Constants.UNIFY_EXTERNAL_SYSTEM)
		);
		generator.writeStringField(
			COM_DXL_Constants.SFDC_ACCOUNT_ID,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].AccountId)
		);
		generator.writeStringField('kvk', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.KVK_number__c));
		generator.writeStringField(
			'establishDate',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(String.valueOf(opportunityData[0].Account.date_of_establishment__c))
		);
		generator.writeStringField(
			'legalEntity',
			COM_DXL_CreateCustomerService.getDXLMapping(
				COM_DXL_Constants.COM_DXL_LEGAL_ENTITY_MAPPING,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Legal_Structure_Code__c)
			)
		);
		generator.writeStringField(
			'numOfEmployees',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(String.valueOf(opportunityData[0].Account.NumberOfEmployees))
		);
		generator.writeStringField('companyName', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Name));
		generator.writeStringField(
			'accountType',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Unify_Account_Type__c)
		);
		generator.writeStringField(
			'accountSubType',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Unify_Account_SubType__c)
		);
		generator.writeStringField('fax', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Fax));
		generator.writeStringField(
			'phone',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Authorized_to_sign_1st__r.Phone)
		);
		generator.writeStringField('dunsNo', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.duns_number__c));
		generator.writeStringField(
			'BOPCompanyCode',
			getExternalAccountExternalId(
				accountsWithExternalAccounts.get(opportunityData[0].AccountId),
				COM_DXL_Constants.COM_DXL_EXTERNAL_SOURCE_BOP
			)
		);
		generator.writeEndObject();
	}

	private void generateBillingCustomer() {
		generator.writeFieldName('billingCustomer');
		generator.writeStartObject();
		generator.writeStringField(
			'unifyBillingCustomerRefId',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Ban__r.Unify_Ref_Id__c)
		);
		generator.writeStringField('banNumber', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Ban__r.BAN_Number__c));
		generator.writeStringField('customerName', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Ban__r.BAN_Name__c));
		generator.writeStringField(
			'customerType',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Ban__r.Unify_Customer_Type__c)
		);
		generator.writeStringField(
			'customerSubType',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Ban__r.Unify_Customer_SubType__c)
		);
		generator.writeEndObject();
	}

	@TestVisible
	private void generateCompanyAddress() {
		generator.writeFieldName('companyAddress');
		Map<String, String> addressData = generateAddressData(opportunityData[0]);
		COM_DXL_Address dxlAddress = new COM_DXL_Address(generator, addressData);
		dxlAddress.generateAddress();
	}

	private void generateAccountManager() {
		generator.writeFieldName('accountManager');
		generator.writeStartArray();

		if (opportunityData[0].Account.Fixed_Dealer__c != null) {
			generateAccountManagerRecord(COM_DXL_Constants.ACCOUNT_MANAGER_FIXED);
		}

		if (opportunityData[0].Account.Mobile_Dealer__c != null) {
			generateAccountManagerRecord(COM_DXL_Constants.ACCOUNT_MANAGER_MOBILE);
		}

		if (opportunityData[0].Account.Owner != null) {
			generateAccountManagerRecord(COM_DXL_Constants.ACCOUNT_MANAGER_OWNER);
		}

		generator.writeEndArray();
	}

	private void generateAccountManagerRecord(String accountManagerType) {
		generator.writeStartObject();
		if (accountManagerType.equals(COM_DXL_Constants.ACCOUNT_MANAGER_FIXED)) {
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_USER_TYPE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Fixed_Dealer__r.Contact__r.UserId__r.UserType)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_NAME,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Fixed_Dealer__r.Contact__r.UserId__r.Name)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_MANAGER_NAME,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Fixed_Dealer__r.Contact__r.UserId__r.Manager.Name)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_BOP_CODE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Fixed_Dealer__r.Contact__r.Account.BOPCode__c)
			);
			generator.writeStringField(COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_TYPE, accountManagerType);
		} else if (accountManagerType.equals(COM_DXL_Constants.ACCOUNT_MANAGER_MOBILE)) {
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_USER_TYPE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Mobile_Dealer__r.Contact__r.UserId__r.UserType)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_NAME,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Mobile_Dealer__r.Contact__r.UserId__r.Name)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_MANAGER_NAME,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Mobile_Dealer__r.Contact__r.UserId__r.Manager.Name)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_BOP_CODE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Mobile_Dealer__r.Contact__r.Account.BOPCode__c)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_TYPE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(accountManagerType)
			);
		} else if (accountManagerType.equals(COM_DXL_Constants.ACCOUNT_MANAGER_OWNER)) {
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_USER_TYPE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Owner.UserType)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_NAME,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Owner.Name)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_MANAGER_NAME,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Owner.Manager.Name)
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_BOP_CODE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Owner.Account.BOPCode__c)
			);
			generator.writeStringField(COM_DXL_Constants.COM_DXL_ACCOUNT_MANAGER_TYPE, accountManagerType);
		}

		generator.writeEndObject();
	}

	private String getExternalAccountExternalId(List<External_Account__c> externalAccounts, String externalSystemName) {
		String returnValue = '';

		if (externalAccounts != null) {
			for (External_Account__c externalAccount : externalAccounts) {
				if (
					!String.isBlank(externalAccount.External_Account_Id__c) && externalSystemName.equalsIgnoreCase(externalAccount.External_Source__c)
				) {
					returnValue = externalAccount.External_Account_Id__c;
				}
			}
		}

		return returnValue;
	}

	private Map<String, String> generateAddressData(Opportunity opportunityData) {
		Map<String, String> returnData = new Map<String, String>();
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_STREET, opportunityData.Account.Visiting_Street__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUMBER, String.valueOf(opportunityData.Account.Visiting_HouseNumber1__c));
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUM_EXT, opportunityData.Account.Visiting_HouseNumber_Suffix__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_POSTCODE, opportunityData.Account.Visiting_Postal_Code__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_CITY, opportunityData.Account.Visiting_City__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_COUNTRY, opportunityData.Account.Visiting_Country__c);
		return returnData;
	}
}
