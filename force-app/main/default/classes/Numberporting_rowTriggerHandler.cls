/**
 * @description			This is the trigger handler for the Numberporting_row sObject.
 * @author				Guy Clairbois
 */
public with sharing class Numberporting_rowTriggerHandler extends TriggerHandler {

	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterInsert(){
		List<Numberporting_row__c> newnumberportingRows = (List<Numberporting_row__c>) this.newList;
		Map<Id,Numberporting_row__c> newnumberportingRowsMap = (Map<Id,Numberporting_row__c>) this.newMap;	
		
		checkOverlap(newnumberportingRowsMap);
		checkCityNetnumberCombination(newnumberportingRows);
	}

	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterUpdate(){
		List<Numberporting_row__c> newnumberportingRows = (List<Numberporting_row__c>) this.newList;	
		List<Numberporting_row__c> oldnumberportingRows = (List<Numberporting_row__c>) this.oldList;
		Map<Id,Numberporting_row__c> newnumberportingRowsMap = (Map<Id,Numberporting_row__c>) this.newMap;
		Map<Id,Numberporting_row__c> oldnumberportingRowsMap = (Map<Id,Numberporting_row__c>) this.oldMap;
		
		checkOverlap(newnumberportingRowsMap);
		checkCityNetnumberCombination(newnumberportingRows);
	}	
	
	/**
	 * @description			This method checks if the numberporting rows in 1 numberporting overlap each other
	 * @author				Guy Clairbois
	 */
	private void checkOverlap(Map<Id,Numberporting_row__c> numberportingRowsMap){
		Map<Id,List<Numberporting_row__c>> npIdToNpRows = new Map<Id,List<Numberporting_row__c>>();
		Set<Id> npIds = new Set<Id>();
		
		for(Numberporting_row__c np : numberportingRowsMap.values()){
			system.debug(np);
			npIds.add(np.Numberporting__c);
		}
		
		for(Numberporting_row__c npr : [Select Id, Numberporting__c, Number_block__c, Range_Start__c, Range_End__c From Numberporting_row__c 
										Where Numberporting__c in :npIds
										AND Action__c = 'Porting_in'
										AND Number_block__c != null]){
			if(npIdToNpRows.containsKey(npr.Numberporting__c)){
				npIdToNpRows.get(npr.Numberporting__c).add(npr);
			} else {
				npIdToNpRows.put(npr.Numberporting__c,new List<Numberporting_row__c>{npr});
			}
		}
		
		for(Id npId : npIds){
			if(npIdToNpRows.containsKey(npId)){
				// match all rows with each other
				for(Numberporting_row__c npr : npIdToNpRows.get(npId)){
					for(Numberporting_row__c npr2 : npIdToNpRows.get(npId)){
						if(npr.Id != npr2.Id){
							if(npr.Range_End__c >= npr2.Range_Start__c && npr.Range_Start__c <= npr2.Range_End__c){
								if(numberportingRowsMap.containsKey(npr.Id)){
									numberportingRowsMap.get(npr.Id).addError('Numberblock '+npr.Number_block__c+' overlaps with Numberblock '+npr2.Number_block__c+'!');
								}
							}
						}
					}
				}
			}			
		}

	}

	/**
	 * @description			This method checks if the cities and netnumbers in the numberporting rows match
	 * @author				Guy Clairbois
	 */
	private void checkCityNetnumberCombination(List<Numberporting_row__c> numberportingRows){
		// first collect sites (city data)
		Set<Id> siteIds = new Set<Id>();
		for(Numberporting_row__c npr : numberportingRows){
			if(npr.Location__c != null && npr.Number_block__c != null){
				siteIds.add(npr.Location__c);
			}
		}
		Map<Id,String> siteIdToCity = new Map<Id,String>();
		for(Site__c s : [Select Id, Site_City__c from Site__c Where Id in :siteIds]){
			siteIdToCity.put(s.Id,s.Site_City__c);
		}
		// now do the actual check

		for(Numberporting_row__c npr : numberportingRows){
			if(npr.Location__c != null && npr.Number_block__c != null){
				Boolean matchfound = false;
				String city = siteIdToCity.get(npr.Location__c).toLowerCase();
				if(cityToNetNumbers.containsKey(city)){
					for(String s : cityToNetNumbers.get(city)){
						system.debug(s);
						system.debug(cityToNetNumbers.get(city));
						if(npr.Number_block__c.startsWith(s)){
							matchfound = true;
						}
					}
					if(!matchfound){
						// also check for any NULL values in City, as numbers marked like this are allowed to be used anywhere
						for(String s : cityToNetNumbers.get('null')){
							if(npr.Number_block__c.startsWith(s)){
								matchfound = true;
							}
						}
					}
					if(!matchfound){
						npr.addError('Netnumber does not match selected site. City of '+ siteIdToCity.get(npr.Location__c) + ' has netnumbers '+cityToNetNumbers.get(siteIdToCity.get(npr.Location__c).toLowerCase()) + '. You can resolve this by changing the netnumber, changing the Site, or adding a new Site if necessary.');
					}						
				} else {
					// city not found
					npr.addError('Site City '+siteIdToCity.get(npr.Location__c)+' not found in BOP netnumber table. Please check the spelling or contact your administrator');
				}

			}

		}

	}

	public static Map<String,Set<String>> cityToNetNumbers{
		get{
			if(cityToNetNumbers == null){
				cityToNetNumbers = new Map<String,Set<String>>();
				for(City_Netnumber_Combination__c cnc : [Select City__c, NetNumber__c From City_Netnumber_Combination__c]){
					if(cityToNetNumbers.containsKey(cnc.City__c.toLowerCase())){
						cityToNetNumbers.get(cnc.City__c.toLowerCase()).add(cnc.NetNumber__c);
					} else {
						cityToNetNumbers.put(cnc.City__c.toLowerCase(),new Set<String>{cnc.Netnumber__c});
					}
				}
			}
			return cityToNetNumbers;
		}
		set;
	}

}