@SuppressWarnings('PMD')
public with sharing class AccountExport {
	public static Set<Id> accountsInExport {
		get {
			if (accountsInExport == null) {
				accountsInExport = new Set<Id>();
			}
			return accountsInExport;
		}
		set;
	}

	@future(callout=true)
	public static void exportAccountsOffline(Set<Id> accountIds) {
		AccountExport.accountsInExport.addAll(accountIds);
		exportAccounts(accountIds);
	}

	@future(callout=true)
	public static void exportBansOffline(Set<Id> banIds) {
		AccountExport.accountsInExport.addAll(banIds);
		exportBansById(banIds);
	}

	public static void exportBansById(Set<Id> banIds) {
		exportAccounts(null, banIds);
	}

	public static void exportAccounts(Set<Id> acctIds) {
		exportAccounts(acctIds, null);
	}

	private static Map<String, List<External_Account__c>> accountIdsWithBopCodeMap = new Map<String, List<External_Account__c>>();

	private static List<Account> createCustomerAccounts;
	private static List<Account> updateCustomerAccounts;
	private static List<Account> createResellerAccounts;
	@testVisible
	private static List<Account> updateResellerAccounts;
	@testVisible
	private static List<Account> accountsToUpdate;
	private static List<External_Account__c> extAcctsToInsert = new List<External_Account__c>();
	// Note: Only partner accounts are exported to BOP (isPartner=true)
	// Non partner accounts will instead run a BAN export
	public static void exportAccounts(Set<Id> acctIds, Set<Id> banIds) {
		// possibly switch to BAN export instead of Account export
		Set<Id> banAccountIds = new Set<Id>();
		Set<Id> acctAccountIds = new Set<Id>();
		// if banIds are provided (thus individual bans should be exported), use those. If not, look up the accountids and use those to export the full accounts
		if (banIds == null) {
			for (Account a : [SELECT Id, Name, Phone, IsPartner FROM Account WHERE Id IN :acctIds]) {
				if (a.IsPartner) {
					acctAccountIds.add(a.Id);
				} else {
					banAccountIds.add(a.Id);
				}
			}
			exportBans(banAccountIds, new Set<Id>());
		} else {
			exportBans(new Set<Id>(), banIds);
		}
		acctIds = acctAccountIds;
		// end of temporary switch to BAN export

		if (acctIds.isEmpty()) {
			return;
		}

		filterOutAccounts(acctIds);

		if (!createCustomerAccounts.isEmpty()) {
			makeCallout(createCustomerAccounts, 'createCustomer');
		}
		if (!updateCustomerAccounts.isEmpty()) {
			makeCallout(updateCustomerAccounts, 'updateCustomer');
		}
		if (!createResellerAccounts.isEmpty()) {
			makeCallout(createResellerAccounts, 'createReseller');
		}
		if (!updateResellerAccounts.isEmpty()) {
			makeCallout(updateResellerAccounts, 'updateReseller');
		}

		// allow partial success
		Database.update(accountsToUpdate, false);
		Database.insert(extAcctsToInsert, false);
	}

	@testVisible
	private static Map<Id, Account> partnerAcctIdToAcct;
	@testVisible
	private static Set<Id> newAccounts;
	@testVisible
	private static void filterOutAccounts(Set<Id> acctIds) {
		partnerAcctIdToAcct = new Map<Id, Account>();
		createCustomerAccounts = new List<Account>();
		updateCustomerAccounts = new List<Account>();
		createResellerAccounts = new List<Account>();
		updateResellerAccounts = new List<Account>();
		accountsToUpdate = new List<Account>();
		newAccounts = new Set<Id>();

		List<Account> acctList = [
			SELECT
				Id,
				Name,
				BAN_Number__c,
				BOPCode__c,
				BOP_export_Errormessage__c,
				IsPartner,
				Phone,
				Fax,
				KVK_Number__c,
				Website,
				BillingStreet,
				BillingPostalCode,
				BillingCity,
				BillingCountry,
				Dealer_code__c,
				ParentId,
				Parent.BAN_Number__c,
				Parent.BOPCode__c,
				Parent.Id,
				Owner.UserType,
				Owner.Name,
				Owner.Manager.Name,
				Owner.AccountId,
				Unify_ref_Id__c
			FROM Account
			WHERE Id IN :acctIds
		];

		Set<Id> parentAcctIds = new Set<Id>();
		for (Account a : acctList) {
			parentAcctIds.add(a.Parent?.Id);
		}

		/*	External_Account__c extAcct = [
			SELECT Id, Account__c, External_Account_Id__c, External_Source__c
			FROM External_Account__c
			LIMIT 1
		];

		System.debug('accIds: ' + acctIds + ' AND extAcct yo: ' + extAcct);*/

		for (External_Account__c externalAcc : [
			SELECT Id, Account__c, External_Account_Id__c, External_Source__c
			FROM External_Account__c
			WHERE (Account__c IN :acctIds OR Account__c IN :parentAcctIds) AND External_Source__c = 'BOP'
		]) {
			if (!accountIdsWithBopCodeMap.containsKey(externalAcc.Account__c)) {
				accountIdsWithBopCodeMap.put(externalAcc.Account__c, new List<External_Account__c>());
			}
			accountIdsWithBopCodeMap.get(externalAcc.Account__c).add(externalAcc);
		}

		for (Account a : acctList) {
			if (a.IsPartner) {
				// partner accounts handling
				if (String.isNotBlank(a.BOPCode__c)) {
					updateResellerAccounts.add(a);
				} else {
					createResellerAccounts.add(a);
				}
				continue;
			}
			// customer accounts handling
			// first do some checks that can't be handled by validation rules
			if (banValidation(a)) {
				continue;
			}

			// if bopcode is filled, do update, else do create
			if (accountIdsWithBopCodeMap.get(a.Id) != null && accountIdsWithBopCodeMap.get(a.Id).size() > 0) {
				updateCustomerAccounts.add(a);
			} else {
				createCustomerAccounts.add(a);
			}
			if (a.Owner.AccountId != null) {
				// prepare a map for partner accounts. Temporarily put the client account in (to prevent using 2 variables)
				partnerAcctIdToAcct.put(a.Owner.AccountId, null);
			}
		}
		// create a map of partner account account information
		if (!partnerAcctIdToAcct.isEmpty()) {
			for (Account pa : [SELECT Id, BAN_Number__c, BOPCode__c FROM Account WHERE Id IN :partnerAcctIdToAcct.keySet()]) {
				partnerAcctIdToAcct.put(pa.Id, pa);
			}
		}
	}

	@testVisible
	private static boolean banValidation(Account a) {
		if (a.BAN_Number__c == null) {
			a.BOP_export_Errormessage__c = 'Valid BAN Number is required for exporting an Account';
			accountsToUpdate.add(a);
			return true;
		} else if (!StringUtils.checkBan(a.BAN_Number__c)) {
			a.BOP_export_Errormessage__c = 'Valid BAN Number is required for exporting an Account';
			accountsToUpdate.add(a);
			return true;
		} else if (a.Parent != null) {
			if (a.Parent.BAN_Number__c != null) {
				if (!StringUtils.checkBan(a.BAN_Number__c)) {
					a.BOP_export_Errormessage__c = 'Valid Parent BAN Number is required for exporting an Account';
					accountsToUpdate.add(a);
					return true;
				}
			}
		}
		return false;
	}

	@testVisible
	private static void makeCallout(List<Account> accList, String operation) {
		ECSCompanyService service = new ECSCompanyService();
		List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
		Set<Id> acctId = new Set<Id>();
		for (Account a : accList) {
			ECSCompanyService.request req = accountToRequest(a, partnerAcctIdToAcct);

			acctId.add(a.Id);
			requests.add(req);
		}
		service.setRequest(requests);

		Boolean success = false;
		try {
			service.makeRequest(operation);
			success = true;
		} catch (ExWebServiceCalloutException ce) {
			for (Account a : accList) {
				// only update the error if it has not been set before
				if (a.BOP_export_Errormessage__c != ce.get255CharMessage()) {
					a.BOP_export_Errormessage__c = ce.get255CharMessage();
					accountsToUpdate.add(a);
				}
			}
		}
		if (success) {
			handleResponse(service.getResponse(), acctId, operation);
		}
	}

	@testVisible
	private static void handleResponse(List<ECSCompanyService.response> responses, Set<Id> acctId, String operation) {
		for (ECSCompanyService.response response : responses) {
			if (operation.contains('Reseller') || acctId.contains(response.referenceId)) {
				Account a = new Account(Id = response.referenceId);
				if (operation.contains('create') && response.bopcode != null) {
					extAcctsToInsert.add(upsertBOPCode(response.referenceId, response.bopCode));
					// a.BOPCode__c = response.bopCode;
				}
				if (String.isBlank(response.errorCode) || response.errorCode == '0') {
					// success
					// add info to account
					a.BOP_export_datetime__c = system.now();
					a.BOP_export_Errormessage__c = null;
					// startup location and user sync
					newAccounts.add(a.Id);
				} else {
					// failure
					// add errormessage to account
					a.BOP_export_Errormessage__c = (response.errorCode + ' - ' + response.errorMessage).abbreviate(255);

					createScenarioValidation(response, a, operation);
				}
				accountsToUpdate.add(a);
			} else {
				throw new ExMissingDataException('No referenceId returned by BOP');
			}
		}
	}

	private static void createScenarioValidation(ECSCompanyService.response response, sObject obj, String operation) {
		if (operation.contains('create') && (response.errorCode == '48' || response.errorCode == '1005')) {
			// if error is 'existing customer (48)' then do update instead of insert
			// do this by updating the BOPcode. That will trigger the update process
			if (obj.getSObjectType() == Schema.Account.getSObjectType()) {
				extAcctsToInsert.add(upsertBOPCode(obj.Id, response.bopCode));
			} else {
				obj.put('BOPCode__c', response.bopCode);
			}
			obj.put('BOP_export_datetime__c', system.now());
			// also overwrite the errormessage, since errormsgs starting with a number will not be retried
			obj.put('BOP_export_Errormessage__c', 'awaiting update');
		}
	}

	public static Boolean skipContractCheck {
		get {
			ExportBatchSchedule__c b = ExportBatchSchedule__c.getInstance();
			return b.skipContractCheck__c;
		}
		set;
	}

	@testVisible
	private static List<Ban__c> createBanCustomerAccounts;
	@testVisible
	private static List<Ban__c> updateBanCustomerAccounts;
	@testVisible
	private static List<Ban__c> bansToUpdate;
	@testVisible
	private static Set<Id> banIdSet;
	public static void exportBans(Set<Id> acctIds, Set<Id> banIds) {
		partnerAcctIdToAcct = new Map<Id, Account>();
		createBanCustomerAccounts = new List<Ban__c>();
		updateBanCustomerAccounts = new List<Ban__c>();
		bansToUpdate = new List<Ban__c>();
		newAccounts = new Set<Id>();
		banIdSet = new Set<ID>();

		// fetch the relevant bans (filtering out the ones without a contract)
		@SuppressWarnings('PMD.UnusedLocalVariable')
		Id unifyOrphansAccount = GeneralUtils.unifyOrphansAccount.Id;
		@SuppressWarnings('PMD.UnusedLocalVariable')
		Id vodafoneAccount = GeneralUtils.VodafoneAccount.Id;
		String query =
			'SELECT Name, Account__r.Id, Account__r.Name, BAN_Number__c, BOPCode__c, BOP_export_Errormessage__c, OwnerId, ' +
			'Corporate_Id__c, Unify_ref_Id__c, Account__r.GT_Fixed__c, Account__r.IsPartner, Account__r.Phone, Account__r.Fax, Account__r.KVK_Number__c, ' +
			'Account__r.Website, Account__r.BillingStreet, Account__r.BillingPostalCode, Account__r.BillingCity, Account__r.BillingCountry, Account__r.Owner.UserType, ' +
			'Account__r.Owner.Name, Account__r.Owner.Manager.Name, Account__r.Owner.AccountId, Account__r.Fixed_Dealer__r.Contact__r.UserId__r.UserType, ' +
			'Account__r.Fixed_Dealer__r.Contact__r.UserId__r.Name, Account__r.Fixed_Dealer__r.Contact__r.UserId__r.Manager.Name, Account__r.Fixed_Dealer__r.Contact__r.UserId__r.AccountId, ' +
			'Account__r.Fixed_Dealer__r.Contact__r.Account.Id, Account__r.Fixed_Dealer__r.Contact__r.Account.BAN_Number__c, Account__r.Fixed_Dealer__r.Contact__r.Account.BOPCode__c, ' +
			'Account__r.Mobile_Dealer__r.Contact__r.UserId__r.UserType, Account__r.Mobile_Dealer__r.Contact__r.UserId__r.Name, Account__r.Mobile_Dealer__r.Contact__r.UserId__r.Manager.Name, ' +
			'Account__r.Mobile_Dealer__r.Contact__r.UserId__r.AccountId, Account__r.Mobile_Dealer__r.Contact__r.Account.Id, Account__r.Mobile_Dealer__r.Contact__r.Account.BAN_Number__c, ' +
			'Account__r.Mobile_Dealer__r.Contact__r.Account.BOPCode__c FROM Ban__c WHERE (Account__c IN :acctIds OR Id IN :banIds) AND ' +
			'Account__c != :unifyOrphansAccount AND (Account__c != :vodafoneAccount OR BopCode__c = \'TNF\') ';

		if (!Test.isRunningTest()) {
			query += ' AND Owner.Type = \'User\'  ';
		}

		if (!skipContractCheck) {
			Set<Id> bansWithContract = new Set<Id>();

			for (Order__c o : [
				SELECT VF_Contract__r.Opportunity__r.Ban__c
				FROM Order__c
				WHERE
					VF_Contract__r.Opportunity__r.Ban__c != NULL
					AND Export_System_Customerdata__c = 'BOP'
					AND (VF_Contract__r.Opportunity__r.Ban__c IN :banIds
					OR Account__c IN :acctIds)
			]) {
				bansWithContract.add(o.VF_Contract__r.Opportunity__r.Ban__c);
			}
			query += 'AND (Id IN :bansWithContract OR BOPCode__c != NULL OR BOP_export_Errormessage__c != NULL)';
		} else {
			query += 'AND BAN_Status__c = \'Opened\'';
		}

		@SuppressWarnings('PMD.ApexSOQLInjection')
		List<Ban__c> banList = Database.query(query);

		// since we cannot fetch the owner fields directly, we need to create a map for it
		Map<Id, User> banOwnerToUser = populateUserOwnerMap(banList);

		validateBans(banList, banOwnerToUser);

		// create a map of partner account account information
		createPartnerAccMap();

		if (!createBanCustomerAccounts.isEmpty()) {
			makeCallout(createBanCustomerAccounts, banOwnerToUser, 'createCustomer');
		}
		if (!updateBanCustomerAccounts.isEmpty()) {
			makeCallout(updateBanCustomerAccounts, banOwnerToUser, 'updateCustomer');
		}

		// allow partial success
		List<Database.SaveResult> ir = Database.update(bansToUpdate, false);
		System.debug(System.LoggingLevel.ERROR, 'Error: ' + ir);
		triggerRelatedSitesAndContacts();
	}

	private static Map<Id, User> populateUserOwnerMap(List<Ban__c> banList) {
		// since we cannot fetch the owner fields directly, we need to create a map for it
		Map<Id, User> banOwnerToUser = new Map<Id, User>();
		for (Ban__c b : banList) {
			banOwnerToUser.put(b.OwnerId, null);
		}

		for (User u : [SELECT UserType, Name, Manager.Name, AccountId FROM User WHERE Id IN :banOwnerToUser.keySet()]) {
			banOwnerToUser.put(u.Id, u);
		}
		return banOwnerToUser;
	}

	private static void triggerRelatedSitesAndContacts() {
		if (!newAccounts.isEmpty()) {
			SiteExport.scheduleSiteExportFromBanIds(newAccounts);
			ContactExport.scheduleContactExportFromBanIds(newAccounts);
		}
	}

	@testVIsible
	private static void createPartnerAccMap() {
		if (!partnerAcctIdToAcct.isEmpty()) {
			for (Account pa : [SELECT Id, BAN_Number__c, BOPCode__c FROM Account WHERE Id IN :partnerAcctIdToAcct.keySet()]) {
				partnerAcctIdToAcct.put(pa.Id, pa);
			}
		}
	}

	@testVisible
	private static void validateBans(List<Ban__c> banList, Map<Id, User> banOwnerToUser) {
		for (Ban__c banInfo : banList) {
			User banOwner = banOwnerToUser.get(banInfo.ownerId);

			// customer accounts handling
			// first do some checks that can't be handled by validation rules
			if (baninfo.BAN_Number__c == null) {
				baninfo.BOP_export_Errormessage__c = 'Valid BAN Number is required for exporting an Account';
				if (!banIdSet.contains(baninfo.id)) {
					bansToUpdate.add(baninfo);
					banIdSet.add(baninfo.id);
				}

				continue;
			} else if (!StringUtils.checkBan(baninfo.BAN_Number__c)) {
				baninfo.BOP_export_Errormessage__c = 'Valid BAN Number is required for exporting an Account';
				if (!banIdSet.contains(baninfo.id)) {
					bansToUpdate.add(baninfo);
					banIdSet.add(baninfo.id);
				}
				continue;
			}

			// if bopcode is filled, do update, else do create
			if (baninfo.BOPCode__c != null && baninfo.BOPCode__c != '') {
				updateBanCustomerAccounts.add(baninfo);
			} else {
				// GC 4-7-2017  No BOP Accounts should be created anymore directly from SFDC. Always should go via SIAS
				// However a flag can be switched to permit creates if needed
				createBopAccount(baninfo);
			}
			if (banOwner.AccountId != null) {
				// prepare a map for partner accounts. Temporarily put the client account in (to prevent using 2 variables)
				partnerAcctIdToAcct.put(banOwner.AccountId, null);
			}
		}
	}

	@testVisible
	private static void createBopAccount(Ban__c baninfo) {
		ExportBatchSchedule__c b = ExportBatchSchedule__c.getInstance();
		if (b.createBOPAccount__c) {
			createBanCustomerAccounts.add(baninfo);
		}
	}

	@testVisible
	private static void makeCallout(List<Ban__c> banList, Map<Id, User> banOwnerToUser, String operation) {
		ECSCompanyService service = new ECSCompanyService();
		List<ECSCompanyService.request> requests = new List<ECSCompanyService.request>();
		Map<String, Id> refToBanId = new Map<String, Id>();
		Set<Id> exportedBanIds = new Set<Id>();
		for (Ban__c b : banList) {
			ECSCompanyService.request req = banToRequest(b, partnerAcctIdToAcct);

			if (operation.contains('create')) {
				exportedBanIds.add(b.Id);
				if (b.Unify_ref_Id__c != null) {
					refToBanId.put(b.Unify_ref_Id__c, b.Id);
				}
				requests.add(req);
			} else {
				refToBanId.put(b.BOPCode__c, b.id);
			}
		}
		service.setRequest(requests);

		Boolean success = false;
		try {
			service.makeRequest(operation);
			success = true;
		} catch (ExWebServiceCalloutException ce) {
			for (Ban__c b : banList) {
				// only update the error if it has not been set before
				if (b.BOP_export_Errormessage__c != ce.get255CharMessage()) {
					b.BOP_export_Errormessage__c = ce.get255CharMessage();
					if (!banIdSet.contains(b.id)) {
						bansToUpdate.add(b);
						banIdSet.add(b.id);
					}
				}
			}
		}
		if (success) {
			handleResponse(service.getResponse(), refToBanId, exportedBanIds);
		}
	}

	@testVisible
	private static void handleResponse(List<ECSCompanyService.response> responses, Map<String, Id> refToBanId, Set<Id> exportedBanIds) {
		for (ECSCompanyService.response response : responses) {
			String reference;
			String errorMessage;
			String operation = '';

			if (exportedBanIds.IsEmpty()) {
				reference = response.bopCode;
				errorMessage = 'No bopCode returned by BOP';
			} else {
				reference = response.referenceId;
				errorMessage = 'No referenceId returned by BOP';
				operation = 'createCustomer';
			}

			Ban__c b = createBan(refToBanId, reference, exportedBanIds);

			if (b != null) {
				if (response.bopcode != null) {
					b.BOPCode__c = response.bopCode;
				}
				if (String.isBlank(response.errorCode) || response.errorCode == '0') {
					// success
					// add info to account
					b.BOP_export_datetime__c = system.now();
					b.BOP_export_Errormessage__c = null;
					// startup location and user sync
					newAccounts.add(b.Id);
				} else {
					// failure
					// add errormessage to account
					b.BOP_export_Errormessage__c = (response.errorCode + ' - ' + response.errorMessage).abbreviate(255);

					createScenarioValidation(response, b, operation);
				}
				if (!banIdSet.contains(b.id)) {
					bansToUpdate.add(b);
					banIdSet.add(b.id);
				}
			} else {
				throw new ExMissingDataException(errorMessage);
			}
		}
	}

	@testVisible
	private static Ban__c createBan(Map<String, Id> refToBanId, String reference, Set<Id> exportedBanIds) {
		Ban__c b;
		if (refToBanId.containsKey(reference)) {
			b = new Ban__c(Id = refToBanId.get(reference));
		} else if (exportedBanIds.contains(reference)) {
			b = new Ban__c(Id = reference);
		}
		return b;
	}

	@testVisible
	private static ECSCompanyService.request accountToRequest(Account a, Map<Id, Account> partnerAcctIdToAcct) {
		ECSCompanyService.request req = new ECSCompanyService.request();
		req.banNumber = a.BAN_Number__c;
		List<External_Account__c> extAccts = accountIdsWithBopCodeMap.get(a.Id);
		if (extAccts != null && extAccts.size() > 0) {
			req.bopCode = extAccts[0].External_Account_Id__c;
			// req.bopCode = a.BOPCode__c;
		}
		req.referenceId = a.Id;
		req.name = a.Name;

		req.phone = a.Phone;
		req.fax = a.Fax;
		req.countryId = null; //not used (yet)
		req.kvkNumber = a.KVK_number__c;
		req.website = a.Website;
		req.billingStreet = a.BillingStreet;
		if (a.BillingPostalCode != null) {
			req.billingPostalCode = a.BillingPostalCode.replaceAll(' ', '');
		}
		req.billingCity = a.BillingCity;
		req.billingCountry = a.BillingCountry;
		req.dealerCode = a.Dealer_code__c;
		if (a.Owner.UserType == 'PowerPartner') {
			if (partnerAcctIdToAcct.containsKey(a.owner.AccountId)) {
				Account partner = partnerAcctIdToAcct.get(a.owner.AccountId);
				req.resellerBanNumber = partner.BAN_Number__c;
				req.resellerBopCode = partner.BOPCode__c;
				req.resellerReferenceId = partner.Id;
			}

			if (a.Owner.Manager.Name != null) {
				req.accountManagerEbu = a.Owner.Manager.Name;
			}
		} else {
			req.accountManagerEbu = a.Owner.Name;
		}

		List<External_Account__c> extParAccts = accountIdsWithBopCodeMap.get(a.ParentId);
		if (extParAccts != null && (a.Parent?.BAN_Number__c != null || extParAccts.size() > 0)) {
			req.parentCompanyBanNumber = a.Parent.BAN_Number__c;
			if (extParAccts.size() > 0) {
				req.parentCompanyBopCode = extParAccts[0].External_Account_Id__c;
				// req.parentCompanyBopCode = a.Parent.BOPCode__c;
			}
			req.parentCompanyReferenceId = a.Parent.Id;
		}

		req.internalInvoicing = null; //not used?
		return req;
	}

	@testVisible
	private static ECSCompanyService.request banToRequest(Ban__c b, Map<Id, Account> partnerAcctIdToAcct) {
		ECSCompanyService.request req = new ECSCompanyService.request();
		req.banNumber = b.BAN_Number__c;
		req.bopCode = b.BOPCode__c;

		banToReqInitialPart(b, req);
		// fill reseller with fixed owner
		Account partner;
		String fixedAccountManagerUser;
		if (b.Account__r.Fixed_Dealer__c != null) {
			if (b.Account__r.Fixed_Dealer__r.Contact__r.UserId__r.UserType == 'PowerPartner') {
				partner = b.Account__r.Fixed_Dealer__r.Contact__r.Account;
				fixedAccountManagerUser = b.Account__r.Fixed_Dealer__r.Contact__r.UserId__r.Manager.Name;
			} else {
				fixedAccountManagerUser = b.Account__r.Fixed_Dealer__r.Contact__r.UserId__r.Name;
			}
		} else if (b.Account__r.Mobile_Dealer__c != null) {
			if (b.Account__r.Mobile_Dealer__r.Contact__r.UserId__r.UserType == 'PowerPartner') {
				partner = b.Account__r.Mobile_Dealer__r.Contact__r.Account;
				fixedAccountManagerUser = b.Account__r.Mobile_Dealer__r.Contact__r.UserId__r.Manager.Name;
			} else {
				fixedAccountManagerUser = b.Account__r.Mobile_Dealer__r.Contact__r.UserId__r.Name;
			}
		} else {
			if (b.Account__r.Owner.UserType == 'PowerPartner') {
				partner = partnerAcctIdToAcct.get(b.Account__r.Owner.AccountId);
				fixedAccountManagerUser = b.Account__r.Owner.Manager.Name;
			} else {
				fixedAccountManagerUser = b.Account__r.Owner.Name;
			}
		}

		if (partner != null) {
			req.resellerBanNumber = partner.BAN_Number__c;
			req.resellerBopCode = partner.BOPCode__c;
			req.resellerReferenceId = partner.Id;
		}
		if (fixedAccountManagerUser != null) {
			req.accountManagerEbu = fixedAccountManagerUser;
		}

		req.internalInvoicing = null; //not used?

		// GT Fixed
		if (b.Account__r.GT_Fixed__c) {
			req.companyGroup = 'GT Vast';
		}

		return req;
	}

	@testVisible
	private static void banToReqInitialPart(Ban__c b, ECSCompanyService.request req) {
		// On advice from BOP this should only pass the salesforce id when creating an account
		// Once the account is created, it is identified in BOP by using the banNumber and bopCode
		if (b.BOPCode__c == null) {
			req.referenceId = b.Id;
		}

		req.name = b.Account__r.Name;

		req.phone = b.Account__r.Phone;
		req.fax = b.Account__r.Fax;
		req.countryId = null; //not used (yet)
		req.kvkNumber = b.Account__r.KVK_number__c;
		req.website = b.Account__r.Website;
		req.billingStreet = b.Account__r.BillingStreet;
		if (b.Account__r.BillingPostalCode != null) {
			req.billingPostalCode = b.Account__r.BillingPostalCode.replaceAll(' ', '');
		}
		req.billingCity = b.Account__r.BillingCity;
		req.billingCountry = b.Account__r.BillingCountry;
	}

	/*
	 *  Description:  This method can be used to schedule the export of BANs for a set of ban ids
	 */
	public static void scheduleBANExportFromBanIds(Set<Id> banIds) {
		// By putting a 'dummy' error in the ban export results, the scheduled export is triggered
		List<Ban__c> bansToUpdate = new List<Ban__c>();

		for (Ban__c b : [SELECT Id, Account__c FROM Ban__c WHERE Id IN :banIds]) {
			b.BOP_export_Errormessage__c = 'Pending scheduled export..';
			bansToUpdate.add(b);
		}

		if (!bansToUpdate.isEmpty()) {
			try {
				update bansToUpdate;
			} catch (dmlException e) {
				ExceptionHandler.handleException(e);
			}
		}
	}

	private static External_Account__c upsertBOPCode(Id accId, String bopCode) {
		External_Account__c newExtAcct;
		Boolean matchFound = false;
		if (accountIdsWithBopCodeMap.get(accId) != null) {
			for (External_Account__c extAcct : accountIdsWithBopCodeMap.get(accId)) {
				if (extAcct.External_Account_Id__c == bopCode) {
					matchFound = true;
				}
			}
		}
		if (!matchFound) {
			newExtAcct = new External_Account__c(Account__c = accId, External_Account_Id__c = bopCode, External_Source__c = 'BOP');
		}
		return newExtAcct;
	}
}