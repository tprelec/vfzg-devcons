/**
 * @description       : SFLO : 200 Utility class to create Winback leads when opp gets closed
 * @author            : Saurabh
 * @group             : LeadsUnited
 * @last modified on  : 03-07-2022
 * @last modified by  : Saurabh
 **/
public class CreateWinbackLeadFromOpportunity {
	public static Set<Id> uniqueOppIds = new Set<Id>();
	public static void createWinbackLeads(List<Opportunity> closedOppList) {
		List<Lead> winBackLeadsToInsert = new List<Lead>();
		Set<Id> accountIds = new Set<Id>();
		Map<Id, Account> accountsMap = new Map<Id, Account>();
		// get opportunity record ids
		Set<Id> opportunityIds = (new Map<Id, Opportunity>(closedOppList)).keySet();
		// get opportunities already related to leads (meaning, a winback lead has been already created for them)
		Set<Id> winbackLeadExits = winbackLeadExits(opportunityIds);
		Id winbackQueueId;
		//if method fails due to any error, exception would give details to user
		try {
			for (Opportunity updatedOpp : closedOppList) {
				if (!uniqueOppIds.contains(updatedOpp.id) && checkWinbackOpp(updatedOpp) && !winbackLeadExits.contains(updatedOpp.Id)) {
					accountIds.add(updatedOpp.AccountId);
				}
			}
			//trying to avoid SOQL in for loop
			if (!accountIds.isEmpty()) {
				accountsMap = new Map<Id, Account>(
					[
						SELECT
							Visiting_Housenumber1__c,
							Visiting_Housenumber_Suffix__c,
							Visiting_City__c,
							Visiting_street__c,
							Visiting_Postal_Code__c,
							LG_ReadyForService__c,
							Vodafone_Customer__c,
							(SELECT Id, MobilePhone, Email FROM Contacts)
						FROM Account
						WHERE Id IN :accountIds
					]
				);
				//Moving this query inside if for SFOP-1975 to limit query execution for unimportant Opp updates
				winbackQueueId = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperNAME = 'Winback_Leads']?.Id;
			}
			for (Opportunity updatedOpp : closedOppList) {
				//remove the awareness of interest leads for now
				if (checkWinbackOpp(updatedOpp) && !uniqueOppIds.contains(updatedOpp.id) && !winbackLeadExits.contains(updatedOpp.Id)) {
					Lead winbackLead = new Lead();
					winbackLead.LastName = updatedOpp.Name;
					winbackLead.recordTypeId = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('CSADetails').getRecordTypeId();
					winbackLead.Description = 'Winback Lead from Opportunity ---' + updatedOpp.id + '  which was in stage --' + updatedOpp.StageName;
					WinbackLead.Opportunity__c = updatedOpp.id;
					winbackLead.OwnerId = winbackQueueId;
					winbackLead.Company = updatedOpp.Name;
					winbackLead.Visiting_Housenumber1__c = accountsMap.get(updatedOpp.AccountId).Visiting_Housenumber1__c;
					winbackLead.House_Number_Suffix__c = accountsMap.get(updatedOpp.AccountId).Visiting_Housenumber_Suffix__c;
					winbackLead.LeadSource = 'Winback';
					winbackLead.Email = accountsMap.get(updatedOpp.AccountId).Contacts[0]?.Email;
					winbackLead.Phone = accountsMap.get(updatedOpp.AccountId).Contacts[0]?.MobilePhone;
					winbackLead.City = accountsMap.get(updatedOpp.AccountId).Visiting_City__c; //insttalation address fields on opportunity
					winbackLead.Town__c = accountsMap.get(updatedOpp.AccountId).Visiting_City__c; //insttalation address fields on opportunity
					winbackLead.Street__c = accountsMap.get(updatedOpp.AccountId).Visiting_street__c; //insttalation address fields on opportunity
					winbackLead.Postcode_number__c = string.valueof(accountsMap.get(updatedOpp.AccountId).Visiting_Postal_Code__c)?.substring(0, 4); //insttalation address fields on opportunity
					winbackLead.Postcode_char__c = string.valueof(accountsMap.get(updatedOpp.AccountId).Visiting_Postal_Code__c)?.substring(4, 6); //insttalation address fields on opportunity
					winbacklead.Vodafone_customer__c = updatedOpp.Mobile_Customer__c; //accountsMap.get(updatedOpp.AccountId).Vodafone_Customer__c; // mobile customer on opportunity
					winbacklead.LG_DTVCustomer__c = updatedOpp.DTV_Customer__c;
					winbacklead.LG_InternetCustomer__c = updatedOpp.Internet_Customer__c;
					winbacklead.LG_TelephonyCustomer__c = updatedOpp.Telephony_Customer__c;
					winbacklead.LG_ReadyForService__c = accountsMap.get(updatedOpp.AccountId).LG_ReadyForService__c; //use as is
					//to avoid duplicate leads in case of recursion
					winBackLeadsToInsert.add(winbackLead);
					uniqueOppIds.add(updatedOpp.id);
				}
			}

			insert winBackLeadsToInsert;
		} catch (Exception e) {
			throw new DMLException('Winback Lead couldn\'t be created' + e.getMessage());
		}
	}

	public static Boolean checkWinbackOpp(Opportunity opp) {
		return ((opp.LG_NEWSalesChannel__c == 'D2D') &&
		(opp.StageName == 'Related Back Office order- Cancelled' ||
		opp.StageName == 'Quotation Lost' ||
		opp.StageName == 'Closed Lost'));
	}

	private static Set<Id> winbackLeadExits(Set<Id> oppportunityIds) {
		Set<Id> existingOpportunityIds = new Set<Id>();
		for (Lead winbackLead : [SELECT Id, Opportunity__c FROM Lead WHERE Opportunity__c IN :oppportunityIds AND LeadSource = 'Winback']) {
			existingOpportunityIds.add(winbackLead.Opportunity__c);
		}
		return existingOpportunityIds;
	}
}
