public without sharing class AccountTeamMemberTriggerHandler extends TriggerHandler {
	private List<AccountTeamMember> newAtmList;
	private Map<Id, AccountTeamMember> newAtmMap;
	private Map<Id, AccountTeamMember> oldAtmMap;

	private static final String CHANNEL_MANAGER_ROLE = 'Channel Manager';

	private void init() {
		newAtmList = (List<AccountTeamMember>) this.newList;
		newAtmMap = (Map<Id, AccountTeamMember>) this.newMap;
		oldAtmMap = (Map<Id, AccountTeamMember>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		preventAnotherChannelManager();
	}

	public override void beforeUpdate() {
		init();
		preventAnotherChannelManager();
	}

	private void preventAnotherChannelManager() {
		List<AccountTeamMember> atmsToProcess = GeneralUtils.filterChangedRecords(
			newAtmList,
			oldAtmMap,
			'TeamMemberRole'
		);

		if (atmsToProcess.isEmpty()) {
			return;
		}

		Boolean isChannelMgr = false;

		for (AccountTeamMember atm : newAtmList) {
			if (atm.TeamMemberRole == CHANNEL_MANAGER_ROLE) {
				isChannelMgr = true;
				break;
			}
		}

		if (!isChannelMgr) {
			return;
		}

		Set<Id> accIds = new Set<Id>();

		for (AccountTeamMember atm : newAtmList) {
			accIds.add(atm.AccountId);
		}

		List<AccountTeamMember> siblingChannelMgrs = [
			SELECT AccountId
			FROM AccountTeamMember
			WHERE AccountId IN :accIds AND TeamMemberRole = :CHANNEL_MANAGER_ROLE
		];

		for (AccountTeamMember atm : newAtmList) {
			for (AccountTeamMember sibling : siblingChannelMgrs) {
				if (
					atm.TeamMemberRole == CHANNEL_MANAGER_ROLE &&
					atm.AccountId == sibling.AccountId
				) {
					atm.addError(Label.Prevent_Another_Channel_Manager);
				}
			}
		}
	}
}