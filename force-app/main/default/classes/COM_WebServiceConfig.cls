public with sharing class COM_WebServiceConfig {
	public static String getEndpoint(String namedCredential, String webParam) {
		WebServices_Param_Settings__mdt config = WebServices_Param_Settings__mdt.getInstance(webParam);

		String endpoint = 'callout:' + namedCredential + config.Partial_URL__c;
		return endpoint;
	}

	public static String getUUID() {
		Blob b = Crypto.GenerateAESKey(128);
		String h = EncodingUtil.ConvertTohex(b);
		String guid = h.SubString(0, 8) + '-' + h.SubString(8, 12) + '-' + h.SubString(12, 16) + '-' + h.SubString(16, 20) + '-' + h.substring(20);
		return guid;
	}

	public static String retrieveCaseInsensitiveHeader(HttpResponse response, String headerConstant) {
		String headerValue = '';

		for (String key : response.getHeaderKeys()) {
			if (key.toLowerCase() == headerConstant.toLowerCase()) {
				headerValue = response.getHeader(key);
			}
		}
		return headerValue;
	}
}
