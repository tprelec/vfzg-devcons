public with sharing class ContractService {
	/**
	 * Searches Contracts based on the Contract Numbers
	 * @param  contractNumbers 	Contract Numbers
	 * @return                  Map of Contracts, with Contract Number as a key.
	 */
	public Map<String, VF_Contract__c> getContractsByNumber(Set<String> contractNumbers) {
		Map<String, VF_Contract__c> result = new Map<String, VF_Contract__c>();
		contractNumbers.remove(null);
		if (contractNumbers.isEmpty()) {
			return result;
		}
		List<VF_Contract__c> contracts = [
			SELECT
				Id,
				Name,
				Contract_Status__c,
				Opportunity__r.Primary_Basket__c,
				Opportunity__r.Primary_Basket__r.Name,
				Opportunity__r.Primary_Quote__c,
				Opportunity__r.Primary_Quote__r.Name,
				Contract_Number__c,
				Dealer_Information__c,
				Type_of_Service__c
			FROM VF_Contract__c
			WHERE
				Opportunity__r.Primary_Basket__r.Name IN :contractNumbers
				OR Opportunity__r.Primary_Quote__r.Name IN :contractNumbers
				OR Contract_Number__c IN :contractNumbers
				OR Name IN :contractNumbers
		];
		for (VF_Contract__c contract : contracts) {
			result.put(contract.Name, contract);
			if (contract.Opportunity__r.Primary_Basket__c != null) {
				result.put(contract.Opportunity__r.Primary_Basket__r.Name, contract);
			} else if (contract.Opportunity__r.Primary_Quote__c != null) {
				result.put(contract.Opportunity__r.Primary_Quote__r.Name, contract);
			} else if (contract.Contract_Number__c != null) {
				result.put(contract.Contract_Number__c, contract);
			}
		}
		return result;
	}

	/**
	 * Creates new Contract from Asset.
	 * @param  asset 	Asset
	 * @return          New Contract
	 */
	public VF_Contract__c createContractFromAsset(VF_Asset__c asset) {
		Id activeRecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName(
			SObjectType.VF_Contract__c.getName(),
			'Contract_Active'
		);
		Id unlockedRecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName(
			SObjectType.VF_Contract__c.getName(),
			'Contract_Retainable_Unlocked'
		);

		VF_Contract__c newContract = new VF_Contract__c(
			Contract_Number__c = asset.Contract_Number__c,
			Account__c = asset.Account__c,
			Type_of_Service__c = String.isNotBlank(asset.CTN__c)
				? Constants.VF_CONTRACT_MOBILE_SERVICE
				: null
		);

		if (asset.Contract_End_Date__c > System.today()) {
			newContract.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName(
				'VF_Contract__c',
				'Contract_Active'
			);
			newContract.Contract_Status__c = Constants.VF_CONTRACT_STATUS_ACTIVE;
		} else {
			newContract.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName(
				'VF_Contract__c',
				'Contract_Retainable_Unlocked'
			);
			newContract.Contract_Status__c = Constants.VF_CONTRACT_STATUS_UNLOCKED;
		}
		newContract.Contract_Activation_Date_Actual_del__c = asset.Contract_Start_Date__c;
		if (asset.Contract_Start_Date__c != null && asset.Contract_End_Date__c != null) {
			newContract.Contract_Duration__c = asset.Contract_Start_Date__c.monthsBetween(
				asset.Contract_End_Date__c
			);
		}
		return newContract;
	}
}