public with sharing class CollectionUtils {
	/**
	 * Joins multiple Sets of Strings into a single Set.
	 * @param  sets List of Sets to join.
	 * @return      Joined Set.
	 */
	public static Set<String> joinStringSets(List<Set<String>> sets) {
		Set<String> s = new Set<String>();
		for (Set<String> setToJoin : sets) {
			s.addAll(setToJoin);
		}
		return s;
	}
}