@isTest
public with sharing class CS_ContractManagementTest {
	@TestSetup
	private static void init() {
		List<Profile> pList = [
			SELECT Id, Name
			FROM Profile
			WHERE Name = 'System Administrator'
			LIMIT 1
		];
		List<UserRole> roleList = [
			SELECT Id, Name, DeveloperName
			FROM UserRole u
			WHERE ParentRoleId = NULL
		];

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Account testAccount = CS_DataTest.createAccount('Test Account 1');
			insert testAccount;

			Contact contact1 = new Contact(
				AccountId = testAccount.id,
				LastName = 'Last',
				FirstName = 'First',
				Contact_Role__c = 'Consultant',
				Email = 'test@vf.com'
			);
			insert contact1;

			Opportunity opp = CS_DataTest.createOpportunity(
				testAccount,
				'Service opportunity',
				simpleUser.id
			);
			insert opp;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(
				opp,
				'Service Basket'
			);
			basket.Name = 'Service Basket';
			basket.cscfga__Basket_Status__c = 'Valid';
			basket.Primary__c = true;
			basket.csbb__Synchronised_with_Opportunity__c = true;
			basket.csbb__Account__c = testAccount.Id;
			insert basket;

			cscfga__Product_Definition__c internetDefinition = CS_DataTest.createProductDefinition(
				'Internet'
			);
			internetDefinition.Product_Type__c = 'Fixed';
			insert internetDefinition;

			cscfga__Product_Configuration__c internetConf = CS_DataTest.createProductConfiguration(
				internetDefinition.Id,
				'Internet',
				basket.Id
			);
			internetConf.Contract_Number_Group__c = 'BI';
			internetConf.Contract_Number__c = 'CS-2022-VZ-BI-00137 2.1 d.d. 01-04-2022';
			internetConf.ContractNumber_JSON__c = '{"year":2022,"originalContractDate":null,"minorVersion":1,"majorVersion":2,"incrementNumber":137,"format":"CS-#YYYY#-VZ-#GROUP#-#NUMBER# #MAJOR#.#MINOR# d.d. #DATE#","contractGroup":"BI","contractDate":"2022-04-01"}';
			internetConf.cscfga__Contract_Term__c = 24;
			insert internetConf;

			csord__Order__c order = CS_DataTest.createOrder();
			order.csordtelcoa__Opportunity__c = opp.Id;
			insert order;

			csord__Subscription__c subscription1 = CS_DataTest.createSubscription(internetConf.Id);
			subscription1.csord__Identification__c = 's1';
			subscription1.csord__Order__c = order.Id;

			csord__Subscription__c subscription2 = CS_DataTest.createSubscription(internetConf.Id);
			subscription2.csord__Identification__c = 's2';
			subscription2.csord__Order__c = order.Id;

			List<csord__Subscription__c> subscriptions = new List<csord__Subscription__c>{
				subscription1,
				subscription2
			};
			insert subscriptions;

			csord__Service__c service1 = CS_DataTest.createService(
				internetConf.Id,
				subscription1.Id,
				'serv1'
			);
			service1.csord__Activation_Date__c = Date.newInstance(2022, 4, 1);

			csord__Service__c service2 = CS_DataTest.createService(
				internetConf.Id,
				subscription2.Id,
				'serv2'
			);
			service2.csordtelcoa__Replaced_Service__c = service1.Id;

			List<csord__Service__c> services = new List<csord__Service__c>{ service1, service2 };
			insert services;

			csclm__Document_Definition__c testDocumentDefinition = CS_DataTest.createDocumentDefinition();
			testDocumentDefinition.ExternalID__c = 'SomeId3';
			testDocumentDefinition.csclm__Linked_Object__c = 'cscfga__Product_Basket__c';
			insert testDocumentDefinition;

			csclm__Document_Template__c template = new csclm__Document_Template__c();
			template.csclm__Effective_From__c = Date.newInstance(2000, 1, 1);
			template.csclm__Effective_To__c = Date.newInstance(2200, 1, 1);
			template.csclm__Valid__c = true;
			template.csclm__Active__c = true;
			template.ExternalID__c = 'anavolimilovana';
			template.csclm__Document_Definition__c = testDocumentDefinition.Id;
			insert template;

			csclm__Agreement__c agreement = new csclm__Agreement__c();
			agreement.Name = 'CS-2022-VZ-BI-00137 2.1 d.d. 01-04-2022';
			agreement.csclm__Opportunity__c = opp.Id;
			agreement.csclm__Document_Template__c = template.Id;
			agreement.Product_Basket__c = basket.Id;
			insert agreement;

			csclm__Agreement__c agreement2 = new csclm__Agreement__c();
			agreement2.Name = 'CS-2022-VZ-BI-00137 3.1 d.d. 01-04-2022';
			agreement2.csclm__Opportunity__c = opp.Id;
			agreement2.csclm__Document_Template__c = template.Id;
			agreement2.Product_Basket__c = basket.Id;
			insert agreement2;
		}
	}

	@isTest
	private static void testCreateContractRecords() {
		Map<Id, csord__Subscription__c> subscriptionMap = new Map<Id, csord__Subscription__c>(
			[SELECT Id FROM csord__Subscription__c]
		);

		Test.startTest();
		List<csconta__Contract__c> res = CS_ContractManagement.createContractRecords(
			new List<Id>(subscriptionMap.keySet())
		);
		System.assertEquals(1, res.size(), 'Expecting 1 inserted contract');
		Test.stopTest();
	}

	@isTest
	private static void testCreateContractRecordsMacd() {
		Map<Id, csord__Subscription__c> subscriptionMap = new Map<Id, csord__Subscription__c>(
			[SELECT Id FROM csord__Subscription__c]
		);
		List<cscfga__Product_Definition__c> definitions = [
			SELECT Id, Name
			FROM cscfga__Product_Definition__c
		];
		List<cscfga__Product_Basket__c> baskets = [SELECT Id, Name FROM cscfga__Product_Basket__c];

		List<csconta__Contract__c> res = CS_ContractManagement.createContractRecords(
			new List<Id>(subscriptionMap.keySet())
		);

		cscfga__Product_Configuration__c internetConf2 = CS_DataTest.createProductConfiguration(
			definitions[0].Id,
			'Internet',
			baskets[0].Id
		);
		internetConf2.Contract_Number_Group__c = 'BI';
		internetConf2.Contract_Number__c = 'CS-2022-VZ-BI-00137 3.1 d.d. 01-04-2022';
		internetConf2.ContractNumber_JSON__c = '{"year":2022,"originalContractDate":null,"minorVersion":1,"majorVersion":3,"incrementNumber":137,"format":"CS-#YYYY#-VZ-#GROUP#-#NUMBER# #MAJOR#.#MINOR# d.d. #DATE#","contractGroup":"BI","contractDate":"2022-04-01"}';
		internetConf2.cscfga__Contract_Term__c = 24;
		insert internetConf2;

		csord__Order__c order = [
			SELECT Id, csordtelcoa__Opportunity__c
			FROM csord__Order__c
			LIMIT 1
		];

		csord__Subscription__c subscription3 = CS_DataTest.createSubscription(internetConf2.Id);
		subscription3.csord__Identification__c = 's3';
		subscription3.csordtelcoa__Replaced_Subscription__c = (subscriptionMap.values())[0].Id;
		subscription3.csord__Order__c = order.Id;
		insert subscription3;

		subscriptionMap.put(subscription3.Id, subscription3);

		Test.startTest();
		res = CS_ContractManagement.createContractRecords(new List<Id>{ subscription3.Id });
		System.assertEquals(1, res.size(), 'Expecting 1 inserted contract');

		Test.stopTest();
	}
}
