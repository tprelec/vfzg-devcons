public with sharing class OEPromotionDiscountTriggerHandler extends TriggerHandler {
	private List<OE_Promotion_Discount__c> newOEPromotionDiscounts;
	private Map<Id, OE_Promotion_Discount__c> newOEPromotionDiscountsMap;
	private Map<Id, OE_Promotion_Discount__c> oldOEPromotionDiscounts;

	private void init() {
		newOEPromotionDiscounts = (List<OE_Promotion_Discount__c>) this.newList;
		newOEPromotionDiscountsMap = (Map<Id, OE_Promotion_Discount__c>) this.newMap;
		oldOEPromotionDiscounts = (Map<Id, OE_Promotion_Discount__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	public void setExternalIds() {
		Sequence seq = new Sequence('OE_Promotion_Discount');

		if (seq.canBeUsed()) {
			seq.startMutex();
			for (OE_Promotion_Discount__c o : newOEPromotionDiscounts) {
				o.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}