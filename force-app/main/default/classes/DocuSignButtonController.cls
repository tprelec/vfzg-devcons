public without sharing class DocuSignButtonController {
	public static final String DOCUSIGN_ENVELOPE_STATUS_COMPLETED = 'Completed';

	/*
	 * @description DocuSign functionality is not available to all users and
	 *  and to those that it is available, it's not available at all times.
	 *  This method checks various conditions to determine if DocuSign button
	 *  should be visible.
	 * @param opportunityId an opportunity ID
	 * @return true if DocuSign button should be visible, false otherwise.
	 */
	@AuraEnabled
	public static Boolean isDocuSignButtonVisible(String opportunityId) {
		Opportunity oppy = [
			SELECT
				Id,
				name,
				IsClosed,
				Select_Journey__c,
				Account.Authorized_to_sign_1st__c,
				Account.Authorized_to_sign_2nd__c,
				Primary_Basket__r.cscfga__Basket_Status__c,
				(SELECT dsfs__Sender_Email__c, dsfs__Completed_Date_Time__c, dsfs__Envelope_Status__c FROM dsfs__R00N80000002fD9vEAE__r)
			FROM opportunity
			WHERE id = :opportunityId
		];
		User currentUser = [SELECT id, dsfs__DSProSFUsername__c, Profile.Name FROM User WHERE id = :UserInfo.getUserId()];

		Boolean isDocuSignUser = !String.isBlank(currentUser.dsfs__DSProSFUsername__c);
		Boolean isAdmin = String.valueOf(currentUser.Profile.Name).containsIgnoreCase('admin');
		Boolean authorizedToSign1stIsPopulated = !String.isBlank(oppy.Account.Authorized_to_sign_1st__c);
		Boolean authorizedToSign2ndIsPopulated = !String.isBlank(oppy.Account.Authorized_to_sign_2nd__c);
		Boolean oppyIsOpen = !Boolean.valueOf(oppy.IsClosed);
		Boolean basketStatusEqualsContractCreated = oppy.Primary_Basket__r.cscfga__Basket_Status__c == 'Contract created';
		Boolean oppyJourneyEqualsSales = oppy.Select_Journey__c == 'Sales';
		Boolean noCompletedDocuSignEnvelopes = !isDocuSignEnvelopeCompleted(oppy);

		if (isDocuSignUser && isAdmin && oppyJourneyEqualsSales) {
			return true;
		}
		if (
			isDocuSignUser &&
			(authorizedToSign1stIsPopulated || authorizedToSign2ndIsPopulated) &&
			oppyIsOpen &&
			basketStatusEqualsContractCreated &&
			oppyJourneyEqualsSales &&
			noCompletedDocuSignEnvelopes
		) {
			return true;
		}

		return false;
	}

	/*
	 * @description Attachments on an opportunity are currently used by DocuSign process to
	 *  attach relevant documents against the envelope. Because, later in the process, these
	 *  documents live elsewhere, we want to clean the opportunity. This method deletes
	 *  DocuSign relevant documents from an opportunity
	 * @param List<Opportunity> a list of opportunities where attachments need to be deleted from.
	 */
	public static void deleteDocuSignAttachments(List<Opportunity> opportunities) {
		if (opportunities == null || opportunities.isEmpty()) {
			return;
		}
		Set<String> contractSummaryIds = new Set<String>();
		Set<Id> oppIds = new Set<Id>();
		for (Opportunity opp : opportunities) {
			oppIds.add(opp.Id);
			contractSummaryIds.add(opp.Contract_Summary_Attachment_Id__c);
		}

		delete [SELECT Id FROM Attachment WHERE ParentId IN :oppIds AND Id NOT IN :contractSummaryIds];

		delete [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId IN :oppIds];

		for (Opportunity opp : opportunities) {
			opp.Docusign_Attachment_Status__c = Constants.DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_REMOVED;
		}
	}

	/*
	 * @description Any opportuntiy can have multiple docusign status records connected to it.
	 *  Each of those records has their own envelope status. If one of those statuses is Completed,
	 *  it means that contract has been signed by both parties. This method checks for that.
	 * @param Opportunity an opportunity to check for DocuSign completion
	 * @return true if a Completed docusign status is found, false otherwise
	 */
	private static Boolean isDocuSignEnvelopeCompleted(Opportunity oppy) {
		for (dsfs__DocuSign_Status__c docuSignStatus : oppy.dsfs__R00N80000002fD9vEAE__r) {
			if (docuSignStatus.dsfs__Envelope_Status__c == DOCUSIGN_ENVELOPE_STATUS_COMPLETED) {
				return true;
			}
		}
		return false;
	}

	@AuraEnabled
	public static String resolveDocuSignFieldsOnOpportunity(Id oppyId) {
		new Flow.Interview.Docusign_Signer_Update_Autolaunched(new Map<String, Object>{ 'recordId' => oppyId }).start();
		Opportunity oppy = fetchOpportunity(oppyId);
		validateOpportunity(oppy);
		Map<String, String> response = new Map<String, String>{
			'DS_Authorized_Internal_Signer_1_Name' => oppy.DS_Authorized_Internal_Signer_1__r.Name,
			'DS_Authorized_Internal_Signer_1_Mail' => oppy.DS_Authorized_Internal_Signer_1__r.Email,
			'DS_Authorized_Internal_Signer_2_Name__c' => oppy.DS_Authorized_Internal_Signer_2_Name__c,
			'DS_Authorized_Internal_Signer_2_Mail__c' => oppy.DS_Authorized_Internal_Signer_2_Mail__c,
			'DS_Authorized_to_sign_1st_Name__c' => oppy.DS_Authorized_to_sign_1st_Name__c,
			'DS_Authorized_to_sign_1st_Mail__c' => oppy.DS_Authorized_to_sign_1st_Mail__c,
			'DS_Authorized_to_sign_2nd_Name__c' => oppy.DS_Authorized_to_sign_2nd_Name__c,
			'DS_Authorized_to_sign_2nd_Mail__c' => oppy.DS_Authorized_to_sign_2nd_Mail__c
		};
		return JSON.serialize(response);
	}

	@AuraEnabled
	public static String prepareOpportunityAndCreateEnvelopeURL(Id oppyId, Boolean skipResolvingDocusignOppyFields) {
		try {
			if (!skipResolvingDocusignOppyFields) {
				resolveDocuSignFieldsOnOpportunity(oppyId);
			}
			Opportunity opp = fetchOpportunity(oppyId);
			resolveDocuSignAttachmentsOnOpportunity(opp);
			validateOpportunity(opp);
			Map<String, String> enveloperParams = assembleDocuSignEnvelopeParameters(opp);
			return createDocuSignEnvelopePageReference(enveloperParams).getUrl();
		} catch (DmlException e) {
			if (e.getMessage().contains('INVALID_CROSS_REFERENCE_KEY')) {
				throw getAuraException(
					'Something went wrong in inserting the Opportunity attachments. Make sure the related contract conditions refer to existing records in this org. ' +
					e.getMessage()
				);
			}
			throw getAuraException('Something went wrong: ' + e.getMessage());
		} catch (Exception e) {
			throw getAuraException('Something went wrong: ' + e.getMessage());
		}
	}

	public static Opportunity fetchOpportunity(Id oppyId) {
		Opportunity result = [
			SELECT
				Id,
				Docusign_Attachment_Status__c,
				OwnerId,
				Segment__c,
				Contract_Summary_Attachment_Id__c,
				Enterprise_Legal_Case__c,
				DS_Authorized_Internal_Signer_1__c,
				DS_Authorized_Internal_Signer_1__r.Name,
				DS_Authorized_Internal_Signer_1__r.Email,
				DS_Authorized_to_sign_1st_Name__c,
				DS_Authorized_to_sign_1st_Mail__c,
				DS_Authorized_Internal_Signer_2_Name__c,
				DS_Authorized_Internal_Signer_2_Mail__c,
				DS_Authorized_to_sign_2nd_Name__c,
				DS_Authorized_to_sign_2nd_Mail__c,
				DS_Mail_Message_Digital_Signing__c,
				DS_Mail_Subject_Digital_Signing__c,
				DocuSign_On_Click_Option__c
			FROM Opportunity
			WHERE Id = :oppyId
		];
		return result;
	}

	private static void resolveDocuSignAttachmentsOnOpportunity(Opportunity oppy) {
		Boolean dsAttachmentStatusIsBlank = String.isBlank(oppy.Docusign_Attachment_Status__c);
		Boolean dsAttachmentStatusIsDefault = oppy.Docusign_Attachment_Status__c == Constants.DOCUSIGN_ATTACHMENT_STATUS_DEFAULT;
		DocumentService documentService = DocumentService.getInstance();

		deleteDocuSignAttachments(new List<Opportunity>{ oppy });

		List<CS_DocItem> documents = CS_OppDocController.getDocuments(oppy.Id);
		if (oppy.Enterprise_Legal_Case__c == null) {
			documentService.attachDocumentsToSObjectForRegularAttachments(oppy.Id, documents);
		}
		if (oppy.Enterprise_Legal_Case__c != null) {
			Set<Id> caseAttachmentIds = documentService.getCaseAttachmentIdsByCaseId(oppy.Enterprise_Legal_Case__c);
			documentService.attachDocumentsToSObjectFromSObject(oppy.Id, caseAttachmentIds);
		}
		documentService.attachDocumentsToSObjectForContractConditions(oppy.Id, documents);

		oppy.Docusign_Attachment_Status__c = Constants.DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED;
		update oppy;
	}

	private static List<Contract_Summary_Setting__mdt> fetchContractSummarySettings(String opportunitySegment) {
		return [SELECT Contract_Summary_Required__c FROM Contract_Summary_Setting__mdt WHERE Segment__c = :opportunitySegment];
	}

	private static void validateOpportunity(Opportunity oppy) {
		validateEmptyParams(
			new Map<String, String>{
				'DS_Authorized_Internal_Signer_1_Name' => oppy.DS_Authorized_Internal_Signer_1__r.Name,
				'DS_Authorized_Internal_Signer_1_Mail' => oppy.DS_Authorized_Internal_Signer_1__r.Email,
				'DS_Authorized_to_sign_1st_Mail__c' => oppy.DS_Authorized_to_sign_1st_Mail__c,
				'DS_Authorized_to_sign_1st_Name__c' => oppy.DS_Authorized_to_sign_1st_Name__c
			}
		);

		Boolean secondSignatoryExists = String.isNotBlank(oppy.DS_Authorized_to_sign_2nd_Name__c);
		if (secondSignatoryExists) {
			validateEmptyParams(
				new Map<String, String>{
					'DS_Authorized_to_sign_2nd_Mail__c' => oppy.DS_Authorized_to_sign_2nd_Mail__c,
					'DS_Authorized_to_sign_2nd_Name__c' => oppy.DS_Authorized_to_sign_2nd_Name__c
				}
			);
		}

		Boolean secondInternalSignerExists = String.isNotBlank(oppy.DS_Authorized_Internal_Signer_2_Name__c);
		if (secondInternalSignerExists) {
			validateEmptyParams(
				new Map<String, String>{
					'DS_Authorized_Internal_Signer_2_Mail__c' => oppy.DS_Authorized_Internal_Signer_2_Mail__c,
					'DS_Authorized_Internal_Signer_2_Name__c' => oppy.DS_Authorized_Internal_Signer_2_Name__c
				}
			);
		}

		List<Contract_Summary_Setting__mdt> contructSummarySettings = fetchContractSummarySettings(oppy.Segment__c);
		List<CS_DocItem> documents = CS_OppDocController.getDocuments(oppy.Id);
		if (contructSummarySettings.size() > 0 && contructSummarySettings[0].Contract_Summary_Required__c) {
			validateContractSummaryDocumentExists(documents);
		}
	}

	@TestVisible
	private static Map<String, String> assembleDocuSignEnvelopeParameters(Opportunity oppy) {
		Map<String, String> params = new Map<String, String>{
			'SourceID' => oppy.Id,
			'DSEID' => '0',
			'CEM' => oppy.DS_Mail_Message_Digital_Signing__c,
			'CES' => oppy.DS_Mail_Subject_Digital_Signing__c,
			'OCO' => String.isBlank(oppy.DocuSign_On_Click_Option__c) ? 'Send' : oppy.DocuSign_On_Click_Option__c,
			'LF' => '1',
			'CRL' => 'Email~' +
			oppy.DS_Authorized_Internal_Signer_1__r.Email +
			';' +
			'LastName~' +
			oppy.DS_Authorized_Internal_Signer_1__r.Name +
			';' +
			'RoutingOrder~1' +
			';' +
			'Role~Signer 1, ' +
			'Email~' +
			oppy.DS_Authorized_to_sign_1st_Mail__c +
			';' +
			'LastName~' +
			oppy.DS_Authorized_to_sign_1st_Name__c +
			';' +
			'RoutingOrder~2' +
			';' +
			'Role~Signer 3',
			'CCRM' => 'Signer 1~Signer 1;Signer 3~Signer 3',
			'CCTM' => 'Signer 1~Signer;Signer 3~Signer'
		};

		Boolean secondSignatoryExists = String.isNotBlank(oppy.DS_Authorized_to_sign_2nd_Name__c);
		if (secondSignatoryExists) {
			params.put(
				'CRL',
				params.get('CRL') +
				',Email~' +
				oppy.DS_Authorized_to_sign_2nd_Mail__c +
				';' +
				'LastName~' +
				oppy.DS_Authorized_to_sign_2nd_Name__c +
				';' +
				'RoutingOrder~2' +
				';' +
				'Role~Signer 4'
			);
			params.put('CCRM', params.get('CCRM') + ';Signer 4~Signer 4');
			params.put('CCTM', params.get('CCTM') + ';Signer 4~Signer');
		}

		Boolean secondInternalSignerExists = String.isNotBlank(oppy.DS_Authorized_Internal_Signer_2_Name__c);
		if (secondInternalSignerExists) {
			params.put(
				'CRL',
				params.get('CRL') +
				',Email~' +
				oppy.DS_Authorized_Internal_Signer_2_Mail__c +
				';' +
				'LastName~' +
				oppy.DS_Authorized_Internal_Signer_2_Name__c +
				';' +
				'RoutingOrder~1' +
				';' +
				'Role~Signer 2'
			);
			params.put('CCRM', params.get('CCRM') + ';Signer 2~Signer 2');
			params.put('CCTM', params.get('CCTM') + ';Signer 2~Signer');
		}
		return params;
	}

	@TestVisible
	private static Pagereference createDocuSignEnvelopePageReference(Map<String, String> params) {
		PageReference ref = new PageReference('/apex/dsfs__DocuSign_CreateEnvelope');
		ref.setRedirect(true);
		ref.getParameters().putAll(params);
		return ref;
	}

	@TestVisible
	private static void validateEmptyParams(Map<String, String> paramsToValidateIfEmpty) {
		List<String> emptyParams = new List<String>();
		for (String param : paramsToValidateIfEmpty.keySet()) {
			if (String.isBlank(paramsToValidateIfEmpty.get(param))) {
				emptyParams.add(param);
			}
		}
		if (emptyParams.size() > 0) {
			throw new DocuSignButtonControllerException('Missing required params on the Opportunity: ' + emptyParams);
		}
	}

	@TestVisible
	private static void validateContractSummaryDocumentExists(List<CS_DocItem> opportunityDocuments) {
		Boolean result = false;
		for (CS_DocItem csDocItem : opportunityDocuments) {
			if (csDocItem.isContractSummary) {
				result = true;
				break;
			}
		}
		if (!result) {
			throw getAuraException('We found no contract summary document attached to the opportunity!');
		}
	}

	private static AuraHandledException getAuraException(String errorMessage) {
		AuraHandledException exceptionToThrow = new AuraHandledException(errorMessage);
		exceptionToThrow.setMessage(errorMessage);
		return exceptionToThrow;
	}

	public class DocuSignButtonControllerException extends Exception {
	}
}
