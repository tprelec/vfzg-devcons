public with sharing class CS_COMDeliveryComponentUpdateWrapper {
    public CS_COMDeliveryComponentUpdateWrapper(){

    }

    @AuraEnabled
    public String deliveryComponentId;

    @AuraEnabled
    public String checkboxType;

    @AuraEnabled
    public Boolean checkboxValue;
    
    @AuraEnabled
    public Datetime value;
}