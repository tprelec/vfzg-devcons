public with sharing class ZiggoCampaignTriggerHandler extends TriggerHandler {
    public override void BeforeInsert() {
        this.updateParent(this.newList);
    }

    public override void BeforeUpdate() {
        this.updateParent(this.newList);
    }

    private void updateParent(List<ZiggoCampaign__c> ziggoCampaignList) {
        List<ZiggoCampaign__c> newZiggoOppList = (List<ZiggoCampaign__c>) ziggoCampaignList;
        Map<Id, Id> parentCampaignMap = new Map<Id, Id>();
        for (ZiggoCampaign__c zc : newZiggoOppList) {
            if (!String.isEmpty(zc.Parent_ID__c)) {
                parentCampaignMap.put(zc.Parent_ID__c, null);
            }
        }
        if (parentCampaignMap.size() > 0 ) {
            List<ZiggoCampaign__c> tmpList = [Select Id, External_ID__c From ZiggoCampaign__c Where External_ID__c IN : parentCampaignMap.keySet()];
            for (ZiggoCampaign__c tmpZc : tmpList) {
                parentCampaignMap.put(tmpZc.External_ID__c, tmpZc.Id);
            }
        }

        for (ZiggoCampaign__c zc : newZiggoOppList) {
            if (!String.isEmpty(zc.Parent_ID__c)) {
                if (parentCampaignMap.get(zc.Parent_ID__c) != null) {
                    zc.Parent__c = parentCampaignMap.get(zc.Parent_ID__c);
                }
            }
        }
    }
}