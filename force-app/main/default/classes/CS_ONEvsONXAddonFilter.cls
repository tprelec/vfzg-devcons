global with sharing class CS_ONEvsONXAddonFilter extends cscfga.ALookupSearch {
     
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
    
    Integer contractDuration = ((searchFields.get('Contract Duration') == null)||(searchFields.get('Contract Duration') == ''))?0:Integer.valueOf(searchFields.get('Contract Duration'));
    String CSPricePlan = searchFields.get('CS_PricePlan');
    String userSkillLevel = String.valueOf(searchFields.get('User Skill level'));
    String oneNetScenario = searchFields.get('One Net Scenario');
    Set<String> addOnExternalIds = new Set<String> {'AD-0000337','AD-0000255','AD-0000336','AD-0000256'};
    List<cspmb__Price_Item_Add_On_Price_Item_Association__c> result = new List<cspmb__Price_Item_Add_On_Price_Item_Association__c>();
    List<cspmb__Price_Item_Add_On_Price_Item_Association__c> unfilteredResult = [SELECT id, name, cspmb__add_on_price_item__r.name, cspmb__group__c, cspmb__add_on_price_item__r.cspmb__one_off_charge__c, cspmb__add_on_price_item__r.cspmb__recurring_charge__c, cspmb__max__c, cspmb__default_quantity__c, cspmb__price_item__c, cspmb__add_on_price_item__c, cspmb__add_on_price_item__r.skilled_based_level__c, cspmb__min__c, cspmb__add_on_price_item__r.cspmb__one_off_charge_external_id__c, cspmb__add_on_price_item__r.max_duration__c, cspmb__add_on_price_item__r.discount_allowed__c, add_on_type__c, cspmb__add_on_price_item__r.image_url__c, cspmb__add_on_price_item__r.min_duration__c, cspmb__add_on_price_item__r.recurringthresholdgroup__c, cspmb__add_on_price_item__r.one_off_thresholdgroup__c, cspmb__add_on_price_item__r.cspmb__recurring_charge_external_id__c, cspmb__add_on_price_item__r.product__c, cspmb__add_on_price_item__r.cspmb__recurring_cost__c, cspmb__add_on_price_item__r.cspmb__one_off_cost__c, cspmb__add_on_price_item__r.recurring_charge_product__c, cspmb__add_on_price_item__r.one_off_charge_product__c, cspmb__add_on_price_item__r.cspmb__product_definition_name__c, cspmb__add_on_price_item__r.cspmb__add_on_price_item_description__c, cspmb__add_on_price_item__r.External_ID__c
                                                                       FROM cspmb__Price_Item_Add_On_Price_Item_Association__c 
                                                                       WHERE cspmb__add_on_price_item__r.Max_Duration__c > :contractDuration AND cspmb__add_on_price_item__r.Min_Duration__c <= :contractDuration AND cspmb__Price_Item__c = :CSPricePlan AND cspmb__add_on_price_item__r.Skilled_Based_Level__c <= :userSkillLevel];

    for (cspmb__Price_Item_Add_On_Price_Item_Association__c filteredResult : unfilteredResult) {
        if (oneNetScenario != 'One Net Express' || !addOnExternalIds.contains(filteredResult.cspmb__add_on_price_item__r.External_ID__c) ) {
          result.add(filteredResult);       
        } 
    }
    
    return result; 

    }


    public override String getRequiredAttributes() { 
  
      return '["Contract Duration","CS_PricePlan","User Skill level","One Net Scenario"]';
      
    } 
}