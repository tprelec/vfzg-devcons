/**
 * @description:    Abstract class for all dealer SObject builder classes
 * @testClass:      TestBICC_DealerSObjectBuilderAbstract
 **/
public inherited sharing abstract class BICC_DealerSObjectBuilderAbstract {
	protected Map<String, Schema.SObjectField> sObjectFieldsPerFieldName;

	protected List<SObject> biccSObjectRecords;
	protected Map<String, Field_Sync_Mapping__c> biccFieldSyncMappingForDestinationField;

	protected Map<Id, SObject> generatedRecordPerBICCSobjectId = new Map<Id, SObject>();

	/**
	 * @description:    Sets value of sObjectFieldsPerFieldName field
	 * @param:          sObjectFieldsPerFieldName - value to set
	 * @return          BICC_DealerSObjectBuilderAbstract - instance of this
	 **/
	public BICC_DealerSObjectBuilderAbstract withSobjectFieldsPerFieldName(Map<String, Schema.SObjectField> sObjectFieldsPerFieldName) {
		this.sObjectFieldsPerFieldName = sObjectFieldsPerFieldName;

		return this;
	}

	/**
	 * @description:    Sets value of biccSObjectRecords field
	 * @param:          biccSObjectRecords - value to set
	 * @return          BICC_DealerSObjectBuilderAbstract - instance of this
	 **/
	public BICC_DealerSObjectBuilderAbstract withBICCSObjectRecords(List<BICC_Dealer_Information__c> biccSObjectRecords) {
		this.biccSObjectRecords = biccSObjectRecords;

		return this;
	}

	/**
	 * @description:    Sets value of biccFieldSyncMappingForDestinationField field
	 * @param:          biccFieldSyncMappingForDestinationField - value to set
	 * @return          BICC_DealerSObjectBuilderAbstract - instance of this
	 **/
	public BICC_DealerSObjectBuilderAbstract withBICCFieldSyncMappingForDestinationField(
		Map<String, Field_Sync_Mapping__c> biccFieldSyncMappingForDestinationField
	) {
		this.biccFieldSyncMappingForDestinationField = biccFieldSyncMappingForDestinationField;

		return this;
	}

	/**
	 * @description:    Returns generated record per BICC SObject id map
	 * @return          Map<Id, SObject> - generated record per BICC sobject id map
	 **/
	public Map<Id, SObject> getRecordsPerId() {
		return this.generatedRecordPerBICCSobjectId;
	}

	/**
	 * @description:    Executes logic to build SObject
	 * @return          BICC_DealerSObjectBuilderAbstract - instance of this
	 **/
	public abstract BICC_DealerSObjectBuilderAbstract build();
}
