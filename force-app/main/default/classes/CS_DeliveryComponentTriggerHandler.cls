public with sharing class CS_DeliveryComponentTriggerHandler extends TriggerHandler{
    public override void beforeUpdate() {
    }

    public override void beforeInsert() {
    }
    
    public override void afterUpdate(){
    }

    public override void afterInsert(){
        Map<Id,Delivery_Component__c> newMap = (Map<Id,Delivery_Component__c>)Trigger.newMap;
        Set<Id> deliveryComponentIds = new Set<Id>();
        List<SObject> objectsToUpdate = new List<SObject>();

        for (Id recordId : newMap.keySet()) {
            if (newMap.get(recordId).Installation_Required__c) {
                deliveryComponentIds.add(recordId);
            }
        }

        List<Delivery_Component__c> deliveryComponents = [SELECT Id,
                                                                Name,
                                                                Service__c,
                                                                Service__r.csord__Subscription__c,
                                                                Service__r.COM_Delivery_Order__c,
                                                                Service__r.COM_Delivery_Order__r.Order__c
                                                            FROM Delivery_Component__c
                                                            WHERE Id IN : deliveryComponentIds];

        for (Delivery_Component__c deliveryComponentRecord : deliveryComponents) {
            if (deliveryComponentRecord.Service__r.COM_Delivery_Order__c != null) {
                COM_Delivery_Order__c newDeliveryOrder = new COM_Delivery_Order__c(
                        Id = deliveryComponentRecord.Service__r.COM_Delivery_Order__c,
                        Installation_Required__c = true
                );
                objectsToUpdate.add(newDeliveryOrder);
            }
        }

        if (!objectsToUpdate.isEmpty()){
            Set<SObject> sobjectSet = new Set<SObject>();
            sobjectSet.addAll(objectsToUpdate);

            List<SObject> newSObjectsToUpdate = new List<SObject>();
            newSObjectsToUpdate.addAll(sobjectSet);
            newSObjectsToUpdate.sort();
            update newSObjectsToUpdate;
        }
    }
}