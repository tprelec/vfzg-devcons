global without sharing class CustomButtonCalculateDate extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		Savepoint sp = Database.setSavepoint();
		try {
			cscfga__Product_Basket__c basket = [
				SELECT
					Id,
					csbb__Account__c,
					Business_Mobile_Retention_Type__c,
					Number_of_CTN_Retention__c,
					Number_of_CTN_Retention_Data__c,
					Block_Expiration__c
				FROM cscfga__Product_Basket__c
				WHERE Id = :basketId
				LIMIT 1
			];
			AccountService.getInstance().setExpectedDeliveryDateForVoiceAndVoiceDataAssets(new Set<Id>{ basket.csbb__Account__c });

			if (Test.isRunningTest() || FeatureFlagging.isActive('Business_Mobile_Retention_Scenarios')) {
				setBasketCalculatedDeliveryDate(basket);
			}

			return JSON.serialize(
				new CustomButtonResponse('ok', System.Label.SUCCESS, System.Label.SUCCESS_Expected_Delivery_Date_Calculation),
				true
			);
		} catch (Exception ex) {
			Database.rollback(sp);
			return JSON.serialize(new CustomButtonResponse('error', System.Label.ERROR, System.Label.ERROR_Expected_Delivery_Date_Calculation), true);
		}
	}

	public void setBasketCalculatedDeliveryDate(cscfga__Product_Basket__c basket) {
		Account acc = [
			SELECT Id, Expected_Delivery_Date_Voice_Data__c, Expected_Delivery_Date_Voice__c
			FROM Account
			WHERE Id = :basket.csbb__Account__c
		];

		cscfga__Product_Basket__c updateBasket = new cscfga__Product_Basket__c();
		updateBasket.Id = basket.Id;
		updateBasket.Calc_Basket_Delivery_Date_Voice__c = acc.Expected_Delivery_Date_Voice__c;
		updateBasket.Calc_Basket_Delivery_Date_Voice_Data__c = acc.Expected_Delivery_Date_Voice_Data__c;

		if (basket.Business_Mobile_Retention_Type__c == 'Partial Retention') {
			// && basket.Block_Expiration__c == true
			updateBasket.Expected_Delivery_Date_for_Partial_Ret__c = calculateExpectedDeliveryDateForPartialRetention(basket);
		}

		update updateBasket;
	}

	public Date calculateExpectedDeliveryDateForPartialRetention(cscfga__Product_Basket__c basket) {
		List<VF_Asset__c> voiceAssets = new List<VF_Asset__c>();
		List<VF_Asset__c> dataAssets = new List<VF_Asset__c>();

		List<VF_Asset__c> voiceDataAssets = [
			SELECT Id, Account__c, PricePlan_Class__c, Contract_End_Date__c
			FROM VF_Asset__c
			WHERE
				Account__c = :basket.csbb__Account__c
				AND CTN_Status__c = 'Active'
				AND Contract_End_Date__c != NULL
				AND (PricePlan_Class__c = 'Mobile Voice'
				OR PricePlan_Class__c = 'Data Only')
				AND Retainable__c = TRUE
			ORDER BY Contract_End_Date__c ASC
		];

		for (VF_Asset__c asset : voiceDataAssets) {
			if (asset.PricePlan_Class__c == 'Mobile Voice') {
				voiceAssets.add(asset);
			}
			if (asset.PricePlan_Class__c == 'Data Only') {
				dataAssets.add(asset);
			}
		}

		try {
			if (basket.Number_of_CTN_Retention__c == 0 && basket.Number_of_CTN_Retention_Data__c > 0) {
				return dataAssets[Integer.valueof(basket.Number_of_CTN_Retention_Data__c) - 1].Contract_End_Date__c;
			} else if (basket.Number_of_CTN_Retention__c > 0 && basket.Number_of_CTN_Retention_Data__c == 0) {
				return voiceAssets[Integer.valueof(basket.Number_of_CTN_Retention__c) - 1].Contract_End_Date__c;
			} else if (basket.Number_of_CTN_Retention__c > 0 && basket.Number_of_CTN_Retention_Data__c > 0) {
				//if there are voice and data assets then we take the highest date from each list compare and return the one that is the highest
				Date voiceContractEndDate = voiceAssets[Integer.valueof(basket.Number_of_CTN_Retention__c) - 1].Contract_End_Date__c;
				Date dataContractEndDate = dataAssets[Integer.valueof(basket.Number_of_CTN_Retention_Data__c) - 1].Contract_End_Date__c;
				if (voiceContractEndDate > dataContractEndDate) {
					return voiceContractEndDate;
				} else {
					return dataContractEndDate;
				}
			}
		} catch (Exception ex) {
			return null;
		}
		return null;
	}
}
