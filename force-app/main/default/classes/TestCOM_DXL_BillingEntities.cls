@isTest
private class TestCOM_DXL_BillingEntities {
	@testSetup
	static void setup() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test', Email = 'contact@test.com', Phone = '+31 111111789');
		insert c;

		TestUtils.autoCommit = false;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, c.Id);
		fa.Bank_Account_Name__c = 'test bank account';
		fa.Bank_Account_Number__c = 'NL91 ABNA 0417 1643 00';
		fa.Payment_method__c = 'Invoice';
		fa.Unify_Ref_Id__c = '';
		insert fa;

		TestUtils.autoCommit = false;
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
		ba.Unify_Ref_Id__c = '';
		ba.Bill_Format__c = 'EB';
		ba.Billing_Arrangement_Alias__c = 'Test Name';
		ba.Billing_Street__c = 'test billing street';
		ba.Billing_Housenumber__c = '12';
		ba.Billing_Housenumber_Suffix__c = '';
		ba.Billing_Postal_Code__c = '11111';
		ba.Billing_City__c = 'City';
		ba.Billing_Country__c = 'Country';
		insert ba;

		Opportunity testOpportunity = CS_DataTest.createOpportunity(acct, 'Test Opportunity', owner.Id);
		testOpportunity.Financial_Account__c = fa.Id;
		testOpportunity.Billing_Arrangement__c = ba.Id;
		testOpportunity.Main_Contact_Person__c = c.Id;
		insert testOpportunity;
	}

	@isTest
	static void testMethod1() {
		List<Opportunity> oppList = [
			SELECT
				Id,
				Name,
				Financial_Account__c,
				Financial_Account__r.Payment_method__c,
				Financial_Account__r.Unify_Ref_Id__c,
				Financial_Account__r.Bank_Account_Name__c,
				Financial_Account__r.Bank_Account_Number__c,
				Financial_Account__r.Financial_Contact__r.Name,
				Billing_Arrangement__c,
				Billing_Arrangement__r.Unify_Ref_Id__c,
				Billing_Arrangement__r.Bill_Format__c,
				Billing_Arrangement__r.Billing_Arrangement_Alias__c,
				Billing_Arrangement__r.Billing_Street__c,
				Billing_Arrangement__r.Billing_Housenumber__c,
				Billing_Arrangement__r.Billing_Housenumber_Suffix__c,
				Billing_Arrangement__r.Billing_Postal_Code__c,
				Billing_Arrangement__r.Billing_City__c,
				Billing_Arrangement__r.Billing_Country__c
			FROM Opportunity
		];

		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		COM_DXL_BillingEntities billingEntities = new COM_DXL_BillingEntities(generator, oppList);
		billingEntities.generateBillingEntities();
		generator.writeEndObject();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(generator.getAsString());
		Object billingEntitiesMap = (Object) m.get('billingEntities');
		Map<String, Object> billingEntitiesItem = (Map<String, Object>) billingEntitiesMap;

		System.assertEquals(
			'EB',
			billingEntitiesItem.get('billFormat'),
			'Billing Entities -> billFormat: Expected "EB" == Actual: ' + billingEntitiesItem.get('billFormat')
		);
	}
}
