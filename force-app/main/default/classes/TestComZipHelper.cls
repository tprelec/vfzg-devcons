@isTest
public class TestComZipHelper {
	public static List<String> lSiteAddresses = new List<String>{
		'Sonny Rollingstraat',
		'Pablo Picassostraat',
		'Paul Gauguinhof',
		'Hendrik Werkmanhof',
		'Parkzichtlaan',
		'Ella Fitzgeraldplein'
	};
	public static List<Integer> lSiteHouseNumber = new List<Integer>{ 232, 187, 28, 40, 190, 6 };
	public static List<String> lSitePostalCode = new List<String>{ '3543GR', '3544NX', '3544KA', '3544MB', '3544MN', '3543EL' };
	public static List<String> lAppointmentDates = new List<String>{
		'2030-07-31T08:00:00+00:00',
		'2030-07-31T08:30:00+00:00',
		'2030-07-31T09:00:00+00:00',
		'2030-07-31T10:00:00+00:00',
		'2030-07-31T11:00:00+00:00',
		'2030-07-31T12:00:00+00:00'
	};
	public static String sCity = 'Utrecht';

	public static List<Site__c> getSiteRecords(Account testAccount) {
		List<Site__c> lSites = new List<Site__c>();
		for (Integer i = 0; i < lSiteAddresses.size(); i++) {
			String sSiteName = sCity + ', ' + lSiteAddresses.get(i) + ' ' + String.valueOf(lSiteHouseNumber.get(i));
			lSites.add(CS_DataTest.createSite(sSiteName, testAccount, lSitePostalCode.get(i), lSiteAddresses.get(i), sCity, lSiteHouseNumber.get(i)));
		}
		return lSites;
	}

	public static List<COM_Delivery_Order__c> getDeliveryOrders(Id contactId, List<Site__c> lSites, csord__Order__c testOrder) {
		List<COM_Delivery_Order__c> lDeliveryOrders = new List<COM_Delivery_Order__c>();
		for (Integer i = 0; i < lSiteAddresses.size(); i++) {
			String sSiteName = sCity + ', ' + lSiteAddresses.get(i) + ' ' + String.valueOf(lSiteHouseNumber.get(i));
			COM_Delivery_Order__c recDelOrd = CS_DataTest.createDeliveryOrder(sSiteName, testOrder.Id, false);
			recDelOrd.Site__c = lSites.get(i).Id;
			recDelOrd.Products__c = 'Zakkelijk Internet Pro';
			recDelOrd.Status__c = 'Created';
			recDelOrd.Onsite_Visit_Required__c = true;

			if (contactId != null) {
				recDelOrd.Technical_Contact__c = contactId;
			}
			lDeliveryOrders.add(recDelOrd);
		}
		return lDeliveryOrders;
	}

	public static List<Delivery_Appointment__c> getDeliveryAppointments(List<COM_Delivery_Order__c> lDeliveryOrders) {
		List<Delivery_Appointment__c> lDeliveryAppointments = new List<Delivery_Appointment__c>();
		for (Integer i = 0; i < lDeliveryOrders.size(); i++) {
			COM_Delivery_Order__c selDelOrd = lDeliveryOrders.get(i);
			Delivery_Appointment__c deliAppt = new Delivery_Appointment__c();
			deliAppt.Name = selDelOrd.Name;
			deliAppt.Start__c = (DateTime) JSON.deserialize('"' + lAppointmentDates.get(i) + '"', DateTime.class);
			deliAppt.Duration__c = 120;
			deliAppt.Location__c = selDelOrd.Name;
			if (selDelOrd.Technical_Contact__c != null) {
				deliAppt.Contact__c = selDelOrd.Technical_Contact__c;
			} else {
				deliAppt.Contact__c = null;
			}
			deliAppt.Description__c = '';
			deliAppt.Related_Delivery_Order__c = selDelOrd.Id;
			deliAppt.Field_Engineer__c = UserInfo.getUserId();
			lDeliveryAppointments.add(deliAppt);
		}
		return lDeliveryAppointments;
	}
}
