/*
 * @author Rahul Sharma
 * @date 06-05-2022
 *
 * @description Class used as a wrapper to pass a uniform data from apex to LWC
 */

public class LightningResponse {
	@TestVisible
	private enum StatusEnum {
		SUCCESS,
		ERROR,
		INFO,
		WARNING
	}

	@AuraEnabled
	public Object body;

	@AuraEnabled
	public String message;

	@AuraEnabled
	public String variant;

	// empty constructor is required for builder pattern
	@SuppressWarnings('PMD.EmptyStatementBlock')
	public LightningResponse() {
	}

	public LightningResponse setBody(Object body) {
		this.body = body;
		return this;
	}

	public LightningResponse setSuccess(String message) {
		this.message = message;
		this.variant = getSuccessVariant();
		return this;
	}

	public LightningResponse setError(String message) {
		this.message = message;
		this.variant = getErrorVariant();
		return this;
	}

	public LightningResponse setWarning(String message) {
		this.message = message;
		this.variant = getWarningVariant();
		return this;
	}

	public LightningResponse setInfo(String message) {
		this.message = message;
		this.variant = getInfoVariant();
		return this;
	}

	@TestVisible
	private static String getSuccessVariant() {
		return getVariant(StatusEnum.SUCCESS);
	}

	@TestVisible
	private static String getErrorVariant() {
		return getVariant(StatusEnum.ERROR);
	}

	@TestVisible
	private static String getWarningVariant() {
		return getVariant(StatusEnum.WARNING);
	}

	@TestVisible
	private static String getInfoVariant() {
		return getVariant(StatusEnum.INFO);
	}

	private static String getVariant(StatusEnum statusEnum) {
		return statusEnum.name().toLowerCase();
	}
}
