public with sharing class CS_COMInstallComponentController {
    private static Map<Id,Delivery_Component__c> deliveryComponentsWrappersToUpdateMap = new Map<Id,Delivery_Component__c>();
    private static String DELIVERY_COMPONENT_STATUS_IMPLEMENTED = 'Implemented';
    
    @AuraEnabled
    public static String closeTask(Id recordId) {
        return COM_CloseTaskController.closeTask(recordId);
    }

    @AuraEnabled
    public static String updateDeliveryComponents(String wrappers){
        Database.SaveResult[] returnValue;
        List<CS_COMDeliveryComponentUpdateWrapper> wrappersList = (List<CS_COMDeliveryComponentUpdateWrapper>)JSON.deserialize(wrappers,List<CS_COMDeliveryComponentUpdateWrapper>.class);

        for (CS_COMDeliveryComponentUpdateWrapper updateWrapper : wrappersList) {
            if (deliveryComponentsWrappersToUpdateMap.get(updateWrapper.deliveryComponentId) != null) {
                updateDeliveryComponent(updateWrapper);
            } else {
                createDeliveryComponent(updateWrapper);
            }
        }

        if(!deliveryComponentsWrappersToUpdateMap.values().isEmpty()){
            returnValue = Database.update(deliveryComponentsWrappersToUpdateMap.values());
        }

        return JSON.serialize(returnValue);
    }

    private static void updateDeliveryComponent(CS_COMDeliveryComponentUpdateWrapper updateWrapper){
        Delivery_Component__c delComp = deliveryComponentsWrappersToUpdateMap.get(updateWrapper.deliveryComponentId);
        delComp.Implemented_Date__c = updateWrapper.value;
        if(updateWrapper.value != null) {
            delComp.Status__c = DELIVERY_COMPONENT_STATUS_IMPLEMENTED;
        }
    }

    private static void createDeliveryComponent(CS_COMDeliveryComponentUpdateWrapper updateWrapper){
        if(updateWrapper != null) {
            
            Delivery_Component__c deliveryComponent = new Delivery_Component__c(Id = updateWrapper.deliveryComponentId, Implemented_Date__c = updateWrapper.value);
            
            if(updateWrapper.value != null) {
                deliveryComponent.Status__c = DELIVERY_COMPONENT_STATUS_IMPLEMENTED;
            }
            
            deliveryComponentsWrappersToUpdateMap.put(updateWrapper.deliveryComponentId, deliveryComponent);
        }
    }
}