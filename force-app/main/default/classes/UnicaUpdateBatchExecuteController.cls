public class UnicaUpdateBatchExecuteController {

	public UnicaUpdateBatchExecuteController(ApexPages.StandardSetController ignored) {
		
	}

	public PageReference callBatch(){
		if([SELECT Id FROM Unica_Update__c].size() > 0){
			UnicaUpdateBatch controller = new UnicaUpdateBatch();
			database.executebatch(controller);
		}
		Schema.DescribeSObjectResult prefix = Unica_Update__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}
}