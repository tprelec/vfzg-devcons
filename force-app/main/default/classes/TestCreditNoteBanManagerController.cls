@isTest
private class TestCreditNoteBanManagerController {
	@isTest
	static void testCreditNoteBanManager() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);

		//Create User with Manager
		User managedUser = TestUtils.createAdministrator();
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 1.1 =' + Limits.getQueries());

		//to allow us to modify before insert
		TestUtils.autoCommit = false;

		//Create the Partner and Dealer accounts
		list<Account> testAccounts = new List<Account>();
		testAccounts.add(TestUtils.createAccount(managedUser));
		testAccounts.add(TestUtils.createPartnerAccount());
		insert testAccounts;
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 1.2 =' + Limits.getQueries());

		// Create Partner and Dealer Contacts
		list<Contact> testContacts = new List<Contact>();
		testContacts.add(TestUtils.createContact(testAccounts[0]));
		testContacts.add(TestUtils.portalContact(testAccounts[1]));
		testContacts[1].Userid__c = managedUser.Id;
		insert testContacts;
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 1.3 =' + Limits.getQueries());

		// Create Dealer Information
		Dealer_Information__c testDealer = TestUtils.createDealerInformation(testContacts[1].Id);
		insert testDealer;
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 1.4 =' + Limits.getQueries());

		//Create customer account
		Account customerAccount = TestUtils.createAccount(managedUser, testDealer);
		insert customerAccount;
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 1.5 =' + Limits.getQueries());

		list<VF_Asset__c> newContracts = new List<VF_Asset__c>();
		newContracts.add(createAsset(customerAccount.Id, testDealer.Dealer_Code__c, '1', 'Active', '399999932', false));
		insert newContracts;
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 1.6 =' + Limits.getQueries());

		Ban__c ban = TestUtils.createBan(customerAccount);
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 1.7 =' + Limits.getQueries());

		User myAccountManager = TestUtils.createAccountManager();
		User mysalesManager = TestUtils.createManager();
		CreditNote_Approvals__c newDirector = new CreditNote_Approvals__c();
		newDirector.Name = 'Corperate';
		newDirector.User__c = myAccountManager.Id;
		newDirector.Is_Director__c = true;
		insert newDirector;

		list<VF_Contract__c> contractsVF = [
			SELECT Id, Name, Account__c, BAN__c, contract_number__c, No_of_CTN_Assets__c, Dealer_Information__c
			FROM VF_Contract__c
		];
		System.debug(LoggingLevel.INFO, '------------------ contractsVF =' + contractsVF);
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 2 =' + Limits.getQueries());
		contractsVF[0].Dealer_Information__c = testDealer.Id;
		contractsVF[0].No_of_CTN_Assets__c = 1;
		update contractsVF[0];

		Test.startTest();
		list<Credit_Note__c> testCNotes = new List<Credit_Note__c>();
		//Direct
		Credit_Note__c testCNote = new Credit_Note__c();
		testCNote.RecordTypeId = Schema.SObjectType.Credit_Note__c.getRecordTypeInfosByName().get('Credit Note PH&S').getRecordTypeId();
		testCNote.Account__c = contractsVF[0].Account__c;
		testCNote.Contract_VF__c = contractsVF[0].Id;
		testCNote.Description__c = 'Creditnote';
		testCNote.Portfolio__c = 'Site_Connectivity';
		testCNote.Product_Family__c = 'Fixed_Connectivity';
		testCNote.Product_Category__c = 'Corporate Internet';
		testCNote.Subscription__c = 'Mobile';
		testCNote.Credit_Type__c = 'Coulance';
		testCNote.Credit_Amount__c = 1;
		testCNotes.add(testCNote);

		insert testCNotes;
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 3 =' + Limits.getQueries());

		testCNote.Sales_Manager__c = mysalesManager.Id;
		testCNote.Account_Owner__c = myAccountManager.Id;
		update testCNote;

		ApexPages.StandardController controller = new ApexPages.Standardcontroller(testCNote);
		CreditNoteBanManagerController cnbmc = new CreditNoteBanManagerController(controller);

		cnbmc.getNumbers();
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 3.1 =' + Limits.getQueries());
		cnbmc.theBmData.banSelected = ban.Ban_Number__c;
		cnbmc.updateBan();
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 3.2 =' + Limits.getQueries());
		cnbmc.cancelUpdateBan();
		System.debug(LoggingLevel.INFO, '------------------ Limits.getQueries() 3.3 =' + Limits.getQueries());

		Credit_Note__c cnVerify = [SELECT Id, Customer_Ban__r.Name FROM Credit_Note__c WHERE Id = :testCNote.Id];
		System.assertEquals(ban.Ban_Number__c, cnVerify.Customer_Ban__r.Name, 'The ban is different ' + cnVerify.Customer_Ban__r.Name);
		test.stopTest();
	}

	public static VF_Asset__c createAsset(
		Id accId,
		String dealerCode,
		String contractNumber,
		String ctnStatus,
		String banNumber,
		Boolean isRetention
	) {
		VF_Asset__c newAsset = new VF_Asset__c();
		newAsset.Account__c = accId;
		newAsset.Dealer_Code__c = dealerCode;
		newAsset.Contract_Number__c = contractNumber;
		newAsset.CTN_Status__c = ctnStatus;
		newAsset.BAN_Number__c = banNumber;
		if (isRetention) {
			newAsset.Retention_Date__c = system.today();
			newAsset.Retention_Dealer_code__c = dealerCode;
			newAsset.Contract_Start_Date__c = system.today();
			newAsset.Contract_End_Date__c = newAsset.Contract_Start_Date__c.addMonths(12);
		}
		return newAsset;
	}
}
