/**
 * @description       :
 * @author            : mcubias
 * @group             : leads united
 * @last modified on  : 19-09-2022
 * @last modified by  :
 **/
public without sharing class TaskTriggerHandler extends TriggerHandler {
	private List<Task> newTasks;
	private Map<Id, Task> newTasksMap;
	private Map<Id, Task> oldTasksMap;

	private void init() {
		newTasks = (List<Task>) this.newList;
		newTasksMap = (Map<Id, Task>) this.newMap;
		oldTasksMap = (Map<Id, Task>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		copyGeolocationValuesFromLeads();
	}

	// In order to plot tasks on salesforce maps, we need to have lead's location on the task record
	private void copyGeolocationValuesFromLeads() {
		Set<Id> leadsId = new Set<Id>();
		for (Task task : this.newTasks) {
			if (task.WhoId?.getSobjectType() == Schema.Lead.SObjectType && task.Subject == 'D2D Visit') {
				leadsId.add(task.WhoId);
			}
		}
		Map<Id, Lead> relatedLeads = new Map<Id, Lead>([SELECT Id, Longitude, Latitude FROM Lead WHERE Id IN :leadsId]);
		for (Task task : this.newTasks) {
			if (relatedLeads.containsKey(task.WhoId)) {
				task.Created_Longitude__c = relatedLeads.get(task.WhoId).Longitude;
				task.Created_Latitude__c = relatedLeads.get(task.WhoId).Latitude;
			}
		}
	}
}
