public with sharing class EmailCalenderEvent {
	private static Boolean isDeliverabilityActive = null;
	public static Boolean checkDeliverability() {
		if (isDeliverabilityActive == null) {
			try {
				Messaging.reserveSingleEmailCapacity(0);
				isDeliverabilityActive = true;
			} catch (System.NoAccessException e) {
				isDeliverabilityActive = false;
			}
		}
		return isDeliverabilityActive;
	}

	@InvocableMethod(
		label='Send Calender Event'
		description='Sends email with Calender event attached.'
		category='Email'
	)
	public static void sendEmail(List<EmailRequest> requests) {
		for (EmailRequest request : requests) {
			sendEmail(				
				request.toAddresses,
				request.orgWideEmailId,
				request.textBody,
				request.textSubject,
				request.eventStart,
				request.eventEnd,
				request.organizer,
				request.attendee
			);
		}
	}

	public static void sendEmail(
		List<String> toAddresses,
		Id orgWideEmailId,
		String textBody,
		String textSubject,
		DateTime eventStart,
		DateTime eventEnd,
		User organizer,
		Lead attendee
	) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();		
		if (toAddresses != null) {
			mail.setToAddresses(toAddresses);
		}
		if (orgWideEmailId != null) {
			mail.setOrgWideEmailAddressId(orgWideEmailId);
		}
		if (textBody != null) {
			mail.setPlainTextBody(textBody);
		}
		if (textSubject != null) {
			mail.setSubject(textSubject);
		}        
		if (eventStart != null && eventEnd != null && toAddresses !=null) {
			Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();   
			attach.filename = 'reminder.ics';
			attach.ContentType = 'text/calendar; charset=utf-8; method=REQUEST';
			attach.inline = true;
			attach.body = event(eventStart,eventEnd,toAddresses,organizer,attendee);
			mail.setFileAttachments(new Messaging.EmailFileAttachment[] {attach});
		}
		if (checkDeliverability()) {
			List<Messaging.SendEmailResult> result = Messaging.sendEmail(
				new List<Messaging.SingleEmailMessage>{ mail }
			);
			if (!result[0].isSuccess()) {
				throw new EmailException(result[0].getErrors()[0].getMessage());
			}
		}
	}

	public class EmailRequest {
		@InvocableVariable(label='To Addresses')
		public List<String> toAddresses;

		@InvocableVariable(label='Org Wide Email Address Id')
		public Id orgWideEmailId;

		@InvocableVariable(label='Email Body')
		public String textBody;

		@InvocableVariable(label='Email Subject')
		public String textSubject;

		@InvocableVariable(label='Event Start Date Time')
		public DateTime eventStart;

		@InvocableVariable(label='Event End Date Time')
		public DateTime eventEnd;

		@InvocableVariable(label='Organizer User')
		public User organizer;

		@InvocableVariable(label='Attendee Lead')
		public Lead attendee;
	}

	public class EmailException extends Exception {
	}

	private static Blob event(DateTime startDT, DateTime endDT, List<String> emailList, User organizer, Lead attendee) {
		String startHours = startDT.hour()>9 ? String.valueOf(startDT.hour()) : '0'+startDT.hour();
		String startMinutes = startDT.minute()>9 ? String.valueOf(startDT.minute()) : '0'+startDT.minute();
		String endHours = endDT.hour()>9 ? String.valueOf(endDT.hour()) : '0'+endDT.hour();
		String endMinutes = endDT.minute()>9 ? String.valueOf(endDT.minute()) : '0'+endDT.minute();

		String startMonth = startDT.month()>9 ? String.valueOf(startDT.month()) : '0'+startDT.month();
		String startDay = startDT.day()>9 ? String.valueOf(startDT.day()) : '0'+startDT.day();
		String endMonth = endDT.month()>9 ? String.valueOf(endDT.month()) : '0'+endDT.month();
		String endDay = endDT.day()>9 ? String.valueOf(endDT.day()) : '0'+endDT.day();

		String startdatetime = String.valueof(startDT.year()+''+startMonth+''+startDay+'T'+startHours+''+startMinutes+'00');
		String enddatetime = String.valueof(endDT.year()+''+endMonth+''+endDay+'T'+endHours+''+endMinutes+'00');

		String txtInvite = '';
		txtInvite += 'BEGIN:VCALENDAR\n';
		txtInvite += 'PRODID:-//Microsoft Corporation//Outlook 16.0 MIMEDIR//EN\n';
		txtInvite += 'VERSION:2.0\n';
		txtInvite += 'METHOD:REQUEST\n';
		txtInvite += 'X-MS-OLK-FORCEINSPECTOROPEN:TRUE\n';
		txtInvite += 'BEGIN:VEVENT\n';
		txtInvite += 'ATTENDEE;CN="'+organizer.LastName+','+organizer.FirstName+'":mailto:'+organizer.Email+'\n';
		txtInvite += 'ATTENDEE;CN="'+attendee.LastName+','+attendee.FirstName+'":mailto:'+attendee.Email+'\n';
		txtInvite += 'CLASS:PUBLIC\n';
		txtInvite += 'CREATED:20211123T072601Z\n';
		txtInvite += 'DTEND;TZID="W. Europe Standard Time":'+enddatetime+'\n';
		txtInvite += 'DTSTAMP:20211119T095653Z\n';
		txtInvite += 'DTSTART;TZID="W. Europe Standard Time":'+startdatetime+'\n';
		txtInvite += 'LAST-MODIFIED:20211123T072601Z\n';
		txtInvite += 'LOCATION:Online\n';
		txtInvite += 'ORGANIZER;CN="'+organizer.LastName+','+organizer.FirstName+'":mailto:'+organizer.Email+'\n';
		txtInvite += 'PRIORITY:5\n';
		txtInvite += 'SEQUENCE:0\n';
		txtInvite += 'SUMMARY:Vodafone Afspraak\n';
		txtInvite += 'LANGUAGE=en-us:Meeting Reminder\n';
		txtInvite += 'TRANSP:OPAQUE\n';
		txtInvite += 'UID:'+DateTime.now()+'vodafonescheduler\n';
		txtInvite += 'X-ALT-DESC;FMTTYPE=text/html:<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN"><HTML><HEAD><META NAME="Generator" CONTENT="MS Exchange Server version 08.00.0681.000"><TITLE></TITLE></HEAD><BODY></BODY></HTML>\n';
		txtInvite += 'X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n';
		txtInvite += 'X-MICROSOFT-CDO-IMPORTANCE:1\n';
		txtInvite += 'X-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\n';
		txtInvite += 'X-MICROSOFT-DISALLOW-COUNTER:FALSE\n';
		txtInvite += 'END:VEVENT\n';
		txtInvite += 'END:VCALENDAR';
		return Blob.valueOf(txtInvite);
	}
}