/*
 *
 *  Luis Aguiar
 *  @date 2022-04-26
 *  @description trigger handler for handling the CST_Question_Bank__c object. Also to be reused in the cst_questionaire component .
 */

public class CST_QuestionBank_Handler extends TriggerHandler {
	// lists and maps for the trigger records
	private List<CST_Question_Bank__c> newQuestionBanks = (List<CST_Question_Bank__c>) this.newList;
	private Map<Id, CST_Question_Bank__c> newQuestionBankMap = (Map<Id, CST_Question_Bank__c>) this.newMap;
	private Map<Id, CST_Question_Bank__c> oldQuestionBankMap = (Map<Id, CST_Question_Bank__c>) this.oldMap;

	// initializing those references
	private void init() {
		newQuestionBanks = (List<CST_Question_Bank__c>) this.newList;
		newQuestionBankMap = (Map<Id, CST_Question_Bank__c>) this.newMap;
		oldQuestionBankMap = (Map<Id, CST_Question_Bank__c>) this.oldMap;
	}

	/*
	 * @description initializing AND validating thoses references
	 * @param none
	 * @return void
	 */

	public override void beforeInsert() {
		init();
		//validateQuestionsOrder();
	}

	/*
	 * @description method for validation, to be used in beforeInsert
	 * @param none
	 * @return void
	 */
	/*
    private void validateQuestionsOrder() {

        // several variable declarations
        List<String> productName = new List<String>();
        List<String> recordType = new List<String>();
        List<String> caseType = new List<String>();
        List<String> caseSubType = new List<String>();

        List<String> caseLevel2 = new List<String>();

        Map<String, CST_Question_Bank__c> mapExistingQuestions = new Map<String, CST_Question_Bank__c>();

        // updating the mapped objects, with lists of those properties. It needs to be be lists, to be used in the query ahead
        for(CST_Question_Bank__c qb : newQuestionBanks){
            productName.add(qb.CST_Product__c);
            recordType.add(qb.CST_Case_RecordType__c);
            caseType.add(qb.CST_Case_Type__c);
            caseSubType.add(qb.CST_Case_Level1__c);

            caseLevel2.add(qb.CST_Case_Level_2__c);
        }

        List<CST_Question_Bank__c> existingQuestions = [SELECT id, name, CST_Question_Order__c, CST_Product__c, CST_Case_RecordType__c, CST_Case_Type__c, CST_Case_Level1__c
                                                        FROM CST_Question_Bank__c
                                                        WHERE CST_Product__c IN :productName
                                                        AND CST_Case_RecordType__c IN :recordType
                                                        AND CST_Case_Type__c IN :caseType
                                                        AND CST_Case_Level1__c IN :caseSubType
                                                        AND CST_Case_Level_2__c IN :caseLevel2];

        // creating a map with Map<String, CST_Question_Bank__c>()>, where the String is a unique identifier. Used to validade if the question exists already, and in that case, returning an error
        for(CST_Question_Bank__c qb : existingQuestions){
            mapExistingQuestions.put(qb.CST_Question_Order__c+'-'+qb.CST_Product__c+'-'+qb.CST_Case_RecordType__c+'-'+qb.CST_Case_Type__c+'-'+qb.CST_Case_Level1__c+'-'+qb.CST_Case_Level_2__c, qb);
        }
        system.debug('existingQuestions'+ existingQuestions);
        system.debug('mapExistingQuestions'+ mapExistingQuestions);
        for(CST_Question_Bank__c qb : newQuestionBanks){
            String key = qb.CST_Question_Order__c+'-'+qb.CST_Product__c+'-'+qb.CST_Case_RecordType__c+'-'+qb.CST_Case_Type__c+'-'+qb.CST_Case_Level1__c+'-'+qb.CST_Case_Level_2__c;
            CST_Question_Bank__c existingQuestion = mapExistingQuestions.get(key);
            if(existingQuestion != null){
                // older format
                // qb.addError('The chosen Question Order ('+existingQuestion.CST_Question_Order__c+') is already in place for record '+existingQuestion.name+'. Please select a different one.');

                // with formated string
                String message = String.format('The chosen Question Order ({0}) is already in place for record {1}. Please select a different one.', new string[]{string.valueof(existingQuestion.CST_Question_Order__c), existingQuestion.name});
                qb.addError(message);
                
            }
        }

    }
*/

	/*
	 * @description method to be used in a the cst_questionaire component, passing the caseId. The same flag is suitable for aura components
	 * @param ID caseId
	 * @return List<CST_Question_Bank__c>
	 */

	@AuraEnabled
	public static List<CST_Question_Bank__c> CST_GetQuestionsFromBank(ID caseId) {
		// finding Case related to the question. If caseId is null, the rest of the method is invalid
		Case caseWQuest = [SELECT id, RecordType.DeveloperName, CST_Product__c, Type, CST_Level1__c, CST_Level2__c FROM Case WHERE id = :caseId];

		// using the properties found on the object
		String caseRecordType = caseWQuest.RecordType.DeveloperName;
		String caseProduct = caseWQuest.CST_Product__c;
		String caseType = caseWQuest.Type;
		String caseSubType = caseWQuest.CST_Level1__c;

		String caseLevel2 = caseWQuest.CST_Level2__c;

		System.debug('caseWQuest -> ' + caseWQuest);

		// filter questions from bank that need to go to questionnaire
		List<CST_Question_Bank__c> questionList = [
			SELECT id, CST_Question__c, CST_Question_Type__c, Name, CST_Mandatory__c, CST_Questionaire_HelpText__c
			FROM CST_Question_Bank__c
			WHERE
				CST_Product__c = :caseProduct
				AND CST_Case_RecordType__c = :caseRecordType
				AND CST_Active__c = TRUE
				AND CST_Case_Type__c = :caseType
				AND CST_Case_Level1__c = :caseSubType
				AND CST_Case_Level_2__c = :caseLevel2
			ORDER BY CST_Question_Order__c ASC
		];

		System.debug('questionList -> ' + questionList);

		return questionList;
	}

	/*
	 * @description method for checking if the current questionaire was already submitted
	 * @param ID caseId
	 * @return Boolean
	 */

	@AuraEnabled
	public static Boolean checkExistingQuestionnaire(ID caseId) {
		List<CST_Questionnaire__c> existingQuestionnaire = [SELECT id, name FROM CST_Questionnaire__c WHERE CST_Case__c = :caseId];

		return existingQuestionnaire.size() > 0 ? true : false;
	}

	/*
	 * @description handler to submit new CST_Questionnaire_Line_Item__c items, performing the necessary DML insert operation
	 * @param List<CST_Questionnaire_Line_Item__c> answers
	 * @param ID caseId
	 * @return List<CST_Questionnaire_Line_Item__c>
	 */

	@AuraEnabled
	public static CST_Questionnaire__c handleApexSubmit(List<CST_Questionnaire_Line_Item__c> answers, ID caseId) {
		CST_Questionnaire__c newQuestionnaire = new CST_Questionnaire__c(CST_Case__c = caseId, CST_Type__c = 'Advisor');
		insert newQuestionnaire;

		for (CST_Questionnaire_Line_Item__c qli : answers) {
			qli.CST_Questionnaire__c = newQuestionnaire.id;
		}
		insert answers;

		CST_Questionnaire__c newQuest = [SELECT Id, Name FROM CST_Questionnaire__c WHERE id = :newQuestionnaire.id LIMIT 1];

		return newQuest;
	}

	/*
	 * @description another lwc method, for updating item records
	 * @param List<CST_Questionnaire_Line_Item__c> answers
	 * @return void
	 */

	@AuraEnabled
	public static void updateLineItems(List<CST_Questionnaire_Line_Item__c> answers) {
		update answers;
	}
}
