@isTest
private class TestAccountGenerateFrameworkButtonContr {
    @testSetup
    static void makeData(){
        TestUtils.createAccount(new User(Id = UserInfo.getUserId()));
        insert new Framework__c(Framework_Sequence_Number__c = 99990, VodafoneZiggo_Framework_Sequence_Number__c = 99991);
    }

    @isTest
    static void generateFrameworkForVodafoneAccount() {
        Account acc = getAccount();
        acc.VZ_Framework_Agreement__c = null;
        update acc;
        AccountGenerateFrameworkButtonController.generateFrameworkForVodafoneAccount(acc.Id);
        System.assert(getAccount().Frame_Work_Agreement__c != null);
    }

    @isTest
    static void generateFrameworkForVodafoneZiggoAccount() {
        Account acc = getAccount();
        AccountGenerateFrameworkButtonController.generateFrameworkForVodafoneZiggoAccount(acc.Id);
        System.assert(getAccount().VZ_Framework_Agreement__c != null);
    }

    private static Account getAccount() {
        return [
            SELECT
                Id,
                Frame_Work_Agreement__c,
                VZ_Framework_Agreement__c
            FROM
                Account
            LIMIT 1
        ];
    }
}