/**
 * @description       : Apex Class Handler of the Clause Definition Trigger
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

public class ClauseDefinitionTriggerHandler extends TriggerHandler {
	private List<csclm__Clause_Definition__c> lstNewClauseDefinition;

	private Map<Id, csclm__Clause_Definition__c> mapOldClauseDefinition;
	private Map<Id, csclm__Clause_Definition__c> mapNewClauseDefinition;

	/**
	 * @description Initialize Class Variables
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	private void init() {
		lstNewClauseDefinition = (List<csclm__Clause_Definition__c>) this.newList;
		mapOldClauseDefinition = (Map<Id, csclm__Clause_Definition__c>) this.oldMap;
		mapNewClauseDefinition = (Map<Id, csclm__Clause_Definition__c>) this.newMap;
	}

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public void setExternalIds() {
		Sequence objSequence = new Sequence('Clause Definition');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (csclm__Clause_Definition__c objClauseDefinition : lstNewClauseDefinition) {
				objClauseDefinition.ExternalID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
