/**
 * @description			This is the class that contains logic for adding a new PBX to a site
 *						Requires WITHOUT SHARING as some background processes need to run where the user might not have authorization for
 * @author				Guy Clairbois
 */

public without sharing class PBXSelectionWizardController {

    private final Site__c site;
	public Competitor_Asset__c theCompetitorAsset {get;set;}
	public String retURL {get;set;} 
	public boolean showIFrame {get;set;}

    public PBXSelectionWizardController(ApexPages.StandardController stdController) {
    	this.thisController = stdController;
    	if(!test.isRunningTest())//addfields gives an error in test classes
    		this.thisController.addFields(New List<String>{'Id','Site_Account__c'});
        this.site = (Site__c)stdController.getRecord();
        this.theCompetitorAsset = new Competitor_Asset__c();
        
        initializePBXMap(); 
        
        system.debug(vendorToProductNameToSoftwareVersionToId);
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        showIFrame=true;
        if (retURL==null) {
        	retURL='/'+this.site.Id;
        	showIFrame = false;
        }
    }
    
    private ApexPages.StandardController thisController {get;set;}

	public Site__c getSite(){
		return this.site;
	}
	
	private Map<Id,PBX_Type__c> pbxIdToPBX = new Map<Id,PBX_Type__c>();
	public PBX_Type__c getCurrentPBX(){
		if(vendorSelected != null && productNameSelected != null && softwareVersionSelected != null){
			
			if(vendorToProductNameToSoftwareVersionToId.containsKey(vendorSelected) 
				&& vendorToProductNameToSoftwareVersionToId.get(vendorSelected).containsKey(productNameSelected)
				&& vendorToProductNameToSoftwareVersionToId.get(vendorSelected).get(productNameSelected).containsKey(softwareVersionSelected)){
						return pbxIdToPBX.get(vendorToProductNameToSoftwareVersionToId.get(vendorSelected).get(productNameSelected).get(softwareVersionSelected));
			} else {
				return null;
			}
	
		} else
			return null;	
	}

	public pageReference doSave(){
		Savepoint sp = Database.setSavepoint();
		try{
			// create a new competitor asset and insert it under the Site
			system.debug(softwareVersionSelected);
			theCompetitorAsset = createAsset(this.site,theCompetitorAsset,vendorToProductNameToSoftwareVersionToId.get(vendorSelected).get(productNameSelected).get(softwareVersionSelected));
			
			insert theCompetitorAsset;		
			this.site.PBX__c = theCompetitorAsset.Id;
			update this.site;
			return new PageReference(retURL);
			//return new PageReference('/'+this.site.Id);
			//return thisController.save();
		} catch (dmlException de){
			Database.rollback(sp);
            /*Start: changed by Sander Stulemeijer 12-2-2014
             * error message adjusted with System.Label.LABEL_Error_DML */
            Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,System.Label.LABEL_Error_DML +de));
            /*End: changed*/
			return null;
		}
	}
	
	public static Competitor_Asset__c createAsset(Site__c site, Competitor_Asset__c ca, Id pbxType){
		if(site != null){
			ca.Account__c = site.Site_Account__c;
			ca.Site__c = site.Id;
		}	
		ca.RecordTypeId = GeneralUtils.recordTypeMap.get('Competitor_Asset__c').get('PABX');
		ca.Contract_Expiration_Date__c = date.valueOf('2099-12-31');
		ca.Invoice_value_per_month__c = 0;
		ca.Quantity__c = 1;
		ca.Product__c = 'PABX';
		ca.PBX_Type__c = pbxType;
		return ca;
	}

	public PBX_Type__c newPBXType {get;set;}
	public Boolean showPBXTypeEditor {get;set;}
	
	public pageReference requestNew(){
		newPBXType = new PBX_Type__c();
		showPBXTypeEditor = true;
		return null;
	}

	public pageReference savePBXType(){
		Savepoint sp = Database.setSavepoint();
		
		try{
			insert newPBXType;
			theCompetitorAsset = createAsset(this.site,theCompetitorAsset,newPBXType.Id);		
			insert theCompetitorAsset;		
			this.site.PBX__c = theCompetitorAsset.Id;
			return thisController.save();
		} catch (dmlException de){
			Database.rollback(sp);
            /*Start: changed by Sander Stulemeijer 12-2-2014
             * error message adjusted with System.Label.LABEL_Error_DML */
            Apexpages.AddMessage(new Apexpages.Message(ApexPages.severity.ERROR,System.Label.LABEL_Error_DML +de));
            /*End: changed*/
			return null;
		}
	}
	
	public pageReference cancelPBXType(){
		showPBXTypeEditor = false;
		return new PageReference(retURL);		
		//return null;
	}
		
	/* pbx selection dropdown controllers */
	
	private void initializePBXMap(){
		vendorToProductNameToSoftwareVersionToId = new Map<String,Map<String,Map<String,String>>>();
		pbxIdToPBX = new Map<Id,PBX_Type__c>();
		for(PBX_Type__c pbx : [Select 
							Id, 
							Name, 
							Vendor__c, 
							Product_Name__c, 
							Software_Version__c,
							SIP_Certification__c,
							Office_Voice_approved__c,
							CNoIP_approved__c,
							VF_CPE__c,
							Remarks__c							
						From 
							PBX_Type__c 
						where
							Selectable__c = true
						]
		){
			pbxIdToPBX.put(pbx.Id,pbx);
			// remove leading, trailing and repeating spaces (as the picklist element will do that otherwise, resulting in conflicts)
			String vendor = pbx.Vendor__c.normalizeSpace();
			String productName = pbx.Product_Name__c.normalizeSpace();
			String softwareVersion = pbx.Software_Version__c==null?' ':pbx.Software_Version__c.normalizeSpace();
			if(vendorToProductNameToSoftwareVersionToId.containsKey(vendor)){
				Map<String,Map<String,String>> tempMap2 = vendorToProductNameToSoftwareVersionToId.get(vendor);
				if(tempMap2.containsKey(productName)){
					Map<String,String> tempMap1 = tempMap2.get(productName);
					tempMap1.put(softwareVersion, pbx.Id);
					tempMap2.put(productName,tempMap1);					
				} else {
					Map<String,String> tempMap1 = new Map<String,String>();
					tempMap1.put(softwareVersion, pbx.Id);
					tempMap2.put(productName,tempMap1);					
				}
				vendorToProductNameToSoftwareVersionToId.put(vendor,tempMap2);
				
			} else {
				Map<String,String> tempMap1 = new Map<String,String>();
				tempMap1.put(softwareVersion, pbx.Id);
				Map<String,Map<String,String>> tempMap2 = new Map<String,Map<String,String>>();
				tempMap2.put(productName,tempMap1);
				vendorToProductNameToSoftwareVersionToId.put(vendor,tempMap2);		 
			}
		}	
	}
	
	public Map<String,Map<String,Map<String,String>>> vendorToProductNameToSoftwareVersionToId {get;set;} 
	
	/* Vendor Selection dropdown */
	
	private String vendorSelected;

    public List<SelectOption> getVendors() {
        List<SelectOption> options = new List<SelectOption>();
        List<String> vendors = new List<String>();
        for(String theVendor : vendorToProductNameToSoftwareVersionToId.keySet()){
        	vendors.add(theVendor);
        }
        vendors.sort();
        for(String theVendor : vendors){
        	SelectOption so = new SelectOption(theVendor,theVendor);
        	options.add(so);
        }
        if((vendorSelected == null || vendorSelected == '') && !vendors.isEmpty())  setVendorSelected(vendors[0]);
        
        return options;
    }
        
    public String getVendorSelected() {
        return vendorSelected;
    }
        
    public void setVendorSelected(String vendorSelected) {
        this.vendorSelected = vendorSelected;
        for(String s : vendorToProductNameToSoftwareVersionToId.get(vendorSelected).keySet()){
        	setProductNameSelected(s);
        	break;
        }
    }
    
	/* ProductName Selection dropdown */
	
	private String productNameSelected;

    public List<SelectOption> getProductNames() {
        List<SelectOption> options = new List<SelectOption>();
        List<String> products = new List<String>();
        if(vendorSelected != null && vendorSelected != ''){
	        for(String theProduct : vendorToProductNameToSoftwareVersionToId.get(vendorSelected).keySet()){
	        	products.add(theProduct);
	        }
	        products.sort();
	        for(String theProduct : products){
	        	SelectOption so = new SelectOption(theProduct,theProduct);
	        	options.add(so);
	        }
	        /*if(!products.isEmpty()){
	        	setProductNameSelected(products[0]);
	        }*/
        }
        return options;
    }
        
    public String getProductNameSelected() {
        return productNameSelected;
    }
        
    public void setProductNameSelected(String productNameSelected) {
        this.productNameSelected = productNameSelected;
        for(String s : vendorToProductNameToSoftwareVersionToId.get(vendorSelected).get(productNameSelected).keySet()){
        	setSoftwareVersionSelected(s);
        	break;
        }
        
    }
    
	/* SoftwareVersion Selection dropdown */
	
	private String softwareVersionSelected;

    public List<SelectOption> getSoftwareVersions() {
        List<SelectOption> options = new List<SelectOption>();
        List<String> versions = new List<String>();
        if(productNameSelected != null && productNameSelected != '' && vendorSelected != null && vendorSelected != ''){
	        for(String theVersion : vendorToProductNameToSoftwareVersionToId.get(vendorSelected).get(productNameSelected).keySet()){
	        	versions.add(theVersion);
	        }
	        versions.sort();
	        for(String theVersion : versions){
	        	SelectOption so = new SelectOption(theVersion,theVersion);
	        	options.add(so);
	        }
	        //if(!versions.isEmpty())  setSoftwareVersionSelected(versions[0]);
	        system.debug(softwareVersionSelected);
        }
        return options;
    }
        
    public String getSoftwareVersionSelected() {
    	system.debug(softwareVersionSelected);
        return softwareVersionSelected;
    }
        
    public void setSoftwareVersionSelected(String softwareVersionSelected) {
        this.softwareVersionSelected = softwareVersionSelected;
    }
            
}