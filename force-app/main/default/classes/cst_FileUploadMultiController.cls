/**
 * @author Luis Aguiar
 * @date 2022
 *
 * @description this class connects to a web component, recieving information in a JSON format, to process case updates and file uploads, related to Remedy Tickets
 */

public class cst_FileUploadMultiController {
	private CaseInfo remedyTicket;

	// hardcoded map, for the allowed status options, in a current status, of the related External Ticket
	private static Map<String, List<String>> statusTransitionsMap = new Map<String, List<String>>{
		'Pending' => new List<String>{ 'Assigned', 'Cancelled', 'Closed' },
		'Resolved' => new List<String>{ 'Assigned' }, //detach scenario
		'Assigned' => new List<String>{ 'Cancelled', 'Closed' },
		'In Progress' => new List<String>{ 'Cancelled', 'Closed' }
	};
	/* private static Map<String, List<String>> statusTransitionsMap =  new Map<String, List<String>>{
        'Pending' => new List<String>{'Assigned', 'Cancelled'},
        'Resolved' => new List<String>{ 'Assigned'},
        'Assigned' => new List<String>{'Cancelled'},
        'In Progress' => new List<String>{'Cancelled'}
    };*/
	/**
	 * @desc inner class to describe the Case
	 */
	public class Response {
		public String error;
		public String message;
		public String ticketid;
		public String caseId;
		public String status;
		public String contentversionid;
		public String contentdocumentid;
		public String linkedentityid;
		public List<uploadInfo> files;
		public Response() {
		}
	}
	public class CaseInfo {
		public String caseId;
		public String comment;
		public String remedyStatus;
		public Decimal ticketCount;
		public List<uploadInfo> files;
		public List<StatusLine> statusLines;
	}

	/**
	 * @desc inner class to describe the uploaded files, and used for processing the JSON String
	 */
	public class uploadInfo {
		public String id;
		public String linkid;
		public String ticketid;
		public String contentversionid;
		public String contentdocumentid;
		public String linkedentityid;
		public String title;
		public String filename;
		public String fileContent;
		public String status;
		public integer fileSize;
	}
	/**
	 * @desc inner class to describe the Status choices, and used for processing the JSON String
	 */
	public class StatusLine {
		public String value;
		public String label;
		public boolean isDefault;
		public Integer index;
	}
	/**
     * @method getTicketInfo() [public]
     * @desc from Case.Id, find case and CST_External_Ticket__c information
     * @return (String) formated JSON 

    
    [{"value":"Assigned","label":"(Default) Assigned","isDefault":null,"index":1},{"value":"Cancelled","label":"Cancelled","isDefault":null,"index":2}],"remedyStatus":"Assigned",
    "files":null,"comment":null,
    "caseId":"5005r0000025psZAAQ"}
    */
	@AuraEnabled
	public static String getTicketInfo(String caseId) {
		List<CST_External_Ticket__c> externalTickets = [
			SELECT CST_Case__c, CST_Status__c, Id, Name
			FROM CST_External_Ticket__c
			WHERE CST_Case__c = :caseId AND CST_Status__c != 'Closed' AND CST_Status__c != 'Cancelled'
		];

		System.debug('externalTickets ' + externalTickets);

		// uses a private inner classe to format the method response
		CaseInfo caseInfo = new CaseInfo();
		caseInfo.caseId = caseId;
		caseInfo.remedyStatus = externalTickets[0].CST_Status__c;

		// StatusLine is also private inner class of this one
		List<StatusLine> sLines = new List<StatusLine>();

		// getting the picklist values from CST_Status__c
		Schema.DescribeFieldResult objFieldDescribe = CST_External_Ticket__c.CST_Status__c.getDescribe();
		List<Schema.PicklistEntry> lstPickListValues = objFieldDescribe.getPickListValues();

		// outputs the current status as the defualt option.
		// If selected, the value will be empty, only the label filled
		StatusLine defaultLine = new StatusLine();
		defaultLine.label = '(Default) ' + caseInfo.remedyStatus;
		defaultLine.value = caseInfo.remedyStatus;
		defaultLine.index = 1;
		System.debug('line 1: ' + defaultLine);

		sLines.add(defaultLine);
		Integer index = 2;

		// if the submitted status is one of the keys in the map,
		// the matching value is converted into a possible option for the combobox
		for (String transition : statusTransitionsMap.get(caseInfo.remedyStatus)) {
			StatusLine transitionLine = new StatusLine();
			System.debug('transition: ' + transition);
			transitionLine.label = transition;
			transitionLine.value = transition;
			transitionLine.index = index;
			System.debug('line ' + index + ': ' + transitionLine);
			sLines.add(transitionLine);
			index++;
		}

		System.debug('sLines ' + sLines);
		caseInfo.statusLines = sLines;

		String JSONOut = JSON.serialize(caseInfo);
		System.debug('JSONOut ' + JSONOut);

		return JSONOut;
	}
	@AuraEnabled
	public static String deleteFiles(String recordid) {
		Response response = new Response();
		try {
			System.debug('deleteFiles recordid ' + recordid);

			List<ContentVersion> contentVersions = [
				SELECT Id, Title, ContentDocumentId
				FROM ContentVersion
				WHERE ContentDocumentId = :recordid
				LIMIT 1
			];

			System.debug('contentVersions ' + contentVersions);

			List<ContentDocumentLink> links = [
				SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility
				FROM ContentDocumentLink
				WHERE contentdocumentid = :contentVersions[0].ContentDocumentId
			];
			System.debug('links: ' + links);

			String entityId = links[0].LinkedEntityId;
			System.debug('entityId ' + entityId);

			List<String> linkIds = new List<String>();
			for (ContentDocumentLink cds : links) {
				linkIds.add(cds.contentdocumentid);
			}
			System.debug('linkIds: ' + linkIds);

			List<ContentDocument> cds = [SELECT Id, Title, ParentId, FileType, ContentAssetId FROM ContentDocument WHERE id IN :(linkIds)];
			System.debug('cds ' + cds);

			response.error = '';
			response.status = 'success';
			delete cds;
		} catch (Exception ex) {
			response.error = '';
			response.status = 'error';
			String template = 'error: {0} detail: {1}';
			response.message = String.format(template, new List<String>{ ex.getMessage(), ex.getStackTraceString() });

			System.debug(ex.getMessage() + ' -> ' + ex.getStackTraceString());
			// throw new AuraHandledException(ex.getMessage());
		}

		String strResponse = JSON.serialize(response);
		System.debug('strResponse ' + strResponse);
		return JSON.serialize(strResponse);
	}
	@AuraEnabled
	public static String getFiles(String ids) {
		System.debug('getFiles --> recordid ' + ids);

		System.debug('validateFiles ids ' + ids);
		List<String> idList = (List<String>) JSON.deserialize(ids, List<String>.class);

		// Map<Id, String> mapStatus = (Map<Id, String>)JSON.deserialize(mapStatusByIds, Map<Id, String>.class);

		System.debug('validateFiles idList ' + idList);

		List<ContentDocumentLink> contDocLinks = [
			SELECT ContentDocumentId, LinkedEntityId, ContentDocument.Title, id, ContentDocument.ContentSize
			FROM ContentDocumentLink
			WHERE ContentDocumentId IN :idList
		];
		System.debug('getFiles contDocLinks ' + contDocLinks);

		Map<String, uploadInfo> fileMapOutput = new Map<String, uploadInfo>();

		Map<String, ContentDocumentLink> contDocLinksMap = new Map<String, ContentDocumentLink>();
		for (ContentDocumentLink cdl : contDocLinks) {
			contDocLinksMap.put(cdl.ContentDocumentId, cdl);
		}

		System.debug('getFiles contDocLinksMap ' + contDocLinksMap);

		String warning;
		String msg;
		Integer fSize;
		Response response = new Response();

		for (ContentDocumentLink linkDoc : contDocLinksMap.values()) {
			uploadInfo fi = new uploadInfo();

			fSize = linkDoc.ContentDocument.ContentSize;

			response.status = 'success';

			fi.status = 'smallFile';
			fi.title = linkDoc.ContentDocument.Title;
			fi.id = linkDoc.id;
			fi.contentdocumentid = linkDoc.contentdocumentid;
			fi.linkedentityid = linkDoc.LinkedEntityId;
			fi.fileSize = fSize;

			fileMapOutput.put(fi.ContentDocumentId, fi);
		}
		System.debug('getFiles fileMapOutput ' + fileMapOutput);

		response.files = fileMapOutput.values();

		String respString = JSON.serialize(response);
		return respString;
	}

	/**
   * @method uploadFiles() [public]
   * @desc saves input from LWC component, including uploaded files as Documents and CaseComment
   * 
   * @param {String} recordId - matching Case.Id
   * @param {String} comment - comment inserted by the user
   * @param {String} status - status inserted by the user

   {[
       {"base64":"(file binary information)", title: "image title"}
    ]}
    
   * @return {ContentVersion} - returns now only '', but can be formatted with a JSON response
   */

	@AuraEnabled
	public static String createWorkLog(String recordId, String comment, String status) {
		System.debug('comment -> ' + comment);
		System.debug('status -> ' + status);

		Response response = new Response();
		if (String.isBlank(recordId) || String.isBlank(comment))
			return '';

		User integrationUser = [SELECT id, username FROM User WHERE alias = 'integ' LIMIT 1];

		CST_External_Ticket__c remedyTicket = [
			SELECT CST_Case__c, CST_Status__c, Id, Name, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			WHERE CST_Case__c = :recordId AND CST_Status__c != 'Closed' AND CST_Status__c != 'Cancelled'
			LIMIT 1
		];

		response.ticketid = remedyTicket.Id;

		Case ttCase = [SELECT id, CST_Case_is_with_Team__c, CST_Sub_Status__c, OwnerId, CST_End2End_Owner__c, Reason FROM Case WHERE id = :recordId];

		try {
			if (String.isNotBlank(status)) {
				if (status == 'Assigned') {
					System.debug('status Assigned: ' + status);
					System.debug('status Assigned 2: ' + (status == 'Assigned'));

					// ticket
					remedyTicket.CST_Status__c = status;
					remedyTicket.CST_Status_Reason__c = '';

					// case
					ttCase.CST_Sub_Status__c = status;
					ttCase.CST_Case_is_with_Team__c = 'External - Remedy';
					ttCase.OwnerId = integrationUser.Id;
				} else if (status == 'Cancelled') {
					System.debug('status Cancelled: ' + status);
					System.debug('status Cancelled 2: ' + (status == 'Cancelled'));

					// ticket
					remedyTicket.CST_Status__c = status;
					remedyTicket.CST_Status_Reason__c = '';

					// case
					ttCase.CST_Sub_Status__c = 'Working on it';
					ttCase.CST_Case_is_with_Team__c = 'Internal - Advisor';
					ttCase.OwnerId = ttCase.CST_End2End_Owner__c;
				} else if (status == 'Closed') {
					System.debug('status Closed: ' + status);
					System.debug('status Closed 2: ' + (status == 'Closed'));

					// ticket
					remedyTicket.CST_Status__c = status;
					remedyTicket.CST_Status_Reason__c = '';

					// case
					ttCase.OwnerId = ttCase.CST_End2End_Owner__c;
					ttCase.Reason = 'Case interim Resolved';
				}

				remedyTicket.CST_Status_Reason__c = '';
				System.debug('input status: ' + status);
				System.debug('remedyTicket: ' + remedyTicket);
				System.debug('ttCase: ' + ttCase);
			}

			// Updating case Status
			if (String.isNotBlank(comment)) {
				CaseComment comm = new CaseComment(ParentId = recordId, IsPublished = false, CommentBody = '[WorkLog] ' + comment);
				insert comm;
			}
			// Updating case Comment
			// String xmlRequest = buildWorkLogToRemedyXML(recordId, comment, status, fileList);
			// remedyTicket.CST_Request_Message__c = xmlRequest;
			update remedyTicket;
			update ttCase;

			String jsonResponse = JSON.serialize(response);

			System.debug('jsonResponse: ' + jsonResponse);

			return jsonResponse;
		} catch (Exception ex) {
			System.debug(ex.getMessage() + ' -> ' + ex.getStackTraceString());
			throw new AuraHandledException(ex.getMessage());
		}
	}

	/**
     * @author Luis Lameira
     * @date 2022
     * 
     * @method buildWorkLogToRemedyXML() [public]
     * @description this class processes data into a XML format, suitable to send in a SOAP webservice request
     * 
     * @param {String} recordId - matching Case.Id
     * @param {String} comment - comment inserted by the user
     * @param {String} status - status inserted by the user
     * @param {List<uploadInfo>} fileList - list of files for upload. Uses uploadInfo inner class for formatting
     *
     * @return {String} the xml value content, in the format:
     * 
        <?xml version="1.0" encoding="UTF-8"?>
        <soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:VZ_GenericIncidentAPI">
            <soapenv:Body>
                <urn:UpdateIncident xmlns:urn="http://schemas.xmlsoap.org/soap/envelope/">
                    <urn:SourceSystem>(Source System name)</urn:SourceSystem>
                    <urn:ExternalSystem>CST</urn:ExternalSystem>
                    <urn:ExternalTicketNumber>(external ticket number)</urn:ExternalTicketNumber>
                    <urn:Action>UPDATE</urn:Action>
                    <urn:ExternalStatus>(external ticket status)</urn:ExternalStatus>
                    <urn:RemedyWorkLogNotes>(external ticket number)</urn:RemedyWorkLogNotes>
                    <urn:z2AF_Act_Attachment_1_attachmentName>(file name)</urn:z2AF_Act_Attachment_1_attachmentName>
                    <urn:z2AF_Act_Attachment_1_attachmentOrigSize>(file size)</urn:z2AF_Act_Attachment_1_attachmentOrigSize>
                    <urn:z2AF_Act_Attachment_2_attachmentName>(file name)</urn:z2AF_Act_Attachment_2_attachmentName>
                    <urn:z2AF_Act_Attachment_2_attachmentOrigSize>(file size)</urn:z2AF_Act_Attachment_2_attachmentOrigSize>
                </urn:UpdateIncident>
            </soapenv:Body>
        </soapenv:Envelope>
    */
	public static String buildWorkLogToRemedyXML(String recordId, String comment, String status, List<uploadInfo> fileList) {
		Case remedyCase = [SELECT id, casenumber FROM Case WHERE id = :recordId];

		// creates an instance of a XML object
		DOM.Document doc = new DOM.Document();

		// information for the header
		String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String urnNamespace = 'urn:VZ_GenericIncidentAPI';

		// add the 1st element, the parent of all others
		// creates <soapenv:Envelope />
		dom.XmlNode envelopeTag = doc.createRootElement('Envelope', soapNS, 'soapenv');

		// namepsace to add in all child elemetns
		envelopeTag.setNamespace('urn', urnNamespace);

		// body of a SOAP request
		dom.XmlNode body = envelopeTag.addChildElement('Body', soapNS, 'soapenv');

		// reference for a method on the remedy server
		// in "urn:UpdateIncident", the prefix is the namespace (common to every child of <soapenv:Body />), UpdateIncident is the method name
		dom.XmlNode remedyTag = body.addChildElement('UpdateIncident', soapNS, 'urn');

		// define parameters for that method, children of the prevous DOM node
		remedyTag.addChildElement('SourceSystem', soapNS, 'urn').addTextNode('VodafoneZiggo ITSM');
		remedyTag.addChildElement('ExternalSystem', soapNS, 'urn').addTextNode('CST');
		remedyTag.addChildElement('ExternalTicketNumber', soapNS, 'urn').addTextNode((String) remedyCase.casenumber);

		// action we wnat to perform
		remedyTag.addChildElement('Action', soapNS, 'urn').addTextNode('UPDATE');
		if (String.isNotBlank(status)) {
			remedyTag.addChildElement('ExternalStatus', soapNS, 'urn').addTextNode(status);
		}
		remedyTag.addChildElement('RemedyWorkLogNotes', soapNS, 'urn').addTextNode(comment);
		Integer fileCounter = 1;

		if (!fileList.isEmpty()) {
			// list of files passed to the request
			for (uploadInfo file : fileList) {
				// all children are at the same level
				remedyTag.addChildElement('z2AF_Act_Attachment_' + fileCounter + '_attachmentName', soapNS, 'urn').addTextNode(file.fileName);

				// encoding binary data in those files
				Blob contentFile = EncodingUtil.base64Decode(file.fileContent);
				System.debug('contentFile -> ' + contentFile);
				System.debug('file.fileContent -> ' + file.fileContent);
				String csvBody = EncodingUtil.base64Encode(contentFile);
				System.debug('csvBody -> ' + csvBody);
				//remedyTag.addChildElement('z2AF_Act_Attachment_'+fileCounter+'_attachmentData', soapNS, 'urn').addTextNode(csvBody);

				// formatting the filename and adding it to a node
				remedyTag.addChildElement('z2AF_Act_Attachment_' + fileCounter + '_attachmentOrigSize', soapNS, 'urn')
					.addTextNode(String.valueOf(file.fileSize));
				++fileCounter;
			}
		}
		System.debug('doc -> ' + doc.toXmlString());

		// returns the String values of this XML Document
		return doc.toXmlString();
	}
}
