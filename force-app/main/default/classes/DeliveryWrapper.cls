@SuppressWarnings('PMD')
public without sharing class DeliveryWrapper {
	//public static Set<String> deliveryProducts = new Set<String>{'One Net','Internet','One Fixed','IPVPN'};
	public static Set<String> vdslLinkTypes = new Set<String>{ 'VVDSL', 'BVDSL', 'BVVDSL' };

	public String groupId { get; set; }
	public Boolean readyForOrder { get; set; }

	public String externalAccountId;

	//public Boolean hasERS {get;set;}
	public Boolean hasMobileVPN { get; set; }
	public Boolean hasMobile { get; set; }
	public Boolean hasFixedVoiceOnExistingInfra { get; set; }

	public Site__c site { get; set; }
	public List<Competitor_Asset__c> pbxs { get; set; } // this is the main pbx
	public List<Competitor_Asset__c> overflowPbxs { get; set; } // this is the overflow pbx
	public List<Competitor_Asset__c> otherPbxs { get; set; } // used for selecting overflow pbx

	public List<Contracted_Products__c> additionalLocationArticles { get; set; }
	public List<Contracted_Products__c> legacyArticles { get; set; }
	public List<Contracted_Products__c> ersArticles { get; set; }
	public List<Contracted_Products__c> phones { get; set; }

	public Contracted_Products__c priceplan { get; set; }
	List<Contact> locationContacts { get; set; }
	public HBO__c hbo { get; set; } // MP 2019-11-08

	public DeliveryWrapper() {
		this.readyForOrder = false;
		//this.hasERS = false;
		this.hasMobileVPN = false;
		this.hasMobile = false;
		this.hasFixedVoiceOnExistingInfra = false;

		this.pbxs = new List<Competitor_Asset__c>();
		this.overflowPbxs = new List<Competitor_Asset__c>();
		this.otherPbxs = new List<Competitor_Asset__c>();

		this.additionalLocationArticles = new List<Contracted_Products__c>();
		this.legacyArticles = new List<Contracted_Products__c>();
		this.ersArticles = new List<Contracted_Products__c>();
		this.phones = new List<Contracted_Products__c>();
	}

	/**
	 * Update the site in this wrapper, make sure to update the groupId as well
	 */
	public void updateSiteDetails(Site__c site) {
		this.site = site;
		// Update group id as well
		updateGroupId();
	}

	public void updateExternalAccountId(String extAccount) {
		externalAccountId = extAccount;
		updateGroupId();
	}

	private void updateGroupId() {
		groupId =
			externalAccountId +
			'-' +
			getProposition() +
			'-' +
			getSIPCode() +
			'-' +
			site.Site_Postal_Code__c +
			site.Site_House_Number__c +
			'-' +
			getBOPExportOrderId();
	}

	// update methods e.g. called after a 'save' button on the orderform is pressed
	public void updateSite() {
		SharingUtils.updateRecordsWithoutSharing(new List<Site__c>{ this.site });
	}

	public void updatePbxs() {
		SharingUtils.updateRecordsWithoutSharing(this.pbxs);
		SharingUtils.updateRecordsWithoutSharing(this.overflowpbxs);
	}

	public void updateLinks() {
		List<Contracted_Products__c> linksToUpdate = new List<Contracted_Products__c>();
		for (ContractedProductWrapper cpw : allLinks) {
			linksToUpdate.add(cpw.cp);
		}
		SharingUtils.updateRecordsWithoutSharing(linksToUpdate);
	}

	public void updateCpes() {
		List<Contracted_Products__c> cpesToUpdate = new List<Contracted_Products__c>();
		for (ContractedProductWrapper cpw : allCpes) {
			cpesToUpdate.add(cpw.cp);
		}
		SharingUtils.updateRecordsWithoutSharing(cpesToUpdate);
	}

	public void updateAdditionalLocationArticles() {
		SharingUtils.updateRecordsWithoutSharing(this.additionalLocationArticles);
		SharingUtils.updateRecordsWithoutSharing(this.ersArticles);
	}

	public void updatePhones() {
		SharingUtils.updateRecordsWithoutSharing(this.phones);
	}

	public void updateDelivery() {
		try {
			checkDelivery();

			this.updateSite();
			this.updatePbxs();
			this.updateLinks();
			this.updateCpes();
			this.updateAdditionalLocationArticles();
			this.updatePhones();
		} catch (ExMissingDataException ide) {
			Apexpages.addMessages(ide);
		}
	}

	// link fetch methods

	public List<ContractedProductWrapper> allVoiceLinks {
		get {
			if (allVoiceLinks == null)
				allVoiceLinks = new List<ContractedProductWrapper>();
			return allVoiceLinks;
		}
		set;
	}

	public List<ContractedProductWrapper> allInternetLinks {
		get {
			if (allInternetLinks == null)
				allInternetLinks = new List<ContractedProductWrapper>();
			return allInternetLinks;
		}
		set;
	}

	public List<ContractedProductWrapper> allIPVPNLinks {
		get {
			if (allIPVPNLinks == null)
				allIPVPNLinks = new List<ContractedProductWrapper>();
			return allIPVPNLinks;
		}
		set;
	}

	public List<ContractedProductWrapper> allLinks {
		get {
			if (allLinks == null) {
				allLinks = new List<ContractedProductWrapper>();

				// prevent dupes
				Set<Id> linkIds = new Set<Id>();
				List<ContractedProductWrapper> allLinkWrappers = new List<ContractedProductWrapper>();
				allLinkWrappers.addAll(allVoiceLinks);
				allLinkWrappers.addAll(allInternetLinks);
				allLinkWrappers.addAll(allIPVPNLinks);
				for (ContractedProductWrapper cpw : allLinkWrappers) {
					if (!linkIds.contains(cpw.cp.Id)) {
						linkIds.add(cpw.cp.Id);
						allLinks.add(cpw);
					}
				}
			}
			return allLinks;
		}
		set;
	}

	// end of link fetch methods

	// cpe fetch methods
	public List<ContractedProductWrapper> allVoiceCpes {
		get {
			if (allVoiceCpes == null)
				allVoiceCpes = new List<ContractedProductWrapper>();
			return allVoiceCpes;
		}
		set;
	}

	public List<ContractedProductWrapper> allInternetCpes {
		get {
			if (allInternetCpes == null)
				allInternetCpes = new List<ContractedProductWrapper>();
			return allInternetCpes;
		}
		set;
	}

	public List<ContractedProductWrapper> allIPVPNCpes {
		get {
			if (allIPVPNCpes == null)
				allIPVPNCpes = new List<ContractedProductWrapper>();
			return allIPVPNCpes;
		}
		set;
	}

	public List<ContractedProductWrapper> allCpes {
		get {
			if (allCpes == null) {
				allCpes = new List<ContractedProductWrapper>();

				// prevent dupes
				Set<Id> cpeIds = new Set<Id>();
				List<ContractedProductWrapper> allCpeWrappers = new List<ContractedProductWrapper>();
				allCpeWrappers.addAll(allVoiceCpes);
				allCpeWrappers.addAll(allInternetCpes);
				allCpeWrappers.addAll(allIPVPNCpes);
				for (ContractedProductWrapper cpw : allCpeWrappers) {
					if (!cpeIds.contains(cpw.cp.Id)) {
						cpeIds.add(cpw.cp.Id);
						allCpes.add(cpw);
					}
				}
			}
			return allCpes;
		}
		set;
	}

	// end of cpe fetch methods

	public List<ContractedProductWrapper> allCarriers {
		get {
			if (allCarriers == null)
				allCarriers = new List<ContractedProductWrapper>();
			return allCarriers;
		}
		set;
	}

	public void setOverflowPbx() {
		overflowPbxs.clear();
		if (!pbxs.isEmpty()) {
			for (Competitor_Asset__c ca : otherPbxs) {
				if (ca.Id == pbxs[0].Overflow_PBX__c) {
					overflowPbxs.add(ca);
				}
			}
		}
	}

	/*
	 *      Description:    With this method, you can check if the Delivery contains fixed voice
	 */
	public Boolean getHasFixedVoice() {
		if (!allVoiceLinks.isEmpty() || hasFixedVoiceOnExistingInfra || hasOnenet) {
			return true;
		}
		return false;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery is a OneNet delivery (of any type)
	 */
	public Boolean hasOneNet {
		get {
			if (hasOneNet == null) {
				hasOneNet = false;
				system.debug('1');
				if (
					this.hasOneNetClassic ||
					this.hasOneNet12 ||
					this.hasOneNetSub30 ||
					this.hasOneNetExpress
				)
					hasOneNet = true;
				system.debug('1');
			}
			return hasOneNet;
		}
		private set;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery is a (classic) OneNet delivery. This is the only onenet that
	 *                      cannot be identified by the family_condition
	 */
	public Boolean hasOneNetClassic {
		get {
			hasOneNetClassic = false;
			system.debug('1');
			system.debug(this.priceplan);
			system.debug(this.priceplan.Proposition__c);
			system.debug(allLinks);
			system.debug(additionalLocationArticles);

			if (this.priceplan != null && this.priceplan.Proposition__c != null) {
				if (this.priceplan.Proposition__c.contains('One Net')) {
					if (!this.hasOneNet12 && !this.hasOneNetSub30 && !this.hasOneNetExpress) {
						hasOneNetClassic = true;
					}
				}
				system.debug('1');
			} else if (allLinks != null && !allLinks.isEmpty()) {
				for (ContractedProductWrapper cpw : allLinks) {
					if (
						cpw.cp.Proposition__c != null && cpw.cp.Proposition__c.contains('One Net')
					) {
						if (!this.hasOneNet12 && !this.hasOneNetSub30 && !this.hasOneNetExpress) {
							hasOneNetClassic = true;
							continue;
						}
					}
				}
				system.debug('1');
			} else if (!additionalLocationArticles.isEmpty()) {
				for (Contracted_Products__c cp : additionalLocationArticles) {
					if (cp.Proposition__c != null && cp.Proposition__c.contains('One Net')) {
						if (!this.hasOneNet12 && !this.hasOneNetSub30 && !this.hasOneNetExpress) {
							hasOneNetClassic = true;
							continue;
						}
					}
				}
				system.debug('1');
			}

			return hasOneNetClassic;
		}
		private set;
	}

	// utility method to check an order's onenet family
	public Boolean hasOnenetFamily(String familyToCheck) {
		Boolean hasThisOnenetFamily = false;
		if (this.priceplan != null && this.priceplan.Proposition__c != null) {
			if (this.priceplan.Proposition__c.contains('One Net')) {
				if (this.priceplan.Family_Condition__c != null)
					if (this.priceplan.Family_Condition__c == familyToCheck)
						hasThisOnenetFamily = true;
			}
		} else if (!allLinks.isEmpty()) {
			for (ContractedProductWrapper cpw : allLinks) {
				if (cpw.cp.Proposition__c != null && cpw.cp.Proposition__c.contains('One Net')) {
					if (
						cpw.cp.Family_Condition__c != null &&
						cpw.cp.Family_Condition__c == familyToCheck
					) {
						hasThisOnenetFamily = true;
						continue;
					}
				}
			}
		} else if (!additionalLocationArticles.isEmpty()) {
			for (Contracted_Products__c cp : additionalLocationArticles) {
				if (cp.Proposition__c != null && cp.Proposition__c.contains('One Net')) {
					if (cp.Family_Condition__c != null && cp.Family_Condition__c == familyToCheck) {
						hasThisOnenetFamily = true;
						continue;
					}
				}
			}
		} else if (!phones.isEmpty()) {
			for (Contracted_Products__c cp : phones) {
				if (cp.Proposition__c != null && cp.Proposition__c.contains('One Net')) {
					if (cp.Family_Condition__c != null && cp.Family_Condition__c == familyToCheck) {
						hasThisOnenetFamily = true;
						continue;
					}
				}
			}
		}
		return hasThisOnenetFamily;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery is a OneNet 2014 delivery (AKA Onenet 12)
	 */
	public Boolean hasOneNet12 {
		get {
			if (hasOneNet12 == null) {
				hasOneNet12 = hasOnenetFamily('ONENET2014');
			}
			return hasOneNet12;
		}
		private set;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery is a OneNet sub30 delivery
	 */
	public Boolean hasOneNetSub30 {
		get {
			if (hasOneNetSub30 == null) {
				hasOneNetSub30 = hasOnenetFamily('ONENET2014SUB30');
			}
			return hasOneNetSub30;
		}
		private set;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery is a OneNet Express delivery
	 */
	public Boolean hasOneNetExpress {
		get {
			if (hasOneNetExpress == null) {
				hasOneNetExpress = hasOnenetFamily('ONENETX2014');
			}
			return hasOneNetExpress;
		}
		private set;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery is a OneFixed delivery
	 */
	public Boolean getHasOneFixed() {
		if (this.priceplan.Proposition__c != null) {
			if (
				this.priceplan.Proposition__c.contains('One Fixed') //|| this.priceplan.Proposition__c.equals('One Fixed Express'))
			)
				return true;
		} else {
			for (ContractedProductWrapper cpw : allLinks) {
				if (cpw.cp.Proposition__c.contains('One Fixed')) {
					return true;
				}
			}
		}

		return false;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery contains an Internet component
	 */
	public Boolean getHasInternet() {
		if (!this.allInternetLinks.isEmpty()) {
			return true;
		}

		return false;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery is a IPVPN delivery
	 */

	public Boolean getHasIPVPN() {
		if (!allIPVPNLinks.isEmpty()) {
			return true;
		}
		return false;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery is a legacy delivery
	 */
	public Boolean getHasLegacy() {
		if (!this.legacyArticles.isEmpty()) {
			return true;
		}

		return false;
	}

	/*
	 *      Description:    With this method, you can check if the Delivery has ERS
	 */
	public Boolean getHasERS() {
		if (!ersArticles.isEmpty()) {
			return true;
		}

		return false;
	}

	/*
	 *      Description:    With this method, any checks that need to be done before saving can be performed
	 */
	public void checkDelivery() {
		for (ContractedProductWrapper cpe : this.allVoiceCpes) {
			if (cpe.cp.Inbound_Channel_Restriction__c > getMaxSessionsPerCPE()) {
				throw new ExMissingDataException(
					'Inbound Channel Restriction cannot be more than Max Sessions on Location ' +
					site.Name
				);
			}
			if (cpe.cp.Outbound_Channel_Restriction__c > getMaxSessionsPerCPE()) {
				throw new ExMissingDataException(
					'Outbound Channel Restriction cannot be more than Max Sessionson location ' +
					site.Name
				);
			}
		}
		for (Competitor_Asset__c ca : this.pbxs) {
			if (
				ca.PBX_Type__c == GeneralUtils.defaultISDN2PBXType &&
				(this.allVoiceLinks.isEmpty() ||
				this.allVoiceLinks[0].cp.Interface_Type__c != 'ISDN2')
			) {
				throw new ExMissingDataException(
					'ISDN2 PBX does not match configuration on location ' + site.Name
				);
			} else if (
				ca.PBX_Type__c == GeneralUtils.defaultISDN30PBXType &&
				(this.allVoiceLinks.isEmpty() ||
				this.allVoiceLinks[0].cp.Interface_Type__c != 'ISDN30')
			) {
				throw new ExMissingDataException(
					'ISDN30 PBX does not match configuration on location ' + site.Name
				);
			}
		}

		// check the priceplans once more (except for IPVPN and Legacy)
		if (this.priceplan == null && getHasFixedVoice() && !hasOneNet) {
			throw new ExMissingDataException(
				'Missing main priceplan on location ' +
				this.site.Name +
				'. Order cannot be created. Please contact your System Administrator.'
			);
		}

		for (ContractedProductWrapper cpe : this.allCpes) {
			if (cpe.getCodec() == null && (cpe.cp.Proposition_Component__c.contains('Voice'))) {
				throw new ExMissingDataException(
					'Missing Codec on cpe ' +
					cpe.cp.Product__r.Name +
					' on location ' +
					site.Name +
					'. Please log a Case with SF support.'
				);
			}
		}
		for (ContractedProductWrapper cpe : this.allVoiceCpes) {
			system.debug(cpe.getInterfaceType());
			if (cpe.getInterfaceType() == null) {
				throw new ExMissingDataException(
					'Missing Interface Type on cpe ' +
					cpe.cp.Product__r.Name +
					' on location ' +
					site.Name +
					'. Please log a Case with SF support.'
				);
			}
		}
		for (ContractedProductWrapper link : this.allLinks) {
			if (link.getCodec() == null && (link.cp.Proposition_Component__c.contains('Voice'))) {
				throw new ExMissingDataException(
					'Missing Codec on link ' +
					link.cp.Product__r.Name +
					' on location ' +
					site.Name +
					'. Please log a Case with SF support.'
				);
			}
			if (link.getLinkType() == null) {
				throw new ExMissingDataException(
					'Missing Link Type on link ' +
					link.cp.Product__r.Name +
					' on location ' +
					site.Name +
					'. Please log a Case with SF support.'
				);
			}
		}
		for (ContractedProductWrapper link : this.allVoiceLinks) {
			if (link.getNumberOfSessions() == null) {
				throw new ExMissingDataException(
					'Missing Number of Sessions on link ' +
					link.cp.Product__r.Name +
					' on location ' +
					site.Name +
					' on location ' +
					site.Name +
					'. Please log a Case with SF support.'
				);
			}
		}
		for (ContractedProductWrapper cpe : this.allVoiceCpes) {
			if (cpe.getInterfaceType() != 'IP' && cpe.cp.Product__r.Role__c == 'SBC') {
				throw new ExMissingDataException(
					'Invalid configuration: interface type ' +
					cpe.getInterfaceType() +
					' with cpe role ' +
					cpe.cp.Product__r.Role__c +
					' on location ' +
					site.Name +
					'. Please log a Case with SF support.'
				);
			}
		}

		for (ContractedProductWrapper link : this.allLinks) {
			if (link.cp.Proposition_Component__c != null) {
				Integer propCompCounter = 0;
				Set<String> allowMultiple = new Set<String>{ 'QoS', 'IPVPdata', 'IPVPNvoip' };
				for (String propComp : link.cp.Proposition_Component__c.split(';')) {
					if (!allowMultiple.contains(propComp)) {
						propCompCounter++;
					}
				}
				if (propCompCounter > 1) {
					throw new ExMissingDataException(
						'Multiple proposition components found on link ' +
						link.cp.Product__r.Name +
						' on location ' +
						site.Name +
						'. Please log a Case with SF support.'
					);
				}
			}
		}

		// check if all carriers are assigned to a link, group by model number, and check only within the model number
		// go through all carriers, assign a link to each carrier, once a link is assigned, then it cannot be assigned to another carrier
		Map<Id, Id> linkCarriers = new Map<Id, Id>();
		Set<String> carriersWoLink = new Set<String>();
		for (ContractedProductWrapper carrier : this.allCarriers) {
			for (ContractedProductWrapper l : this.allLinks) {
				if (
					(l.cp.Model_Number__c == null ||
					carrier.cp.Model_Number__c == l.cp.Model_Number__c) &&
					carrier.cp.Proposition_Component__c.contains(l.cp.Proposition_Component__c)
				) {
					if (!linkCarriers.containsKey(l.cp.Id)) {
						linkCarriers.put(l.cp.Id, carrier.cp.Id);
					} else {
						carriersWoLink.add(carrier.cp.Name);
					}
				}
			}
		}
		if (!carriersWoLink.isEmpty()) {
			throw new ExMissingDataException(
				'Link is missing for carrier(s): ' +
				carriersWoLink.toString() +
				'. Please log a Case with SF support.'
			);
		}
	}

	/*
	 *      Description:    Returns a selectlist of any other pbxs on the site
	 */
	public List<SelectOption> getOtherPbxsSelectOptions() {
		List<SelectOption> options = new List<SelectOption>();

		options.add(new SelectOption('', '--None--'));

		// retrieve all other pbxs
		for (Competitor_Asset__c ca : this.otherPbxs) {
			SelectOption so = new SelectOption(ca.Id, ca.Name + '_' + ca.PBX_Type__r.Name);
			options.add(so);
		}
		return options;
	}

	/*
	 *      Description:    In case of clock source 'other', the clock source needs to be specified
	 */
	public String clockSourceOther {
		get {
			if (this.pbxs[0].Clock_Source__c == 'OTHER') {
				return this.pbxs[0].Clock_Source_Device__c;
			} else {
				return '';
			}
		}
		set {
			clockSourceOther = value;
			this.pbxs[0].Clock_Source_Device__c = clockSourceOther;
		}
	}

	public String clockSourceSelected {
		get {
			if (!this.pbxs.isEmpty()) {
				if (
					this.pbxs[0].Clock_Source__c == 'TBD' ||
					this.pbxs[0].Clock_Source__c == 'OTHER'
				) {
					return this.pbxs[0].Clock_Source__c;
				} else {
					return this.pbxs[0].Clock_Source__c + '_' + this.pbxs[0].Clock_Source_Device__c;
				}
			} else {
				return null;
			}
		}
		set {
			clockSourceSelected = value;
			if (clockSourceSelected != null) {
				// set the clock source type and the device reference
				String[] parts = clockSourceSelected.split('_');

				if (!parts.isEmpty()) {
					String cs = parts[0];
					this.pbxs[0].Clock_Source__c = cs;

					if (cs == 'IAD' || cs == 'PBX') {
						this.pbxs[0].Clock_Source_Device__c = parts[1];
					} else if (cs == 'OTHER') {
						this.pbxs[0].Clock_Source_Device__c = clockSourceOther;
					} else {
						this.pbxs[0].Clock_Source_Device__c = '';
					}
				}
			}
		}
	}

	public List<SelectOption> getClockSourceSelectOptions() {
		List<SelectOption> options = new List<SelectOption>();
		if (!this.allVoiceCpes.isEmpty() && this.allVoiceCpes[0].getInterfaceType() != 'IP') {
			// enter the default values
			options.add(new SelectOption('TBD', 'TBD'));
			options.add(new SelectOption('OTHER', 'OTHER'));

			// retrieve all IADs
			for (ContractedProductWrapper cpw : this.allVoiceCpes) {
				if (cpw.cp.Product__r.Role__c == 'IAD') {
					SelectOption so = new SelectOption(
						'IAD_' + cpw.cp.Id,
						'IAD' +
						'_' +
						cpw.cp.Product__r.Model__c
					);
					options.add(so);
				}
			}
		}

		// retrieve all pbxs
		for (Competitor_Asset__c ca : this.pbxs) {
			SelectOption so = new SelectOption('PBX_' + ca.Id, 'PBX' + '_' + ca.PBX_Type__r.Name);
			options.add(so);
		}

		return options;
	}

	/*
	 *      Description:    Methods for defining the amount of CPEs and the number of sessions per CPE
	 */
	public Boolean getHasMultipleCPEs() {
		if (allCpes.size() > 1) {
			return true;
		} else {
			return false;
		}
	}

	public Integer getMaxSessionsPerCPE() {
		Integer size = 0;
		for (ContractedProductWrapper cpw : this.allVoiceCpes) {
			// only count iads and routers
			if (OrderUtils.cpeArticleRoles.contains(cpw.cp.Product__r.Role__c)) {
				size++;
			}
		}

		if (
			size < 1 ||
			this.allVoiceLinks.isEmpty() ||
			this.allVoiceLinks[0].cp.Number_of_sessions__c == null
		) {
			return null;
		} else if (size == 1) {
			return Integer.valueOf(this.allVoiceLinks[0].cp.Number_of_sessions__c);
		} else {
			return Integer.valueOf(this.allVoiceLinks[0].cp.Number_of_sessions__c / (size - 1));
		}
	}

	public String getSipCode() {
		String sipCode;

		if (allVoiceLinks.isEmpty()) {
			sipCode = 'novoice';
		} else {
			sipCode = 'SIP-';
			// nr of sessions
			sipCode += String.valueOf(this.allVoiceLinks[0].getNumberOfSessions()) + '-';
			// codec
			sipCode += this.allVoiceLinks[0].getCodec() + '-';
			// interface type
			if (!this.allVoiceCpes.isEmpty())
				sipCode += this.allVoiceCpes[0].getInterfaceType();
			else
				sipCode += 'nocpe';
		}

		return sipCode;
	}

	public String getProposition() {
		if (getHasLegacy())
			return 'Legacy';
		else if (!this.allLinks.isEmpty()) {
			return this.allLinks[0].cp.Proposition__c;
		} else
			return '';
	}

	private String getBOPExportOrderId() {
		//return priceplan.Order__r.BOP_Export_Order_Id__c;
		if (!this.allLinks.isEmpty()) {
			return this.allLinks[0].cp.Order__r.BOP_Export_Order_Id__c;
		} else if (!this.legacyArticles.isEmpty()) {
			return this.legacyArticles[0].Order__r.BOP_Export_Order_Id__c;
		} else if (!this.additionalLocationArticles.isEmpty()) {
			return this.additionalLocationArticles[0].Order__r.BOP_Export_Order_Id__c;
		} else {
			return '';
		}
	}

	public List<ContractedProductWrapper> getLinksWithoutCarrier() {
		List<ContractedProductWrapper> linksWithoutCarrier = new List<ContractedProductWrapper>();
		// get all carriers
		Set<String> carriers = new Set<String>();
		for (ContractedProductWrapper carrier : allCarriers) {
			for (String s : carrier.cp.Proposition_Component__c.split(';')) {
				carriers.add(s);
			}
		}

		for (ContractedProductWrapper link : allIPVPNLinks) {
			// TODO: should be replaced by all links once carrier is provided in all propositions
			system.debug(carriers);
			system.debug(link.cp.Proposition_Component__c);
			system.debug(link.cp.Proposition_Component__c.split(';')[0]);
			if (!carriers.contains(link.cp.Proposition_Component__c.split(';')[0])) {
				linksWithoutCarrier.add(link);
			}
		}
		return linksWithoutCarrier;
	}

	/*
	 *      Description:    A wrapper for ContractedProduct, so that fields can be presented as editable to the user while they're not editable on the object
	 */
	public class ContractedProductWrapper {
		public Contracted_Products__c cp { get; set; }
		public String israReference {
			get;
			set {
				this.israReference = StringUtils.cleanNLPhonenumber(value);
				if (this.cp != null)
					this.cp.IS_RA_reference_phone_number__c = this.israReference;
			}
		}
		public String israSpecs {
			get;
			set {
				this.israSpecs = value;
				if (this.cp != null)
					this.cp.IS_RA_specs__c = this.israSpecs;
			}
		}
		public Decimal inboundChannelRestriction {
			get;
			set {
				this.inboundChannelRestriction = value;
				if (this.cp != null)
					this.cp.Inbound_Channel_Restriction__c = this.inboundChannelRestriction;
			}
		}
		public Decimal outboundChannelRestriction {
			get;
			set {
				this.outboundChannelRestriction = value;
				if (this.cp != null)
					this.cp.Outbound_Channel_Restriction__c = this.outboundChannelRestriction;
			}
		}
		public String lineIdentifier {
			get;
			set {
				this.lineIdentifier = value;
				if (this.cp != null)
					this.cp.Line_Identifier__c = this.lineIdentifier;
			}
		}

		// some mappings methods to map between values in SF (coming from BM) and the output required for Orderform and BOP
		public String getCodec() {
			if (this.cp.Codec__c == 'G711')
				return 'G.711';
			else if (this.cp.Codec__c == 'G729')
				return 'G.729';
			else
				return this.cp.Codec__c;
		}
		public String getInterfaceType() {
			return this.cp.Interface_Type__c;
		}
		public String getLinkType() {
			if (this.cp.Link_Type__c == 'EthernetOverFiber')
				return 'FIBER';
			else if (this.cp.Link_Type__c == 'FTTH')
				return 'FIBER';
			else if (this.cp.Link_Type__c == 'EthernetOverCopper')
				return 'EOC';
			else if (vdslLinkTypes.contains(this.cp.Link_Type__c))
				return 'VDSL';
			else
				return this.cp.Link_Type__c;
		}
		public String getNumberOfSessions() {
			return String.valueOf(this.cp.Number_of_sessions__c);
		}
		public List<String> getServices() {
			List<String> services = new List<String>();
			// DATA / VOIP
			if (this.cp.Proposition_Component__c.contains('IPVPNdata'))
				services.add('DATA');
			if (this.cp.Proposition_Component__c.contains('IPVPNvoice'))
				services.add('VOIP');
			if (this.cp.Proposition_Component__c.contains('IPVPNvoip'))
				services.add('VOIP');

			return services;
		}
		public Boolean getQos() {
			if (this.cp.Proposition_Component__c.contains('QoS'))
				return true;
			else
				return false;
		}

		// the create method
		public ContractedProductWrapper(Contracted_Products__c cp) {
			this.israReference = cp.IS_RA_reference_phone_number__c;
			this.israSpecs = cp.IS_RA_specs__c;
			this.inboundChannelRestriction = cp.Inbound_Channel_Restriction__c;
			this.outboundChannelRestriction = cp.Outbound_Channel_Restriction__c;
			this.lineIdentifier = cp.Line_Identifier__c;
			this.cp = cp;
		}
	}
}