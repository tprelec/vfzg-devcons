@isTest
private class TestECSUserService {
	@TestSetup
	static void makeData() {
		TestUtils.autoCommit = true;
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'AFD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		Ban__c ban = TestUtils.createBan(acct);
		ban.BOP_export_datetime__c = System.now();
		update ban;
		acct.BAN_Number__c = '346474';
		update acct;
		Contact cont = TestUtils.createContact(acct);
		cont.Email = 'test@gmail.com';
		update cont;
	}

	@isTest
	static void testMakeCreateThenUpdateRequest() {
		String operation = 'create';
		Contact theContact = [
			SELECT
				Id,
				Account.BAN_Number__c,
				Account.BOPCode__c,
				BOP_reference__c,
				Email,
				Firstname,
				Lastname,
				Phone,
				MobilePhone,
				Fax,
				Title,
				BOP_export_Errormessage__c,
				BOP_export_datetime__c,
				userid__c
			FROM Contact
			WHERE Email = 'test@gmail.com'
			LIMIT 1
		];
		ECSUserService userService = new ECSUserService();
		List<ECSUserService.request> requests = new List<ECSUserService.request>();
		ECSUserService.request req = new ECSUserService.request();

		req.companyBanNumber = theContact.Account.BAN_Number__c;
		req.companyCorporateId = 'not Used'; //not used in ContactExport when Using ECSUserService
		req.companyBopCode = theContact.Account.BOPCode__c;
		req.BOPemail = theContact.Email;
		req.email = theContact.Email;

		req.prefix = 'not used'; //not used in ContactExport when Using ECSUserService
		req.firstname = theContact.firstname;
		req.infix = 'not used'; //not used in ContactExport when Using ECSUserService
		req.lastname = theContact.LastName;
		req.language = 'not used'; //not used in ContactExport when Using ECSUserService
		req.groupEmail = 'not used'; //not used in ContactExport when Using ECSUserService
		req.jobTitle = theContact.Title;
		req.address = 'not used'; //not used in ContactExport when Using ECSUserService
		req.zipcode = 'not used'; //not used in ContactExport when Using ECSUserService
		req.city = 'not used'; //not used in ContactExport when Using ECSUserService
		req.countryId = 'not used'; //not used in ContactExport when Using ECSUserService
		req.phone = theContact.Phone;
		req.mobile = theContact.MobilePhone;
		req.fax = theContact.Fax;
		req.notes = 'not used'; //not used in ContactExport when Using ECSUserService
		requests.add(req);
		Test.setMock(WebServiceMock.class, new ECSSOAPUserMocks.CreateUpdateUserResponse());
		Test.startTest();
		userService.setRequest(requests);
		userService.makeRequest(operation);
		List<ECSUserService.response> responseList = userService.getResponse();
		System.assertEquals('ABC', responseList[0].bopCode, 'Bop Code returned: ' + responseList[0].bopCode);
		userService.makeRequest('update');
		List<ECSUserService.response> responseListUpdate = userService.getResponse();
		System.assertEquals('ABC', responseListUpdate[0].bopCode, 'Bop Code returned: ' + responseListUpdate[0].bopCode);
		Test.stopTest();
	}

	@isTest
	public static void testExceptions() {
		Test.startTest();
		ECSUserService userService = new ECSUserService();
		List<ECSUserService.request> requests = new List<ECSUserService.request>();
		ECSUserService.request req = new ECSUserService.request();
		requests.add(req);
		userService.setRequest(requests);
		try {
			//remove items from list
			requests.clear();
			userService.setRequest(requests);
			userService.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		try {
			//remove items from list
			requests = null;
			userService.setRequest(requests);
			userService.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		Test.stopTest();
	}

	@isTest
	public static void testAdditionalErrors() {
		Contact theContact = [
			SELECT
				Id,
				Account.BAN_Number__c,
				Account.BOPCode__c,
				BOP_reference__c,
				Email,
				Firstname,
				Lastname,
				Phone,
				MobilePhone,
				Fax,
				Title,
				BOP_export_Errormessage__c,
				BOP_export_datetime__c,
				userid__c
			FROM Contact
			WHERE Email = 'test@gmail.com'
			LIMIT 1
		];
		ECSUserService userService = new ECSUserService();
		List<ECSUserService.request> requests = new List<ECSUserService.request>();
		ECSUserService.request req = new ECSUserService.request();

		req.companyBanNumber = theContact.Account.BAN_Number__c;
		req.companyCorporateId = 'not Used'; //not used in ContactExport when Using ECSUserService
		req.companyBopCode = theContact.Account.BOPCode__c;
		req.BOPemail = theContact.Email;
		req.email = theContact.Email;

		req.prefix = 'not used'; //not used in ContactExport when Using ECSUserService
		req.firstname = theContact.firstname;
		req.infix = 'not used'; //not used in ContactExport when Using ECSUserService
		req.lastname = theContact.LastName;
		req.language = 'not used'; //not used in ContactExport when Using ECSUserService
		req.groupEmail = 'not used'; //not used in ContactExport when Using ECSUserService
		req.jobTitle = theContact.Title;
		req.address = 'not used'; //not used in ContactExport when Using ECSUserService
		req.zipcode = 'not used'; //not used in ContactExport when Using ECSUserService
		req.city = 'not used'; //not used in ContactExport when Using ECSUserService
		req.countryId = 'not used'; //not used in ContactExport when Using ECSUserService
		req.phone = theContact.Phone;
		req.mobile = theContact.MobilePhone;
		req.fax = theContact.Fax;
		req.notes = 'not used'; //not used in ContactExport when Using ECSUserService
		requests.add(req);
		userService.setRequest(requests);
		Test.startTest();
		try {
			userService.makeRequest('create');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to create the user(s): '),
				'when there is no mock Create'
			);
		}
		try {
			userService.makeRequest('update');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to update the user(s): '),
				'when there is no mock Update'
			);
		}
		Test.stopTest();
	}
}
