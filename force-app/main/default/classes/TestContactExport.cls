@isTest
public class TestContactExport {
    @isTest
    static void contactExportMethod() {
        Set<Id> banIds = new Set<Id>();
        Set<Id> conIds = new Set<Id>();
        Set<Id> contIds = new Set<Id>();
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
         External_Account__c externalAcc = new External_Account__c();
        externalAcc.Account__c = acct.Id;
        externalAcc.External_Account_Id__c = 'AFD';
        externalAcc.External_Source__c = 'BOP';
        insert externalAcc;
        Contact cont = TestUtils.createContact(acct);
        cont.Email = 'test@gmail.com';
        cont.userid__c = owner.Id;
        update cont;

        Site__c site = TestUtils.createSite(acct);
        Ban__c ban = TestUtils.createBan(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        //VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
        opp.Ban__c = ban.Id;
        update opp;
        //Order__c ord = TestUtils.createOrder(contr);
        banIds.add(ban.Id);
        conIds.add(cont.Id);
        Test.startTest();
       
        Contact_Role__c contRole = new Contact_Role__c();
        contRole.Account__c = acct.Id;
        contRole.Contact__c = cont.Id;
        contRole.Site__c = site.Id;
        contRole.Active__c = true;
        insert contRole;
        contIds.add(contRole.Id);
        Set<Id> conSet = ContactExport.contactRolesInExport;
        Set<Id> conSet1 = ContactExport.contactsInExport;
        ContactExport.scheduleContactExportFromBanIds(banIds);
        ContactExport.scheduleContactRoleDeleteFromContactIds(contIds);
        ContactExport.scheduleContactRoleExportFromContactIds(contIds);
        ContactExport.scheduleContactExportFromContactIds(conIds);
        ContactExport.exportDeletedContactRolesOffline(contIds);
        system.assert(!banIds.isEmpty(), 'Test assert');
        ContactExport.exportNewContactRolesOffline(contIds);
        ContactExport.exportUpdatedContactsOffline(conIds);
        ContactExport.exportNewContactsOffline(conIds);
        List<Contact> conlist =  ContactExport.exportContacts(contIds,'create');

        Test.stopTest();
    }
    @isTest
    static void contactExportMethodOne() {
        Set<Id> banIds = new Set<Id>();
        Set<Id> conIds = new Set<Id>();
        Set<Id> contIds = new Set<Id>();
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.BAN_Number__c = '346474';
        update acct;
        Contact cont = TestUtils.createContact(acct);
        cont.Email = 'test@gmail.com';
        //cont.userid__c = owner.Id;
        update cont;
        Site__c site = TestUtils.createSite(acct);
        Ban__c ban = TestUtils.createBan(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        //VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
        opp.Ban__c = ban.Id;
        update opp;
        //Order__c ord = TestUtils.createOrder(contr);
        banIds.add(ban.Id);
        conIds.add(cont.Id);
        Test.startTest();
        External_Account__c externalAcc = new External_Account__c();
        externalAcc.Account__c = acct.Id;
        externalAcc.External_Account_Id__c = 'AFD';
        externalAcc.External_Source__c = 'BOP';
        insert externalAcc;
        Contact_Role__c contRole = new Contact_Role__c();
        contRole.Account__c = acct.Id;
        contRole.Contact__c = cont.Id;
        contRole.Site__c = site.Id;
        contRole.Active__c = true;
        insert contRole;
        contIds.add(contRole.Id);
        Set<Id> conSet = ContactExport.contactRolesInExport;
        Set<Id> conSet1 = ContactExport.contactsInExport;
        ContactExport.scheduleContactExportFromBanIds(banIds);
        ContactExport.scheduleContactRoleDeleteFromContactIds(contIds);
        ContactExport.scheduleContactRoleExportFromContactIds(contIds);
        ContactExport.scheduleContactExportFromContactIds(conIds);
        ContactExport.exportDeletedContactRolesOffline(contIds);
        system.assert(!banIds.isEmpty(), 'Test assert');
        ContactExport.exportNewContactRolesOffline(contIds);
        ContactExport.exportUpdatedContactsOffline(conIds);
        ContactExport.exportNewContactsOffline(conIds);

        Test.stopTest();
    }
    @isTest
    static void contactExportMethodTwo() {
        Set<Id> banIds = new Set<Id>();
        Set<Id> conIds = new Set<Id>();
        Set<Id> contIds = new Set<Id>();
        User owner = TestUtils.createAdministrator();
        // Account acct = TestUtils.createAccount(owner);
        Account acct = TestUtils.createPartnerAccount();
        Contact cont = TestUtils.createContact(acct);
        cont.Email = 'test@gmail.com';
        cont.userid__c = owner.Id;
        update cont;
        Account partnerAcct = TestUtils.createPartnerAccount('135');
        partnerAcct.IsPartner = true;
        update partnerAcct;
        Site__c site = TestUtils.createSite(acct);
        //Ban__c ban = TestUtils.createBan(acct);
        Ban__c ban = new Ban__c(
            Account__c = acct.Id,
            Unify_Customer_Type__c = 'C',
            Unify_Customer_SubType__c = 'A',
            Name = '388888888',
            Dealer_code__c = '999998'
        );
        insert ban;
        
        banIds.add(ban.Id);
        conIds.add(cont.Id);
        Test.startTest();
        External_Account__c externalAcc = new External_Account__c();
        externalAcc.Account__c = acct.Id;
        externalAcc.External_Account_Id__c = 'AFD';
        externalAcc.External_Source__c = 'BOP';
        insert externalAcc;
        Contact_Role__c contRole = new Contact_Role__c();
        contRole.Account__c = acct.Id;
        contRole.Contact__c = cont.Id;
        contRole.Site__c = site.Id;
        contRole.Active__c = true;
        insert contRole;
        contIds.add(contRole.Id);
        Set<Id> conSet = ContactExport.contactRolesInExport;
        Set<Id> conSet1 = ContactExport.contactsInExport;
        ContactExport.scheduleContactExportFromBanIds(banIds);
        ContactExport.scheduleContactRoleDeleteFromContactIds(contIds);
        ContactExport.scheduleContactRoleExportFromContactIds(contIds);
        ContactExport.scheduleContactExportFromContactIds(conIds);
        ContactExport.exportDeletedContactRolesOffline(contIds);
        system.assert(!banIds.isEmpty(), 'Test assert');
        ContactExport.exportNewContactRolesOffline(contIds);
        ContactExport.exportUpdatedContactsOffline(conIds);
        ContactExport.exportNewContactsOffline(conIds);
        
        Test.stopTest();
    }
}