public with sharing class COM_DXL_Helper {
	public static void createRequestAttachment(Id parentId, String content, String name) {
		Attachment attachment = new Attachment();
		attachment.Body = Blob.valueOf(content);
		attachment.Name = name;
		attachment.ParentId = parentId;
		insert attachment;
	}

	public static COM_DXL_Integration_Log__c createBillingTransactionLog(BillingData billData, String errorMessage) {
		Set<Id> serviceLineItemIds = retrieveSLIIds(billData);

		Id opportunityId = retrieveOpportunityId(billData);

		List<COM_DxlAffectedObjects> affectedObjects = getAffectedObjects(serviceLineItemIds, 'csord__Service_Line_Item__c');

		COM_DXL_Integration_Log__c dxlLog = new COM_DXL_Integration_Log__c(
			Type__c = 'createBSSOrder',
			Affected_Objects__c = JSON.serialize(affectedObjects),
			Error__c = errorMessage,
			Transaction_Id__c = billData.transactionId,
			Opportunity__c = String.valueOf(opportunityId)
		);
		return dxlLog;
	}

	@TestVisible
	private static List<COM_DxlAffectedObjects> getAffectedObjects(Set<Id> itemIds, String itemType) {
		List<COM_DxlAffectedObjects> returnValue = new List<COM_DxlAffectedObjects>();

		for (Id itemId : itemIds) {
			COM_DxlAffectedObjects affectedItem = new COM_DxlAffectedObjects();
			affectedItem.type = itemType;
			affectedItem.sfdcId = String.valueOf(itemId);
			returnValue.add(affectedItem);
		}

		return returnValue;
	}

	private static Id retrieveOpportunityId(BillingData billData) {
		Id opportunityId;
		for (Id key : billData.mapServices.keySet()) {
			if (!billData.replacedServices.contains(key)) {
				opportunityId = billData.mapServices.get(key).COM_Delivery_Order__r.Order__r.csordtelcoa__Opportunity__c;
				break;
			}
		}

		return opportunityId;
	}

	private static Set<Id> retrieveSLIIds(BillingData billData) {
		Set<Id> result = new Set<Id>();
		for (Id key : billData.mapServiceServiceLineItem.keySet()) {
			for (csord__Service_Line_Item__c sli : billData.mapServiceServiceLineItem.get(key)) {
				if (
					COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE.contains(sli.csord__line_item_type__c) &&
					!billData.replacedServices.contains(sli.csord__Service__c)
				) {
					result.add(sli.Id);
				}
			}
		}
		return result;
	}

	public static List<csord__Service_Line_Item__c> retrieveSliList(
		Map<Id, List<csord__Service_Line_Item__c>> mapServiceServiceLineItem,
		Set<Id> remove
	) {
		List<csord__Service_Line_Item__c> result = new List<csord__Service_Line_Item__c>();
		for (Id key : mapServiceServiceLineItem.keySet()) {
			if (remove != null && !remove.contains(key)) {
				result.addAll(mapServiceServiceLineItem.get(key));
			}
		}
		return result;
	}

	private static String specialCharactersReplacement(String input) {
		String result = input;
		result = input.replaceAll('\\+', 'plus').replaceAll(';', ',');
		return result;
	}

	private static Map<Id, SpecificationItem> getSpecificationItems(Set<Id> serviceIds, Map<Id, Attachment> mapServiceAttachment) {
		Map<Id, SpecificationItem> result = new Map<Id, SpecificationItem>();
		for (Id serviceId : serviceIds) {
			if (mapServiceAttachment.containsKey(serviceId)) {
				String attachmentBody = mapServiceAttachment.get(serviceId).Body.toString();
				attachmentBody = attachmentBody.replace('"baseOTC" : ""', '"baseOTC" : 0.00');
				attachmentBody = attachmentBody.replace('"baseMRC" : ""', '"baseMRC" : 0.00');
				attachmentBody = attachmentBody.replace('"quantity" : ""', '"quantity" : 1.00');
				Map<String, Object> bodyMap = (Map<String, Object>) JSON.deserializeUntyped(attachmentBody);
				List<Map<String, Object>> data = new List<Map<String, Object>>();
				for (Object instance : (List<Object>) bodyMap.get(COM_DXL_Constants.COM_DXL_BSS_SPECIFICATION_KEY)) {
					data.add((Map<String, Object>) instance);
				}
				for (Map<String, Object> instance : data) {
					for (String key : instance.keySet()) {
						if (key == COM_DXL_Constants.COM_DXL_BSS_ATTRIBUTES_KEY) {
							SpecificationItem item = (SpecificationItem) JSON.deserialize(JSON.serialize(instance.get(key)), SpecificationItem.class);
							result.put(serviceId, item);
						}
					}
				}
			}
		}
		return result;
	}

	private static OrderLineItem getOrderLineItemValuesPerSLIType(
		OrderLineItem orderLineItem,
		SpecificationItem specItem,
		csord__Service_Line_Item__c sli
	) {
		orderLineItem.typeOfCharge = sli.csord__Is_Recurring__c
			? COM_DXL_Constants.COM_DXL_BSS_CHARGE_TYPE_RECURRING
			: COM_DXL_Constants.COM_DXL_BSS_CHARGE_TYPE_ONEOFF;
		orderLineItem.quantity = specItem.quantity != null ? Integer.valueOf(specItem.quantity) : 1;
		orderLineItem.externalReferenceID = sli.Id;

		if (sli.csord__Is_Recurring__c) {
			orderLineItem = getSpecificationItemDataForRecurring(orderLineItem, specItem);
		} else {
			orderLineItem = getSpecificationItemDataForOneOff(orderLineItem, specItem);
		}

		return orderLineItem;
	}

	private static OrderLineItem getSpecificationItemDataForOneOff(OrderLineItem orderLineItem, SpecificationItem specItem) {
		orderLineItem.chargeCode = specItem.oneOffChargeCode != null ? specItem.oneOffChargeCode : '';
		orderLineItem.chargeDescription = specItem.oneOffChargeDescription != null
			? specialCharactersReplacement(specItem.oneOffChargeDescription)
			: '';
		orderLineItem.chargeAmount = specItem.baseOTC != null ? specItem.baseOTC : 0.00;
		orderLineItem.bItag1 = specItem.oneOffBITag1 != null ? specItem.oneOffBITag1 : '';
		orderLineItem.bItag2 = specItem.oneOffBITag2 != null ? specItem.oneOffBITag2 : '';
		orderLineItem.bItag3 = specItem.oneOffBITag3 != null ? specItem.oneOffBITag3 : '';
		orderLineItem.bItag4 = specItem.oneOffBITag4 != null ? specItem.oneOffBITag4 : '';

		return orderLineItem;
	}

	private static OrderLineItem getSpecificationItemDataForRecurring(OrderLineItem orderLineItem, SpecificationItem specItem) {
		orderLineItem.chargeCode = specItem.recurringChargeCode != null ? specItem.recurringChargeCode : '';
		orderLineItem.chargeDescription = specItem.recurringChargeDescription != null
			? specialCharactersReplacement(specItem.recurringChargeDescription)
			: '';
		orderLineItem.chargeAmount = specItem.baseMRC != null ? specItem.baseMRC : 0.00;
		orderLineItem.bItag1 = specItem.recurringBITag1 != null ? specItem.recurringBITag1 : '';
		orderLineItem.bItag2 = specItem.recurringBITag2 != null ? specItem.recurringBITag2 : '';
		orderLineItem.bItag3 = specItem.recurringBITag3 != null ? specItem.recurringBITag3 : '';
		orderLineItem.bItag4 = specItem.recurringBITag4 != null ? specItem.recurringBITag4 : '';

		return orderLineItem;
	}

	public static BillingData retrieveBillingData(Id orderId) {
		BillingData result = new BillingData();
		result.orderId = orderId;

		COM_DXL_settings__mdt bITAG5 = COM_DXL_settings__mdt.getInstance(COM_DXL_Constants.COM_DXL_BSS_SETTING_BITAG5);

		COM_DXL_settings__mdt b2bGenericProductCatalogId = COM_DXL_settings__mdt.getInstance(
			COM_DXL_Constants.COM_DXL_BSS_SETTING_B2B_GENERIC_PRODUCT_ID
		);

		result.bITAG5 = bITAG5 != null ? bITAG5.Value__c : COM_DXL_Constants.COM_DXL_BSS_SETTING_BITAG5_MOCK;
		result.b2bGPCI = b2bGenericProductCatalogId != null ? b2bGenericProductCatalogId.Value__c : COM_DXL_Constants.COM_DXL_BSS_SETTING_GP_CI_MOCK;

		result.mapServices = retrieveServiceData(orderId);
		result.replacedServices = retrieveReplacedServices(result.mapServices);
		Id firstEl = new List<Id>(result.mapServices.keySet())[0];
		result.accountId = result.mapServices.get(firstEl).csord__Order__r.csord__Account__c;
		result.osOrderId = result.mapServices.get(firstEl).csord__Order__c;

		CustomerInformation customerInformation = new CustomerInformation();
		customerInformation.baid = result.mapServices.get(firstEl).csord__Order__r.Billing_Arrangement__r.Unify_Ref_Id__c != null
			? result.mapServices.get(firstEl).csord__Order__r.Billing_Arrangement__r.Unify_Ref_Id__c
			: '';

		customerInformation.bcid = result.mapServices.get(firstEl).csord__Order__r.Ban_Information__r.Unify_Ref_Id__c != null
			? result.mapServices.get(firstEl).csord__Order__r.Ban_Information__r.Unify_Ref_Id__c
			: '';

		result.customerInformation = customerInformation;

		Set<Id> serviceIds = new Set<Id>();
		serviceIds.addAll(result.mapServices.keySet());
		serviceIds.addAll(result.replacedServices);

		result.mapServiceServiceLineItem = retrieveServiceLineItemData(serviceIds);
		result.mapServiceSpecification = retrieveServiceSpecifications(serviceIds);
		return result;
	}

	private static Set<Id> retrieveReplacedServices(Map<Id, csord__Service__c> mapServices) {
		Set<Id> result = new Set<Id>();

		for (Id key : mapServices.keySet()) {
			if (mapServices.get(key).csordtelcoa__Replaced_Service__c != null) {
				result.add(mapServices.get(key).csordtelcoa__Replaced_Service__c);
			}
		}

		return result;
	}

	public static String mapDiscountMethodValue(Object input) {
		String result = '';
		if (input != null && input != '') {
			result = String.valueOf(input);
			result = (result == COM_DXL_Constants.COM_DXL_BSS_DISCOUNT_PERCENTAGE)
				? COM_DXL_Constants.COM_DXL_BSS_DISCOUNT_PERCENTAGE
				: COM_DXL_Constants.COM_DXL_BSS_DISCOUNT_AMOUNT;
		}
		return result;
	}

	public static String emptyStringInsteadOfNull(Object input) {
		String result = '';
		if (input != null) {
			result = String.valueOf(input);
		}
		return result;
	}

	private static Map<Id, csord__Service__c> retrieveServiceData(Id orderId) {
		List<csord__Service__c> services = [
			SELECT
				Id,
				csord__Status__c,
				Implemented_Date__c,
				Site__r.Assigned_Product_Id__c,
				csord__Order__r.csord__Account__c,
				csord__Order__r.Ban_Information__r.Unify_Ref_Id__c,
				csord__Order__r.Billing_Arrangement__r.Unify_Ref_Id__c,
				csord__Subscription__c,
				csordtelcoa__Replaced_Service__c,
				csord__Deactivation_Date__c,
				csord__Activation_Date__c,
				csordtelcoa__Delta_Status__c,
				Name,
				COM_Delivery_Order__r.Order__r.csordtelcoa__Opportunity__c
			FROM csord__Service__c
			WHERE COM_Delivery_Order__c = :orderId
		];

		Map<Id, csord__Service__c> mapService = new Map<Id, csord__Service__c>();
		for (csord__Service__c service : services) {
			mapService.put(service.Id, service);
		}
		return mapService;
	}

	public static String getComponentAssignedProductId(Id serviceId, BillingData billingData, csord__Service_Line_Item__c sli) {
		String assignedProductId = '';

		if (
			billingData.mapServices != null &&
			billingData.mapServices.get(serviceId) != null &&
			billingData.mapServices.get(serviceId).csordtelcoa__Replaced_Service__c != null
		) {
			Id replacedServiceId = billingData.mapServices.get(serviceId).csordtelcoa__Replaced_Service__c;

			if (replacedServiceId != null) {
				List<csord__Service_Line_Item__c> serviceLineItems = billingData.mapServiceServiceLineItem.get(replacedServiceId);
				csord__Service_Line_Item__c matchingItem = getServiceLineItemPerType(
					serviceLineItems,
					COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE,
					sli.csord__Is_Recurring__c
				);
				assignedProductId = matchingItem != null ? matchingItem.Assigned_Product_Id__c : '';
			} else {
				assignedProductId = sli.Assigned_Product_Id__c != null ? sli.Assigned_Product_Id__c : '';
			}
		}
		return assignedProductId;
	}

	private static Map<Id, List<csord__Service_Line_Item__c>> retrieveServiceLineItemData(Set<Id> serviceIds) {
		List<csord__Service_Line_Item__c> serviceLineItems = [
			SELECT
				Id,
				csord__Discount_Type__c,
				csord__Discount_Value__c,
				csord__line_item_type__c,
				csord__Is_Recurring__c,
				csord__Service__c,
				csord__Line_Description__c,
				Number_of_failed_attempts__c,
				Billing_Status__c,
				SFIBID__c,
				Assigned_Product_Id__c
			FROM csord__Service_Line_Item__c
			WHERE csord__Service__c IN :serviceIds
		];
		Map<Id, List<csord__Service_Line_Item__c>> mapServiceServiceLineItem = new Map<Id, List<csord__Service_Line_Item__c>>();
		for (csord__Service_Line_Item__c item : serviceLineItems) {
			if (mapServiceServiceLineItem != null && mapServiceServiceLineItem.get(item.csord__Service__c) != null) {
				mapServiceServiceLineItem.get(item.csord__Service__c).add(item);
			} else {
				List<csord__Service_Line_Item__c> sliList = new List<csord__Service_Line_Item__c>();
				sliList.add(item);
				mapServiceServiceLineItem.put(item.csord__Service__c, sliList);
			}
		}
		return mapServiceServiceLineItem;
	}

	private static Map<Id, SpecificationItem> retrieveServiceSpecifications(Set<Id> serviceIds) {
		List<Attachment> serviceSpecifications = [
			SELECT Id, Name, Body, ParentId
			FROM Attachment
			WHERE name = :COM_DXL_Constants.COM_DXL_BSS_SPECIFICATION_NAME AND ParentId IN :serviceIds
		];
		Map<Id, Attachment> mapServiceAttachment = new Map<Id, Attachment>();
		for (Attachment att : serviceSpecifications) {
			mapServiceAttachment.put(att.ParentId, att);
		}
		Map<Id, SpecificationItem> mapServiceSpecificationItems = getSpecificationItems(serviceIds, mapServiceAttachment);
		return mapServiceSpecificationItems;
	}

	private static String getActivityType(Id serviceId, BillingData billData) {
		csord__Service__c service = billData.mapServices.get(serviceId);

		if (service.csordtelcoa__Delta_Status__c == COM_DXL_Constants.COM_DELTA_STATUS_DELETION) {
			return COM_DXL_Constants.COM_DXL_BSS_ACTIVITY_TYPE_CEASE;
		} else if (service.csordtelcoa__Delta_Status__c == COM_DXL_Constants.COM_DELTA_STATUS_CONTINUING) {
			return COM_DXL_Constants.COM_DXL_BSS_ACTIVITY_TYPE_CHANGE;
		}
		return COM_DXL_Constants.COM_DXL_BSS_ACTIVITY_TYPE_PROVIDE;
	}
	public static Map<String, List<orderLineItem>> getOrderLineItemsPerSite(BillingData billData) {
		Map<String, List<orderLineItem>> result = new Map<String, List<orderLineItem>>();
		for (Id serviceId : billData.mapServices.keySet()) {
			SpecificationItem specItem = billData.mapServiceSpecification.get(serviceId);
			if (specItem != null) {
				List<OrderLineItem> orderLineItems = getOrderLineItems(serviceId, specItem, billData);
				for (OrderLineItem oli : orderLineItems) {
					String sLSAPID = oli.sLSAPID != null ? oli.sLSAPID : '*';
					if (result.containsKey(sLSAPID)) {
						result.get(sLSAPID).add(oli);
					} else {
						List<OrderLineItem> orderLineItemList = new List<OrderLineItem>();
						orderLineItemList.add(oli);
						result.put(sLSAPID, orderLineItemList);
					}
				}
			}
		}
		return result;
	}

	private static Boolean checkIfOrderLineItemValid(OrderLineItem orderLineItem) {
		Boolean result = true;
		if (orderLineItem.chargeDescription == '' || orderLineItem.chargeCode == '') {
			result = false;
		}
		return result;
	}

	private static csord__Service_Line_Item__c getServiceLineItemPerType(
		List<csord__Service_Line_Item__c> serviceLineItems,
		List<String> typeOfLineItem,
		Boolean isRecurring
	) {
		csord__Service_Line_Item__c sli;
		for (csord__Service_Line_Item__c item : serviceLineItems) {
			if (typeOfLineItem.contains(item.csord__line_item_type__c) && item.csord__Is_Recurring__c == isRecurring) {
				sli = item;
			}
		}
		return sli;
	}

	private static List<OrderLineItem> getOrderLineItems(Id serviceId, SpecificationItem specItem, BillingData billData) {
		List<OrderLineItem> orderLineItems = new List<OrderLineItem>();
		String activityType = getActivityType(serviceId, billData);

		for (csord__Service_Line_Item__c serviceLineItem : billData.mapServiceServiceLineItem.get(serviceId)) {
			if (COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE.contains(serviceLineItem.csord__line_item_type__c)) {
				OrderLineItem orderLineItem = new OrderLineItem();
				orderLineItem.sLSAPID = billData.mapServices.get(serviceId).Site__r.Assigned_Product_Id__c != null
					? billData.mapServices.get(serviceId).Site__r.Assigned_Product_Id__c
					: '';
				orderLineItem.sFIBID = serviceLineItem.SFIBID__c;
				orderLineItem.componentAPID = '';
				if (activityType != COM_DXL_Constants.COM_DXL_BSS_ACTIVITY_TYPE_PROVIDE) {
					orderLineItem.componentAPID = getComponentAssignedProductId(serviceId, billData, serviceLineItem);
					serviceLineItem.Assigned_Product_Id__c = orderLineItem.componentAPID;
				}
				orderLineItem.activityType = activityType;
				orderLineItem.activationDate = calculateActivationDate();

				orderLineItem.invoicingStartDate = calculateStartInvoicingDate(
					billData.mapServices.get(serviceId).csord__Activation_Date__c,
					billData.mapServices.get(serviceId).csord__Deactivation_Date__c,
					activityType
				);

				orderLineItem.bItag5 = billData.bITAG5 != null ? billData.bITAG5 : COM_DXL_Constants.COM_DXL_BSS_SETTING_BITAG5_MOCK;
				orderLineItem = getOrderLineItemValuesPerSLIType(orderLineItem, specItem, serviceLineItem);

				orderLineItem = retrieveDiscountData(orderLineItem, billData, serviceId);

				if (checkIfOrderLineItemValid(orderLineItem)) {
					orderLineItems.add(orderLineItem);
				}
			}
		}

		return orderLineItems;
	}

	private static OrderLineItem retrieveDiscountData(OrderLineItem orderLineItem, BillingData billData, Id serviceId) {
		Boolean isRecurring = orderLineItem.typeOfCharge == COM_DXL_Constants.COM_DXL_BSS_CHARGE_TYPE_RECURRING ? true : false;
		orderLineItem.discountAmount = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE_DISCOUNT,
				isRecurring
			) != null
			? getServiceLineItemPerType(
						billData.mapServiceServiceLineItem.get(serviceId),
						COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE_DISCOUNT,
						isRecurring
					)
					.csord__Discount_Value__c
			: null;

		orderLineItem.discountDescription = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE_DISCOUNT,
				isRecurring
			) != null
			? specialCharactersReplacement(
					getServiceLineItemPerType(
							billData.mapServiceServiceLineItem.get(serviceId),
							COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE_DISCOUNT,
							isRecurring
						)
						.csord__Line_Description__c
			  )
			: '';

		orderLineItem.discountMethod = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE_DISCOUNT,
				isRecurring
			) != null
			? getServiceLineItemPerType(
						billData.mapServiceServiceLineItem.get(serviceId),
						COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE_DISCOUNT,
						isRecurring
					)
					.csord__Discount_Type__c
			: '';

		return orderLineItem;
	}

	private static String calculateActivationDate() {
		return System.today().format();
	}

	private static String calculateStartInvoicingDate(DateTime implementedDate, Date deactivationDate, String activityType) {
		if (implementedDate == null && activityType != COM_DXL_Constants.COM_DXL_BSS_ACTIVITY_TYPE_CEASE) {
			return '';
		}
		Date result = activityType == COM_DXL_Constants.COM_DXL_BSS_ACTIVITY_TYPE_CEASE
			? deactivationDate
			: date.newinstance(implementedDate.year(), implementedDate.month(), implementedDate.day());
		return result.format();
	}

	private class SpecificationItem {
		private String recurringBITag4;
		private String recurringBITag3;
		private String recurringBITag2;
		private String recurringBITag1;
		private String recurringChargeCode;
		private String recurringChargeDescription;
		private String oneOffBITag4;
		private String oneOffBITag3;
		private String oneOffChargeDescription;
		private String oneOffChargeCode;
		private String oneOffBITag2;
		private String oneOffBITag1;
		private Decimal quantity;
		private Decimal baseOTC;
		private Decimal baseMRC;
	}

	public class OrderLineItem {
		public String sFIBID;
		public String sLSAPID;
		public String componentAPID;
		public String activityType;
		public String activationDate;
		public String invoicingStartDate;
		public String typeOfCharge;
		public String chargeDescription;
		public Decimal chargeAmount;
		public Integer quantity;
		public Decimal discountAmount;
		public String discountDescription;
		public String discountMethod;
		public String chargeCode;
		public String bItag1;
		public String bItag2;
		public String bItag3;
		public String bItag4;
		public String bItag5;
		public String externalReferenceID;
	}

	public class CustomerInformation {
		public String baid;
		public String bcid;
	}

	public class BillingData {
		private Id accountId;
		private Id orderId;
		private Id osOrderId;
		public String b2bGPCI;
		public String bITAG5;
		public CustomerInformation customerInformation;
		public String transactionId;
		public Map<Id, csord__Service__c> mapServices;
		public Map<Id, SpecificationItem> mapServiceSpecification;
		public Map<Id, List<csord__Service_Line_Item__c>> mapServiceServiceLineItem;
		public Set<Id> replacedServices;
	}
}
