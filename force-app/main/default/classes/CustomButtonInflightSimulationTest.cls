@isTest
public with sharing class CustomButtonInflightSimulationTest {
    @isTest
    private static void test() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
          List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
          User simpleUser = CS_DataTest.createUser(pList, roleList); 
          insert simpleUser;
          System.runAs (simpleUser) { 

          Test.startTest();

          Account testAccount = CS_DataTest.createAccount('Test Account');
          insert testAccount;
          
          Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
          insert testOpp;
          
          cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
          basket.Primary__c = true;
          basket.cscfga__Basket_Status__c = 'Valid';
          insert basket;
          
          CustomButtonInflightSimulation button = new CustomButtonInflightSimulation();
         
          String result = button.performAction(String.valueOf(basket.Id));
          System.assertNotEquals('', result);
          
          Test.stopTest();
       }
    }
}