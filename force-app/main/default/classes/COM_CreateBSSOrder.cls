/*
COM project 2021
Used for Billing to Unify 
*/

global with sharing class COM_CreateBSSOrder {
	@TestVisible
	private static final String UNIFY_SYSTEM = 'Unify';

	@TestVisible
	private static final String ATTACHMENT_NAME = 'Request_payload';

	@TestVisible
	private static final String UNIFY_SETTING_NAME = 'createBSSOrder_setting';

	@TestVisible
	private static final String INTEGRATION_SETTING_NAME = 'SIASRESTCreateBSSOrder';

	@TestVisible
	private static final String BILLING_STATUS_REQUESTED = 'Requested';

	@TestVisible
	private static final String BILLING_STATUS_REQUEST_FAILED = 'Failed';

	@TestVisible
	private static final String BILLING_STATUS_REQUEST_ACTIVE = 'Active';

	@TestVisible
	private static final String BILLING_STATUS_REQUEST_DISCOUNT = 'N/A';

	@TestVisible
	private static final Integer MAX_NR_OF_FAILED_REQUESTS = 3;

	@TestVisible
	private static final String SPECIFICATION_NAME = 'ServiceSpecifications.json';

	@TestVisible
	private static final String SPECIFICATION_KEY = 'specifications';

	@TestVisible
	private static final String ATTRIBUTES_KEY = 'attributes';

	@TestVisible
	private static final List<String> SLI_CHARGE_DISCOUNT = new List<String>{
		'Time Limited Discount Charge',
		'Discount Charge'
	};

	@TestVisible
	private static final List<String> SLI_CHARGE = new List<String>{ 'Charge' };

	@TestVisible
	private static final String DISCOUNT_PERCENTAGE = 'Percentage';

	@TestVisible
	private static final String DISCOUNT_AMOUNT = 'Absolute';

	@TestVisible
	private static final String CHARGE_TYPE_ONEOFF = 'OC';

	@TestVisible
	private static final String CHARGE_TYPE_RECURRING = 'RC';

	@TestVisible
	private static final String ACTIVITY_TYPE_PROVIDE = 'Provide';

	@TestVisible
	private static final String ACTIVITY_TYPE_CHANGE = 'Change';

	@TestVisible
	private static final String ACTIVITY_TYPE_CEASE = 'Cease';

	@InvocableMethod(label='Unify_CreateBSSOrder ' description='Send order to Unify for billing')
	global static void sendToBilling(List<Id> orderIds) {
		COM_Integration_setting__mdt unifySetting = COM_Integration_setting__mdt.getInstance(
			UNIFY_SETTING_NAME
		);
		Boolean doNotExecuteReq = unifySetting != null ? unifySetting.Execute_mock__c : true;
		Boolean doNotExecuteStatus = unifySetting != null
			? unifySetting.Mock_Execution_result__c
			: true;
		Boolean generateRequestAttachment = unifySetting != null
			? unifySetting.Generate_Attachment__c
			: false;

		String successStatus = unifySetting != null
			? unifySetting.Sync_success_status__c
			: BILLING_STATUS_REQUEST_ACTIVE;
		String intermittentStatus = unifySetting != null
			? unifySetting.Sync_intermittent_status__c
			: BILLING_STATUS_REQUESTED;
		String failedStatus = unifySetting != null
			? unifySetting.Sync_failed_status__c
			: BILLING_STATUS_REQUEST_FAILED;

		String transactionId = 'TR-' + Datetime.now().getTime();
		BillingData billingData = retrieveBillingData(orderIds[0]);
		if (billingData != null) {
			List<BillingItem> billingItems = getBillingItems(billingData);
			System.debug('billingItems');
			System.debug(billingItems);
			if (billingItems != null && billingItems.size() > 0) {
				String requestBody = generateCreateBSSOrderRequestBody(billingItems, transactionId);
				System.debug(requestBody);
				HTTPResponse response = sendCreateBSSOrderRequest(
					transactionId,
					billingItems,
					doNotExecuteReq,
					doNotExecuteStatus
				);
				Id parentId = parseResponse(
					response,
					billingData,
					transactionID,
					successStatus,
					intermittentStatus,
					failedStatus
				);
				if (generateRequestAttachment && (parentId != null)) {
					createRequestAttachment(parentId, requestBody);
				}
			}
		}
	}

	public static Id parseResponse(
		HTTPResponse response,
		BillingData billData,
		String transactionID,
		String statusSuccess,
		String statusIntermittent,
		String statusFailed
	) {
		List<csord__Service_Line_Item__c> sliList = retrieveSliList(
			billData.mapServiceServiceLineItem
		);
		if (response.getStatusCode() == 200) {
			updateSLI(true, sliList, statusSuccess, statusIntermittent, statusFailed);
			updateService(billData, statusSuccess);
			updateSubscription(billData, statusSuccess);
		} else {
			Boolean failed = updateSLI(
				false,
				sliList,
				statusSuccess,
				statusIntermittent,
				statusFailed
			);
			if (failed) {
				updateService(billData, statusFailed);
				updateSubscription(billData, statusFailed);
			} else {
				updateService(billData, statusIntermittent);
				updateSubscription(billData, statusIntermittent);
			}
		}

		Order_Billing_Transaction__c obt = createBillingTransaction(billData, transactionID, '');
		if (obt != null) {
			insert obt;
			return obt.Id;
		}
		return null;
	}

	public static HTTPResponse sendCreateBSSOrderRequest(
		String transactionId,
		List<BillingItem> billingItems,
		Boolean doNotExecute,
		Boolean doNotExecuteStatus
	) {
		// Retrieve the credentials from the custom setting
		External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(
			INTEGRATION_SETTING_NAME
		);
		String endpointURL = webServiceConfig.URL__c;
		String authorizationHeaderString =
			webServiceConfig.Username__c +
			':' +
			webServiceConfig.Password__c;
		String authorizationHeader =
			'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(authorizationHeaderString));
		HttpRequest reqData = new HttpRequest();
		Http http = new Http();
		reqData.setHeader('Content-Type', 'application/json');
		reqData.setHeader('Connection', 'keep-alive');
		reqData.setHeader('Content-Length', '0');
		reqData.setHeader('Authorization', authorizationHeader);
		reqData.setTimeout(120000);
		reqData.setEndpoint(endpointURL);
		if (String.isNotEmpty(webServiceConfig.Certificate_Name__c)) {
			reqData.setClientCertificateName(webServiceConfig.Certificate_Name__c);
		}
		String requestBody = generateCreateBSSOrderRequestBody(billingItems, transactionId);
		System.debug('### Request body: ' + requestBody);
		reqData.setBody(requestBody);
		reqData.setMethod('POST');
		System.debug('@@@@ reqData: ' + reqData);
		HTTPResponse response = new HttpResponse();
		if (doNotExecute && !Test.isRunningTest()) {
			response.setHeader('Content-Type', 'application/json');
			if (doNotExecuteStatus) {
				System.debug('Execute mock!');
				response.setBody('{"status":"success"}');
				response.setStatusCode(200);
			} else {
				response.setBody('ERROR MESSAGE');
				response.setStatusCode(400);
			}
		} else {
			response = http.send(reqData);
		}
		System.debug('@@@@ response: ' + response);
		return response;
	}

	public static void updateService(BillingData billData, String status) {
		List<csord__Service__c> serviceToUpdate = new List<csord__Service__c>();
		for (Id serviceId : billData.mapServices.keyset()) {
			csord__Service__c service = new csord__Service__c();
			service.Id = serviceId;
			service.csord__Status__c = status;
			serviceToUpdate.add(service);
		}
		if (serviceToUpdate.size() > 0) {
			update serviceToUpdate;
		}
	}

	public static void updateSubscription(BillingData billData, String status) {
		List<csord__Subscription__c> subscriptionToUpdate = new List<csord__Subscription__c>();
		Set<Id> subscriptionIds = new Set<Id>();
		for (Id serviceId : billData.mapServices.keyset()) {
			subscriptionIds.add(billData.mapServices.get(serviceId).csord__Subscription__c);
		}
		for (Id subscriptionId : subscriptionIds) {
			csord__Subscription__c subscription = new csord__Subscription__c();
			subscription.Id = subscriptionId;
			subscription.csord__Status__c = status;
			subscriptionToUpdate.add(subscription);
		}
		if (subscriptionToUpdate.size() > 0) {
			update subscriptionToUpdate;
		}
	}

	//value true is returned in case service needs to be marked as failed
	public static Boolean updateSLI(
		Boolean success,
		List<csord__Service_Line_Item__c> sliList,
		String statusSuccess,
		String statusIntermittent,
		String statusFailed
	) {
		Boolean result = false;
		List<csord__Service_Line_Item__c> sliUpdate = new List<csord__Service_Line_Item__c>();
		if (success) {
			for (csord__Service_Line_Item__c sli : sliList) {
				sli.Number_of_failed_attempts__c = 0;
				sli.Billing_Status__c = statusSuccess;

				if (SLI_CHARGE_DISCOUNT.contains(sli.csord__line_item_type__c)) {
					sli.Billing_Status__c = BILLING_STATUS_REQUEST_DISCOUNT;
				}
				sliUpdate.add(sli);
			}
		} else {
			for (csord__Service_Line_Item__c sli : sliList) {
				if (sli.Number_of_failed_attempts__c == null) {
					sli.Number_of_failed_attempts__c = 1;
				} else {
					sli.Number_of_failed_attempts__c = sli.Number_of_failed_attempts__c + 1;
				}
				if (sli.Number_of_failed_attempts__c == 3) {
					sli.Billing_Status__c = statusFailed;
					result = true;
				} else {
					sli.Billing_Status__c = statusIntermittent;
				}

				if (SLI_CHARGE_DISCOUNT.contains(sli.csord__line_item_type__c)) {
					sli.Billing_Status__c = BILLING_STATUS_REQUEST_DISCOUNT;
				}
				sliUpdate.add(sli);
			}
		}
		if (sliUpdate.size() > 0) {
			update sliUpdate;
		}
		return result;
	}

	public static Order_Billing_Transaction__c createBillingTransaction(
		BillingData billData,
		String transactionId,
		String errorMessage
	) {
		Set<Id> serviceLineItemIds = retrieveIds(billData.mapServiceServiceLineItem);
		Order_Billing_Transaction__c orderBillingTransaction = new Order_Billing_Transaction__c(
			Service_Line_Items__c = JSON.serialize(serviceLineItemIds),
			Account__c = billData.accountId,
			Transaction_Id__c = transactionId,
			Delivery_Order__c = billData.orderId,
			OS_Order__c = billData.osOrderId,
			Error_Info__c = errorMessage != '' ? errorMessage : ''
		);
		return orderBillingTransaction;
	}

	public static Set<Id> retrieveIds(
		Map<Id, List<csord__Service_Line_Item__c>> mapServiceServiceLineItem
	) {
		Set<Id> result = new Set<Id>();
		for (Id key : mapServiceServiceLineItem.keySet()) {
			for (csord__Service_Line_Item__c sli : mapServiceServiceLineItem.get(key)) {
				if (SLI_CHARGE.contains(sli.csord__line_item_type__c)) {
					result.add(sli.Id);
				}
			}
		}
		return result;
	}

	public static List<csord__Service_Line_Item__c> retrieveSliList(
		Map<Id, List<csord__Service_Line_Item__c>> mapServiceServiceLineItem
	) {
		List<csord__Service_Line_Item__c> result = new List<csord__Service_Line_Item__c>();
		for (Id key : mapServiceServiceLineItem.keySet()) {
			result.addAll(mapServiceServiceLineItem.get(key));
		}
		return result;
	}

	public static String generateCreateBSSOrderRequestBody(
		List<BillingItem> billingItems,
		String transactionId
	) {
		String requestString;
		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeFieldName('createBSSOrderRequest');
		generator.writeStartObject();
		generator.writeStringField('transactionID', transactionId);
		generator.writeFieldName('orderLinesList');
		generator.writeStartArray();
		for (BillingItem billingItem : billingItems) {
			if (billingItem.activityType == ACTIVITY_TYPE_PROVIDE) {
				generator = generateNewProvide(generator, billingItem);
			} else if (billingItem.activityType == ACTIVITY_TYPE_CHANGE) {
				generator = generateChange(generator, billingItem);
			} else if (billingItem.activityType == ACTIVITY_TYPE_CEASE) {
				generator = generateCease(generator, billingItem);
			}
		}
		generator.writeEndArray();
		generator.writeEndObject();
		generator.writeEndObject();
		requestString = generator.getAsString();
		return requestString;
	}

	private static JSONGenerator generateCease(JSONGenerator generator, BillingItem billingItem) {
		generator.writeStartObject();
		generator.writeStringField('SFIBID', billingItem.sFIBID);
		generator.writeStringField('componentAPID', billingItem.componentAPID);
		generator.writeStringField('activityType', billingItem.activityType);
		generator.writeStringField('activationDate', billingItem.activationDate);
		generator.writeStringField('externalReferenceID', billingItem.externalReferenceID);
		generator.writeEndObject();
		return generator;
	}
	private static JSONGenerator generateChange(JSONGenerator generator, BillingItem billingItem) {
		generator.writeStartObject();
		generator.writeStringField('SFIBID', billingItem.sFIBID);
		generator.writeStringField('componentAPID', billingItem.componentAPID);
		generator.writeStringField('activityType', billingItem.activityType);
		generator.writeStringField('activationDate', billingItem.activationDate);
		generator.writeStringField('invoicingStartDate', billingItem.invoicingStartDate);
		generator.writeStringField('typeOfCharge', billingItem.typeOfCharge);
		generator.writeStringField('chargeDescription', billingItem.chargeDescription);
		generator.writeStringField(
			'chargeAmount',
			emptyStringInsteadOfNull(billingItem.chargeAmount)
		);
		generator.writeNumberField('quantity', billingItem.quantity);
		generator.writeStringField(
			'discountAmount',
			emptyStringInsteadOfNull(billingItem.discountAmount)
		);
		generator.writeStringField('discountDescription', billingItem.discountDescription);
		generator.writeStringField(
			'discountMethod',
			mapDiscountMethodValue(billingItem.discountMethod)
		);
		generator.writeStringField('chargeCode', billingItem.chargeCode);
		generator.writeStringField('BItag1', billingItem.bItag1);
		generator.writeStringField('BItag2', billingItem.bItag2);
		generator.writeStringField('BItag3', billingItem.bItag3);
		generator.writeStringField('BItag4', billingItem.bItag4);
		generator.writeStringField('externalReferenceID', billingItem.externalReferenceID);
		generator.writeEndObject();
		return generator;
	}

	private static JSONGenerator generateNewProvide(
		JSONGenerator generator,
		BillingItem billingItem
	) {
		generator.writeStartObject();
		generator.writeStringField('SLSAPID', billingItem.sLSAPID);
		generator.writeStringField('SFIBID', billingItem.sFIBID);
		generator.writeStringField('BAID', billingItem.bAID);
		generator.writeStringField('BCID', billingItem.bCID);
		generator.writeStringField('componentAPID', billingItem.componentAPID); // Not required
		generator.writeStringField('activityType', billingItem.activityType);
		generator.writeStringField('activationDate', billingItem.activationDate);
		generator.writeStringField('invoicingStartDate', billingItem.invoicingStartDate);
		generator.writeStringField('typeOfCharge', billingItem.typeOfCharge);
		generator.writeStringField('chargeDescription', billingItem.chargeDescription);
		generator.writeStringField(
			'chargeAmount',
			emptyStringInsteadOfNull(billingItem.chargeAmount)
		);
		generator.writeNumberField('quantity', billingItem.quantity);
		generator.writeStringField(
			'discountAmount',
			emptyStringInsteadOfNull(billingItem.discountAmount)
		);
		generator.writeStringField('discountDescription', billingItem.discountDescription);
		generator.writeStringField(
			'discountMethod',
			mapDiscountMethodValue(billingItem.discountMethod)
		);
		generator.writeStringField('chargeCode', billingItem.chargeCode);
		generator.writeStringField('BItag1', billingItem.bItag1);
		generator.writeStringField('BItag2', billingItem.bItag2);
		generator.writeStringField('BItag3', billingItem.bItag3);
		generator.writeStringField('BItag4', billingItem.bItag4);
		generator.writeStringField('externalReferenceID', billingItem.externalReferenceID);
		generator.writeEndObject();
		return generator;
	}

	public static BillingData retrieveBillingData(Id orderId) {
		BillingData result = new BillingData();
		result.orderId = orderId;
		if (
			csord__Service__c.SObjectType.getDescribe().isAccessible() &&
			Schema.SObjectType.csord__Service__c.fields.Id.isAccessible()
		) {
			result.mapServices = retrieveServiceData(orderId);
			Id firstEl = new List<Id>(result.mapServices.keySet())[0];
			result.accountId = result.mapServices.get(firstEl).csord__Order__r.csord__Account__c;
			result.osOrderId = result.mapServices.get(firstEl).csord__Order__c;
			result.mapServiceServiceLineItem = retrieveServiceLineItemData(
				result.mapServices.keySet()
			);
			if (
				Attachment.SObjectType.getDescribe().isAccessible() &&
				Schema.SObjectType.Attachment.fields.Id.isAccessible() &&
				Schema.SObjectType.Attachment.fields.Body.isAccessible() &&
				Schema.SObjectType.Attachment.fields.Name.isAccessible() &&
				Schema.SObjectType.Attachment.fields.ParentId.isAccessible()
			) {
				result.mapServiceSpecification = retrieveServiceSpecifications(
					result.mapServices.keySet()
				);
			}
		}
		return result;
	}

	private static String mapDiscountMethodValue(Object input) {
		String result = '';
		if (input != null && input != '') {
			result = String.valueOf(input);
			result = (result == DISCOUNT_PERCENTAGE) ? DISCOUNT_PERCENTAGE : DISCOUNT_AMOUNT;
		}
		return result;
	}

	private static String emptyStringInsteadOfNull(Object input) {
		String result = '';
		if (input != null) {
			result = String.valueOf(input);
		}
		return result;
	}

	public static Map<Id, csord__Service__c> retrieveServiceData(Id orderId) {
		List<csord__Service__c> services = [
			SELECT
				Id,
				csord__Status__c,
				Implemented_Date__c,
				Site__r.Assigned_Product_Id__c,
				csord__Order__r.csord__Account__c,
				csord__Order__r.Ban_Information__r.Unify_Ref_Id__c,
				csord__Order__r.Billing_Arrangement__r.Unify_Ref_Id__c,
				csord__Subscription__c
			FROM csord__Service__c
			WHERE COM_Delivery_Order__c = :orderId
		];
		Set<Id> serviceIds = new Set<Id>();
		Map<Id, csord__Service__c> mapService = new Map<Id, csord__Service__c>();
		for (csord__Service__c service : services) {
			mapService.put(service.Id, service);
		}
		return mapService;
	}

	public static Map<Id, List<csord__Service_Line_Item__c>> retrieveServiceLineItemData(
		Set<Id> serviceIds
	) {
		List<csord__Service_Line_Item__c> serviceLineItems = [
			SELECT
				Id,
				csord__Discount_Type__c,
				csord__Discount_Value__c,
				csord__line_item_type__c,
				csord__Is_Recurring__c,
				csord__Service__c,
				csord__Line_Description__c,
				Number_of_failed_attempts__c,
				Billing_Status__c,
				SFIBID__c
			FROM csord__Service_Line_Item__c
			WHERE csord__Service__c IN :serviceIds
		];
		Map<Id, List<csord__Service_Line_Item__c>> mapServiceServiceLineItem = new Map<Id, List<csord__Service_Line_Item__c>>();
		for (csord__Service_Line_Item__c item : serviceLineItems) {
			if (
				mapServiceServiceLineItem != null &&
				mapServiceServiceLineItem.get(item.csord__Service__c) != null
			) {
				mapServiceServiceLineItem.get(item.csord__Service__c).add(item);
			} else {
				List<csord__Service_Line_Item__c> sliList = new List<csord__Service_Line_Item__c>();
				sliList.add(item);
				mapServiceServiceLineItem.put(item.csord__Service__c, sliList);
			}
		}
		return mapServiceServiceLineItem;
	}

	public static Map<Id, SpecificationItem> retrieveServiceSpecifications(Set<Id> serviceIds) {
		List<Attachment> serviceSpecifications = [
			SELECT Id, Name, Body, ParentId
			FROM Attachment
			WHERE name = :SPECIFICATION_NAME AND ParentId IN :serviceIds
		];
		Map<Id, Attachment> mapServiceAttachment = new Map<Id, Attachment>();
		for (Attachment att : serviceSpecifications) {
			mapServiceAttachment.put(att.ParentId, att);
		}
		Map<Id, SpecificationItem> mapServiceSpecificationItems = getSpecificationItems(
			serviceIds,
			mapServiceAttachment
		);
		return mapServiceSpecificationItems;
	}

	public static List<BillingItem> getBillingItems(BillingData billData) {
		List<BillingItem> result = new List<BillingItem>();
		for (Id serviceId : billData.mapServices.keySet()) {
			SpecificationItem specItem = billData.mapServiceSpecification.get(serviceId);
			if (specItem != null) {
				BillingItem oneOff = getBillingItem(
					serviceId,
					specItem,
					billData,
					ACTIVITY_TYPE_PROVIDE,
					false
				); //getOneOffBillingLineItem(serviceId, specItem, billData);
				BillingItem recurring = getBillingItem(
					serviceId,
					specItem,
					billData,
					ACTIVITY_TYPE_PROVIDE,
					true
				); //getRecurringBillingLineItem(serviceId, specItem, billData);
				if (checkIfBillingItemValid(oneOff)) {
					result.add(oneOff);
				}
				if (checkIfBillingItemValid(recurring)) {
					result.add(recurring);
				}
			}
		}
		return result;
	}

	private static Boolean checkIfBillingItemValid(BillingItem billingItem) {
		Boolean result = true;
		if (billingItem.chargeDescription == '' || billingItem.chargeCode == '') {
			result = false;
		}
		return result;
	}

	private static csord__Service_Line_Item__c getServiceLineItemPerType(
		List<csord__Service_Line_Item__c> serviceLineItems,
		List<String> typeOfLineItem,
		Boolean isRecurring
	) {
		csord__Service_Line_Item__c sli;
		for (csord__Service_Line_Item__c item : serviceLineItems) {
			if (
				typeOfLineItem.contains(item.csord__line_item_type__c) &&
				item.csord__Is_Recurring__c == isRecurring
			) {
				sli = item;
			}
		}
		return sli;
	}

	private static BillingItem getBillingItem(
		Id serviceId,
		SpecificationItem specItem,
		BillingData billData,
		String activityType,
		Boolean isRecurring
	) {
		BillingItem billingItem = new BillingItem();
		billingItem.sLSAPID = billData.mapServices.get(serviceId).Site__r.Assigned_Product_Id__c !=
			null
			? billData.mapServices.get(serviceId).Site__r.Assigned_Product_Id__c
			: '';
		billingItem.sFIBID = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				SLI_CHARGE,
				false
			)
			.SFIBID__c;
		billingItem.bAID = billData.mapServices.get(serviceId)
				.csord__Order__r.Billing_Arrangement__r.Unify_Ref_Id__c != null
			? billData.mapServices.get(serviceId)
					.csord__Order__r.Billing_Arrangement__r.Unify_Ref_Id__c
			: '';
		billingItem.bCID = billData.mapServices.get(serviceId)
				.csord__Order__r.Ban_Information__r.Unify_Ref_Id__c != null
			? billData.mapServices.get(serviceId).csord__Order__r.Ban_Information__r.Unify_Ref_Id__c
			: '';
		billingItem.componentAPID = '';
		billingItem.activityType = activityType;
		billingItem.activationDate = billData.mapServices.get(serviceId).Implemented_Date__c != null
			? string.valueOfGmt(
					calculateActivationDate(billData.mapServices.get(serviceId).Implemented_Date__c)
			  )
			: '';
		billingItem.invoicingStartDate = billData.mapServices.get(serviceId).Implemented_Date__c !=
			null
			? string.valueOfGmt(billData.mapServices.get(serviceId).Implemented_Date__c)
			: '';
		billingItem = isRecurring
			? getRecurringBillingLineItem(billingItem, serviceId, specItem, billData)
			: getOneOffBillingLineItem(billingItem, serviceId, specItem, billData);
		return billingItem;
	}

	private static BillingItem getOneOffBillingLineItem(
		BillingItem billingItem,
		Id serviceId,
		SpecificationItem specItem,
		BillingData billData
	) {
		billingItem.typeOfCharge = CHARGE_TYPE_ONEOFF;
		billingItem.chargeDescription = specItem.oneOffChargeDescription != null
			? specialCharactersReplacement(specItem.oneOffChargeDescription)
			: '';
		billingItem.chargeAmount = specItem.baseOTC != null ? specItem.baseOTC : 0.00;
		billingItem.quantity = specItem.quantity != null ? Integer.valueOf(specItem.quantity) : 1;
		billingItem.discountAmount = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				SLI_CHARGE_DISCOUNT,
				false
			) != null
			? getServiceLineItemPerType(
						billData.mapServiceServiceLineItem.get(serviceId),
						SLI_CHARGE_DISCOUNT,
						false
					)
					.csord__Discount_Value__c
			: null;
		billingItem.discountDescription = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				SLI_CHARGE_DISCOUNT,
				false
			) != null
			? specialCharactersReplacement(
					getServiceLineItemPerType(
							billData.mapServiceServiceLineItem.get(serviceId),
							SLI_CHARGE_DISCOUNT,
							false
						)
						.csord__Line_Description__c
			  )
			: '';
		billingItem.discountMethod = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				SLI_CHARGE_DISCOUNT,
				false
			) != null
			? getServiceLineItemPerType(
						billData.mapServiceServiceLineItem.get(serviceId),
						SLI_CHARGE_DISCOUNT,
						false
					)
					.csord__Discount_Type__c
			: '';
		billingItem.chargeCode = specItem.oneOffChargeCode != null ? specItem.oneOffChargeCode : '';
		billingItem.bItag1 = specItem.oneOffBITag1 != null ? specItem.oneOffBITag1 : '';
		billingItem.bItag2 = specItem.oneOffBITag2 != null ? specItem.oneOffBITag2 : '';
		billingItem.bItag3 = specItem.oneOffBITag3 != null ? specItem.oneOffBITag3 : '';
		billingItem.bItag4 = specItem.oneOffBITag4 != null ? specItem.oneOffBITag4 : '';
		billingItem.externalReferenceID = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				SLI_CHARGE,
				false
			)
			.Id;
		return billingItem;
	}

	private static BillingItem getRecurringBillingLineItem(
		BillingItem billingItem,
		Id serviceId,
		SpecificationItem specItem,
		BillingData billData
	) {
		billingItem.typeOfCharge = CHARGE_TYPE_RECURRING;
		billingItem.chargeDescription = specItem.recurringChargeDescription != null
			? specialCharactersReplacement(specItem.recurringChargeDescription)
			: '';
		billingItem.chargeAmount = specItem.baseMRC != null ? specItem.baseMRC : 0.00;
		billingItem.quantity = specItem.quantity != null ? Integer.valueOf(specItem.quantity) : 1;
		billingItem.discountAmount = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				SLI_CHARGE_DISCOUNT,
				true
			) != null
			? getServiceLineItemPerType(
						billData.mapServiceServiceLineItem.get(serviceId),
						SLI_CHARGE_DISCOUNT,
						true
					)
					.csord__Discount_Value__c
			: null;
		billingItem.discountDescription = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				SLI_CHARGE_DISCOUNT,
				true
			) != null
			? specialCharactersReplacement(
					getServiceLineItemPerType(
							billData.mapServiceServiceLineItem.get(serviceId),
							SLI_CHARGE_DISCOUNT,
							true
						)
						.csord__Line_Description__c
			  )
			: '';
		billingItem.discountMethod = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				SLI_CHARGE_DISCOUNT,
				true
			) != null
			? getServiceLineItemPerType(
						billData.mapServiceServiceLineItem.get(serviceId),
						SLI_CHARGE_DISCOUNT,
						true
					)
					.csord__Discount_Type__c
			: '';
		billingItem.chargeCode = specItem.recurringChargeCode != null
			? specItem.recurringChargeCode
			: '';
		billingItem.bItag1 = specItem.recurringBITag1 != null ? specItem.recurringBITag1 : '';
		billingItem.bItag2 = specItem.recurringBITag2 != null ? specItem.recurringBITag2 : '';
		billingItem.bItag3 = specItem.recurringBITag3 != null ? specItem.recurringBITag3 : '';
		billingItem.bItag4 = specItem.recurringBITag4 != null ? specItem.recurringBITag4 : '';
		billingItem.externalReferenceID = getServiceLineItemPerType(
				billData.mapServiceServiceLineItem.get(serviceId),
				SLI_CHARGE,
				true
			)
			.Id;
		return billingItem;
	}

	public static Map<Id, SpecificationItem> getSpecificationItems(
		Set<Id> serviceIds,
		Map<Id, Attachment> mapServiceAttachment
	) {
		Map<Id, SpecificationItem> result = new Map<Id, SpecificationItem>();
		for (Id serviceId : serviceIds) {
			if (mapServiceAttachment.containsKey(serviceId)) {
				String attachmentBody = mapServiceAttachment.get(serviceId).Body.toString();
				attachmentBody = attachmentBody.replace('"baseOTC" : ""', '"baseOTC" : 0.00');
				attachmentBody = attachmentBody.replace('"baseMRC" : ""', '"baseMRC" : 0.00');
				attachmentBody = attachmentBody.replace('"quantity" : ""', '"quantity" : 1.00');
				Map<String, Object> bodyMap = (Map<String, Object>) JSON.deserializeUntyped(
					attachmentBody
				);
				List<Map<String, Object>> data = new List<Map<String, Object>>();
				for (Object instance : (List<Object>) bodyMap.get(SPECIFICATION_KEY)) {
					data.add((Map<String, Object>) instance);
				}
				for (Map<String, Object> instance : data) {
					for (String key : instance.keySet()) {
						if (key == ATTRIBUTES_KEY) {
							SpecificationItem item = (SpecificationItem) JSON.deserialize(
								JSON.serialize(instance.get(key)),
								SpecificationItem.class
							);
							result.put(serviceId, item);
						}
					}
				}
			}
		}
		return result;
	}

	private static Date calculateActivationDate(Datetime dateFromDelivery) {
		Date deliveryDate = date.newinstance(
			dateFromDelivery.year(),
			dateFromDelivery.month(),
			dateFromDelivery.day()
		);
		return deliveryDate.day() == 1 ? deliveryDate : deliveryDate.addMonths(1).toStartOfMonth();
	}

	private static String specialCharactersReplacement(String input) {
		String result = input;
		result = input.replaceAll('\\+', 'plus').replaceAll(';', ',');
		return result;
	}

	private static void createRequestAttachment(Id parentId, String content) {
		Attachment attachment = new Attachment();
		attachment.Body = Blob.valueOf(content);
		attachment.Name = String.valueOf(ATTACHMENT_NAME);
		attachment.ParentId = parentId;
		insert attachment;
	}

	public class SpecificationItem {
		public String recurringBITag4;
		public String recurringBITag3;
		public String recurringBITag2;
		public String recurringBITag1;
		public String recurringChargeCode;
		public String recurringChargeDescription;
		public String oneOffBITag4;
		public String oneOffBITag3;
		public String oneOffChargeDescription;
		public String oneOffChargeCode;
		public String oneOffBITag2;
		public String oneOffBITag1;
		public Decimal quantity;
		public Decimal baseOTC;
		public Decimal baseMRC;
	}

	public class BillingItem {
		public String sLSAPID;
		public String sFIBID;
		public String bAID;
		public String bCID;
		public String componentAPID;
		public String activityType;
		public String activationDate;
		public String invoicingStartDate;
		public String typeOfCharge;
		public String chargeDescription;
		public Decimal chargeAmount;
		public Integer quantity;
		public Decimal discountAmount;
		public String discountDescription;
		public String discountMethod;
		public String chargeCode;
		public String bItag1;
		public String bItag2;
		public String bItag3;
		public String bItag4;
		public String externalReferenceID; //SLI ID
	}

	public class BillingData {
		Id accountId;
		Id orderId;
		Id osOrderId;
		Map<Id, csord__Service__c> mapServices;
		Map<Id, SpecificationItem> mapServiceSpecification;
		Map<Id, List<csord__Service_Line_Item__c>> mapServiceServiceLineItem;
	}

	public class CreateBSSOrderResponse {
		public String status;
	}
}
