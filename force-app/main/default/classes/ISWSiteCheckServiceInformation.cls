public with sharing class ISWSiteCheckServiceInformation {
    @TestVisible
    private static final String ORDERING_POSSIBLE_INTERNET_Y = 'Y';
    @TestVisible
    private static final String ORDERING_POSSIBLE_GIGA_Y = 'Y';

    @AuraEnabled
    public Boolean areTcpLinesEmpty { get; set; }
    @AuraEnabled
    public String serviceabilityRestrictions { get; set; }
    @AuraEnabled
    public String lineStatus { get; set; }
    @AuraEnabled
    public Boolean isInternetAvailable { get; set; }
    @AuraEnabled
    public Boolean isGigaAvailable { get; set; }

    @AuraEnabled
    public String tcpService { get; set; }
    @AuraEnabled
    public String footprint { get; set; }

    public ISWSiteCheckServiceInformation(
        ISWSiteCheckAPI.ISWSiteResponse iSWSiteResponse
    ) {
        this.tcpService = iSWSiteResponse.tcpService;
        this.footprint = iSWSiteResponse.footprint;
        this.isInternetAvailable =
            iSWSiteResponse.functionalOrderingPossibleInternet ==
            ORDERING_POSSIBLE_INTERNET_Y;
        this.isGigaAvailable =
            iSWSiteResponse.functionalOrderingPossibleGiga ==
            ORDERING_POSSIBLE_GIGA_Y;
        this.areTcpLinesEmpty = iSWSiteResponse.areTcpLinesEmpty;
        this.serviceabilityRestrictions = iSWSiteResponse.serviceabilityRestrictions;
        this.lineStatus = iSWSiteResponse.lineStatus;
    }
}