public without sharing class UnicaMassDelete {

	private ApexPages.StandardSetController setCon;
	private String retURL = '';
	private set<String> setToDelete = new set<String>();	

	public UnicaMassDelete(ApexPages.StandardSetController controller) {
		setCon = controller;
		retURL = ApexPages.currentPage().getParameters().get('retURL');
	}

	public PageReference DoDelete(){

		String theObject = '';
		for(sObject c : setCon.getSelected()){
			setToDelete.add(c.Id);
			if(theObject == ''){
				theObject = c.getSObjectType().getDescribe().getName();
			}
		}

		if(theObject == 'Unica_Campaign__c'){
			deleteRest();
		} else if(setToDelete.size() > 0){
			list<sObject> objItemList = new list<sObject>();
			objItemList = Database.query('Select Id From '+ theObject +' Where Id = :setToDelete');
			if(objItemList.size() > 0){
				delete objItemList;
			}
		}

		if(string.isBlank(retURL) == false){
			return new PageReference(retURL);
		} else{
			return new PageReference('/a0H/o');
		}
	}

	public void deleteRest(){
		set<String> ntaSet = new set<String>();
		for(Unica_Campaign__c uc : [SELECT Id, NTA_Id__c FROM Unica_Campaign__c WHERE Id in: setToDelete]){
			ntaSet.add(uc.NTA_Id__c);
		}
		list<Unica_Campaign__c> ucList = [SELECT Id FROM Unica_Campaign__c WHERE NTA_Id__c in: ntaSet];
		list<Lead> leadList = [SELECT Id FROM Lead WHERE NTA_Id__c in: ntaSet];
		if(ucList.size() > 0){
			delete ucList;
		}
		if(leadList.size() > 0){
			delete leadList;
		}
	}
}