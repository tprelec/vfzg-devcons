/*
	Author: Rahul Sharma
	Description: Tests class for SFCC_OrderDetailApi.cls
	Jira ticket: COM-3242
*/

@IsTest
private class TestSFCC_OrderDetailApi {
	@TestSetup
	static void makeData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		User owner = new User(Id = UserInfo.getUserId());
		Account account = TestUtils.createAccount(owner);
		TestUtils.createBan(account);
		Site__c site = TestUtils.createSite(account);
		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		VF_Contract__c vfContract = TestUtils.createVFContract(account, opportunity);
		OrderType__c orderType = TestUtils.createOrderType();
		System.assertNotEquals(null, orderType.Id);

		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct();
		insert product;
		System.assertNotEquals(null, product.Id);

		// create an order with new status
		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = orderType.Id,
			VF_Contract__c = vfContract.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;
		System.assertNotEquals(null, order.Id);

		List<Order__c> orders = [SELECT Id, Status__c FROM Order__c LIMIT 2];
		System.assertEquals(1, orders.size());

		List<Contracted_Products__c> contractedProducts = new List<Contracted_Products__c>();
		// add contracted products
		for (Integer count = 0; count < 4; count++) {
			contractedProducts.add(
				new Contracted_Products__c(
					Quantity__c = 1,
					Arpu_Value__c = 100,
					Duration__c = 1,
					Product__c = product.Id,
					VF_Contract__c = vfContract.Id,
					Site__c = site.Id,
					Order__c = order.Id,
					CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ
				)
			);
		}
		insert contractedProducts;

		for (Contracted_Products__c cp : contractedProducts) {
			System.assertNotEquals(null, cp.Id);
		}

		csord__Order__c csOrder = CS_DataTest.createOrder();
		csOrder.csord__Account__c = account.Id;
		csOrder.csordtelcoa__Opportunity__c = opportunity.Id;
		csOrder.Name = 'TestOrder1';
		csOrder.csord__Status2__c = 'In Delivery';
		insert csOrder;

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';

		insert subscription;

		csord__Service__c service = new csord__Service__c(
			csord__Identification__c = 'TestService',
			Name = 'TestService',
			csord__Subscription__c = subscription.Id,
			csord__Order__c = csOrder.Id,
			Installation_Required__c = true,
			Has_Delivery_Components__c = true
		);
		insert service;
	}

	@IsTest
	static void testMessageBadRequestDataFormat() {
		setRequest('Invalid JSON Request Body');

		Test.startTest();

		SFCC_OrderDetailApi.getDetail();

		Test.stopTest();

		System.assertEquals(SFCC_OrderDetailApi.SUCCESS_CODE_400, RestContext.response.statusCode);

		String responseString = RestContext.response.responseBody.toString();
		SFCC_OrderDetailApi.ResponseWrapper responseWrapper = (SFCC_OrderDetailApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderDetailApi.ResponseWrapper.class
		);
		System.assertEquals(System.Label.SFCC_OrderApi_UnableToParseRequestMessage, responseWrapper.errorMessage);
		System.assertEquals(null, responseWrapper.order);
	}

	@IsTest
	static void testMessageMandatoryOrderId() {
		setRequest('{"a": 1}');

		Test.startTest();

		SFCC_OrderDetailApi.getDetail();

		Test.stopTest();

		System.assertEquals(SFCC_OrderDetailApi.SUCCESS_CODE_400, RestContext.response.statusCode);

		String responseString = RestContext.response.responseBody.toString();
		SFCC_OrderDetailApi.ResponseWrapper responseWrapper = (SFCC_OrderDetailApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderDetailApi.ResponseWrapper.class
		);
		System.assertEquals(System.Label.SFCC_OrderApi_InvalidOrderIdMessage, responseWrapper.errorMessage);
		System.assertEquals(null, responseWrapper.order);
	}

	@IsTest
	static void testGetLegacyOrderDetail() {
		Order__c order = [SELECT Id FROM Order__c LIMIT 1];

		SFCC_OrderDetailApi.RequestWrapper requestWrapper = new SFCC_OrderDetailApi.RequestWrapper(order.Id);
		setRequest(JSON.serialize(requestWrapper));

		Test.startTest();

		SFCC_OrderDetailApi.getDetail();

		Test.stopTest();

		String responseString = RestContext.response.responseBody.toString();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_200, RestContext.response.statusCode, responseString);
		SFCC_OrderDetailApi.ResponseWrapper responseWrapper = (SFCC_OrderDetailApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderDetailApi.ResponseWrapper.class
		);
		System.assertEquals(null, responseWrapper.errorMessage);
		System.assertNotEquals(null, responseWrapper.order);
	}

	@IsTest
	static void testGetCsOrderDetail() {
		csord__Order__c csOrder = [SELECT Id FROM csord__Order__c LIMIT 1];

		SFCC_OrderDetailApi.RequestWrapper requestWrapper = new SFCC_OrderDetailApi.RequestWrapper(csOrder.Id);
		setRequest(JSON.serialize(requestWrapper));

		Test.startTest();

		SFCC_OrderDetailApi.getDetail();

		Test.stopTest();

		String responseString = RestContext.response.responseBody.toString();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_200, RestContext.response.statusCode, responseString);
		SFCC_OrderDetailApi.ResponseWrapper responseWrapper = (SFCC_OrderDetailApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderDetailApi.ResponseWrapper.class
		);
		System.assertEquals(null, responseWrapper.errorMessage);
		System.assertNotEquals(null, responseWrapper.order);
	}

	@IsTest
	static void testGetLegacyOrderDetailRecordNotFound() {
		Order__c order = [SELECT Id FROM Order__c LIMIT 1];
		Id deletedOrderId = order.Id;
		delete order;

		SFCC_OrderDetailApi.RequestWrapper requestWrapper = new SFCC_OrderDetailApi.RequestWrapper(deletedOrderId);
		setRequest(JSON.serialize(requestWrapper));

		Test.startTest();

		SFCC_OrderDetailApi.getDetail();

		Test.stopTest();

		String responseString = RestContext.response.responseBody.toString();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_400, RestContext.response.statusCode, responseString);
		SFCC_OrderDetailApi.ResponseWrapper responseWrapper = (SFCC_OrderDetailApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderDetailApi.ResponseWrapper.class
		);
		System.assertEquals(SFCC_OrderDetailApi.getOrderNotFoundMessage(deletedOrderId), responseWrapper.errorMessage);
		System.assertEquals(null, responseWrapper.order);
	}

	@IsTest
	static void testGetCsOrderDetailRecordNotFound() {
		csord__Order__c csOrder = [SELECT Id FROM csord__Order__c LIMIT 1];
		Id deletedOrderId = csOrder.Id;
		delete csOrder;

		SFCC_OrderDetailApi.RequestWrapper requestWrapper = new SFCC_OrderDetailApi.RequestWrapper(deletedOrderId);
		setRequest(JSON.serialize(requestWrapper));

		Test.startTest();

		SFCC_OrderDetailApi.getDetail();

		Test.stopTest();

		String responseString = RestContext.response.responseBody.toString();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_400, RestContext.response.statusCode, responseString);
		SFCC_OrderDetailApi.ResponseWrapper responseWrapper = (SFCC_OrderDetailApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderDetailApi.ResponseWrapper.class
		);
		System.assertEquals(SFCC_OrderDetailApi.getOrderNotFoundMessage(deletedOrderId), responseWrapper.errorMessage);
		System.assertEquals(null, responseWrapper.order);
	}

	private static void setRequest(String requestBody) {
		RestRequest req = new RestRequest();
		req.requestURI = '/orders/getDetail';
		req.httpMethod = 'POST';
		RestContext.request = req;
		req.requestBody = Blob.valueOf(requestBody);
		RestResponse res = new RestResponse();
		RestContext.response = res;
	}
}
