@isTest
public class CS_AvoidRecursionTest {
    
    private static testMethod void avoidRecursionFirstRun() {
        System.assertEquals(true, CS_AvoidRecursion.isFirstRun());
    }
    
    private static testMethod void avoidRecursionNotFirstRun() {
        CS_AvoidRecursion.isFirstRun();
        System.assertEquals(false, CS_AvoidRecursion.isFirstRun());
    }
}