@isTest
public with sharing class TestOrderMobileAutomation {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		NetProfit_Settings__c settings = TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationOpportunity();

		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, owner);
		opp.Select_Journey__c = 'Sales';
		update opp;
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, claimerContact.id);
		Billing_Arrangement__c bar = new Billing_Arrangement__c(
			Financial_Account__c = fa.Id,
			Billing_Arrangement_Alias__c = '123test',
			Unify_Ref_Id__c = '1234565234'
		);
		insert bar;
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(claimerContact.id, '123321');
		contr.Dealer_Information__c = dealerInfo.Id;
		contr.Unify_Framework_Id__c = '9634563A';
		contr.Signed_by_Customer__c = claimerContact.Id;
		contr.Contract_Duration__c = 24;
		contr.Contract_Activation_Date_Actual_del__c = system.today();
		contr.Contract_Activation_Date__c = system.today();
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Mobile',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			Billing_Arrangement__c = bar.id
		);
		insert order;
		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);

		TestUtils.autoCommit = false;
		VF_Product__c vfp = new VF_Product__c(
			Name = 'tst',
			Family__c = 'tst',
			Product_Line__c = 'fVodafone',
			ExternalID__c = 'VFP-02-1234567',
			ProductCode__c = 'DUMMYADDMOBILEPRODUCT',
			OrderType__c = ot.Id,
			AllowsGroupDataSharing__c = 'YES',
			Unify_Group_Level_Product_CatalogId__c = '3720595',
			Unify_Group_Level_Product_Code__c = 'GL_MASS_MARKET',
			Unify_Group_Lvl_Pricing_Element_Branch__c = 'Shared_Allowance',
			Unify_Group_Lvl_Billing_Offer_CatalogId__c = '8883473',
			Unify_Group_Level_Billing_Offer_Code__c = 'GL_AO_GROUP_DATA_3_BIZ_RED'
		);
		insert vfp;
		List<Product2> products = new List<Product2>();
		products.add(TestUtils.createProduct());
		products[0].VF_Product__c = vfp.Id;
		insert products;
		List<PricebookEntry> pbEntries = new List<PricebookEntry>();

		pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products[0]));

		insert pbEntries;

		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = 4,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Unify_Template_Id__c = '12345678A',
			Product__c = products[0].Id,
			VF_Contract__c = contr.Id,
			Order__c = order.Id
		);
		insert cp;

		NetProfit_CTN__c ctn1 = new NetProfit_CTN__c(
			NetProfit_Information__c = npi.Id,
			Contracted_Product__c = cp.Id,
			Order__c = order.Id,
			Service_Provider_Name__c = 'test Serv',
			Network_Provider_Code__c = 'Test Net',
			Simcard_number_current_provider__c = 'KPN',
			Simcard_number_VF__c = '2763572634275',
			APN__c = 'live.vodafone.com',
			apn2_Text__c = 'FLEXNET',
			apn3_Text__c = 'MP.TNF.NL',
			apn4_Text__c = 'BLGG.NL',
			dataBlock__c = 'Yes',
			outgoingService__c = 'All_outgoing_enabled',
			SMSPremium__c = 'Enabled',
			thirdPartyContent__c = 'Yes',
			premiumDestinations__c = 'No_erotic_destinations',
			roamingspendingLimit__c = '100',
			Contract_Enddate__c = system.today().addDays(1),
			Contract_Number_Current_Provider__c = '2345234',
			Action__c = 'New',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			CTN_Status__c = 'MSISDN Assigned',
			CTN_Number__c = '31612310001'
		);
		insert ctn1;
	}

	@isTest
	public static void testOrderFromTestSetup() {
		Order__c orderRecord = [SELECT Id, VF_Contract__c FROM Order__c LIMIT 1][0];
		Test.startTest();

		Map<Id, OrderWrapper> orderIdToOrderWrapper = new Map<Id, OrderWrapper>();

		for (Order__c o : OrderUtils.getOrderDataByContractId(orderRecord.VF_Contract__c, new Set<id>{})) {
			OrderWrapper ow = new OrderWrapper();
			ow.theOrder = o;
			orderIdToOrderWrapper.put(o.Id, ow);
		}

		Boolean assertActualValue = getReturnBooleanFromTestedMethod(orderIdToOrderWrapper, orderRecord);
		System.assertEquals(false, assertActualValue, 'Meaning Order is Automated');
		Test.stopTest();
	}

	@isTest
	public static void testWhenOrderStatusIsNotAllow() {
		Order__c orderRecord = [SELECT Id, VF_Contract__c FROM Order__c LIMIT 1][0];

		Test.startTest();
		Map<Id, OrderWrapper> orderIdToOrderWrapper = new Map<Id, OrderWrapper>();

		for (Order__c o : OrderUtils.getOrderDataByContractId(orderRecord.VF_Contract__c, new Set<id>{})) {
			OrderWrapper ow = new OrderWrapper();
			ow.theOrder = o;
			//override assignation
			ow.theOrder.Status__c = 'Submitted';
			orderIdToOrderWrapper.put(o.Id, ow);
		}

		Boolean assertActualValue = getReturnBooleanFromTestedMethod(orderIdToOrderWrapper, orderRecord);
		System.assertEquals(false, assertActualValue, 'Meaning Order is Automated');
		Test.stopTest();
	}

	@isTest
	public static void testWhenMDTConfigurationNotExist() {
		Order__c orderRecord = [SELECT Id, VF_Contract__c FROM Order__c LIMIT 1][0];

		Test.startTest();
		Map<Id, OrderWrapper> orderIdToOrderWrapper = new Map<Id, OrderWrapper>();

		for (Order__c o : OrderUtils.getOrderDataByContractId(orderRecord.VF_Contract__c, new Set<id>{})) {
			OrderWrapper ow = new OrderWrapper();
			//override assignation
			o.VF_Contract__r.Opportunity__r.Select_Journey__c = null;
			ow.theOrder = o;
			orderIdToOrderWrapper.put(o.Id, ow);
		}

		Boolean assertActualValue = getReturnBooleanFromTestedMethod(orderIdToOrderWrapper, orderRecord);
		System.assertEquals(false, assertActualValue, 'Meaning Order is Automated');
		Test.stopTest();
	}

	@isTest
	public static void testWhenAccountIsOneNetOrOneFixed() {
		Order__c orderRecord = [SELECT Id, VF_Contract__c FROM Order__c LIMIT 1][0];

		Test.startTest();
		Map<Id, OrderWrapper> orderIdToOrderWrapper = new Map<Id, OrderWrapper>();

		for (Order__c o : OrderUtils.getOrderDataByContractId(orderRecord.VF_Contract__c, new Set<id>{})) {
			OrderWrapper ow = new OrderWrapper();
			//override assignation
			o.Account__r.One_Net_Express__c = true;
			o.Account__r.One_Fixed__c = true;
			ow.theOrder = o;
			orderIdToOrderWrapper.put(o.Id, ow);
		}

		Boolean assertActualValue = getReturnBooleanFromTestedMethod(orderIdToOrderWrapper, orderRecord);
		System.assertEquals(false, assertActualValue, 'Meaning Order is Automated');
		Test.stopTest();
	}

	@isTest
	public static void testWhenOrderCanBeAutomated() {
		Order__c orderRecord = [SELECT Id, VF_Contract__c FROM Order__c LIMIT 1][0];

		Test.startTest();
		Map<Id, OrderWrapper> orderIdToOrderWrapper = new Map<Id, OrderWrapper>();

		for (Order__c o : OrderUtils.getOrderDataByContractId(orderRecord.VF_Contract__c, new Set<id>{})) {
			OrderWrapper ow = new OrderWrapper();
			//override assignation
			o.VF_Contract__r.Opportunity__r.Segment__c = 'SME IDS';
			o.VF_Contract__r.Opportunity__r.Select_Journey__c = 'Add Mobile Product';
			ow.theOrder = o;
			orderIdToOrderWrapper.put(o.Id, ow);
		}

		Boolean assertActualValue = getReturnBooleanFromTestedMethod(orderIdToOrderWrapper, orderRecord);
		System.assertEquals(true, assertActualValue, 'Meaning Order is NOT Automated');
		Test.stopTest();
	}

	@isTest
	public static void testWhenContratedProductAreNotValidForAutomation() {
		Order__c orderRecord = [SELECT Id, VF_Contract__c FROM Order__c LIMIT 1][0];

		Test.startTest();
		Map<Id, OrderWrapper> orderIdToOrderWrapper = new Map<Id, OrderWrapper>();

		for (Order__c o : OrderUtils.getOrderDataByContractId(orderRecord.VF_Contract__c, new Set<id>{})) {
			OrderWrapper ow = new OrderWrapper();
			//override assignation
			o.VF_Contract__r.Opportunity__r.Segment__c = 'SME IDS';
			// o.VF_Contract__r.Opportunity__r.Select_Journey__c = 'Add Mobile Product';
			ow.theOrder = o;
			orderIdToOrderWrapper.put(o.Id, ow);
		}

		OrderWrapper ow = orderIdToOrderWrapper.get(orderRecord.Id);
		List<Contracted_Products__c> listOfContractedProducts = orderUtils.getContractIdToContractedProducts(null, orderIdToOrderWrapper.keySet())
			.get(ow.theOrder.Id);
		//Override Value of CLC and Product Group
		listOfContractedProducts.get(0).CLC__c = 'Ret';
		listOfContractedProducts.get(0).Product__r.Product_Group__c = 'Other';
		OrderMobileAutomation classInstanceOMA = new OrderMobileAutomation();
		classInstanceOMA.init(ow, listOfContractedProducts);
		ow.theOrder.EMP_Automated_Mobile_order__c = classInstanceOMA.canMobileOrderdBeAutomated();

		Boolean assertActualValue = getReturnBooleanFromTestedMethod(orderIdToOrderWrapper, orderRecord);
		System.assertEquals(false, assertActualValue, 'Meaning Order is NOT Automated');
		Test.stopTest();
	}

	private static Boolean getReturnBooleanFromTestedMethod(Map<Id, OrderWrapper> orderIdToOrderWrapper, Order__c orderRecord) {
		OrderWrapper ow = orderIdToOrderWrapper.get(orderRecord.Id);
		List<Contracted_Products__c> listOfContractedProducts = orderUtils.getContractIdToContractedProducts(null, orderIdToOrderWrapper.keySet())
			.get(ow.theOrder.Id);
		OrderMobileAutomation classInstanceOMA = new OrderMobileAutomation();
		classInstanceOMA.init(ow, listOfContractedProducts);
		ow.theOrder.EMP_Automated_Mobile_order__c = classInstanceOMA.canMobileOrderdBeAutomated();

		return ow.theOrder.EMP_Automated_Mobile_order__c;
	}
}
