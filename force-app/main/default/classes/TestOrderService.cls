@isTest
public with sharing class TestOrderService {
	@isTest
	public static void testGetOpportunityNumberWithOrderId() {
		TestUtils.autoCommit = true;
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, null);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		Order__c ord = TestUtils.createOrder(contr);

		Test.startTest();
		String result = OrderService.getOpportunityNumberWithOrderId([SELECT Id, Name FROM Order__c WHERE Id = :ord.Id].Name);
		Test.stopTest();

		System.assertEquals(
			[SELECT Id, Opportunity_Number__c FROM Opportunity WHERE id = :opp.Id]
			.Opportunity_Number__c,
			result,
			'The method should return related Opportunities Opportunity_Number__c'
		);
	}

	@isTest
	public static void testOrderLegacy() {
		Test.startTest();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.createOrderValidationOrder();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, null);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Contact con = TestUtils.createContact(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		//create OrderType
		TestUtils.createOrderType();
		//create site
		Site__c site = TestUtils.createSite(acct);

		TestUtils.autoCommit = false;
		List<Product2> lstProdToInsert = new List<Product2>();
		Product2 product = TestUtils.createProduct(new Map<String, Object>{ 'ProductCode' => 'C112312' });
		Product2 product1 = TestUtils.createProduct(new Map<String, Object>{ 'ProductCode' => 'C113309' });
		Product2 productTwo = TestUtils.createProduct(new Map<String, Object>{ 'ProductCode' => 'C113317' });
		lstProdToInsert.add(product);
		lstProdToInsert.add(product1);
		lstProdToInsert.add(productTwo);
		insert lstProdToInsert;
		TestUtils.autoCommit = true;

		Order__c ord = TestUtils.createOrder(contr);

		List<Contracted_Products__c> cpInsert = new List<Contracted_Products__c>();
		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = 1,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Product__c = lstProdToInsert[0].Id,
			VF_Contract__c = contr.Id,
			Site__c = site.Id,
			Order__c = ord.Id,
			Proposition_Component__c = 'Voice; Internet; IPVPN',
			Proposition__c = 'One Net',
			Family_Condition__c = 'ONENETX2014,ONENET2014,ONENET2014SUB30,One Net Enterprise,One Net Express',
			Number_of_sessions__c = 2
		);
		cpInsert.add(cp);
		Contracted_Products__c cp1 = new Contracted_Products__c(
			Quantity__c = 1,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Product__c = lstProdToInsert[1].Id,
			VF_Contract__c = contr.Id,
			Site__c = site.Id,
			Order__c = ord.Id,
			Proposition_Component__c = 'Voice; Internet; IPVPN',
			Proposition__c = 'IPVPN',
			Number_of_sessions__c = 2
		);
		cpInsert.add(cp1);
		Contracted_Products__c cp2 = new Contracted_Products__c(
			Quantity__c = 1,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Product__c = lstProdToInsert[2].Id,
			VF_Contract__c = contr.Id,
			Site__c = site.Id,
			Order__c = ord.Id,
			Proposition_Component__c = 'Voice; Internet; IPVPN',
			Proposition__c = 'One Net',
			Family_Condition__c = 'ONENET2014SUB30,One Net Express',
			Number_of_sessions__c = 2
		);
		cpInsert.add(cp2);

		insert cpInsert;

		ord.Export__c = 'BOP';
		ord.Propositions__c = 'Legacy';
		ord.Customer_Main_Contact__c = con.Id;
		ord.Status__c = 'Clean';
		ord.Billing_Arrangement__c = bar.Id;
		update ord;

		Test.stopTest();
		//query info to assert
		String strAssertTitle = [SELECT Title__c FROM Order__c WHERE Id = :ord.Id LIMIT 1]?.Title__c;
		System.assertEquals(true, string.isNotEmpty(strAssertTitle), 'validating that the Order gets a title for the VF calling information');
	}
}
