@IsTest
public with sharing class TestOrderEntryValidityService {
	@TestSetup
	static void makeData() {
		OrderEntryTestDataFactory.createOpportunityWithAllRelatedRecords();
	}

	@isTest
	static void testValidatePeriod() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

		List<OE_Product__c> products = OrderEntryTestDataFactory.createOrderEntryProducts();
		List<OE_Promotion__c> promotions = OrderEntryTestDataFactory.createPromotions();

		Test.startTest();
		Set<Id> productsReturnList = OrderEntryValidityService.validatePeriod(opp.Id, products);
		Set<Id> promotionReturnList = OrderEntryValidityService.validatePeriod(opp.Id, promotions);
		Test.stopTest();

		System.assertEquals(productsReturnList.size(), 4, 'Four product should be returned.');
		System.assertEquals(promotionReturnList.size(), 3, 'Three promotion should be returned.');
	}
}
