public class UserManagementController {
	@TestVisible
	public class Info{
		public String name {get;set;}
		public Boolean check {get;set;}
		public String comment {get;set;}
		public User u {get;set;}
		public String role {get;set;}

		public Info(User u){
			this.u = u;
			this.name = u.Name;
			this.check = u.IsActive;
			this.comment = '';
			this.role = u.UserRole.Name;
		}
	}
	public String additionalComments{get;set;}

	public list<Info> infoList {get;set;}

	public List<Info> getInfos() {
		if(infoList == null) {
			infoList = new List<Info>();
			for (User u : [
				SELECT Id, Name, IsActive, UserRole.Name
				FROM User
				WHERE ManagerId = :UserInfo.getUserId()
			]) {
				if(u.IsActive == true){
					infoList.add(new Info(u));
				}
			}
		}
		return infoList;
	}
	
	public UserManagementController() {
	}

	public Pagereference send(){
		OrgWideEmailAddress orgWideEmailAddress = EmailService.getDefaultOrgWideEmailAddress();

		String overview = 'Overzicht van ' + UserInfo.getName() + '<br/><br/>';
		for(Info i : infoList){
			overview +=
				i.u.Id +
				'     ' +
				i.Check +
				'     ' +
				i.Name +
				'     ' +
				i.Role +
				'     ' +
				i.Comment +
				'<br/>';
		}
		overview +=
			'<br />Additional Comments (new employees, etc.): ' +
			additionalComments +
			'<br/>';

		Blob body = Blob.toPdf(overview);

		Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
		attach.setContentType('application/pdf');
		attach.setFileName('Useroverview '+UserInfo.getName()+'.pdf');
		attach.setInline(false);
		attach.Body = body;

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setUseSignature(false);
		mail.setToAddresses(
			new List<String>{ 'eelco.devries@vodafone.com', 'laurens.ubert@vodafone.com' }
		);
		mail.setSubject('User overview of '+ UserInfo.getName());
		mail.setHtmlBody('Overview of the users of ' + UserInfo.getName());
		mail.setFileAttachments(new List<Messaging.EmailFileAttachment>{ attach });

		if (orgWideEmailAddress != null) {
			mail.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
		}

		if (EmailService.checkDeliverability()) {
			Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ mail });
		}

		return new PageReference('/');
	}
}