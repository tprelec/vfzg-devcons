public with sharing class CS_OrchestratorStepUtility {
	public static void updateTasksAndPauseSteps(List<Task> tasks, Map<Id, CSPOFA__Orchestration_Step__c> deliveryOrderStepMap) {
		List<Task> tasksToBeUpdated = new List<Task>();
		Set<CSPOFA__Orchestration_Step__c> stepsToBePutOnHold = new Set<CSPOFA__Orchestration_Step__c>();

		for (Task taskRecord : tasks) {
			if (taskRecord.CSPOFA__Orchestration_Step__c != null) {
				stepsToBePutOnHold.add(getStepForOnHold(taskRecord.CSPOFA__Orchestration_Step__c, true));
				tasksToBeUpdated.add(
					getTasksForClosing(
						taskRecord.Id,
						deliveryOrderStepMap.get(taskRecord.CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c)
							.COM_Task_Status__c
					)
				);
			}
		}

		if (!stepsToBePutOnHold.isEmpty()) {
			updateSteps(stepsToBePutOnHold);
		}

		if (!tasksToBeUpdated.isEmpty()) {
			updateTasks(tasksToBeUpdated);
		}
	}

	private static void updateSteps(Set<CSPOFA__Orchestration_Step__c> stepSet) {
		List<CSPOFA__Orchestration_Step__c> stepsToUpdate = new List<CSPOFA__Orchestration_Step__c>();
		stepsToUpdate.addAll(stepSet);
		update stepsToUpdate;
	}

	private static void updateTasks(List<Task> tasksToBeUpdated) {
		Set<Task> taskSet = new Set<Task>();
		taskSet.addAll(tasksToBeUpdated);
		List<Task> updateTaskList = new List<Task>();
		updateTaskList.addAll(taskSet);
		update updateTaskList;
	}

	private static CSPOFA__Orchestration_Step__c getStepForOnHold(Id stepId, Boolean onHold) {
		CSPOFA__Orchestration_Step__c returnValue = new CSPOFA__Orchestration_Step__c();
		returnValue.Id = stepId;
		//returnValue.CSPOFA__Step_On_Hold__c = onHold;
		returnValue.CSPOFA__Related_Object_ID__c = '';
		return returnValue;
	}

	private static Task getTasksForClosing(Id taskId, String status) {
		Task returnValue = new Task();
		returnValue.Id = taskId;
		returnValue.CSPOFA__Orchestration_Step__c = null;
		returnValue.Status = status;
		returnValue.Delivery_Task_Finished__c = true;
		return returnValue;
	}

	public static List<CSPOFA__Orchestration_Step__c> getStepList(List<SObject> steps) {
		return [
			SELECT
				ID,
				Name,
				CSPOFA__Orchestration_Process__c,
				CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c,
				CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c,
				CSPOFA__Orchestration_Process__r.Name,
				CSPOFA__Status__c,
				CSPOFA__Completed_Date__c,
				CSPOFA__Message__c,
				CSPOFA__Expression_Result__c,
				CSPOFA__Related_Object__c,
				CSPOFA__Related_Object_ID__c,
				COM_Task_Status__c,
				COM_MACD_Type__c,
				COM_Provisioning_Queue__c
			FROM CSPOFA__Orchestration_Step__c
			WHERE Id IN :steps
		];
	}

	// Called in custom Orchestrator step classes after a successful execution to edit fields accordingly.
	public static CSPOFA__Orchestration_Step__c setStepToComplete(CSPOFA__Orchestration_Step__c step) {
		step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_COMPLETE;
		step.CSPOFA__Completed_Date__c = Date.today();
		step.CSPOFA__Message__c = 'Custom step succeeded';

		return step;
	}

	// Called in custom Orchestrator step classes after a failed execution to edit fields accordingly.
	public static CSPOFA__Orchestration_Step__c setStepToError(CSPOFA__Orchestration_Step__c step, Exception ex) {
		step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
		step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();

		return step;
	}

	public static String generateRandomString(Integer len) {
		final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
		String randStr = '';
		while (randStr.length() < len) {
			Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
			randStr += chars.substring(idx, idx + 1);
		}
		return randStr;
	}
}
