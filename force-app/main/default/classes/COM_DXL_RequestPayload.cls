public class COM_DXL_RequestPayload {
	public CustomerDetails customerDetails;
	public BillingEntities billingEntities;
	public Contacts[] contacts;
	public Sites[] sites;

	class CustomerDetails {
		public CompanyDetails companyDetails;
		public BillingCustomer billingCustomer;
		public CompanyAddress companyAddress;
		public AccountManager[] accountManager;
	}
	class CompanyDetails {
		public String unifyAccountRefId; //113400922
		public String kvk; //20121191
		public String establishDate; //2019-12-04
		public String legalEntity; //01
		public String numOfEmployees; //100
		public String companyName; //AdministratiekantoorMaxximaal ITE2
		public String accountType; //C
		public String accountSubType; //CRP
		public String fax; //
		public String phone; //0612345678
		public String dunsNo; //414253695
		public String bopCompanyCode; //
	}
	class BillingCustomer {
		public String unifyBillingCustomerRefId; //
		public String banNumber; //
		public String customerName; //AdministratiekantoorMaxximaal ITE2
		public String customerType; //C
		public String customerSubType; //GDS
	}
	class CompanyAddress {
		public String postalCode; //4708NN
		public String houseNumber; //128
		public String houseNumberExtension; //
		public String street; //Meesberg
		public String city; //ROOSENDAAL
		public String country; //NL
	}
	class AccountManager {
		public String userType; //
		public String name; //
		public String managerName; //
		public String bopCode; //
		public String type; //mobile_dealer
	}
	class BillingEntities {
		public String paymentMethod; //Invoice
		public String financialAccountName; //RealtechIC Primary von Demo User
		public String unifyFinancialAccountRefId; //
		public String unifyBillingAccountRefId; //12343
		public String billFormat; //EB
		public String billingArrangementName; //test
		public PayMeans payMeans;
		public InvoiceAddress invoiceAddress;
	}
	class PayMeans {
		public String bankAccountName; //
		public String bankAccountNumber; //
	}
	class InvoiceAddress {
		public String street; //test
		public String houseNumber; //2
		public String houseNumberExtension; //
		public String postalCode; //2595AW
		public String city; //test
		public String country; //NL
	}
	class Contacts {
		public String unifyContactRefID; //TEST EXTERNAL ID
		public String email; //juan.cardona@vodafoneziggo.com
		public String roleType; //director
		public String primaryPhone; //0612345678
		public String salesforceAccountId; //0013O00000VLejmQAD
		public String salesforceContactId; //0033O00000Uk4GbQAJ
		public Address address;
		public PersonData personData;
	}
	class Address {
		public String street; //5611KW
		public String houseNumber; //951
		public String houseNumberExtension; //
		public String postalCode; //5611KW
		public String city; //EINDHOVEN
		public String country; //NL
	}
	class PersonData {
		public String birthDate; //
		public String firstName; //Administra
		public String lastName; //ITE2
		public String phoneNumber; //0612345678
	}
	class Sites {
		public String unifySiteRefId; //
		public String name; //
		public String unifySitename; //EINDHOVEN, Don Boscostraat, 951  - 5611KW
		public String salesforceSiteID; //a063O000003vNFmQAM
		public String slaCode; //
		public String locationTypeName; //sub
		public String phone; //040  2193344
		public String buildingName; //
		public Address address;
		public CanvasAddress canvasAddress;
	}
	class CanvasAddress {
		public String street; //
		public String houseNumber; //
		public String houseNumberExtension; //
		public String postalCode; //
		public String city; //
	}
	public static COM_DXL_RequestPayload parse(String json) {
		return (COM_DXL_RequestPayload) System.JSON.deserialize(json, COM_DXL_RequestPayload.class);
	}
}
