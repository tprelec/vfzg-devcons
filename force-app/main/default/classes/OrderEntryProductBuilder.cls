/**
 * Builds Opportunity Products for selected Bundles
 */
public with sharing class OrderEntryProductBuilder {
	private static final String SEGMENT_SOHO = 'SoHo';
	private static final String RECURRING = 'Recurring';
	private static final String ONE_OFF = 'One Off';
	private static final String CONTRACT_LABEL = ' maand contract';
	private static final String ORDER_ENTRY_TOOL = 'Order Entry Tool';

	OrderEntryData data;
	Map<String, PricebookEntry> pricebookEntriesMap;
	List<BundleProductBuilder> bundleBuilders;

	public OrderEntryProductBuilder(OrderEntryData data) {
		this.data = data;
		this.getPricebookEntries();
	}

	/**
	 * @description Sets pricebook entries for selected bundles and addons based on Product Code.
	 *              Sets pricebook entries for selected discounts based on Promo Name.
	 *              Creates new Promo Products and Pricebook entries if they don't exist.
	 */
	private void getPricebookEntries() {
		this.pricebookEntriesMap = new Map<String, PricebookEntry>();
		Set<String> productCodes = new Set<String>();
		Set<String> promoNames = new Set<String>();
		for (OrderEntryData.OrderEntryBundle bundle : this.data.bundles) {
			if (bundle.bundle != null) {
				productCodes.add(bundle.bundle.code);
			}
			if (bundle.addons != null) {
				for (OrderEntryData.OrderEntryAddon addon : bundle.addons) {
					productCodes.add(addon.code);
				}
			}
			if (bundle.promos != null) {
				for (OrderEntryData.Promotion promo : bundle.promos) {
					promoNames.add(promo.name);
				}
			}
		}
		List<PricebookEntry> entries = [
			SELECT Id, Product2.ProductCode, Product2.Name, Product2Id
			FROM PricebookEntry
			WHERE IsActive = TRUE AND (Product2.ProductCode IN :productCodes OR Product2.Name IN :promoNames)
		];
		for (PricebookEntry entry : entries) {
			if (productCodes.contains(entry.Product2.ProductCode)) {
				this.pricebookEntriesMap.put(entry.Product2.ProductCode, entry);
			} else {
				this.pricebookEntriesMap.put(entry.Product2.Name, entry);
			}
		}
		this.createMissingPromoProducts(promoNames);
	}

	/**
	 * @description Checks if all promos have matching Products and Pricebook Entries.
	 *              If missing, creates new Products and Pricebook Entries
	 * @param  promoNames Names of all used Promos.
	 */
	private void createMissingPromoProducts(Set<String> promoNames) {
		Set<String> missingPromoNames = new Set<String>();
		for (String promoName : promoNames) {
			if (!this.pricebookEntriesMap.containsKey(promoName)) {
				missingPromoNames.add(promoName);
			}
		}
		if (missingPromoNames.isEmpty()) {
			return;
		}

		// Create Products
		Id ziggoOrderTypeId = [SELECT Id FROM OrderType__c WHERE Name = 'Ziggo'][0].Id;
		List<Product2> newPromoProducts = new List<Product2>();
		for (String promoName : missingPromoNames) {
			newPromoProducts.add(
				new Product2(Name = promoName, Family = 'Zakelijk Internet', OrderType__c = ziggoOrderTypeId, Product_Line__c = 'fZiggo Only')
			);
		}
		SharingUtils.insertRecordsWithoutSharing(newPromoProducts);

		// Create Pricebook Entries
		Id standardPricebookId = [SELECT Id FROM Pricebook2 WHERE Name = 'Standard Price Book'][0].Id;
		List<PricebookEntry> newPricebookEntries = new List<PricebookEntry>();
		for (Product2 promoProduct : newPromoProducts) {
			PricebookEntry pbe = new PricebookEntry(Pricebook2Id = standardPricebookId, Product2Id = promoProduct.Id, IsActive = true, UnitPrice = 0);
			newPricebookEntries.add(pbe);
			this.pricebookEntriesMap.put(promoProduct.Name, pbe);
		}
		SharingUtils.insertRecordsWithoutSharing(newPricebookEntries);
	}

	/**
	 * @description Deletes existing Opportunity Products and creates new ones
	 */
	public void build() {
		this.buildOlis();
		this.buildProductDetails();
	}

	/**
	 * @description Deletes existing Opportunity Products and creates new ones
	 */
	private void buildOlis() {
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);

		this.deleteOldOlis();

		// For each Bundle build new Products
		this.bundleBuilders = new List<BundleProductBuilder>();
		List<OpportunityLineItem> bundleOlis = new List<OpportunityLineItem>();
		for (OrderEntryData.OrderEntryBundle bundle : this.data.bundles) {
			BundleProductBuilder builder = new BundleProductBuilder(
				this.data.opportunityId,
				this.data.sites[0].siteId,
				bundle,
				this.pricebookEntriesMap
			);
			builder.build();
			this.bundleBuilders.add(builder);
			bundleOlis.add(builder.bundleOli);
		}

		// Insert Bundle OLIs
		insert bundleOlis;

		// Link Addon and Promo OLIs with Parent
		List<OpportunityLineItem> childOlis = new List<OpportunityLineItem>();
		for (BundleProductBuilder builder : this.bundleBuilders) {
			for (OpportunityLineItem addonOli : builder.addonOlis) {
				addonOli.Parent_Product__c = builder.bundleOli.Id;
				childOlis.add(addonOli);
			}
			for (OpportunityLineItem promoOli : builder.promoOlis) {
				promoOli.Parent_Product__c = builder.bundleOli.Id;
				childOlis.add(promoOli);
			}
		}

		// Insert Add On Olis
		if (!childOlis.isEmpty()) {
			insert childOlis;
		}
	}

	/**
	 * Deletes existing Opportunity Products (if they exist)
	 */
	private void deleteOldOlis() {
		List<OpportunityLineItem> olis = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :this.data.opportunityId];
		if (!olis.isEmpty()) {
			delete olis;
		}
	}

	/**
	 * @description Deletes existing Product Details and creates new ones
	 */
	private void buildProductDetails() {
		this.deleteOldProductDetails();

		// Get details related to selected Products and Add Ons
		List<Order_Entry_Product_Detail__c> orderEntryProductDetails = this.getOrderEntryProductDetails();
		Map<Id, List<Order_Entry_Product_Detail__c>> detailsByProductId = GeneralUtils.groupByIDField(orderEntryProductDetails, 'Product__c');
		Map<Id, List<Order_Entry_Product_Detail__c>> detailsByAddonId = GeneralUtils.groupByIDField(orderEntryProductDetails, 'Add_On__c');

		// Create new Product Details
		List<LG_ProductDetail__c> newProductDetails = new List<LG_ProductDetail__c>();
		for (BundleProductBuilder bundleBuilder : this.bundleBuilders) {
			// Build Bundle Details
			for (OrderEntryData.OrderEntryProduct product : bundleBuilder.bundle.products) {
				List<Order_Entry_Product_Detail__c> oeProductDetails = detailsByProductId.get(product.id);
				if (oeProductDetails == null) {
					continue;
				}
				for (Order_Entry_Product_Detail__c oeProductDetail : oeProductDetails) {
					LG_ProductDetail__c prodDetail = new LG_ProductDetail__c(
						LG_Opportunity__c = this.data.opportunityId,
						LG_Name__c = oeProductDetail.Name,
						LG_Product__c = oeProductDetail.Category__c,
						LG_Type__c = oeProductDetail.Type__c,
						LG_Value__c = oeProductDetail.Value__c,
						Opportunity_Product__c = bundleBuilder.bundleOli.Id
					);
					newProductDetails.add(prodDetail);
				}
			}
			// Build Addon Details
			for (OrderEntryData.OrderEntryAddon addOn : bundleBuilder.bundle.addons) {
				List<Order_Entry_Product_Detail__c> addonOEProductDetails = detailsByAddonId.get(addon.id);
				if (addonOEProductDetails == null) {
					continue;
				}
				for (Order_Entry_Product_Detail__c oeProductDetail : addonOEProductDetails) {
					LG_ProductDetail__c prodDetail = new LG_ProductDetail__c(
						LG_Opportunity__c = this.data.opportunityId,
						LG_Name__c = oeProductDetail.Name,
						LG_Product__c = oeProductDetail.Category__c,
						LG_Type__c = oeProductDetail.Type__c,
						LG_Value__c = oeProductDetail.Value__c,
						Opportunity_Product__c = bundleBuilder.addonToOliMap.get(addOn).Id
					);
					newProductDetails.add(prodDetail);
				}
			}
		}
		if (!newProductDetails.isEmpty()) {
			insert newProductDetails;
		}
	}

	/**
	 * Deletes existing Opportunity Products (if they exist)
	 */
	private void deleteOldProductDetails() {
		List<LG_ProductDetail__c> oldDetails = [SELECT Id FROM LG_ProductDetail__c WHERE LG_Opportunity__c = :this.data.opportunityId];
		if (!oldDetails.isEmpty()) {
			delete oldDetails;
		}
	}

	/**
	 * @description Gets Order Entry Product Details based on selected Products and Add Ons
	 * @return   Order Entry Product Details
	 */
	private List<Order_Entry_Product_Detail__c> getOrderEntryProductDetails() {
		Set<Id> productAndAddonIds = new Set<Id>();
		for (OrderEntryData.OrderEntryBundle bundle : this.data.bundles) {
			for (OrderEntryData.OrderEntryProduct product : bundle.products) {
				productAndAddonIds.add(product.id);
			}
			for (OrderEntryData.OrderEntryAddon addon : bundle.addons) {
				productAndAddonIds.add(addon.id);
			}
		}
		return [
			SELECT Name, Category__c, Type__c, Value__c, Product__c, Add_On__c
			FROM Order_Entry_Product_Detail__c
			WHERE Product__c IN :productAndAddonIds OR Add_On__c IN :productAndAddonIds
		];
	}

	private class BundleProductBuilder {
		private Id oppId;
		private Id siteId;
		private OrderEntryData.OrderEntryBundle bundle;
		private Integer contractTerm;
		private List<OrderEntryData.Discount> discounts;
		private Map<String, PricebookEntry> pricebookEntriesMap;

		public OpportunityLineItem bundleOli;
		public List<OpportunityLineItem> addonOlis;
		public List<OpportunityLineItem> promoOlis;
		public Map<OrderEntryData.OrderEntryAddon, OpportunityLineItem> addonToOliMap;

		@SuppressWarnings('PMD.ExcessiveParameterList')
		public BundleProductBuilder(Id oppId, Id siteId, OrderEntryData.OrderEntryBundle bundle, Map<String, PricebookEntry> pricebookEntriesMap) {
			this.oppId = oppId;
			this.siteId = siteId;
			this.bundle = bundle;
			this.contractTerm = Integer.valueOf(this.bundle.contractTerm);
			this.discounts = new List<OrderEntryData.Discount>();
			if (this.bundle.promos != null) {
				for (OrderEntryData.Promotion promotion : this.bundle.promos) {
					if (this.bundle.type == 'Mobile' && promotion.customerType == 'Existing') {
						// for mobile bundles we skip adding existing promotions
						continue;
					}
					this.discounts.addAll(promotion.discounts);
				}
			}
			this.pricebookEntriesMap = pricebookEntriesMap;
		}

		/**
		 * @description Builds all Opportunity Products (Bundle, Addon, Promo)
		 */
		public void build() {
			this.buildBundleOli();
			this.buildAddonOlis();
			this.buildPromoOlis();
		}

		/**
		 * @description Builds Bundle Opportunity Product
		 */
		private void buildBundleOli() {
			this.bundleOli = new OpportunityLineItem(
				OpportunityId = this.oppId,
				Quantity = this.bundle.bundle.quantity,
				UnitPrice = this.bundle.bundle.recurring,
				SIM_Card__c = this.bundle.simCard,
				Installation_Status__c = 'New',
				LG_MainOLI__c = true,
				LG_OneOffPrice__c = this.bundle.bundle.oneOff == null ? null : this.bundle.bundle.oneOff * this.bundle.bundle.quantity,
				LG_ProductDetail__c = String.valueOf(this.contractTerm * 12) + CONTRACT_LABEL,
				LG_Segment__c = SEGMENT_SOHO,
				LG_Type__c = (this.bundle.bundle.oneOff != null && this.bundle.bundle.oneOff != 0) ? ONE_OFF : RECURRING,
				Location__c = this.siteId,
				LG_ContractTerm__c = this.contractTerm == 0 ? 1 : this.contractTerm * 12,
				PricebookEntryId = this.pricebookEntriesMap.get(this.bundle.bundle.code)?.Id,
				Product_Bundle_Name__c = this.bundle.bundle.bundleName,
				Promotion_Message__c = this.bundle.bundle.promoDescription,
				Source_System__c = ORDER_ENTRY_TOOL
			);

			List<OrderEntryData.Discount> appliedDiscounts = new List<OrderEntryData.Discount>();
			for (OrderEntryData.Discount discount : this.discounts) {
				if (discount.level == 'Bundle') {
					appliedDiscounts.add(discount);
				}
			}
			this.calculateOliDiscount(this.bundleOli, appliedDiscounts);
		}

		/**
		 * @description Builds Addon Opportunity Products
		 */
		private void buildAddonOlis() {
			this.addonOlis = new List<OpportunityLineItem>();
			this.addonToOliMap = new Map<OrderEntryData.OrderEntryAddon, OpportunityLineItem>();
			for (OrderEntryData.OrderEntryAddon addon : this.bundle.addons) {
				if (addon.quantity > 0) {
					OpportunityLineItem oli = new OpportunityLineItem(
						OpportunityId = this.oppId,
						Quantity = addon.quantity,
						UnitPrice = addon.recurring == null ? 0 : addon.recurring,
						LG_MainOLI__c = false,
						LG_OneOffPrice__c = addon.oneOff == null ? null : addon.quantity * addon.oneOff,
						LG_ProductDetail__c = String.valueOf(this.contractTerm * 12) + CONTRACT_LABEL,
						LG_Segment__c = SEGMENT_SOHO,
						LG_Type__c = (addon.oneOff != null && addon.oneOff != 0) ? ONE_OFF : RECURRING,
						Location__c = this.siteId,
						LG_ContractTerm__c = this.contractTerm * 12,
						PricebookEntryId = this.pricebookEntriesMap.get(addon.code)?.Id,
						Source_System__c = ORDER_ENTRY_TOOL
					);

					List<OrderEntryData.Discount> appliedDiscounts = new List<OrderEntryData.Discount>();
					for (OrderEntryData.Discount discount : this.discounts) {
						if (discount.level == 'Add On' && discount.addon == addon.id) {
							appliedDiscounts.add(discount);
						}
					}
					this.calculateOliDiscount(oli, appliedDiscounts);
					this.addonOlis.add(oli);
					this.addonToOliMap.put(addon, oli);
				}
			}
		}

		/**
		 * @description Builds Addon Opportunity Products
		 */
		private void buildPromoOlis() {
			this.promoOlis = new List<OpportunityLineItem>();
			if (this.bundle.promos == null) {
				return;
			}
			for (OrderEntryData.Promotion promo : this.bundle.promos) {
				OpportunityLineItem oli = new OpportunityLineItem(
					OpportunityId = this.oppId,
					Quantity = 1,
					UnitPrice = 0,
					LG_MainOLI__c = false,
					LG_OneOffPrice__c = 0,
					LG_Segment__c = SEGMENT_SOHO,
					LG_Type__c = promo.type == 'Standard' ? 'Promotional Action' : 'Closing Deal',
					LG_ContractTerm__c = Decimal.valueOf(bundle.contractTerm),
					Product_Bundle_Name__c = promo.name,
					Duration__c = getPromoDuration(promo),
					PricebookEntryId = this.pricebookEntriesMap.get(promo.name)?.Id,
					Source_System__c = ORDER_ENTRY_TOOL
				);
				this.promoOlis.add(oli);
			}
		}

		/**
		 * @description Gets Max Duration of the Promo based on duration of all related discounts
		 * @param  promo Promotion
		 * @return       Promotion Duration
		 */
		private Decimal getPromoDuration(OrderEntryData.Promotion promo) {
			Decimal duration = 0;
			for (OrderEntryData.Discount discount : promo.discounts) {
				if (discount.duration > duration) {
					duration = discount.duration;
				}
			}
			return duration;
		}

		/**
		 * @description Calculates Discuont for Bundle or Addon Opprotunity Product
		 * @param  oli              Bundle/Addon Opportunity Product
		 * @param  appliedDiscounts Discounts Applied to the Opportunity Product
		 */
		private void calculateOliDiscount(OpportunityLineItem oli, List<OrderEntryData.Discount> appliedDiscounts) {
			String discountMessage = '';
			Decimal recurringDiscount = 0;
			Decimal oneOffDiscount = 0;

			for (OrderEntryData.Discount discount : appliedDiscounts) {
				discountMessage += discount.name + '\n';
				recurringDiscount += calculateDiscount(oli.UnitPrice, discount.value, discount.type);
				oneOffDiscount += calculateDiscount(oli.LG_OneOffPrice__c, discount.oneOffValue, discount.type);
			}

			if (oli.UnitPrice != null && recurringDiscount > oli.UnitPrice) {
				recurringDiscount = oli.UnitPrice;
			}
			if (oli.LG_OneOffPrice__c != null && oneOffDiscount > oli.LG_OneOffPrice__c) {
				oneOffDiscount = oli.LG_OneOffPrice__c;
			}

			oli.Discount_Recurring__c = recurringDiscount;
			oli.Discount_One_Off__c = oneOffDiscount;
			oli.First_Month_Price__c = oli.Quantity * (oli.UnitPrice - recurringDiscount);
			oli.Discount_Messages__c = discountMessage;
		}

		private Decimal calculateDiscount(Decimal price, Decimal discountValue, String discountType) {
			if (price == null || discountValue == null) {
				return 0;
			}
			Decimal discount = 0;
			if (discountType == 'Percentage') {
				discount = price * discountValue / 100;
			} else if (discountType == 'Discounted Amount') {
				discount = discountValue;
			} else if (discountType == 'Amount') {
				discount = price - discountValue;
			}
			return discount;
		}
	}
}
