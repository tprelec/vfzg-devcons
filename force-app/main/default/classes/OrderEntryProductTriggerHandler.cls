public with sharing class OrderEntryProductTriggerHandler extends TriggerHandler {
	private List<OE_Product__c> newProducts;
	private Map<Id, OE_Product__c> newProductsMap;
	private Map<Id, OE_Product__c> oldProductsMap;

	private void init() {
		newProducts = (List<OE_Product__c>) this.newList;
		newProductsMap = (Map<Id, OE_Product__c>) this.newMap;
		oldProductsMap = (Map<Id, OE_Product__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	public void setExternalIds() {
		Sequence seq = new Sequence('OE_Product');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (OE_Product__c p : newProducts) {
				p.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}