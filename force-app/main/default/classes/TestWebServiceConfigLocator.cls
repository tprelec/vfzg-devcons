/**
 * @description			This is the test class that contains the tests for the TestWebServiceConfigLocator
 * @author				Ferdinand Bondt
 */

@isTest
private class TestWebServiceConfigLocator {
	@isTest
	private static void creatingNewInstanceOfConfigShouldNotBeNull() {
		System.assertNotEquals(null, WebServiceConfigLocator.createConfig(), 'A new instance of a webservice config object should have been created');
	}

	@isTest
	private static void shouldThrowExceptionIfDataSetNameNull() {
		try {
			WebServiceConfigLocator.getConfig(null);
			System.assert(false, 'Exception should have been thrown');
		} catch (ExMissingDataException ex) {
			System.assert(true, 'An exception should not have been thrown');
		}
	}

	@isTest
	private static void shouldThrowExceptionIfInvalidDataSetProvided() {
		try {
			WebServiceConfigLocator.getConfig('invalid');
			System.assert(false, 'An exception should have been thrown');
		} catch (ExMissingDataException ex) {
			System.assert(true, 'An exception should not have been thrown');
		}
	}

	@isTest
	private static void shouldCreateConfigWithValidConfig() {
		insert new External_WebService_Config__c(
			Name = 'TestService',
			Username__c = 'username',
			Password__c = 'password',
			URL__c = 'http://www.test.com?hello=world',
			certificate_name__c = 'testCertificate'
		);
		insert new External_WebService_Config__c(Name = 'ECSProduction_IP', Username__c = '', Password__c = '', URL__c = 'http://www.test.com');

		IWebServiceConfig config = WebServiceConfigLocator.getConfig('TestService');
		System.assertEquals('username', config.getUsername(), 'Username was not correctly retrieved from config');
		System.assertEquals('password', config.getPassword(), 'Password was not correctly retrieved from config');
		System.assertEquals('http://www.test.com?hello=world', config.getEndpoint(), 'Endpoint was not correctly retrieved from config');
		System.assertEquals('testCertificate', config.getCertificateName(), 'Certificate was not correctly retrieved from config');
	}

	@isTest
	private static void shouldCreateConfigWithNamedCredentials() {
		IWebServiceConfig config = WebServiceConfigLocator.getConfigNamedCredential('ECSSOAPCompany');
		System.assertEquals(true, config.getEndpoint().contains('/v1/BOP/protected/company'), 'Endpoint was not correctly retrieved from config');
		try {
			@SuppressWarnings('PMD.UnusedLocalVariable')
			IWebServiceConfig configError = WebServiceConfigLocator.getConfigNamedCredential('ECSSOAP');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Attempted to configure an endpoint without settings'),
				'Metadata record found when testing exception'
			);
		}
	}
}
