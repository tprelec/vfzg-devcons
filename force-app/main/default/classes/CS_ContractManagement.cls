public with sharing class CS_ContractManagement {
	/*
	 * @description creates contract records after O&S decomposition
	 * for each agreement (agreement Name defined with Contract_Number__c on related Product Configuration) new contract record is created
	 * contract lookup fields are then populated to make the connections with subscriptions, agreement etc.
	 * in case of MACD, "old" contract's status is set to Amended and links/lookups between contracts are set up
	 */
	public static List<csconta__Contract__c> createContractRecords(List<Id> subscriptionIds) {
		List<csconta__Contract__c> contractsToInsert = new List<csconta__Contract__c>();
		Map<String, CS_ContractNumber> contractNumberDetailsMap = new Map<String, CS_ContractNumber>();
		Map<String, csord__Order__c> contractNumberOrderMap = new Map<String, csord__Order__c>();
		Set<Id> originalContractIds = new Set<Id>();

		List<csord__Subscription__c> subscriptions = [
			SELECT
				Id,
				Name,
				csord__Order__r.Id,
				csord__Order__r.csordtelcoa__Opportunity__c,
				csordtelcoa__Product_Configuration__c,
				csordtelcoa__Replaced_Subscription__c,
				csordtelcoa__Replaced_Subscription__r.Contract__c,
				csordtelcoa__Replaced_Subscription__r.Contract__r.csconta__Contract_Replacing_This_Contract__c,
				csordtelcoa__Replaced_Subscription__r.Contract__r.csconta__Status__c,
				csordtelcoa__Replaced_Subscription__r.Contract__r.csconta__Valid_To__c,
				csordtelcoa__Product_Configuration__r.Contract_Number__c,
				csordtelcoa__Product_Configuration__r.ContractNumber_JSON__c
			FROM csord__Subscription__c
			WHERE Id IN :subscriptionIds AND csordtelcoa__Product_Configuration__c != NULL
		];

		if (subscriptions.size() == 0) {
			return null;
		}

		System.debug(Logginglevel.DEBUG, 'Create contract records - subscriptions: ' + JSON.serializePretty(subscriptions));

		for (csord__Subscription__c subscription : subscriptions) {
			if (subscription.csordtelcoa__Product_Configuration__r.ContractNumber_JSON__c == null) {
				continue;
			}
			contractNumberOrderMap.put(subscription.csordtelcoa__Product_Configuration__r.Contract_Number__c, subscription.csord__Order__r);
			CS_ContractNumber contractNumber = (CS_ContractNumber) JSON.deserialize(
				subscription.csordtelcoa__Product_Configuration__r.ContractNumber_JSON__c,
				CS_ContractNumber.class
			);
			contractNumberDetailsMap.put(subscription.csordtelcoa__Product_Configuration__r.Contract_Number__c, contractNumber);
			if (
				subscription.csordtelcoa__Replaced_Subscription__c != null &&
				subscription.csordtelcoa__Replaced_Subscription__r.Contract__c != null
			) {
				originalContractIds.add(subscription.csordtelcoa__Replaced_Subscription__r.Contract__c);
			}
		}

		System.debug(Logginglevel.DEBUG, 'Create contract records - contractNumberOrderMap: ' + JSON.serializePretty(contractNumberOrderMap));

		Set<Id> opptyIds = new Set<Id>();
		for (csord__Order__c order : contractNumberOrderMap.values()) {
			if (order != null && order.csordtelcoa__Opportunity__c != null) {
				opptyIds.add(order.csordtelcoa__Opportunity__c);
			}
		}

		List<csclm__Agreement__c> opptyAgreements = [
			SELECT Id, Name, Contract_Number__c, csclm__Opportunity__c, csclm__Opportunity__r.AccountId
			FROM csclm__Agreement__c
			WHERE Contract_Number__c IN :contractNumberOrderMap.keySet() AND csclm__Opportunity__c IN :opptyIds
		];

		System.debug(Logginglevel.DEBUG, 'Create contract records - opptyAgreements: ' + JSON.serializePretty(opptyAgreements));

		if (opptyAgreements.size() == 0) {
			return null;
		}

		for (csclm__Agreement__c agreement : opptyAgreements) {
			csconta__Contract__c newContract = createContract(agreement, contractNumberOrderMap, contractNumberDetailsMap);
			contractsToInsert.add(newContract);
		}

		if (contractsToInsert.size() > 0) {
			insert contractsToInsert;
		}

		Map<Id, csconta__Contract__c> originalContracts;
		if (originalContractIds.size() > 0) {
			originalContracts = new Map<Id, csconta__Contract__c>(
				[
					SELECT Id, csconta__Status__c, csconta__Valid_To__c, csconta__Contract_Replacing_This_Contract__c
					FROM csconta__Contract__c
					WHERE Id IN :originalContractIds
				]
			);
		}

		System.debug(Logginglevel.DEBUG, 'Create contract records - contractsToInsert: ' + JSON.serializePretty(contractsToInsert));

		// now that we have the contract in the system, still need to fill lookups on the related objects
		for (csord__Subscription__c subscription : subscriptions) {
			for (csconta__Contract__c contract : contractsToInsert) {
				if (contract.csconta__Contract_Name__c == subscription.csordtelcoa__Product_Configuration__r.Contract_Number__c) {
					subscription.Contract__c = contract.Id;

					if (originalContracts != null && subscription.csordtelcoa__Replaced_Subscription__r?.Contract__c != null) {
						csconta__Contract__c originalContract = originalContracts.get(subscription.csordtelcoa__Replaced_Subscription__r.Contract__c);
						originalContract.csconta__Status__c = 'Amended';
						originalContract.csconta__Valid_To__c = contract.csconta__Valid_From__c;
						originalContract.csconta__Contract_Replacing_This_Contract__c = contract.Id;
						contract.csconta__Contract_Being_Replaced__c = originalContract.Id;
					}
					break;
				}
			}
		}

		update subscriptions;
		if (contractsToInsert.size() > 0) {
			update contractsToInsert;
		}

		if (originalContractIds.size() > 0) {
			System.debug(Logginglevel.DEBUG, 'Create contract records - originalContracts: ' + JSON.serializePretty(originalContracts));
			List<csconta__Contract__c> originalContractList = originalContracts.values();
			update originalContractList;
		}

		return contractsToInsert;
	}

	private static csconta__Contract__c createContract(
		csclm__Agreement__c agreement,
		Map<String, csord__Order__c> contractNumberOrderMap,
		Map<String, CS_ContractNumber> contractNumberDetailsMap
	) {
		csconta__Contract__c newContract = new csconta__Contract__c();
		newContract.csconta__Account__c = agreement.csclm__Opportunity__r.AccountId;
		newContract.csconta__Order__c = contractNumberOrderMap.get(agreement.Name).Id;
		newContract.csconta__Contract_Name__c = agreement.Name;
		newContract.ClmAgreement__c = agreement.Id;
		newContract.csconta__Status__c = CS_ConstantsCOM.CONTRACT_STATUS_IMPLEMENTATION;
		//newContract.csconta__Valid_To__c = Date.newInstance(2100, 12, 31);
		newContract.csconta__Valid_From__c = contractNumberDetailsMap.get(agreement.Name).contractDate;
		return newContract;
	}
}
