public with sharing class CS_ComPortingDetailsWrapper {
    @AuraEnabled
    public String name;

    @AuraEnabled
    public String value;
}