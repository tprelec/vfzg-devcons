public with sharing class COM_CDOServiceCharacteristicHelper {
	public static void parseBIMOCharacteristics(COM_CDO_ServiceOrder.Service service) {
		Map<Id, COM_CDO_Integration_Article_setting__mdt> allArticles = getAllServiceCharacteristics();
		Map<Id, COM_CDO_Integration_Article_setting__mdt> outputArticles = getOutputArticles(allArticles);

		if (outputCharacteristicsPresent(service, outputArticles)) {
			List<csord__Service__c> services = COM_CDOHelper.getServicesPerUmbrellaService(service.id);

			Map<Id, csord__Service__c> servicesFromOrderMap = new Map<Id, csord__Service__c>(services);

			List<Id> serviceIds = new List<Id>(servicesFromOrderMap.keySet());
			COM_DeliveryComponentsGenerator gen = new COM_DeliveryComponentsGenerator();
			COM_MetadataInformation meta = new COM_MetadataInformation();
			meta = gen.prepareDataToGenerateComponents(serviceIds);
			Map<Id, COM_Delivery_Article__mdt> deliveryArticleMappingMap = new Map<Id, COM_Delivery_Article__mdt>();
			deliveryArticleMappingMap = meta.getDeliveryArticlesMap();
			Map<Id, COM_Decomposition__mdt> decompositionsMapdecompositionsMap = meta.getDecompositionsMap();
			Map<Id, List<csord__Service__c>> mapUpdateServices = getServicesPerOutputArticle(
				decompositionsMapdecompositionsMap,
				services,
				outputArticles
			);

			updateServiceFields(mapUpdateServices, service, outputArticles);
		}
	}

	public static String modifyRequestBody(String body) {
		String result = '';
		Map<Id, COM_CDO_Integration_Article_setting__mdt> articles = new Map<Id, COM_CDO_Integration_Article_setting__mdt>();

		Set<String> elementNames = new Set<String>();
		List<String> serviceCharacteristics = new List<String>();
		articles = getAllServiceCharacteristics();

		for (Id key : articles.keySet()) {
			elementNames.add(articles.get(key).Element_name__c);
		}

		for (String characteristic : elementNames) {
			serviceCharacteristics.add(characteristic);
		}
		result = modifyJsonToFitDeserializeClass(body, serviceCharacteristics);
		return result;
	}

	private static Map<Id, COM_CDO_Integration_Article_setting__mdt> getAllServiceCharacteristics() {
		Map<Id, COM_CDO_Integration_Article_setting__mdt> result = new Map<Id, COM_CDO_Integration_Article_setting__mdt>(
			[
				SELECT
					Id,
					Element_name__c,
					External_System__c,
					IsSiteElement__c,
					IsTenantElement__c,
					IsOutput__c,
					COM_Delivery_Component__c,
					Request_name__c,
					Requires_site_information__c,
					Requires_tenantId__c,
					OrderInRequest__c,
					COM_Delivery_Article__c,
					ArticleCode__c,
					Output_Service_API_Field__c
				FROM COM_CDO_Integration_Article_setting__mdt
				WHERE External_System__c = 'CDO'
			]
		);

		return result;
	}

	private static String modifyJsonToFitDeserializeClass(String json, List<String> serviceCharacteristics) {
		String result = '';

		for (String characteristic : serviceCharacteristics) {
			json = json.replaceAll('"' + characteristic + '":', '"valueOf":');
		}
		json = modifyJsonLiteToFitDeserializeClass(json);

		result = json;
		return result;
	}

	public static String modifyJsonLiteToFitDeserializeClass(String json) {
		json = json.replaceAll('"@type":', '"typeVal":');

		return json;
	}

	private static Boolean outputCharacteristicsPresent(
		COM_CDO_ServiceOrder.Service service,
		Map<Id, COM_CDO_Integration_Article_setting__mdt> outputArticles
	) {
		Boolean result = false;
		if (outputArticles == null) {
			return false;
		}

		for (Id key : outputArticles.keyset()) {
			for (COM_CDO_ServiceOrder.ServiceCharacteristic serviceCharacteristic : service.serviceCharacteristic) {
				if (serviceCharacteristic.name == outputArticles.get(key).Element_name__c) {
					return true;
				}
			}
		}

		return result;
	}

	private static void updateServiceFields(
		Map<Id, List<csord__Service__c>> mapUpdateServices,
		COM_CDO_ServiceOrder.Service service,
		Map<Id, COM_CDO_Integration_Article_setting__mdt> outputArticles
	) {
		Map<String, OutputArticle> outputValuePerUmbrellaService = getArticleValueMap(outputArticles, service);

		List<csord__Service__c> servicesForUpdate = processServicesForUpdate(outputValuePerUmbrellaService, mapUpdateServices);
		try {
			update servicesForUpdate;
		} catch (Exception ex) {
			LoggerService.log(LoggingLevel.DEBUG, 'Error while Service CDO update processing: ' + ex.getMessage());
		}
	}

	private static List<csord__Service__c> processServicesForUpdate(
		Map<String, OutputArticle> outputValuePerUmbrellaService,
		Map<Id, List<csord__Service__c>> mapUpdateServices
	) {
		List<csord__Service__c> result = new List<csord__Service__c>();
		Map<Id, csord__Service__c> servicesToUpdateMap = new Map<Id, csord__Service__c>();

		for (Id outputMetadataId : mapUpdateServices.keySet()) {
			OutputArticle outputArticle = outputValuePerUmbrellaService.get(outputMetadataId);
			for (csord__Service__c service : mapUpdateServices.get(outputMetadataId)) {
				if (servicesToUpdateMap.containsKey(service.id)) {
					servicesToUpdateMap.get(service.id).put(outputArticle.serviceFieldAPIName, outputArticle.outputCharacteristicValue);
				} else {
					csord__Service__c serviceToUpdate = new csord__Service__c();
					serviceToUpdate.put('Id', service.id);
					serviceToUpdate.put(outputArticle.serviceFieldAPIName, outputArticle.outputCharacteristicValue);
					servicesToUpdateMap.put(service.id, serviceToUpdate);
				}
			}
		}

		for (Id serviceId : servicesToUpdateMap.keySet()) {
			result.add(servicesToUpdateMap.get(serviceId));
		}
		return result;
	}

	private static Map<String, OutputArticle> getArticleValueMap(
		Map<Id, COM_CDO_Integration_Article_setting__mdt> outputArticles,
		COM_CDO_ServiceOrder.Service service
	) {
		Map<String, OutputArticle> result = new Map<String, OutputArticle>();

		for (Id outputArticleId : outputArticles.keySet()) {
			for (COM_CDO_ServiceOrder.ServiceCharacteristic serviceCharacteristic : service.serviceCharacteristic) {
				if (serviceCharacteristic.name == outputArticles.get(outputArticleId).Element_name__c) {
					OutputArticle outputArticleEntity = new OutputArticle();
					outputArticleEntity.serviceFieldAPIName = outputArticles.get(outputArticleId).Output_Service_API_Field__c;
					outputArticleEntity.outputCharacteristicValue = serviceCharacteristic.value.valueOf;
					result.put(outputArticleId, outputArticleEntity);
				}
			}
		}
		return result;
	}

	private static Map<Id, COM_CDO_Integration_Article_setting__mdt> getOutputArticles(
		Map<Id, COM_CDO_Integration_Article_setting__mdt> allArticles
	) {
		Map<Id, COM_CDO_Integration_Article_setting__mdt> result = new Map<Id, COM_CDO_Integration_Article_setting__mdt>();
		for (Id articleKey : allArticles.keySet()) {
			if (allArticles.get(articleKey).IsOutput__c) {
				result.put(articleKey, allArticles.get(articleKey));
			}
		}
		return result;
	}

	private static List<csord__Service__c> getServicesPerDeliveryMap(COM_Decomposition__mdt decomposition, List<csord__Service__c> services) {
		List<csord__Service__c> result = new List<csord__Service__c>();
		for (csord__Service__c service : services) {
			if (
				(decomposition.Commercial_Component__c == service.VFZ_Commercial_Component_Name__c &&
				decomposition.Commercial_Article__c == service.VFZ_Commercial_Article_Name__c) ||
				(decomposition.Commercial_Component__c == service.VFZ_Commercial_Component_Name__c &&
				decomposition.Commercial_Article__c == '*')
			) {
				result.add(service);
			}
		}
		return result;
	}

	private static Map<Id, List<csord__Service__c>> getServicesPerOutputArticle(
		Map<Id, COM_Decomposition__mdt> decompositionsMapdecompositionsMap,
		List<csord__Service__c> services,
		Map<Id, COM_CDO_Integration_Article_setting__mdt> outputArticles
	) {
		Map<Id, List<csord__Service__c>> result = new Map<Id, List<csord__Service__c>>();

		for (Id articleKey : outputArticles.keySet()) {
			Id deliveryComponentId = outputArticles.get(articleKey).COM_Delivery_Component__c;
			for (Id decompositionId : decompositionsMapdecompositionsMap.keySet()) {
				COM_Decomposition__mdt decomposition = decompositionsMapdecompositionsMap.get(decompositionId);
				if (decomposition.Delivery_Article__r.Delivery_Component__c == deliveryComponentId) {
					List<csord__Service__c> servicesPerDelivery = getServicesPerDeliveryMap(decomposition, services);

					if (servicesPerDelivery != null && servicesPerDelivery.size() > 0) {
						result.put(articleKey, servicesPerDelivery);
					}
				}
			}
		}
		return result;
	}

	public static List<COM_CDO_ServiceOrder.ServiceCharacteristic> getServiceCharacteristics(List<csord__Service__c> services) {
		List<COM_CDO_ServiceOrder.ServiceCharacteristic> result = new List<COM_CDO_ServiceOrder.ServiceCharacteristic>();

		List<Id> serviceIds = new List<Id>();

		for (csord__Service__c service : services) {
			serviceIds.add(service.Id);
		}

		List<DeliveryArticleMapping> articleMappings = getArticleMapping(serviceIds);

		COM_CDOAccountCharacteristicsHelper.AccountSiteTenantData accountData = COM_CDOAccountCharacteristicsHelper.getAccountServiceCharacteristicValues(
			services
		);

		result = getServiceCharacteristicList(articleMappings, accountData);

		return result;
	}

	private static List<COM_CDO_ServiceOrder.ServiceCharacteristic> getServiceCharacteristicList(
		List<DeliveryArticleMapping> articleMappings,
		COM_CDOAccountCharacteristicsHelper.AccountSiteTenantData accountData
	) {
		List<COM_CDO_ServiceOrder.ServiceCharacteristic> result = new List<COM_CDO_ServiceOrder.ServiceCharacteristic>();

		for (DeliveryArticleMapping article : articleMappings) {
			COM_CDO_ServiceOrder.ServiceCharacteristic characteristic = new COM_CDO_ServiceOrder.ServiceCharacteristic();

			characteristic.name = article.elementName;
			characteristic.valueType = 'string';

			COM_CDO_ServiceOrder.GenValue genValue = new COM_CDO_ServiceOrder.GenValue();
			genValue.typeVal = 'string';
			genValue.valueOf = article.deliveryArticleCode;

			if (article.isSiteCharacteristic || article.isTenantCharacteristic) {
				genValue.valueOf = COM_CDOAccountCharacteristicsHelper.getAccountSiteDataInfo(accountData, article.deliveryArticleCode);
			}

			characteristic.value = genValue;

			result.add(characteristic);
		}

		return result;
	}

	private static List<DeliveryArticleMapping> getDeliveryArticlesCharacteristics(Map<Id, COM_CDO_Integration_Article_setting__mdt> articles) {
		List<DeliveryArticleMapping> comparator = new List<DeliveryArticleMapping>();

		for (Id key : articles.keySet()) {
			DeliveryArticleMapping article = new DeliveryArticleMapping();
			article.elementName = articles.get(key).Element_name__c;
			article.elementOrder = Integer.valueOf(articles.get(key).OrderInRequest__c);
			article.deliveryArticleCode = articles.get(key).ArticleCode__c;
			article.deliverySettingId = articles.get(key).Id;
			article.isSiteCharacteristic = articles.get(key).IsSiteElement__c;
			article.isTenantCharacteristic = articles.get(key).IsTenantElement__c;
			comparator.add(article);
		}

		comparator.sort();

		return comparator;
	}

	private static Map<Id, COM_CDO_Integration_Article_setting__mdt> getRelevantServiceCharacteristics(Set<Id> deliveryArticles) {
		Map<Id, COM_CDO_Integration_Article_setting__mdt> result = new Map<Id, COM_CDO_Integration_Article_setting__mdt>(
			[
				SELECT
					Id,
					Element_name__c,
					External_System__c,
					IsSiteElement__c,
					IsTenantElement__c,
					Request_name__c,
					Requires_site_information__c,
					Requires_tenantId__c,
					OrderInRequest__c,
					COM_Delivery_Article__c,
					ArticleCode__c,
					IsOutput__c,
					COM_Delivery_Component__c
				FROM COM_CDO_Integration_Article_setting__mdt
				WHERE
					External_System__c = 'CDO'
					AND Request_name__c = 'provisionBIMOService'
					AND IsOutput__c = FALSE
					AND (COM_Delivery_Article__c IN :deliveryArticles
					OR COM_Delivery_Article__c = '')
			]
		);

		return result;
	}

	public static List<COM_CDO_Integration_Article_setting__mdt> getInputServiceCharacteristics(String reqName) {
		List<COM_CDO_Integration_Article_setting__mdt> result = [
			SELECT
				Id,
				Element_name__c,
				External_System__c,
				IsSiteElement__c,
				IsTenantElement__c,
				Request_name__c,
				Requires_site_information__c,
				Requires_tenantId__c,
				OrderInRequest__c,
				COM_Delivery_Article__c,
				ArticleCode__c,
				IsOutput__c,
				COM_Delivery_Component__c
			FROM COM_CDO_Integration_Article_setting__mdt
			WHERE External_System__c = 'CDO' AND Request_name__c = :reqName AND IsOutput__c = FALSE
			//AND DeveloperName = 'createTenant_sourceSystem'
		];

		return result;
	}

	private static List<DeliveryArticleMapping> getArticleMapping(List<Id> serviceIds) {
		COM_DeliveryComponentsGenerator gen = new COM_DeliveryComponentsGenerator();

		COM_MetadataInformation meta = new COM_MetadataInformation();
		meta = gen.prepareDataToGenerateComponents(serviceIds);

		Map<Id, COM_Delivery_Article__mdt> deliveryArticleMappingMap = new Map<Id, COM_Delivery_Article__mdt>();
		deliveryArticleMappingMap = meta.getDeliveryArticlesMap();

		List<DeliveryArticleMapping> result = new List<DeliveryArticleMapping>();

		Map<Id, COM_CDO_Integration_Article_setting__mdt> integrationServiceCharacteristics = getRelevantServiceCharacteristics(
			deliveryArticleMappingMap.keySet()
		);

		result = getDeliveryArticlesCharacteristics(integrationServiceCharacteristics);

		return result;
	}

	private class OutputArticle {
		public String serviceFieldAPIName;
		public String outputCharacteristicValue;
	}

	public class DeliveryArticleMapping implements Comparable {
		public String elementName;
		public Integer elementOrder;
		public String deliveryArticleCode;
		public String deliverySettingId;
		public Boolean isSiteCharacteristic;
		public Boolean isTenantCharacteristic;

		public Integer compareTo(Object compareTo) {
			DeliveryArticleMapping dam = (DeliveryArticleMapping) compareTo;
			if (this.elementOrder > dam.elementOrder) {
				return 1;
			}
			if (this.elementOrder == dam.elementOrder) {
				return 0;
			}
			return -1;
		}
	}
}
