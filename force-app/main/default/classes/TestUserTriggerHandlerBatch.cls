@isTest
private class TestUserTriggerHandlerBatch {
	@isTest
	static void testFreeCPQLicense() {
		Account partnerAccount = TestUtils.createPartnerAccount();
		User partnerUser = TestUtils.createPortalUser(partnerAccount);

		BigMachines__Configuration_Record__c cpqCOnfig = new BigMachines__Configuration_Record__c(
			BigMachines__action_id_copy__c = '1',
			BigMachines__action_id_open__c = '1',
			BigMachines__bm_site__c = '1',
			BigMachines__document_id__c = '1',
			BigMachines__process__c = '1',
			BigMachines__process_id__c = '1',
			BigMachines__version_id__c = '1'
		);
		insert cpqCOnfig;

		BigMachines__Oracle_User__c bmUser = new BigMachines__Oracle_User__c();
		bmUser.BigMachines__Salesforce_User__c = partnerUser.Id;
		bmUser.Name = 'Test User';
		bmUser.BigMachines__Access_Type__c = 'BuyAccess';
		bmUser.BigMachines__Allow_Quote_Creation__c = true;
		bmUser.BigMachines__Association_to_Organization__c = 'Internal User';
		bmUser.BigMachines__Bulk_Synchronization__c = false;
		bmUser.BigMachines__Oracle_CPQ_Cloud_Site__c = cpqCOnfig.Id;
		insert bmUser;

		Test.StartTest();
		Set<Id> newUserIds = new Set<Id>();
		newUserIds.Add(partnerUser.Id);

		System.enqueueJob(new UserTriggerHandlerBatch(newUserIds));

		BigMachines__Oracle_User__c user = [
			SELECT Id, BigMachines__Provisioned__c, BigMachines__Bulk_Synchronization__c
			FROM BigMachines__Oracle_User__c
			WHERE Id = :bmUser.Id
		];

		System.assertEquals(
			user.BigMachines__Bulk_Synchronization__c,
			false,
			'BigMachines__Bulk_Synchronization__c is false'
		);
		System.assertEquals(
			user.BigMachines__Provisioned__c,
			false,
			'BigMachines__Provisioned__c is false'
		);

		Test.StopTest();
	}
}