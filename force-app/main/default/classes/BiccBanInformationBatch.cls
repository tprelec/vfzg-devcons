global class BiccBanInformationBatch implements Database.Batchable <sObject>, Database.Stateful{

	global final map<String,String> biccBanFieldMapping = SyncUtil.fullMapping('BICC_BAN_Information__c -> Ban__c').get('BICC_BAN_Information__c -> Ban__c');
	global final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	global final Schema.SObjectType biccSchema = schemaMap.get('BICC_BAN_Information__c');
	global final map<String, Schema.SObjectField> biccFieldMap = biccSchema.getDescribe().fields.getMap();
	global final set<String> queriedFields = SyncUtil.getQueriedFields(biccBanFieldMapping);
	global final list<String> sortList = SyncUtil.getSortedList(biccFieldMap);
	global String header = SyncUtil.getHeader(sortList, queriedFields);
	global String dataError = '';
	global Boolean chain = false;
	global Boolean kvk = false;

	global Database.QueryLocator start(Database.BatchableContext BC){

		//Create a dynamic query with info from the Field Mapping Object
		String query = 'SELECT ';
		set<String> testset = new set<String>();
		testset.addall(biccBanFieldMapping.values());
		for (String biccField : testset) {
			query += biccField + ',';
		}
		query = query.subString(0, query.length() - 1);
		query += ' FROM BICC_BAN_Information__c';

		return Database.getQueryLocator(query);
	}

	global void execute(Database.BatchableContext BC, list<BICC_BAN_Information__c> scope){

		if(Job_Management__c.getOrgDefaults().Cancel_Batch__c == true){
			System.abortJob(BC.getJobId());
		}

		//Create lists and maps
		map<String, BICC_BAN_Information__c> scopeMap = new map<String, BICC_BAN_Information__c>();
		list<BICC_BAN_Information__c> biccErrorList = new list<BICC_BAN_Information__c>();
		list<Ban__c> banList = new list<Ban__c>();
		map<String, Id> kvkMap = new map<String, Id>();
		map<String, BICC_BAN_Information__c> kvkToBicc = new map<String, BICC_BAN_Information__c>();
		set<String> kvkSet = new set<String>();
		list<BICC_BAN_Information__c> biccDeleteList = new list<BICC_BAN_Information__c>();
		map<String, Id> existingBans = new map<String, Id>();
		list<BICC_BAN_Information__c> biccKvKList = new list<BICC_BAN_Information__c>();
		String kvkError = 'New Ban with not existing KVK';

		//Put scope in a map for deletion
		for(BICC_BAN_Information__c bicc : scope){
			if(StringUtils.checkBan(bicc.BAN__c)){
				//Remove the trailing characters from the KvK number
				if(bicc.COC_Number__c != null){
					if(bicc.COC_Number__c.length() > 8){
						bicc.COC_Number__c = bicc.COC_Number__c.subString(0,8);
					}
					kvkMap.put(bicc.COC_Number__c, null);
					kvkToBicc.put(bicc.COC_Number__c, bicc);
				}
				scopeMap.put(bicc.BAN__c, bicc);
			} else {
				bicc.Error__c = 'The BAN number is not valid';
				biccErrorList.add(bicc);
			}
		}

		//Get matching accountid's from kvk
		for(Account acc : [select Id, KVK_Number__c from Account where KVK_Number__c in: kvkMap.keySet()]){
			kvkMap.put(acc.KVK_Number__c, acc.Id);
		}

		//Get Vodafone Account
		Id vodafoneAcc = [select Id from Account where Name =: 'Vodafone Libertel B.v.' limit 1].Id;

		//Delete null values from map
		for(String s : kvkMap.keySet()){
			if(kvkMap.get(s) == null){
				kvkMap.remove(s);
				kvkToBicc.get(s).Error__c = kvkError;
				biccKvKList.add(kvkToBicc.get(s));
				scopeMap.remove(kvkToBicc.get(s).BAN__c);
				kvk = true;
			}
		}
		
		//Get matching Bans
		for(Ban__c ban : [select Name, Account__c from Ban__c where Name in: scopeMap.keySet()]){
			existingBans.put(ban.Name, ban.Account__c);
		}

		//Create Ban from BICC
		for (BICC_BAN_Information__c biccBan : scopeMap.values()){
			Ban__c ban = new Ban__c();
			for (String banField : biccBanFieldMapping.keySet()) {
				String biccBanField = biccBanFieldMapping.get(banField);
				Object biccBanValue = biccBan.get(biccBanField);
				ban.put(banField,biccBanValue);
			}
			if(kvkMap.containsKey(biccBan.COC_Number__c)){
				ban.Account__c = kvkMap.get(biccBan.COC_Number__c);
			} else if(existingBans.containsKey(biccBan.Ban__c)){
				ban.Account__c = existingBans.get(biccBan.Ban__c);
			} else {
				ban.Account__c = vodafoneAcc;
			}
			banList.add(ban);
		}

		//Upsert Bans in the database
		list<Database.UpsertResult> UR = Database.upsert(banList, Ban__c.Fields.Name, false);
		for (Integer i = 0; i < UR.size(); i++) {
			if(!UR[i].isSuccess()){
				scopeMap.get(banList[i].Name).Error__c = 'Error while creating Ban';
				biccErrorList.add(scopeMap.get(banList[i].Name));
			} else {
				biccDeleteList.add(scopeMap.get(banList[i].Name));
			}
		}

		for(BICC_BAN_Information__c error : biccErrorList){
			for(String field : sortList){
				if(queriedFields.contains(field)){
					dataError += '"' + error.get(field) + '",';
				}
			}
			dataError += '"' + error.Error__c + '"\n';
		}

		//Do DML
		update biccKvKList;
		delete biccErrorList;
		delete biccDeleteList;
	}

	global void finish(Database.BatchableContext BC){

		//Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							TotalJobItems
						  FROM
							AsyncApexJob
						  WHERE
							Id =: BC.getJobId()];

		// Send an email to the running user notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		header = header.subString(0, header.length() - 1) + ',"Error"\n';
		Blob b = blob.valueOf(header + dataError);
		Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
		efa.setFileName('BiccBanInformationErrors.csv');
		efa.setBody(b);

		String[] toAddresses = new String[] {UserInfo.getUserEmail()};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Bicc Ban Information database import ' + a.Status);
		mail.setPlainTextBody('The Bicc Ban Information database import job processed ' + a.TotalJobItems +
							  ' batches with '+ a.NumberOfErrors + ' failures.');
		mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

		if(kvk == true){
			BiccKvkBatch controller = new BiccKvkBatch();
			controller.chain = chain;
			Database.executeBatch(controller);
		} else if(chain == true){
			//BiccBoaInformationBatch controller = new BiccBoaInformationBatch();
			// 2015-05-29 GC BICC BOA is not run any longer as BOA is running inside SFDC from now on
			BiccVodacomDataBanBatch controller = new BiccVodacomDataBanBatch();
			controller.chain = true;
			database.executebatch(controller);
		}
	}
}