/**
 * Created by henk on 02/08/2018.
 */
@isTest
private class TestBdsRestAddProduct {
    @isTest
    static void runNoOpportunityIdInValidation() {
        Test.startTest();
        List<BdsResponseClasses.ValidationError> errList = BdsRestAddProducts.validateValues('', null);
        System.assertEquals(2, errList.size());
        system.debug('##errList: '+errList);
        System.assertEquals('opportunityId', errList[0].field);
        Test.stopTest();
    }

    @isTest
    static void runMissingProductValues() {
        Test.startTest();
        List<BdsRestAddProducts.Product> prodList = new List<BdsRestAddProducts.Product>();
        BdsRestAddProducts.Product prodClass = new BdsRestAddProducts.Product();
        prodList.add(prodClass);

        List<BdsResponseClasses.ValidationError> errList = BdsRestAddProducts.validateValues('abc', prodList);
        System.assertEquals(5, errList.size());
        Test.stopTest();
    }
        
    @isTest
    static void errorValidation() {
        Test.startTest();
        BdsRestAddProducts.doPost('', new List<BdsRestAddProducts.Product>());
        Test.stopTest();
    }

    @isTest
    static void runCreateOppAddProduct() {

        User owner = TestUtils.createManager();
        owner.Email = 'ss@example.com';
        update owner;
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0623456789';
        conClass.mobilephone = '0623456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.housenumber = 1;
        siteClass.zipcode = '1010AA';
        siteClass.housenumberExt = 'a';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Test.startTest();
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN', '1', 'ss@example.com');
        system.debug('### retClass: ' + retClass);
        OrderType__c ot = new OrderType__c();
        ot.Status__c = 'New';
        ot.ExportSystem__c = 'SIAS';
        ot.ExternalId__c = 'OD-04-1234568';
        insert ot;

        Product2 prod = new Product2();
        prod.Name = 'Test Product';
        prod.ProductCode = 'C108790';
        prod.Product_Line__c = 'fVodafone';
        // prod.BigMachines__Part_Number__c = 'C108790';
        prod.OrderType__c = ot.Id;
        prod.isActive = true;
        insert prod;

        Id pricebookId = Test.getStandardPricebookId();

        PricebookEntry customPrice = new PricebookEntry();
        customPrice.Pricebook2Id = pricebookId;
        customPrice.Product2Id = prod.Id;
        customPrice.UnitPrice = 12000;
        customPrice.IsActive = true;
        insert customPrice;


        List<BdsRestAddProducts.Product> prodList = new List<BdsRestAddProducts.Product>();
        BdsRestAddProducts.Product prodClass = new BdsRestAddProducts.Product();

        List<BdsRestAddProducts.Site> siteListPr = new List<BdsRestAddProducts.Site>();
        BdsRestAddProducts.Site siteClassPr = new BdsRestAddProducts.Site();
        siteClassPr.housenumber = 1;
        siteClassPr.zipcode = '1010AA';
        siteClassPr.housenumberExt = 'a';
        siteListPr.add(siteClassPr);

        prodClass.sites = siteListPr;
        prodClass.productCode = 'C108790';
        prodClass.quantity = 1;
        prodClass.price = 1;
        prodClass.discount = 0;
        prodClass.duration = 1;

        prodList.add(prodClass);

        BdsRestAddProducts.doPost(retClass.opportunityId, prodList);

        Test.stopTest();
    }

    
    @isTest
    static void siteNotFound() {

        User owner = TestUtils.createManager();
        owner.Email = 'ss@example.com';
        update owner;
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0623456789';
        conClass.mobilephone = '0623456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.housenumber = 1;
        siteClass.zipcode = '1010AA';
        siteClass.housenumberExt = 'a';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Test.startTest();
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN', '1', 'ss@example.com');
        system.debug('### retClass: ' + retClass);
        OrderType__c ot = new OrderType__c();
        ot.Status__c = 'New';
        ot.ExportSystem__c = 'SIAS';
        ot.ExternalId__c = 'OD-04-1234568';
        insert ot;

        Product2 prod = new Product2();
        prod.Name = 'Test Product';
        prod.ProductCode = 'C108790';
        prod.Product_Line__c = 'fVodafone';
        // prod.BigMachines__Part_Number__c = 'C108790';
        prod.OrderType__c = ot.Id;
        prod.isActive = true;
        insert prod;

        Id pricebookId = Test.getStandardPricebookId();

        PricebookEntry customPrice = new PricebookEntry();
        customPrice.Pricebook2Id = pricebookId;
        customPrice.Product2Id = prod.Id;
        customPrice.UnitPrice = 12000;
        customPrice.IsActive = true;
        insert customPrice;


        List<BdsRestAddProducts.Product> prodList = new List<BdsRestAddProducts.Product>();
        BdsRestAddProducts.Product prodClass = new BdsRestAddProducts.Product();

        List<BdsRestAddProducts.Site> siteListPr = new List<BdsRestAddProducts.Site>();
        BdsRestAddProducts.Site siteClassPr = new BdsRestAddProducts.Site();
        siteClassPr.housenumber = 1;
        siteClassPr.zipcode = '1010AA';
        siteClassPr.housenumberExt = 'b';
        siteListPr.add(siteClassPr);

        prodClass.sites = siteListPr;
        prodClass.productCode = 'C108790';
        prodClass.quantity = 1;
        prodClass.price = 1;
        prodClass.discount = 0;
        prodClass.duration = 1;

        prodList.add(prodClass);

        BdsRestAddProducts.doPost(retClass.opportunityId, prodList);

        Test.stopTest();
    }
}