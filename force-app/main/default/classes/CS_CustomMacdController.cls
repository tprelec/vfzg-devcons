public with sharing class CS_CustomMacdController {

    @AuraEnabled
    public static List<Opportunity> getAccountOpportunities(Id accountId) {
        List<Opportunity> opps = [SELECT Id, Name FROM Opportunity WHERE AccountId = :accountId];
        return opps;
    }
}