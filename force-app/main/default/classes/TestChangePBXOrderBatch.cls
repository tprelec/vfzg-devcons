/**
 * Test class for ChangePBXOrderBatch.cls
 */
@IsTest
public class TestChangePBXOrderBatch {
    private static final String MOCK_URL = 'http://example.com/SiasService';

    @TestSetup
    static void makeData() {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Site__c site = TestUtils.createSite(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
        VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
        OrderType__c ot = TestUtils.createOrderType();

        VF_Family_Tag__c familyTag = new VF_Family_Tag__c(
            Name = Constants.PRODUCT2_FAMILYTAG_PBX_Trunking,
            ExternalID__c = 'FT-06-ABCDEF'
        );
        insert familyTag;

        TestUtils.autoCommit = false;
        Product2 product = TestUtils.createProduct(
            new Map<String, Object>{
                'Billing_Type__c' => Constants.PRODUCT2_BILLINGTYPE_STANDARD,
                'Family_Tag__c' => familyTag.Id,
                'ProductCode' => 'C106929'
            }
        );
        insert product;

        // SIP Charging
        VF_Family_Tag__c familyTagSIP = new VF_Family_Tag__c(
            Name = Constants.PRODUCT2_FAMILYTAG_PBX_Trunking_SIP_Charging,
            ExternalID__c = 'FT-06-GHIJKL'
        );
        insert familyTagSIP;

        Product2 productSIP = TestUtils.createProduct(
            new Map<String, Object>{
                'Billing_Type__c' => Constants.PRODUCT2_BILLINGTYPE_STANDARD,
                'Family_Tag__c' => familyTagSIP.Id,
                'ProductCode' => 'C106928'
            }
        );
        insert productSIP;

        // create an order with new status
        Order__c order = new Order__c(
            Status__c = 'New',
            Propositions__c = 'Legacy',
            OrderType__c = ot.Id,
            VF_Contract__c = contr.Id,
            Number_of_items__c = 100,
            O2C_Order__c = true
        );
        insert order;

        Competitor_Asset__c compAsset = new Competitor_Asset__c(
            PABX_Id__c = '1234',
            Assigned_Product_Id__c = '9999'
        );
        insert compAsset;

        List<Contracted_Products__c> cpList = new List<Contracted_Products__c>();

        Contracted_Products__c contractedProduct = new Contracted_Products__c(
            Quantity__c = 1,
            Arpu_Value__c = 10,
            Duration__c = 1,
            Product__c = product.Id,
            VF_Contract__c = contr.Id,
            Site__c = site.Id,
            Order__c = order.Id,
            CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ,
            PBX__c = compAsset.Id,
            External_Reference_Id__c = '12345',
            ProductCode__c = 'C106929'
        );
        cpList.add(contractedProduct);

        Contracted_Products__c contractedProductSIP = new Contracted_Products__c(
            Quantity__c = 1,
            Arpu_Value__c = 10,
            Duration__c = 1,
            Product__c = productSIP.Id,
            VF_Contract__c = contr.Id,
            Site__c = site.Id,
            Order__c = order.Id,
            CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ,
            PBX__c = compAsset.Id,
            External_Reference_Id__c = '67890',
            ProductCode__c = 'C106929',
            Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED
        );
        cpList.add(contractedProductSIP);
        insert cpList;

        TriggerHandler.resetAlreadyModified();
        TestUtils.orderChangeStatus(order.Id, Constants.ORDER_STATUS_SUBMITTED);

        setAuthenticationInSetting();
    }

    @isTest
    private static void verifyContractedProduct() {
        List<Customer_Asset__c> customerAssets = [
            SELECT Id, Installed_Base_Id__c
            FROM Customer_Asset__c
            LIMIT 2
        ];
        System.assertEquals(2, customerAssets.size(), 'Size must be 2');

        // verify no error on contracted product
        List<Contracted_Products__c> contractedProducts = [
            SELECT
                Id,
                PBX_Change_Error_Info__c,
                PBX__r.Assigned_Product_Id__c,
                Billing_Offer_synced_to_Unify_timestamp__c,
                External_Reference_Id__c,
                Customer_Asset__c
            FROM Contracted_Products__c
            LIMIT 2
        ];
        System.assertEquals(2, contractedProducts.size(), 'Size must be 2');

        Contracted_Products__c contractedProduct = contractedProducts[0];
        System.assertEquals(null, contractedProduct.PBX_Change_Error_Info__c, 'Error must be null');
        System.assertEquals(
            null,
            contractedProduct.Billing_Offer_synced_to_Unify_timestamp__c,
            'Timestamp empty'
        );
        System.assertNotEquals(
            null,
            contractedProduct.Customer_Asset__c,
            'Asset must be populated'
        );

        // verify fields which is required by service to update the right contracted product
        System.assertNotEquals(null, contractedProduct.PBX__c, 'PBX must be populated');
        System.assertNotEquals(
            null,
            contractedProduct.PBX__r.Assigned_Product_Id__c,
            'Assigned product must be populated'
        );
        System.assertNotEquals(
            null,
            contractedProduct.External_Reference_Id__c,
            'External reference must be populated'
        );
    }

    @IsTest
    private static void validatePositiveStatusCode200() {
        Contracted_Products__c contractedProduct = getFirstContractedProduct();
        External_WebService_Config__c config = getConfig();

        SiasService.ChangePBXOrderSuccessResponse response = getResponse(
            contractedProduct.PBX__r.Assigned_Product_Id__c,
            contractedProduct.Pbx__c,
            null
        );

        TestUtilMultiRequestMock.setSingleMock(config.URL__c, 200, JSON.serialize(response));

        Test.startTest();

        // change the status run once flag as trigger may already ran
        TriggerHandler.resetAlreadyModified();

        // change the status to 'Project Created', this would then call the batch's execute method in future context and do the processing of records
        changeDeliveryStatus(
            contractedProduct.Id,
            Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED
        );

        Test.stopTest();

        List<Contracted_Products__c> contractedProducts = getContractedProducts();

        System.assertEquals(2, contractedProducts.size(), 'Size must be 2');
        contractedProduct = contractedProducts[0];

        // there must be no error on contracted product
        System.assertEquals(null, contractedProduct.PBX_Change_Error_Info__c, 'Error must be null');
        // unify time stamp must be populated
        System.assertNotEquals(
            null,
            contractedProduct.Billing_Offer_synced_to_Unify_timestamp__c,
            'Timestamp missing'
        );
        // unify time stamp on PBX must be populated
        System.assertNotEquals(
            null,
            contractedProduct.PBX__r.Billing_Offer_synced_to_Unify_timestamp__c,
            'Timestamp missing'
        );
    }

    @IsTest
    private static void validateNegativeLineItemStatusCode200() {
        Contracted_Products__c contractedProduct = getFirstContractedProduct();
        External_WebService_Config__c config = getConfig();

        // prepare response
        SiasService.ErrorInfo errorInfo = new SiasService.ErrorInfo(
            'errorCode',
            'errorDescription',
            'targetSystemName',
            'targetServiceName'
        );
        SiasService.ChangePBXOrderSuccessResponse response = getResponse('', '', errorInfo);

        // set mock
        TestUtilMultiRequestMock.setSingleMock(config.URL__c, 200, JSON.serialize(response));

        // start testing
        Test.startTest();

        // change the status run once flag as trigger may already ran
        TriggerHandler.resetAlreadyModified();

        // change the status to 'Project Created', this would then call the batch's execute method in future context and do the processing of records
        changeDeliveryStatus(
            contractedProduct.Id,
            Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED
        );

        Test.stopTest();

        List<Contracted_Products__c> contractedProducts = getContractedProducts();

        System.assertEquals(2, contractedProducts.size(), 'Size must be 2');
        contractedProduct = contractedProducts[0];

        System.assertNotEquals(
            null,
            contractedProduct.PBX_Change_Error_Info__c,
            'Error can not be null'
        );
        System.assertEquals(
            null,
            contractedProduct.Billing_Offer_synced_to_Unify_timestamp__c,
            'Timestamp must be populated'
        );
    }

    @IsTest
    private static void validateNegativeStatusCodeNot200() {
        Contracted_Products__c contractedProduct = getFirstContractedProduct();
        External_WebService_Config__c config = getConfig();

        TestUtilMultiRequestMock.setSingleMock(config.URL__c, 400, '"Error message"');

        Test.startTest();

        // change the status run once flag as trigger may already ran
        TriggerHandler.resetAlreadyModified();

        // change the status to 'Project Created', this would then call the batch's execute method in future context and do the processing of records
        changeDeliveryStatus(
            contractedProduct.Id,
            Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED
        );

        Test.stopTest();

        List<Contracted_Products__c> contractedProducts = getContractedProducts();
        System.assertEquals(2, contractedProducts.size(), 'Size must be 2');
        contractedProduct = contractedProducts[0];

        // there must be an error on contracted product
        System.assertNotEquals(
            null,
            contractedProduct.PBX_Change_Error_Info__c,
            'Error must be populated'
        );
        // unify time stamp must not be populated
        System.assertEquals(
            null,
            contractedProduct.Billing_Offer_synced_to_Unify_timestamp__c,
            'Timestamp must not be populated'
        );
    }

    @isTest
    private static void coverBatchStartFinish() {
        Test.startTest();
        ChangePBXOrderBatch changePBXOrderBatchInstance = new ChangePBXOrderBatch(new Set<Id>());
        Database.QueryLocator queryLocator = changePBXOrderBatchInstance.start(null);
        changePBXOrderBatchInstance.finish(null);
        Test.stopTest();

        System.assertNotEquals(null, queryLocator, 'Must not be null');
    }

    /**
     * create custom setting with integration data
     */
    private static void setAuthenticationInSetting() {
        External_WebService_Config__c config = new External_WebService_Config__c(
            Name = SiasService.INTEGRATION_SETTING_NAME,
            URL__c = MOCK_URL,
            Username__c = 'username',
            Password__c = 'password'
        );
        insert config;
    }

    private static External_WebService_Config__c getConfig() {
        External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(
            SiasService.INTEGRATION_SETTING_NAME
        );
        return webServiceConfig;
    }

    /**
     * generate positive response
     */
    @SuppressWarnings(
        'PMD.FormalParameterNamingConventions'
    ) // APID property must be all uppercase because class is used for JSON deserialization
    public static SiasService.ChangePBXOrderSuccessResponse getResponse(
        String APID,
        String externalReferenceID,
        SiasService.ErrorInfo errorInfo
    ) {
        SiasService.OrderLineListType orderLineListType = new SiasService.OrderLineListType(
            APID,
            externalReferenceID,
            errorInfo
        );

        SiasService.ChangePBXOrderRespData changePBXOrderResponse = new SiasService.ChangePBXOrderRespData(
            new List<SiasService.OrderLineListType>{ orderLineListType }
        );

        return new SiasService.ChangePBXOrderSuccessResponse(changePBXOrderResponse);
    }

    /**
     * verify delivery status change
     */
    private static void changeDeliveryStatus(Id cpId, String status) {
        Contracted_Products__c contractedProduct = new Contracted_Products__c(
            Id = cpId,
            Delivery_Status__c = status
        );

        update contractedProduct;
    }

    /**
     * Get all contracted products
     */
    private static List<Contracted_Products__c> getContractedProducts() {
        List<Contracted_Products__c> contractedProducts = [
            SELECT
                Id,
                PBX_Change_Error_Info__c,
                PBX__r.Assigned_Product_Id__c,
                PBX__r.Billing_Offer_synced_to_Unify_timestamp__c,
                Billing_Offer_synced_to_Unify_timestamp__c,
                External_Reference_Id__c
            FROM Contracted_Products__c
            LIMIT 2
        ];
        return contractedProducts;
    }

    /**
     * Get first contracted product
     */
    private static Contracted_Products__c getFirstContractedProduct() {
        return getContractedProducts()[0];
    }
}