/**
 * @description:    Order summary counter update handler
 * @testClass:      TestEMP_OrderSummaryCounterUpdateHandler
 **/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class EMP_OrderSummaryCounterUpdateHandler implements CSPOFA.ExecutionHandler {
	/**
	 * @description     Implementation of process method
	 * @param           orchestratorSObjectSteps - list of orchestrator steps
	 * @return          List<SObject> - list of orchestrator processed steps
	 */
	public List<SObject> process(List<SObject> orchestratorSObjectSteps) {
		List<CSPOFA__Orchestration_Step__c> orchestratorSteps = (List<CSPOFA__Orchestration_Step__c>) orchestratorSObjectSteps;

		OrchUtils.getProcessDetails(GeneralUtils.getIDSetFromList(orchestratorSteps, 'CSPOFA__Orchestration_Process__c'));

		Map<String, Id> processIdPerOrderId = this.getProcessIdPerOrderId(OrchUtils.mapOrderId);
		List<Order__c> ordersForCounterUpdate = this.getOrdersForCounterUpdate(processIdPerOrderId?.keySet());

		this.updateOrderSummaryCounterContext(ordersForCounterUpdate);

		Map<String, CSPOFA__Orchestration_Step__c> orchestratorStepPerProcessId = this.getOrchestratorStepPerProcessId(orchestratorSteps);

		List<Database.SaveResult> orderForCounterUpdateUpdateResults = Database.update(ordersForCounterUpdate, false);

		return this.buildOrchestratorStepProcessingResults(orderForCounterUpdateUpdateResults, orchestratorStepPerProcessId, processIdPerOrderId);
	}

	/**
	 * @description     Retrieves process id per order id
	 * @param           Map<Id, String> orderIdPerProcessId - order id per process id
	 * @return          Map<String, Id> - process id per order id
	 */
	private Map<String, Id> getProcessIdPerOrderId(Map<Id, String> orderIdPerProcessId) {
		Map<String, Id> processIdPerOrderId = new Map<String, Id>();

		if (orderIdPerProcessId != null && !orderIdPerProcessId.isEmpty()) {
			for (Id processId : orderIdPerProcessId.keySet()) {
				processIdPerOrderId.put(orderIdPerProcessId.get(processId), processId);
			}
		}

		return processIdPerOrderId;
	}

	/**
	 * @description     Retrieves orders for counter update
	 * @param           orderIds - set of order ids
	 * @return          List<Order__c> - list of orders for counter update
	 */
	private List<Order__c> getOrdersForCounterUpdate(Set<String> orderIds) {
		return [SELECT Id, Name, Order_Summary_Current_Counter__c, Order_Summary_Max_Counter__c FROM Order__c WHERE Id IN :orderIds];
	}

	/**
	 * @description     Updates order summary counter context
	 * @param           ordersForCounterUpdate - list of orders for counter update
	 */
	private void updateOrderSummaryCounterContext(List<Order__c> ordersForCounterUpdate) {
		for (Order__c orderForCounterUpdate : ordersForCounterUpdate) {
			if (orderForCounterUpdate.Order_Summary_Current_Counter__c == null || orderForCounterUpdate.Order_Summary_Max_Counter__c == null) {
				orderForCounterUpdate.Order_Summary_Current_Counter__c = 1;
				orderForCounterUpdate.Order_Summary_Max_Counter__c = 1;
			}

			orderForCounterUpdate.Order_Summary_Current_Counter__c -= 1;

			if (orderForCounterUpdate.Order_Summary_Current_Counter__c <= 0) {
				orderForCounterUpdate.Order_Summary_Max_Counter__c *= 2;
				orderForCounterUpdate.Order_Summary_Current_Counter__c = orderForCounterUpdate.Order_Summary_Max_Counter__c;
			}
		}
	}

	/**
	 * @description     Retrieves orchestrator step per process id
	 * @param           orchestratorSteps - list of orchestrator steps
	 * @return          Map<String, CSPOFA__Orchestration_Step__c> - orchestrator step per process id
	 */
	private Map<String, CSPOFA__Orchestration_Step__c> getOrchestratorStepPerProcessId(List<CSPOFA__Orchestration_Step__c> orchestratorSteps) {
		Map<String, CSPOFA__Orchestration_Step__c> orchestratorStepPerProcessId = new Map<String, CSPOFA__Orchestration_Step__c>();

		for (CSPOFA__Orchestration_Step__c orchestratorStep : orchestratorSteps) {
			orchestratorStepPerProcessId.put(orchestratorStep.CSPOFA__Orchestration_Process__c, orchestratorStep);
		}

		return orchestratorStepPerProcessId;
	}

	/**
	 * @description     Builds orchestrator step processing results
	 * @param           orderForCounterUpdateUpdateResults - list of Database.SaveResult
	 * @param           orchestratorStepPerProcessId - orchestrator step per process id
	 * @param           processIdPerOrderId - process id per order id
	 * @return          List<SObject> - list of orchestrator processed steps
	 */
	private List<SObject> buildOrchestratorStepProcessingResults(
		List<Database.SaveResult> orderForCounterUpdateUpdateResults,
		Map<String, CSPOFA__Orchestration_Step__c> orchestratorStepPerProcessId,
		Map<String, Id> processIdPerOrderId
	) {
		List<SObject> orchestratorStepProcessingResults = new List<SObject>();

		for (Database.SaveResult orderForCounterUpdateUpdateResult : orderForCounterUpdateUpdateResults) {
			CSPOFA__Orchestration_Step__c orchestratorStep = orchestratorStepPerProcessId.get(
				processIdPerOrderId?.get(orderForCounterUpdateUpdateResult?.getId())
			);

			String errorMessage = '';

			if (!orderForCounterUpdateUpdateResult.isSuccess()) {
				for (Database.Error err : orderForCounterUpdateUpdateResult.getErrors()) {
					errorMessage = err.getStatusCode() + ': ' + err.getMessage();
				}
			}

			orchestratorStepProcessingResults.add(
				OrchUtils.setStepRecord(orchestratorStep, !orderForCounterUpdateUpdateResult.isSuccess(), errorMessage)
			);
		}

		return orchestratorStepProcessingResults;
	}
}
