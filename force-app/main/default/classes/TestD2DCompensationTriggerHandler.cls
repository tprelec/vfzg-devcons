/**
 * @description       : Apex Test Class for the D2DCompensationTriggerHandler Apex Class
 * @author            : Filip Vucic (filip.vucic@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 03-11-2022
 * @last modified by  : Filip Vucic (filip.vucic@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                        Modification
 * 1.0   03-11-2022   Filip Vucic (filip.vucic@vodafoneziggo.com)   Initial Version
 **/
@isTest
public class TestD2DCompensationTriggerHandler {
	@testSetup
	static void setup() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'D2D Compensation',
			External_Number__c = 1,
			Object_Prefix__c = 'D2D-58-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;
	}

	@isTest
	static void testSetExternalIds() {
		TestUtils.autoCommit = false;
		D2D_Compensation__c objD2DCompensation = TestUtils.createD2Dcompensation();
		Test.startTest();
		insert objD2DCompensation;
		Test.stopTest();
		D2D_Compensation__c objD2DCompensationUpdate = [SELECT Id, ExternalID__c FROM D2D_Compensation__c WHERE Id = :objD2DCompensation.Id LIMIT 1];
		System.assertEquals('D2D-58-000002', objD2DCompensationUpdate.ExternalID__c, 'The External ID on D2D Compensation was not set correctly.');
	}
}
