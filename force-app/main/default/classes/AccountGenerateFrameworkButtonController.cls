public class AccountGenerateFrameworkButtonController {

    @AuraEnabled
    public static void generateFrameworkForVodafoneAccount(Id accountId) {
        Account acc = new Account(Id = accountId);
        acc.Frame_Work_Agreement__c = AccountService.getInstance().generateFrameworkId(AccountService.FrameworkAgreementType.VODAFONE);
        update acc;
    }

    @AuraEnabled
    public static void generateFrameworkForVodafoneZiggoAccount(Id accountId) {
        Account acc = new Account(Id = accountId);
        acc.VZ_Framework_Agreement__c = AccountService.getInstance().generateFrameworkId(AccountService.FrameworkAgreementType.VODAFONE_ZIGGO);
        update acc;
    }

}