public with sharing class OrderEntryAddOnTriggerHandler extends TriggerHandler {
	private List<OE_Add_On__c> newOEAddOns;
	private Map<Id, OE_Add_On__c> newOEAddonsMap;
	private Map<Id, OE_Add_On__c> oldOEAddonsMap;

	private void init() {
		newOEAddOns = (List<OE_Add_On__c>) this.newList;
		newOEAddonsMap = (Map<Id, OE_Add_On__c>) this.newMap;
		oldOEAddonsMap = (Map<Id, OE_Add_On__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	public void setExternalIds() {
		Sequence seq = new Sequence('OE_Add_On');

		if (seq.canBeUsed()) {
			seq.startMutex();
			for (OE_Add_On__c o : newOEAddOns) {
				o.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}