/**
 * 	@description	This class contains unit tests for the GenerateTestDataSet class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestGenerateTestDataSet {
	
	@isTest
	private static void testDataGeneration(){

		GenerateTestDataSet.GenerateTestDataSet();
		
		// query some of the data
		List<Site__c> testsites = [select Id From Site__c Where Site_Account__r.Logo_URL__c = 'testdataset'];
		System.assert(testsites.size() > 0);
	}
}