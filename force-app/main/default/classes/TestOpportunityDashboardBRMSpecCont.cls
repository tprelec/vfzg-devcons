@isTest
private class TestOpportunityDashboardBRMSpecCont {

    @isTest
    static void testSolSalesJson() {

        User accMgr = TestUtils.createManager();
        UserRole mgrRole = [SELECT Id, Name FROM UserRole WHERE Id = :accMgr.UserRoleId];
        GeneralUtils.roleIdToRoleName.put(accMgr.UserRoleId, mgrRole.Name);

        Role_Segment_Mapping__c segmentMapping = new Role_Segment_Mapping__c(
            Name = mgrRole.Name,
            Main_Segment__c = 'Solution Sales',
            Segment__c = 'Cloud Productivity Solution Sales'
        );

        insert segmentMapping;

        String fiscalYear = String.valueOf(System.today().addmonths(-12).year());
        Test.setCurrentPage(Page.OpportunityDashboardBRMSpecialist);

        String cookieString = OpportunityDashboardUtils.getBrmSettingCookieString('specialist', mgrRole.Id, fiscalYear, 'License');
        Cookie brmSettings = new Cookie('brmSettingsSpecialist', cookieString, null, -1, true, 'Strict');

        ApexPages.currentPage().setCookies(new Cookie[] { brmSettings });

        TestUtils.createOrderValidationOpportunity();
        TestUtils.createOpportunityFieldMappings();
        TestUtils.createCompleteOpportunity();
        VF_Product__c vfProduct = new VF_Product__c(
            Family__c = 'Fixed Data',
            Product_Line__c = 'fVodafone',
            ExternalID__c = '1234567890',
            OrderType__c = TestUtils.theOrderType.Id,
            AllowsGroupDataSharing__c = 'Yes'
        );
        insert vfProduct;

        TestUtils.autoCommit = false;
        Product2 product = TestUtils.createProduct();
        product.VF_Product__c = vfProduct.Id;
        insert product;
        TestUtils.autoCommit = true;

        Opportunity opp = [SELECT Id, StageName, Partner_Account__c, CloseDate FROM Opportunity WHERE Id = :TestUtils.theOpportunity.Id];

        PricebookEntry pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), product);
        TestUtils.createOpportunityLineItem(opp, pbe);

        opp.Solution_Sales__c = accMgr.Id;
        opp.CloseDate = System.today();
        update opp;

        Test.startTest();

        opp.StageName = 'Closed Won';
        update opp;

        String solJson;

        // run as sales mgr to prevent heap size error!
        System.runAs(TestUtils.createAccountManager()) {
            OpportunityDashboardBRMSpecController odbc = new OpportunityDashboardBRMSpecController();

            odbc.dataTypeSelected = 'MRR';
            odbc.fiscalYearInteger = System.today().year();
            odbc.fiscalQuarterSelected = '1';
            odbc.verticalSelected = 'Health';

            solJson = odbc.solSalesDatapointsJSON;
        }

        Test.stopTest();

        System.assertNotEquals(null, solJson, 'Json should not be empty');
    }

    @isTest
    static void testAllJsons () {
        String test1,
               test2,
               test3,
               test4,
               test5,
               test6,
               test7;

        Test.startTest();

        // run as sales mgr to prevent heap size error!
        System.runAs(TestUtils.createAccountManager()) {
            OpportunityDashboardBRMSpecController odbc = new OpportunityDashboardBRMSpecController();

            odbc.dataTypeSelected = 'MRR';
            odbc.fiscalYearInteger = System.today().year();
            odbc.fiscalQuarterSelected = '1';
            odbc.verticalSelected = 'Health';

            odbc.getDataTypeOptions();
            odbc.getDashboardTypeOptions();
            odbc.getTeamOptions();
            odbc.initializeVariables();
            odbc.activateSecondaryTabs();
            odbc.getAccountManagerOptions();
            odbc.getVerticalOptions();
            odbc.getCorporateSolSalesSegmentFilter();
            odbc.getSMEDSSolSalesSegmentFilter();
            odbc.getSMEIDSSolSalesSegmentFilter();
            odbc.getCorpSMESolSalesSegmentFilter();
            odbc.getCloudProdSolSalesSegmentFilter();
            odbc.getEntServSolSalesSegmentFilter();

            test1 = odbc.msSegmentDatapointsJSON;
            test2 = odbc.msDatapointsJSON;
            test3 = odbc.miteSegmentDatapointsJSON;
            test4 = odbc.miteDatapointsJSON;
            test5 = odbc.solSalesSegmentDatapointsJSON;
            test6 = odbc.solSalesDatapointsJSON;
            test7 = odbc.corpSolSalesSegmentDatapointsJSON;

            odbc.getFiscalYearOptions();
            odbc.loadDashboard();
        }

        Test.stopTest();

        System.assertNotEquals(null, test1, 'Json not null');
        System.assertNotEquals(null, test2, 'Json not null');
        System.assertNotEquals(null, test3, 'Json not null');
        System.assertNotEquals(null, test4, 'Json not null');
        System.assertNotEquals(null, test5, 'Json not null');
        System.assertNotEquals(null, test6, 'Json not null');
        System.assertNotEquals(null, test7, 'Json not null');
    }

    @isTest
    static void testDatapointClassMethods () {
        OpportunityDashboardBRMSpecController odbc = new OpportunityDashboardBRMSpecController();

        OpportunityDashboardBRMSpecController.Datapoint dp = new OpportunityDashboardBRMSpecController.Datapoint();
        dp.Amount = 100;
        dp.AccountManager = 'Test manager';
        dp.Segment = 'test';
        dp.ProductFamily = 'test';
        dp.Type = 'test';
        dp.sType = 'test';
        dp.Period = 'test';

        String period = dp.Period;
        OpportunityDashboardBRMSpecController.Datapoint dp2 = dp.clone();

        Map<String, OpportunityDashboardBRMSpecController.Datapoint> typeDatapointMap = new Map<String, OpportunityDashboardBRMSpecController.Datapoint>();
        typeDatapointMap.put('test', dp);

        odbc.addTypeAmountToMap('test', dp, typeDatapointMap);
        System.assertEquals(200, typeDatapointMap.get('test').Amount, 'Amount should be 200. Current amount: ' + typeDatapointMap.get('test').Amount);

        String gapType = 'Gap';
        System.assertEquals(false, typeDatapointMap.containsKey(gapType), 'No such key: Gap');

        odbc.addTypeAmountToMap('TargetGap', dp2, typeDatapointMap);
        System.assertEquals(true, typeDatapointMap.containsKey(gapType), 'Key should exists: Gap');
        System.assertEquals(-100, typeDatapointMap.get(gapType).Amount, 'Amount should be negative. Current amount: ' + typeDatapointMap.get(gapType).Amount);

        odbc.addTypeToMap('Prospect', dp, typeDatapointMap);
        odbc.addTypeToMap('Qualify', dp, typeDatapointMap);
        odbc.addTypeToMap('Offer', dp, typeDatapointMap);
        odbc.addTypeToMap('Closing', dp, typeDatapointMap);
        odbc.addTypeToMap('Closing by SAG', dp, typeDatapointMap);
        odbc.addTypeToMap('Closed Won', dp, typeDatapointMap);
        odbc.addTypeToMap('Closed Lost', dp, typeDatapointMap);
        odbc.addTypeToMap('Target', dp, typeDatapointMap);

        List<String> typeKeys = new List<String> {
            'Funnel', 'Coverage', 'Offer', 'Commit', 'Gap', 'Closed Won', 'Closed Lost', 'Target'
        };

        for (String type : typeKeys) {
            System.assertEquals(true, typeDatapointMap.containsKey(type), 'Key should exist: ' + type);
        }
    }
}