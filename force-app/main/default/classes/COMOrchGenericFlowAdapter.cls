global class COMOrchGenericFlowAdapter implements CSPOFA.FlowAdapter {

  Flow.Interview.COMOrchGeneric comGenericFlow;

  global void start(Map<String, Object> variableMap) {
	comGenericFlow = new Flow.Interview.COMOrchGeneric(variableMap);
	comGenericFlow.start();
  }
  
  global Object getVariableValue(String variableName) {
  	return comGenericFlow.getVariableValue(variableName);
  }
}