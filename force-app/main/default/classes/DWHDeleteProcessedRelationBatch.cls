global class DWHDeleteProcessedRelationBatch implements Database.Batchable<sObject>, Schedulable{
    static Date today5 = Date.today().addDays(-5);

    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('Select ID from Customer_Relations_Potential__c where CreatedDate < :today5');
    }

    public void execute(Database.BatchableContext context, List<SObject> records) {
        system.debug('##records: '+records);
        delete records;
    }

    global void execute(SchedulableContext ctx) {
        List<DWH_Relation_SF_Interface__c> relTmpList = [SELECT Id FROM DWH_Relation_SF_Interface__c WHERE Processed__c = true LIMIT 10];
        if (!relTmpList.isEmpty()) {
            Database.executeBatch(new DWHDeleteProcessedRelationBatch(), 100);
        }
    }

    global void finish(Database.BatchableContext BC) {}
}