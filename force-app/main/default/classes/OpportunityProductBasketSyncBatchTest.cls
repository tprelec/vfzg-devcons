/**
 * Created by miaaugustinovic on 26/04/2021.
 */
@isTest
public with sharing class OpportunityProductBasketSyncBatchTest {

    private static testMethod void test() {

        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs (simpleUser) {

            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', simpleUser.id);
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            basket.Primary__c = true;
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.ProfitLoss_JSON__c = '{"KPI Direct Capex / Net Revenue":"0,00","KPI Payback Period(Months)":"0,00","KPI NPV":"0,00",' +
                    '"KPI Net Incremental Billed Revenue":"0,00","KPI Free cash flow / Net revenue":"1,50","KPI EBITDA / Net revenue":"1,56",' +
                    '"KPI Sales Margin / Net revenue":"1,71","NET RESULT":"56.993,07","NET RESULT first":"58.996,33","Capex":"1.228,43","Network Depreciation":"774,83",' +
                    '"Direct Capex":"0,00","EBITDA":"58.996,33","EBITDA first":"62.203,26","Overhead allocation":"3.206,93","Network Opex":"2.819,96",' +
                    '"Incremental EBITDA":"65.023,22","Incremental OPEX":"0,00","Total Gross Margin":"65.023,22","Total costs of sales":"10.816,78","A&R":"0,00",' +
                    '"Revenue share":"0,00","Other Costs":"0,00","Opportunity cost":"0,00","Opportunity cost Fixed":"0,00","Opportunity cost Mobile":"0,00",' +
                    '"Total cost of sales":"10.816,78","Total Net Revenue":"75.840,00","Total discounts":"0,00","Benchmark":"0,00",' +
                    '"Total credit amount / loyalty bonus":"0,00","Benchmark Fixed":"0,00","Benchmark Mobile":"0,00",' +
                    '"Operator other costs / other migration costs":"0,00","Operator other costs / other migration costs Fixed":"0,00",' +
                    '"Operator other costs / other migration costs Mobile":"0,00","Discounts":"0,00","Total Gross Revenue (SUM)":"75.840,00",' +
                    '"Total Gross Revenue for Operator other costs / other migration costs":"0,00","Total Gross Revenue":"75.840,00"}';
            insert basket;

            test.startTest();

            OpportunityProductBasketSyncBatch controller = new OpportunityProductBasketSyncBatch();
            Database.executeBatch(controller, 1);

            test.stopTest();
        }
    }

    private static testMethod void canSchedule() {

        OpportunityProductBasketSyncBatch controller = new OpportunityProductBasketSyncBatch();
        String start = '0 0 23 * * ?';
        system.schedule('Test OpportunityProductBasketSyncBatch', start, controller);

    }
}