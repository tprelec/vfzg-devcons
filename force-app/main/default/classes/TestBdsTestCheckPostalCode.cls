@isTest
public with sharing class TestBdsTestCheckPostalCode {
	@isTest
	public static void testCheckPostalCode() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		acct.KVK_number__c = '01234567';

		Site__c s = new Site__c();
		s.Site_Account__c = acct.Id;
		s.Site_Street__c = 'existingSiteStreet';
		s.Site_City__c = 'Testville';
		s.Site_Postal_Code__c = '3511WR';
		s.Site_House_Number__c = 128;
		s.Site_House_Number_Suffix__c = 'a';
		insert s;

		Site_Postal_Check__c spc = new Site_Postal_Check__c();
		spc.Access_Site_ID__c = s.Id;
		spc.Access_Vendor__c = 'DLM Entry';
		spc.Access_Max_Up_Speed__c = 232323;
		spc.Access_Max_Down_Speed__c = 676767;
		spc.Technology_Type__c = 'fiber';
		spc.Accessclass__c = 'E2E';
		//spc.Access_Active_and_Usable__c = postalCodeCheckAccessActiveAndUsable[i];
		spc.Access_Result_Check__c = 'ONNET';
		spc.Access_Priority__c = 2;
		//spc.Result_Check_Generic__c = postalCodeCheckResultCheckGeneric[i];
		spc.Access_Infrastructure__c = 'VVDSL';
		spc.Access_Active__c = true;
		insert spc;

		Test.startTest();
		BdsTestCheckPostalCode.checkPostalCodeService('3511WR', 128, '');
		Test.stopTest();

		System.assert(RestContext.response.statusCode == 200, 'We should get a positive response');
	}

	@isTest
	public static void testNegativeResult() {
		Test.startTest();
		BdsTestCheckPostalCode.checkPostalCodeService('null', 1, '');
		Test.stopTest();

		System.assert(RestContext.response.statusCode == 400, 'We should get a negative response');
	}
}
