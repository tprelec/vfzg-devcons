@isTest
private class TestVF_ContractTiersController {
	@isTest
	static void testPageController() {
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);

		// create contract
		TestUtils.createCompleteContract();
		VF_Contract__c theContract = TestUtils.theContract;

		// load page
		ApexPages.StandardController controller = new ApexPages.StandardController(theContract);
		VF_ContractTiersController vctc = new VF_ContractTiersController(controller);

		PageReference pr = Page.VF_ContractTiers;
		pr.getParameters().put('Id', theContract.Id);
		Test.setCurrentPage(pr);

		Test.startTest();

		Boolean checkReadonly = vctc.readOnly;
		// fetch the current groups
		List<ContractTierGroupWrapper> theTierGroups = vctc.theTierGroups;

		// (edit legal fields) and update
		vctc.saveContract();

		// add tier type and refresh
		vctc.addType();
		theTierGroups = vctc.theTierGroups;

		VF_ContractTierEditorController vctec = new VF_ContractTierEditorController();
		vctec.theWrapper = theTierGroups[0];

		// add tiers
		vctec.addRow();
		vctec.addRow();

		// edit tier
		theTierGroups = vctc.theTierGroups;
		theTierGroups[0].theTiers[0].Usage_From__c = '123';
		vctec.theWrapper = theTierGroups[0];
		vctec.saveChanges();

		// requery and check edited tier
		theTierGroups = vctc.theTierGroups;
		system.debug(theTierGroups);
		system.assertEquals('123', theTierGroups[0].theTiers[0].Usage_From__c);

		// remove tier
		//vctec.rowNum = 0;
		pr = Page.VF_ContractTiers;
		pr.getParameters().put('Id', theContract.Id);
		pr.getParameters().put('index', '0');
		Test.setCurrentPage(pr);
		vctec.delRow();
		// requery and check removed tier
		theTierGroups = vctc.theTierGroups;
		system.debug(theTierGroups);
		system.assertEquals(1, theTierGroups[0].theTiers.size());

		// reload in component and remove entire tier
		vctec.theWrapper = theTierGroups[0];
		vctec.deleteAll();
		// requery and check removed tiers
		theTierGroups = vctc.theTierGroups;
		system.debug(theTierGroups);
		system.assertEquals(0, theTierGroups.size());

		Test.stopTest();

		// go back to contract
		vctc.backToContract();
	}
}
