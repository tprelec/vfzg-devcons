@isTest
private class CustomButtonContractRelatedQuestionsTest {
	private static testMethod void test() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Test.startTest();

			No_Triggers__c notriggers = new No_Triggers__c();
			notriggers.SetupOwnerId = simpleUser.Id;
			notriggers.Flag__c = true;
			insert notriggers;

			Account tmpAcc = CS_DataTest.createAccount('Test Account');
			insert tmpAcc;

			Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', simpleUser.Id);
			insert tmpOpp;

			cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
			insert tmpProductBasket;

			CustomButtonContractRelatedQuestions contractQuestions = new CustomButtonContractRelatedQuestions();
			contractQuestions.performAction(tmpProductBasket.Id);

			Test.stopTest();
		}
	}

	@isTest
	private static void testContractValidation() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Test.startTest();

			No_Triggers__c notriggers = new No_Triggers__c();
			notriggers.SetupOwnerId = simpleUser.Id;
			notriggers.Flag__c = true;
			insert notriggers;

			Account tmpAcc = CS_DataTest.createAccount('Test Account');
			tmpAcc.VZ_Framework_Agreement__c = 'VZ-0015';
			tmpAcc.Frame_Work_Agreement__c = 'VF-0001';
			insert tmpAcc;

			Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', simpleUser.Id);
			tmpOpp.Deal_Type__c = 'acquisition';
			tmpOpp.Segment__c = 'SoHo';
			tmpOpp.DLRS_WholeSale_Only_Products__c = 0;
			tmpOpp.No_Contract__c = false;
			//tmpOpp.Acq_SUM_Total_Price__c = 100;  //gets from Opp Line Item
			//tmpOpp.Credit_Check_Status_Number__c = 1; //gets from Credit Check Object
			insert tmpOpp;

			OrderType__c tmpOrderType = new OrderType__c();
			tmpOrderType.Name = 'MAC';
			tmpOrderType.Status__c = 'New';
			tmpOrderType.ExternalID__c = '1234567890';
			tmpOrderType.ExportSystem__c = 'EMAIL';
			insert tmpOrderType;

			Product2 prod = new Product2(
				Name = 'Test product',
				Brand__c = 'Testing Brand',
				Cost_Price__c = 100,
				Description = 'This is a test product',
				IsActive = true,
				ProductCode = 'C999999',
				Product_Group__c = 'Priceplan',
				Quantity_type__c = 'Months',
				SDC_Product_matrix__c = 'Fixed - Data',
				CLC__c = 'Acq',
				Product_Line__c = 'fVodafone',
				Ordertype__c = tmpOrderType.id,
				ExternalID__c = 'P2-01-2923773'
			);
			insert prod;
			PriceBookEntry pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);

			OpportunityLineItem tmpOppLineItem = new OpportunityLineItem();
			tmpOppLineItem.OpportunityId = tmpOpp.Id;
			tmpOppLineItem.PricebookEntryId = pbe.Id;
			tmpOppLineItem.Product2Id = prod.Id;
			tmpOppLineItem.Quantity = 1;
			tmpOppLineItem.TotalPrice = 100;
			tmpOppLineItem.Deal_Type__c = 'New';
			tmpOppLineItem.CLC__c = 'Acq';
			tmpOppLineItem.Duration__c = 1;
			tmpOppLineItem.Product_Arpu_Value__c = 1;
			tmpOppLineItem.Gross_List_Price__c = 10;
			tmpOppLineItem.DiscountNew__c = 2;
			tmpOppLineItem.Cost__c = 3;
			tmpOppLineItem.Net_Revenue__c = 55;
			tmpOppLineItem.Net_Unit_Price__c = 10;
			tmpOppLineItem.Model_Number_Identifier__c = 'Test MNI';
			insert tmpOppLineItem;

			Credit_Check__c tmpCreditCheck = new Credit_Check__c();
			tmpCreditCheck.Opportunity__c = tmpOpp.Id;
			tmpCreditCheck.Credit_Check_Status_Number__c = 1;
			insert tmpCreditCheck;

			cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
			insert tmpProductBasket;

			CustomButtonContractRelatedQuestions contractQuestions = new CustomButtonContractRelatedQuestions();
			contractQuestions.performAction(tmpProductBasket.Id);
			Test.stopTest();
		}
	}
}
