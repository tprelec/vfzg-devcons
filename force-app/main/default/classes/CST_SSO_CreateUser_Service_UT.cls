@isTest
public class CST_SSO_CreateUser_Service_UT {
	@TestSetup
	static void makeData() {
		Profile profile = [SELECT id FROM Profile WHERE name = 'VF CST Customer'];
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.Unify_Ref_Id__c = 'testAccount1';
		update acc;

		Contact existingContact = TestUtils.createContact(acc);
		existingContact.Email = '123456abcde@mail.com';
		existingContact.Unify_Ref_Id__c = 'testContact1';
		update existingContact;

		Contact existingContact2 = TestUtils.createContact(acc);
		existingContact2.Email = '96587poiuyt@mail.com';
		existingContact2.Unify_Ref_Id__c = 'testContact2';
		update existingContact2;

		User existingUser = new User(
			profileId = profile.Id,
			username = '123456abcde' + system.now().millisecond() + '@mail.com',
			IsActive = true,
			contactId = existingContact.Id,
			email = '123456abcde@mail.com',
			alias = '1234',
			emailencodingkey = 'UTF-8',
			CommunityNickname = existingContact.Unify_Ref_Id__c,
			FirstName = 'Test',
			LastName = 'Class',
			LanguageLocaleKey = 'nl_NL',
			LocaleSidKey = 'nl_NL',
			TimeZoneSidKey = 'Europe/Amsterdam'
		);

		insert existingUser;
	}

	private static Blob getRequestBlob(String contactId, String accountId) {
		return Blob.valueOf(
			'<s11:Envelope xmlns:s11=\'http://schemas.xmlsoap.org/soap/envelope/\'><s11:Header><usr:authenticationHeader xmlns:usr=\'http://sb.ecs.vodafone.nl/servicebus/\'><username>???</username><password>???</password><applicationId>???</applicationId></usr:authenticationHeader></s11:Header><s11:Body><usr:createUsersRequest xmlns:usr=\'http://sb.ecs.vodafone.nl/servicebus/\'><user><company><banNumber>???</banNumber><corporateId>???</corporateId><bopCode>???</bopCode><referenceId>' +
			accountId +
			'</referenceId><accountId>?</accountId><externalSystem>???</externalSystem></company><email>userfortestclass@vodafoneziggo.com.test</email><referenceId>' +
			contactId +
			'</referenceId><additionalReferences><additionalReference><externalSystem>???</externalSystem><referenceId>???</referenceId></additionalReference></additionalReferences><prefix>Mr.</prefix><firstname>New</firstname><infix>???</infix><lastname>User1</lastname><language>Nederlands</language><groupEmail>???</groupEmail><jobTitle>Admin</jobTitle><address>Main Address</address><address_1>Other Address</address_1><address_2/><address_3/><zipcode>12345</zipcode><city>Amsterdam</city><county>Noord-Holland</county><countryId>Holland</countryId><phone>+351918556699</phone><mobile>0611596452</mobile><fax>08577596855</fax><notes>Here will lie the notes...</notes></user></usr:createUsersRequest></s11:Body></s11:Envelope>'
		);
	}

	@isTest
	static void testCreateUserAndContact() {
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		RestRequest request = new RestRequest();
		request.requestUri = baseUrl + '/services/apexrest/CreateUser';
		request.httpMethod = 'POST';
		request.requestBody = getRequestBlob('testUser', 'testAccount1');

		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		CST_SSO_CreateUser_Service.createUser();
		Test.stopTest();
	}

	@isTest
	static void testWrongAccount() {
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		RestRequest request = new RestRequest();
		request.requestUri = baseUrl + '/services/apexrest/CreateUser';
		request.httpMethod = 'POST';
		request.requestBody = getRequestBlob('testUser', '134545');
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		CST_SSO_CreateUser_Service.createUser();
		Test.stopTest();
	}

	@isTest
	static void testOnlyCreateUser() {
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		RestRequest request = new RestRequest();
		request.requestUri = baseUrl + '/services/apexrest/CreateUser';
		request.httpMethod = 'POST';
		request.requestBody = getRequestBlob('testContact2', 'testAccount1');

		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		CST_SSO_CreateUser_Service.createUser();
		Test.stopTest();
	}

	@isTest
	static void testActivateUser() {
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		RestRequest request = new RestRequest();
		request.requestUri = baseUrl + '/services/apexrest/CreateUser';
		request.httpMethod = 'POST';
		request.requestBody = getRequestBlob('testContact1', 'testAccount1');

		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		User usr = [SELECT id, username, IsActive FROM User WHERE CommunityNickname = 'testContact1' LIMIT 1];
		System.debug('testActivateUser -> usr ' + usr);
		usr.IsActive = false;
		update usr;
		Test.startTest();
		CST_SSO_CreateUser_Service.createUser();
		Test.stopTest();
	}

	@isTest
	static void testCreateNewUser() {
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		RestRequest request = new RestRequest();
		request.requestUri = baseUrl + '/services/apexrest/CreateUser';
		request.httpMethod = 'POST';
		request.requestBody = getRequestBlob('testContact1', 'testAccount1');

		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		User usr = [SELECT id, username, IsActive FROM User WHERE CommunityNickname = 'testContact1' LIMIT 1];
		usr.IsPortalEnabled = false;
		update usr;
		Test.startTest();
		CST_SSO_CreateUser_Service.createUser();
		Test.stopTest();
	}

	@isTest
	static void testAssignPermissionSets() {
		User usr = [SELECT id, username, IsActive FROM User WHERE CommunityNickname = 'testContact1' LIMIT 1];
		CST_SSO_CreateUser_Service.assignPermissionSets(usr.Id);
	}
}
