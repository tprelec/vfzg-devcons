public with sharing class SectionClauseDefAssTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<csclm__Section_Clause_Definition_Association__c> newProducts = (List<csclm__Section_Clause_Definition_Association__c>) this.newList;
		Sequence seq = new Sequence('Section/Clause Definition Ass');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (csclm__Section_Clause_Definition_Association__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
