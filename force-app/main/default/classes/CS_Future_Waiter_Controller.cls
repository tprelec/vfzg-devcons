public class CS_Future_Waiter_Controller
{
    public Boolean allChildTasksEnded = true;
    public Integer numberOfMillisecondsToWait {get; set;}
    public String basketId {get; set;}
    public String parentProcessId {get; set;}
    public cscfga__Product_Basket__c basket;
    public List<CS_Future__c> listOfChildProcesses {get; set;}
    public CS_Future__c parentProcess {get; set;}
    public Boolean failureFound;
    
    
    public CS_Future_Waiter_Controller() {
        Future_Optimisation__c optimisationSettings = Future_Optimisation__c.getInstance('Standard Optimisation');
        numberOfMillisecondsToWait = Integer.valueOf(optimisationSettings.Refresh_number_of_seconds__c) * 1000;
        
        basketId = System.currentPageReference().getParameters().get('basketId');
        parentProcessId = System.currentPageReference().getParameters().get('parentProcessId');
        failureFound = false;
        if(parentProcessId != null && parentProcessId != '') {
            parentProcess = [SELECT id, Success_Url__c, Failure_Url__c, Description__c, Status__c, Additional_Status_Information__c FROM CS_Future__c WHERE id = :parentProcessId];
            listOfChildProcesses = [SELECT id, Description__c, Status__c, Additional_Status_Information__c FROM CS_Future__c WHERE Parent_Process__c = :parentProcessId];
            
            parentProcess.Additional_Status_Information__c = '';
            
            for(CS_Future__c process : listOfChildProcesses) {
                if(process.Status__c != 'Failure' && process.Status__c != 'Finished') {
                    allChildTasksEnded = false;
                }
                
                if(process.Status__c == 'Failure') {
                    failureFound = true;
                    if(process.Additional_Status_Information__c != null) {
                        parentProcess.Additional_Status_Information__c += process.Additional_Status_Information__c + '; ';
                    }
                }
            }
        }
    }
    
    public PageReference checkFuture()
    {
        if(basketId != null && basketId != '') {
            if(allChildTasksEnded == true) {
                //PageReference pageRef = new PageReference('/apex/appro__VFSubmitPreview?id=' + basketId);
                PageReference pageRef;
                
                if(failureFound) {
                    pageRef = new PageReference(parentProcess.Failure_Url__c);
                    parentProcess.Status__c = 'Failure';
                    parentProcess.End_Date__c = DateTime.Now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
                } else {
                    pageRef = new PageReference(parentProcess.Success_Url__c);
                    parentProcess.Status__c = 'Finished';
                    parentProcess.End_Date__c = DateTime.Now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSZ');
                }
                
                update parentProcess;
                
                pageRef.setRedirect(true);
                return pageRef;
            }
        
        
            if(basketId != null && basketId != '' && parentProcessId != null && parentProcessId != '') {
                PageReference pageRef = ApexPages.currentPage();
                pageRef.setRedirect(true);
                return pageRef;
            }
        }
        
        return null;
    }
}