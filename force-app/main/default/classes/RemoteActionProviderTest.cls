/**
 * Created by miaaugustinovic on 09/04/2021.
 *
 * @description Test class for RemoteActionProvider class
 */
@IsTest
private with sharing class RemoteActionProviderTest {
	@testSetup
	private static void setup() {
		Account tmpAcc = CS_DataTest.createAccount('Account');
		tmpAcc.OwnerId = UserInfo.getUserId();
		insert tmpAcc;

		Opportunity tmpOpp = CS_DataTest.createOpportunity(
			tmpAcc,
			'New Opportunity',
			UserInfo.getUserId()
		);
		tmpOpp.Type_of_service__c = 'Mobile';
		tmpOpp.Segment__c = 'SoHo';
		tmpOpp.Document_Signature__c = 'Manual';
		insert tmpOpp;

		cscfga__Product_Basket__c productBasket = CS_DataTest.createProductBasket(
			tmpOpp,
			'VZ-1999-001'
		);
		productBasket.Primary__c = false;
		productBasket.Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]';
		productBasket.MobileScenarios__c = '[OneMobile]';
		productBasket.csordtelcoa__in_flight_change_type__c = 'Selective';
		insert productBasket;

		cspmb__Price_Item__c masterPI = new cspmb__Price_Item__c(
			Name = 'Master PI',
			cspmb__Is_Active__c = true
		);
		insert masterPI;

		Category__c category = CS_DataTest.createCategory('Business Internet');
		insert category;

		Vendor__c vendor = CS_DataTest.createVendor('ZIGGO');
		insert vendor;

		OrderType__c orderType = CS_DataTest.createOrderType('Complex order');
		insert orderType;

		Product2 product2 = CS_DataTest.createProduct2('P2', 'The Simpsons', orderType, true);
		insert product2;

		cspmb__Price_Item__c lowerBand = CS_DataTest.createPriceItem(
			product2,
			category,
			30,
			100,
			vendor,
			null,
			null
		);
		lowerBand.Name = 'Coax ansulting 100/30';
		lowerBand.cspmb__Product_Definition_Name__c = 'Access';
		lowerBand.cspmb__Effective_Start_Date__c = Date.newInstance(2021, 3, 4);
		lowerBand.cspmb__Effective_End_Date__c = Date.newInstance(2029, 12, 21);
		lowerBand.cspmb__Contract_Term__c = '12 Months';
		lowerBand.Access_Type_Multi__c = 'Coax';
		lowerBand.cspmb__Master_Price_item__c = masterPI.Id;

		cspmb__Price_Item__c siteConnectPriceItem = CS_DataTest.createPriceItem(
			product2,
			category,
			30,
			100,
			vendor,
			null,
			null
		);
		siteConnectPriceItem.Name = 'Coax ansulting 100/30';
		siteConnectPriceItem.cspmb__Product_Definition_Name__c = 'Site Connect';
		siteConnectPriceItem.cspmb__Effective_Start_Date__c = Date.newInstance(2021, 3, 4);
		siteConnectPriceItem.cspmb__Effective_End_Date__c = Date.newInstance(2029, 12, 21);
		siteConnectPriceItem.cspmb__Contract_Term__c = '12 Months';
		siteConnectPriceItem.Access_Type_Multi__c = 'Coax';
		siteConnectPriceItem.cspmb__Master_Price_item__c = masterPI.Id;

		cspmb__Price_Item__c higherBand = CS_DataTest.createPriceItem(
			product2,
			category,
			50,
			1000,
			vendor,
			null,
			null
		);
		higherBand.Name = 'Coax ansulting 1000/50';
		higherBand.cspmb__Product_Definition_Name__c = 'Access';
		higherBand.cspmb__Effective_Start_Date__c = Date.newInstance(2021, 3, 4);
		higherBand.cspmb__Effective_End_Date__c = Date.newInstance(2029, 12, 21);
		higherBand.cspmb__Contract_Term__c = '12 Months';
		higherBand.Access_Type_Multi__c = 'Coax';
		higherBand.cspmb__Master_Price_item__c = masterPI.Id;

		List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>{
			lowerBand,
			higherBand,
			siteConnectPriceItem
		};
		insert priceItems;
	}

	private static cscfga__Product_Basket__c getBasket() {
		return [
			SELECT
				Id,
				Name,
				cscfga__Opportunity__r.Direct_Indirect__c,
				csbb__Account__c,
				csordtelcoa__Account__c,
				csordtelcoa__in_flight_change_type__c,
				csordtelcoa__Change_Type__c,
				Primary__c
			FROM cscfga__Product_Basket__c
			LIMIT 1
		];
	}

	@isTest
	private static void testGetProductBasketDetailsPositive() {
		cscfga__Product_Basket__c productBasket = getBasket();

		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'productBasketDetails',
			'productBasketId' => productBasket.Id
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();

		System.assertEquals(
			'Direct',
			String.valueOf(result.get('opportunityDirectIndirect')),
			'Invalid Opp. Direct/Indirect value returned.'
		);
	}

	@isTest
	private static void testGetProductBasketDetailsNegative() {
		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'productBasketDetails',
			'productBasketId' => null
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();

		system.assertEquals(false, result.get('status'), 'Status is not updated properly');
	}

	@isTest
	private static void testGetProductBasketDetailsInflight() {
		cscfga__Product_Basket__c productBasket = getBasket();

		cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = productBasket.Id
		);
		insert config;

		cscfga__Attribute__c att = new cscfga__Attribute__c(
			cscfga__Product_Configuration__c = config.Id,
			Name = 'GUID',
			cscfga__Value__c = 'GUID VAL 1'
		);
		insert att;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(config.Id);
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(
			config.Id,
			subscription.Id,
			'Service_a7Z1l000000A3nSEAS_0'
		);
		service.Applicable_for_inflight_change__c = false;
		insert service;

		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'productBasketDetails',
			'productBasketId' => productBasket.Id
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();

		System.assertNotEquals(
			null,
			String.valueOf(result.get('configurations')),
			'Inflight information expected'
		);
	}

	@isTest
	private static void testApplyAutomaticDiscount() {
		cscfga__Product_Basket__c basket = getBasket();

		cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = basket.Id,
			cscfga__discounts__c = ''
		);
		insert config;

		cspmb__Price_Item__c cp = new cspmb__Price_Item__c(Name = 'Coax ansulting 100/30');
		insert cp;

		cspmb__Discount_Level__c discountLevel = new cspmb__Discount_Level__c(
			Name = 'coax-100-30-12m',
			cspmb__Discount_Level_Code__c = 'coax-100-30-12m-atl',
			cspmb__Discount_Type__c = 'Percentage',
			cspmb__Discount__c = 100,
			cspmb__Charge_Type__c = 'recurring',
			cspmb__duration__c = 1,
			cspmb__offset__c = 0,
			csdiscounts__Period__c = 1,
			csdiscounts__Offset__c = 0
		);
		insert discountLevel;

		cspmb__Discount_Association__c discountAssociation = new cspmb__Discount_Association__c(
			cspmb__Price_Item__c = cp.Id,
			cspmb__Discount_Level__c = discountLevel.Id
		);
		insert discountAssociation;

		List<RemoteActionProvider.ProductConfigurationCommercialProductWrapper> wrapperList = new List<RemoteActionProvider.ProductConfigurationCommercialProductWrapper>();
		RemoteActionProvider.ProductConfigurationCommercialProductWrapper wrapperClass = new RemoteActionProvider.ProductConfigurationCommercialProductWrapper();
		wrapperClass.productConfigurationId = config.Id;
		wrapperClass.commercialProductId = cp.Id;
		wrapperList.add(wrapperClass);

		String pCcPMap = JSON.serialize(wrapperList);

		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'applyAutomaticDiscount',
			'basketId' => basket.Id,
			'pCcPMap' => pCcPMap
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();

		System.assertEquals(true, result.get('status'));
	}

	@isTest
	private static void testApplyAutomaticDiscountNoDiscount() {
		cscfga__Product_Basket__c basket = getBasket();

		cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = basket.Id,
			cscfga__discounts__c = ''
		);
		insert config;

		cspmb__Price_Item__c cp = new cspmb__Price_Item__c(Name = 'Coax ansulting 100/30');
		insert cp;

		List<RemoteActionProvider.ProductConfigurationCommercialProductWrapper> wrapperList = new List<RemoteActionProvider.ProductConfigurationCommercialProductWrapper>();
		RemoteActionProvider.ProductConfigurationCommercialProductWrapper wrapperClass = new RemoteActionProvider.ProductConfigurationCommercialProductWrapper();
		wrapperClass.productConfigurationId = config.Id;
		wrapperClass.commercialProductId = cp.Id;
		wrapperList.add(wrapperClass);

		String pCcPMap = JSON.serialize(wrapperList);

		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'applyAutomaticDiscount',
			'basketId' => basket.Id,
			'pCcPMap' => pCcPMap
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();

		System.assertEquals(true, result.get('status'));
	}

	@isTest
	private static void testDefaultMaxBandwidthAccess() {
		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'defaultHighestBandwidthAccess',
			'technology' => 'Coax',
			'contractDuration' => '12',
			'siteVendor' => 'ZIGGO',
			'bandwidthUp' => '100',
			'bandwidthDown' => '500'
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();

		System.assertEquals(true, result.get('status'));
	}

	@isTest
	private static void testDefaultMaxBandwidthSiteConnect() {
		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'defaultHighestBandwidthSiteConnect',
			'accessUp' => '30',
			'accessDown' => '100',
			'contractDuration' => '12',
			'ownInfra' => 'true'
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();
		cspmb__Price_Item__c resultingItem = (cspmb__Price_Item__c) result.get(
			'commercialProducts'
		);
		System.assertEquals('Coax ansulting 100/30', resultingItem.Name);
	}

	@isTest
	private static void testDefaultMaxBandwidthSiteConnectwithNoOwnInfra() {
		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'defaultHighestBandwidthSiteConnect',
			'accessUp' => '30',
			'accessDown' => '100',
			'contractDuration' => '12',
			'ownInfra' => 'false'
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();
		cspmb__Price_Item__c resultingItem = (cspmb__Price_Item__c) result.get(
			'commercialProducts'
		);
		System.assertEquals('Coax ansulting 100/30', resultingItem.Name);
	}

	@isTest
	private static void testgetCommercialProductDetails() {
		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'getCommercialProductDetails',
			'contractDuration' => '12'
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();
		cspmb__Price_Item__c resultingItem = (cspmb__Price_Item__c) result.get('commercialProduct');
		System.assertEquals('Coax ansulting 100/30', resultingItem.Name);
	}

	@isTest
	private static void testApplyAutomaticDiscountAddon() {
		Account tmpAcc = new Account(
			OwnerId = UserInfo.getUserId(),
			Name = 'Account',
			Type = 'End Customer'
		);
		insert tmpAcc;

		Opportunity opportunity = new Opportunity(
			Name = 'New Opportunity',
			OwnerId = UserInfo.getUserId(),
			StageName = 'Qualification',
			Probability = 0,
			CloseDate = system.today(),
			AccountId = tmpAcc.id,
			Segment__c = 'SoHo',
			Type_of_service__c = 'Mobile',
			Document_Signature__c = 'Manual'
		);
		insert opportunity;

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
			Name = 'New Basket',
			Primary__c = false,
			OwnerId = UserInfo.getUserId(),
			cscfga__Opportunity__c = opportunity.Id,
			Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]',
			MobileScenarios__c = '[OneMobile]'
		);
		insert basket;

		cscfga__Product_Configuration__c parentConfig = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = basket.Id,
			cscfga__discounts__c = ''
		);
		insert parentConfig;

		cscfga__Product_Configuration__c childConfig = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = basket.Id,
			cscfga__discounts__c = '',
			cscfga__Parent_Configuration__c = parentConfig.Id
		);
		insert childConfig;

		cscfga__Attribute__c guidAttribute = new cscfga__Attribute__c(
			Name = 'GUID',
			cscfga__Value__c = 'guid-123-guid-123',
			cscfga__Product_Configuration__c = childConfig.Id
		);
		insert guidAttribute;

		cspmb__Add_On_Price_Item__c aonPi = new cspmb__Add_On_Price_Item__c(
			Name = 'Service Level - Premium'
		);
		insert aonPi;

		cspmb__Discount_Level__c discountLevel = new cspmb__Discount_Level__c(
			Name = 'helpdesk-premium-atl',
			cspmb__Discount_Level_Code__c = 'helpdesk-premium-atl',
			cspmb__Discount_Type__c = 'Percentage',
			cspmb__Discount__c = 100,
			cspmb__Charge_Type__c = 'recurring',
			cspmb__offset__c = 0,
			csdiscounts__Offset__c = 0
		);
		insert discountLevel;

		cspmb__Discount_Association__c discountAssociation = new cspmb__Discount_Association__c(
			cspmb__Add_On_Price_Item__c = aonPi.Id,
			cspmb__Discount_Level__c = discountLevel.Id
		);
		insert discountAssociation;

		List<RemoteActionProvider.ProductConfigurationAddOnWrapper> wrapperList = new List<RemoteActionProvider.ProductConfigurationAddOnWrapper>();
		RemoteActionProvider.ProductConfigurationAddOnWrapper wrapperItem = new RemoteActionProvider.ProductConfigurationAddOnWrapper();
		wrapperItem.parentConfigurationId = parentConfig.Id;
		wrapperItem.addOnId = aonPi.Id;
		wrapperItem.configurationGuid = 'guid-123-guid-123';
		wrapperList.add(wrapperItem);

		String pCaddOnMap = JSON.serialize(wrapperList);

		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'applyAutomaticDiscountAddOn',
			'basketId' => basket.Id,
			'pCaddOnMap' => pCaddOnMap
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();

		System.assertEquals(true, result.get('status'));
	}

	@isTest
	private static void testIncrementMacdContractNumber() {
		Account tmpAcc = new Account(
			OwnerId = UserInfo.getUserId(),
			Name = 'Account',
			Type = 'End Customer'
		);
		insert tmpAcc;

		Opportunity opportunity = new Opportunity(
			Name = 'New Opportunity',
			OwnerId = UserInfo.getUserId(),
			StageName = 'Qualification',
			Probability = 0,
			CloseDate = System.today(),
			AccountId = tmpAcc.id,
			Segment__c = 'SoHo',
			Type_of_service__c = 'Mobile',
			Document_Signature__c = 'Manual'
		);
		insert opportunity;

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
			Name = 'New Basket',
			Primary__c = false,
			OwnerId = UserInfo.getUserId(),
			cscfga__Opportunity__c = opportunity.Id
		);
		insert basket;

		cscfga__Product_Configuration__c parentConfig = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = basket.Id,
			cscfga__discounts__c = ''
		);
		insert parentConfig;

		cscfga__Product_Configuration__c childConfig = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = basket.Id,
			cscfga__discounts__c = '',
			cscfga__Parent_Configuration__c = parentConfig.Id,
			ContractNumber_JSON__c = '{"year":2021,"minorVersion":1,"majorVersion":2,"incrementNumber":3,"format":"CS-#YYYY#-VZ-#GROUP#-#NUMBER# #MAJOR#.#MINOR# d.d. #DATE#","contractGroup":"OM","contractDate":"2021-12-09"}',
			Contract_Number_Group__c = 'BI'
		);
		insert childConfig;

		cscfga__Attribute__c guidAttribute = new cscfga__Attribute__c(
			Name = 'GUID',
			cscfga__Value__c = 'guid-123-guid-123',
			cscfga__Product_Configuration__c = childConfig.Id
		);
		insert guidAttribute;

		Map<String, Object> inputMap = new Map<String, Object>{
			'method' => 'incrementMacdContractNumber',
			'basketId' => basket.Id,
			'configurationId' => childConfig.Id,
			'configurationGuid' => 'guid-123-guid-123'
		};

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		Test.stopTest();

		System.assertEquals(true, result.get('status'));
	}

	@isTest
	private static void testGetSdwans() {
		cspmb__Price_Item__c sdwan = new cspmb__Price_Item__c(
			cspmb__Is_Active__c = true,
			cspmb__Price_Item_Code__c = 'Flex CPE'
		);
		insert sdwan;

		Map<String, Object> inputMap = new Map<String, Object>{ 'method' => 'getSdwans' };

		Test.startTest();
		Map<String, Object> result = RemoteActionProvider.getData(inputMap);
		System.assertEquals(false, result.get('status'));
		Test.stopTest();
	}
}
