public with sharing class PostalCodeAssignmentHelper {
	
	public PostalCodeAssignmentHelper() {
		
	}

	public static void updatePostalCodeAssignments(List<Id> postalCodeIds){
		
		system.debug('***SP*** postalCodeIds: '+ postalCodeIds); 
		List<Postal_Code_Assignment__c> allAssignments = 
							[select 	id, 
										Dealer_Information__c, 
										ZSP__c, 
										MajorAMSoHo__c, 
										Userid_Major_AM_SoHo__c, 
										UserId__c
							 from Postal_Code_Assignment__c 
							 where id in :postalCodeIds];
		system.debug('***SP*** allAssignments: '+ allAssignments); 

		for(Postal_Code_Assignment__c assignment : allAssignments){
			//copy values from formulas to the lookups
			system.debug('***SP*** formula assignment.UserId__c: '+ assignment.UserId__c); 
			system.debug('***SP*** formula assignment.Userid_Major_AM_SoHo__c: '+ assignment.Userid_Major_AM_SoHo__c); 
			assignment.ZSP__c =  assignment.UserId__c;
			assignment.MajorAMSoHo__c = assignment.Userid_Major_AM_SoHo__c;
		}

		update allAssignments;
	}

	public static void updateDealerInfosFromPostalCodeAssignments(List<Id> dealerInfoIds){

		List<Id> postalCodesToUpdate = new List<Id>();
		system.debug('***SP*** dealerInfoIds: '+ dealerInfoIds); 
		List<Postal_Code_Assignment__c> allAssignments = 
							[select 	id, 
										Dealer_Information__c
							 from Postal_Code_Assignment__c 
							 where Dealer_Information__c in :dealerInfoIds];

		For(Postal_Code_Assignment__c assignment : allAssignments){
			postalCodesToUpdate.add(assignment.id);
		}
		system.debug('***SP*** postalCodesToUpdate: '+ postalCodesToUpdate); 
		updatePostalCodeAssignments(postalCodesToUpdate);
	}
}