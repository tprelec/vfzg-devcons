/**
 * @description       : Test class for ProductBasketModalPageController.class
 * @author            : marin.mamic@vodafoneziggo.com
 * @last modified on  : 09-22-2022
 * @last modified by  : marin.mamic@vodafoneziggo.com
 **/
@isTest
private class ProductBasketModalPageControllerTest {
	@TestSetup
	static void setup() {
		CustomButtonBase.ActionPayload payload = new CustomButtonBase.ActionPayload(
			CustomButtonBase.ActionStatus.SUCCESS,
			'Status message',
			'/apex/SuccessPage',
			'/apex/FailurePage',
			TestUtils.getFakeId(cscfga__Product_Basket__c.sObjectType)
		);
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(Custom_Button_Action_Payload__c = payload.toJSON());
		insert basket;
	}

	@IsTest
	static void whenCustomActionIsCompletedControllerDetectsThat() {
		cscfga__Product_Basket__c basket = [SELECT Custom_Button_Action_Payload__c FROM cscfga__Product_Basket__c LIMIT 1];
		PageReference modalPage = Page.ProductBasketModalPage;
		Test.setCurrentPage(modalPage);
		ApexPages.currentPage().getParameters().put('basketId', basket.Id);
		ProductBasketModalPageController controller = new ProductBasketModalPageController();
		controller.checkBackgroundProcessStatus();
		System.assert(
			controller.backgroundProcessCompleted == true,
			'When payload status is successfull, controller should treat background process as completed'
		);
	}
}
