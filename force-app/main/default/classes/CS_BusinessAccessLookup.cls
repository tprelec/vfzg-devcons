global with sharing class CS_BusinessAccessLookup extends cscfga.ALookupSearch {
	/* global List<Id> sites;
	global  CS_BusinessAccessLookup(){
		sites = new List<Id>();
	} */
	public override Object[] doLookupSearch(
		Map<String, String> searchFields,
		String productDefinitionID,
		Id[] excludeIds,
		Integer pageOffset,
		Integer pageLimit
	) {
		List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>();
		String contractDuration = searchFields.get('contractDuration');
		String technology = '%' + searchFields.get('Technology') + '%';
		String siteVendor = searchFields.get('siteVendor');
		Decimal bandwidthSellableDown = Decimal.valueOf(searchFields.get('BandwidthSellableDown'));
		Date today = Date.valueOf(searchFields.get('Today'));
		String site = searchFields.get('SiteId');
		System.debug(site);
		Site__c s = [SELECT Id, Name FROM Site__c WHERE Id = :site];
		System.debug(s);

		System.debug(
			Logginglevel.DEBUG,
			'Input parameters for query: ' +
			' contractDuration => ' +
			contractDuration +
			' technology => ' +
			technology +
			' siteVendor => ' +
			siteVendor +
			' bandwidthSellableDown => ' +
			bandwidthSellableDown +
			' today => ' +
			today
		);

		priceItems = [
			SELECT
				Name,
				Id,
				Vendor_lookup__r.Name,
				cspmb__Contract_Term__c,
				cspmb__recurring_charge__c,
				cspmb__One_Off_Charge__c,
				Contract_Term_Number__c,
				Available_bandwidth_down__c,
				Available_bandwidth_up__c,
				Recurring_Charge_Product__r.Taxonomy__r.Portfolio__c,
				One_Off_Charge_Product__r.Unify_Charge_Description__c,
				One_Off_Charge_Product__r.Unify_Charge_Code__c,
				One_Off_Charge_Product__r.ProductCode,
				One_Off_Charge_Product__r.Taxonomy__r.Name,
				One_Off_Charge_Product__r.Taxonomy__r.Portfolio__c,
				One_Off_Charge_Product__r.Taxonomy__r.Product_Family__c,
				Recurring_Charge_Product__r.Unify_Charge_Description__c,
				Recurring_Charge_Product__r.Unify_Charge_Code__c,
				Recurring_Charge_Product__r.ProductCode,
				Recurring_Charge_Product__r.Taxonomy__r.Name,
				Recurring_Charge_Product__r.Taxonomy__r.Product_Family__c,
				cspmb__Is_Active__c,
				Category__c,
				VFZ_Variation_Tier_Name__c,
				cspmb__Effective_End_Date__c,
				cspmb__Effective_Start_Date__c,
				cspmb__Master_Price_item__c,
				cspmb__Recurring_Charge_External_Id__c,
				Recurring_Charge_Product__c,
				One_Off_Charge_Product__r.Name,
				LG_UploadSpeed__c,
				VFZ_Commercial_Article_Name__c,
				LG_DownloadSpeed__c,
				VFZ_Commercial_Component_Name__c,
				LG_ProductFamily__c,
				Recurring_Charge_Product__r.Name,
				cspmb__One_Off_Charge_External_Id__c,
				cspmb__Product_Definition_Name__c,
				One_Off_Charge_Product__c,
				Mobile_Add_on_category__c,
				cspmb__One_Off_Cost__c,
				One_Off_Charge_Product__r.ThresholdGroup__c,
				Mobile_Scenario__c,
				cspmb__Recurring_Cost__c,
				Recurring_Charge_Product__r.ThresholdGroup__c,
				Group__c
			FROM cspmb__Price_Item__c
			WHERE
				cspmb__Is_Active__c = TRUE
				AND Access_Type_Text__c LIKE :technology
				AND Available_bandwidth_down__c <= :bandwidthSellableDown
				AND Category__c = 'Business Access'
				AND Contract_Term_Number__c = :contractDuration
				AND cspmb__Effective_End_Date__c >= :today
				AND cspmb__Effective_Start_Date__c <= :today
				AND Vendor_lookup__r.Name = :siteVendor
				AND cspmb__Master_Price_item__c != ''
			ORDER BY Available_bandwidth_down__c ASC
		];
		System.debug(Logginglevel.DEBUG, 'Number of priceItems: ' + priceItems.size());

		return priceItems;
	}

	public override String getRequiredAttributes() {
		return '["Technology","contractDuration","siteVendor", "BandwidthSellableDown", "Today"]';
	}
}
