@IsTest
public with sharing class MavenDocumentsTestFactory {
	public static void createMavenDocumentsRecords() {
		createSettings();
		// New Document Solution
		mmdoc__Document_Solution__c docSolution = createSolution('Ziggo Documents');
		insert docSolution;
		// New Templates
		mmdoc__Document_Template__c quote = createTemplate(
			docSolution.Id,
			'Ziggo Quote',
			'Google Doc'
		);
		mmdoc__Document_Template__c orderConfirmation = createTemplate(
			docSolution.Id,
			'Ziggo Order Confirmation',
			'Google Doc'
		);
		mmdoc__Document_Template__c contractSummary = createTemplate(
			docSolution.Id,
			'Ziggo Contract Summary',
			'Google Doc'
		);
		insert new List<mmdoc__Document_Template__c>{ quote, orderConfirmation, contractSummary };
	}

	public static void createSettings() {
		mmdoc__Tokens__c token = mmdoc__Tokens__c.getOrgDefaults();
		token.mmdoc__Google_Drive_Refresh_Token__c = 'token';
		insert token;
		mmdoc.PostInstall pi = new mmdoc.PostInstall();
		pi.onInstall(null);
	}

	public static mmdoc__Document_Solution__c createSolution(String solutionName) {
		return new mmdoc__Document_Solution__c(Name = solutionName);
	}

	public static mmdoc__Document_Template__c createTemplate(
		Id solutionId,
		String templateName,
		String templateType
	) {
		return new mmdoc__Document_Template__c(
			Name = templateName,
			mmdoc__Document_Solution__c = solutionId,
			mmdoc__Template_Type__c = templateType,
			mmdoc__Template_Document_Id__c = '123'
		);
	}
}