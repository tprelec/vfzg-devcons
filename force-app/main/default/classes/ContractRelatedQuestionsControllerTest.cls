@isTest
private class ContractRelatedQuestionsControllerTest {
	@TestSetup
	static void makeData() {
		// Disable Triggers
		No_Triggers__c notriggers = new No_Triggers__c(SetupOwnerId = UserInfo.getUserId(), Flag__c = true);
		insert notriggers;

		// Prepare Settings
		Contract_Generation_Settings__c contractGenSettings = new Contract_Generation_Settings__c(
			Document_template_name_direct__c = 'Direct Sales Template',
			Document_template_name_direct_2__c = 'Direct Sales Template 2',
			Document_template_name_BPA__c = 'BPA Template new',
			Document_template_name_indirect__c = 'Indirect Sales Template'
		);
		insert contractGenSettings;

		// Document Definition
		csclm__Document_Definition__c docDefinition = CS_DataTest.createDocumentDefinition();
		insert docDefinition;

		// Document Templates
		List<csclm__Document_Template__c> docTemplates = new List<csclm__Document_Template__c>();
		Set<String> templateNames = new Set<String>{
			'Direct Sales Template',
			'BPA Template new',
			'Direct Sales Template 2',
			'Indirect Sales Template'
		};
		for (String templateName : templateNames) {
			csclm__Document_Template__c docTemplate = CS_DataTest.createDocumentTemplate(templateName);
			docTemplate.csclm__Document_Definition__c = docDefinition.Id;
			docTemplate.csclm__Valid__c = true;
			docTemplate.csclm__Active__c = true;
			docTemplates.add(docTemplate);
		}
		insert docTemplates;

		// Create Basket
		TestUtils.theAdministrator = new User(Id = UserInfo.getUserId());
		cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();

		// Create Product Configs
		cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
		prodDef.Product_Type__c = 'Mobile';
		insert prodDef;
		cscfga__Product_Configuration__c ctnConfig = CS_DataTest.createProductConfiguration(prodDef.Id, 'Mobile CTN Profile', basket.Id);
		insert ctnConfig;
	}

	@IsTest
	private static void testGetBasketDetails() {
		cscfga__Product_Basket__c basket = getBasket();
		Test.startTest();
		cscfga__Product_Basket__c freshBasket = ContractRelatedQuestionsController.getBasketDetails(basket.Id);
		ContractRelatedQuestionsController.getContractCustomSettings(freshBasket);
		Test.stopTest();
		System.assert(freshBasket != null, 'Basket should be retreived.');
	}

	@IsTest
	private static void testUpdateBasket() {
		cscfga__Product_Basket__c basket = getBasket();
		Test.startTest();
		ContractRelatedQuestionsController.updateBasketStatus(basket, 'Approved');
		ContractRelatedQuestionsController.updateBasket(
			basket,
			new Map<String, Object>{
				'Existing_Mobile_Contract_Date__c' => '2020-01-01',
				'Primary__c' => true,
				'cscfga__Basket_Status__c' => 'Approved'
			}
		);
		Test.stopTest();
		basket = getBasket();
		System.assertEquals('Approved', basket.cscfga__Basket_Status__c, 'Basket status should be updated.');
	}

	@IsTest
	private static void testGetUserDetails() {
		Test.startTest();
		User usr = ContractRelatedQuestionsController.getCurrentUser();
		Test.stopTest();
		System.assertEquals(UserInfo.getUserId(), usr.Id, 'Current user should be returned.');
	}

	@IsTest
	private static void testCreateNoBPA() {
		cscfga__Product_Basket__c basket = getBasket();
		Test.startTest();
		csclm__Agreement__c agreement = ContractRelatedQuestionsController.createBPAIfNeeded(basket);
		Test.stopTest();
		System.assertEquals(null, agreement, 'BPA Agreement should not be created.');
	}

	@IsTest
	private static void testCreateBPA() {
		Account partnerAcc = TestUtils.createPartnerAccount();
		User portalUser = TestUtils.createPortalUser(partnerAcc);

		ContractRelatedQuestionsController.featureFlaggingDisableBpaGeneration = 'DisableBPAGenerationTestCase';

		Test.startTest();
		Opportunity opp = getOpportunity();
		opp.OwnerId = portalUser.Id;
		update opp;
		cscfga__Product_Basket__c basket = getBasket();
		csclm__Agreement__c agreement = ContractRelatedQuestionsController.createBPAIfNeeded(basket);
		Test.stopTest();
		System.assert(agreement != null, 'BPA Agreement should be created.');
	}

	@IsTest
	private static void testCreateDirectAgreement() {
		Test.startTest();
		cscfga__Product_Basket__c basket = getBasket();
		csclm__Agreement__c agreement = ContractRelatedQuestionsController.createAgreement(basket);
		Test.stopTest();
		System.assert(agreement != null, 'Direct Agreement should be created.');
	}

	@IsTest
	private static void testCreateIndirectAgreement() {
		Account partnerAcc = TestUtils.createPartnerAccount();
		User portalUser = TestUtils.createPortalUser(partnerAcc);
		Test.startTest();
		Opportunity opp = getOpportunity();
		opp.OwnerId = portalUser.Id;
		update opp;
		cscfga__Product_Basket__c basket = getBasket();
		csclm__Agreement__c agreement = ContractRelatedQuestionsController.createAgreement(basket);
		Test.stopTest();
		System.assert(agreement != null, 'Indirect Agreement should be created.');
	}

	private static cscfga__Product_Basket__c getBasket() {
		cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c];
		return ContractRelatedQuestionsController.getBasketDetails(basket.Id);
	}

	private static Opportunity getOpportunity() {
		return [SELECT Id FROM Opportunity];
	}
}
