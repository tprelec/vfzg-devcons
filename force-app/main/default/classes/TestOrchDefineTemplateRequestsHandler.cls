@IsTest
public class TestOrchDefineTemplateRequestsHandler {
	@TestSetup
	static void makeData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			ban,
			owner
		);
		opp.Select_Journey__c = 'Sales';
		update opp;
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, claimerContact.id);
		Billing_Arrangement__c bar = new Billing_Arrangement__c(
			Financial_Account__c = fa.Id,
			Billing_Arrangement_Alias__c = '123test',
			Unify_Ref_Id__c = '1234565234'
		);
		insert bar;
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(
			claimerContact.id,
			'123321'
		);
		contr.Dealer_Information__c = dealerInfo.Id;
		contr.Unify_Framework_Id__c = '9634563A';
		contr.Signed_by_Customer__c = claimerContact.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			Billing_Arrangement__c = bar.id
		);
		insert order;

		VF_Product__c vfp = new VF_Product__c(
			Name = 'tst',
			Family__c = 'tst',
			Product_Line__c = 'fVodafone',
			ExternalID__c = 'VFP-02-1234567',
			ProductCode__c = 'DUMMYADDMOBILEPRODUCT',
			OrderType__c = ot.Id,
			AllowsGroupDataSharing__c = 'YES',
			Unify_Group_Level_Product_CatalogId__c = '3720595',
			Unify_Group_Level_Product_Code__c = 'GL_MASS_MARKET',
			Unify_Group_Lvl_Pricing_Element_Branch__c = 'Shared_Allowance',
			Unify_Group_Lvl_Billing_Offer_CatalogId__c = '8883473',
			Unify_Group_Level_Billing_Offer_Code__c = 'GL_AO_GROUP_DATA_3_BIZ_RED',
			Is_an_Addon_for_template_description__c = 'Yes'
		);
		insert vfp;

		Product2 product = TestUtils.createProduct(
			new Map<String, Object>{
				'Product_Group__c' => 'Addons',
				'VF_Product__c' => vfp.Id,
				'Is_an_Addon_for_template_description__c' => 'Yes'
			}
		);
		Product2 productPriceplan = TestUtils.createProduct(
			new Map<String, Object>{
				'Product_Group__c' => 'Priceplan',
				'VF_Product__c' => vfp.Id,
				'Is_an_Addon_for_template_description__c' => 'No'
			}
		);

		// Create Contracted Products
		List<Contracted_Products__c> contProds = new List<Contracted_Products__c>();
		for (Integer i = 0; i < 2; i++) {
			for (Integer j = 0; j < 2; j++) {
				contProds.add(
					new Contracted_Products__c(
						Quantity__c = 1,
						UnitPrice__c = 10,
						Duration__c = 1,
						Model_Number__c = i,
						Product__c = productPriceplan.Id,
						VF_Contract__c = contr.Id,
						Order__c = order.Id
					)
				);
			}
		}
		contProds.add(
			new Contracted_Products__c(
				Quantity__c = 1,
				UnitPrice__c = 10,
				Duration__c = 1,
				Model_Number__c = 1,
				Product__c = product.Id,
				VF_Contract__c = contr.Id,
				Order__c = order.Id
			)
		);
		insert contProds;

		// Create Orchestrator Template and Step Template
		List<CSPOFA__Orchestration_Process_Template__c> processTemplates = OrchTestDataFactory.createOrchestrationProcessTemplates(
			null,
			1,
			true
		);
		OrchTestDataFactory.createOrchestrationStepTemplates(processTemplates, null, 1, true);
	}

	@IsTest
	public static void testDefineTemplateRequests() {
		Order__c order = getOrder();
		createOrchProcess(order.Id);

		Test.startTest();

		List<CSPOFA__Orchestration_Step__c> steps = getProcessSteps();
		OrchDefineTemplateRequestsHandler handler = new OrchDefineTemplateRequestsHandler();
		handler.process(steps);

		Test.stopTest();

		List<Contracted_Products__c> conProds = [
			SELECT Id
			FROM Contracted_Products__c
			WHERE Request_New_Template__c = TRUE
		];

		System.assertEquals(
			2,
			conProds.size(),
			'Number of requested templates should match count of different model numbers.'
		);
	}

	private static VF_Contract__c getContract() {
		return [SELECT Id FROM VF_Contract__c LIMIT 1];
	}

	private static Product2 getProduct() {
		return [SELECT Id FROM Product2 WHERE Product_group__c = 'Priceplan' LIMIT 1];
	}

	private static Order__c getOrder() {
		return [SELECT Id, VF_Contract__c FROM Order__c LIMIT 1];
	}

	private static List<CSPOFA__Orchestration_Process_Template__c> getProcessTemplates() {
		return [SELECT Id, Name FROM CSPOFA__Orchestration_Process_Template__c];
	}

	private static List<CSPOFA__Orchestration_Step_Template__c> getProcessStepTemplates() {
		return [SELECT Id, Name FROM CSPOFA__Orchestration_Step_Template__c];
	}

	private static List<CSPOFA__Orchestration_Step__c> getProcessSteps() {
		return [SELECT Id, CSPOFA__Orchestration_Process__c FROM CSPOFA__Orchestration_Step__c];
	}

	private static void createOrchProcess(Id orderId) {
		Map<String, String> fieldsMap = new Map<String, String>{ 'VZ_Order__c' => orderId };
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(
			getProcessTemplates(),
			fieldsMap,
			true
		);
		OrchTestDataFactory.createOrchestrationSteps(processes, getProcessStepTemplates(), true);
	}
}