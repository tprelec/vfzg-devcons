/*******************************************************************************************************************************************
* File Name     :  LG_PremiseTriggerHandlerTest
* Description   :  This is a test class for the class: LG_PremiseTriggerHandler

* @author       :   Shreyas
* Modification Log
===================================================================================================
* Ver.    Date          Author              Modification
---------------------------------------------------------------------------------------------------
* 1.0     13th-Jun-16    Shreyas             Created the class for release R2

********************************************************************************************************************************************/
@isTest 
private class LG_PremiseTriggerHandlerTest {

    @testsetup
    private static void setupTestData() {
        
    }

    /*
        Name: TestMethod1
        Purpose: Method for testing the creation of premise record and getting the address Id (without exception)
        Argument: none
        Return type: none
     */
    static testmethod void TestMethod1() {
    
        List<cscrm__Address__c> premiseListToInsert = new List<cscrm__Address__c>();
        for(integer i=0;i<9;i++){
            
            cscrm__Address__c address = new cscrm__Address__c();
            address.LG_HouseNumber__c = '2'+i;
            address.cscrm__Zip_Postal_Code__c = '357'+i+'RE'; 
            address.LG_HouseNumberExtension__c = 'A';          
            premiseListToInsert.add(address);

        }
        
        Test.startTest();
        
        LG_EnvironmentVariables__c EnvSettings = new LG_EnvironmentVariables__c();
        EnvSettings.LG_SandboxInstance__c = 'dev';
        EnvSettings.LG_CloudSenceAnywhereIconID__c = '01524000000NroL';
        EnvSettings.LG_ServiceAvailabilityIconID__c = '01524000000NroM';
        insert EnvSettings; 
        
        insert premiseListToInsert;
        system.debug('############premiseListToInsert test method1 '+premiseListToInsert);
        Test.stopTest();
        
    }
    
    /*
        Name: TestMethod2
        Purpose: Method for testing the creation of premise record and getting the address Id (with exception)
        Argument: none
        Return type: none
     */
     static testmethod void TestMethod2() {
        Test.startTest();
        List<cscrm__Address__c> premiseListToInsert1 = new List<cscrm__Address__c>();
        for(integer i=0;i<9;i++){
            
            cscrm__Address__c address = new cscrm__Address__c();
            address.LG_HouseNumber__c = '2'+i;
            address.cscrm__Zip_Postal_Code__c = '357'+i+'RE'; 
            address.LG_HouseNumberExtension__c = 'A';          
            premiseListToInsert1.add(address);

        }
        
        insert premiseListToInsert1;
        system.debug('$$$$$$$$$premiseListToInsert1 testmethod 2 '+premiseListToInsert1);
        for(cscrm__Address__c csrmadduniqkey:premiseListToInsert1)
        system.debug('*************csrmadduniqkey.LG_Uniquekey__c' +csrmadduniqkey.LG_Uniquekey__c);
        Test.stopTest();
        
    }
    
}