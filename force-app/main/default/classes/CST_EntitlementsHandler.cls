public without sharing class CST_EntitlementsHandler {
	/*************************************************************************************************************************
	 *  Method Name : checkMilestoneClock
	 *  Parameter   : -
	 *  Returns     : void
	 *  Description : Method to stop/enable milestone clock based on status transition.
	 **************************************************************************************************************************/
	public static void checkMilestoneClock(List<Case> newCases, Map<Id, Case> oldCaseMap) {
		Map<ID, Schema.RecordTypeInfo> caseRecordTypeMap = Schema.SObjectType.Case.getRecordTypeInfosById();
		Set<String> statusToStopClock = new Set<String>{ 'Waiting for Customer Information', 'Waiting for Customer Validation' };
		Set<String> statusToRestartClock = new Set<String>{ 'New', 'In progress', 'Assessing', 'Resolved' };

		for (Case c : newCases) {
			Case oldCase = oldCaseMap.get(c.Id);
			String caseRecordType = caseRecordTypeMap.get(c.RecordTypeId).getDeveloperName();

			if (c.Status != oldCase.Status && caseRecordType.contains('CST')) {
				if (statusToStopClock.contains(c.Status)) {
					c.IsStopped = true;
				} else if (c.IsStopped && statusToRestartClock.contains(c.Status)) {
					c.IsStopped = false;
				}
			}
		}
	}

	/*************************************************************************************************************************
	 *  Method Name : checkMilestoneClockOLA
	 *  Parameter   : -
	 *  Returns     : void
	 *  Description : Method to stop/enable OLA milestone clock based on status transition.
	 **************************************************************************************************************************/

	public static void checkMilestoneClockOLA(List<Case> ListnewCases, Map<Id, Case> oldCaseMapOLA) {
		Map<ID, Schema.RecordTypeInfo> caseRecordTypeMapOLA = Schema.SObjectType.Case.getRecordTypeInfosById();
		Set<String> statusToStopClockOLA = new Set<String>{ 'Pending' };
		Set<String> statusToRestartClockOLA = new Set<String>{ 'Assigned' };

		for (Case cs : ListnewCases) {
			Case oldCaseOLA = oldCaseMapOLA.get(cs.Id);
			Case caseRemedy = [SELECT Id, CST_End2End_Owner__c, Status FROM Case WHERE id = :oldCaseOLA.id LIMIT 1];
			System.debug(caseRemedy);
			String caseRecordTypeOLA = caseRecordTypeMapOLA.get(cs.RecordTypeId).getDeveloperName();
			CST_External_Ticket__c remedyExtTicket = [
				SELECT Id, Name, CST_Status__c, CST_Case__c
				FROM CST_External_Ticket__c
				WHERE CST_Case__c = :oldCaseOLA.id
				LIMIT 1
			];

			if (caseRecordTypeOLA.contains('CST')) {
				if (remedyExtTicket.CST_Status__c == 'Pending') {
					cs.IsStopped = true;
				} else if (remedyExtTicket.CST_Status__c == 'Assigned') {
					cs.IsStopped = false;
				}
				system.debug(cs.IsStopped);
			}
		}

		update ListnewCases;
	}

	/*************************************************************************************************************************
	 *  Method Name : setEntitlementCST
	 *  Parameter   : -
	 *  Returns     : void
	 *  Description : Method to automatically associate an Entitlement to a newly created Case, based on the Account+Product combination.
	 **************************************************************************************************************************/
	public static void setEntitlementCST(List<Case> newCases, Map<Id, Case> oldCaseMap, Boolean isInsert) {
		Map<ID, Schema.RecordTypeInfo> caseRecordTypeMap = Schema.SObjectType.Case.getRecordTypeInfosById();
		Set<Id> accountIds = new Set<Id>();
		List<String> products = new List<String>();
		for (Case c : newCases) {
			Case oldCase = isInsert ? null : oldCaseMap.get(c.Id);
			String caseRecordType = caseRecordTypeMap.get(c.RecordTypeId).getDeveloperName();
			Boolean condition2SetEntitlement =
				caseRecordType.contains('CST') && (isInsert || c.AccountId != oldCase.AccountId || c.CST_Product__c != oldCase.CST_Product__c);
			if (condition2SetEntitlement) {
				accountIds.add(c.AccountId);
				products.add(c.CST_Product__c);
				System.debug('accountIds -> ' + accountIds);
				System.debug('products -> ' + products);
			}
		}
		if (accountIds.size() <= 0) {
			return;
		}

		Map<String, Entitlement> mapEntitlementsByKey = new Map<String, Entitlement>();

		for (Entitlement e : [
			SELECT id, StartDate, EndDate, AccountId, AssetId, Type
			FROM Entitlement
			WHERE Status = 'Active' AND AccountId IN :accountIds AND Type IN :products
		]) {
			mapEntitlementsByKey.put(e.AccountId + '-' + e.Type, e);
		}
		System.debug('mapEntitlementsByKey -> ' + mapEntitlementsByKey);

		for (Case c : newCases) {
			System.debug('entrou no ultimo for... ');
			Entitlement newE = mapEntitlementsByKey.get(c.Accountid + '-' + c.CST_Product__c);
			System.debug('newE ->  ' + newE);
			if (newE != null) {
				c.EntitlementId = newE.Id;
				c.SlaStartDate = newE.StartDate;
			} else {
				c.EntitlementId = null;
				c.SlaStartDate = null;
			}
		}
	}

	/*************************************************************************************************************************
	 *  Method Name : closeCaseMilestones
	 *  Parameter   : -
	 *  Returns     : void
	 *  Description : Method do automatically close any existing case milestones once a Case changes its status to Closed.
	 **************************************************************************************************************************/
	public static void closeCaseMilestones(List<Case> newCases, Map<Id, Case> oldCaseMap) {
		Map<ID, Schema.RecordTypeInfo> caseRecordTypeMap = Schema.SObjectType.Case.getRecordTypeInfosById();
		Set<Id> closeCaseIds = new Set<Id>();
		Set<Id> inProgressCaseIds = new Set<Id>();

		for (Case c : newCases) {
			Case oldCase = oldCaseMap.get(c.Id);
			String caseRecordType = caseRecordTypeMap.get(c.RecordTypeId).getDeveloperName();
			if (caseRecordType.contains('CST') && c.Status != oldCase.Status) {
				if (c.Status == 'Closed') {
					closeCaseIds.add(c.Id);
				} else if (c.Status == 'In Progress') {
					inProgressCaseIds.add(c.Id);
				}
			}
		}

		if (closeCaseIds.size() > 0) {
			List<CaseMilestone> caseMilestones2Close = [
				SELECT Id, CaseId, Case.ClosedDate, CompletionDate, IsCompleted
				FROM CaseMilestone
				WHERE caseId IN :closeCaseIds AND CompletionDate = NULL AND MilestoneType.Name != 'Remedy Leadtime'
			];
			for (CaseMilestone cm : caseMilestones2Close) {
				cm.CompletionDate = cm.Case.ClosedDate;
			}

			update caseMilestones2Close;
		}
		if (inProgressCaseIds.size() > 0) {
			List<CaseMilestone> caseMilestonesInProgress = [
				SELECT Id, CaseId, Case.ClosedDate, MilestoneType.Name, CompletionDate, IsCompleted
				FROM CaseMilestone
				WHERE caseId IN :inProgressCaseIds AND CompletionDate = NULL AND MilestoneType.Name = 'Analysis Finished'
			];
			for (CaseMilestone cm : caseMilestonesInProgress) {
				cm.CompletionDate = System.now();
			}

			update caseMilestonesInProgress;
		}
	}

	/*************************************************************************************************************************
	 *  Method Name : forceEntitlement
	 *  Parameter   : -
	 *  Returns     : void
	 *  Description : Method to force setting Milestones running for Cases created from customer portal
	 **************************************************************************************************************************/
	public static void forceEntitlement(List<Case> newCases, Map<Id, Case> oldCaseMap) {
		Map<ID, Schema.RecordTypeInfo> caseRecordTypeMap = Schema.SObjectType.Case.getRecordTypeInfosById();
		Set<Id> caseIds = new Set<Id>();

		for (Case c : newCases) {
			String caseRecordType = caseRecordTypeMap.get(c.RecordTypeId).getDeveloperName();
			Case oldCase = oldCaseMap.get(c.Id);
			Boolean condition2force = caseRecordType.contains('CST') && c.EntitlementId != null && oldCase.EntitlementId == null;
			if (condition2force) {
				caseIds.add(c.Id);
			}
		}

		if (caseIds.size() > 0) {
			futureUpdateCase(caseIds);
		}
	}

	@future(callout=true)
	private static void futureUpdateCase(Set<Id> caseIds) {
		List<Case> cases = [SELECT id, subject FROM Case WHERE id IN :caseIds];

		for (Case c : cases) {
			String caseSubject = c.subject;
			caseSubject = caseSubject.replace('CST_DC01', 'Business Internet Standard Fit');
			c.subject = caseSubject;
		}

		update cases;
	}
}
