@isTest
private class TestEmailTemplateHelper {
	private static final String TEMPLATE_NAME = 'TestEmailTemplate_';
	private static final String DELIVERY_ORDER_NAME = 'Test_';
	private static final String EMAIL_CONTENT = 'TestContent_';
	private static final String DELIVERY_COMPONENT_NAME = 'TestComponent';
	public static final String STYLING_TEMPLATE_DEVELOPER_NAME = 'COM_Styling_Template';
	private static final String STYLING_CONTENT = 'TestStyling [EMAIL_CONTENT]';

	@isTest
	private static void testProcessEmailTemplate() {
		String index = '1';
		String templateName = TEMPLATE_NAME + index;
		EmailTemplate emailTempl = CS_DataTest.createEmailTemplate(
			templateName,
			templateName,
			'Subject',
			'[BLOCK_GREETING] {DeliveryOrder.Name}',
			'body',
			false
		);
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index
		);

		Test.startTest();
		String result = EmailTemplateHelper.processEmailTemplate(emailTempl, deliveryOrder);
		Test.stopTest();

		System.assertEquals(
			true,
			result.contains(
				EMAIL_CONTENT +
				index +
				' ' +
				DELIVERY_ORDER_NAME +
				index +
				'_DeliveryOrder'
			) && result.contains('font-family: "myfont"'),
			'Required text not found.'
		);
	}

	@isTest
	private static void testReplaceEmailTemplateHtmlFromMetadataNoConditions() {
		String index = '1';
		String templateName = TEMPLATE_NAME + index;
		EmailTemplate emailTempl = CS_DataTest.createEmailTemplate(
			templateName,
			templateName,
			'Subject',
			'[BLOCK_GREETING]',
			'body',
			false
		);
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index
		);

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		String result = EmailTemplateHelper.replaceEmailTemplateHtmlFromMetadata(
			emailTempl,
			deliveryOrder
		);
		Test.stopTest();

		System.assertEquals(EMAIL_CONTENT + index, result, 'Required text not found.');
	}

	@isTest
	private static void testReplaceEmailTemplateHtmlFromMetadataChangeTypeAndDeliveryCompMatch() {
		String index = '2';
		String templateName = TEMPLATE_NAME + index;
		EmailTemplate emailTempl = CS_DataTest.createEmailTemplate(
			templateName,
			templateName,
			'Subject',
			'[BLOCK_GREETING]',
			'body',
			false
		);
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index,
			DELIVERY_COMPONENT_NAME
		);

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		String result = EmailTemplateHelper.replaceEmailTemplateHtmlFromMetadata(
			emailTempl,
			deliveryOrder
		);
		Test.stopTest();

		System.assertEquals(EMAIL_CONTENT + index, result, 'Required text not found.');
	}
	/*
    @isTest
    private static void testReplaceEmailTemplateHtmlFromMetadata_ChangeTypeAndDeliveryCompNoMatch() {
        String index = '2';
        String templateName = TEMPLATE_NAME + index;
        EmailTemplate emailTempl = CS_DataTest.createEmailTemplate(templateName, templateName, 'Subject', '[BLOCK_GREETING]', 'body', false);
        COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

        deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);

        Test.startTest();
        String result = EmailTemplateHelper.replaceEmailTemplateHtmlFromMetadata(emailTempl, deliveryOrder);
        Test.stopTest();

        System.assertEquals('', result);
    }
    */

	@isTest
	private static void testReplaceEmailTemplateHtmlFromDeliveryOrder() {
		String index = '4';
		String siteStreet = 'Street';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index
		);

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);
		String emailBody = '{DeliveryOrder.SiteStreet} {DeliveryOrder.Name}';

		Test.startTest();
		String result = EmailTemplateHelper.replaceEmailTemplateHtmlFromDeliveryOrder(
			emailBody,
			deliveryOrder
		);
		Test.stopTest();

		System.assertEquals(
			siteStreet +
			' ' +
			DELIVERY_ORDER_NAME +
			index +
			'_DeliveryOrder',
			result,
			'Required text not found.'
		);
	}

	@isTest
	private static void testCheckForChangeTypeTrue() {
		String index = '4';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index
		);

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		Boolean result = EmailTemplateHelper.checkForChangeType('New', deliveryOrder);
		Test.stopTest();

		System.assertEquals(true, result, 'Change not found.');
	}

	@isTest
	private static void testCheckForChangeTypeFalse() {
		String index = '4';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index
		);

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		Boolean result = EmailTemplateHelper.checkForChangeType('Change', deliveryOrder);
		Test.stopTest();

		System.assertEquals(false, result, 'Change not found.');
	}

	@isTest
	private static void testCheckForDeliveryComponentTrue() {
		String index = '5';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index,
			DELIVERY_COMPONENT_NAME
		);

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		Boolean result = EmailTemplateHelper.checkForDeliveryComponent(
			DELIVERY_COMPONENT_NAME,
			deliveryOrder
		);
		Test.stopTest();

		System.assertEquals(true, result, 'Change not found.');
	}

	@isTest
	private static void testCheckForDeliveryComponentFalse() {
		String index = '5';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index,
			DELIVERY_COMPONENT_NAME
		);

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		Boolean result = EmailTemplateHelper.checkForDeliveryComponent(
			DELIVERY_COMPONENT_NAME + '_NOT',
			deliveryOrder
		);
		Test.stopTest();

		System.assertEquals(false, result, 'Change not found.');
	}

	@isTest
	private static void testFetchTemplateEmailAttachments() {
		String index = '3';
		String templateName = TEMPLATE_NAME + index;
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index
		);

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);

		Document doc = new Document();
		doc.DeveloperName = 'TestAttachment';
		doc.FolderId = UserInfo.getUserId();
		doc.Name = 'TestAttachment';
		doc.ContentType = 'pdf';
		doc.Body = Blob.valueof('Test Value');
		insert doc;

		Test.startTest();
		List<Messaging.EmailFileAttachment> result = EmailTemplateHelper.fetchTemplateEmailAttachments(
			templateName,
			deliveryOrder
		);
		Test.stopTest();

		System.assertEquals(1, result.size(), 'Size is not the same.');
	}

	@isTest
	private static void testFormatServiceData() {
		String index = '3';
		String serviceName = 'TestService';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index
		);

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Sub' + index;
		subscription.csord__Status__c = 'Subscription Created';
		insert subscription;

		csord__Service__c parentService = CS_DataTest.createService(
			null,
			subscription,
			'Service Created'
		);
		parentService.csord__Identification__c = 'Serv' + index;
		parentService.Name = serviceName;
		parentService.COM_Delivery_Order__c = deliveryOrder.Id;
		insert parentService;

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		String result = EmailTemplateHelper.formatServiceData(deliveryOrder);
		Test.stopTest();

		System.assertEquals(true, result.contains(serviceName), 'Service name not found.');
	}

	@isTest
	private static void testFetchDeliveryOrder() {
		String index = '8';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrder(
			DELIVERY_ORDER_NAME + index,
			null,
			true
		);

		Test.startTest();
		COM_Delivery_Order__c result = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);
		Test.stopTest();

		System.assertEquals(DELIVERY_ORDER_NAME + index, result.Name, 'Name is not the same.');
	}

	@isTest
	private static void testReplaceEmailTemplateHtmlFromDeliveryOrderNewServices() {
		String index = '4';
		String serviceName = 'TestService';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index,
			DELIVERY_COMPONENT_NAME
		);

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Sub' + index;
		subscription.csord__Status__c = 'Subscription Created';
		insert subscription;

		csord__Service__c serv1 = CS_DataTest.createService(null, subscription, 'Service Created');
		serv1.csord__Identification__c = 'Serv1' + index;
		serv1.Name = serviceName + '1';
		serv1.COM_Delivery_Order__c = deliveryOrder.Id;
		insert serv1;

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);
		String emailBody = '{DeliveryOrder.NewServices}';

		Test.startTest();
		String result = EmailTemplateHelper.replaceEmailTemplateHtmlFromDeliveryOrder(
			emailBody,
			deliveryOrder
		);
		Test.stopTest();

		System.assertEquals(true, result.contains(serviceName + '1'), 'Service name not found.');
	}

	@isTest
	private static void testReplaceEmailTemplateHtmlFromDeliveryOrderSoftwareServices() {
		String index = '4';
		String serviceName = 'TestService';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(
			DELIVERY_ORDER_NAME + index,
			DELIVERY_COMPONENT_NAME
		);

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Sub' + index;
		subscription.csord__Status__c = 'Subscription Created';
		insert subscription;

		csord__Service__c serv1 = CS_DataTest.createService(null, subscription, 'Service Created');
		serv1.csord__Identification__c = 'Serv1' + index;
		serv1.Name = serviceName + '1';
		serv1.COM_Delivery_Order__c = deliveryOrder.Id;
		insert serv1;

		deliveryOrder = EmailTemplateHelper.fetchDeliveryOrder(deliveryOrder.Id);
		String emailBody = '{DeliveryOrder.SoftwareServices}';

		Test.startTest();
		String result = EmailTemplateHelper.replaceEmailTemplateHtmlFromDeliveryOrder(
			emailBody,
			deliveryOrder
		);
		Test.stopTest();

		System.assertEquals(false, result.contains(serviceName + '1'), 'Service name not found.');
	}

	@isTest
	private static void testFetchStylingTemplateHTML() {
		Test.startTest();
		String result = EmailTemplateHelper.fetchStylingTemplateHTML();
		Test.stopTest();

		System.assertEquals(true, result.contains('font-family: "myfont"'), 'Font not found.');
	}

	@isTest
	private static void testStyleEmail() {
		String mailBody = 'TestBody';

		Test.startTest();
		String result = EmailTemplateHelper.styleEmail(mailBody);
		Test.stopTest();

		System.assertEquals(
			true,
			result.contains('TestBody') && result.contains('font-family: "myfont"'),
			'Font not found.'
		);
	}

	@isTest
	private static void testReturnBodyIfStylingNeededNegative() {
		String index = '11';
		String templateName = TEMPLATE_NAME + index;
		EmailTemplate emailTempl = CS_DataTest.createEmailTemplate(
			templateName,
			templateName,
			'Subject',
			'Test',
			'body',
			true
		);

		Test.startTest();
		String result = EmailTemplateHelper.returnBodyIfStylingNeeded(emailTempl.Id);
		Test.stopTest();

		System.assertEquals(null, result, 'Result is not null.');
	}

	@isTest
	private static void testReturnBodyIfStylingNeededPositive() {
		EmailTemplate emailTempl = [
			SELECT Id
			FROM EmailTemplate
			WHERE DeveloperName = :STYLING_TEMPLATE_DEVELOPER_NAME
		];

		Test.startTest();
		String result = EmailTemplateHelper.returnBodyIfStylingNeeded(emailTempl.Id);
		Test.stopTest();

		System.assertEquals(true, result.contains('EMAIL_CONTENT'), 'Text not found.');
	}
}
