@RestResource(urlMapping='/mockedEndpoints/tboCdoNotificationsListener')
global with sharing class tb_test_listener {
	@HttpPost
	global static void processRequest() {
		RestRequest request = RestContext.request;
		System.debug('request ' + request);
		RestResponse response = RestContext.response;
		response.statusCode = 200;
		response.responseBody = request.requestBody;
		insert new COM_Delivery_Order__c(Name = 'async resp ' + response.responseBody.toString());
	}
}
