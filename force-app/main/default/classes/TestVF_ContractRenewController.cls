@isTest
private class TestVF_ContractRenewController {
	@isTest
    private static void testConstructor() {
        VF_Contract__c contract = new VF_Contract__c();
        insert contract;
                
        PageReference pageRef = Page.VF_ContractRenew;
        Test.setCurrentPage(pageRef);
                
        pageRef.getParameters().put('Id', String.valueOf(contract.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        
        VF_ContractRenewController contrRenCtr = new VF_ContractRenewController(sc);
    }
    
    @isTest
    private static void testUpdateContractStatus_StatusRejected() {
        VF_Contract__c contract = new VF_Contract__c();
        contract.Implementation_Status__c = 'Rejected';
        contract.Contract_Receive_Date__c = null;
        insert contract;
                
        PageReference pageRef = Page.VF_ContractRenew;
        Test.setCurrentPage(pageRef);
                
        pageRef.getParameters().put('Id', String.valueOf(contract.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        
        VF_ContractRenewController contrRenCtr = new VF_ContractRenewController(sc);
        
        Test.startTest();
		pageReference result = contrRenCtr.updateContractStatus();
		Test.stopTest();

        VF_Contract__c updatedContr = [
            SELECT Id, Implementation_Status__c, Contract_Receive_Date__c
            FROM VF_Contract__c
            WHERE Id = :contract.Id
        ];
        
		System.assertEquals('/'+ contract.Id, result.getUrl());
		System.assertEquals('Renewed', updatedContr.Implementation_Status__c);
		System.assertEquals(System.today(), updatedContr.Contract_Receive_Date__c); 
    }
    
    @isTest
    private static void testUpdateContractStatus_StatusNotStarted() {        
        VF_Contract__c contract = new VF_Contract__c();
        contract.Implementation_Status__c = 'Not started';
        contract.Contract_Receive_Date__c = null;
        insert contract;
                
        PageReference pageRef = Page.VF_ContractRenew;
        Test.setCurrentPage(pageRef);
                
        pageRef.getParameters().put('Id', String.valueOf(contract.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        
        VF_ContractRenewController contrRenCtr = new VF_ContractRenewController(sc);
        
        Test.startTest();
		pageReference result = contrRenCtr.updateContractStatus();
		Test.stopTest();

        VF_Contract__c updatedContr = [
            SELECT Id, Implementation_Status__c, Contract_Receive_Date__c
            FROM VF_Contract__c
            WHERE Id = :contract.Id
        ];
        
		System.assertEquals(null, result);
		System.assertEquals('Not started', updatedContr.Implementation_Status__c);
		System.assertEquals(null, updatedContr.Contract_Receive_Date__c); 
    }
    
    @isTest
    private static void testBackToContract() {
        VF_Contract__c contract = new VF_Contract__c();
        insert contract;
                
        PageReference pageRef = Page.VF_ContractRenew;
        Test.setCurrentPage(pageRef);
                
        pageRef.getParameters().put('Id', String.valueOf(contract.Id));
        ApexPages.StandardController sc = new ApexPages.StandardController(contract);
        
        VF_ContractRenewController contrRenCtr = new VF_ContractRenewController(sc);

        Test.startTest();
		pageReference result = contrRenCtr.backToContract();
		Test.stopTest();
        
        System.assertEquals('/'+ contract.Id, result.getUrl());        
    }
}