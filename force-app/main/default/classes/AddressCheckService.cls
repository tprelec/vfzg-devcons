public with sharing class AddressCheckService {
	public static final String POSTAL_CODE = 'zipcode';
	public static final String HOUSE_NUMBER = 'housenumber';
	public static final String HOUSE_NUMBER_EXT = 'extension';

	public static String callAddressCheckService(Map<String, String> params) {
		String response = '';
		String urlParams =
			POSTAL_CODE +
			'/' +
			params.get(POSTAL_CODE) +
			'/' +
			HOUSE_NUMBER +
			'/' +
			params.get(HOUSE_NUMBER);

		if (params.containsKey(HOUSE_NUMBER_EXT)) {
			urlParams += '/' + HOUSE_NUMBER_EXT + '/' + params.get(HOUSE_NUMBER_EXT);
		}

		HttpRequest req = new HttpRequest();
		req.setEndpoint(String.format('callout:AddressCheck/{0}', new List<String>{ urlParams }));
		req.setMethod('GET');

		Http http = new Http();
		HttpResponse res = http.send(req);
		if (res.getStatusCode() == 200) {
			response = res.getBody();
		} else {
			throw new CalloutException(res.getBody());
		}
		return response;
	}

	public class AddressCheckResult {
		public String street { get; set; }
		public String houseNumber { get; set; }
		public String houseNumberExt { get; set; }
		public String zipCode { get; set; }
		public String city { get; set; }
		public List<AddressCheckAvailabilityResult> availability { get; set; }
	}

	public class AddressCheckAvailabilityResult {
		public String name { get; set; }
		public Boolean available { get; set; }
	}
}