@isTest
public with sharing class TestOrderEntryProductAddOnTriggerHandler {
	@isTest
	public static void testAutoNumbering() {
		ExternalIDNumber__c custSetting = new ExternalIDNumber__c(
			Name = 'OE_Product_Add_On',
			External_Number__c = 1,
			Object_Prefix__c = 'OEPAO-52-',
			blocked__c = false,
			runningOrg__c = '00D1l0000008i1vEAA'
		);
		insert custSetting;

		String orgId = UserInfo.getOrganizationId();
		OE_Product__c product = OrderEntryTestFactory.createOEProduct('Main', true);
		OE_Add_On__c addOn = OrderEntryTestFactory.createOEAddOn(
			'Internet Additional',
			'1111',
			true
		);

		Test.startTest();
		OrderEntryTestFactory.createOEProductAddOn(product, addOn, true);
		Test.stopTest();

		OE_Product_Add_On__c result = [SELECT Id, External_Id__c FROM OE_Product_Add_On__c];
		if (custSetting.runningOrg__c == orgId) {
			System.assertEquals(
				'OEPAO-52-000002',
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		} else {
			System.assertEquals(
				null,
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		}
	}
}