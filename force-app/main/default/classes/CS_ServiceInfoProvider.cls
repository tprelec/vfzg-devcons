global with sharing class CS_ServiceInfoProvider implements cssmgnt.RemoteActionDataProvider {
	@RemoteAction
	global static Map<String, Object> getData(Map<String, Object> inputData) {
		System.debug(LoggingLevel.DEBUG, 'CS_ServiceInfoProvider remote action started');

		Map<String, Object> outputMap = new Map<String, Object>();

		try {
			String configurationInput = String.valueOf(inputData.get('configurationInput'));
			List<CS_ServiceInfoProvider.ServiceInfo> inputList = (List<CS_ServiceInfoProvider.ServiceInfo>) JSON.deserialize(
				configurationInput,
				List<CS_ServiceInfoProvider.ServiceInfo>.class
			);

			Map<Id, CS_ServiceInfoProvider.ServiceInfo> configIdToServiceMap = new Map<Id, CS_ServiceInfoProvider.ServiceInfo>();
			for (CS_ServiceInfoProvider.ServiceInfo item : inputList) {
				configIdToServiceMap.put(item.configurationId, item);
			}

			List<csord__Service__c> services = [
				SELECT Id, csord__Status__c, csordtelcoa__Product_Configuration__c
				FROM csord__Service__c
				WHERE csordtelcoa__Product_Configuration__c IN :configIdToServiceMap.keyset()
			];

			for (csord__Service__c service : services) {
				CS_ServiceInfoProvider.ServiceInfo currentItem = configIdToServiceMap.get(service.csordtelcoa__Product_Configuration__c);
				currentItem.serviceActive = service.csord__Status__c == 'Active';
			}

			outputMap.put('status', true);
			outputMap.put('serviceInfo', JSON.serialize(configIdToServiceMap.values()));
		} catch (Exception ex) {
			outputMap.put('status', false);
			outputMap.put('message', ex.getMessage());
			System.debug(LoggingLevel.DEBUG, 'CS_ServiceInfoProvider error ' + ex.getMessage());
		}

		return outputMap;
	}

	/*
	 * Contains parameters needed to fetch the service info (configurationId, configurationGuid) parameter which is populated based on query (serviceActive)
	 */
	global with sharing class ServiceInfo {
		global Id configurationId { get; set; }
		global String configurationGuid { get; set; }
		global Boolean serviceActive { get; set; }
	}
}
