//This is the custom controller class for the CS Orchestrator custom step 'Validate FWA Order'.
global class OrchSubmitFWAOrderExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable{
    private Map<Id,List<OrchCalloutsWrapperCls.calloutData>> calloutResultsMap = new Map<Id,List<OrchCalloutsWrapperCls.calloutData>>();
    private Map<Id,String> mapOrderId;
    private Map<Id,String> mapUnifyOrderId;
    
    public Boolean performCallouts(List<SObject> data) {
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        calloutResultsMap = new Map<Id,List<OrchCalloutsWrapperCls.calloutData>>();
        Boolean calloutsPerformed = false;
        try{
            Set<Id> resultIds = new Set<Id>();
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                resultIds.add( step.CSPOFA__Orchestration_Process__c );
            }
            OrchUtils.getProcessDetails(resultIds);
            mapOrderId = OrchUtils.mapOrderId;
            mapUnifyOrderId = OrchUtils.mapUnifyOrderId;
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                Id processId = step.CSPOFA__Orchestration_Process__c;
                
                String orderId = mapOrderId.get( processId );
                String unifyFrameworkOrderId = mapUnifyOrderId.get( processId );

                calloutResultsMap.put(step.Id,EMP_BSLintegration.submitOrder(unifyFrameworkOrderId, orderId));      
                calloutsPerformed = true;          
            }
        } catch (exception e) {
            system.debug(e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString());
        }
        return calloutsPerformed;
    }
    
    public List<sObject> process(List<sObject> data) {
        List<sObject> result = new List<sObject>();
        List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        for (CSPOFA__Orchestration_Step__c step : stepList) {
            Boolean success = false;
            if(calloutResultsMap.containsKey(step.Id)) {
                List<OrchCalloutsWrapperCls.calloutData> calloutResult = calloutResultsMap.get(step.Id);
                if(calloutResult != null && (Boolean) calloutResult[0].success == true ) {
                    success = true;
                }
                if(success) { 
                    step = OrchUtils.setStepRecord( step , false , 'Submit Create Framework step completed' );
                } else { 
                    step = OrchUtils.setStepRecord( step , true , 'Error occurred: ' + (String) calloutResult[0].errorMessage  );
                }
                step.Request__c = calloutResult[0].strReq;
                step.Response__c = calloutResult[0].strRes;
            } else {
                step = OrchUtils.setStepRecord( step , true , 'Error occurred: Callout results not received.' );  	                
            }
            result.add(step);
    	}
        return result;
    }
}