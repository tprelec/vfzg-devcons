public class MockedEndpointsRoute extends RestRoute {
	protected override boolean hasResource() {
		return false; //do parse the next url part as resourceId
	}

	protected override Map<String, RestRoute> getNextRouteMap() {
		return new Map<String, RestRoute>{
			'cdo' => new MockedEndpoints_CDORoute(),
			//'isw' => new CompanyEmployeeRoute(),
			'dxl' => new MockedEndpoints_DXLRoute()
		};
	}
}
