public with sharing class Sequence {
    ExternalIDNumber__c currRec;
    Boolean blockMassive = false;

    public class SequenceException extends Exception {}

    // TODO: if we will use Cache, then load values to the cache.
    public Sequence(String sequenceName) {
        this.currRec = ExternalIDNumber__c.getInstance(sequenceName);
    }

    public Boolean canBeUsed() {
        if(UserInfo.getOrganizationId() == this.currRec.runningOrg__c) {
            return true;
        } else {
            return false;
        }
    }

    public void startMutex() {
        // This code lives in a multithreaded environment, this flag doesn't guarantee consistency because the
        // process manager could decide to interrupt this process between lines, with another instance of `inc`
        // which could try to block the same record before the following line be executed.
        // ref: https://stackoverflow.com/questions/34524/what-is-a-mutex
        if(currRec.blocked__c == true) {
            throw new SequenceException('The current Seq has been blocked. Try again later.');
        } else {
            currRec.blocked__c = true;
            update currRec;
        }
        this.blockMassive = true;
    }

    public void endMutex() {
        currRec.blocked__c = false;
        update currRec;
        this.blockMassive = false;
    }

    // Use useMutex = true for a single execution.
    // Use startMutex() ... incr()... incr() ... endMutex() on massive increments to enhance performance.
    public String incr(Integer incrementor, Integer ciphers, Boolean useMutex) {
        if(useMutex == true) {
            if(currRec.blocked__c == true) {
                throw new SequenceException('The current Seq has been blocked. Try again later.');
            }
            currRec.blocked__c = true;
            update currRec;
        } else if(this.blockMassive == false) {
            // This avoid the increment without any kind of blocking method.
            throw new SequenceException('You must to set mutex with startMutex method or useMutex = true.');
        }
        currRec.External_Number__c = currRec.External_Number__c + incrementor;
        if (useMutex == true) {
            currRec.blocked__c = false;
        }
        update currRec;
        return this.formatValue(currRec.External_Number__c.intValue(), ciphers);
    }

    public Integer show() {
        return currRec.External_Number__c.intValue();
    }

    public String showFormatted(Integer ciphers) {
        return formatValue(currRec.External_Number__c.intValue(), ciphers);
    }

    public Boolean isBlocked() {
        return (currRec.blocked__c || this.blockMassive);

    }

    public String formatValue(Integer iValue, Integer ciphers) {
        String counter = String.valueOf(iValue);
        String formatter = ('00000'+counter).right(ciphers);
        return currRec.Object_Prefix__c + formatter;

    }
}