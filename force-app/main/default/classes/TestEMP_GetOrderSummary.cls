@isTest
public class TestEMP_GetOrderSummary {

    @isTest
    public static void testResponseGetOrderSummaryBulk() {
        String allcontents = '{"data":[{"orderSummary":[{"provideOrderActionsSummary":[{"implementedOffer":{},"orderActionSummary":[{"implementedProduct":{},"portingStatus":"Completed","orderActionType":"PR","orderActionStatus":"DO","serviceRequiredDate":1599170399000,"wishDate":1599201000000}]}],"orderHeader":{"orderID":"557474284A","daysToExpiration":-13,"applicationDate":1599091026000,"expiryDate":1600300626000,"orderStatus":"DO","customerProfileHeader":{"customerName":"ITE2 A & Z klusje","customerID":800050330},"creditVettingStatus":"Status: REFERRED.Aanvraag wordt handmatig behandeld","creditVettingReason":"Aanvraag wordt handmatig behandeld","amendPonr":"No","newPhoneNrInd":false},"totalPrices":{"totalOC":25,"totalRC":12.59,"previousTotalRC":0,"originalTotalOC":25,"discountTotalOC":0,"overrideTotalOC":0,"taxTotalOC":5.25,"originalTotalRC":12.59,"discountTotalRC":0,"taxTotalRC":2.64,"proratedTotalRC":11.75,"proratedDiscountTotalRC":0,"proratedTaxTotalRC":2.47,"previousTaxTotalRC":0}}]}]}';
        test.startTest();
            EMP_GetOrderSummary.Response res = EMP_GetOrderSummary.parse(allcontents);
            System.assertEquals(true, res != null, 'meaning the parse was done sucessfully');
        test.stopTest();
    }

    @isTest
    public static void testRequestGetOrderSummaryBulk() {
        List<EMP_GetOrderSummary.BslHttpHeader> requestHeaders = new List<EMP_GetOrderSummary.BslHttpHeader>();
        requestHeaders.add(new EMP_GetOrderSummary.BslHttpHeader('DealerCode', '00822033'));
        requestHeaders.add(new EMP_GetOrderSummary.BslHttpHeader('MessageID', string.valueOf(system.now())));

        List<EMP_GetOrderSummary.GetOrderSummaryRequest> getOrderSummaryBulkData = New List<EMP_GetOrderSummary.GetOrderSummaryRequest>();
        getOrderSummaryBulkData.add(New EMP_GetOrderSummary.GetOrderSummaryRequest(requestHeaders, '0vx7862394sfw', 5, '8672572A'));

        //request headers
        List<EMP_GetOrderSummary.SharedBslHttpHeader> sharedRequestHeaders = new List<EMP_GetOrderSummary.SharedBslHttpHeader>();
        sharedRequestHeaders.add(new EMP_GetOrderSummary.SharedBslHttpHeader('DealerCode', 'not used'));
        sharedRequestHeaders.add(new EMP_GetOrderSummary.SharedBslHttpHeader('MessageID', string.valueOf(system.now())));
        //request wrapper
        EMP_GetOrderSummary.GetOrderSummaryBulkRequest getOrderSummaryBulkRequest = New EMP_GetOrderSummary.GetOrderSummaryBulkRequest(sharedRequestHeaders, getOrderSummaryBulkData);
        EMP_GetOrderSummary.GetOrderSummaryBulkRequestTop getOrderSummaryBulkRequestComplete = New EMP_GetOrderSummary.GetOrderSummaryBulkRequestTop(getOrderSummaryBulkRequest);

        test.startTest();
            EMP_GetOrderSummary.Request obj = new EMP_GetOrderSummary.Request( '8675675234A' );
            System.assertEquals(true, obj != null, 'meaning the parse was done sucessfully');
        test.stopTest();
    }
}