/**
 * @author: Rahul Sharma (07-09-2021)
 * @description: Handler for SitePostalCheckEventTrigger, trigger framework is not compatible with platform events object
 * Jira ticket reference: COM-1960
 */

public with sharing class SitePostalCheckEventTriggerHandler {
    // used to isolate the unit test of ISW check functionality
    @TestVisible
    private static Boolean ISW_CHECK_ENABLED;

    @TestVisible
    private static Boolean isIswCheckEnabled() {
        if (ISW_CHECK_ENABLED == null) {
            return !Test.isRunningTest();
        }
        return ISW_CHECK_ENABLED;
    }

    public void afterInsert(
        List<SitePostalCheckEvent__e> newSitePostalCheckEvents
    ) {
        // Without this check, all existing tests where sites is created would fail.
        if (isIswCheckEnabled()) {
            triggerISWSiteCheckQueueable(newSitePostalCheckEvents);
        }
    }

    private void triggerISWSiteCheckQueueable(
        List<SitePostalCheckEvent__e> sitePostalCheckEvents
    ) {
        Set<Id> setSiteId = new Set<Id>();
        for (
            SitePostalCheckEvent__e sitePostalCheckEvent : sitePostalCheckEvents
        ) {
            if (sitePostalCheckEvent.Site_Id__c != null) {
                setSiteId.add(sitePostalCheckEvent.Site_Id__c);
            }
        }

        if (!setSiteId.isEmpty()) {
            List<Id> lstSetId = new List<Id>();
            lstSetId.addAll(setSiteId);
            System.enqueueJob(new ISWSiteCheckQueueable(lstSetId));
        }
    }
}