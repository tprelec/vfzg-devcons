public class LG_OpenCloudSenseController 
{
    public String pageTitle {get;set;}
    public String entityId {get;set;}
    public String redirectUrl {public get; private set;}

    public LG_OpenCloudSenseController() 
    {
        this.entityId = ApexPages.currentPage().getParameters().get('id');
        pageTitle = 'Open Cloudsense Anywhere'; 
        this.redirectUrl = 'cloudsenseanywhere://entity/viewDetails?id=' + this.entityId;
    }
    


    // public PageReference openCloudsense() 
    // {
    //     PageReference cloudsenseAnywhereApp = new PageReference('cloudsenseanywhere://entity/viewDetails?id=' + this.entityId);
    //     //PageReference cloudsenseAnywhereApp = new PageReference('http://www.google.com/q?=' + this.leadID);
    //     cloudsenseAnywhereApp.setRedirect(true);
    //     return cloudsenseAnywhereApp;
    // }

}