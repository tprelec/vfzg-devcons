@isTest
public class TestUserTriggerHandler {
	@isTest
	static void testActivateCommunityUser() {
		Account partnerAccount;
		Contact portalContact;
		User commUser;

		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount = TestUtils.createPartnerAccount();
			portalContact = TestUtils.createContact(partnerAccount);
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			TestUtils.autoCommit = false;
			commUser = TestUtils.createPortalUser(partnerAccount);
			commUser.isActive = false;
			commUser.contactId = portalContact.Id;
		}

		Test.startTest();
		System.runAs(adminsByNumber.get('Admin2')) {
			insert commUser;
		}
		Test.stopTest();

		User user = [SELECT Id, IsActive FROM User WHERE Email = :adminsByNumber.get('Admin1').Email];
		System.assertEquals(true, user.IsActive, 'User is added');
	}

	@isTest
	static void testActivateUser() {
		User u;
		System.runAs(GeneralUtils.currentUser) {
			TestUtils.autoCommit = false;
			u = TestUtils.createAdministrator();
			u.isActive = false;
			insert u;
		}

		Test.startTest();
		u.Email = 'You\'renotfired@vodafoneziggo.nl';
		u.IsActive = true;
		update u;
		Test.stopTest();

		User user = [SELECT Id, IsActive FROM User WHERE Email = 'You\'renotfired@vodafoneziggo.nl'];
		System.assertEquals(true, user.IsActive, 'User is active');
	}

	@isTest
	static void testCheckLocaleEnUs() {
		User testUser = TestUtils.generateTestUser('My Test User', 'McTesty', 'VF Sales Manager');
		testUser.LocaleSidKey = 'en_US';
		String result = null;

		Test.startTest();
		try {
			insert testUser;
		} catch (Exception e) {
			result = e.getDmlMessage(0);
		}
		Test.stopTest();

		System.assertEquals(
			'Locales English (United States), English (Canada) are blocked by VF NL because they cause invalid /// number formats.',
			result,
			'System should throw an error'
		);
	}

	@isTest
	static void testCheckLocaleNl() {
		User testUser = TestUtils.generateTestUser('My Test User', 'McTesty', 'VF Sales Manager');
		testUser.LocaleSidKey = 'nl';
		String result = null;

		Test.startTest();
		try {
			insert testUser;
		} catch (Exception e) {
			result = e.getDmlMessage(0);
		}
		Test.stopTest();
		System.assertEquals('Please select Dutch(Netherlands) for dutch locale.', result, 'System should throw an error');
	}

	/**The UserTriggerHandlerBatch has it's own TestClass as well*/
	@isTest
	static void testFreeUpCPQLicencse() {
		System.runAs(GeneralUtils.currentUser) {
			TestUtils.createVodafoneAccount();
		}
		User testUser = TestUtils.generateTestUser('My Test User', 'McTesty', 'VF Sales Manager');
		insert testUser;

		Test.startTest();
		testUser.isActive = false;
		System.assert([SELECT Id, IsActive FROM User WHERE Id = :testUser.Id].IsActive, 'User is not Active');
		update testUser;
		Test.stopTest();
	}

	@isTest
	static void testPreventDeleteDealerInformation() {
		Account partnerAccount;
		Contact portalContact;
		User commUser;

		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

		// Create Account and set the Type to Dealer
		System.runAs(adminsByNumber.get('Admin1')) {
			TestUtils.autoCommit = false;
			partnerAccount = TestUtils.createPartnerAccount();
			partnerAccount.Type = 'Dealer';
			insert partnerAccount;
			partnerAccount.IsPartner = true;
			update partnerAccount;
			TestUtils.autoCommit = true;
			portalContact = TestUtils.createContact(partnerAccount);
		}

		// Create a dealer user as owner for the dealer info
		System.runAs(adminsByNumber.get('Admin2')) {
			TestUtils.autoCommit = false;
			commUser = TestUtils.createPortalUser(partnerAccount);
			commUser.contactId = portalContact.Id;
			insert commUser;
		}

		//Create the dealer info and update the contact with the ID from our created user
		System.runAs(adminsByNumber.get('Admin1')) {
			TestUtils.autoCommit = true;
			portalContact.UserId__c = commUser.Id;
			update portalContact;
			Dealer_Information__c di = TestUtils.createDealerInformation(portalContact.Id);
		}

		String errorThrown;
		Test.startTest();
		System.runAs(adminsByNumber.get('Admin2')) {
			commUser.IsActive = false;
			try {
				TriggerHandler.resetRecursion('UserTriggerHandler');
				update commUser;
			} catch (Exception e) {
				errorThrown = e.getMessage() + e.getStackTraceString();
			}
		}
		Test.stopTest();

		System.assert(
			errorThrown != null && errorThrown.contains('This user is the main contact for Dealer Information(s)'),
			'Trigger should throw a custom exception for Dealer Information. The caught exception is: ' + errorThrown
		);
	}

	@isTest
	static void testPreventDeleteAccountOwner() {
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
		Account acc1;
		System.runAs(adminsByNumber.get('Admin1')) {
			acc1 = TestUtils.createAccount(adminsByNumber.get('Admin1'));
		}
		User toDeactivate = [SELECT Id, IsActive FROM User WHERE Id = :acc1.OwnerId];

		String errorThrown;
		Test.startTest();
		toDeactivate.IsActive = false;
		try {
			System.runAs(adminsByNumber.get('Admin2')) {
				update toDeactivate;
			}
		} catch (Exception e) {
			errorThrown = e.getMessage() + e.getStackTraceString();
		}
		Test.stopTest();

		System.assert(
			errorThrown != null && errorThrown.contains('This user is the Account Owner for one or more accounts.'),
			'Trigger should throw a custom exception for Account Ownership. The caught exception is: ' + errorThrown
		);
	}

	@isTest
	static void testPreventDeleteAccountTeamMember() {
		List<Account> accounts = new List<Account>();
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

		System.runAs(adminsByNumber.get('Admin1')) {
			accounts.add(TestUtils.createAccount(adminsByNumber.get('Admin1')));
			accounts.add(TestUtils.createAccount(adminsByNumber.get('Admin1')));

			for (account a : accounts) {
				a.IsPartner = true;
				a.Dealer_code__c = '999998';
			}

			update accounts;

			TestUtils.autoCommit = true;
			TestUtils.createAccountTeamMember(adminsByNumber.get('Admin1'), accounts[0]);
			TestUtils.createAccountTeamMember(adminsByNumber.get('Admin1'), accounts[1]);
		}

		String errorThrown;
		Test.startTest();
		adminsByNumber.get('Admin1').IsActive = false;
		try {
			System.runAs(adminsByNumber.get('Admin2')) {
				update adminsByNumber.get('Admin1');
			}
		} catch (Exception e) {
			errorThrown = e.getMessage() + e.getStackTraceString();
		}
		Test.stopTest();

		System.assert(
			errorThrown != null && errorThrown.contains('This user is an Account Team Member for one or more Partner Accounts.'),
			'Trigger should throw a custom exception for Account Ownership. The caught exception is: ' + errorThrown
		);
	}

	@isTest
	static void testPreventDeleteOpportunityOwner() {
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

		System.runAs(adminsByNumber.get('Admin1')) {
			Account acct = TestUtils.createAccount(adminsByNumber.get('Admin1'));
			Pricebook2 pb = TestUtils.createPricebook(false);
			Ban__c ban = TestUtils.createBan(acct);

			TestUtils.createOpportunityWithBan(acct, pb.id, ban, adminsByNumber.get('Admin1'));
			TestUtils.createOpportunityWithBan(acct, pb.id, ban, adminsByNumber.get('Admin1'));
		}

		User toDeactivate = [SELECT Id, IsActive FROM User WHERE Id = :adminsByNumber.get('Admin1').id];

		String errorThrown;
		Test.startTest();
		toDeactivate.IsActive = false;
		try {
			System.runAs(adminsByNumber.get('Admin2')) {
				update toDeactivate;
			}
		} catch (Exception e) {
			errorThrown = e.getMessage() + e.getStackTraceString();
		}
		Test.stopTest();

		System.assert(
			errorThrown != null && errorThrown.contains('This user is the owner of one or more open Opportunities.'),
			'Trigger should throw a custom exception for open Opportunity Ownership The caught exception is: ' + errorThrown
		);
	}

	@isTest
	static void testPreventDeleteOpportunitySolutionSales() {
		Map<String, User> adminsByNumber = TestUtils.createAdmins(3);
		User setNewProfile;

		System.runAs(adminsByNumber.get('Admin3')) {
			setNewProfile = [SELECT id, profileId FROM user WHERE id = :adminsByNumber.get('Admin1').Id];
			Profile prof = [SELECT id FROM Profile WHERE name = 'VF Sales Manager'];
			setNewProfile.ProfileId = prof.Id;
			update setNewProfile;
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			Account acct = TestUtils.createAccount(GeneralUtils.currentUser);
			Pricebook2 pb = TestUtils.createPricebook(false);
			Ban__c ban = TestUtils.createBan(acct);

			TestUtils.autoCommit = false;
			Opportunity opp1 = TestUtils.createOpportunityWithBan(acct, pb.id, ban, GeneralUtils.currentUser);
			Opportunity opp2 = TestUtils.createOpportunityWithBan(acct, pb.id, ban, GeneralUtils.currentUser);

			opp1.Solution_Sales__c = setNewProfile.Id;
			opp2.Solution_Sales__c = setNewProfile.Id;

			insert opp1;
			insert opp2;
		}

		//User toDeactivate = [select Id, IsActive from User where Id =: adminsByNumber.get('Admin1').Id];

		String errorThrown;
		Test.startTest();
		setNewProfile.IsActive = false;
		try {
			System.runAs(adminsByNumber.get('Admin3')) {
				TriggerHandler.resetRecursion('UserTriggerHandler');
				update setNewProfile;
			}
		} catch (Exception e) {
			errorThrown = e.getMessage() + e.getStackTraceString();
		}
		Test.stopTest();

		System.assert(
			errorThrown != null && errorThrown.contains('This user is the solution sales of one or more open Opportunities.'),
			'Trigger should throw a custom exception for open Opportunity Solution Sales. The caught exception is: ' + errorThrown
		);
	}

	@isTest
	static void testPreventDeleteManager() {
		Map<String, User> adminsByNumber = TestUtils.createAdmins(4);

		System.runAs(adminsByNumber.get('Admin1')) {
			adminsByNumber.get('Admin2').ManagerId = adminsByNumber.get('Admin1').Id;
			adminsByNumber.get('Admin3').ManagerId = adminsByNumber.get('Admin1').Id;
			update adminsByNumber.values();
		}

		String errorThrown;
		Test.startTest();
		TriggerHandler.resetRecursion('UserTriggerHandler');
		adminsByNumber.get('Admin1').IsActive = false;
		try {
			System.runAs(adminsByNumber.get('Admin4')) {
				update adminsByNumber.get('Admin1');
			}
		} catch (Exception e) {
			errorThrown = e.getMessage() + e.getStackTraceString();
		}
		Test.stopTest();

		System.assert(
			errorThrown != null && errorThrown.contains('This user is the manager of one or more other users.'),
			'Trigger should throw a custom exception for open Opportunity Solution Sales. The caught exception is: ' + errorThrown
		);
	}

	@isTest
	static void testPreventDeleteDashboardRunningUser() {
		AggregateResult multipleDashboards;
		list<AggregateResult> dashboardsByRunningUser = [
			SELECT RunningUserId, Count(Id)
			FROM Dashboard
			WHERE RunningUser.IsActive = TRUE
			GROUP BY RunningUserId
		];

		for (AggregateResult ar : dashboardsByRunningUser) {
			if (Integer.valueOf(ar.get('expr0')) >= 2) {
				multipleDashboards = ar;
			}
		}
		if (multipleDashboards != null) {
			User toDeactivate = [SELECT Id, IsActive FROM User WHERE Id = :String.valueOf(multipleDashboards.get('RunningUserId'))];

			String errorThrown;
			Test.startTest();
			toDeactivate.IsActive = false;
			try {
				update toDeactivate;
			} catch (Exception e) {
				errorThrown = e.getMessage() + e.getStackTraceString();
			}
			Test.stopTest();
			System.assert(
				errorThrown != null && errorThrown.contains('This user is the running user on one or more dashboards.'),
				'Trigger should throw a custom exception for being dashboard running user. The caught exception is: ' + errorThrown
			);
		}
	}

	@isTest
	static void testCheckLicensesZeroLicense() {
		Account partnerAccount;

		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount = TestUtils.createPartnerAccount();
			partnerAccount.IsPartner = true;
			partnerAccount.Total_Licenses__c = 0;
			update partnerAccount;
		}
		System.runAs(adminsByNumber.get('Admin2')) {
			String error;
			try {
				User testPortalUser = TestUtils.createPortalUser(partnerAccount);
			} catch (Exception e) {
				error = e.getMessage() + e.getStackTraceString();
			}
			System.assert(
				error != null && error.contains('There are no available licenses left.'),
				'Trigger should throw an error. The caught exception is: ' + error
			);
		}
	}

	@isTest
	static void testCheckLicensesNull() {
		Account partnerAccount;

		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount = TestUtils.createPartnerAccount();
			partnerAccount.IsPartner = true;
			partnerAccount.Total_Licenses__c = null;
			update partnerAccount;
		}
		System.runAs(adminsByNumber.get('Admin2')) {
			String error;
			try {
				User testPortalUser = TestUtils.createPortalUser(partnerAccount);
			} catch (Exception e) {
				error = e.getMessage();
			}
			System.assert(
				error != null && error.contains('There are no licenses defined.'),
				'Trigger should throw an error. The caught exception is: ' + error
			);
		}
	}

	@isTest
	static void testCheckLicensesUpdateNull() {
		Account partnerAccount;
		User testPortalUser;

		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount = TestUtils.createPartnerAccount();
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			testPortalUser = TestUtils.createPortalUser(partnerAccount);
		}

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount.IsPartner = true;
			partnerAccount.Total_Licenses__c = null;
			update partnerAccount;
		}

		TriggerHandler.resetRecursion();
		System.runAs(adminsByNumber.get('Admin2')) {
			String error;
			try {
				testPortalUser.isActive = false;
				update testPortalUser;
			} catch (Exception e) {
				error = e.getMessage() + e.getStackTraceString();
			}
			System.assert(
				error != null && error.contains('There are no licenses defined.'),
				'Trigger should throw an error. The caught exception is: ' + error
			);
		}
	}

	@isTest
	static void testCheckLicensesUpdateZero() {
		Account partnerAccount;
		User testPortalUser;

		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 1);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);

		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount = TestUtils.createPartnerAccount();
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			testPortalUser = TestUtils.createPortalUser(partnerAccount);
		}

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount.IsPartner = true;
			partnerAccount.Total_Licenses__c = 0;
			update partnerAccount;
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			String error;
			testPortalUser.isActive = false;
			update testPortalUser;

			TriggerHandler.resetRecursion('UserTriggerHandler');
			try {
				testPortalUser.isActive = true;
				update testPortalUser;
			} catch (Exception e) {
				error = e.getMessage() + e.getStackTraceString();
			}
			System.assert(
				error != null && error.contains('There are no available licenses left.'),
				'Trigger should throw an error. The caught exception is: ' + error
			);
		}
	}

	@isTest
	static void testCheckPrimaryUserUpdate() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
		User primaryUser1;
		User primaryUser2;
		Account partnerAccount;
		Contact portalContact1;
		Contact portalContact2;

		System.runAs(adminsByNumber.get('Admin1')) {
			TestUtils.autoCommit = false;
			partnerAccount = TestUtils.createPartnerAccount();
			partnerAccount.Dealer_code__c = '945312';
			insert partnerAccount;
			TestUtils.autoCommit = true;
			portalContact1 = TestUtils.createContact(partnerAccount);
			portalContact2 = TestUtils.createContact(partnerAccount);
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			TestUtils.autoCommit = false;

			primaryUser1 = TestUtils.createPortalUser(partnerAccount);
			primaryUser1.ContactId = portalContact1.Id;
			primaryUser1.Primary_Partner__c = true;

			primaryUser2 = TestUtils.createPortalUser(partnerAccount);
			primaryUser2.ContactId = portalContact2.Id;

			TriggerHandler.resetRecursion('UserTriggerHandler');
			insert primaryUser1;
			insert primaryUser2;

			TriggerHandler.resetRecursion('UserTriggerHandler');

			primaryUser2.Primary_Partner__c = true;
		}

		String error;

		Test.startTest();
		System.runAs(adminsByNumber.get('Admin2')) {
			try {
				update primaryUser2;
			} catch (Exception e) {
				error = e.getMessage();
			}
		}
		Test.stopTest();

		System.assert(
			error != null && error.contains('There can only be one Primary Partner User'),
			'Error should be thrown for making a second primary partner. The caught exception is: ' + error
		);
	}

	@isTest
	static void testCheckPrimaryUserInsert() {
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
		User primaryUser1;
		User primaryUser2;
		Account partnerAccount;
		Contact portalContact1;
		Contact portalContact2;

		System.runAs(adminsByNumber.get('Admin1')) {
			TestUtils.autoCommit = false;
			partnerAccount = TestUtils.createPartnerAccount();
			partnerAccount.Dealer_code__c = '154365';
			insert partnerAccount;
			TestUtils.autoCommit = true;
			portalContact1 = TestUtils.createContact(partnerAccount);
			portalContact2 = TestUtils.createContact(partnerAccount);
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			TestUtils.autoCommit = false;

			primaryUser1 = TestUtils.createPortalUser(partnerAccount);
			primaryUser1.ContactId = portalContact1.Id;
			primaryUser1.Primary_Partner__c = true;

			primaryUser2 = TestUtils.createPortalUser(partnerAccount);
			primaryUser2.ContactId = portalContact2.Id;
			primaryUser2.Primary_Partner__c = true;
		}

		String error;

		Test.startTest();
		System.runAs(adminsByNumber.get('Admin2')) {
			try {
				TriggerHandler.resetRecursion('UserTriggerHandler');
				insert primaryUser1;
				insert primaryUser2;
			} catch (Exception e) {
				error = e.getMessage();
			}
		}
		Test.stopTest();
		System.assert(
			error != null && error.contains('There can only be one Primary Partner User'),
			'Error should be thrown for making a second primary partner. The caught exception is: ' + error
		);
	}

	@isTest
	static void testAssignPermissionsSetToPartnerInsert() {
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
		Account partnerAccount;
		Contact portalContact;
		User testPortalUser;

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount = TestUtils.createPartnerAccount();
			portalContact = TestUtils.createContact(partnerAccount);
		}

		Test.startTest();
		System.runAs(adminsByNumber.get('Admin2')) {
			TestUtils.autoCommit = false;
			testPortalUser = TestUtils.createPortalUser(partnerAccount);
			testPortalUser.ContactId = portalContact.Id;
			testPortalUser.Admin_Partner__c = true;
			insert testPortalUser;
		}
		Test.stopTest();

		list<PermissionSetAssignment> pSetAssignment = [
			SELECT id
			FROM PermissionSetAssignment
			WHERE PermissionSet.Name = 'Delegated_External_User_Administrator' AND AssigneeId = :testPortalUser.Id
		];

		System.assertEquals(1, pSetAssignment.size(), 'PermissionSet should be assigned to user');
	}

	@isTest
	static void testAssignPermissionsSetToPartnerUpdate() {
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
		Account partnerAccount;
		User testPortalUser;

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount = TestUtils.createPartnerAccount();
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			testPortalUser = TestUtils.createPortalUser(partnerAccount);
		}
		TriggerHandler.resetRecursion();
		Test.startTest();
		System.runAs(adminsByNumber.get('Admin2')) {
			testPortalUser.Admin_Partner__c = true;
			update testPortalUser;
		}
		Test.stopTest();

		list<PermissionSetAssignment> pSetAssignment = [
			SELECT id
			FROM PermissionSetAssignment
			WHERE PermissionSet.Name = 'Delegated_External_User_Administrator' AND AssigneeId = :testPortalUser.Id
		];

		System.assertEquals(1, pSetAssignment.size(), 'PermissionSet should be assigned to user');
	}

	@isTest
	static void testAssignPermissionsSetToPartnerDelete() {
		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
		Account partnerAccount;
		Contact portalContact;
		User testPortalUser;

		System.runAs(adminsByNumber.get('Admin1')) {
			partnerAccount = TestUtils.createPartnerAccount();
			portalContact = TestUtils.createContact(partnerAccount);
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			TestUtils.autoCommit = false;
			testPortalUser = TestUtils.createPortalUser(partnerAccount);
			testPortalUser.ContactId = portalContact.Id;
			testPortalUser.Admin_Partner__c = true;
			insert testPortalUser;
		}

		Test.startTest();
		TriggerHandler.resetRecursion('UserTriggerHandler');
		testPortalUser.Admin_Partner__c = false;
		update testPortalUser;
		Test.stopTest();

		list<PermissionSetAssignment> pSetAssignment = [
			SELECT id
			FROM PermissionSetAssignment
			WHERE PermissionSet.Name = 'Delegated_External_User_Administrator' AND AssigneeId = :testPortalUser.Id
		];

		System.assertEquals(0, pSetAssignment.size(), 'PermissionSet should be deleted for user');
	}

	@isTest
	static void whenUserRecordChangesThenConnectedContactChanges() {
		String contactFirstName = 'John';
		String contactChangedFirstName = 'Martin';

		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);
		Account account;
		Contact contact;
		User user;

		System.runAs(adminsByNumber.get('Admin1')) {
			user = TestUtils.generateTestUser(contactFirstName, 'McTesty', 'VF Sales Manager');
			insert user;
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			account = TestUtils.createAccount(user);
			TestUtils.autoCommit = false;
			contact = TestUtils.createContact(account);
			contact.UserId__c = user.Id;
			insert contact;
		}

		Test.startTest();
		user.FirstName = contactChangedFirstName;
		update user;
		Test.stopTest();

		Contact contactAfterUpdate = [SELECT FirstName FROM Contact WHERE id = :contact.Id];

		System.assertEquals(contactChangedFirstName, contactAfterUpdate.FirstName, 'Contact record should have reflected changes on the User record');
	}
}
