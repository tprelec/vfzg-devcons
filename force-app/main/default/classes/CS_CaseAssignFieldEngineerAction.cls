public with sharing class CS_CaseAssignFieldEngineerAction {
    @InvocableMethod(label = 'Case Assign Field Engineer')
    public static void caseAssignFieldEngineer(List<Id> caseSet) {
        // TODO: Finish this
        if (!caseSet.isEmpty()) {
            List<Case> lCasesToUpdate = new List<Case>();
            Set<Id> solutionIdSet = new Set<Id>();
            for(Case currCase: [SELECT Id, Suborder__c, OwnerId FROM Case WHERE Id IN :caseSet]) {
                if(currCase.Suborder__c != null) {
                    lCasesToUpdate.add(currCase);
                    solutionIdSet.add(currCase.Suborder__c);
                }
            }

            Map<Id, Id> fieldEngineerPerSolution = new Map<Id, Id>();
            for(Delivery_Appointment__c currAppt: [
                SELECT Related_Suborder__c, Field_Engineer__c FROM Delivery_Appointment__c
                WHERE Confirmed_Installation__c = true AND Related_Suborder__c IN :solutionIdSet
                ORDER BY Related_Suborder__c
            ]) {
                fieldEngineerPerSolution.put(currAppt.Related_Suborder__c, currAppt.Field_Engineer__c);
            }

            for(Integer i=0; i < lCasesToUpdate.size(); i++) {
                lCasesToUpdate[i].OwnerId = fieldEngineerPerSolution.get(lCasesToUpdate[i].Suborder__c);
            }
            update lCasesToUpdate;
        }
    }
}