@IsTest
private class PP_LightningControllerTest
{
    @IsTest
    static void test_current_context()
    {
        ApexPages.StandardController stdCtrl = null;
        PP_LightningController ctrl = new PP_LightningController(stdCtrl);

        System.assertNotEquals(null, ctrl.getIsLEX());
        System.assertNotEquals(null, ctrl.getIsLightningCommunity());

        try
        {
            // dummy tests to cover constructors

            ApexPages.StandardSetController stdSetCtrl = null;
            ctrl = new PP_LightningController(stdSetCtrl);

            TrainingRegistrationController trc = null;
            ctrl = new PP_LightningController(trc);

            PartnerInfo pi = null;
            ctrl = new PP_LightningController(pi);

            OpportunityAttachmentManagerController oamc = null;
            ctrl = new PP_LightningController(oamc);

            OrderFormCustomerDataController ofcdc = null;
            ctrl = new PP_LightningController(ofcdc);

            OrderFormTemplateController oftc = null;
            ctrl = new PP_LightningController(oftc);

            OrderFormAddProductDataController ofapdc = null;
            ctrl = new PP_LightningController(ofapdc);

            OrderFormBillingDataController ofbdc = null;
            ctrl = new PP_LightningController(ofbdc);

            OrderFormCTNDetailsController ctnCtrl = null;
            ctrl = new PP_LightningController(ctnCtrl);

            OrderFormDeliveryDataController ofddc = null;
            ctrl = new PP_LightningController(ofddc);

            OrderFormNumberPortingDataController ofnpdc = null;
            ctrl = new PP_LightningController(ofnpdc);

            OrderFormOrderDataController ofodc = null;
            ctrl = new PP_LightningController(ofodc);

            OrderFormOrderProgressController progrCtrl = null;
            ctrl = new PP_LightningController(progrCtrl);

            OrderFormPhonebookRegDataController phoneCtrl = null;
            ctrl = new PP_LightningController(phoneCtrl);
        }
        catch(Exception exc)
        {
            System.debug(LoggingLevel.ERROR, exc.getMessage());
            System.assert(false, 'Should not be here');
        }
    }
}