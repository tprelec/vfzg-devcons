@isTest
private class TestSiteTriggerHandler {
	public static Sales_Settings__c ss;
	public static Account acct;
	public static Ban__c ban;

	private static void createTestData() {
		ss = new Sales_Settings__c(
			Max_weekly_postalcode_checks__c = 0,
			Max_Daily_Postalcode_Checks__c = 1,
			Postalcode_check_validity_days__c = 3
		);
		insert ss;

		User owner = TestUtils.createAdministrator();
		acct = TestUtils.createAccount(owner);
		acct.BOPCode__c = 'ABC';
		update acct;
		ban = TestUtils.createBan(acct);
		ban.BOP_export_datetime__c = null;
	}
	@isTest
	public static void testPreparePostalCodeCheckWeeklyMaximum() {
		createTestData();
		ss.Max_weekly_postalcode_checks__c = -1;
		update ss;

		TestUtils.autoCommit = false;
		Site__c s1 = TestUtils.createSite(acct);
		s1.Last_dsl_check__c = System.today();

		Test.startTest();
		try {
			insert s1;
		} catch (Exception e) {
			System.assertEquals(
				'Weekly maximum (0) reached. Please request manually.',
				e.getMessage(),
				'Weekly maximum.'
			);
		}
		Test.stopTest();
	}

	@isTest
	public static void testPreparePostalCodeCheck() {
		createTestData();
		TestUtils.autoCommit = false;
		Site__c s1 = TestUtils.createSite(acct);
		s1.Last_dsl_check__c = System.today();

		Test.startTest();
		try {
			insert s1;
		} catch (Exception e) {
			System.assertEquals(
				'Weekly maximum (0) reached. Please request manually.',
				e.getMessage(),
				'Weekly maximum.'
			);
		}
		Test.stopTest();
	}

	@isTest
	public static void testTriggerPostalCodeCheck() {
		createTestData();
		TestUtils.autoCommit = false;
		Site__c s1 = TestUtils.createSite(acct);
		s1.Last_dsl_check__c = System.today();
		insert s1;

		Site__c site = [SELECT Id, Postal_Code_Check_Status__c FROM Site__c WHERE Id = :s1.Id];

		System.assertEquals(site.Postal_Code_Check_Status__c, 'Requested', 'Status is Requested');
	}

	@isTest
	public static void testTriggerSiteExport() {
		createTestData();
		TestUtils.autoCommit = false;
		Site__c s1 = TestUtils.createSite(acct);
		s1.Last_dsl_check__c = System.today();
		insert s1;
		Test.startTest();
		s1.Name = 'Test';
		update s1;
		s1.BOP_export_datetime__c = System.today();
		update s1;

		ban.BOPCode__c = 'ABC';
		update ban;
		Test.stopTest();

		Ban__c banResult = [SELECT Id, BOPCode__c FROM Ban__c WHERE Id = :ban.Id];
		System.assertEquals(banResult.BOPCode__c, 'ABC', 'Ban__c is updated');
	}
}