public with sharing class COM_CancelDeliveryOrderController {
    @AuraEnabled
    public static String getDeliveryOrderList(String requestJson){
        RequestWrap request = (RequestWrap) JSON.deserialize(requestJson, RequestWrap.class);
        List<COM_Delivery_Order__c> deliveryOrders = [
            SELECT Id, Name, Status__c, PONR_Reached__c
            FROM COM_Delivery_Order__c
            WHERE Order__c = :request.recordId
        ];

        DeliveryOrderListWrap response = new DeliveryOrderListWrap(deliveryOrders);
        return JSON.serialize(response);
    }

    @AuraEnabled
    public static String cancelDeliveryOrder(String requestJson){
        Boolean result = false;
        RequestListWrap request = (RequestListWrap) JSON.deserialize(requestJson, RequestListWrap.class);
        List<COM_Delivery_Order__c> deliveryOrdersToUpdate = new List<COM_Delivery_Order__c>();
        List<COM_Delivery_Order__c> deliveryOrders = [SELECT Id,
                                                Status__c
                                                FROM COM_Delivery_Order__c
                                                WHERE Id in :request.deliveryOrderList];

        for (COM_Delivery_Order__c deliveryOrderRecord :deliveryOrders) {
            deliveryOrdersToUpdate.add(new COM_Delivery_Order__c(
                    Id = deliveryOrderRecord.Id,
                    Status__c = 'Cancellation Requested'
                ));
        }

        if (!deliveryOrdersToUpdate.isEmpty()) {
            update deliveryOrdersToUpdate;
            result = true;
        }

        ResponseWrap response = new ResponseWrap();
        response.deliveryOrderCancelled = result;
        return JSON.serialize(response);
    }

    public class DeliveryOrderListWrap {
        public List<COM_Delivery_Order__c> deliveryOrderList;
        public DeliveryOrderListWrap(List<COM_Delivery_Order__c> subList) {
            deliveryOrderList = subList;
        }
    }

    public class ResponseWrap {
        public Boolean deliveryOrderCancelled { get; set; }
    }

    public class RequestWrap {
        public String recordId { get; set; }
    }

    public class RequestListWrap {
        public List<COM_Delivery_Order__c> deliveryOrderList { get; set; }
    }
}