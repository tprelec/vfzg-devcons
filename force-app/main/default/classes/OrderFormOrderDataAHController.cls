public with sharing class OrderFormOrderDataAHController {
	private Boolean isInitialized = false;
	public Id basketId { get; set; }

	public OrderFormOrderDataController parentController {
		get;
		set {
			if (!isInitialized) {
				isInitialized = true;
				fetchProductBasketId(value?.theOrderWrapper?.theOrder?.VF_Contract__r?.Opportunity__c);
			}
		}
	}

	@testVisible
	private void fetchProductBasketId(Id opportunityId) {
		if (opportunityId == null) {
			LoggerService.log(Logginglevel.WARN, 'Did not get opportunity id for order:' + parentController?.theOrderWrapper?.theOrder?.Id);
			return;
		}

		List<cscfga__Product_Basket__c> productBaskets = [
			SELECT Id
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c = :opportunityId AND csbb__Synchronised_With_Opportunity__c = TRUE
			ORDER BY CreatedDate DESC
		];
		if (productBaskets.size() > 0) {
			basketId = productBaskets[0].Id;
		} else {
			LoggerService.log(Logginglevel.WARN, 'Could not fetch basket id for order:' + parentController?.theOrderWrapper?.theOrder?.Id);
		}
	}
}
