public with sharing class CS_ActivityTimelineItemTaskController {

    @AuraEnabled
    public static void updateRecord(Id recordId, Boolean checkboxValue) {
        List<Task> taskList = new List<Task>();
        String status = '';

        if (checkboxValue) {
            status = 'Completed';
        } else {
            status = 'In progress';
        }

        taskList.add(new Task(
            Id = recordId,
            Status = status)
        );

        if (taskList.size() > 0) {
            update taskList;
        }
    }
}