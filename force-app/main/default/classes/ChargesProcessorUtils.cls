global with sharing class ChargesProcessorUtils {
	global static String DISCOUNT_VERSION = '2-0-0';
	global static String OVERRIDE_CHARGE_TYPE = 'override';
	global static String ABSOLUTE_DISCOUNT_TYPE = 'absolute';
	global static String PERCENTAGE_DISCOUNT_TYPE = 'percentage';
	global static String ONE_OFF_CHARGE_TYPE = 'oneOff';
	global static String RECURRING_CHARGE_TYPE = 'recurring';
	global static String SALES_DISCOUNT_PRICE = 'sales';
	global static String LIST_DISCOUNT_PRICE = 'list';
	global static String PRODUCT_LEVEL_CHARGE = '__PRODUCT__';
	global static String QUANTITY_STRATEGY_MULTIPLIER = 'Multiplier';
	global static String QUANTITY_STRATEGY_DECOMPOSITION = 'Decomposition';
	global static String QUANTITY_STRATEGY_QBS = 'Quantity Based Selling';

	private static Decimal getAmountBasedOnQuantityStrategy(Decimal amount, String CS_QuantityStrategy, Integer quantity) {
		Decimal totalAmount;
		if (CS_QuantityStrategy == QUANTITY_STRATEGY_MULTIPLIER) {
			totalAmount = amount * quantity;
		} else {
			totalAmount = amount;
		}
		return totalAmount;
	}

	/*
        The only way to create Charges in CSCFGA is
        to create Discounts with type Override and set them against PC and call calc totals
        Calc totals will be called by csb2c package once all observers are done
    */
	public static cscfga.ProductConfiguration.Discount createDiscountToInitCharge(
		CCAQDProcessor.CCCharge aqdCharge,
		String discountPrice,
		String CS_QuantityStrategy,
		Integer quantity
	) {
		cscfga.ProductConfiguration.Discount returnCharge = new cscfga.ProductConfiguration.Discount();
		Decimal price = (discountPrice == SALES_DISCOUNT_PRICE) ? aqdCharge.salesPrice : aqdCharge.listPrice;
		returnCharge.amount = getAmountBasedOnQuantityStrategy(price, CS_QuantityStrategy, quantity);
		returnCharge.chargeType = aqdCharge.chargeType == 'recurring' ? 'recurring' : 'oneOff';
		returnCharge.type = OVERRIDE_CHARGE_TYPE;
		returnCharge.discountCharge = aqdCharge.name + ' Charge';
		returnCharge.discountPrice = discountPrice;
		returnCharge.source = aqdCharge.source;
		returnCharge.version = DISCOUNT_VERSION;
		returnCharge.description = aqdCharge.description;

		return returnCharge;
	}

	public static cscfga.ProductConfiguration.Discount createChargeDiscount(
		CCAQDProcessor.CCDiscount aqdDiscount,
		String CS_QuantityStrategy,
		Integer quantity
	) {
		cscfga.ProductConfiguration.Discount returnDiscount = new cscfga.ProductConfiguration.Discount();

		returnDiscount.amount = aqdDiscount.type != 'PER'
			? getAmountBasedOnQuantityStrategy(aqdDiscount.amount, CS_QuantityStrategy, quantity)
			: aqdDiscount.amount;
		returnDiscount.type = aqdDiscount.type == 'PER' ? PERCENTAGE_DISCOUNT_TYPE : ABSOLUTE_DISCOUNT_TYPE;
		returnDiscount.chargeType = aqdDiscount.chargeType == 'recurring' ? 'recurring' : 'oneOff';
		returnDiscount.discountCharge = aqdDiscount.discountCharge + ' Charge';
		returnDiscount.discountPrice = SALES_DISCOUNT_PRICE;
		returnDiscount.source = aqdDiscount.source;
		returnDiscount.version = DISCOUNT_VERSION;
		returnDiscount.description = aqdDiscount.source + ' for ' + aqdDiscount.discountCharge;

		return returnDiscount;
	}

	public static cspsi.CommonCartWrapper.Discount getCommonCartDiscount(
		CCAQDProcessor.CCDiscount aqdDiscount,
		String CS_QuantityStrategy,
		Integer quantity
	) {
		cspsi.CommonCartWrapper.Discount returnDiscount = new cspsi.CommonCartWrapper.Discount();

		returnDiscount.amount = aqdDiscount.type != 'PER'
			? getAmountBasedOnQuantityStrategy(aqdDiscount.amount, CS_QuantityStrategy, quantity)
			: aqdDiscount.amount;
		returnDiscount.type = aqdDiscount.type == 'PER' ? PERCENTAGE_DISCOUNT_TYPE : ABSOLUTE_DISCOUNT_TYPE;
		returnDiscount.chargeType = aqdDiscount.chargeType == 'recurring' ? 'recurring' : 'oneOff';
		returnDiscount.discountCharge = aqdDiscount.discountCharge + ' Charge';
		returnDiscount.discountPrice = SALES_DISCOUNT_PRICE;
		returnDiscount.source = aqdDiscount.source;
		returnDiscount.version = DISCOUNT_VERSION;
		returnDiscount.description = aqdDiscount.source + ' for ' + aqdDiscount.discountCharge;

		return returnDiscount;
	}

	public static cspsi.CommonCartWrapper.Discount getCommonCartDiscountFromAQDCharge(
		CCAQDProcessor.CCCharge aqdCharge,
		String discountPrice,
		String CS_QuantityStrategy,
		Integer quantity
	) {
		cspsi.CommonCartWrapper.Discount returnCharge = new cspsi.CommonCartWrapper.Discount();

		Decimal price = (discountPrice == SALES_DISCOUNT_PRICE) ? aqdCharge.salesPrice : aqdCharge.listPrice;

		returnCharge.amount = getAmountBasedOnQuantityStrategy(price, CS_QuantityStrategy, quantity);
		returnCharge.chargeType = aqdCharge.chargeType;
		returnCharge.type = OVERRIDE_CHARGE_TYPE;
		returnCharge.discountCharge = aqdCharge.name + ' Charge';
		returnCharge.discountPrice = discountPrice;
		returnCharge.source = aqdCharge.source;
		returnCharge.version = DISCOUNT_VERSION;
		returnCharge.description = aqdCharge.description;
		return returnCharge;
	}
}
