@isTest
private class TestBdsRestUploadFiles {
	@TestSetup
	static void makeData() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		acct.KVK_number__c = '01234567';
		update acct;

		List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
		BdsRestCreateAccount.ContactClass conClass = new BdsRestCreateAccount.ContactClass();
		conClass.phone = '+31645061135';
		conClass.mobilephone = '+31645061135';
		conClass.isprimary = 'True';
		conClass.lastname = 'Smith';
		conClass.firstname = 'John';
		conClass.email = 'name@example.com';
		contactList.add(conClass);

		List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
		BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
		banClass.financialcontact = 'name@example.com';
		banList.add(banClass);

		List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
		BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
		siteClass.street = 'Mainstreet';
		siteClass.zipcode = '1010AA';
		siteClass.city = 'Amsterdam';
		siteClass.housenumber = 18;
		siteList.add(siteClass);

		BdsRestCreateAccount.createAccountWithKvk('01234567', contactList, banList, siteList);

		/** Add Dealer */
		Contact con = [SELECT Id FROM Contact WHERE Account.KVK_number__c = '01234567'];
		con.Userid__c = UserInfo.getUserId();
		update con;
		Dealer_Information__c di = new Dealer_Information__c();
		di.Contact__c = con.Id;
		di.Dealer_Code__c = '001001';
		insert di;
	}

	@isTest
	static void runAttachKvkPDF() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'kvk');
		req.params.put('docExt', 'pdf');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachKvkDoc() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'kvk');
		req.params.put('docExt', 'doc');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachKvkDocx() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'kvk');
		req.params.put('docExt', 'docx');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachKvkXls() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'kvk');
		req.params.put('docExt', 'xls');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachKvkXlsx() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'kvk');
		req.params.put('docExt', 'xlsx');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachContractPdf() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'pdf');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachContractDoc() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'doc');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachContractDocx() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'docx');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachContractXls() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'xls');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachContractXlsx() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'xlsx');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachCustomFileNamePdf() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'pdf');
		req.params.put('fileName', 'CustomFileName');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachCustomFileNameDoc() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'doc');
		req.params.put('fileName', 'CustomFileName');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachCustomFileNameDocx() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'docx');
		req.params.put('fileName', 'CustomFileName');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachCustomFileNameXls() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'xls');
		req.params.put('fileName', 'CustomFileName');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void runAttachCustomFileNameXlsx() {
		addManualSharing();

		Test.startTest();
		BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity(
			'01234567',
			'001001',
			'comments...',
			'name@example.com',
			'New',
			'IPVPN',
			'1',
			''
		);

		RestRequest req = new RestRequest();
		req.params.put('oppId', retClass.opportunityId);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'xlsx');
		req.params.put('fileName', 'CustomFileName');

		RestContext.request = req;

		BdsRestUploadFiles.internalAttachFile();
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(retClass.opportunityId);
	}

	@isTest
	static void checkInsertAccountShare() {
		Account acct = [SELECT Id FROM Account WHERE KVK_number__c = '01234567' LIMIT 1];

		Test.startTest();
		User adminUser = TestUtils.createAdministrator();
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());

		RestRequest req = new RestRequest();

		req.params.put('oppId', opp.Id);
		req.params.put('docType', 'contract');
		req.params.put('docExt', 'docx');

		RestContext.request = req;

		System.runAs(adminUser) {
			BdsRestUploadFiles.internalAttachFile();
		}
		Test.stopTest();

		BdsResponseClasses.ValidationErrorReturnClass validationErrorReturnClass = (BdsResponseClasses.ValidationErrorReturnClass) JSON.deserialize(
			RestContext.response.responseBody.toString(),
			BdsResponseClasses.ValidationErrorReturnClass.class
		);

		System.assertEquals(true, validationErrorReturnClass.isSuccess, 'Post method should return success');

		System.assert(String.isBlank(validationErrorReturnClass.errorMessage), 'Post method error message should be blank');

		runDocumentCreatedAssert(opp.Id);
	}

	private static void addManualSharing() {
		Account acct = [SELECT Id FROM Account WHERE KVK_number__c = '01234567' LIMIT 1];

		// Manual sharing
		AccountShare acctShr = new AccountShare();
		acctShr.AccountId = acct.Id;
		acctShr.UserOrGroupId = UserInfo.getUserId();
		acctShr.AccountAccessLevel = 'Edit';
		acctShr.OpportunityAccessLevel = 'Edit';
		acctShr.CaseAccessLevel = 'Edit';
		acctShr.RowCause = Schema.AccountShare.RowCause.Manual;
		insert acctShr;
	}

	private static void runDocumentCreatedAssert(Id opportunityId) {
		List<Opportunity_Attachment__c> opportunityAttachments = [SELECT Id FROM Opportunity_Attachment__c WHERE Opportunity__c = :opportunityId];

		System.assertEquals(1, opportunityAttachments.size(), 'Opportunity attachment should be created');

		List<ContentDocumentLink> contentDocumentLinks = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :opportunityAttachments[0].Id];

		System.assertEquals(1, contentDocumentLinks.size(), 'Content document link should be created');
	}
}
