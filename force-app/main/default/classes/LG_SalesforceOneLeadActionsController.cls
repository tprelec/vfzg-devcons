/*******************************************************************************************************************************************
* File Name     :  LG_SalesforceOneLeadActionsController
* Description   :  This is the controller that drives the SF1 actions on lead object

* @author       :   Shreyas
* Modification Log
===================================================================================================
* Ver.    Date          Author              Modification
---------------------------------------------------------------------------------------------------
* 1.0     9th-Aug-16    Shreyas             Created the class for release R1.5

**Last modified by Manu -- CRQ000000530333,CRQ000000584139

********************************************************************************************************************************************/

public with sharing class LG_SalesforceOneLeadActionsController {
	public static Lead leadRecord { get; set; }
	public boolean showError { get; set; }
	public static string leadId { get; set; }
	public static string callResult;
	public Lead leadRecord_NonStatic { get; set; }
	public date lead_PreferredContactDate { get; set; }
	public string lead_PreferredContactTime { get; set; }
	public string lead_FollowUpDescription { get; set; }

	// Lead Statuses
	public static final string leadStatus_Qualified = 'Qualified';
	public static final string leadStatus_NotAvailable = 'Not Available';
	public static final string leadStatus_FutureInterest = 'Future Interest';
	public static final string leadStatus_NotReached = 'Not Reached';
	public static final String leadStatus_2ndNotReached = '2nd Not Reached';
	public static final String leadStatus_3rdNotReached = '3rd Not Reached';
	public static final string leadStatus_Disqualified = 'Reject';
	public static final string leadStatus_FollowUp = 'Follow Up';
	public static final string leadStatus_AppointmentPlanned = 'Appointment Planned';

	public static string status_Temp = '';
	public static string reason_Temp = '';

	public LG_SalesforceOneLeadActionsController(ApexPages.standardcontroller stdcontroller) {
		showError = false;
		leadId = '';
		leadId = ApexPages.currentPage().getParameters().get('Id');
		leadRecord = new Lead();
		leadRecord = [
			SELECT
				Id,
				Status,
				OwnerId,
				LG_ReasonDisqualified__c,
				LG_PreferredContactDate__c,
				LG_PreferredContactTime__c,
				LG_FollowUpDescription__c,
				Phone_Opt_In__c,
				Phone,
				Appointment__c,
				Appointment_Time__c,
				Email,
				LG_CompetitorName__c,
				LG_CompetitorContractEndDate__c,
				LG_MobileCompetitorName__c,
				LG_MobileCompetitorContractEndDate__c,
				Reject_Reason__c,
				Reject_Sub_Reason__c
			FROM Lead
			WHERE Id = :leadId
		];

		leadRecord_NonStatic = new Lead();
		leadRecord_NonStatic = [
			SELECT
				Id,
				Status,
				OwnerId,
				LG_ReasonDisqualified__c,
				LG_PreferredContactDate__c,
				LG_PreferredContactTime__c,
				LG_FollowUpDescription__c,
				Phone_Opt_In__c,
				Phone,
				Appointment__c,
				Appointment_Time__c,
				Email,
				LG_CompetitorName__c,
				LG_CompetitorContractEndDate__c,
				LG_MobileCompetitorName__c,
				LG_MobileCompetitorContractEndDate__c,
				Reject_Reason__c,
				Reject_Sub_Reason__c
			FROM Lead
			WHERE Id = :leadId
		];

		callResult = '';

		lead_PreferredContactDate = leadRecord.LG_PreferredContactDate__c;
		lead_PreferredContactTime = leadRecord.LG_PreferredContactTime__c;
		lead_FollowUpDescription = leadRecord.LG_FollowUpDescription__c;
	}

	/*
		Name: acceptLead
		Purpose: updates lead owner to the logged in user
		Argument: leadRecordId
		Return type: NA
	*/
	@RemoteAction
	public static string acceptLead(string leadRecordId) {
		string result = '';
		try {
			Lead leadRecordToUpdate = new Lead();
			leadRecordToUpdate = [SELECT Id, OwnerId FROM Lead WHERE Id = :leadRecordId];
			leadRecordToUpdate.OwnerId = userInfo.getUserId();
			update leadRecordToUpdate;
			result = 'Success';
			return result;
		} catch (exception e) {
			result = e.getMessage();
			return result;
		}
	}

	/*
		Name: updateLeadStatusToQualified
		Purpose: updates lead status to "Qualified"
		Argument: leadRecordId
		Return type: NA
	*/
	@RemoteAction
	public static string updateLeadStatusToQualified(string leadRecordId) {
		string result = '';
		try {
			Lead leadRecordToUpdate = new Lead();
			leadRecordToUpdate = [SELECT Id, OwnerId, Status, LG_ReasonDisqualified__c FROM Lead WHERE Id = :leadRecordId];

			//leadRecordToUpdate.Status = leadStatus_Qualified;
			leadRecordToUpdate.LG_ReasonDisqualified__c = null;
			leadRecordToUpdate.OwnerId = userInfo.getUserId();
			update leadRecordToUpdate;

			callResult = 'Qualified';
			logACallAfterLeadAction(callResult, leadRecordId);
			result = 'Success';
			return result;
		} catch (exception e) {
			result = e.getMessage();
			return result;
		}
	}

	/*
		Name: updateLeadStatusToDisqualified_OnLoadAction
		Purpose: updates lead status to "Disqualified"
		Argument: NA
		Return type: pageReference
	*/
	public static pageReference updateLeadStatusToDisqualified_OnLoadAction() {
		leadRecord.Status = leadStatus_Disqualified;
		return null;
	}

	/*
		Name: updateLeadStatusToDisqualified
		Purpose: updates lead status to "Disqualified"
		Argument: leadRecordId
		Return type: NA
	*/
	public pageReference updateLeadStatusToDisqualified() {
		return null;
	}

	@RemoteAction
	public static string updateLeadStatusToDisqualified_Remote(string leadRecordId, string status, string reason, string subReason) {
		string result = '';
		try {
			Lead leadRecordToUpdate = new Lead();
			leadRecordToUpdate = [SELECT Id, Status, LG_ReasonDisqualified__c FROM Lead WHERE Id = :leadRecordId];
			leadRecordToUpdate.Status = status;
			leadRecordToUpdate.Reject_Reason__c = reason;
			leadRecordToUpdate.Reject_Sub_Reason__c = subReason;
			if (status == 'Reject' && String.isBlank(reason)) {
				throw new LG_Exception('blank_reason');
			}
			update leadRecordToUpdate;

			callResult = 'Rejected';
			logACallAfterLeadAction(callResult, leadRecordId);

			result = 'Success';
			return result;
		} catch (exception e) {
			result = e.getMessage();
			return result;
		}
	}

	/*
		Name: updateLeadStatusToFollowUp_OnLoadAction
		Purpose: updates lead status to "Follow Up"
		Argument: NA
		Return type: pageReference
	*/
	public static pageReference updateLeadStatusToFollowUp_OnLoadAction() {
		leadRecord.Status = leadStatus_FollowUp;
		return null;
	}

	public static pageReference updateLeadStatusToAppointmentPlanned_OnLoadAction() {
		leadRecord.Status = leadStatus_AppointmentPlanned;
		return null;
	}

	/*
		Name: updateLeadStatusToFollowUp
		Purpose: updates lead status to "Follow-Up"
		Argument: leadRecordId
		Return type: NA
	*/
	public pageReference updateLeadStatusToFollowUp() {
		return null;
	}

	@RemoteAction
	public static string updateLeadStatusToFollowUp_Remote(
		string leadRecordId,
		string status,
		string followUpDate,
		string followUpTime,
		string description,
		string compName,
		string compDate,
		string mobileCompName,
		string mobileCompDate,
		string email,
		string optIn,
		string phone
	) {
		string result = '';
		try {
			Lead leadRecordToUpdate = new Lead();
			leadRecordToUpdate = [
				SELECT
					Id,
					Status,
					LG_ReasonDisqualified__c,
					LG_PreferredContactDate__c,
					LG_PreferredContactTime__c,
					LG_FollowUpDescription__c,
					Phone_Opt_In__c,
					Phone,
					Appointment__c,
					Appointment_Time__c,
					Email,
					LG_CompetitorName__c,
					LG_CompetitorContractEndDate__c,
					LG_MobileCompetitorName__c,
					LG_MobileCompetitorContractEndDate__c,
					Reject_Reason__c,
					Reject_Sub_Reason__c
				FROM Lead
				WHERE Id = :leadRecordId
			];
			leadRecordToUpdate.Status = status;
			if (followUpDate != '') {
				leadRecordToUpdate.Appointment__c = Date.valueOf(followUpDate);
			}
			leadRecordToUpdate.Appointment_Time__c = followUpTime;
			leadRecordToUpdate.LG_FollowUpDescription__c = description;
			leadRecordToUpdate.LG_CompetitorName__c = compName;

			if (compDate != '') {
				leadRecordToUpdate.LG_CompetitorContractEndDate__c = Date.valueOf(compDate);
			}
			leadRecordToUpdate.LG_MobileCompetitorName__c = mobileCompName;
			if (mobileCompDate != '') {
				leadRecordToUpdate.LG_MobileCompetitorContractEndDate__c = Date.valueOf(mobileCompDate);
			}
			leadRecordToUpdate.Email = email;
			leadRecordToUpdate.Phone_Opt_In__c = optIn == 'true' ? true : false;
			leadRecordToUpdate.LG_ReasonDisqualified__c = null;
			//CRQ000000584139
			// https://jiranl.vodafoneziggo.com/browse/SF-774 Lead Owner Id must not be updated, as requested on SF-774 ticket
			// leadRecordToUpdate.OwnerId = userInfo.getUserId();
			//CRQ000000584139
			update leadRecordToUpdate;

			callResult = Status;

			insertCalendarEvent(leadRecordId, leadRecordToUpdate.Appointment__c, followUpTime);

			result = 'Success';
			return result;
		} catch (Exception e) {
			result = e.getMessage();
			return result;
		}
	}

	private static void insertCalendarEvent(String leadRecordId, Date activityDate, String activityDatetime) {
		String[] appointmentTime = activityDatetime.split(':');
		Event appointment = new Event();
		appointment.Type = 'Meeting';
		appointment.Description = 'Appointment Planned';
		appointment.OwnerId = System.UserInfo.getUserId();
		appointment.WhoId = leadRecordId;
		appointment.ActivityDate = activityDate;
		appointment.ActivityDateTime = Datetime.newInstance(
			activityDate.year(),
			activityDate.month(),
			activityDate.day(),
			Integer.valueOf(appointmentTime[0]),
			Integer.valueOf(appointmentTime[1]),
			0
		);
		appointment.DurationInMinutes = 30;
		appointment.Subject = 'D2D Event';
		insert appointment;
	}

	/*
		Name: updateLeadStatusToNotAvailabale
		Purpose: updates lead status to "Not available"
		Argument: leadRecordId
		Return type: NA
	*/
	@RemoteAction
	public static string updateLeadStatusToNotAvailable(string leadRecordId) {
		string result = '';
		try {
			Lead leadRecordToUpdate = new Lead();
			leadRecordToUpdate = [SELECT Id, Status, LG_ReasonDisqualified__c FROM Lead WHERE Id = :leadRecordId];

			leadRecordToUpdate.Status = leadStatus_NotAvailable;
			leadRecordToUpdate.LG_ReasonDisqualified__c = null;
			update leadRecordToUpdate;

			//CRQ000000584139
			callResult = 'Not available';
			//CRQ000000584139
			logACallAfterLeadAction(callResult, leadRecordId);
			result = 'Success';
			return result;
		} catch (Exception e) {
			result = e.getMessage();
			return result;
		}
	}

	/*
		Name: updateLeadStatusToFutureInterest
		Purpose: updates lead status to "Future Interest"
		Argument: leadRecordId
		Return type: NA
	*/
	@RemoteAction
	public static string updateLeadStatusToFutureInterest(string leadRecordId) {
		string result = '';
		try {
			Lead leadRecordToUpdate = new Lead();
			leadRecordToUpdate = [SELECT Id, Status, LG_ReasonDisqualified__c FROM Lead WHERE Id = :leadRecordId];

			leadRecordToUpdate.Status = leadStatus_FutureInterest;
			leadRecordToUpdate.LG_ReasonDisqualified__c = null;
			update leadRecordToUpdate;

			callResult = 'Future Interest';
			logACallAfterLeadAction(callResult, leadRecordId);
			result = 'Success';
			return result;
		} catch (exception e) {
			result = e.getMessage();
			return result;
		}
	}

	/*
		Name: updateLeadStatusToNotReached
		Purpose: updates lead status to "Not Reached"
		Argument: leadRecordId
		Return type: NA
	*/
	@RemoteAction
	public static string updateLeadStatusToNotReached(string leadRecordId) {
		string result = '';
		try {
			Lead leadRecordToUpdate = [SELECT Id, Status, Reject_Sub_Reason__c FROM Lead WHERE Id = :leadRecordId];
			//  updates for https://jiranl.vodafoneziggo.com/browse/SFLO-324 | Date: 2022.04.19
			//  updating lead status to Not Reached and Reject Sub-Reason to 1st/2nd/3rd not reached when "Not Reached" action is clicked
			String newSubreason = '1st Not Reached';
			if (leadRecordToUpdate.Status == 'Not Reached' && leadRecordToUpdate.Reject_Sub_Reason__c == '1st Not Reached') {
				newSubreason = '2nd Not Reached';
			} else if (
				leadRecordToUpdate.Status == 'Not Reached' &&
				(leadRecordToUpdate.Reject_Sub_Reason__c == '2nd Not Reached' ||
				leadRecordToUpdate.Reject_Sub_Reason__c == '3rd Not Reached')
			) {
				newSubreason = '3rd Not Reached';
			}
			leadRecordToUpdate.Status = 'Not Reached';
			leadRecordToUpdate.Reject_Reason__c = 'Not reached';
			leadRecordToUpdate.Reject_Sub_Reason__c = newSubreason;

			leadRecordToUpdate.LG_ReasonDisqualified__c = null;
			update leadRecordToUpdate;

			Task call = logACallAfterLeadAction('Not Reached', leadRecordId);
			call.Reject_Reason__c = 'Not Reached';
			call.Reject_Sub_Reason__c = newSubreason;
			update call;
			result = 'Success';
			return result;
		} catch (exception e) {
			result = e.getMessage();
			return result;
		}
	}

	/*
		Name: logACallAfterLeadAction
		Purpose: logs a call after performing the lead actions
		Argument: None
		Return type: pagereference
	*/
	public static Task logACallAfterLeadAction(string callResultValue, string leadRecordId) {
		Task t = new Task();
		t.Subject = 'D2D Visit';
		t.ActivityDate = System.today();
		t.Type = 'Result of D2D Visit';
		t.LG_Result__c = callResultValue;
		t.Status = 'Completed';
		t.WhoId = leadRecordId;
		t.OwnerId = userinfo.getUserId();
		// prevent double registration between different sales agents.
		t.IsVisibleInSelfService = true;
		insert t;
		return t;
	}
}
