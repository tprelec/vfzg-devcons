/**
 * @description         This class schedules the batch processing of BigMachines User Sync.
 * @author              Guy Clairbois
 */
global class ScheduleBigMachinesUserSyncBatch implements Schedulable {

	/**
	 * @description         This method executes the batch job.
	 */
	global void execute (SchedulableContext SBatch){

		BigMachinesUserSyncBatch bmUserSyncBatch = new BigMachinesUserSyncBatch();
		Id batchprocessId = Database.executeBatch(bmUserSyncBatch);
	}

}