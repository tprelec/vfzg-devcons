/**
 * @description:    Test class for CreditNoteApprovalValidator
 **/
@isTest
public class TestCreditNoteApprovalValidator {
	private static final String TEST_FAKE_USER_ID = TestUtils.getFakeId(User.SObjectType);
	private static final String TEST_FAKE_QUEUE_DEVELOPER_NAME = 'TestCreditNoteApprovalValidatorQueue';

	/**
	 * @description:    Tests adding error message when next
	 *      	        approver manual field is empty on manual
	 *  	            assignment rule
	 **/
	@isTest
	static void shouldAddErrorMessageWhenNextApproverManualFieldIsEmptyOnManualAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_MANUAL,
			Next_Approver_Manual__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(1, creditNote.getErrors().size(), '1 error should be added to the credit note');

		String expectedErrorMessage = String.format(
			CreditNoteApprovalValidator.ERROR_MESSAGE_YOU_NEED_TO_POPULATE_APPROVER_FIELD,
			new List<String>{ CreditNoteApprovalValidator.NEXT_APPROVER_MANUAL_FIELD_NAME }
		);

		System.assertEquals(
			expectedErrorMessage,
			creditNote.getErrors()[0].getMessage(),
			'Proper error message text should be added to the credit note'
		);
	}

	/**
	 * @description:    Tests not adding error message when next
	 *      	        approver manual field is populated on manual
	 *  	            assignment rule
	 **/
	@isTest
	static void shouldNotAddErrorMessageWhenNextApproverManualFieldIsPopulatedOnManualAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_MANUAL,
			Next_Approver_Manual__c = TEST_FAKE_USER_ID
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(0, creditNote.getErrors().size(), '0 error should be added to the credit note');
	}

	/**
	 * @description:    Tests adding error message when next
	 *      	        queue approver field is empty on queue
	 *  	            assignment rule
	 **/
	@isTest
	static void shouldAddErrorMessageWhenNextQueueApproverFieldIsEmptyOnQueueAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE,
			Next_Queue_Approver__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(1, creditNote.getErrors().size(), '1 error should be added to the credit note');

		String expectedErrorMessage = String.format(
			CreditNoteApprovalValidator.ERROR_MESSAGE_YOU_NEED_TO_POPULATE_APPROVER_FIELD,
			new List<String>{ CreditNoteApprovalValidator.NEXT_QUEUE_APPROVER_FIELD_NAME }
		);

		System.assertEquals(
			expectedErrorMessage,
			creditNote.getErrors()[0].getMessage(),
			'Proper error message text should be added to the credit note'
		);
	}

	/**
	 * @description:    Tests not adding error message when next
	 *      	        queue approver field is populated on queue
	 *  	            assignment rule
	 **/
	@isTest
	static void shouldNotAddErrorMessageWhenNextQueueApproverFieldIsPopulatedOnQueueAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE,
			Next_Queue_Approver__c = TEST_FAKE_QUEUE_DEVELOPER_NAME
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(0, creditNote.getErrors().size(), '0 error should be added to the credit note');
	}

	/**
	 * @description:    Tests adding error message when all next
	 *      	        approver manual fields are empty on user
	 *  	            assignment rule
	 **/
	@isTest
	static void shouldAddErrorMessageWhenAllNextApproverFieldsAreEmptyOnUserAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER,
			Next_Approver_1__c = null,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(1, creditNote.getErrors().size(), '1 error should be added to the credit note');

		String expectedErrorMessage = CreditNoteApprovalValidator.ERROR_MESSAGE_YOU_NEED_TO_POPULATE_ANY_NEXT_APPROVER_FIELD;

		System.assertEquals(
			expectedErrorMessage,
			creditNote.getErrors()[0].getMessage(),
			'Proper error message text should be added to the credit note'
		);
	}

	/**
	 * @description:    Tests not adding error message when at least one next
	 *      	        approver field is populated on user
	 *  	            assignment rule
	 **/
	@isTest
	static void shouldNotAddErrorMessageWhenAtLeastOneNextApproverFieldIsPopulatedOnUserAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER,
			Next_Approver_1__c = TEST_FAKE_USER_ID,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(0, creditNote.getErrors().size(), '0 error should be added to the credit note');
	}

	/**
	 * @description:    Tests adding error message when all next
	 *      	        approver manual fields are empty on partner
	 *                  channel manager and solution specialist assignment rule
	 **/
	@isTest
	static void shouldAddErrorMessageWhenAllNextApproverFieldsAreEmptyOnPartnerChannelManagerAndSolutionSpecialistAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_CHANNEL_MANAGER_AND_SOLUTION_SPECIALIST,
			Next_Approver_1__c = null,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(1, creditNote.getErrors().size(), '1 error should be added to the credit note');

		String expectedErrorMessage = CreditNoteApprovalValidator.ERROR_MESSAGE_YOU_NEED_TO_POPULATE_ANY_NEXT_APPROVER_FIELD;

		System.assertEquals(
			expectedErrorMessage,
			creditNote.getErrors()[0].getMessage(),
			'Proper error message text should be added to the credit note'
		);
	}

	/**
	 * @description:    Tests not adding error message when at least one next
	 *      	        approver field is populated on partner
	 *                  channel manager and solution specialist assignment rule
	 **/
	@isTest
	static void shouldNotAddErrorMessageWhenAtLeastOneNextApproverFieldIsPopulatedOnPartnerChannelManagerAndSolutionSpecialistAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_CHANNEL_MANAGER_AND_SOLUTION_SPECIALIST,
			Next_Approver_1__c = TEST_FAKE_USER_ID,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(0, creditNote.getErrors().size(), '0 error should be added to the credit note');
	}

	/**
	 * @description:    Tests adding error message when all next
	 *      	        approver manual fields are empty on partner
	 *                  operational manager assignment rule
	 **/
	@isTest
	static void shouldAddErrorMessageWhenAllNextApproverFieldsAreEmptyOnPartnerOperationalManagerAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_OPERATIONAL_MANAGER,
			Next_Approver_1__c = null,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(1, creditNote.getErrors().size(), '1 error should be added to the credit note');

		String expectedErrorMessage = CreditNoteApprovalValidator.ERROR_MESSAGE_YOU_NEED_TO_POPULATE_ANY_NEXT_APPROVER_FIELD;

		System.assertEquals(
			expectedErrorMessage,
			creditNote.getErrors()[0].getMessage(),
			'Proper error message text should be added to the credit note'
		);
	}

	/**
	 * @description:    Tests not adding error message when at least one next
	 *      	        approver field is populated on partner
	 *                  operational manager assignment rule
	 **/
	@isTest
	static void shouldNotAddErrorMessageWhenAtLeastOneNextApproverFieldIsPopulatedOnPartnerOperationalManagerAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_OPERATIONAL_MANAGER,
			Next_Approver_1__c = TEST_FAKE_USER_ID,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(0, creditNote.getErrors().size(), '0 error should be added to the credit note');
	}

	/**
	 * @description:    Tests adding error message when all next
	 *      	        approver manual fields are empty on owner
	 *                  manager assignment rule
	 **/
	@isTest
	static void shouldAddErrorMessageWhenAllNextApproverFieldsAreEmptyOnOwnerManagerAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_OWNER_MANAGER,
			Next_Approver_1__c = null,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(1, creditNote.getErrors().size(), '1 error should be added to the credit note');

		String expectedErrorMessage = CreditNoteApprovalValidator.ERROR_MESSAGE_YOU_NEED_TO_POPULATE_ANY_NEXT_APPROVER_FIELD;

		System.assertEquals(
			expectedErrorMessage,
			creditNote.getErrors()[0].getMessage(),
			'Proper error message text should be added to the credit note'
		);
	}

	/**
	 * @description:    Tests not adding error message when at least one next
	 *      	        approver field is populated on owner
	 *                  manager assignment rule
	 **/
	@isTest
	static void shouldNotAddErrorMessageWhenAtLeastOneNextApproverFieldIsPopulatedOnOwnerManagerAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_OWNER_MANAGER,
			Next_Approver_1__c = TEST_FAKE_USER_ID,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(0, creditNote.getErrors().size(), '0 error should be added to the credit note');
	}

	/**
	 * @description:    Tests adding error message when all next
	 *      	        approver manual fields are empty on
	 *                  sales manager assignment rule
	 **/
	@isTest
	static void shouldAddErrorMessageWhenAllNextApproverFieldsAreEmptyOnSalesManagerAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_SALES_MANAGER,
			Next_Approver_1__c = null,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(1, creditNote.getErrors().size(), '1 error should be added to the credit note');

		String expectedErrorMessage = CreditNoteApprovalValidator.ERROR_MESSAGE_YOU_NEED_TO_POPULATE_ANY_NEXT_APPROVER_FIELD;

		System.assertEquals(
			expectedErrorMessage,
			creditNote.getErrors()[0].getMessage(),
			'Proper error message text should be added to the credit note'
		);
	}

	/**
	 * @description:    Tests not adding error message when at least one next
	 *      	        approver field is populated on
	 *                  sales manager assignment rule
	 **/
	@isTest
	static void shouldNotAddErrorMessageWhenAtLeastOneNextApproverFieldIsPopulatedOnSalesManagerAssignmentRule() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_SALES_MANAGER,
			Next_Approver_1__c = TEST_FAKE_USER_ID,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(0, creditNote.getErrors().size(), '0 error should be added to the credit note');
	}

	/**
	 * @description:    Tests adding error message when at
	 *                  least one validation condition is not
	 *                  satisfied in multi assignment rule scenario
	 **/
	@isTest
	static void shouldAddErrorMessageWhenAtLeastOneValidationConditionIsNotSatisfiedInMultiAssignmentRuleScenario() {
		// prepare data
		Credit_Note__c creditNote = new Credit_Note__c(
			Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER +
				CreditNoteApprovalFieldMapper.APPROVAL_ASSIGNMENT_RULES_SEPARATOR +
				Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE,
			Next_Approver_1__c = null,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null,
			Next_Queue_Approver__c = TEST_FAKE_QUEUE_DEVELOPER_NAME
		);

		// perform testing
		Test.startTest();
		new CreditNoteApprovalValidator(creditNote).validate();
		Test.stopTest();

		// verify results
		System.assertEquals(1, creditNote.getErrors().size(), '1 error should be added to the credit note');

		String expectedErrorMessage = CreditNoteApprovalValidator.ERROR_MESSAGE_YOU_NEED_TO_POPULATE_ANY_NEXT_APPROVER_FIELD;

		System.assertEquals(
			expectedErrorMessage,
			creditNote.getErrors()[0].getMessage(),
			'Proper error message text should be added to the credit note'
		);
	}
}
