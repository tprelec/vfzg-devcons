@isTest
private class TestCOM_DeliveryEmailTemplatesMerge {
	private static final String TEMPLATE_NAME = 'TestEmailTemplate_';
	private static final String DELIVERY_ORDER_NAME = 'Test_';
	private static final String EMAIL_CONTENT = 'TestContent_';
	private static final String DELIVERY_COMPONENT_NAME = 'TestComponent';
	public static final String STYLING_TEMPLATE_DEVELOPER_NAME = 'COM_Styling_Template';
	private static final String STYLING_CONTENT = 'TestStyling [EMAIL_CONTENT]';

	@isTest
	private static void testProcessEmailTemplate() {
		String index = '1';
		String templateName = TEMPLATE_NAME + index;
		EmailTemplate emailTempl = CS_DataTest.createEmailTemplate(
			templateName,
			templateName,
			'Subject',
			'[BLOCK_GREETING] {DeliveryOrder.Name}',
			'body',
			false
		);
		emailTempl.TemplateType = 'html';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.processEmailTemplate(emailTempl, deliveryOrder);
		Test.stopTest();

		System.assertEquals(
			true,
			result.contains(EMAIL_CONTENT + index + ' ' + DELIVERY_ORDER_NAME + index + '_DeliveryOrder'),
			'Required text not found.'
		);
	}

	@isTest
	private static void testReplaceHtmlFromMetadataNoConditions() {
		String index = '1';
		String templateName = TEMPLATE_NAME + index;
		EmailTemplate emailTempl = CS_DataTest.createEmailTemplate(templateName, templateName, 'Subject', '[BLOCK_GREETING]', 'body', false);
		emailTempl.TemplateType = 'html';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.replaceEmailTemplateHtmlFromMetadata(emailTempl, deliveryOrder);
		Test.stopTest();

		System.assertEquals(EMAIL_CONTENT + index, result, 'Required text not found.');
	}

	@isTest
	private static void testReplaceHtmlFromMetadataChangeTypeAndDeliveryCompMatch() {
		String index = '2';
		String templateName = TEMPLATE_NAME + index;
		EmailTemplate emailTempl = CS_DataTest.createEmailTemplate(templateName, templateName, 'Subject', '[BLOCK_GREETING]', 'body', false);
		emailTempl.TemplateType = 'html';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index, DELIVERY_COMPONENT_NAME);

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.replaceEmailTemplateHtmlFromMetadata(emailTempl, deliveryOrder);
		Test.stopTest();

		System.assertEquals(EMAIL_CONTENT + index, result, 'Required text not found.');
	}

	@isTest
	private static void testReplaceHtmlFromDeliveryOrder() {
		String index = '4';
		String siteStreet = 'Street';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);
		String emailBody = '{DeliveryOrder.SiteStreet} {DeliveryOrder.Name}';

		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.replaceEmailTemplateHtmlFromDeliveryOrder(emailBody, deliveryOrder);
		Test.stopTest();

		System.assertEquals(siteStreet + ' ' + DELIVERY_ORDER_NAME + index + '_DeliveryOrder', result, 'Required text not found.');
	}

	@isTest
	private static void testReplaceHtmlFromDeliveryOrder_additionalVariation() {
		String index = '4b';
		String siteStreet = 'Street';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

		Contact ct = new Contact();
		ct.FirstName = 'TestFN';
		ct.LastName = 'TestLN';
		ct.Email = 'testemail@gmail.com';
		ct.Phone = '0612345678';
		insert ct;

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);
		deliveryOrder.Site__c = null;
		deliveryOrder.Termination_Date__c = System.today().AddDays(1);
		deliveryOrder.Installation_Start__c = System.now().AddDays(1);
		deliveryOrder.Installation_End__c = System.now().AddDays(1);
		deliveryOrder.Products__c = 'TestProduct';
		deliveryOrder.MACD_Type__c = COM_Constants.MACD_TYPE_TERMINATION;
		deliveryOrder.Technical_Contact__c = ct.Id;
		update deliveryOrder;

		String emailBody = '{DeliveryOrder.SiteStreet} {DeliveryOrder.Name}';
		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.replaceEmailTemplateHtmlFromDeliveryOrder(emailBody, deliveryOrder);
		Test.stopTest();

		System.assertEquals(' ' + DELIVERY_ORDER_NAME + index + '_DeliveryOrder', result, 'Required text not found.');
	}

	@isTest
	private static void testProcessEmailSubject() {
		String index = '4';
		String siteStreet = 'Street';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);
		String subject = '{DeliveryOrder.SiteStreet} {DeliveryOrder.Name}';

		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.processEmailSubject(subject, deliveryOrder);
		Test.stopTest();

		System.assertEquals(siteStreet + ' ' + DELIVERY_ORDER_NAME + index + '_DeliveryOrder', result, 'Required text not found.');
	}

	@isTest
	private static void testCheckForChangeTypeTrue() {
		String index = '4';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		Boolean result = COM_DeliveryEmailTemplatesMerge.checkForChangeType('New', deliveryOrder);
		Test.stopTest();

		System.assertEquals(true, result, 'Change not found.');
	}

	@isTest
	private static void testCheckForChangeTypeFalse() {
		String index = '4';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		Boolean result = COM_DeliveryEmailTemplatesMerge.checkForChangeType('Change', deliveryOrder);
		Test.stopTest();

		System.assertEquals(false, result, 'Change not found.');
	}

	@isTest
	private static void testcontainsDeliveryComponentTrue() {
		String index = '5';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index, DELIVERY_COMPONENT_NAME);

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		Boolean result = COM_DeliveryEmailTemplatesMerge.containsDeliveryComponent(DELIVERY_COMPONENT_NAME, deliveryOrder);
		Test.stopTest();

		System.assertEquals(true, result, 'Change not found.');
	}

	@isTest
	private static void testcontainsDeliveryComponentFalse() {
		String index = '5';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index, DELIVERY_COMPONENT_NAME);

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		Boolean result = COM_DeliveryEmailTemplatesMerge.containsDeliveryComponent(DELIVERY_COMPONENT_NAME + '_NOT', deliveryOrder);
		Test.stopTest();

		System.assertEquals(false, result, 'Change not found.');
	}

	@isTest
	private static void testFetchTemplateEmailAttachments() {
		String index = '3';
		String templateName = TEMPLATE_NAME + index;
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		ContentVersion cv = new ContentVersion();
		cv.Title = 'TestAttachment';
		cv.VersionData = Blob.valueof('Test Value');
		cv.PathOnClient = 'TestAttachment.pdf';
		insert cv;

		Test.startTest();
		List<Messaging.EmailFileAttachment> result = COM_DeliveryEmailTemplatesMerge.fetchTemplateEmailAttachments(templateName, deliveryOrder);
		Test.stopTest();

		System.assertEquals(1, result.size(), 'Size is not the same.');
	}

	@isTest
	private static void testFormatServiceData() {
		String index = '3';
		String serviceName = 'TestService';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index);

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Sub' + index;
		subscription.csord__Status__c = 'Subscription Created';
		insert subscription;

		csord__Service__c parentService = CS_DataTest.createService(null, subscription, 'Service Created');
		parentService.csord__Identification__c = 'Serv' + index;
		parentService.Name = serviceName;
		parentService.COM_Delivery_Order__c = deliveryOrder.Id;
		insert parentService;

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.formatServiceData(deliveryOrder);
		Test.stopTest();

		System.assertEquals(true, result.contains(serviceName), 'Service name not found.');
	}

	@isTest
	private static void testFetchDeliveryOrder() {
		String index = '8';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrder(DELIVERY_ORDER_NAME + index, null, true);

		Test.startTest();
		COM_Delivery_Order__c result = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);
		Test.stopTest();

		System.assertEquals(DELIVERY_ORDER_NAME + index, result.Name, 'Name is not the same.');
	}

	@isTest
	private static void testReplaceEmailTemplateHtmlFromDeliveryOrderNewServices() {
		String index = '4';
		String serviceName = 'TestService';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index, DELIVERY_COMPONENT_NAME);

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Sub' + index;
		subscription.csord__Status__c = 'Subscription Created';
		insert subscription;

		csord__Service__c serv1 = CS_DataTest.createService(null, subscription, 'Service Created');
		serv1.csord__Identification__c = 'Serv1' + index;
		serv1.Name = serviceName + '1';
		serv1.COM_Delivery_Order__c = deliveryOrder.Id;
		insert serv1;

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);
		String emailBody = '{DeliveryOrder.NewServices}';

		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.replaceEmailTemplateHtmlFromDeliveryOrder(emailBody, deliveryOrder);
		Test.stopTest();

		System.assertEquals(true, result.contains(serviceName + '1'), 'Service name not found.');
	}

	@isTest
	private static void testReplaceEmailTemplateHtmlFromDeliveryOrderSoftwareServices() {
		String index = '4';
		String serviceName = 'TestService';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index, DELIVERY_COMPONENT_NAME);

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Sub' + index;
		subscription.csord__Status__c = 'Subscription Created';
		insert subscription;

		csord__Service__c serv1 = CS_DataTest.createService(null, subscription, 'Service Created');
		serv1.csord__Identification__c = 'Serv1' + index;
		serv1.Name = serviceName + '1';
		serv1.COM_Delivery_Order__c = deliveryOrder.Id;
		insert serv1;

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);
		String emailBody = '{DeliveryOrder.SoftwareServices}';

		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.replaceEmailTemplateHtmlFromDeliveryOrder(emailBody, deliveryOrder);
		Test.stopTest();

		System.assertEquals(false, result.contains(serviceName + '1'), 'Service name not found.');
	}

	@isTest
	private static void testFetchStylingTemplateId() {
		Test.startTest();
		String result = COM_DeliveryEmailTemplatesMerge.fetchStylingTemplateId();
		Test.stopTest();

		System.assertNotEquals(null, result, 'Styling template not found.');
	}

	@isTest
	private static void testFindL2Service() {
		String index = '10';
		String serviceName = 'TestService';
		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrderChain(DELIVERY_ORDER_NAME + index, DELIVERY_COMPONENT_NAME);

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Sub' + index;
		subscription.csord__Status__c = 'Subscription Created';
		insert subscription;

		csord__Service__c serv1 = CS_DataTest.createService(null, subscription, 'Service Created');
		serv1.csord__Identification__c = 'Serv1' + index;
		serv1.Name = serviceName + '1';
		serv1.COM_Delivery_Order__c = deliveryOrder.Id;
		insert serv1;

		csord__Service__c serv2 = CS_DataTest.createService(null, subscription, 'Service Created');
		serv2.csord__Identification__c = 'Serv1' + index;
		serv2.Name = serviceName + '1';
		serv2.COM_Delivery_Order__c = deliveryOrder.Id;
		serv2.Delivery_Components__c = 'L2';
		insert serv2;

		deliveryOrder = COM_DeliveryEmailTemplatesMerge.fetchDeliveryOrder(deliveryOrder.Id);

		Test.startTest();
		csord__Service__c result = COM_DeliveryEmailTemplatesMerge.findL2Service(deliveryOrder);
		Test.stopTest();

		System.assertNotEquals(null, result, 'L2 Service not found.');
	}

	@isTest
	public static void testContainsDeliveryArticle() {
		COM_SrvDeliveryComponentsTableCntrl.ServiceWrapper wrap1 = new COM_SrvDeliveryComponentsTableCntrl.ServiceWrapper();
		wrap1.deliveryArticleName = 'TestArticle';
		List<COM_SrvDeliveryComponentsTableCntrl.ServiceWrapper> wrappers = new List<COM_SrvDeliveryComponentsTableCntrl.ServiceWrapper>{ wrap1 };

		Test.startTest();
		Boolean result1 = COM_DeliveryEmailTemplatesMerge.containsDeliveryArticle(wrappers, 'TestArticle_NO');
		Boolean result2 = COM_DeliveryEmailTemplatesMerge.containsDeliveryArticle(wrappers, 'TestArticle');
		Test.stopTest();

		System.assertEquals(false, result1, 'Should be false.');
		System.assertEquals(true, result2, 'Should be true.');
	}

	@isTest
	public static void testGetL2Date() {
		csord__Service__c serv = new csord__Service__c();
		DateTime date1 = DateTime.newInstance(2022, 1, 1);
		DateTime date2 = DateTime.newInstance(2022, 1, 2);
		serv.Implemented_Date__c = date1;
		serv.Deprovisioned_Date__c = date2;

		Test.startTest();
		DateTime result1 = COM_DeliveryEmailTemplatesMerge.getL2Date(serv, '');
		DateTime result2 = COM_DeliveryEmailTemplatesMerge.getL2Date(serv, COM_Constants.MACD_TYPE_TERMINATION);
		Test.stopTest();

		System.assertEquals(date1, result1);
		System.assertEquals(date2, result2);
	}
}
