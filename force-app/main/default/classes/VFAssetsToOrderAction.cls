public with sharing class VFAssetsToOrderAction {
	@InvocableMethod(
		label='Get Orders list with all CTNs activated'
		description='Combine VF_Asset__c and NetProfit_CTN__c data to find all activated CTNs grouped by Order'
	)
	public static List<ReturnValues> getOrdersWithAllCTNsActivated() {
		List<ReturnValues> returnValueList = new List<ReturnValues>();

		Map<String, List<VF_Asset__c>> orderToVFAssetMap = getVFAssetsProcessedToday();
		Map<String, List<NetProfit_CTN__c>> orderToNetProfitCTNMap = getNetProfitCTNsByOrder(orderToVFAssetMap.keySet());

		Map<String, List<NetProfit_CTN__c>> netProfitCTNsToUpdateMap = new Map<String, List<NetProfit_CTN__c>>();
		List<NetProfit_CTN__c> netProfitCTNListData = new List<NetProfit_CTN__c>();
		for (String orderId : orderToNetProfitCTNMap.keySet()) {
			if (orderToVFAssetMap.containsKey(orderId)) {
				List<VF_Asset__c> vfAssetListData = orderToVFAssetMap.get(orderId);

				for (NetProfit_CTN__c netProfitCTNData : orderToNetProfitCTNMap.get(orderId)) {
					for (VF_Asset__c vfAsset : vfAssetListData) {
						if (
							netProfitCTNData.CTN_Number__c.equals(vfAsset.CTN__c) &&
							vfAsset.Agreement_Number__c.equals(netProfitCTNData.Framework_Agreement_Id__c) &&
							vfAsset.Base_Start_Date__c <= System.now() &&
							netProfitCTNData.Is_Activated_In_Unify__c == false
						) {
							netProfitCTNData.Is_Activated_In_Unify__c = true;

							if (!netProfitCTNsToUpdateMap.containsKey(orderId)) {
								netProfitCTNsToUpdateMap.put(orderId, new List<NetProfit_CTN__c>());
							}
							netProfitCTNListData.add(netProfitCTNData);
							netProfitCTNsToUpdateMap.get(orderId).add(netProfitCTNData);
						}
					}
				}
			}
		}

		Database.update(netProfitCTNListData);

		ReturnValues returnValues = new ReturnValues();
		returnValues.orderList = calculateOrdersWithCTNsActive(netProfitCTNsToUpdateMap, orderToNetProfitCTNMap);
		returnValueList.add(returnValues);

		return returnValueList;
	}

	private static List<String> calculateOrdersWithCTNsActive(
		Map<String, List<NetProfit_CTN__c>> netProfitCTNsToUpdateMap,
		Map<String, List<NetProfit_CTN__c>> orderToNetProfitCTNMap
	) {
		Set<String> ordersWithAllCTNsActivated = new Set<String>();
		for (String orderId : netProfitCTNsToUpdateMap.keySet()) {
			if (netProfitCTNsToUpdateMap.get(orderId).size() == orderToNetProfitCTNMap.get(orderId).size()) {
				ordersWithAllCTNsActivated.add(orderId);
			}
		}

		List<String> ordersList = new List<String>();
		ordersList.addAll(ordersWithAllCTNsActivated);
		return ordersList;
	}

	private static Map<String, List<VF_Asset__c>> getVFAssetsProcessedToday() {
		List<VF_Asset__c> vfAssetList = [
			SELECT Id, Name, Order__c, CTN__c, Contract_Number__c, Base_Start_Date__c, Agreement_Number__c
			FROM VF_Asset__c
			WHERE (LastModifiedDate = TODAY OR CreatedDate = TODAY) AND Order__c != NULL AND Agreement_Number__c != NULL AND CTN_Status__c = 'Active'
		];
		return GeneralUtils.groupByStringField(vfAssetList, 'Order__c');
	}

	private static Map<String, List<NetProfit_CTN__c>> getNetProfitCTNsByOrder(Set<String> orderIdList) {
		List<NetProfit_CTN__c> netProfitCTNList = [
			SELECT id, CTN_Number__c, Framework_Agreement_Id__c, Order__c, Is_Activated_In_Unify__c
			FROM NetProfit_CTN__c
			WHERE Order__c IN :orderIdList AND Framework_Agreement_Id__c != NULL AND Action__c = 'Porting'
		];

		return GeneralUtils.groupByStringField(netProfitCTNList, 'Order__c');
	}

	public class ReturnValues {
		@InvocableVariable
		public List<String> orderList;
	}
}
