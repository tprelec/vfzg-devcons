public with sharing class COM_CDO_Constants {
	public static final String COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT = 'createTenant';
	public static final String COM_CDO_SERVICE_CHARACTERISTIC_CREATE_TENANT = 'createTenant';
	public static final String COM_WEB_PARAM = 'CDONaaSServiceCatalog';
	public static final String COM_CREATE_TENANT_SYNC_SUCCESS_MOCK = 'COM_CDO_createTenantSyncSuccess';
	public static final String COM_CREATE_TENANT_SYNC_FAILED_MOCK = 'COM_CDO_createTenantSyncFailed';

	public static final String COM_DEPROVISION_BIMO_SYNC_SUCCESS_MOCK = 'COM_CDO_deleteBIMOServiceSyncSuccess';
	public static final String COM_DEPROVISION_BIMO_SYNC_FAILED_MOCK = 'COM_CDO_deleteBIMOServiceSyncFailed';

	public static final String COM_DATA_PROPAGATION_MOCK = 'COM_CDO_BIMO_Data_Propagation';
	public static final String COM_SERVICE_STATE_CHANGE_EVENT_TYPE = 'ServiceStateChangeNotification';
	public static final String COM_CDO_SERVICE_CHARACTERISTIC_DATA_PROPAGATION = 'dataPropagation';

	public static final String COM_CDO_TYPE = '@type';
	public static final String COM_CDO_ID = 'id';
	public static final String COM_CDO_NAME = 'name';
	public static final String COM_CDO_SERVICE_SPECIFICATION = 'serviceSpecification';
	public static final String COM_CDO_SERVICE_CHARACTERISTICS = 'serviceCharacteristic';
	public static final String COM_CDO_SERVICE = 'service';
	public static final String COM_CDO_VALUE = 'value';
	public static final String COM_CDO_VALUE_TYPE = 'valueType';
	public static final String COM_CDO_INVARIANTID = 'invariantID';
	public static final String COM_CDO_VERSION = 'version';
	public static final String COM_CDO_SOURCE_SYSTEM = 'sourceSystem';
	public static final String COM_CDO_ACTION = 'action';
	public static final String COM_CDO_ORDER_ITEM = 'orderItem';
	public static final String COM_CDO_CATEGORY = 'category';
	public static final String COM_CDO_DESCRIPTION = 'description';
	public static final String COM_CDO_PRIORITY = 'priority';
	public static final String COM_CDO_EXTERNALID = 'externalId';
	public static final String COM_MOCKSYNCFAILURE_BODY = '{ "code": 40020, "reason": "FAILURE", "message": "FAILED TO RETRIEVE MOCK RESPONSE! Please check static resource" }';

	public static final String COM_CDO_ACTION_DELETE = 'delete';
	public static final String COM_CDO_ACTION_ADD = 'add';
	public static final String COM_CDO_ACTION_MODIFY = 'modify';

	public static final String COM_CDO_NEW_CONS = 'NEW';
	public static final String COM_CDO_STANDARD_CONS = 'standard';

	public static final String CDO_PROVISION_BIMO_SETTING_NAME = 'provisionBIMOService';
	public static final String CDO_DEPROVISION_BIMO_SETTING_NAME = 'deprovisionBIMOService';

	public static final String COM_CDO_MOCK_CREATE_BIMO_SYNC_FAILED = 'COM_CDO_Create_BIMO_Service_Sync_Resp_Failure';
	public static final String COM_CDO_MOCK_CREATE_BIMO_SYNC_SUCCESS = 'COM_CDO_Create_BIMO_Service_Sync_Resp_Success';

	public static final String COM_CDO_MOCK_MODIFY_BIMO_SYNC_FAILED = '	COM_CDO_Modify_BIMO_Service_Sync_Resp_Failure';
	public static final String COM_CDO_MOCK_MODIFY_BIMO_SYNC_SUCCESS = 'COM_CDO_Modify_BIMO_Service_Sync_Resp_Success';

	public static final String COM_CDO_PROVISION_PAYLOAD_ATTACHMENT_NAME = 'COM_CDO_Provision_payload';
	public static final String COM_CDO_DEPROVISION_PAYLOAD_ATTACHMENT_NAME = 'COM_CDO_Deprovision_payload';
	public static final String COM_CDO_CREATE_TENANT_PAYLOAD_ATTACHMENT_NAME = 'COM_CDO_CreateTenant_payload';
}
