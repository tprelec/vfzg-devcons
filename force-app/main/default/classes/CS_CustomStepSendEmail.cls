/**
 * @description Orchestration process step that needs to have the global modifier in
 * order to work properly because it is called from a different namespace.
 */
@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class CS_CustomStepSendEmail implements CSPOFA.ExecutionHandler {
	private static OrgWideEmailAddress orgWideEmailAddress;
	static final String TIPS_AND_TRICKS_ATTACHMENT = 'TipsAndTricks';
	static final String DELIVERY_ORDER_INSTALL_DATE_TEMPLATE = 'COM Install Delivery Order Date Agreed';

	global List<SObject> process(List<SObject> param0) {
		try {
			orgWideEmailAddress = EmailService.getDefaultOrgWideEmailAddress();

			List<CSPOFA__Orchestration_Step__c> stepList = [
				SELECT
					ID,
					Name,
					CSPOFA__Orchestration_Process__c,
					CSPOFA__Status__c,
					CSPOFA__Completed_Date__c,
					CSPOFA__Message__c,
					CSPOFA__Expression_Result__c,
					CS_Email_From__c,
					CS_Email_To__c,
					CS_Email_Template__c
				FROM CSPOFA__Orchestration_Step__c
				WHERE Id IN :param0
			];

			return processSteps(stepList);
		} catch (Exception ex) {
			return (List<CSPOFA__Orchestration_Step__c>) param0;
		}
	}

	//Wrapper class that creates and sends an email. Candidate for future refactor.
	public static List<SObject> processSteps(List<CSPOFA__Orchestration_Step__c> steps) {
		List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
		List<CSPOFA__Orchestration_Step__c> stepsWithEmails = new List<CSPOFA__Orchestration_Step__c>();
		Map<Id, EmailTemplate> emailTemplateMap;
		Map<String, Id> emailTemplateNameMap = new Map<String, Id>();

		List<Id> processIds = new List<Id>();
		Set<Id> vfContractIds = new Set<Id>();
		Set<Id> deliveryOrderIds = new Set<Id>();
		List<String> emailTemplateNames = new List<String>();

		for (CSPOFA__Orchestration_Step__c step : steps) {
			processIds.add(step.CSPOFA__Orchestration_Process__c);
			if (String.isNotEmpty(step.CS_Email_Template__c)) {
				emailTemplateNames.add(step.CS_Email_Template__c);
			}
		}
		if (emailTemplateNames.size() > 0) {
			emailTemplateMap = new Map<Id, EmailTemplate>(
				[
					SELECT ID, Subject, Name, DeveloperName, HtmlValue, Body, TemplateType
					FROM EmailTemplate
					WHERE Name IN :emailTemplateNames AND IsActive = TRUE
				]
			);
		}

		for (EmailTemplate et : emailTemplateMap.values()) {
			emailTemplateNameMap.put(et.Name, et.Id);
		}

		Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c>(
			[SELECT Id, Contract_VF__c, CSPOFA__Process_Type__c, COM_Delivery_Order__c FROM CSPOFA__Orchestration_Process__c WHERE Id IN :processIds]
		);

		for (CSPOFA__Orchestration_Process__c p : processesMap.values()) {
			vfContractIds.add(p.Contract_VF__c);
			deliveryOrderIds.add(p.COM_Delivery_Order__c);
		}

		Map<Id, VF_Contract__c> vfContractMap;
		Map<Id, COM_Delivery_Order__c> deliveryOrderMap;

		if (!vfContractIds.isEmpty()) {
			vfContractMap = new Map<Id, VF_Contract__c>(
				[SELECT Id, Name, Implementation_Manager__c, Implementation_Manager__r.Email FROM VF_Contract__c WHERE Id IN :vfContractIds]
			);
		}

		if (!deliveryOrderIds.isEmpty()) {
			deliveryOrderMap = new Map<Id, COM_Delivery_Order__c>(
				[
					SELECT
						Id,
						Name,
						Technical_Contact__c,
						Technical_Contact__r.Email,
						Technical_Contact__r.Name,
						Technical_Contact__r.Language__c,
						Site__r.Name,
						Site__r.Site_Street__c,
						Site__r.Site_House_Number__c,
						Site__r.Site_House_Number_Suffix__c,
						Site__r.Site_Postal_Code__c,
						Site__r.Site_City__c,
						Installation_Start__c,
						Installation_End__c,
						Products__c,
						Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
						(SELECT id, Name, csordtelcoa__Delta_Status__c FROM Services__r)
					FROM COM_Delivery_Order__c
					WHERE Id IN :deliveryOrderIds
				]
			);
		}

		List<Document> fileAttachments = [SELECT Id, Name, DeveloperName, ContentType, Body FROM Document WHERE Name = :TIPS_AND_TRICKS_ATTACHMENT];

		for (CSPOFA__Orchestration_Step__c step : steps) {
			try {
				Messaging.SingleEmailMessage tmpEmailMessage;

				if (processesMap.get(step.CSPOFA__Orchestration_Process__c).Contract_VF__c != null) {
					Id vfContractId = processesMap.get(step.CSPOFA__Orchestration_Process__c).Contract_VF__c;
					VF_Contract__c vfContract = vfContractMap.get(vfContractId);

					if (emailTemplateMap.get(emailTemplateNameMap.get(step.CS_Email_Template__c)) != null) {
						tmpEmailMessage = createSingleEmailMessage(
							step,
							vfContract,
							emailTemplateMap.get(emailTemplateNameMap.get(step.CS_Email_Template__c))
						);
					}
				} else if (processesMap.get(step.CSPOFA__Orchestration_Process__c).COM_Delivery_Order__c != null) {
					Id deliveryOrderId = processesMap.get(step.CSPOFA__Orchestration_Process__c).COM_Delivery_Order__c;
					COM_Delivery_Order__c deliveryOrder = deliveryOrderMap.get(deliveryOrderId);
					if (emailTemplateMap.get(emailTemplateNameMap.get(step.CS_Email_Template__c)) != null) {
						tmpEmailMessage = createSingleEmailMessageForDeliveryOrder(
							step,
							deliveryOrder,
							getEmailTemplateByLanguage(emailTemplateMap, step.CS_Email_Template__c, deliveryOrder.Technical_Contact__r.Language__c)
						);
					}
				}

				if (tmpEmailMessage != null) {
					emailMessages.add(tmpEmailMessage);
				}

				stepsWithEmails.add(step);
				step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_COMPLETE;
				step.CSPOFA__Completed_Date__c = Date.today();
				step.CSPOFA__Message__c = 'No message retrieved';
			} catch (Exception ex) {
				step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
				step.CSPOFA__Completed_Date__c = null;
				step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
			}
		}

		List<Messaging.SendEmailResult> emailResults;
		if (Test.isRunningTest()) {
			emailResults = new List<Messaging.SendEmailResult>{
				(Messaging.SendEmailResult) JSON.deserialize('{"success":false}', Messaging.SendEmailResult.class)
			};
		} else {
			emailResults = Messaging.sendEmail(emailMessages, false);
		}

		for (Integer i = 0; i < stepsWithEmails.size() && i < emailResults.size(); i++) {
			if (!emailResults[i].isSuccess()) {
				stepsWithEmails[i].CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
				stepsWithEmails[i].CSPOFA__Completed_Date__c = null;
				stepsWithEmails[i].CSPOFA__Message__c = 'Exception:' + JSON.serialize(emailResults[i].getErrors());
			}
		}
		return stepsWithEmails;
	}

	@SuppressWarnings('PMD.ExcessiveParameterList')
	public static Messaging.SingleEmailMessage createSingleEmailMessageForDeliveryOrder(
		CSPOFA__Orchestration_Step__c step,
		COM_Delivery_Order__c deliveryOrder,
		EmailTemplate emailTemplate
	) {
		OrgWideEmailAddress deliveryOrgWideEmailAddress = EmailService.getDeliveryOrgWideEmailAddress();
		Id orgWideEmailId;

		if (deliveryOrgWideEmailAddress != null) {
			orgWideEmailId = deliveryOrgWideEmailAddress.Id;
		} else if (orgWideEmailAddress != null) {
			orgWideEmailId = orgWideEmailAddress.Id;
		}

		List<String> toAddresses;
		if (Test.isRunningTest()) {
			toAddresses = (new List<String>{ 'test@mailinator.com' });
		} else {
			toAddresses = (new List<String>{ calculateDestinationAddress(step, deliveryOrder.Technical_Contact__r.Email) });
		}

		Messaging.SingleEmailMessage emailMessage = EmailService.prepareEmail(
			null,
			deliveryOrder.Id,
			toAddresses,
			new List<String>(),
			new List<String>(),
			emailTemplate.Id,
			COM_DeliveryEmailTemplatesMerge.fetchStylingTemplateId(),
			COM_DeliveryEmailTemplatesMerge.processEmailSubject(emailTemplate.subject, deliveryOrder),
			new List<Id>(),
			orgWideEmailId
		);

		emailTemplate.HtmlValue = emailMessage.getHtmlBody();

		String mailBody = COM_DeliveryEmailTemplatesMerge.processEmailTemplate(emailTemplate, deliveryOrder);

		if (emailTemplate.TemplateType != 'text') {
			emailMessage.setHtmlBody(mailBody);
		} else {
			emailMessage.setPlainTextBody(mailBody);
		}

		List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
		emailAttachments.addAll(COM_DeliveryEmailTemplatesMerge.fetchTemplateEmailAttachments(emailTemplate.DeveloperName, deliveryOrder));

		if (!emailAttachments.isEmpty()) {
			emailMessage.setFileAttachments(emailAttachments);
		}

		return emailMessage;
	}

	public static Messaging.SingleEmailMessage createSingleEmailMessage(
		CSPOFA__Orchestration_Step__c step,
		VF_Contract__c vfContract,
		EmailTemplate emailTemplate
	) {
		Messaging.SingleEmailMessage emailMessage = Messaging.renderStoredEmailTemplate(
			emailTemplate.Id,
			vfContract.Implementation_Manager__c,
			vfContract.Id
		);
		emailMessage.setTargetObjectId(vfContract.Implementation_Manager__c);
		emailMessage.setSaveAsActivity(false);
		emailMessage.setTemplateId(emailTemplate.Id);
		emailMessage.optOutPolicy = 'FILTER';

		if (orgWideEmailAddress != null) {
			emailMessage.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
		}

		if (Test.isRunningTest()) {
			emailMessage.setToAddresses(new List<String>{ 'test@mailinator.com' });
		} else {
			emailMessage.setToAddresses(new List<String>{ calculateDestinationAddress(step, vfContract.Implementation_Manager__r.Email) });
		}
		return emailMessage;
	}

	@TestVisible
	private static String calculateDestinationAddress(CSPOFA__Orchestration_Step__c step, String contactEmailAddr) {
		return String.isEmpty(step.CS_Email_To__c) ? contactEmailAddr : step.CS_Email_To__c;
	}

	@TestVisible
	private static List<Messaging.EmailFileAttachment> getFileAttachments(List<Document> documents, String language) {
		List<Messaging.EmailFileAttachment> returnValue = new List<Messaging.EmailFileAttachment>();

		for (Document att : documents) {
			if (att.DeveloperName.endsWithIgnoreCase('_' + language)) {
				Messaging.EmailFileAttachment emailAttachment = new Messaging.EmailFileAttachment();
				emailAttachment.setContentType(att.ContentType);
				emailAttachment.setFileName(att.DeveloperName + '.' + att.ContentType);
				emailAttachment.setInline(false);
				emailAttachment.Body = att.Body;

				returnValue.add(emailAttachment);
			}
		}
		return returnValue;
	}

	private static EmailTemplate getEmailTemplateByLanguage(Map<Id, EmailTemplate> emailTemplateMap, String emailTemplateName, String language) {
		EmailTemplate returnValue;
		Map<String, Id> emailTemplateDeveloperNameMap = new Map<String, Id>();
		String emailTemplateNameWithUnderscores = emailTemplateName.replace(' ', '_');
		String emailTemplateNameWithLanguage = emailTemplateNameWithUnderscores + '_' + language;
		String defaultTemplateName = emailTemplateNameWithUnderscores + '_Nederlands';

		for (EmailTemplate et : emailTemplateMap.values()) {
			emailTemplateDeveloperNameMap.put(et.DeveloperName, et.Id);
		}

		if (emailTemplateDeveloperNameMap.get(emailTemplateNameWithLanguage) != null) {
			returnValue = emailTemplateMap.get(emailTemplateDeveloperNameMap.get(emailTemplateNameWithLanguage));
		} else {
			returnValue = emailTemplateMap.get(emailTemplateDeveloperNameMap.get(defaultTemplateName));
		}

		return returnValue;
	}
}
