public inherited sharing class DocumentService {
	@testVisible
	private static DocumentService instance;

	public static DocumentService getInstance() {
		if (instance == null) {
			instance = new DocumentService();
		}
		return instance;
	}

	public void attachDocumentsToSObjectForRegularAttachments(Id sobjectId, List<CS_DocItem> documents) {
		Set<Id> attachmentIds = new Set<Id>();
		for (CS_DocItem document : documents) {
			if (document.docId == null || document.isContractSummary == true) {
				continue;
			}
			attachmentIds.add(document.docId);
		}
		if (attachmentIds.isEmpty()) {
			return;
		}
		List<Attachment> attachments = [SELECT Id, Name, Body, ContentType, ParentId, OwnerId FROM Attachment WHERE Id IN :attachmentIds];

		List<ContentVersion> contentVersions = new List<ContentVersion>();
		for (Attachment attachment : attachments) {
			contentVersions.add(
				new ContentVersion(
					PathOnClient = attachment.Name,
					Title = attachment.Name,
					OwnerId = UserInfo.getUserId(),
					firstPublishLocationId = UserInfo.getUserId(),
					ContentLocation = 'S',
					Origin = 'C',
					VersionData = attachment.Body
				)
			);
		}
		insert contentVersions;

		Set<Id> contentDocumentIds = new Set<Id>();
		List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
		List<ContentVersion> contentVersionsWithContentDocuments = [SELECT ContentDocumentId FROM ContentVersion WHERE Id IN :contentVersions];
		for (ContentVersion contentVersionWithContentDocument : contentVersionsWithContentDocuments) {
			contentDocumentLinks.add(
				new ContentDocumentLink(
					ContentDocumentId = contentVersionWithContentDocument.ContentDocumentId,
					LinkedEntityId = sobjectId,
					ShareType = 'V', // V = Viewer permission, because the file originates from elsewhere and is only put here temporarily for DocuSign.
					Visibility = 'AllUsers' // Is set to AllUsers by triggger on ContentDocumentLink anyway
				)
			);
		}
		insert contentDocumentLinks;
	}

	public void attachDocumentsToSObjectForContractConditions(Id sobjectId, List<CS_DocItem> documents) {
		List<String> serviceDescriptionUrls = new List<String>();
		for (CS_DocItem document : documents) {
			if (document.docId == null && !String.isBlank(document.serviceDescriptionUrl)) {
				serviceDescriptionUrls.add(document.serviceDescriptionUrl);
			}
		}
		if (serviceDescriptionUrls.isEmpty()) {
			return;
		}

		Set<Id> idsToCheckSObjectType = new Set<Id>();
		for (String serviceDescriptionUrl : serviceDescriptionUrls) {
			List<Id> retrievedIds = new List<Id>();
			retrievedIds.add(Id.valueOf(serviceDescriptionUrl.substringAfterLast('/').substringBefore('?')));
			if (retrievedIds.size() != 1) {
				throw new DocumentServiceException(
					'The URL configured on the Contract Condition contains no or more than one id\'s. The URL is: ' + serviceDescriptionUrl
				);
			}
			idsToCheckSObjectType.add(retrievedIds[0]);
		}

		Set<Id> contentDocumentIds = new Set<Id>();
		Set<Id> contentVersionIds = new Set<Id>();
		for (Id idToCheckSObjectType : idsToCheckSObjectType) {
			String sObjectType = idToCheckSObjectType.getSObjectType().getDescribe().getName();
			if (sObjectType == 'ContentDocument') {
				contentDocumentIds.add(idToCheckSObjectType);
				continue;
			}
			if (sObjectType == 'ContentVersion') {
				contentVersionIds.add(idToCheckSObjectType);
				continue;
			}
			throw new DocumentServiceException(
				'The Id in the Contract Condition URL is not a ContentDocumentId or a ContentVersion. The Id is: ' + idToCheckSObjectType
			);
		}
		for (ContentVersion contentVersion : [SELECT ContentDocumentId FROM ContentVersion WHERE Id IN :contentVersionIds]) {
			contentDocumentIds.add(contentVersion.ContentDocumentId);
		}

		List<ContentDocumentLink> contentDocumentLinks = new List<ContentDocumentLink>();
		for (Id contentDocumentId : contentDocumentIds) {
			contentDocumentLinks.add(
				new ContentDocumentLink(
					ContentDocumentId = contentDocumentId,
					LinkedEntityId = sobjectId,
					ShareType = 'V', // V = Viewer permission, because the file originates from elsewhere and is only put here temporarily for DocuSign.
					Visibility = 'AllUsers' // Is set to AllUsers by triggger on ContentDocumentLink anyway
				)
			);
		}
		insert contentDocumentLinks;
	}

	public Set<Id> getCaseAttachmentIdsByCaseId(Id caseId) {
		Set<Id> ret = new Set<Id>();
		Id contractAttachmentContractDocumentRecordTypeId = Schema.SObjectType.Case_Attachments__c
			.getRecordTypeInfosByDeveloperName()
			.get('Contract_Document')
			.getRecordTypeId();
		List<Case_Attachments__c> caseAttachments = [
			SELECT (SELECT Id FROM Case_Attachments__r WHERE RecordTypeId = :contractAttachmentContractDocumentRecordTypeId)
			FROM Case
			WHERE Id = :caseId
		]
		.Case_Attachments__r;
		for (Case_Attachments__c caseAttachment : caseAttachments) {
			ret.add(caseAttachment.Id);
		}
		return ret;
	}

	public void attachDocumentsToSObjectFromSObject(Id destinationId, Set<Id> sourceIds) {
		List<ContentDocumentLink> contentDocumentLinksFromSourceSObject = [
			SELECT ContentDocumentId
			FROM ContentDocumentLink
			WHERE LinkedEntityId IN :sourceIds
		];
		List<ContentDocumentLink> destinationContentDocumentLinks = new List<ContentDocumentLink>();
		for (ContentDocumentLink contentDocumentLink : contentDocumentLinksFromSourceSObject) {
			destinationContentDocumentLinks.add(
				new ContentDocumentLink(
					ContentDocumentId = contentDocumentLink.ContentDocumentId,
					LinkedEntityId = destinationId,
					ShareType = 'V', // V = Viewer permission, because the file originates from elsewhere and is only put here temporarily for DocuSign.
					Visibility = 'AllUsers' // Is set to AllUsers by triggger on ContentDocumentLink anyway
				)
			);
		}
		insert destinationContentDocumentLinks;
	}

	public class DocumentServiceException extends Exception {
	}
}
