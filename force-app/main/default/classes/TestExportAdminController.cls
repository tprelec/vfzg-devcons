@isTest
private class TestExportAdminController {
	
	@isTest static void test_general_calls() {
		ExportBatchSchedule__c b = new ExportBatchSchedule__c();
		b.Scheduled_Id__c = '0000abcde';
		insert b;

		ExportAdminController eac = new ExportAdminController();
		// first load them all with the right tab selected
		ExportAdminController.selectedTab='accounts';
		eac.getFailedAccounts();
		ExportAdminController.selectedTab='contacts';
		eac.getFailedContacts();
		ExportAdminController.selectedTab='orders';
		eac.getFailedOrders();
		ExportAdminController.selectedTab='sites';
		eac.getFailedSites();
		ExportAdminController.selectedTab='users';
		eac.getFailedUsers();

		// then with the wrong tab selected
		ExportAdminController.selectedTab='dummies';
		eac.getFailedAccounts();
		eac.getFailedContacts();
		eac.getFailedOrders();
		eac.getFailedSites();
		eac.getFailedUsers();

		eac.getTabSelectOptions();
		eac.startExportSchedule();
	}
	
	
}