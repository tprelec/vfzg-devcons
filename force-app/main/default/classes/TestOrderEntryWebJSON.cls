@IsTest
private class TestOrderEntryWebJSON {
	@IsTest
	private static void testJSON() {
		OrderEntryWebJSON oe = new OrderEntryWebJSON();
		oe.data = null;

		OrderEntryWebJSON.Data oeData = new OrderEntryWebJSON.Data();
		oeData.simOnlyBundles = new List<OrderEntryWebJSON.Bundle>();

		OrderEntryWebJSON.Bundle oeBundle = new OrderEntryWebJSON.Bundle();
		oeBundle.id = null;
		oeBundle.sku = null;
		oeBundle.price = null;
		oeBundle.subscription = null;

		OrderEntryWebJSON.Price oePrice = new OrderEntryWebJSON.Price();
		oePrice.oneTimeCharge = null;
		oePrice.recurringCharge = null;

		OrderEntryWebJSON.PromotionPrice oePPrice = new OrderEntryWebJSON.PromotionPrice();
		oePPrice.oneTimeCharge = null;
		oePPrice.recurringCharge = null;

		OrderEntryWebJSON.Subscription oeSubscription = new OrderEntryWebJSON.Subscription();
		oeSubscription.id = null;
		oeSubscription.sku = null;
		oeSubscription.name = null;
		oeSubscription.promotions = new List<OrderEntryWebJSON.Promotion>();
		oeSubscription.hasUnlimitedInternet = false;
		oeSubscription.internetGB = null;
		oeSubscription.hasUnlimitedVoice = false;
		oeSubscription.voiceMinutes = null;
		oeSubscription.hasUnlimitedSMS = false;
		oeSubscription.numberSMS = null;
		oeSubscription.durationMonths = 12;
		oeSubscription.price = null;
		oeSubscription.simCards = new List<OrderEntryWebJSON.SimCard>();
		oeSubscription.defaultSimCard = null;

		OrderEntryWebJSON.Charge oeCharge = new OrderEntryWebJSON.Charge();
		oeCharge.normalPrice = null;
		oeCharge.discounts = new List<OrderEntryWebJSON.Discount>();
		oeCharge.discountedPrice = null;

		OrderEntryWebJSON.Promotion oePromotion = new OrderEntryWebJSON.Promotion();
		oePromotion.name = null;
		oePromotion.id = null;
		oePromotion.sku = null;
		oePromotion.promotionType = null;
		oePromotion.price = null;

		OrderEntryWebJSON.SimCard oeSimCard = new OrderEntryWebJSON.SimCard();
		oeSimCard.id = null;
		oeSimCard.sku = null;
		oeSimCard.name = null;
		oeSimCard.type = null;

		OrderEntryWebJSON.PriceDetail oePriceDetail = new OrderEntryWebJSON.PriceDetail();
		oePriceDetail.includingVAT = null;
		oePriceDetail.excludingVAT = null;

		OrderEntryWebJSON.Discount oeDiscount = new OrderEntryWebJSON.Discount();
		oeDiscount.discountPrice = null;
		oeDiscount.discountName = null;
		oeDiscount.discountType = null;
		oeDiscount.duration = null;

		System.assert(oeDiscount.duration == null, 'Duration should be null');
	}
}
