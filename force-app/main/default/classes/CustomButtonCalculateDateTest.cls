@isTest
public with sharing class CustomButtonCalculateDateTest {
	@testSetup
	static void makeData() {
		Account tmpAcc = CS_DataTest.createAccount('Test Account');
		insert tmpAcc;

		Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity', UserInfo.getUserId());
		insert tmpOpp;

		List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>();
		cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);
		basketList.add(tmpProductBasket);

		cscfga__Product_Basket__c tmpProductBasketPartialRetention = CS_DataTest.createProductBasket(tmpOpp, 'Test basket retention', tmpAcc);
		tmpProductBasketPartialRetention.Business_Mobile_Retention_Type__c = 'Partial Retention';
		tmpProductBasketPartialRetention.Block_Expiration__c = true;
		basketList.add(tmpProductBasketPartialRetention);

		cscfga__Product_Basket__c tmpProductBasketPartialRetention1 = CS_DataTest.createProductBasket(tmpOpp, 'Test basket retention2', tmpAcc);
		tmpProductBasketPartialRetention1.Business_Mobile_Retention_Type__c = 'Partial Retention';
		tmpProductBasketPartialRetention1.Block_Expiration__c = true;
		tmpProductBasketPartialRetention1.Number_of_CTN_Retention__c = 1;
		tmpProductBasketPartialRetention1.Number_of_CTN_Retention_Data__c = 1;
		basketList.add(tmpProductBasketPartialRetention1);
		insert basketList;

		List<VF_Asset__c> assetList = new List<VF_Asset__c>();
		VF_Asset__c tmpAsset = new VF_Asset__c();
		tmpAsset.CTN_Status__c = 'Active';
		tmpAsset.PricePlan_Class__c = 'Mobile Voice';
		tmpAsset.Contract_End_Date__c = System.today().addDays(122);
		tmpAsset.Account__c = tmpAcc.Id;
		assetList.add(tmpAsset);

		VF_Asset__c tmpAssetData = new VF_Asset__c();
		tmpAssetData.CTN_Status__c = 'Active';
		tmpAssetData.PricePlan_Class__c = 'Data Only';
		tmpAssetData.Contract_End_Date__c = System.today().addDays(122);
		tmpAssetData.Account__c = tmpAcc.Id;
		assetList.add(tmpAssetData);
		insert assetList;
	}

	@isTest
	private static void testPositive() {
		Test.startTest();
		cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
		CustomButtonCalculateDate calculateDate = new CustomButtonCalculateDate();
		String res = calculateDate.performAction(basket.Id);
		Test.stopTest();
		CustomButtonResponse response = (CustomButtonResponse) JSON.deserialize(res, CustomButtonResponse.class);
		System.assertEquals(System.Label.SUCCESS_Expected_Delivery_Date_Calculation, response.text);
	}

	@isTest
	private static void testNegative() {
		Test.startTest();
		CustomButtonCalculateDate calculateDate = new CustomButtonCalculateDate();
		String res = calculateDate.performAction(null);
		Test.stopTest();
		CustomButtonResponse response = (CustomButtonResponse) JSON.deserialize(res, CustomButtonResponse.class);
		System.assertEquals(System.Label.ERROR_Expected_Delivery_Date_Calculation, response.text);
	}

	@isTest
	private static void testPartialRetentionBasket() {
		Test.startTest();
		cscfga__Product_Basket__c basket = [
			SELECT Id
			FROM cscfga__Product_Basket__c
			WHERE Business_Mobile_Retention_Type__c = 'Partial Retention' AND Number_of_CTN_Retention__c = NULL
			LIMIT 1
		];
		cscfga__Product_Basket__c basket1 = [
			SELECT Id
			FROM cscfga__Product_Basket__c
			WHERE Business_Mobile_Retention_Type__c = 'Partial Retention' AND Number_of_CTN_Retention__c = 1
			LIMIT 1
		];
		CustomButtonCalculateDate calculateDate = new CustomButtonCalculateDate();
		String res = calculateDate.performAction(basket.Id);
		String res1 = calculateDate.performAction(basket1.Id);
		Test.stopTest();
		CustomButtonResponse response = (CustomButtonResponse) JSON.deserialize(res, CustomButtonResponse.class);
		System.assertEquals(System.Label.SUCCESS_Expected_Delivery_Date_Calculation, response.text);
	}
}
