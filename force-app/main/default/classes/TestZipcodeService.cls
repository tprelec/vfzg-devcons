@IsTest
public class TestZipcodeService {
	
	public static final String ZIPCODE = '1000',
        OTHER_ZIPCODE = '1010',
        PROVINCE = 'ZH',
        OTHER_PROVINCE ='NH';
        
    private static final Set<String> ZIPCODE_SET = new Set<String>{ZIPCODE, OTHER_ZIPCODE};
        
    private static final String ZIPCODE_FIELD_NAME = Zipcode_Info__c.postalcode__c.getDescribe().getName();
        
    /**
     * Test the zipcode mapping
     */
    @IsTest
    private static void testZipcodeInformationMap() {
    	Test.startTest();
    		Map<String, Id> zipcodeInformationMap = ZipcodeService.zipcodeInformationMap(ZIPCODE_SET);
    	Test.stopTest();
    	System.assert(ZIPCODE_SET.size() == zipcodeInformationMap.size(), ZIPCODE_SET.size() + ' zipcodes should have been found');
    	System.assert(zipcodeInformationMap.keySet().containsAll(ZIPCODE_SET), 'Both zipcodes should have been retrieved');
    }
    
    /**
     * Test zipcode mapping
     */
    @IsTest
    private static void testZipcodeSObjectMap() {
    	List<Zipcode_Info__c> ziList = [select Id, postalcode__c from ZipCode_Info__c where postalcode__c in :ZIPCODE_SET];
    	Test.startTest();
    		Map<String, List<sObject>> zipcodeSObjectMap = ZipcodeService.getZipcodeSObjectMap(ziList, ZIPCODE_FIELD_NAME);
		Test.stopTest();
		System.assert(ZIPCODE_SET.size() == zipcodeSObjectMap.size(), ZIPCODE_SET.size() + ' zipcodes should have been mapped');
    	System.assert(zipcodeSObjectMap.keySet().containsAll(ZIPCODE_SET), 'Both zipcodes must be available in the mapping');
    	System.assert(zipcodeSObjectMap.values().size() == ziList.size(), ziList.size() + ' zipcode info records must be mapped');
	}
	
	/**
	 * Test the zipcode changes..
	 */
	@IsTest
	private static void testZipcodeChanged() {
		List<Zipcode_Info__c> ziList = [select Id, postalcode__c from ZipCode_Info__c where postalcode__c in :ZIPCODE_SET];
		List<Zipcode_Info__c> ziChangedList = new List<Zipcode_Info__c>();
		for (Zipcode_Info__c zi : ziList) {
			Zipcode_Info__c ziChanged = zi.clone(true, true, true, true);
			ziChanged.postalcode__c = ziChanged.postalcode__c == ZIPCODE ? OTHER_ZIPCODE : ZIPCODE;
			ziChangedList.add(ziChanged);
		}
		Test.startTest();
			List<sObject> changedList = ZipcodeService.getZipcodeChangedList(ziList, new Map<Id, Zipcode_Info__c>(ziChangedList), ZIPCODE_FIELD_NAME, new Set<String>());
		Test.stopTest();
		System.assert(changedList.size() == ziList.size(), ziList.size() + ' records should have been identified as changed');
	}
	
	/**
	 * Aid in setting up test environment for zipcode service
	 */
	@TestSetup
	private static void prepareZipcodeTestEnv() {
		createZipcodeInfo();
	}

	/**
     * Create zipcode data
     */
    private static void createZipcodeInfo() {
    	List<Zipcode_Info__c> ziList = new List<Zipcode_Info__c>();
        Zipcode_Info__c z = new Zipcode_Info__c();
        z.postalcode__c = ZIPCODE;
        z.lead_province_cat__c = PROVINCE;
        
        ziList.add(z);
        
        Zipcode_Info__c z2 = new Zipcode_Info__c();
        z2.postalcode__c = OTHER_ZIPCODE;
        z2.lead_province_cat__c = OTHER_PROVINCE;
        ziList.add(z2);
        
        insert ziList;
	}

}