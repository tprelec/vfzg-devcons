/**
 * Controller for the Billing Data Order Form.
 */
public without sharing class OrderFormBillingDataController {
	private final static String REQUEST_NEW_BAN = 'Request New Unify BAN';
	public Order__c order { get; set; }

	public Ban__c ban { get; set; }
	public Financial_Account__c fa { get; set; }
	public Billing_Arrangement__c bar { get; set; }
	public String contractId { get; private set; }
	public String orderId { get; private set; }
	public Boolean locked { get; set; }

	public String banIdSelected { get; set; }
	public String banNameSelected { get; set; }

	public String barIdSelected { get; set; }
	public Boolean errorFound { get; set; }
	public String faIdSelected { get; set; }

	public transient String banNewSelectedId { get; set; }

	public OrderFormBillingDataController() {
		errorFound = false;
		getParams();
		validateParams();
		getBillingData();
	}

	/**
	 * Gets required parameters from URL
	 */
	private void getParams() {
		contractId = ApexPages.currentPage().getParameters().get('contractId');
		orderId = ApexPages.currentPage().getParameters().get('orderId');
	}

	/**
	 * Validates if all required params are provided
	 */
	private void validateParams() {
		if (orderId == null) {
			ApexPages.addMessage(
				new ApexPages.Message(ApexPages.severity.ERROR, 'No OrderId provided.')
			);
			errorFound = true;
		}
		if (contractId == null) {
			ApexPages.addMessage(
				new ApexPages.Message(ApexPages.severity.ERROR, 'No ContractId provided.')
			);
			errorFound = true;
		}
	}

	/**
	 * Gets Order and related billing data (BAN, Billing Arrangement, Financial Account)
	 */
	private void getBillingData() {
		if (orderId == null) {
			return;
		}

		order = OrderUtils.getOrderDataByContractId(null, new Set<Id>{ Id.valueOf(orderId) })[0];
		banIdSelected = order.VF_Contract__r.Opportunity__r.Ban__c;
		banNameSelected = order.VF_Contract__r.Opportunity__r.Ban__r.Name;
		changeBAN();

		// If Billing Arrangement is already connected to the Order, load the BAR and associated FA
		if (order.Billing_Arrangement__c != null) {
			faIdSelected = Order.Billing_Arrangement__r.Financial_Account__c;
			changeFA();
			barIdSelected = Order.Billing_Arrangement__c;
			changeBAR();
		}

		if (ban == null) {
			ban = new Ban__c();
		}
		if (fa == null) {
			fa = new Financial_Account__c();
			faIdSelected = 'empty';
		}
		if (bar == null) {
			bar = new Billing_Arrangement__c();
			barIdSelected = 'empty';
		}

		locked = OrderUtils.getLocked(order);
	}

	public Account theAccount {
		get {
			if (theAccount == null) {
				theAccount = [
					SELECT
						Id,
						Billing_City__c,
						Billing_Housenumber__c,
						Billing_Housenumber_Suffix__c,
						Billing_Postal_Code__c,
						Billing_Street__c,
						Visiting_street__c,
						Visiting_Housenumber1__c,
						Visiting_Housenumber_Suffix__c,
						Visiting_Postal_Code__c,
						Visiting_City__c,
						Visiting_Country__c,
						Name
					FROM Account
					WHERE Id = :order.Account__c
				];
			}
			return theAccount;
		}
		set;
	}

	/**
	 * Fetches BAN once value is changed on the Page
	 * @return   Current Page Reference
	 */
	public PageReference changeBAN() {
		if (banNewSelectedId != null) {
			if (banNewSelectedId == 'new') {
				banNameSelected = REQUEST_NEW_BAN;
			} else {
				banIdSelected = banNewSelectedId;
			}
		}
		if (
			banNameSelected != null &&
			banNameSelected != REQUEST_NEW_BAN &&
			banNameSelected != 'empty'
		) {
			if (ban?.Id == banIdSelected) {
				return null;
			}
			// Gerhard: Variant code to use OrderValidationField__c as I don't want to add duplicate fields into the object
			// but also don't want to run the full billing_arrangement__c query here
			String query = 'SELECT Id';
			// Get the fields from OrderValidationField__c
			for (OrderValidationField__c ovf : OrderValidationField__c.getAll().values()) {
				if (ovf.Object__c == 'Billing_Arrangement__c') {
					// We want all fields in the config that hang off Ban__r.
					String faField = ovf.Field__c.substringAfter('Ban__r.');
					if (faField != '') {
						query += ',' + faField;
					}
				}
			}
			query += ' FROM Ban__c WHERE Id = :banIdSelected';
			ban = Database.query(query);
		} else if (banNameSelected == REQUEST_NEW_BAN) {
			ban = new Ban__c();
		}

		if (ban != null) {
			// default ban name if empty
			if (ban.BAN_Name__c == null) {
				ban.BAN_Name__c = theAccount.Name;
			}
			// default ban values for partners
			if (order.Direct_Indirect__c == 'Indirect') {
				if (ban.Unify_Customer_Type__c == null) {
					ban.Unify_Customer_Type__c = 'B'; // Business
				}
				if (ban.Unify_Customer_SubType__c == null) {
					ban.Unify_Customer_SubType__c = 'BU'; // Zakelijk
				}
			}
		}
		faIdSelected = 'empty';
		barIdSelected = 'empty';
		return null;
	}

	/**
	 * Fetches FA once value is changed on the Page
	 * @return   Current Page Reference
	 */
	public PageReference changeFA() {
		if (faIdSelected != 'new' && faIdSelected != 'empty') {
			// Gerhard: Variant code to use OrderValidationField__c as I don't want to add duplicate fields into the object
			// but also don't want to run the full billing_arrangement__c query here
			String query = 'SELECT Id';
			// Get the fields from OrderValidationField__c
			for (OrderValidationField__c ovf : OrderValidationField__c.getAll().values()) {
				if (ovf.object__c == 'Billing_Arrangement__c') {
					// We want all fields in the config that hang off financial_account__r
					String faField = ovf.Field__c.substringAfter('Financial_Account__r.');
					if (faField != '') {
						query += ',' + faField;
					}
				}
			}
			query += ' FROM Financial_Account__c WHERE Id = :faIdSelected';
			fa = Database.query(query);
		} else {
			fa = new Financial_Account__c();
		}

		// reset bar selection
		barIdSelected = 'empty';
		bar = new Billing_Arrangement__c();
		return null;
	}

	/**
	 * Fetches BAR once value is changed on the Page
	 * @return   Current Page Reference
	 */
	public PageReference changeBAR() {
		if (barIdSelected != 'new' && barIdSelected != 'empty') {
			// Gerhard: Variant code to use OrderValidationField__c as I don't want to add duplicate fields into the object
			// but also don't want to run the full billing_arrangement__c query here
			String query = 'SELECT Id';
			// Get the fields from OrderValidationField__c
			for (OrderValidationField__c ovf : OrderValidationField__c.getAll().values()) {
				if (ovf.object__c == 'Billing_Arrangement__c') {
					// We want all fields in the config that hang off Billing_Arrangement__r
					String barField = ovf.Field__c.substringAfter('Billing_Arrangement__r.');
					if (barField != '') {
						query += ',' + barField;
					}
				}
			}
			query += ' FROM Billing_Arrangement__c WHERE Id = :barIdSelected';
			bar = Database.query(query);
		} else {
			bar = new Billing_Arrangement__c();
			// set default values
			if (theAccount.Billing_Postal_Code__c != null) {
				bar.Billing_Street__c = theAccount.Billing_Street__c;
				bar.Billing_Housenumber__c = String.valueOf(theAccount.Billing_Housenumber__c);
				bar.Billing_Housenumber_Suffix__c = theAccount.Billing_Housenumber_Suffix__c;
				bar.Billing_Postal_Code__c = theAccount.Billing_Postal_Code__c;
				bar.Billing_City__c = theAccount.Billing_City__c;
				bar.Billing_Country__c = 'NL'; // default to NL as country is not captured by D&B
			} else {
				// if billing address is not filled on account, default to visiting address
				bar.Billing_Street__c = theAccount.Visiting_Street__c;
				bar.Billing_Housenumber__c = String.valueOf(theAccount.Visiting_Housenumber1__c);
				bar.Billing_Housenumber_Suffix__c = theAccount.Visiting_Housenumber_Suffix__c;
				bar.Billing_Postal_Code__c = theAccount.Visiting_Postal_Code__c;
				bar.Billing_City__c = theAccount.Visiting_City__c;
				bar.Billing_Country__c = theAccount.Visiting_Country__c;
			}

			bar.Billing_Arrangement_Alias__c = StringUtils.truncate(theAccount.Name, 50);
			bar.Bill_Format__c = 'EB';
			bar.Bill_Production_Indicator__c = 'Y';

			bar.Bank_Account_Name__c = theAccount.Name;
			bar.Payment_method__c = 'Invoice';
		}

		return null;
	}

	/**
	 * Refreshes Billing Data.
	 * Checks for Order Billing Status and updates Order if Billing Status is changed.
	 * @return   Current Page Reference
	 */
	public PageReference refresh() {
		getBillingData();
		// if the billing details are complete, update the order
		if (OrderValidation.checkCompleteBillingFlag(ban, fa, bar, order)) {
			if (!order.billingReady__c) {
				order.billingReady__c = true;
				SharingUtils.updateRecordsWithoutSharing(order);
			}
		} else {
			if (order.billingReady__c) {
				order.billingReady__c = false;
				SharingUtils.updateRecordsWithoutSharing(order);
			}
		}
		// recheck if this order is now clean
		OrderUtils.updateOrderStatus(order, true);
		return null;
	}

	public PageReference saveBilling() {
		Savepoint sp = database.setSavepoint();
		try {
			// update/insert ban, financial account and BAR
			// if new ban, insert it
			if (banNameSelected == REQUEST_NEW_BAN) {
				ban.Account__c = order.Account__c;
				ban.Name = 'Requested New Unify BAN ' + System.now();
				ban.BAN_Status__c = 'Requested';
			}
			if (banNameSelected != 'empty') {
				upsert ban;
				banNameSelected = ban.Name;
			}

			// if new ban or ban was changed, update it on the opportunity
			if (banNameSelected != order.VF_Contract__r.Opportunity__r.Ban__r.Name) {
				Opportunity opp = new Opportunity(
					Id = order.VF_Contract__r.Opportunity__c,
					Ban__c = ban?.Id
				);
				update opp;
			}

			// if fa is selected but bar is not, generate an error (fa is linked to order via the bar)
			if (faIdSelected != 'empty' && barIdSelected == 'empty') {
				ApexPages.addMessage(
					new ApexPages.Message(
						ApexPages.severity.ERROR,
						'You need to select both an FA and a BAR in order to save the FA data.'
					)
				);
				return null;
			} else {
				//upsert the fa
				if (banNameSelected != null && faIdSelected != 'empty') {
					// only set ban on insert
					if (fa.Id == null) {
						fa.Ban__c = ban.Id;
					}
					upsert fa;
				}

				//upsert the bar
				if (fa != null && fa.Id != null && barIdSelected != 'empty') {
					// only set fa on insert
					if (bar.Id == null) {
						bar.Financial_Account__c = fa.Id;
					}
					upsert bar;

					// update the order
					if (bar.Id != order.Billing_Arrangement__c) {
						order.Billing_Arrangement__c = bar.Id;
						update order;
					}
				}
			}
		} catch (dmlException e) {
			ApexPages.addMessages(e);
			Database.rollback(sp);
			return null;
		}
		// refetch the data and update order status
		refresh();

		return null;
	}

	public List<SelectOption> faSelectOptions {
		get {
			faSelectOptions = new List<SelectOption>();
			faSelectOptions.add(new SelectOption('empty', Label.LABEL_Not_specified));
			if (banNameSelected != null) {
				for (Financial_Account__c fa : [
					SELECT Id, Name
					FROM Financial_Account__c
					WHERE Ban__r.Name = :banNameSelected
				]) {
					faSelectOptions.add(new SelectOption(fa.Id, fa.Name));
				}
				faSelectOptions.add(new SelectOption('new', 'Create new in Unify'));
			}
			return faSelectOptions;
		}
		private set;
	}

	public List<SelectOption> barSelectOptions {
		get {
			barSelectOptions = new List<SelectOption>();
			barSelectOptions.add(new SelectOption('empty', Label.LABEL_Not_specified));

			if (faIdSelected != null) {
				for (Billing_Arrangement__c bar : [
					SELECT Id, Name, Payment_method__c, Bill_Format__c, Unify_Ref_Id__c
					FROM Billing_Arrangement__c
					WHERE Financial_Account__c = :faIdSelected AND Status__c != 'Closed'
				]) {
					barSelectOptions.add(
						new SelectOption(
							bar.Id,
							bar.Name +
							' ' +
							bar.Payment_method__c +
							(bar.Unify_Ref_Id__c == null ? '' : ' ' + bar.Unify_Ref_Id__c)
						)
					);
				}
				barSelectOptions.add(new SelectOption('new', 'Create new in Unify'));
			}
			return barSelectOptions;
		}
		private set;
	}

	public String faContactIdSelected {
		get {
			if (fa != null && fa.Financial_Contact__c != null) {
				faContactIdSelected = fa.Financial_Contact__c;
			}
			return faContactIdSelected;
		}
		set;
	}

	public List<SelectOption> faContactSelectOptions {
		get {
			faContactSelectOptions = new List<SelectOption>();

			for (Contact c : [SELECT Id, Name FROM Contact WHERE AccountId = :order.Account__c]) {
				faContactSelectOptions.add(new SelectOption(c.Id, c.Name));
			}
			return faContactSelectOptions;
		}
		private set;
	}
}