@isTest
private class TestBdsGetOpportunityStatus
{
    @isTest
    static void runValidateValuesMissingOpportunitiyID() {
        Test.startTest();

        BdsResponseClasses.OpportunityStatusClass statusClass= BdsGetOpportunityStatus.validateValues('');
        System.assertEquals(false, statusClass.isSuccess);
        Test.stopTest();
    }

    @isTest
    static void runValidateValuesInvalidOpportunitiyID() {
        Test.startTest();

        BdsResponseClasses.OpportunityStatusClass statusClass= BdsGetOpportunityStatus.validateValues('123');
        System.assertEquals(false, statusClass.isSuccess);
        Test.stopTest();
    }

    @isTest
    static void runValidateValuesPost() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;

        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0623456789';
        conClass.mobilephone = '0623456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);
        Test.startTest();

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        /** Add Dealer */
        Contact con = [Select Id From Contact Where Account.KVK_number__c = '01234567' ];
        con.Userid__c = UserInfo.getUserId();
        update con;
        Dealer_Information__c di = new Dealer_Information__c();
        di.Contact__c = con.Id;
        di.Dealer_Code__c = '001001';
        insert di;

        BdsResponseClasses.OpportunityStatusClass retClass = BdsRestCreateOpportunity.createOpportunity('01234567', '001001', 'comments...', 'name@example.com', 'New', 'IPVPN', '1', '');

        Account acc = [SELECT Id FROM Account WHERE KVK_number__c = '01234567'];
        Ban__c ban = new Ban__c(Name = '312345678', Account__c = acc.Id, Ban_Number__c = '312345678');
        insert ban;
        update new Opportunity(Id = retClass.opportunityId, BAN__c = ban.Id);

        BdsGetOpportunityStatus.doPost(retClass.opportunityId);

        BdsResponseClasses.OpportunityStatusClass responseClass = (BdsResponseClasses.OpportunityStatusClass) JSON.deserialize(
            RestContext.response.responseBody.toString(),
            BdsResponseClasses.OpportunityStatusClass.class
        );
        System.assertEquals(ban.Ban_Number__c, responseClass.billingCustomerId);
        Test.stopTest();
    }
}