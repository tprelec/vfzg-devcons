@isTest
private class TestCOM_InflightChangesController {
//TB commented out all

	// @IsTest
    // static void testInflightChange() {
    //     User simpleUser = CS_DataTest.createSystemAdministratorUser();

    //     System.runAs(simpleUser) {
    //         Map<Id,csord__Service__c> serviceMap = new Map<Id,csord__Service__c>();
    //         List<Delivery_Component__c> replacedDeliveryComponentList = new List<Delivery_Component__c>();
    //         Map<Id, csord__Order__c> newOrdersMap = new Map<Id, csord__Order__c>();

    //         Account testAccount = CS_DataTest.createAccount('Test Account');
    //         insert testAccount;

    //         Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
    //         testOpp.csordtelcoa__Change_Type__c = 'Change';
    //         insert testOpp;

    //         csord__Order_Request__c orderRequest = CS_DataTest.createOrderRequest(true);
    //         List<csord__Order__c> ordersList = CS_DataTest.createOrders(1, 'testOrderName',testAccount,'Order Created', orderRequest,testOpp,true);

    //         for (csord__Order__c orderRecord : ordersList){
    //             newOrdersMap.put(orderRecord.Id, orderRecord);
    //         }

    //         cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
    //         insert basket;

    //         Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

    //         cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
    //         testDef.RecordTypeId = productDefinitionRecordType;
    //         testDef.Product_Type__c = 'Fixed';
    //         insert testDef;

    //         cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
    //         insert testConfConf;

    //         csord__Subscription__c subscription= CS_DataTest.createSubscription(testConfConf.Id);
    //         subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
    //         subscription.csord__Status__c = 'Subscription Created';
    //         insert subscription;

    //         COM_Delivery_Order__c oldSolution = CS_DataTest.createDeliveryOrder('OldSolution',ordersList[0].Id,true);

    //         csord__Service__c zipReplacedParentService= CS_DataTest.createService(testConfConf.Id,subscription,'Service Created');
    //         zipReplacedParentService.csord__Identification__c = 'testSubscription';
    //         zipReplacedParentService.VFZ_Commercial_Component_Name__c = 'Coax/HFC';
    //         zipReplacedParentService.VFZ_Commercial_Article_Name__c = 'Coax aansluiting 1000/50 Mbps';
    //         zipReplacedParentService.COM_Delivery_Order__c = oldSolution.Id;
    //         //zipReplacedParentService.csord__Order__c = ordersList[0].Id;
    //         insert zipReplacedParentService;

    //         Integer numOfRecords = 2;
    //         List<Delivery_Component__c> zipReplacedDeliveryComponents = CS_DataTest.createDeliveryComponents(zipReplacedParentService,'',numOfRecords,false);
    //         for (Integer i = 0; i < numOfRecords; i++) {
    //             if (i == 0) {
    //                 zipReplacedDeliveryComponents[i].Name = 'Coax Internet Access';
    //                 zipReplacedDeliveryComponents[i].Article_Name__c = 'INT_DYNIP_ZZ4';
    //                 zipReplacedDeliveryComponents[i].Article_Code__c = '1000/50';
    //             } else if (i == 1){
    //                 zipReplacedDeliveryComponents[i].Name = 'L1 coax access';
    //                 zipReplacedDeliveryComponents[i].Article_Name__c = 'Coax/HFC';
    //                 zipReplacedDeliveryComponents[i].Article_Code__c = 'Coax/HFC';
    //             }
    //         }
    //         replacedDeliveryComponentList.addAll(zipReplacedDeliveryComponents);

    //         csord__Service__c ipAddressReplacedService= CS_DataTest.createService(testConfConf.Id,subscription,'Service Created');
    //         ipAddressReplacedService.csord__Identification__c = 'testSubscription';
    //         ipAddressReplacedService.VFZ_Commercial_Component_Name__c = 'IP Address';
    //         ipAddressReplacedService.VFZ_Commercial_Article_Name__c = '1 IPv4 Address';
    //         ipAddressReplacedService.csord__Service__c = zipReplacedParentService.Id;
    //         ipAddressReplacedService.COM_Delivery_Order__c = oldSolution.Id;
    //         //ipAddressReplacedService.csord__Order__c = ordersList[0].Id;
    //         insert ipAddressReplacedService;

    //         List<Delivery_Component__c> ipv4ReplacedDeliveryComponents = CS_DataTest.createDeliveryComponents(zipReplacedParentService,'IPv4 Block',1,false);
    //         for (Integer i = 0; i < numOfRecords; i++) {
    //             if (i == 0) {
    //                 ipv4ReplacedDeliveryComponents[i].Article_Name__c = '/30';
    //                 ipv4ReplacedDeliveryComponents[i].Article_Code__c = '/30';
    //             }
    //         }
    //         replacedDeliveryComponentList.addAll(ipv4ReplacedDeliveryComponents);

    //         insert replacedDeliveryComponentList;
    //         COM_Delivery_Order__c newSolution = CS_DataTest.createDeliveryOrder('NewSolution',ordersList[0].Id,true);

    //         csord__Service__c zipParentService= CS_DataTest.createService(testConfConf.Id,subscription,'Service Created');
    //         zipParentService.csord__Identification__c = 'testSubscription';
    //         zipParentService.VFZ_Commercial_Component_Name__c = 'Coax/HFC';
    //         zipParentService.VFZ_Commercial_Article_Name__c = 'Coax aansluiting 1000/50 Mbps';
    //         zipParentService.csordtelcoa__Replaced_Service__c = zipReplacedParentService.Id;
    //         zipParentService.csord__Order__c = ordersList[0].Id;
    //         zipParentService.COM_Delivery_Order__c = newSolution.Id;
    //         zipParentService.csordtelcoa__Cancelled_By_Change_Process__c = true;
    //         insert zipParentService;

    //         csord__Service__c ipAddressService= CS_DataTest.createService(testConfConf.Id,subscription,'Service Created');
    //         ipAddressService.csord__Identification__c = 'testSubscription';
    //         ipAddressService.VFZ_Commercial_Component_Name__c = 'IP Address';
    //         ipAddressService.VFZ_Commercial_Article_Name__c = '5 IPv4 Addresses';
    //         ipAddressService.csord__Service__c = zipParentService.Id;
    //         ipAddressService.csord__Order__c = ordersList[0].Id;
    //         ipAddressService.COM_Delivery_Order__c = newSolution.Id;
    //         insert ipAddressService;
            
    //         List<csord__Service__c> serviceList = new List<csord__Service__c>();
    //         serviceList.add(zipParentService);
    //         serviceList.add(ipAddressService);

    //         COM_InflightChangesController inflightChangesController = new COM_InflightChangesController();
    //         inflightChangesController.executeBusinessLogic(serviceList);
            

    //         List<Delivery_Component__c> deliveryComponents = [SELECT Id,
    //                 Name,
    //                 Article_Name__c,
    //                 Article_Code__c
    //         FROM Delivery_Component__c
    //         WHERE Service__r.csord__Order__c IN :newOrdersMap.keySet()];

    //         System.debug('***test Inflight change: ' + JSON.serializePretty(deliveryComponents));
    //         System.assertEquals(0,deliveryComponents.size());
    //     }
    // }

}