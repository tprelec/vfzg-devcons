@isTest
public class TestOrchGetOrderSummaryExecutionHandler {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, owner);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(claimerContact.id, '123321');
		contr.Dealer_Information__c = dealerInfo.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;
		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);

		NetProfit_CTN__c ctn2 = new NetProfit_CTN__c(
			Order__c = order.Id,
			Action__c = 'New',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			NetProfit_Information__c = npi.Id,
			Simcard_number_VF__c = Constants.NET_PROFIT_CTN_VALID_SIM_CARD_NUMBER,
			CTN_Status__c = 'SIM Activation Order ID received',
			CTN_Number__c = '31612310001',
			SIM_Activation_Order_Id__c = '1234567A',
			SIM_Activation_Timestamp__c = system.now().addMinutes(-90)
		);
		insert ctn2;
	}

	@isTest
	public static void testOrchGOSHandlerPerformCalloutError() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);

		Test.startTest();
		OrchGetOrderSummaryExecutionHandler getOrchGOS = new OrchGetOrderSummaryExecutionHandler();
		Boolean continueToProcess = getOrchGOS.performCallouts(steps);
		System.assertEquals(false, continueToProcess, 'Meaning there is no Callout');
		Test.stopTest();
	}

	@isTest
	public static void testOrchGOSHandler202API() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		TestUtils.createEMPCustomSettings();

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_Tibco202Response');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchGetOrderSummaryExecutionHandler getOrchGOS = new OrchGetOrderSummaryExecutionHandler();
		Boolean continueToProcess = getOrchGOS.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Meaning the Callout was executed');
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchGOS.process(steps);
		System.assertEquals('Complete', lstOrchSteps[0].CSPOFA__Status__c, 'Meaning the Orchestrator custom step is executed without errors');
		Test.stopTest();
	}

	@isTest
	public static void testOrchGOSHandler400API() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		TestUtils.createEMPCustomSettings();

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_GenericAPI400_Error');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchGetOrderSummaryExecutionHandler getOrchGOS = new OrchGetOrderSummaryExecutionHandler();
		Boolean continueToProcess = getOrchGOS.performCallouts(steps);
		System.assertEquals(true, continueToProcess, 'Meaning the Callout was executed');
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchGOS.process(steps);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c, 'Meaning the Orchestrator custom step is executed with errors');
		Test.stopTest();
	}

	@isTest
	public static void testOrchGOSHandlerNoCalloutMade() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);

		Test.startTest();
		OrchGetOrderSummaryExecutionHandler getOrchGOS = new OrchGetOrderSummaryExecutionHandler();
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchGOS.process(steps);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c, 'Meaning the Orchestrator custom step is executed with errors');
		Test.stopTest();
	}
}
