global with sharing class CustomButtonDealSummary extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		cscfga__Product_Basket__c basket = [
			SELECT id, Used_Snapshot_Objects__c, cscfga__Basket_Status__c, cscfga__Opportunity__c
			FROM cscfga__Product_Basket__c
			WHERE id = :basketId
		];

		/*
        if(basket.cscfga__Basket_Status__c != 'Approved' && basket.cscfga__Basket_Status__c != 'Contract created') {
            calculateAndStoreContributionMargin(basketId);
        }
        */

		PageReference editPage = new PageReference('/apex/CS_DealSummary?basketId=' + basketId);
		Id profileId = UserInfo.getProfileId();
		String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;

		if (profileName == 'VF Partner Portal User') {
			editPage = new PageReference('/partnerportal/apex/CS_DealSummary?basketId=' + basketId);
		}

		if (recreateNewSnapshots(basket.cscfga__Basket_Status__c)) {
			if (
				(basket.Used_Snapshot_Objects__c == '') ||
				(basket.Used_Snapshot_Objects__c == null) ||
				(basket.Used_Snapshot_Objects__c != '[CS_Basket_Snapshot_Transactional__c]')
			) {
				basket.Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]';
				update basket;
			}
			CS_BasketSnapshotManager.TakeBasketSnapshot(basket, true);
		}

		return '{"status":"ok","redirectURL":"' + editPage.getUrl() + '"}';
	}

	private Boolean recreateNewSnapshots(String basketStatus) {
		if (
			(basketStatus == 'Closed Won') ||
			(basketStatus == 'Approved') ||
			(basketStatus == 'Submitted for approval') ||
			(basketStatus == 'Pending approval') ||
			(basketStatus == 'Contract created')
		) {
			return false;
		} else {
			return true;
		}
	}
	/*    
    private void calculateAndStoreContributionMargin(String basketId) {
        List<Object> results = cspl.CreateCalculationList.basketList(basketId, null);

        String totalGrossMarginString = '';
        String totalGrossRevenueSumString = '';
        Decimal totalGrossMargin = 0;
        Decimal totalGrossRevenue = 0;
        Decimal contributionMargin = 0;
        
        for (Object obj : results) {
            Map<String, Object> objMap = (Map<String, Object>) obj;
        	String name = (String) objMap.get('Name');
        	if(name == 'Total Gross Margin') {
        		totalGrossMarginString = (String) objMap.get('Total');	
        	}
        	if(name == 'Total Gross Revenue (SUM)') {
        		totalGrossRevenueSumString = (String) objMap.get('Total');	
        	}
        }
        
        if(totalGrossMarginString != '') {
            totalGrossMargin = changePLStringToDecimal(totalGrossMarginString);
        } else {
            System.debug('ERROR: Total Gross Margin String was empty');
            return;
        }
        
        if(totalGrossRevenueSumString != '') {
            totalGrossRevenue = changePLStringToDecimal(totalGrossRevenueSumString);
        } else {
            System.debug('ERROR: Total Gross Revenue(SUM) String was empty');
            return;
        }
        
        if(totalGrossRevenue != 0) {
            contributionMargin = totalGrossMargin / totalGrossRevenue * 100;
        } else {
            System.debug('ERROR: Total Gross Revenue(SUM) is 0 and it should be used in division operation while calculating contribution margin');
            return;
        }
        
        cscfga__Product_Basket__c basket = [SELECT Contribution_Margin__c FROM cscfga__Product_Basket__c WHERE id =: basketId];
        basket.Contribution_Margin__c = contributionMargin.setScale(2);
        update basket;
    }
    
    /**
     * PL managed package always stores decimals with ',' and this is a temporary fix for that behaviour.
     */
	/*
    private Decimal changePLStringToDecimal(String decimalString) {
        decimalString = decimalString.replace('.','');
        if (decimalString.substring(decimalString.length() - 3, decimalString.length() - 2) == ',') {
            decimalString = decimalString.replace(',', '.');
        }
        Decimal decimalNumber = Decimal.valueOf(decimalString);
        return decimalNumber;
    }
    */
}
