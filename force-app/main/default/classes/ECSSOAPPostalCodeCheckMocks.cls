@isTest
@SuppressWarnings('PMD.ExcessiveParameterList')
public with sharing class ECSSOAPPostalCodeCheckMocks {
	public class CombinedCheck implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPPostalCodeCheck.locationsResponseType respElement = new ECSSOAPPostalCodeCheck.locationsResponseType();
			List<ECSSOAPPostalCodeCheck.locationTypeResponse> locs = new List<ECSSOAPPostalCodeCheck.locationTypeResponse>();
			ECSSOAPPostalCodeCheck.locationTypeResponse loc = new ECSSOAPPostalCodeCheck.locationTypeResponse();
			loc.zipcode = '1234AB';
			loc.housenumber = '5';
			loc.housenumberext = 'a';
			ECSSOAPPostalCodeCheck.lineoptionsType lineops = new ECSSOAPPostalCodeCheck.lineoptionsType();
			List<ECSSOAPPostalCodeCheck.optionType> ops = new List<ECSSOAPPostalCodeCheck.optionType>();
			ops.add(getFiberOption());
			ops.add(getDslOption());
			lineops.option = ops;
			loc.lineoptions = lineops;
			locs.add(loc);
			respElement.location = locs;
			response.put('response_x', respElement);
		}
	}
	public class SdslbisCheck implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPPostalCodeCheck.locationsResponseType respElement = new ECSSOAPPostalCodeCheck.locationsResponseType();
			List<ECSSOAPPostalCodeCheck.locationTypeResponse> locs = new List<ECSSOAPPostalCodeCheck.locationTypeResponse>();
			ECSSOAPPostalCodeCheck.locationTypeResponse loc = new ECSSOAPPostalCodeCheck.locationTypeResponse();
			loc.zipcode = '1234AB';
			loc.housenumber = '5';
			loc.housenumberext = 'a';
			ECSSOAPPostalCodeCheck.lineoptionsType lineops = new ECSSOAPPostalCodeCheck.lineoptionsType();
			List<ECSSOAPPostalCodeCheck.optionType> ops = new List<ECSSOAPPostalCodeCheck.optionType>();
			ECSSOAPPostalCodeCheck.remarksListType remarks = new ECSSOAPPostalCodeCheck.remarksListType();
			List<ECSSOAPPostalCodeCheck.remarkType> lstRemark = new List<ECSSOAPPostalCodeCheck.remarkType>();
			lstRemark.add(ECSSOAPPostalCodeCheckMocks.getRemark());
			ops.add(ECSSOAPPostalCodeCheckMocks.getFiberOption());
			ops.add(ECSSOAPPostalCodeCheckMocks.getDslOption());
			remarks.remark = lstRemark;
			lineops.option = ops;
			loc.lineoptions = lineops;
			loc.remarks = remarks;
			locs.add(loc);
			respElement.location = locs;
			response.put('response_x', respElement);
		}
	}
	public class DslCheck implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPPostalCodeCheck.locationsResponseType respElement = new ECSSOAPPostalCodeCheck.locationsResponseType();
			List<ECSSOAPPostalCodeCheck.locationTypeResponse> locs = new List<ECSSOAPPostalCodeCheck.locationTypeResponse>();
			ECSSOAPPostalCodeCheck.locationTypeResponse loc = new ECSSOAPPostalCodeCheck.locationTypeResponse();
			loc.zipcode = '1234AB';
			loc.housenumber = '5';
			loc.housenumberext = 'a';
			ECSSOAPPostalCodeCheck.lineoptionsType lineops = new ECSSOAPPostalCodeCheck.lineoptionsType();
			List<ECSSOAPPostalCodeCheck.optionType> ops = new List<ECSSOAPPostalCodeCheck.optionType>();
			ops.add(ECSSOAPPostalCodeCheckMocks.getDslOption());
			lineops.option = ops;
			loc.lineoptions = lineops;
			locs.add(loc);
			respElement.location = locs;
			response.put('response_x', respElement);
		}
	}
	public class FiberCheck implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPPostalCodeCheck.locationsResponseType respElement = new ECSSOAPPostalCodeCheck.locationsResponseType();
			List<ECSSOAPPostalCodeCheck.locationTypeResponse> locs = new List<ECSSOAPPostalCodeCheck.locationTypeResponse>();
			ECSSOAPPostalCodeCheck.locationTypeResponse loc = new ECSSOAPPostalCodeCheck.locationTypeResponse();
			loc.zipcode = '1234AB';
			loc.housenumber = '5';
			loc.housenumberext = 'a';
			ECSSOAPPostalCodeCheck.lineoptionsType lineops = new ECSSOAPPostalCodeCheck.lineoptionsType();
			List<ECSSOAPPostalCodeCheck.optionType> ops = new List<ECSSOAPPostalCodeCheck.optionType>();
			ops.add(ECSSOAPPostalCodeCheckMocks.getFiberOption());
			lineops.option = ops;
			loc.lineoptions = lineops;
			locs.add(loc);
			respElement.location = locs;
			response.put('response_x', respElement);
		}
	}
	public class CoaxCheck implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPPostalCodeCheck.locationsResponseType respElement = new ECSSOAPPostalCodeCheck.locationsResponseType();
			List<ECSSOAPPostalCodeCheck.locationTypeResponse> locs = new List<ECSSOAPPostalCodeCheck.locationTypeResponse>();
			ECSSOAPPostalCodeCheck.locationTypeResponse loc = new ECSSOAPPostalCodeCheck.locationTypeResponse();
			loc.zipcode = '1234AB';
			loc.housenumber = '5';
			loc.housenumberext = 'a';
			ECSSOAPPostalCodeCheck.lineoptionsType lineops = new ECSSOAPPostalCodeCheck.lineoptionsType();
			List<ECSSOAPPostalCodeCheck.optionType> ops = new List<ECSSOAPPostalCodeCheck.optionType>();
			ops.add(ECSSOAPPostalCodeCheckMocks.getCoaxOption());
			lineops.option = ops;
			loc.lineoptions = lineops;
			locs.add(loc);
			respElement.location = locs;
			response.put('response_x', respElement);
		}
	}
	public class CoaxCheckError implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPPostalCodeCheck.locationsResponseType respElement = new ECSSOAPPostalCodeCheck.locationsResponseType();
			List<ECSSOAPPostalCodeCheck.locationTypeResponse> locs = new List<ECSSOAPPostalCodeCheck.locationTypeResponse>();
			ECSSOAPPostalCodeCheck.locationTypeResponse loc = new ECSSOAPPostalCodeCheck.locationTypeResponse();
			loc.zipcode = '1234AB';
			loc.housenumber = '5';
			loc.housenumberext = 'a';
			List<ECSSOAPPostalCodeCheck.errorType> lstErrorType = new List<ECSSOAPPostalCodeCheck.errorType>();
			ECSSOAPPostalCodeCheck.errorType objErrorType = new ECSSOAPPostalCodeCheck.errorType();
			objErrorType.code = '1';
			objErrorType.message = 'error on coax';
			objErrorType.portfolio = 'test portfolio';
			lstErrorType.add(objErrorType);
			loc.error = lstErrorType;
			locs.add(loc);
			respElement.location = locs;
			response.put('response_x', respElement);
		}
	}

	public static ECSSOAPPostalCodeCheck.optionType getFiberOption() {
		ECSSOAPPostalCodeCheck.optionType op1 = new ECSSOAPPostalCodeCheck.optionType();
		op1.technologytype = 'Fiber';
		ECSSOAPPostalCodeCheck.bandwidthType ul = new ECSSOAPPostalCodeCheck.bandwidthType();
		ul.min = 0;
		ul.max = 2048;
		op1.upload = ul;
		ECSSOAPPostalCodeCheck.bandwidthType dl = new ECSSOAPPostalCodeCheck.bandwidthType();
		dl.min = 0;
		dl.max = 2048;
		op1.download = ul;
		op1.plandate = '';
		op1.accessclass = 'fiber';
		op1.serviceable = 'Groen';
		op1.classification = '';
		op1.portfolio = 'test-fiber-business';
		op1.linetype = 'fiber';

		return op1;
	}

	public static ECSSOAPPostalCodeCheck.optionType getCoaxOption() {
		ECSSOAPPostalCodeCheck.optionType op1 = new ECSSOAPPostalCodeCheck.optionType();
		op1.technologytype = 'Coax';
		ECSSOAPPostalCodeCheck.bandwidthType ul = new ECSSOAPPostalCodeCheck.bandwidthType();
		ul.min = 0;
		ul.max = 2048;
		op1.upload = ul;
		ECSSOAPPostalCodeCheck.bandwidthType dl = new ECSSOAPPostalCodeCheck.bandwidthType();
		dl.min = 1;
		dl.max = 2049;
		op1.download = ul;
		op1.plandate = '';
		op1.accessclass = 'Coax';
		op1.serviceable = 'Groen';
		op1.classification = '';
		op1.portfolio = 'test-Coax-business';
		op1.linetype = 'Coax';

		return op1;
	}

	public static ECSSOAPPostalCodeCheck.optionType getDslOption() {
		ECSSOAPPostalCodeCheck.optionType op1 = new ECSSOAPPostalCodeCheck.optionType();
		op1.technologytype = 'ADSL_ISDN';
		ECSSOAPPostalCodeCheck.bandwidthType ul = new ECSSOAPPostalCodeCheck.bandwidthType();
		ul.min = 0;
		ul.max = 2048;
		op1.upload = ul;
		ECSSOAPPostalCodeCheck.bandwidthType dl = new ECSSOAPPostalCodeCheck.bandwidthType();
		dl.min = 0;
		dl.max = 2048;
		op1.download = ul;
		op1.plandate = '';
		op1.accessclass = 'ETH,ATM';
		op1.serviceable = 'Green';
		op1.classification = '';
		op1.portfolio = 'test-dsl-business';
		op1.linetype = 'dsl';

		return op1;
	}

	public static ECSSOAPPostalCodeCheck.remarkType getRemark() {
		ECSSOAPPostalCodeCheck.remarkType remark1 = new ECSSOAPPostalCodeCheck.remarkType();
		ECSSOAPPostalCodeCheck.remarkSpecificationType remarkType1 = new ECSSOAPPostalCodeCheck.remarkSpecificationType();
		remarkType1.extension = 'a';
		remarkType1.housenumber = '5';
		remarkType1.zipcode = '1234AB';
		remark1.connectionType = 'sdslbis';
		remark1.text = 'sdslbis';
		remark1.remarkSpecification = remarkType1;
		return remark1;
	}
}
