public with sharing class CS_ComOrderGenerationObserverHandler {
    private List<Id> serviceIds = new List<Id>();
    private List<Id> subscriptionIds = new List<Id>();
    private List<csord__Service__c> servicesList = new List<csord__Service__c>();
    private List<Id> replacedServiceIds = new List<Id>();
    private List<csord__Service__c> replacedServicesList = new List<csord__Service__c>();
    private List<csord__Service__c> parentServicesList = new List<csord__Service__c>();
    private Map<Id, COM_Delivery_Order__c> subordersToUpdate = new Map<Id, COM_Delivery_Order__c>();
    private Map<Id, csord__Subscription__c> subscriptionsToUpdate = new Map<Id, csord__Subscription__c>();
    private Map<Id, String> regionCodesByGroupId = new Map<Id, String>();
    private Set<Id> orderIds = new Set<Id>();
    private List<Id> subscriptionIdsForSpecificationGeneration = new List<Id>();
    private Map<Id,Id> deliveryOrderIdByOrderId = new Map<Id,Id>();

    public CS_ComOrderGenerationObserverHandler(List<Id> services, List<Id> subscriptionsForSpecification) {
        this.subscriptionIdsForSpecificationGeneration = subscriptionsForSpecification;
        this.serviceIds = services;
        this.servicesList = [
            SELECT Id,
                COM_Delivery_Order__c,
                Installation_Wishdate__c,
                csord__Service__c,
                csord__Service__r.COM_Delivery_Order__c,
                Site__c,
                Site__r.Name,
                Site__r.Technical_Contact__c,
                csord__Subscription__c,
                csord__Subscription__r.Name,
                csord__Order__c,
                csord__Order__r.csord__Account__c,
                csord__Order__r.csord__Account__r.Name,
                csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
                csordtelcoa__Replaced_Service__c,
                csordtelcoa__Replaced_Service__r.COM_Delivery_Order__c,
                csordtelcoa__Replacement_Service__c,
                csordtelcoa__Replacement_Service__r.csord__Subscription__c,
                VFZ_Product_Abbreviation__c
            FROM csord__Service__c
            WHERE Id IN :services /* AND Site__c != null */
        ];

        for (csord__Service__c s : this.servicesList) {
            if (s.csord__Service__c == null) {
                this.parentServicesList.add(s);
            }
            this.subscriptionIds.add(s.csord__Subscription__c);
            if (s.csordtelcoa__Replaced_Service__c != null) {
                this.subscriptionIds.add(s.csordtelcoa__Replaced_Service__r.COM_Delivery_Order__c);
            }
            if (s.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c == 'Move') {
                this.replacedServiceIds.add(s.csordtelcoa__Replaced_Service__c);
            }

            if (s.csord__Order__c != null) {
                this.orderIds.add(s.csord__Order__c);
            }
        }

        if (!this.replacedServiceIds.isEmpty()) {
            this.replacedServicesList = [
                SELECT Id,
                    COM_Delivery_Order__c,
                    Installation_Wishdate__c,
                    csord__Service__c,
                    csord__Service__r.COM_Delivery_Order__c,
                    Site__c,
                    Site__r.Name,
                    Site__r.Technical_Contact__c,
                    csord__Subscription__c,
                    csord__Subscription__r.Name,
                    csord__Order__c,
                    csord__Order__r.csord__Account__c,
                    csord__Order__r.csord__Account__r.Name,
                    csordtelcoa__Replaced_Service__c,
                    csordtelcoa__Replaced_Service__r.COM_Delivery_Order__c,
                    csordtelcoa__Replacement_Service__c,
                    csordtelcoa__Replacement_Service__r.csord__Subscription__c,
                    csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__r.Name,
                    csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__c,
                    csordtelcoa__Replacement_Service__r.csord__Order__c,
                    csordtelcoa__Replacement_Service__r.Site__c,
                    csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
                    VFZ_Product_Abbreviation__c
                FROM csord__Service__c
                WHERE Id IN :replacedServiceIds /*AND Site__c != null */
            ];
        }
    }

    public void run() {
        //generateServiceSpecifications();
        
        if (!this.parentServicesList.isEmpty()) {
            List<csord__Service__c> newAndReplacedServices = new List<csord__Service__c>();
            newAndReplacedServices.addAll(this.servicesList);
            if (!this.replacedServicesList.isEmpty()) {
                newAndReplacedServices.addAll(this.replacedServicesList);
            }

            createParentDeliveryOrder(orderIds);

            createDeliveryOrders(newAndReplacedServices);

            List<csord__Service__c> allServices = [
                SELECT Id,
                    COM_Delivery_Order__c,
                    Installation_Wishdate__c,
                    csord__Service__c,
                    csord__Service__r.COM_Delivery_Order__c,
                    Site__c, csord__Subscription__c,
                    csord__Subscription__r.Name,
                    csord__Order__c,
                    VFZ_Product_Abbreviation__c,
                    csordtelcoa__Replacement_Service__c,
                    csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__r.Name,
                    csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__c,
                    csordtelcoa__Replacement_Service__r.csord__Order__c,
                    csordtelcoa__Replacement_Service__r.csord__Subscription__c
                FROM csord__Service__c
                WHERE (Id IN :this.serviceIds OR Id IN :this.replacedServiceIds) /* AND Site__c != null */
            ];

            //REFACTOR THESE TWO
            updateSuborderFields(allServices);
            updateSuborderProducts(this.subscriptionIds);

            //updateSubscriptionProvisioningRequired(allServices);

            updateList(this.subordersToUpdate.values());
            updateList(this.subscriptionsToUpdate.values());
        }
    
        if (!this.orderIds.isEmpty()) {
            updateOrders();
        }
    }
/*
    private void generateServiceSpecifications() {
        System.debug('****Generating this.subscriptionIdsForSpecificationGeneration '+this.subscriptionIdsForSpecificationGeneration);

        if (!this.subscriptionIdsForSpecificationGeneration.isEmpty()) {
            if(Test.isRunningTest()) {
                System.debug('****Generating JSONS end test');
                Attachment att = new Attachment();
                att.ParentId = this.subscriptionIdsForSpecificationGeneration[0];
                att.Name = 'ServiceSpecification.json';
                att.Body = Blob.valueOf('test');
                insert att;
            }
            else {
                System.debug('****+Generating JSONS actual START');
                csedm.API_1.generateSpecifications(this.subscriptionIdsForSpecificationGeneration, new List<Id>(), true);
                System.debug('****+Generating JSONS actual END');
            }
        }
    }
    */
    private void updateOrders() {
        Map<Id, Boolean> orderBasketManagedByPm = new Map<Id, Boolean>();
        List<csord__Order__c> ordersToUpdate = new List<csord__Order__c>();
        List<csord__Subscription__c> subs = [
            SELECT Id,
                csord__Order__c,
                csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__r.Managed_by_Project_Manager__c
            FROM csord__Subscription__c
            WHERE csord__Order__c IN :this.orderIds
        ];

        for (csord__Subscription__c subscription : subs) {
            if (orderBasketManagedByPm.get(subscription.csord__Order__c) == null) {
                orderBasketManagedByPm.put(subscription.csord__Order__c, subscription.csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__r.Managed_by_Project_Manager__c);
            }
        }

        for (Id orderId : orderBasketManagedByPm.keySet()) {
            ordersToUpdate.add(new csord__Order__c(
                Id = orderId,
                Managed_by_Project_Manager__c = orderBasketManagedByPm.get(orderId)
            ));
        }

        if (!ordersToUpdate.isEmpty()) {
            update ordersToUpdate;
        }
    }

    private void createDeliveryOrders(List<csord__Service__c> services) {
        Set<String> setSiteIds = new Set<String>();
        List<COM_Delivery_Order__c> deliveryOrders = new List<COM_Delivery_Order__c>();
        Map<Id, csord__Subscription__c> subsToUpdate = new Map<Id, csord__Subscription__c>();
        List<csord__Service__c> servicesToUpdate = new List<csord__Service__c>();
        String strSuborderCreated = 'Created';
        Boolean sitelessSuborderCreated = false;

        for (csord__Service__c s : services) {            
            System.debug('Services: ' + s);
            
            String siteId = '';
            if (s.Site__c != null) {
                siteId = String.valueOf(s.Site__c);
            }

            if (String.isNotEmpty(siteId)) {
                if (!setSiteIds.contains(siteId)) {
                    setSiteIds.add(siteId);
                    String accountName, orderId;
                    if (s.csordtelcoa__Replacement_Service__c == null) {
                        accountName = s.csord__Order__r.csord__Account__r.Name;
                        orderId = s.csord__Order__c;
                    } else {
                        accountName = s.csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__r.Name;
                        orderId = s.csordtelcoa__Replacement_Service__r.csord__Order__c;
                    }
                    
                    //TEMPORARY FIX - feel free to remove
                    String deliveryOrderName = 'Delivery Order: ' + accountName + ' - ' + s.Site__r.Name;
                    deliveryOrders.add(new COM_Delivery_Order__c(
                        //TEMPORARY FIX - feel free to remove
                        Name = deliveryOrderName.length()>80 ? deliveryOrderName.substring(0,80) : deliveryOrderName,//'Delivery Order: ' + accountName + ' - ' + s.Site__r.Name,
                        Order__c = orderId,
                        Parent_Delivery_Order__c = deliveryOrderIdByOrderId.get(orderId),
                        Status__c = strSuborderCreated,
                        Site__c = Id.valueOf(siteId),
                        Account__c = s.csord__Order__r.csord__Account__c,
                        Technical_Contact__c = s.Site__r.Technical_Contact__c
                    ));

                }

                // copy the Site__c code from service to subscription
                csord__Subscription__c subscription = new csord__Subscription__c(
                    Id = s.csord__Subscription__c,
                    Site__c = s.Site__c
                );
                subsToUpdate.put(subscription.Id, subscription);
            } else {
                if(sitelessSuborderCreated == false && s.csord__Service__c == null) {
                    sitelessSuborderCreated = true;
                    String accountName, orderId;
                    setSiteIds.add('');
                    if (s.csordtelcoa__Replacement_Service__c == null) {
                        accountName = s.csord__Order__r.csord__Account__r.Name;
                        orderId = s.csord__Order__c;
                    } else {
                        accountName = s.csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__r.Name;
                        orderId = s.csordtelcoa__Replacement_Service__r.csord__Order__c;
                    }
    
                    deliveryOrders.add(new COM_Delivery_Order__c(
                        Name = 'Delivery Order: ' + accountName,
                        Order__c = orderId,
                        Parent_Delivery_Order__c = deliveryOrderIdByOrderId.get(orderId),
                        Status__c = strSuborderCreated,
                        Account__c = s.csord__Order__r.csord__Account__c
                    ));
                    
                    csord__Subscription__c subscription = new csord__Subscription__c(
                        Id = s.csord__Subscription__c,
                        Site__c = null
                    );
                    subsToUpdate.put(subscription.Id, subscription);
                }
            }
        }

        if (deliveryOrders.size() > 0) {
            // Assign the suborders to the right queue based on configuration of postal codes.
            // TODO: Check if is possible to pass the newSuborders list by reference and get the modified results before insert.
            this.assignTeamToDeliveryOrders(deliveryOrders, setSiteIds);
            insert deliveryOrders;
        }
        system.debug('*** new Delivery Orders: ' + JSON.serializePretty(deliveryOrders));

        Map<String, String> siteIdDeliveryOrderIdMap = new Map<String, String>();
        Integer idx = 0;
        for (String siteId : setSiteIds) {
            siteIdDeliveryOrderIdMap.put(siteId, String.valueOf(deliveryOrders[idx].Id));
            idx++;
        }
        system.debug('*** siteIdDeliveryOrderIdMap: ' + JSON.serializePretty(siteIdDeliveryOrderIdMap));
        //link Subscription with Suborder
        for (Integer i = 0; i < subsToUpdate.values().size(); i++) {
            String sCurrSuborderKey = String.valueOf(subsToUpdate.values()[i].Site__c);
            if(sCurrSuborderKey == null) {
                sCurrSuborderKey = '';
            }
            subsToUpdate.values()[i].COM_Delivery_Order__c = Id.valueOf(siteIdDeliveryOrderIdMap.get(sCurrSuborderKey));
        }

        // update the Subscriptions
        System.debug('*** create Suborders subscriptionsToUpdate: ' + JSON.serializePretty(subsToUpdate));
        updateList(subsToUpdate.values());

        //link Service to Suborder
        for (csord__Service__c s : services) {
            Id deliveryOrderId;
            
            if(s.Site__c != null) {
                deliveryOrderId = Id.valueOf(siteIdDeliveryOrderIdMap.get(String.valueOf(s.Site__c)));
            } else {
                deliveryOrderId = siteIdDeliveryOrderIdMap.get('');
            }
            
            servicesToUpdate.add(new csord__Service__c(
                Id = s.Id,
                COM_Delivery_Order__c = deliveryOrderId
            ));
        }

        //update Services
        System.debug('*** create Delivery Components servicesToUpdate: ' + JSON.serializePretty(servicesToUpdate));
        updateList(servicesToUpdate);
    }

    //REFACTOR TO BE IN CREATE SUBORDERS
    private void updateSuborderFields(List<csord__Service__c> services) {
        Map<Id, List<csord__Service__c>> suborderIdServicesMap = new Map<Id, List<csord__Service__c>>();
        Set<String> productAbbreviations = new Set<String>();

        suborderIdServicesMap = createMapDeliveryOrderIdServicesList(services);

        for (Id x : suborderIdServicesMap.keySet()) {
            if (suborderIdServicesMap.get(x) != null) {
                for (csord__Service__c s : suborderIdServicesMap.get(x)) {
                    productAbbreviations.add(s.VFZ_Product_Abbreviation__c);

                    if (s.Installation_Wishdate__c != null) {
                        if (this.subordersToUpdate.get(x) != null) {
                            if (this.subordersToUpdate.get(x).Wish_Date__c == null || this.subordersToUpdate.get(x).Wish_Date__c < s.Installation_Wishdate__c) {
                                this.subordersToUpdate.get(x).Wish_Date__c = s.Installation_Wishdate__c;
                            }
                        } else {
                            this.subordersToUpdate.put(x, new COM_Delivery_Order__c(
                                Id = x,
                                Wish_Date__c = s.Installation_Wishdate__c
                            ));
                        }
                    }
                }

                //save Product_Abbreviations__c to Suborder
                productAbbreviations.remove('');
                productAbbreviations.remove(null);
                if (!productAbbreviations.isEmpty()) {
                    if (this.subordersToUpdate.get(x) != null) {
                        if (this.subordersToUpdate.get(x).Product_Abbreviations__c == null) {
                            this.subordersToUpdate.get(x).Product_Abbreviations__c = String.join(new List<String>(productAbbreviations), ',');
                        }
                    } else {
                        this.subordersToUpdate.put(x, new COM_Delivery_Order__c(
                            Id = x,
                            Product_Abbreviations__c = String.join(new List<String>(productAbbreviations), ',')
                        ));
                    }
                }
                productAbbreviations.clear();
            }
        }
    }

    //REFACTOR to be done in createsuborders
    private void updateSuborderProducts(List<Id> subscriptionIds) {
        Map<Id, List<csord__Subscription__c>> suborderIdSubscriptionsMap = new Map<Id, List<csord__Subscription__c>>();
        List<csord__Subscription__c> subscriptions = [
            SELECT Id, Name, COM_Delivery_Order__c
            FROM csord__Subscription__c
            WHERE Id IN :subscriptionIds
        ];

        suborderIdSubscriptionsMap = createMapDeliveryOrderIdSubscriptionsList(subscriptions);

        for (Id x : suborderIdSubscriptionsMap.keySet()) {
            Set<String> subscriptionNames = new Set<String>();
            if (suborderIdSubscriptionsMap.get(x) != null) {

                for (csord__Subscription__c subs : suborderIdSubscriptionsMap.get(x)) {
                    subscriptionNames.add(subs.Name);
                }

                subscriptionNames.remove('');
                subscriptionNames.remove(null);
                if (this.subordersToUpdate.get(x) != null) {
                    this.subordersToUpdate.get(x).Products__c = String.join(new List<String>(subscriptionNames), ',');
                } else {
                    subordersToUpdate.put(x, new COM_Delivery_Order__c(
                        Id = x,
                        Products__c = String.join(new List<String>(subscriptionNames), ',')
                    ));
                }
            }
            subscriptionNames.clear();
        }
    }

    private Map<Id, List<csord__Subscription__c>> createMapDeliveryOrderIdSubscriptionsList(List<csord__Subscription__c> subscriptions) {
        Map<Id, List<csord__Subscription__c>> returnValue = new Map<Id, List<csord__Subscription__c>>();

        for (csord__Subscription__c subs : subscriptions) {
            if (returnValue.get(subs.COM_Delivery_Order__c) != null) {
                returnValue.get(subs.COM_Delivery_Order__c).add(subs);
            } else {
                returnValue.put(subs.COM_Delivery_Order__c, new List<csord__Subscription__c>{
                    subs
                });
            }
        }

        return returnValue;
    }

    private Map<Id, List<csord__Service__c>> createMapSubscriptionIdServicesList(List<csord__Service__c> services) {
        Map<Id, List<csord__Service__c>> returnValue = new Map<Id, List<csord__Service__c>>();

        for (csord__Service__c s : services) {
            if (returnValue.get(s.csord__Subscription__c) != null) {
                returnValue.get(s.csord__Subscription__c).add(s);
            } else {
                returnValue.put(s.csord__Subscription__c, new List<csord__Service__c>{
                    s
                });
            }
        }

        return returnValue;
    }

    private Map<Id, List<csord__Service__c>> createMapDeliveryOrderIdServicesList(List<csord__Service__c> services) {
        Map<Id, List<csord__Service__c>> returnValue = new Map<Id, List<csord__Service__c>>();

        for (csord__Service__c s : services) {
            if (s.csord__Service__c == null) {
                if (returnValue.get(s.COM_Delivery_Order__c) != null) {
                    returnValue.get(s.COM_Delivery_Order__c).add(s);
                } else {
                    returnValue.put(s.COM_Delivery_Order__c, new List<csord__Service__c>{
                        s
                    });
                }
            } else {
                if (returnValue.get(s.csord__Service__r.COM_Delivery_Order__c) != null) {
                    returnValue.get(s.csord__Service__r.COM_Delivery_Order__c).add(s);
                } else {
                    returnValue.put(s.csord__Service__r.COM_Delivery_Order__c, new List<csord__Service__c>{
                        s
                    });
                }
            }
        }

        return returnValue;
    }

    private void updateList(List<SObject> objects) {
        if (!objects.isEmpty()) {
            update objects;
        }
    }

    /**
     * @assignTeamToDeliveryOrders
     * @author Ernesto S Melgin <sebastian.melgin@vodafoneziggo.com>
     * @description
     * Assign each suborder from the parameter, into the right queue, based on suborder's Site__c.Site_Postal_Code__c
     * @param lSuborders
     */
    @TestVisible
    private void assignTeamToDeliveryOrders(List<COM_Delivery_Order__c> deliveryOrders, Set<String> setSiteIds) {
        Map<Id, Id> mapQueueIdBySiteId = this.getQueueByPostalCode(setSiteIds);
        for (COM_Delivery_Order__c deliveryOrder : deliveryOrders) {
            // This store the current Queue and Region
            system.debug('*** mapQueueIdBySiteId: ' + JSON.serializePretty(mapQueueIdBySiteId));
            system.debug('*** suborder.Site__c: ' + deliveryOrder.Site__c);
            system.debug('*** mapQueueIdBySiteId.get(suborder.Site__c): ' + mapQueueIdBySiteId.get(deliveryOrder.Site__c));
            deliveryOrder.OwnerId = mapQueueIdBySiteId.get(deliveryOrder.Site__c);
            deliveryOrder.Team__c = this.regionCodesByGroupId.get(deliveryOrder.OwnerId);
            
            // TODO - needs to be checked who is the owner for Mobile
            if(deliveryOrder.OwnerId == null) {
                deliveryOrder.OwnerId = UserInfo.getUserId();
            }
        }
    }

    /**
     * @getQueueByPostalCode
     * Creates a Map with SiteId and the QueueId (to be used on assignTeamToDeliveryOrders)
     * @author Ernesto S Melgin <sebastian.melgin@vodafoneziggo.com>
     * @param setSiteIds
     * @return
     */
    private Map<Id, Id> getQueueByPostalCode(Set<String> setSiteIds) {
        // Map the current Site_Postal_Code__c (first 4 numbers) to the Region and get the Queue_API_Name from Custom Metadata Type
        // With that, create a SOQL query to get the current Queue IDs based on Queue_API_Names and fill the Map.
        String sGroupTypeQueue = 'Queue';
        String sQueuePrefixName = 'COM_region_';
        String sOverflowQueueSuffix = 'Overflow';
        Map<Id, String> mapSiteIdsPostalCodeId = new Map<Id, String>();
        for (Site__c sSite : [SELECT Id, Site_Postal_Code__c FROM Site__c WHERE Id IN :setSiteIds]) {
            mapSiteIdsPostalCodeId.put(sSite.Id, sSite.Site_Postal_Code__c.left(4));
        }
        // Fill a map with postal codes and region name
        Map<String, String> mapPostalCodeRegionCode = new Map<String, String>();
        Set<String> setQueueNames = new Set<String>();
        for (Postal_Code__c pc : [SELECT Name, Region_Code__c FROM Postal_Code__c WHERE Name IN :mapSiteIdsPostalCodeId.values()]) {
            String sRegionTrimmed = pc.Region_code__c.trim();
            mapPostalCodeRegionCode.put(pc.Name, sRegionTrimmed);
            setQueueNames.add(sQueuePrefixName + sRegionTrimmed);
        }
        // add also the overflow queue (just in case there is some error, Site will be assigned to it and also their suborders).
        setQueueNames.add(sQueuePrefixName + sOverflowQueueSuffix);
        // Fill a map with Queue Names and Queue Ids
        Map<String, String> mapQueueNameQueueId = new Map<String, String>();
        Map<Id, String> mapRegionCodeByGroupId = new Map<Id, String>();
        for (Group grp : [SELECT Id, DeveloperName FROM Group WHERE DeveloperName IN :setQueueNames AND Type = :sGroupTypeQueue]) {
            mapQueueNameQueueId.put(grp.DeveloperName, String.valueOf(grp.Id));
            mapRegionCodeByGroupId.put(grp.Id, grp.DeveloperName.substringAfterLast('_'));
        }
        // Set for internal use on previous methods.
        this.regionCodesByGroupId = mapRegionCodeByGroupId;
        // Fill a map with SiteIds and QueueIds.
        // In case the Postal Code doesn't exists, the QueueId is the Overflow queue.
        Map<Id, String> mapSiteIdQueueId = new Map<Id, String>();
        for (Id iSiteId : mapSiteIdsPostalCodeId.keySet()) {
            String sPostalCode = mapSiteIdsPostalCodeId.get(iSiteId);
            String sRegionCode;
            if (mapPostalCodeRegionCode.containsKey(sPostalCode)) {
                sRegionCode = mapPostalCodeRegionCode.get(sPostalCode);
            } else {
                sRegionCode = sOverflowQueueSuffix;
            }
            String sQueueIdKey = sQueuePrefixName + sRegionCode;
            mapSiteIdQueueId.put(iSiteId, mapQueueNameQueueId.get(sQueueIdKey));
        }
        return mapSiteIdQueueId;
    }

    private void createParentDeliveryOrder(Set<Id> orderIds) {
        List<COM_Delivery_Order__c> deliveryOrders = new List<COM_Delivery_Order__c>();
        for(Id orderId : orderIds) {
            deliveryOrders.add(new COM_Delivery_Order__c(
                        Name = 'Delivery Order: ' + orderId + ' - Parent', //TO DO PUT ACCOUNT NAME HERE INSTEAD OF ORDERID
                        Order__c = orderId
                    ));
        }

        insert deliveryOrders;

        for(COM_Delivery_Order__c deliveryOrder : deliveryOrders) {
            deliveryOrderIdByOrderId.put(deliveryOrder.Order__c, deliveryOrder.Id);
        }
    }
}