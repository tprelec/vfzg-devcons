@isTest
private class TestAccountKeysWebService {
	@TestSetup
	static void makeData() {
		User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		TestUtils.autoCommit = true;
		Account acc = TestUtils.createAccount(u);
		acc.KVK_number__c = '12345678';
		update acc;

		Account acc1 = TestUtils.createAccount(u);

		Account acc2 = TestUtils.createAccount(u);
		acc2.KVK_number__c = '12345679';
		update acc2;

		List<External_Account__c> externalAccounts = new List<External_Account__c>();
		External_Account__c externalAccount = new External_Account__c();
		externalAccount.Account__c = acc.Id;
		externalAccount.External_Source__c = 'Unify';
		externalAccount.External_Account_Id__c = '12345678';
		externalAccounts.add(externalAccount);

		External_Account__c externalAccount2 = new External_Account__c();
		externalAccount2.Account__c = acc.Id;
		externalAccount2.External_Source__c = 'Unify';
		externalAccount2.External_Account_Id__c = '23456789';
		externalAccounts.add(externalAccount2);

		External_Account__c externalAccount3 = new External_Account__c();
		externalAccount3.Account__c = acc1.Id;
		externalAccount3.External_Source__c = 'Unify';
		externalAccount3.External_Account_Id__c = '34567891';
		externalAccounts.add(externalAccount3);
		insert externalAccounts;
	}
	@isTest
	static void testRunRequest() {
		RestRequest request = new RestRequest();
		request.requestUri =
			URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/accountkeys/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf('{"sourceSystem":"kvk","value":"12345678"}');
		RestContext.request = request;
		AccountKeysWebService.AccountKeysWebServiceResponse respons = AccountKeysWebService.runRequest();
		System.assertEquals(4, respons.accountKeys.size(), 'Two external account!');
	}

	@isTest
	static void testRunRequest2() {
		RestRequest request = new RestRequest();
		request.requestUri =
			URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/accountkeys/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf('{"sourceSystem":"unify","value":"34567891"}');
		RestContext.request = request;
		AccountKeysWebService.AccountKeysWebServiceResponse respons = AccountKeysWebService.runRequest();
		System.assertEquals(1, respons.errors.size(), 'One error exist!');
	}

	@isTest
	static void testRunRequest3() {
		RestRequest request = new RestRequest();
		request.requestUri =
			URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/accountkeys/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf('{"sourceSystem":"unify","value":"45678912"}');
		RestContext.request = request;
		AccountKeysWebService.AccountKeysWebServiceResponse respons = AccountKeysWebService.runRequest();
		System.assertEquals(
			'NOT_FOUND',
			respons.errors[0].errorCode,
			'External account not exist!'
		);
	}

	@isTest
	static void testRunRequest4() {
		RestRequest request = new RestRequest();
		request.requestUri =
			URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/accountkeys/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf('{"sourceSystem":"kvk","value":"12345679"}');
		RestContext.request = request;
		AccountKeysWebService.AccountKeysWebServiceResponse respons = AccountKeysWebService.runRequest();
		System.assertEquals(
			'MISSING_KEY',
			respons.errors[0].errorCode,
			'External account not exist!'
		);
	}

	@isTest
	static void testRunRequest5() {
		RestRequest request = new RestRequest();
		request.requestUri =
			URL.getSalesforceBaseUrl().toExternalForm() + '/services/apexrest/accountkeys/';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf('{"sourceSystem":"salescloud","value":"'+[SELECT Id FROM Account WHERE KVK_number__c = '12345679'][0].Id+'"}');
		RestContext.request = request;
		AccountKeysWebService.AccountKeysWebServiceResponse respons = AccountKeysWebService.runRequest();
		System.assertEquals(
			'MISSING_KEY',
			respons.errors[0].errorCode,
			'External account not exist!'
		);
	}
}