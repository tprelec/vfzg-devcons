@isTest
private class TestApprovalTriggerHandler {
    private static User owner;
    private static Account acc;
    private static Opportunity opp;
    private static cscfga__Product_Basket__c basket;
    private static appro__Approval_Path__c testPath;
    private static appro__Approval_Step__c testStep;
    private static appro__Approval_Request__c testRequest;
    private static appro__approval__c testApproval;

    private static void createTestData(Boolean insertApproval) {
        owner = TestUtils.createAdministrator();
        acc = TestUtils.createAccount(owner);
        opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

        basket = new cscfga__Product_Basket__c();
        basket.cscfga__Opportunity__c = opp.id;
        basket.Name = 'New Basket ' + system.now();
        insert basket;

        testPath = new appro__Approval_Path__c();
        insert testPath;

        testStep = new appro__Approval_Step__c();
        testStep.appro__Approval_Path__c = testPath.Id;
        testStep.Approver_gets_edit_rights__c = true;
        testStep.IsPendingUsageTrigger__c = true;
        insert testStep;

        testRequest = new appro__Approval_Request__c();
        testRequest.appro__RID__c = basket.Id;
        insert testRequest;

        testApproval = new appro__approval__c();
        testApproval.appro__Approval_Request__c = testRequest.Id;
        testApproval.appro__Approval_Step__c = testStep.Id;
        testApproval.appro__Approver_User__c = UserInfo.getUserId();
        testApproval.appro__Status__c = 'New';

        if (insertApproval) {
            insert testApproval;
        }
    }

    @isTest
    static void testSetApproverEditorWhenAssigned() {
        createTestData(true);

        Test.startTest();

        testApproval.appro__Status__c = 'Assigned';
        update testApproval;

        Test.stopTest();

        cscfga__Product_Basket__c resultProdBasket = [
            SELECT Users_with_edit_access__c
            FROM cscfga__Product_Basket__c
            WHERE Id = :basket.Id
        ];

        System.assertEquals(
            testApproval.appro__Approver_User__c + ';',
            resultProdBasket.Users_with_edit_access__c,
            'Product Basket Users with edit access should not be null.'
        );
    }

    @isTest
    static void testSetApproverEditorWhenNotAssigned() {
        createTestData(false);

        Test.startTest();

        insert testApproval;

        Test.stopTest();

        cscfga__Product_Basket__c resultProdBasket = [
            SELECT Users_with_edit_access__c
            FROM cscfga__Product_Basket__c
            WHERE Id = :basket.Id
        ];

        System.assertEquals(
            null,
            resultProdBasket.Users_with_edit_access__c,
            'Product Basket Users with edit access should be null.'
        );
    }

    @isTest
    static void testAutoApproval() {
        createTestData(true);

        Test.startTest();

        testApproval.appro__Status__c = 'Assigned';
        update testApproval;

        Test.stopTest();

        appro__approval__c resultApproval = [
            SELECT appro__status__c
            FROM appro__approval__c
            WHERE Id = :testApproval.Id
        ];

        System.assertEquals(
            'Approved',
            resultApproval.appro__status__c,
            'Approval status should be Approved.'
        );

        appro__Approval_Request__c resultApprReq = [
            SELECT Usage_Required__c
            FROM appro__Approval_Request__c
            WHERE Id = :testApproval.appro__Approval_Request__c
        ];

        System.assertEquals(
            true,
            resultApprReq.Usage_Required__c,
            'Approval Request Usage Required should be true.'
        );
    }
}