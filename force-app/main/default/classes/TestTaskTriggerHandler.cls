/**
 * @description       : provides use and code coverage for Task Trigger Handler
 * @author            : mcubias
 * @group             : leads united
 * @last modified on  : 19-09-2022
 * @last modified by  :
 **/
@IsTest
public class TestTaskTriggerHandler {
	@IsTest
	static void testCopyGeoValuesFromLeadsSuccess() {
		TestUtils.autoCommit = false;
		Lead d2dLead = TestUtils.createLead();
		d2dLead.Latitude = 51.81223;
		d2dLead.Longitude = 4.34343;
		insert d2dLead;
		Test.startTest();
		LG_SalesforceOneLeadActionsController.updateLeadStatusToNotReached(d2dLead.Id);
		Test.stopTest();
		List<Task> d2dTasks = [SELECT Id, Created_Longitude__c, Created_Latitude__c FROM Task WHERE WhoId = :d2dLead.Id];
		System.assertEquals(d2dLead.Latitude, d2dTasks.get(0).Created_Latitude__c, 'Latitude value was not copied over from Lead record');
		System.assertEquals(d2dLead.Longitude, d2dTasks.get(0).Created_Longitude__c, 'Latitude value was not copied over from Lead record');
	}

	@IsTest
	static void testCopyGeoValuesFromLeadsFail() {
		Account partnerAccount = TestUtils.createPartnerAccount();
		Test.startTest();
		Task t = new Task();
		t.Subject = 'D2D Visit';
		t.ActivityDate = System.today();
		t.Type = 'Result of D2D Visit';
		t.LG_Result__c = 'Not Reached';
		t.Status = 'Completed';
		t.WhatId = partnerAccount.Id;
		insert t;
		Test.stopTest();
		List<Task> d2dTasks = [SELECT Id, Created_Longitude__c, Created_Latitude__c FROM Task WHERE WhatId = :partnerAccount.Id];
		System.assert(d2dTasks.get(0).Created_Latitude__c == null, 'Latitude should only be populated when task is related to Lead record');
		System.assert(d2dTasks.get(0).Created_Longitude__c == null, 'Longitue should only be populated when task is related to Lead record');
	}
}
