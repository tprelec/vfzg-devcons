/**
 * @description       : Apex Test Class for the ClauseDefinitionTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestClauseDefinitionTriggerHandler {
	@testSetup
	static void setup() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'Clause Definition',
			External_Number__c = 1,
			Object_Prefix__c = 'CD-16-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;
	}

	@isTest
	static void testSetExternalIds() {
		csclm__Clause_Definition__c objClauseDefinition = new csclm__Clause_Definition__c(
			ExternalID__c = CS_DataTest.generateRandomString(10),
			Promo_start_date__c = System.today().addDays(-10),
			Promo_end_date__c = System.today().addDays(-3),
			DynamicId__c = 'Test'
		);

		Test.startTest();
		insert objClauseDefinition;
		Test.stopTest();

		csclm__Clause_Definition__c objClauseDefinitionUpdate = [
			SELECT Id, ExternalID__c
			FROM csclm__Clause_Definition__c
			WHERE Id = :objClauseDefinition.Id
			LIMIT 1
		];

		System.assertEquals('CD-16-000002', objClauseDefinitionUpdate.ExternalID__c, 'The External ID on Clause Definition was not set correctly.');
	}
}
