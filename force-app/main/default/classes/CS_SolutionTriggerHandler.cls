public with sharing class CS_SolutionTriggerHandler extends TriggerHandler{

    public override void afterUpdate(){
        Map<Id,Suborder__c> newMap = (Map<Id,Suborder__c>)Trigger.newMap;
        Map<Id,Suborder__c> oldMap = (Map<Id,Suborder__c>)Trigger.oldMap;
        Set<Id> changedSolutions = new Set<Id>();

        for(Id x :newMap.keySet()){
            if((newMap.get(x).Confirmed_Installation__c && !oldMap.get(x).Confirmed_Installation__c)
                    || (newMap.get(x).Customer_Signed_Off__c == 'Yes' && oldMap.get(x).Customer_Signed_Off__c != 'Yes')
                    || (newMap.get(x).Appointment_Changed__c && !oldMap.get(x).Appointment_Changed__c)
                    || (newMap.get(x).Start_Delivery__c && !oldMap.get(x).Start_Delivery__c)
                    || ((newMap.get(x).Status__c.equalsIgnoreCase('Cancellation Requested') && !oldMap.get(x).Status__c.equalsIgnoreCase('Cancellation Requested')))){
                changedSolutions.add(x);
            }
            /*//Trigger RBM New Provide
            if (newMap.get(x).Ready_for_billing__c && !oldMap.get(x).Ready_for_billing__c) {
                RBM_Integration_Endpoints__c rbmSettings = RBM_Integration_Endpoints__c.getOrgDefaults();
                if(rbmSettings.RBM_Enabled__c){
                    RBM_Utilities.triggerRBMNewProvision(oldMap.get(x), newMap.get(x));
                }
            }*/
        }
        
        if (changedSolutions.size() > 0) {
            CSPOFA.Events.emit('update', changedSolutions);
        }
    }
}