@isTest
public class TestOrderFormComponentListController {
	@TestSetup
	static void makeData() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);

		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationSite();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createOrderValidations();
		TestUtils.createOrderValidationNumberportingRow();
		TestUtils.createOrderValidationPhonebookRegistration();
		TestUtils.createOrderValidationHBO();
		TestUtils.createOrderValidationCompetitorAsset();
		TestUtils.createDefaultOnenetPBXType();
		TestUtils.createCompleteContract();

		TestUtils.autoCommit = true;
		Order__c order = TestUtils.createOrder(TestUtils.theContract);
		order.Status__c = 'Clean';
		update order;
	}

	@isTest
	static void testOrderWithFixedPropositions() {
		Test.startTest();
		Order__c order = [SELECT Id, Propositions__c FROM Order__c];
		order.Propositions__c = 'One Fixed;One Net;IPVPN;Legacy;Internet;Numberporting;bla bla';

		OrderFormComponentsListController controller = new OrderFormComponentsListController();
		controller.theCurrentOrder = order;
		OrderFormComponentsListController.orderFormComponent component1 = controller.customerComponent;
		OrderFormComponentsListController.orderFormComponent component2 = controller.billingComponent;
		OrderFormComponentsListController.orderFormComponent component3 = controller.orderComponent;
		OrderFormComponentsListController.orderFormComponent component4 = OrderFormComponentsListController.deliveryComponent;
		OrderFormComponentsListController.orderFormComponent component6 = controller.numberportingComponent;
		OrderFormComponentsListController.orderFormComponent component7 = controller.phonebookRegistrationComponent;
		OrderFormComponentsListController.orderFormComponent component8 = controller.orderProgressComponent;
		OrderFormComponentsListController.orderFormComponent component9 = controller.contractedProductsComponent;
		//Added Asserts to avoid suppressing PMD warnings for unused variables
		System.assertEquals('Customer', component1.componentType, 'Customer component not created');
		System.assertEquals('Billing', component2.componentType, 'Billing component not created');
		System.assertEquals('Order', component3.componentType, 'Order component not created');
		System.assertEquals('Delivery', component4.componentType, 'Delivery component not created');
		System.assertEquals('Numberblocks', component6.componentType, 'Numberblocks component created');
		System.assertEquals('PhonebookRegistration', component7.componentType, 'PhonebookRegistration component created');
		System.assertEquals('Progress', component8.componentType, 'Progress component not created');
		System.assertEquals('ContractedProducts', component9.componentType, 'ContractedProducts component not created');
		Test.stopTest();
	}

	@isTest
	public static void testOrderWithMobilePropositions() {
		Test.startTest();
		//creation of a new order for mobile under the same contract
		Ordertype__c ot = new Ordertype__c(Name = 'Enterprise Services', ExportSystem__c = 'BOP', Status__c = 'New', ExternalId__c = 'OD-04-abc-ES');
		insert ot;

		Billing_Arrangement__c objBar = [SELECT Id FROM Billing_Arrangement__c];
		Opportunity opp = [SELECT Id, Segment__c, SmallBusiness_Onderstroom__c, Escalation__c FROM Opportunity];
		opp.Segment__c = 'SoHo';
		opp.SmallBusiness_Onderstroom__c = true;
		opp.Escalation__c = true;

		VF_Contract__c objContract = [SELECT Id, Opportunity__c FROM VF_Contract__c];
		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Mobile',
			OrderType__c = ot.Id,
			VF_Contract__c = objContract.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			Billing_Arrangement__c = objBar.id
		);
		insert order;

		Order__c debugOrder = [
			SELECT
				Id,
				propositions__c,
				VF_Contract__r.Opportunity__c,
				VF_Contract__r.Opportunity__r.Escalation__c,
				Mobile_Fixed__c,
				Ordertype__r.Name,
				CustomerReady__c,
				BillingReady__c,
				Status__c,
				OrderReady__c,
				CTNReady__c,
				NumberportingReady__c,
				PhonebookregistrationReady__c,
				ContractedproductsReady__c,
				VF_Contract__r.Opportunity__r.Owner.UserType,
				VF_Contract__r.Opportunity__r.Segment__c
			FROM Order__c
			WHERE Propositions__c = 'Mobile'
			LIMIT 1
		];

		debugOrder.VF_Contract__r.Opportunity__r = opp;

		OrderFormComponentsListController controller = new OrderFormComponentsListController();
		controller.theCurrentOrder = debugOrder;
		OrderFormComponentsListController.orderFormComponent component1 = controller.customerComponent;
		OrderFormComponentsListController.orderFormComponent component2 = controller.billingComponent;
		OrderFormComponentsListController.orderFormComponent component3 = controller.orderComponent;
		OrderFormComponentsListController.orderFormComponent component5 = controller.ctnComponent;
		OrderFormComponentsListController.orderFormComponent component8 = controller.orderProgressComponent;
		OrderFormComponentsListController.orderFormComponent component9 = controller.contractedProductsComponent;
		//Added Asserts to avoid suppressing PMD warnings for unused variables
		System.assertEquals('Customer', component1.componentType, 'Customer component not created');
		System.assertEquals('Billing', component2.componentType, 'Billing component not created');
		System.assertEquals('Order', component3.componentType, 'Order component not created');
		System.assertEquals('CTN', component5.componentType, 'CTN component created for fixed order');
		System.assertEquals('Progress', component8.componentType, 'Progress component not created');
		System.assertEquals('ContractedProducts', component9.componentType, 'ContractedProducts component not created');
		Test.stopTest();
	}
}
