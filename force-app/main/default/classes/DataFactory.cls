/**
 * @description         The purpose of this class is to automate the manual process of creating test data for Testers
 *
 * @author              Chris Appels
 *
 */
public with sharing class DataFactory {

    public static String setupTestAccounts(Integer numberOfAccountsToSetup){
        List<Account> accounts = [select id, Unify_Account_Type__c, Unify_Account_SubType__c, SBI_Main_Description__c, SBI_Description__c, NumberOfEmployees,
                                   Legal_Structure_Code__c, Legal_Structure__c, DUNS_Number__c, Date_of_Establishment__c, Current_Fixed_Owner__c,
                                   BIK_Code__c, type, Visiting_Country__c, Visiting_Housenumber1__c, visiting_City__c, visiting_Postal_Code__c,
                                   visiting_Street__c, Vat_Number__c,  phone, Mobile_Dealer__c, ownerId, recordTypeId, name, KVK_number__c
                                   from Account where type = 'Prospect' and (NOT Name like '%ITE2%') and KVK_number__c != null limit :numberOfAccountsToSetup];
        List<User> owner= [select Id from User where name = 'Test Account Manager'];
        List<Dealer_Information__c> dealerInfo = [select Id from Dealer_Information__c where name = 'Test Account Manager'];
        Boolean isProduction = GeneralUtils.IsProduction();
        String toReturn = '';

        if (isProduction && !Test.isRunningTest()){
            throw new DataFactoryException('You are trying to run this Class on the production instance of Salesforce. This class is meant to be run in Sandboxes/UAT only');
        }
        if (dealerInfo.isEmpty()){
            throw new DataFactoryException('Can \'t find a Dealer Information record with the name \'Test Account Manager\'. This is required to run this class so please verify that it exists and the name is correct.');
        }
        if(owner.isEmpty()){
            throw new DataFactoryException('Can \'t find a User with the name \'Test Account Manager\'. This is required to run this class so please verify that it exists and the name is correct.');
        }
        if (accounts.size() == 0){
            throw new DataFactoryException('No Accounts where found of the Type \'Prospect\' with a name that dosen\'t start with \'ITE2\' found. Please import some more Accounts that have a valid KVK as well');
        }
        if (numberOfAccountsToSetup > accounts.size()){
            throw new DataFactoryException('You requested ' + String.valueOf(numberOfAccountsToSetup) + ' but only '+ String.valueOf(accounts.size()) +' are available. Either run the script for this number of Accounts or import new ones  of the Type \'Prospect\' with a name that dosen\'t start with \'ITE2\' and a valid KVK number.');
        }
        //String firstName = UserInfo.getFirstName();
        //String lastName = UserInfo.getLastName();
        Id customerRecordTypeId = GeneralUtils.recordTypeMap.get('Account').get('VF_Account');

        if (numberOfAccountsToSetup == 1){
            toReturn = new List<Id>(processData(accounts, customerRecordTypeId, dealerInfo[0], owner[0]))[0];
        } else {
            toReturn = new List<Id>(processData(new List<Account>{accounts[0]}, customerRecordTypeId,  dealerInfo[0], owner[0]))[0];
            DataFactoryBatch batch = new DataFactoryBatch(numberOfAccountsToSetup - 1,customerRecordTypeId);
            Database.executeBatch(batch, 5);
        }

        return toReturn;
    }

    public static Set<Id> processData(List<Account> accounts, Id customerRecordTypeId, Dealer_Information__c dealerInfo, User owner){
        Boolean isProduction = GeneralUtils.IsProduction();

        if (isProduction && !Test.isRunningTest()){
            throw new DataFactoryException('You are trying to run this Class on the production instance of Salesforce. This class is meant to be run in Sandboxes/UAT only');
        }
        for (Account a : accounts) {
            updateAccountInfo(a, customerRecordTypeId, dealerInfo, owner);
        }

        map<Id, Contact> contactsByAccountId = createContacts(accounts);
        Map<Id, Account> updatedAccounts = updateAuthorizedSigner(contactsByAccountId, accounts);

        /** Apparently the correct Accreditation Levels on Basket not being set is a CloudSense issue
            Going to leave this here just in case it dosen't get solved by them*/

        //list<Accreditation_Level__c> accreditations = createAccreditations(accounts);

        list<Opportunity> opportunities = createOpportunities(updatedAccounts.values(), owner, contactsByAccountId);
        createBaskets(opportunities);
        Map<Id, List<Competitor_Asset__c>> caByAccountId = createCompetitorAssets(updatedAccounts.values());
        Map<Id, Site__c> sitesByPbx = DatafactoryObjectGenerator.createSites(caByAccountId);
        updateAssetsWithSites(caByAccountId, sitesByPbx);
        DatafactoryObjectGenerator.createSitePostalCodeChecks(sitesByPbx.values());

        return updatedAccounts.keySet();
    }

    private static Account updateAccountInfo(Account a, Id customerRecordTypeId, Dealer_Information__c dealerInfo, User owner){
        a.name = formatAccountName(a.Name);
        a.recordTypeId = customerRecordTypeId;
        a.ownerId = owner.Id;
        a.Account_Owner_Manager__c = owner.id;
        a.Mobile_Dealer__c = dealerInfo.Id;
        a.Location_Status__c = 'A';

        if (a.phone == null) {
            a.phone = '0612345678';
        }
        if (a.Vat_Number__c == null) {
            a.Vat_Number__c = 'NL 123456789B01';
        }
        if (a.visiting_Street__c == null) {
            a.visiting_Street__c = 'Boven Vredenburgpassage';
        }
        if (a.visiting_Postal_Code__c == null) {
            a.visiting_Postal_Code__c = '3511 WR';
        }
        if (a.visiting_City__c == null) {
            a.visiting_City__c = 'Utrecht';
        }
        if (a.Visiting_Housenumber1__c == null) {
            a.Visiting_Housenumber1__c = 128;
        }
        if (a.Visiting_Country__c == null) {
            a.Visiting_Country__c = 'NL';
        }
        if (a.type == null) {
            a.type = 'Prospect';
        }
        if (a.BIK_Code__c == null) {
            a.BIK_Code__c = '18129';
        }
        if (a.Current_Fixed_Owner__c == null) {
            a.Current_Fixed_Owner__c = 'Test Account Manager';
        }
        if (a.Date_of_Establishment__c == null) {
            a.Date_of_Establishment__c = Date.today().addDays(-365);
        }
        if (a.DUNS_Number__c == null) {
            a.DUNS_Number__c = '0' + a.KVK_number__c;
        }
        if (a.Legal_Structure__c == null) {
            a.Legal_Structure__c = 'Eenmanszaak';
        }
        if (a.Legal_Structure_Code__c == null) {
            a.Legal_Structure_Code__c = '01';
        }
        if (a.NumberOfEmployees == null) {
            a.NumberOfEmployees = 100;
        }
        if (a.SBI_Description__c == null) {
            a.SBI_Description__c = 'Overige drukkerijen (rest)';
        }
        if (a.SBI_Main_Description__c == null) {
            a.SBI_Main_Description__c = 'Industrie';
        }
        if (a.Unify_Account_SubType__c == null) {
            a.Unify_Account_SubType__c = 'CRP';
        }
        if (a.Unify_Account_Type__c == null) {
            a.Unify_Account_Type__c = 'C';
        }

        return a;
    }

    private static Map<Id, Contact> createContacts(list<Account> accounts){
        Map<Id, Contact> toReturn = new Map<Id, Contact>();

        for (Account a : accounts) {
            Contact signer = new Contact();

            signer.firstName = formatAccountNameForOtherRecordNames(a.Name);
            signer.lastName = 'ITE2';
            signer.Gender__c = 'Male';
            signer.Phone = '0612345678';
            signer.MobilePhone = '0612345678';
            signer.Email = formatAccountNameForOtherRecordNames(a.Name) +  '@ITE2.nl';
            signer.Authorized_to_sign__c = true;
            signer.accountId = a.Id;
            toReturn.put(a.Id, signer);
        }
        Database.insert(toReturn.values(), false);
        return toReturn;
    }

    private static Map<Id, Account> updateAuthorizedSigner(map<Id, Contact> contactsByAccountId, List<Account> accounts){

        for (Account a : accounts) {
            a.Authorized_to_sign_1st__c = contactsByAccountId.get(a.Id).Id;
        }

        update accounts;
        return new Map<Id, Account>(accounts);
    }

    private static list<Opportunity> createOpportunities(list<Account> accounts, User owner, map<Id, Contact> contactsByAccount){
		List<Opportunity> toReturn = new List<Opportunity>();
		String recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Default').getRecordTypeId();

        for (Account a : accounts) {
            Opportunity o = new Opportunity();
            o.Name = formatAccountNameForOtherRecordNames(a.Name);
            o.AccountId = a.Id;
            o.Type = 'New Business';
            o.StageName = 'Closing';
            o.CloseDate = Date.today().addDays(100);
            o.OwnerId = owner.Id;
			o.Main_Contact_Person__c = contactsByAccount.get(a.Id).Id;
			o.RecordTypeId = recordTypeId;
            toReturn.add(o);
        }

        insert toReturn;
        return toReturn;
    }

    private static Map<Id, List<Competitor_Asset__c>> createCompetitorAssets(List<Account> accounts){
        Map<Id, List<Competitor_Asset__c>> caByAccountId = new Map<Id, List<Competitor_Asset__c>>();
        Id pabxRecordType = GeneralUtils.recordTypeMap.get('Competitor_Asset__c').get('PABX');
        List<Competitor_Asset__c> toInsert = new List<Competitor_Asset__c>();
        List<PBX_Type__c> pbxTypes = [select Id from PBX_Type__c limit 10];

        for (Account a : accounts){
            List<Competitor_Asset__c> cAssets = new List<Competitor_Asset__c>();

            for (integer i = 0; i<10; i++) {
                Competitor_Asset__c cAsset = new Competitor_Asset__c();
                cAsset.Account__c = a.Id;
                cAsset.PBX_type__c = pbxTypes[i].Id;
                cAsset.recordTypeId = pabxRecordType;
                cAssets.add(cAsset);
                toInsert.add(cAsset);
            }

            caByAccountId.put(a.Id, cAssets);
        }

        insert toInsert;
        return caByAccountId;
    }

    private static void updateAssetsWithSites(Map<Id, List<Competitor_Asset__c>> caByAccountId, Map<Id, Site__c> sitesByPbx){
        List<Competitor_Asset__c> toUpdate = new List<Competitor_Asset__c>();

        for (Id accountId : caByAccountID.keySet()) {
            for (Competitor_Asset__c ca : caByAccountID.get(accountId)) {
                ca.Site__c = sitesByPbx.get(ca.Id).Id;
                toUpdate.add(ca);
            }
        }

        update toUpdate;

        for (Id id : sitesByPbx.keySet()) {
            sitesByPbx.get(id).PBX__c = id;
        }

        update sitesByPbx.values();
    }

    private static List<cscfga__Product_Basket__c> createBaskets(List<Opportunity> opportunites){
        List<cscfga__Product_Basket__c> toReturn = new List<cscfga__Product_Basket__c>();

        for (Opportunity opp : opportunites) {
            cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
            basket.cscfga__Opportunity__c = opp.Id;
            basket.csbb__Account__c = opp.AccountId;

            /** Apparently the correct Accreditation Levels on Basket not being set is a CloudSense issue
            Going to leave this here just in case it dosen't get solved by them*/

            //basket.Accreditation_Level_Cloud__c = 2;
            //basket.Accreditation_Level_Infra__c = 2;
            //basket.Accreditation_Level_Mobility__c = 2;

            basket.Expected_delivery_date_for_Fixed__c = Date.today();
            basket.Expected_delivery_date_for_Mobile__c  = Date.today();
            toReturn.add(basket);
        }

        insert toReturn;
        return toReturn;
    }

    public class DataFactoryException extends Exception{}

    private static String formatAccountName(String accountName){
        accountName = accountName.replaceAll('[^a-zA-Z0-9\\s+]', '');
        accountName = accountName.deleteWhitespace();
        accountName += ' ITE2';

        return accountName;
    }

    private static String formatAccountNameForOtherRecordNames(String accountName){
        accountName = accountName.replace(' ITE2', '');
        accountName = accountName.left(10);
        return accountName;
    }
}