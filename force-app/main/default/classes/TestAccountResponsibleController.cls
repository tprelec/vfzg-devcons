@isTest
public class TestAccountResponsibleController {
	public static testMethod void TestARController_testCase() {
		// claim account
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		AccountResponsibleController ctrl = new AccountResponsibleController();
		AccountResponsibleController.fetchAll(acc.Id);
		AccountResponsibleController.claim(acc.Id, 'comment');
	}

	public static testMethod void TestARController_testCase2() {
		// record locked
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		AccountResponsibleController.claim(acc.Id, 'comment');
		AccountResponsibleController.fetchAll(acc.Id);

		Contact currContact = new Contact(
			LastName = 'CurrentUser',
			UserId__c = GeneralUtils.currentUser.Id,
			AccountId = [SELECT Id FROM Account LIMIT 1]
			.Id
		);
		insert currContact;
		Dealer_Information__c currDealer = new Dealer_Information__c(
			Contact__c = currContact.Id,
			Name = 'CurrentDealerInfo',
			Dealer_Code__c = '888888'
		);
		insert currDealer;
		acc.Claimed__c = true;
		acc.User__c = GeneralUtils.currentUser.Id;
		acc.New_Dealer__c = currDealer.Id;
		update acc;
	}

	public static testMethod void TestARController_testCase3() {
		// no dealer info error
		User manager = TestUtils.createManager();
		Account acc = TestUtils.createAccount(manager);
		Contact c = new Contact(LastName = 'DealerLastName', AccountId = acc.Id);
		insert c;
		Dealer_Information__c dealer = new Dealer_Information__c(
			Contact__c = c.Id,
			Name = 'DealerInfo',
			Dealer_Code__c = Label.Dealer_Code_CVM_Owner
		);
		insert dealer;
		AccountResponsibleController.fetchAll(acc.Id);

		// no validation error
		Contact currContact = new Contact(
			LastName = 'CurrentUser',
			UserId__c = GeneralUtils.currentUser.Id,
			AccountId = [SELECT Id FROM Account LIMIT 1]
			.Id
		);
		insert currContact;
		Dealer_Information__c currDealer = new Dealer_Information__c(
			Contact__c = currContact.Id,
			Name = 'CurrentDealerInfo',
			Dealer_Code__c = '888888'
		);
		insert currDealer;
		AccountResponsibleController.fetchAll(acc.Id);
		AccountResponsibleController.claim(acc.Id, 'comment');
	}
}
