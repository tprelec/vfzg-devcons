@isTest
public with sharing class TestCOM_CDOcreateTenant {
	@testSetup
	static void setup() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		Account testUmbrellaAccount = CS_DataTest.createAccount('Test Umbrella Account');
		testUmbrellaAccount.CDO_Umbrella_Service_Id__c = '111-222-333-444';
		insert testUmbrellaAccount;
	}

	@isTest
	static void testcreateTenantSyncSuccessfulRequests() {
		String umbrellaService = '2222-4444-5555';

		String serviceOrderRequest = COM_WebServiceConfig.getUUID();

		COM_Integration_setting__mdt tenantSpecificationSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);

		COM_CDO_integration_settings__mdt createTenantCDOSettings = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);

		String mockSyncSuccess =
			'{ "id":"' +
			serviceOrderRequest +
			'", "href":"https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId": "' +
			createTenantCDOSettings.externalId__c +
			'", "priority": "' +
			+createTenantCDOSettings.priority__c +
			'", "description": "' +
			+createTenantCDOSettings.Description__c +
			+'", "category": "' +
			+createTenantCDOSettings.Category__c +
			'", "state": "' +
			createTenantCDOSettings.serviceOrderItemSyncSuccessStatus__c +
			'", "orderDate":"2022-02-09T10:37:15.97221Z", "@type":"standard", "orderItem":[ { "id":"1", "action":"add", "state":"' +
			tenantSpecificationSetting.Sync_success_status__c +
			'", "@type":"standard", "service":{ "id":"' +
			umbrellaService +
			'", "name": "' +
			createTenantCDOSettings.serviceName__c +
			+'", "@type": "' +
			createTenantCDOSettings.serviceType__c +
			'","serviceCharacteristic":[{"name":"sourceSystem","valueType":"string","value":{"@type":"string","sourceSystem":"COM"}},{"name":"tenantName","valueType":"string","value":{"@type":"string","tenantName":"{{TenantName}}"}},{"name":"tenantContactName","valueType":"string","value":{"@type":"string","tenantContactName":"{{TenantContact}}"}},{"name":"tenantContactPhone","valueType":"string","value":{"@type":"string","tenantContactPhone":"{{TenantContactPhone}}"}},{"name":"tenantContactEmail","valueType":"string","value":{"@type":"string","tenantContactEmail":"{{TenantContactEmail}}"}}],"serviceSpecification":' +
			'{"id":"' +
			createTenantCDOSettings.serviceSpecificationId__c +
			'", "invariantID": "' +
			createTenantCDOSettings.serviceSpecification_invariantID__c +
			'", "version": "' +
			createTenantCDOSettings.serviceSpecification_version__c +
			'", "name": "' +
			createTenantCDOSettings.serviceSpecification_name__c +
			'", "@type": "' +
			createTenantCDOSettings.serviceSpecificationType__c +
			'"} } } ] }';

		Account account = [SELECT Id FROM Account WHERE CDO_Umbrella_Service_Id__c != NULL];
		Id accountId = account.Id;
		Test.startTest();
		COM_MockWebService.setTestMockResponse(200, 'OK', mockSyncSuccess);
		COM_CDOcreateTenant.createTenantService(accountId);

		Test.stopTest();

		account = [SELECT Id, CDO_Umbrella_Service_Id__c, CDO_Integration_Status__c FROM Account WHERE Id = :accountId];
		System.assertEquals(account.CDO_Umbrella_Service_Id__c, umbrellaService, 'Umbrella service not set correctly in sync success test');
	}

	@isTest
	static void testcreateTenantSyncFailedRequests() {
		String mockSyncFailure = '{"code":"40020","reason":"SERVICE_ORDER_CHARACTERISTICS_VALIDATION_ERROR","message":"Request validation has failed due the following issue(s) - [ Input parameter tenantContactEmail violates constraint, Invalid value ]"}';

		Account account = [SELECT Id FROM Account WHERE CDO_Umbrella_Service_Id__c != NULL];
		Id accountId = account.Id;
		Test.startTest();
		COM_MockWebService.setTestMockResponse(200, 'OK', mockSyncFailure);
		COM_CDOcreateTenant.createTenantService(accountId);

		Test.stopTest();
		account = [SELECT Id, CDO_Umbrella_Service_Id__c, CDO_Integration_Status__c FROM Account WHERE Id = :accountId];
		System.assertEquals(account.CDO_Umbrella_Service_Id__c, null, 'Umbrella service not set correctly');
		System.assertEquals(
			account.CDO_Integration_Status__c,
			COM_Constants.CDO_INTEGRATION_STATUS_FAILURE,
			'Integration status not set correctly in sync failed test'
		);
	}

	@isTest
	static void testcreateTenantSuccessfulRequestAsync() {
		String umbrellaService = '111-222-333-444';
		String tenantId = umbrellaService;
		String serviceOrderRequest = COM_WebServiceConfig.getUUID();
		COM_Integration_setting__mdt tenantSpecificationSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);
		COM_CDO_integration_settings__mdt createTenantCDOSettings = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);

		String mockASyncSuccess =
			'{ "eventId": "deff54ee-ba37-4dba-b1c9-1fe1e554a4e1", "eventTime": "2022-02-08T11:30:15.97221Z", "eventType": "ServiceOrderStateChangeNotification", "event": { "serviceOrder": { "id": "' +
			serviceOrderRequest +
			'", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId": "' +
			createTenantCDOSettings.externalId__c +
			'", "priority": "' +
			+createTenantCDOSettings.priority__c +
			'", "description": "' +
			+createTenantCDOSettings.Description__c +
			+'", "category": "' +
			+createTenantCDOSettings.Category__c +
			'", "state": "' +
			+createTenantCDOSettings.serviceOrderItemAsyncSuccessStatus__c +
			'", "orderDate": "2022-02-08T10:37:15.97221Z", "startDate": "2022-02-08T10:37:15.97321Z","completionDate":"2022-02-08T10:37:15.97321Z", "@type": "' +
			+createTenantCDOSettings.requestType__c +
			'", "orderItem": [ { "id": "1", "action": "add", "state": "' +
			+createTenantCDOSettings.serviceOrderItemAsyncSuccessStatus__c +
			'", "@type": "standard", "service": { "id": "' +
			umbrellaService +
			'", "name": "' +
			createTenantCDOSettings.serviceName__c +
			+'", "serviceState": "' +
			tenantSpecificationSetting.Async_success_status__c +
			+'", "@type": "' +
			createTenantCDOSettings.serviceType__c +
			'", "serviceCharacteristic":[{"name":"sourceSystem","valueType":"string","value":{"@type":"string","sourceSystem":"COM"}},{"name":"tenantName","valueType":"string","value":{"@type":"string","tenantName":"{{TenantName}}"}},{"name":"tenantContactName","valueType":"string","value":{"@type":"string","tenantContactName":"{{TenantContact}}"}},{"name":"tenantContactPhone","valueType":"string","value":{"@type":"string","tenantContactPhone":"{{TenantContactPhone}}"}},{"name":"tenantContactEmail","valueType":"string","value":{"@type":"string","tenantContactEmail":"{{TenantContactEmail}}"}}],"serviceSpecification":{"id":"' +
			createTenantCDOSettings.serviceSpecificationId__c +
			'", "invariantID": "' +
			createTenantCDOSettings.serviceSpecification_invariantID__c +
			'", "version": "' +
			createTenantCDOSettings.serviceSpecification_version__c +
			'", "name": "' +
			createTenantCDOSettings.serviceSpecification_name__c +
			'", "@type": "' +
			createTenantCDOSettings.serviceSpecificationType__c +
			'" } } } ] } } }';

		RestRequest request = new RestRequest();
		request.requestUri = '/testCDOCreateTenant/*';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(mockAsyncSuccess);
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		COM_CDONotificationListener.processNotification();
		Test.stopTest();

		List<Account> accounts = [
			SELECT Id, CDO_Tenant_Id__c, CDO_Integration_Status__c, CDO_Umbrella_Service_Id__c
			FROM Account
			WHERE CDO_Umbrella_Service_Id__c = :umbrellaService
		];
		Account acc = accounts != null ? accounts[0] : null;
		System.assertEquals(tenantId, acc.CDO_Tenant_Id__c, 'CDO tenant ID not set correctly in async success test!');
		System.assertEquals(umbrellaService, acc.CDO_Umbrella_Service_Id__c, 'CDO umbrella service not set correctly in async success test!');
		System.assertEquals(
			COM_Constants.CDO_INTEGRATION_STATUS_COMPLETE,
			acc.CDO_Integration_Status__c,
			'Completed CDO Integration status not set correctly in async success test!'
		);
	}

	@isTest
	static void testcreateTenantFailedRequestAsync() {
		String umbrellaService = '111-222-333-444';

		COM_Integration_setting__mdt tenantSpecificationSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);

		COM_CDO_integration_settings__mdt createTenantCDOSettings = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);

		String serviceOrderRequest = COM_WebServiceConfig.getUUID();

		String mockAsyncFailed =
			'{ "eventId": "deff54ee-ba37-4dba-b1c9-1fe1e554a4e1", "eventTime": "2022-02-08T11:30:15.97221Z", "eventType": "ServiceOrderStateChangeNotification", "event": { "serviceOrder": { "id": "' +
			serviceOrderRequest +
			'", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId": "' +
			createTenantCDOSettings.externalId__c +
			'", "priority": "' +
			+createTenantCDOSettings.priority__c +
			'", "description": "' +
			+createTenantCDOSettings.Description__c +
			+'", "category": "' +
			+createTenantCDOSettings.Category__c +
			'", "state": "' +
			createTenantCDOSettings.serviceOrderItemAsyncFailedStatus__c +
			'", "orderDate": "2022-02-08T10:37:15.97221Z", "startDate": "2022-02-08T10:37:15.97321Z", "@type": "standard", "orderItem": [ { "id": "1", "action": "add", "state": "' +
			createTenantCDOSettings.serviceOrderItemAsyncFailedStatus__c +
			'", "@type": "standard", "service": { "id": "' +
			umbrellaService +
			'", "name": "' +
			createTenantCDOSettings.serviceName__c +
			+'", "serviceState": "' +
			tenantSpecificationSetting.Async_failed_status__c +
			+'", "@type": "' +
			createTenantCDOSettings.serviceType__c +
			'","serviceCharacteristic":[{"name":"sourceSystem","valueType":"string","value":{"@type":"string","sourceSystem":"COM"}},{"name":"tenantName","valueType":"string","value":{"@type":"string","tenantName":"{{TenantName}}"}},{"name":"tenantContactName","valueType":"string","value":{"@type":"string","tenantContactName":"{{TenantContact}}"}},{"name":"tenantContactPhone","valueType":"string","value":{"@type":"string","tenantContactPhone":"{{TenantContactPhone}}"}},{"name":"tenantContactEmail","valueType":"string","value":{"@type":"string","tenantContactEmail":"{{TenantContactEmail}}"}}],"serviceSpecification":{"id":"' +
			createTenantCDOSettings.serviceSpecificationId__c +
			'", "invariantID": "' +
			createTenantCDOSettings.serviceSpecification_invariantID__c +
			'", "version": "' +
			createTenantCDOSettings.serviceSpecification_version__c +
			'", "name": "' +
			createTenantCDOSettings.serviceSpecification_name__c +
			'", "@type": "' +
			createTenantCDOSettings.serviceSpecificationType__c +
			'" } } } ] }, "errorEvents": [ { "code": 510105000, "reason": "EXTERNAL_SYSTEM_ERROR", "message": "A fulfillment activity failed with error-code 5000, error-message some error message." } ] } }';

		RestRequest request = new RestRequest();
		request.requestUri = '/testCDOCreateTenant/*';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(mockAsyncFailed);
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		COM_CDONotificationListener.processNotification();
		Test.stopTest();

		List<Account> accounts = [
			SELECT Id, CDO_Tenant_Id__c, CDO_Integration_Status__c, CDO_Umbrella_Service_Id__c
			FROM Account
			WHERE CDO_Umbrella_Service_Id__c = :umbrellaService
		];
		Account acc = accounts != null ? accounts[0] : null;
		System.assertEquals(
			COM_Constants.CDO_INTEGRATION_STATUS_FAILURE,
			acc.CDO_Integration_Status__c,
			'Failed CDO Integration status not set correctly in async failed test!'
		);
	}
}
