@isTest
private class TestOrderFormOrderDataAHController {
	@isTest
	static void testFetchProductBasketId() {
		cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
		basket.csbb__Synchronised_With_Opportunity__c = true;
		update basket;

		OrderFormOrderDataAHController orderFormDataApprovalHistoryController = new OrderFormOrderDataAHController();
		Test.startTest();
		orderFormDataApprovalHistoryController.fetchProductBasketId(basket.cscfga__Opportunity__c);
		Test.stopTest();

		System.assertEquals(basket.Id, orderFormDataApprovalHistoryController.basketId, 'Same basket must be selected in the controller.');
	}

	@isTest
	static void testOrderFormOrderDataControllerNull() {
		OrderFormOrderDataAHController orderFormDataApprovalHistoryController = new OrderFormOrderDataAHController();

		Test.startTest();
		orderFormDataApprovalHistoryController.parentController = null;
		Test.stopTest();

		System.assertEquals(null, orderFormDataApprovalHistoryController.basketId, 'No basket should be fetched without parent controller.');
	}

	@isTest
	static void testOpporutnityWithoutBasketFetchingBasketId() {
		Opportunity opportunity = new Opportunity(Name = 'Test Opportunity', StageName = 'Closing', CloseDate = system.today());
		insert opportunity;

		OrderFormOrderDataAHController orderFormDataApprovalHistoryController = new OrderFormOrderDataAHController();

		Test.startTest();
		orderFormDataApprovalHistoryController.fetchProductBasketId(opportunity.Id);
		Test.stopTest();

		System.assertEquals(
			null,
			orderFormDataApprovalHistoryController.basketId,
			'No basket should be fetched when not connected to the opportunity.'
		);
	}
}
