public with sharing class CS_PlInputs {

    public Map<String, Double> flexibilityMargins { get; set; }

    public Map<Id, Integer> siteCountForConfig { get; set; }

    public CS_PlInputs() {
        flexibilityMargins = new Map<String, Double>();
        siteCountForConfig = new Map<Id, Integer>();
    }
}