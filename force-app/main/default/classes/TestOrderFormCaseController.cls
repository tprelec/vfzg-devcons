@isTest
public with sharing class TestOrderFormCaseController {

    public static testMethod void testRedirectToOrderForm() {
        PageReference pageRef = Page.OrderFormLogCase;
        Test.setCurrentPage(pageRef);

        Account acc = TestUtils.createAccount(null, null);
        Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
        VF_Contract__c vfContract = TestUtils.createVFContract(acc, opp);
        Order__c order = TestUtils.createOrder(vfContract);         

        ApexPages.currentPage().getParameters().put('orderId', order.Id);
        ApexPages.currentPage().getParameters().put('contractId', vfContract.Id);

        OrderFormCaseController controller = new OrderFormCaseController();
        controller.fileName = 'FileName.txt';
        controller.document = Blob.valueOf('Hello World');
        Pagereference page = controller.insertCaseRedirectToOrderForm();

        System.assertEquals('/apex/orderformpreload?contractId=' + vfContract.Id + '&orderId=' + order.Id, 
                            page.getUrl(),
                            'We should be redirected back to the OrderForm with the correct paramaters');
    }

    public static testMethod void testRedirectToCase() {
        PageReference pageRef = Page.OrderFormLogCase;
        Test.setCurrentPage(pageRef);
        
        Account acc = TestUtils.createAccount(null, null);
        Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
        VF_Contract__c vfContract = TestUtils.createVFContract(acc, opp);
        Order__c order = TestUtils.createOrder(vfContract);   

        ApexPages.currentPage().getParameters().put('orderId', order.Id);
        ApexPages.currentPage().getParameters().put('contractId', vfContract.Id);

        OrderFormCaseController controller = new OrderFormCaseController();
        controller.fileName = 'FileName.txt';
        controller.document = Blob.valueOf('Hello World');
        Pagereference page = controller.insertCaseRedirectToCase();

        Case theCase = [select Id from Case];

        System.assertEquals('/' + theCase.Id, 
                            page.getUrl(),
                            'We should be redirected back to the OrderForm with the correct paramaters');
    }

    public static testMethod void testRedirectToOrderFormFail() {
        PageReference pageRef = Page.OrderFormLogCase;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('orderId', UserInfo.getOrganizationId());
        ApexPages.currentPage().getParameters().put('contractId', UserInfo.getOrganizationId());
        OrderFormCaseController controller = new OrderFormCaseController();

        controller.insertCaseRedirectToOrderForm();
        String error = ApexPages.getMessages()[0].getDetail();
        System.debug(error);

        System.assert(error.contains('id value of incorrect type:'), 
                      'The controller should throw an error message');
    }

    public static testMethod void testRedirectToCaseFail() {
        PageReference pageRef = Page.OrderFormLogCase;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('orderId', UserInfo.getOrganizationId());
        ApexPages.currentPage().getParameters().put('contractId', UserInfo.getOrganizationId());
        OrderFormCaseController controller = new OrderFormCaseController();

        controller.insertCaseRedirectToCase();
        String error = ApexPages.getMessages()[0].getDetail();
        System.debug(error);

        System.assert(error.contains('id value of incorrect type:'), 
                        'The controller should throw an error message');
    }
}