@isTest
private class TestECSPostalCodeCheckService {
	@TestSetup
	static void makeData() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Site__c s = createSite(acct, false);
		s.Postal_Code_Check_Status__c = 'Combined check requested';
		insert s;
	}

	public static Site__c createSite(Account acct, Boolean hasCanvas) {
		Site__c s = new Site__c();
		s.Site_Account__c = acct.Id;
		s.Site_Street__c = 'test';
		s.Site_City__c = 'testCity';
		s.Site_Postal_Code__c = '1234AB';
		s.Site_House_Number__c = 5;
		s.Site_House_Number_Suffix__c = 'a';
		if (hasCanvas) {
			s.Canvas_Street__c = 'Canvas test Street';
			s.Canvas_City__c = 'Canvas test City';
			s.Canvas_Postal_Code__c = '1234CD';
			s.Canvas_House_Number__c = 987;
			s.Canvas_House_Number_Suffix__c = 'Cprefex';
		}
		return s;
	}

	@isTest
	static void testMakeChecks() {
		Site__c theSite = [
			SELECT
				Id,
				Blocked_checks__c,
				Last_fiber_check__c,
				Last_dsl_check__c,
				Last_dsl_Canvas_check_c__c,
				Last_fiber_Canvas_check_c__c,
				Postal_Code_Check_Status__c,
				Site_Street__c,
				Site_Postal_Code__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c
			FROM Site__c
			WHERE Site_City__c = 'testCity'
			LIMIT 1
		];
		ECSPostalCodeCheckService service = new ECSPostalCodeCheckService();
		List<ECSPostalCodeCheckService.request> requests = new List<ECSPostalCodeCheckService.Request>{};
		ECSPostalCodeCheckService.request req = new ECSPostalCodeCheckService.request();
		req.postalCode = theSite.Site_Postal_Code__c;
		req.houseNumber = Integer.valueOf(theSite.Site_House_Number__c);
		req.houseNumberExtension = theSite.Site_House_Number_Suffix__c;
		requests.add(req);
		Test.setMock(WebServiceMock.class, new ECSSOAPPostalCodeCheckMocks.SdslbisCheck());
		Test.startTest();
		service.setRequest(requests);
		service.makeRequest('sdslbis');
		List<ECSPostalCodeCheckService.response> responseList = service.getResponse();
		System.assertEquals('1234AB', responseList[0].postalCode, 'Postal/ZIP Code returned: ' + responseList[0].postalCode);
		Test.setMock(WebServiceMock.class, new ECSSOAPPostalCodeCheckMocks.DslCheck());
		service.makeRequest('dsl');
		List<ECSPostalCodeCheckService.response> responseListDSL = service.getResponse();
		System.assertEquals('1234AB', responseListDSL[0].postalCode, 'Postal/ZIP Code returned: ' + responseListDSL[0].postalCode);
		Test.setMock(WebServiceMock.class, new ECSSOAPPostalCodeCheckMocks.CoaxCheck());
		service.makeRequest('coax');
		List<ECSPostalCodeCheckService.response> responseListCoax = service.getResponse();
		System.assertEquals('1234AB', responseListCoax[0].postalCode, 'Postal/ZIP Code returned: ' + responseListCoax[0].postalCode);
		Test.setMock(WebServiceMock.class, new ECSSOAPPostalCodeCheckMocks.FiberCheck());
		service.makeRequest('fiber');
		List<ECSPostalCodeCheckService.response> responseListFiber = service.getResponse();
		System.assertEquals('1234AB', responseListFiber[0].postalCode, 'Postal/ZIP Code returned: ' + responseListFiber[0].postalCode);
		Test.setMock(WebServiceMock.class, new ECSSOAPPostalCodeCheckMocks.CombinedCheck());
		service.makeRequest('combined');
		List<ECSPostalCodeCheckService.response> responseListCombined = service.getResponse();
		System.assertEquals('1234AB', responseListCombined[0].postalCode, 'Postal/ZIP Code returned: ' + responseListCombined[0].postalCode);
		Test.stopTest();
	}

	@isTest
	static void testMakeCheckError() {
		Site__c theSite = [
			SELECT
				Id,
				Blocked_checks__c,
				Last_fiber_check__c,
				Last_dsl_check__c,
				Last_dsl_Canvas_check_c__c,
				Last_fiber_Canvas_check_c__c,
				Postal_Code_Check_Status__c,
				Site_Street__c,
				Site_Postal_Code__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c
			FROM Site__c
			WHERE Site_City__c = 'testCity'
			LIMIT 1
		];
		ECSPostalCodeCheckService service = new ECSPostalCodeCheckService();
		List<ECSPostalCodeCheckService.request> requests = new List<ECSPostalCodeCheckService.Request>{};
		ECSPostalCodeCheckService.request req = new ECSPostalCodeCheckService.request();
		req.postalCode = theSite.Site_Postal_Code__c;
		req.houseNumber = Integer.valueOf(theSite.Site_House_Number__c);
		req.houseNumberExtension = theSite.Site_House_Number_Suffix__c;
		requests.add(req);
		Test.setMock(WebServiceMock.class, new ECSSOAPPostalCodeCheckMocks.CoaxCheckError());
		Test.startTest();
		service.setRequest(requests);
		service.makeRequest('coax');
		List<ECSPostalCodeCheckService.response> responseListCoax = service.getResponse();
		System.assertEquals(
			'error on coax',
			responseListCoax[0].errorMessages[0].errorMessage,
			'Postal/ZIP Code returned: ' + responseListCoax[0].errorMessages[0].errorMessage
		);
		Test.stopTest();
	}

	@isTest
	public static void testExceptions() {
		Test.startTest();
		ECSPostalCodeCheckService postalService = new ECSPostalCodeCheckService();
		List<ECSPostalCodeCheckService.request> requests = new List<ECSPostalCodeCheckService.request>();
		ECSPostalCodeCheckService.request req = new ECSPostalCodeCheckService.request();
		requests.add(req);
		postalService.setRequest(requests);
		try {
			//remove items from list
			requests.clear();
			postalService.setRequest(requests);
			postalService.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		try {
			//remove items from list
			requests = null;
			postalService.setRequest(requests);
			postalService.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		Test.stopTest();
	}

	@isTest
	static void testMakeNoCallout() {
		Site__c theSite = [
			SELECT
				Id,
				Blocked_checks__c,
				Last_fiber_check__c,
				Last_dsl_check__c,
				Last_dsl_Canvas_check_c__c,
				Last_fiber_Canvas_check_c__c,
				Postal_Code_Check_Status__c,
				Site_Street__c,
				Site_Postal_Code__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c
			FROM Site__c
			WHERE Site_City__c = 'testCity'
			LIMIT 1
		];
		ECSPostalCodeCheckService service = new ECSPostalCodeCheckService();
		List<ECSPostalCodeCheckService.request> requests = new List<ECSPostalCodeCheckService.Request>{};
		ECSPostalCodeCheckService.request req = new ECSPostalCodeCheckService.request();
		req.postalCode = theSite.Site_Postal_Code__c;
		req.houseNumber = Integer.valueOf(theSite.Site_House_Number__c);
		req.houseNumberExtension = theSite.Site_House_Number_Suffix__c;
		requests.add(req);
		service.setRequest(requests);
		Test.startTest();

		try {
			service.makeRequest('fiber');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage()
					.contains(
						'There was an error when attempting to do the fiber check. EoF might have completed OK, though. Please contact your administrator.'
					),
				'when there is no mock fiber'
			);
		}
		try {
			service.makeRequest('sdslbis');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage()
					.contains(
						'There was an error when attempting to do the EoF check. Fiber might have completed OK, though. Please contact your administrator.'
					),
				'when there is no mock sdslbis'
			);
		}
		try {
			service.makeRequest('coax');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage()
					.contains(
						'There was an error when attempting to do the coax check. Fiber and EoK might have completed OK, though. Please contact your administrator.'
					),
				'when there is no mock coax'
			);
		}
		try {
			service.makeRequest('dsl');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('There was an error when attempting to do the dsl check. Please contact your administrator.'),
				'when there is no mock dsl'
			);
		}
		try {
			service.makeRequest('combined');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('There was an error when attempting to do the combined check. Please contact your administrator.'),
				'when there is no mock combined'
			);
		}
		Test.stopTest();
	}
}
