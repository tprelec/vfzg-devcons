public class EMP_SubmitOrderFE {
    public class Request {
        public SubmitOrderInputData submitOrderInputData;
    }

    public class SubmitOrderInputData {
        public String orderID; //mandatory, comes from ValidateOrderFE
        public String externalOrderID; //optional, but left in case its needed for Drop2
        public SubmitOrderInputData(String tempOrderID, String tempExternalOrderID) {
            this.orderID = tempOrderID;
            this.externalOrderID = tempExternalOrderID;
        }
    }

    public class Response {
        public Integer status; 
        public List<Data> data; 
        public List<EMP_ResponseError.Error> error; 
        public Response(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        switch on text {
                            when 'status' {
                                status = parser.getIntegerValue();
                            }
                            when 'data' {
                                data = arrayOfData(parser);
                            }
                            when 'error' {
                                error = EMP_ResponseError.arrayOfError(parser);
                            }
                            when else {
                                System.debug(LoggingLevel.WARN, 'Response consuming unrecognized property: '+text);  
                            }
                        }
                    }
                }
            }
        }
    }

    public class Data {
        public String status;
		public Data(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        switch on text {
                            when 'status' {
                                status = parser.getText();
                            }
                            when else {
                                System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
                            }
                        }
					}
				}
			}
		}
    }

    private static List<Data> arrayOfData(System.JSONParser p) {
        List<Data> res = new List<Data>();
        if (p.getCurrentToken() == null) {
            p.nextToken();
        }
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Data(p));
        }
        return res;
    }

    public static Response parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new Response(parser);
	}
}