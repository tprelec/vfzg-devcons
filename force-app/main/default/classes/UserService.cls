public with sharing class UserService {
	private static final Set<String> CLOUDSENSE_PACKAGES = new Set<String>{
		'csbb',
		'csclm',
		'csclmcb',
		'csordtelcoa',
		'cscfga',
		'cspl',
		'csdiscounts'
	};
	private static final Set<String> MAVENMULE_PACKAGES = new Set<String>{ 'mmdoc' };
	private static final Set<String> CLOUDSENSE_MAVENMULE_PACKAGES = CollectionUtils.joinStringSets(
		new List<Set<String>>{ CLOUDSENSE_PACKAGES, MAVENMULE_PACKAGES }
	);

	private static final Set<String> VFZ_MAVENDOCS_USER = new Set<String>{
		'VFZ_Maven_Documents_User'
	};

	private static final Map<String, Set<String>> PROFILE_TO_PACKAGE_MAP = new Map<String, Set<String>>{
		'VF Solution Sales' => CLOUDSENSE_MAVENMULE_PACKAGES,
		'VF Sales Support' => CLOUDSENSE_MAVENMULE_PACKAGES,
		'VF Retail ZS' => CLOUDSENSE_MAVENMULE_PACKAGES,
		'VF Retail manager ZS' => CLOUDSENSE_MAVENMULE_PACKAGES,
		'VF Sales Manager' => CLOUDSENSE_PACKAGES,
		'VF Proposal User' => CLOUDSENSE_PACKAGES,
		'VF Partner Portal User' => CLOUDSENSE_MAVENMULE_PACKAGES,
		'VF Manager EBU' => CLOUDSENSE_PACKAGES,
		'VF Indirect Inside Sales' => CLOUDSENSE_PACKAGES,
		'VF Head Sales Manager' => CLOUDSENSE_PACKAGES,
		'VF Financial Sales Control' => CLOUDSENSE_PACKAGES,
		'VF EBU/WBU Control' => CLOUDSENSE_PACKAGES,
		'VF Enterprise Legal' => CLOUDSENSE_PACKAGES,
		'VF Direct Inside Sales' => CLOUDSENSE_PACKAGES,
		'VF Business Partner Manager' => CLOUDSENSE_PACKAGES,
		'VF Account Manager' => CLOUDSENSE_MAVENMULE_PACKAGES,
		'VF Business Improvement' => CLOUDSENSE_PACKAGES,
		'System Administrator' => CLOUDSENSE_MAVENMULE_PACKAGES,
		'VF Customer Advocacy' => CLOUDSENSE_PACKAGES,
		'VF Telesales' => MAVENMULE_PACKAGES,
		'LG_NL D2D_Partner Sales User' => MAVENMULE_PACKAGES,
		'LG_NL D2D_Partner Manager' => MAVENMULE_PACKAGES,
		'LG_NL Sales Management User' => MAVENMULE_PACKAGES
	};

	private static final Set<String> PERMISSION_SETS_NAMES_FOR_ALL_INTERNAL_USERS = new Set<String>{
		'Multi_Factor_Authentication_Enabled'
	};

	private static final Map<String, Set<String>> PROFILE_TO_PERMISSION_SET_MAP = new Map<String, Set<String>>{
		'VF Account Manager' => VFZ_MAVENDOCS_USER,
		'VF Partner Portal User' => VFZ_MAVENDOCS_USER,
		'VF Retail manager ZS' => VFZ_MAVENDOCS_USER,
		'VF Retail ZS' => VFZ_MAVENDOCS_USER,
		'VF Solution Sales' => VFZ_MAVENDOCS_USER,
		'LG_NL D2D_Partner Manager' => VFZ_MAVENDOCS_USER,
		'LG_NL Sales Management User' => VFZ_MAVENDOCS_USER,
		'VF Telesales' => VFZ_MAVENDOCS_USER,
		'VF Sales Support' => VFZ_MAVENDOCS_USER
	};

	private static final Map<String, Set<String>> PROFILE_TO_PERMISSION_SET_GROUP_MAP = new Map<String, Set<String>>{
		'LG_NL D2D_Partner Sales User' => new Set<String>{ 'D2D_Partner_Users_Permissions' }
	};

	private Map<Id, UserWrapper> userMap = new Map<Id, UserWrapper>();
	private Map<String, PackageLicense> packageLicenseMap = new Map<String, PackageLicense>();
	private Map<String, PermissionSet> permissionSetMap = new Map<String, PermissionSet>();
	private Map<String, PermissionSetGroup> permissionSetGroupMap = new Map<String, PermissionSetGroup>();

	public UserService(Set<Id> userIds) {
		this.getUsers(userIds);
	}

	public UserService(Id userId) {
		this(new Set<Id>{ userId });
	}

	/**
	 * Gets all users based on their IDs and builds wrappers.
	 */
	private void getUsers(Set<Id> userIds) {
		Set<Id> newUserIds = new Set<Id>();
		for (Id userId : userIds) {
			if (!userMap.containsKey(userId)) {
				newUserIds.add(userId);
			}
		}
		if (newUserIds.isEmpty()) {
			return;
		}
		List<User> users = [
			SELECT
				Id,
				IsActive,
				Profile.Name,
				UserType,
				(
					SELECT
						Id,
						PermissionSetId,
						PermissionSet.Name,
						PermissionSetGroupId,
						PermissionSetGroup.DeveloperName
					FROM PermissionSetAssignments
				)
			FROM User
			WHERE Id IN :newUserIds
		];
		for (User u : users) {
			userMap.put(u.Id, new UserWrapper(u));
		}
	}

	/**
	 * Gets User Wrapper based on the User Id.
	 * @param  userId User Id.
	 * @return        User Wrapper.
	 */
	public UserWrapper getUser(Id userId) {
		if (!userMap.containsKey(userId)) {
			getUsers(new Set<Id>{ userId });
		}
		return userMap.get(userId);
	}

	/**
	 * Assigns new licenses based on the Profile.
	 * Unassignes licenses for inactive users.
	 */
	public void assignLicenses() {
		this.getUserPackageLicenses();
		this.getPackageLicenses();
		this.assignNewLicenses();
		this.unassignExistingLicenses();
	}

	/**
	 * Gets User Package Licenses and sets them for User Wrappers.
	 */
	private void getUserPackageLicenses() {
		List<UserPackageLicense> existingLicenses = [
			SELECT Id, UserId, PackageLicense.NamespacePrefix
			FROM UserPackageLicense
			WHERE UserId IN :userMap.keySet()
		];
		for (UserPackageLicense license : existingLicenses) {
			userMap.get(license.UserId).addExistingLicense(license);
		}
	}

	/**
	 * Gets Package Licenses based on user profiles.
	 */
	private void getPackageLicenses() {
		Set<String> packagePrefixes = new Set<String>();
		for (UserWrapper user : userMap.values()) {
			if (user.user.IsActive && PROFILE_TO_PACKAGE_MAP.containsKey(user.user.Profile.Name)) {
				packagePrefixes.addAll(PROFILE_TO_PACKAGE_MAP.get(user.user.Profile.Name));
			}
		}
		if (packagePrefixes.isEmpty()) {
			return;
		}
		List<PackageLicense> packageLicenses = [
			SELECT Id, NamespacePrefix
			FROM PackageLicense
			WHERE NamespacePrefix IN :packagePrefixes
		];
		for (PackageLicense license : packageLicenses) {
			packageLicenseMap.put(license.NamespacePrefix, license);
		}
	}

	/**
	 * Creates new User Package Licenses based on user profiles.
	 */
	private void assignNewLicenses() {
		List<UserPackageLicense> newLicenses = new List<UserPackageLicense>();
		for (UserWrapper user : userMap.values()) {
			if (user.user.IsActive && PROFILE_TO_PACKAGE_MAP.containsKey(user.user.Profile.Name)) {
				Set<String> namespacePrefixes = PROFILE_TO_PACKAGE_MAP.get(user.user.Profile.Name);
				for (String namespacePrefix : namespacePrefixes) {
					PackageLicense packageLicense = packageLicenseMap.get(namespacePrefix);
					user.addNewLicense(packageLicense);
				}
			}
			newLicenses.addAll(user.getNewLicenses());
		}
		if (!newLicenses.isEmpty()) {
			insert newLicenses;
		}
	}

	/**
	 * Deletes existing User Package Licenses for inactive users.
	 */
	private void unassignExistingLicenses() {
		List<UserPackageLicense> existingLicenses = new List<UserPackageLicense>();
		for (UserWrapper user : userMap.values()) {
			if (!user.user.IsActive && PROFILE_TO_PACKAGE_MAP.containsKey(user.user.Profile.Name)) {
				existingLicenses.addAll(user.getExistingLicenses());
			}
		}
		if (!existingLicenses.isEmpty()) {
			delete existingLicenses;
		}
	}

	/**
	 * Assigns permission sets based on the profile.
	 */
	public void assignPermissionSets() {
		System.debug('12e3jhfu8g');
		this.getUserPermissionSets();
		this.getPermissionSets();
		this.assignNewPermissionSets();
	}

	/**
	 * Gets User Permission Set Assignments and sets them for User Wrappers.
	 */
	private void getUserPermissionSets() {
		List<PermissionSetAssignment> existingAssignments = [
			SELECT Id, AssigneeId, PermissionSetId, PermissionSet.Name
			FROM PermissionSetAssignment
			WHERE PermissionSetId != NULL AND AssigneeId IN :userMap.keySet()
		];
		for (PermissionSetAssignment assignment : existingAssignments) {
			userMap.get(assignment.AssigneeId).addExistingPermissionSet(assignment);
			permissionSetMap.put(
				assignment.PermissionSet.Name,
				new PermissionSet(
					Id = assignment.PermissionSetId,
					Name = assignment.PermissionSet.Name
				)
			);
		}
	}

	/**
	 * Gets Permission Sets based on user profiles.
	 */
	private void getPermissionSets() {
		Set<String> permissionSetNames = new Set<String>();
		for (UserWrapper user : userMap.values()) {
			if (user.user.IsActive) {
				if (PROFILE_TO_PERMISSION_SET_MAP.containsKey(user.user.Profile.Name)) {
					permissionSetNames.addAll(
						PROFILE_TO_PERMISSION_SET_MAP.get(user.user.Profile.Name)
					);
				}

				if (!GeneralUtils.IsPartnerUser(user.user)) {
					permissionSetNames.addAll(PERMISSION_SETS_NAMES_FOR_ALL_INTERNAL_USERS);
				}
			}
		}
		if (permissionSetNames.isEmpty() || permissionSetNames.equals(permissionSetMap.keySet())) {
			return;
		}
		List<PermissionSet> permissionSets = [
			SELECT Id, Name
			FROM PermissionSet
			WHERE Name IN :permissionSetNames
		];
		for (PermissionSet permSet : permissionSets) {
			permissionSetMap.put(permSet.Name, permSet);
		}
	}

	/**
	 * Creates new Permission Set Assignments based on user profiles.
	 */
	private void assignNewPermissionSets() {
		List<PermissionSetAssignment> newAssignments = new List<PermissionSetAssignment>();
		for (UserWrapper user : userMap.values()) {
			if (user.user.IsActive) {
				Set<String> setNames = new Set<String>();

				if (PROFILE_TO_PERMISSION_SET_MAP.containsKey(user.user.Profile.Name)) {
					setNames.addAll(PROFILE_TO_PERMISSION_SET_MAP.get(user.user.Profile.Name));
				}

				if (!GeneralUtils.IsPartnerUser(user.user)) {
					setNames.addAll(PERMISSION_SETS_NAMES_FOR_ALL_INTERNAL_USERS);
				}

				for (String setName : setNames) {
					PermissionSet permSet = permissionSetMap.get(setName);
					user.addNewPermissionSet(permSet);
				}
				newAssignments.addAll(user.getNewPermissionSetAssignments());
			}
		}

		insert newAssignments;
	}

	/**
	 * Assigns permission set groups based on the profile.
	 */
	public void assignPermissionSetGroups() {
		System.debug('12e3jhfu8g');
		this.getUserPermissionSetGroups();
		this.getPermissionSetGroups();
		this.assignNewPermissionSetGroups();
	}

	/**
	 * Gets User Permission Set Assignments and sets them for User Wrappers.
	 */
	private void getUserPermissionSetGroups() {
		List<PermissionSetAssignment> existingAssignments = [
			SELECT Id, AssigneeId, PermissionSetGroupId, PermissionSetGroup.DeveloperName
			FROM PermissionSetAssignment
			WHERE PermissionSetGroupId != NULL AND AssigneeId IN :userMap.keySet()
		];
		for (PermissionSetAssignment assignment : existingAssignments) {
			userMap.get(assignment.AssigneeId).addExistingPermissionSetGroup(assignment);
		}
	}

	/**
	 * Gets Permission Set Groups based on user profiles.
	 */
	private void getPermissionSetGroups() {
		Set<String> permissionSetGroupNames = new Set<String>();
		for (UserWrapper user : userMap.values()) {
			if (
				user.user.IsActive &&
				PROFILE_TO_PERMISSION_SET_GROUP_MAP.containsKey(user.user.Profile.Name)
			) {
				permissionSetGroupNames.addAll(
					PROFILE_TO_PERMISSION_SET_GROUP_MAP.get(user.user.Profile.Name)
				);
			}
		}
		if (permissionSetGroupNames.isEmpty()) {
			return;
		}
		List<PermissionSetGroup> permissionSetGroups = [
			SELECT Id, DeveloperName
			FROM PermissionSetGroup
			WHERE DeveloperName IN :permissionSetGroupNames AND Status = 'Updated'
		];
		for (PermissionSetGroup permSetGroup : permissionSetGroups) {
			permissionSetGroupMap.put(permSetGroup.DeveloperName, permSetGroup);
		}
	}

	/**
	 * Creates new Permission Set Group Assignments based on user profiles.
	 */
	private void assignNewPermissionSetGroups() {
		List<PermissionSetAssignment> newAssignments = new List<PermissionSetAssignment>();
		for (UserWrapper user : userMap.values()) {
			if (
				user.user.IsActive &&
				PROFILE_TO_PERMISSION_SET_GROUP_MAP.containsKey(user.user.Profile.Name)
			) {
				Set<String> groupNames = PROFILE_TO_PERMISSION_SET_GROUP_MAP.get(
					user.user.Profile.Name
				);
				for (String groupName : groupNames) {
					PermissionSetGroup permSetGroup = permissionSetGroupMap.get(groupName);
					user.addNewPermissionSetGroup(permSetGroup);
				}
			}
			newAssignments.addAll(user.getNewPermissionSetGroupAssignments());
		}
		if (!newAssignments.isEmpty()) {
			insert newAssignments;
		}
	}

	public class UserWrapper {
		private User user;
		private Map<String, PermissionSetAssignment> permissionSets;
		private Map<String, PermissionSetAssignment> permissionSetGroups;
		private Map<String, UserPackageLicense> packageLicenses;

		public UserWrapper(User user) {
			this.user = user;
			this.permissionSets = new Map<String, PermissionSetAssignment>();
			this.permissionSetGroups = new Map<String, PermissionSetAssignment>();
			this.packageLicenses = new Map<String, UserPackageLicense>();
		}

		public Boolean hasLicense(String namespacePrefix) {
			return this.packageLicenses.containsKey(namespacePrefix);
		}

		public void addExistingLicense(UserPackageLicense license) {
			this.packageLicenses.put(license.PackageLicense.NamespacePrefix, license);
		}

		public void addNewLicense(PackageLicense packageLicense) {
			if (this.packageLicenses.containsKey(packageLicense.NamespacePrefix)) {
				return;
			}
			UserPackageLicense license = new UserPackageLicense(
				UserId = this.user.Id,
				PackageLicenseId = packageLicense.Id
			);
			this.packageLicenses.put(packageLicense.NamespacePrefix, license);
		}

		public List<UserPackageLicense> getNewLicenses() {
			List<UserPackageLicense> newLicenses = new List<UserPackageLicense>();
			for (UserPackageLicense license : this.packageLicenses.values()) {
				if (license.Id == null) {
					newLicenses.add(license);
				}
			}
			return newLicenses;
		}

		public List<UserPackageLicense> getExistingLicenses() {
			List<UserPackageLicense> existingLicenses = new List<UserPackageLicense>();
			for (UserPackageLicense license : this.packageLicenses.values()) {
				if (license.Id != null) {
					existingLicenses.add(license);
				}
			}
			return existingLicenses;
		}

		public Boolean hasPermissionSetAssigned(String permissionSetName) {
			return this.permissionSets.containsKey(permissionSetName);
		}

		public void addExistingPermissionSet(PermissionSetAssignment assignment) {
			if (assignment.PermissionSetId != null) {
				this.permissionSets.put(assignment.PermissionSet.Name, assignment);
			}
		}

		public void addNewPermissionSet(PermissionSet permSet) {
			if (!this.permissionSets.containsKey(permSet.Name)) {
				PermissionSetAssignment assignment = new PermissionSetAssignment(
					AssigneeId = this.user.Id,
					PermissionSetId = permSet.Id
				);
				this.permissionSets.put(permSet.Name, assignment);
			}
		}

		public List<PermissionSetAssignment> getNewPermissionSetAssignments() {
			List<PermissionSetAssignment> newAssignments = new List<PermissionSetAssignment>();
			for (PermissionSetAssignment assignment : this.permissionSets.values()) {
				if (assignment.Id == null) {
					newAssignments.add(assignment);
				}
			}
			return newAssignments;
		}

		public Boolean hasPermissionSetGroupAssigned(String permissionSetGroupName) {
			return this.permissionSetGroups.containsKey(permissionSetGroupName);
		}

		public void addExistingPermissionSetGroup(PermissionSetAssignment assignment) {
			if (assignment.PermissionSetGroupId != null) {
				this.permissionSetGroups.put(
					assignment.PermissionSetGroup.DeveloperName,
					assignment
				);
			}
		}

		public void addNewPermissionSetGroup(PermissionSetGroup permSetGroup) {
			if (permSetGroup == null) {
				return;
			}
			if (!this.permissionSetGroups.containsKey(permSetGroup.DeveloperName)) {
				PermissionSetAssignment assignment = new PermissionSetAssignment(
					AssigneeId = this.user.Id,
					PermissionSetGroupId = permSetGroup.Id
				);
				this.permissionSetGroups.put(permSetGroup.DeveloperName, assignment);
			}
		}

		public List<PermissionSetAssignment> getNewPermissionSetGroupAssignments() {
			List<PermissionSetAssignment> newAssignments = new List<PermissionSetAssignment>();
			for (PermissionSetAssignment assignment : this.permissionSetGroups.values()) {
				if (assignment.Id == null) {
					newAssignments.add(assignment);
				}
			}
			return newAssignments;
		}
	}
}