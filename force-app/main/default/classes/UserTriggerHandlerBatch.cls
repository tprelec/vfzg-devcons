@SuppressWarnings('PMD')
global class UserTriggerHandlerBatch implements Queueable {
	private Set<Id> mUsers;
	public UserTriggerHandlerBatch(Set<Id> userIds) {
		mUsers = userIds;
	}

	global void execute(QueueableContext context) {
		if (!mUsers.isEmpty()) {
			List<BigMachines__Oracle_User__c> usersToUpdate = new List<BigMachines__Oracle_User__c>();
			Map<Id, BigMachines__Oracle_User__c> usersWithBmUser = new Map<Id, BigMachines__Oracle_User__c>();
			for (BigMachines__Oracle_User__c bmUser : [
				SELECT BigMachines__Salesforce_User__c
				FROM BigMachines__Oracle_User__c
				WHERE BigMachines__Salesforce_User__c IN :mUsers
			]) {
				usersWithBmUser.put(bmUser.BigMachines__Salesforce_User__c, bmUser);
			}

			for (Id uId : mUsers) {
				if (usersWithBmUser.containsKey(uId)) {
					BigMachines__Oracle_User__c bmUsr = usersWithBmUser.get(uId);
					bmUsr.BigMachines__Bulk_Synchronization__c = false;
					bmUsr.BigMachines__Provisioned__c = false;
					usersToUpdate.add(bmUsr);
				}
			}
			if (usersToUpdate.size() > 0) {
				update usersToUpdate;
			}
		}
	}
}