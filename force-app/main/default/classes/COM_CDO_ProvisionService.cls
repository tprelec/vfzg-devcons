public with sharing class COM_CDO_ProvisionService {
	public static COM_MetadataInformation metadata;

	public static String provisionServices(Id deliveryOrderId) {
		List<csord__Service__c> servicesFromOrder = COM_CDOHelper.getServicesFromDeliveryOrder(deliveryOrderId);

		servicesFromOrder = COM_CDOHelper.retrievePartialMACDServices(servicesFromOrder);
		COM_CDO_ServiceOrder request = getRequest(deliveryOrderId, servicesFromOrder);

		RequestGenerateData requestData = new RequestGenerateData();
		requestData.deliveryOrderId = deliveryOrderId;
		requestData.request = request;
		requestData.servicesList = servicesFromOrder;

		String result = provisionService(requestData);
		return result;
	}

	private static String provisionService(RequestGenerateData requestData) {
		COM_Integration_setting__mdt createBIMOServiceSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.CDO_PROVISION_BIMO_SETTING_NAME
		);

		HTTPResponse response = sendProvisionServiceRequest(requestData, createBIMOServiceSetting);

		String result = '';

		if (failedResponse(response)) {
			ProvisionServiceSyncErrorResponse responseDeserialized = (ProvisionServiceSyncErrorResponse) JSON.deserialize(
				response.getBody(),
				ProvisionServiceSyncErrorResponse.class
			);

			COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
			updates.deliveryOrderId = requestData.deliveryOrderId;
			updates.status = COM_Constants.CDO_INTEGRATION_STATUS_FAILURE;
			updates.errorMessage = responseDeserialized.message;
			updates.success = false;

			COM_CDOHelper.performOrderUpdatesAfterResponse(updates, null);
		} else {
			String reqBody = COM_CDOServiceCharacteristicHelper.modifyRequestBody(response.getBody());

			result = reqBody;
			COM_CDO_ServiceOrder body = (COM_CDO_ServiceOrder) JSON.deserialize(reqBody, COM_CDO_ServiceOrder.class);

			COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
			updates.deliveryOrderId = requestData.deliveryOrderId;
			updates.status = '';
			updates.errorMessage = '';
			updates.success = true;
			COM_CDOHelper.performOrderUpdatesAfterResponse(updates, body);

			String umbrellaServiceId = COM_CDOHelper.getUmbrellaServiceId(body);
			COM_CDOHelper.updateServices(requestData.servicesList, umbrellaServiceId);
		}

		return result;
	}

	private static Boolean failedResponse(HTTPResponse response) {
		Boolean result = false;
		try {
			ProvisionServiceSyncErrorResponse responseDeserialized = (ProvisionServiceSyncErrorResponse) JSON.deserializeStrict(
				response.getBody(),
				ProvisionServiceSyncErrorResponse.class
			);
			if (responseDeserialized.code != null) {
				result = true;
			}
		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	private static HTTPResponse sendProvisionServiceRequest(RequestGenerateData requestData, COM_Integration_setting__mdt createBIMOServiceSetting) {
		Boolean doNotExecuteReq = createBIMOServiceSetting != null ? createBIMOServiceSetting.Execute_mock__c : true;
		Boolean doNotExecuteStatus = createBIMOServiceSetting != null ? createBIMOServiceSetting.Mock_Execution_result__c : true;
		Boolean generateAttachment = createBIMOServiceSetting != null ? createBIMOServiceSetting.Generate_Attachment__c : true;

		COM_CDO_ServiceOrder req = requestData.request;

		String namedCredentials = createBIMOServiceSetting.NamedCredential__c;

		HttpRequest reqData = new HttpRequest();
		Http http = new Http();

		String endpoint = COM_WebServiceConfig.getEndpoint(namedCredentials, COM_CDO_Constants.COM_WEB_PARAM);

		reqData.setHeader('Content-Type', 'application/json');
		reqData.setEndpoint(endpoint);

		String requestBody = generatePayloadJson(req);

		reqData.setBody(requestBody);
		reqData.setMethod('POST');
		HTTPResponse response = new HttpResponse();
		if (doNotExecuteReq && !Test.isRunningTest()) {
			response.setHeader('Content-Type', 'application/json');
			if (doNotExecuteStatus) {
				String mockSuccess = req.orderItem[0].action == COM_CDO_Constants.COM_CDO_ACTION_ADD
					? COM_CDOHelper.getMockResponse(
							COM_CDO_Constants.COM_CDO_MOCK_CREATE_BIMO_SYNC_SUCCESS,
							COM_CDO_Constants.CDO_PROVISION_BIMO_SETTING_NAME
					  )
					: COM_CDOHelper.getMockResponse(
							COM_CDO_Constants.COM_CDO_MOCK_MODIFY_BIMO_SYNC_SUCCESS,
							COM_CDO_Constants.CDO_PROVISION_BIMO_SETTING_NAME
					  );

				response.setBody(mockSuccess);
				response.setStatusCode(200);
			} else {
				String mockFailed = req.orderItem[0].action == COM_CDO_Constants.COM_CDO_ACTION_ADD
					? COM_CDOHelper.getMockResponse(
							COM_CDO_Constants.COM_CDO_MOCK_CREATE_BIMO_SYNC_FAILED,
							COM_CDO_Constants.CDO_PROVISION_BIMO_SETTING_NAME
					  )
					: COM_CDOHelper.getMockResponse(
							COM_CDO_Constants.COM_CDO_MOCK_MODIFY_BIMO_SYNC_FAILED,
							COM_CDO_Constants.CDO_PROVISION_BIMO_SETTING_NAME
					  );

				response.setBody(mockFailed);

				response.setStatusCode(400);
			}
		} else {
			response = http.send(reqData);
		}

		if (generateAttachment) {
			COM_CDOHelper.createPayloadAttachment(requestData.deliveryOrderId, 'CDO_Provision_payload', requestBody);
		}

		return response;
	}

	private static COM_CDO_ServiceOrder getRequest(Id deliveryOrderId, List<csord__Service__c> servicesFromOrder) {
		COM_CDO_integration_settings__mdt comSetting = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.CDO_PROVISION_BIMO_SETTING_NAME
		);

		COM_CDO_ServiceOrder request = new COM_CDO_ServiceOrder();

		request.externalId = comSetting.externalId__c;
		request.priority = comSetting.priority__c;
		request.description = comSetting.Description__c;
		request.category = comSetting.Category__c;
		request.typeVal = comSetting.requestType__c;
		request.orderItem = new List<COM_CDO_ServiceOrder.OrderItem>();

		Map<String, List<csord__Service__c>> servicesPerUmbrella = COM_CDOHelper.getAllUmbrellaServices(servicesFromOrder);

		Integer orderItemIndex = 0;
		for (String umbrellaServiceId : servicesPerUmbrella.keySet()) {
			orderItemIndex++;
			COM_CDO_ServiceOrder.OrderItem orderitem = new COM_CDO_ServiceOrder.OrderItem();
			orderItem.id = String.valueOf(orderItemIndex);
			orderItem.action = umbrellaServiceId == COM_CDO_Constants.COM_CDO_NEW_CONS
				? COM_CDO_Constants.COM_CDO_ACTION_ADD
				: COM_CDO_Constants.COM_CDO_ACTION_MODIFY;
			orderItem.action = umbrellaServiceId == COM_CDO_Constants.COM_CDO_NEW_CONS
				? COM_CDO_Constants.COM_CDO_ACTION_ADD
				: COM_CDO_Constants.COM_CDO_ACTION_MODIFY;
			orderItem.typeVal = COM_CDO_Constants.COM_CDO_STANDARD_CONS;

			COM_CDO_ServiceOrder.Service service = new COM_CDO_ServiceOrder.Service();

			COM_CDO_ServiceOrder.ServiceSpecification specification = new COM_CDO_ServiceOrder.ServiceSpecification();
			specification.id = comSetting.serviceSpecificationId__c;
			specification.invariantID = comSetting.serviceSpecification_invariantID__c;
			specification.version = comSetting.serviceSpecification_version__c;
			specification.name = comSetting.serviceSpecification_name__c;
			specification.typeVal = comSetting.serviceSpecificationType__c;
			service.serviceSpecification = specification;

			service.id = umbrellaServiceId == COM_CDO_Constants.COM_CDO_NEW_CONS ? null : umbrellaServiceId;

			service.typeVal = comSetting.serviceType__c;
			service.name = comSetting.serviceName__c;
			service.serviceCharacteristic = new List<COM_CDO_ServiceOrder.ServiceCharacteristic>();
			service.serviceCharacteristic.addAll(
				COM_CDOServiceCharacteristicHelper.getServiceCharacteristics(servicesPerUmbrella.get(umbrellaServiceId))
			);

			orderItem.service = service;
			request.orderItem.add(orderItem);
		}

		return request;
	}

	private static String generatePayloadJson(COM_CDO_ServiceOrder req) {
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeStringField(COM_CDO_Constants.COM_CDO_EXTERNALID, req.externalId);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_PRIORITY, req.priority);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_DESCRIPTION, req.description);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_CATEGORY, req.category);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, req.typeVal);
		gen.writeFieldName(COM_CDO_Constants.COM_CDO_ORDER_ITEM);
		gen.writeStartArray();
		for (COM_CDO_ServiceOrder.OrderItem orderItem : req.orderItem) {
			gen.writeStartObject();

			gen.writeStringField(COM_CDO_Constants.COM_CDO_ID, orderItem.id);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_ACTION, orderItem.action);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, orderItem.typeVal);

			gen.writeFieldName(COM_CDO_Constants.COM_CDO_SERVICE);
			gen.writeStartObject();
			if (orderItem.service.id != null) {
				gen.writeStringField(COM_CDO_Constants.COM_CDO_ID, orderItem.service.id);
			}
			gen.writeStringField(COM_CDO_Constants.COM_CDO_NAME, orderItem.service.name);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, orderItem.service.typeVal);

			gen.writeFieldName(COM_CDO_Constants.COM_CDO_SERVICE_SPECIFICATION);
			gen.writeStartObject();
			gen.writeStringField(COM_CDO_Constants.COM_CDO_ID, orderItem.service.serviceSpecification.id);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_INVARIANTID, orderItem.service.serviceSpecification.invariantID);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_VERSION, orderItem.service.serviceSpecification.version);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_NAME, orderItem.service.serviceSpecification.name);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, orderItem.service.serviceSpecification.typeVal);
			gen.writeEndObject();

			gen.writeFieldName(COM_CDO_Constants.COM_CDO_SERVICE_CHARACTERISTICS);
			gen.writeStartArray();
			for (COM_CDO_ServiceOrder.ServiceCharacteristic serviceCharacteristic : orderItem.service.serviceCharacteristic) {
				gen.writeStartObject();
				gen.writeStringField(COM_CDO_Constants.COM_CDO_NAME, serviceCharacteristic.name);
				gen.writeStringField(COM_CDO_Constants.COM_CDO_VALUE_TYPE, serviceCharacteristic.valueType);

				gen.writeFieldName(COM_CDO_Constants.COM_CDO_VALUE);
				gen.writeStartObject();
				gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, serviceCharacteristic.value.typeVal);
				String value = serviceCharacteristic.value.valueOf != null ? serviceCharacteristic.value.valueOf : '';
				gen.writeStringField(serviceCharacteristic.name, value);
				gen.writeEndObject();

				gen.writeEndObject();
			}
			gen.writeEndArray();
			gen.writeEndObject();
			gen.writeEndObject();
		}

		gen.writeEndArray();
		gen.writeEndObject();

		return gen.getAsString();
	}

	private class ProvisionServiceSyncErrorResponse {
		private Integer code;
		private String reason;
		private String message;
	}

	private class RequestGenerateData {
		private COM_CDO_ServiceOrder request;
		private Id deliveryOrderId;
		private List<csord__Service__c> servicesList;
	}
}
