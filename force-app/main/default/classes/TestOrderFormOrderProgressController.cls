/**
 * Test class for OrderFormOrderProgressController.cls
 * 
 * @author: Rahul Sharma
 */
 @IsTest
private class TestOrderFormOrderProgressController {
    
    @IsTest
    static void testProgressPage_ValidateContractedProducts() {

        Order__c order = createOrderWithContractedProducts(4);

        // start testing
        Test.startTest();

        OrderFormOrderProgressController ctlr = new OrderFormOrderProgressController();
        ctlr = ctlr.getApexController();
        String contractId = ctlr.contractId;

        // set order id in current page
        ApexPages.currentPage().getParameters().put('orderId', order.Id);

        // call the page
        OrderFormOrderProgressController progressController = new OrderFormOrderProgressController();

        // verify that order id is passed to controller
        System.assertNotEquals(null, progressController.orderId);
        System.assertNotEquals('', progressController.orderId);

        System.assertEquals(100, progressController.getProgressValue());

        progressController.refresh();
        progressController.refreshOrder();

        progressController.order.Export_System_CustomerData__c = 'BOP';
        Integer progressVal = progressController.getProgressValue();
        System.assertEquals(progressVal, 100);
        progressController.order.Status__c = 'Validation';
        progressController.refresh();
        Test.stopTest();
    }

    public static Order__c createOrderWithContractedProducts(Integer totalContractedProducts) {
        TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
        TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
        TestUtils.createOrderValidationOrder();
        TestUtils.createOrderValidationContractedProducts();
        TestUtils.createOrderValidationSite();
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Ban__c ban = TestUtils.createBan(acct);
        Site__c site = TestUtils.createSite(acct);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
        OrderType__c ot = TestUtils.createOrderType();
        System.assertNotEquals(null, ot.Id);

        TestUtils.autoCommit = false;
        Product2 product = TestUtils.createProduct(new Map<String, Object> {
            'Billing_Type__c' => Constants.PRODUCT2_BILLINGTYPE_STANDARD
        });
        insert product;
        System.assertNotEquals(null, product.Id);

        // create an order with new status
        Order__c order = new Order__c(
            Status__c = 'New',
            Propositions__c = 'Legacy',
            OrderType__c = ot.Id,
            VF_Contract__c = contr.Id,
            Number_of_items__c = 100,
            O2C_Order__c = true,
            Sales_Signoff_Datetime__c = System.now(),
            SAG_Signoff_Datetime__c = System.now(),
            BOP_export_datetime__c = System.now(),
            Unify_Customer_export_datetime__c = System.now(),
            Unify_Order_export_datetime__c = System.now(),
            Export__c = 'BOP',
            Export_System_CustomerData__c = 'BOP'
        );
        insert order;
        System.assertNotEquals(null, order.Id);

        List<Order__c> orders = [SELECT Id, Status__c FROM Order__c LIMIT 2];
        System.assertEquals(1, orders.size());

        List<Contracted_Products__c> contractedProducts = new List<Contracted_Products__c>();
        // add contracted products
        for(Integer count = 0; count < 4; count++) {
            contractedProducts.add(new Contracted_Products__c (
                Quantity__c = 1,
                Arpu_Value__c = 100,
                Duration__c = 1,
                Product__c = product.Id,
                VF_Contract__c = contr.Id,
                Site__c = site.Id,
                Order__c = order.Id,
                CLC__c = Constants.CONTRACTED_PRODUCT_CLC_ACQ
            ));
        }
        insert contractedProducts;

        for(Contracted_Products__c cp: contractedProducts) {
            System.assertNotEquals(null, cp.Id);
        }

        return order;
    }
}