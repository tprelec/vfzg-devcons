@isTest
public with sharing class TestOrderEntryController {
	@TestSetup
	static void makeData() {
		TestUtils.theAdministrator = new User(Id = UserInfo.getUserId());
		MavenDocumentsTestFactory.createMavenDocumentsRecords();

		OrderEntryTestDataFactory.insertMockJsonData(true);
		List<OE_Add_On__c> addOns = OrderEntryTestDataFactory.createOrderEntryAddOns();
		List<OE_Product__c> oeProducts = OrderEntryTestDataFactory.createOrderEntryProducts();
		OrderEntryTestDataFactory.createPromotions();
		Id mainProductId;
		Id internetProductId;
		Id tvProductId;
		for (OE_Product__c prod : oeProducts) {
			if (prod.Type__c == 'Main') {
				mainProductId = prod.Id;
			} else if (prod.Type__c == 'Internet') {
				internetProductId = prod.Id;
			} else if (prod.Type__c == 'TV') {
				tvProductId = prod.Id;
			}
		}

		OrderEntryTestDataFactory.createOrderEntryProductAddOn(internetProductId, addOns[0].Id);
		OrderEntryTestDataFactory.createOEProductBundle(mainProductId, internetProductId, tvProductId);
	}

	@isTest
	static void testGetOpportunityData() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		Test.startTest();
		OrderEntryData oed = OrderEntryController.getOrderEntryData(opp.Id);
		Test.stopTest();

		List<Attachment> attPost = [SELECT Id FROM Attachment WHERE ParentId = :opp.Id AND Name = 'OrderEntryData.json'];
		System.assertEquals(oed.opportunityId, opp.Id, 'Wrong opportunity Id.');
		System.assertEquals(oed.accountId, opp.AccountId, 'Wrong account Id.');
		System.assert(attPost.size() > 0, 'OrderEntryData attachment should be generated.');
	}

	@IsTest
	static void testGetHeaderInfo() {
		Opportunity opp = [SELECT Id, Account.Name FROM Opportunity LIMIT 1];

		Test.startTest();
		String headerInfo = OrderEntryController.getHeaderInfo(opp.Id);
		Test.stopTest();

		System.assertEquals(opp.Account.Name + '  •   ', headerInfo, 'Wrong header info format.');
	}

	@isTest
	static void testUpdateState() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		Test.startTest();
		insert new Attachment(Name = 'OrderEntryData.json', Body = Blob.valueOf('Blobby'), ParentId = opp.Id);

		Boolean updated = OrderEntryController.updateState(opp.Id, 'New State');
		Test.stopTest();

		List<Attachment> attPost = [SELECT Id, Body FROM Attachment WHERE ParentId = :opp.Id AND Name = 'OrderEntryData.json'];

		System.assert(attPost[0].Body == Blob.valueOf('New State'), 'OrderEntryData attachment body not as expected.');
		System.assert(updated, 'Attachment should be updated.');
	}

	@isTest
	static void updateOpportunityStage() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		OrderEntryController.updateOpportunityStage(opp.Id, 'Customer Approved');
		Test.stopTest();
		Opportunity oppResult = [SELECT Id, StageName FROM Opportunity WHERE Id = :opp.Id];
		System.assertEquals('Customer Approved', oppResult.StageName, 'Opportunity should be updated.');
	}

	@isTest
	static void testCloneOrder() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

		OrderEntryData data = new OrderEntryData();
		data.opportunityId = opp.Id;
		data.lastStep = 'Select Product';
		data.init();

		Attachment att = new Attachment(Name = 'OrderEntryData.json', Body = Blob.valueOf(JSON.serializePretty(data)), ParentId = opp.Id);

		insert att;

		Test.startTest();
		Opportunity newOpp = OrderEntryController.cloneOrder(opp.Id, 'Test Clone');
		Test.stopTest();
		System.assertEquals('Awareness of interest', newOpp.StageName, 'Opportunity is inserted.');
		System.assertEquals(Date.today(), newOpp.CloseDate, 'Opportunity is inserted. ');
	}

	@isTest
	static void getOpportunity() {
		Opportunity opp = [SELECT Id, Name, StageName FROM Opportunity LIMIT 1];

		List<String> oppField = new List<String>();
		oppField.add('Name');
		oppField.add('StageName');
		Test.startTest();
		Opportunity result = OrderEntryController.getOpportunity(opp.Id, oppField);
		Test.stopTest();
		System.assertEquals(opp.Name, result.Name, 'Name is same');
		System.assertEquals(opp.StageName, result.StageName, 'StageName is same');
	}

	@IsTest
	static void testNewOrder() {
		Opportunity opp = [SELECT Id, Name, StageName FROM Opportunity LIMIT 1];

		List<Attachment> att = [SELECT Id, Body FROM Attachment WHERE ParentId = :opp.Id AND Name = 'OrderEntryData.json'];

		OrderEntryData oldData = new OrderEntryData();

		oldData = (OrderEntryData) JSON.deserialize(att[0].Body.toString(), OrderEntryData.class);

		OrderEntryData data = new OrderEntryData();

		Test.startTest();

		Opportunity newOpp = OrderEntryController.newOrder(opp.Id);
		Test.stopTest();

		att = [SELECT Id, Body FROM Attachment WHERE ParentId = :newOpp.Id AND Name = 'OrderEntryData.json'];

		data = (OrderEntryData) JSON.deserialize(att[0].Body.toString(), OrderEntryData.class);

		System.assertEquals(data.accountId, oldData.accountId, 'Attachment is created corect.');
		System.assertEquals(data.primaryContactId, oldData.primaryContactId, 'Attachment is created corect.');
	}
}
