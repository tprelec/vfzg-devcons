@isTest
public class CS_DiscountJsonElementTest {
    private static testMethod void callConstructorWithArguments() {
        // CS_DiscountCustomData testCustomData = new CS_DiscountCustomData('CustomDataString');
        Map<String, String> testCustomData = new Map<String, String>();
        testCustomData.put('test', 'test');
        CS_DiscountJsonElement testObject = new CS_DiscountJsonElement(0.0, 'type', 'source', 'discountCharge', 'description', testCustomData);
        System.assertEquals(testCustomData, testObject.customData);
    }
}