/**
 * @description       : provides use case and code coverage for the logic implemented on AccountQueueUserSharingBatch apex job
 * @author            : mcubias
 * @group             : leads united
 * @last modified on  : 07-09-2022
 * @last modified by  :
 **/
@IsTest
public class TestAccountQueueUserSharingBatch {
	@TestSetup
	static void initData() {
		Map<String, String> mapDefaults = new Map<String, String>{
			'NL_Default_Zoom_Level' => '1',
			'NL_Default_Latitude' => '52.16812913256766',
			'NL_Default_Longitude' => '5.059651772744132'
		};
		TestUtils.createMapsSetting(mapDefaults);
	}

	@IsTest
	static void assignAccountOwnerToManagerWhenAgentIsDeactivated() {
		Profile profile = [SELECT Id FROM Profile WHERE Name = 'LG_NL D2D_Partner Sales User'];

		Account partnerAccount;
		Account subPartnerAccount;
		Contact portalContact;
		User testPortalUser;

		Map<String, User> adminsByNumber = TestUtils.createAdmins(2);

		System.runAs(adminsByNumber.get('Admin1')) {
			TestUtils.autoCommit = false;
			partnerAccount = TestUtils.createPartnerAccount();
			insert partnerAccount;
			subPartnerAccount = TestUtils.createPartnerAccount();
			subPartnerAccount.Name = 'SecondParterAccount123';
			subPartnerAccount.Dealer_code__c = '888880';
			subPartnerAccount.BigMachines__Partner_Organization__c = '233112';
			subPartnerAccount.BOPCode__c = 'ACM';
			insert subPartnerAccount;
			portalContact = TestUtils.createContact(partnerAccount);
			insert portalContact;
		}

		System.runAs(adminsByNumber.get('Admin2')) {
			TestUtils.autoCommit = false;
			testPortalUser = TestUtils.createPortalUser(partnerAccount);
			testPortalUser.ContactId = portalContact.Id;
			testPortalUser.ProfileId = profile.Id;
			insert testPortalUser;
		}

		System.runAs(adminsByNumber.get('Admin1')) {
			//	Create Queue Sharing object for Sales Agent
			Queue_User_Sharing__c queueSharing = new Queue_User_Sharing__c(OwnerId = testPortalUser.Id);
			insert queueSharing;
			//	Assign Sales agent as owner of subPartner Account
			subPartnerAccount.Queue_User_Assigned__c = queueSharing.Id;
			update subPartnerAccount;
		}

		Map<Id, Id> managerIdByUser = new Map<Id, Id>();
		//	populate map to change ownership from testPortalUser to his/her manager
		managerIdByUser.put(testPortalUser.Id, adminsByNumber.get('Admin1').Id);
		Test.startTest();
		System.runAs(adminsByNumber.get('Admin1')) {
			QueueUserSharingService.ownersIdsByQueueUser = null;
			System.enqueueJob(new AccountQueueUserSharingBatch(managerIdByUser));
		}
		Test.stopTest();
		Account subPartnerAccountUpdated = [SELECT Id, Queue_User_Assigned__r.OwnerId FROM Account WHERE Id = :subPartnerAccount.Id];
		System.assertEquals(
			adminsByNumber.get('Admin1').Id,
			subPartnerAccountUpdated.Queue_User_Assigned__r.OwnerId,
			'Queue User Owner was not updated to User\'s Manager'
		);
	}
}
