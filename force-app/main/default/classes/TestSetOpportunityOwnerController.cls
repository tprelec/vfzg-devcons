@isTest
private class TestSetOpportunityOwnerController {
	@isTest
	static void test_method_one() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		User owner = TestUtils.createAdministrator();
		User u1 = TestUtils.createManager();
		Account acct = TestUtils.createAccount(owner);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());

		//Need to create dealer and put onto account as general and fixed owner
		Account acct1 = TestUtils.createPartnerAccount('ABC');
		Contact cont1 = TestUtils.createContact(acct1);

		cont1.Userid__c = u1.Id;
		update cont1;
		System.Test.setMock(WebServiceMock.class, new MetadataServiceTest.WebServiceMockImpl());
		Dealer_Information__c di1 = TestUtils.createDealerInformation(cont1.Id, '999999');
		acct.Fixed_Dealer__c = di1.id;
		acct.Mobile_Dealer__c = di1.id;
		update acct;

		test.startTest();

		Apexpages.StandardController sc = new Apexpages.standardController(opp);
		SetOpportunityOwnerController controller = new SetOpportunityOwnerController(sc);

		controller.getEnableSetGeneralOwner();
		controller.getEnableSetFixedOwner();
		controller.setGeneralOwner();
		controller.setFixedOwner();
		controller.putSAGonOpportunityTeam();

		test.stopTest();
	}
}
