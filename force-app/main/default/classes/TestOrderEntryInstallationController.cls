@IsTest
public with sharing class TestOrderEntryInstallationController {
	@TestSetup
	static void makeData() {
		OrderEntryTestDataFactory.createOpportunityWithAllRelatedRecords();
	}

	@IsTest
	public static void testSaveOpportunityDescription() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

		Test.startTest();
		Opportunity result = OrderEntryInstallationController.saveOpportunityDescription(opp.Id, 'Test Notes');
		Test.stopTest();
		Opportunity oppResult = [SELECT Id, Description FROM Opportunity WHERE Id = :opp.Id];
		System.assertEquals('Test Notes', oppResult.Description, 'Description should be updated. ');
	}

	@IsTest
	public static void testUpdateOpportunityOverstapService() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

		Test.startTest();

		OrderEntryInstallationController.updateOpportunityOverstapService(opp.Id, true, 'Tele2', '1111111111');

		Test.stopTest();

		opp = [SELECT Id, Operator_Switch__c, Provider__c, Current_Provider_Contract_Number__c FROM Opportunity LIMIT 1];

		System.assertEquals(true, opp.Operator_Switch__c, 'Switch operator is selected.');
		System.assertEquals('Tele2', opp.Provider__c, 'Provider is Tele2.');
		System.assertEquals('1111111111', opp.Current_Provider_Contract_Number__c, 'Contract Number is 1111111111.');
	}
}
