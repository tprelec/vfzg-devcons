@IsTest
public with sharing class TestConvertLeadsService {
	@TestSetup
	static void makeData() {
		TestUtils.autoCommit = false;
		Lead l = TestUtils.createLead();
		l.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName('Lead', 'CSADetails');
		l.KVK_number__c = '17047512';
		l.Street__c = 'Leistraat';
		l.Visiting_Housenumber1__c = 12;
		l.Visiting_postalcode_merged_input__c = 'RE3572';
		l.Town__c = 'Utrecht';
		insert l;

		List<Account> accForInsert = new List<Account>();
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.KVK_number__c = '17047512';
		Account acc1 = TestUtils.createAccount(GeneralUtils.currentUser);
		acc1.KVK_number__c = '87654321';
		accForInsert.add(acc);
		accForInsert.add(acc1);
		insert accForInsert;

		Contact con = TestUtils.createContact(acc1);
		con.Email = 'test@test.com';
		insert con;

		List<OlbicoSettings__c> olbicoSettings = new List<OlbicoSettings__c>();
		olbicoSettings.add(
			new OlbicoSettings__c(
				Olbico_JSon_Endpoint__c = 'https://api.dqbox.com/api/search',
				Olbico_Json_Username__c = 'usr_vod2325sitf',
				Olbico_Json_Password__c = 'testpass',
				Olbico_Json_BAG_Streets__c = 'ef62f95b-30fc-4520-9be1-8af222e57ef6'
			)
		);
		insert olbicoSettings;

		List<BAG_Mapping__c> bagmappingList = new List<BAG_Mapping__c>();
		bagmappingList.add(new BAG_Mapping__c(Account_Field__c = 'Name', Name = 'bedrijfsnaam'));
		bagmappingList.add(new BAG_Mapping__c(Account_Field__c = 'NumberOfEmployees', Name = 'aantalmedewerkers'));
		bagmappingList.add(new BAG_Mapping__c(Account_Field__c = 'KVK_number__c', Name = 'kvk_8'));
		bagmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'FirstName', Name = 'bevoegd_functionaris_voornaam'));
		bagmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'LastName', Name = 'bevoegd_functionaris_achternaam'));
		bagmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'MobilePhone', Name = 'mobielnummer'));
		insert bagmappingList;
	}

	@IsTest
	static void testLeadConvert() {
		Lead l = getLead();

		Test.startTest();
		ConvertLeadsService cls = new ConvertLeadsService(new List<Lead>{ l });
		cls.convertLeads('process');

		Test.stopTest();

		l = getLead();
		System.assert(l.IsConverted, 'Lead should be converted');

		Account acc = getAccount(l.KVK_number__c);
		System.assertEquals(l.ConvertedAccountId, acc.Id, 'Lead should be converted to the Account with same KVK Number');
	}

	@IsTest
	static void testLeadConversionUsingOlbicoService() {
		Lead l = getLead();
		l.KVK_number__c = '11223344';
		update l;

		Account acc;
		try {
			acc = getAccount(l.KVK_number__c);
		} catch (Exception e) {
			System.debug(LoggingLevel.debug, 'No accounts found');
		}
		System.assertEquals(null, acc, 'Account is not returned!');

		Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout());
		Test.startTest();

		l.KVK_number__c = '87654321';
		update l;
		ConvertLeadsService cls = new ConvertLeadsService(new List<Lead>{ l });
		cls.convertLeads('Ziggo');

		Test.stopTest();

		l = getLead();
		System.assert(l.IsConverted, 'Lead should be converted');

		try {
			acc = getAccount(l.KVK_number__c);
		} catch (Exception e) {
			System.debug(LoggingLevel.debug, 'Account not found after lead conversion');
		}

		System.assertEquals(l.KVK_number__c, acc.KVK_number__c, 'Account and Lead have the same KVK!');
	}

	@IsTest
	static void testZiggoLeadConvert() {
		Lead l = getLead();
		l.KVK_number__c = '87654321';
		l.Email = 'test@test.com';
		update l;

		Test.startTest();
		ConvertLeadsService cls = new ConvertLeadsService(new List<Lead>{ l });
		cls.convertLeads('Ziggo');

		Test.stopTest();

		l = getLead();
		System.assert(l.IsConverted, 'Lead should be converted');

		Account acc = getAccount(l.KVK_number__c);
		System.assertEquals(l.ConvertedAccountId, acc.Id, 'Lead should be converted to the Account with same KVK Number');

		Contact con = getContact(acc.Id, l.Email);
		System.assertEquals(l.ConvertedContactId, con.Id, 'Lead should be converted to the Account with same Email');

		Site__c site = getSite(acc.Id, l.Visiting_postalcode_merged_input__c);
		System.assertEquals(
			l.Visiting_postalcode_merged_input__c,
			site.Site_Postal_Code__c,
			'Lead should be converted to the Site with same Postcode'
		);

		Opportunity opp = getOpportunity(l.ConvertedOpportunityId);
		System.assertEquals(opp.LG_PrimaryContact__c, con.Id, 'Opportunity should be created');
		System.assertEquals(getAttachment(l.ConvertedOpportunityId).Name, 'OrderEntryData.json', 'Attachment should be created');

		OpportunityContactRole contactRole = getOpportunityContactRole(opp.Id);
		System.assertEquals(
			contactRole.Role,
			'Administrative Contact',
			'Opportunity Contact Role should exist and its role should be Administrative Contact'
		);
	}

	static Lead getLead() {
		return [
			SELECT
				Id,
				RecordTypeId,
				Email,
				KVK_Number__c,
				LG_VisitStreet__c,
				LG_VisitHouseNumber__c,
				LG_VisitHouseNumberExtension__c,
				Visiting_postalcode_merged_input__c,
				LG_VisitCity__c,
				Visiting_Country__c,
				LG_InternetCustomer__c,
				IsConverted,
				ConvertedAccountId,
				ConvertedContactId,
				ConvertedOpportunityId
			FROM Lead
			LIMIT 1
		];
	}

	static Account getAccount(String kvkNumber) {
		return [SELECT Id, KVK_Number__c FROM Account WHERE KVK_Number__c = :kvkNumber];
	}

	static Contact getContact(Id accountId, String email) {
		return [SELECT Id FROM Contact WHERE AccountId = :accountId AND Email = :email];
	}

	static Site__c getSite(Id accountId, String postcode) {
		return [SELECT Id, Site_Postal_Code__c FROM Site__c WHERE Site_Account__c = :accountId AND Site_Postal_Code__c = :postcode];
	}

	static Opportunity getOpportunity(Id oppId) {
		return [SELECT Id, LG_PrimaryContact__c FROM Opportunity WHERE Id = :oppId];
	}

	static Attachment getAttachment(Id oppId) {
		return [SELECT Id, ParentId, Name FROM Attachment WHERE ParentId = :oppId AND Name = 'OrderEntryData.json'];
	}

	static OpportunityContactRole getOpportunityContactRole(Id oppId) {
		return [SELECT Id, OpportunityId, ContactId, Role FROM OpportunityContactRole WHERE OpportunityId = :oppId];
	}
}
