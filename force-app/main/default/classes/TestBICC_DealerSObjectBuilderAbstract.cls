/**
 * @description:    Test class for BICC_DealerSObjectBuilderAbstract
 **/
@isTest
public class TestBICC_DealerSObjectBuilderAbstract {
	private static final Id TEST_DEALER_INFORMATION_ID = TestUtils.getFakeId(Dealer_Information__c.sObjectType);
	private static final String TEST_DEALER_INFORMATION_NAME = 'TestBICC_DealerSObjectBuilderAbstract';
	/**
	 * @description:    Tests calling the builder and returning data built by
	 *                  builder when extension is called
	 **/
	@isTest
	static void shouldCallBuilderAndReturnDataBuiltByBuilderWhenExtensionIsCalled() {
		// prepare data

		// perform testing
		Test.startTest();
		Map<Id, sObject> generatedRecordPerBICCSobjectId = new TestBICC_DealerSObjectBuilderAbstractExtension()
			.withSobjectFieldsPerFieldName(new Map<String, Schema.SObjectField>())
			.withBICCSObjectRecords(new List<BICC_Dealer_Information__c>())
			.withBICCFieldSyncMappingForDestinationField(new Map<String, Field_Sync_Mapping__c>())
			.build()
			.getRecordsPerId();
		Test.stopTest();

		// verify results
		List<Id> generatedRecordIds = new List<Id>(generatedRecordPerBICCSobjectId.keySet());
		Dealer_Information__c generatedDealerRecord = (Dealer_Information__c) generatedRecordPerBICCSobjectId.get(generatedRecordIds[0]);

		System.assertEquals(1, generatedRecordPerBICCSobjectId.size(), '1 SObject record should be returned');
		System.assertEquals(TEST_DEALER_INFORMATION_ID, generatedRecordIds[0], 'Proper dealer information ID should be returned in keyset');
		System.assertEquals(
			TEST_DEALER_INFORMATION_NAME,
			generatedDealerRecord.Name,
			'Proper dealer information name should be returned from record'
		);
	}

	/**
	 * @description:    Test class implementing BICC_DealerSObjectBuilderAbstract
	 **/
	private inherited sharing class TestBICC_DealerSObjectBuilderAbstractExtension extends BICC_DealerSObjectBuilderAbstract {
		/**
		 * @description:    Executes logic to build SObject
		 * @return          BICC_DealerSObjectBuilderAbstract - instance of this
		 **/
		public override BICC_DealerSObjectBuilderAbstract build() {
			this.generatedRecordPerBICCSobjectId = new Map<Id, sObject>{
				TEST_DEALER_INFORMATION_ID => new Dealer_Information__c(Name = TEST_DEALER_INFORMATION_NAME)
			};

			return this;
		}
	}
}
