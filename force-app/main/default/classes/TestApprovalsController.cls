@isTest
global class TestApprovalsController{

    static testMethod void testMyApprovals() {
        ApprovalsController.runningInTestMode = true;
        ApprovalsController c= new ApprovalsController();        

        ApprovalsController.getPendingApprovals();
    }

    static testMethod void testMyDelegatedApprovals() {
        ApprovalsController.runningInTestMode = true;
        ApprovalsController.showDelegated = true;

        ApprovalsController c= new ApprovalsController();        

        
        c.approveRejectSelected();
        c.cancel();
        c.getPendingDelegatedApprovals();
        c.getSelectedApprovals();
        c.manageAll();
        c.massApprove();
        c.massReassign();
        c.massReject();
        c.reassignSelected();
    }    
}