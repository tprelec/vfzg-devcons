@IsTest
public with sharing class TestOrderEntryAccountController {
	@TestSetup
	static void makeData() {
		TestUtils.createCompleteOpportunity();
	}

	@IsTest
	static void testGetAccountData() {
		Opportunity opp = [SELECT Id, Account.Id, Account.Name FROM Opportunity LIMIT 1];

		Test.startTest();
		Account acc = OrderEntryAccountController.getAccountData(opp.Account.Id);
		Test.stopTest();

		System.assertEquals(opp.Account.Name, acc.Name, 'Wrong acc name.');
	}

	@IsTest
	static void testSaveAccountData() {
		Opportunity opp = [SELECT Id, Account.Id, Account.Name FROM Opportunity LIMIT 1];

		Account acc = [SELECT Id, Name FROM Account WHERE Id = :opp.AccountId LIMIT 1];

		acc.Name = 'NewName';

		Test.startTest();
		Account returned = OrderEntryAccountController.saveAccountData(acc);
		Test.stopTest();

		System.assertEquals('NewName', returned.Name, 'Account is updated.');
	}
}
