/**
 * @description       : Apex Class Handler of the Order Validation Trigger
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   20-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

public class OrderValidationTriggerHandler extends TriggerHandler {
	private List<Order_Validation__c> lstOrderValidation;

	private Map<Id, Order_Validation__c> mapOldOrderValidation;
	private Map<Id, Order_Validation__c> mapNewOrderValidation;

	/**
	 * @description Initialize Class Variables
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	private void init() {
		lstOrderValidation = (List<Order_Validation__c>) this.newList;
		mapOldOrderValidation = (Map<Id, Order_Validation__c>) this.oldMap;
		mapNewOrderValidation = (Map<Id, Order_Validation__c>) this.newMap;
	}

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public void setExternalIds() {
		Sequence objSequence = new Sequence('Order Validation');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (Order_Validation__c objOrderValidation : lstOrderValidation) {
				objOrderValidation.ExternalID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
