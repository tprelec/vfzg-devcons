/**
 * @description: This class is responsible for invoking the ChangePBXOrder REST service
 */
public with sharing class SiasService {
    @TestVisible
    private static final String INTEGRATION_SETTING_NAME = 'SIASRESTChangePBXOrder';

    @SuppressWarnings(
        'PMD.ApexSuggestUsingNamedCred'
    ) // Named credentials would not be possible because of way authentication works, SF doesn't support
    public static void changeOrderPBX(String transactionId, String contProdId) {
        // Retrieve the credentials from the custom setting
        External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(
            INTEGRATION_SETTING_NAME
        );

        String endpointURL = webServiceConfig.URL__c;
        String authorizationHeaderString =
            webServiceConfig.Username__c +
            ':' +
            webServiceConfig.Password__c;
        String authorizationHeader =
            'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(authorizationHeaderString));

        HttpRequest reqData = new HttpRequest();
        Http http = new Http();

        reqData.setHeader('Content-Type', 'application/json');
        reqData.setHeader('Connection', 'keep-alive');
        reqData.setHeader('Content-Length', '0');
        reqData.setHeader('Authorization', authorizationHeader);
        reqData.setTimeout(120000);
        reqData.setEndpoint(endpointURL);

        if (String.isNotEmpty(webServiceConfig.Certificate_Name__c)) {
            reqData.setClientCertificateName(webServiceConfig.Certificate_Name__c);
        }

        try {
            // Retrieve the necessary product data based on passed Contracted Product Id
            List<Contracted_Products__c> prodList = [
                SELECT
                    Id,
                    PBX__r.Assigned_Product_Id__c,
                    Site__r.Assigned_Product_Id__c,
                    Customer_Asset__r.Assigned_Product_Id__c,
                    Order__r.Billing_Arrangement__c,
                    PBX__r.Billing_Arrangement__r.Unify_Ref_Id__c,
                    PBX__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c,
                    Unify_Product_Family__c,
                    Unify_Billing_Offer__c,
                    PBX__c,
                    PBX_Change_Error_Info__c,
                    Billing_Offer_synced_to_Unify_timestamp__c
                FROM Contracted_Products__c
                WHERE Id = :contProdId
                LIMIT 1
            ];
            if (prodList.size() > 0) {
                Contracted_Products__c contProd = prodList[0];
                JSONGenerator generator = buildRequestBody(transactionId, contProd);

                reqData.setBody(generator.getAsString());
                reqData.setMethod('POST');

                HTTPResponse response = http.send(reqData);

                if (String.isEmpty(contProd.PBX_Change_Error_Info__c)) {
                    contProd.PBX_Change_Error_Info__c = '';
                }

                if (response.getStatusCode() == 200) {
                    ChangePBXOrderSuccessResponse resultSuccess = (ChangePBXOrderSuccessResponse) JSON.deserializeStrict(
                        response.getBody(),
                        ChangePBXOrderSuccessResponse.class
                    );
                    ChangePBXOrderRespData result = resultSuccess.changePBXOrderResponse;
                    OrderLineListType orderLine = result.orderLinesList[0];

                    // When successfull set the processing timestamp
                    updateCompetitorAsset(contProd, orderLine);

                    update contProd;
                } else {
                    // Otherwise write the error response to the Contracted Product
                    // ChangePBXOrderErrorResponse result = (ChangePBXOrderErrorResponse) JSON.deserializeStrict(response.getBody(), ChangePBXOrderErrorResponse.class);
                    // String result = (String) JSON.deserialize(response.getBody(), String.class);
                    String result = response.getBody();
                    contProd.PBX_Change_Error_Info__c = result.replace('&quot;', '"').left(255);
                    update contProd;
                }
            }
        } catch (Exception e) {
            throw e;
        }
    }

    private static void updateCompetitorAsset(
        Contracted_Products__c contProd,
        OrderLineListType orderLine
    ) {
        if (
            contProd.PBX__r.Assigned_Product_Id__c == orderLine.APID &&
            contProd.PBX__c == orderLine.externalReferenceID
        ) {
            String assetId = contProd.PBX__c;
            DateTime timestamp = datetime.now();
            Competitor_Asset__c compAsset = [
                SELECT Id
                FROM Competitor_Asset__c
                WHERE Id = :assetId
            ];
            if (compAsset != null) {
                compAsset.Billing_Offer_synced_to_Unify_timestamp__c = timestamp;
                compAsset.PBX_Trunking_Done__c = true;
                update compAsset;
            }
            contProd.Billing_Offer_synced_to_Unify_timestamp__c = timestamp;
        } else if (orderLine.errorInfo != null) {
            // Otherwise write the error response to the Contracted Product
            String errInfo =
                orderLine.errorInfo.errorCode +
                ': ' +
                orderLine.errorInfo.errorDescription +
                ' (' +
                orderLine.errorInfo.targetSystemName +
                ': ' +
                orderLine.errorInfo.targetServiceName +
                ')';
            contProd.PBX_Change_Error_Info__c = errInfo.replace('&quot;', '"').left(255);
        }
    }

    /**
     * Generate the request JSON message
     */
    private static JSONGenerator buildRequestBody(
        String transactionId,
        Contracted_Products__c contProd
    ) {
        JSONGenerator generator = JSON.createGenerator(false);
        generator.writeStartObject();
        generator.writeFieldName('changePBXOrderRequest');
        generator.writeStartObject();
        generator.writeStringField('transactionID', transactionId);
        generator.writeStringField(
            'APID',
            contProd.PBX__r.Assigned_Product_Id__c != null
                ? string.valueOf(contProd.PBX__r.Assigned_Product_Id__c)
                : ''
        );
        generator.writeStringField(
            'SLSAPID',
            contProd.Site__r.Assigned_Product_Id__c != null
                ? string.valueOf(contProd.Site__r.Assigned_Product_Id__c)
                : ''
        );
        generator.writeFieldName('orderLinesList');
        generator.writeStartArray();
        generator.writeStartObject();
        generator.writeStringField(
            'BAID',
            contProd.PBX__r.Billing_Arrangement__r.Unify_Ref_Id__c != null
                ? string.valueOf(contProd.PBX__r.Billing_Arrangement__r.Unify_Ref_Id__c)
                : ''
        );
        generator.writeStringField(
            'BCID',
            contProd.PBX__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c !=
                null
                ? string.valueOf(
                      contProd.PBX__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c
                  )
                : ''
        );
        generator.writeStringField('chargeDescription', ''); // Not used
        generator.writeStringField('chargeAmount', ''); // Not used
        generator.writeStringField(
            'unifyFamilyTag',
            contProd.Unify_Product_Family__c != null
                ? string.valueOf(contProd.Unify_Product_Family__c)
                : ''
        );
        generator.writeFieldName('listOfBORefs');
        generator.writeStartArray();
        generator.writeString(
            contProd.Unify_Billing_Offer__c != null
                ? string.valueOf(contProd.Unify_Billing_Offer__c)
                : ''
        );
        generator.writeEndArray();
        generator.writeStringField(
            'externalReferenceID',
            contProd.PBX__c != null ? string.valueOf(contProd.PBX__c) : ''
        );
        generator.writeEndObject();
        generator.writeEndArray();
        generator.writeEndObject();
        generator.writeEndObject();
        return generator;
    }

    public class ChangePBXOrderSuccessResponse {
        public ChangePBXOrderRespData changePBXOrderResponse { get; set; }

        public ChangePBXOrderSuccessResponse(ChangePBXOrderRespData changePBXOrderResponse) {
            this.changePBXOrderResponse = changePBXOrderResponse;
        }
    }

    public class ChangePBXOrderRespData {
        List<OrderLineListType> orderLinesList { get; set; }

        public ChangePBXOrderRespData(List<OrderLineListType> orderLinesList) {
            this.orderLinesList = orderLinesList;
        }
    }

    @SuppressWarnings(
        'PMD.PropertyNamingConventions'
    ) // APID property must be all uppercase because class is used for JSON deserialization
    public class OrderLineListType {
        public String APID { get; set; }
        public String externalReferenceID { get; set; }
        public ErrorInfo errorInfo { get; set; }

        @SuppressWarnings('PMD.FormalParameterNamingConventions') // used for JSON deserialization
        public OrderLineListType(String APID, String externalReferenceID, ErrorInfo errorInfo) {
            this.APID = APID;
            this.externalReferenceID = externalReferenceID;
            this.errorInfo = errorInfo;
        }
    }

    @SuppressWarnings('PMD.ExcessiveParameterList') // used for JSON deserialization
    public class ErrorInfo {
        public String errorCode { get; set; }
        public String errorDescription { get; set; }
        public String targetSystemName { get; set; }
        public String targetServiceName { get; set; }

        public ErrorInfo(
            String errorCode,
            String errorDescription,
            String targetSystemName,
            String targetServiceName
        ) {
            this.errorCode = errorCode;
            this.errorDescription = errorDescription;
            this.targetSystemName = targetSystemName;
            this.targetServiceName = targetServiceName;
        }
    }
}