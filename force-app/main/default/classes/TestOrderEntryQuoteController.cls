@isTest
public with sharing class TestOrderEntryQuoteController {
	@TestSetup
	static void makeData() {
		TestUtils.theAdministrator = new User(Id = UserInfo.getUserId());
		TestUtils.createCompleteOpportunity();
		MavenDocumentsTestFactory.createMavenDocumentsRecords();
	}

	@isTest
	static void generateConfirmation() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		mmdoc__Document_Request__c doc = OrderEntryQuoteController.generateConfirmation(opp.Id, 'Fixed');
		Test.stopTest();
		System.assertEquals(doc.mmdoc__Status__c, 'New', 'Confirmation should be created.');
	}

	@isTest
	static void testGenerateContractSummary() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		mmdoc__Document_Request__c doc = OrderEntryQuoteController.generateContractSummary(opp.Id, 'Fixed');
		Test.stopTest();
		System.assertEquals(doc.mmdoc__Status__c, 'New', 'Confirmation should be created.');
		OrderEntryQuoteController.getDocumentRequest(doc.Id);
		System.assertEquals(doc.mmdoc__Status__c, 'New', 'Quote should be created.');
	}

	@isTest
	static void updateOpportunitySingedQuote() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Attachment attachment = new Attachment(ParentId = opp.Id, Name = 'quot.pdf', Body = Blob.toPdf('string'));
		insert attachment;
		List<Attachment> attachmentList = [SELECT Id FROM Attachment WHERE ParentId = :opp.Id];
		Test.startTest();
		if (!attachmentList.isEmpty()) {
			OrderEntryQuoteController.updateOpportunitySingedQuote(opp.Id, attachmentList[0].Id, true);
		}
		Test.stopTest();
		Opportunity oppResult = [SELECT Id, LG_SignedQuoteAvailable__c FROM Opportunity WHERE Id = :opp.Id];
		System.assertEquals('true', oppResult.LG_SignedQuoteAvailable__c, 'Opportunity should be updated.');
	}

	@isTest
	static void updateQuoteAttachment() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Attachment attachment = new Attachment(ParentId = opp.Id, Name = 'quot.pdf', Body = Blob.toPdf('string'));
		insert attachment;

		ContentVersion cv = new ContentVersion(Title = 'test', PathOnClient = 'quot.pdf', VersionData = Blob.toPdf('string'));
		insert cv;
		ContentVersion conVer = [SELECT Id, ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id];
		ContentDocumentLink cdl = new ContentDocumentLink();
		cdl.ContentDocumentId = conVer.ContentDocumentId;
		cdl.LinkedEntityId = opp.Id;
		cdl.ShareType = 'V';
		insert cdl;

		List<Attachment> attachmentList = [SELECT Id FROM Attachment WHERE ParentId = :opp.Id];
		Test.startTest();

		Boolean attachmentUpdate = OrderEntryQuoteController.updateQuoteAttachment(
			attachmentList[0].Id,
			EncodingUtil.base64Encode(Blob.toPdf('String'))
		);
		Boolean contentVersionUpdate = OrderEntryQuoteController.updateQuoteAttachment(conVer.Id, EncodingUtil.base64Encode(Blob.toPdf('String')));

		Test.stopTest();
		System.assertEquals(true, attachmentUpdate, 'Attachment should be updated.');
		System.assertEquals(true, contentVersionUpdate, 'ContentVersion should be updated.');
	}

	@isTest
	static void updateOpportunityAutomatedQuoteDelivery() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		OrderEntryQuoteController.updateOpportunityAutomatedQuoteDelivery(opp.Id, 'Quote Requested');
		Test.stopTest();
		Opportunity oppResult = [SELECT Id, LG_AutomatedQuoteDelivery__c FROM Opportunity WHERE Id = :opp.Id];
		System.assertEquals('Quote Requested', oppResult.LG_AutomatedQuoteDelivery__c, 'Opportunity should be updated.');
	}
}
