/*
    Author: Juan Cardona
    Description: Component that serves as controller of the Accordion Lighting Web Component.
    Jira ticket: COM-2682
*/

public class AccordionFormController {
	@AuraEnabled
	public static LightningResponse parseJsonInput(String jsonInput) {
		List<AccordionSection> lstAccordionSection = (List<AccordionSection>) JSON.deserialize(jsonInput, List<AccordionSection>.class);

		return new LightningResponse().setBody(lstAccordionSection);
	}

	/*
	 * Class that holds the section attributes to show
	 */
	public class AccordionSection {
		@AuraEnabled
		public String tittleLabel;
		@AuraEnabled
		public String tittleReference;
		@AuraEnabled
		public List<AccordionSectionRow> listRows;
		@AuraEnabled
		public List<AccordionSection> listAccordionSections;
	}

	/*
	 * Class that holds the section row attributes to show
	 */
	public class AccordionSectionRow {
		@AuraEnabled
		public Integer order;
		@AuraEnabled
		public List<AccordionFormField> listFields;
	}

	/*
	 * Class that holds the field attributes to show
	 */
	public class AccordionFormField {
		@AuraEnabled
		public String fieldId;
		@AuraEnabled
		public String fieldLabel;
		@AuraEnabled
		public String fieldeReference;
		@AuraEnabled
		public String fieldApiName;
		@AuraEnabled
		public String fieldObjectApiName;
		@AuraEnabled
		public String fieldRecordId;
		@AuraEnabled
		public Boolean isRequired;
		@AuraEnabled
		public Integer order;
		@AuraEnabled
		public String fieldTypeString;
		@AuraEnabled
		public String value;
		@AuraEnabled
		public Boolean isBlank;
		@AuraEnabled
		public Boolean isInput;
	}
}
