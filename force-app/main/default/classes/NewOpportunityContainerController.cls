public with sharing class NewOpportunityContainerController {
	@AuraEnabled
	public static List<NewOpportunityContainerController.RecordTypePicklistOption> getRecordTypes() {
		List<NewOpportunityContainerController.RecordTypePicklistOption> options = new List<NewOpportunityContainerController.RecordTypePicklistOption>();
		Schema.DescribeSObjectResult dsObjectResult = Opportunity.SObjectType.getDescribe();
		List<Schema.RecordTypeInfo> recordTypeInfos = dsObjectResult.getRecordTypeInfos();

		for (Schema.RecordTypeInfo rti : recordTypeInfos) {
			if (rti.isAvailable() && !rti.isMaster()) {
				NewOpportunityContainerController.RecordTypePicklistOption opt = new NewOpportunityContainerController.RecordTypePicklistOption();
				opt.label = rti.getName();
				opt.value = rti.getRecordTypeId();
				options.add(opt);
			}
		}
		return options;
	}

	public class RecordTypePicklistOption {
		@AuraEnabled
		public String label;
		@AuraEnabled
		public String value;
	}
}
