@IsTest
private class TestCS_OrderTriggerHandler {
	@IsTest
	private static void testDeliveryDecomposition() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole WHERE ParentRoleId = NULL];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Account testAccount = CS_DataTest.createAccount('Test Account');
			insert testAccount;

			Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112', testAccount, '1111AA', 'Breestraat', 'LEIDEN', 112);
			insert site;

			Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', simpleUser.id);
			insert testOpp;

			csord__Order_Request__c coreq = new csord__Order_Request__c(csord__Module_Name__c = 'Test', csord__Module_Version__c = '1.0');
			insert coreq;

			csord__Order__c order = new csord__Order__c(
				Name = 'Test Order',
				csord__Account__c = testAccount.Id,
				csord__Status2__c = 'Created',
				csord__Order_Request__c = coreq.Id,
				csord__Identification__c = 'DWHTestBatchOn_' + System.now(),
				csordtelcoa__Opportunity__c = testOpp.Id
			);
			insert order;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
			insert basket;

			Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
				.get('Product Definition')
				.getRecordTypeId();

			cscfga__Product_Definition__c accessDef = CS_DataTest.createProductDefinition('Access Infrastructure');
			accessDef.RecordTypeId = productDefinitionRecordType;
			accessDef.Product_Type__c = 'Fixed';
			insert accessDef;

			cscfga__Product_Configuration__c accessConf = CS_DataTest.createProductConfiguration(accessDef.Id, 'Access Infrastructure', basket.Id);
			accessConf.ClonedSiteIds__c = '{"0000":"", "0002":""}';
			accessConf.ClonedPBXIds__c = '{"0000":"", "0001":""}';
			insert accessConf;

			csord__Subscription__c subscription = CS_DataTest.createSubscription(accessConf.Id);
			subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
			subscription.csord__Order__c = order.Id;
			insert subscription;

			csord__Service__c service = CS_DataTest.createService(accessConf.Id, subscription);
			service.csordtelcoa__Product_Configuration__c = accessConf.Id;
			service.csord__Identification__c = 'testSubscription';
			service.csord__Subscription__c = subscription.Id;
			service.csord__Status__c = 'Test';
			service.csord__Order__c = order.Id;
			service.Site__c = site.Id;
			insert service;

			Test.startTest();
			order.csord__Status2__c = 'Ready for Delivery';
			update order;
			Test.stopTest();

			csord__Order__c result = [SELECT Id, csord__Status2__c FROM csord__Order__c WHERE Id = :order.Id];

			// New O2U status is In Delivery (old status Decomposed).
			System.assertEquals('In Delivery', result.csord__Status2__c, 'Status is not the same.');
		}
	}

	@IsTest
	static void testPopulateBanInformation() {
		Account acc = CS_DataTest.createAccount('TestAcc');
		insert acc;

		Contact c = TestUtils.createContact(acc);
		Ban__c ban = TestUtils.createBan(acc);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, c.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = CS_DataTest.createOpportunity(acc, 'TestOpp', UserInfo.getUserId());
		opp.BAN__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		insert opp;

		csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csordtelcoa__Opportunity__c = opp.Id, csord__Identification__c = 'ID_4568978');
		ord.csord__status2__c = 'Decomposed';
		ord.csordtelcoa__Opportunity__c = opp.id;
		ord.csord__Account__c = acc.Id;

		Test.startTest();
		insert ord;
		Test.stopTest();

		csord__Order__c o = [SELECT Id, BAN_Information__c, Financial_Account__c, Billing_Arrangement__c FROM csord__Order__c WHERE Id = :ord.Id];

		System.assertEquals(ban.Id, o.BAN_Information__c, 'Wrong ban on order.');
		System.assertEquals(fa.Id, o.Financial_Account__c, 'Wrong financial account on order.');
		System.assertEquals(bar.Id, o.Billing_Arrangement__c, 'Wrong billing arrangement on order.');
	}
}
