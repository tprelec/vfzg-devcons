@IsTest
private class TestCS_ComHelper {
	@IsTest
	static void testgenerateRandomIpAddress4() {
		String numOfIpAdd = 'Connect MKB Internet 1 IP V4';
		String result = CS_ComHelper.generateRandomIpAddress(numOfIpAdd);
		System.assertEquals(16, result.length(), 'IP Address should be 16 characters long.');
	}
	@IsTest
	static void testgenerateRandomIpAddress5x4() {
		String numOfIpAdd = 'Connect MKB Internet 5 IP V4';
		String result = CS_ComHelper.generateRandomIpAddress(numOfIpAdd);
		System.assertEquals(83, result.length(), 'IP Address should be 83 characters long.');
	}
	@IsTest
	static void testGenerateRandomIp6Address() {
		String numOfIpAdd = 'Connect MKB Internet IP V6';
		String result = CS_ComHelper.generateRandomIpV6Address(numOfIpAdd);
		System.assertEquals(39, result.length(), 'IP Address should be 39 characters long.');
	}

	@IsTest
	static void testCreateMobileFlowProcessUpdateTrigger() {
		Map<Id, VF_Contract__c> newMap;
		Map<Id, VF_Contract__c> oldMap;
		List<VF_Contract__c> initialList = new List<VF_Contract__c>();
		List<VF_Contract__c> newList = new List<VF_Contract__c>();

		List<Profile> pList = CS_DataTest.returnSystemAdminProfile();
		List<UserRole> roleList = CS_DataTest.returnUserRoleWithoutParentRole();

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			// process and step template part
			CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Mobile Flow', '5', true);
			CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);
			CSPOFA__Orchestration_Step_Template__c step2Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 2', '2', true);
			Integer numberOfContracts = 3;
			initialList = CS_DataTest.createVfContracts(numberOfContracts, simpleUser);

			oldMap = new Map<Id, VF_Contract__c>(initialList);

			newList = initialList.deepClone(true);
			for (VF_Contract__c vfCon : newList) {
				vfCon.Eligible_for_CS_Mobile_Flow__c = true;
				vfCon.Ready_for_Implementation__c = true;
			}
			newMap = new Map<Id, VF_Contract__c>(newList);

			CS_ComHelper.createMobileFlowProcess(oldMap, newMap);

			List<CSPOFA__Orchestration_Process__c> orchProcess = new List<CSPOFA__Orchestration_Process__c>(
				[SELECT Id, Name, Contract_VF__c FROM CSPOFA__Orchestration_Process__c WHERE Contract_VF__c IN :newMap.keySet()]
			);

			System.assertEquals(numberOfContracts, orchProcess.size(), 'Size is not the same.');
		}
	}

	@IsTest
	static void testOrchestratorSubscribeToEvent() {
		Map<Id, VF_Contract__c> newMap;
		Map<Id, VF_Contract__c> oldMap;
		List<VF_Contract__c> initialList = new List<VF_Contract__c>();
		List<VF_Contract__c> newList = new List<VF_Contract__c>();

		List<Profile> pList = CS_DataTest.returnSystemAdminProfile();
		List<UserRole> roleList = CS_DataTest.returnUserRoleWithoutParentRole();

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			initialList = CS_DataTest.createVfContracts(2, simpleUser);

			oldMap = new Map<Id, VF_Contract__c>(initialList);

			newList = initialList.deepClone(true);
			for (VF_Contract__c vfCon : newList) {
				vfCon.Implementation_Status__c = 'Renewed';
			}
			newMap = new Map<Id, VF_Contract__c>(newList);

			Boolean exceptionHappened = false;
			try {
				CS_ComHelper.orchestratorSubscribeToEvent(oldMap, newMap);
			} catch (Exception e) {
				exceptionHappened = true;
			}
			System.assertEquals(false, exceptionHappened, 'Exception happened.');
		}
	}

	@IsTest
	static void testGetServiceSpecificationSimpleAttribute() {
		List<Profile> pList = CS_DataTest.returnSystemAdminProfile();
		List<UserRole> roleList = CS_DataTest.returnUserRoleWithoutParentRole();
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;
		System.runAs(simpleUser) {
			CS_EDMServiceSpecification serSpec = new CS_EDMServiceSpecification();
			serSpec.status = 'Status';
			serSpec.guid = '789-654312';
			serSpec.startDate = '233.08.2020';
			serSpec.name = 'Name';
			serSpec.description = 'description';
			serSpec.version = '1';
			serSpec.replacedSpecification = '';
			serSpec.identifier = '456987123';
			serSpec.endDate = '25.01.3000';
			serSpec.code = 'xCode';
			serSpec.instanceId = 'id_56';
			serSpec.productConfigurationId = '45678';
			List<CS_EDMServiceSpecification.SimpleAttribute> simpleAttributes = new List<CS_EDMServiceSpecification.SimpleAttribute>();
			CS_EDMServiceSpecification.SimpleAttribute sa = new CS_EDMServiceSpecification.SimpleAttribute('sa name', 'sa value');
			simpleAttributes.add(sa);
			serSpec.simpleAttributes = simpleAttributes;
			serSpec.additionalSimpleAttributes = simpleAttributes;
			serSpec.serviceId = '456892';
			Map<String, List<CS_EDMServiceSpecification.ComplexAttribute>> complexAttributesMap = new Map<String, List<CS_EDMServiceSpecification.ComplexAttribute>>();
			List<CS_EDMServiceSpecification.ComplexAttribute> complexAttributes = new List<CS_EDMServiceSpecification.ComplexAttribute>();
			CS_EDMServiceSpecification.ComplexAttribute comAtt = new CS_EDMServiceSpecification.ComplexAttribute();
			comAtt.productConfigurationId = '456789';
			comAtt.simpleAttributes = simpleAttributes;
			complexAttributes.add(comAtt);
			complexAttributesMap.put('key_2', complexAttributes);
			serSpec.complexAttributes = complexAttributesMap;
			Set<String> simpleAttributeNames = new Set<String>();
			simpleAttributeNames.add('sa name');
			test.startTest();
			Map<String, String> result = CS_ComHelper.getServiceSpecificationSimpleAttribute(serSpec, simpleAttributeNames);
			System.assertEquals('sa value', result.get('sa name'), 'Value in the map is not correct!');
			test.stopTest();
		}
	}

	@IsTest
	static void testGetServiceSpecificationAdditionalAttribute() {
		List<Profile> pList = CS_DataTest.returnSystemAdminProfile();
		List<UserRole> roleList = CS_DataTest.returnUserRoleWithoutParentRole();

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			CS_EDMServiceSpecification serSpec = new CS_EDMServiceSpecification();
			serSpec.status = 'Status';
			serSpec.guid = '789-654312';
			serSpec.startDate = '233.08.2020';
			serSpec.name = 'Name';
			serSpec.description = 'description';
			serSpec.version = '1';
			serSpec.replacedSpecification = '';
			serSpec.identifier = '456987123';
			serSpec.endDate = '25.01.3000';
			serSpec.code = 'xCode';
			serSpec.instanceId = 'id_56';
			serSpec.productConfigurationId = '45678';

			List<CS_EDMServiceSpecification.SimpleAttribute> simpleAttributes = new List<CS_EDMServiceSpecification.SimpleAttribute>();
			CS_EDMServiceSpecification.SimpleAttribute sa = new CS_EDMServiceSpecification.SimpleAttribute('sa name', 'sa value');
			simpleAttributes.add(sa);

			serSpec.simpleAttributes = simpleAttributes;
			serSpec.additionalSimpleAttributes = simpleAttributes;
			serSpec.serviceId = '456892';

			Map<String, List<CS_EDMServiceSpecification.ComplexAttribute>> complexAttributesMap = new Map<String, List<CS_EDMServiceSpecification.ComplexAttribute>>();
			List<CS_EDMServiceSpecification.ComplexAttribute> complexAttributes = new List<CS_EDMServiceSpecification.ComplexAttribute>();
			CS_EDMServiceSpecification.ComplexAttribute comAtt = new CS_EDMServiceSpecification.ComplexAttribute();
			comAtt.productConfigurationId = '456789';
			comAtt.simpleAttributes = simpleAttributes;
			complexAttributes.add(comAtt);
			complexAttributesMap.put('key_2', complexAttributes);
			serSpec.complexAttributes = complexAttributesMap;

			Set<String> simpleAttributeNames = new Set<String>();
			simpleAttributeNames.add('sa name');

			Test.startTest();
			Map<String, String> result = CS_ComHelper.getServiceSpecificationAdditionalAttributes(serSpec, simpleAttributeNames);

			System.assertEquals('sa value', result.get('sa name'), 'Value in the map is not correct!');
			Test.stopTest();
		}
	}
}
