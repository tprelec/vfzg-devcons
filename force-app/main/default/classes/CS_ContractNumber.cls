/*
 * @description represents ContractNumber, structured string used to connect created contracts/agreements with other records (e.g. configurations, subscriptions&services etc.)
 *
 */
public with sharing class CS_ContractNumber {
	/*
	 * @description format of the ContractNumber, #placeholders are replaced with property values during serialization (toString)
	 */
	public final String format = 'CS-#YYYY#-VZ-#GROUP#-#NUMBER# #MAJOR#.#MINOR# d.d. #DATE#';
	/*
	 * @description values: 2 for Acquisittion, 3 for MACD
	 */
	public Integer majorVersion { get; set; }
	/*
	 * @description values: always 1 for Acquisition, starting from 1 for MACD and incrementing for subsequent MACDs
	 */
	public Integer minorVersion { get; set; }
	/*
	 * @description date of basket synchronization
	 */
	public Date contractDate { get; set; }
	/*
	 * @description in case of MACD basket this field contains original "acquisition contract date"
	 */
	public Date originalContractDate { get; set; }
	/*
	 * @description year of basket synchronization
	 */
	public Integer year { get; set; }
	/*
	 * @description defines a group of products, e.g. "BI" for Business Internet (obtained from Solution/Product Configuration)
	 */
	public String contractGroup { get; set; }

	/*
	 * @description number which increments each time we create new contract number (global custom setting)
	 * awarded on acquisition and stays the same in case of MACD
	 */
	public Integer incrementNumber { get; set; }

	public CS_ContractNumber(Integer majorVersion, Integer minorVersion, String contractGroup) {
		this.majorVersion = majorVersion;
		this.minorVersion = minorVersion;
		this.contractGroup = contractGroup;
		this.contractDate = Date.today();
		this.year = Date.today().year();
	}

	public CS_ContractNumber(Integer majorVersion, Integer minorVersion, String contractGroup, Integer incrementNumber) {
		this(majorVersion, minorVersion, contractGroup);
		this.incrementNumber = incrementNumber;
	}

	public Boolean isUpToDate() {
		return this.contractDate == Date.today();
	}

	public Boolean isMacd() {
		return this.majorVersion == 3;
	}

	public void bringItUpToDate() {
		this.contractDate = Date.today();
		this.year = Date.today().year();
	}

	public override String toString() {
		return format.replace('#YYYY#', String.valueOf(this.year))
			.replace('#GROUP#', this.contractGroup)
			.replace('#NUMBER#', String.valueOf(this.incrementNumber).leftPad(5, '0'))
			.replace('#MAJOR#', String.valueOf(this.majorVersion))
			.replace('#MINOR#', String.valueOf(this.minorVersion))
			.replace('#DATE#', Datetime.newInstance(this.contractDate, Time.newInstance(0, 0, 0, 0)).format('dd-MM-YYYY'));
	}
}
