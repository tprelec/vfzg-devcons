@isTest
public class CS_SiteUtilityTest {

    public CS_SiteUtilityTest() {

    }

    private static cscfga__Product_Configuration__c initConfig(Id productDefId, String productConfigName, Id basketId)
    {
        cscfga__Product_Configuration__c config = CS_DataTest.createProductConfiguration(productDefId, productConfigName, basketId);

        config.cscfga__Contract_Term__c = 24;
        config.cscfga__total_one_off_charge__c = 225;
        config.cscfga__Total_Price__c = 20;
        config.Number_of_SIP__c = 20;
        config.RC_Cost__c = 0;
        config.NRC_Cost__c = 0;
        config.cscfga__Contract_Term_Period__c = 24;
        config.cscfga__Configuration_Status__c = 'Valid';
        config.cscfga__Quantity__c = 1;
        config.cscfga__total_recurring_charge__c = 18;
        config.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
        config.cscfga__one_off_charge_product_discount_value__c = 0;
        config.cscfga__recurring_charge_product_discount_value__c = 0;

        return config;
    }

    @isTest
    private static void testHelpers()
    {        
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
	    
    	System.runAs (simpleUser) {
    	    PriceReset__c priceResetSetting = new PriceReset__c();
           
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
            
            insert priceResetSetting;
    
            Account testAccount = CS_DataTest.createAccount('Test Account 1');
            Account testAccount2 = CS_DataTest.createAccount('Test Account 2');
            List<Account> accList = new List<Account>{testAccount,testAccount2};
            insert accList;
            
            Contact contact1 = new Contact(
                AccountId = testAccount.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'   
            );
            insert contact1;
            
            testAccount.Authorized_to_sign_1st__c = contact1.Id;
            testAccount.Contract_rule_no_mailing__c = true;
            testAccount.Frame_Work_Agreement__c = 'VFZA-2018-8';
            testAccount.Version_FWA__c = 4;
            testAccount.Framework_agreement_date__c = Date.today();
            update testAccount;
            
            Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Sites opportunity',simpleUser.id);
            Opportunity opp2 = new Opportunity();
            opp2.Name = 'testName';
            opp2.Account = testAccount2;
            opp2.StageName= 'Qualification';
            opp2.CloseDate = Date.Today() + 10;
            
            List<Opportunity> oppList = new List<Opportunity>{opp, opp2};
            insert oppList;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Sites Basket');
            basket.Name = 'Contract Basket';
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = testAccount.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 24;
            basket.OneNet_Scenario__c = 'One Net Enterprise';
            basket.Fixed_Scenario__c = 'One Fixed Enterprise';
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 02, 02);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 02, 02);
            basket.Number_of_SIP__c = 12;
            basket.Approved_Date__c = Date.today() - 4;
            basket.PortingPercentage__c = 10;
 
            List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>{basket};
            insert basketList;
            
            cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
            accessInfraDef.Product_Type__c = 'Fixed';
    
            cscfga__Product_Definition__c managedInternetDef = CS_DataTest.createProductDefinition('Managed Internet');
            managedInternetDef.Product_Type__c = 'Fixed';
            
            cscfga__Product_Definition__c ipvpnDef = CS_DataTest.createProductDefinition('IPVPN');
            ipvpnDef.Product_Type__c = 'Fixed';
    
            cscfga__Product_Definition__c oneFixedDef = CS_DataTest.createProductDefinition('One Fixed');
            oneFixedDef.Product_Type__c = 'Fixed';
            
            cscfga__Product_Definition__c vodAccessDefn = CS_DataTest.createProductDefinition('Vodafone Access');
            vodAccessDefn.Product_Type__c = 'Fixed';
            
            cscfga__Product_Definition__c mobCTNSubs = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
            mobCTNSubs.Product_Type__c = 'Mobile';
            
            List<cscfga__Product_Definition__c> prodDefList = new List<cscfga__Product_Definition__c>{accessInfraDef,managedInternetDef,ipvpnDef,oneFixedDef,vodAccessDefn,mobCTNSubs};
            insert prodDefList;

            Site__c testSite = new Site__c();
        	testSite.Name = 'testSite';
        	testSite.Site_Postal_Code__c = '1234AA';
        	testSite.Site_Account__c = testAccount.Id;
        	testSite.Site_Street__c = 'teststreet';
        	testSite.Site_City__c = 'testTown';
        	testSite.Site_House_Number__c = 3;
        	insert testSite;
        	
        	Site_Availability__c testSA = new Site_Availability__c();
        	testSA.Name = 'testSA';
        	testSA.Site__c = testSite.Id;
        	testSA.Vendor__c = 'ZIGGO'; 
        	testSA.Premium_Vendor__c = 'TELECITY'; 
        	testSA.Access_Infrastructure__c ='Coax'; 
        	testSA.Bandwith_Down_Entry__c = 1; 
        	testSA.Bandwith_Up_Entry__c = 10; 
        	testSA.Bandwith_Down_Premium__c = 1; 
        	testSA.Bandwith_Up_Premium__c = 10;
        	insert testSA;
    
            Site_Availability__c testSA2 = new Site_Availability__c();
        	testSA2.Name = 'testSA2';
        	testSA2.Site__c = testSite.Id;
        	testSA2.Vendor__c = 'ZIGGO'; 
        	testSA2.Premium_Vendor__c = 'TELECITY'; 
        	testSA2.Access_Infrastructure__c ='Coax'; 
        	testSA2.Bandwith_Down_Entry__c = 1; 
        	testSA2.Bandwith_Up_Entry__c = 10; 
        	testSA2.Bandwith_Down_Premium__c = 1; 
        	testSA2.Bandwith_Up_Premium__c = 10;
            insert testSA2;

            // EXAMPLE: {"0000":"a4D3N000000CxEpUAK","jtE5":"a4D3N000000CxEkUAK","QUhf":"a4D3N000000CxFPUA0"}
            String clonedIds = '{"0000":"' + testSA.Id + '","jtE5":"' + testSA2.Id + '"}';
            
    
            cscfga__Product_Configuration__c pcAccessInf = initConfig(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
            pcAccessInf.cspl__Type__c = 'Mobile Voice Services';
            pcAccessInf.OneNet_Scenario__c = 'One Fixed';
            pcAccessInf.Deal_Type__c = 'Acquisition';
            pcAccessInf.Mobile_Scenario__c = 'test';
            pcAccessInf.cscfga__Product_Family__c = 'Access Infrastructure';
            pcAccessInf.Site_Availability_Id__c = testSA.Id;
            pcAccessInf.ClonedSiteIds__c = clonedIds;
            insert pcAccessInf;
    
            cscfga__Product_Configuration__c pcOneFixed = initConfig(oneFixedDef.Id, 'One Fixed',basket.Id);
            pcOneFixed.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcOneFixed.Number_of_SIP_Clone_Multiplicator__c = 4;
            pcOneFixed.Mobile_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcSkype = initConfig(oneFixedDef.Id, 'Skype for Business',basket.Id);
            pcSkype.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcSkype.Number_of_SIP_Clone_Multiplicator__c = 4;
            pcSkype.Mobile_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcBMS = initConfig(oneFixedDef.Id, 'Business Managed Services',basket.Id);
            pcBMS.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcBMS.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcBMS.Mobile_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcMobCTN = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN.Deal_Type__c = 'Disconnect';
            pcMobCTN.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN.Mobile_Scenario__c = 'OneBusiness';
            
            cscfga__Product_Configuration__c pcMobCTN2 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN2.Deal_Type__c = 'Disconnect';
            pcMobCTN2.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN2.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN2.Mobile_Scenario__c = 'Data only';
            
            cscfga__Product_Configuration__c pcMobCTN3 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN3.Deal_Type__c = 'Disconnect';
            pcMobCTN3.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN3.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN3.Mobile_Scenario__c = 'RedPro';
            
            cscfga__Product_Configuration__c pcMobCTN4 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN4.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN4.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN4.Mobile_Scenario__c = 'OneBusiness IOT';
            
            cscfga__Product_Configuration__c pcMobCTN5 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN5.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN5.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN5.Mobile_Scenario__c = 'OneMobile';
            
            cscfga__Product_Configuration__c pcMobCTN6 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN6.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN6.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN6.Mobile_Scenario__c = 'OneBusiness';
            
            cscfga__Product_Configuration__c pcVodCal = initConfig(vodAccessDefn.Id, 'Vodafone Calling',basket.Id);
            pcVodCal.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcVodCal.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcVodCal.Mobile_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcOneNet = initConfig(vodAccessDefn.Id, 'One Net',basket.Id);
            pcOneNet.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcOneNet.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcOneNet.Mobile_Scenario__c = 'test';
            pcOneNet.OneNet_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcWOS = initConfig(vodAccessDefn.Id, 'Wireless Only Standalone',basket.Id);
            pcWOS.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcWOS.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcWOS.Mobile_Scenario__c = 'test';
            pcWOS.OneNet_Scenario__c = 'test';

            cscfga__Product_Configuration__c pcCLFV = initConfig(vodAccessDefn.Id, 'Company Level Fixed Voice',basket.Id);
            pcCLFV.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcCLFV.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcCLFV.Mobile_Scenario__c = 'test';
            pcCLFV.OneNet_Scenario__c = 'test';

            List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>{ pcOneFixed, pcSkype, pcBMS, pcMobCTN, pcMobCTN2, 
                pcMobCTN3, pcMobCTN4, pcMobCTN5, pcMobCTN6, pcVodCal, pcOneNet, pcCLFV };
            insert pcList;
            
            pcAccessInf.OneFixed__c = pcOneFixed.id;
            update pcAccessInf;

            Test.startTest();

            Set<Id> pcIdList = new Set<Id>{ pcAccessInf.Id, pcOneFixed.Id, pcSkype.Id,
                 pcBMS.Id, pcMobCTN.Id, pcMobCTN2.Id, pcMobCTN3.Id, pcMobCTN4.Id, pcMobCTN5.Id, pcMobCTN6.Id, pcVodCal.Id, pcOneNet.Id, pcCLFV.Id };

            List<cscfga__Product_Configuration__c> configs = CS_SiteUtility.getConfigData(pcIdList);
    
            Map<Id, List<String>> siteAvailabilities = CS_SiteUtility.getSiteAvailabilityForConfig(configs);
            System.assertNotEquals(0, siteAvailabilities.size());

            Map<Id, List<String>> siteLocations = CS_SiteUtility.getSiteLocationsForConfig(configs);
            System.assertNotEquals(0, siteLocations.size());

            cscfga__Product_Configuration__c associatedAccess = CS_SiteUtility.getAssociatedAccess(pcOneFixed.id, configs);

            Map<Id, Integer> siteConfigurationQuantities = CS_SiteUtility.getConfigurationSiteQuantity(pcIdList);
            System.assertNotEquals(0, siteConfigurationQuantities.keyset().size());

            Test.stopTest();
        }
    }
}