public with sharing class OrderEntryProductDetailTriggerHandler extends TriggerHandler {
	private List<Order_Entry_Product_Detail__c> newOEProductDetails;
	private Map<Id, Order_Entry_Product_Detail__c> newOEProductDetailsMap;
	private Map<Id, Order_Entry_Product_Detail__c> oldOEProductDetailsMap;

	private void init() {
		newOEProductDetails = (List<Order_Entry_Product_Detail__c>) this.newList;
		newOEProductDetailsMap = (Map<Id, Order_Entry_Product_Detail__c>) this.newMap;
		oldOEProductDetailsMap = (Map<Id, Order_Entry_Product_Detail__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	public void setExternalIds() {
		Sequence seq = new Sequence('Order_Entry_Product_Detail');

		if (seq.canBeUsed()) {
			seq.startMutex();
			for (Order_Entry_Product_Detail__c o : newOEProductDetails) {
				o.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}