@isTest
private class TestADMWorkTriggerHandler {
	@testSetup
	static void makeData() {
		User sysAdmin = TestUtils.createAdministrator();
		agf__ADM_Scrum_Team__c team = TestUtils.createScrumTeams(1, true)[0];
		Account acc = TestUtils.createAccount(sysAdmin);
		TestUtils.createContact(acc);
		TestUtils.createProductTags(1, true, team.Id);
		TestUtils.createThemes(1, true, sysAdmin.Id);
		TestUtils.createThemes(1, true, GeneralUtils.currentUser.Id);
		TestUtils.autoCommit = false;
		List<Field_Sync_Mapping__c> fsmList = new List<Field_Sync_Mapping__c>{
			TestUtils.createSync('agf__ADM_Work__c -> Case', 'agf__Subject__c', 'Subject'),
			TestUtils.createSync('Case -> agf__ADM_Work__c', 'Subject', 'agf__Subject__c'),
			TestUtils.createSync('Case -> agf__ADM_Work__c', 'ContactId', 'Case_Contact__c')
		};
		insert fsmlist;
		TestUtils.createAgileAccelerators(true);
	}

	static agf__ADM_Work__c createWork() {
		agf__ADM_Product_Tag__c productTag = [SELECT Id FROM agf__ADM_Product_Tag__c];
		return TestUtils.createWorkItems(1, true, TestUtils.createAdministrator().Id, productTag.Id)[0];
	}

	@isTest
	static void testWorkStatus() {
		Test.startTest();

		createWork();
		agf__ADM_Work__c work = TestUtils.getWork()[0];

		System.assertEquals(work.VF_Status__c, work.agf__Status__c, 'VF Status and AGF Status should be equal.');

		work.agf__Status__c = 'Closed - Sprint Released';
		work.Actual_resolution__c = 'Closed';
		update work;

		work = TestUtils.getWork()[0];

		System.assertEquals('Closed', work.agf__Status__c, 'AGF Status should be Closed.');

		Test.stopTest();
	}

	@isTest
	static void testCaseSync() {
		Contact c = [SELECT Id FROM Contact LIMIT 1];
		Id sfCaseId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName != 'Salesforce_Case' AND IsActive = TRUE LIMIT 1][0].Id;

		Test.startTest();

		Case testCase = new Case(RecordTypeId = sfCaseId, Sprint_Case__c = true, Subject = 'Test', Description = 'Test', ContactId = c.Id);
		insert testCase;

		agf__ADM_Work__c work = TestUtils.getWork()[0];

		// This should trigger a sync from work to case
		work.agf__Subject__c = 'Subject to final review';
		update work;

		testCase = [SELECT Subject FROM Case WHERE Id = :testCase.Id];

		System.assertEquals('Subject to final review', testCase.Subject, 'Case Subject should be Subject to final review.');
	}

	@isTest
	static void testCaseSyncNoCases() {
		Test.startTest();

		Integer createdCases = [SELECT COUNT() FROM Case];

		System.assertEquals(0, createdCases, 'No cases should be created.');

		Test.stopTest();
	}

	@isTest
	static void testPlaceholderTask() {
		Test.startTest();

		agf__ADM_Work__c work = createWork();

		work.agf__Assignee__c = GeneralUtils.currentUser.Id;
		update work;

		System.assert(Database.countQuery('SELECT COUNT() FROM agf__ADM_Task__c') > 0, 'Task should be created.');

		Test.stopTest();
	}

	@isTest
	static void testPlaceholderTaskWithExistingTasks() {
		Test.startTest();

		agf__ADM_Work__c work = createWork();

		agf__ADM_Task__c tsk = new agf__ADM_Task__c(
			agf__Work__c = work.Id,
			agf__Assigned_To__c = GeneralUtils.currentUser.Id,
			agf__Subject__c = 'Test'
		);
		insert tsk;

		work.agf__Assignee__c = GeneralUtils.currentUser.Id;
		update work;

		System.assert(Database.countQuery('SELECT COUNT() FROM agf__ADM_Task__c') > 0, 'Task should be created.');

		Test.stopTest();
	}
}
