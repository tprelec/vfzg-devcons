/**
 * @description Service class for operations on Product Basket object
 */
public with sharing class ProductBasketService {
	/**
	 * @description Queries detail fields for a given Product Basket record. Used by the RemoteActionProvider class.
	 *
	 * @param productBasketId ID of the product basket record to query the details for
	 *
	 * @return Product Basket record with the given ID and all necessary fields queried
	 */
	public cscfga__Product_Basket__c queryProductBasketDetails(Id productBasketId) {
		return [
			SELECT
				Id,
				Name,
				cscfga__Opportunity__r.Direct_Indirect__c,
				csbb__Account__c,
				csordtelcoa__Account__c,
				csordtelcoa__in_flight_change_type__c,
				csordtelcoa__Change_Type__c
			FROM cscfga__Product_Basket__c
			WHERE Id = :productBasketId
		];
	}

	/**
	 * @description Queries configuration details for a given Product Basket record. Used by the RemoteActionProvider class.
	 *
	 * @return List of configurations from the basket with information on the inflight applicability
	 */
	public Map<String, RemoteActionProvider.ConfigurationInfo> queryInflightConfigurations(Id productBasketId) {
		List<cscfga__Attribute__c> attributes = [
			SELECT
				Id,
				cscfga__Product_Configuration__c,
				cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c,
				cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,
				cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csordtelcoa__Change_Type__c,
				cscfga__Value__c
			FROM cscfga__Attribute__c
			WHERE
				Name = 'GUID'
				AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :productBasketId
				AND cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c != NULL
		];

		Set<Id> configIds = new Set<Id>();
		for (cscfga__Attribute__c attribute : attributes) {
			configIds.add(attribute.cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c);
		}

		if (configIds.isEmpty()) {
			return new Map<String, RemoteActionProvider.ConfigurationInfo>();
		}

		List<csord__Service__c> services = [
			SELECT Id, Name, csordtelcoa__Product_Configuration__c, Applicable_for_inflight_change__c
			FROM csord__Service__c
			WHERE csordtelcoa__Product_Configuration__c IN :configIds
		];

		Map<String, RemoteActionProvider.ConfigurationInfo> resultConfigs = new Map<String, RemoteActionProvider.ConfigurationInfo>();

		for (cscfga__Attribute__c attribute : attributes) {
			RemoteActionProvider.ConfigurationInfo configInfo = new RemoteActionProvider.ConfigurationInfo();
			configInfo.configurationId = attribute.cscfga__Product_Configuration__c;
			configInfo.configurationGuid = attribute.cscfga__Value__c;
			configInfo.inflightApplicable = isServiceInflightApplicable(
				attribute.cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,
				services
			);
			resultConfigs.put(attribute.cscfga__Value__c, configInfo);
		}

		return resultConfigs;
	}

	private Boolean isServiceInflightApplicable(Id configId, List<csord__Service__c> services) {
		for (csord__Service__c service : services) {
			if (service.csordtelcoa__Product_Configuration__c == configId) {
				return service.Applicable_for_inflight_change__c;
			}
		}
		return false;
	}
}
