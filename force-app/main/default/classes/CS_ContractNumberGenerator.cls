public with sharing class CS_ContractNumberGenerator {
	public static void generateContractNumbers(List<cscfga__Product_Configuration__c> configList) {
		if (configList.size() == 0) {
			return;
		}
		System.debug(Logginglevel.DEBUG, 'Config list: ' + configList);

		if (configList[0].cscfga__Product_Basket__r.csordtelcoa__Change_Type__c == 'Change Solution') {
			processMacdBasketConfigs(configList);
		} else {
			generateAcquisitionContractNumbers(configList);
		}
	}

	public static void processMacdBasketConfigs(List<cscfga__Product_Configuration__c> configList) {
		List<cscfga__Product_Configuration__c> macdConfigs = new List<cscfga__Product_Configuration__c>();
		List<cscfga__Product_Configuration__c> newConfigs = new List<cscfga__Product_Configuration__c>();

		System.debug(Logginglevel.DEBUG, 'MACD Config list before iteration: ');
		for (cscfga__Product_Configuration__c config : configList) {
			if (config.csordtelcoa__Replaced_Product_Configuration__c != null) {
				macdConfigs.add(config);
			} else {
				// in case parent configuration is changed, then we need to add current configuration to MACD list too
				// also, we make sure that such configuration has the same Contract Number as the parent configuration
				// use case --> addon configuration which is added in MACD basket
				if (
					config.cscfga__Parent_Configuration__c != null &&
					config.cscfga__Parent_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c != null
				) {
					config.Contract_Number__c = config.cscfga__Parent_Configuration__r.Contract_Number__c;
					config.ContractNumber_JSON__c = config.cscfga__Parent_Configuration__r.ContractNumber_JSON__c;
					macdConfigs.add(config);
				} else {
					newConfigs.add(config);
				}
			}
		}
		System.debug(Logginglevel.DEBUG, 'newConfigs: ' + newConfigs);
		System.debug(Logginglevel.DEBUG, 'macdConfigs: ' + macdConfigs);

		generateAcquisitionContractNumbers(newConfigs);
		System.debug(Logginglevel.DEBUG, 'Before sameContractConfigurations');
		List<cscfga__Product_Configuration__c> sameContractConfigurations = getSameMacdSolutionConfigurations(macdConfigs);
		if (!sameContractConfigurations.isEmpty()) {
			System.debug(Logginglevel.DEBUG, 'sameContractConfigurations: ' + sameContractConfigurations);
			macdConfigs.addAll(sameContractConfigurations);
		}
		incrementMacdContractNumber(macdConfigs);
	}

	/*
    @description generates acquisition contract numbers for configs in the list and updates configs
    */
	public static void generateAcquisitionContractNumbers(List<cscfga__Product_Configuration__c> configList) {
		if (configList.size() == 0) {
			return;
		}
		System.debug(Logginglevel.DEBUG, 'Input list: ' + configList);

		List<cscfga__Product_Configuration__c> updateList = new List<cscfga__Product_Configuration__c>();
		// all products within same Contract Number Group will receive same Contract Number
		Map<String, ProcessedContractGroupConfigs> processedConfigs = processConfigs(configList);

		System.debug(Logginglevel.DEBUG, 'Processed groups: ' + processedConfigs);
		for (String groupKey : processedConfigs.keySet()) {
			ProcessedContractGroupConfigs currentGroup = processedConfigs.get(groupKey);
			CS_ContractNumber contractNumber = null;

			try {
				// if contractNumberJson exists, reuse it --> potentially date/year can change
				if (currentGroup.contractNumberJson != null) {
					contractNumber = (CS_ContractNumber) JSON.deserialize(currentGroup.contractNumberJson, CS_ContractNumber.class);
					if (!contractNumber.isUpToDate()) {
						contractNumber.bringItUpToDate();
					}
				} else {
					contractNumber = new CS_ContractNumber(2, 1, groupKey, getNewNumber());
				}

				for (Integer i = 0; i < currentGroup.congfigList.size(); i++) {
					currentGroup.congfigList[i].ContractNumber_JSON__c = JSON.serialize(contractNumber);
					currentGroup.congfigList[i].Contract_Number__c = contractNumber.toString();
					updateList.add(currentGroup.congfigList[i]);
				}
			} catch (Exception ex) {
				System.debug(System.LoggingLevel.DEBUG, 'generateAcquisitionContractNumbers failed with error: ' + ex.getMessage());
			}
		}

		System.debug(Logginglevel.DEBUG, 'Update list: ' + updateList);
		update updateList;
	}

	public static Integer getNewNumber() {
		ContractNumberGeneration__c globalSetting = [SELECT Id, Contract_Number__c FROM ContractNumberGeneration__c LIMIT 1];
		Integer newValue = (Integer) globalSetting.Contract_Number__c;
		globalSetting.Contract_Number__c += 1;
		update globalSetting;

		return newValue;
	}

	/*
    @description Processes input configList, grouping by their Contract Number Group
    Also, checks if there is a config in a group that already has ContractNumber_JSON__c set
    If so, this string will be later applied to all configs in the group (there is no need to create new Contract Number)
    */
	private static Map<String, ProcessedContractGroupConfigs> processConfigs(List<cscfga__Product_Configuration__c> configList) {
		Map<String, ProcessedContractGroupConfigs> contractGroups = new Map<String, ProcessedContractGroupConfigs>();

		for (cscfga__Product_Configuration__c config : configList) {
			if (config.Contract_Number_Group__c != null && config.Contract_Number_Group__c != '') {
				if (contractGroups.containsKey(config.Contract_Number_Group__c)) {
					System.debug(Logginglevel.DEBUG, 'Add to existing: ' + config.Name);
					contractGroups.get(config.Contract_Number_Group__c).congfigList.add(config);

					if (
						contractGroups.get(config.Contract_Number_Group__c).contractNumberJson == null &&
						config.ContractNumber_JSON__c != null &&
						config.ContractNumber_JSON__c != ''
					) {
						contractGroups.get(config.Contract_Number_Group__c).contractNumberJson = config.ContractNumber_JSON__c;
					}
				} else {
					System.debug(Logginglevel.DEBUG, 'Create new: ' + config.Name);
					ProcessedContractGroupConfigs newGroup = new ProcessedContractGroupConfigs(config.Contract_Number_Group__c);
					newGroup.congfigList.add(config);
					if (config.ContractNumber_JSON__c != null && config.ContractNumber_JSON__c != '') {
						newGroup.contractNumberJson = config.ContractNumber_JSON__c;
					}
					contractGroups.put(config.Contract_Number_Group__c, newGroup);
				}
			}
		}
		return contractGroups;
	}

	/*
	 * @description In case we are doing a MACD, then all configurations with the same contract numbers need to have their contract version upgraded
	 * even if they are not changed in MACD themselves
	 * @returns list of configurations matching this criteria
	 */
	public static List<cscfga__Product_Configuration__c> getSameMacdSolutionConfigurations(List<cscfga__Product_Configuration__c> configList) {
		Set<String> contractNumbers = new Set<String>();
		Set<Id> processedConfigs = new Set<Id>();

		for (cscfga__Product_Configuration__c config : configList) {
			if (config.Contract_Number__c != null) {
				contractNumbers.add(config.Contract_Number__c);
			}
			processedConfigs.add(config.Id);
		}

		if (contractNumbers.isEmpty()) {
			return new List<cscfga__Product_Configuration__c>();
		}

		return [
			SELECT
				Id,
				Name,
				cscfga__Product_Basket__c,
				cscfga__Product_Definition__c,
				cscfga__Product_Definition__r.Name,
				cscfga__Quantity__c,
				Package_Name__c,
				cscfga__Product_Family__c,
				ContractNumber_JSON__c,
				Contract_Number__c,
				Contract_Number_Group__c,
				csordtelcoa__Replaced_Product_Configuration__c,
				cscfga__Parent_Configuration__c,
				cscfga__Parent_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,
				cscfga__Parent_Configuration__r.ContractNumber_JSON__c,
				cscfga__Parent_Configuration__r.Contract_Number__c
			FROM cscfga__Product_Configuration__c
			WHERE Contract_Number__c IN :contractNumbers AND Id NOT IN :processedConfigs
		];
	}

	public static void incrementMacdContractNumber(List<cscfga__Product_Configuration__c> configList) {
		for (Integer i = 0; i < configList.size(); i++) {
			incrementMacdContractNumber(configList[i]);
		}
		update configList;
	}

	/*
	 * @description increments MACD version in contract number version on a single configuration (does NOT update)
	 */
	public static void incrementMacdContractNumber(SObject config) {
		String contractNumberJson = (String) config.get('ContractNumber_JSON__c');
		if (contractNumberJson == null) {
			System.debug(Logginglevel.DEBUG, 'Unable to get ContractNumber_JSON__c from object with Id: ' + config.get('Id'));
			return;
		}

		try {
			CS_ContractNumber contractNumber = (CS_ContractNumber) JSON.deserialize(contractNumberJson, CS_ContractNumber.class);
			if (contractNumber.majorVersion == 2) {
				contractNumber.majorVersion = 3;
				contractNumber.minorVersion = 1;
				contractNumber.originalContractDate = contractNumber.contractDate;
			} else {
				contractNumber.minorVersion += 1;
			}

			contractNumber.bringItUpToDate();
			config.put('ContractNumber_JSON__c', JSON.serialize(contractNumber));
			config.put('Contract_Number__c', contractNumber.toString());
		} catch (Exception ex) {
			System.debug(System.LoggingLevel.DEBUG, 'incrementMacdContractNumber failed with error: ' + ex.getMessage());
		}
	}

	private class ProcessedContractGroupConfigs {
		public String contractNumberJson { get; set; }
		public String contracNumberGroup { get; set; }
		public List<cscfga__Product_Configuration__c> congfigList { get; set; }

		ProcessedContractGroupConfigs(String contracNumberGroup) {
			this.contracNumberGroup = contracNumberGroup;
			congfigList = new List<cscfga__Product_Configuration__c>();
		}
	}
}
