public with sharing class OrderEntryProductController {
	private static final String PRODUCT_TYPE_INTERNET = 'Internet';
	private static final String PRODUCT_TYPE_MOBILE = 'Mobile';
	private static final String PRODUCT_TYPE_MOBILE_SIM_CARD = 'Mobile SIM Card';
	private static final String PRODUCT_DETAIL_DOWNLOAD_SPEED = 'Download Speed';

	/**
	 * @description Gets Products based on Product Types and Validity Period
	 * @param  oppId        Opportunity ID (used to check validity period)
	 * @param  productTypes List of Product Types
	 * @return              List of available Products
	 */
	@AuraEnabled(cacheable=true)
	public static List<OE_Product__c> getProducts(Id oppId, List<String> productTypes) {
		String query = 'SELECT Name, Sort_Order__c, Type__c, Journey_Type__c, Max_Quantity__c, (SELECT Id, Name, Value__c FROM Order_Entry_Product_Details__r) FROM OE_Product__c';
		if (productTypes != null && productTypes.size() != 0) {
			query += ' WHERE Type__c IN :productTypes';
		}
		query += ' ORDER BY Sort_Order__c';

		List<OE_Product__c> products = Database.query(String.escapeSingleQuotes(query));
		List<OE_Product__c> validProducts = new List<OE_Product__c>();
		Set<Id> prodIds = OrderEntryValidityService.validatePeriod(oppId, products);
		for (OE_Product__c product : products) {
			if (prodIds.contains(product.Id)) {
				validProducts.add(product);
			}
		}
		return validProducts;
	}

	/**
	 * @description Gets Internet Products
	 * @param  oppId Opportunity ID
	 * @param  site  Installation Site
	 * @return       List of available Products
	 */
	@AuraEnabled(cacheable=true)
	public static List<OE_Product__c> getInternetProducts(Id oppId, OrderEntryData.OrderEntrySite site) {
		List<OE_Product__c> allInternetProducts = getProducts(oppId, new List<String>{ PRODUCT_TYPE_INTERNET });
		// Filter based on speed (if Giga is not available, offer 700)
		Boolean isGigaAvailable = site == null ? true : site.isGigaAvailable();
		List<OE_Product__c> internetProducts = new List<OE_Product__c>();
		for (OE_Product__c product : allInternetProducts) {
			Integer speed = getProductDownloadSpeed(product);
			if (isGigaAvailable) {
				if (speed != 700) {
					internetProducts.add(product);
				}
			} else {
				if (speed != 1000) {
					internetProducts.add(product);
				}
			}
		}
		return internetProducts;
	}

	/**
	 * @description Gets Download Speed for selected Internet Product
	 * @param  product Internet Product
	 * @return         Download Speed in MB/s
	 */
	private static Integer getProductDownloadSpeed(OE_Product__c product) {
		Integer speed = 0;
		for (Order_Entry_Product_Detail__c productDetail : product.Order_Entry_Product_Details__r) {
			if (productDetail.Name == PRODUCT_DETAIL_DOWNLOAD_SPEED) {
				speed = Integer.valueOf(productDetail.Value__c);
			}
		}
		return speed;
	}

	/**
	 * @description Gets product details
	 * @param  product Internet Product
	 * @return         Order_Entry_Product_Detail__c
	 */
	@AuraEnabled
	public static List<Order_Entry_Product_Detail__c> getProductDetails(Id productId) {
		List<Order_Entry_Product_Detail__c> productDetails = [
			SELECT Id, Name, Value__c
			FROM Order_Entry_Product_Detail__c
			WHERE Product__c = :productId AND Name != 'Internet'
		];

		return productDetails;
	}

	/**
	 * @description Gets TV and Telephony Products for selected Internet Product
	 * @param  oppId             Opportunity ID
	 * @param  internetProductId Internet Product ID
	 * @return                   List of TV and Telephony Products
	 */
	@AuraEnabled(cacheable=true)
	public static List<OE_Product__c> getTvAndTelephonyProducts(Id oppId, Id internetProductId) {
		List<OE_Product_Bundle__c> productBundles = [
			SELECT Id, Name, TV_Product__c, Telephony_Product__c
			FROM OE_Product_Bundle__c
			WHERE Internet_Product__c = :internetProductId
		];

		Set<Id> tvProducts = new Set<Id>();
		Set<Id> telephonyProducts = new Set<Id>();
		for (OE_Product_Bundle__c pb : productBundles) {
			tvProducts.add(pb.TV_Product__c);
			telephonyProducts.add(pb.Telephony_Product__c);
		}

		List<OE_Product__c> products = [
			SELECT Name, Sort_Order__c, Type__c
			FROM OE_Product__c
			WHERE Id IN :tvProducts OR Id IN :telephonyProducts
			ORDER BY Sort_Order__c
		];

		List<OE_Product__c> validProducts = new List<OE_Product__c>();
		Set<Id> prodIds = OrderEntryValidityService.validatePeriod(oppId, products);

		for (OE_Product__c product : products) {
			if (prodIds.contains(product.Id)) {
				validProducts.add(product);
			}
		}

		return validProducts;
	}

	/**
	 * @description Gets Mobile Products and SIM Cars
	 * @param  oppId Opportunity ID
	 * @return       List of available Products
	 */
	@AuraEnabled(cacheable=true)
	public static List<OE_Product__c> getMobileProductsAndSIMCards(Id oppId) {
		return getProducts(oppId, new List<String>{ PRODUCT_TYPE_MOBILE, PRODUCT_TYPE_MOBILE_SIM_CARD });
	}

	/**
	 * @description Gets Default Bundle for selected Main Product.
	 * @param  opportunityId Opportunity ID
	 * @param  mainProductId Main Product Id.
	 * @return               Default Bundle
	 */
	@AuraEnabled
	public static OE_Product_Bundle__c getDefaultBundle(Id opportunityId, Id mainProductId) {
		List<OE_Product_Bundle__c> bundles = [
			SELECT Name, Main_Product__c, Internet_Product__c, Telephony_Product__c, TV_Product__c, Bundle_Product_Code__c, Default__c
			FROM OE_Product_Bundle__c
			WHERE Main_Product__c = :mainProductId AND Default__c = TRUE
		];
		Set<Id> internetProductIds = new Set<Id>();
		for (OE_Product_Bundle__c pb : bundles) {
			internetProductIds.add(pb.Internet_Product__c);
		}
		Set<Id> productsIds = OrderEntryValidityService.validatePeriod(opportunityId, [SELECT Id FROM OE_Product__c WHERE Id IN :internetProductIds]);
		for (OE_Product_Bundle__c pb : bundles) {
			if (productsIds.contains(pb.Internet_Product__c)) {
				return pb;
			}
		}
		return null;
	}
	@AuraEnabled
	public static Id getDefaultSIM(List<OrderEntryData.OrderEntryProduct> products, String contractTerm) {
		Id mainId;
		Id mobileId;
		for (OrderEntryData.OrderEntryProduct product : products) {
			if (product.type == 'Main') {
				mainId = product.id;
			} else if (product.type == 'Mobile') {
				mobileId = product.id;
			}
		}
		String query =
			'SELECT Name, Bundle_Product_Code__c, Type__c, Promo_Description__c, SIM_Card_Product__c' +
			' FROM OE_Product_Bundle__c' +
			' WHERE Main_Product__c = :mainId ' +
			' AND Mobile_Product__c = :mobileId' +
			' AND Default__c = true';

		if (contractTerm != null) {
			query += ' AND Contract_Term__c INCLUDES (:contractTerm)';
		}
		List<OE_Product_Bundle__c> bundles = Database.query(query);

		if (!bundles.isEmpty()) {
			return bundles[0].SIM_Card_Product__c;
		}
		return null;
	}
	/**
	 * @description Gets bundle
	 * @param  products List<OrderEntryData.OrderEntryProduct> (selected products in OrderEntryTool)
	 * @param  contractTerm String
	 * @return        OrderEntryData.ProductBundle
	 */
	@AuraEnabled
	public static OrderEntryData.ProductBundle getBundle(List<OrderEntryData.OrderEntryProduct> products, String contractTerm) {
		// now hardcoded to 1 but can be changed in the future to something else
		Integer quantity = 1;
		Id mainId;
		Id internetId;
		Id tvId;
		Id telephonyId;
		Id mobileId;
		Id mobileSIMCardId;
		for (OrderEntryData.OrderEntryProduct product : products) {
			if (product.type == 'Main') {
				mainId = product.id;
			} else if (product.type == 'Internet') {
				internetId = product.id;
			} else if (product.type == 'TV') {
				tvId = product.id;
			} else if (product.type == 'Telephony') {
				telephonyId = product.id;
			} else if (product.type == 'Mobile') {
				mobileId = product.id;
			} else if (product.type == 'Mobile SIM Card') {
				mobileSIMCardId = product.id;
			}
		}
		String query =
			'SELECT Name, Bundle_Product_Code__c, Type__c, Promo_Description__c' +
			' FROM OE_Product_Bundle__c' +
			' WHERE Main_Product__c = :mainId ' +
			' AND Internet_Product__c = :internetId' +
			' AND TV_Product__c = :tvId' +
			' AND Telephony_Product__c = :telephonyId' +
			' AND Mobile_Product__c = :mobileId';
		if (mobileSIMCardId != null) {
			query += ' AND SIM_Card_Product__c = :mobileSIMCardId';
		}

		if (contractTerm != null) {
			query += ' AND Contract_Term__c INCLUDES (:contractTerm)';
		}
		List<OE_Product_Bundle__c> bundles = Database.query(query);
		if (bundles.size() > 0) {
			String productCode = bundles[0].Bundle_Product_Code__c;
			List<cspmb__Price_Item__c> prices = [
				SELECT Id, cspmb__One_Off_Charge__c, cspmb__Recurring_Charge__c, cspmb__Product_Definition_Name__c
				FROM cspmb__Price_Item__c
				WHERE
					cspmb__Is_Active__c = TRUE
					AND (cspmb__One_Off_Charge_External_Id__c = :productCode
					OR cspmb__Recurring_Charge_External_Id__c = :productCode)
			];

			if (prices.size() > 0) {
				OrderEntryData.ProductBundle b = new OrderEntryData.ProductBundle();
				b.code = productCode;
				b.id = bundles[0].Id;
				b.name = bundles[0].Name;
				b.quantity = quantity;
				b.oneOff = prices[0].cspmb__One_Off_Charge__c;
				b.recurring = prices[0].cspmb__Recurring_Charge__c;
				b.totalOneOff = prices[0].cspmb__One_Off_Charge__c != null ? prices[0].cspmb__One_Off_Charge__c * quantity : null;
				b.totalRecurring = prices[0].cspmb__Recurring_Charge__c != null ? prices[0].cspmb__Recurring_Charge__c * quantity : null;
				b.type = bundles[0].Type__c;
				b.bundleName = prices[0].cspmb__Product_Definition_Name__c;
				b.promoDescription = bundles[0].Promo_Description__c;
				return b;
			}

			return null;
		}

		return null;
	}

	/**
	 * @description Gets Addons for given Product
	 * @param  oppId         Opportunity ID
	 * @param  parentProduct Product Id
	 * @return               List of available Add Ons
	 */
	@AuraEnabled(cacheable=true)
	public static List<OE_Product_Add_On__c> getAddons(Id oppId, Id parentProduct) {
		List<OE_Product_Add_On__c> addons = [
			SELECT
				Id,
				Product__c,
				Sort_Order__c,
				Automatic_Add_On__c,
				Recurring_Price_Override__c,
				One_Off_Price_Override__c,
				Add_On__c,
				Add_On__r.Type__c,
				Add_On__r.Product_Code__c,
				Add_On__r.Name,
				Add_On__r.Max_Quantity__c
			FROM OE_Product_Add_On__c
			WHERE Product__c = :parentProduct
			ORDER BY Sort_Order__c
		];
		Set<Id> addonsIds = OrderEntryValidityService.validatePeriod(oppId, addons);
		List<OE_Product_Add_On__c> validAddons = new List<OE_Product_Add_On__c>();
		for (OE_Product_Add_On__c addon : addons) {
			if (addonsIds.contains(addon.Id)) {
				validAddons.add(addon);
			}
		}
		return validAddons;
	}

	/**
	 * @description Gets Price for Selected Addon
	 * @param  code Add On Code
	 * @return      Add On Price Item
	 */
	@AuraEnabled
	public static cspmb__Add_On_Price_Item__c getAddonPrice(String code) {
		if (code == null) {
			throw new AuraHandledException('No code selected!');
		}
		List<cspmb__Add_On_Price_Item__c> prices = [
			SELECT
				Id,
				Name,
				cspmb__One_Off_Charge_External_Id__c,
				cspmb__Recurring_Charge_External_Id__c,
				cspmb__One_Off_Charge__c,
				cspmb__Recurring_Charge__c,
				cspmb__Product_Definition_Name__c,
				Product_Name__c
			FROM cspmb__Add_On_Price_Item__c
			WHERE cspmb__Is_Active__c = TRUE AND (cspmb__One_Off_Charge_External_Id__c = :code OR cspmb__Recurring_Charge_External_Id__c = :code)
		];
		if (prices.size() > 0) {
			return prices[0];
		}
		return null;
	}

	/**
	 * @description Updates Opportunity Journey Type based on selected Main Product
	 * @param  oppId Opportunity ID
	 * @param  type  Main Product Journey Type
	 */
	@AuraEnabled
	public static void updateOpportunityJourneyType(Id oppId, String type) {
		Opportunity opp = [SELECT Id, Journey_Type__c FROM Opportunity WHERE Id = :oppId];
		opp.Journey_Type__c = type;
		update opp;
	}

	/**
	 * @description Saves Opportunity Line Items based on selected products
	 * @param  opportunityId Opportunity Id
	 */
	@AuraEnabled
	public static void saveOpportunityProducts(Id oppId) {
		OrderEntryData data = OrderEntryController.getOrderEntryData(oppId);
		OrderEntryProductBuilder builder = new OrderEntryProductBuilder(data);
		builder.build();
	}

	/**
	 * @description Get contract terms for Mobile products
	 * @param  products List<OE_Product__c>
	 */
	@AuraEnabled
	public static Map<String, List<String>> getMobileContractTerms(List<OE_Product__c> products) {
		Map<String, List<String>> result = new Map<String, List<String>>();
		Map<String, Set<String>> temp = new Map<String, Set<String>>();
		Set<String> productIds = new Set<String>();
		for (OE_Product__c p : products) {
			productIds.add(p.Id);
		}

		List<OE_Product_Bundle__c> productBundles = [
			SELECT Contract_Term__c, Mobile_Product__c
			FROM OE_Product_Bundle__c
			WHERE Mobile_Product__c IN :productIds
		];
		for (OE_Product_Bundle__c b : productBundles) {
			if (b.Contract_Term__c == null) {
				continue;
			}
			List<String> contractTerms = b.Contract_Term__c.split(';');
			if (!temp.containsKey(b.Mobile_Product__c)) {
				temp.put(b.Mobile_Product__c, new Set<String>());
			}
			for (String contractTerm : contractTerms) {
				temp.get(b.Mobile_Product__c).add(contractTerm);
			}
		}
		for (String productId : temp.keySet()) {
			List<String> l = new List<String>(temp.get(productId));
			l.sort();
			result.put(productId, l);
		}
		return result;
	}

	/**
	 * @description Updates Opportunity Contrac Term
	 * @param  oppId Opportunity ID
	 * @param  contractTerm  Contract term
	 */
	@AuraEnabled
	public static void updateOpportunityContractTerm(Id oppId, String contractTerm) {
		Opportunity opp = [SELECT Id, LG_ContractTermMonths__c FROM Opportunity WHERE Id = :oppId];
		opp.LG_ContractTermMonths__c = 12 * Integer.valueOf(contractTerm);
		update opp;
	}

	/**
	 * @description Get product dependencies for product bundles with products.
	 * Currently used in mobile journey.
	 */
	@AuraEnabled
	public static Map<String, List<String>> getProductDependencies() {
		Map<String, List<String>> result = new Map<String, List<String>>();

		List<OE_Product__c> children = [SELECT Id, Parent_Product__c FROM OE_Product__c WHERE Parent_Product__c != NULL];
		for (OE_Product__c c : children) {
			List<String> childProducts = result.get(c.Parent_Product__c);
			if (childProducts == null) {
				childProducts = new List<String>();
				result.put(c.Parent_Product__c, childProducts);
			}
			childProducts.add(c.Id);
		}
		return result;
	}
}
