public with sharing class ClosingOpportunityEventTriggerHandler {
	private List<Closing_Opportunity__e> events;
	private Set<Id> oppIds = new Set<Id>();
	private Boolean isAsync;

	public void afterInsert(List<Closing_Opportunity__e> events) {
		this.events = events;
		init();
		checkIsAsync();
		OpportunityClosingService svc = new OpportunityClosingService(this.oppIds);
		if (this.isAsync) {
			System.enqueueJob(svc);
		} else {
			svc.handleOpportunityClosing();
		}
	}

	private void init() {
		this.oppIds = new Set<Id>();
		for (Closing_Opportunity__e event : this.events) {
			oppIds.add(event.Opportunity__c);
		}
	}

	private void checkIsAsync() {
		List<AggregateResult> oppLineItems = [
			SELECT COUNT(Id)
			FROM OpportunityLineItem
			WHERE OpportunityId IN :oppIds
		];
		Integer oliTotalCount = (Integer) oppLineItems[0].get('expr0');
		Opportunity_Closing_Settings__c closingSettings = Opportunity_Closing_Settings__c.getInstance();
		this.isAsync = oliTotalCount > closingSettings.Opportunity_Line_Items_Limit__c;
	}
}