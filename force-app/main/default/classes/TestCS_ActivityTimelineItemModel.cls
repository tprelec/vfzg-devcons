@IsTest
private class TestCS_ActivityTimelineItemModel {
    @IsTest
    static void testBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            CS_ActivityTimelineItemModel activityTimelineItemModel = new CS_ActivityTimelineItemModel();
            activityTimelineItemModel.ActivityTimelineType = 'Test type';
            activityTimelineItemModel.Subject = 'Subject';
            activityTimelineItemModel.Detail = 'Detail';
            activityTimelineItemModel.ShortDate = '23/08/2020';
            activityTimelineItemModel.EventTime = '10';
            activityTimelineItemModel.Recipients = 'Recipients';
            activityTimelineItemModel.Assigned = 'Assigned To';
            activityTimelineItemModel.Complete = false;
            activityTimelineItemModel.ActualDate = Datetime.now();
            activityTimelineItemModel.InternalNumber = 'I-52';
            activityTimelineItemModel.Sequence = 1;
            activityTimelineItemModel.MainTask = true;

            CS_ActivityTimelineItemModel activityTimelineItemModelToCompare = new CS_ActivityTimelineItemModel();
            activityTimelineItemModelToCompare.ActivityTimelineType = 'Test type';
            activityTimelineItemModelToCompare.Subject = 'Subject';
            activityTimelineItemModelToCompare.Detail = 'Detail';
            activityTimelineItemModelToCompare.ShortDate = '23/08/2020';
            activityTimelineItemModelToCompare.EventTime = '10';
            activityTimelineItemModelToCompare.Recipients = 'Recipients';
            activityTimelineItemModelToCompare.Assigned = 'Assigned To';
            activityTimelineItemModelToCompare.Complete = false;
            activityTimelineItemModelToCompare.ActualDate = Datetime.now();
            activityTimelineItemModelToCompare.InternalNumber = 'I-53';
            activityTimelineItemModelToCompare.Sequence = 2;
            activityTimelineItemModelToCompare.MainTask = true;
            
            Integer valueIfPrimaryActivitySequenceIsSmaller = 1;
            Integer valueIfPrimaryActivitySequenceIsGreater = -1;
            
            System.assertEquals(valueIfPrimaryActivitySequenceIsSmaller, activityTimelineItemModelToCompare.compareTo(activityTimelineItemModel));
            System.assertEquals(valueIfPrimaryActivitySequenceIsGreater, activityTimelineItemModel.compareTo(activityTimelineItemModelToCompare));
        }
    }
}