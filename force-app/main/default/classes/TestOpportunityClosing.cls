@IsTest
public with sharing class TestOpportunityClosing {
	@TestSetup
	static void makeData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		TestUtils.createOpportunityFieldMappings();
		TestUtils.createCompleteOpportunity();

		// Complete Credit Check
		List<Credit_Check__c> cchecks = [SELECT Id FROM Credit_Check__c WHERE Opportunity__c = :TestUtils.theOpportunity.Id LIMIT 1];
		if (!cchecks.isEmpty()) {
			cchecks[0].Credit_Check_Status_Number__c = 2;
			update cchecks;
		}

		Site__c site = TestUtils.createSite(TestUtils.theAccount);
		Site_Postal_Check__c spc = new Site_Postal_Check__c(
			Access_Site_ID__c = TestUtils.theSite.Id,
			Access_Active__c = false,
			Access_Vendor__c = 'ZIGGO',
			Access_Result_Check__c = 'OFFNET'
		);
		insert spc;
		Site_Availability__c sa = new Site_Availability__c(
			Site__c = TestUtils.theSite.Id,
			Access_Infrastructure__c = 'Coax',
			Bandwith_Down_Entry__c = 50,
			Bandwith_Down_Premium__c = 50,
			Bandwith_Up_Entry__c = 100,
			Bandwith_Up_Premium__c = 100,
			Existing_Infra__c = false,
			Vendor__c = 'ZIGGO',
			Premium_Vendor__c = 'ZIGGO',
			Result_Check__c = 'test',
			Region__c = 'Test',
			Source_Check_Entry__c = spc.Id
		);
		insert sa;

		List<OpportunityLineItem> oliList = [SELECT Id, Site_List__c FROM OpportunityLineItem];

		for (OpportunityLineItem oli : oliList) {
			oli.Site_List__c = site.Id;
		}
		update oliList;

		Opportunity_Closing_Settings__c settings = new Opportunity_Closing_Settings__c(Opportunity_Line_Items_Limit__c = 100);
		insert settings;

		Opportunity_Attachment__c oa = new Opportunity_Attachment__c(
			Attachment_Type__c = 'Contract (Signed)',
			Opportunity__c = TestUtils.theOpportunity.Id
		);
		insert oa;

		ContentVersion cv = new ContentVersion(
			Title = 'New file 2',
			PathOnClient = 'newfile.txt',
			VersionData = Blob.valueOf('Hi!!!!'),
			FirstPublishLocationId = oa.id
		);
		insert cv;

		TestUtils.theProduct.Product_Group__c = 'SLA';
		update TestUtils.theProduct;
	}

	private static Opportunity getOpportunity() {
		return [SELECT Id, Name, Contract_VF__c, Account.Name FROM Opportunity LIMIT 1];
	}

	private static List<Contracted_Products__c> getContractedProducts() {
		return [SELECT Id FROM Contracted_Products__c];
	}

	private static List<Contract_Attachment__c> getContractAttachments() {
		return [SELECT Id FROM Contract_Attachment__c];
	}

	@IsTest
	private static void testClosing() {
		Opportunity opp = getOpportunity();

		List<OpportunityLineItem> olis = [SELECT Id, New_Portfolio__c FROM OpportunityLineItem];
		olis[0].New_Portfolio__c = true;
		update olis[0];

		Test.startTest();

		opp.StageName = 'Closed Won';
		update opp;

		Test.stopTest();

		opp = getOpportunity();
		System.assert(opp.Contract_VF__c != null, 'Contract should be created.');
		System.assert(!getContractedProducts().isEmpty(), 'Contracted Products should be created.');
		System.assert(!getContractAttachments().isEmpty(), 'Contract Attachments should be created.');
	}

	@IsTest
	private static void testCreateInitialContractTasksWithLongName() {
		Opportunity opp = getOpportunity();
		Opp.Name = '1Stichting Limburgs Voortgezet Onderwijs Samenwerkingsbestuur voor bijzonder en openbaar onderwijs - Nummerbehoud 1nr123';
		Opp.Account.Name = 'StichtingBewindvoeringClintenBartimus DEVM Heeeeeeeeeeeeeeeeeeeeel laaaaaaaaaaaa12255445445646545646454456StichtingBew';
		opp.StageName = 'Closing';
		update opp;
		update opp.Account;

		Test.startTest();
		opp.StageName = 'Closed Won';
		update opp;
		Test.stopTest();

		opp = getOpportunity();
		System.assert(opp.Contract_VF__c != null, 'Contract should be created.');
		System.assert(!getContractedProducts().isEmpty(), 'Contracted Products should be created.');
		System.assert(!getContractAttachments().isEmpty(), 'Contract Attachments should be created.');
	}

	@IsTest
	private static void testUpdateClosingErrorStatus() {
		Opportunity startingOpportunity = getOpportunity();
		startingOpportunity.StageName = 'Closed Won';

		Test.startTest();
		OpportunityClosingService opprotunityClosingService = new OpportunityClosingService(new Set<Id>{ startingOpportunity.Id });
		opprotunityClosingService.execute(null);
		opprotunityClosingService.getOpportunities();
		opprotunityClosingService.updateClosingErrorStatus();
		Test.stopTest();

		Opportunity resultingOpporunity = [SELECT Closing_Status__c FROM Opportunity WHERE Id = :startingOpportunity.Id];
		System.assertEquals('Error', resultingOpporunity.Closing_Status__c, 'Opportunity closing status should be Error');
	}
}
