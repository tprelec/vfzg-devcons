/**
 * @description         This is the trigger handler for the Dealer Information sObject.
 */
public with sharing class DealerInfoTriggerHandler extends TriggerHandler {
	/**
	 * @description         This handles the before insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void beforeInsert() {
		List<Dealer_Information__c> newDIs = (List<Dealer_Information__c>) this.newList;
		checkForExistingDI(newDIs, null);
	}

	/**
	 * @description         This handles the after insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void afterInsert() {
		List<Dealer_Information__c> newDIs = (List<Dealer_Information__c>) this.newList;

		updateDealerInfoDetails(newDIs, null);
		processNewSharingForDealer(newDIs);
		uncheckDefault(newDIs, null);
	}

	/**
	 * @description         This handles the after insert trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void beforeUpdate() {
		List<Dealer_Information__c> newDIs = (List<Dealer_Information__c>) this.newList;
		Map<Id, Dealer_Information__c> oldDIsmap = (Map<Id, Dealer_Information__c>) this.oldMap;
		checkForExistingDI(newDIs, oldDIsmap);
	}

	/**
	 * @description         This handles the after update trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void afterUpdate() {
		List<Dealer_Information__c> newDIs = (List<Dealer_Information__c>) this.newList;
		List<Dealer_Information__c> oldDIs = (List<Dealer_Information__c>) this.oldList;
		Map<Id, Dealer_Information__c> newDIsmap = (Map<Id, Dealer_Information__c>) this.newMap;
		Map<Id, Dealer_Information__c> oldDIsmap = (Map<Id, Dealer_Information__c>) this.oldMap;

		updateDealerInfoDetails(newDIs, oldDIsmap);
		updateSharingForDealer(newDIs, oldDIsmap);
		processAccountOwnerChange(newDIs, oldDIsmap);
		uncheckDefault(newDIs, oldDIsmap);
		updateRelatedRecordsOwnership(newDIs, oldDIsmap);
		sendNotificationsToManagerAfterUpdate(oldDIsmap, newDIsmap);
	}

	/**
	 * @description         Create new sharing rules if required
	 * @author              Gerhard Newman
	 */
	private void processNewSharingForDealer(List<Dealer_Information__c> newDIs) {
		List<Id> dealerUserIDs = new List<Id>();
		List<Dealer_Information__c> diWithParent = new List<Dealer_Information__c>();
		List<String> parentCodeList = new List<String>();
		Map<Id, Id> groupMembersForInsertMap = new Map<Id, Id>();

		for (Dealer_Information__c di : newDIs) {
			// does the dealer have a contact (if they don't no sharing rules are required)
			if (di.Contact__c != null && di.Is_Dealer_in_SFDC__c && di.Status__c == 'Active') {
				dealerUserIDs.add(di.ContactUserId__c);
				// Check if this dealer has a parent dealer
				if (di.HasParent__c) {
					// Add to list to check for existence of parent group
					diWithParent.add(di);
					parentCodeList.add(di.Parent_Dealer_Code__c);
				}
			}
		}
		// Create map of users to retrieve developer name (User must be active!)
		Map<Id, User> userMap = new Map<Id, User>([SELECT UserRole.DeveloperName FROM User WHERE Id IN :dealerUserIDs AND isActive = TRUE]);

		// Retreive Name of Parents and build list
		// If the parent does not have a contact no sharing is required
		List<Dealer_Information__c> parentDealerList = new List<Dealer_Information__c>(
			[
				SELECT Name, Dealer_Code__c, ContactUserId__c
				FROM Dealer_Information__c
				WHERE Dealer_Code__c IN :parentCodeList AND Contact__c != NULL AND Is_Dealer_in_SFDC__c = TRUE
			]
		);

		Set<Id> parentUsers = new Set<Id>();
		List<String> diWithParentName = new List<String>();
		Map<String, Dealer_Information__c> parentDealerMapByDealerCode = new Map<String, Dealer_Information__c>();
		for (Dealer_Information__c di : parentDealerList) {
			diWithParentName.add(groupName(di.Name));
			parentDealerMapByDealerCode.put(di.Dealer_Code__c, di);
			parentUsers.add(di.ContactUserId__c);
		}
		// Get the developernames for use in creating heirarchy shares
		Map<Id, User> parentUserMap = new Map<Id, User>([SELECT UserRole.DeveloperName FROM User WHERE Id IN :parentUsers]);

		// Retrieve parent groups
		List<Group> partnerGroups = [SELECT Name FROM Group WHERE Name IN :diWithParentName];
		Map<String, Group> partnerGroupMap = new Map<String, Group>();
		for (Group g : partnerGroups) {
			partnerGroupMap.put(g.Name, g);
		}

		// Loop to see if any parents are missing groups
		List<Group> groupsForInsert = new List<Group>();
		for (Dealer_Information__c di : parentDealerList) {
			if (partnerGroupMap.get(groupName(di.Name)) == null) {
				Group g = new Group();
				g.Name = groupName(di.Name);
				g.Type = 'Regular';
				groupsForInsert.add(g);
			}
		}

		if (!groupsForInsert.isEmpty()) {
			insert groupsForInsert;
			// Refresh the list to retrieve the developername of the groups
			groupsForInsert = [SELECT Id, Name, DeveloperName FROM Group WHERE Id IN :groupsForInsert];
			// Now insert the role of the parent user into the group
			// and add the new group into the partnerGroupMap
			List<String> dealerNamesGroupsForH = new List<String>();
			for (Group g : groupsForInsert) {
				partnerGroupMap.put(g.Name, g);

				for (Dealer_Information__c di : parentDealerList) {
					// Find the group/dealer information match
					if (g.Name == groupName(di.name)) {
						groupMembersForInsertMap.put(di.ContactUserId__c, g.Id);
						dealerShareHelper dsh = new dealerShareHelper(parentUserMap.get(di.ContactUserId__c).UserRole.DeveloperName, g.DeveloperName);
						dealerNamesGroupsForH.add(JSON.serialize(dsh));
					}
				}
			}
			// Now create the hierarchy shares for these new groups
			createDealerSharingOwnerRule('Lead', dealerNamesGroupsForH, 'Edit', UserInfo.getSessionId(), 'hierarchy');
			createDealerSharingOwnerRule('Opportunity', dealerNamesGroupsForH, 'Edit', UserInfo.getSessionId(), 'hierarchy');
			createDealerSharingOwnerRule('BAN__c', dealerNamesGroupsForH, 'Read', UserInfo.getSessionId(), 'hierarchy');
			createDealerSharingOwnerRule('Competitor_Asset__c', dealerNamesGroupsForH, 'Edit', UserInfo.getSessionId(), 'hierarchy');
			createDealerSharingOwnerRule('VF_Contract__c', dealerNamesGroupsForH, 'Edit', UserInfo.getSessionId(), 'hierarchy');
			createDealerSharingOwnerRule('Bigmachines__Quote__c', dealerNamesGroupsForH, 'Edit', UserInfo.getSessionId(), 'hierarchy');
			createDealerSharingOwnerRule('cscfga__Product_Basket__c', dealerNamesGroupsForH, 'Edit', UserInfo.getSessionId(), 'hierarchy');
			createDealerSharingOwnerRule('cscfga__Product_Configuration__c', dealerNamesGroupsForH, 'Edit', UserInfo.getSessionId(), 'hierarchy');

			// These two sharing rules must be run a little in the future as only one sharing rule can be processed at a time
			// (Flat level sharing is also done for these two objects)
			Datetime startProcess = Datetime.now().addMinutes(2);
			SharingOwnerRuleSchedulable schedClass = new SharingOwnerRuleSchedulable(
				'User',
				dealerNamesGroupsForH,
				'Read',
				UserInfo.getSessionId(),
				'hierarchy'
			);
			String schedTime = cronFormat(startProcess);
			System.schedule(this.generateUniqueScheduleJobName('UserHeirachyShare'), schedTime, schedClass);

			schedClass = new SharingOwnerRuleSchedulable('Account', dealerNamesGroupsForH, 'Edit', UserInfo.getSessionId(), 'hierarchy');
			System.schedule(this.generateUniqueScheduleJobName('AccountHeirachyShare'), schedTime, schedClass);
		}

		// Add the new child partner role into the group
		// only looping on the di's that have parents
		for (Dealer_Information__c di : diWithParent) {
			// Get the parent partner DI
			Dealer_Information__c parentDi = parentDealerMapByDealerCode.get(di.Parent_Dealer_Code__c);
			if (parentDi != null) {
				// Get the parent partner group
				Group g = partnerGroupMap.get(groupName(parentDi.Name));
				if (g != null) {
					// Insert a new group member
					groupMembersForInsertMap.put(di.ContactUserId__c, g.Id);
				}
			}
		}

		if (!groupMembersForInsertMap.isEmpty() && !System.isBatch()) {
			creatGroupMembers(groupMembersForInsertMap);
		}

		// Retrieve the Dealer Account Roles that needs to be shared to themselves for Account and User
		List<String> dealerNames = new List<String>();
		List<String> dealerNamesGroups = new List<String>();
		for (ID key : userMap.keySet()) {
			User u = userMap.get(key);
			dealerNames.add(u.UserRole.DeveloperName);
			dealerShareHelper dsh = new dealerShareHelper(u.UserRole.DeveloperName, '');
			dealerNamesGroups.add(JSON.serialize(dsh));
		}

		if (!dealerNames.isEmpty() && !System.isBatch()) {
			createDealerSharingOwnerRule('Account', dealerNamesGroups, 'Edit', UserInfo.getSessionId(), 'flat');
			createDealerSharingOwnerRule('User', dealerNamesGroups, 'Read', UserInfo.getSessionId(), 'flat');
		}
	}

	public static string cronFormat(Datetime dt) {
		return '0 ' + dt.minute() + ' ' + dt.hour() + ' ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
	}

	/**
	 * @description         Returns the name of the partner group. Maximum length is 40 characters
	 *
	 * @author              Gerhard Newman
	 */
	private static String groupName(String partnerName) {
		return 'PMs:' + partnerName.left(36);
	}

	// Inserts groupmembers into group for hierarchy sharing
	@future
	private static void creatGroupMembers(Map<Id, Id> groupMembers) {
		List<GroupMember> groupMembersForInsert = new List<GroupMember>();

		Map<Id, User> users = new Map<Id, User>([SELECT UserRoleId FROM User WHERE Id = :groupMembers.keySet()]);
		Set<Id> roleSet = GeneralUtils.getIDSetFromList(users.values(), 'UserRoleId');
		List<Group> roleGroups = [SELECT Id, RelatedId FROM Group WHERE Type = 'RoleAndSubordinates' AND RelatedId IN :roleSet];
		Map<Id, Group> relatedRoleToGroup = new Map<Id, Group>();
		for (Group g : roleGroups) {
			relatedRoleToGroup.put(g.RelatedId, g);
		}

		for (Id key : groupMembers.keySet()) {
			GroupMember newMember = new GroupMember();
			newMember.GroupId = groupMembers.get(key);

			Id theRole = users.get(key).UserRoleId;
			Group roleGroup = relatedRoleToGroup.get(theRole);
			newMember.UserOrGroupId = roleGroup.Id;
			groupMembersForInsert.add(newMember);
		}

		insert groupMembersForInsert;
	}

	/**
	 * @description         Wrapper used for dealer sharing rules
	 *
	 * @author              Gerhard Newman
	 */
	public class DealerShareHelper {
		public String dealerRoleName { get; set; }
		public String groupName { get; set; }

		public DealerShareHelper(String d, String g) {
			dealerRoleName = d;
			groupName = g;
		}
	}

	/**
	 * @description         Creates a sharing rule to the object specified using the metadata API
	 *                      The RoleName is used to create the share
	 * @author              Gerhard Newman
	 */
	public static void createDealerSharingOwnerRuleHelper(
		String objectName,
		List<String> dshList,
		String accessLevel,
		String sessionId,
		String shareType
	) {
		MetadataService.MetadataPort service = new MetadataService.MetadataPort();
		service.SessionHeader = new MetadataService.SessionHeader_element();
		service.SessionHeader.sessionId = SessionId;
		service.timeout_x = 45000;

		MetadataService.AccountSharingRuleSettings accountSettings = new MetadataService.AccountSharingRuleSettings();
		// Only required for the account object
		if (objectName == 'Account') {
			accountSettings.caseAccessLevel = 'None';
			accountSettings.contactAccessLevel = 'Edit';
			accountSettings.opportunityAccessLevel = 'None';
		}

		List<MetadataService.SharingOwnerRule> sorList = new List<MetadataService.SharingOwnerRule>();

		for (String ser : dshList) {
			dealerShareHelper dsh = (dealerShareHelper) JSON.deserialize(ser, dealerShareHelper.class);
			String dealerName = dsh.dealerRoleName;
			String groupName = dsh.groupName;

			MetadataService.SharedTo toRole = new MetadataService.SharedTo();
			List<String> portalToRoleList = new List<String>();
			portalToRoleList.add(dealerName); // This is the name of the role
			toRole.portalRoleAndSubordinates = portalToRoleList;

			MetadataService.SharedTo fromRole = new MetadataService.SharedTo();
			List<String> fromRoleList = new List<String>();
			String nameSuffix;
			if (shareType == 'flat') {
				fromRoleList.add(dealerName);
				fromRole.portalRoleAndSubordinates = fromRoleList;
				nameSuffix = 'F';
			} else {
				fromRoleList.add(groupName);
				fromRole.group_x = fromRoleList;
				nameSuffix = 'H';
			}

			MetadataService.SharingOwnerRule accountOwnerRule = new MetadataService.SharingOwnerRule();
			accountOwnerRule.fullName = objectName + '.VFDLR' + nameSuffix + dealerName;
			accountOwnerRule.accessLevel = accessLevel;
			if (objectName == 'Account') {
				accountOwnerRule.accountSettings = accountSettings;
			}
			accountOwnerRule.label = 'VFDLR' + nameSuffix + dealerName;
			accountOwnerRule.sharedTo = toRole;
			accountOwnerRule.sharedFrom = fromRole;
			accountOwnerRule.description = 'Do not delete.';

			sorList.add(accountOwnerRule);
		}

		MetadataService.SaveResult[] results;

		if (Test.isRunningtest()) {
			results = new List<MetadataService.SaveResult>();
		} else {
			results = service.createMetadata(sorList);
		}

		for (MetadataService.SaveResult r : results) {
			if (r.Success) {
				System.Debug(logginglevel.INFO, 'Created component: ' + r.FullName);
			} else {
				System.Debug(logginglevel.INFO, 'Errors were encountered while creating ' + r.FullName);
				for (MetadataService.Error e : r.Errors) {
					System.debug(logginglevel.INFO, 'Error message: ' + e.Message);
					System.debug(logginglevel.INFO, 'Status code: ' + e.StatusCode);
				}
			}
		}
	}

	/**
	 * @description         Creates a sharing rule to the object specified using the metadata API
	 *                      The RoleName is used to create the share
	 * @author              Gerhard Newman
	 */
	@future(callout=true)
	private static void createDealerSharingOwnerRule(
		String objectName,
		List<String> dshList,
		String accessLevel,
		String sessionId,
		String shareType
	) {
		createDealerSharingOwnerRuleHelper(objectName, dshList, accessLevel, sessionId, shareType);
	}

	/**
	 * @description         Creates/Deletes sharing rules as required
	 * @author              Gerhard Newman
	 */
	private void updateSharingForDealer(List<Dealer_Information__c> newDIs, Map<Id, Dealer_Information__c> oldDIsmap) {
		List<Id> dealerContactIDs = new List<Id>();
		List<Id> dealerUserIDs = new List<Id>();
		List<Dealer_Information__c> expiredDealers = new List<Dealer_Information__c>();
		Map<Id, Id> groupMembersForDeleteMap = new Map<Id, Id>();
		List<String> parentCodeList = new List<String>();
		List<Dealer_Information__c> newDealers = new List<Dealer_Information__c>();
		Set<String> dealerCodesInUse = new Set<String>(); // prevent any group for this dealer being deleted
		Set<Id> dealerGroupsInUse = new Set<Id>(); // prevent this group being deleted

		for (Dealer_Information__c di : newDIs) {
			// check if the dealer is no longer active or if the contact has been removed
			// or if the contact has changed
			// or if the parent dealer code has changed
			// These conditions handle removal/deletion
			if (
				((di.Status__c != oldDIsmap.get(di.Id).Status__c &&
				di.Status__c == 'Expired') ||
				di.Contact__c != oldDIsmap.get(di.Id).Contact__c ||
				di.Parent_Dealer_Code__c != oldDIsmap.get(di.Id).Parent_Dealer_Code__c) && oldDIsmap.get(di.Id).Contact__c != null
			) {
				// does the dealer have a contact (if they don't no sharing rules need deleting)
				dealerContactIDs.add(oldDIsmap.get(di.Id).Contact__c);
				expiredDealers.add(oldDIsmap.get(di.Id));
				parentCodeList.add(oldDIsmap.get(di.Id).Parent_Dealer_Code__c);
			}

			// Check if the contact has changed,
			// or if the dealer has become active
			// or if the parent dealer has changed
			// This condition handles adding of the contact
			if (
				(di.Contact__c != null &&
				di.Contact__c != oldDIsmap.get(di.Id).Contact__c) ||
				(di.Status__c == 'Active' &&
				di.Status__c != oldDIsmap.get(di.Id).Status__c) ||
				(di.Parent_Dealer_Code__c != null &&
				di.Parent_Dealer_Code__c != oldDIsmap.get(di.Id).Parent_Dealer_Code__c)
			) {
				newDealers.add(di);
				dealerCodesInUse.add(di.Parent_Dealer_Code__c);
			}
		}

		// Insert any new dealer shares required
		if (!newDealers.isEmpty()) {
			processNewSharingForDealer(newDealers);
		}

		// Retreive Name of Parents and build list
		// If the parent does not have a contact no sharing is required
		List<Dealer_Information__c> parentDealerList = new List<Dealer_Information__c>(
			[
				SELECT Name, Dealer_Code__c, ContactUserId__c
				FROM Dealer_Information__c
				WHERE Dealer_Code__c IN :parentCodeList AND Contact__c != NULL AND Is_Dealer_in_SFDC__c = TRUE
			]
		);

		Set<Id> parentUsers = new Set<Id>();
		List<String> diWithParentName = new List<String>();
		Map<String, Dealer_Information__c> parentDealerMapByDealerCode = new Map<String, Dealer_Information__c>();
		for (Dealer_Information__c di : parentDealerList) {
			diWithParentName.add(groupName(di.name));
			parentDealerMapByDealerCode.put(di.Dealer_Code__c, di);
			parentUsers.add(di.ContactUserId__c);
		}
		// Get the developernames for use in creating heirarchy shares
		Map<Id, User> parentUserMap = new Map<Id, User>([SELECT UserRole.DeveloperName FROM User WHERE Id IN :parentUsers]);
		Map<Id, String> groupDeveloperNameMap = new Map<Id, String>();

		// Retrieve parent groups
		List<Group> partnerGroups = [SELECT Name FROM Group WHERE Name IN :diWithParentName];
		Map<String, Group> partnerGroupMap = new Map<String, Group>();
		for (Group g : partnerGroups) {
			partnerGroupMap.put(g.name, g);
		}

		// Retrieve only matching contacts that belong to dealer accounts
		Map<Id, contact> dealerContactMap = new Map<Id, Contact>(
			[SELECT Id, Account.Id FROM Contact WHERE Id IN :dealerContactIDs AND Account.Type = 'Dealer']
		);
		Map<Id, Id> mapForDelete = new Map<Id, Id>();

		// Get active contact users on partner account
		// Add entries to mapForDelete (this is a map of Dealer Information and matching Partner Account)
		List<Id> dealerAccountIDs = new List<Id>();
		for (Id idKey : dealerContactMap.keySet()) {
			Contact c = dealerContactMap.get(idKey);
			dealerAccountIDs.add(c.Account.Id);
			for (Dealer_Information__c di : expiredDealers) {
				if (di.contact__c == c.Id) {
					mapForDelete.put(di.Id, c.Account.Id);
				}
			}
		}

		Map<Id, Contact> broadDealerContactMap = new Map<Id, Contact>(
			[SELECT Id, Account.Id FROM Contact WHERE Account.Id IN :dealerAccountIDs AND Status_Contact__c = 'Active SF User']
		);

		// Get any active dealer_informations that use that partner account
		List<Dealer_Information__c> activeDealerList = [
			SELECT Id, Contact__c
			FROM Dealer_Information__c
			WHERE Status__c = 'Active' AND Contact__c IN :broadDealerContactMap.keySet()
		];
		// Add entries to mapStillInUse (this is a map of Dealer Information and matching Partner Account)
		Map<Id, Id> mapStillInUse = new Map<Id, Id>();
		for (Dealer_Information__c di : activeDealerList) {
			if (broadDealerContactMap.get(di.Contact__c) != null) {
				mapStillInUse.put(di.Id, broadDealerContactMap.get(di.Contact__c).Account.Id);
			}
		}

		for (Dealer_Information__c di : expiredDealers) {
			// check if the expired dealer has a contact that belongs to a dealer acoount
			if (dealerContactMap.get(di.Contact__c) != null) {
				// Now make sure that there is no other dealer information that is still using that dealer account
				Boolean okToDelete = true;
				for (Id did : mapForDelete.keySet()) {
					Id dealerAccount = mapStillInUse.get(di.Id);
					if (mapForDelete.get(did) == dealerAccount) {
						okToDelete = false;
					}
				}
				if (okToDelete) {
					dealerUserIDs.add(di.ContactUserId__c);
				}
			}
			// delete dealer from group
			// Get the parent partner DI
			Dealer_Information__c parentDi = parentDealerMapByDealerCode.get(di.Parent_Dealer_Code__c);
			if (parentDi != null) {
				// Get the parent partner group
				Group g = partnerGroupMap.get(groupName(parentDi.Name));
				if (g != null) {
					// Add to list for group member deletion
					groupMembersForDeleteMap.put(di.ContactUserId__c, g.Id);
					// If the dealer is in use, then set the group to be in use to prevent deletion
					if (dealerCodesInUse.contains(di.Parent_Dealer_Code__c)) {
						dealerGroupsInUse.add(g.Id);
					}
				}
			}
		}

		// Delete the group members
		if (!groupMembersForDeleteMap.isEmpty() && !System.isBatch()) {
			deleteGroupMembers(groupMembersForDeleteMap, dealerGroupsInUse);
		}

		// Retrieve the Dealer Account Roles that have been shared to themselves for Account and User
		List<User> userList = [SELECT UserRole.DeveloperName FROM User WHERE Id IN :dealerUserIDs];
		List<String> dealerNames = new List<String>();
		for (User u : userList) {
			dealerNames.add('Account.VFDLRF' + u.UserRole.DeveloperName);
			dealerNames.add('User.VFDLRF' + u.UserRole.DeveloperName);
		}

		if (!dealerNames.isEmpty() && !System.isBatch()) {
			deleteDealerSharingOwnerRule(dealerNames, UserInfo.getSessionId());
		}
	}

	/**
	 * @description         Deletes groupmembers from hierarchy sharing group
	 *                      <ID=UserID> <ID=groupID>
	 * @author              Gerhard Newman
	 */
	@future
	private static void deleteGroupMembers(Map<Id, Id> groupMembers, Set<Id> dealerGroupsInUse) {
		List<GroupMember> groupMembersForDelete = new List<GroupMember>();
		Map<Id, Integer> groupSizeMap = new Map<Id, Integer>(); // ID of group, number of group members
		Integer groupSize;
		for (Id key : groupMembers.keySet()) {
			Id theRole = [SELECT UserRole.Id FROM User WHERE Id = :key].UserRole.Id;
			Group roleGroup;
			try {
				roleGroup = [SELECT Id FROM Group WHERE Type = 'RoleAndSubordinates' AND relatedId = :theRole];
			} catch (Exception e) {
				// This can occur if contact has been disabled before removing from Dealer Information
				System.debug(logginglevel.INFO, 'Group not found for relatedid:' + theRole);
			}
			List<GroupMember> groupMemberList = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId = :groupMembers.get(key)];
			Map<Id, GroupMember> groupMemberByUser = new Map<Id, GroupMember>();
			for (GroupMember gm : groupMemberList) {
				groupMemberByUser.put(gm.UserOrGroupId, gm);
			}
			// Check for entry in groupSizeMap - store current size. If not found create new entry and store new size
			if (groupSizeMap.get(groupMembers.get(key)) != null) {
				groupSize = groupSizeMap.get(groupMembers.get(key));
			} else {
				groupSize = groupMemberList.size();
			}
			groupSize--;
			groupSizeMap.put(groupMembers.get(key), groupSize);
			// Make sure there is a roleGroup
			if (roleGroup != null) {
				groupMembersForDelete.add(groupMemberByUser.get(roleGroup.Id));
			}
		}

		if (!groupMembersForDelete.isEmpty()) {
			delete groupMembersForDelete;
		}

		// Which groups only have 1 member left? These need to be deleted and have the sharing heirarchy rules deleted
		Set<Id> groupsForDelete = new Set<Id>();
		for (Id key : groupSizeMap.keySet()) {
			if (groupSizeMap.get(key) <= 1) {
				// Make sure the group is not still in use
				if (!dealerGroupsInUse.contains(key)) {
					groupsForDelete.add(key);
				}
			}
		}

		if (!groupsForDelete.isEmpty()) {
			// Deleting the hierarchy shares happens automatically when deleting theg group
			List<Group> groupsWithDeveloperName = [SELECT Name, DeveloperName FROM Group WHERE Id IN :groupsForDelete];
			delete groupsWithDeveloperName;
		}
	}

	/**
	 * @description         Deletes sharing rules using the metadata API
	 *
	 * @author              Gerhard Newman
	 */
	@future(callout=true)
	private static void deleteDealerSharingOwnerRule(List<String> dealerRoleNames, String sessionId) {
		MetadataService.MetadataPort service = new MetadataService.MetadataPort();
		service.SessionHeader = new MetadataService.SessionHeader_element();
		service.SessionHeader.sessionId = SessionId;
		service.timeout_x = 45000;

		List<MetadataService.DeleteResult> results;

		if (Test.isRunningtest()) {
			results = new List<MetadataService.DeleteResult>();
		} else {
			results = service.deleteMetadata('SharingOwnerRule', dealerRoleNames);
		}

		for (MetadataService.DeleteResult r : results) {
			if (r.Success) {
				System.Debug(logginglevel.INFO, 'Deleted component: ' + r.FullName);
			} else {
				System.Debug(logginglevel.INFO, 'Errors were encountered while deleting ' + r.FullName);
				for (MetadataService.Error e : r.Errors) {
					System.debug(logginglevel.INFO, 'Error message: ' + e.Message);
					System.debug(logginglevel.INFO, 'Status code: ' + e.StatusCode);
				}
			}
		}
	}

	private void updateDealerInfoDetails(List<Dealer_Information__c> newDIs, Map<Id, Dealer_Information__c> oldDIsmap) {
		List<Id> dealerInfosChanged = new List<Id>();

		for (Dealer_Information__c di : newDIs) {
			if (oldDIsmap == null) {
				//inserts
				dealerInfosChanged.add(di.Id);
			} else if (newDIs == null) {
				//deletes
				dealerInfosChanged.add(di.Id);
			} else {
				//updates, if one of the fields changed
				if (
					di.Contact__c != oldDIsmap.get(di.Id).Contact__c ||
					di.Major_Account_Manager_SoHo__c != oldDIsmap.get(di.Id).Major_Account_Manager_SoHo__c
				) {
					dealerInfosChanged.add(di.Id);
				}
			}
		}

		PostalCodeAssignmentHelper.updateDealerInfosFromPostalCodeAssignments(dealerInfosChanged);
	}

	/**
	 * @description         When the contact on the dealer changes the ownership of accounts owned by that DEALER
	 *                      must change to the new contact on the dealer.
	 * @author              Gerhard Newman
	 */
	private void processAccountOwnerChange(List<Dealer_Information__c> newDIs, Map<Id, Dealer_Information__c> oldDIsmap) {
		Map<Id, Id> oldToNewUserMap = new Map<Id, Id>();
		Set<Id> contactSet = new Set<Id>();
		Map<Id, Dealer_Information__c> dealerMap = new Map<Id, Dealer_Information__c>();

		for (Dealer_Information__c di : newDIs) {
			// Check if the contact has changed (if changing to null nothing needs to happen)
			if (di.Contact__c != null && di.Contact__c != oldDIsmap.get(di.Id).Contact__c) {
				if (!contactSet.contains(oldDIsmap.get(di.Id).Contact__c)) {
					contactSet.add(oldDIsmap.get(di.Id).Contact__c);
				}
				if (!contactSet.contains(di.Contact__c)) {
					contactSet.add(di.Contact__c);
				}
				dealerMap.put(di.Id, di);
			}
		}

		Map<Id, Contact> contactMap = new Map<Id, Contact>([SELECT Id, UserId__c FROM Contact WHERE Id IN :contactSet]);
		for (Dealer_Information__c di : newDIs) {
			// Check if the contact has changed (if changing to null nothing needs to happen)
			if (di.Contact__c != null && di.Contact__c != oldDIsmap.get(di.Id).Contact__c && oldToNewUserMap.get(di.Id) == null) {
				oldToNewUserMap.put(di.Id, contactMap.get(di.Contact__c).UserId__c);
			}
		}

		// Retrieve the accounts owned by the dealer
		List<Account> accountsForUpdate = [SELECT Id, Mobile_Dealer__c, OwnerId FROM Account WHERE Mobile_Dealer__c IN :dealerMap.keySet()];
		for (Account a : accountsForUpdate) {
			// Set the owner to the new contact
			a.OwnerId = oldToNewUserMap.get(a.Mobile_Dealer__c);
		}

		// If this is a bulk update disable the createMainSite method to avoid SOQL 101 error
		TriggerSettings__c config = TriggerSettings__c.getOrgDefaults();
		if (accountsForUpdate.size() > 100) {
			config.createMainSite_Enabled__c = false;
			upsert config;
		}
		update accountsForUpdate;

		//Reset the flag to enable createMainSite
		if (accountsForUpdate.size() > 100) {
			config.createMainSite_Enabled__c = true;
			upsert config;
		}
	}

	/**
	 * @description         When the default checkbox is set, uncheck default on any other dealer that has the same user
	 * @author              Gerhard Newman
	 */
	private void uncheckDefault(List<Dealer_Information__c> newDIs, Map<Id, Dealer_Information__c> oldDIsmap) {
		Set<Id> userSet = new Set<Id>();
		// populate the userSet with all users who have just had their default dealer set
		// for these users there should be no other dealer records with default being true
		for (Dealer_Information__c di : newDIs) {
			Boolean justSet = false;
			// See if the default flag has just been set
			if (di.Default__c && di.ContactUserId__c != null) {
				if (oldDIsmap == null) {
					justSet = true;
					// Default just set, or contact has changed (and default is set to true)
				} else if (!oldDIsmap.get(di.Id).Default__c || di.ContactUserId__c != oldDIsmap.get(di.Id).ContactUserId__c) {
					justSet = true;
				}
				if (justSet) {
					userSet.add(di.ContactUserId__c);
				}
			}
		}

		if (!userSet.isEmpty()) {
			// Retrieve dealer records for these users that are not part of this update where they are currently marked as default
			List<Dealer_Information__c> dealersToUncheck = [
				SELECT Id
				FROM Dealer_Information__c
				WHERE ContactUserId__c IN :userSet AND Id NOT IN :newDis AND Default__c = TRUE
			];
			if (!dealersToUncheck.isEmpty()) {
				for (Dealer_Information__c di : dealersToUncheck) {
					di.default__c = false;
				}
				update dealersToUncheck;
			}
		}
	}

	private void checkForExistingDI(List<Dealer_Information__c> newDIs, Map<Id, Dealer_Information__c> oldDIsmap) {
		// First map the to-be situation
		Map<Id, Id> contactMap = new Map<Id, Id>();
		for (Dealer_Information__c di : newDIs) {
			contactMap.put(di.Id, di.Contact__c);
		}
		List<Id> contactList = contactMap.values();
		Map<Id, Id> acctMap = new Map<Id, Id>();
		for (Contact c : [
			SELECT Id, Userid__r.AccountId
			FROM Contact
			WHERE Userid__c != NULL AND Userid__r.AccountId != NULL AND Id IN :contactList
		]) {
			acctMap.put(c.Id, c.Userid__r.AccountId);
		}
		Map<Id, Id> dealerInfoAccountsMap = new Map<Id, Id>();
		for (Dealer_Information__c di : newDIs) {
			dealerInfoAccountsMap.put(di.Id, acctMap.get(contactMap.get(di.Id)));
		}
		List<Id> dealerInfoAccounts = dealerInfoAccountsMap.values();
		// Then list the existing situation
		List<Dealer_Information__c> existingDIList = [
			SELECT Id, Contact__r.Userid__r.AccountId, Dealer_Code__c, Name
			FROM Dealer_Information__c
			WHERE Status__c = 'Active' AND Contact__r.Userid__r.AccountId != NULL AND Contact__r.Userid__r.AccountId IN :dealerInfoAccounts
		];
		Set<Id> existingDIAccounts = new Set<Id>();
		Map<Id, String> dealerCodeMap = new Map<Id, String>();
		for (Dealer_Information__c edi : existingDIList) {
			existingDIAccounts.add(edi.Contact__r.Userid__r.AccountId);
			dealerCodeMap.put(edi.Contact__r.Userid__r.AccountId, edi.Dealer_Code__c + ' (' + edi.Name + ')');
		}

		// Finally compare the both
		for (Dealer_Information__c di : newDIs) {
			if (oldDIsmap == null || (oldDIsmap.get(di.Id) != null && di.Contact__c != oldDIsmap.get(di.Id).Contact__c)) {
				if (existingDIAccounts.contains(dealerInfoAccountsMap.get(di.Id))) {
					di.addError(
						'This Contact (and/or the related Account) is already attached to another Dealer Code: ' +
						dealerCodeMap.get(dealerInfoAccountsMap.get(di.Id))
					);
				}
			}
		}
	}

	private void updateRelatedRecordsOwnership(List<Dealer_Information__c> newDIs, Map<Id, Dealer_Information__c> oldDIsmap) {
		Map<Id, Dealer_Information__c> arDealers = new Map<Id, Dealer_Information__c>();
		Set<Id> newARDealer = new Set<Id>();
		Map<Id, Dealer_Information__c> coDealers = new Map<Id, Dealer_Information__c>();
		Map<Id, Dealer_Information__c> oppDealers = new Map<Id, Dealer_Information__c>();
		Set<Id> newOppDealer = new Set<Id>();
		Map<Id, Dealer_Information__c> pcaDealers = new Map<Id, Dealer_Information__c>();

		for (Dealer_Information__c di : newDIs) {
			if (di.New_Account_Responsible__c != null && GeneralUtils.isRecordFieldChanged(di, oldDIsmap, 'New_Account_Responsible__c')) {
				arDealers.put(di.Id, di);
				newARDealer.add(di.New_Account_Responsible__c);
			}
			if (di.New_Contract_Owner__c != null && GeneralUtils.isRecordFieldChanged(di, oldDIsmap, 'New_Contract_Owner__c')) {
				coDealers.put(di.Id, di);
			}
			if (di.New_Opportunity_Owner__c != null && GeneralUtils.isRecordFieldChanged(di, oldDIsmap, 'New_Opportunity_Owner__c')) {
				oppDealers.put(di.Id, di);
				newOppDealer.add(di.New_Opportunity_Owner__c);
			}
			if (
				di.New_Postal_code_Area_Responsible__c != null &&
				GeneralUtils.isRecordFieldChanged(di, oldDIsmap, 'New_Postal_code_Area_Responsible__c')
			) {
				pcaDealers.put(di.Id, di);
			}
		}

		Map<Id, Dealer_Information__c> dealerInfos = new Map<Id, Dealer_Information__c>();
		if (!newARDealer.isEmpty() || newOppDealer.isEmpty()) {
			dealerInfos = new Map<Id, Dealer_Information__c>(
				[SELECT Id, ContactUserId__c FROM Dealer_Information__c WHERE Id IN :newARDealer OR Id IN :newOppDealer]
			);
		}

		// do updates
		if (!arDealers.isEmpty()) {
			updateARRecords(arDealers, dealerInfos);
		}
		if (!coDealers.isEmpty()) {
			updateCORecords(coDealers);
		}
		if (!oppDealers.isEmpty()) {
			updateOPPRecords(oppDealers, dealerInfos);
		}
		if (!pcaDealers.isEmpty()) {
			updatePCARecords(pcaDealers);
		}
	}

	private void updateARRecords(Map<Id, Dealer_Information__c> dealerIds, Map<Id, Dealer_Information__c> dealerInfos) {
		Map<Id, Account> accToUpdate = new Map<Id, Account>();

		for (Account a : [
			SELECT Mobile_Dealer__c, Fixed_Dealer__c
			FROM Account
			WHERE Mobile_Dealer__c IN :dealerIds.keySet() OR Fixed_Dealer__c IN :dealerIds.keySet()
		]) {
			if (
				a.Mobile_Dealer__c != null &&
				dealerIds.containsKey(a.Mobile_Dealer__c) &&
				a.Mobile_Dealer__c == dealerIds.get(a.Mobile_Dealer__c).Id
			) {
				if (!accToUpdate.containsKey(a.Id)) {
					accToUpdate.put(a.Id, a);
				}
				Id newDealerId = dealerIds.get(a.Mobile_Dealer__c).New_Account_Responsible__c;
				accToUpdate.get(a.Id).Mobile_Dealer__c = newDealerId;
				accToUpdate.get(a.Id).OwnerId = dealerInfos.get(newDealerId).ContactUserId__c;
			}
			if (a.Fixed_Dealer__c != null && dealerIds.containsKey(a.Fixed_Dealer__c) && a.Fixed_Dealer__c == dealerIds.get(a.Fixed_Dealer__c).Id) {
				if (!accToUpdate.containsKey(a.Id)) {
					accToUpdate.put(a.Id, a);
				}
				Id newDealerId = dealerIds.get(a.Fixed_Dealer__c).New_Account_Responsible__c;
				accToUpdate.get(a.Id).Fixed_Dealer__c = newDealerId;
				accToUpdate.get(a.Id).OwnerId = dealerInfos.get(newDealerId).ContactUserId__c;
			}
		}

		if (!accToUpdate.isEmpty()) {
			update accToUpdate.values();
		}
	}

	private void updateCORecords(Map<Id, Dealer_Information__c> dealerIds) {
		List<VF_Contract__c> cosToUpdate = new List<VF_Contract__c>();

		for (VF_Contract__c c : [SELECT Id, Dealer_Information__c, OwnerId FROM VF_Contract__c WHERE Dealer_Information__c IN :dealerIds.keySet()]) {
			Dealer_Information__c di = dealerIds.get(c.Dealer_Information__c);
			if (di != null && di.New_Contract_Owner__c != null) {
				c.Dealer_Information__c = di.New_Contract_Owner__c;
				cosToUpdate.add(c);
			}
		}

		if (!cosToUpdate.isEmpty()) {
			update cosToUpdate;
		}
	}

	private void updateOPPRecords(Map<Id, Dealer_Information__c> dealerIds, Map<Id, Dealer_Information__c> dealerInfos) {
		Map<Id, List<SObject>> ownerMap = GeneralUtils.groupByIDField(dealerIds.values(), 'ContactUserId__c');

		List<Opportunity> oppsToUpdate = new List<Opportunity>();
		for (Opportunity opp : [SELECT Id, OwnerId FROM Opportunity WHERE OwnerId IN :ownerMap.keySet() AND Opportunity_Status__c = 'Open']) {
			Dealer_Information__c dealer = (Dealer_Information__c) ownerMap.get(opp.OwnerId)[0];
			if (dealerInfos.containsKey(dealer.New_Opportunity_Owner__c)) {
				opp.OwnerId = dealerInfos.get(dealer.New_Opportunity_Owner__c).ContactUserId__c;
				oppsToUpdate.add(opp);
			}
		}

		if (!oppsToUpdate.isEmpty()) {
			update oppsToUpdate;
		}
	}

	private void updatePCARecords(Map<Id, Dealer_Information__c> dealerIds) {
		List<Postal_Code_Assignment__c> pcsToUpdate = new List<Postal_Code_Assignment__c>();
		for (Postal_Code_Assignment__c pca : [
			SELECT Id, Dealer_Information__c
			FROM Postal_Code_Assignment__c
			WHERE Dealer_Information__c IN :dealerIds.keySet()
		]) {
			Dealer_Information__c di = dealerIds.get(pca.Dealer_Information__c);
			if (di != null && di.New_Postal_code_Area_Responsible__c != null) {
				pca.Dealer_Information__c = di.New_Postal_code_Area_Responsible__c;
				pcsToUpdate.add(pca);
			}
		}

		if (!pcsToUpdate.isEmpty()) {
			update pcsToUpdate;
		}
	}

	private void sendNotificationsToManagerAfterUpdate(Map<Id, Dealer_Information__c> oldMap, Map<Id, Dealer_Information__c> newMap) {
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
		List<Dealer_Information__c> deactivated = new List<Dealer_Information__c>();
		List<Dealer_Information__c> contactChanged = new List<Dealer_Information__c>();

		for (Dealer_Information__c di : newMap.values()) {
			if (di.Status__c == 'Inactive' && oldmap.get(di.Id).Status__c != 'Inactive') {
				deactivated.add(oldmap.get(di.Id));
			}
			if (GeneralUtils.isRecordFieldChanged(di, oldMap, 'Contact__c')) {
				contactChanged.add(di);
				contactChanged.add(oldMap.get(di.Id));
			}
		}
		emailsToSend.addAll(getDeactivatedEmails(deactivated));
		emailsToSend.addAll(getContactChangedEmails(contactChanged));

		Messaging.sendEmail(emailsToSend, false);
	}

	private List<Messaging.SingleEmailMessage> getDeactivatedEmails(List<Dealer_Information__c> deactivated) {
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Dealer_Information_deactivation_notification'];
		OrgWideEmailAddress orgWideEmailAddress = EmailService.getDefaultOrgWideEmailAddress();
		for (Dealer_Information__c di : deactivated) {
			Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
			mail.setToAddresses(new List<String>{ di.contact__r.Userid__r.Manager.email });
			mail.setTargetObjectId(di.Id);
			mail.setTemplateId(template.Id);
			if (orgWideEmailAddress != null) {
				mail.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
			}
			mails.add(mail);
		}

		return mails;
	}

	private List<Messaging.SingleEmailMessage> getContactChangedEmails(List<Dealer_Information__c> contactChanged) {
		List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
		EmailTemplate template = [SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Dealer_Information_Contact_change_notification'];
		Set<String> dupeCheck = new Set<String>();

		OrgWideEmailAddress orgWideEmailAddress = EmailService.getDefaultOrgWideEmailAddress();

		for (Dealer_Information__c di : contactChanged) {
			if (!dupeCheck.contains(di.contact__r.Userid__r.Manager.email)) {
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
				mail.setToAddresses(new List<String>{ di.contact__r.Userid__r.Manager.email });
				mail.setTargetObjectId(di.Id);
				mail.setTemplateId(template.Id);
				if (orgWideEmailAddress != null) {
					mail.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
				}
				mails.add(mail);
				dupeCheck.add(di.contact__r.Userid__r.Manager.email);
			}
		}
		return mails;
	}

	private String generateUniqueScheduleJobName(String jobName) {
		Integer randomNumber = Integer.valueof((Math.random() * 100000));

		String uniqueScheduledJobId = jobName + '_' + EncodingUtil.base64encode(Crypto.generateDigest('SHA1', Blob.valueOf(randomNumber + jobName)));

		return uniqueScheduledJobId;
	}
}
