@isTest
public class CustomButtonNewCaseTest {
	private static testMethod void test() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;
		System.runAs(simpleUser) {
			Framework__c frameworkSetting = new Framework__c();
			frameworkSetting.Framework_Sequence_Number__c = 2;
			insert frameworkSetting;

			PriceReset__c priceResetSetting = new PriceReset__c();

			priceResetSetting.MaxRecurringPrice__c = 200.00;
			priceResetSetting.ConfigurationName__c = 'IP Pin';

			insert priceResetSetting;
			Account testAccount = CS_DataTest.createAccount('Test Account');
			insert testAccount;

			Sales_Settings__c ssettings = new Sales_Settings__c();
			ssettings.Postalcode_check_validity_days__c = 2;
			ssettings.Max_Daily_Postalcode_Checks__c = 2;
			ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
			ssettings.Postalcode_check_block_period_days__c = 2;
			ssettings.Max_weekly_postalcode_checks__c = 15;
			insert ssettings;

			Test.startTest();

			Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', simpleUser.id);
			insert testOpp;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
			basket.Primary__c = true;
			basket.cscfga__Basket_Status__c = 'Valid';
			insert basket;

			CustomButtonNewCase button = new CustomButtonNewCase();

			button.performAction(String.valueOf(basket.Id));
			CustomButtonNewCase.getBasketData(basket.Id);

			Test.stopTest();
		}
	}

	@isTest
	static void test2() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		User testUser = CS_DataTest.createUser(pList, roleList);
		insert testUser;

		Test.startTest();
		System.runAs(testUser) {
			Account testAccount = CS_DataTest.createAccount('Test Account');
			insert testAccount;
			Opportunity testOpp = CS_DataTest.createVodafoneOpportunity(testAccount, 'Test Opp', testUser.Id);
			insert testOpp;

			Map<String, String> testData = new Map<String, String>();
			testData.put('Account', testAccount.Id);
			testData.put('Opportunity__c', testOpp.Id);
			testData.put('Subject', 'Test');
			testData.put('RecordTypeId', Schema.SObjectType.Case.getRecordTypeInfosByName().get('SAG Case').getRecordTypeId());
			testData.put('Description', 'Description');
			testData.put('Product_Basket__c', null);

			CustomButtonNewCase button = new CustomButtonNewCase();
			String result = CustomButtonNewCase.createNewCase(
				testData,
				Schema.SObjectType.Case.getRecordTypeInfosByName().get('SAG Case').getRecordTypeId()
			);
		}
		Test.stopTest();
	}
}
