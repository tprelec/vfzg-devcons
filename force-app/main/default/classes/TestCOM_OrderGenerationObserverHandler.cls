@isTest
private class TestCOM_OrderGenerationObserverHandler {
	@TestSetup
	static void testSetup() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', null);
		insert testOpp;

		VF_Contract__c contr = new VF_Contract__c();
		contr.Opportunity__c = testOpp.Id;
		contr.Account__c = testAccount.Id;
		insert contr;

		OrderType__c ot = new OrderType__c();
		ot.Name = 'TestOT';
		ot.ExportSystem__c = 'BOP';
		ot.Status__c = 'New';
		ot.ExternalID__c = 'Test11';
		insert ot;

		Order__c ord = new Order__c();
		ord.VF_Contract__c = contr.Id;
		ord.OrderType__c = ot.Id;
		ord.BEN_Number__c = '55555';
		insert ord;

		csord__Order_Request__c coreq = new csord__Order_Request__c(csord__Module_Name__c = 'Test', csord__Module_Version__c = '1.0');
		insert coreq;

		csord__Order__c order = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Order_Request__c = coreq.Id,
			csord__Identification__c = 'TestCOM_OrderGenerationObserverHandler_' + system.now(),
			csordtelcoa__Opportunity__c = testOpp.Id
		);
		insert order;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'TestCOM_OrderGenerationObserverHandler_Sub';
		subscription.csord__Order__c = order.Id;
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(null, subscription);
		service.csord__Identification__c = 'TestCOM_OrderGenerationObserverHandler_Serv';
		service.csord__Status__c = 'Test';
		service.csord__Order__c = order.Id;
		service.csord__Subscription__c = subscription.Id;
		insert service;

		CS_DataTest.createDeliveryOrder('TestCOM_OrderGenerationObserverHandler_DelOrd', order.Id, true);
	}

	@isTest
	private static void testLinkVFOrderToDeliveryOrder() {
		List<csord__Service__c> serviceList = [
			SELECT Id, csord__Order__c, csord__Order__r.csordtelcoa__Opportunity__c
			FROM csord__Service__c
			WHERE csord__Identification__c = 'TestCOM_OrderGenerationObserverHandler_Serv'
		];

		List<COM_Delivery_Order__c> deliveryOrderList = [
			SELECT Id, Order__c
			FROM COM_Delivery_Order__c
			WHERE Name = 'TestCOM_OrderGenerationObserverHandler_DelOrd'
		];

		COM_OrderGenerationObserverHandler ogoh = new COM_OrderGenerationObserverHandler();
		Test.startTest();
		ogoh.linkVFOrderToDeliveryOrder(serviceList, deliveryOrderList);
		Test.stopTest();

		Order__c result = [SELECT Id, COM_Delivery_Order__c FROM Order__c WHERE BEN_Number__c = '55555'];

		System.assertEquals(deliveryOrderList[0].Id, result.COM_Delivery_Order__c, 'Delivery Order Id is not correct.');
	}
}
