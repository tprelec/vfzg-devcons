@isTest
private class OmniChannelChargesProcessorTest {
	@isTest
	static void getSFCCContributionTest() {
		CCAQDProcessor.CCDiscount discount1 = new CCAQDProcessor.CCDiscount();
		discount1.source = 'discount1';
		discount1.isPRSDiscount = false;
		discount1.type = 'PER';
		discount1.amount = 2;
		discount1.discountCharge = 'charge 1';
		discount1.chargeType = 'oneOff';

		CCAQDProcessor.CCDiscount discount2 = new CCAQDProcessor.CCDiscount();
		discount2.source = 'discount2';
		discount2.type = 'PER';
		discount2.amount = 2;
		discount2.discountCharge = 'charge 1';
		discount2.chargeType = 'oneOff';

		CCAQDProcessor.CCDiscount discount3 = new CCAQDProcessor.CCDiscount();
		discount3.source = 'discount3';
		discount3.isPRSDiscount = true;
		discount3.type = 'PER';
		discount3.amount = 2;
		discount3.discountCharge = 'charge 1';
		discount3.chargeType = 'oneOff';

		List<CCAQDProcessor.CCDiscount> aqdDiscounts = new List<CCAQDProcessor.CCDiscount>{ discount1, discount2, discount3 };

		System.Test.startTest();
		List<cspsi.CommonCartWrapper.Discount> sfccDiscounts = OmniChannelChargesProcessor.getSFCCContribution(
			aqdDiscounts,
			ChargesProcessorUtils.QUANTITY_STRATEGY_DECOMPOSITION,
			2
		);
		System.Test.stopTest();

		System.assertEquals(2, sfccDiscounts.size());
	}

	@isTest
	static void getPSContributionTest() {
		CCAQDProcessor.CCDiscount discount1 = new CCAQDProcessor.CCDiscount();
		discount1.source = 'discount1';
		discount1.isPRSDiscount = false;
		discount1.type = 'PER';
		discount1.amount = 2;
		discount1.discountCharge = 'charge 1';
		discount1.chargeType = 'oneOff';

		CCAQDProcessor.CCDiscount discount3 = new CCAQDProcessor.CCDiscount();
		discount3.source = 'discount3';
		discount3.isPRSDiscount = true;
		discount3.type = 'PER';
		discount3.amount = 2;
		discount3.discountCharge = 'charge 1';
		discount3.chargeType = 'oneOff';

		CCAQDProcessor.CCCharge aqdCharge = new CCAQDProcessor.CCCharge();
		aqdCharge.source = 'charge1';
		aqdCharge.listPrice = 100;
		aqdCharge.salesPrice = 86;
		aqdCharge.name = 'charge 1';
		aqdCharge.chargeType = 'oneOff';
		aqdCharge.description = 'charge 1 description';

		List<CCAQDProcessor.CCDiscount> aqdDiscounts = new List<CCAQDProcessor.CCDiscount>{ discount1, discount3 };
		List<CCAQDProcessor.CCCharge> aqdCharges = new List<CCAQDProcessor.CCCharge>{ aqdCharge };

		System.Test.startTest();
		List<cspsi.CommonCartWrapper.Discount> psDiscounts = OmniChannelChargesProcessor.getPSContribution(
			aqdCharges,
			aqdDiscounts,
			ChargesProcessorUtils.QUANTITY_STRATEGY_DECOMPOSITION,
			2
		);
		System.Test.stopTest();

		System.assertEquals(3, psDiscounts.size());
	}
}
