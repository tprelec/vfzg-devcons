global with sharing class CS_SiteLookup extends cscfga.ALookupSearch {

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit) {

        string componentType = searchFields.get('componentType');
        
        ISiteCheck siteCheck = new SiteCheckFactory().getSiteCheck(componentType);
        
        return siteCheck.getSites(searchFields, productDefinitionID, excludeIds, pageOffset, pageLimit);
    }
    
    public override String getRequiredAttributes() { 
        return '["componentType"]';
    }
}