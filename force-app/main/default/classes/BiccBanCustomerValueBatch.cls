global class BiccBanCustomerValueBatch implements Database.Batchable <sObject>, Database.Stateful{

	global final map<String,String> biccBanFieldMapping = SyncUtil.fullMapping('BICC_BAN_CUST_VALUE__c -> Ban__c').get('BICC_BAN_CUST_VALUE__c -> Ban__c');
	global final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	global final Schema.SObjectType biccSchema = schemaMap.get('BICC_BAN_CUST_VALUE__c');
	global final map<String, Schema.SObjectField> biccFieldMap = biccSchema.getDescribe().fields.getMap();
	global final set<String> queriedFields = SyncUtil.getQueriedFields(biccBanFieldMapping);
	global final list<String> sortList = SyncUtil.getSortedList(biccFieldMap);
	global String header = SyncUtil.getHeader(sortList, queriedFields);
	global String dataError = '';
	global Boolean chain = false;

	global Database.QueryLocator start(Database.BatchableContext BC){

		//Create a dynamic query with info from the Field Mapping Object
		String query = 'SELECT ';
		Set<String> testset = new Set<String>();
		testset.addall(BiccBanFieldMapping.values());
		for (String biccField : testset) {
			query += biccField + ',';
		}
		query = query.subString(0, query.length() - 1);
		query += ' FROM BICC_BAN_CUST_VALUE__c';

		return Database.getQueryLocator(Query);
	}
	
	global void execute(Database.BatchableContext BC, List<BICC_BAN_CUST_VALUE__c> scope){

		if(Job_Management__c.getOrgDefaults().Cancel_Batch__c == true){
			System.abortJob(BC.getJobId());
		}

		Map<String, BICC_BAN_CUST_VALUE__c> scopeMap = new Map<String, BICC_BAN_CUST_VALUE__c>();
		List<BICC_BAN_CUST_VALUE__c> biccErrorList = new List<BICC_BAN_CUST_VALUE__c>();
		List<BICC_BAN_CUST_VALUE__c> biccDeleteList = new List<BICC_BAN_CUST_VALUE__c>();
		list<Ban__c> banList = new list<Ban__c>();
		list<Ban__c> banDeleteList = new list<Ban__c>();

		//Put scope in a map for deletion
		for(BICC_BAN_CUST_VALUE__c bicc : scope){			
			if(scopeMap.get(bicc.BAN__c) != null){
				biccDeleteList.add(bicc);
			} else if(StringUtils.checkBan(bicc.BAN__c)){
				scopeMap.put(bicc.BAN__c, bicc);
			} else {
				bicc.Error__c = 'The BAN number is not valid';
				biccErrorList.add(bicc);
			}
		}

		//Create accounts from BICC
		for (BICC_BAN_CUST_VALUE__c biccBan : scopeMap.values()) {
			Ban__c ban = new Ban__c();
			for (String banField : BiccBanFieldMapping.keySet()) {
				String biccBanField = BiccBanFieldMapping.get(banField);
				Object biccBanValue = biccBan.get(biccBanField);
				ban.put(banField,biccBanValue);
			}
			banList.add(ban);
		}

		//Upsert Accounts in the database
		list<Database.UpsertResult> UR = Database.upsert(banList, Ban__c.Fields.Name, false);
		for (Integer i = 0; i < UR.size(); i++) {
			if(UR[i].isSuccess() && !UR[i].isCreated()){
				biccDeleteList.add(scopeMap.get(banList[i].Name));
			} else if (UR[i].isSuccess() && UR[i].isCreated()){
				scopeMap.get(banList[i].Name).Error__c = 'Ban number doesn\'t exist';
				biccErrorList.add(scopeMap.get(banList[i].Name));
				banDeleteList.add(banList[i]);
			} else {
				scopeMap.get(banList[i].Name).Error__c = 'Error while creating Ban';
				biccErrorList.add(scopeMap.get(banList[i].Name));
			}
		}

		for(BICC_BAN_CUST_VALUE__c error : biccErrorList){
			for(String field : sortList){
				if(queriedFields.contains(field)){
					dataError += '"' + error.get(field) + '",';
				}
			}
			dataError += '"' + error.Error__c + '"\n';
		}

		//Do DML
		delete biccDeleteList;
		delete biccErrorList;
		delete banDeleteList;
	}

	global void finish(Database.BatchableContext BC){

		//Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							TotalJobItems
						  FROM
							AsyncApexJob
						  WHERE
							Id =: BC.getJobId()];

		// Send an email to InsideSalesSystems.nl@vodafone.com notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		header = header.subString(0, header.length() - 1) + ',"Error__c"\n';
		Blob b = blob.valueOf(header + dataError);
		Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
		efa.setFileName('BiccBanCustomerValueErrors.csv');
		efa.setBody(b);

		String[] toAddresses = new String[] {'EBUSalesForce.nl@vodafone.com'};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Bicc Ban Customer Value database import ' + a.Status);
		mail.setPlainTextBody('Bicc Ban Customer Value database import job processed ' + a.TotalJobItems +
							  ' batches with '+ a.NumberOfErrors + ' failures.');
		mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

		if(chain == true){
			BiccCtnInformationBatch controller = new BiccCtnInformationBatch();
			controller.chain = true;
			database.executebatch(controller, 100); //SP: added batching in parts of 100, so the heap limit does not get reached
		}
	}
}