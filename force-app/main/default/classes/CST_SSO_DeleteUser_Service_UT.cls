@isTest
public with sharing class CST_SSO_DeleteUser_Service_UT {
	@TestSetup
	static void makeData() {
		Profile prof = [SELECT id FROM Profile WHERE name = 'VF CST Customer'];
		Integer index = 21;

		String email = 'test21@vodafoneziggo.com';
		String externalId = 'test21';

		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.Unify_Ref_Id__c = externalId;
		update acc;

		Contact cont = TestUtils.createContact(acc);
		cont.Email = email;
		cont.Unify_Ref_Id__c = externalId;
		cont.Gender__c = 'X';
		update cont;

		Contact cont2 = TestUtils.createContact(acc);
		cont2.Email = email;
		cont2.Unify_Ref_Id__c = 'test22';
		cont2.Gender__c = 'X';
		update cont;

		User usr = new User(
			profileId = prof.Id,
			username = email,
			IsActive = true,
			contactId = cont.Id,
			email = email,
			alias = externalId,
			emailencodingkey = 'UTF-8',
			CommunityNickname = externalId,
			LastName = 'Jane',
			LanguageLocaleKey = 'nl_NL',
			LocaleSidKey = 'nl_NL',
			TimeZoneSidKey = 'Europe/Amsterdam'
		);

		insert usr;
		string userid = site.createExternalUser(usr, acc.Id, 'password');

		/*

        
        cont.ownerId =  usr.Id;
        update cont;

        acc.ownerId =  usr.Id;
        update acc;
        */
	}

	private static Blob getBody(String externalId) {
		Blob bBody = Blob.valueOf(
			'<s11:Envelope xmlns:s11=\'http://schemas.xmlsoap.org/soap/envelope/\'><s11:Header><usr:authenticationHeader xmlns:usr=\'http://sb.ecs.vodafone.nl/servicebus/\'><username>???</username><password>???</password><applicationId>???</applicationId></usr:authenticationHeader></s11:Header><s11:Body><usr:deleteUsersRequest xmlns:usr=\'http://sb.ecs.vodafone.nl/servicebus/\'><user><company><banNumber>???</banNumber><corporateId>???</corporateId><bopCode>???</bopCode><referenceId>???</referenceId><accountId>???</accountId><externalSystem>???</externalSystem></company><email>abc@gmail.com</email><referenceId>' +
			externalId +
			'</referenceId><externalSystem>???</externalSystem></user></usr:deleteUsersRequest></s11:Body></s11:Envelope>'
		);

		return bBody;
	}

	@isTest
	static void testDisableUser() {
		String externalId = 'test21';

		RestRequest request = new RestRequest();

		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();

		request.requestUri = baseUrl + '/services/apexrest/DisableUser';
		request.httpMethod = 'PATCH';
		request.requestBody = getBody(externalId);

		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();

		CST_SSO_DeleteUser_Service.disableUser();
		Test.stopTest();
	}
	@isTest
	static void testDeleteContact() {
		// Contact cnt = [select id, Unify_Ref_Id__c from contact where Unify_Ref_Id__c = 'test22' limit 1];
		CST_SSO_DeleteUser_Service.deleteContact('test22');
	}
}
