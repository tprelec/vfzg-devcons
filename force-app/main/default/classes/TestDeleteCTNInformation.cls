@isTest
public with sharing class TestDeleteCTNInformation {
	@isTest
	public static void testDeleteCTNInformation() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OrderTriggerHandler', null, 0);

		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		opp.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opp;
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.id);
		insert basket;
		Opportunity theOpp = [SELECT Id, Segment__c, Type_of_service__c FROM Opportunity];

		TestUtils.createOrderType();
		Product2 theProd = TestUtils.createOneOffProduct();
		PricebookEntry pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), theProd);
		OpportunityLineItem lineItem = TestUtils.createOpportunityLineItem(theOpp, pbe);

		theOpp.Segment__c = 'SoHo';
		theOpp.NetProfit_Complete__c = true;
		update theOpp;

		cscfga__Product_Basket__c theBasket = [SELECT Id, cscfga__Basket_Status__c FROM cscfga__Product_Basket__c];
		theBasket.cscfga__Basket_Status__c = 'Approved';
		update theBasket;

		TestUtils.createAccountManager();

		Test.startTest();
		System.runAs(TestUtils.theAccountManager) {
			DeleteCTNInformation.deleteCtnInformation(theBasket.Id);
		}
		Test.stopTest();

		List<NetProfit_Information__c> npInfo = [SELECT Id FROM NetProfit_Information__c];
		System.assert(npInfo.size() == 0, 'The NetProfit_Information__c should be deleted');
	}
}
