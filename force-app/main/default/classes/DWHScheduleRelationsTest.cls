@IsTest
public class DWHScheduleRelationsTest {
	@TestSetup
	static void setup() {
		//Data preparation for sync table
		TestUtils.autoCommit = false;

		List<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('DWH_Account_SF_Interface__c -> Account', 'TmeActive__c', 'Active_Company__c'));
		fsmlist.add(TestUtils.createSync('DWH_Lead_dummy__c -> Lead', 'CompanyName__c', 'Company'));
		fsmlist.add(TestUtils.createSync('DWH_Asset_SF_Interface__c -> VF_Asset__c', 'CommercialProductCode__c', 'Priceplan_Name__c'));
		fsmlist.add(
			TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'CustomerNumber__c', 'Customer_Number__c')
		);
		//fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'PlayLevel__c', 'Playlevel__c'));
		//fsmlist.add(TestUtils.createSync('DWH_Relation_SF_Interface__c -> Customer_Relations_Potential__c', 'BusinessSubSegment__c', 'Business_segment__c'));
		fsmlist.add(TestUtils.createSync('DWH_KvkDelta_SF_Interface__c -> VF_Asset__c', 'Today__c', 'Retention_Date__c'));
		insert fsmlist;

		// Data preparation for destination records
		TestUtils.autoCommit = true;
		User admin = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;

		//Account account = TestUtils.createAccount(admin);
		Account account = TestUtils.createAccount(admin);
		account.KVK_number__c = '12345678';
		upsert account;

		Id ziggoRecordTypeId = Schema.SObjectType.VF_Asset__c.getRecordTypeInfosByName().get('IB Ziggo').getRecordTypeId();
		VF_Asset__c asset = new VF_Asset__c(
			RecordTypeId = ziggoRecordTypeId,
			account__c = account.id,
			account_KVK__c = '12345678',
			Priceplan_Class__c = 'fZiggo IB'
		);
		insert asset;

		//Data preparation for load table
		List<SObject> loadList = new List<SObject>{
			//existing account
			new DWH_Account_SF_Interface__c(TmeCode__c = 'TME_12345678', CompanyKvkNumber__c = '12345678', CompanyLocationCode__c = '9257RN|18||'),
			//lead
			new DWH_Account_SF_Interface__c(TmeCode__c = 'TME_12345679', CompanyKvkNumber__c = '12345679', CompanyLocationCode__c = '9657RN|18||'),
			//new account
			new DWH_Account_SF_Interface__c(
				TmeCode__c = 'TME_12345689',
				CompanyKvkNumber__c = '12345689',
				CompanyLocationCode__c = '9658RN|18||',
				CompanyIsActiveCustomer__c = true
			),
			//new delta
			new DWH_KvkDelta_SF_Interface__c(KvkNumber__c = '12345678'),
			//new asset
			new DWH_Asset_SF_Interface__c(
				CommercialProductCode__c = 'CP_IPVPNPRMM5MBPSFBR',
				CustomerNumber__c = '12345678',
				KvkNumber__c = '12345678',
				LocationCode__c = '9257RN|18||',
				Country__c = 'NL',
				City__c = 'Amsterdam',
				Street__c = 'Hoofdstraat'
			),
			//new relation
			new DWH_Relation_SF_Interface__c(MatchStart__c = '01012001', CustomerNumber__c = '12345678', KvkNumber__c = '12345678')
		};
		insert loadList;
	}

	@IsTest
	static void testDWHScheduleRelations() {
		Test.startTest();
		// This test runs a scheduled job at midnight Sept. 3rd. next year
		String nextYear = String.valueOf(Date.Today().Year() + 1);
		String CRON_EXP = '0 0 0 3 9 ? ' + nextYear;

		// Schedule the test job
		String jobId = System.schedule('DWHScheduleRelationsTest', CRON_EXP, new DWHScheduleRelations());

		// Get the information from the CronTrigger API object
		CronTrigger cronTrigger = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

		// Verify the expressions are the same System.assertEquals(CRON_EXP, cronTrigger.CronExpression);
		// Verify the job has not run
		System.assertEquals(0, cronTrigger.TimesTriggered, 'DWHScheduleRelationsTest should not have run yet');
		// Verify the next time the job will run
		System.assertEquals(
			nextYear + '-09-03 00:00:00',
			String.valueOf(cronTrigger.NextFireTime),
			'Next time when job should run is "' +
			nextYear +
			'-09-03 00:00:00"'
		);
		Test.stopTest();
	}
}
