/**
 * 	@description	This class contains unit tests for the NumberportingTriggerHandler class
 *	@Author			Guy Clairbois
 */
@isTest
private class TestNumberportingTriggerHandler {
	
	@isTest
	static void testCreateNumberporting(){
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        //Pricebook2 pricebook = TestUtils.createPricebook(true);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
		VF_Contract__c contr = TestUtils.createVFContract(acct,opp);
		Order__c ord = TestUtils.createOrder(contr);

		Test.startTest();

		Numberporting__c np = new Numberporting__c();
		np.Order__c = ord.Id;
		np.Customer__c = acct.Id;
		np.Name = 'testnp12345';
		insert np;
		
		np.Name = 'testnp54321';
		update np;
		
		Test.stopTest();

		System.assert(np.Id!=null,'Insert should have been succesfull.') ;
	}
}