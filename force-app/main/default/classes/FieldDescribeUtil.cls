/**
 * @description       : utility class to provide access to Field description
 * @author            : mcubias
 * @last modified on  : 06-01-2022
 **/
public with sharing class FieldDescribeUtil {
	private static final String BASE64CHARS = '' + 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' + 'abcdefghijklmnopqrstuvwxyz' + '0123456789+/';

	/**
	 * @description return a list of valid dependent values for a given controlling picklist field
	 * @param dependentField
	 * @param ctrlField
	 * @return Map<String, List<String>>
	 **/
	public static Map<String, List<String>> getDependentOptions(Schema.SObjectField dependentField, Schema.SObjectField ctrlField) {
		// validFor property cannot be accessed via a method or a property,
		// so we need to serialize the PicklistEntry object and then deserialize into a wrapper.
		List<Schema.PicklistEntry> contrEntries = ctrlField.getDescribe().getPicklistValues();
		List<PicklistEntryWrapper> depEntries = wrapPicklistEntries(dependentField.getDescribe().getPicklistValues());

		// Set up the return container - Map<ControllingValue, List<DependentValues>>
		Map<String, List<String>> objResults = new Map<String, List<String>>();
		List<String> controllingValues = new List<String>();

		for (Schema.PicklistEntry ple : contrEntries) {
			String label = ple.getLabel();
			objResults.put(label, new List<String>());
			controllingValues.add(label);
		}

		for (PicklistEntryWrapper plew : depEntries) {
			String label = plew.label;
			String validForBits = base64ToBits(plew.validFor);
			for (Integer i = 0; i < validForBits.length(); i++) {
				// For each bit, in order: if it's a 1, add this label to the dependent list for the corresponding controlling value
				String bit = validForBits.mid(i, 1);
				if (bit == '1') {
					objResults.get(controllingValues.get(i)).add(label);
				}
			}
		}

		return objResults;
	}

	/**
	 * @description Convert decimal to binary representation. (eg. 4 => '100', 19 => '10011', etc.)
	 *				Divide by 2 repeatedly until 0. At each step note the remainder (0 or 1).
	 *				These, in reverse order, are the binary.
	 * @param val
	 * @return String
	 **/
	public static String decimalToBinary(Integer val) {
		String bits = '';
		while (val > 0) {
			Integer remainder = Math.mod(val, 2);
			val = Integer.valueOf(Math.floor(val / 2));
			bits = String.valueOf(remainder) + bits;
		}
		return bits;
	}

	/**
	 * @description Convert a base64 token into a binary/bits representation (e.g. 'gAAA' => '100000000000000000000')
	 * @param validFor
	 * @return String
	 **/
	public static String base64ToBits(String validFor) {
		if (String.isEmpty(validFor)) {
			return '';
		}

		String validForBits = '';

		for (Integer i = 0; i < validFor.length(); i++) {
			String thisChar = validFor.mid(i, 1);
			Integer val = BASE64CHARS.indexOf(thisChar);
			String bits = decimalToBinary(val).leftPad(6, '0');
			validForBits += bits;
		}

		return validForBits;
	}

	private static List<PicklistEntryWrapper> wrapPicklistEntries(List<Schema.PicklistEntry> picklistEntries) {
		return (List<PicklistEntryWrapper>) JSON.deserialize(JSON.serialize(picklistEntries), List<PicklistEntryWrapper>.class);
	}

	/**
	 * @description wrapper class used to deserialize Picklist Entries
	 */
	public class PicklistEntryWrapper {
		public String active { get; set; }
		public String defaultValue { get; set; }
		public String label { get; set; }
		public String value { get; set; }
		public String validFor { get; set; }
	}
}
