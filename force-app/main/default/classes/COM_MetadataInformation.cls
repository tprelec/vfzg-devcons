/*
	@description: data holder for the Custom Metadata Settings used to define the process for each product, class stores these to avoid multiple SOQLs. 
*/
public class COM_MetadataInformation {
	private Set<Id> deliveryComponentIds = new Set<Id>();
	private List<COM_Delivery_Article_Materials__mdt> deliveryArticleMaterials = new List<COM_Delivery_Article_Materials__mdt>();
	private List<COM_Delivery_Component__mdt> deliveryComponents = new List<COM_Delivery_Component__mdt>();
	private Map<String, COM_Delivery_Component__mdt> deliveryComponentNameDeliveryComponentMap = new Map<String, COM_Delivery_Component__mdt>();
	private Map<Id, COM_Decomposition__mdt> decompositionsMap = new Map<Id, COM_Decomposition__mdt>();
	private Map<Id, COM_Delivery_Article__mdt> deliveryArticlesMap = new Map<Id, COM_Delivery_Article__mdt>();
	private Map<Id, COM_Delivery_Component__mdt> deliveryComponentsMap = new Map<Id, COM_Delivery_Component__mdt>();
	private Set<String> deliveryComponentsRequiringInstallation = new Set<String>();
	private Set<String> deliveryComponentsRequiringProvisioning = new Set<String>();
	private List<csord__Service__c> serviceList = new List<csord__Service__c>();

	public class DeliveryComponent {
		public String name;
		public Id serviceId;
		public Id deliveryOrderId;
		public String articleName;
		public String articleCode;
		public Boolean installationRequired;
		public Boolean provisioningRequired;
		public String status;
		public String macdAction;
	}

	public List<csord__Service__c> getServiceList() {
		return serviceList;
	}

	public void setServiceList(List<csord__Service__c> serviceList) {
		this.serviceList = serviceList;
	}

	public Set<String> getDeliveryComponentsRequiringInstallation() {
		return deliveryComponentsRequiringInstallation;
	}

	public void setDeliveryComponentsRequiringInstallation(Set<String> deliveryComponentsRequiringInstallation) {
		this.deliveryComponentsRequiringInstallation = deliveryComponentsRequiringInstallation;
	}

	public Set<String> getDeliveryComponentsRequiringProvisioning() {
		return deliveryComponentsRequiringProvisioning;
	}

	public void setDeliveryComponentsRequiringProvisioning(Set<String> deliveryComponentsRequiringProvisioning) {
		this.deliveryComponentsRequiringProvisioning = deliveryComponentsRequiringProvisioning;
	}

	public Set<Id> getDeliveryComponentIdSet() {
		return deliveryComponentIds;
	}

	public List<COM_Delivery_Article_Materials__mdt> getDeliveryArticleMaterials() {
		return deliveryArticleMaterials;
	}

	public List<COM_Delivery_Component__mdt> getDeliveryComponents() {
		return deliveryComponents;
	}

	public void setDeliveryComponents(Set<Id> deliveryComponentIdSet) {
		this.deliveryComponentIds = deliveryComponentIdSet;
	}

	public void setDeliveryArticleMaterials(List<COM_Delivery_Article_Materials__mdt> deliveryArticleMaterialList) {
		this.deliveryArticleMaterials = deliveryArticleMaterialList;
	}

	public void setDeliveryComponents(List<COM_Delivery_Component__mdt> deliveryComponentActivityList) {
		this.deliveryComponents = deliveryComponentActivityList;
	}

	public Map<String, COM_Delivery_Component__mdt> getDeliveryComponentNameDeliveryComponentMap() {
		return deliveryComponentNameDeliveryComponentMap;
	}

	public Map<Id, COM_Decomposition__mdt> getDecompositionsMap() {
		return decompositionsMap;
	}

	public Map<Id, COM_Delivery_Article__mdt> getDeliveryArticlesMap() {
		return deliveryArticlesMap;
	}

	public Map<Id, COM_Delivery_Component__mdt> getDeliveryComponentsMap() {
		return deliveryComponentsMap;
	}

	public void setDeliveryComponentNameDeliveryComponentMap(Map<String, COM_Delivery_Component__mdt> deliveryComponentNameDeliveryComponentMap) {
		this.deliveryComponentNameDeliveryComponentMap = deliveryComponentNameDeliveryComponentMap;
	}

	public void setDecompositionsMap(Map<Id, COM_Decomposition__mdt> decompositionsMap) {
		this.decompositionsMap = decompositionsMap;
	}

	public void setDeliveryArticlesMap(Map<Id, COM_Delivery_Article__mdt> deliveryArticlesMap) {
		this.deliveryArticlesMap = deliveryArticlesMap;
	}

	public void setDeliveryComponentsMap(Map<Id, COM_Delivery_Component__mdt> deliveryComponentsMap) {
		this.deliveryComponentsMap = deliveryComponentsMap;
	}
}
