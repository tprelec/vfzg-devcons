public with sharing class PriceItemAddOnPriceItemAssHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<cspmb__Price_Item_Add_On_Price_Item_Association__c> newProducts = (List<cspmb__Price_Item_Add_On_Price_Item_Association__c>) this.newList;
		Sequence seq = new Sequence('PI Add On Price Item Association');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (cspmb__Price_Item_Add_On_Price_Item_Association__c o : newProducts) {
				o.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
