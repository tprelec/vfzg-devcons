/**
 * @description			This class is responsible for exporting customers to ECS.
 * @author				Guy Clairbois
 */
public class ECSCompanyService extends ECSSOAPBaseWebService implements IWebService<List<Request>, List<Response>> {
	private Request[] requests;
	private Response[] responses;
	/**
	 * @description			This is the request which will be sent to ECS update a
	 *						customer.
	 */
	@SuppressWarnings('PMD.ExcessivePublicCount,PMD.TooManyFields')
	public class Request {
		public String banNumber;
		public String corporateId;
		public String bopCode;
		public String referenceId;
		public String name;
		public String accountManagerEbu;
		public String phone;
		public String fax;
		public String email;
		public String countryId;
		public String kvkNumber;
		public String website;
		public String billingStreet;
		public String billingPostalCode;
		public String billingCity;
		public String billingCountry;
		public String dealerCode;
		public String companyGroup;
		public String resellerBanNumber;
		public String resellerCorporateId;
		public String resellerBopCode;
		public String resellerReferenceId;
		public String parentCompanyBanNumber;
		public String parentCompanyCorporateId;
		public String parentCompanyBopCode;
		public String parentCompanyReferenceId;
		public String internalInvoicing;
		public String requestType;
	}

	/**
	 * @description			This is the response that will be returned after making a request to ECS.
	 */
	public class Response {
		public String corporateId;
		public String bopCode;
		public String errorCode;
		public String errorMessage;
		public String referenceId;
	}

	/**
	 * @description			The constructor is responsible for setting the default web service configuration.
	 */
	public ECSCompanyService() {
		if (!Test.isRunningTest()) {
			if (FeatureFlagging.isActive('BOP_Company')) {
				setWebServiceConfig(WebServiceConfigLocator.getConfigNamedCredential('ECSSOAPCompany'));
			} else {
				setWebServiceConfig(WebServiceConfigLocator.getConfig('ECSSOAPCompany'));
			}
		} else {
			setWebServiceConfig(WebServiceConfigLocator.createConfig());
		}
	}

	/**
	 * @description			This sets the customer data to create/update customer details in ECS.
	 */
	public void setRequest(Request[] request) {
		this.requests = request;
	}

	public void makeRequest(String requestType) {
		if (requestType == 'createCustomer') {
			makeCreateRequest();
		} else if (requestType == 'updateCustomer') {
			makeUpdateRequest();
		} else if (requestType == 'createReseller') {
			makeResellerCreateRequest();
		} else if (requestType == 'updateReseller') {
			makeResellerUpdateRequest();
		}
	}

	/**
	 * @description			This will make the generic request to ECS.
	 */
	public void genericRequest() {
		checkExceptions();
		@SuppressWarnings('PMD.UnusedLocalVariable')
		ECSSOAPCompany.companySoap service = setServiceSOAP();
	}

	/**
	 * @description			This will make the request to ECS to create customers with the request.
	 */
	public void makeCreateRequest() {
		checkExceptions();
		ECSSOAPCompany.companySoap service = setServiceSOAP();

		List<ECSSOAPCompany.customerCreateType> createRequest = buildCustomerCreateUsingRequest();
		System.debug(LoggingLevel.INFO, 'createRequest: ' + JSON.serialize(createRequest));

		// create queryResult
		List<ECSSOAPCompany.companyResponseType> createResults;
		try {
			system.debug(LoggingLevel.INFO, createRequest);
			createResults = service.createCustomers(createRequest);
			system.debug(LoggingLevel.INFO, createResults);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException('Error received from BOP when attempting to create the customer(s): ' + ex.getMessage(), ex);
		}
		parseResponse(createResults);
	}

	/**
	 * @description			This will make the request to ECS to update customers with the request.
	 */
	public void makeUpdateRequest() {
		checkExceptions();
		ECSSOAPCompany.companySoap service = setServiceSOAP();

		List<ECSSOAPCompany.customerUpdateType> updateRequest = buildCustomerUpdateUsingRequest();
		System.debug(LoggingLevel.INFO, 'updateRequest: ' + JSON.serialize(updateRequest));

		// create queryResult
		List<ECSSOAPCompany.companyResponseType> updateResults;

		try {
			updateResults = service.updateCustomers(updateRequest);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException('Error received from BOP when attempting to update the customer(s): ' + ex.getMessage(), ex);
		}
		parseResponse(updateResults);
	}

	/**
	 * @description			This will make the request to ECS to create customers with the request.
	 */
	public void makeResellerCreateRequest() {
		checkExceptions();
		ECSSOAPCompany.companySoap service = setServiceSOAP();

		List<ECSSOAPCompany.resellerCreateType> createRequest = buildResellerCreateUsingRequest();
		System.debug(LoggingLevel.INFO, 'createRequest: ' + JSON.serialize(createRequest));

		// create queryResult
		List<ECSSOAPCompany.companyResponseType> createResults;

		try {
			system.debug(LoggingLevel.INFO, createRequest);
			createResults = service.createResellers(createRequest);
			system.debug(LoggingLevel.INFO, createResults);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException('Error received from BOP when attempting to create the customer(s): ' + ex.getMessage(), ex);
		}
		parseResponse(createResults);
	}

	/**
	 * @description			This will make the request to ECS to update customers with the request.
	 */
	public void makeResellerUpdateRequest() {
		checkExceptions();
		ECSSOAPCompany.companySoap service = setServiceSOAP();

		List<ECSSOAPCompany.resellerUpdateType> updateRequest = buildResellerUpdateUsingRequest();
		System.debug(LoggingLevel.INFO, 'updateRequest: ' + JSON.serialize(updateRequest));

		// create queryResult
		List<ECSSOAPCompany.companyResponseType> updateResults;

		try {
			updateResults = service.updateResellers(updateRequest);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException('Error received from BOP when attempting to update the customer(s): ' + ex.getMessage(), ex);
		}
		parseResponse(updateResults);
	}

	/**
	 * @description			This will build the create request object which will be sent to ECS.
	 */
	private List<ECSSOAPCompany.customerCreateType> buildCustomerCreateUsingRequest() {
		List<ECSSOAPCompany.customerCreateType> requestList = new List<ECSSOAPCompany.customerCreateType>();

		for (Request request : this.requests) {
			ECSSOAPCompany.customerCreateType thisCustomer = new ECSSOAPCompany.customerCreateType();

			thisCustomer.accountManagerEbu = request.accountManagerEbu;
			thisCustomer.banNumber = request.banNumber;

			thisCustomer.bopCode = request.bopCode;
			thisCustomer.referenceId = request.referenceId;
			thisCustomer.corporateId = request.corporateId;
			thisCustomer.countryId = request.countryId;
			thisCustomer.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisCustomer.internalInvoicing = request.internalInvoicing;
			thisCustomer.kvkNumber = request.kvkNumber;
			thisCustomer.name = request.name;
			if (thisCustomer.name.length() > 100) {
				thisCustomer.name = thisCustomer.name.subString(0, 100);
			}

			if (request.parentCompanyReferenceId != null) {
				// only create a parentCompany entry if there is a parent company
				thisCustomer.parentCompany = buildParentCompany(request);
			}

			thisCustomer.phone = StringUtils.cleanNLPhoneNumber(request.phone);

			if (request.resellerReferenceId != null) {
				// only create a reseller entry if there is a reseller
				thisCustomer.reseller = buildReseller(request);
			}
			thisCustomer.website = request.website;

			requestList.add(thisCustomer);
		}

		return requestList;
	}

	/**
	 * @description			This will build the update request object which will be sent to ECS.
	 */
	private List<ECSSOAPCompany.customerUpdateType> buildCustomerUpdateUsingRequest() {
		List<ECSSOAPCompany.customerUpdateType> requestList = new List<ECSSOAPCompany.customerUpdateType>();

		for (Request request : this.requests) {
			ECSSOAPCompany.customerUpdateType thisCustomer = new ECSSOAPCompany.customerUpdateType();

			thisCustomer.accountManagerEbu = request.accountManagerEbu;
			thisCustomer.banNumber = request.banNumber;

			thisCustomer.bopCode = request.bopCode;
			thisCustomer.referenceId = request.referenceId;
			thisCustomer.corporateId = request.corporateId;
			thisCustomer.countryId = request.countryId;
			thisCustomer.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisCustomer.internalInvoicing = request.internalInvoicing;
			thisCustomer.kvkNumber = request.kvkNumber;
			thisCustomer.name = request.name;
			if (thisCustomer.name.length() > 100) {
				thisCustomer.name = thisCustomer.name.subString(0, 100);
			}

			if (request.parentCompanyReferenceId != null) {
				// only create a parentCompany entry if there is a parent company
				thisCustomer.parentCompany = buildParentCompany(request);
			}

			thisCustomer.phone = StringUtils.cleanNLPhoneNumber(request.phone);

			if (request.resellerReferenceId != null) {
				// only create a reseller entry if there is a reseller
				thisCustomer.reseller = buildReseller(request);
			}

			thisCustomer.website = request.website;
			thisCustomer.companyGroup = request.companyGroup;

			requestList.add(thisCustomer);
		}

		return requestList;
	}

	/**
	 * @description			This will build the create request object which will be sent to ECS.
	 */
	private List<ECSSOAPCompany.resellerCreateType> buildResellerCreateUsingRequest() {
		List<ECSSOAPCompany.resellerCreateType> requestList = new List<ECSSOAPCompany.resellerCreateType>();

		for (Request request : this.requests) {
			ECSSOAPCompany.resellerCreateType thisCustomer = new ECSSOAPCompany.resellerCreateType();

			thisCustomer.accountManagerEbu = request.accountManagerEbu;
			thisCustomer.banNumber = request.banNumber;

			thisCustomer.bopCode = request.bopCode;
			thisCustomer.referenceId = request.referenceId;
			thisCustomer.corporateId = request.corporateId;
			thisCustomer.countryId = request.countryId;
			thisCustomer.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisCustomer.kvkNumber = request.kvkNumber;
			thisCustomer.name = request.name;
			thisCustomer.dealerCode = request.dealerCode;

			thisCustomer.phone = StringUtils.cleanNLPhoneNumber(request.phone);
			thisCustomer.website = request.website;

			requestList.add(thisCustomer);
		}

		return requestList;
	}

	/**
	 * @description			This will build the update request object which will be sent to ECS.
	 */
	private List<ECSSOAPCompany.resellerUpdateType> buildResellerUpdateUsingRequest() {
		List<ECSSOAPCompany.resellerUpdateType> requestList = new List<ECSSOAPCompany.resellerUpdateType>();

		for (Request request : this.requests) {
			ECSSOAPCompany.resellerUpdateType thisCustomer = new ECSSOAPCompany.resellerUpdateType();

			thisCustomer.accountManagerEbu = request.accountManagerEbu;
			thisCustomer.banNumber = request.banNumber;

			thisCustomer.bopCode = request.bopCode;
			thisCustomer.referenceId = request.referenceId;
			thisCustomer.corporateId = request.corporateId;
			thisCustomer.countryId = request.countryId;
			thisCustomer.fax = StringUtils.cleanNLPhoneNumber(request.fax);
			thisCustomer.kvkNumber = request.kvkNumber;
			thisCustomer.name = request.name;
			thisCustomer.dealerCode = request.dealerCode;
			thisCustomer.phone = StringUtils.cleanNLPhoneNumber(request.phone);
			thisCustomer.website = request.website;
			requestList.add(thisCustomer);
		}

		return requestList;
	}

	/**
	 * @description			This will parse the result returned by ECS.
	 * @param	updateResult	The result object returned by ECS which contains messages and the customer number.
	 */
	private void parseResponse(List<ECSSOAPCompany.companyResponseType> theResults) {
		//if(responses == null)
		responses = new List<Response>{};
		if (theResults == null) {
			return;
		}

		System.debug(LoggingLevel.INFO, '##### RAW RESPONSE: ' + theResults);

		for (ECSSOAPCompany.companyResponseType result : theResults) {
			if (result != null) {
				Response response = new Response();
				response.errorCode = result.errorCode;
				response.bopCode = result.bopCode;
				response.corporateId = result.corporateId;
				response.errorMessage = result.errorMessage;
				response.referenceId = result.referenceId;

				System.debug(LoggingLevel.INFO, '##### Messages: ' + response.errorMessage);

				responses.add(response);
			}
		}
	}

	/**
	 * @description			This will return the response returned by ECS. It will contain the customer
	 *						number and optionally a list of messages that may need to be displayed to the user.
	 */
	public Response[] getResponse() {
		return responses;
	}

	private ECSSOAPCompany.companySoap setServiceSOAP() {
		// WebService callout logic
		ECSSOAPCompany.companySoap service = new ECSSOAPCompany.companySoap();
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPCompany.authenticationHeader_element header = new ECSSOAPCompany.authenticationHeader_element();
		header.applicationId = 'sfdc';
		header.username = webServiceConfig.getUsername();
		header.password = webServiceConfig.getPassword();
		service.authenticationHeader = header;
		return service;
	}

	@testVisible
	private void checkExceptions() {
		if (webserviceConfig == null) {
			throw new ExWebServiceCalloutException('Missing configuration');
		}
		if (requests == null) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}
		if (requests.isEmpty()) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}
	}

	@testVisible
	private ECSSOAPCompany.companyRefType buildParentCompany(Request buildReq) {
		ECSSOAPCompany.companyRefType parentCompany = new ECSSOAPCompany.companyRefType();
		parentCompany.banNumber = buildReq.parentCompanyBanNumber;
		parentCompany.bopCode = buildReq.parentCompanyBopCode;
		parentCompany.referenceId = buildReq.parentCompanyReferenceId;
		parentCompany.corporateId = buildReq.parentCompanyCorporateId;
		return parentCompany;
	}
	@testVisible
	private ECSSOAPCompany.companyRefType buildReseller(Request buildReq) {
		ECSSOAPCompany.companyRefType reseller = new ECSSOAPCompany.companyRefType();
		reseller.banNumber = buildReq.resellerBanNumber;
		reseller.bopCode = buildReq.resellerBopCode;
		reseller.referenceId = buildReq.resellerReferenceId;
		reseller.corporateId = buildReq.resellerCorporateId;
		return reseller;
	}
}
