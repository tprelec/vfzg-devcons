@isTest
public with sharing class TestOrderEntryDataOld {
	@TestSetup
	static void makeData() {
		TestUtils.createCompleteOpportunity();
	}

	@isTest
	static void testDataCreation() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Contact cnt = [SELECT Id FROM Contact LIMIT 1];

		OrderEntryDataOld oeData = new OrderEntryDataOld();
		oeData.accountId = acc.Id;
		oeData.opportunityId = opp.Id;
		oeData.primaryContactId = cnt.Id;
		oeData.siteId = acc.Id;
		oeData.lastStep = 'last step';
		oeData.contractTerm = 'contract term';
		oeData.addressCheckResult = 'addressCheckResult';
		oeData.offNetType = 'offNetType';
		oeData.b2cInternetCustomer = false;
		oeData.notes = 'notes';

		OrderEntryDataOld.OrderEntryProduct oep = new OrderEntryDataOld.OrderEntryProduct();
		oep.id = acc.Id;
		oep.type = 'type';
		oeData.products = new List<OrderEntryDataOld.OrderEntryProduct>{ oep };

		OrderEntryDataOld.OrderEntryAddon oea = new OrderEntryDataOld.OrderEntryAddon();
		oea.id = acc.Id;
		oea.type = 'type';
		oea.name = 'name';
		oea.code = 'code';
		oea.quantity = 5;
		oea.recurring = 0.5;
		oea.oneOff = 5.0;
		oea.totalRecurring = 5.0;
		oea.totalOneOff = 5.0;
		oea.parentProduct = acc.Id;
		oea.bundleName = 'bundleName';
		oea.parentType = 'parentType';
		oeData.addons = new List<OrderEntryDataOld.OrderEntryAddon>{ oea };

		OrderEntryDataOld.OrderEntryBundle bundle = new OrderEntryDataOld.OrderEntryBundle();
		bundle.id = acc.Id;
		bundle.type = 'type';
		bundle.name = 'name';
		bundle.code = 'code';
		bundle.quantity = 5;
		bundle.recurring = 0.5;
		bundle.oneOff = 5.0;
		bundle.totalRecurring = 5.0;
		bundle.totalOneOff = 5.0;
		bundle.parentProduct = acc.Id;
		bundle.bundleName = 'bundleName';
		oeData.bundle = bundle;

		OrderEntryDataOld.Availability availability = new OrderEntryDataOld.Availability();
		availability.available = true;
		availability.name = 'name';
		OrderEntryDataOld.SiteCheck siteCheck = new OrderEntryDataOld.SiteCheck();
		siteCheck.availability = new List<OrderEntryDataOld.Availability>{ availability };
		siteCheck.city = 'city';
		siteCheck.street = 'street';
		siteCheck.houseNumber = 'houseNumber';
		siteCheck.houseNumberExt = 'houseNumberExt';
		siteCheck.zipCode = 'zipCode';
		siteCheck.status = new List<String>{ 'status1', 'status2' };
		siteCheck.footprint = 'footprint';
		oeData.siteCheck = siteCheck;

		OrderEntryDataOld.Telephony tel = new OrderEntryDataOld.Telephony();
		tel.enabled = false;
		tel.portingEnabled = true;
		tel.portingNumber = '13';
		Map<String, OrderEntryDataOld.Telephony> telMap = new Map<String, OrderEntryDataOld.Telephony>();
		telMap.put('tel1', tel);
		oeData.telephony = telMap;

		OrderEntryDataOld.Discount disc = new OrderEntryDataOld.Discount();
		disc.id = acc.Id;
		disc.name = 'name';
		disc.type = 'type';
		disc.duration = 12.0;
		disc.value = 13.0;
		disc.level = 'level';
		disc.product = acc.Id;
		disc.product = acc.Id;
		disc.addon = acc.Id;
		OrderEntryDataOld.Promotion prom = new OrderEntryDataOld.Promotion();
		prom.id = acc.Id;
		prom.name = 'name';
		prom.type = 'type';
		prom.contractTerms = 'contractTerms';
		prom.customerType = 'customerType';
		prom.connectionType = 'connectionType';
		prom.offNetType = 'offNetType';
		prom.discounts = new List<OrderEntryDataOld.Discount>{ disc };
		oeData.promos = new List<OrderEntryDataOld.Promotion>{ prom };

		OrderEntryDataOld.Payment payment = new OrderEntryDataOld.Payment();
		payment.bankAccountHolder = 'bankAccountHolder';
		payment.paymentType = 'paymentType';
		payment.iban = 'iban';
		payment.billingChannel = 'billingChannel';
		oeData.payment = payment;

		OrderEntryDataOld.PreferredDate d = new OrderEntryDataOld.PreferredDate();
		d.selectedDate = Date.today();
		d.dayPeriod = 'night';

		OrderEntryDataOld.Installation install = new OrderEntryDataOld.Installation();
		install.earliestInstallationDate = Date.today();
		install.assignTechnician = true;
		install.preferredDate1 = d;
		install.preferredDate2 = d;
		install.preferredDate3 = d;
		oeData.installation = install;

		OrderEntryDataOld.OperatorSwitch oSwitch = new OrderEntryDataOld.OperatorSwitch();
		oSwitch.requested = true;
		oSwitch.currentProvider = 'currentProvider';
		oSwitch.currentContractNumber = 'currentContractNumber';
		oSwitch.potentialFeeAccepted = true;
		oeData.operatorSwitch = oSwitch;

		System.assertEquals(oeData.accountId, acc.Id, 'Wrong value.');
		System.assertEquals(oeData.opportunityId, opp.Id, 'Wrong value.');
		System.assertEquals(oeData.primaryContactId, cnt.Id, 'Wrong value.');
		System.assertEquals(oeData.addressCheckResult, 'addressCheckResult', 'Wrong value.');
		System.assertEquals(oeData.b2cInternetCustomer, false, 'Wrong value.');
		System.assertEquals(oeData.addons[0].bundleName, 'bundleName', 'Wrong value.');
		System.assertEquals(
			oeData.operatorSwitch.currentProvider,
			'currentProvider',
			'Wrong value.'
		);
	}
}
