public without sharing class COM_DeliveryComponentsGenerator {
	public COM_MetadataInformation run(Set<Id> orderIdSet) {
		return prepareDataToGenerateComponents(getServiceIdList(orderIdSet));
	}

	public void updateDeliveryOrder(
		List<csord__Service__c> serviceList,
		List<COM_Delivery_Article_Materials__mdt> deliveryArticleMaterials,
		List<COM_Delivery_Component__mdt> deliveryComponentsMdt,
		List<COM_MetadataInformation.DeliveryComponent> deliveryComponents
	) {
		Set<Id> deliveryOrderIds = new Set<Id>();
		Map<Id, SObject> sobjectsToUpdateMap = new Map<Id, SObject>();
		Map<Id, List<COM_MetadataInformation.DeliveryComponent>> deliveryOrderIdDeliveryComponents = new Map<Id, List<COM_MetadataInformation.DeliveryComponent>>();

		String objectName = COM_Constants.DELIVERY_MAIN_OBJECT;

		Map<id, boolean> deliveryOrderHasAddActionsMap = new Map<id, boolean>();
		Map<id, boolean> deliveryOrderHasDeleteActionsMap = new Map<id, boolean>();
		Map<id, boolean> deliveryOrderHasChangeActionsMap = new Map<id, boolean>();
		List<csord__Service__c> updateWithTerminationRequested = new List<csord__Service__c>();

		for (csord__Service__c serviceRecord : serviceList) {
			if (serviceRecord.COM_Delivery_Order__c != null) {
				deliveryOrderIds.add(serviceRecord.COM_Delivery_Order__c);

				if (
					serviceRecord.csord__Service__c == null &&
					(serviceRecord.csordtelcoa__Delta_Status__c == COM_Constants.SERVICES_DELTA_STATUS_ADD ||
					serviceRecord.csordtelcoa__Delta_Status__c == '' ||
					serviceRecord.csordtelcoa__Delta_Status__c == null)
				) {
					deliveryOrderHasAddActionsMap.put(serviceRecord.COM_Delivery_Order__c, true);
				}

				if (
					serviceRecord.csord__Service__c == null &&
					serviceRecord.csordtelcoa__Delta_Status__c == COM_Constants.SERVICES_DELTA_STATUS_DELETE
				) {
					deliveryOrderHasDeleteActionsMap.put(serviceRecord.COM_Delivery_Order__c, true);
				}

				if (
					serviceRecord.csord__Service__c == null &&
					(serviceRecord.csordtelcoa__Delta_Status__c == COM_Constants.SERVICES_DELTA_STATUS_CHANGE &&
					serviceRecord.VFZ_Commercial_Article_Name__c != serviceRecord.csordtelcoa__Replaced_Service__r.VFZ_Commercial_Article_Name__c)
				) {
					deliveryOrderHasChangeActionsMap.put(serviceRecord.COM_Delivery_Order__c, true);
				}

				if (serviceRecord.csordtelcoa__Delta_Status__c == 'continuing in subscription') {
					for (csord__Service__c serviceRecordIteration : serviceList) {
						if (serviceRecord.Id == serviceRecordIteration.csord__Service__c) {
							if (
								serviceRecord.csordtelcoa__Delta_Status__c == COM_Constants.SERVICES_DELTA_STATUS_ADD ||
								serviceRecord.csordtelcoa__Delta_Status__c == COM_Constants.SERVICES_DELTA_STATUS_DELETE
							) {
								deliveryOrderHasChangeActionsMap.put(serviceRecord.COM_Delivery_Order__c, true);
							}
						}
					}
				}

				if (
					serviceRecord.csordtelcoa__Cancelled_By_Change_Process__c == true ||
					serviceRecord.COM_Delivery_Order__r.MACD_Type__c == COM_Constants.MACD_TYPE_TERMINATION
				) {
					serviceRecord.csord__Status__c = COM_Constants.SERVICE_STATUS_TERMINATION_REQUESTED;
					updateWithTerminationRequested.add(serviceRecord);
				}
			}
		}

		try {
			update updateWithTerminationRequested;
		} catch (Exception e) {
			LoggerService.log(e);
			System.debug(LoggingLevel.DEBUG, 'Database update exception: ' + e);
		}

		for (COM_MetadataInformation.DeliveryComponent deliveryComponent : deliveryComponents) {
			if (deliveryOrderIdDeliveryComponents.get(deliveryComponent.deliveryOrderId) != null) {
				deliveryOrderIdDeliveryComponents.get(deliveryComponent.deliveryOrderId).add(deliveryComponent);
			} else {
				deliveryOrderIdDeliveryComponents.put(
					deliveryComponent.deliveryOrderId,
					new List<COM_MetadataInformation.DeliveryComponent>{ deliveryComponent }
				);
			}
		}

		Map<Id, List<Asset>> assetsByDeliveryOrderId = COM_Helper.fetchAssetMapByDeliveryOrderId(deliveryOrderIdDeliveryComponents.keySet());
		Map<String, COM_Delivery_Materials__mdt> materialsByModelName = COM_Helper.fetchDeliveryMaterialsMapByModelName();

		for (Id deliveryOrderId : deliveryOrderIdDeliveryComponents.keySet()) {
			Boolean deliveryComponentInstallationRequired = false;
			Boolean deliveryComponentActionAdd = false;
			Boolean containsReturnableByBoxItem = false;
			Set<String> macdActions = new Set<String>();
			Set<String> deliveryComponentsSet = new Set<String>();
			Map<String, Object> fieldsAndValuesToAdd = new Map<String, Object>();

			for (COM_MetadataInformation.DeliveryComponent deliveryComponent : deliveryOrderIdDeliveryComponents.get(deliveryOrderId)) {
				if (deliveryComponent.installationRequired) {
					deliveryComponentInstallationRequired = true;
				}

				if (deliveryComponent.macdAction == 'Add') {
					deliveryComponentActionAdd = true;
				}

				//macdActions.add(deliveryComponent.macdAction);
				deliveryComponentsSet.add(deliveryComponent.name);
			}

			if (assetsByDeliveryOrderId.get(deliveryOrderId) != null) {
				for (Asset a : assetsByDeliveryOrderId.get(deliveryOrderId)) {
					if (materialsByModelName.get(a.Name).Returnable_by_Box__c) {
						containsReturnableByBoxItem = true;
					}
				}
			}

			if (containsReturnableByBoxItem) {
				fieldsAndValuesToAdd.put('Any_Items_Returnable_By_Box__c', true);
			}

			if (deliveryComponentInstallationRequired && deliveryComponentActionAdd) {
				fieldsAndValuesToAdd.put('Onsite_Visit_Required__c', true);
			} else {
				fieldsAndValuesToAdd.put('Onsite_Visit_Required__c', false);
			}

			if (deliveryOrderHasAddActionsMap.get(deliveryOrderId) == true) {
				macdActions.add(COM_Constants.MACD_ACTION_ADD);
			}

			if (deliveryOrderHasDeleteActionsMap.get(deliveryOrderId) == true) {
				macdActions.add(COM_Constants.MACD_ACTION_DELETE);
			}

			if (deliveryOrderHasChangeActionsMap.get(deliveryOrderId) == true) {
				macdActions.add(COM_Constants.MACD_ACTION_CHANGE);
			}

			if (!macdActions.isEmpty()) {
				fieldsAndValuesToAdd.put('MACD_Actions__c', String.join(new List<String>(macdActions), ','));
			}

			if (!deliveryComponentsSet.isEmpty()) {
				fieldsAndValuesToAdd.put('Delivery_Components__c', String.join(new List<String>(deliveryComponentsSet), ','));
			}

			for (String fieldName : fieldsAndValuesToAdd.keySet()) {
				if (sobjectsToUpdateMap.get(deliveryOrderId) == null) {
					SObject tmpSObject = Schema.getGlobalDescribe().get(objectName).newSObject();
					tmpSObject.Id = deliveryOrderId;
					tmpSObject.put(fieldName, fieldsAndValuesToAdd.get(fieldName));
					sobjectsToUpdateMap.put(deliveryOrderId, tmpSObject);
				} else {
					sobjectsToUpdateMap.get(deliveryOrderId).put(fieldName, fieldsAndValuesToAdd.get(fieldName));
				}
			}
		}

		if (!sobjectsToUpdateMap.values().isEmpty()) {
			update sobjectsToUpdateMap.values();
		}
	}

	public COM_MetadataInformation prepareDataToGenerateComponents(List<Id> serviceIds) {
		COM_DeliveryComponentService deliveryComponentService = new COM_DeliveryComponentService();
		COM_MetadataInformation metadataInformation = new COM_MetadataInformation();

		/**
		 * Add service commercial articles and components
		 */
		Set<String> serviceCommercialComponents = new Set<String>();
		Set<String> serviceCommercialArticles = new Set<String>();
		serviceCommercialArticles.add(COM_Constants.ALL_STAR_SIGN);

		List<sObject> serviceList = [
			SELECT
				Id,
				Name,
				VFZ_Commercial_Component_Name__c,
				VFZ_Commercial_Article_Name__c,
				csord__Service__c,
				csord__Service__r.Name,
				csord__Service__r.csordtelcoa__Replaced_Service__c,
				csord__Service__r.csordtelcoa__Replacement_Service__c,
				csord__Status__c,
				csordtelcoa__Replaced_Service__c,
				csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
				csord__Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
				COM_Delivery_Order__c,
				csord__Service__r.COM_Delivery_Order__c,
				csord__Subscription__c,
				Termination_Wish_Date__c,
				csordtelcoa__Delta_Status__c,
				csord__Service__r.csordtelcoa__Delta_Status__c,
				csordtelcoa__Cancelled_By_Change_Process__c,
				csord__Service__r.csordtelcoa__Cancelled_By_Change_Process__c,
				COM_Delivery_Order__r.MACD_Type__c,
				csordtelcoa__Replaced_Service__r.VFZ_Commercial_Article_Name__c
			FROM csord__Service__c
			WHERE Id IN :serviceIds
		];
		serviceCommercialComponents.addAll(getServiceCommercialComponents(serviceList));
		serviceCommercialArticles.addAll(getServiceCommercialArticles(serviceList));

		/**
		 * Create Delivery article mappings map
		 * Create Delivery component decomposition map
		 */
		Map<Id, COM_Decomposition__mdt> decompositionsMap = deliveryComponentService.getDecompositionsMap(
			serviceCommercialComponents,
			serviceCommercialArticles
		);
		Set<Id> deliveryArticleIds = new Set<Id>();
		Set<Id> deliveryComponentIds = new Set<Id>();
		for (COM_Decomposition__mdt decomp : decompositionsMap.values()) {
			deliveryArticleIds.add(decomp.Delivery_Article__c);
			deliveryComponentIds.add(decomp.Delivery_Article__r.Delivery_Component__c);
		}

		/**
		 * Create Delivery component map
		 */
		Map<Id, COM_Delivery_Component__mdt> deliveryComponentsMap = deliveryComponentService.getDeliveryComponentMap(deliveryComponentIds);

		/**
		 * Create Delivery component name map
		 */
		Map<String, COM_Delivery_Component__mdt> deliveryComponentNameDeliveryComponentMap = new Map<String, COM_Delivery_Component__mdt>();
		for (COM_Delivery_Component__mdt deliveryComponentMdtRecord : deliveryComponentsMap.values()) {
			deliveryComponentNameDeliveryComponentMap.put(deliveryComponentMdtRecord.Label, deliveryComponentMdtRecord);
		}

		/**
		 * Create Delivery articles map
		 */
		Map<Id, COM_Delivery_Article__mdt> deliveryArticlesMap = deliveryComponentService.getDeliveryArticlesMap(deliveryArticleIds);

		/**
		 * Create Component to Delivery articles map
		 */
		Map<Id, List<COM_Delivery_Article__mdt>> deliveryComponentDeliveryArticlesMap = new Map<Id, List<COM_Delivery_Article__mdt>>();
		for (COM_Delivery_Article__mdt deliveryArticle : deliveryArticlesMap.values()) {
			if (deliveryComponentDeliveryArticlesMap.get(deliveryArticle.Delivery_Component__c) != null) {
				deliveryComponentDeliveryArticlesMap.get(deliveryArticle.Delivery_Component__c).add(deliveryArticle);
			} else {
				deliveryComponentDeliveryArticlesMap.put(
					deliveryArticle.Delivery_Component__c,
					new List<COM_Delivery_Article__mdt>{ deliveryArticle }
				);
			}
		}

		/**
		 * Create Delivery Component activites list
		 */
		List<COM_Delivery_Component__mdt> deliveryComponents = deliveryComponentService.getDeliveryComponentMap(deliveryComponentIds).values();

		Set<String> deliveryComponentsRequiringInstallation = new Set<String>();
		Set<String> deliveryComponentsRequiringProvisioning = new Set<String>();

		for (COM_Delivery_Component__mdt delComp : deliveryComponents) {
			if (delComp.Installation__c) {
				deliveryComponentsRequiringInstallation.add(delComp.Label);
			}

			if (delComp.Provisioning__c) {
				deliveryComponentsRequiringProvisioning.add(delComp.Label);
			}
		}

		/**
		 * Create Delivery Component article materials list
		 */

		List<COM_Delivery_Article_Materials__mdt> deliveryArticleMaterials = deliveryComponentService.getDeliveryArticleMaterials(deliveryArticleIds);

		metadataInformation.setDeliveryComponents(deliveryComponentIds);
		metadataInformation.setDeliveryArticleMaterials(deliveryArticleMaterials);
		metadataInformation.setDeliveryComponents(deliveryComponents);
		metadataInformation.setDeliveryComponentsRequiringInstallation(deliveryComponentsRequiringInstallation);
		metadataInformation.setDeliveryComponentsRequiringProvisioning(deliveryComponentsRequiringProvisioning);
		metadataInformation.setDeliveryComponentNameDeliveryComponentMap(deliveryComponentNameDeliveryComponentMap);
		metadataInformation.setDecompositionsMap(decompositionsMap);
		metadataInformation.setDeliveryArticlesMap(deliveryArticlesMap);
		metadataInformation.setDeliveryComponentsMap(deliveryComponentsMap);
		metadataInformation.setServiceList(serviceList);
		return metadataInformation;
	}

	private List<COM_MetadataInformation.DeliveryComponent> createDeliveryComponents(
		csord__Service__c serviceRecord,
		Map<Id, COM_Decomposition__mdt> decompositionsMap,
		Map<Id, COM_Delivery_Article__mdt> deliveryArticlesMap,
		Map<Id, COM_Delivery_Component__mdt> deliveryComponentsMap,
		Set<String> deliveryComponentsRequiringInstallation,
		Set<String> deliveryComponentsRequiringProvisioning
	) {
		List<COM_MetadataInformation.DeliveryComponent> deliveryComponents = new List<COM_MetadataInformation.DeliveryComponent>();
		List<Id> deliveryArticleIds = new List<Id>();

		for (COM_Decomposition__mdt decomposition : decompositionsMap.values()) {
			if (
				(decomposition.Commercial_Component__c == serviceRecord.VFZ_Commercial_Component_Name__c &&
				decomposition.Commercial_Article__c == serviceRecord.VFZ_Commercial_Article_Name__c) ||
				(decomposition.Commercial_Component__c == serviceRecord.VFZ_Commercial_Component_Name__c &&
				decomposition.Commercial_Article__c == '*')
			) {
				if (decomposition.Delivery_Article__c != null) {
					deliveryArticleIds.add(decomposition.Delivery_Article__c);
				}
			}
		}

		for (Id deliveryArticleId : deliveryArticleIds) {
			COM_MetadataInformation.DeliveryComponent deliveryComponent = new COM_MetadataInformation.DeliveryComponent();
			COM_Delivery_Article__mdt deliveryArticle = deliveryArticlesMap.get(deliveryArticleId);
			COM_Delivery_Component__mdt deliveryComponentMdtRecord = deliveryComponentsMap.get(deliveryArticle.Delivery_Component__c);

			deliveryComponent.name = deliveryComponentMdtRecord.Label;
			deliveryComponent.serviceId = serviceRecord.Id;
			deliveryComponent.deliveryOrderId = serviceRecord.COM_Delivery_Order__c == null
				? serviceRecord.csord__Service__r.COM_Delivery_Order__c
				: serviceRecord.COM_Delivery_Order__c; // COM_Delivery_Order__c
			deliveryComponent.articleName = deliveryArticle.Delivery_Article_Name__c;
			deliveryComponent.articleCode = deliveryArticle.Delivery_Article_Code__c;
			deliveryComponent.installationRequired = getInstallationRequiredMetadataActivity(
				deliveryComponent.Name,
				deliveryComponentsRequiringInstallation
			);
			deliveryComponent.provisioningRequired = getProvisioningRequiredMetadataActivity(
				deliveryComponent.Name,
				deliveryComponentsRequiringProvisioning
			);
			deliveryComponent.macdAction = 'Add';
			deliveryComponents.add(deliveryComponent);
		}
		return deliveryComponents;
	}

	private Boolean getInstallationRequiredMetadataActivity(String deliveryComponentName, Set<String> deliveryComponentsRequiringInstallation) {
		Boolean returnValue = false;

		if (deliveryComponentsRequiringInstallation.contains(deliveryComponentName)) {
			returnValue = true;
		}

		return returnValue;
	}

	private Boolean getProvisioningRequiredMetadataActivity(String deliveryComponentName, Set<String> deliveryComponentsRequiringProvisioning) {
		Boolean returnValue = false;

		if (deliveryComponentsRequiringProvisioning.contains(deliveryComponentName)) {
			returnValue = true;
		}

		return returnValue;
	}

	private Set<String> getServiceCommercialComponents(List<csord__Service__c> services) {
		Set<String> returnValue = new Set<String>();

		for (csord__Service__c serviceRecord : services) {
			if (serviceRecord.VFZ_Commercial_Component_Name__c != null) {
				returnValue.add(serviceRecord.VFZ_Commercial_Component_Name__c);
			}
		}
		return returnValue;
	}

	private Set<String> getServiceCommercialArticles(List<csord__Service__c> services) {
		Set<String> returnValue = new Set<String>();

		for (csord__Service__c serviceRecord : services) {
			if (serviceRecord.VFZ_Commercial_Article_Name__c != null) {
				returnValue.add(serviceRecord.VFZ_Commercial_Article_Name__c);
			}
		}
		return returnValue;
	}

	private List<sObject> getServiceList(Set<Id> orderIds) {
		return [SELECT id FROM csord__Service__c WHERE csord__Order__c IN :orderIds];
	}

	private List<Id> getServiceIdList(Set<Id> orderIdSet) {
		List<Id> serviceIdList = new List<Id>();
		List<csord__Service__c> serviceRecordList = getServiceList(orderIdSet);

		for (csord__Service__c serviceRecord : serviceRecordList) {
			serviceIdList.add(serviceRecord.Id);
		}

		return serviceIdList;
	}

	public List<COM_MetadataInformation.DeliveryComponent> generateDeliveryComponents(COM_MetadataInformation metadataInformation) {
		List<COM_MetadataInformation.DeliveryComponent> deliveryComponents = new List<COM_MetadataInformation.DeliveryComponent>();

		for (csord__Service__c serviceRecord : (List<csord__Service__c>) metadataInformation.getServiceList()) {
			if (serviceRecord.VFZ_Commercial_Article_Name__c != null && serviceRecord.VFZ_Commercial_Component_Name__c != null) {
				deliveryComponents.addAll(
					createDeliveryComponents(
						serviceRecord,
						metadataInformation.getDecompositionsMap(),
						metadataInformation.getDeliveryArticlesMap(),
						metadataInformation.getDeliveryComponentsMap(),
						metadataInformation.getDeliveryComponentsRequiringInstallation(),
						metadataInformation.getDeliveryComponentsRequiringProvisioning()
					)
				);
			}
		}

		return deliveryComponents;
	}
}
