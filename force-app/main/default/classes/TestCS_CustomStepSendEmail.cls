@IsTest
private class TestCS_CustomStepSendEmail {
	@TestSetup
	static void testSetup() {
		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
		CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);
		CSPOFA__Orchestration_Step_Template__c step2Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 2', '2', true);

		Account tmpAcc = CS_DataTest.createAccount('Test Account');
		insert tmpAcc;

		csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
		insert ord;

		Contact testContact = CS_DataTest.createContact('Contact Name', 'String lastName', 'not so importan role', 'mail@address.com', tmpAcc.Id);
		testContact.Language__c = 'Engels';
		insert testContact;

		Site__c testSite = CS_DataTest.createSite('LEIDEN, Breestraat 112', tmpAcc, '1111AA', 'Breestraat', 'LEIDEN', 112);
		insert testSite;

		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrder('DeliveryOrder_EmailTest', ord.Id, false);
		deliveryOrder.Technical_Contact__c = testContact.Id;
		deliveryOrder.Installation_Start__c = Datetime.now().AddDays(1);
		deliveryOrder.Installation_End__c = Datetime.now().AddDays(2);
		deliveryOrder.Products__c = 'Product A, Product B';
		deliveryOrder.Site__c = testSite.Id;
		insert deliveryOrder;

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = deliveryOrder.Id;
		testProcess.Name = 'EmailTest_Process';
		insert testProcess;

		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);
	}

	@IsTest
	static void testOrchestrationCustomMail() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
			List<SObject> objects = new List<SObject>();

			CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate(
				'Test process Template2',
				'5',
				true
			);
			CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);
			CSPOFA__Orchestration_Step_Template__c step2Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 2', '2', true);

			VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(simpleUser, false);
			vfContract.Implementation_Manager__c = simpleUser.Id;
			insert vfContract;

			CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
				testProcessTemplate.Id,
				null,
				Datetime.newInstance(2020, 3, 27),
				Datetime.newInstance(2020, 3, 27),
				false
			);
			testProcess.Contract_VF__c = vfContract.Id;
			insert testProcess;

			CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);
			objects.add(step1);

			CSPOFA__Orchestration_Step__c step2 = CS_DataTest.createOrchStep(step2Template.Id, testProcess.Id, false);
			step2.CS_Email_Template__c = 'Test_Email_Template';
			insert step2;
			objects.add(step2);

			CS_CustomStepSendEmail csCustomSendEmail = new CS_CustomStepSendEmail();
			List<CSPOFA__Orchestration_Step__c> result = csCustomSendEmail.process(objects);

			for (CSPOFA__Orchestration_Step__c step : result) {
				System.assertNotEquals(null, step.CSPOFA__Message__c, 'Step was not updated.');
			}
		}
	}

	@IsTest
	static void customStepSendEmailTestWithDeliveryOrder() {
		List<SObject> objects = new List<SObject>();

		CSPOFA__Orchestration_Step_Template__c step2Template = [SELECT Id FROM CSPOFA__Orchestration_Step_Template__c WHERE Name = 'Step 2'];
		CSPOFA__Orchestration_Process__c testProcess = [SELECT Id FROM CSPOFA__Orchestration_Process__c WHERE Name = 'EmailTest_Process'];

		CSPOFA__Orchestration_Step__c step2 = CS_DataTest.createOrchStep(step2Template.Id, testProcess.Id, false);
		step2.CS_Email_Template__c = 'Test_Email_Template';
		insert step2;
		objects.add(step2);

		CS_CustomStepSendEmail csCustomSendEmail = new CS_CustomStepSendEmail();
		List<CSPOFA__Orchestration_Step__c> result = csCustomSendEmail.process(objects);

		for (CSPOFA__Orchestration_Step__c step : result) {
			System.assert(step.CSPOFA__Status__c != 'Error', 'Process step ended with exceptions' + step.CSPOFA__Message__c);
		}
	}

	@IsTest
	static void testCreateSingleEmailMessageForDeliveryOrder() {
		List<SObject> objects = new List<SObject>();

		CSPOFA__Orchestration_Step_Template__c step2Template = [SELECT Id FROM CSPOFA__Orchestration_Step_Template__c WHERE Name = 'Step 2'];
		CSPOFA__Orchestration_Process__c testProcess = [SELECT Id FROM CSPOFA__Orchestration_Process__c WHERE Name = 'EmailTest_Process'];

		CSPOFA__Orchestration_Step__c step2 = CS_DataTest.createOrchStep(step2Template.Id, testProcess.Id, false);
		step2.CS_Email_Template__c = 'Test_Email_Template';
		insert step2;
		objects.add(step2);

		EmailTemplate emailTemplate = [
			SELECT ID, Subject, Name, DeveloperName, HtmlValue, Body, TemplateType
			FROM EmailTemplate
			WHERE Name = 'Test_Email_Template'
		];

		COM_Delivery_Order__c deliveryOrder = [
			SELECT
				Id,
				Name,
				Technical_Contact__c,
				Technical_Contact__r.Email,
				Technical_Contact__r.Name,
				Technical_Contact__r.Language__c,
				Site__r.Name,
				Site__r.Site_Street__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_City__c,
				Installation_Start__c,
				Installation_End__c,
				Products__c,
				Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
				(SELECT id, Name, csordtelcoa__Delta_Status__c FROM Services__r)
			FROM COM_Delivery_Order__c
			WHERE Name = 'DeliveryOrder_EmailTest'
		];

		CS_CustomStepSendEmail csCustomSendEmail = new CS_CustomStepSendEmail();
		Messaging.SingleEmailMessage result = CS_CustomStepSendEmail.createSingleEmailMessageForDeliveryOrder(step2, deliveryOrder, emailTemplate);

		System.assertNotEquals(null, result, 'Email message has been created.');
	}

	@IsTest
	static void testGetFileAttachments() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
			List<Document> documentList = CS_DataTest.createDocuments(2, simpleUser, 'Engels', true);
			List<Messaging.EmailFileAttachment> result = new List<Messaging.EmailFileAttachment>();

			result = CS_CustomStepSendEmail.getFileAttachments(documentList, 'Engels');
			system.assertEquals(documentList.size(), result.size(), 'Size is not the same.');
		}
	}

	@isTest
	static void testCalculateDestinationAddress() {
		CSPOFA__Orchestration_Step__c step1 = new CSPOFA__Orchestration_Step__c();
		step1.CS_Email_To__c = 'TestEmail1';
		CSPOFA__Orchestration_Step__c step2 = new CSPOFA__Orchestration_Step__c();

		Test.startTest();
		String result1 = CS_CustomStepSendEmail.calculateDestinationAddress(step1, 'TestEmail2');
		String result2 = CS_CustomStepSendEmail.calculateDestinationAddress(step2, 'TestEmail2');
		Test.stopTest();

		System.assertEquals('TestEmail1', result1, 'Email Address is not correct.');
		System.assertEquals('TestEmail2', result2, 'Email Address is not correct.');
	}
}
