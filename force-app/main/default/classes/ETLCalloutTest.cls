@isTest
public class ETLCalloutTest {
	@isTest
	static void testGetPRGsForPublication() {
		final String DEFAULT_PRICING_RULE_GROUP_CODE = 'Group-001';
		final String DEFAULT_PRICING_RULE_GROUP_NAME = 'Default PRG';
		final String CURRENCY_CODE = 'INR';

		cspmb__Pricing_Rule_Group__c defaultPriceGroup = new cspmb__Pricing_Rule_Group__c(
			name = DEFAULT_PRICING_RULE_GROUP_NAME,
			cspmb__pricing_rule_group_code__c = DEFAULT_PRICING_RULE_GROUP_CODE,
			sfcc_currency_code__c = CURRENCY_CODE
		);
		insert defaultPriceGroup;

		System.Test.startTest();
		List<cspmb__Pricing_Rule_Group__c> prgs = ETLCallout.getPRGsForPublication('a2p3z000001a6xIAAQ');
		System.Test.stopTest();

		System.assertEquals(1, prgs.size());
		System.assertEquals(DEFAULT_PRICING_RULE_GROUP_NAME, prgs[0].Name, 'Call to getPRGsForPublication should return Group-001 PRG');
		System.assertEquals(
			DEFAULT_PRICING_RULE_GROUP_CODE,
			prgs[0].cspmb__pricing_rule_group_code__c,
			'Call to getPRGsForPublication should return cspmb__pricing_rule_group_code__c for returned PRGs'
		);
		System.assertEquals(
			CURRENCY_CODE,
			prgs[0].sfcc_currency_code__c,
			'Call to getPRGsForPublication should return sfcc_currency_code__c for returned PRGs'
		);
	}

	@isTest
	static void testETLPayload() {
		final String DEFAULT_PRICING_RULE_GROUP_CODE = 'Group-001';
		final String DEFAULT_PRICING_RULE_GROUP_NAME = 'Default PRG';
		final String CURRENCY_CODE = 'INR';
		final String CLIENT_ID = 'test-client-id';
		final String CATALOG_NAME = 'testcatalogname';

		final String CUSTOM_FIELD_SCHEMA = '{}';

		CCETLSettings__c etlSettings = new CCETLSettings__c();
		etlSettings.Client_ID__c = CLIENT_ID;
		etlSettings.Import_Pricebooks__c = true;
		etlSettings.Import_Inventory__c = true;
		etlSettings.Import_Catalog_Identifiers__c = true;
		etlSettings.Import_Recommendation__c = true;
		etlSettings.Import_Catalog__c = true;
		insert etlSettings;

		DCETLCatalogImportOptions__c etlCatalogImportOptions = new DCETLCatalogImportOptions__c();
		etlCatalogImportOptions.Import_Display_Labels__c = true;
		etlCatalogImportOptions.Import_Categories__c = true;
		etlCatalogImportOptions.Import_User_Defined_Attributes__c = true;
		insert etlCatalogImportOptions;

		cspmb__Catalogue__c testCatalog = new cspmb__Catalogue__c();
		testCatalog.Name = CATALOG_NAME;
		testCatalog.cc_etl_custom_field_schema__c = CUSTOM_FIELD_SCHEMA;
		insert testCatalog;

		csb2c__E_Commerce_Publication__c testPublication = new csb2c__E_Commerce_Publication__c();
		insert testPublication;

		csb2c__Publication_Catalogue_Association__c testPublicationAssocication = new csb2c__Publication_Catalogue_Association__c();
		testPublicationAssocication.csb2c__Catalogue__c = testCatalog.Id;
		testPublicationAssocication.csb2c__Publication__c = testPublication.Id;
		insert testPublicationAssocication;

		cspmb__Pricing_Rule_Group__c defaultPriceGroup = new cspmb__Pricing_Rule_Group__c(
			name = DEFAULT_PRICING_RULE_GROUP_NAME,
			cspmb__pricing_rule_group_code__c = DEFAULT_PRICING_RULE_GROUP_CODE,
			sfcc_currency_code__c = CURRENCY_CODE
		);
		insert defaultPriceGroup;

		System.Test.startTest();
		ETLCallout.PublicationPayload payload = ETLCallout.getETLPayload(testPublication.Id);
		System.Test.stopTest();

		ETLCallout.CatalogImportOptions expectedCatalogImportOptions = new ETLCallout.CatalogImportOptions();
		expectedCatalogImportOptions.displayLabels = true;
		expectedCatalogImportOptions.categories = true;
		expectedCatalogImportOptions.userDefinedAttributes = true;

		ETLCallout.ImportOptions expectedImportOptions = new ETLCallout.ImportOptions();
		expectedImportOptions.priceBook = true;
		expectedImportOptions.inventory = true;
		expectedImportOptions.identifiers = true;
		expectedImportOptions.recommendations = true;
		expectedImportOptions.catalog = true;
		expectedImportOptions.catalogImportOptions = expectedCatalogImportOptions;

		ETLCallout.PRG expectedPRG = new ETLCallout.PRG();
		expectedPRG.id = DEFAULT_PRICING_RULE_GROUP_CODE;
		expectedPRG.name = DEFAULT_PRICING_RULE_GROUP_NAME;
		expectedPRG.currencyCode = CURRENCY_CODE;

		System.assertEquals(testPublication.Id, payload.publicationId, 'should return publication id');
		System.assertEquals(CLIENT_ID, payload.clientId, 'should return client id');
		System.assertEquals(testCatalog.Id, payload.elasticCatalogId, 'should return elasticCatalogId');
		System.assertEquals(CATALOG_NAME, payload.elasticCatalogName, 'should return elasticCatalogName');
		System.assertEquals(JSON.serialize(expectedImportOptions), JSON.serialize(payload.importOptions), 'should return import options');
		System.assertEquals(JSON.serialize(expectedPRG), JSON.serialize(payload.prgs[0]), 'should return prgs');
	}
}
