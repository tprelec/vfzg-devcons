public with sharing class COM_CloseTaskController {
	@AuraEnabled
	public static String closeTask(Id recordId) {
		String result = '';

		Task task = [
			SELECT
				Id,
				Record_Type_Text__c,
				OwnerId,
				Owner_Type__c,
				Delivery_Task_Finished__c,
				WhatId,
				COM_Delivery_Order__c,
				Type,
				COM_Delivery_Order__r.On_Hold__c
			FROM Task
			WHERE Id = :recordId
		];

		try {
			if (task != null) {
				if (task.COM_Delivery_Order__r.On_Hold__c == true) {
					throw new COM_CustomException(COM_Constants.NO_CHANGES_DURING_INFLIGHT_ERROR_MSG);
				}

				if (task.Record_Type_Text__c.equals('COM_Delivery')) {
					if (task.Type.equals('COM_Installation') || task.Type.equals('COM_Installation Jeopardy')) {
						closeTaskForDeliveryOrderInstallation(task);
					}
					updateTaskStatus(task, 'Completed');
					result = 'Task Closed';
				}
			}
		} catch (Exception e) {
			return e.getMessage();
		}

		return result;
	}

	private static void updateTaskStatus(Task task, String status) {
		task.Status = status;
		task.Delivery_Task_Finished__c = true;

		List<Task> tasksToUpdate = new List<Task>();
		tasksToUpdate.add(task);
		update tasksToUpdate;
	}

	@TestVisible
	private static Task closeTaskForDeliveryOrderInstallation(Task task) {
		Boolean servicesImplemented = true;

		Task taskWithAttachments = [
			SELECT
				Id,
				COM_Delivery_Order__c,
				COM_Delivery_Order__r.Installation_Issue_Category__c,
				COM_Delivery_Order__r.Installation_Issue_Subcategories__c,
				COM_Delivery_Order__r.Installation_Issue_Comments__c,
				(SELECT Id FROM CombinedAttachments)
			FROM Task
			WHERE Id = :task.Id
		];

		if (taskWithAttachments != null) {
			for (csord__Service__c service : [
				SELECT Id, Implemented_Date__c
				FROM csord__Service__c
				WHERE COM_Delivery_Order__c = :taskWithAttachments.COM_Delivery_Order__c AND Has_Delivery_Components__c = TRUE
			]) {
				if (service.Implemented_Date__c == null) {
					servicesImplemented = false;
				}
			}

			if (servicesImplemented && taskWithAttachments.CombinedAttachments.size() == 0) {
				throw new COM_CustomException(COM_Constants.IMPLEMENTED_TASK_NEEDS_ATTACHMENTS_ERROR_MSG);
			}
		}

		return task;
	}
}
