@isTest
public class CS_MobileHardwarePriceItemLookupTest
{
    private static testMethod void testRequiredAttributes(){
         CS_MobileHardwarePriceItemLookup caLookup = new  CS_MobileHardwarePriceItemLookup();
        caLookup.getRequiredAttributes();
    }
  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
     System.runAs (simpleUser) { 
      
     
      Framework__c frameworkSetting = new Framework__c();
      frameworkSetting.Framework_Sequence_Number__c = 2;
      insert frameworkSetting;
      
      PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
     insert priceResetSetting;
      Account testAccount = CS_DataTest.createAccount('Test Account');
      insert testAccount;
      
      Sales_Settings__c ssettings = new Sales_Settings__c();
      ssettings.Postalcode_check_validity_days__c = 2;
      ssettings.Max_Daily_Postalcode_Checks__c = 2;
      ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
      ssettings.Postalcode_check_block_period_days__c = 2;
      ssettings.Max_weekly_postalcode_checks__c = 15;
      insert ssettings;
      
      
      //price items
      OrderType__c orderType = CS_DataTest.createOrderType();
      insert orderType;
      Product2 product1 = CS_DataTest.createProduct('Mobile hardware', orderType);
      insert product1;
      
      Category__c mobileCategory = CS_DataTest.createCategory('Mobile hardware');
      insert mobileCategory;
      
      Vendor__c vendor1 = CS_DataTest.createVendor('KPNWEAS');
      insert vendor1;
    
        
    
        cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(product1, mobileCategory, 20480.0, 20480.0, vendor1, 'ONNET', 'OfficeTTR2BD');
        priceItem1.mobile_add_on_category__c = 'Mobile hardware';
        priceItem1.Mobile_Scenario_Text__c = 'Connect subscription';
        insert priceItem1;
      
      
      
      Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
      insert testOpp;
      
      
      cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        insert basket;
        
       
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
        
        cscfga__Product_Definition__c mobileCTNProfileDef = CS_DataTest.createProductDefinition('Mobile CTN Profile');
        mobileCTNProfileDef.Product_Type__c = 'Mobile';
        mobileCTNProfileDef.RecordTypeId = productDefinitionRecordType;
        insert mobileCTNProfileDef;
        
        
        
        
        cscfga__Product_Configuration__c mobileCTNProfileConf = CS_DataTest.createProductConfiguration(mobileCTNProfileDef.Id, 'Mobile CTN Profile',basket.Id);
        mobileCTNProfileConf.cscfga__Root_Configuration__c = null;
        mobileCTNProfileConf.cscfga__Parent_Configuration__c = null;
        insert mobileCTNProfileConf;
        
        
        
       
        csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(mobileCTNProfileConf);
        pcr.csbb__Product_Configuration__c = mobileCTNProfileConf.Id;
        insert pcr;
        
        
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('Subscription', 'Connect subscription');
        searchFields.put('Scenario', 'RedPro');
        
        
        CS_MobileHardwarePriceItemLookup caLookup = new CS_MobileHardwarePriceItemLookup();
        caLookup.doLookupSearch(searchFields, String.valueOf(mobileCTNProfileDef.Id),null, 0, 0);
     
        searchFields.put('Scenario', 'Data Only');
        caLookup.doLookupSearch(searchFields, String.valueOf(mobileCTNProfileDef.Id),null, 0, 0);
     }
  }

}