@isTest
private class TestCOM_DXLCreateBSSOrderListener {
	@testSetup
	static void setup() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);

		Opportunity testOpp = CS_DataTest.createOpportunity(acct, 'Test Opp', UserInfo.getUserId());
		testOpp.csordtelcoa__Change_Type__c = 'Change';
		testOpp.Name = 'testOpp';
		testOpp.StageName = 'Ready for Order';
		testOpp.CloseDate = System.today();
		testOpp.AccountId = acct.Id;
		insert testOpp;

		cscfga__Product_Basket__c testBasket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
		testBasket.Name = 'testBasket';
		testBasket.cscfga__Opportunity__c = testOpp.Id;
		insert testBasket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c businessInternetDef = CS_DataTest.createProductDefinition('Business Internet');
		businessInternetDef.RecordTypeId = productDefinitionRecordType;
		businessInternetDef.Product_Type__c = 'Fixed';
		insert businessInternetDef;

		cscfga__Product_Configuration__c businessInternetConf = CS_DataTest.createProductConfiguration(
			businessInternetDef.Id,
			'Business Internet',
			testBasket.Id
		);
		insert businessInternetConf;

		csord__Order__c order = new csord__Order__c();
		order.csord__Identification__c = 'Order_' + CS_DataTest.generateRandomString(6);
		insert order;

		Site__c site = CS_DataTest.createSite('Utrecht Lestraat 12', acct, '3572RE', 'Lestraat', 'Utrecht', 12);
		site.Footprint__c = null;
		site.Site_House_Number_Suffix__c = 'A';
		site.Country__c = 'NL';
		insert site;

		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', order.Id, false);
		insert deliveryOrder;

		csord__Subscription__c businessInternetSub = CS_DataTest.createSubscription(businessInternetConf.Id);
		businessInternetSub.csord__Identification__c = 'Business_Internet_Sub_' + CS_DataTest.generateRandomString(6);
		businessInternetSub.csord__Status__c = 'Subscription created';
		businessInternetSub.csord__Order__c = order.Id;
		insert businessInternetSub;

		List<csord__Service__c> servicesToInsert = new List<csord__Service__c>();

		csord__Service__c businessInternetService = CS_DataTest.createService(
			order.Id,
			deliveryOrder.Id,
			'Business Internet',
			testBasket.Id,
			businessInternetSub.Id,
			site.Id,
			null,
			businessInternetConf.Id
		);
		servicesToInsert.add(businessInternetService);

		csord__Service__c businessInternetService2 = CS_DataTest.createService(
			order.Id,
			deliveryOrder.Id,
			'Business Internet',
			testBasket.Id,
			businessInternetSub.Id,
			site.Id,
			null,
			businessInternetConf.Id
		);
		businessInternetService2.Deprovisioned_Date__c = DateTime.newInstance(2022, 5, 14);
		servicesToInsert.add(businessInternetService2);

		insert servicesToInsert;

		List<csord__Service__c> childServicesToInsert = new List<csord__Service__c>();

		csord__Service__c businessInternet2ChildService1 = CS_DataTest.createService(
			order.Id,
			deliveryOrder.Id,
			'Business Internet Child',
			testBasket.Id,
			businessInternetSub.Id,
			site.Id,
			null,
			businessInternetConf.Id
		);
		businessInternet2ChildService1.csord__Service__c = businessInternetService2.Id;
		childServicesToInsert.add(businessInternet2ChildService1);

		csord__Service__c businessInternet1ChildService1 = CS_DataTest.createService(
			order.Id,
			deliveryOrder.Id,
			'Business Internet Child',
			testBasket.Id,
			businessInternetSub.Id,
			site.Id,
			null,
			businessInternetConf.Id
		);
		businessInternet1ChildService1.csord__Service__c = businessInternetService.Id;
		childServicesToInsert.add(businessInternet1ChildService1);

		insert childServicesToInsert;

		List<csord__Service_Line_Item__c> slisToInsert = new List<csord__Service_Line_Item__c>();

		csord__Service_Line_Item__c businessInternetSLIOneOff = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet One Off SLI',
			businessInternetService.Id,
			false,
			0,
			'Charge',
			'Internet SLI OF'
		);
		slisToInsert.add(businessInternetSLIOneOff);

		csord__Service_Line_Item__c businessInternetSLIRec = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet Recurring SLI',
			businessInternetService.Id,
			true,
			0,
			'Charge',
			'Internet SLI REC'
		);
		slisToInsert.add(businessInternetSLIRec);

		csord__Service_Line_Item__c businessInternetSLIOneOff2 = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet One Off SLI',
			businessInternetService2.Id,
			false,
			0,
			'Charge',
			'Internet SLI OF'
		);
		slisToInsert.add(businessInternetSLIOneOff2);

		csord__Service_Line_Item__c businessInternetSLIRec2 = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet Recurring SLI',
			businessInternetService2.Id,
			true,
			0,
			'Charge',
			'Internet SLI REC'
		);
		slisToInsert.add(businessInternetSLIRec2);

		csord__Service_Line_Item__c businessInternetSLIOneOff21 = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet One Off SLI',
			businessInternet2ChildService1.Id,
			false,
			0,
			'Charge',
			'Internet SLI OF'
		);
		slisToInsert.add(businessInternetSLIOneOff21);

		csord__Service_Line_Item__c businessInternetSLIRec22 = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet Recurring SLI',
			businessInternet2ChildService1.Id,
			true,
			0,
			'Charge',
			'Internet SLI REC'
		);
		slisToInsert.add(businessInternetSLIRec22);

		insert slisToInsert;

		String affectedObjects =
			'[{"type":"csord__Service_Line_Item__c","sfdcId":"' +
			businessInternetSLIOneOff.Id +
			'"},{"type":"csord__Service_Line_Item__c","sfdcId":"' +
			businessInternetSLIRec.Id +
			'"}]';
		Com_dxl_integration_log__c comdxlintegrationlogcObj = new Com_dxl_integration_log__c(
			Transaction_Id__c = 'TR-123456',
			Type__c = 'createCustomer',
			Status__c = 'Success',
			Affected_Objects__c = affectedObjects,
			Error__c = 'Test Value'
		);
		insert comdxlintegrationlogcObj;
	}

	@IsTest
	static void testUpdateServiceLineItems() {
		List<csord__Service_Line_Item__c> serviceLineItemList = [SELECT Id, Name FROM csord__Service_Line_Item__c];

		String payload = COM_DXLNotificationListenerService.generateAsyncReponse('COM_DXL_CREATE_BSS_ORDER_ASYNC_RESPONSE');
		COM_DXLCreateBSSOrderAsyncResponse asyncResponse = (COM_DXLCreateBSSOrderAsyncResponse) JSON.deserialize(
			payload,
			COM_DXLCreateBSSOrderAsyncResponse.class
		);

		for (Integer i = 0; i < asyncResponse.payload.orderLines.size(); i++) {
			asyncResponse.payload.orderLines[i].externalReferenceId = serviceLineItemList[i].Id;
			if (Math.mod(i, 2) == 0) {
				asyncResponse.payload.orderLines[i].orderLineStatus.statusCode = '500';
				asyncResponse.payload.orderLines[i].orderLineStatus.statusMessage = 'Error message';
			}
		}

		COM_DXLCreateBSSOrderListener orderListener = new COM_DXLCreateBSSOrderListener(asyncResponse, 'TR-123456');
		orderListener.processPayload();

		List<csord__Service_Line_Item__c> sliList = [SELECT Id, Name, Billing_Status__c FROM csord__Service_Line_Item__c];

		for (csord__Service_Line_Item__c sli : sliList) {
			for (Integer i = 0; i < asyncResponse.payload.orderLines.size(); i++) {
				if (
					sli.Id == asyncResponse.payload.orderLines[i].externalReferenceId &&
					asyncResponse.payload.orderLines[i].orderLineStatus.statusCode.equals('200')
				) {
					System.assertEquals('Active', sli.Billing_Status__c, 'Expected: ' + 'Active' + ' Actual: ' + sli.Billing_Status__c);
				} else if (
					sli.Id == asyncResponse.payload.orderLines[i].externalReferenceId &&
					!asyncResponse.payload.orderLines[i].orderLineStatus.statusCode.equals('200')
				) {
					System.assertEquals('Failed', sli.Billing_Status__c, 'Expected: ' + 'Failed' + ' Actual: ' + sli.Billing_Status__c);
				}
			}
		}

		String msg = 'Expected: true Actual: ' + orderListener.payloadValid;
		Boolean expected = true;
		Boolean actual = orderListener.payloadValid;
		System.assertEquals(expected, actual, msg);
	}
}
