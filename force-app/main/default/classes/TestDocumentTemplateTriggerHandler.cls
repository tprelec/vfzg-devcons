/**
 * @description       : Apex Test Class for the DocumentTemplateTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestDocumentTemplateTriggerHandler {
	@testSetup
	static void setup() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'Document Template',
			External_Number__c = 1,
			Object_Prefix__c = 'DT-14-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;

		csclm__Document_Definition__c objDocumentDefinition = CS_DataTest.createDocumentDefinition();
		insert objDocumentDefinition;
	}

	@isTest
	static void testSetExternalIds() {
		csclm__Document_Definition__c objDocumentDefinition = [SELECT Id FROM csclm__Document_Definition__c LIMIT 1];

		csclm__Document_Template__c objDocumentTemplate = CS_DataTest.createDocumentTemplate('Test');
		objDocumentTemplate.csclm__Document_Definition__c = objDocumentDefinition.Id;
		objDocumentTemplate.csclm__Valid__c = true;
		objDocumentTemplate.csclm__Active__c = true;

		Test.startTest();
		insert objDocumentTemplate;
		Test.stopTest();

		csclm__Document_Template__c objDocumentTemplateUpdated = [
			SELECT Id, ExternalID__c
			FROM csclm__Document_Template__c
			WHERE Id = :objDocumentTemplate.Id
			LIMIT 1
		];

		System.assertEquals('DT-14-000002', objDocumentTemplateUpdated.ExternalID__c, 'The External ID on Document Template was not set correctly.');
	}
}
