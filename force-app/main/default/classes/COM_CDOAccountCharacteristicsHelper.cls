public with sharing class COM_CDOAccountCharacteristicsHelper {
	public static AccountSiteTenantData getAccountServiceCharacteristicValues(List<csord__Service__c> services) {
		Set<Id> siteFromService = new Set<Id>();

		for (csord__Service__c service : services) {
			if (service.Site__c != null) {
				siteFromService.add(service.Site__c);
			}
		}
		AccountSiteTenantData data = new AccountSiteTenantData();
		if (siteFromService.size() != 1) {
			throw new DeliveryException('Defect in Delivery components - components related to multiple sites within one delivery order');
		} else {
			Site__c site = [
				SELECT Site_Account__r.CDO_Tenant_Id__c, Site_House_Number__c, Site_House_Number_Suffix__c, Site_Postal_Code__c
				FROM Site__c
				WHERE Id IN :siteFromService
			];
			data.postalCode = site.Site_Postal_Code__c;
			data.houseNumber = site.Site_House_Number__c != null ? String.valueOf(site.Site_House_Number__c) : '';
			data.houseNumberExt = site.Site_House_Number_Suffix__c;
			data.tenantId = site.Site_Account__r.CDO_Tenant_Id__c;
		}

		return data;
	}
	public static String getAccountSiteDataInfo(AccountSiteTenantData accountData, String mapping) {
		String result = '';

		switch on mapping {
			when 'tenantId' {
				result = accountData.tenantId;
			}
			when 'postalCode' {
				result = accountData.postalCode;
			}
			when 'houseNumber' {
				result = accountData.houseNumber;
			}
			when 'houseNumberExt' {
				result = accountData.houseNumberExt;
			}
		}
		return result;
	}
	public class AccountSiteTenantData {
		private String postalCode;
		private String tenantId;
		private String houseNumber;
		private String houseNumberExt;
	}

	public class AccountTenantData {
		private String tenantName;
		private String tenantContactPhone;
		private String tenantContactEmail;
	}

	private class DeliveryException extends Exception {
	}
}
