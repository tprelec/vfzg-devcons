global with sharing class CustomButtonOrderDecompositionSimulation extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		PageReference editPage = new PageReference('/apex/csordtelcoa__OrderDecompositionSimulation?id=' + basketId);
		System.debug(Logginglevel.DEBUG, 'Custom button URL: ' + editPage.getUrl());
		return '{"status":"ok","redirectURL":"' + editPage.getUrl() + '"}';
	}
}
