/**
 * @description:    AccountTeamMember accessor class
 * @testClass:      TestAccountTeamMemberAccessor
 **/
public inherited sharing class AccountTeamMemberAccessor {
	private static Map<Id, Map<String, AccountTeamMember>> accountTeamMemberPerTeamMemberRoleAndAccountId;

	static {
		initAccountTeamMemberMapping(getAccountTeamMembers());
	}

	/**
	 * @description:    Retrieves account team member for the given account and role
	 * @param           accountId - account Id
	 * @param           teamMemberRole - team member role
	 * @return          AccountTeamMember - account team member for the given account and role
	 */
	public static AccountTeamMember getAccountTeamMemberForAccountIdAndTeamMemberRole(Id accountId, String teamMemberRole) {
		return accountTeamMemberPerTeamMemberRoleAndAccountId?.get(accountId)?.get(teamMemberRole);
	}

	/**
	 * @description:    Initializes the account team member mapping
	 * @param           accountTeamMembers - list of account team members
	 */
	@TestVisible
	private static void initAccountTeamMemberMapping(List<AccountTeamMember> accountTeamMembers) {
		accountTeamMemberPerTeamMemberRoleAndAccountId = new Map<Id, Map<String, AccountTeamMember>>();

		for (AccountTeamMember accountTeamMember : accountTeamMembers) {
			if (!accountTeamMemberPerTeamMemberRoleAndAccountId.containsKey(accountTeamMember.AccountId)) {
				accountTeamMemberPerTeamMemberRoleAndAccountId.put(accountTeamMember.AccountId, new Map<String, AccountTeamMember>());
			}

			accountTeamMemberPerTeamMemberRoleAndAccountId.get(accountTeamMember.AccountId).put(accountTeamMember.TeamMemberRole, accountTeamMember);
		}
	}

	/**
	 * @description:    Retrieves all AccountTeamMember records
	 * @return          List<AccountTeamMember> - list of AccountTeamMember
	 */
	private static List<AccountTeamMember> getAccountTeamMembers() {
		return [SELECT UserId, TeamMemberRole, Id, AccountId FROM AccountTeamMember];
	}
}
