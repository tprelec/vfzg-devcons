public class EMP_UpdateProductMultipleSettings {
    public class Request{
        public String assigned_product_id; //comes from VF Asset
        public String action; //hardoced as 'SET'
        public List<Component_detail> component_details;
        public Boolean simulate_only; //hardcoded as false
        public Boolean return_eligible_settings; //hardcoded as false
        public Request(String assigned_product_id, List<Component_detail> component_details) {
            this.assigned_Product_Id = assigned_product_id;
            this.action = 'SET';
            this.component_details = component_details;
            this.simulate_only = false;
            this.return_eligible_settings = false;
        }
    }

    public class Component_detail{
        public String component_code; //Mandatory
        public List<ComponentSetting> component_settings;
        public Component_detail(String component_code, List<ComponentSetting> component_settings) {
            this.component_code = component_code;
            this.component_settings = component_settings;
        }
    }

    public class ComponentSetting{
        public String settingcode;
        public String value;
        public ComponentSetting(String tempSettingcode, String tempValue) {
            this.settingcode = tempValue!=null ? tempSettingcode:null;
            this.value = tempValue;
        }
    }

    public class Response{
        public Integer status;
        public List<Data> data;
        public List<EMP_ResponseError.Error> error;
    }

    public class Data {
        public String orderID;
        public Quotation_information quotation_information;
        public Data(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        switch on text {
                            when 'orderID' {
                                orderID = parser.getText();
                            }
                            when 'quotation_information' {
                                quotation_information = (Quotation_information)parser.readValueAs(Quotation_information.class);
                            }
                            when else {
                                System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                            }
                        }
                    }
                }
            }
        }
    }

	public class Quotation_information {
		public Double totalOC;
		public Double totalRC;
		public Double previousTotalRC;
		public Double originalTotalOC;
		public Double discountTotalOC;
		public Double overrideTotalOC;
		public Double taxTotalOC;
		public Double originalTotalRC;
		public Double discountTotalRC;
        public Double taxTotalRC;
        public Double proratedTotalRC;
        public Double proratedDiscountTotalRC;
        public Double proratedTaxTotalRC;
        public Double previousTaxTotalRC;
    }

    public class UpdateProductMultipleSettingsBulkRequestTop{
        public UpdateProductMultipleSettingsBulkRequest updateProductMultipleSettingsBulkRequest;
        public UpdateProductMultipleSettingsBulkRequestTop(UpdateProductMultipleSettingsBulkRequest updateProductMultipleSettingsBulkRequest) {
            this.updateProductMultipleSettingsBulkRequest = updateProductMultipleSettingsBulkRequest;
        }
    }
    
    public class UpdateProductMultipleSettingsBulkRequest{
        public List<SharedBslHttpHeader> sharedBslHttpHeaders;
        public List<UpdateProductMultipleSettingsRequest> updateProductMultipleSettingsRequests;
        public UpdateProductMultipleSettingsBulkRequest(List<SharedBslHttpHeader> sharedBslHttpHeaders, List<UpdateProductMultipleSettingsRequest> updateProductMultipleSettingsRequests) {
            this.sharedBslHttpHeaders = sharedBslHttpHeaders;
            this.updateProductMultipleSettingsRequests = updateProductMultipleSettingsRequests;
        }
    }
    
    public class UpdateProductMultipleSettingsRequest{
        public List<BslHttpHeader> bslHttpHeaders;
        public String correlationID; //SF Id
        public Integer priority;
        public Request updateProductMultipleSettingsRequest;
        public String assigned_product_id;
        public UpdateProductMultipleSettingsRequest(List<BslHttpHeader> requestHeaders, String correlationID, Integer priority, Request updateProductMultipleSettingsRequest, String assigned_product_id) {
            this.bslHttpHeaders = requestHeaders;
            this.correlationID = correlationID;
            this.priority = priority;
            this.updateProductMultipleSettingsRequest = updateProductMultipleSettingsRequest;
            this.assigned_product_id = assigned_product_id;
        }
    }
    
    public class SharedBslHttpHeader{
        public String name;
        public String value;
        public SharedBslHttpHeader(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }
    
    public class BslHttpHeader{
        public String name;
        public String value;
        public BslHttpHeader(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }
}