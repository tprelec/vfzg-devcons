@IsTest
private class TestAccountService {
	@TestSetup
	static void makeData() {
		TestUtils.createAccount(new User(Id = UserInfo.getUserId()));
		insert new Framework__c(Framework_Sequence_Number__c = 99997, VodafoneZiggo_Framework_Sequence_Number__c = 99998);
	}

	@IsTest
	static void generateFrameworkAgreementIdAndInvokeFrameworkAgreementCreation() {
		Account acc = getAccount();
		acc.Frame_Work_Agreement__c = null;
		update acc;

		Test.startTest();

		AccountService.getInstance().generateFrameworkAgreementIdAndInvokeFrameworkAgreementCreation(new Set<Id>{ acc.Id }, new Set<Id>{ acc.Id });

		Test.stopTest();

		acc = getAccount();
		System.assert(acc.Frame_Work_Agreement__c != null, 'Framework Agreement should be created');
		System.assertEquals(true, acc.Create_Framework_Agreement__c, 'Framework Agreement should be created');
		System.assert(acc.VZ_Framework_Agreement__c != null, 'Framework Agreement should be created');
		System.assertEquals(true, acc.Create_VZ_Framework_Agreement__c, 'Framework Agreement should be created');
	}

	@IsTest
	static void generateVodafoneFrameworkId() {
		Decimal currentFrameworkSequenceNumber = Framework__c.getOrgDefaults().Framework_Sequence_Number__c;
		Decimal nextFrameworkSequenceNumber = currentFrameworkSequenceNumber + 1;

		String newFrameworkId = AccountService.getInstance().generateFrameworkId(AccountService.FrameworkAgreementType.VODAFONE);

		System.assertEquals(
			nextFrameworkSequenceNumber,
			Framework__c.getOrgDefaults().Framework_Sequence_Number__c,
			'Framework Sequence should be increased.'
		);
		System.assertEquals(
			true,
			newFrameworkId.contains(String.valueOf(nextFrameworkSequenceNumber)),
			'New Framework Number should be increased by one.'
		);
	}

	@IsTest
	static void generateVodafoneZiggoFrameworkId() {
		Decimal currentFrameworkSequenceNumber = Framework__c.getOrgDefaults().VodafoneZiggo_Framework_Sequence_Number__c;
		Decimal nextFrameworkSequenceNumber = currentFrameworkSequenceNumber + 1;

		String newFrameworkId = AccountService.getInstance().generateFrameworkId(AccountService.FrameworkAgreementType.VODAFONE_ZIGGO);

		System.assertEquals(
			nextFrameworkSequenceNumber,
			Framework__c.getOrgDefaults().VodafoneZiggo_Framework_Sequence_Number__c,
			'Framework Sequence should be increased.'
		);
		System.assertEquals(
			true,
			newFrameworkId.contains(String.valueOf(nextFrameworkSequenceNumber)),
			'New Framework Number should be increased by one.'
		);
	}

	@IsTest
	static void updateFrameworkAgreementDateAndVersionForNewContract() {
		Account a = getAccount();
		a.Create_Framework_Agreement__c = true;
		a.Create_VZ_Framework_Agreement__c = true;
		update a;

		Date someDate = Date.newInstance(2000, 10, 27);

		AccountService.getInstance().updateFrameworkAgreementDateAndVersionForNewContract(new Map<Id, Date>{ a.Id => someDate });

		Account updatedAccount = getAccount();
		System.assertEquals(someDate, updatedAccount.Framework_agreement_date__c, 'Framework agreement date should be updated.');
		System.assertEquals(someDate, updatedAccount.VZ_Framework_Agreement_Date__c, 'Framework agreement date should be updated.');
		System.assertEquals(1, updatedAccount.Version_FWA__c, 'Framework Version should be updated.');
		System.assertEquals(1, updatedAccount.VZ_Framework_Agreement_Version__c, 'Framework Version should be updated.');
	}

	@IsTest
	private static void setExpectedDeliveryDateForVoiceAndVoiceDataAssets() {
		Account acc = getAccount();

		List<Date> endOfContractDates = new List<Date>{
			Date.newInstance(2001, 10, 18),
			Date.newInstance(2003, 10, 18),
			Date.newInstance(2004, 10, 18),
			Date.newInstance(2005, 10, 18),
			Date.newInstance(2030, 10, 18),
			Date.newInstance(2053, 10, 18)
		};
		List<VF_Asset__c> assets = new List<VF_Asset__c>();
		for (Integer i = 0; i < endOfContractDates.size(); i++) {
			assets.add(
				new VF_Asset__c(
					Account__c = acc.Id,
					CTN_Status__c = 'Active',
					Priceplan_Class__c = 'Mobile Voice',
					Contract_End_Date__c = endOfContractDates[i]
				)
			);
		}
		assets[endOfContractDates.size() - 1].Priceplan_Class__c = 'Data Only';
		insert assets;

		Test.startTest();

		AccountService.getInstance().setExpectedDeliveryDateForVoiceAndVoiceDataAssets(new Set<Id>{ acc.Id });

		Test.stopTest();

		Account accAfterUpdate = getAccount();
		// Date should match 2005-10-18
		System.assertEquals(
			endOfContractDates[4],
			accAfterUpdate.Expected_Delivery_Date_Voice_Data__c,
			'Incorrect expected delivery date for voice and data assets'
		);
	}

	@IsTest
	static void testUpdateAccountsToCustomer() {
		Account acc = getAccount();

		Test.startTest();

		TestUtils.createBan(acc);

		Test.stopTest();

		acc = getAccount();

		System.assertEquals('Customer', acc.Type, 'Account Type should be Customer');
	}

	private static Account getAccount() {
		return [
			SELECT
				Id,
				Type,
				IsPartner,
				Frame_Work_Agreement__c,
				Create_Framework_Agreement__c,
				VZ_Framework_Agreement__c,
				Create_VZ_Framework_Agreement__c,
				Framework_agreement_date__c,
				VZ_Framework_Agreement_Date__c,
				Version_FWA__c,
				VZ_Framework_Agreement_Version__c,
				Expected_Delivery_Date_Voice_Data__c,
				Expected_Delivery_Date_Voice__c
			FROM Account
			LIMIT 1
		];
	}
}
