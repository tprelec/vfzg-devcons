global with sharing class CustomButtonContractRelatedQuestions extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		List<cscfga__Product_Basket__c> basket = [
			SELECT
				Id,
				Total_Number_of_Expected_Locations__c,
				DirectIndirect__c,
				Approved_Date__c,
				Clauses_in_promotion__c,
				Basket_Approval_Status__c,
				cscfga__Opportunity__r.Deal_Type__c,
				cscfga__Opportunity__r.Credit_Check_Status_Number__c
			FROM cscfga__Product_Basket__c
			WHERE Id = :basketId
		];

		String enabledContract = 'false';
		if (basket.size() > 0) {
			if (basket[0].Total_Number_of_Expected_Locations__c == null) {
				enabledContract = 'true';
			}
		}

		//SFO-1118
		String message = checkPrerequesitesForContractGeneration(basket[0]);
		if (message != 'OK') {
			return '{"status":"error","text":"' + message + '"}';
		}

		PageReference editPage = new PageReference('/apex/ContractRelatedQuestions?basketId=' + basketId + '&enabledContract=' + enabledContract);

		if (basket != null && basket[0] != null)
			CS_ContractsTableHelper.setPromotionClauses(basket[0]);

		Id profileId = UserInfo.getProfileId();
		String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;

		String redirectUrl = '/' + basketId;
		if (profileName == 'VF Partner Portal User') {
			editPage = new PageReference(
				'/partnerportal/apex/ContractRelatedQuestions?basketId=' +
				basketId +
				'&enabledContract=' +
				enabledContract +
				'&showheader=false&sidebar=false'
			);
		}

		editPage = new PageReference(checkBasketComplexity(basketId, editPage.getUrl(), redirectUrl));

		return '{"status":"ok","redirectURL":"' + editPage.getUrl() + '"}';
	}

	public String checkBasketComplexity(String basketId, String successUrl, String failureUrl) {
		CS_Future_Controller futureController;
		Map<String, Future_Optimisation__c> optimisationSettingsMap = Future_Optimisation__c.getAll();
		Future_Optimisation__c optimisationSettings = optimisationSettingsMap.get('Standard Optimisation');

		List<AggregateResult> countProductConfigurations = [
			SELECT count(id) total
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c = :basketId
		];
		Integer countProductConfigurationsInteger = Integer.valueOf(countProductConfigurations[0].get('total'));

		if (
			optimisationSettings != null &&
			optimisationSettings.Optimisation_Enabled__c == true &&
			optimisationSettings.Configuration_number_limit__c < countProductConfigurationsInteger
		) {
			//String successUrl = '/apex/appro__VFSubmitPreview?id=' + basketId;
			//String failureUrl = '/' + basketId;
			futureController = new CS_Future_Controller(basketId, 'Parent process for preparing Contract Data.', successUrl, failureUrl);
			String newUrl = '/apex/c__CS_Future_Waiter_Page?basketId=' + basketId + '&parentProcessId=' + futureController.parentId;

			Id newFutureId = futureController.defineNewFutureJob('Collect contract data');
			CS_ContractPDFController.populateDataAsync(basketId, newFutureId);

			return newUrl;
		}

		return successUrl;
	}

	//SFO-1118
	public String checkPrerequesitesForContractGeneration(cscfga__Product_Basket__c basket) {
		if (
			basket.cscfga__Opportunity__c != null &&
			basket.cscfga__Opportunity__r.Deal_Type__c != null &&
			basket.cscfga__Opportunity__r.Deal_Type__c != ''
		) {
			if (basket.cscfga__Opportunity__r.Deal_Type__c.toLowercase().Contains('acquisition')) {
				//Credit check validaion is done only baskets containing "aquisition" word
				if (
					basket.cscfga__Opportunity__r.Credit_Check_Status_Number__c != 2 &&
					basket.cscfga__Opportunity__r.Credit_Check_Status_Number__c != 0
				) {
					//Credit_Check_Status_Number__c = 2 means it is approved
					if (basket.Basket_Approval_Status__c != 'Approved') {
						return 'Basket must be Approved before generating the contract';
					}
					if (basket.cscfga__Opportunity__r.Credit_Check_Status_Number__c == 1) {
						return 'Credit Check is pending approval. Contract can be generated after the Credit Check is approved';
					}
					return 'Credit Check must be approved before generating contracts';
				}
			}
		}
		return 'OK';
	}
}
