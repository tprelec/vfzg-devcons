global with sharing class COM_OrderFinishObserver implements csordcb.ObserverApi.IObserver {
    global void execute(csordcb.ObserverApi.Observable observableInst, Object arg) {
        csordtelcoa.OrderGenerationObservable observable = (csordtelcoa.OrderGenerationObservable) observableInst;
        generateServiceSpecifications(observable.getSubscriptionIds());
    }

    @future(callout=true)
    public static void generateServiceSpecifications(List<Id> subIds) {
        if(validateSpecificationList(subIds)) {
            if(Test.isRunningTest()) {
                Attachment att = new Attachment();
                att.ParentId = subIds[0];
                att.Name = 'ServiceSpecification.json';
                att.Body = Blob.valueOf('test');
                insert att;
            }
            else {
                csedm.API_1.generateSpecifications(subIds, new List<Id>(), true);
            }
        }
    }
    
    private static Boolean validateSpecificationList(Object obj) {
        Boolean result = false;
        if (obj != null) {
            List<Id> listOfObj = (List<Id>)obj;
            if(listOfObj.size() > 0) {
                result = true;
            }
        }
        return result;
    }
}