@isTest
public with sharing class TestPriceItemAddOnPriceItemAssHan {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'PI Add On Price Item Association';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'AOPA-46-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		cspmb__Price_Item_Add_On_Price_Item_Association__c testItem = new cspmb__Price_Item_Add_On_Price_Item_Association__c();
		insert testItem;

		cspmb__Price_Item_Add_On_Price_Item_Association__c queryRecord = [
			SELECT Id, External_ID__c
			FROM cspmb__Price_Item_Add_On_Price_Item_Association__c
			LIMIT 1
		];
		System.assert(queryRecord.External_ID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
