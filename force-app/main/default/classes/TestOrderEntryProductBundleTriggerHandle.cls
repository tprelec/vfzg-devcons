@isTest
public with sharing class TestOrderEntryProductBundleTriggerHandle {
	@isTest
	public static void testAutoNumbering() {
		ExternalIDNumber__c custSetting = new ExternalIDNumber__c(
			Name = 'OE_Product_Bundle',
			External_Number__c = 1,
			Object_Prefix__c = 'OEPB-53-',
			blocked__c = false,
			runningOrg__c = '00D1l0000008i1vEAA'
		);
		insert custSetting;

		String orgId = UserInfo.getOrganizationId();
		OE_Product__c main = OrderEntryTestFactory.createOEProduct('Main', true);
		OE_Product__c internet = OrderEntryTestFactory.createOEProduct('Internet', true);

		Test.startTest();
		OrderEntryTestFactory.createOEProductBundle(main, internet, '1111', true, true);
		Test.stopTest();

		OE_Product_Bundle__c result = [SELECT Id, External_Id__c FROM OE_Product_Bundle__c];
		if (custSetting.runningOrg__c == orgId) {
			System.assertEquals(
				'OEPB-53-000002',
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		} else {
			System.assertEquals(
				null,
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		}
	}
}