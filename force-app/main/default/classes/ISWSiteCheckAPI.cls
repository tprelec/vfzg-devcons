/**
 * @author: Rahul Sharma (07-09-2021)
 * @description: ISW Site Check outbound Rest service
 *      TestSitePostalCheckEventTriggerHandler covers unit test of this class
 *      Jira ticket reference: COM-1960
 * Note:
 *      WSDLtoApex doesn't support SOAP version 1.1 which was provided by ISW interface team, due to which a REST call is being made.
 */

public with sharing class ISWSiteCheckAPI {
    @TestVisible
    private static final String ENDPOINT_URL = 'callout:Unify_Layer7_NL_Credentials/v1/ISWInventory/soap12';

    @TestVisible
    private static final String SERVICE_TYPE_GIGA = 'GIGA';
    @TestVisible
    private static final String SERVICE_TYPE_INTERNET = 'INTERNET';
    @TestVisible
    private static final String ERROR_CODE_0 = '0';
    @TestVisible
    private static final Integer HTTP_STATUS_CODE_200 = 200;

    private static final String SOAP_RESPONSE_NAMESPACE = 'http://xy.spatial-eye.com/soap';

    public ISWSiteResponse getServiceAvailability(ISWSiteRequest request) {
        Id siteId = request.siteId;
        String requestBody = getRequestBody(siteId, request);
        System.debug('@@@@requestBody: ' + requestBody);

        HttpRequest req = new HttpRequest();
        // no username and password is needed/provided by ISW team
        // authentication happens with certificate shared between Salesforce and ISW, its attached to named credentials with which ISW authorizes the API call
        req.setEndpoint(ENDPOINT_URL);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/soap+xml');
        req.setHeader('SOAPAction', 'GetServiceAvailabilityRequest_2');
        req.setBody(requestBody);
        req.setTimeout(60000);

        HTTPResponse response;

        try {
            response = new Http().send(req);
        } catch (Exception objEx) {
            throw objEx;
        }

        System.debug('@@@@response.getBody(): ' + response.getBody());

        if (response.getStatusCode() == HTTP_STATUS_CODE_200) {
            try {
                return getParsedResponse(response.getBody());
            } catch (ISWSiteCheckException responseException) {
                throw responseException;
            } catch (Exception objEx) {
                throw new ISWSiteCheckException(
                    System.Label.ISWSiteCheck_ResponseFormatNotValid,
                    objEx
                );
            }
        } else {
            throw new ISWSiteCheckException(
                getHttpFailureMessage(response.getStatusCode())
            );
        }
    }

    private String getRequestBody(Id siteId, ISWSiteRequest request) {
        return '<?xml version="1.0" encoding="UTF-8"?>' +
            '<soapenv:Envelope xmlns:soapenv="http://www.w3.org/2003/05/soap-envelope" xmlns:soap="http://xy.spatial-eye.com/soap">' +
            '<soapenv:Body>' +
            '<soap:GetServiceAvailabilityRequest_2>' +
            '<soap:Header>' +
            '<soap:BusinessTransactionID>' +
            siteId +
            '</soap:BusinessTransactionID>' +
            '<soap:SentTimestamp>' +
            DateTime.now().format('yyyy-MM-dd\'T\'HH:mm:ss.SSSXXX') +
            '</soap:SentTimestamp>' +
            '<soap:SourceContext>' +
            '<soap:Host>' +
            System.URL.getSalesforceBaseUrl().getHost() +
            '</soap:Host>' +
            '<soap:Application>SFDC</soap:Application>' +
            '</soap:SourceContext>' +
            '</soap:Header>' +
            '<soap:Body>' +
            '<soap:EXACTMATCHFLAG>Y</soap:EXACTMATCHFLAG>' +
            '<soap:HOUSENUMBER>' +
            request.houseNumber +
            '</soap:HOUSENUMBER>' +
            (String.isEmpty(request.houseNumberExtension)
                ? ''
                : '<soap:HOUSENUMBEREXTENSION>' +
                  request.houseNumberExtension +
                  '</soap:HOUSENUMBEREXTENSION>') +
            '<soap:POSTCODE>' +
            request.postCode +
            '</soap:POSTCODE>' +
            '</soap:Body>' +
            '</soap:GetServiceAvailabilityRequest_2>' +
            '</soapenv:Body>' +
            '</soapenv:Envelope>';
    }

    private ISWSiteResponse getParsedResponse(String responseBody) {
        return new ISWSiteResponse(responseBody);
    }

    @TestVisible
    private static String getResponseFailureMessage(String errorCode) {
        return String.format(
            System.Label.ISWSiteCheck_ResponseFailureMessage,
            new List<String>{ errorCode }
        );
    }

    @TestVisible
    private static String getHttpFailureMessage(Integer statusCode) {
        return String.format(
            System.Label.ISWSiteCheck_HttpFailureMessage,
            new List<String>{ '' + statusCode }
        );
    }

    public class ISWSiteCheckException extends Exception {
    }

    public class ISWSiteRequest {
        public Id siteId;
        public String postCode;
        public String houseNumber;
        public String houseNumberExtension;
        public ISWSiteRequest(
            Id siteId,
            String postCode,
            String houseNumber,
            String houseNumberExtension
        ) {
            this.siteId = siteId;
            this.postCode = postCode;
            this.houseNumber = houseNumber;
            this.houseNumberExtension = houseNumberExtension;
        }
    }

    public class ISWSiteResponse {
        public String tcpService;
        public String footprint;
        public Boolean areTcpLinesEmpty = false;
        public String serviceabilityRestrictions;
        public String lineStatus;
        public String functionalOrderingPossibleInternet;
        public String functionalOrderingPossibleGiga;

        public ISWSiteResponse(String responseBody) {
            Dom.Document doc = new Dom.Document();
            doc.load(responseBody);

            Dom.XmlNode parentNode = doc.getRootElement().getChildElements()[0]
                .getChildElements()[0];
            Dom.XmlNode bodyNode = parentNode.getChildElements()[1];

            Dom.XmlNode tcpLinesNode = getChildElement(bodyNode, 'TCPLINES');
            String errorCode = getChildElement(bodyNode, 'ErrorCode').getText();

            // System.debug('@@@@errorCode: ' + errorCode);

            if (
                tcpLinesNode != null &&
                !tcpLinesNode.getChildElements().isEmpty()
            ) {
                Dom.XmlNode tcpLineNode = tcpLinesNode.getChildElements()[0];
                this.tcpService = getChildElement(tcpLineNode, 'TCPSERVICE')
                    .getText();
                this.footprint = getChildElement(tcpLineNode, 'FOOTPRINT')
                    .getText();
                this.lineStatus = getChildElement(tcpLineNode, 'LINESTATUS')
                    .getText();
                this.serviceabilityRestrictions = getChildElement(
                        tcpLineNode,
                        'SERVICEABILITYRESTRICTIONS'
                    )
                    .getText();

                Dom.XmlNode tcpServiceLineNode = tcpLineNode.getChildElement(
                    'TCPSERVICELINES',
                    SOAP_RESPONSE_NAMESPACE
                );

                if (tcpServiceLineNode != null) {
                    setOrderingPossible(tcpServiceLineNode);
                }
            } else {
                this.areTcpLinesEmpty = true;
                if (errorCode != ERROR_CODE_0) {
                    String errorMessage = getResponseFailureMessage(errorCode);
                    throw new ISWSiteCheckException(errorMessage);
                }
            }
        }

        private void setOrderingPossible(Dom.XmlNode node) {
            String gigaForCoax = '';
            for (Dom.XmlNode childNode : node.getChildElements()) {
                String serviceType = childNode.getChildElement(
                        'TCPPOSSIBLESERVICETYPE',
                        SOAP_RESPONSE_NAMESPACE
                    )
                    .getText();

                if (serviceType == SERVICE_TYPE_GIGA) {
                    this.functionalOrderingPossibleGiga = childNode.getChildElement(
                            'FUNCTIONALORDERINGPOSSIBLE',
                            SOAP_RESPONSE_NAMESPACE
                        )
                        .getText();
                } else if (serviceType == SERVICE_TYPE_INTERNET) {
                    this.functionalOrderingPossibleInternet = childNode.getChildElement(
                            'FUNCTIONALORDERINGPOSSIBLE',
                            SOAP_RESPONSE_NAMESPACE
                        )
                        .getText();
                }
            }
        }

        private Dom.XmlNode getChildElement(
            Dom.XmlNode parentNode,
            String nodeName
        ) {
            return parentNode.getChildElement(
                nodeName,
                SOAP_RESPONSE_NAMESPACE
            );
        }
    }
}