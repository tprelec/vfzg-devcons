@RestResource(urlMapping='/createorder/*')
global with sharing class BdsRestCreateOrder {

    @HttpPost
    global static void doPost(String opportunityID) {

        RestResponse res = RestContext.response;
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
            res.addHeader('Content-Type', 'text/json');
            res.statusCode = 200;
        }

        /** Validate the incoming body values */
        BdsResponseClasses.OpportunityStatusClass validatedResultClass = BdsRestCreateOrder.validateValues(opportunityID);
        validatedResultClass.opportunityId = opportunityID;
        if (!validatedResultClass.isSuccess && !String.isEmpty(validatedResultClass.errorMessage) ) {
            res.statusCode = 400;
            res.responseBody = Blob.valueOf(JSON.serializePretty(validatedResultClass));
        } else {
            /** Proceed */
            try {
                String query = 'Select Id, Bespoke__c';
                // Get the fields from OrderValidationField__c
                for(OrderValidationField__c ovf : OrderValidationField__c.getAll().values()){
                    if (ovf.object__c=='Opportunity') {
                        if (ovf.Field__c != 'Bespoke__c') {
                            query += ',' + ovf.Field__c;
                        }
                    }
                }
                query+=' From Opportunity Where Id = :opportunityID';
                Opportunity opp = Database.query(query);
                 //= [Select Id,Main_Contact_Person__c,Solution_Sales__c,Main_and_or_Sub_domeins__c,Services__c,No_contract_required__c,Owner.UserRole.Name,Owner.Ziggo__c,Ziggo_Order_Reference__c,Ziggo_Only_Products__c,Opportunity_Standard_Resolution_time__c,Vodafone_Products__c,Count_GT_Fixed__c,Name,CloseDate,AccountId,IsClosed,Organic_Growth__c,StageName,Special_project_type__c,OwnerId,Owner.Manager.Manager.Signature_Document_Id__c,Owner.Manager.Signature_Document_Id__c,Ziggo_XSell_Products__c,Has_Ziggo_Owner__c,Primary_Quote__c,Primary_Quote_Closed__c,Number_of_added_products_from_BM_Quote__c,Number_of_manually_added_products__c,BAN__r.Unify_Customer_Type__c,BAN__r.Unify_Customer_SubType__c,Account.Unify_Account_Type__c,Account.Unify_Account_SubType__c,Temp_OV_help_field__c,Temp_OV_help_field_2__c,RoleCreateOrChangeOwner__c,Department__c,RecordType.Name,Owner.UserType,Project_Comments__c,BigMachines__Line_Items__c,Hidden_Roll_up_Kvk_opp__c,Hidden_Fixed_Products__c,Partner_Account__c,Confirm_Mixed_Opportunity__c,RecordTypeId,Account.Authorized_to_sign_1st__c,Hidden_Roll_up_Contract_Opp__c,Account.Mobile_Dealer__r.Name,Account.GT_Fixed__c
                //From Opportunity Where Id =: opportunityID ];

                String errorMessage = OrderValidation.checkCompleteOpportunitiesExt(new List<Opportunity> {opp}, new Set<Id> {opportunityID}, false, null);
                validatedResultClass.errorMessage = errorMessage;

                if (opp != null && String.isEmpty(errorMessage)) {
                    opp.StageName = 'Closing By SAG';
                    update opp;
                    validatedResultClass.isSuccess = true;
                } else {
                    res.statusCode = 400;
                }
            } catch(Exception e) {
                res.statusCode = 400;
                validatedResultClass.errorMessage = e.getMessage();
            }
            res.responseBody = Blob.valueOf(JSON.serializePretty(validatedResultClass));
        }
    }

    /**
     * Validate the provided opportunity ID
     *
     * @param opportunityID
     *
     * @return
     */
    public static BdsResponseClasses.OpportunityStatusClass validateValues(String opportunityID) {
        Boolean errorFound = false;
        BdsResponseClasses.OpportunityStatusClass returnClass = new BdsResponseClasses.OpportunityStatusClass();
        /** Check for empty value */
        if (String.isEmpty(opportunityID)) {
            returnClass.errorMessage = 'No Opportunity ID provided';
            errorFound = true;
        }
        /** Check if provided ID is actually a valid ID */
        if (!errorFound) {
            try {
                Id oppId = opportunityID;
            } catch(Exception e) {
                returnClass.errorMessage = 'No valid Opportunity ID provided';
                errorFound = true;
            }
        }
        /** Query to see if there actually is an Opportunity with this ID */
        if (!errorFound) {
            try {
                Opportunity opp = [Select Id From Opportunity Where Id =: opportunityID];
            } catch(Exception e) {
                returnClass.errorMessage = 'No opportunity found with this ID';
                errorFound = true;
            }
        }
        /** Check to see if the opportunity is already closed */
        if (!errorFound) {
            List<Opportunity> oppList = [Select Id, StageName From Opportunity Where Id =: opportunityID AND StageName = 'Closing By SAG'];
            if (oppList.size() == 1) {
                returnClass.errorMessage = 'Opportunity already closed';
                errorFound = true;
            }
        }
        return returnClass;
    }
}