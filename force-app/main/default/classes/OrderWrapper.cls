/**
 * @description			This is a wrapper class for the order. It is used for both displaying the order details on the Order Form and collecting the  
 *						data before exporting the Order
 * @author				Guy Clairbois
 */
public class OrderWrapper {
	
	public OrderWrapper(){
		this.locations = new List<DeliveryWrapper>();
		this.portingRowWrappers = new List<NumberportingRowWrapper>();
		this.phonebookRegistrationWrappers = new List<PhonebookRegistrationWrapper>();
		this.additionalArticles = new List<Contracted_Products__c>(); 
	}
	
	public Order__c theOrder {get;set;}
	
    public Map<Id,DeliveryWrapper> SiteMap {get;set;}
	
	public List<NumberportingRowWrapper> portingRowWrappers {get;set;}
	public List<PhonebookRegistrationWrapper> phonebookRegistrationWrappers {get;set;}	
	public List<Contracted_Products__c> additionalArticles {get;set;}
	public Contact_Role__c customerMainContact {get;set;}
	public Contact_Role__c customerMaintenanceContact {get;set;}
	public Contact_Role__c customerIncidentContact {get;set;}
	public Contact_Role__c customerChooserContact {get;set;}
	

	public List<DeliveryWrapper> locations {get;set;}
	
	public Boolean getOrderHasOneNet(){
		for(DeliveryWrapper dw : locations){
			if(dw.hasOneNet){
				return true;
			}
		}
		return false;
	}

	public Boolean getOrderHasOneFixed(){
		for(DeliveryWrapper dw : locations){
			if(dw.getHasOneFixed()){
				return true;
			}
		}
		return false;
	}

	public String getLegacyOrderType(){
		for(DeliveryWrapper dw : locations){
			for(Contracted_Products__c cp : dw.legacyArticles){
				return cp.Deal_Type__c;
			}
		}
		return null;
	}

	public static Boolean getHasOneNetFromPropositions(String orderPropositions){
		if(orderPropositions != null && orderPropositions.contains('One Net'))
			return true;
		else
			return false;
	}

	public static Boolean getHasFixedVoiceFromPropositions(String orderPropositions){
		if(orderPropositions != null && (orderPropositions.contains('One Fixed') || orderPropositions.contains('One Net')|| orderPropositions.contains('Numberporting')))
			return true;
		else
			return false;
	}

    public Boolean getOrderIsEMPautomatic(){
        return theOrder.EMP_Automated_Mobile_order__c;
	}
}