public with sharing class AddSiteController {
	/**
	 * @description Searches sites via Olbico
	 * @param  zip Postcode
	 * @return     Search results
	 */
	@AuraEnabled
	public static Map<String, String> oblicoSearch(String zip) {
		Map<String, String> addressInformation = new Map<String, String>();
		if (String.isNotBlank(zip) && Pattern.matches('^[0-9]{4}[a-z,A-Z]{2} ?[0-9,a-z,A-Z]*$', zip)) {
			OlbicoServiceJSON jService = new OlbicoServiceJSON();
			jService.setRequestRestJsonStreet(zip);
			jService.makeRequestRestJsonStreet(zip);

			for (SelectOption so : jService.AdresInfo()) {
				addressInformation.put(so.getValue(), so.getLabel());
			}
		}
		return addressInformation;
	}

	/**
	 * @description Saves new Site
	 * @param  selectedAddress Address (as returned by Olbico)
	 * @param  accId           Account ID.
	 * @return                 Created Site.
	 */
	@AuraEnabled
	public static Site__c saveSite(String selectedAddress, Id accId) {
		List<Site__c> sites = new List<Site__c>();
		List<String> errorMessages = new List<String>();
		String errorMsg = '';

		Account acc = [SELECT Id, KVK_number__c FROM Account WHERE Id = :accId];

		List<String> address = selectedAddress.split('\\|');

		String street = address[0];
		Integer houseNr = Integer.valueof(address[1].Trim());
		String houseNrLetter = address[2];
		String houseNrLetterAddition = address[3];
		String postcode = address[4];
		String cityAddition = address.size() > 6 ? ' - ' + address[6] : '';
		String city = address[5] + cityAddition;

		Site__c newSite = new Site__c();
		newSite.Site_Account__c = acc.Id;
		newSite.KVK_Number__c = acc.KVK_number__c;
		newSite.Site_Street__c = street;
		newSite.Site_House_Number__c = houseNr;
		newSite.Site_House_Number_Suffix__c = houseNrLetter + ' ' + houseNrLetterAddition;
		newSite.Site_Postal_Code__c = postcode;
		newSite.Site_City__c = city;
		newSite.Olbico__c = true;
		newSite.Name = street;

		sites.add(newSite);

		List<Database.upsertResult> uResults = Database.upsert(sites, false);
		List<Database.Error> errors = new List<Database.Error>();
		for (Database.upsertResult result : uResults) {
			if (!result.isSuccess()) {
				errors = result.getErrors();
				for (Database.Error error : errors) {
					errorMessages.add(error.getMessage());
				}
			}
		}

		if (errorMessages.size() == 0) {
			return newSite;
		}

		errorMsg = String.join(errorMessages, '.\n');

		throw new AuraHandledException(errorMsg);
	}
}
