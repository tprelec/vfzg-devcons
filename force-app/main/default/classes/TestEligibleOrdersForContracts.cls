@IsTest
private class TestEligibleOrdersForContracts {
	enum Section {
		DIRECT,
		MMS0,
		MMS1
	}

	@TestSetup
	static void prepareData() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;
	}

	@IsTest
	static void testPreparedData() {
		Test.startTest();
		List<VF_Contract__c> vfContracts = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c];
		List<Opportunity> opportunities = [SELECT Id FROM Opportunity];
		List<Order__c> orders = [SELECT Id FROM Order__c];
		Test.stopTest();

		System.assertEquals(1, vfContracts.size(), 'There should be 1 VF Contract');
		System.assertEquals(1, opportunities.size(), 'There should be 1 Opportunity');
		System.assertEquals(0, orders.size(), 'There should not be an Order');
		System.assertEquals(
			false,
			vfContracts[0].Eligible_for_CS_Mobile_Flow__c,
			'The default VF contract created should not be eligible for mobile flow'
		);
	}

	@IsTest
	static void testDirectCorporateCommercial() {
		Test.startTest();
		updateData('Corporate Commercial Suffix', Section.DIRECT);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testDirectCorporateHealthcare() {
		Test.startTest();
		updateData('Corporate Healthcare Suffix', Section.DIRECT);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testDirectPublicOOH() {
		Test.startTest();
		updateData('Public OOH Suffix', Section.DIRECT);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testDirectEnterpriseServicesSolutionSales() {
		Test.startTest();
		updateData('Enterprise Services Solution Sales', Section.DIRECT);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testDirectEnterpriseServicesSolutionSalesSuffix() {
		Test.startTest();
		updateData('Enterprise Services Solution Sales Suffix', Section.DIRECT);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testDirectVGE() {
		Test.startTest();
		updateData('VGE', Section.DIRECT);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testDirectVGESuffix() {
		Test.startTest();
		updateData('VGE Suffix', Section.DIRECT);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testDirectM2M() {
		Test.startTest();
		updateData('M2M', Section.DIRECT);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testDirectM2MSuffix() {
		Test.startTest();
		updateData('M2M Suffix', Section.DIRECT);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsCorporateCommercial() {
		Test.startTest();
		updateData('Corporate Commercial Suffix', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsCorporateHealthcare() {
		Test.startTest();
		updateData('Corporate Healthcare Suffix', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsPublicOOH() {
		Test.startTest();
		updateData('Public OOH Suffix', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsEnterpriseServicesSolutionSales() {
		Test.startTest();
		updateData('Enterprise Services Solution Sales', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsEnterpriseServicesSolutionSalesSuffix() {
		Test.startTest();
		updateData('Enterprise Services Solution Sales Suffix', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsOther() {
		Test.startTest();
		updateData('Other', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsOtherSuffix() {
		Test.startTest();
		updateData('Other Suffix', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsVGE() {
		Test.startTest();
		updateData('VGE', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsVGESuffix() {
		Test.startTest();
		updateData('VGE Suffix', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsM2M() {
		Test.startTest();
		updateData('M2M', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedDoesNotExistsM2MSuffix() {
		Test.startTest();
		updateData('M2M Suffix', Section.MMS0);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsCorporateCommercial() {
		Test.startTest();
		updateData('Corporate Commercial Suffix', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsCorporateHealthcare() {
		Test.startTest();
		updateData('Corporate Healthcare Suffix', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsPublicOOH() {
		Test.startTest();
		updateData('Public OOH Suffix', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsEnterpriseServicesSolutionSales() {
		Test.startTest();
		updateData('Enterprise Services Solution Sales', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsEnterpriseServicesSolutionSalesSuffix() {
		Test.startTest();
		updateData('Enterprise Services Solution Sales Suffix', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsOther() {
		Test.startTest();
		updateData('Other', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsOtherSuffix() {
		Test.startTest();
		updateData('Other Suffix', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsVGE() {
		Test.startTest();
		updateData('VGE', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsVGESuffix() {
		Test.startTest();
		updateData('VGE Suffix', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsM2M() {
		Test.startTest();
		updateData('M2M', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assert(vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should be eligible for mobile flow');
	}

	@IsTest
	static void testMmsUncheckedExistsM2MSuffix() {
		Test.startTest();
		updateData('M2M Suffix', Section.MMS1);
		Test.stopTest();

		VF_Contract__c vfContract = [SELECT Eligible_for_CS_Mobile_Flow__c FROM VF_Contract__c LIMIT 1];
		System.assertEquals(false, vfContract.Eligible_for_CS_Mobile_Flow__c, 'Should not be eligible for mobile flow');
	}

	static void updateData(String opportunitySegment, Section sectionForDataUpdate) {
		VF_Contract__c vfContract = [
			SELECT Id, OwnerId, Type_of_Service__c, Opportunity__r.Billing_Arrangement__c, Contract_Status__c
			FROM VF_Contract__c
			LIMIT 1
		];
		setOpportunitySegment(opportunitySegment);
		if (sectionForDataUpdate == Section.MMS0 || sectionForDataUpdate == Section.MMS1) {
			setVfContractOwnerAndServiceType(vfContract);
		}
		if (sectionForDataUpdate == Section.MMS1) {
			setProductsToEnterpriseServices();
		}
		Order__c order = TestUtils.createOrder(vfContract);
		if (sectionForDataUpdate == Section.DIRECT) {
			setOrderToMobileManual(order);
		}
	}

	static void setOpportunitySegment(String segment) {
		Opportunity opportunity = [SELECT Segment__c FROM Opportunity LIMIT 1];
		opportunity.Segment__c = segment;
		update opportunity;
	}

	static void setOrderToMobileManual(Order__c order) {
		order.Status__c = 'Processed manually';
		order.Propositions__c = 'Mobile';
		update order;
	}

	static void setVfContractOwnerAndServiceType(VF_Contract__c vfContract) {
		Account partnerAcc = TestUtils.createPartnerAccount();
		User owner;
		System.runAs(GeneralUtils.currentUser) {
			owner = TestUtils.createPortalUser(partnerAcc);
		}
		GeneralUtils.userMap = null; // remove caching
		vfContract.OwnerId = owner.Id;
		vfContract.Type_of_Service__c = 'Mobile';
		vfContract.Contract_Status__c = 'Implementation';
		update vfContract;
	}

	static void setProductsToEnterpriseServices() {
		List<Contracted_Products__c> contractedProducts = [SELECT Product_Family__c FROM Contracted_Products__c];
		for (Contracted_Products__c contractedProduct : contractedProducts) {
			contractedProduct.Product_Family__c = 'Enterprise Services';
		}
		update contractedProducts;
	}
}
