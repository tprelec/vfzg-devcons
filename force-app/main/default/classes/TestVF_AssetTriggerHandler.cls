/**
 * @description		This is the test class for VF_AssetTriggerHandler class
 * @author        	Guy Clairbois
 */
@isTest
public with sharing class TestVF_AssetTriggerHandler {
	public static NetProfit_Information__c netProfitInformation;
	public static User owner;
	public static Order__c order;
	public static VF_Contract__c contract;
	public static testMethod void verifyCorrectVFAssetAndAccountUpdates() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		Test.startTest();
		User admin = TestUtils.createAdministrator();
		User dealer = TestUtils.createDealer();

		List<Contact> contactsList = new List<Contact>();
		List<VF_Asset__c> assetsList = new List<VF_Asset__c>();

		Account acc = new Account();
		acc.Name = 'test1';
		acc.OwnerId = dealer.Id;
		insert acc;

		contactsList.add(
			new Contact(AccountId = acc.Id, LastName = 'TestLastname', FirstName = 'TestFirstname', Userid__c = dealer.Id, isactive__c = true)
		);
		contactsList.add(
			new Contact(
				AccountId = acc.Id,
				LastName = 'parentTestLastname',
				FirstName = 'parentTestFirstname',
				Userid__c = admin.Id,
				isactive__c = true
			)
		);
		insert contactsList;

		String random4charNumericString = String.valueOf(Math.mod(Math.round(Math.random() * 1000), 1000) + 1000);
		String random4charNumericString2 = String.valueOf(Math.mod(Math.round(Math.random() * 1000), 1000) + 1000);
		Dealer_Information__c newDealerParent = TestUtils.createDealerInformation(contactsList[1].Id, '99' + random4charNumericString);
		Dealer_Information__c newDealer = TestUtils.createDealerInformation(
			contactsList[0].Id,
			'99' + random4charNumericString2,
			'99' + random4charNumericString
		);

		//to have a contract created
		VF_Asset__c initContract = createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', false);
		insert initContract;

		//found -> retention -> status (on Insert)
		//contract found/status Active/not retention
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', false));
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', false));
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', false));
		//contract not found/status Active/not retention
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '2', 'Active', '399999932', false));
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '2', 'Active', '399999932', false));
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '2', 'Active', '399999932', false));
		//contract found/status Active/ retention
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Active', '399999932', true));
		//contract not found/status Active/ retention
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '3', 'Active', '399999932', true));
		//contract found/status Suspended/not retention
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Suspended', '399999932', false));
		//contract not found/status Suspended/not retention
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '4', 'Suspended', '399999932', false));
		//contract found/status Suspended/ retention
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '1', 'Suspended', '399999932', true));
		//contract not found/status Suspended/ retention
		assetsList.add(createAsset(acc.Id, newDealer.Dealer_Code__c, '5', 'Suspended', '399999932', true));
		insert assetsList;

		// contracts 1, 2 and 3 created
		//found -> retention -> status (on Update)
		//contract found/status Active/not retention
		assetsList[0].Contract_Number__c = '2';
		//contract not found/status Active/not retention
		assetsList[1].Contract_Number__c = '10';
		//contract found/status Active/ retention
		assetsList[2].Contract_Number__c = '2';
		assetsList[2].Retention_Date__c = system.today().addDays(2);
		//contract not found/status Active/ retention
		assetsList[3].Contract_Number__c = '11';
		assetsList[3].Retention_Date__c = system.today().addDays(2);
		//contract found/status Suspended/not retention
		assetsList[4].Contract_Number__c = '2';
		//contract not found/status Suspended/not retention
		assetsList[5].Contract_Number__c = '12';
		//contract found/status Suspended/ retention
		assetsList[6].Contract_Number__c = '2';
		assetsList[6].Retention_Date__c = system.today().addDays(2);
		//contract not found/status Suspended/ retention
		assetsList[7].Contract_Number__c = '13';
		assetsList[7].Retention_Date__c = system.today().addDays(2);
		update assetsList;

		System.assertNotEquals(null, [SELECT Id, Error__c, Contract_VF__c FROM VF_Asset__c WHERE Id = :assetsList[5].Id].Contract_VF__c);

		delete initContract;
		Test.stopTest();
	}

	public static testMethod void verifyVFAssetChangeOwner() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		Test.startTest();
		User admin = TestUtils.createAdministrator();
		User dealer = TestUtils.createDealer();

		List<Contact> contactsList = new List<Contact>();
		List<VF_Asset__c> assetsList = new List<VF_Asset__c>();

		Account acc = new Account();
		acc.Name = 'test1';
		acc.OwnerId = dealer.Id;
		insert acc;

		contactsList.add(
			new Contact(AccountId = acc.Id, LastName = 'TestLastname', FirstName = 'TestFirstname', Userid__c = dealer.Id, isactive__c = true)
		);
		contactsList.add(
			new Contact(
				AccountId = acc.Id,
				LastName = 'parentTestLastname',
				FirstName = 'parentTestFirstname',
				Userid__c = admin.Id,
				isactive__c = true
			)
		);
		insert contactsList;

		String random4charNumericString = String.valueOf(Math.mod(Math.round(Math.random() * 1000), 1000) + 1000);
		String random4charNumericString2 = String.valueOf(Math.mod(Math.round(Math.random() * 1000), 1000) + 1000);
		Dealer_Information__c newDealer = TestUtils.createDealerInformation(
			contactsList[0].Id,
			'99' + random4charNumericString2,
			'99' + random4charNumericString
		);

		VF_Asset__c vfAsset = createAsset(acc.Id, newDealer.Dealer_Code__c, '', 'Active', '399999932', true);
		vfAsset.Dealer_Code__c = newDealer.Dealer_Code__c;
		vfAsset.Contract_VF__c = null;
		assetsList.add(vfAsset);
		insert assetsList;

		System.assertNotEquals(
			null,
			[SELECT Id, Error__c, Contract_VF__c, Dealer_Code__c FROM VF_Asset__c WHERE Id = :assetsList[0].Id]
			.Dealer_Code__c
		);

		Test.stopTest();
	}

	public static testMethod void verifyOrderSetOnVFAsset() {
		setupTestData();
		Test.startTest();
		User dealerUser = TestUtils.createDealer();

		List<Contact> contactsList = new List<Contact>();
		List<VF_Asset__c> assetsList = new List<VF_Asset__c>();

		Account acc = new Account();
		acc.Name = 'test1';
		acc.OwnerId = dealerUser.Id;
		insert acc;

		contactsList.add(
			new Contact(AccountId = acc.Id, LastName = 'TestLastname', FirstName = 'TestFirstname', Userid__c = dealerUser.Id, isactive__c = true)
		);
		contactsList.add(
			new Contact(
				AccountId = acc.Id,
				LastName = 'parentTestLastname',
				FirstName = 'parentTestFirstname',
				Userid__c = owner.Id,
				isactive__c = true
			)
		);
		insert contactsList;

		String random4charNumericString = String.valueOf(Math.mod(Math.round(Math.random() * 1000), 1000) + 1000);
		String random4charNumericString2 = String.valueOf(Math.mod(Math.round(Math.random() * 1000), 1000) + 1000);
		Dealer_Information__c newDealer = TestUtils.createDealerInformation(
			contactsList[0].Id,
			'99' + random4charNumericString2,
			'99' + random4charNumericString
		);

		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = 3,
			Duration__c = 1,
			CLC__c = 'Acq',
			VF_Contract__c = contract.Id,
			Order__c = order.Id,
			Framework_Id__c = '11254033501'
		);
		insert cp;

		NetProfit_CTN__c netProfitCTN = new NetProfit_CTN__c();
		netProfitCTN.CTN_Number__c = '31621505108';
		netProfitCTN.Framework_Agreement_Id__c = '11254033501';
		netProfitCTN.NetProfit_Information__c = netProfitInformation.Id;
		netProfitCTN.Order__c = order.Id;
		netProfitCTN.Contracted_Product__c = cp.Id;
		netProfitCTN.Action__c = Constants.NETPROFIT_CTN_ACTION_NEW;

		insert netProfitCTN;

		VF_Asset__c vfAsset = createAsset(acc.Id, newDealer.Dealer_Code__c, '5', 'Active', '399999932', true);
		vfAsset.Agreement_Number__c = '11254033501';
		vfAsset.CTN__c = '31621505108';
		assetsList.add(vfAsset);
		insert assetsList;

		VF_Asset__c vfAssetQuery = [
			SELECT Id, Error__c, Contract_VF__c, Agreement_Number__c, Order__c
			FROM VF_Asset__c
			WHERE CTN__c = '31621505108'
			LIMIT 1
		];
		System.assertNotEquals(null, vfAssetQuery.Order__c);
		Test.stopTest();
	}

	public static VF_Asset__c createAsset(
		Id accId,
		String dealerCode,
		String contractNumber,
		String ctnStatus,
		String banNumber,
		Boolean isRetention
	) {
		return createAsset(accId, null, dealerCode, contractNumber, ctnStatus, banNumber, isRetention);
	}

	public static VF_Asset__c createAsset(
		Id accId,
		String pricePlanClass,
		String dealerCode,
		String contractNumber,
		String ctnStatus,
		String banNumber,
		Boolean isRetention
	) {
		VF_Asset__c newAsset = new VF_Asset__c();
		newAsset.Account__c = accId;
		if (!String.isBlank(pricePlanClass)) {
			newAsset.Priceplan_Class__c = pricePlanClass;
		}
		newAsset.Initial_dealer_code__c = dealerCode;
		newAsset.Contract_Number__c = contractNumber;
		newAsset.CTN_Status__c = ctnStatus;
		newAsset.BAN_Number__c = banNumber;
		newAsset.Contract_Start_Date__c = system.today().addMonths(-6);
		newAsset.Contract_End_Date__c = newAsset.Contract_Start_Date__c.addMonths(6);
		if (isRetention) {
			newAsset.Retention_Date__c = system.today();
			newAsset.Retention_Dealer_code__c = dealerCode;
			newAsset.Contract_Start_Date__c = system.today();
			newAsset.Contract_End_Date__c = newAsset.Contract_Start_Date__c.addMonths(12);
		}
		return newAsset;
	}

	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, owner);
		opp.Select_Journey__c = 'Sales';
		update opp;
		contract = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		claimerContact.Email = 'a@b.com';
		update claimerContact;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, claimerContact.id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(claimerContact.id, '123321');
		contract.Dealer_Information__c = dealerInfo.Id;
		contract.Unify_Framework_Id__c = '9634563A';
		contract.Signed_by_Customer__c = claimerContact.Id;
		update contract;

		order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contract.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			Billing_Arrangement__c = ba.id
		);
		insert order;

		netProfitInformation = TestUtils.createNetProfitInformation(order);
	}
}
