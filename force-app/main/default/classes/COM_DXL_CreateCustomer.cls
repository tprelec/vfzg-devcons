/*
Author: Nikola Culej, May 4th 2022.
COM_DXL_CreateCustomer - call the DXL CreateCustomer API
 */
global with sharing class COM_DXL_CreateCustomer {
	@InvocableMethod(label='COM_DXL_CreateCustomer' description='Send information to DXL for customer creation')
	global static void callDxlCreateCustomer(List<Id> orderIds) {
		dxlFutureCreateCustomer(orderIds);
	}

	@future(callout=true)
	public static void dxlFutureCreateCustomer(List<Id> orderIds) {
		dxlCreateCustomer(orderIds);
	}

	public static void dxlCreateCustomer(List<Id> orderIds) {
		System.debug(LoggingLevel.DEBUG, 'Call DXL CreateCustomer API for following Orders: ' + JSON.serializePretty(orderIds));

		List<csord__Order__c> orderRecordList = [SELECT Id, Name, csordtelcoa__Opportunity__c FROM csord__Order__c WHERE Id = :orderIds[0]];

		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(
			orderRecordList[0].csordtelcoa__Opportunity__c,
			orderIds[0]
		);
		String payloadString = customerService.generateDxlCreateCustomerPayload();
		System.debug(LoggingLevel.DEBUG, '****payloadString: ' + payloadString);
		performCallout(payloadString, customerService);
	}

	public static void performCallout(String payloadString, COM_DXL_CreateCustomerService customerService) {
		COM_Integration_setting__mdt dxlIntegrationSettings = COM_Integration_setting__mdt.getInstance(
			COM_DXL_Constants.COM_DXL_INTEGRATION_SETTING_NAME
		);
		Boolean doNotExecuteReq = dxlIntegrationSettings != null ? dxlIntegrationSettings.Execute_mock__c : true;

		Boolean generateAttachment = dxlIntegrationSettings != null ? dxlIntegrationSettings.Generate_Attachment__c : true;

		COM_DXL_settings__mdt businessTransactionIdSetting = COM_DXL_settings__mdt.getInstance(
			COM_DXL_Constants.CREATECUSTOMER_SYNC_RESPONSE_TRANSACTION_ID
		);

		String businessTransactionIdKey = businessTransactionIdSetting != null
			? businessTransactionIdSetting.Value__c
			: COM_DXL_Constants.BUSINESS_TRANSACTION_ID;

		Http http = new Http();
		COM_DXLHttpService dxlHTTPService = new COM_DXLHttpService(payloadString);
		HttpRequest dxlRequest = dxlHTTPService.createDxlHttpRequest(
			dxlIntegrationSettings,
			businessTransactionIdKey,
			customerService.opportunityData
		);
		HttpResponse dxlSyncResponse = new HttpResponse();

		CreateCustomerResponseData customerResponseData = new CreateCustomerResponseData();
		customerResponseData.businessTransactionIdKey = businessTransactionIdKey;
		customerResponseData.businessTransactionId = dxlRequest.getHeader(businessTransactionIdKey);

		if (doNotExecuteReq && !Test.isRunningTest()) {
			customerResponseData.success = true;
			customerResponseData.statusCode = 200;
			dxlSyncResponse = dxlHTTPService.createDxlHttpResponse(customerResponseData);
		} else {
			dxlSyncResponse = http.send(dxlRequest);
		}

		customerService.createTransactionLog(dxlSyncResponse, customerService.opportunityData[0].Id, businessTransactionIdKey);

		if (generateAttachment) {
			customerService.createRequestAttachment(customerService.opportunityData[0].Id, payloadString);
		}
	}

	public class CreateCustomerResponseData {
		public String businessTransactionId;
		public String businessTransactionIdKey;
		public Integer statusCode;
		public Boolean success;
	}
}
