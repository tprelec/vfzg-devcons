/**
* Batch class which deletes product configurations and associated attributes to get more data storage space.
* Product Configurations will be deleted according to following criteria:
*     - createdDate of Product Configuration is within the time frame defined in Delete_Product_Configurations_Setting__c
*       custom setting with format: 'yyyy-MM-dd' 
*     - product basket is null.
*/
public with sharing class DeleteProductConfigurations implements Database.Batchable<sObject>{
    
    private DateTime startDate;
    private DateTime endDate;
    private final String query;
    
    public DeleteProductConfigurations(){
        query = 'SELECT Id, CreatedDate, cscfga__Configuration_Offer__c FROM cscfga__Product_Configuration__c WHERE (CreatedDate <= :endDate AND CreatedDate >= :startDate AND cscfga__Configuration_Offer__c = \'\') OR (cscfga__Product_Basket__c = \'\' AND cscfga__Configuration_Offer__c = \'\') LIMIT 50000000';
    }
    
    public DeleteProductConfigurations(String q){
        query = q;
    }
    
    public static void deleteStart(){
        DeleteProductConfigurations deleteProductConfigurations = new DeleteProductConfigurations();
        ID batchProcessId = Database.executeBatch(deleteProductConfigurations);
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){
        
        String configStart = Label.CS_ConfigDeleteStart;
        String configEnd = Label.CS_ConfigDeleteEnd;
        
        Integer startDateInt = Integer.valueOf(configStart);
        Integer endDateInt = Integer.valueOf(configEnd);
        
        //endDate = DateTime.now().addDays(-28);
        //startDate = endDate.addDays(-360);
        
        endDate = DateTime.now().addDays(-startDateInt);
        startDate = endDate.addDays(-endDateInt);
        
        
        System.debug('***LOG*** start date: ' + startDate);
        System.debug('***LOG*** end date: ' + endDate);
        
        return Database.getQueryLocator(query);
        
    }
        
    public void execute(Database.BatchableContext bc, List<sObject> scope){
        delete scope;
    }
    
    public void finish(Database.BatchableContext BC){
    
        AsyncApexJob a = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems FROM AsyncApexJob WHERE Id = :BC.getJobId()];
    
        System.debug('***LOG*** The batch Apex job processed ' + a.TotalJobItems + ' batches with '+ a.NumberOfErrors + ' failures.');
    }
}