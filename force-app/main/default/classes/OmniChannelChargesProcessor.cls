global with sharing class OmniChannelChargesProcessor {
	private static List<cspsi.CommonCartWrapper.Discount> getCommonCartDiscounts(
		List<CCAQDProcessor.CCDiscount> aqdDiscounts,
		String CS_QuantityStrategy,
		Integer quantity,
		Boolean filterSFCCContribution
	) {
		List<cspsi.CommonCartWrapper.Discount> commonCartDiscounts = new List<cspsi.CommonCartWrapper.Discount>();
		for (CCAQDProcessor.CCDiscount aqdDiscount : aqdDiscounts) {
			if (aqdDiscount.isPRSDiscount != filterSFCCContribution) {
				commonCartDiscounts.add(ChargesProcessorUtils.getCommonCartDiscount(aqdDiscount, CS_QuantityStrategy, quantity));
			}
		}
		return commonCartDiscounts;
	}

	private static List<cspsi.CommonCartWrapper.Discount> getCommonCartDiscountsFromAQDCharges(
		List<CCAQDProcessor.CCCharge> aqdCharges,
		String discountPrice,
		String CS_QuantityStrategy,
		Integer quantity
	) {
		List<cspsi.CommonCartWrapper.Discount> commonCartDiscounts = new List<cspsi.CommonCartWrapper.Discount>();
		for (CCAQDProcessor.CCCharge aqdCharge : aqdCharges) {
			commonCartDiscounts.add(
				ChargesProcessorUtils.getCommonCartDiscountFromAQDCharge(aqdCharge, discountPrice, CS_QuantityStrategy, quantity)
			);
		}
		return commonCartDiscounts;
	}

	global static List<cspsi.CommonCartWrapper.Discount> getSFCCContribution(
		List<CCAQDProcessor.CCDiscount> aqdDiscounts,
		String CS_QuantityStrategy,
		Integer quantity
	) {
		return getCommonCartDiscounts(aqdDiscounts, CS_QuantityStrategy, quantity, true);
	}

	global static List<cspsi.CommonCartWrapper.Discount> getPSContribution(
		List<CCAQDProcessor.CCCharge> aqdCharges,
		List<CCAQDProcessor.CCDiscount> aqdDiscounts,
		String CS_QuantityStrategy,
		Integer quantity
	) {
		List<cspsi.CommonCartWrapper.Discount> psContribution = new List<cspsi.CommonCartWrapper.Discount>();

		List<cspsi.CommonCartWrapper.Discount> chargeSalesOverride = getCommonCartDiscountsFromAQDCharges(
			aqdCharges,
			ChargesProcessorUtils.SALES_DISCOUNT_PRICE,
			CS_QuantityStrategy,
			quantity
		);
		List<cspsi.CommonCartWrapper.Discount> chargeListOverride = getCommonCartDiscountsFromAQDCharges(
			aqdCharges,
			ChargesProcessorUtils.LIST_DISCOUNT_PRICE,
			CS_QuantityStrategy,
			quantity
		);
		List<cspsi.CommonCartWrapper.Discount> discounts = getCommonCartDiscounts(aqdDiscounts, CS_QuantityStrategy, quantity, false);

		psContribution.addAll(chargeListOverride);
		psContribution.addAll(chargeSalesOverride);
		psContribution.addAll(discounts);

		return psContribution;
	}
}
