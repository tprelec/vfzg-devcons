@isTest
public class CS_ONEvsONXAddonFilterTest {

    @TestSetup
    private static void testSetup() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            OrderType__c orderType = CS_DataTest.createOrderType();
            insert orderType;

            Product2 product1 = CS_DataTest.createProduct('Mobile hardware', orderType);
            insert product1;
      
            Category__c mobileCategory = CS_DataTest.createCategory('Mobile hardware');
            insert mobileCategory;
      
            Vendor__c vendor1 = CS_DataTest.createVendor('KPNWEAS');
            insert vendor1;

            cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(product1, mobileCategory, 20480.0, 20480.0, vendor1, 'ONNET', 'OfficeTTR2BD');
            priceItem1.mobile_add_on_category__c = 'Mobile hardware';
            priceItem1.Mobile_Scenario_Text__c = 'Connect subscription';
            insert priceItem1; 
      
            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;
      
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            cspmb__Add_On_Price_Item__c testAddOn = CS_DataTest.createAddOnPriceItem('Test Add-On');
            testAddOn.cspmb__one_off_charge__c = 10;
            testAddOn.cspmb__recurring_charge__c = 1;
            testAddOn.External_ID__c = 'TestExId';
            testAddOn.Max_Duration__c = 9999;
            testAddOn.Min_Duration__c = 12;
            testAddOn.Skilled_Based_Level__c = '2';
            insert testAddOn;

            cspmb__Price_Item_Add_On_Price_Item_Association__c testPiaopi = CS_DataTest.createPriceItemAddOnAssociation(priceItem1, testAddOn);
            testPiaopi.cspmb__group__c = 'testGroup';
            insert testPiaopi;
        }
    }

    @isTest
    private static void testRequiredAttributes() {
        CS_ONEvsONXAddonFilter reqAttrs = new CS_ONEvsONXAddonFilter();
        String result;
        Test.startTest();
        result = reqAttrs.getRequiredAttributes();
        Test.stopTest();
        System.assertNotEquals(result.length() , 0, 'String should be longer than 0');        
    }

    @isTest
    private static void testDoLookupSearch() {
        cspmb__Price_Item__c testPriceItem = [SELECT id FROM cspmb__Price_Item__c LIMIT 1];

        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('Contract Duration', '24');
        searchFields.put('CS_PricePlan', testPriceItem.Id);
        searchFields.put('User Skill level', '2');
        searchFields.put('One Net Scenario', 'One Net Express');

        CS_ONEvsONXAddonFilter reqAttrs = new CS_ONEvsONXAddonFilter();
        List<Object> result = new List<Object>();
        Test.startTest();
        result = (List<cspmb__Price_Item_Add_On_Price_Item_Association__c>)reqAttrs.doLookupSearch(searchFields,'',null,0,0);
        Test.stopTest();

        System.assertNotEquals(result, null, 'result should be one object');
    }
}