@isTest
private class TestAddOnPriceItemTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Add On Price Item';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'AOPI-45-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	static void createAddOn() {
		TestUtils.createOrderType();
		TestUtils.createOneOffProduct();
		TestUtils.createRecurringProduct();

		Product2[] ooprod = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = :TestUtils.theOneOffProduct.ProductCode AND IsActive = TRUE];
		Product2[] recprod = [
			SELECT Id, ProductCode
			FROM Product2
			WHERE ProductCode = :TestUtils.theRecurringProduct.ProductCode AND IsActive = TRUE
		];

		cspmb__Add_On_Price_Item__c aopi = new cspmb__Add_On_Price_Item__c();
		aopi.cspmb__One_Off_Charge_External_Id__c = ooprod[0].ProductCode;
		aopi.cspmb__Recurring_Charge_External_Id__c = recprod[0].ProductCode;

		test.startTest();
		insert aopi;
		system.assertEquals(
			ooprod[0].Id,
			[SELECT One_Off_Charge_Product__c FROM cspmb__Add_On_Price_Item__c LIMIT 1]
			.One_Off_Charge_Product__c,
			'Must be equal'
		);
		system.assertEquals(
			recprod[0].Id,
			[SELECT Recurring_Charge_Product__c FROM cspmb__Add_On_Price_Item__c LIMIT 1]
			.Recurring_Charge_Product__c,
			'Must be equal'
		);
		test.stopTest();
	}

	@isTest
	static void updateAddOn() {
		cspmb__Add_On_Price_Item__c aopi = new cspmb__Add_On_Price_Item__c();
		insert aopi;

		aopi.Name = 'Test';

		test.startTest();
		update aopi;
		system.assertEquals(aopi.Name, 'Test', 'Name should be updated');
		test.stopTest();
	}
}
