public with sharing class EMP_TIBCO_ValidateOrderFEBulk {
	public Request validateOrderFEBulkRequest { get; set; }

	public EMP_TIBCO_ValidateOrderFEBulk(Request req) {
		this.validateOrderFEBulkRequest = req;
	}

	public class Request {
		public List<EMP_TIBCO_HttpHeader> sharedBslHttpHeaders { get; set; }
		public List<ValidateOrderFERequest> validateOrderFERequests { get; set; }

		public Request(
			List<EMP_TIBCO_HttpHeader> sharedBslHttpHeaders,
			List<ValidateOrderFERequest> validateOrderFERequests
		) {
			this.sharedBslHttpHeaders = sharedBslHttpHeaders;
			this.validateOrderFERequests = validateOrderFERequests;
		}
	}

	public class ValidateOrderFERequest {
		public List<EMP_TIBCO_HttpHeader> bslHttpHeaders { get; set; }
		public String correlationID { get; set; }
		public Integer priority { get; set; }
		public EMP_ValidateOrderFE.Request validateOrderFERequest { get; set; }

		public ValidateOrderFERequest(
			List<EMP_TIBCO_HttpHeader> bslHttpHeaders,
			String correlationID,
			EMP_ValidateOrderFE.Request validateOrderFERequest
		) {
			this.bslHttpHeaders = bslHttpHeaders;
			if (this.bslHttpHeaders == null) {
				this.bslHttpHeaders = new List<EMP_TIBCO_HttpHeader>();
			}
			this.correlationID = correlationID;
			this.validateOrderFERequest = validateOrderFERequest;
			this.priority = 1;
		}
	}

	public class Response {
		public Integer status { get; set; }
		public String httpRequest { get; set; }
		public String httpResponse { get; set; }
		public List<ResponseData> data { get; set; }
		public List<ResponseError> error { get; set; }

		public String getErrorMessages() {
			String errorMsg = '';
			for (ResponseError errorDetails : error) {
				errorMsg += 'Status code: ' + errorDetails.code;
				errorMsg += ' => Message: ' + errorDetails.message + ';  ';
			}
			return errorMsg;
		}
	}

	public class ResponseData {
		public String status { get; set; }
	}

	public class ResponseError {
		public String code { get; set; }
		public String message { get; set; }
	}
}