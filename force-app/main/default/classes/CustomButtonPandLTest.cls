@isTest
public class CustomButtonPandLTest {

  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        System.runAs (simpleUser) { 
      
     
        Framework__c frameworkSetting = new Framework__c();
        frameworkSetting.Framework_Sequence_Number__c = 2;
        insert frameworkSetting;
      
        PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
        insert priceResetSetting;
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;
      
      
      
        Sales_Settings__c ssettings = new Sales_Settings__c();
        ssettings.Postalcode_check_validity_days__c = 2;
        ssettings.Max_Daily_Postalcode_Checks__c = 2;
        ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
        ssettings.Postalcode_check_block_period_days__c = 2;
        ssettings.Max_weekly_postalcode_checks__c = 15;
        insert ssettings;
      
        Test.startTest();
        
        Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
        insert testOpp;
        
        
        cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        basket.Primary__c = true;
        basket.cscfga__Basket_Status__c = 'Valid';
        insert basket;
        
        CustomButtonPandLLIG button1 = new CustomButtonPandLLIG();
       
        button1.performAction(String.valueOf(basket.Id));
        
        CustomButtonPandLVF button2 = new CustomButtonPandLVF();
       
        button2.performAction(String.valueOf(basket.Id));
        
        
        Test.stopTest();
     }
  }

  @isTest
  private static void testAsync() {
    List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
    List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
    User simpleUser = CS_DataTest.createUser(pList, roleList); 
    insert simpleUser;
    System.runAs (simpleUser) { 
      Framework__c frameworkSetting = new Framework__c();
      frameworkSetting.Framework_Sequence_Number__c = 2;
      insert frameworkSetting;
  
      PriceReset__c priceResetSetting = new PriceReset__c();
      priceResetSetting.MaxRecurringPrice__c = 200.00;
      priceResetSetting.ConfigurationName__c = 'IP Pin';
      insert priceResetSetting;

      Account testAccount = CS_DataTest.createAccount('Test Account');
      insert testAccount;
  
      Sales_Settings__c ssettings = new Sales_Settings__c();
      ssettings.Postalcode_check_validity_days__c = 2;
      ssettings.Max_Daily_Postalcode_Checks__c = 2;
      ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
      ssettings.Postalcode_check_block_period_days__c = 2;
      ssettings.Max_weekly_postalcode_checks__c = 15;
      insert ssettings;
  
      Test.startTest();
    
      Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
      insert testOpp;
    
      cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
      basket.Primary__c = true;
      basket.cscfga__Basket_Status__c = 'Valid';
      insert basket;

      cscfga__Product_Definition__c def = CS_DataTest.createProductDefinitionRegular('Vodafone Calling');
      insert def;

      List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>();
      for(Integer i = 0; i<120; i++) {
        String cfgName = 'Config' + i;
        configs.add(CS_DataTest.createProductConfiguration(def.Id, cfgName, basket.Id));
      }
      insert configs;

      CustomButtonPandLVF button2 = new CustomButtonPandLVF();
      button2.performAction(String.valueOf(basket.Id));

      CS_Future_Controller futureController = new CS_Future_Controller(basket.Id, 'Parent process for P&L', '', '');
      Id newFutureId = futureController.defineNewFutureJob('P&L snapshots and inputs');
      CustomButtonPandLVF.createPlPrerequisitiesAsync((String)basket.Id, newFutureId);

      cscfga__Product_Basket__c basket1 = [SELECT Id, PL_Inputs__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
      System.assertNotEquals('', basket1.PL_Inputs__c, 'PL Inputs should be populated');
    
      Test.stopTest();
    }
  }
}