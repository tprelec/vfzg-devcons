public without sharing class OrderEntryContactController {
	/**
	 * @description Gets all Account Contacts
	 * @param  accId Account ID
	 * @return       List of Account Contacts
	 */
	@AuraEnabled
	public static List<Contact> getContacts(Id accId) {
		return [
			SELECT
				Id,
				FirstName,
				LastName,
				MobilePhone,
				Phone,
				Email,
				Birthdate,
				LG_PreferredCommunication__c,
				Document_Type__c,
				Document_Expiration_Date__c,
				Document_Number__c,
				Document_Issuing_Country__c,
				Salutation
			FROM Contact
			WHERE AccountId = :accId
		];
	}

	/**
	 * @description Gets Contact details
	 * @param  contId Contact ID
	 * @return        Contact
	 */
	@AuraEnabled(cacheable=false)
	public static Contact getContact(Id contId) {
		return [
			SELECT
				Id,
				FirstName,
				LastName,
				MobilePhone,
				Phone,
				Email,
				Birthdate,
				LG_PreferredCommunication__c,
				Document_Type__c,
				Document_Expiration_Date__c,
				Document_Number__c,
				Document_Issuing_Country__c,
				Salutation
			FROM Contact
			WHERE Id = :contId
		];
	}
	/**
	 * @description Returns list of countries used for Document Issuing Country
	 * @return List of all countries
	 */
	@AuraEnabled(cacheable=true)
	public static Map<String, String> getCountries() {
		Map<String, String> countries = new Map<String, String>();

		countries = AddressService.getCountries();

		return countries;
	}

	/**
	 * @description Saves Contact & Contact Roles
	 * @param  newContact New Contact
	 * @param  oppId      Opporunity ID
	 * @return            Newly Created Contact
	 */
	@AuraEnabled
	public static Contact saveContact(Contact newContact, Id oppId) {
		Boolean createContactRole = true;
		// Save newly created contact
		Contact cnt = newContact;
		upsert cnt;

		// Set Primary contact on opportunity
		Opportunity opp = [SELECT Id, LG_PrimaryContact__c FROM Opportunity WHERE Id = :oppId];

		// Get old primary contact(s) role and un-set the flag isPrimary
		List<OpportunityContactRole> oldPrimCnt = [
			SELECT Id, IsPrimary, OpportunityId, ContactId
			FROM OpportunityContactRole
			WHERE OpportunityId = :oppId
		];

		List<OpportunityContactRole> ocrForDelete = new List<OpportunityContactRole>();
		for (OpportunityContactRole ocr : oldPrimCnt) {
			if (ocr.ContactId != cnt.Id) {
				ocrForDelete.add(ocr);
			} else {
				createContactRole = false;
			}
		}

		delete ocrForDelete;

		// Set new contact as primary contact on opportunity
		if (createContactRole) {
			insert new OpportunityContactRole(ContactId = cnt.Id, Role = 'Administrative Contact', IsPrimary = true, opportunityId = oppId);
		}

		opp.LG_PrimaryContact__c = cnt.Id;
		update opp;

		return cnt;
	}
}
