global with sharing class CustomButtonManageApprovals extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		String newUrl = CustomButtonManageApprovals.redirectToManageApprovals(basketId);
		return '{"status":"ok","redirectURL":"' + newUrl + '"}';
	}

	// Set url for redirect after action
	public static String redirectToManageApprovals(String basketId) {
		//PageReference editPage = new PageReference(url.URL__c+'/apex/ProductBasketApproval?Id='+basketId);
		PageReference editPage = new PageReference('/apex/appro__VFSubmitPreview?id=' + basketId);
		return editPage.getUrl();
	}
}
