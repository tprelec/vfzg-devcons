@isTest
private class CustomButtonResponseTest {
    
    @IsTest
    static void test(){
        Test.startTest();
        
        CustomButtonResponse responseError = new CustomButtonResponse('error', 'Error', 'test error');
        String responseErrorSerialized = JSON.serialize(responseError);
        System.assertEquals(true, responseErrorSerialized.contains('"status":"error"'));
        System.assertEquals(true, responseErrorSerialized.contains('"timeout":1000'));
        System.assertEquals(true, responseErrorSerialized.contains('"title":"Error"'));
        System.assertEquals(true, responseErrorSerialized.contains('"text":"test error"'));

        
        CustomButtonResponse responseSuccess = new CustomButtonResponse('ok', 'Success', 'test success');
        String responseSuccessSerialized = JSON.serialize(responseSuccess);
        System.assertEquals(true, responseSuccessSerialized.contains('"status":"ok"'));
        System.assertEquals(true, responseSuccessSerialized.contains('"title":"Success"'));
        System.assertEquals(true, responseSuccessSerialized.contains('"text":"test success"'));
        
        Test.stopTest();
    }
}