public with sharing class RecordTypeService {
	public static String getRecordTypeNameById(String objectName, Id strRecordTypeId) {
		return Schema.getGlobalDescribe()
			.get(objectName)
			.getDescribe()
			.getRecordTypeInfosById()
			.get(strRecordTypeId)
			.getName();
	}

	public static Id getRecordTypeIdByName(String objectName, String recordTypeName) {
		return ((SObject) Type.forName(objectName).newInstance())
			.getSObjectType()
			.getDescribe()
			.getRecordTypeInfosByName()
			.get(recordTypeName)
			.getRecordTypeId();
	}
}