/**
 * Created by juan.cardona on 02/06/2021.
 * Iterable implementation for Object.
 */

global with sharing class CustomIterableUtils implements Iterator<Object> {

    List<Object> usList { get; set; }
    Integer i { get; set; }
    Integer listSize;

    global CustomIterableUtils(List<Object> listObj) {
        usList = listObj;
        listSize = usList.size();
        i = 0;
    }

    global Boolean hasNext() {
        if (i >= usList.size()) {
            return false;
        } else {
            return true;
        }
    }

    global Object next() {
        if (i == listSize) {
            return null;
        }
        i++;
        return usList[i - 1];
    }
}