/*
 *    Description:  Contains methods for validating different types of Orders in various phases of the order lifecycle
 *    History    :  29.03.2018 Marcel Vreuls Removed orderformCache references
 */
public with sharing class OrderValidation {
	// list of customer-export related objects. To handle split export validations (e.g. customer to sias, order to bop)
	public static final Set<String> customerObjects = new Set<String>{
		'account',
		'ban__c',
		'billing_arrangement__c',
		'contact',
		'contact_role__c',
		'financial_account__c',
		'site__c',
		'user'
	};
	public static final Set<String> attachmentObjectTypes = new Set<String>{ 'contract_attachments__r', 'opportunity_attachments__r' };
	public static final Set<String> lineItemObjectTypes = new Set<String>{ 'contracted_products__r', 'opportunitylineItems' };
	public static string profile = '';

	/*
	 *    Description:  provides a map of all orderValidations Object -> Validations
	 */

	public static Map<String, List<Order_Validation__c>> objectToValidations {
		get {
			if (objectToValidations == null) {
				objectToValidations = new Map<String, List<Order_Validation__c>>();
				for (Order_Validation__c ov : [
					SELECT
						Id,
						Name,
						Proposition__c,
						Object__c,
						Who__c,
						Export_System__c,
						Operator__c,
						API_Fieldname__c,
						Value__c,
						Logical_Operator__c,
						Operator_2__c,
						API_Fieldname_2__c,
						Value_2__c,
						Operator_3__c,
						API_Fieldname_3__c,
						Value_3__c,
						Error_message__c
					FROM Order_Validation__c
					WHERE Active__c = TRUE
				]) {
					if (objectToValidations.containsKey(ov.Object__c.toLowerCase())) {
						objectToValidations.get(ov.Object__c.toLowerCase()).add(ov);
					} else {
						objectToValidations.put(ov.Object__c.toLowerCase(), new List<Order_Validation__c>{ ov });
					}
				}
			}
			return objectToValidations;
		}
		set;
	}

	/*
	 *    Description:  check one single object (all validations) in the context of an order
	 */
	public static String checkObject(sObject obj, Order__c theOrder) {
		System.debug(obj);
		String errors = '';

		if (theOrder == null || obj == null) {
			return '';
		}

		// for partner users, check the partner contact instead
		if (obj.getSObjectType().getDescribe().getName() == 'User') {
			User u = (User) obj;
			try {
				if (u.ContactId != null)
					obj = [
						SELECT
							Id,
							Userid__c,
							Status_Contact__c,
							Email,
							Gender__c,
							Phone,
							MobilePhone,
							BOP_Export_ErrorMessage__c,
							BOP_Export_Datetime__c
						FROM Contact
						WHERE Id = :u.ContactId
					];
			} catch (SObjectException e) {
				// no problem if this happens. The contactId might not have been queried. In that case, it's not a partner user
			}
		}

		// collect rules that are applicable
		String objectName = obj.getSObjectType().getDescribe().getName().toLowerCase();

		// in case of a contactrole, also check the contact
		if (objectname == 'contact_role__c') {
			Contact_Role__c cr = (Contact_Role__c) obj;
			if (cr.Contact__r != null)
				errors += checkObject(cr.Contact__r, theOrder);
		}

		transient List<Order_Validation__c> validations = objectToValidations.get(objectName);

		if (validations != null) {
			String who = '';
			if (theOrder.Status__c == 'Validation' || theOrder.Status__c == 'Validation failed') {
				who = 'Order Check';
			} else if (theOrder.Ready_for_Inside_Sales__c) {
				who = 'Inside Sales';
			} else {
				if (theOrder.IsPartner__c) {
					who = 'Indirect Sales';
				} else {
					who = 'Direct Sales';
				}
			}

			// Get the profile of the current user
			if (String.isBlank(profile)) {
				User currUser = [SELECT Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
				profile = currUser.Profile.Name;
			}

			// do checks that match 'who' and 'proposition'
			for (Order_Validation__c validation : validations) {
				String[] whoList = validation.Who__c.split(';');
				Boolean whoMatch = false;
				for (String s : whoList) {
					if (s == who || s == profile) {
						whoMatch = true;
						break;
					}
				}
				if (whoMatch) {
					System.debug('val ' + validation);
					// validation to use depends on whether it's a customer or an order validation
					String exportSystem;
					if (customerObjects.contains(objectName)) {
						exportSystem = theOrder.Export_system_Customerdata__c;
					} else {
						exportSystem = theOrder.Export__c;
					}

					// only filter for export system if both the validation and the order have one specified
					if (validation.Export_System__c == null || exportSystem == null || validation.Export_System__c.contains(exportSystem)) {
						//system.debug(validation.Proposition__c);
						List<String> validationPropositions = validation.Proposition__c.split(';');
						// if the validation is for 'mobile', expand it to all mobile propositions
						for (Integer i = 0; i < validationPropositions.size(); i++) {
							String prop = validationPropositions[i];
							if (prop == 'Mobile') {
								validationPropositions.addAll(OrderFormController.allPropositionsByType.get('Mobile'));
							}
						}
						for (String valProp : validationPropositions) {
							// if the object is a contractedProduct, only apply the checks based on the cp's proposition
							// for all other objects, apply all checks
							valProp = valProp.trim();
							if (objectname == 'contracted_products__c') {
								// default to 'legacy'. If a proposition if found on cp, replace it by that one
								String cpProposition = 'Legacy';
								if (obj.get('Proposition__c') != null) {
									cpProposition = String.valueOf(obj.get('Proposition__c'));
								}
								if (cpProposition.contains(valProp)) {
									errors += runValidation(obj, validation);
									// each validation only has to run once, even if there are more proposition matches
									break;
								}
							} else {
								if (theOrder.Propositions__c == null || theOrder.Propositions__c.contains(valProp)) {
									errors += runValidation(obj, validation);
									// each validation only has to run once, even if there are more proposition matches
									break;
								}
							}
						}
					}
				}
			}
		}

		return errors;
	}

	/*
	 *    Description:  run 1 single order validation
	 */
	public static String runValidation(sObject obj, Order_Validation__c validation) {
		String error = '';
		Integer errorCount = 0;
		Integer numberOfFields = 0;

		Boolean errorFound = false;
		Boolean skipANDChecks = false;
		Boolean skipORChecks = false;

		if (validation.API_Fieldname__c != null) {
			numberOfFields++;
			errorCount += fieldChecker(obj, validation.Operator__c, validation.API_Fieldname__c.toLowerCase(), validation.Value__c);
		}
		if (validation.Logical_Operator__c == 'AND' && errorCount == 0) {
			skipANDChecks = true; // shortcut for AND
		} else if (validation.Logical_Operator__c == 'OR' && errorCount > 0) {
			skipORChecks = true; // shortcut for OR
		}

		if (!skipANDChecks && !skipORChecks) {
			if (validation.API_Fieldname_2__c != null) {
				numberOfFields++;
				errorCount += fieldChecker(obj, validation.Operator_2__c, validation.API_Fieldname_2__c.toLowerCase(), validation.Value_2__c);
			}
		}
		if (validation.Logical_Operator__c == 'AND' && errorCount == 0) {
			skipANDChecks = true; // shortcut for AND
		} else if (validation.Logical_Operator__c == 'OR' && errorCount > 0) {
			skipORChecks = true; // shortcut for OR
		}

		if (!skipANDChecks && !skipORChecks) {
			if (validation.API_Fieldname_3__c != null) {
				if (validation.Logical_Operator__c == 'XOR') {
					return 'XOR can only be used with 2 fields.';
				}

				numberOfFields++;
				errorCount += fieldChecker(obj, validation.Operator_3__c, validation.API_Fieldname_3__c.toLowerCase(), validation.Value_3__c);
			}
		}

		if (!skipANDChecks) {
			// taking into account the shortcuts for 'AND'
			if (validation.Logical_Operator__c == null && errorCount > 0) {
				errorFound = true;
			} else if (validation.Logical_Operator__c == 'AND' && errorCount > (numberOfFields - 1)) {
				errorFound = true;
			} else if (validation.Logical_Operator__c == 'OR' && errorCount > 0) {
				errorFound = true;
			} else if (validation.Logical_Operator__c == 'XOR' && errorCount != 1) {
				errorFound = true;
			} else if (validation.Logical_Operator__c == 'NOR' && errorCount == 0) {
				errorFound = true;
			}
		}

		if (errorFound) {
			// replace error_message by label-text if a label is used. DOES THIS ACTUALLY WORK?
			String errorMessage;
			String result = validation.Error_message__c;
			if (result.tolowercase().startsWith('label.')) {
				try {
					if (!Trigger.IsExecuting) {
						String[] label = result.split('\\.');
						errorMessage = StringUtils.translateLabel(label[1], null);
					} else {
						// if an error happens in a trigger, just dispay the label name as a workaround, until this is fixed by SFDC.
						errorMessage = result;
					}
				} catch (Exception e) {
					Apexpages.AddMessage(
						new Apexpages.Message(ApexPages.severity.ERROR, 'Error while retrieving the custom label:' + e.getMessage())
					);
				}
			} else {
				errorMessage = result;
			}

			error += errorMessage + ' ';
		}

		return error;
	}

	/*
	 *  check 1 single field. Returns 1 if check is not ok
	 */
	@TestVisible
	private static Integer fieldChecker(sObject obj, String operator, String fieldName, String value) {
		if (fieldName.contains('.')) {
			// process relationship fields
			sObject s = obj.getSobject(fieldName.split('\\.', 2)[0]);
			if (s == null) {
				// if the child object is empty, the validation fails (means the field is not queried)
				return 1;
			} else {
				return fieldChecker(s, operator, fieldName.split('\\.', 2)[1], value);
			}
		} else {
			Id objId;
			if (obj.get('Id') != null) {
				objId = Id.valueOf(String.valueOf(obj.get('Id')));
			}

			// normal fields
			if (operator == 'isblank') {
				if (obj.get(fieldName) == null || obj.get(fieldName) == '') {
					return 1;
				}
			} else if (operator == 'isnotblank') {
				if (obj.get(fieldName) != null && obj.get(fieldName) != '') {
					return 1;
				}
			} else if (operator == '==') {
				// allow multiple comma-separate values (if any matches, then error)
				if (value == null) {
					value = '';
				}
				for (String subValue : value.split(',')) {
					if (obj.get(fieldName) == subValue) {
						return 1;
					}
				}
			} else if (operator == '!=') {
				// allow multiple comma-separate values (if any matches, then no error)
				if (value == null)
					value = '';
				for (String subValue : value.split(',')) {
					if (obj.get(fieldName) == subValue) {
						return 0;
					}
				}
				return 1;
			} else if (operator == '<') {
				if (Double.valueOf(obj.get(fieldName)) < Double.valueOf(value)) {
					return 1;
				}
			} else if (operator == '>') {
				if (Double.valueOf(obj.get(fieldName)) > Double.valueOf(value)) {
					return 1;
				}
			} else if (operator == '<=') {
				if (Double.valueOf(obj.get(fieldName)) <= Double.valueOf(value)) {
					return 1;
				}
			} else if (operator == '>=') {
				if (Double.valueOf(obj.get(fieldName)) >= Double.valueOf(value)) {
					return 1;
				}
			} else if (operator == 'false') {
				if (obj.get(fieldName) == false) {
					return 1;
				}
			} else if (operator == 'true') {
				if (obj.get(fieldName) == true) {
					return 1;
				}
			} else if (operator == 'contains') {
				if (String.valueOf(obj.get(fieldName)) != null && String.valueOf(obj.get(fieldName)).contains(value)) {
					return 1;
				}
			} else if (operator == 'startsWith') {
				if (String.valueOf(obj.get(fieldName)) != null && String.valueOf(obj.get(fieldName)).startsWith(value)) {
					return 1;
				}
			} else if (operator == 'notStartsWith') {
				if (String.valueOf(obj.get(fieldName)) != null && !String.valueOf(obj.get(fieldName)).startsWith(value)) {
					return 1;
				}
			} else if (operator == 'minlength') {
				if (String.valueOf(obj.get(fieldName)) != null && String.valueOf(obj.get(fieldName)).length() < Integer.valueOf(value)) {
					return 1;
				}
			} else if (operator == 'maxlength') {
				if (String.valueOf(obj.get(fieldName)) != null && String.valueOf(obj.get(fieldName)).length() > Integer.valueOf(value)) {
					return 1;
				}
			} else if (operator == 'requiredAttachmentType') {
				Integer success = 1;
				Integer fail = 0;
				// check if this is the attachments required (for a certain lineitem type) or the attachments found
				if (lineItemObjectTypes.contains(fieldName)) {
					success = 0;
					fail = 1;
				}
				// all types need to be present
				for (String subValue : value.split(',')) {
					//system.debug(subValue);
					//system.debug(attachmentTypesFound(obj,fieldName));
					if (!attachmentTypesFound(objId, fieldName).contains(subValue)) {
						return success;
					}
				}
				return fail;
			} else if (operator == 'requiredAttachmentTypeOR') {
				Integer success = 1;
				Integer fail = 0;
				// check if this is the attachments required (for a certain lineitem type) or the attachments found
				if (lineItemObjectTypes.contains(fieldName)) {
					success = 0;
					fail = 1;
				}
				// at least 1 type needs to be present
				for (String subValue : value.split(',')) {
					if (attachmentTypesFound(objId, fieldName).contains(subValue)) {
						return fail;
					}
				}
				return success;
			} else if (operator == 'requiredChildType') {
				// all types needs to be present
				if (!childrenTypesFound(objId, fieldName).containsAll(value.split(','))) {
					return 1;
				}

				return 0;
			} else if (operator == 'requiredChildTypeOR') {
				// at least 1 role type needs to be present
				for (String subValue : value.split(',')) {
					if (childrenTypesFound(objId, fieldName).contains(subValue)) {
						return 0;
					}
				}

				return 1;
			} else if (operator == 'hasChildType') {
				// all types needs to be present
				if (childrenTypesFound(objId, fieldName).containsAll(value.split(','))) {
					return 1;
				}

				return 0;
			} else if (operator == 'hasChildTypeOR') {
				// all types needs to be present
				for (String subValue : value.split(',')) {
					if (childrenTypesFound(objId, fieldName).contains(subValue)) {
						return 1;
					}
				}

				return 0;
			}
		}

		return 0;
	}

	/*
	 * Map to store all attachmenttypes found per parent object, to prevent having to query multiple times
	 */
	public static Map<Id, Set<String>> objectIdToAttachmentTypes {
		get {
			if (objectIdToAttachmentTypes == null) {
				objectIdToAttachmentTypes = new Map<Id, Set<String>>();
			}
			return objectIdToAttachmentTypes;
		}
		set;
	}

	/*
	 * Map to store all required attachmenttypes found per parent object, to prevent having to query multiple times
	 */
	public static Map<Id, Set<String>> objectIdToLineItemTypes {
		get {
			if (objectIdToLineItemTypes == null) {
				objectIdToLineItemTypes = new Map<Id, Set<String>>();
			}
			return objectIdToLineItemTypes;
		}
		set;
	}

	/*
	 * Find all attachmenttypes for a parent object. Store in static map after retrieving
	 */
	public static Set<String> attachmentTypesFound(Id objId, String fieldName) {
		// check if this is the attachments required or the attachments found
		if (attachmentObjectTypes.contains(fieldName)) {
			if (objectIdToAttachmentTypes.containsKey(objId)) {
				return objectIdToAttachmentTypes.get(objId);
			} else {
				Set<String> attachmentTypesFound = new Set<String>();

				// find out attachment object type and fetch all types
				if (objId.getSObjectType().getDescribe().getName() == 'VF_Contract__c') {
					for (Contract_Attachment__c ca : [SELECT Attachment_Type__c FROM Contract_Attachment__c WHERE Contract_VF__c = :objId]) {
						attachmentTypesFound.add(ca.Attachment_Type__c);
					}
				} else if (objId.getSObjectType().getDescribe().getName() == 'Opportunity') {
					for (Opportunity_Attachment__c oa : [SELECT Attachment_Type__c FROM Opportunity_Attachment__c WHERE Opportunity__c = :objId]) {
						attachmentTypesFound.add(oa.Attachment_Type__c);
					}
				}
				objectIdToAttachmentTypes.put(objId, attachmentTypesFound);
				return attachmentTypesFound;
			}
		} else if (lineItemObjectTypes.contains(fieldName)) {
			if (objectIdToLineItemTypes.containsKey(objId)) {
				return objectIdToLineItemTypes.get(objId);
			} else {
				Set<String> lineitemTypesFound = new Set<String>();

				// find out lineitem object type and fetch all types
				if (objId.getSObjectType().getDescribe().getName() == 'VF_Contract__c') {
					for (Contracted_Products__c cp : [
						SELECT Product__r.Mandatory_Attachment_Type__c
						FROM Contracted_Products__c
						WHERE VF_Contract__c = :objId AND Product__r.Mandatory_Attachment_Type__c != NULL
					]) {
						lineitemTypesFound.addAll(cp.Product__r.Mandatory_Attachment_Type__c.split(';'));
					}
				} else if (objId.getSObjectType().getDescribe().getName() == 'Opportunity') {
					for (OpportunityLineItem oli : [
						SELECT PriceBookEntry.Product2.Mandatory_Attachment_Type__c
						FROM OpportunityLineItem
						WHERE OpportunityId = :objId AND PriceBookEntry.Product2.Mandatory_Attachment_Type__c != NULL
					]) {
						lineitemTypesFound.addAll(oli.PriceBookEntry.Product2.Mandatory_Attachment_Type__c.split(';'));
					}
				}
				objectIdToLineItemTypes.put(objId, lineitemTypesFound);
				return lineitemTypesFound;
			}
		}
		return null;
	}

	/*
	 * Map to store all queried objectid/fieldname/childrentypes found
	 */
	public static Map<String, Map<String, Set<String>>> objectIdToFieldNameToChildrenTypes {
		get {
			if (objectIdToFieldNameToChildrenTypes == null) {
				objectIdToFieldNameToChildrenTypes = new Map<String, Map<String, Set<String>>>();
			}
			return objectIdToFieldNameToChildrenTypes;
		}
		set;
	}

	/*
	 * Find all children types for a parent object. Store in static map after retrieving
	 */
	@TestVisible
	public static Set<String> childrenTypesFound(Id objId, String fieldName) {
		// check if this object/field combination was already queried before. If so, use that data. If not, create an empty  placeholder.
		if (objectIdToFieldNameToChildrenTypes.containsKey(objId)) {
			if (objectIdToFieldNameToChildrenTypes.get(objId).containsKey(fieldName)) {
				return objectIdToFieldNameToChildrenTypes.get(objId).get(fieldName);
			} else {
				objectIdToFieldNameToChildrenTypes.get(objId).put(fieldName, new Set<String>());
			}
		} else {
			objectIdToFieldNameToChildrenTypes.put(objId, new Map<String, Set<String>>{ fieldName => new Set<String>() });
		}

		Set<String> childrenTypesFound = new Set<String>();

		// find out child object type and fetch all types
		if (objId.getSObjectType().getDescribe().getName() == 'Account') {
			if (fieldName == 'contact_roles__r') {
				for (Contact_Role__c cr : [SELECT Type__c FROM Contact_Role__c WHERE Active__c = TRUE AND Account__c = :objId]) {
					childrenTypesFound.add(cr.Type__c);
				}
			}
		} else if (objId.getSObjectType().getDescribe().getName() == 'VF_Contract__c') {
			if (fieldName == 'family_condition__c') {
				for (Contracted_Products__c cp : [
					SELECT family_condition__c
					FROM Contracted_Products__c
					WHERE VF_Contract__c = :objId AND family_condition__c != NULL
				]) {
					for (String s : cp.family_condition__c.split(';'))
						childrenTypesFound.add(s);
				}
			}
		} else if (objId.getSObjectType().getDescribe().getName() == 'Contracted_Products__c') {
			if (fieldName == 'contracted_Products__r') {
				for (Contracted_Products__c cp : [
					SELECT Unify_Product_Family__c
					FROM Contracted_Products__c
					WHERE Unify_Parent_Component__c = :objId AND Unify_Product_Family__c != NULL
				]) {
					childrenTypesFound.add(cp.Unify_Product_Family__c);
				}
			}
		} else if (objId.getSObjectType().getDescribe().getName() == 'Opportunity') {
			if (fieldName == 'opportunityLineItems') {
				for (OpportunityLineItem cp : [SELECT PriceBookEntry.Product2.Family FROM OpportunityLineItem WHERE OpportunityId = :objId]) {
					childrenTypesFound.add(cp.PriceBookEntry.Product2.Family);
				}
			} else if (fieldName == 'family_condition__c') {
				for (OpportunityLineItem cp : [
					SELECT family_condition__c
					FROM OpportunityLineItem
					WHERE OpportunityId = :objId AND family_condition__c != NULL
				]) {
					for (String s : cp.family_condition__c.split(';'))
						childrenTypesFound.add(s);
				}
			} else if (fieldName == 'proposition__c') {
				for (OpportunityLineItem cp : [
					SELECT proposition__c
					FROM OpportunityLineItem
					WHERE OpportunityId = :objId AND proposition__c != NULL
				]) {
					childrenTypesFound.add(cp.proposition__c);
				}
			} else if (fieldName == 'clc__c') {
				for (OpportunityLineItem cp : [SELECT clc__c FROM OpportunityLineItem WHERE OpportunityId = :objId]) {
					childrenTypesFound.add(cp.clc__c);
				}
			} else if (fieldName == 'deal_type__c') {
				for (OpportunityLineItem cp : [SELECT deal_type__c FROM OpportunityLineItem WHERE OpportunityId = :objId]) {
					childrenTypesFound.add(cp.deal_type__c);
				}
			}
		}

		// put the result in the map of queries already done
		objectIdToFieldNameToChildrenTypes.get(objId).get(fieldName).addAll(childrenTypesFound);
		return childrenTypesFound;
	}

	/*
	 * Check complete set of objects for Order flag
	 */
	@TestVisible
	public static Boolean checkCompleteOrderFlag(Order__c theOrder) {
		Boolean success = false;

		if (OrderValidation.checkObject(theOrder, theOrder) == '')
			if (OrderValidation.checkObject(theOrder.Project_Contact__r, theOrder) == '')
				if (OrderValidation.checkObject(theOrder.Infra_Contact__r, theOrder) == '')
					if (OrderValidation.checkObject(theOrder.CC_Contact__r, theOrder) == '')
						if (OrderValidation.checkObject(theOrder.Porting_Contact__r, theOrder) == '')
							if (OrderValidation.checkObject(theOrder.Inside_Sales_Owner__r, theOrder) == '')
								success = true;

		if (success) {
			return true;
		} else {
			return false;
		}
	}

	public static String checkCompleteValidationBeforeBOP(Order__c theOrder) {
		String error = '';
		error += OrderValidation.checkObject(theOrder, theOrder);

		BAN__c ban = theOrder.VF_Contract__r.Opportunity__r.Ban__r;
		error += OrderValidation.checkObject(ban, theOrder);
		System.debug('error ban ' + error);
		System.debug('theOrder.Id' + theOrder.Id);

		Map<Id, Map<Id, DeliveryWrapper>> mapOrderLocations = OrderUtils.retrieveOrderIdToLocationIdToLocation(
			new Set<Id>{ theOrder.VF_Contract__c },
			new Set<Id>{ theOrder.Id }
		);
		if (!mapOrderLocations.isEmpty() && mapOrderLocations.containsKey(theOrder.Id)) {
			Map<Id, DeliveryWrapper> locMap = mapOrderLocations.get(theOrder.Id);
			if (!locMap.isEmpty()) {
				for (Id siteId : locMap.keySet()) {
					Site__c site = locMap.get(siteId).site;
					error += OrderValidation.checkObject(site, theOrder);
					System.debug('error site ' + error);
				}
			}
		}

		return error;
	}

	/*
	 *  Check complete set of objects for Customer flag
	 */
	public static Boolean checkCompleteCustomerFlag(VF_Contract__c theContract, List<Contact_Role__c> customerContacts, Order__c theOrder) {
		Boolean success = false;

		if (OrderValidation.checkObject(theContract, theOrder) == '') {
			// then check the related objects, account and accountmanager info
			if (OrderValidation.checkObject(theContract.Account__r, theOrder) == '') {
				if (OrderValidation.checkObject(theContract.Opportunity__r.Owner, theOrder) == '') {
					success = true;
				}
			}
		}

		if (success) {
			// check the customer contacts separately
			if (!customerContacts.isEmpty()) {
				Boolean validMainContactFound = false;
				for (Contact_Role__c cr : customerContacts) {
					if (OrderValidation.checkObject(cr, theOrder) != '') {
						return false;
					} else {
						if (cr.Type__c == 'Main') {
							validMainContactFound = true;
						}
					}
				}
				if (!validMainContactFound) {
					return false;
				}
			} else {
				return false;
			}
			return true;
		} else {
			return false;
		}

		return success;
	}

	/*
	 *  Check complete set of objects for Billing flag
	 */
	public static Boolean checkCompleteBillingFlag(Ban__c ban, Financial_Account__c fa, Billing_Arrangement__c bar, Order__c currentOrder) {
		Boolean financialContactReady = true;
		if (fa != null && fa.Financial_Contact__c != null) {
			if (OrderValidation.checkObject(fa.Financial_Contact__r, currentOrder) != '') {
				financialContactReady = false;
			}
		}

		Boolean success = false;
		if (OrderValidation.checkObject(ban, currentOrder) == '')
			if (OrderValidation.checkObject(fa, currentOrder) == '')
				if (financialContactReady)
					if (OrderValidation.checkObject(bar, currentOrder) == '')
						success = true;

		return success;
	}

	public static DeliveryWrapper checkCompleteDeliveryWrapper(DeliveryWrapper dw, Order__c currentOrder) {
		// Check sites and for invalid sites, remove the relevant order(s) from the orderId set
		system.debug(dw.readyForOrder);
		dw.readyForOrder = true;
		if (OrderValidation.checkObject(dw.site, currentOrder) != '') {
			dw.readyForOrder = false;
			return dw;
		}
		// check pbxs
		for (Competitor_Asset__c ca : dw.pbxs) {
			if (OrderValidation.checkObject(ca, currentOrder) != '') {
				dw.readyForOrder = false;
				return dw;
			}
		}
		// check overflow pbxs
		for (Competitor_Asset__c ca : dw.overflowPbxs) {
			if (OrderValidation.checkObject(ca, currentOrder) != '') {
				dw.readyForOrder = false;
				return dw;
			}
		}

		// check cpes
		for (DeliveryWrapper.ContractedProductWrapper cpw : dw.allCpes) {
			if (OrderValidation.checkObject(cpw.cp, currentOrder) != '') {
				dw.readyForOrder = false;
				return dw;
			}
		}
		// check links
		for (DeliveryWrapper.ContractedProductWrapper cpw : dw.allLinks) {
			if (OrderValidation.checkObject(cpw.cp, currentOrder) != '') {
				dw.readyForOrder = false;
				return dw;
			}
		}
		// check carriers
		for (DeliveryWrapper.ContractedProductWrapper cpw : dw.allCarriers) {
			if (OrderValidation.checkObject(cpw.cp, currentOrder) != '') {
				dw.readyForOrder = false;
				return dw;
			}
		}
		// check phones
		for (Contracted_Products__c cp : dw.phones) {
			if (OrderValidation.checkObject(cp, currentOrder) != '') {
				dw.readyForOrder = false;
				return dw;
			}
		}
		// check ersArticles
		for (Contracted_Products__c cp : dw.ersArticles) {
			if (OrderValidation.checkObject(cp, currentOrder) != '') {
				dw.readyForOrder = false;
				return dw;
			}
		}
		// check legacyArticles
		for (Contracted_Products__c cp : dw.legacyArticles) {
			if (OrderValidation.checkObject(cp, currentOrder) != '') {
				dw.readyForOrder = false;
				return dw;
			}
		}

		// check additional articles
		for (Contracted_Products__c cp : dw.additionalLocationArticles) {
			if (OrderValidation.checkObject(cp, currentOrder) != '') {
				dw.readyForOrder = false;
				return dw;
			}
		}

		// do some hardcoded 'special' checks that span multiple articles in 1 delivery
		try {
			dw.checkDelivery();
		} catch (ExMissingDataException ide) {
			dw.readyForOrder = false;
			Apexpages.addMessages(ide);
			return dw;
		}

		return dw;
	}

	/*
	 *  Check complete opportunity
	 */
	public static String checkCompleteOpportunities(List<Opportunity> opps, Set<Id> oppIdsToCheck) {
		/** Function extented to be used n the API's, api failed when adding errormessages to records */
		return OrderValidation.checkCompleteOpportunitiesExt(opps, oppIdsToCheck, true, null);
	}

	public static String checkCompleteOpportunitiesExt(
		List<Opportunity> opps,
		Set<Id> oppIdsToCheck,
		Boolean addErrorsToRecords,
		Map<Id, Opportunity> newOppMap
	) {
		String errorsFound = '';
		Boolean triggerContext = false;

		// query additional lineitem details
		Map<Id, List<OpportunityLineItem>> oppIdToOppLineItems = new Map<Id, List<OpportunityLineItem>>();
		for (OpportunityLineItem oli : [
			SELECT Id, OpportunityId, PriceBookEntry.Product2.Role__c, Proposition__c, Fixed_Mobile__c
			FROM OpportunityLineItem
			WHERE OpportunityId IN :oppIdsToCheck
		]) {
			if (!oppIdToOppLineItems.containsKey(oli.OpportunityId)) {
				oppIdToOppLineItems.put(oli.OpportunityId, new List<OpportunityLineItem>{ oli });
			} else {
				oppIdToOppLineItems.get(oli.OpportunityId).add(oli);
			}
		}

		// requery if the check was triggered by the opp trigger (as not all relationship fields are filled then)
		if (opps == null) {
			opps = OpportunityClosedWonController.fetchOppList(oppIdsToCheck);
			triggerContext = true;
		}

		for (Opportunity o : opps) {
			if (oppIdsToCheck.contains(o.Id)) {
				// generate a dummy order , as in Opp stage we don't have an order yet
				Order__c dummyOrder = new Order__c();
				dummyOrder.Ready_for_Inside_Sales__c = false;
				dummyOrder.IsPartner__c = (o.Partner_Account__c != null ? true : false);
				dummyOrder.Export__c = null;
				// define propositions contained in opp
				Set<String> propositions = new Set<String>();
				if (o.RecordTypeId == GeneralUtils.recordTypeMap.get('Opportunity').get('MAC')) {
					propositions.add('Legacy');
				} else {
					if (oppIdToOppLineItems.containsKey(o.Id)) {
						for (OpportunityLineItem oli : oppIdToOppLineItems.get(o.Id)) {
							if (oli.PriceBookEntry.Product2.Role__c == 'ERS') {
								propositions.add('ERS');
							} else if (oli.PriceBookEntry.Product2.Role__c == 'Numberporting') {
								propositions.add('Numberporting');
							} else if (oli.Proposition__c != null) {
								propositions.addAll(oli.Proposition__c.split('\\+'));
							} else {
								// if the opp is not mac and the oli has no proposition, assume it's a mobile article
								propositions.add('Mobile');
							}
						}
					} else {
						// how to handle empty propositions?
						propositions.add('Legacy');
					}
				}
				String propString = '';
				for (String s : propositions)
					propString += s + ';';
				dummyOrder.Propositions__c = propString;
				String errors = OrderValidation.checkObject(o, dummyOrder);
				system.debug('Vreuls Errors:' + errors);
				if (errors.length() > 0) {
					if (addErrorsToRecords) {
						if (!triggerContext) {
							o.addError(errors);
						} else {
							// JvW 06-11-2019: When invoked from a trigger context, use trigger.newMap
							if (newOppMap != null && newOppMap.containsKey(o.Id))
								newOppMap.get(o.Id).addError(errors);
						}
					}
					errorsFound += errors;
				}
			}
		}
		return errorsFound;
	}
}
