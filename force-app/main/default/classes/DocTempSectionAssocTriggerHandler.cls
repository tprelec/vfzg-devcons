/**
 * @description       : Apex Class Handler of the Document Template / Section Association Trigger
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

public class DocTempSectionAssocTriggerHandler extends TriggerHandler {
	private List<csclm__Document_Template_Section_Association__c> lstNewDocumentTemplateSectionAssociation;

	private Map<Id, csclm__Document_Template_Section_Association__c> mapOldDocumentTemplateSectionAssociation;
	private Map<Id, csclm__Document_Template_Section_Association__c> mapNewDocumentTemplateSectionAssociation;

	/**
	 * @description Initialize Class Variables
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	private void init() {
		lstNewDocumentTemplateSectionAssociation = (List<csclm__Document_Template_Section_Association__c>) this.newList;
		mapOldDocumentTemplateSectionAssociation = (Map<Id, csclm__Document_Template_Section_Association__c>) this.oldMap;
		mapNewDocumentTemplateSectionAssociation = (Map<Id, csclm__Document_Template_Section_Association__c>) this.newMap;
	}

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public void setExternalIds() {
		Sequence objSequence = new Sequence('Document Template/Section Ass');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (csclm__Document_Template_Section_Association__c objDocumentTemplateSectionAssociation : lstNewDocumentTemplateSectionAssociation) {
				objDocumentTemplateSectionAssociation.ExternalID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
