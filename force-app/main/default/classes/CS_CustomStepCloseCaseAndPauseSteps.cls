global without sharing class CS_CustomStepCloseCaseAndPauseSteps implements CSPOFA.ExecutionHandler{
    public static String INSTALL_SOLUTION_CASE_CLOSED_RESCHEDULED = 'Closed - Planning Rescheduled';

    global List<SObject> process(List<SObject> steps) {
        List<SObject> result = new List<sObject>();
        Map<Id, CSPOFA__Orchestration_Step__c> processStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
        Map<Id, CSPOFA__Orchestration_Step__c> rootProcessStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
        Map<String,CSPOFA__Orchestration_Step__c> casesRelatedToSteps = new Map<String,CSPOFA__Orchestration_Step__c>();
        List<CSPOFA__Orchestration_Step__c> stepList = [SELECT ID,
                Name,
                CSPOFA__Orchestration_Process__c,
                CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c,
                CSPOFA__Status__c,
                CSPOFA__Completed_Date__c,
                CSPOFA__Message__c,
                CSPOFA__Expression_Result__c,
                CSPOFA__Related_Object__c,
                CSPOFA__Related_Object_ID__c,
                COM_Case_Status__c
        FROM CSPOFA__Orchestration_Step__c
        WHERE Id IN :steps];

        try {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                processStepMap.put(step.CSPOFA__Orchestration_Process__c, step);
                rootProcessStepMap.put(step.CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c, step);
            }

            Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c>([
                    SELECT Id,
                            Suborder__c,
                            CSPOFA__Root_Process__c
                    FROM CSPOFA__Orchestration_Process__c
                    WHERE Id IN :processStepMap.keySet()
                    OR CSPOFA__Root_Process__c IN :rootProcessStepMap.keySet()
            ]);

            casesRelatedToSteps.putAll(getRelatedObjectStepMap(processesMap));

            List<Case> cases = [SELECT Id,
                                    Subject,
                                    Case_Record_Type_Text__c
                                        FROM Case
                                        WHERE (Case_Record_Type_Text__c IN :getCaseRecordTypes(true))
                                        AND IsClosed = FALSE
                                        AND Id IN :casesRelatedToSteps.keySet()];

            updateCasesAndPauseSteps(cases, casesRelatedToSteps, rootProcessStepMap);

            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_COMPLETE;
                step.CSPOFA__Completed_Date__c = Date.today();
                step.CSPOFA__Message__c = 'Custom step succeeded';
                result.add(step);
            }
        } catch (Exception ex) {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
                step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
                if(step.CSPOFA__Message__c.length() > 255 ){
                    step.CSPOFA__Message__c = step.CSPOFA__Message__c.substring(0, 254);
                }
                result.add(step);
            }
        }
        return result;
    }

    private Map<String,CSPOFA__Orchestration_Step__c> getRelatedObjectStepMap(Map<Id,CSPOFA__Orchestration_Process__c> processMap){
        Map<String,CSPOFA__Orchestration_Step__c> returnValue = new Map<String,CSPOFA__Orchestration_Step__c>();

        List<CSPOFA__Orchestration_Step__c> stepList = [SELECT Id,
                                                        Name,
                                                        CSPOFA__Related_Object_ID__c,
                                                        CSPOFA__Case_Record_Type__c,
                                                        CSPOFA__Orchestration_Process__c,
                                                        CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c
                                                        FROM CSPOFA__Orchestration_Step__c
                                                        WHERE CSPOFA__Orchestration_Process__c IN :processMap.keySet()
                                                        AND CSPOFA__Status__c IN :getStepActiveStatuses()
                                                        AND CSPOFA__Case_Record_Type__c IN :getCaseRecordTypes(false)];

        for (CSPOFA__Orchestration_Step__c step :stepList) {
            returnValue.put(step.CSPOFA__Related_Object_ID__c, step);
        }
        return returnValue;
    }

    private void updateSteps(Set<CSPOFA__Orchestration_Step__c> stepSet){
        List<CSPOFA__Orchestration_Step__c> stepsToUpdate = new List<CSPOFA__Orchestration_Step__c>();
        stepsToUpdate.addAll(stepSet);
        update stepsToUpdate;
    }

    private void updateCases(List<Case> casesToBeUpdated){
        Set<Case> caseSet = new Set<Case>();
        caseSet.addAll(casesToBeUpdated);
        List<Case> updateCaseList = new List<Case>();
        updateCaseList.addAll(caseSet);
        update updateCaseList;
    }

    private Set<String> getStepActiveStatuses(){
        Set<String> returnValue = new Set<String>();
        returnValue.add('In Progress');
        returnValue.add('Waiting For Feedback');

        return returnValue;
    }

    private Set<String> getCaseRecordTypes(Boolean withUnderscore){
        Set<String> returnValue = new Set<String>();

        if (withUnderscore) {
            returnValue.add('CS_COM_Install_Solution_Jeopardy');
            returnValue.add('CS_COM_Install_Solution');
            returnValue.add('CS_COM_Provisioning_Robot');
            returnValue.add('CS_COM_Provisioning_Team');
            returnValue.add('CS_COM_Provisioning_Coordinator');
            returnValue.add('CS_COM_Suspend_Service');
            returnValue.add('CS_COM_Return_Envelopes');
        } else {
            returnValue.add('CS COM Install Solution Jeopardy');
            returnValue.add('CS COM Install Solution');
            returnValue.add('CS COM Provisioning Robot');
            returnValue.add('CS COM Provisioning Team');
            returnValue.add('CS COM Provisioning Coordinator');
            returnValue.add('CS COM Suspend Service');
            returnValue.add('CS COM Return Envelopes');
        }
        return returnValue;
    }

    private Set<String> getCaseRecordTypesForUpdating(String type) {
        Set<String> returnValue = new Set<String>();

        if (type.equalsIgnoreCase('Closed - Planning Rescheduled')) {
            returnValue.add('CS_COM_Install_Solution_Jeopardy');
            returnValue.add('CS_COM_Install_Solution');
        } else if (type.equalsIgnoreCase('Closed - Solution Delivery Cancelled')) {
            returnValue.addAll(getCaseRecordTypes(true));
        }
        return returnValue;
    }

    private void updateCasesAndPauseSteps(List<Case> cases, Map<String,CSPOFA__Orchestration_Step__c> casesRelatedToSteps, Map<Id, CSPOFA__Orchestration_Step__c> rootProcessStepMap){
        List<Case> casesToBeUpdated = new List<Case>();
        Set<CSPOFA__Orchestration_Step__c> stepsToBePutOnHold = new Set<CSPOFA__Orchestration_Step__c>();

        for (Case caseRecord : cases) {
            if ((casesRelatedToSteps.get(caseRecord.Id) != null)
                    && (rootProcessStepMap.get(casesRelatedToSteps.get(caseRecord.Id).CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c).COM_Case_Status__c == 'Closed - Planning Rescheduled')
                    && (getCaseRecordTypesForUpdating('Closed - Planning Rescheduled').contains(caseRecord.Case_Record_Type_Text__c))) {
                        stepsToBePutOnHold.add(getStepForOnHold(casesRelatedToSteps.get(caseRecord.Id).Id,true));
                        casesToBeUpdated.add(getCasesForClosing(caseRecord.Id,rootProcessStepMap.get(casesRelatedToSteps.get(caseRecord.Id).CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c).COM_Case_Status__c));
                } else if ((casesRelatedToSteps.get(caseRecord.Id) != null)
                    && (rootProcessStepMap.get(casesRelatedToSteps.get(caseRecord.Id).CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c).COM_Case_Status__c == 'Closed - Solution Delivery Cancelled')
                    && (getCaseRecordTypesForUpdating('Closed - Solution Delivery Cancelled').contains(caseRecord.Case_Record_Type_Text__c))){
                        stepsToBePutOnHold.add(getStepForOnHold(casesRelatedToSteps.get(caseRecord.Id).Id,true));
                        casesToBeUpdated.add(getCasesForClosing(caseRecord.Id,rootProcessStepMap.get(casesRelatedToSteps.get(caseRecord.Id).CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c).COM_Case_Status__c));
            }
        }

        if (!stepsToBePutOnHold.isEmpty()) {
            updateSteps(stepsToBePutOnHold);
        }

        if (!casesToBeUpdated.isEmpty()) {
            updateCases(casesToBeUpdated);
        }

        for (CSPOFA__Orchestration_Step__c step :stepsToBePutOnHold){
            step.CSPOFA__Step_On_Hold__c = false;
        }

        if (!stepsToBePutOnHold.isEmpty()) {
            updateSteps(stepsToBePutOnHold);
        }
    }

    private CSPOFA__Orchestration_Step__c getStepForOnHold(Id stepId, Boolean onHold){
        CSPOFA__Orchestration_Step__c returnValue = new CSPOFA__Orchestration_Step__c();
        returnValue.Id = stepId;
        returnValue.CSPOFA__Step_On_Hold__c = onHold;
        returnValue.CSPOFA__Related_Object_ID__c = '';
        return returnValue;
    }

    private Case getCasesForClosing(Id caseId, String status){
        Case returnValue = new Case();
        returnValue.Id = caseId;
        returnValue.CSPOFA__Orchestration_Step__c = null;
        returnValue.Status = status;
        return returnValue;
    }
}