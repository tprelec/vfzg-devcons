@SuppressWarnings('PMD')
@isTest
private class TestUserDeactivationController {
	@testSetup
	static void setup() {
		// Setup data for test
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		Account partnerAccount = TestUtils.createPartnerAccount('ABC');
		Account partnerAccount2 = TestUtils.createPartnerAccount('DEF');
		User portalUser = TestUtils.createPortalUser(partnerAccount);
		User portalUser2 = TestUtils.createPortalUser(partnerAccount2);

		list<Contact> updateContacts = new List<Contact>();
		Contact temp1 = new contact(
			Id = portalUser.ContactId,
			UserID__C = portalUser.Id,
			Replacement_User__c = portalUser2.Id
		);
		updateContacts.add(temp1);
		Contact temp2 = new contact(
			Id = portalUser2.ContactId,
			UserID__C = portalUser2.Id,
			Replacement_User__c = portalUser.Id
		);
		updateContacts.add(temp2);
		update updateContacts;

		Dealer_Information__c di = TestUtils.createDealerInformation(portalUser.ContactId);
		Dealer_Information__c di2 = TestUtils.createDealerInformation(portalUser2.ContactId);
		Postal_Code_Assignment__c pca = TestUtils.createPCA('1234', di.Id);
		Postal_Code_Assignment__c pca2 = TestUtils.createPCA('5678', di2.Id);
	}

	@isTest
	static void testConstructorWithCurentUser() {
		PageReference deactivationForm = page.UserDeactivation;
		Test.setCurrentPage(deactivationForm);
		ApexPages.currentPage().getParameters().put('Id', GeneralUtils.currentUser.Id);
		UserDeactivationController controller = new UserDeactivationController();	
		System.assertEquals(true, controller.errorFound, 'User cannot be deactivated because Replacement User is not selected.');
	}

	@isTest
	static void testDeactivationUserWithWrongContactId() {
		
		List<Contact> testData = [SELECT Id, Name, UserID__C, Replacement_User__c FROM Contact ];
		List<User> users = [SELECT Id FROM User WHERE AccountId != null AND ContactId !=: testData[0].Id];
		PageReference deactivationForm = page.UserDeactivation;
		Test.setCurrentPage(deactivationForm);
		ApexPages.currentPage().getParameters().put('Id',users[0].Id);
		// Id is hardcoding because we need it to test the parameter and for the query to give us back an empty list
		ApexPages.currentPage().getParameters().put('contactId', '0035r00000FPRxNAAX'); 
		UserDeactivationController controller = new UserDeactivationController();
		System.assertEquals(controller.errorFound, true, 'User cannot be deactivated because User is not synced  with a Contact.');
	} 

	@isTest
	static void testDeactivateUsert() {
		Test.startTest();
		List<Contact> testData = [SELECT Id, Name, UserID__C, Replacement_User__c FROM Contact];

		PageReference deactivationForm = page.UserDeactivation;
		Test.setCurrentPage(deactivationForm);
		ApexPages.currentPage().getParameters().put('Id', testData[0].UserID__C);
		ApexPages.currentPage().getParameters().put('contactId', testData[0].Id);
		UserDeactivationController controller = new UserDeactivationController();
		controller.userContact = testData[1];
		PageReference p = controller.deactivateUser();
		System.assertEquals(
			String.valueOf(p).contains(String.valueOf(testData[0].UserID__C)),
			true,
			'User is deactivated'
		);
		Test.stopTest();
	}

	@isTest
	static void withReplacement() {
		// Start test
		Test.startTest();
		List<Contact> testData = [SELECT Id, Name, UserID__C, Replacement_User__c FROM Contact];

		PageReference deactivationForm = page.UserDeactivation;
		Test.setCurrentPage(deactivationForm);
		ApexPages.currentPage().getParameters().put('Id', testData[0].UserID__C);
		ApexPages.currentPage().getParameters().put('contactId', testData[0].Id);

		// Create an instance of the controller
		UserDeactivationController controller = new UserDeactivationController();
		controller.userContact = testData[1];
		Boolean recordsFound = controller.noRecordsFound;

		User owner = [SELECT Id FROM User WHERE Id = :testData[0].UserID__C];
		Account acct = TestUtils.createAccount(owner);

		acct.Type = 'Dealer';
		update acct;

		AccountTeamMember accTM = new AccountTeamMember(
			AccountId = acct.Id,
			UserId = owner.Id,
			TeamMemberRole = 'Solution Specialist'
		);
		insert accTM;

		// Call controller methods
		Boolean b = controller.getIsPartnerUser();
		List<Dealer_Information__c> diList = controller.dealerInformations;
		List<Account> accList = controller.ownedAccounts;
		List<AccountTeamMember> atm = controller.accountTeamMembers;
		List<Opportunity> oppList = controller.openOpportunities;
		List<Lead> leadList = controller.openLeads;
		List<User> subList = controller.subordinates;
		List<User> primList = controller.primaryPartners;
		List<User> adminList = controller.adminPartners;
		List<Dashboard> dashList = controller.runningUserDashboards;
		List<VF_Contract__c> contractsVFList = controller.contractsVF;
		List<Postal_Code_Assignment__c> pcaList = controller.postalCodeAssignments;
		List<ProcessInstanceWorkitem> papList = controller.pendingApprovalProcesses;
		List<CreditNote_Approvals__c> cnaList = controller.creditNoteApprovals;

		PageReference p = controller.updateAllUsers();

		recordsFound = controller.noRecordsFound;

		Test.stopTest();

		System.assert(recordsFound, 'Records is found.');
	}

	@isTest
	static void withoutReplacement() {
		// Start test
		Test.startTest();

		List<Contact> testData = [SELECT Id, Name, UserID__C, Replacement_User__c FROM Contact];
		Contact replacementContact = testData[1];

		replacementContact.Replacement_User__c = null;
		update replacementContact;

		PageReference deactivationForm = page.UserDeactivation;
		Test.setCurrentPage(deactivationForm);
		ApexPages.currentPage().getParameters().put('Id', testData[0].UserID__c);
		ApexPages.currentPage().getParameters().put('contactId', testData[0].Id);

		// Create an instance of the controller
		UserDeactivationController controller = new UserDeactivationController();
		controller.userContact = replacementContact;
		Boolean recordsFound = controller.noRecordsFound;

		User owner = [SELECT Id FROM User WHERE Id = :testData[0].UserID__C];
		Account acct = TestUtils.createAccount(owner);

		acct.Type = 'Dealer';
		update acct;

		AccountTeamMember accTM = new AccountTeamMember(
			AccountId = acct.Id,
			UserId = owner.Id,
			TeamMemberRole = 'Solution Specialist'
		);
		insert accTM;

		// Call controller methods
		Boolean b = controller.getIsPartnerUser();
		List<Dealer_Information__c> diList = controller.dealerInformations;
		List<Account> accList = controller.ownedAccounts;
		List<AccountTeamMember> atm = controller.accountTeamMembers;
		List<Opportunity> oppList = controller.openOpportunities;
		List<Lead> leadList = controller.openLeads;
		List<User> subList = controller.subordinates;
		List<User> primList = controller.primaryPartners;
		List<User> adminList = controller.adminPartners;
		List<Dashboard> dashList = controller.runningUserDashboards;
		List<VF_Contract__c> contractsVFList = controller.contractsVF;
		List<Postal_Code_Assignment__c> pcaList = controller.postalCodeAssignments;
		List<ProcessInstanceWorkitem> papList = controller.pendingApprovalProcesses;
		List<CreditNote_Approvals__c> cnaList = controller.creditNoteApprovals;

		PageReference p = controller.updateAllUsers();
		recordsFound = controller.noRecordsFound;

		// Stop test
		Test.stopTest();

		System.assert(!recordsFound, 'Records not found.');
	}

	@isTest
	static void testEmptyData() {
		Test.startTest();
		UserDeactivationController controller = new UserDeactivationController();
		Boolean recordsFound = controller.noRecordsFound;
		Test.stopTest();
		System.assert(!recordsFound, 'Records not found.');
	}
}