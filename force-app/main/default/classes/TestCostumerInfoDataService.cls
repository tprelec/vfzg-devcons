@IsTest
public class TestCostumerInfoDataService {
	@TestSetup
	static void makeData() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.KVK_number__c = '12345678';
		update acc;
		Contact con = TestUtils.createContact(acc);
		con.Email = 'mostUniqueEmailInTheWorld@vodafoneziggo.com';
		con.AccountId = acc.Id;
		update con;
		Site__c site = TestUtils.createSite(acc);
	}

	@IsTest
	static void testGetContactData() {
		Id accId = [SELECT Id FROM Account LIMIT 1].Id;
		List<Contact> cnt = [SELECT Id, AccountId, FirstName, LastName, Gender__c, MobilePhone, Email, Birthdate, Authorized_to_sign__c FROM Contact];
		Contact cnt2 = new Contact(
			Email = 'funky.guy@vodafoneziggo.com',
			LastName = 'Funky',
			AccountId = accId,
			FirstName = 'Guy',
			Authorized_to_sign__c = true,
			Gender__c = 'M'
		);
		List<Contact> contacts = new List<Contact>{ cnt[0], cnt2 };

		System.assertEquals(1, cnt.size(), 'There shuld be only one contact.');

		Test.startTest();
		List<Contact> cnts = CostumerInfoDataService.getContactData(contacts);
		Test.stopTest();

		List<Contact> postCnt = [SELECT Id, Email FROM Contact];
		System.assertEquals(2, postCnt.size(), 'There shuld be now two contacts.');
		System.assert(cnts[0] != null, 'Contact should be created and have an Id.');
		System.assert(cnts[1] != null, 'Contact should be created and have an Id.');
	}

	@IsTest
	static void testGetExistingAccount() {
		Test.startTest();
		Account acc = CostumerInfoDataService.getExistingAccount('12345678');
		Test.stopTest();

		System.assert(acc.Id != null, 'Account should be returned.');
	}

	@IsTest
	static void testGetExistingAccountFailScenario() {
		Test.startTest();
		Account acc = CostumerInfoDataService.getExistingAccount('87654321');
		Test.stopTest();

		System.assert(acc == null, 'No account should be returned.');
	}

	@IsTest
	static void testCreateNewAccount() {
		OlbicoSettings__c oblico = new OlbicoSettings__c();
		oblico.Olbico_JSon_Endpoint__c = 'Fake endpoint';
		oblico.Olbico_Json_Username__c = 'Fake username';
		oblico.Olbico_Json_Password__c = 'Fake password';
		oblico.Olbico_Json_BAG_Streets__c = 'Fake bag street';
		insert oblico;

		Test.startTest();
		Account acc = CostumerInfoDataService.createNewAccount('87654321');
		Test.stopTest();

		System.assert(acc == null, 'No account should be returned.');
	}

	@IsTest
	static void testGetAccountData() {
		OlbicoSettings__c oblico = new OlbicoSettings__c();
		oblico.Olbico_JSon_Endpoint__c = 'Fake endpoint';
		oblico.Olbico_Json_Username__c = 'Fake username';
		oblico.Olbico_Json_Password__c = 'Fake password';
		oblico.Olbico_Json_BAG_Streets__c = 'Fake bag street';
		insert oblico;

		Test.startTest();
		Account acc = CostumerInfoDataService.getAccountData('12345678');
		Account acc2 = CostumerInfoDataService.getAccountData('87654321');
		Test.stopTest();

		System.assert(acc.Id != null, 'Existing account should be returned.');
		System.assert(acc2 == null, 'No account should be returned.');
	}

	@IsTest
	static void testFetchExistingSites() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Site__c st = [SELECT Id, Site_Street__c, Site_Postal_Code__c, Site_House_Number__c, Site_House_Number_Suffix__c FROM Site__c LIMIT 1];

		Map<String, String> siteMap = new Map<String, String>{
			CustomerInfoRestService.POSTCODE => st.Site_Postal_Code__c,
			CustomerInfoRestService.HOUSENUMBER => String.valueOf(st.Site_House_Number__c),
			CustomerInfoRestService.HOUSENRSUFFIX => st.Site_House_Number_Suffix__c
		};

		List<Map<String, String>> siteData = new List<Map<String, String>>{ siteMap };

		Test.startTest();
		List<Site__c> sites = CostumerInfoDataService.fetchExistingSites(acc.Id, siteData);
		Test.stopTest();

		System.assertEquals(acc.Id, sites[0].Site_Account__c, 'Site not associated to correct account.');
		System.assertEquals(st.Site_Street__c, sites[0].Site_Street__c, 'Wrong site street.');
	}

	@IsTest
	static void testCreateSite() {
		Account acc = [SELECT Id, KVK_number__c FROM Account LIMIT 1];
		Site__c st = [SELECT Id, Site_Postal_Code__c, Site_House_Number__c, Site_House_Number_Suffix__c FROM Site__c LIMIT 1];

		Map<String, String> siteMap = new Map<String, String>{
			CustomerInfoRestService.POSTCODE => '1086VE',
			CustomerInfoRestService.HOUSENUMBER => '14',
			CustomerInfoRestService.HOUSENRSUFFIX => '',
			CustomerInfoRestService.SITECITY => 'Amsterdam',
			CustomerInfoRestService.SITESTREET => 'Test Street'
		};

		Test.startTest();
		Site__c site = CostumerInfoDataService.createSite(acc.Id, acc.KVK_number__c, siteMap);
		Test.stopTest();

		System.assertEquals('1086VE', site.Site_Postal_Code__c.trim(), 'Wrong site postal code.');
	}

	@IsTest
	static void testGetSiteData() {
		Account acc = [SELECT Id, KVK_number__c FROM Account LIMIT 1];
		List<Site__c> preSites = [SELECT Id, Site_Street__c, Site_Postal_Code__c, Site_House_Number__c, Site_House_Number_Suffix__c FROM Site__c];

		Map<String, String> siteMap = new Map<String, String>{
			CustomerInfoRestService.POSTCODE => preSites[0].Site_Postal_Code__c,
			CustomerInfoRestService.HOUSENUMBER => String.valueOf(preSites[0].Site_House_Number__c),
			CustomerInfoRestService.HOUSENRSUFFIX => preSites[0].Site_House_Number_Suffix__c
		};

		Map<String, String> siteMap2 = new Map<String, String>{
			CustomerInfoRestService.POSTCODE => '1086VE',
			CustomerInfoRestService.HOUSENUMBER => '14',
			CustomerInfoRestService.HOUSENRSUFFIX => '',
			CustomerInfoRestService.SITECITY => 'Amsterdam',
			CustomerInfoRestService.SITESTREET => 'Test Street'
		};
		List<Map<String, String>> siteData = new List<Map<String, String>>{ siteMap, siteMap2 };

		System.assertEquals(1, preSites.size(), 'There should be only one site in the system.');

		Test.startTest();
		List<Site__c> sites = CostumerInfoDataService.getSiteData(acc.Id, siteData);
		Test.stopTest();

		System.assertEquals(2, sites.size(), 'There should be two sites in the system now.');
	}
}
