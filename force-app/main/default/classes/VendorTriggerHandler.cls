public with sharing class VendorTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<Vendor__c> newProducts = (List<Vendor__c>) this.newList;
		Sequence seq = new Sequence('Vendor');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (Vendor__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
