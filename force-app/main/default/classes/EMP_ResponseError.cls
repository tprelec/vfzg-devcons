public class EMP_ResponseError {
    public class Error {
		public String code {get;set;} 
		public String message {get;set;} 

		public Error(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
						switch on text {
							when 'code' {
								code = parser.getText();
							}
							when 'message' {
								message = parser.getText();
							}
							when else {
								System.debug(LoggingLevel.WARN, 'Error consuming unrecognized property: '+text);
							}
						}
					}
				}
			}
		}
	}

	public static List<Error> arrayOfError(System.JSONParser p) {
        List<Error> res = new List<Error>();
        if (p.getCurrentToken() == null) {
            p.nextToken();
        }
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Error(p));
        }
        return res;
    }
}