global with sharing class CS_BusinessMobileLookup extends cscfga.ALookupSearch {
	public override Object[] doLookupSearch(
		Map<String, String> searchFields,
		String productDefinitionID,
		Id[] excludeIds,
		Integer pageOffset,
		Integer pageLimit
	) {
		List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>();
		Decimal contractTerm = Decimal.valueOf(searchFields.get('ContractTerm'));
		String mobileScenario = searchFields.get('MobileScenario');
		string salesChannel = searchfields.get('SalesChannel');
		Date today = Date.valueOf(searchFields.get('Today'));

		System.debug(
			Logginglevel.DEBUG,
			'Input parameters for query: ' +
			' contractTerm => ' +
			contractTerm +
			' mobileScenario => ' +
			mobileScenario +
			' salesChannel => ' +
			salesChannel +
			' today => ' +
			today
		);

		priceItems = [
			SELECT
				Name,
				Id,
				Vendor_lookup__r.Name,
				cspmb__Contract_Term__c,
				cspmb__recurring_charge__c,
				cspmb__One_Off_Charge__c,
				Contract_Term_Number__c,
				Available_bandwidth_down__c,
				Available_bandwidth_up__c,
				Recurring_Charge_Product__r.Taxonomy__r.Portfolio__c,
				One_Off_Charge_Product__r.Unify_Charge_Description__c,
				One_Off_Charge_Product__r.Unify_Charge_Code__c,
				One_Off_Charge_Product__r.ProductCode,
				One_Off_Charge_Product__r.Taxonomy__r.Name,
				One_Off_Charge_Product__r.Taxonomy__r.Portfolio__c,
				One_Off_Charge_Product__r.Taxonomy__r.Product_Family__c,
				Recurring_Charge_Product__r.Unify_Charge_Description__c,
				Recurring_Charge_Product__r.Unify_Charge_Code__c,
				Recurring_Charge_Product__r.ProductCode,
				Recurring_Charge_Product__r.Taxonomy__r.Name,
				Recurring_Charge_Product__r.Taxonomy__r.Product_Family__c,
				cspmb__Is_Active__c,
				Category__c,
				VFZ_Variation_Tier_Name__c,
				cspmb__Effective_End_Date__c,
				cspmb__Effective_Start_Date__c,
				cspmb__Master_Price_item__c,
				cspmb__Recurring_Charge_External_Id__c,
				Recurring_Charge_Product__c,
				One_Off_Charge_Product__r.Name,
				LG_UploadSpeed__c,
				VFZ_Commercial_Article_Name__c,
				LG_DownloadSpeed__c,
				VFZ_Commercial_Component_Name__c,
				LG_ProductFamily__c,
				Recurring_Charge_Product__r.Name,
				cspmb__One_Off_Charge_External_Id__c,
				cspmb__Product_Definition_Name__c,
				One_Off_Charge_Product__c,
				Mobile_Add_on_category__c,
				cspmb__One_Off_Cost__c,
				One_Off_Charge_Product__r.ThresholdGroup__c,
				Mobile_Scenario__c,
				cspmb__Recurring_Cost__c,
				Recurring_Charge_Product__r.ThresholdGroup__c,
				Group__c,
				cspmb__Price_Item_Code__c
			FROM cspmb__Price_Item__c
			WHERE
				cspmb__Is_Active__c = TRUE
				AND cspmb__Effective_End_Date__c >= :today
				AND cspmb__Effective_Start_Date__c <= :today
				AND cspmb__Master_Price_item__c != ''
				AND cspmb__Product_Definition_Name__c = 'Business Mobile'
				/*AND Mobile_Add_on_category__c = '*Subscription*'*/
				AND Min_Duration__c <= :contractTerm
				AND Max_Duration__c > :contractTerm
				AND Sales_Channel__c INCLUDES (:salesChannel)
				AND (Mobile_Scenario__c = :mobileScenario
				OR Mobile_Scenario__c = 'Business Mobile OneMonth')
			ORDER BY Category__c ASC, External_ID__c ASC, VFZ_Variation_Tier_Name__c DESC
		];
		System.debug(Logginglevel.DEBUG, 'Number of priceItems: ' + priceItems.size());
		return priceItems;
	}

	public override String getRequiredAttributes() {
		return '["ContractTerm","MobileScenario","SalesChannel","Today"]';
	}
}
