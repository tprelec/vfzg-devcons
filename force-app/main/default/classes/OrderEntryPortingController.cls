public with sharing class OrderEntryPortingController {
	/**
	 * @description Update porting numbers for telephony products
	 * @param  oppId Opportunity Id
	 */
	@AuraEnabled
	public static void saveFixedPortingNumbers(Id oppId) {
		deleteOldPortings(oppId);
		OrderEntryData data = OrderEntryController.getOrderEntryData(oppId);
		List<LG_PortingNumber__c> nums = new List<LG_PortingNumber__c>();
		for (OrderEntryData.OrderEntryBundle bundle : data.bundles) {
			nums.addAll(createBundlePortings(oppId, bundle));
		}
		if (!nums.isEmpty()) {
			insert nums;
		}
	}

	/**
	 * @description Deletes existing Porting Number records
	 * @param  oppId Opportunity Id
	 */
	private static void deleteOldPortings(Id oppId) {
		List<LG_PortingNumber__c> current = [SELECT Id FROM LG_PortingNumber__c WHERE LG_Opportunity__c = :oppId];
		if (!current.isEmpty()) {
			delete current;
		}
	}

	/**
	 * @description Creates Porting Records for single Bundle
	 * @param  oppId Opportunity Id
	 * @param  bundle Order Entry Bundle
	 * @return        List of Porting Records
	 */
	private static List<LG_PortingNumber__c> createBundlePortings(Id oppId, OrderEntryData.OrderEntryBundle bundle) {
		Map<String, OrderEntryData.Telephony> telephony = bundle.telephony;
		List<LG_PortingNumber__c> nums = new List<LG_PortingNumber__c>();
		if (telephony != null && telephony.values().size() > 0) {
			for (OrderEntryData.Telephony tel : telephony.values()) {
				if (tel.portingEnabled != null && tel.portingEnabled && tel.portingNumber != null) {
					nums.add(
						new LG_PortingNumber__c(LG_Opportunity__c = oppId, LG_PhoneNumber__c = tel.portingNumber, LG_Type__c = 'SOHO Telephony')
					);
				}
			}
		}
		return nums;
	}

	/**
	 * @description Update Porting Number
	 * @param  opportunityId Opportunity Id
	 */
	@AuraEnabled
	public static void updatePortingNumber(Id opportunityId) {
		OrderEntryData data = OrderEntryController.getOrderEntryData(opportunityId);
		deleteOldPortings(opportunityId);

		List<LG_PortingNumber__c> portingNumbers = new List<LG_PortingNumber__c>();
		LG_PortingNumber__c portingNumber;

		for (OrderEntryData.OrderEntryBundle b : data.bundles) {
			if (b.porting != null) {
				portingNumber = new LG_PortingNumber__c();
				portingNumber.LG_Opportunity__c = opportunityId;
				portingNumber.LG_ContractType__c = b.porting.type;
				portingNumber.LG_SimType__c = b.porting.currentSimType;
				portingNumber.LG_CustomerNumber__c = b.porting.contractNumber;
				portingNumber.LG_PhoneNumber__c = b.porting.mobileNumber;
				portingNumber.LG_ContractEndDate__c = b.porting.contractEndDate;
				portingNumbers.add(portingNumber);
			}
		}
		insert portingNumbers;
	}
}
