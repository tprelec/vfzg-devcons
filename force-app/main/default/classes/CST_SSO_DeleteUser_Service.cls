@RestResource(urlMapping='/DisableUser')
global without sharing class CST_SSO_DeleteUser_Service {
	public CST_SSO_DeleteUser_Service() {
	}

	public class UserInfo {
		public String username;
		public String referenceId;
		public Contact contacts;
	}
	@future
	public static void deleteContact(String refId) {
		List<Contact> ct = [SELECT id, Unify_Ref_Id__c FROM contact WHERE Unify_Ref_Id__c = :refId];
		delete ct;
	}
	@HttpPatch
	global static String disableUser() {
		String referenceId;

		String responseString = '';

		try {
			RestRequest req = RestContext.request;
			RestResponse res = RestContext.response;

			String reqBody = req.requestBody.toString();

			List<User> usr;
			List<contact> ctList;

			Boolean inputValid = false;
			Boolean userFound = false;
			Boolean contactFound = false;
			String contactName;

			if (reqBody != null) {
				referenceId = parseResponseDom(reqBody);

				if (referenceId != null && referenceId != '') {
					inputValid = true;
					ctList = [SELECT id, Name, Unify_Ref_Id__c FROM contact WHERE Unify_Ref_Id__c = :referenceId];

					if (ctList != null && !ctList.isEmpty()) {
						contactFound = true;
						contactName = ctList[0].Name;

						usr = [SELECT id, contactid, isActive, IsPortalEnabled, username FROM user WHERE contactid = :ctList[0].id LIMIT 1];

						if (usr != null && !usr.isEmpty()) {
							String ContactId = usr[0].ContactId;
							userFound = true;
							if (usr[0].IsPortalEnabled != null && usr[0].IsPortalEnabled == true)
								usr[0].IsPortalEnabled = false;

							if (usr[0].isActive == true)
								usr[0].isActive = false;

							update usr[0];
						}
						if (ctList.size() > 0) {
							if (!Test.isRunningTest())
								deleteContact(referenceId);
						}
					}
				}
			}

			if (inputValid == true) {
				if (contactFound == true) {
					responseString = 'We found the requested contact. ';

					if (userFound == true) {
						responseString = usr[0].username + ' was disabled successfully. ';
					} else {
						responseString = 'The contact was disabled successfully. ';
					}
				} else {
					// contactFound == false
					responseString = 'referenceId ' + referenceId + ' not found. ';
				}
			} else {
				// inputValid == false
				responseString = 'Your request cannot be processed.';
			}
		} catch (CalloutException e) {
			System.debug('CalloutException  ->' + e.getMessage());
			responseString = 'There was an error with this webservice';
		} catch (SObjectException e) {
			System.debug('SObjectException ->' + e.getMessage());
			responseString = 'There was an error processing this object';
		} catch (System.DmlException e) {
			System.debug('DmlException ->' + e.getMessage());
			responseString = 'There was an error processing this record';
		} catch (Exception e) {
			//something else happened
			System.debug('Exception ->' + e.getMessage());
			responseString = 'An unspecified error occured';
		}

		return responseString;
	}

	public static String parseResponseDom(String url) {
		if (url == null)
			return '';

		System.debug('url: ' + url);
		Dom.Document doc = new Dom.Document();
		doc.load(url);
		if (doc == null)
			return '';

		System.debug('doc: ' + doc);

		Dom.XMLNode rootNode = doc.getRootElement();
		System.debug('rootNode: ' + rootNode);

		if (rootNode == null)
			return '';

		Map<String, UserInfo> usrMap = new Map<String, UserInfo>();

		Dom.XMLNode body = rootNode.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/');
		System.debug('body: ' + body);
		if (body == null)
			return '';

		Dom.XMLNode deleteUsersRequest = body.getChildElement('deleteUsersRequest', 'http://sb.ecs.vodafone.nl/servicebus/');
		System.debug('deleteUsersRequest: ' + deleteUsersRequest);
		if (deleteUsersRequest == null)
			return '';

		Dom.XMLNode userx = deleteUsersRequest.getChildElement('user', null);
		System.debug('userx: ' + userx);
		if (userx == null)
			return '';

		String reqXMLID = userx.getChildElement('referenceId', null).getText();
		System.debug('reqXMLID : ' + reqXMLID);
		if (reqXMLID == null)
			return '';

		return reqXMLID;
	}
}
