public with sharing class CS_COMCancelSuborderController {
    @AuraEnabled
    public static String getSuborderList(String requestJson){
        RequestWrap request = (RequestWrap) JSON.deserialize(requestJson, RequestWrap.class);
        List<Suborder__c> suborders = [
            SELECT Id, Name, Status__c, Customer_Signed_Off__c, PONR_Reached__c
            FROM Suborder__c
            WHERE Order__c = :request.recordId
        ];

        SuborderListWrap response = new SuborderListWrap(suborders);
        return JSON.serialize(response);
    }
    
    @AuraEnabled
    public static String cancelSuborder(String requestJson){
        Boolean result = false;
        RequestListWrap request = (RequestListWrap) JSON.deserialize(requestJson, RequestListWrap.class);
        List<Suborder__c> subordersToUpdate = new List<Suborder__c>();
        List<Suborder__c> suborders = [SELECT Id,
                                                Status__c
                                                FROM Suborder__c
                                                WHERE Id in :request.suborderList];

        for (Suborder__c suborderRecord :suborders) {
            subordersToUpdate.add(new Suborder__c(
                    Id = suborderRecord.Id,
                    Status__c = 'Cancellation Requested'
                ));
        }

        if (!subordersToUpdate.isEmpty()) {
            update subordersToUpdate;
            result = true;
        }

        ResponseWrap response = new ResponseWrap();
        response.suborderCancelled = result;
        return JSON.serialize(response);
    }

    public class SuborderListWrap {
        public List<Suborder__c> suborderList;
        public SuborderListWrap(List<Suborder__c> subList) {
            suborderList = subList;
        }
    }

    public class ResponseWrap {
        public Boolean suborderCancelled { get; set; }
    }

    public class RequestWrap {
        public String recordId { get; set; }
    }
    
    public class RequestListWrap {
        public List<Suborder__c> suborderList { get; set; }
    }
}