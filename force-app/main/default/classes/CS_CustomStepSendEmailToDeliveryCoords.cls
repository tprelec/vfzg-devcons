global without sharing class CS_CustomStepSendEmailToDeliveryCoords implements CSPOFA.ExecutionHandler {
	private static OrgWideEmailAddress orgWideEmailAddress;
	public static final String COM_CUSTOM_PERMISSION = 'COM_Delivery_Planning_Write';
	public static final String COM_DELIVERY_COORDINATOR_EMAIL_TEMPLATE = 'CS COM Delivery Order Cancellation For Delivery Coordinators';

	global List<SObject> process(List<SObject> param0) {
		try {
			orgWideEmailAddress = EmailService.getDefaultOrgWideEmailAddress();

			List<CSPOFA__Orchestration_Step__c> stepList = [
				SELECT
					ID,
					Name,
					CSPOFA__Orchestration_Process__c,
					CSPOFA__Status__c,
					CSPOFA__Completed_Date__c,
					CSPOFA__Message__c,
					CSPOFA__Expression_Result__c,
					CS_Email_From__c,
					CS_Email_To__c,
					CS_Email_Template__c
				FROM CSPOFA__Orchestration_Step__c
				WHERE Id IN :param0
			];

			return processSteps(stepList);
		} catch (Exception ex) {
			List<CSPOFA__Orchestration_Step__c> steps = (List<CSPOFA__Orchestration_Step__c>) param0;
			for (CSPOFA__Orchestration_Step__c step : steps) {
				step = CS_OrchestratorStepUtility.setStepToError(step, ex);
			}
			return steps;
		}
	}

	private static List<SObject> processSteps(List<CSPOFA__Orchestration_Step__c> steps) {
		List<Messaging.SingleEmailMessage> emailMessages = new List<Messaging.SingleEmailMessage>();
		List<CSPOFA__Orchestration_Step__c> stepsWithEmails = new List<CSPOFA__Orchestration_Step__c>();
		Map<Id, EmailTemplate> emailTemplateMap;
		Map<String, Id> emailTemplateNameMap = new Map<String, Id>();

		List<Id> processIds = new List<Id>();
		Set<Id> deliveryOrderIds = new Set<Id>();
		Set<String> deliveryOrderTeams = new Set<String>();
		List<String> emailTemplateNames = new List<String>{ COM_DELIVERY_COORDINATOR_EMAIL_TEMPLATE };

		for (CSPOFA__Orchestration_Step__c step : steps) {
			processIds.add(step.CSPOFA__Orchestration_Process__c);
		}

		if (emailTemplateNames.size() > 0) {
			emailTemplateMap = new Map<Id, EmailTemplate>(
				[SELECT ID, Subject, Name, DeveloperName, HtmlValue, Body, TemplateType FROM EmailTemplate WHERE Name IN :emailTemplateNames]
			);
		}

		for (EmailTemplate et : emailTemplateMap.values()) {
			emailTemplateNameMap.put(et.Name, et.Id);
		}

		Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c>(
			[
				SELECT Id, CSPOFA__Process_Type__c, COM_Delivery_Order__c, COM_Delivery_Order__r.Team__c
				FROM CSPOFA__Orchestration_Process__c
				WHERE Id IN :processIds
			]
		);

		for (CSPOFA__Orchestration_Process__c p : processesMap.values()) {
			deliveryOrderIds.add(p.COM_Delivery_Order__c);
			deliveryOrderTeams.add(getDeliveryOrderTeam(p.COM_Delivery_Order__r.Team__c));
		}

		Map<String, List<Id>> teamNameTeamMembersMap = new Map<String, List<Id>>();
		Set<Id> groupMembersUserIds = new Set<Id>();
		List<GroupMember> groupMembers = [
			SELECT UserOrGroupId, Group.DeveloperName
			FROM GroupMember
			WHERE Group.DeveloperName IN :deliveryOrderTeams
		];
		for (GroupMember gm : groupMembers) {
			if (teamNameTeamMembersMap.get(gm.Group.DeveloperName) != null) {
				teamNameTeamMembersMap.get(gm.Group.DeveloperName).add(gm.UserOrGroupId);
			} else {
				teamNameTeamMembersMap.put(gm.Group.DeveloperName, new List<Id>{ gm.UserOrGroupId });
			}
			groupMembersUserIds.add(gm.UserOrGroupId);
		}

		Map<Id, User> deliveryCoordinators = getDeliveryCoordinators(COM_CUSTOM_PERMISSION, groupMembersUserIds);

		Map<Id, COM_Delivery_Order__c> deliveryOrderMap;
		if (!deliveryOrderIds.isEmpty()) {
			deliveryOrderMap = new Map<Id, COM_Delivery_Order__c>(
				[
					SELECT
						Id,
						Name,
						Technical_Contact__c,
						Technical_Contact__r.Email,
						Technical_Contact__r.Name,
						Technical_Contact__r.Language__c,
						Site__r.Name,
						Installation_Start__c,
						Installation_End__c,
						Products__c,
						Team__c,
						OwnerId
					FROM COM_Delivery_Order__c
					WHERE Id IN :deliveryOrderIds
				]
			);
		}

		for (CSPOFA__Orchestration_Step__c step : steps) {
			try {
				Messaging.SingleEmailMessage tmpEmailMessage;

				if (processesMap.get(step.CSPOFA__Orchestration_Process__c).COM_Delivery_Order__c != null) {
					Id deliveryOrderId = processesMap.get(step.CSPOFA__Orchestration_Process__c).COM_Delivery_Order__c;
					COM_Delivery_Order__c deliveryOrder = deliveryOrderMap.get(deliveryOrderId);
					if (emailTemplateMap.get(emailTemplateNameMap.get(COM_DELIVERY_COORDINATOR_EMAIL_TEMPLATE)) != null) {
						tmpEmailMessage = createSingleEmailMessageForDeliveryOrder(
							step,
							deliveryOrder,
							getEmailTemplateByLanguage(
								emailTemplateMap,
								COM_DELIVERY_COORDINATOR_EMAIL_TEMPLATE,
								deliveryOrder.Technical_Contact__r.Language__c
							),
							teamNameTeamMembersMap,
							deliveryCoordinators
						);
					}
				}

				if (tmpEmailMessage != null) {
					emailMessages.add(tmpEmailMessage);
				}

				stepsWithEmails.add(step);
				step = CS_OrchestratorStepUtility.setStepToComplete(step);
			} catch (Exception ex) {
				step = CS_OrchestratorStepUtility.setStepToError(step, ex);
			}
		}

		List<Messaging.SendEmailResult> emailResults;
		if (Test.isRunningTest()) {
			emailResults = new List<Messaging.SendEmailResult>();
		} else {
			emailResults = Messaging.sendEmail(emailMessages, false);
		}

		for (Integer i = 0; i < stepsWithEmails.size() && i < emailResults.size(); i++) {
			if (!emailResults[i].isSuccess()) {
				stepsWithEmails[i].CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
				stepsWithEmails[i].CSPOFA__Completed_Date__c = null;
				stepsWithEmails[i].CSPOFA__Message__c = 'Exception:' + JSON.serialize(emailResults[i].getErrors());
			}
		}
		return stepsWithEmails;
	}

	private static Messaging.SingleEmailMessage createSingleEmailMessageForDeliveryOrder(
		CSPOFA__Orchestration_Step__c step,
		COM_Delivery_Order__c deliveryOrder,
		EmailTemplate emailTemplate,
		Map<String, List<Id>> teamNameTeamMembersMap,
		Map<Id, User> deliveryCoordinators
	) {
		Messaging.SingleEmailMessage emailMessage = Messaging.renderStoredEmailTemplate(emailTemplate.Id, null, deliveryOrder.Id);
		emailMessage.setSaveAsActivity(false);
		emailMessage.setTemplateId(emailTemplate.Id);
		emailMessage.optOutPolicy = 'FILTER';

		OrgWideEmailAddress deliveryOrgWideEmailAddress = EmailService.getDeliveryOrgWideEmailAddress();

		if (deliveryOrgWideEmailAddress != null) {
			emailMessage.setOrgWideEmailAddressId(deliveryOrgWideEmailAddress.Id);
		} else if (orgWideEmailAddress != null) {
			emailMessage.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
		}

		if (Test.isRunningTest()) {
			emailMessage.setToAddresses(new List<String>{ 'test@mailinator.com' });
		} else {
			List<String> emails = new List<String>();

			for (Id userId : teamNameTeamMembersMap.get(getDeliveryOrderTeam(deliveryOrder.Team__c))) {
				if (deliveryCoordinators.get(userId) != null) {
					emails.add(deliveryCoordinators.get(userId).Email);
				}
			}

			emailMessage.setToAddresses(emails);
		}

		String mailBody = COM_DeliveryEmailTemplatesMerge.processEmailTemplate(emailTemplate, deliveryOrder);
		if (emailTemplate.TemplateType != 'text') {
			emailMessage.setHtmlBody(mailBody);
		} else {
			emailMessage.setPlainTextBody(mailBody);
		}
		emailMessage.setSubject(COM_DeliveryEmailTemplatesMerge.processEmailSubject(emailTemplate.subject, deliveryOrder));

		return emailMessage;
	}

	private static EmailTemplate getEmailTemplateByLanguage(Map<Id, EmailTemplate> emailTemplateMap, String emailTemplateName, String language) {
		EmailTemplate returnValue;
		Map<String, Id> emailTemplateDeveloperNameMap = new Map<String, Id>();
		String emailTemplateNameWithUnderscores = emailTemplateName.replace(' ', '_');
		String emailTemplateNameWithLanguage = emailTemplateNameWithUnderscores + '_' + language;
		String defaultTemplateName = emailTemplateNameWithUnderscores + '_Nederlands';

		for (EmailTemplate et : emailTemplateMap.values()) {
			emailTemplateDeveloperNameMap.put(et.DeveloperName, et.Id);
		}

		if (emailTemplateDeveloperNameMap.get(emailTemplateNameWithLanguage) != null) {
			returnValue = emailTemplateMap.get(emailTemplateDeveloperNameMap.get(emailTemplateNameWithLanguage));
		} else {
			returnValue = emailTemplateMap.get(emailTemplateDeveloperNameMap.get(defaultTemplateName));
		}

		return returnValue;
	}

	private static Map<Id, User> getDeliveryCoordinators(String customPermissionName, Set<Id> userIds) {
		Map<Id, User> deliveryCoordinators = new Map<Id, User>();
		Set<Id> permissionSetIds = new Set<Id>();

		for (SetupEntityAccess access : [
			SELECT ParentId
			FROM SetupEntityAccess
			WHERE SetupEntityId IN (SELECT Id FROM CustomPermission WHERE DeveloperName = :customPermissionName)
		]) {
			permissionSetIds.add(access.ParentId);
		}

		deliveryCoordinators = permissionSetIds.isEmpty()
			? new Map<Id, User>()
			: new Map<Id, User>(
					[
						SELECT Username, Email
						FROM User
						WHERE
							IsActive = TRUE
							AND Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSetId IN :permissionSetIds)
							AND Id IN :userIds
					]
			  );

		return deliveryCoordinators;
	}

	private static String getDeliveryOrderTeam(String deliveryOrderTeam) {
		return COM_Constants.REGION_PREFIX + deliveryOrderTeam;
	}
}
