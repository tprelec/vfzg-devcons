@isTest
public with sharing class TestSLATriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'SLA';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'SLA-05-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		SLA__c testSla = new SLA__c();
		testSla.Name__c = 'Test';
		insert testSla;

		SLA__c queryRecord = [SELECT Id, ExternalID__c FROM SLA__c LIMIT 1];
		System.assert(queryRecord.ExternalID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
