global class BdsResponseClasses {

    global class ValidationErrorReturnClass {
        public Boolean isSuccess = false;
        public String errorMessage {
            get {
                if (errorMessage == null) {
                    errorMessage = '';
                    if (!isSuccess) {
                        errorMessage = 'Error(s) found';
                    }
                }
                return errorMessage;
            }
            public set;
        }

        public List<BdsResponseClasses.ValidationError> errors;
    }

    global class ValidationError {
        public String field;
        public String message;

        public ValidationError(String fieldName, String errorMessage) {
            field = fieldName;
            message = errorMessage;
        }
    }

    global class SuccessReturnClass {
        public Boolean isSuccess = true;
    }

    global class OpportunityStatusClass {
        public Boolean isSuccess = false;
        public String opportunityId;
        public String opportunityStageName = '';
        public String opportunityNumber = '';
        public String billingCustomerId = '';
        public String errorMessage = '';
    }

    global class OpportunityIdResponse {
        String opportunityNumber;

        public OpportunityIdResponse(String oppId) {
            opportunityNumber = oppId;
        }
    }

    global class OpportunityIdError {
        String error;

        public OpportunityIdError(String message) {
            error = message;
        }
    }
}