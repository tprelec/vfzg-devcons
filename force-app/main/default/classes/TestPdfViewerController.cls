@isTest
public with sharing class TestPdfViewerController {
	@TestSetup
	static void makeData() {
		User owner = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(owner);
		Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

		Attachment attachment = new Attachment();
		attachment.ParentId = opp.Id;
		attachment.Name = 'PDFViewer.pdf';
		attachment.Body = Blob.toPdf('test string');
		insert attachment;

		insert new ContentVersion(
			PathOnClient = 'PDFViewer.pdf',
			Title = 'PDFViewer.pdf',
			ContentLocation = 'S',
			Origin = 'C',
			VersionData = Blob.toPdf('test string')
		);
	}

	@isTest
	public static void testGetContentFromContentVersion() {
		List<ContentVersion> cvList = [SELECT Id FROM ContentVersion WHERE Title = 'PDFViewer.pdf'];

		List<Id> cvIdList = new List<Id>();
		cvIdList.add(cvList[0].Id);

		List<PdfViewerController.PdfFile> pdfs;
		if (!cvList.isEmpty()) {
			pdfs = PdfViewerController.getContent(cvIdList);
		}
		System.assertEquals(pdfs[0].name, 'PDFViewer.pdf', 'PDF exists');
	}

	@isTest
	public static void testGetContentFromAttachment() {
		List<Attachment> aList = [SELECT Id FROM Attachment WHERE Name = 'PDFViewer.pdf'];

		List<Id> aIdList = new List<Id>();
		aIdList.add(aList[0].Id);
		List<PdfViewerController.PdfFile> pdfs;
		if (!aList.isEmpty()) {
			pdfs = PdfViewerController.getContent(aIdList);
		}
		System.assertEquals(pdfs[0].name, 'PDFViewer.pdf', 'PDF exists');
	}
}