/**
 * @description       : provieds test and use cases coverage for methods on `LightningAppointmentCustom` apex class
 * @last modified on  : 05-16-2022
 **/
@IsTest
public class TestLightningAppointmentCustom {
	@IsTest
	static void testgetDecodedValues() {
		List<String> encodedValues = new List<String>{ 'AM%20Small%20lead%20from%20IAM' };
		Test.startTest();
		List<String> decodedValues = LightningAppointmentCustom.getDecodedValues(encodedValues);
		Test.stopTest();
		System.assert(!decodedValues.isEmpty(), 'The input string was not returned in the response list');
		System.assertEquals('AM Small lead from IAM', decodedValues.get(0), 'returned decoded value does not match with expected value');
	}

	@IsTest
	static void testgetDecodedValuesWhenEmpty() {
		List<String> encodedValues = new List<String>();
		Test.startTest();
		List<String> decodedValues = LightningAppointmentCustom.getDecodedValues(encodedValues);
		Test.stopTest();
		System.assert(decodedValues.isEmpty(), 'No input string was sent, an empty response is expected');
	}
}
