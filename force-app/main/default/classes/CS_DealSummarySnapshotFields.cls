global with sharing class CS_DealSummarySnapshotFields {
    
    @AuraEnabled
    public String value;
    @AuraEnabled
    public String label;
    @AuraEnabled
    public String name;
    @AuraEnabled
    public String type;
    @AuraEnabled
    public Boolean visibleIndirect;
    @AuraEnabled
    public Boolean visibleDirect;
    @AuraEnabled
    public Integer sequence;
    @AuraEnabled
    public String recurringOneOff;

    global CS_DealSummarySnapshotFields(){}
    
    
    
    global CS_DealSummarySnapshotFields(String label, String name,  String type, Boolean visibleIndirect, Boolean visibleDirect,Integer sequence,String reccuringOneOff) {
        this.value = value;
        this.label = label;
        this.name = name;
        this.type = type;
        this.visibleIndirect = visibleIndirect;
        this.visibleDirect = visibleDirect;
        this.sequence = sequence;
        this.recurringOneOff = reccuringOneOff;

    }
    
}