/**
 * @description         This is the class that contains logic for performing the Contact export to other systems
 *                      Requires WITHOUT SHARING as data might have to be exported (and thus updated) that is not owned by the user
 * @author              Guy Clairbois
 */
@SuppressWarnings('PMD')
public without sharing class ContactExport {
	public static Set<Id> contactsInExport {
		get {
			if (contactsInExport == null) {
				contactsInExport = new Set<Id>();
			}
			return contactsInExport;
		}
		set;
	}

	/*
	 *  Description:    This method can be used to schedule the export of all Contacts for a set of account ids
	 */
	public static void scheduleContactExportFromBanIds(Set<Id> banIds) {
		Set<Id> acctIds = new Set<Id>();
		for (Ban__c b : [SELECT Id, Account__c FROM Ban__c WHERE Id IN :banIds]) {
			acctIds.add(b.Account__c);
		}

		// By putting a 'dummy' error in the contact export results, the scheduled export is triggered
		List<Contact> contactsToUpdate = new List<Contact>();
		for (Contact c : [
			SELECT Id, Email, BOP_reference__c, BOP_export_datetime__c
			FROM Contact
			WHERE AccountId IN :acctIds
		]) {
			// only export if the contact was not exported before..
			if (c.Email != null && c.BOP_export_datetime__c == null) {
				c.BOP_export_Errormessage__c = 'Pending scheduled export..';
				contactsToUpdate.add(c);
			}
		}
		if (!contactsToUpdate.isEmpty()) {
			try {
				update contactsToUpdate;
			} catch (dmlException e) {
				ExceptionHandler.handleException(e);
			}
		}
	}

	/*
	 *  Description:    This method can be used to schedule the export of all Contacts for a set of account ids
	 */
	/*public static void scheduleContactExportFromAccountIds(Set<Id> acctIds){
        // By putting a 'dummy' error in the contact export results, the scheduled export is triggered
        List<Contact> contactsToUpdate = new List<Contact>();
        for(Contact c : [Select Id,Email,BOP_export_datetime__c From Contact Where AccountId in :acctIds]){
            // only export if the contact was not exported before..
            if(c.Email != null && c.BOP_export_datetime__c == null){
                c.BOP_export_Errormessage__c = 'Pending scheduled export..';
                contactsToUpdate.add(c);
            }
        }
        if(!contactsToUpdate.isEmpty()){
            try{
                update contactsToUpdate;
            } catch (dmlException e){
                ExceptionHandler.handleException(e);
            }
        }
    }*/

	/*
	 *  Description:    This method can be used to schedule the export of a set of Contacts based on ids
	 */
	public static void scheduleContactExportFromContactIds(Set<Id> contIds) {
		// By putting a 'dummy' error in the contact export results, the scheduled export is triggered
		List<Contact> contactsToUpdate = new List<Contact>();
		for (Id cId : contIds) {
			Contact c = new Contact(Id = cId);
			c.BOP_export_Errormessage__c = 'Pending scheduled export..';
			contactsToUpdate.add(c);
		}
		if (!contactsToUpdate.isEmpty()) {
			try {
				update contactsToUpdate;
			} catch (dmlException e) {
				ExceptionHandler.handleException(e);
			}
		}
	}

	@future(callout=true)
	public static void exportNewContactsOffline(Set<Id> contactIds) {
		// allow partial success
		Database.update(exportContacts(contactIds, 'create'), false);
		// TODO: handle failed updates

		// check if any contactroles exist and if so, schedule them for export
		Set<Id> crIds = new Set<Id>();
		for (Contact_Role__c cr : [
			SELECT Id
			FROM Contact_Role__c
			WHERE Active__c = TRUE AND Contact__c IN :contactIds
		]) {
			crIds.add(cr.Id);
		}
		if (!crIds.isEmpty())
			scheduleContactRoleExportFromContactIds(crIds);
	}

	@future(callout=true)
	public static void exportUpdatedContactsOffline(Set<Id> contactIds) {
		// allow partial success
		Database.update(exportContacts(contactIds, 'update'), false);
		// TODO: handle failed updates

		// check if any contactroles exist and if so, schedule them for export
		Set<Id> crIds = new Set<Id>();
		for (Contact_Role__c cr : [
			SELECT Id
			FROM Contact_Role__c
			WHERE Active__c = TRUE AND BOP_Export_Datetime__c = NULL AND Contact__c IN :contactIds
		]) {
			crIds.add(cr.Id);
		}
		if (!crIds.isEmpty())
			scheduleContactRoleExportFromContactIds(crIds);
	}

	public static List<Contact> exportContacts(set<Id> contactIDs, String exportType) {
		Map<Id, Contact> contactsToUpdate = new Map<Id, Contact>();
		List<Contact> theContacts = [
			SELECT
				Id,
				Account.BAN_Number__c,
				Account.BOPCode__c,
				BOP_reference__c,
				Email,
				Firstname,
				Lastname,
				Phone,
				MobilePhone,
				Fax,
				Title,
				BOP_export_Errormessage__c,
				BOP_export_datetime__c,
				userid__c
			FROM Contact
			WHERE Id IN :contactIds
		];

		if (!theContacts.isEmpty()) {
			ECSUserService userService = new ECSUserService();
			List<ECSUserService.request> requests = new List<ECSUserService.request>();
			Map<String, Contact> bopCodeToContact = new Map<String, Contact>();

			// collect accountids
			Set<Id> accountIds = new Set<Id>();
			Set<ID> internalUserIds = new Set<Id>();
			Map<ID, User> contactToUserMap = new Map<ID, User>();
			for (Contact c : theContacts) {
				accountIds.add(c.AccountId);
				// If this contact is for an internal user
				if (c.userid__c != null) {
					internalUserIds.add(c.userid__c);
				}
			}
			Map<ID, User> userMap = new Map<ID, User>(
				[
					SELECT contactId, BOP_User_Type__c
					FROM User
					WHERE contactId IN :contactIds OR id IN :internalUserIds
				]
			);
			for (ID key : userMap.keySet()) {
				User u = userMap.get(key);
				if (u.contactId != null) {
					contactToUserMap.put(u.contactId, u);
				}
			}

			// create map of bans to retrieve account details for customer contacts
			Map<Id, List<Ban__c>> accountIdToBanList = new Map<Id, List<Ban__c>>();
			/*for(Ban__c b : [Select Id, Account__c, Name, BAN_Number__c, BopCode__c, Corporate_Id__c 
                            From Ban__c 
                            Where Account__c in :accountIds 
                            AND BopCode__c != null
                            AND Account__c != :GeneralUtils.unifyOrphansAccount.Id
                            AND (Account__c != :GeneralUtils.VodafoneAccount.Id OR BopCode__c = 'TNF')]){
                            // see above special exclusions for bans under the vodafone account
                if(accountIdToBanList.containsKey(b.Account__c)){
                    accountIdToBanList.get(b.Account__c).add(b);
                } else {
                    accountIdToBanList.put(b.Account__c,new List<Ban__c>{b});
                }
            }*/

			// key ring
			Map<String, List<External_Account__c>> accountIdsWithBopCodeMap = KeyRingService.buildBopCodeMap(
				accountIds
			);
			// create map of accounts to retrieve account details for all contacts
			Map<Id, List<Account>> accountIdToAccountList = new Map<Id, List<Account>>();
			for (Account a : [
				SELECT Id, Name, BAN_Number__c, BopCode__c
				FROM Account
				WHERE Id IN :accountIdsWithBopCodeMap.keySet()
			]) {
				if (accountIdToAccountList.containsKey(a.Id)) {
					accountIdToAccountList.get(a.Id).add(a);
				} else {
					accountIdToAccountList.put(a.Id, new List<Account>{ a });
				}
			}

			for (Contact c : theContacts) {
				// If the contact has a related User, make sure it is of BOP_User_Type__c
				Boolean okToExport = true;
				User u;
				if (c.userid__c != null) {
					u = userMap.get(c.userid__c);
				} else {
					u = contactToUserMap.get(c.id);
				}
				if (u != null) {
					okToExport = u.BOP_User_Type__c;
				}

				// for Contact export, first check if the Contact has an email address, because that's the unique key in the interface
				// if email is empty then there will be no export
				if (c.Email != null && okToExport) {
					if (accountIdToAccountList.containsKey(c.AccountId)) {
						for (Account a : accountIdToAccountList.get(c.AccountId)) {
							ECSUserService.request req = new ECSUserService.request();
							//req.companyBanNumber = StringUtils.checkBan(a.BAN_Number__c)?a.BAN_Number__c:null;
							//req.companyBanNumber = a.BAN_Number__c;
							List<External_Account__c> extAccts = accountIdsWithBopCodeMap.get(a.Id);
							String bopCode = '';
							if (extAccts.size() > 0) {
								bopCode = extAccts[0].External_Account_Id__c;
							}
							req.companyBopCode = bopCode;
							//req.companyBopCode = a.BOPCode__c;
							//req.companyCorporateId = a.Corporate_Id__c;
							req.BOPemail = c.BOP_reference__c != null
								? c.BOP_reference__c
								: c.Email;
							req.email = c.Email;
							req.fax = c.Fax;
							req.firstname = c.FirstName;
							req.jobTitle = c.Title;
							req.lastname = c.LastName;
							req.mobile = c.MobilePhone;
							req.phone = c.Phone;

							// bopCodeToContact.put(a.BopCode__c+req.BOPemail,c);
							bopCodeToContact.put(bopCode + req.BOPemail, c);
							// Store the new email address as a key as well because if there is a duplicate error from BOP it returns the new email address and not the BOP email address
							if (req.email != req.BOPemail) {
								// bopCodeToContact.put(a.BopCode__c+req.email,c);
								bopCodeToContact.put(bopCode + req.email, c);
							}
							requests.add(req);
						}
					}
				}
			}
			if (!requests.isEmpty()) {
				userService.setRequest(requests);
				Boolean success = false;
				try {
					userService.makeRequest(exportType); // create / update
					success = true;
				} catch (ExWebServiceCalloutException ce) {
					for (Contact c : theContacts) {
						// only update the error if it has not been set before
						if (c.BOP_export_Errormessage__c != ce.get255CharMessage()) {
							c.BOP_export_Errormessage__c = ce.get255CharMessage();
							contactsToUpdate.put(c.Id, c);
						}
					}
				}

				if (success) {
					for (ECSUserService.response response : userService.getResponse()) {
						system.debug(response);
						String key = response.bopCode + response.email;
						if (bopCodeToContact.containsKey(key)) {
							// retrieve correct contact by ban/bop/corporate/zip/housenr/housenrsuffix
							Contact c = new Contact(Id = bopCodeToContact.get(key).Id);

							if (
								response.errorCode == null ||
								response.errorCode == '' ||
								response.errorCode == '0'
							) {
								// success
								c.BOP_export_datetime__c = system.now();
								c.BOP_export_Errormessage__c = null;
								// set BOP reference to (new) email address
								c.BOP_reference__c = bopCodeToContact.get(key).Email;
							} else {
								// failure
								if (response.errorCode == '45' || response.errorCode == '3005') {
									if (exportType == 'create') {
										// user already exists. Make this an update instead of a create
										c.BOP_export_datetime__c = system.now();
										c.BOP_reference__c = response.email;
										// also overwrite the errormessage, since errormsgs starting with a number will not be retried
										c.BOP_export_Errormessage__c = 'awaiting update';
									} else {
										// If an update throws an error we want to know about it
										c.BOP_export_Errormessage__c =
											response.errorCode +
											' - ' +
											response.errorMessage;
									}
								} else {
									// add errormessage to user
									c.BOP_export_Errormessage__c =
										response.errorCode +
										' - ' +
										response.errorMessage;
								}
							}
							contactsToUpdate.put(c.Id, c);
						} else {
							System.Debug(bopCodeToContact);
							ExceptionHandler.handleException(
								'No BAN+email returned by BOP for key ' +
								key +
								'. Response: ' +
								response +
								'BopCodeToContact map: ' +
								bopCodeToContact
							);
							//throw new ExMissingDataException();
						}
					}
				}
			}
		}
		return contactsToUpdate.values();
	}

	public static Set<Id> contactRolesInExport {
		get {
			if (contactRolesInExport == null) {
				contactRolesInExport = new Set<Id>();
			}
			return contactRolesInExport;
		}
		set;
	}

	@future(callout=true)
	public static void exportNewContactRolesOffline(Set<Id> contactRoleIds) {
		// allow partial success
		Database.update(exportContactRoles(contactRoleIds, 'create'), false);
		// TODO: handle failed updates
	}

	@future(callout=true)
	public static void exportDeletedContactRolesOffline(Set<Id> contactRoleIds) {
		// allow partial success
		Database.update(exportContactRoles(contactRoleIds, 'delete'), false);
		// TODO: handle failed updates
	}

	public static List<Contact_Role__c> exportContactRoles(
		set<Id> contactRoleIds,
		String exportType
	) {
		Map<Id, Contact_Role__c> contactRolesToUpdate = new Map<Id, Contact_Role__c>();
		List<Contact_Role__c> theContactRoles = [
			SELECT
				Id,
				Account__c,
				Account__r.BAN_Number__c,
				Account__r.BOPCode__c,
				//Account__r.Corporate_Id__c,
				Contact__r.AccountId,
				Contact__r.Account.BAN_Number__c,
				Contact__r.Account.BOPCode__c,
				//Contact__r.Account.Corporate_Id__c,
				Contact__r.BOP_reference__c,
				Contact__r.Email,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Account__c,
				Site__r.Site_Account__r.BAN_Number__c,
				Site__r.Site_Account__r.BOPCode__c,
				//Site__r.Site_Account__r.Corporate_Id__c,
				Type__c,
				BOP_export_Errormessage__c,
				BOP_export_datetime__c
			FROM Contact_Role__c
			WHERE Id IN :contactRoleIds
		];

		if (!theContactRoles.isEmpty()) {
			ECSContactService contactService = new ECSContactService();
			List<ECSContactService.request> requests = new List<ECSContactService.request>();
			Map<String, Contact_Role__c> keyToContactRole = new Map<String, Contact_Role__c>();

			// collect accountids
			Set<Id> accountIds = new Set<Id>();
			Set<Id> partnerAccountIds = new Set<Id>();
			for (Contact_Role__c c : theContactRoles) {
				accountIds.add(c.Account__c);
				accountIds.add(c.Site__r.Site_Account__c);
				partnerAccountIds.add(c.Contact__r.AccountId);
			}
			// key ring
			Map<String, List<External_Account__c>> accountIdsWithBopCodeMap = KeyRingService.buildBopCodeMap(
				accountIds
			);
			// create map of accounts to retrieve account details for customer contacts
			Map<Id, List<Account>> accountIdToCustomerAccountList = new Map<Id, List<Account>>();
			for (Account b : [
				SELECT Id, BAN_Number__c, BopCode__c
				FROM Account
				WHERE Id IN :accountIdsWithBopCodeMap.keySet()
			]) {
				if (accountIdToCustomerAccountList.containsKey(b.Id)) {
					accountIdToCustomerAccountList.get(b.Id).add(b);
				} else {
					accountIdToCustomerAccountList.put(b.Id, new List<Account>{ b });
				}
			}
			// create map of accounts to retrieve account details for partner contacts
			Map<Id, List<Account>> accountIdToAccountList = new Map<Id, List<Account>>();
			for (Account a : [
				SELECT Id, Name, BAN_Number__c, BopCode__c
				FROM Account
				WHERE IsPartner = TRUE AND Id IN :partnerAccountIds AND BopCode__c != NULL
			]) {
				if (accountIdToAccountList.containsKey(a.Id)) {
					accountIdToAccountList.get(a.Id).add(a);
				} else {
					accountIdToAccountList.put(a.Id, new List<Account>{ a });
				}
			}

			system.debug(accountIdToCustomerAccountList);
			system.debug(accountIdToAccountList);

			for (Contact_Role__c c : theContactRoles) {
				system.debug(c);

				// for ContactRole export, first check if the Contact was synced
				if (c.Contact__r.Email != null) {
					if (c.Account__c != null) {
						// Customer Contact
						if (accountIdToCustomerAccountList.containsKey(c.Account__c)) {
							// check if account actually has any bans
							for (Account b : accountIdToCustomerAccountList.get(c.Account__c)) {
								ECSContactService.request req = new ECSContactService.request();
								req.email = c.Contact__r.BOP_reference__c;
								//req.referenceId = c.Contact__r.User__c;
								if (accountIdToAccountList.containsKey(c.Contact__r.AccountId)) {
									// partner's contact (only 1 entry in list)
									Account pa = accountIdToAccountList.get(
										c.Contact__r.AccountId
									)[0];
									//req.userCompanyBanNumber = pa.Ban_Number__c;
									if (
										pa.BAN_Number__c != null && pa.BAN_Number__c.startsWith('3')
									) {
										req.userCompanyBanNumber = pa.BAN_Number__c;
									}
									//req.userCompanyBanNumber = pa.BAN_Number__c.startsWith('3')?pa.BAN_Number__c:null;

									req.userCompanyBopCode = pa.BOPCode__c;
									//req.userCompanyCorporateId = pa.Corporate_Id__c;
								} else {
									// customer's contact, repeat for all bans
									//req.userCompanyBanNumber = b.BAN_Number__c.startsWith('3')?b.BAN_Number__c:null;
									if (
										b.BAN_Number__c != null && b.BAN_Number__c.startsWith('3')
									) {
										req.userCompanyBanNumber = b.BAN_Number__c;
									}

									//req.userCompanyBanNumber = b.Ban_Number__c;
									req.userCompanyBopCode = b.BOPCode__c;
									//req.userCompanyCorporateId = b.Corporate_Id__c;
								}

								//req.companyBanNumber = b.Ban_Number__c;
								if (b.BAN_Number__c != null && b.BAN_Number__c.startsWith('3')) {
									req.companyBanNumber = b.BAN_Number__c;
								}

								//req.companyCorporateId = b.Corporate_Id__c;
								req.companyBopCode = b.BOPCode__c;

								req.type = c.Type__c;

								keyToContactRole.put(
									req.companyBopCode +
									c.Contact__r.BOP_reference__c +
									c.Type__c,
									c
								);
								requests.add(req);
							}
						}
					}
					if (
						c.Site__c != null &&
						accountIdToCustomerAccountList.containsKey(c.Site__r.Site_Account__c)
					) {
						// Site Contact
						if (accountIdToCustomerAccountList.containsKey(c.Account__c)) {
							// check if account actually has any bans
							for (Account b : accountIdToCustomerAccountList.get(c.Account__c)) {
								ECSContactService.request req = new ECSContactService.request();
								req.email = c.Contact__r.BOP_reference__c;
								//req.referenceId = c.Contact__r.User__c;
								if (accountIdToAccountList.containsKey(c.Contact__r.AccountId)) {
									// partner's contact
									Account pa = accountIdToAccountList.get(
										c.Contact__r.AccountId
									)[0];
									req.userCompanyBanNumber = StringUtils.checkBan(
											pa.BAN_Number__c
										)
										? pa.BAN_Number__c
										: null;

									//req.userCompanyBanNumber = pa.Ban_Number__c;
									req.userCompanyBopCode = pa.BOPCode__c;
									//req.userCompanyCorporateId = pa.Corporate_Id__c;
								} else {
									// customer's contact, repeat for all bans
									req.userCompanyBanNumber = StringUtils.checkBan(b.BAN_Number__c)
										? b.BAN_Number__c
										: null;
									//req.userCompanyBanNumber = b.Ban_Number__c;
									req.userCompanyBopCode = b.BOPCode__c;
									//req.userCompanyCorporateId = b.Corporate_Id__c;
								}

								//req.companyBanNumber = b.Ban_Number__c;
								//req.companyCorporateId = b.Corporate_Id__c;
								//req.companyBopCode = b.BOPCode__c;

								req.zipCode = c.Site__r.Site_Postal_Code__c;
								req.houseNumber = String.valueOf(c.Site__r.Site_House_Number__c);
								req.houseNumberExtension = c.Site__r.Site_House_Number_Suffix__c;

								req.type = c.Type__c;

								keyToContactRole.put(
									req.companyBopCode +
									c.Contact__r.BOP_reference__c +
									c.Type__c,
									c
								);
								requests.add(req);
							}
						}
					}
				} else {
					// in case of no email, ignore the contact assuming it is not meant to be exported
					// users should be educated to know that if they want the contact in BOP, they should add an email address
					// therefore commented out below code
					//c.BOP_export_Errormessage__c = 'Email is required for triggering the export to BOP';
					//contactsToUpdate.add(c);
				}
			}
			if (!requests.isEmpty()) {
				contactService.setRequest(requests);
				Boolean success = false;
				try {
					contactService.makeRequest(exportType); // create / delete
					success = true;
				} catch (ExWebServiceCalloutException ce) {
					for (Contact_Role__c c : theContactRoles) {
						// only update the error if it has not been set before
						if (c.BOP_export_Errormessage__c != ce.get255CharMessage()) {
							c.BOP_export_Errormessage__c = ce.get255CharMessage();
							contactRolesToUpdate.put(c.Id, c);
						}
					}
				}

				if (success) {
					// use a map to store contacts that need a retrigger (as there might be duplicates in case of multiple roles)
					Map<Id, Contact> contactsToUpdate = new Map<Id, Contact>();
					for (ECSContactService.response response : contactService.getResponse()) {
						system.debug(response);
						String key = response.bopCode + response.email + response.type;
						if (keyToContactRole.containsKey(key)) {
							// retrieve correct contactrole by bop/email/type
							Contact_Role__c c = new Contact_Role__c(
								Id = keyToContactRole.get(key).Id
							);

							if (
								response.errorCode == null ||
								response.errorCode == '' ||
								response.errorCode == '0'
							) {
								// success
								c.BOP_export_datetime__c = system.now();
								c.BOP_export_Errormessage__c = null;
							} else {
								// failure
								if (response.errorCode == '3001') {
									// user does not exist. Retrigger the contact
									Contact cont = new Contact(
										Id = keyToContactRole.get(key).Contact__c
									);
									cont.BOP_export_datetime__c = null;
									cont.BOP_export_Errormessage__c = 'retrigger';
									contactsToUpdate.put(cont.Id, cont);
									// add errormessage to contactrole to retrigger export
									c.BOP_export_Errormessage__c =
										response.errorCode +
										' - ' +
										response.errorMessage;
								} else if (response.errorCode == '7002') {
									// role already exists. Means success!

									c.BOP_export_datetime__c = system.now();
									c.BOP_export_Errormessage__c = null;
								} else if (response.errorCode == '7003') {
									// removal is failing because role doesn't exist. No problem. Means success!
									if (c.BOP_Export_Datetime__c == null)
										c.BOP_export_datetime__c = system.now();
									c.BOP_export_Errormessage__c = null;
								} else {
									// add errormessage to contactrole
									c.BOP_export_Errormessage__c =
										response.errorCode +
										' - ' +
										response.errorMessage;
								}
							}
							contactRolesToUpdate.put(c.Id, c);
						} else {
							ExceptionHandler.handleException(
								'No BAN+email returned by BOP for key ' +
								key +
								'. Response: ' +
								response +
								'KeyToContactRole map: ' +
								keyToContactRole
							);
							//throw new ExMissingDataException('No BOP+email+type returned by BOP');
						}
					}
					if (!contactsToUpdate.isEmpty())
						update contactsToUpdate.values();
				}
			}
		}
		return contactRolesToUpdate.values();
	}

	/*
	 *  Description:    This method can be used to schedule the export of a set of ContactRoles based on ids
	 */
	public static void scheduleContactRoleExportFromContactIds(Set<Id> contIds) {
		// By putting a 'dummy' error in the contactrole export results, the scheduled export is triggered
		List<Contact_Role__c> contactRolesToUpdate = new List<Contact_Role__c>();
		for (Id cId : contIds) {
			Contact_Role__c c = new Contact_Role__c(Id = cId);
			c.BOP_export_Errormessage__c = 'Pending scheduled export..';
			contactRolesToUpdate.add(c);
		}
		if (!contactRolesToUpdate.isEmpty()) {
			try {
				update contactRolesToUpdate;
			} catch (dmlException e) {
				ExceptionHandler.handleException(e);
			}
		}
	}

	/*
	 *  Description:    This method can be used to schedule the export of a set of ContactRoles based on ids
	 */
	public static void scheduleContactRoleDeleteFromContactIds(Set<Id> contIds) {
		// By putting a 'dummy' error in the contactrole export results, the scheduled export is triggered
		List<Contact_Role__c> contactRolesToUpdate = new List<Contact_Role__c>();
		for (Id cId : contIds) {
			Contact_Role__c c = new Contact_Role__c(Id = cId);
			c.BOP_export_Errormessage__c = 'Pending scheduled delete..';
			contactRolesToUpdate.add(c);
		}
		if (!contactRolesToUpdate.isEmpty()) {
			try {
				update contactRolesToUpdate;
			} catch (dmlException e) {
				ExceptionHandler.handleException(e);
			}
		}
	}
}