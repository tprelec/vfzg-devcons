public class EMP_ActivateSIMForTemplate {
    public class Request{
        public SelectPricePlanData SelectPricePlanData;
        public List<UpdateProductSettingsInput> UpdateProductSettingsInput;
        public Request(SelectPricePlanData tempSelectPricePlanData, List<UpdateProductSettingsInput> tempUpdateProductSettingsInput) {
            this.SelectPricePlanData = tempSelectPricePlanData;
            this.UpdateProductSettingsInput = tempUpdateProductSettingsInput;
        }
    }
    
    public class SelectPricePlanData{
        public CustomerInfo customerInfo;
        public String creatorId; // dealer code
        public String billingArrangementId; // comes from order form
        public String frameworkAgreementId; // comes from Contract VF
        public String templateId; // comes from VF Product
        public String msisdn; // CTN number, comes from NetProfit CTN
        public String simCardNumber; // comes from NetProfit CTN
        public PortInData portInData; // comes from NetProfit CTN
        public SelectPricePlanData(CustomerInfo tempCustomerInfo, String tempCreatorId, String tempBillingArrangementId, String tempFrameworkAgreementId, String tempTemplateId, String tempMsisdn, String tempSimCardNumber, PortInData tempPortInData) {
            this.customerInfo = tempCustomerInfo;
            this.creatorId = tempCreatorId;
            this.billingArrangementId = tempBillingArrangementId;
            this.frameworkAgreementId = tempFrameworkAgreementId;
            this.templateId = tempTemplateId;
            this.msisdn = tempMsisdn;
            this.simCardNumber = tempSimCardNumber;
            this.portInData = tempPortInData;
        }
    }
    
    public class CustomerInfo{
        public String billingCustomerId; // ban number
        public CustomerInfo( String tempBillingCustomerId) { 
            this.billingCustomerId = tempBillingCustomerId;
        }
    }
    
    public class PortInData{
        public Boolean prepaidIndicator; // hardcoded as false, This project is about Postpaid 
        public String donorServiceProviderID; // NetProfit CTN
        public String donorNetworkOperatorID; // NetProfit CTN
        public String simNumber; // NetProfit CTN
        public String wishDateTime; // mandatory, NetProfit CTN
        public String clientNumber; //ban number
        public String ctn; // mandatory, NetProfit CTN
        public String resourceID; // NetProfit CTN
        public final String resourceType; // hardcoded 'MSISDN'
        public final String SmartValidation; // mandatory, Assumption is to Hardcoded either of the following values "Yes/No/DONE" check with business should be
        public final Boolean AutoScheduleIndicator; // mandatory, Assumption is to Hardcoded as false because SF provide the wish date, changed to TRUE because it fails on false

        public PortInData(String donorServiceProviderID, String donorNetworkOperatorID, String simNumber, String wishDateTime, String clientNumber, String ctn, String resourceID) {
            this.prepaidIndicator = false;
            //this.donorServiceProviderID = donorServiceProviderID;
            //this.donorNetworkOperatorID = donorNetworkOperatorID;
            this.simNumber = simNumber;
            this.wishDateTime = wishDateTime;
            this.clientNumber = clientNumber;
            this.ctn = ctn;
            this.resourceID = resourceID;
            this.resourceType = 'MSISDN';
            this.SmartValidation = 'Done';//Yes/No/Done
            this.AutoScheduleIndicator = true;
        }
    }
    
    public class UpdateProductSettingsInput{
        public final String action; // hardcoded as 'SET'
        public String component_code; // mandatory
        public List<ComponentSetting> component_settings;
        public Boolean simulate_only; //hardcoded as false
        public Boolean return_eligible_settings; //hardcoded as false

        public UpdateProductSettingsInput(String componentCode, List<ComponentSetting> componentSettings) {
            this.action = 'SET';
            this.component_code = componentCode;
            this.component_settings = componentSettings;
            this.simulate_only = false;
            this.return_eligible_settings = false;
        }
    }
    
    public class ComponentSetting{
        public String settingcode;
        public String value;
        public ComponentSetting(String settingcode, String value) {
            this.settingcode = value!=null ? settingcode:null;
            this.value = value;
        }	
    }
    
    public class ActivateSimForTemplateBulkRequestTop{
        public ActivateSimForTemplateBulkRequest activateSimForTemplateBulkRequest;
        public ActivateSimForTemplateBulkRequestTop(ActivateSimForTemplateBulkRequest activateSimForTemplateBulkRequest) {
            this.activateSimForTemplateBulkRequest = activateSimForTemplateBulkRequest;
        }
    }
    
    public class ActivateSimForTemplateBulkRequest{
        public List<SharedBslHttpHeader> sharedBslHttpHeaders;
        public List<ActivateSimForTemplateRequest> activateSimForTemplateRequests;
        public ActivateSimForTemplateBulkRequest(List<SharedBslHttpHeader> sharedBslHttpHeaders, List<ActivateSimForTemplateRequest> activateSimForTemplateRequests) {
            this.sharedBslHttpHeaders = sharedBslHttpHeaders;
            this.activateSimForTemplateRequests = activateSimForTemplateRequests;
        }
    }
    
    public class ActivateSimForTemplateRequest{
        public List<BslHttpHeader> bslHttpHeaders;
        public String correlationID; //SF Id
        public Integer priority;
        public Request activateSimForTemplateRequest;
        public ActivateSimForTemplateRequest(List<BslHttpHeader> requestHeaders, String correlationID, Integer priority, Request data) {
            this.bslHttpHeaders = requestHeaders;
            this.correlationID = correlationID;
            this.priority = priority;
            this.activateSimForTemplateRequest = data;
        }
    }
    
    public class SharedBslHttpHeader{
        public String name;
        public String value;
        public SharedBslHttpHeader(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }
    
    public class BslHttpHeader{
        public String name;
        public String value;
        public BslHttpHeader(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

    public class Response{
        public Integer status; 
        public List<Data> data; 
        public List<EMP_ResponseError.Error> error; 
        public Response(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        switch on text {
                            when 'status' {
                                status = parser.getIntegerValue();
                            }
                            when 'data' {
                                data = arrayOfData(parser);
                            }
                            when 'error' {
                                error = EMP_ResponseError.arrayOfError(parser);
                            }
                            when else {
                                System.debug(LoggingLevel.WARN, 'Response consuming unrecognized property: '+text);  
                            }
                        }
                    }
                }
            }
        }
    }

    public class Data {
        public String orderID;
		public Data(JSONParser parser) {
			while (parser.nextToken() != System.JSONToken.END_OBJECT) {
				if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
					String text = parser.getText();
					if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        switch on text {
                            when 'orderID' {
                                orderID = parser.getText();
                            }
                            when else {
                                System.debug(LoggingLevel.WARN, 'Data consuming unrecognized property: '+text);
                            }
                        }
					}
				}
			}
		}
    }

    private static List<Data> arrayOfData(System.JSONParser p) {
        List<Data> res = new List<Data>();
        if (p.getCurrentToken() == null) {
            p.nextToken();
        }
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Data(p));
        }
        return res;
    }

    public static Response parse(String json) {
		System.JSONParser parser = System.JSON.createParser(json);
		return new Response(parser);
	}
}