public with sharing class MapsRecordRedirectController {
	public PageReference redirectToRecord() {
		Id recordId = ApexPages.currentPage().getParameters().get('RecordId');
		if (recordId != null) {
			return new PageReference('/' + recordId);
		} else {
			return null;
		}
	}
}