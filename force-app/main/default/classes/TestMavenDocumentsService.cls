@IsTest
public with sharing class TestMavenDocumentsService {
	@TestSetup
	static void makeData() {
		MavenDocumentsTestFactory.createMavenDocumentsRecords();
	}

	@IsTest
	public static void testGetDocumentTemplate() {
		Test.startTest();
		mmdoc__Document_Template__c template = MavenDocumentsService.getDocumentTemplate(
			'Ziggo Quote'
		);
		Test.stopTest();
		System.assert(template != null, 'Template should be retreived.');
	}

	@IsTest
	public static void testCreateDocRequest() {
		Test.startTest();
		mmdoc__Document_Template__c template = MavenDocumentsService.getDocumentTemplate(
			'Ziggo Quote'
		);
		mmdoc__Document_Request__c req = MavenDocumentsService.createDocRequest(
			UserInfo.getUserId(),
			template,
			'Attachment',
			'PDF',
			'Batch',
			true
		);
		req = MavenDocumentsService.getDocumentRequest(req.Id);
		Test.stopTest();
		System.assert(req != null, 'Document Request should be inserted.');
	}
}