/**
 * @description         This is the trigger handler for the cspmb__Add_On_Price_Item__c sObject.
 * @author              Guy Clairbois
 */
public with sharing class AddOnPriceItemTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
		List<cspmb__Add_On_Price_Item__c> newPriceitems = (List<cspmb__Add_On_Price_Item__c>) this.newList;
		updateProductLink(newPriceitems, null);
	}

	public override void AfterInsert() {
		List<cspmb__Add_On_Price_Item__c> newPriceitems = (List<cspmb__Add_On_Price_Item__c>) this.newList;
	}

	public override void BeforeUpdate() {
		List<cspmb__Add_On_Price_Item__c> newPriceitems = (List<cspmb__Add_On_Price_Item__c>) this.newList;
		Map<Id, cspmb__Add_On_Price_Item__c> oldPriceitemsMap = (Map<Id, cspmb__Add_On_Price_Item__c>) this.oldMap;
		updateProductLink(newPriceitems, oldPriceitemsMap);
	}

	public override void AfterUpdate() {
		List<cspmb__Add_On_Price_Item__c> newPriceitems = (List<cspmb__Add_On_Price_Item__c>) this.newList;
		Map<Id, cspmb__Add_On_Price_Item__c> oldPriceitemsMap = (Map<Id, cspmb__Add_On_Price_Item__c>) this.oldMap;
	}

	/**
	 * @description         This method updates the prduct2 lookup fields by taking the productcodes from the external id fields
	 */
	private void updateProductLink(List<cspmb__Add_On_Price_Item__c> newPriceitems, Map<Id, cspmb__Add_On_Price_Item__c> oldPriceitemsMap) {
		// check if there are any priceitems without a product__c but with cspmb__One_Off_Charge_External_Id__c or cspmb__Recurring_Charge_External_Id__c
		// if so, store the id's for lookup
		Map<String, Id> productCodeToId = new Map<String, Id>();
		for (cspmb__Add_On_Price_Item__c pi : newPriceitems) {
			if (oldPriceitemsMap == null) {
				// insert
				if (pi.cspmb__One_Off_Charge_External_Id__c != null || pi.cspmb__Recurring_Charge_External_Id__c != null) {
					productCodeToId.put(pi.cspmb__One_Off_Charge_External_Id__c, null);
					productCodeToId.put(pi.cspmb__Recurring_Charge_External_Id__c, null);
				}
			} else {
				// update
				if (
					pi.cspmb__One_Off_Charge_External_Id__c != oldPriceitemsMap.get(pi.Id).cspmb__One_Off_Charge_External_Id__c ||
					pi.cspmb__Recurring_Charge_External_Id__c != oldPriceitemsMap.get(pi.Id).cspmb__Recurring_Charge_External_Id__c
				) {
					productCodeToId.put(pi.cspmb__One_Off_Charge_External_Id__c, null);
					productCodeToId.put(pi.cspmb__Recurring_Charge_External_Id__c, null);
				}
			}
		}
		if (!productCodeToId.isEmpty()) {
			for (Product2 p : [
				SELECT Id, ProductCode
				FROM Product2
				WHERE ProductCode IN :productCodeToId.keySet() AND CLC__c = NULL AND IsActive = TRUE
			]) {
				productCodeToId.put(p.ProductCode, p.Id);
			}

			for (cspmb__Add_On_Price_Item__c pi : newPriceitems) {
				if (pi.cspmb__One_Off_Charge_External_Id__c != null && pi.cspmb__One_Off_Charge_External_Id__c != '') {
					if (
						productCodeToId.containsKey(pi.cspmb__One_Off_Charge_External_Id__c) &&
						productCodeToId.get(pi.cspmb__One_Off_Charge_External_Id__c) != null
					) {
						pi.One_Off_Charge_Product__c = productCodeToId.get(pi.cspmb__One_Off_Charge_External_Id__c);
					} else {
						pi.cspmb__One_Off_Charge_External_Id__c.addError('Productcode does not exist or product is not active.');
					}
				} else {
					pi.One_Off_Charge_Product__c = null;
				}
				if (pi.cspmb__Recurring_Charge_External_Id__c != null && pi.cspmb__Recurring_Charge_External_Id__c != '') {
					if (
						productCodeToId.containsKey(pi.cspmb__Recurring_Charge_External_Id__c) &&
						productCodeToId.get(pi.cspmb__Recurring_Charge_External_Id__c) != null
					) {
						pi.Recurring_Charge_Product__c = productCodeToId.get(pi.cspmb__Recurring_Charge_External_Id__c);
					} else {
						pi.cspmb__Recurring_Charge_External_Id__c.addError('Productcode does not exist or product is not active.');
					}
				} else {
					pi.Recurring_Charge_Product__c = null;
				}
			}
		}
	}

	public void increaseExternalId() {
		List<cspmb__Add_On_Price_Item__c> newProducts = (List<cspmb__Add_On_Price_Item__c>) this.newList;
		Sequence seq = new Sequence('Add On Price Item');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (cspmb__Add_On_Price_Item__c o : newProducts) {
				o.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
