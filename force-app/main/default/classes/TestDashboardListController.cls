/**
 * @description       :
 * @author            : mcubias
 * @group             :
 * @last modified on  : 27-09-2022
 * @last modified by  : mcubias
 **/

@IsTest
public class TestDashboardListController {
	@IsTest(SeeAllData=true)
	static void testGetAllDashboards() {
		List<Dashboard> allDashboards = new List<Dashboard>();
		Test.startTest();
		allDashboards = DashboardListController.getAllDashboards(5, 0, null);
		Test.stopTest();
		System.assert(allDashboards != null, 'A list of all dashboards should be retrieve');
	}

	@IsTest
	static void testGetAllDashboardsByFolderId() {
		List<Dashboard> allDashboards = new List<Dashboard>();
		Test.startTest();
		allDashboards = DashboardListController.getAllDashboards(5, 0, '006000000000000AAA');
		Test.stopTest();
		System.assert(allDashboards.isEmpty(), 'No Dashboards should be returned.');
	}

	@IsTest
	static void testGetTotalDashboards() {
		Integer totalDashboardsInFolder = 0;
		Test.startTest();
		totalDashboardsInFolder = DashboardListController.getTotalDashboards('006000000000000AAA');
		Test.stopTest();
		System.assert(totalDashboardsInFolder >= 0, 'A total number of dashboards should have been retrieved');
	}

	@IsTest(SeeAllData=true)
	static void testGetAllFolders() {
		List<Folder> allFolders = new List<Folder>();
		Test.startTest();
		allFolders = DashboardListController.getAllDashboardFolders();
		Test.stopTest();
		System.assert(allFolders != null, 'A list of all folders should be retrieve');
	}
}
