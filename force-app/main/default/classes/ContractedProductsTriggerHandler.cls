public with sharing class ContractedProductsTriggerHandler extends TriggerHandler {
	private List<Contracted_Products__c> newContractedProducts;
	private Map<Id, Contracted_Products__c> oldContractedProductsMap;

	private void init() {
		newContractedProducts = (List<Contracted_Products__c>) this.newList;
		oldContractedProductsMap = (Map<Id, Contracted_Products__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setClc();
		copyPbxFromSite();
		enrichData();
	}

	public override void beforeUpdate() {
		init();
		setClc();
		copyPbxFromSite();
		enrichData();
	}

	public override void afterInsert() {
		init();
		calculateNumberOfSitesInContract();
		rollupFamilyConditions();
		updateOrderReadyFlag();
		BillingLogicHelper.createInstallBaseForCps(newContractedProducts);
		triggerEcsDelivery();
		linkCTNS();
		executeOnlyOnce();
	}

	public override void afterUpdate() {
		init();
		calculateNumberOfSitesInContract();
		rollupFamilyConditions();
		updateOrderReadyFlag();
		executeOnlyOnce(); // This should run only once
		triggerEcsDelivery();
	}

	private void executeOnlyOnce() {
		if (!TriggerHandler.isAlreadyModified()) {
			TriggerHandler.setAlreadyModified();
			if (
				FeatureFlagging.isActive(ContractedProductService.FEAT_FLAG_ODR_HBO) ||
				FeatureFlagging.isActive(ContractedProductService.FEAT_FLAG_ODR_RED1) ||
				Test.isRunningTest()
			) {
				List<Contracted_Products__c> lContractedProducts = newContractedProducts.deepClone(true, true, true);
				setOneDataRedundancyAndHbo(lContractedProducts);
			}

			if (Trigger.isAfter && Trigger.isUpdate) {
				triggerChangePabx();
			}
		}
	}

	private static void setOneDataRedundancyAndHbo(List<Contracted_Products__c> lContractedProducts) {
		Set<Id> carrierProductIds = new Set<Id>();
		if (FeatureFlagging.isActive(ContractedProductService.FEAT_FLAG_ODR_HBO) || Test.isRunningTest()) {
			carrierProductIds = ContractedProductService.setRedundancyReferences(lContractedProducts);
		}
		if (FeatureFlagging.isActive(ContractedProductService.FEAT_FLAG_ODR_RED1) || Test.isRunningTest()) {
			ContractedProductService.setHboS(lContractedProducts, carrierProductIds);
		}
	}

	/*
	 *      Description:    set the clc field if it's not filled yet
	 */
	private void setClc() {
		Map<Id, String> productIdToClc = new Map<Id, String>();
		// collect all missing clc's
		for (Contracted_Products__c cp : newContractedProducts) {
			// if new cp or existing cp with changed product, update the clc
			if (cp.CLC__c == null) {
				productIdToCLC.put(cp.Product__c, null);
			}
		}
		// fetch the actual clc values
		if (!productIdToCLC.isEmpty()) {
			for (Product2 prod : [SELECT Id, CLC__c FROM Product2 WHERE Id IN :productIdToCLC.keySet() AND CLC__c != NULL]) {
				productIdToCLC.put(prod.Id, prod.CLC__c);
			}
			// paste the clc values to the cp's, or if none is filled in and none is found, return an error
			for (Contracted_Products__c cp : newContractedProducts) {
				// if new cp update the clc
				if (cp.CLC__c == null) {
					if (productIdToCLC.containsKey(cp.Product__c) && productIdToCLC.get(cp.Product__c) != null) {
						cp.CLC__c = productIdToCLC.get(cp.Product__c);
					} else {
						cp.CLC__c.addError('Please specify a CLC value for this lineitem.');
					}
				}
			}
		}
	}

	/**
	 * @description         This method copies the PBX from the Site__c (One Fixed) or Account (One Net) for PBX_Trunking tags when the site is added or changed
	 * @author              Guy Clairbois
	 */
	private void copyPbxFromSite() {
		Map<Id, Id> siteIdToPbxId = new Map<Id, Id>();
		Map<Id, Id> cpIdToProductId = new Map<Id, Id>();
		populateSitePbxAndCpProductMaps(siteIdToPbxId, cpIdToProductId);

		if (!siteIdToPbxId.isEmpty()) {
			// collect the site pbx ids and the account ids
			Map<Id, Id> siteIdToAccountId = new Map<Id, Id>();
			updPopSitePbxAndSiteAccountMaps(siteIdToAccountId, siteIdToPbxId);

			// collect the one net pbx id
			Map<Id, Id> acctIdToOneNetPbxId = new Map<Id, Id>();
			populateAccountOneNetPbxMap(acctIdToOneNetPbxId, siteIdToAccountId);

			// add pbx ids to the cps if they are of type PBX_Trunking
			for (Contracted_Products__c cp : newContractedProducts) {
				if (cpIdToProductId.containsKey(cp.Id)) {
					cp.PBX__c = siteIdToPbxId.get(cp.Site__c);
					// if there's a One Net proposition, set the one net pbx
					if (
						cp.Proposition__c != null &&
						cp.Proposition__c.contains('One Net') &&
						acctIdToOneNetPbxId.containsKey(siteIdToAccountId.get(cp.Site__c))
					) {
						cp.PBX__c = acctIdToOneNetPbxId.get(siteIdToAccountId.get(cp.Site__c));
					}
				}
			}
		}
	}

	private void populateSitePbxAndCpProductMaps(Map<Id, Id> siteIdToPbxId, Map<Id, Id> cpIdToProductId) {
		for (Contracted_Products__c cp : newContractedProducts) {
			if (cp.Site__c != null && cp.PBX__c == null && CONSTANTS.familyTagsThatRequirePBX.contains(cp.Unify_Product_Family__c)) {
				siteIdToPbxId.put(cp.Site__c, null);
				cpIdToProductId.put(cp.Id, cp.Product__c);
			}
		}
	}

	private void updPopSitePbxAndSiteAccountMaps(Map<Id, Id> siteIdToAccountId, Map<Id, Id> siteIdToPbxId) {
		for (Site__c s : [SELECT Id, PBX__c, Site_Account__c FROM Site__c WHERE Id IN :siteIdToPbxId.keySet()]) {
			siteIdToPbxId.put(s.Id, s.PBX__c);
			siteIdToAccountId.put(s.Id, s.Site_Account__c);
		}
	}

	private void populateAccountOneNetPbxMap(Map<Id, Id> acctIdToOneNetPbxId, Map<Id, Id> siteIdToAccountId) {
		for (Competitor_Asset__c ca : [
			SELECT Id, Account__c
			FROM Competitor_Asset__c
			WHERE Account__c IN :siteIdToAccountId.values() AND PBX_type__c = :GeneralUtils.defaultOnenetPBXType
		]) {
			acctIdToOneNetPbxId.put(ca.Account__c, ca.Id);
		}
	}

	/**
	 * @description         This method populates (missing) data based on the already available data
	 * @author              Jurgen van Westreenen
	 */
	private void enrichData() {
		// Verify if there are any Contracted Products that have changed fields important for further calculations
		List<Contracted_Products__c> changedContractedProducts = GeneralUtils.filterChangedRecords(
			newContractedProducts,
			oldContractedProductsMap,
			new List<String>{ 'Gross_List_Price__c', 'Discount__c', 'Quantity__c', 'Duration__c', 'Product__c' },
			false
		);
		if (changedContractedProducts.isEmpty()) {
			return;
		}

		// Prepare Data
		Set<Id> productIds = GeneralUtils.getIDSetFromList(changedContractedProducts, 'Product__c');
		Set<Id> contractIds = GeneralUtils.getIDSetFromList(changedContractedProducts, 'VF_Contract__c');
		Map<Id, Product2> productMap = new Map<Id, Product2>(
			[SELECT Id, BAP_SAP__c, Multiplier_Vodacom__c, Family FROM Product2 WHERE Id IN :productIds]
		);
		Map<Id, VF_Contract__c> contractMap = new Map<Id, VF_Contract__c>(
			[SELECT Id, (SELECT Id, Proposition__c, Family_Condition__c FROM Contracted_Products__r) FROM VF_Contract__c WHERE Id IN :contractIds]
		);

		for (Contracted_Products__c cp : newContractedProducts) {
			Product2 prod = productMap.get(cp.Product__c);

			if (prod != null) {
				if (oldContractedProductsMap == null) {
					handleCpInsert(cp, prod, contractMap);
				} else {
					handleCpUpdate(cp, prod);
				}
			}
		}
	}

	private void handleCpInsert(Contracted_Products__c cp, Product2 prod, Map<Id, VF_Contract__c> contractMap) {
		ContProdPricesWrapper prices = new contProdPricesWrapper(cp, prod);

		// Update all fields
		cp.List_price__c = prod.BAP_SAP__c;
		cp.Net_Unit_Price__c = prices.netprice;
		cp.ARPU_Value__c = prices.netprice;
		// only fixed products get added so we can default these (if they are empty)
		if (String.isBlank(cp.Fixed_Mobile__c)) {
			cp.Fixed_Mobile__c = 'Fixed';
		}
		if (String.isBlank(cp.Proposition_Type__c)) {
			cp.Proposition_Type__c = 'FIXED';
		}
		cp.Multiplier_Vodacom__c = prod.Multiplier_Vodacom__c;
		cp.UnitPrice__c = prices.unitprice;
		cp.Subtotal__c = prices.subtotal;
		cp.Product_Family__c = prod.Family;

		// Check the existence of any Proposition on siblings which can be used to update fields
		for (Contracted_Products__c otherCP : contractMap.get(cp.VF_Contract__c).Contracted_Products__r) {
			if (String.isNotBlank(otherCP.Proposition__c)) {
				cp.Proposition__c = otherCP.Proposition__c;
				cp.Family_Condition__c = otherCP.Family_Condition__c;
				break;
			}
		}
	}

	private void handleCpUpdate(Contracted_Products__c cp, Product2 prod) {
		Contracted_Products__c oldCp = oldContractedProductsMap.get(cp.Id);
		ContProdPricesWrapper prices = new contProdPricesWrapper(cp, prod);

		if (
			cp.Gross_List_Price__c != oldCp.Gross_List_Price__c ||
			cp.Discount__c != oldCp.Discount__c ||
			cp.Quantity__c != oldCp.Quantity__c ||
			cp.Duration__c != oldCp.Duration__c
		) {
			cp.Net_Unit_Price__c = prices.netprice;
			cp.ARPU_Value__c = prices.netprice;
			cp.UnitPrice__c = prices.unitprice;
			cp.Subtotal__c = prices.subtotal;
		}
		if (cp.Product__c != oldCp.Product__c) {
			cp.List_price__c = prod.BAP_SAP__c;
			cp.Multiplier_Vodacom__c = prod.Multiplier_Vodacom__c;
			cp.UnitPrice__c = prices.unitprice;
			cp.Subtotal__c = prices.subtotal;
			cp.Product_Family__c = prod.Family;
		}
	}

	public static Decimal calculateNetPrice(Contracted_Products__c cp) {
		return (cp.Gross_List_Price__c == null ? 0 : cp.Gross_List_Price__c) * (1 - (cp.Discount__c == null ? 0 : (cp.Discount__c / 100)));
	}

	public static Decimal calculateUnitPrice(Product2 prod, Contracted_Products__c cp) {
		return (prod.BAP_SAP__c == null ? 0 : prod.BAP_SAP__c) * (cp.Duration__c == null ? 1 : cp.Duration__c);
	}

	public static Decimal calculateSubtotal(Product2 prod, Contracted_Products__c cp, Decimal netprice) {
		return (prod.Multiplier_Vodacom__c == null ? 1 : prod.Multiplier_Vodacom__c) *
			netprice *
			(cp.Duration__c == null ? 1 : cp.Duration__c) *
			(cp.Quantity__c == null ? 1 : cp.Quantity__c);
	}

	/**
	 * @description         This method calculates the number of sites in the contract and updates the contract
	 * @author              Guy Clairbois
	 */
	private void calculateNumberOfSitesInContract() {
		// Collect all Contracts that should be cheked
		Set<Id> contractIds = new Set<Id>();
		for (Contracted_Products__c cp : newContractedProducts) {
			if (GeneralUtils.isRecordFieldChanged(cp, oldContractedProductsMap, new List<String>{ 'VF_Contract__c', 'Site__c' }, false)) {
				contractIds.add(cp.VF_Contract__c);

				if (oldContractedProductsMap != null && GeneralUtils.isRecordFieldChanged(cp, oldContractedProductsMap, 'VF_Contract__c')) {
					contractIds.add(oldContractedProductsMap.get(cp.Id).VF_Contract__c);
				}
			}
		}
		// Remove nulls
		contractIds.remove(null);

		if (!contractIds.isEmpty()) {
			// Aggregate Query
			List<AggregateResult> sitesPerContract = [
				SELECT COUNT_DISTINCT(Site__c) SiteCount, VF_Contract__c
				FROM Contracted_Products__c
				WHERE VF_Contract__c IN :contractIds
				GROUP BY VF_Contract__c
			];
			List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
			for (AggregateResult contractSites : sitesPerContract) {
				contractsToUpdate.add(
					new VF_Contract__c(
						Id = (Id) contractSites.get('VF_Contract__c'),
						Number_of_sites_in_contract__c = (Decimal) contractSites.get('SiteCount')
					)
				);
			}
			SharingUtils.updateRecordsWithoutSharing(contractsToUpdate);
		}
	}

	/**
	 * @description         This method aggregates the family_condition values to relevant contracts and orders
	 * @author              Guy Clairbois
	 */
	private void rollupFamilyConditions() {
		// Skip this method if flag is turned off
		if (!TriggerFlagsService.getFlag('ContractedProductsTriggerHandler', 'rollupFamilyConditions')) {
			return;
		}

		Set<Id> contractsToCheck = new Set<Id>();
		Set<Id> ordersToCheck = new Set<Id>();

		// first collect all family conditions per contract / order
		for (Contracted_Products__c cp : newContractedProducts) {
			if (
				oldContractedProductsMap == null ||
				cp.VF_Contract__c != oldContractedProductsMap.get(cp.Id).VF_Contract__c ||
				cp.Order__c != oldContractedProductsMap.get(cp.Id).Order__c ||
				cp.Family_Condition__c != oldContractedProductsMap.get(cp.Id).Family_Condition__c
			) {
				// make sure that contracts and orders that don't have a remaining family condition get an empty field
				populateContractsAndOrdersToCheck(cp, contractsToCheck, ordersToCheck);
			}
		}
		rollupFamilyConditions(contractsToCheck, ordersToCheck);
	}

	// Rolls up Family Condition to Contract and Order based on values on the related Contracted Product records.
	public void rollupFamilyConditions(Set<Id> contractsToCheck, Set<Id> ordersToCheck) {
		if (!contractsToCheck.isEmpty() || !ordersToCheck.isEmpty()) {
			Map<Id, Set<String>> contractIdToFamilyConditions = new Map<Id, Set<String>>();
			Map<Id, Set<String>> orderIdToFamilyConditions = new Map<Id, Set<String>>();

			for (Contracted_Products__c cp : [
				SELECT Id, Order__c, VF_Contract__c, Family_Condition__c
				FROM Contracted_Products__c
				WHERE VF_Contract__c IN :contractsToCheck OR Order__c IN :ordersToCheck
			]) {
				populateFamilyConditions(cp, contractIdToFamilyConditions, orderIdToFamilyConditions);
			}
			doContractAndOrderUpdates(contractIdToFamilyConditions, orderIdToFamilyConditions);
		}
	}

	private void populateContractsAndOrdersToCheck(Contracted_Products__c cp, Set<Id> contractsToCheck, Set<Id> ordersToCheck) {
		if (oldContractedProductsMap != null && oldContractedProductsMap.get(cp.Id).VF_Contract__c != null) {
			contractsToCheck.add(oldContractedProductsMap.get(cp.Id).VF_Contract__c);
		}
		if (cp.VF_Contract__c != null) {
			contractsToCheck.add(cp.VF_Contract__c);
		}
		if (oldContractedProductsMap != null && oldContractedProductsMap.get(cp.Id).Order__c != null) {
			ordersToCheck.add(oldContractedProductsMap.get(cp.Id).Order__c);
		}
		if (cp.Order__c != null) {
			ordersToCheck.add(cp.Order__c);
		}
	}

	private void populateFamilyConditions(
		Contracted_Products__c cp,
		Map<Id, Set<String>> contractIdToFamilyConditions,
		Map<Id, Set<String>> orderIdToFamilyConditions
	) {
		if (cp.VF_Contract__c != null) {
			if (!contractIdToFamilyConditions.containsKey(cp.VF_Contract__c)) {
				contractIdToFamilyConditions.put(cp.VF_Contract__c, new Set<String>());
			}
			if (cp.Family_Condition__c != null) {
				contractIdToFamilyConditions.get(cp.VF_Contract__c).add(cp.Family_Condition__c);
			}
		}
		if (cp.Order__c != null) {
			if (!orderIdToFamilyConditions.containsKey(cp.Order__c)) {
				orderIdToFamilyConditions.put(cp.Order__c, new Set<String>());
			}
			if (cp.Family_Condition__c != null) {
				orderIdToFamilyConditions.get(cp.Order__c).add(cp.Family_Condition__c);
			}
		}
	}

	private void doContractAndOrderUpdates(Map<Id, Set<String>> contractIdToFamilyConditions, Map<Id, Set<String>> orderIdToFamilyConditions) {
		if (!contractIdToFamilyConditions.isEmpty()) {
			List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();

			for (Id contractId : contractIdToFamilyConditions.keySet()) {
				String fcs = '';

				for (String s : contractIdToFamilyConditions.get(contractId)) {
					fcs += s + ';';
				}
				contractsToUpdate.add(new VF_Contract__c(Id = contractId, Family_Conditions__c = fcs));
			}
			SharingUtils.updateRecordsWithoutSharing(contractsToUpdate);
		}
		if (!orderIdToFamilyConditions.isEmpty()) {
			List<Order__c> ordersToUpdate = new List<Order__c>();

			for (Id orderId : orderIdToFamilyConditions.keySet()) {
				String fcs = '';

				for (String s : orderIdToFamilyConditions.get(orderId)) {
					fcs += s + ';';
				}
				ordersToUpdate.add(new Order__c(Id = orderId, Family_Conditions__c = fcs));
			}
			SharingUtils.updateRecordsWithoutSharing(ordersToUpdate);
		}
	}

	/**
	 * @description         Sends the records to ChangePBXOrderBatch for notifying SIAS about the change on PBX
	 * @author              Rahul Sharma
	 */
	private void triggerChangePabx() {
		Set<Id> cpIdsForChangePbx = new Set<Id>();
		for (Contracted_Products__c newCp : newContractedProducts) {
			Contracted_Products__c oldCp = oldContractedProductsMap.get(newCp.Id);
			// check records have delivery status as approved which is tied to a contracted product
			if (
				newCp.Delivery_Status__c == Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED &&
				newCp.Delivery_Status__c != oldCp.Delivery_Status__c &&
				String.isNotEmpty(newCp.PBX__c) &&
				newCp.Do_not_export__c == false
			) {
				cpIdsForChangePbx.add(newCp.Id);
			}
		}

		if (!cpIdsForChangePbx.isEmpty()) {
			// As 'Database.executeBatch' is treated as DML due to which callout fails, run the batch's code in future context
			if (Test.isRunningTest()) {
				ChangePBXOrderBatch.executeCalloutAsFuture(cpIdsForChangePbx);
			} else {
				// batch size is 1 to avoid 'System.calloutException' as the service processes one contracted product at a time
				// batch will query all cps which has a competetitor asset with PBX Id so as to send change request to SIAS
				Database.executeBatch(new ChangePBXOrderBatch(cpIdsForChangePbx), 1);
			}
		}
	}

	/**
	 * @description         Sends the records to DeliveryExport/ECSDeliveryService for notifying BOP that billing has processed
	 */
	private void triggerEcsDelivery() {
		if (!TriggerFlagsService.getFlag('ContractedProductsTriggerHandler', 'triggerEcsDelivery')) {
			return;
		}

		Set<Id> orderIdSet = new Set<Id>();
		Set<Id> contProdIdSet = new Set<Id>();
		Map<Id, Order__c> ordersMap = new Map<Id, Order__c>();

		for (Contracted_Products__c newCp : newContractedProducts) {
			if (newCp.Order__c != null) {
				ordersMap.put(newCp.Order__c, new Order__c());
			}
		}

		ordersMap = new Map<Id, Order__c>([SELECT Id, O2C_Order__c, Status__c FROM Order__c WHERE Id IN :ordersMap.keySet()]);

		populateOrderContProdSets(orderIdSet, contProdIdSet, ordersMap);

		// run the delivery export
		//      1. SIAS sends a callback with provide invoice info to set the status to processed or failed.
		//      2. In case of No or special billing, SIAS batch calls delivery export directly
		if (!orderIdSet.isEmpty() && !System.isBatch()) {
			System.enqueueJob(new DeliveryExport(orderIdSet, contProdIdSet, 'update'));
		}
	}

	private void populateOrderContProdSets(Set<Id> orderIdSet, Set<Id> contProdIdSet, Map<Id, Order__c> ordersMap) {
		Set<String> orderStatusSet = new Set<String>{ Constants.ORDER_STATUS_ACCEPTED };
		Set<String> billingStatusSet = new Set<String>{
			Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW,
			Constants.CONTRACTED_PRODUCT_BILLING_STATUS_PROCESSED,
			Constants.CONTRACTED_PRODUCT_BILLING_STATUS_FAILED,
			Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NO_BILLING,
			Constants.CONTRACTED_PRODUCT_BILLING_STATUS_SPECIAL_BILLING
		};

		List<String> billingStatus = new List<String>{
			Constants.CONTRACTED_PRODUCT_BILLING_STATUS_SPECIAL_BILLING,
			Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NO_BILLING,
			Constants.CONTRACTED_PRODUCT_BILLING_STATUS_PROCESSED
		};

		for (Contracted_Products__c newCp : newContractedProducts) {
			Order__c parentOrder = ordersMap.get(newCp.Order__c);
			Contracted_Products__c oldCp = getOldCp(newCp);

			if (
				billingStatusSet.contains(newCp.Billing_Status__c) &&
				(GeneralUtils.isRecordFieldChanged(newCp, oldContractedProductsMap, 'Billing_Status__c') ||
				// Enable billing confirmation retry from BOP
				isStatusProcessedApproved(newCp, oldCp, billingStatus)) &&
				String.isNotEmpty(newCp.Order__c) &&
				newCp.Do_not_export__c == false &&
				parentOrder.O2C_Order__c &&
				orderStatusSet.contains(parentOrder.Status__c)
			) {
				orderIdSet.add(newCp.Order__c);
				contProdIdSet.add(newCp.Id);
			}
		}
	}

	private Contracted_Products__c getOldCp(Contracted_Products__c newCp) {
		return oldContractedProductsMap != null ? oldContractedProductsMap.get(newCp.Id) : null;
	}

	private Boolean isStatusProcessedApproved(Contracted_Products__c newCp, Contracted_Products__c oldCp, List<String> billingStatus) {
		return billingStatus.contains(newCp.Billing_Status__c) &&
			newCp.Delivery_Status__c == Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED &&
			newCp.Delivery_Status__c != oldCp.Delivery_Status__c;
	}

	/**
	 * @description         This method updates the ContractedproductsReady__c flag on the Order
	 */
	private void updateOrderReadyFlag() {
		Map<Id, Boolean> orderToFlagMap = new Map<Id, Boolean>();
		List<Order__c> ordersToUpdate = new List<Order__c>();
		List<String> billingStatus = new List<String>{ 'Special Billing', 'No Billing', 'Processed' };
		List<String> orderIdList = new List<String>();

		// first collect all order related to the cp's
		collectCpRelatesOrders(orderIdList);

		if (!orderIdList.isEmpty()) {
			// Then make a map from order to Billing Statuses
			Map<Id, List<String>> orderToCpMap = new Map<Id, List<String>>();
			populateOrderToCpMap(orderIdList, orderToCpMap);

			// Verify if all Billling Statuses are "Done"
			verifyBillingStatuses(orderToCpMap, billingStatus, orderToFlagMap);

			// Update the Order accordingly
			for (Id ordId : orderToFlagMap.keySet()) {
				Order__c ord = new Order__c(Id = ordId, ContractedproductsReady__c = orderToFlagMap.get(ordId));
				ordersToUpdate.add(ord);
			}
			if (!ordersToUpdate.isEmpty()) {
				update ordersToUpdate;
			}
		}
	}

	private void collectCpRelatesOrders(List<String> orderIdList) {
		for (Contracted_Products__c cp : newContractedProducts) {
			if (oldContractedProductsMap == null || cp.Billing_Status__c != oldContractedProductsMap.get(cp.Id).Billing_Status__c) {
				if (cp.Order__c != null) {
					orderIdList.add(cp.Order__c);
				}
			}
		}
	}

	private void populateOrderToCpMap(List<String> orderIdList, Map<Id, List<String>> orderToCpMap) {
		for (Contracted_Products__c contProd : [SELECT Id, Billing_Status__c, Order__c FROM Contracted_Products__c WHERE Order__c IN :orderIdList]) {
			if (orderToCpMap.containsKey(contProd.Order__c)) {
				orderToCpMap.get(contProd.Order__c).add(contProd.Billing_Status__c);
			} else {
				orderToCpMap.put(contProd.Order__c, new List<String>{ contProd.Billing_Status__c });
			}
		}
	}

	private void verifyBillingStatuses(Map<Id, List<String>> orderToCpMap, List<String> billingStatus, Map<Id, Boolean> orderToFlagMap) {
		for (Id oId : orderToCpMap.keySet()) {
			Boolean greenFlag = true;
			for (String billStat : orderToCpMap.get(oId)) {
				if (!billingStatus.contains(billStat)) {
					greenFlag = false;
				}
			}
			orderToFlagMap.put(oId, greenFlag);
		}
	}

	private void linkCTNS() {
		Map<Id, Contracted_Products__c> cpsByLineItem = new Map<Id, Contracted_Products__c>();
		for (Contracted_Products__c cp : newContractedProducts) {
			if (cp.OpportunityLineItemId__c != null) {
				cpsByLineItem.put(cp.OpportunityLineItemId__c, cp);
			}
		}

		List<NetProfit_CTN__c> ctns = [
			SELECT Id, Contracted_Product__c, OpportunityLineItem__c
			FROM NetProfit_CTN__c
			WHERE OpportunityLineItem__c IN :cpsByLineItem.keySet()
		];

		for (NetProfit_CTN__c ctn : ctns) {
			ctn.Contracted_Product__c = cpsByLineItem.get(ctn.OpportunityLineItem__c).Id;
		}

		update ctns;
	}

	public class ContProdPricesWrapper {
		public Decimal netPrice { get; set; }
		public Decimal unitPrice { get; set; }
		public Decimal subtotal { get; set; }

		public contProdPricesWrapper(Contracted_Products__c cp, Product2 prod) {
			netPrice = ContractedProductsTriggerHandler.calculateNetPrice(cp);
			unitPrice = ContractedProductsTriggerHandler.calculateUnitPrice(prod, cp);
			subtotal = ContractedProductsTriggerHandler.calculateSubtotal(prod, cp, netprice);
		}
	}
}
