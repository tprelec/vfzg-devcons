@isTest
public with sharing class TestCOM_DeliveryComponentService {
	@isTest
	private static void testGetDeliveryArticleMaterials() {
		COM_Delivery_Article__mdt article = [SELECT Id FROM COM_Delivery_Article__mdt WHERE DeveloperName = 'TestArticle'];

		COM_DeliveryComponentService dcs = new COM_DeliveryComponentService();

		Test.startTest();
		List<COM_Delivery_Article_Materials__mdt> result = dcs.getDeliveryArticleMaterials(new Set<Id>{ article.Id });
		Test.stopTest();

		System.assertEquals(1, result.size(), 'The record was not fetched properly.');
		System.assertNotEquals(null, result[0].Id, 'The field was not fetched properly.');
		System.assertNotEquals(null, result[0].DeveloperName, 'The field was not fetched properly.');
		System.assertNotEquals(null, result[0].COM_Delivery_Article__c, 'The field was not fetched properly.');
		System.assertNotEquals(null, result[0].COM_Delivery_Article__r.Delivery_Article_Name__c, 'The field was not fetched properly.');
		System.assertNotEquals(null, result[0].COM_Delivery_Materials__c, 'The field was not fetched properly.');
		System.assertNotEquals(null, result[0].COM_Delivery_Materials__r.DeveloperName, 'The field was not fetched properly.');
		System.assertNotEquals(null, result[0].COM_Delivery_Materials__r.Model_Name__c, 'The field was not fetched properly.');
		System.assertNotEquals(null, result[0].COM_Delivery_Materials__r.Generate_Asset__c, 'The field was not fetched properly.');
		System.assertNotEquals(null, result[0].COM_Delivery_Materials__r.Type__c, 'The field was not fetched properly.');
	}

	@isTest
	private static void testGetDeliveryArticlesMap() {
		COM_Delivery_Article__mdt article = [SELECT Id FROM COM_Delivery_Article__mdt WHERE DeveloperName = 'TestArticle'];

		COM_DeliveryComponentService dcs = new COM_DeliveryComponentService();

		Test.startTest();
		Map<Id, COM_Delivery_Article__mdt> result = dcs.getDeliveryArticlesMap(new Set<Id>{ article.Id });
		Test.stopTest();

		System.assertNotEquals(null, result.get(article.Id), 'The record was not fetched properly.');
		System.assertNotEquals(null, result.get(article.Id).Id, 'The field was not fetched properly.');
		System.assertNotEquals(null, result.get(article.Id).Label, 'The field was not fetched properly.');
		System.assertNotEquals(null, result.get(article.Id).DeveloperName, 'The field was not fetched properly.');
		System.assertNotEquals(null, result.get(article.Id).Delivery_Article_Code__c, 'The field was not fetched properly.');
		System.assertNotEquals(null, result.get(article.Id).Delivery_Article_Name__c, 'The field was not fetched properly.');
		System.assertNotEquals(null, result.get(article.Id).Delivery_Component__c, 'The field was not fetched properly.');
	}
}
