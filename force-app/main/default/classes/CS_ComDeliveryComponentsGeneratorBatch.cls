global class CS_ComDeliveryComponentsGeneratorBatch implements Database.Batchable<sObject>{

    global List<Id> orderIds = new List<Id>();
    global CS_ComDeliveryComponentsGeneratorBatch(List<Id> orderIds){
        this.orderIds = orderIds;
    }

    global void execute(Database.BatchableContext param1, List<SObject> param2) {
        Set<Id> orderIdSet = new Set<Id>();
        for (SObject sobjectRecord : param2) {
            orderIdSet.add(sobjectRecord.Id);
        }

        //CS_ComDeliveryComponentsGenerator generator = new CS_ComDeliveryComponentsGenerator(orderIdSet);
        //generator.run();
        
        COM_DeliveryComponentsGenerator generator = new COM_DeliveryComponentsGenerator();
        generator.run(orderIdSet);
    }

    global void finish(Database.BatchableContext param1) {
        List<COM_Delivery_Order__c> deliveryOrders = [
            SELECT Id, Status__c
            FROM COM_Delivery_Order__c
            WHERE Order__c in :this.orderIds AND Parent_Delivery_Order__c = null];

        for(COM_Delivery_Order__c deliveryOrder : deliveryOrders) {
            deliveryOrder.Status__c = 'Decomposed';
        }

        update deliveryOrders;
        
        List<csord__Order__c> ordersToUpdate = new List<csord__Order__c>();

        for (Id orderId : this.orderIds) {
            ordersToUpdate.add(new csord__Order__c(Id = orderId,
                    csord__status2__c = 'In Delivery'));
        }

        update ordersToUpdate;
    }

    global Iterable<sObject> start(Database.BatchableContext param1) {
        List<csord__Order__c> ordersList = [SELECT Id FROM csord__Order__c WHERE Id IN :this.orderIds];
        return ordersList;
    }
}