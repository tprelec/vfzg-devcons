/*
	Author: Rahul Sharma
	Description: Controller responsible to get and save data for LWC com_selectContactRole component
	Jira ticket: COM-3956
*/

public with sharing class Com_SelectContactRoleController {
	@TestVisible
	private static final String ROLE_MAIN = 'main';
	@TestVisible
	private static final String ROLE_MAINTENANCE = 'maintenance';
	@TestVisible
	private static final String ROLE_INCIDENT = 'incident';
	@TestVisible
	private static final String ROLE_CHOOSER = 'chooser';

	@TestVisible
	private static final List<String> CONTACT_ROLES_VALUES = new List<String>{ ROLE_MAIN, ROLE_MAINTENANCE, ROLE_INCIDENT, ROLE_CHOOSER };

	@AuraEnabled(cacheable=true)
	public static LightningResponse getData(Id opportunityId) {
		try {
			LoggerService.log(LoggingLevel.DEBUG, '@@@@opportunityId: ' + opportunityId);

			List<Opportunity> opportunities = [SELECT AccountId FROM Opportunity WHERE Id = :opportunityId];

			if (opportunities.isEmpty() || String.isEmpty(opportunities[0].AccountId)) {
				return new LightningResponse().setError(System.Label.Com_SelectContactRole_NoOpportunityMessage);
			}

			RolesData rolesData = new RolesData();

			rolesData.accountId = opportunities[0].AccountId;
			for (Contact_Role__c contactRole : getContactRoles(rolesData.accountId)) {
				if (contactRole.Type__c == ROLE_MAIN) {
					rolesData.customerMainContactId = contactRole.Contact__c;
					rolesData.customerMainContactRoleId = contactRole.Id;
				} else if (contactRole.Type__c == ROLE_MAINTENANCE) {
					rolesData.customerMaintenanceContactId = contactRole.Contact__c;
					rolesData.customerMaintenanceContactRoleId = contactRole.Id;
				} else if (contactRole.Type__c == ROLE_INCIDENT) {
					rolesData.customerIncidentContactId = contactRole.Contact__c;
					rolesData.customerIncidentContactRoleId = contactRole.Id;
				} else if (contactRole.Type__c == ROLE_CHOOSER) {
					rolesData.customerChooserContactId = contactRole.Contact__c;
					rolesData.customerChooserContactRoleId = contactRole.Id;
				}
			}

			return new LightningResponse().setBody(rolesData);
		} catch (Exception objEx) {
			return setError(null, objEx.getMessage());
		}
	}

	@AuraEnabled
	public static LightningResponse save(RolesData rolesData) {
		Savepoint savepoint = Database.setSavePoint();
		try {
			Map<String, Id> mapSelectedTypeWithContactId = new Map<String, Id>{
				ROLE_MAIN => rolesData.customerMainContactId,
				ROLE_MAINTENANCE => rolesData.customerMaintenanceContactId,
				ROLE_INCIDENT => rolesData.customerIncidentContactId,
				ROLE_CHOOSER => rolesData.customerChooserContactId
			};

			LoggerService.log(LoggingLevel.DEBUG, '@@@@mapSelectedTypeWithContactId: ' + mapSelectedTypeWithContactId);

			List<Contact_Role__c> contactRolesToUpsert = getChosenRolesToActivate(rolesData, mapSelectedTypeWithContactId);
			upsert contactRolesToUpsert;

			List<Contact_Role__c> contactRolesToInactivate = getRolesToDeactivate(
				contactRolesToUpsert,
				mapSelectedTypeWithContactId,
				rolesData.accountId
			);

			update contactRolesToInactivate;

			for (Contact_Role__c contactRole : getContactRoles(rolesData.accountId)) {
				List<String> errors = OrderBillingValidation.getAllErrors(contactRole.Id, contactRole.Contact__c);
				LoggerService.log(LoggingLevel.DEBUG, '@@@@errors: ' + contactRole.Id + '----' + errors);
				if (!errors.isEmpty()) {
					return new LightningResponse().setError(System.Label.Com_SelectContactRole_ContactValidationError);
				}
			}

			return new LightningResponse().setSuccess(System.Label.Com_SelectContactRole_SaveSuccessMessage);
		} catch (DMLException objEx) {
			return setError(savepoint, objEx.getDmlMessage(0));
		} catch (Exception objEx) {
			LoggerService.log(LoggingLevel.DEBUG, '@@@@objEx.getStackTraceString(): ' + objEx.getStackTraceString());
			return setError(savepoint, objEx.getMessage());
		}
	}

	private static List<Contact_Role__c> getContactRoles(Id accountId) {
		return [
			SELECT Id, Type__c, Contact__c
			FROM Contact_Role__c
			WHERE Account__c = :accountId AND Type__c IN :CONTACT_ROLES_VALUES AND Active__c = TRUE
		];
	}

	private static List<Contact_Role__c> getRolesToDeactivate(
		List<Contact_Role__c> activeContactRoles,
		Map<String, Id> mapSelectedTypeWithContactId,
		Id accountId
	) {
		List<Contact_Role__c> contactRolesToInactivate = new List<Contact_Role__c>();

		for (Contact_Role__c contactRole : [
			SELECT Id, Contact__c, Type__c
			FROM Contact_Role__c
			WHERE Account__c = :accountId AND Id NOT IN :activeContactRoles AND Active__c = TRUE
		]) {
			if (
				mapSelectedTypeWithContactId.containsKey(contactRole.Type__c) &&
				mapSelectedTypeWithContactId.get(contactRole.Type__c) == contactRole.Contact__c
			) {
				continue;
			}

			contactRolesToInactivate.add(new Contact_Role__c(Id = contactRole.Id, Active__c = false));
		}

		LoggerService.log(LoggingLevel.DEBUG, '@@@@contactRolesToInactivate: ' + JSON.serialize(contactRolesToInactivate));
		return contactRolesToInactivate;
	}

	private static List<Contact_Role__c> getChosenRolesToActivate(RolesData rolesData, Map<String, Id> mapSelectedTypeWithContactId) {
		List<Contact_Role__c> contactRolesToUpsert = new List<Contact_Role__c>();

		Map<String, Contact_Role__c> mapContactRole = prepareMapOfContactRoles(rolesData.accountId);

		for (String type : mapSelectedTypeWithContactId.keySet()) {
			Id contactId = mapSelectedTypeWithContactId.get(type);
			if (String.isNotEmpty(contactId)) {
				String key = prepareContactRoleKey(contactId, type);
				if (mapContactRole.containsKey(key)) {
					Contact_Role__c contactRole = mapContactRole.get(key);
					if (!contactRole.Active__c) {
						contactRolesToUpsert.add(new Contact_Role__c(Id = contactRole.Id, Active__c = true));
					}
				} else {
					contactRolesToUpsert.add(
						new Contact_Role__c(Account__c = rolesData.accountId, Active__c = true, Contact__c = contactId, Type__c = type)
					);
				}
			}
		}
		LoggerService.log(LoggingLevel.DEBUG, '@@@@contactRolesToUpsert: ' + JSON.serialize(contactRolesToUpsert));
		return contactRolesToUpsert;
	}

	private static Map<String, Contact_Role__c> prepareMapOfContactRoles(Id accountId) {
		Map<String, Contact_Role__c> mapContactRole = new Map<String, Contact_Role__c>();

		for (Contact_Role__c contactRole : [
			SELECT Id, Active__c, Contact__c, Account__c, Type__c
			FROM Contact_Role__c
			WHERE Account__c = :accountId AND Contact__c != NULL AND Type__c IN :CONTACT_ROLES_VALUES
		]) {
			String key = prepareContactRoleKey(contactRole.Contact__c, contactRole.Type__c);
			mapContactRole.put(key, contactRole);
		}
		return mapContactRole;
	}

	private static String prepareContactRoleKey(Id contactId, String role) {
		return contactId + '-' + role;
	}

	@TestVisible
	private static LightningResponse setError(Savepoint savepoint, String message) {
		if (savepoint != null) {
			Database.rollback(savepoint);
		}
		return new LightningResponse().setError(message);
	}

	public class RolesData {
		@AuraEnabled
		public Id accountId { get; set; }

		@AuraEnabled
		public Id customerMainContactId { get; set; }

		@AuraEnabled
		public Id customerMaintenanceContactId { get; set; }

		@AuraEnabled
		public Id customerIncidentContactId { get; set; }

		@AuraEnabled
		public Id customerChooserContactId { get; set; }

		@AuraEnabled
		public Id customerMainContactRoleId { get; set; }

		@AuraEnabled
		public Id customerMaintenanceContactRoleId { get; set; }

		@AuraEnabled
		public Id customerIncidentContactRoleId { get; set; }

		@AuraEnabled
		public Id customerChooserContactRoleId { get; set; }

		// empty constructor is required for setting data
		@SuppressWarnings('PMD.EmptyStatementBlock')
		public RolesData() {
		}

		// used for setting data via test class
		public RolesData(
			Id accountId,
			Id customerMainContactId,
			Id customerMaintenanceContactId,
			Id customerIncidentContactId,
			Id customerChooserContactId
		) {
			this.accountId = accountId;
			this.customerMainContactId = customerMainContactId;
			this.customerMaintenanceContactId = customerMaintenanceContactId;
			this.customerIncidentContactId = customerIncidentContactId;
			this.customerChooserContactId = customerChooserContactId;
		}
	}
}
