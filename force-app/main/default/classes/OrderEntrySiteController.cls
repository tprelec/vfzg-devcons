public with sharing class OrderEntrySiteController {
	/**
	 * @description Gets Account Sites
	 * @param  accId Account Id
	 * @return       List of Account Sites.
	 */
	@AuraEnabled
	public static List<Site__c> getSites(Id accId) {
		return [
			SELECT Id, Site_Street__c, Site_Postal_Code__c, Site_House_Number__c, Site_City__c, Site_House_Number_Suffix__c
			FROM Site__c
			WHERE Site_Account__c = :accId
		];
	}

	/**
	 * @description Gets Site
	 * @param  siteId Site ID.
	 * @return        Site
	 */
	@AuraEnabled
	public static Site__c getSite(Id siteId) {
		return [
			SELECT Id, Site_Street__c, Site_Postal_Code__c, Site_House_Number__c, Site_City__c, Site_House_Number_Suffix__c
			FROM Site__c
			WHERE Id = :siteId
		];
	}

	/**
	 * @description Executes Site/Address Check
	 * @param  postCode    Postcode
	 * @param  houseNumber House Number
	 * @param  extension   House Number Extension
	 * @return             Address Check Result
	 */
	@AuraEnabled
	public static String checkSite(String postCode, Integer houseNumber, String extension) {
		Map<String, String> params = new Map<String, String>();
		params.put(AddressCheckService.POSTAL_CODE, postCode.trim());
		params.put(AddressCheckService.HOUSE_NUMBER, String.valueOf(houseNumber).trim());
		if (extension != '') {
			params.put(AddressCheckService.HOUSE_NUMBER_EXT, extension);
		}
		String result = AddressCheckService.callAddressCheckService(params);
		return result;
	}

	/**
	 * @description Updates Site
	 * @param  updatedSite   Site to be updated
	 * @param  opportunityId Opportunity ID
	 * @return               return description
	 */
	@AuraEnabled
	public static Boolean updateSite(Site__c updatedSite, Id opportunityId) {
		List<String> fields = new List<String>{
			'Site_Street__c',
			'Site_Postal_Code__c',
			'Site_House_Number__c',
			'Site_City__c',
			'Site_House_Number_Suffix__c'
		};
		Boolean updateSite = false;
		Site__c site = getSite(updatedSite.Id);

		for (String field : fields) {
			if (site.get(field) != updatedSite.get(field)) {
				site.put(field, updatedSite.get(field));
				updateSite = true;
			}
		}

		if (updateSite) {
			update site;
		}

		updateOpportunityAddress(site, opportunityId);

		return true;
	}

	/**
	 * @description Updates Address fields on Opportunity
	 * @param  site          Site
	 * @param  opportunityId Opportunity ID
	 */
	public static void updateOpportunityAddress(Site__c site, Id opportunityId) {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		Opportunity opp = [SELECT Id FROM Opportunity WHERE Id = :opportunityId];
		opp.LG_InstallationCity__c = site.Site_City__c;
		opp.LG_InstallationHouseNumber__c = String.valueOf(site.Site_House_Number__c);
		opp.LG_InstallationHouseNumberExtension__c = site.Site_House_Number_Suffix__c;
		opp.LG_InstallationPostalCode__c = site.Site_Postal_Code__c;
		opp.LG_InstallationStreet__c = site.Site_Street__c;
		opp.Installation_Adres__c =
			site.Site_Street__c +
			' ' +
			String.valueOf(site.Site_House_Number__c) +
			' ' +
			(String.isBlank(site.Site_House_Number_Suffix__c) ? '' : site.Site_House_Number_Suffix__c + ', ') +
			site.Site_Postal_Code__c +
			' ' +
			site.Site_City__c;
		update opp;
	}

	/**
	 * @description Searches sites via Olbico
	 * @param  zip Postcode
	 * @return     Search results
	 */
	@AuraEnabled
	public static Map<String, String> oblicoSearch(String zip) {
		Map<String, String> addressInformation = new Map<String, String>();
		if (String.isNotBlank(zip) && Pattern.matches('^[0-9]{4}[a-z,A-Z]{2} ?[0-9,a-z,A-Z]*$', zip)) {
			OlbicoServiceJSON jService = new OlbicoServiceJSON();
			jService.setRequestRestJsonStreet(zip);
			jService.makeRequestRestJsonStreet(zip);

			for (SelectOption so : jService.AdresInfo()) {
				addressInformation.put(so.getValue(), so.getLabel());
			}
		}
		return addressInformation;
	}

	/**
	 * @description Saves new Site
	 * @param  selectedAddress Address (as returned by Olbico)
	 * @param  accId           Account ID.
	 * @return                 Created Site.
	 */
	@AuraEnabled
	public static Site__c saveSite(String selectedAddress, Id accId) {
		Account acc = [SELECT Id, KVK_number__c FROM Account WHERE Id = :accId];

		List<String> address = selectedAddress.split('\\|');
		String street = address[0];
		Integer houseNr = Integer.valueof(address[1].Trim());
		String houseNrLetter = address[2];
		String houseNrLetterAddition = address[3];
		String postcode = address[4];
		String cityAddition = address.size() > 6 ? ' - ' + address[6] : '';
		String city = address[5] + cityAddition;

		Site__c newSite = new Site__c();
		newSite.Site_Account__c = acc.Id;
		newSite.KVK_Number__c = acc.KVK_number__c;
		newSite.Site_Street__c = street;
		newSite.Site_House_Number__c = houseNr;
		newSite.Site_House_Number_Suffix__c = houseNrLetter + ' ' + houseNrLetterAddition;
		newSite.Site_Postal_Code__c = postcode;
		newSite.Site_City__c = city;
		newSite.Olbico__c = true;
		newSite.Name = street;

		insert newSite;
		return newSite;
	}
}
