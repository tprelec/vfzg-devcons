public class COM_Constants {
	public static final String READY_FOR_DELIVERY_STATUS = 'Ready for Delivery';
	public static final String CUSTOM_EXCEPTION_MESSAGE_ORCHESTRATION_PROCESS_CREATION = 'Error during orchestration process creation. ';
	public static final String CUSTOM_EXCEPTION_MESSAGE_ORDER_GENERATION = 'Error during order generation. ';
	public static final String CUSTOM_EXCEPTION_MESSAGE_BATCH_EXECUTION = 'Error during batch execution. ';
	public static final String CUSTOM_EXCEPTION_MESSAGE_BATCH_EXECUTION_ID_NULL = 'Batch execution ID is NULL. ';
	public static final Integer DELIVERY_COMPONENTS_GENERATOR_BATCH_SIZE = 200;

	public static final String OPPORTUNITY_CHANGE_TYPE_MOVE = 'Move';
	public static final String PARENT_DELIVERY_ORDER_NAME_PREFIX = '';

	public static final String DELIVERY_ORDER_NAME_PREFIX = '';
	public static final String DELIVERY_ORDER_CREATED_MESSAGE = 'Created';
	public static final String DELIVERY_ORDER_CREATED_STATUS = 'Created';

	public static final String SERVICE_SPECIFICATION_JSON_NAME = 'ServiceSpecification.json';
	public static final String SERVICE_SPECIFICATION_DUMMY_TEXT = 'test';

	public static final String COM_ASSET_RECORD_TYPE = 'COM_Asset';

	public static final String ORDER_TEMPLATE_NAME = 'Parent Delivery Order Process';
	public static final String MOBILE_FLOW_TEMPLATE_NAME = 'Mobile Flow';

	public static final String DELIVERY_COODINATOR_QUEUE_DEV_NAME = 'COM_Delivery_Coordinator';
	public static final String DELIVERY_MAIN_OBJECT = 'COM_Delivery_Order__c';

	public static final String ALL_STAR_SIGN = '*';
	public static final String COMMA_SIGN = ',';
	public static final String DEFAULT_MACD_TYPE = 'Implementation';

	public static final String DELIVERY_ORG_WIDE_EMAIL_ADDRESS = 'delivery@vodafone.nl';
	public static final String REGION_PREFIX = 'COM_region_';
	public static final String TEST_VALIDATION_MSG = 'FIELD_CUSTOM_VALIDATION_EXCEPTION, Task needs to have at least one attachment';
	public static final String TEST_VALIDATION_2_MSG = 'Task with all components successfully implemented should have at least one attachment!';
	public static final String NO_CHANGES_DURING_INFLIGHT_ERROR_MSG = 'No actions to be performed against the Task due to Inflight Change negotiation in place!';
	public static final String IMPLEMENTED_TASK_NEEDS_ATTACHMENTS_ERROR_MSG = 'Task with all components successfully implemented should have at least one attachment!';

	public static final String CDO_MOCK_STATUS_OK = 'OK';
	public static final String CDO_MOCK_STATUS_TIMEOUT = 'Timeout';
	public static final String CDO_MOCK_STATUS_FAIL = 'Fail';
	public static final String CDO_INTEGRATION_STATUS_COMPLETE = 'Complete';
	public static final String CDO_INTEGRATION_STATUS_FAILURE = 'Failure';
	public static final String CDO_ERROR_MSG_TIMEOUT = 'TIMEOUT';
	public static final String CDO_ERROR_MSG_FAIL = 'NETWORK CANNOT DELIVER';
	public static final String CDO_ERROR_MSG_UNKNOWN = 'UNKNOWN PROBLEM';
	public static final String CDO_CREATE_TENANT_PROCESS_NAME = 'Create Tenant';
	public static final String CDO_PROVISION_PROCESS_NAME = 'Provision services';
	public static final String CDO_SUSPEND_PROCESS_NAME = 'Suspend services';
	public static final String CDO_DEPROVISION_PROCESS_NAME = 'Deprovision services';
	public static final String CDO_STATUS_SUSPENDED = 'Suspended';
	public static final String CDO_STATUS_DEPROVISIONED = 'Deprovisioned';
	public static final String CDO_STATUS_PROVISIONED = 'Provisioned';
	public static final String CDO_STATUS_IMPLEMENTED = 'Implemented';

	public static final String MACD_ACTION_ADD = 'Add';
	public static final String MACD_ACTION_CHANGE = 'Change';
	public static final String MACD_ACTION_DELETE = 'Delete';
	public static final String MACD_TYPE_TERMINATION = 'Termination';
	public static final String MACD_TYPE_TERMINATION_DUTCH = 'Afsluiting';
	public static final String MACD_TYPE_NEW_DUTCH = 'Nieuw';

	public static final String MACD_ACTION_COMPONENT_NO_CHANGE = 'N/A';
	public static final String MACD_ACTION_COMPONENT_DELETE = 'Delete';
	public static final String MACD_ACTION_COMPONENT_CHANGE = 'Change';

	public static final String SERVICES_TABLE_START = '<table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;"><tbody>';
	public static final String SERVICES_TABLE_END = '</tbody></table>';
	public static final String SERVICES_TABLE_ROW_START = '<tr><td valign="top" style="width:206pt;padding:0 5.4pt;"><p style="font-size:14pt; font-family:myfont,Arial; color:rgb(74, 77, 78)"><b><span>';
	public static final String SERVICES_TABLE_ROW_MIDDLE = '</span></b></p></td><td valign="top" style="width:206pt;padding:0 5.4pt;"><p style="font-size:14pt; font-family:myfont,Arial; color:rgb(74, 77, 78)"><b><span>';
	public static final String SERVICES_TABLE_ROW_END = '</span></b></p></td></tr>';
	public static final String SERVICES_FILTERED_ROW_START = '<b>';
	public static final String SERVICES_FILTERED_ROW_END = '</b><br/>';
	public static final String SERVICES_DELTA_STATUS_ADD = 'added to subscription';
	public static final String SERVICES_DELTA_STATUS_DELETE = 'deleted from subscription';
	public static final String SERVICES_DELTA_STATUS_CHANGE = 'continuing in subscription';
	public static final String SERVICE_STATUS_TERMINATION_REQUESTED = 'Termination requested';
	public static final String STYLING_TEMPLATE_DEVELOPER_NAME = 'COM_L2O_Styling_Template';
	public static final String ORDER_MANAGEMENT_FOLDER_NAME = 'COM Order Management';
	public static final String TEMPLATE_TYPE_TEXT = 'text';
	public static final String NO_IP_TEXT = '- Een overzicht van de IP-adressen</br>';

	public static final Map<String, String> EMAIL_TEMPLATE_NAME_FILE_NAME_MAP = new Map<String, String>{
		'' => 'COM-styling',
		'COM_Appointment_Installation_Confirmation_Nederlands' => 'COM-appointment-confirmation',
		'COM_Delivery_Complete_Nederlands' => 'COM-delivery-successful',
		'COM_Deinstallation_Confirmation_Nederlands' => 'COM-deinstallation-confirmation',
		'COM_Move_Appointment_Confirmation_Nederlands' => 'COM-move-confirmation'
	};
	public static final String COVER_PICTURE_TAG_START = '<img alt="" class="light-img" data-assetid="127235" height="44" src="';
	public static final String COVER_PICTURE_TAG_END = '" style="display: block; padding: 0px; text-align: center; height: 176px; width: 432px; border: 0px;" width="432"height="176"id="email-cover-photo">';
	public static final String HTTPS_PREFIX = 'https://';
	public static final String SERVLET_SUFIX = '/sfc/servlet.shepherd/version/download/';

	public static final String MACD_FIELD_MAPPING_FIELDSET_NAME = 'COM_MACD_Field_Mapping';
	public static final String MACD_FIELD_MAPPING_FIELDSET_TEST_NAME = 'COM_MACD_Field_Mapping_Test';
	public static final String CUSTOM_SERVICE_QUERY_PART1 = 'SELECT Id,COM_Delivery_Order__r.MACD_move__c,COM_Delivery_Order__c,csord__Order__r.csord__Account__c';
	public static final String CUSTOM_SERVICE_QUERY_PART2 = ' FROM csord__Service__c WHERE Id in :serviceList';
	public static final String REPLACED_SERVICE_RELATION = 'csordtelcoa__Replaced_Service__r.';
}
