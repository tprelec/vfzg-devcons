/**
 *      @author Guy Clairbois
 *      @description    Class for the Redirect With Variables page. 
 *      Function: Handles the controller logic for the RedirectWithVariables page.
 */

public with sharing class RedirectWithVariablesController {

	public PageReference redirectUser(){
		String strObject = System.currentPageReference().getParameters().get('object');

		//Allow record type selection? if set to yes, then allow RTS
		String strEnableRTS = System.currentPageReference().getParameters().get('allowRTS');

		if(strObject != ''){
			//Create a generic object based on the name
			Schema.Sobjecttype oGenericObj = Schema.getGlobalDescribe().get(strObject);
			if(oGenericObj != null){
				String saveURL = String.format('/{0}/e?nooverride=1&cancelURL=%2F{1}&retURL=%2F{2}', new List<String>{oGenericObj.getDescribe().getKeyPrefix(), 
																													  ApexPages.currentPage().getParameters().get('cancelURL').removeStart('/'),
																													  ApexPages.currentPage().getParameters().get('retURL').removeStart('/')});
				system.debug(saveURL);
				PageReference pReference = null;
				if(strEnableRTS != '' && strEnableRTS == 'true'){
					//If we need RTS, the URL will have to point to the recordtypeselect.jsp
					if(oGenericObj.getDescribe().custom){
						pReference = new PageReference('/setup/ui/recordtypeselect.jsp?ent=' + oGenericObj.getDescribe().getKeyPrefix());
					} else {
						pReference = new PageReference('/setup/ui/recordtypeselect.jsp?ent=' + strObject);
					}
					pReference.getParameters().put('save_new_url', saveURL);
				} else {
					//Else create the page reference to the edit page of this object
					pReference = new PageReference('/' +  oGenericObj.getDescribe().getKeyPrefix() + '/e?nooverride=1');
				}

				//Also get a separate reference - we always need to get the parameters based on the edit page
				PageReference pEditReference = new PageReference('/' +  oGenericObj.getDescribe().getKeyPrefix() + '/e?nooverride=1');
				//Get all current parameters - this could be either edit page or the record type selection.
				Map<String, String> m = pReference.getParameters();
				//Create the parameters for the URL (translates field to ID)
				system.debug(m);

				m.putAll(GeneralUtils.createLabelParameters(pEditReference, oGenericObj.newSObject()));
				//m.put('nooverride', '1');
				m.put('retURL', ApexPages.currentPage().getParameters().get('retURL'));
				m.put('cancelURL', ApexPages.currentPage().getParameters().get('cancelURL'));
				system.debug(m);
				return pReference;
			}
		}

		return null;
	}
}