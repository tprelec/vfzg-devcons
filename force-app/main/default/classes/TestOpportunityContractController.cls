@isTest
private class TestOpportunityContractController {
	@isTest
	public static void testContractCreationFromOpp() {
		TestUtils.createOpportunityFieldMappings();
		TestUtils.createCompleteOpportunity();

		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createOrderValidationOpportunity();

		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		UserRole ur = [SELECT Id, Name FROM UserRole WHERE DeveloperName = 'Network_Planner' LIMIT 1];
		User sysUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		User networkPlanner = new User(
			Alias = 'TestUser',
			Email = 'testUser@ziggo.dev2.com.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'Network',
			LastName = 'Planner',
			LanguageLocaleKey = 'nl_NL',
			LocaleSidKey = 'nl_NL',
			CompanyName = 'CS',
			TimeZoneSidKey = 'America/Los_Angeles',
			UserName = 'testUser@ziggo.dev2.com.com',
			ProfileId = p.Id,
			UserRoleId = ur.Id
		);

		System.runAs(sysUser) {
			insert networkPlanner;
		}

		System.debug('???? 8');
		GeneralUtils.userMap.put(networkPlanner.Id, networkPlanner);

		Test.startTest();
		System.debug('???? 9');
		// dealer info to set on contract creation
		Contact ownerContact = new Contact(
			AccountId = TestUtils.theAccount.Id,
			LastName = 'Current',
			FirstName = 'User',
			UserId__c = TestUtils.theAccountManager.Id
		);
		insert ownerContact;
		Dealer_Information__c dealerInfoCU = TestUtils.createDealerInformation(ownerContact.Id, '123322');
		Opportunity_Attachment__c oa = new Opportunity_Attachment__c();
		oa.Attachment_Type__c = 'Contract (Signed)';
		oa.Opportunity__c = TestUtils.theOpportunity.Id;
		insert oa;
		ContentVersion cv = new ContentVersion(
			Title = 'New file 2',
			PathOnClient = 'newfile.txt',
			VersionData = Blob.valueOf('Hi!!!!'),
			FirstPublishLocationId = oa.id
		);
		insert cv;
		FeedItem fi = new FeedItem(ParentId = oa.Id, ContentFileName = 'hi', ContentData = cv.VersionData);
		insert fi;
		Site_Postal_Check__c spc = new Site_Postal_Check__c(
			Access_Site_ID__c = TestUtils.theSite.Id,
			Access_Active__c = false,
			Access_Vendor__c = 'ZIGGO',
			Access_Result_Check__c = 'OFFNET'
		);
		insert spc;
		User np = [SELECT Id, Name, UserRoleId, UserRole.DeveloperName FROM User WHERE FirstName = 'Network' AND LAstName = 'Planner' LIMIT 1];
		HBO__c hbo = new HBO__c(
			hbo_account__c = TestUtils.theAccount.Id,
			hbo_opportunity__c = TestUtils.theOpportunity.Id,
			hbo_status__c = 'New',
			hbo_site__c = TestUtils.theSite.Id,
			hbo_network_planner__c = np.Id,
			hbo_delivery_time__c = 12,
			hbo_digging_distance__c = 100,
			hbo_total_costs__c = 500,
			hbo_redundancy__c = false,
			hbo_availability__c = 'Yes',
			hbo_postal_check__c = spc.Id,
			hbo_result_type__c = 'OnNet'
		);
		insert hbo;
		ContentNote doc = new ContentNote(Title = 'test_hbo', Content = Blob.valueOf('test doc'));
		insert doc;
		ContentDocumentLink link = new ContentDocumentLink(
			LinkedEntityId = hbo.Id,
			ContentDocumentId = doc.id,
			ShareType = 'V',
			Visibility = 'AllUsers'
		);
		insert link;
		hbo.hbo_status__c = 'Approved';
		update hbo;

		OpportunityLineItem oli = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :TestUtils.theOpportunity.Id LIMIT 1];
		Site_Availability__c sa = new Site_Availability__c(
			Site__c = TestUtils.theSite.Id,
			Access_Infrastructure__c = 'Coax',
			Bandwith_Down_Entry__c = 50,
			Bandwith_Down_Premium__c = 50,
			Bandwith_Up_Entry__c = 100,
			Bandwith_Up_Premium__c = 100,
			Existing_Infra__c = false,
			Vendor__c = 'ZIGGO',
			Premium_Vendor__c = 'ZIGGO',
			Result_Check__c = 'test',
			Region__c = 'Test',
			Source_Check_Entry__c = spc.Id
		);
		insert sa;
		oli.Site_Availability_List__c = sa.Id + ';' + sa.Id;
		update oli;

		Ban__c b = new Ban__c(
			Account__c = TestUtils.theAccount.Id,
			Unify_Customer_Type__c = 'C',
			Unify_Customer_SubType__c = 'A',
			Name = '388898888'
		);
		insert b;
		// update ban on opp
		update new Opportunity(
			Id = TestUtils.theOpportunity.Id,
			Services__c = 'IPVPN',
			RecordTypeId = GeneralUtils.recordtypeMap.get('Opportunity').get('MAC'),
			BAN__c = b.Id
		);

		Credit_Check__c cc = new Credit_Check__c();
		cc.Opportunity__c = TestUtils.theOpportunity.Id;
		cc.Credit_Check_Status_Number__c = 1;
		insert cc;

		// close won opp and contract creation with attachments, feed items and content
		//update new Opportunity(Id = TestUtils.theOpportunity.Id, StageName = 'Closed Won');
		update new Opportunity(Id = TestUtils.theOpportunity.Id, StageName = 'Closing');

		//VF_Contract__c c = [SELECT Id FROM VF_Contract__c LIMIT 1];

		VF_Contract__c c = new VF_Contract__c();
		c.Opportunity__c = TestUtils.theOpportunity.Id;
		c.Account__c = TestUtils.theAccount.Id;
		insert c;

		PageReference pageRef = Page.OpportunityContract;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('contractId', c.Id);
		OpportunityContractController oppContCtrl = new OpportunityContractController();
		oppContCtrl.updateContract();
		oppContCtrl.updateTaskAtt();
		oppContCtrl.doRedirect();

		Test.stopTest();

		List<VF_Contract__c> con = [SELECT Id, (SELECT Id FROM Contracted_Products__r) FROM VF_Contract__c];
		System.assert(!con.isEmpty());
		//System.assertEquals(2, con[0].Contracted_Products__r.size());
		System.assertEquals(0, con[0].Contracted_Products__r.size());
	}

	@isTest
	public static void testContractCreationFromOppNegative() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createOrderValidationOpportunity();

		TestUtils.autoCommit = false;
		List<Field_Sync_Mapping__c> fsmlist = new List<Field_Sync_Mapping__c>();
		fsmlist.add(TestUtils.createSync('Opportunity -> VF_Contract__c', 'AccountId', 'Account__c'));
		fsmlist.add(TestUtils.createSync('Opportunity -> VF_Contract__c', 'OwnerId  ', 'Owner__c'));
		fsmlist.add(TestUtils.createSync('Opportunity -> VF_Contract__c', 'Duration__c', 'Contract_Duration__c'));
		fsmlist.add(TestUtils.createSync('Opportunity -> VF_Contract__c', 'Deal_Type__c', 'Deal_Type__c'));
		fsmlist.add(TestUtils.createSync('Opportunity -> VF_Contract__c', 'Id', 'Opportunity__c'));
		fsmlist.add(TestUtils.createSync('Opportunity -> VF_Contract__c', 'OwnerId', 'OwnerId'));
		fsmlist.add(TestUtils.createSync('OpportunityLineItem -> Contracted_Products__c', 'Quantity', 'Quantity__c'));
		fsmlist.add(TestUtils.createSync('OpportunityLineItem -> Contracted_Products__c', 'CLC__c', 'CLC__c'));
		fsmlist.add(TestUtils.createSync('OpportunityLineItem -> Contracted_Products__c', 'Duration__c', 'Duration__c'));
		fsmlist.add(TestUtils.createSync('OpportunityLineItem -> Contracted_Products__c', 'Site_List__c', 'Site_List__c'));
		fsmlist.add(TestUtils.createSync('OpportunityLineItem -> Contracted_Products__c', 'Subtotal', 'Subtotal__c'));
		fsmlist.add(TestUtils.createSync('OpportunityLineItem -> Contracted_Products__c', 'Proposition__c', 'Proposition__c'));
		fsmlist.add(TestUtils.createSync('OpportunityLineItem -> Contracted_Products__c', 'Proposition_Component__c', 'Proposition_Component__c'));
		// Insert invalid mapping to force error
		fsmlist.add(TestUtils.createSync('OpportunityLineItem -> Contracted_Products__c', 'OpportunityId', 'Cost__c'));
		insert fsmList;
		TestUtils.autoCommit = true;
		TestUtils.createCompleteOpportunity();
		Special_Authorizations__c specAuth = new Special_Authorizations__c(SetupOwnerId = UserInfo.getUserId(), Edit_Closed_Opportunities__c = true);
		insert specAuth;
		Profile p = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		UserRole ur = [SELECT Id, Name FROM UserRole WHERE DeveloperName = 'Network_Planner' LIMIT 1];
		User sysUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		User networkPlanner = new User(
			Alias = 'TestUser',
			Email = 'testUser@ziggo.dev2.com.com',
			EmailEncodingKey = 'UTF-8',
			FirstName = 'Network',
			LastName = 'Planner',
			LanguageLocaleKey = 'nl_NL',
			LocaleSidKey = 'nl_NL',
			CompanyName = 'CS',
			TimeZoneSidKey = 'America/Los_Angeles',
			UserName = 'testUser@ziggo.dev2.com.com',
			ProfileId = p.Id,
			UserRoleId = ur.Id
		);

		System.runAs(sysUser) {
			insert networkPlanner;
		}
		GeneralUtils.userMap.put(networkPlanner.Id, networkPlanner);

		Test.startTest();
		// dealer info to set on contract creation
		Contact ownerContact = new Contact(
			AccountId = TestUtils.theAccount.Id,
			LastName = 'Current',
			FirstName = 'User',
			UserId__c = TestUtils.theAccountManager.Id
		);
		insert ownerContact;
		Dealer_Information__c dealerInfoCU = TestUtils.createDealerInformation(ownerContact.Id, '123322');

		Opportunity_Attachment__c oa = new Opportunity_Attachment__c();
		oa.Attachment_Type__c = 'Contract (Signed)';
		oa.Opportunity__c = TestUtils.theOpportunity.Id;
		insert oa;
		ContentVersion cv = new ContentVersion(
			Title = 'New file 2',
			PathOnClient = 'newfile.txt',
			VersionData = Blob.valueOf('Hi!!!!'),
			FirstPublishLocationId = oa.id
		);
		insert cv;
		FeedItem fi = new FeedItem(ParentId = oa.Id, ContentFileName = 'hi', ContentData = cv.VersionData);
		insert fi;
		Site_Postal_Check__c spc = new Site_Postal_Check__c(
			Access_Site_ID__c = TestUtils.theSite.Id,
			Access_Active__c = false,
			Access_Vendor__c = 'ZIGGO',
			Access_Result_Check__c = 'OFFNET'
		);
		insert spc;
		User np = [SELECT Id, Name, UserRoleId, UserRole.DeveloperName FROM User WHERE FirstName = 'Network' AND LAstName = 'Planner' LIMIT 1];
		HBO__c hbo = new HBO__c(
			hbo_account__c = TestUtils.theAccount.Id,
			hbo_opportunity__c = TestUtils.theOpportunity.Id,
			hbo_status__c = 'New',
			hbo_site__c = TestUtils.theSite.Id,
			hbo_network_planner__c = np.Id,
			hbo_delivery_time__c = 12,
			hbo_digging_distance__c = 100,
			hbo_total_costs__c = 500,
			hbo_redundancy__c = false,
			hbo_availability__c = 'Yes',
			hbo_postal_check__c = spc.Id,
			hbo_result_type__c = 'OnNet'
		);
		insert hbo;
		ContentNote doc = new ContentNote(Title = 'test_hbo', Content = Blob.valueOf('test doc'));
		insert doc;
		ContentDocumentLink link = new ContentDocumentLink(
			LinkedEntityId = hbo.Id,
			ContentDocumentId = doc.id,
			ShareType = 'V',
			Visibility = 'AllUsers'
		);
		insert link;
		hbo.hbo_status__c = 'Approved';
		update hbo;

		OpportunityLineItem oli = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :TestUtils.theOpportunity.Id LIMIT 1];
		Site_Availability__c sa = new Site_Availability__c(
			Site__c = TestUtils.theSite.Id,
			Access_Infrastructure__c = 'Coax',
			Bandwith_Down_Entry__c = 50,
			Bandwith_Down_Premium__c = 50,
			Bandwith_Up_Entry__c = 100,
			Bandwith_Up_Premium__c = 100,
			Existing_Infra__c = false,
			Vendor__c = 'ZIGGO',
			Premium_Vendor__c = 'ZIGGO',
			Result_Check__c = 'test',
			Region__c = 'Test',
			Source_Check_Entry__c = spc.Id
		);
		insert sa;
		oli.Site_Availability_List__c = sa.Id + ';' + sa.Id;
		update oli;

		Ban__c b = new Ban__c(
			Account__c = TestUtils.theAccount.Id,
			Unify_Customer_Type__c = 'C',
			Unify_Customer_SubType__c = 'A',
			Name = '388898888'
		);
		insert b;
		// update ban on opp
		update new Opportunity(
			Id = TestUtils.theOpportunity.Id,
			Services__c = 'IPVPN',
			RecordTypeId = GeneralUtils.recordtypeMap.get('Opportunity').get('MAC'),
			BAN__c = b.Id
		);

		Credit_Check__c cc = new Credit_Check__c();
		cc.Opportunity__c = TestUtils.theOpportunity.Id;
		cc.Credit_Check_Status_Number__c = 2;
		insert cc;

		// close won opp and contract creation with attachments, feed items and content
		update new Opportunity(Id = TestUtils.theOpportunity.Id, StageName = 'Closed Won');

		VF_Contract__c c = [SELECT Id FROM VF_Contract__c LIMIT 1];

		PageReference pageRef = Page.OpportunityContract;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('contractId', c.Id);
		OpportunityContractController oppContCtrl = new OpportunityContractController();
		oppContCtrl.updateContract();
		oppContCtrl.updateTaskAtt();
		oppContCtrl.doRedirect();

		Test.stopTest();

		System.assert(ApexPages.hasMessages(ApexPages.SEVERITY.ERROR));
	}
}
