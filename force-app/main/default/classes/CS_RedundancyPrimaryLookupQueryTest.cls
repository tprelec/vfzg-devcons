@isTest
public class CS_RedundancyPrimaryLookupQueryTest {
private static testMethod void testRequiredAttributes(){
         CS_RedundancyPrimaryLookupQuery caLookup = new  CS_RedundancyPrimaryLookupQuery();
        caLookup.getRequiredAttributes();
    }
  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        System.runAs (simpleUser) { 
      
     
      Framework__c frameworkSetting = new Framework__c();
      frameworkSetting.Framework_Sequence_Number__c = 2;
      insert frameworkSetting;
      
      PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
        insert priceResetSetting;
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;
      
      //createSite(String name, Account siteAccount, String postalCode, String street, String city, Decimal houseNumber)
	    Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112',testAccount, '1111AA', 'Breestraat','LEIDEN', 112); 
	    insert site;
	    
	    Site_Postal_Check__c spc = CS_DataTest.createSite(site);
	    insert spc;
	    
	    Site__c site2 = CS_DataTest.createSite('Eindhoven, Esp 130',testAccount,'2222AA', 'Esp','Eindhoven', 130); 
	    insert site2;
	    
	    Site_Postal_Check__c spc2 = CS_DataTest.createSite(site2);
	    insert spc2;
	    
	    Site_Availability__c siteAvailability = CS_DataTest.createSiteAvailability('LEIDEN, Breestraat 112', site,spc); 
	    insert siteAvailability;
	    
	    Site_Availability__c siteAvailability2 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 130', site,spc2); 
	    insert siteAvailability2;
      
        Sales_Settings__c ssettings = new Sales_Settings__c();
        ssettings.Postalcode_check_validity_days__c = 2;
        ssettings.Max_Daily_Postalcode_Checks__c = 2;
        ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
        ssettings.Postalcode_check_block_period_days__c = 2;
        ssettings.Max_weekly_postalcode_checks__c = 15;
        insert ssettings;
      
      
        //price items
        OrderType__c orderType = CS_DataTest.createOrderType();
        insert orderType;
        Product2 product1 = CS_DataTest.createProduct('Access Infrastructure', orderType);
        insert product1;
        
        Category__c accessCategory = CS_DataTest.createCategory('Access');
        insert accessCategory;
        
        Vendor__c vendor1 = CS_DataTest.createVendor('KPNWEAS');
        insert vendor1;
    
        
    
        cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(product1, accessCategory, 20480.0, 20480.0, vendor1, 'ONNET', 'OfficeTTR2BD');
        
        insert priceItem1;
      
      
      
        Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
        insert testOpp;
        
        
        cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        insert basket;
        
       
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
        
        cscfga__Product_Definition__c accessProductDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        accessProductDef.Product_Type__c = 'Fixed';
        accessProductDef.RecordTypeId = productDefinitionRecordType;
        insert accessProductDef;
        
        
        cscfga__Product_Configuration__c accessProductConf = CS_DataTest.createProductConfiguration(accessProductDef.Id, 'Access Infrastructure',basket.Id);
        accessProductConf.cscfga__Root_Configuration__c = null;
        accessProductConf.cscfga__Parent_Configuration__c = null;
        accessProductConf.Primary__c = true;
        accessProductConf.Site_Check_SiteID__c = String.valueOf(site.Id);
        insert accessProductConf;
        
         cscfga__Product_Configuration__c config2 = CS_DataTest.createProductConfiguration(accessProductDef.Id, 'Access Infrastructure',basket.Id);
        config2.cscfga__Root_Configuration__c = null;
        config2.cscfga__Parent_Configuration__c = null;
        config2.Primary__c = true;
        config2.Name = 'Access Infrastructure';
        config2.Site_Name__c = site2.Name;
        config2.AdvanceCloned__c = false;
        config2.Site_Check_SiteID__c = String.valueOf(site.Id);
        insert config2;
        
         cscfga__Product_Configuration__c config3 = CS_DataTest.createProductConfiguration(accessProductDef.Id, 'Access Infrastructure',basket.Id);
        config3.cscfga__Root_Configuration__c = null;
        config3.cscfga__Parent_Configuration__c = null;
        config3.Secondary__c = true;
        config3.Name = 'Access Infrastructure';
        config3.Site_Name__c = site2.Name;
        config3.AdvanceCloned__c = false;
        config3.Site_Check_SiteID__c = String.valueOf(site.Id);
        insert config3;
        
        cscfga__Attribute_Definition__c accessAttributeDef = new cscfga__Attribute_Definition__c();
        accessAttributeDef.cscfga__Data_Type__c = 'String';
        accessAttributeDef.Name = 'Access Infrastructure';
        accessAttributeDef.cscfga__Product_Definition__c = accessProductDef.Id;
        insert accessAttributeDef;
        
        cscfga__Attribute__c accessAttribute = new cscfga__Attribute__c();
        accessAttribute.Name = 'Access Type';
        accessAttribute.cscfga__Value__c = 'Coax';
        accessAttribute.cscfga__Attribute_Definition__c = accessAttributeDef.Id;
        accessAttribute.cscfga__Product_Configuration__c = accessProductConf.Id;
        insert accessAttribute;
        
        
         cscfga__Attribute_Definition__c vendorAttributeDef = new cscfga__Attribute_Definition__c();
        vendorAttributeDef.cscfga__Data_Type__c = 'String';
        vendorAttributeDef.Name = 'Vendor';
        vendorAttributeDef.cscfga__Product_Definition__c = accessProductDef.Id;
        insert vendorAttributeDef;
        
        cscfga__Attribute__c vendorAttribute = new cscfga__Attribute__c();
        vendorAttribute.Name = 'KPN';
        vendorAttribute.cscfga__Value__c = String.valueOf(config2.Id);
        vendorAttribute.cscfga__Attribute_Definition__c = vendorAttributeDef.Id;
        vendorAttribute.cscfga__Product_Configuration__c = accessProductConf.Id;
        insert vendorAttribute;
        
       
        csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(accessProductConf);
        pcr.csbb__Product_Configuration__c = accessProductConf.Id;
        insert pcr;
        
        
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('BasketId', String.valueOf(basket.Id));
        searchFields.put('ConfigId', String.valueOf(accessProductConf.Id));
        searchFields.put('Vendor', 'KPN');
        searchFields.put('Access Type', 'Coax');
        searchFields.put('Site Check', String.valueOf(siteAvailability.Id));
        searchFields.put('Site Check SiteID', String.valueOf(site.Id));
        
        CS_RedundancyPrimaryLookupQuery caLookup = new CS_RedundancyPrimaryLookupQuery();
        caLookup.doLookupSearch(searchFields, String.valueOf(accessProductDef.Id),null, 0, 0);
        
     
     }
  }

}