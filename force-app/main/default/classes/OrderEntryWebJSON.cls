// class for JSON deserialization
public class OrderEntryWebJSON {
	public OrderEntryWebJSON() {
		this.data = null;
	}

	public Data data;

	public class Data {
		public List<Bundle> simOnlyBundles;
	}

	public class Bundle {
		public String id;
		public String sku;
		public Price price;
		public Subscription subscription;
	}

	public class Price {
		public Charge oneTimeCharge;
		public Charge recurringCharge;
	}

	public class PromotionPrice {
		public PriceDetail oneTimeCharge;
		public PriceDetail recurringCharge;
	}

	public class Subscription {
		public String id;
		public String sku;
		public String name;
		public List<Promotion> promotions;
		public Family family;
		public Boolean hasUnlimitedInternet;
		public String internetGB;
		public Boolean hasUnlimitedVoice;
		public String voiceMinutes;
		public Boolean hasUnlimitedSMS;
		public String numberSMS;
		public Integer durationMonths;
		public Price price;
		public List<SimCard> simCards;
		public SimCard defaultSimCard;
	}

	public class Family {
		public String id;
		public String name;
		public List<Subscription> subscriptions;
	}

	public class Charge {
		public PriceDetail normalPrice;
		public List<Discount> discounts;
		public PriceDetail discountedPrice;
	}

	public class Promotion {
		public String name;
		public String id;
		public String sku;
		public String promotionType;
		public PromotionPrice price;
	}

	public class SimCard {
		public String id;
		public String sku;
		public String name;
		public String type;
	}

	public class PriceDetail {
		public Decimal includingVAT;
		public Decimal excludingVAT;
	}

	public class Discount {
		public PriceDetail discountPrice;
		public String discountName;
		public String discountType;
		public String duration;
	}
}
