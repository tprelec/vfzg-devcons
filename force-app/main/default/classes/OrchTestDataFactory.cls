public with sharing class OrchTestDataFactory {

    //default orchestrator template process type name
    public static final String DEFAULT_ORCHESTRATOR_TEMPLATE_TYPE_NAME = 'New Provide';
    //default orchestrator template process type name
    public static final String DEFAULT_ORCHESTRATOR_STEP_TEMPLATE_RECORD_TYPE_NAME = 'Update Field';
    public static final String DEFAULT_ORCHESTRATOR_STEP_RECORD_TYPE_NAME = 'Update Field';
    /* Orchestrator processes - start */
    /**
     * @description creates number of CSPOFA__Orchestration_Process_Template__c records
     * @param otType process type - if null DEFAULT_ORCHESTRATOR_TEMPLATE_TYPE_NAME is used
     * @param numberOfItems number of items to create
     * @param doInsert perform insert operation on list or not
     *
     * @return list of created CSPOFA__Orchestration_Process_Template__c records
     */
    public static List<CSPOFA__Orchestration_Process_Template__c> createOrchestrationProcessTemplates(String otType, Integer numberOfItems, Boolean doInsert) {
        List<CSPOFA__Orchestration_Process_Template__c> processTemplates = new List<CSPOFA__Orchestration_Process_Template__c>();
        for (Integer i = 0; i < numberOfItems; i++) {
            processTemplates.add(
                new CSPOFA__Orchestration_Process_Template__c(
                    Name = 'Process Template ' + String.valueOf(i),
                    CSPOFA__Process_Type__c = otType != null ? otType : DEFAULT_ORCHESTRATOR_TEMPLATE_TYPE_NAME
                    )
                );
        }
        if (doInsert) {
            insert processTemplates;
        }
        return processTemplates;
    }
    /**
     *
     * @description creates number of CSPOFA__Orchestration_Step_Template__c records
     * @param processTemplates CSPOFA__Orchestration_Process_Template__c list to assign CSPOFA__Orchestration_Step_Template__c list to - if null single CSPOFA__Orchestration_Process_Template__c record will be created
     * @param RecordTypeName record type name (not dev_name!) - if null DEFAULT_ORCHESTRATOR_STEP_TEMPLATE_RECORD_TYPE_NAME is used
     * @param numberOfItems number of items to create
     * @param doInsert perform insert operation on list or not
     *
     * @return list of created CSPOFA__Orchestration_Step_Template__c records
     */
    public static List<CSPOFA__Orchestration_Step_Template__c> createOrchestrationStepTemplates(List<CSPOFA__Orchestration_Process_Template__c> processTemplates, String RecordTypeName, Integer numberOfItems, Boolean doInsert) {
        if (processTemplates == null) {
            processTemplates = createOrchestrationProcessTemplates(null, 1, true);
        }
        List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = new List<CSPOFA__Orchestration_Step_Template__c>();
        Id recordTypeId;
        if (String.isNotBlank(recordTypeName)) {
            recordTypeId = Schema.SObjectType.CSPOFA__Orchestration_Step_Template__c.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId();
        } else {
            recordTypeId = Schema.SObjectType.CSPOFA__Orchestration_Step_Template__c.getRecordTypeInfosByName().get(DEFAULT_ORCHESTRATOR_STEP_TEMPLATE_RECORD_TYPE_NAME).getRecordTypeId();
        }
        for (CSPOFA__Orchestration_Process_Template__c processTemplate : processTemplates) {
            for (Integer i = 0; i < numberOfItems; i++) {
                stepTemplates.add(new CSPOFA__Orchestration_Step_Template__c(
                              Name = 'Step Template ' + String.valueOf(i),
                              RecordTypeId = recordTypeId,
                              CSPOFA__Orchestration_Process_Template__c = processTemplate.Id
                              ));
            }
        }
        if (doInsert) {
            insert stepTemplates;
        }
        return stepTemplates;
    }
    /**
     *
     * @description creates number of CSPOFA__Orchestration_Process__c records, one for each process template
     * @param processTemplates CSPOFA__Orchestration_Process_Template__c list to assign CSPOFA__Orchestration_Step_Template__c list to - if null single CSPOFA__Orchestration_Process_Template__c record will be created
     * @param fieldNamesValues map of field api names and object values (in correct type as target field
     * @param doInsert perform insert operation on list or not
     * @return list of created CSPOFA__Orchestration_Step_Template__c records
     */
    public static List<CSPOFA__Orchestration_Process__c> createOrchestrationProcesses(List<CSPOFA__Orchestration_Process_Template__c> processTemplates, Map<String, Object> fieldNamesValues, Boolean doInsert) {
        if (processTemplates == null) {
            processTemplates = createOrchestrationProcessTemplates(null, 1, true);
        }
        List<CSPOFA__Orchestration_Process__c> processes = new List<CSPOFA__Orchestration_Process__c>();
        for (CSPOFA__Orchestration_Process_Template__c processTemplate : processTemplates) {
            CSPOFA__Orchestration_Process__c process = new CSPOFA__Orchestration_Process__c();
            process.Name = 'Process';
            process.CSPOFA__Orchestration_Process_Template__c = processTemplate.Id;
            if (fieldNamesValues != null) {
                for (String fieldName : fieldNamesValues.keySet()) {
                    process.put(fieldName, fieldNamesValues.get(fieldName));
                }
            }
            processes.add(process);
        }
        if (doInsert) {
            insert processes;
        }
        return processes;
    }
    /**
     * @description creates number of CSPOFA__Orchestration_Step__c records, one for each process  and one for each step template
     * @param processes processes to create steps for
     * @param stepTemplates step templates to create by
     * @param doInsert perform insert operation on list or not
     * @return list of created CSPOFA__Orchestration_Step__c records
     */
    public static List<CSPOFA__Orchestration_Step__c> createOrchestrationSteps(List<CSPOFA__Orchestration_Process__c> processes, List<CSPOFA__Orchestration_Step_Template__c> stepTemplates,
                                                                               Boolean doInsert) {
        List<CSPOFA__Orchestration_Step__c> steps = new List<CSPOFA__Orchestration_Step__c>();
        for (CSPOFA__Orchestration_Process__c process : processes) {
            for (CSPOFA__Orchestration_Step_Template__c stepTemplate : stepTemplates) {
                steps.add(new CSPOFA__Orchestration_Step__c(
                          RecordTypeId = Schema.SObjectType.CSPOFA__Orchestration_Step__c.getRecordTypeInfosByName().get(DEFAULT_ORCHESTRATOR_STEP_RECORD_TYPE_NAME).getRecordTypeId(),
                          Name = stepTemplate.Name,
                          CSPOFA__Orchestration_Process__c = process.Id
                          ));
            }
        }
        if (doInsert) {
            insert steps;
        }
        return steps;
    }
    /* Orchestrator processes - end */
}