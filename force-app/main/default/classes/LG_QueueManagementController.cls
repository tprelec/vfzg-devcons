/**
* Used as a controller for LG_QueueManagement VF page.
*
* @author Tomislav Blazek
* @ticket SFDT-1423
* @since  9/8/2016
*/
public class LG_QueueManagementController {

    public static final String USER_ACCOUNT_NOT_LINKED = 'No Partner Account has been assigned to you in the Partner Queue Management User object.';

    public SelectOption[] selectedUsers { get; set; }
    public SelectOption[] allUsers { get; set; }
    public Account partnerAccount {get; set;}
    public List<Group> queues {get; set;}
    public String selectedQueue {get; set;}

    public LG_QueueManagementController() {
        List<LG_PartnerQueueManagementUser__c> accountUser = [SELECT  Id, LG_PartnerAccount__c,
                                                                        LG_PartnerAccountName__c
                                                                FROM LG_PartnerQueueManagementUser__c
                                                                WHERE LG_UserName__c = :UserInfo.getUserId()
                                                                LIMIT 1];

        if (!accountUser.isEmpty())
        {
            partnerAccount = new Account(Id = accountUser[0].LG_PartnerAccount__c, Name = accountUser[0].LG_PartnerAccountName__c);

            Set<String> queueDevNames = new Set<String>();
            for (LG_PartnerQueueManagement__c qpm : [SELECT Id, LG_QueueAPIName__c FROM LG_PartnerQueueManagement__c
                                                            WHERE LG_PartnerAccount__c = :partnerAccount.Id])
            {
                queueDevNames.add(qpm.LG_QueueAPIName__c);
            }

            queues = [SELECT Id, Name FROM Group
                        WHERE Type = 'Queue'
                        AND DeveloperName IN :queueDevNames
                        ORDER BY Name];
        }
        else
        {
            partnerAccount = new Account(Id = null,
                                        Name = USER_ACCOUNT_NOT_LINKED);
        }

        requeryUsers();
    }

    public void requeryUsers()
    {
        selectedUsers = new List<SelectOption>();
        allUsers = new List<SelectOption>();

        if (selectedQueue instanceof Id)
        {
            List<GroupMember> currentMembers = [SELECT Id, UserOrGroupId
                                                FROM GroupMember
                                                WHERE Group.Type = 'Queue'
                                                AND GroupId = :selectedQueue];

            Set<Id> usersInQueueIds = new Set<Id>();
            for(GroupMember member : currentMembers)
            {
                if (member.UserOrGroupId.getSObjectType() == Schema.User.SObjectType)
                {
                    usersInQueueIds.add(member.UserOrGroupId);
                }
            }

            List<User> usersInQueue = [SELECT Id, Name FROM User
                                        WHERE Id IN :usersInQueueIds
                                        AND Contact.AccountId = :partnerAccount.Id];

            for (User user : usersInQueue)
            {
                selectedUsers.add(new SelectOption(user.Id, user.Name));
            }

            List<User> allPartnerUsers = [SELECT Id, Name FROM User
                                            WHERE Contact.AccountId = :partnerAccount.Id
                                            AND IsActive = true
                                            AND UserType = 'PowerPartner'];

            for (User user : allPartnerUsers)
            {
                if (!usersInQueueIds.contains(user.Id))
                {
                    allUsers.add(new SelectOption(user.Id, user.Name));
                }
            }
        }
    }

    public PageReference save()
    {
        if (selectedQueue instanceof Id)
        {
            Set<Id> selectedUserIds = new Set<Id>();
            for(SelectOption selectedUser : selectedUsers)
            {
                selectedUserIds.add(selectedUser.getValue());
            }

            List<GroupMember> currentMembers = [SELECT Id, UserOrGroupId
                                                FROM GroupMember
                                                WHERE Group.Type = 'Queue'
                                                AND GroupId = :selectedQueue];

            List<GroupMember> membersToDelete = new List<GroupMember>();
            List<GroupMember> membersToAdd = new List<GroupMember>();

            //Check for the GroupMember records to be removed
            for(GroupMember member : currentMembers)
            {
                //if group member record relates to user Id and is not among selected users, we will remove it from queue
                if (member.UserOrGroupId.getSObjectType() == Schema.User.SObjectType
                    && !selectedUserIds.contains(member.UserOrGroupId))
                {
                    membersToDelete.add(member);
                }
            }

            //Check for the GroupMember records that exist
            for(GroupMember member : currentMembers)
            {
                //if group member record relates to user Id and is among selected users, we will remove the
                //id from selectedUsers list so that there won't be any duplicates created
                if (selectedUserIds.contains(member.UserOrGroupId))
                {
                    selectedUserIds.remove(member.UserOrGroupId);
                }
            }

            if(!membersToDelete.isEmpty())
            {
                delete membersToDelete;
            }

            for(Id selectedUserId : selectedUserIds)
            {
                membersToAdd.add(new GroupMember(GroupId = selectedQueue, UserOrGroupId = selectedUserId));
            }

            if (!membersToAdd.isEmpty())
            {
                insert membersToAdd;
            }
        }

        return refreshPage();
    }

    public PageReference refreshPage()
    {
        selectedQueue = null;
        requeryUsers();
        return ApexPages.currentPage();
    }
}