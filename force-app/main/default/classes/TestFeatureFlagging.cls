@isTest
public with sharing class TestFeatureFlagging {
    @isTest
    static void testPositive() {
        System.assertEquals(false, FeatureFlagging.isActive('featureName'), 'the value does not match with the expected');
    }
}