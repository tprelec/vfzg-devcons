@isTest
public class TestWS_GetHboInfo {
	@isTest
	public static void testGetRecord() {
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/get_HBO_info'; //Request URL
		req.httpMethod = 'GET'; //HTTP Request Type
		RestContext.request = req;
		RestContext.response = res;

		WS_GetHboInfo.getRecord();
		System.assertEquals(RestContext.response.statuscode, 400, 'Required params are not set.');
	}

	@isTest
	public static void testGetRecordSite() {
		prepareData();
		// fail 404
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/get_HBO_info';
		req.params.put('orderNoSF', 'O-012345.1');
		req.params.put('zipcode', '1234AB');
		req.params.put('houseNo', '1');
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;

		WS_GetHboInfo.getRecord();
		System.assertEquals(RestContext.response.statuscode, 404, 'Site doesn\'t exist.');
	}

	@isTest
	public static void testGetRecordOrder() {
		prepareData();
		// fail 404
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/get_HBO_info';
		req.params.put('orderNoSF', 'O-012345.3');
		req.params.put('zipcode', '1234AB');
		req.params.put('houseNo', String.valueOf(TestUtils.theSite.Site_House_Number__c));
		req.params.put('houseNoExt', String.valueOf(TestUtils.theSite.Site_House_Number_Suffix__c));
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;

		WS_GetHboInfo.getRecord();
		System.assertEquals(RestContext.response.statuscode, 404, 'Order doesn\'t exist.');
	}

	@isTest
	public static void testGetRecordHBO() {
		prepareData();
		// fail 404
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		Order__c o = getOrder();
		req.requestURI = '/services/apexrest/get_HBO_info';
		req.params.put('orderNoSF', o.BOP_Export_Order_Id__c);
		req.params.put('zipcode', '1234AB');
		req.params.put('houseNo', String.valueOf(TestUtils.theSite.Site_House_Number__c));
		req.params.put('houseNoExt', TestUtils.theSite.Site_House_Number_Suffix__c);
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;

		WS_GetHboInfo.getRecord();
		System.assertEquals(RestContext.response.statuscode, 404, 'HBO doesn\'t exist.');
	}

	@isTest
	public static void testGetRecordSuccess() {
		doCreateHBO();
		Integer restResp = 0;
		Test.startTest();
		RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		Order__c o = getOrder();
		req.requestURI = '/services/apexrest/get_HBO_info';
		req.params.put('orderNoSF', o.BOP_Export_Order_Id__c);
		req.params.put('zipcode', '1234AB');
		req.params.put('houseNo', String.valueOf(TestUtils.theSite.Site_House_Number__c));
		req.params.put('houseNoExt', TestUtils.theSite.Site_House_Number_Suffix__c);
		req.httpMethod = 'GET';
		RestContext.request = req;
		RestContext.response = res;

		WS_GetHboInfo.getRecord();
		restResp = RestContext.response.statuscode;
		Test.stopTest();

		System.assertEquals(RestContext.response.statuscode, 200, 'Success');
	}

	private static void prepareData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(owner);
		TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		TestUtils.createSite(acc);
		VF_Contract__c contr = TestUtils.createVFContract(acc, TestUtils.theOpportunity);
		TestUtils.createOrder(contr);
	}

	private static Order__c getOrder() {
		return [SELECT Id, BOP_Export_Order_Id__c FROM Order__c LIMIT 1];
	}

	private static void doCreateHBO() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);

		Profile p = [SELECT Id FROM Profile WHERE Name = 'VF Indirect Inside Sales' LIMIT 1];
		UserRole ur = [SELECT Id FROM UserRole WHERE DeveloperName = 'Network_Planner' LIMIT 1];
		List<User> userList = new List<User>();
		User owner = TestUtils.generateTestUser('admin', 'user', 'System Administrator');
		User salesUser = TestUtils.generateTestUser('testing', 'is', p.Id, null);
		User networkPlanner = TestUtils.generateTestUser('Network', 'Planner', p.Id, ur.Id);
		userList.add(owner);
		userList.add(salesUser);
		userList.add(networkPlanner);
		insert userList;

		System.runAs(owner) {
			TestUtils.autoCommit = true;
			Account acct = TestUtils.createAccount(owner);
			Contact generalContact = new Contact(
				AccountId = acct.Id,
				LastName = 'Current',
				FirstName = 'User',
				Phone = '+31645061130',
				MobilePhone = '+31645061133',
				Email = 'tstcontact@mail.com'
			);
			insert generalContact;
			Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), null);

			Site__c site = TestUtils.createSite(acct);
			Site_Postal_Check__c sitePosCheck = new Site_Postal_Check__c(
				Access_Site_ID__c = site.Id,
				Access_Active__c = false,
				Access_Vendor__c = 'ZIGGO',
				Access_Result_Check__c = 'OFFNET',
				Access_Redundancy__c = 'Primary'
			);
			insert sitePosCheck;

			Map<String, Id> paramsHbo = new Map<String, Id>{
				'accId' => acct.Id,
				'oppId' => opp.Id,
				'siteId' => site.Id,
				'sitePosCheckId' => sitePosCheck.Id,
				'netwPlannerId' => networkPlanner.Id
			};

			HBO__c hbo = new HBO__c(
				hbo_account__c = paramsHbo.get('accId'),
				hbo_opportunity__c = paramsHbo.get('oppId'),
				hbo_status__c = 'New',
				hbo_site__c = paramsHbo.get('siteId'),
				hbo_network_planner__c = paramsHbo.get('netwPlannerId'),
				hbo_delivery_time__c = 12,
				hbo_digging_distance__c = 100,
				hbo_total_costs__c = 500,
				hbo_redundancy__c = false,
				hbo_availability__c = 'Yes',
				hbo_postal_check__c = paramsHbo.get('sitePosCheckId'),
				hbo_result_type__c = 'Onnet'
			);
			insert hbo;
			hbo.hbo_status__c = 'Approved';
			update hbo;
			VF_Contract__c c = TestUtils.createVFContract(acct, opp);
			TestUtils.createOrder(c);
		}
	}
}
