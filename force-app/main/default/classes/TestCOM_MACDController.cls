@isTest
private class TestCOM_MACDController {
	@TestSetup
	static void testSetup() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112', testAccount, '1111AA', 'Breestraat', 'LEIDEN', 112);
		insert site;

		Site__c site2 = CS_DataTest.createSite('LEIDEN, Breestraat 113', testAccount, '1111AA', 'Breestraat', 'LEIDEN', 113);
		insert site2;
	}

	@isTest
	private static void testTerminateScenario() {
		Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		Site__c site = [SELECT Id FROM Site__c WHERE Name = 'LEIDEN, Breestraat 112'];

		csord__Order__c order = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now()
		);
		insert order;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		subscription.Termination_Wish_Date__c = System.now();
		insert subscription;

		csord__Order__c oldOrder = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now()
		);
		insert oldOrder;

		csord__Subscription__c oldSubscription = CS_DataTest.createSubscription(null);
		oldSubscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		oldSubscription.csord__Order__c = oldOrder.Id;
		insert oldSubscription;

		csord__Service__c oldService = CS_DataTest.createService(null, oldSubscription);
		oldService.csord__Identification__c = 'testSubscription';
		oldService.csord__Subscription__c = oldSubscription.Id;
		oldService.csord__Status__c = 'Test';
		oldService.csord__Order__c = oldOrder.Id;
		oldService.csordtelcoa__Cancelled_By_Change_Process__c = true;
		oldService.VFZ_Commercial_Component_Name__c = 'TestComponent';
		oldService.VFZ_Commercial_Article_Name__c = 'TestArticle';
		oldService.Site__c = site.Id;
		oldService.Termination_Wish_Date__c = System.now();
		oldService.Installation_Wishdate__c = System.today();
		oldService.CDO_Umbrella_Service_Id__c = 'TestUmbrella';
		insert oldService;

		csord__Service__c service = CS_DataTest.createService(null, subscription);
		service.csord__Identification__c = 'testSubscription';
		service.csord__Subscription__c = subscription.Id;
		service.csord__Status__c = 'Test';
		service.csord__Order__c = order.Id;
		service.csordtelcoa__Cancelled_By_Change_Process__c = true;
		service.VFZ_Commercial_Component_Name__c = 'TestComponent';
		service.VFZ_Commercial_Article_Name__c = 'TestArticle';
		service.Site__c = site.Id;
		service.csordtelcoa__Replaced_Service__c = oldService.Id;
		insert service;

		oldService.csordtelcoa__Replacement_Service__c = service.Id;
		update oldService;

		order.csord__Status2__c = 'Ready for Delivery';

		Map<Id, csord__Order__c> newOrdersMap = new Map<Id, csord__Order__c>{ order.Id => order };
		Map<Id, csord__Order__c> oldOrdersMap = new Map<Id, csord__Order__c>{ order.Id => oldOrder };

		Test.startTest();
		COM_O2UDecomposition.orderDecomposition(newOrdersMap, oldOrdersMap);
		Test.stopTest();

		COM_Delivery_Order__c updatedDeliveryOrder = [
			SELECT Id, Termination_Date__c
			FROM COM_Delivery_Order__c
			WHERE Site__c = :site.Id AND Order__c = :order.Id
		];

		System.assertEquals(
			subscription.Termination_Wish_Date__c.date(),
			updatedDeliveryOrder.Termination_Date__c,
			'Termination date is incorrect or missing.'
		);

		csord__Service__c updatedService = [
			SELECT Id, Termination_Wish_Date__c, Installation_Wishdate__c, CDO_Umbrella_Service_Id__c
			FROM csord__Service__c
			WHERE Id = :service.Id
		];

		System.assertEquals(
			oldService.CDO_Umbrella_Service_Id__c,
			updatedService.CDO_Umbrella_Service_Id__c,
			'CDO_Umbrella_Service_Id__c should be the same as on replaced service.'
		);
	}

	@isTest
	private static void testMoveScenario() {
		Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		Site__c site = [SELECT Id FROM Site__c WHERE Name = 'LEIDEN, Breestraat 112'];
		Site__c site2 = [SELECT Id FROM Site__c WHERE Name = 'LEIDEN, Breestraat 113'];

		csord__Order__c order = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now()
		);
		insert order;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		subscription.Termination_Wish_Date__c = System.now();
		insert subscription;

		csord__Order__c oldOrder = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now()
		);
		insert oldOrder;

		csord__Subscription__c oldSubscription = CS_DataTest.createSubscription(null);
		oldSubscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		oldSubscription.csord__Order__c = oldOrder.Id;
		insert oldSubscription;

		csord__Service__c oldService = CS_DataTest.createService(null, oldSubscription);
		oldService.csord__Identification__c = 'testSubscription';
		oldService.csord__Subscription__c = oldSubscription.Id;
		oldService.csord__Status__c = 'Test';
		oldService.csord__Order__c = oldOrder.Id;
		oldService.csordtelcoa__Cancelled_By_Change_Process__c = true;
		oldService.VFZ_Commercial_Component_Name__c = 'TestComponent';
		oldService.VFZ_Commercial_Article_Name__c = 'TestArticle';
		oldService.Site__c = site.Id;
		oldService.Termination_Wish_Date__c = System.now();
		oldService.Installation_Wishdate__c = System.today();
		oldService.CDO_Umbrella_Service_Id__c = 'TestUmbrella';
		insert oldService;

		csord__Service__c service = CS_DataTest.createService(null, subscription);
		service.csord__Identification__c = 'testSubscription';
		service.csord__Subscription__c = subscription.Id;
		service.csord__Status__c = 'Test';
		service.csord__Order__c = order.Id;
		service.csordtelcoa__Cancelled_By_Change_Process__c = false;
		service.VFZ_Commercial_Component_Name__c = 'TestComponent';
		service.VFZ_Commercial_Article_Name__c = 'TestArticle';
		service.Site__c = site2.Id;
		service.csordtelcoa__Replaced_Service__c = oldService.Id;
		insert service;

		csord__Service__c service2 = CS_DataTest.createService(null, subscription);
		service2.csord__Identification__c = 'testSubscription';
		service2.csord__Subscription__c = subscription.Id;
		service2.csord__Status__c = 'Test';
		service2.csord__Order__c = order.Id;
		service2.csordtelcoa__Cancelled_By_Change_Process__c = true;
		service2.VFZ_Commercial_Component_Name__c = 'TestComponent';
		service2.VFZ_Commercial_Article_Name__c = 'TestArticle';
		service2.Site__c = site2.Id;
		service2.csordtelcoa__Replaced_Service__c = oldService.Id;
		insert service2;

		oldService.csordtelcoa__Replacement_Service__c = service.Id;
		update oldService;

		order.csord__Status2__c = 'Ready for Delivery';

		Map<Id, csord__Order__c> newOrdersMap = new Map<Id, csord__Order__c>{ order.Id => order };
		Map<Id, csord__Order__c> oldOrdersMap = new Map<Id, csord__Order__c>{ order.Id => oldOrder };

		Test.startTest();
		COM_O2UDecomposition.orderDecomposition(newOrdersMap, oldOrdersMap);
		Test.stopTest();

		COM_Delivery_Order__c updatedDeliveryOrder = [
			SELECT Id, MACD_Type__c, MACD_Actions__c, Termination_Date__c
			FROM COM_Delivery_Order__c
			WHERE Site__c = :site.Id AND Order__c = :order.Id
		];

		System.assertEquals(COM_Constants.MACD_TYPE_TERMINATION, updatedDeliveryOrder.MACD_Type__c, 'MACD_Type__c is incorrect or missing.');

		System.assertEquals(
			COM_Constants.MACD_ACTION_COMPONENT_DELETE,
			updatedDeliveryOrder.MACD_Actions__c,
			'MACD_Actions__c is incorrect or missing.'
		);

		System.assertEquals(
			subscription.Termination_Wish_Date__c.date(),
			updatedDeliveryOrder.Termination_Date__c,
			'Termination date is incorrect or missing.'
		);

		csord__Service__c updatedService = [
			SELECT Id, Termination_Wish_Date__c, Installation_Wishdate__c, CDO_Umbrella_Service_Id__c
			FROM csord__Service__c
			WHERE Id = :service.Id
		];

		System.assertNotEquals(
			oldService.CDO_Umbrella_Service_Id__c,
			updatedService.CDO_Umbrella_Service_Id__c,
			'CDO_Umbrella_Service_Id__c should not be the same as on replaced service.'
		);

		List<csord__Service__c> deletedService = [SELECT Id FROM csord__Service__c WHERE Id = :service2.Id];

		System.assertEquals(0, deletedService.size(), 'Service should have been deleted.');
	}

	@isTest
	private static void testChangeScenario() {
		Account testAccount = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		Site__c site = [SELECT Id FROM Site__c WHERE Name = 'LEIDEN, Breestraat 112'];

		csord__Order__c order = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now()
		);
		insert order;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		subscription.Termination_Wish_Date__c = System.now();
		insert subscription;

		csord__Order__c oldOrder = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now()
		);
		insert oldOrder;

		csord__Subscription__c oldSubscription = CS_DataTest.createSubscription(null);
		oldSubscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		oldSubscription.csord__Order__c = oldOrder.Id;
		insert oldSubscription;

		csord__Service__c oldService = CS_DataTest.createService(null, oldSubscription);
		oldService.csord__Identification__c = 'testSubscription';
		oldService.csord__Subscription__c = oldSubscription.Id;
		oldService.csord__Status__c = 'Test';
		oldService.csord__Order__c = oldOrder.Id;
		oldService.csordtelcoa__Cancelled_By_Change_Process__c = true;
		oldService.VFZ_Commercial_Component_Name__c = 'TestComponent';
		oldService.VFZ_Commercial_Article_Name__c = 'TestArticle';
		oldService.Site__c = site.Id;
		oldService.Termination_Wish_Date__c = System.now();
		oldService.Installation_Wishdate__c = System.today();
		oldService.CDO_Umbrella_Service_Id__c = 'TestUmbrella';
		insert oldService;

		csord__Service__c service = CS_DataTest.createService(null, subscription);
		service.csord__Identification__c = 'testSubscription';
		service.csord__Subscription__c = subscription.Id;
		service.csord__Status__c = 'Test';
		service.csord__Order__c = order.Id;
		service.csordtelcoa__Cancelled_By_Change_Process__c = false;
		service.VFZ_Commercial_Component_Name__c = 'TestComponent';
		service.VFZ_Commercial_Article_Name__c = 'TestArticle';
		service.Site__c = site.Id;
		service.csordtelcoa__Replaced_Service__c = oldService.Id;
		insert service;

		oldService.csordtelcoa__Replacement_Service__c = service.Id;
		update oldService;

		order.csord__Status2__c = 'Ready for Delivery';

		Map<Id, csord__Order__c> newOrdersMap = new Map<Id, csord__Order__c>{ order.Id => order };
		Map<Id, csord__Order__c> oldOrdersMap = new Map<Id, csord__Order__c>{ order.Id => oldOrder };

		Test.startTest();
		COM_O2UDecomposition.orderDecomposition(newOrdersMap, oldOrdersMap);
		Test.stopTest();

		csord__Service__c updatedService = [SELECT Id, Implemented_Date__c, CDO_Umbrella_Service_Id__c FROM csord__Service__c WHERE Id = :service.Id];

		System.assertEquals(
			oldService.CDO_Umbrella_Service_Id__c,
			updatedService.CDO_Umbrella_Service_Id__c,
			'CDO_Umbrella_Service_Id__c should be the same as on replaced service.'
		);
	}
}
