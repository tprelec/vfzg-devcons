@isTest
public class TestOrchValFWAOrderExecutionHandler {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, owner);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(claimerContact.id, '123321');
		contr.Dealer_Information__c = dealerInfo.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;

		VF_Product__c vfp = new VF_Product__c(
			Name = 'Red Pro Select',
			Family__c = 'Voice',
			Product_Line__c = 'fVodafone',
			ExternalID__c = 'VFP-03-1234567',
			ProductCode__c = 'RCBIZREDPSEL36',
			OrderType__c = ot.Id,
			Available_for_EnhancedMobileProvisioning__c = true,
			AllowsGroupDataSharing__c = 'No',
			SendtoUnifyforAutomaticProvisioning__c = 'Yes',
			Unify_Pricing_Element_Branch__c = 'Base_Plan',
			Unify_Product_CatalogId__c = '9191026',
			Unify_Product_Offer_Code__c = 'BIZ_RED_PRO_3_6_ELITE',
			Unify_Billing_Offer_CatalogId__c = '9173226',
			Unify_Billing_Offer_Code__c = 'BO_BIZ_RED_PRO_SELECT_3_6',
			Discount_Charge_Code__c = 'RCBIZREDPSEL36'
		);
		insert vfp;

		Product2 product = TestUtils.createProduct(new Map<String, Object>{ 'Product_Group__c' => 'Priceplan', 'VF_Product__c' => vfp.Id });

		// Create Contracted Products
		Product2 prod = getProduct();
		Contracted_Products__c contProd = new Contracted_Products__c(
			Quantity__c = 1,
			UnitPrice__c = 10,
			Duration__c = 1,
			Model_Number__c = 1,
			Product__c = prod.Id,
			VF_Contract__c = contr.Id,
			Order__c = order.Id,
			Request_New_Framework__c = true,
			FWAD__c = 12
		);
		insert contProd;
	}

	@isTest
	public static void testFWAValidateOrderFEApi200() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		System.debug('Test ' + [SELECT Id FROM Order__c]);
		String orderId = [SELECT Id FROM Order__c].Id;

		VF_Contract__c contr = [
			SELECT id, Contract_Duration__c, Contract_Activation_Date_Actual_del__c, Contract_Activation_Date__c
			FROM VF_Contract__c
		];
		contr.Contract_Duration__c = 24;
		contr.Contract_Activation_Date_Actual_del__c = system.today();
		contr.Contract_Activation_Date__c = system.today();
		update contr;

		String cPId = [SELECT Id FROM Contracted_Products__c].Id;
		fieldKeyMap.put('VZ_Order__c', orderId);
		fieldKeyMap.put('Contracted_Product__c', cPId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_FWAAPI_200');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchValidateFWAOrderExecutionHandler getOrchFWAValidate = new OrchValidateFWAOrderExecutionHandler();
		Boolean continueToProcess = getOrchFWAValidate.performCallouts(steps);
		System.assertEquals(true, continueToProcess);
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchFWAValidate.process(steps);
		System.assertEquals('Complete', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testFWAValidateOrderFENoCalloutErr() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;
		String cPId = [SELECT Id FROM Contracted_Products__c].Id;

		fieldKeyMap.put('Contracted_Product__c', cPId);
		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);

		Test.startTest();
		OrchValidateFWAOrderExecutionHandler getOrchFWAValidate = new OrchValidateFWAOrderExecutionHandler();
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchFWAValidate.process(steps);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testFWAValidateOrderFELongStringError() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		VF_Contract__c contr = [
			SELECT id, Contract_Duration__c, Contract_Activation_Date_Actual_del__c, Contract_Activation_Date__c
			FROM VF_Contract__c
		];
		contr.Contract_Duration__c = 24;
		contr.Contract_Activation_Date_Actual_del__c = system.today();
		contr.Contract_Activation_Date__c = system.today();
		update contr;

		String cPId = [SELECT Id FROM Contracted_Products__c].Id;

		fieldKeyMap.put('Contracted_Product__c', cPId);
		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_FWAAPI_200_Longstring');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchValidateFWAOrderExecutionHandler getOrchFWAValidate = new OrchValidateFWAOrderExecutionHandler();
		Boolean continueToProcess = getOrchFWAValidate.performCallouts(steps);
		System.assertEquals(true, continueToProcess);
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchFWAValidate.process(steps);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testFWAValidateOrderFE400Error() {
		List<CSPOFA__Orchestration_Process_Template__c> processTemplate = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, true);
		List<CSPOFA__Orchestration_Step_Template__c> stepTemplates = OrchTestDataFactory.createOrchestrationStepTemplates(
			processTemplate,
			null,
			1,
			true
		);

		Map<String, String> fieldKeyMap = new Map<String, String>();

		String orderId = [SELECT Id FROM Order__c].Id;

		VF_Contract__c contr = [
			SELECT id, Contract_Duration__c, Contract_Activation_Date_Actual_del__c, Contract_Activation_Date__c
			FROM VF_Contract__c
		];
		contr.Contract_Duration__c = 24;
		contr.Contract_Activation_Date_Actual_del__c = system.today();
		contr.Contract_Activation_Date__c = system.today();
		update contr;

		String cPId = [SELECT Id FROM Contracted_Products__c].Id;

		fieldKeyMap.put('Contracted_Product__c', cPId);
		fieldKeyMap.put('VZ_Order__c', orderId);
		List<CSPOFA__Orchestration_Process__c> processes = OrchTestDataFactory.createOrchestrationProcesses(processTemplate, fieldKeyMap, true);
		List<CSPOFA__Orchestration_Step__c> steps = OrchTestDataFactory.createOrchestrationSteps(processes, stepTemplates, true);
		//create mock
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('EMP_GenericAPI400_Error');
		mock.setStatusCode(400);
		mock.setHeader('Content-Type', 'application/json');
		//Set the mock callout mode
		Test.setMock(HttpCalloutMock.class, mock);

		Test.startTest();
		OrchValidateFWAOrderExecutionHandler getOrchFWAValidate = new OrchValidateFWAOrderExecutionHandler();
		Boolean continueToProcess = getOrchFWAValidate.performCallouts(steps);
		System.assertEquals(true, continueToProcess);
		List<CSPOFA__Orchestration_Step__c> lstOrchSteps = (List<CSPOFA__Orchestration_Step__c>) getOrchFWAValidate.process(steps);
		System.assertEquals('Error', lstOrchSteps[0].CSPOFA__Status__c);
		Test.stopTest();
	}

	private static Product2 getProduct() {
		return [SELECT Id FROM Product2 WHERE Product_group__c = 'Priceplan' LIMIT 1];
	}
}
