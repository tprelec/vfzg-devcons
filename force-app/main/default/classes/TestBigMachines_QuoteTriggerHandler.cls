@isTest
private class TestBigMachines_QuoteTriggerHandler {
    
    @isTest
    static void quoteCreationTest() {
        
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        //Pricebook2 pricebook = TestUtils.createPricebook(true);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        
        test.startTest();
        
        BigMachines__Quote__c quote = new BigMachines__Quote__c();
        quote.BigMachines__Opportunity__c = opp.Id;
        BigMachines__Configuration_Record__c bcr = new BigMachines__Configuration_Record__c();
        bcr.BigMachines__action_id_copy__c = '4654400';
        bcr.BigMachines__action_id_open__c = '4654396';
        bcr.BigMachines__bm_site__c = 'vodafonenl';
        bcr.BigMachines__document_id__c = '4653823';
        bcr.BigMachines__process__c = 'quickstart_commerce_process';
        bcr.BigMachines__process_id__c = '4653759';
        bcr.BigMachines__version_id__c = '5097783';     
        bcr.BigMachines__Is_Active__c = true;  
        insert bcr;
        quote.BigMachines__Site__c = bcr.Id;
        insert quote;
        

        test.stopTest();
    }
}