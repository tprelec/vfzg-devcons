global class BiccCtnInformationBatch implements Database.Batchable <sObject>, Database.Stateful{

	global map<String,String> BiccBanFieldMapping = SyncUtil.fullMapping('BICC_CTN_Information__c -> VF_Asset__c').get('BICC_CTN_Information__c -> VF_Asset__c');
	global final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	global final Schema.SObjectType assetSchema = schemaMap.get('VF_Asset__c');
	global final Schema.SObjectType biccSchema = schemaMap.get('BICC_CTN_Information__c');
	global final map<String, Schema.SObjectField> assetFieldMap = assetSchema.getDescribe().fields.getMap();
	global final map<String, Schema.SObjectField> biccFieldMap = biccSchema.getDescribe().fields.getMap();
	global final set<String> queriedFields = SyncUtil.getQueriedFields(BiccBanFieldMapping);
	global final list<String> sortList = SyncUtil.getSortedList(biccFieldMap);
	global String header = SyncUtil.getHeader(sortList, queriedFields);
	global String dataError = '';
	global Boolean chain = false;

	global Database.QueryLocator start(Database.BatchableContext BC){

		//Create a dynamic query with info from the Field Mapping Object
		String Query = 'SELECT ';
		set<String> testset = new set<String>();
		testset.addall(BiccBanFieldMapping.values());
		for (String BiccField : testset) {
			Query += BiccField + ',';
		}
		Query = Query.subString(0, Query.length() - 1);
		Query += ' FROM BICC_CTN_Information__c';

		return Database.getQueryLocator(Query);
	}

	global void execute(Database.BatchableContext BC, List<BICC_CTN_Information__c> scope){

		if(Job_Management__c.getOrgDefaults().Cancel_Batch__c == true){
			System.abortJob(BC.getJobId());
		}
		
		map<String, BICC_CTN_Information__c> scopeMap = new map<String, BICC_CTN_Information__c>();
		list<BICC_CTN_Information__c> biccDeleteList = new list<BICC_CTN_Information__c>();
		list<BICC_CTN_Information__c> biccErrorList = new list<BICC_CTN_Information__c>();
		map<String, Ban__c> banToBan = new map<String, Ban__c>();

		//Put scope in a map for deletion
		for(BICC_CTN_Information__c bicc : scope){
			Boolean match = StringUtils.checkBan(bicc.BAN__c);
			if(match){
				scopeMap.put(bicc.CTN__c, bicc);
				banToBan.put(bicc.BAN__c, null);
			} else {
				bicc.Error__c = 'The BAN number is not valid';
				biccErrorList.add(bicc);
			}
		}

		//Make mapping between Ban and Account
		for(Ban__c ban : [select Id, Name, Account__c from Ban__c where Name in: banToBan.keySet()]){
			banToBan.put(ban.Name, ban);
		}

		//Create accounts from BICC
		list<VF_Asset__c> assetList = new list<VF_Asset__c>();
		for (BICC_CTN_Information__c biccCtn : scopeMap.values()) {
			VF_Asset__c vfa = new VF_Asset__c();
			for (String assetField : BiccBanFieldMapping.keySet()) {
				String biccCtnField = BiccBanFieldMapping.get(assetField);
				Object biccCtnValue;
				if(assetFieldMap.get(assetField).getDescribe().getType().name() == 'DATE' && biccCtn.get(biccCtnField) != null){
					biccCtnValue = Date.valueOf(String.valueOf(biccCtn.get(biccCtnField)));
				} else if(assetFieldMap.get(assetField).getDescribe().getType().name() == 'DOUBLE' && biccCtn.get(biccCtnField) != null){
					biccCtnValue = decimal.valueOf(String.valueOf(biccCtn.get(biccCtnField)));
				} else {
					biccCtnValue = biccCtn.get(biccCtnField);
				}
				vfa.put(assetField,biccCtnValue);
			}
			if(banToBan.get(biccCtn.BAN__c) != null){
				vfa.Ban__c = banToBan.get(biccCtn.BAN__c).Id;
				vfa.Account__c = banToBan.get(biccCtn.BAN__c).Account__c;
				assetList.add(vfa);
			} else {
				biccCtn.Error__c = 'There is no matching Account';
				biccErrorList.add(biccCtn);
			}
		}

		//Upsert Accounts in the database
		list<Database.UpsertResult> UR = Database.upsert(assetList, VF_Asset__c.Fields.CTN__c, false);
		for (Integer i = 0; i < UR.size(); i++) {
			if(UR[i].isSuccess() ){
				biccDeleteList.add(scopeMap.get(assetList[i].CTN__c));
			} else {
				scopeMap.get(assetList[i].CTN__c).Error__c = 'Error while creating Asset';
				biccErrorList.add(scopeMap.get(assetList[i].CTN__c));
			}
		}

		for(BICC_CTN_Information__c error : biccErrorList){
			for(String field : sortList){
				if(queriedFields.contains(field)){
					dataError += '"' + error.get(field) + '",';
				}
			}
			dataError += '"' + error.Error__c + '"\n';
		}

		//Delete succesful matches from BICC
		delete biccErrorList;
		delete biccDeleteList;
	}

	global void finish(Database.BatchableContext BC){
		
		//Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							TotalJobItems
						  FROM
							AsyncApexJob
						  WHERE
							Id =: BC.getJobId()];

		// Send an email to InsideSalesSystems.nl@vodafone.com notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		header = header.subString(0, header.length() - 1) + ',"Error__c"\n';
		Blob b = blob.valueOf(header + dataError);
		Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
		efa.setFileName('BiccCtnInformationErrors.csv');
		efa.setBody(b);

		String[] toAddresses = new String[] {'EBUSalesForce.nl@vodafone.com'};
		//String[] toAddresses = new String[] {'ferdinandb@nncourage.com'};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Bicc Ctn Information database import ' + a.Status);
		mail.setPlainTextBody('The Bicc Ctn Information database import job processed ' + a.TotalJobItems +
							  ' batches with '+ a.NumberOfErrors + ' failures.');
		mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
}