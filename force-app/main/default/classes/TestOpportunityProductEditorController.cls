@isTest
private class TestOpportunityProductEditorController {

	@testSetup
    static void testSetup() {
        TestUtils.createOrderValidationOpportunity();
        TestUtils.createCompleteOpportunity();
        TestUtils.createOpportunityFieldMappings();
        
        Order_Form_Settings__c orderFormSetting = new Order_Form_Settings__c(
            Max_no_of_sites_in_list__c = 10
        );
        insert orderFormSetting;
    } 
    
    @isTest
    static void initializeOpp() {
		Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Test Opportunity' LIMIT 1];      
        PageReference pageRef = Page.OpportunityProductEditor;
        Test.setCurrentPage(PageRef);
        pageRef.getParameters().put('Id', opp.Id);
        OpportunityProductEditorController controller = new OpportunityProductEditorController();
        
        System.assertEquals(opp.Id, OpportunityProductEditorController.theOpp.Id);
    }
}