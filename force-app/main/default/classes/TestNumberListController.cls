@isTest
public inherited sharing class TestNumberListController {
	@isTest
	public static void testLoadThePageWithControllerEnglish() {
		cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
		basket.Contract_Language__c = 'English';
		update basket;
		Opportunity opp = [SELECT Id FROM Opportunity];

		Test.startTest();
		PageReference pageRef = Page.NumberList;
		pageRef.getParameters().put('Id', opp.Id);
		Test.setCurrentPage(pageRef);
		NumberListController controller = new NumberListController();

		//added assignment for getter methods
		String getterAttrib =
			controller.modelNumber +
			'- ' +
			controller.productName +
			'- ' +
			controller.quantity +
			'- ' +
			controller.grossListPrice +
			'- ' +
			controller.discountNew +
			'- ' +
			controller.netUnitPrice;
		//debug to bypass PMD rule (rule: Best Practices-UnusedLocalVariable)
		System.debug(loggingLevel.INFO, getterAttrib);
		Test.stopTest();

		System.assertEquals(
			opp.Id,
			controller.theOpp.Id,
			'The controller should have loaded the correct Opportunity'
		);
	}
	@isTest
	public static void testLoadThePageWithControllerDutch() {
		cscfga__Product_Basket__c basket = TestUtils.createCSProductBasket();
		basket.Contract_Language__c = 'Dutch';
		update basket;
		Opportunity opp = [SELECT Id FROM Opportunity];

		Test.startTest();
		PageReference pageRef = Page.NumberList;
		pageRef.getParameters().put('Id', opp.Id);
		Test.setCurrentPage(pageRef);
		NumberListController controller = new NumberListController();
		//added assignment for getter methods
		String getterAttrib =
			controller.modelNumber +
			'- ' +
			controller.productName +
			'- ' +
			controller.quantity +
			'- ' +
			controller.grossListPrice +
			'- ' +
			controller.discountNew +
			'- ' +
			controller.netUnitPrice;
		//debug to bypass PMD rule (rule: Best Practices-UnusedLocalVariable)
		System.debug(loggingLevel.INFO, getterAttrib);
		System.debug(loggingLevel.INFO, controller.oliHardwareMobile);
		Test.stopTest();

		System.assertEquals(
			opp.Id,
			controller.theOpp.Id,
			'The controller should have loaded the correct Opportunity'
		);
	}
}