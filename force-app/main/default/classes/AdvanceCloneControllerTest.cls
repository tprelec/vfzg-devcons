@isTest
public class AdvanceCloneControllerTest {

	private static testMethod void test() {
	    List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
	   System.runAs (simpleUser) { 
	    
	   
	    Framework__c frameworkSetting = new Framework__c();
	    frameworkSetting.Framework_Sequence_Number__c = 2;
	    insert frameworkSetting;
	    
	    PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
	       
	   insert priceResetSetting;
	    Account testAccount = CS_DataTest.createAccount('Test Account');
	    insert testAccount;
	    
	    Sales_Settings__c ssettings = new Sales_Settings__c();
	    ssettings.Postalcode_check_validity_days__c = 2;
	    ssettings.Max_Daily_Postalcode_Checks__c = 2;
	    ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
	    ssettings.Postalcode_check_block_period_days__c = 2;
	    ssettings.Max_weekly_postalcode_checks__c = 15;
	    insert ssettings;
	    
	    //createSite(String name, Account siteAccount, String postalCode, String street, String city, Decimal houseNumber)
	    Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112',testAccount, '1111AA', 'Breestraat','LEIDEN', 112); 
	    insert site;
	    
	    Site_Postal_Check__c spc = CS_DataTest.createSite(site);
	    insert spc;
	    
	    Site__c site2 = CS_DataTest.createSite('Eindhoven, Esp 130',testAccount,'2222AA', 'Esp','Eindhoven', 130); 
	    insert site2;
	    
	    Site_Postal_Check__c spc2 = CS_DataTest.createSite(site2);
	    insert spc2;
	     Site__c site3 = CS_DataTest.createSite('Eindhoven, Esp 123',testAccount,'2232AA', 'Esp','Eindhoven', 123); 
	    insert site3;
	    
	    Site_Postal_Check__c spc3 = CS_DataTest.createSite(site3);
	    insert spc3;
	    Site_Availability__c siteAvailability = CS_DataTest.createSiteAvailability('LEIDEN, Breestraat 112', site,spc); 
	    insert siteAvailability;
	    
	    
	    
	    Site_Availability__c siteAvailability2 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 130', site,spc2); 
	    insert siteAvailability2;
	    
	    Site_Availability__c siteAvailability3 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 123', site,spc3); 
	    insert siteAvailability3;
	    
	    List<Site_Postal_Check__c> spcs = [SELECT Id, Access_Active__c from Site_Postal_Check__c where Id = :spc.Id or Id = :spc2.Id or Id = :spc3.Id];
	    
	    Competitor_Asset__c ca = new Competitor_Asset__c();
	    ca.RecordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByName().get('PABX').getRecordTypeId();
	    ca.Account__c = testAccount.Id;
	    ca.Site__c = site.Id;
	    insert ca;
	    
	    Competitor_Asset__c ca2 = new Competitor_Asset__c();
	    ca2.RecordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByName().get('PABX').getRecordTypeId();
	    ca2.Site__c = site2.Id;
	    ca2.Account__c = testAccount.Id;
	    //ca2.Active_PBX__c = true;
	    insert ca2;
	    
	    Competitor_Asset__c ca3 = new Competitor_Asset__c();
	    ca3.RecordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByName().get('PABX').getRecordTypeId();
	    ca3.Site__c = site2.Id;
	    ca3.Account__c = testAccount.Id;
	    //ca3.Active_PBX__c = true;
	    insert ca3;
	    
	    
	    Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
	    insert testOpp;
	    
	    
	    cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        insert basket;
        
        String fieldsVal = '[ { "name": "Id", "label": "Id", "visibleInUI" :false }, { "name": "Name", "label": "Name", "visibleInUI" :false }, { "name": "Site__r.Name", "label": "Address", "visibleInUI" :true }, { "name": "Vendor__c", "label": "Vendor", "visibleInUI" :true }, { "name": "Order__c", "label": "Order", "visibleInUI" :false }, { "name": "Premium_Vendor__c", "label": "Premium Vendor", "visibleInUI" :true }, { "name": "Access_Infrastructure__c", "label": "Access Infrastructure", "visibleInUI" :true }, { "name": "Existing_Infra__c", "label": "Existing Infra", "visibleInUI" :true }, { "name": "Region__c", "label": "Region", "visibleInUI" :true }, { "name": "Result_Check__c", "label": "Result Check", "visibleInUI" :true }, { "name": "LastModifiedDate", "label": "LastModifiedDate", "visibleInUI" :false }, { "name": "Usable__c", "label": "Usable", "visibleInUI" :false }, { "name": "Site__r.Site_City__c", "label": "City", "visibleInUI" :true }, { "name": "Site__r.Site_Postal_Code__c", "label": "Postal Code", "visibleInUI" :true }, { "name": "Site__c", "label": "Site__c", "visibleInUI" :false }, { "name": "Site__r.Site_Street__c", "label": "Street", "visibleInUI" :false }, { "name": "Site__r.Building__c", "label": "Building", "visibleInUI" :false }, { "name": "Bandwith_Down_Entry__c", "label": "Bandwith Down Entry", "visibleInUI" :false }, { "name": "Bandwith_Down_Premium__c", "label": "Bandwith Down Premium", "visibleInUI" :true }, { "name": "Bandwith_Sellable_Down__c", "label": "Bandwith Sellable Down", "visibleInUI" :false }, { "name": "Bandwith_Sellable_Up__c", "label": "Bandwith Sellable Up", "visibleInUI" :false }, { "name": "Premium_Applicable__c", "label": "Premium Applicable", "visibleInUI" :false }, { "name": "Bandwith_Up_Entry__c", "label": "Bandwith Up Entry", "visibleInUI" :false }, { "name": "Bandwith_Up_Premium__c", "label": "Bandwith Up Premium", "visibleInUI" :true } ]';
        CS_Advance_Clone_Configuration__c fieldsConfig = CS_DataTest.createCSAdvanceCloneConfiguration('Fields', fieldsVal);
        insert fieldsConfig;
        
        String attributeVal = '[ { "name": "Total Bandwidth Down", "label": "Total Bandwidth Down", "visibleInUI" :true }, { "name": "Total Bandwidth Down Entry", "label": "Total Bandwidth Down Entry", "visibleInUI" :true }, { "name": "Total Bandwidth Down Premium", "label": "Total Bandwidth Down Premium", "visibleInUI" :true }, { "name": "Total Bandwidth Up", "label": "Total Bandwidth Up", "visibleInUI" :true }, { "name": "Total Bandwidth Up Entry", "label": "Total Bandwidth Up Entry", "visibleInUI" :false }, { "name": "Total Bandwidth Up Premium", "label": "Total Bandwidth Up Premium", "visibleInUI" :true }, { "name": "Result Check", "label": "Result Check", "visibleInUI" :true }, { "name": "Premium Applicable", "label": "Premium Applicable", "visibleInUI" :true }, { "name": "Vendor", "label": "Vendor", "visibleInUI" :true }, { "name": "Access Type", "label": "Access Type", "visibleInUI" :true } ]';
        CS_Advance_Clone_Configuration__c attrsConfig = CS_DataTest.createCSAdvanceCloneConfiguration('Attributes', attributeVal);
        insert attrsConfig;
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        Id packageDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Package Definition').getRecordTypeId(); 
        
        
        
        cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        accessInfraDef.Product_Type__c = 'Fixed';
        accessInfraDef.RecordTypeId = productDefinitionRecordType;
        insert accessInfraDef;
        
        
        
        cscfga__Product_Definition__c managedInternetDef = CS_DataTest.createProductDefinition('Managed Internet');
        managedInternetDef.Product_Type__c = 'Fixed';
        managedInternetDef.RecordTypeId = productDefinitionRecordType;
        insert managedInternetDef;
        
        cscfga__Product_Configuration__c managedInternetConf = CS_DataTest.createProductConfiguration(managedInternetDef.Id, 'Managed Internet',basket.Id);
        managedInternetConf.cscfga__Root_Configuration__c = null;
        managedInternetConf.cscfga__Parent_Configuration__c = null;
        insert managedInternetConf;
        
        cscfga__Product_Definition__c ipvpnDef = CS_DataTest.createProductDefinition('IPVPN');
        ipvpnDef.Product_Type__c = 'Fixed';
         ipvpnDef.RecordTypeId = productDefinitionRecordType;
        
        insert ipvpnDef;
        cscfga__Product_Configuration__c ipvpnConf = CS_DataTest.createProductConfiguration(ipvpnDef.Id, 'IPVPN',basket.Id);
        ipvpnConf.cscfga__Root_Configuration__c = null;
        ipvpnConf.cscfga__Parent_Configuration__c = null;
        
        insert ipvpnConf;
        
        cscfga__Product_Definition__c ipvpnPackageDef = CS_DataTest.createProductPackageDefinition('IPVPN package');
        ipvpnDef.RecordTypeId = packageDefinitionRecordType;
        ipvpnPackageDef.Product_Type__c = 'Fixed';
        insert ipvpnPackageDef;
        
        cscfga__Product_Configuration__c ipvpnPackageConf = CS_DataTest.createProductConfiguration(ipvpnPackageDef.Id, 'IPVPN package',basket.Id);
        insert ipvpnPackageConf;
        
        cscfga__Attribute_Definition__c accessPackageAttributeDef = new cscfga__Attribute_Definition__c();
        accessPackageAttributeDef.cscfga__Type__c = 'Package Slot';
        accessPackageAttributeDef.cscfga__Data_Type__c = 'String';
        accessPackageAttributeDef.Name = 'Access Infrastructure';
        accessPackageAttributeDef.cscfga__Product_Definition__c = ipvpnPackageDef.Id;
        insert accessPackageAttributeDef;
        
        cscfga__Attribute__c accessPackageAttribute = new cscfga__Attribute__c();
        accessPackageAttribute.Name = 'Access Infrastructure';
        accessPackageAttribute.cscfga__Product_Configuration__c = ipvpnPackageConf.Id;
        insert accessPackageAttribute;
        
        cscfga__Product_Configuration__c accessInfraConf = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        accessInfraConf.Site_Name__c = 'ESP 130';
        accessInfraConf.cscfga__Root_Configuration__c = null;
        accessInfraConf.cscfga__Parent_Configuration__c = null;
        accessInfraConf.cscfga__package_slot__c = accessPackageAttribute.Id;
        
        accessInfraConf.ClonedSiteIds__c = '{"0000":"'+String.valueOf(siteAvailability.Id)+'","UhTY":"'+String.valueOf(siteAvailability2.Id)+'"}';
        accessInfraConf.ClonedPBXIds__c = '{"0000":"'+String.valueOf(ca.Id)+'","UhTY":"'+String.valueOf(ca2.Id)+'"}';
        
        insert accessInfraConf;
        
        
        cscfga__Attribute_Definition__c siteCheckIdAttributeDef = new cscfga__Attribute_Definition__c();
        siteCheckIdAttributeDef.cscfga__Type__c = 'Calculation';
        siteCheckIdAttributeDef.cscfga__Data_Type__c = 'String';
        siteCheckIdAttributeDef.Name = 'Site Check Id';
        siteCheckIdAttributeDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert siteCheckIdAttributeDef;
        
        cscfga__Attribute__c siteCheckIdAttribute = new cscfga__Attribute__c();
        siteCheckIdAttribute.Name = 'Site Check Id';
        siteCheckIdAttribute.cscfga__Value__c = String.valueOf(siteAvailability.Id);
        siteCheckIdAttribute.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert siteCheckIdAttribute;
        
        cscfga__Attribute_Definition__c pbxAttributeDef = new cscfga__Attribute_Definition__c();
        pbxAttributeDef.cscfga__Type__c = 'Calculation';
        pbxAttributeDef.cscfga__Data_Type__c = 'String';
        pbxAttributeDef.Name = 'PBX';
        pbxAttributeDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert pbxAttributeDef;
        
        cscfga__Attribute__c pbxAttribute = new cscfga__Attribute__c();
        pbxAttribute.Name = 'PBX';
        pbxAttribute.cscfga__Value__c = String.valueOf(ca.Id);
        pbxAttribute.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert pbxAttribute;
        
        cscfga__Attribute_Definition__c requiresReCloneAttDef = new cscfga__Attribute_Definition__c();
        requiresReCloneAttDef.cscfga__Type__c = 'Text Display';
        requiresReCloneAttDef.cscfga__Data_Type__c = 'String';
        requiresReCloneAttDef.Name = 'RequiresReClone';
        requiresReCloneAttDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert requiresReCloneAttDef;
        
        cscfga__Attribute__c requiresReCloneAtt = new cscfga__Attribute__c();
        requiresReCloneAtt.Name = 'RequiresReClone';
        requiresReCloneAtt.cscfga__Value__c = '';
        requiresReCloneAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert requiresReCloneAtt;
        
        
        //Region
        cscfga__Attribute_Definition__c regionDef = new cscfga__Attribute_Definition__c();
        regionDef.cscfga__Type__c = 'Text Display';
        regionDef.cscfga__Data_Type__c = 'String';
        regionDef.Name = 'Region';
        regionDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert regionDef;
        
        cscfga__Attribute__c regionAtt = new cscfga__Attribute__c();
        regionAtt.Name = 'Region';
        regionAtt.cscfga__Value__c = 'Test';
        regionAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert regionAtt;
        
        //Access Type
        cscfga__Attribute_Definition__c accessTypeDef = new cscfga__Attribute_Definition__c();
        accessTypeDef.cscfga__Type__c = 'Text Display';
        accessTypeDef.cscfga__Data_Type__c = 'String';
        accessTypeDef.Name = 'Access Type';
        accessTypeDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert accessTypeDef;
        
        cscfga__Attribute__c accessTypeAtt = new cscfga__Attribute__c();
        accessTypeAtt.Name = 'Access Type';
        accessTypeAtt.cscfga__Value__c = 'EthernetOverCopper';
        accessTypeAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert accessTypeAtt;
        
        
         //Vendor
        cscfga__Attribute_Definition__c vendorDef = new cscfga__Attribute_Definition__c();
        vendorDef.cscfga__Type__c = 'Text Display';
        vendorDef.cscfga__Data_Type__c = 'String';
        vendorDef.Name = 'Vendor';
        vendorDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert vendorDef;
        
        cscfga__Attribute__c vendorAtt = new cscfga__Attribute__c();
        vendorAtt.Name = 'Vendor';
        vendorAtt.cscfga__Value__c = 'KPNWEAS';
        vendorAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert vendorAtt;
        
        
        //Premium Applicable
        cscfga__Attribute_Definition__c premiumApplicableDef = new cscfga__Attribute_Definition__c();
        premiumApplicableDef.cscfga__Type__c = 'Checkbox';
        premiumApplicableDef.cscfga__Data_Type__c = 'Boolean';
        premiumApplicableDef.Name = 'Premium Applicable';
        premiumApplicableDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert premiumApplicableDef;
        
        cscfga__Attribute__c premiumApplicableAtt = new cscfga__Attribute__c();
        premiumApplicableAtt.Name = 'Premium Applicable';
        premiumApplicableAtt.cscfga__Value__c = 'false';
        premiumApplicableAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert premiumApplicableAtt;
        
        //Result Check
        cscfga__Attribute_Definition__c resultCheckDef = new cscfga__Attribute_Definition__c();
        resultCheckDef.cscfga__Type__c = 'Text Display';
        resultCheckDef.cscfga__Data_Type__c = 'String';
        resultCheckDef.Name = 'Result Check';
        resultCheckDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert resultCheckDef;
        
        cscfga__Attribute__c resultCheckAtt = new cscfga__Attribute__c();
        resultCheckAtt.Name = 'Result Check';
        resultCheckAtt.cscfga__Value__c = 'ONNET';
        resultCheckAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert resultCheckAtt;
        
        
        //Total Bandwidth Down
        cscfga__Attribute_Definition__c totalBanDownDef = new cscfga__Attribute_Definition__c();
        totalBanDownDef.cscfga__Type__c = 'Display Value';
        totalBanDownDef.cscfga__Data_Type__c = 'Decimal';
        totalBanDownDef.Name = 'Total Bandwidth Down';
        totalBanDownDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert totalBanDownDef;
        
        cscfga__Attribute__c totalBanDownAtt = new cscfga__Attribute__c();
        totalBanDownAtt.Name = 'Total Bandwidth Down';
        totalBanDownAtt.cscfga__Value__c = '20480.0';
        totalBanDownAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert totalBanDownAtt;
        
         //Total Bandwidth Up
        cscfga__Attribute_Definition__c totalBanUpDef = new cscfga__Attribute_Definition__c();
        totalBanUpDef.cscfga__Type__c = 'Display Value';
        totalBanUpDef.cscfga__Data_Type__c = 'Decimal';
        totalBanUpDef.Name = 'Total Bandwidth Down';
        totalBanUpDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert totalBanUpDef;
        
        cscfga__Attribute__c totalBanUpAtt = new cscfga__Attribute__c();
        totalBanUpAtt.Name = 'Total Bandwidth Up';
        totalBanUpAtt.cscfga__Value__c = '20480.0';
        totalBanUpAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert totalBanUpAtt;
        
         //Total Bandwidth Down Entry
        cscfga__Attribute_Definition__c totalBanDownEntryDef = new cscfga__Attribute_Definition__c();
        totalBanDownEntryDef.cscfga__Type__c = 'Display Value';
        totalBanDownEntryDef.cscfga__Data_Type__c = 'Decimal';
        totalBanDownEntryDef.Name = 'Total Bandwidth Down Entry';
        totalBanDownEntryDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert totalBanDownEntryDef;
        
        cscfga__Attribute__c totalBanDownEntryAtt = new cscfga__Attribute__c();
        totalBanDownEntryAtt.Name = 'Total Bandwidth Down Entry';
        totalBanDownEntryAtt.cscfga__Value__c = '20480.0';
        totalBanDownEntryAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert totalBanDownEntryAtt;
        
        //Total Bandwidth Up Entry
        cscfga__Attribute_Definition__c totalBanUpEntryDef = new cscfga__Attribute_Definition__c();
        totalBanUpEntryDef.cscfga__Type__c = 'Display Value';
        totalBanUpEntryDef.cscfga__Data_Type__c = 'Decimal';
        totalBanUpEntryDef.Name = 'Total Bandwidth Up Entry';
        totalBanUpEntryDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert totalBanUpEntryDef;
        
        cscfga__Attribute__c totalBanUpEntryAtt = new cscfga__Attribute__c();
        totalBanUpEntryAtt.Name = 'Total Bandwidth Up Entry';
        totalBanUpEntryAtt.cscfga__Value__c = '20480.0';
        totalBanUpEntryAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert totalBanUpEntryAtt;
        
         //Total Bandwidth Up Premium
        cscfga__Attribute_Definition__c totalBanUpPremiumDef = new cscfga__Attribute_Definition__c();
        totalBanUpPremiumDef.cscfga__Type__c = 'Display Value';
        totalBanUpPremiumDef.cscfga__Data_Type__c = 'Decimal';
        totalBanUpPremiumDef.Name = 'Total Bandwidth Up Premium';
        totalBanUpPremiumDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert totalBanUpPremiumDef;
        
        cscfga__Attribute__c totalBanUpPremiumAtt = new cscfga__Attribute__c();
        totalBanUpPremiumAtt.Name = 'Total Bandwidth Up Premium';
        totalBanUpPremiumAtt.cscfga__Value__c = '20480.0';
        totalBanUpPremiumAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert totalBanUpPremiumAtt;
        
         //Total Bandwidth Down Premium
        cscfga__Attribute_Definition__c totalBanDownPremiumDef = new cscfga__Attribute_Definition__c();
        totalBanDownPremiumDef.cscfga__Type__c = 'Display Value';
        totalBanDownPremiumDef.cscfga__Data_Type__c = 'Decimal';
        totalBanDownPremiumDef.Name = 'Total Bandwidth Down Premium';
        totalBanDownPremiumDef.cscfga__Product_Definition__c = accessInfraDef.Id;
        insert totalBanDownPremiumDef;
        
        cscfga__Attribute__c totalBanDownPremiumAtt = new cscfga__Attribute__c();
        totalBanDownPremiumAtt.Name = 'Total Bandwidth Down Premium';
        totalBanDownPremiumAtt.cscfga__Value__c = '20480.0';
        totalBanDownPremiumAtt.cscfga__Product_Configuration__c = accessInfraConf.Id;
        insert totalBanDownPremiumAtt;
        
        
        csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(ipvpnPackageConf);
        pcr.csbb__Product_Configuration__c = ipvpnPackageConf.Id;
        insert pcr;
        
        Map<String, String> sites = new Map<String, String>();
        sites.put(String.valueOf(siteAvailability.Id), String.valueOf(site.Id));
        
        
        
        
        AdvanceCloneController.getSiteAddresses(pcr.Id);
        
        
        AdvanceCloneController.performAdvanceClone(sites, String.valueOf(basket.Id), String.valueOf(pcr.Id), String.valueOf(accessInfraConf.Id), String.valueOf(siteAvailability.Id), '', String.valueOf(ipvpnPackageConf.Id));
        
        List<FieldValues> siteFields = new List<FieldValues>();
        siteFields.add(new FieldValues('vendor', 'KPN', 'Vendor',true));
        siteFields.add(new FieldValues('Coax', 'Access Type',true));
        siteFields.add(new FieldValues('Bandwidth__c', true));
        siteFields.add(new FieldValues('Bandwidth__c','Available Bandwidth', 'true'));
        
        
        SiteDetails siteDetails1= new SiteDetails(siteAvailability.Id, siteAvailability.Id,'pbx', siteFields);
        
        //accessInfraConf.ClonedSiteIds__c = '{"0000":"'+String.valueOf(siteAvailability.Id)+'","UhTY":"'+String.valueOf(siteAvailability2.Id)+'"}';
        //accessInfraConf.ClonedPBXIds__c = '{"0000":"null","UhTY":"null"}';
        
        //update accessInfraConf;
        
        
        //requiresReCloneAtt.cscfga__Value__c = 'needsReclone';
        //update requiresReCloneAtt;
        
        //AdvanceCloneController.performAdvanceClone(sites, String.valueOf(basket.Id), String.valueOf(pcr.Id), String.valueOf(accessInfraConf.Id), String.valueOf(siteAvailability.Id), '', String.valueOf(ipvpnPackageConf.Id));
        
        
	   }
	}

}