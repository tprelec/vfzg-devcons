@isTest
private class LG_OpportunityTriggerHandlerTest {
	private static Date someDate = Date.newInstance(1960, 2, 17);

	@testsetup
	private static void setupTestData() {
		Framework__c f = Framework__c.getOrgDefaults();
		f.Framework_Sequence_Number__c = 1;
		upsert f;

		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = false;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		Account account = LG_GeneralTest.createAccount('Account', '12345678', 'Ziggo', true);
		Opportunity opp = LG_GeneralTest.createOpportunity(account, false);
		opp.LG_CreatedFrom__c = 'Tablet';
		opp.RecordTypeId = GeneralUtils.getRecordTypeIdByDeveloperName('Opportunity', 'SOHO_SMALL');
		insert opp;
		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', account, null, opp, false);
		basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
		basket.csordtelcoa__Change_Type__c = 'Migrate';
		insert basket;

		cscfga__Product_Category__c prodCategory = LG_GeneralTest.createProductCategory('Test Category', true);

		cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('Phone Numbers', false);

		prodDef.cscfga__Product_Category__c = prodCategory.Id;
		insert prodDef;

		cscrm__Address__c address = new cscrm__Address__c(cscrm__Street__c = 'TestStreet');
		insert address;

		csord__Order_Request__c coreq = new csord__Order_Request__c(csord__Module_Name__c = 'Test', csord__Module_Version__c = '1.0');
		insert coreq;

		csord__Subscription__c sub = new csord__Subscription__c(csord__Identification__c = 'TestIdent', csord__Order_Request__c = coreq.Id);
		insert sub;

		csord__Subscription__c sub2 = new csord__Subscription__c(csord__Identification__c = 'TestIdent2', csord__Order_Request__c = coreq.Id);
		insert sub2;

		csord__Service__c service = new csord__Service__c(
			csord__Identification__c = 'TestService',
			csord__Subscription__c = sub.Id,
			csord__Order_Request__c = coreq.Id
		);
		service.LG_Address__c = address.Id;
		insert service;

		csord__Service__c service2 = new csord__Service__c(
			csord__Identification__c = 'TestService2',
			csord__Subscription__c = sub2.Id,
			csord__Order_Request__c = coreq.Id
		);
		service2.LG_Address__c = address.Id;
		insert service2;

		insert new csordtelcoa__Subscr_MACDProductBasket_Association__c(
			csordtelcoa__Subscription__c = sub.Id,
			csordtelcoa__Product_Basket__c = basket.Id,
			LG_DeactivationWishDate__c = someDate
		);

		insert new csordtelcoa__Subscription_MACDOpportunity_Association__c(
			csordtelcoa__Subscription__c = sub.Id,
			csordtelcoa__Opportunity__c = opp.Id
		);
		insert new csordtelcoa__Subscription_MACDOpportunity_Association__c(
			csordtelcoa__Subscription__c = sub2.Id,
			csordtelcoa__Opportunity__c = opp.Id
		);

		cscfga__Product_Configuration__c pc = LG_GeneralTest.createProductConfiguration('TestConf', 12, basket, prodDef, false);
		pc.csordtelcoa__Replaced_Subscription__c = sub.Id;
		insert pc;

		csordtelcoa__Orders_Subscriptions_Options__c osOptions = new csordtelcoa__Orders_Subscriptions_Options__c();
		osOptions.LG_ServiceRequestDeactivateStatus__c = 'Service Termination Requested';
		osOptions.LG_SubscriptionRequestDeactivateStatus__c = 'Subscription Termination Requested';
		insert osOptions;

		noTriggers.Flag__c = false;
		upsert noTriggers;
	}

	@isTest
	public static void testCalculateD2DCompensationNewCustomer() {
		List<OE_Product__c> products = new List<OE_Product__c>();
		Opportunity theOpp = [
			SELECT Id, LG_NEWSalesChannel__c, Bundle_Name__c, Internet_Customer__c, LG_ContractTermMonths__c
			FROM Opportunity
			LIMIT 1
		];
		theOpp.LG_NEWSalesChannel__c = 'D2D';
		theOpp.Bundle_Name__c = 'Zakelijk Internet Max';
		theOpp.LG_ContractTermMonths__c = 12;
		theOpp.Internet_Customer__c = false;
		theOpp.Order_Date__c = Date.today();
		theOpp.StageName = 'Prospect';

		OE_Product__c main = new OE_Product__c(Type__c = 'Main');
		products.add(main);

		OE_Product__c internet = new OE_Product__c(Type__c = 'Internet');
		products.add(internet);

		insert products;

		OE_Product_Bundle__c bundle = new OE_Product_Bundle__c(
			Main_Product__c = main.Id,
			Internet_Product__c = internet.Id,
			Bundle_Product_Code__c = '1111',
			Default__c = true,
			Name = 'Zakelijk Internet Max'
		);
		insert bundle;

		D2D_Compensation__c compensation = TestUtils.createD2Dcompensation();
		TestUtils.createD2DcompensationItem(bundle, compensation, 10);

		Test.startTest();
		update theOpp;
		Test.stopTest();

		Opportunity result = [SELECT Shadow_Salesfee__c FROM Opportunity WHERE Id = :theOpp.Id];
	}

	@isTest
	public static void testCalculateD2DCompensationExistingCustomer() {
		List<OE_Product__c> products = new List<OE_Product__c>();
		Opportunity theOpp = [
			SELECT Id, LG_NEWSalesChannel__c, Bundle_Name__c, Internet_Customer__c, LG_ContractTermMonths__c, Bundle_Segment__c
			FROM Opportunity
			LIMIT 1
		];
		theOpp.LG_NEWSalesChannel__c = 'D2D';
		theOpp.Bundle_Name__c = 'Zakelijk Internet Max';
		theOpp.LG_ContractTermMonths__c = 12;
		theOpp.Internet_Customer__c = true;
		theOpp.Order_Date__c = Date.today();
		theOpp.Amount = 200;
		theOpp.Current_Amount_incl_VAT__c = 100;
		theOpp.StageName = 'Prospect';
		OE_Product__c main = new OE_Product__c(Type__c = 'Main');
		products.add(main);

		OE_Product__c internet = new OE_Product__c(Type__c = 'Internet');
		products.add(internet);

		insert products;

		OE_Product_Bundle__c bundle = new OE_Product_Bundle__c(
			Main_Product__c = main.Id,
			Internet_Product__c = internet.Id,
			Bundle_Product_Code__c = '1111',
			Default__c = true,
			Name = 'Zakelijk Internet Max'
		);
		insert bundle;

		D2D_Compensation__c compensation = TestUtils.createD2Dcompensation();
		TestUtils.createD2DcompensationItem(bundle, compensation, 10);

		Test.startTest();
		update theOpp;
		Test.stopTest();

		Opportunity result = [SELECT Shadow_Salesfee__c FROM Opportunity WHERE Id = :theOpp.Id];
	}

	@isTest
	public static void testCalculateD2DCompensationPartner() {
		List<OE_Product__c> products = new List<OE_Product__c>();
		Account partnerAccount = TestUtils.createPartnerAccount();
		User partner = TestUtils.createPortalUser(partnerAccount);
		Opportunity theOpp = [
			SELECT Id, LG_NEWSalesChannel__c, Bundle_Name__c, Internet_Customer__c, LG_ContractTermMonths__c, OwnerId
			FROM Opportunity
			LIMIT 1
		];
		theOpp.LG_NEWSalesChannel__c = 'D2D';
		theOpp.Bundle_Name__c = 'Zakelijk Internet Max';
		theOpp.LG_ContractTermMonths__c = 12;
		theOpp.Internet_Customer__c = true;
		theOpp.Order_Date__c = Date.today();
		theOpp.Amount = 200;
		theOpp.StageName = 'Prospect';
		theOpp.Current_Amount_incl_VAT__c = 100;
		theOpp.OwnerId = partner.Id;

		OE_Product__c main = new OE_Product__c(Type__c = 'Main');
		products.add(main);

		OE_Product__c internet = new OE_Product__c(Type__c = 'Internet');
		products.add(internet);

		insert products;

		OE_Product_Bundle__c bundle = new OE_Product_Bundle__c(
			Main_Product__c = main.Id,
			Internet_Product__c = internet.Id,
			Bundle_Product_Code__c = '1111',
			Default__c = true,
			Name = 'Zakelijk Internet Max'
		);
		insert bundle;

		D2D_Compensation__c compensation = TestUtils.createD2Dcompensation();
		TestUtils.createD2DcompensationItem(bundle, compensation, 10);

		TestUtils.createD2DcompensationAccount(partnerAccount, compensation);

		Test.startTest();
		update theOpp;
		Test.stopTest();

		Opportunity result = [SELECT Shadow_Salesfee__c FROM Opportunity WHERE Id = :theOpp.Id];
	}

	@isTest
	public static void testProcessMobileOrder() {
		Opportunity theOpp = [SELECT Id, StageName, Journey_Type__c FROM Opportunity LIMIT 1];
		theOpp.StageName = 'Ready for Order';
		theOpp.Journey_Type__c = 'Mobile';
		Test.startTest();
		// Commented for feature flagging
		Opportunity oldOpp = theOpp.clone();
		oldOpp.StageName = 'Installed/Ready for Billing';
		LG_OpportunityTriggerHandler.processMobileOrder(new List<Opportunity>{ theOpp }, new Map<Id, Opportunity>{ theOpp.Id => oldOpp });

		LG_OpportunityTriggerHandler.processMobileOrder(new List<Opportunity>{ theOpp }, new Map<Id, Opportunity>{ theOpp.Id => oldOpp });
		Test.stopTest();
		theOpp = [SELECT Id, StageName, Journey_Type__c FROM Opportunity LIMIT 1];
		System.assertEquals('Need Additional Information', theOpp.StageName, 'Stage should be Need Additional Information');
	}

	@isTest
	public static void testCalculateD2DCompensationNewCustomerJourneyMobile() {
		List<OE_Product__c> products = new List<OE_Product__c>();
		Opportunity theOpp = [
			SELECT Id, LG_NEWSalesChannel__c, Bundle_Name__c, Internet_Customer__c, LG_ContractTermMonths__c
			FROM Opportunity
			LIMIT 1
		];
		theOpp.LG_NEWSalesChannel__c = 'D2D';
		theOpp.Bundle_Name__c = 'Zakelijk Internet Max';
		theOpp.LG_ContractTermMonths__c = 12;
		theOpp.Internet_Customer__c = false;
		theOpp.Order_Date__c = Date.today();
		theOpp.Journey_Type__c = 'Mobile';
		theOpp.StageName = 'Need Additional Information';

		OE_Product__c main = new OE_Product__c(Type__c = 'Main');
		products.add(main);

		OE_Product__c internet = new OE_Product__c(Type__c = 'Internet');
		products.add(internet);

		insert products;
		OrderType__c ot = TestUtils.createOrderType();

		Product2 p = TestUtils.createProduct();
		PriceBookEntry pbEntry = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), p);

		OpportunityLineItem oppLineItem = new OpportunityLineItem(
			OpportunityId = theOpp.id,
			Quantity = 1,
			Gross_List_Price__c = 1,
			DiscountNew__c = 0,
			UnitPrice = 0,
			Product_Arpu_Value__c = 0,
			Duration__c = 1,
			PricebookEntryId = pbEntry.Id,
			LG_MainOLI__c = true,
			CLC__c = 'Acq'
		);

		insert oppLineItem;
		OE_Product_Bundle__c bundle = new OE_Product_Bundle__c(
			Main_Product__c = main.Id,
			Internet_Product__c = internet.Id,
			Bundle_Product_Code__c = '1111',
			Default__c = true,
			Name = 'Zakelijk Internet Max'
		);
		insert bundle;

		D2D_Compensation__c compensation = TestUtils.createD2Dcompensation();
		TestUtils.createD2DcompensationItem(bundle, compensation, 10);

		Test.startTest();
		update theOpp;
		Test.stopTest();

		Opportunity result = [SELECT Shadow_Salesfee__c FROM Opportunity WHERE Id = :theOpp.Id];
	}
}
