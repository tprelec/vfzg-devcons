public with sharing class CS_COMCloseCaseController {

	public static final String PM_START_CASE_OWNER_NOT_USER = 'FIELD_CUSTOM_VALIDATION_EXCEPTION, Before closing, Owner needs to be changed from Queue to User.';
	public static final String PM_START_CASE_SOLUTIONS_NOT_KICKED_OFF = 'FIELD_CUSTOM_VALIDATION_EXCEPTION, Not all solutions were kicked off or cancelled.';

	@AuraEnabled
	public static String closeCase(Id recordId){
		String result = '';
		
        
        List<Task> tasks = [SELECT Id, Record_Type_Text__c, OwnerId, Owner_Type__c, Delivery_Task_Finished__c, WhatId,
		                    Install_Solution_Jeopardy_Outcome__c, Order__c, Suborder__c, Suborder__r.Delivery_Components_For_Installation__c, Type
		                    FROM Task
		                    WHERE Id = :recordId];
        
		try{
			if(tasks[0].Record_Type_Text__c.equals('COM_Delivery')) {
				if(tasks[0].Type.equals('COM_Installation')) {
					closeTaskForSolutionInstallation(tasks[0]);
				} else if(tasks[0].Type.equals('COM_Installation Jeopardy')) {
					closeTaskForSuborderInstallationJeopardy(tasks[0]);
				} else if(tasks[0].Type.equals('COM_Prepare Project')) {
					closeTaskForDeliveryPMProjectStart(tasks[0]);
				}
				updateTaskStatus(tasks[0],'Completed');
				result = 'Task Closed';
			}

		} catch(Exception e) {
			return e.getMessage();
		}

		return result;
	}

	private static void updateTaskStatus(Task c, String status){
		c.Status = status;
		c.Delivery_Task_Finished__c = true;

		List<Task> tasksToUpdate = new List<Task>();
		tasksToUpdate.add(c);
		update tasksToUpdate;
	}

	private static Task closeTaskForSuborderInstallationJeopardy(Task c){
		List<Suborder__c> suborders = [SELECT Id, Name, Installation_Jeopardy_Result__c
		                                      FROM Suborder__c
		                                      WHERE Id = :c.Suborder__c];

		suborders[0].Installation_Jeopardy_Result__c = c.Install_Solution_Jeopardy_Outcome__c;
		update suborders;

		return c;
	}

	private static Task closeTaskForSolutionInstallation(Task c){
		List<String> deliveryComponentIds = c.Suborder__r.Delivery_Components_For_Installation__c != null ? c.Suborder__r.Delivery_Components_For_Installation__c.split(',') : new List<String>();
		Boolean deliveryComponentsImplemented = true;
		List<Suborder__c> subordersToUpdate = new List<Suborder__c>();

		List<Task>  tasksWithAttachments = [SELECT Id,
											Suborder__c,
		                                    Installation_issue_category__c,
											Installation_Issue_Subcategories__c,
											Installation_Issue_Comments__c,
				(SELECT Id FROM CombinedAttachments)
		                                    FROM Task
		                                    WHERE Id = :c.Id];

		Map<Id,Suborder__c> subordersWithAttachmentsMap = new Map<Id,Suborder__c>(
											[SELECT Id, (SELECT Id FROM CombinedAttachments)
											FROM Suborder__c
											WHERE Id = :c.Suborder__c]
											);

		System.debug('+++deliveryComponentsImplemented casesWithAttachments: ' + JSON.serializePretty(tasksWithAttachments));

		List<Delivery_Component__c> deliveryComponentsForInstallation = [SELECT Id, Implemented__c
																		FROM Delivery_Component__c
																		WHERE Id IN :deliveryComponentIds];

		System.debug('+++deliveryComponentsImplemented deliveryComponentsForInstallation: ' + deliveryComponentsForInstallation);

		for (Delivery_Component__c deliveryComponentRecord : deliveryComponentsForInstallation) {
			if (!deliveryComponentRecord.Implemented__c) {
				deliveryComponentsImplemented = false;
			}
		}

		System.debug('+++deliveryComponentsImplemented: ' + deliveryComponentsImplemented);

		if (deliveryComponentsImplemented) {
		    
			if (tasksWithAttachments[0].CombinedAttachments.size() == 0) {
				throw new CS_ComCustomException('FIELD_CUSTOM_VALIDATION_EXCEPTION, Solution should have at least one Attachment!');
			}

			subordersToUpdate.add(new Suborder__c(Id = c.Suborder__c,
					Delivery_successful__c = true)
			);
		} else {
			if (tasksWithAttachments[0].Installation_issue_category__c == null) {
				throw new CS_ComCustomException('FIELD_CUSTOM_VALIDATION_EXCEPTION, If the Solution is not Implemented, then the Installation_issue_category__c field is mandatory');
			} else if (tasksWithAttachments[0].Installation_Issue_Comments__c == null) {
				throw new CS_ComCustomException('FIELD_CUSTOM_VALIDATION_EXCEPTION, If the Solution is not Implemented, then the Installation_Issue_Comments__c field is mandatory');
			} else if (tasksWithAttachments[0].Installation_issue_category__c != 'Cable damage/civil problem' && tasksWithAttachments[0].Installation_Issue_Subcategories__c == null) {
				throw new CS_ComCustomException('FIELD_CUSTOM_VALIDATION_EXCEPTION, If the Solution is not Implemented, then the Installation_Issue_Subcategories__c field is mandatory');
			} else if (!String.isEmpty(tasksWithAttachments[0].Installation_issue_category__c) && tasksWithAttachments[0].CombinedAttachments.isEmpty()){
				throw new CS_ComCustomException('FIELD_CUSTOM_VALIDATION_EXCEPTION, Case needs to have at least one attachment');
			}
		}

		if (!subordersToUpdate.isEmpty()) {
			update subordersToUpdate;
		}

		return c;
	}

	private static Task closeTaskForDeliveryPMProjectStart(Task c){
		if (c.Owner_Type__c != 'User') {
			throw new CS_ComCustomException(PM_START_CASE_OWNER_NOT_USER);
		}
		Boolean allSubordersKickedOff = true;
		for (Suborder__c sub : [SELECT Id, Start_Delivery__c, Status__c
		                               FROM Suborder__c
		                               WHERE Order__c = :c.WhatId /* AND RecordType.DeveloperName = 'Suborder'*/ ]) {
			if (!sub.Start_Delivery__c && sub.Status__c != 'Cancelled' && sub.Status__c != 'Cancellation Requested') {
				allSubordersKickedOff = false;
				break;
			}
		}
		if(!allSubordersKickedOff) {
			throw new CS_ComCustomException(PM_START_CASE_SOLUTIONS_NOT_KICKED_OFF);
		}
		update new csord__Order__c (Id = c.Order__c, Delivery_Project_Manager__c = c.OwnerId);
		return c;
	}
}