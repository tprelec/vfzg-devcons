@IsTest
public with sharing class TestOrderEntryPortingController {
	@TestSetup
	static void makeData() {
		OrderEntryTestDataFactory.insertMockJsonData(true);
	}

	@IsTest
	public static void testSaveFixedPortingNumbers() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		OrderEntryPortingController.saveFixedPortingNumbers(opp.Id);

		List<LG_PortingNumber__c> portingNumbers = [SELECT Id FROM LG_PortingNumber__c];

		System.assertEquals(1, portingNumbers.size(), 'Porting number added');
	}

	@IsTest
	public static void testUpdatePortingNumber() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];

		Test.startTest();

		OrderEntryPortingController.updatePortingNumber(opp.Id);

		Test.stopTest();

		List<LG_PortingNumber__c> portingNumbers = [SELECT Id FROM LG_PortingNumber__c WHERE LG_Opportunity__c = :opp.Id];

		System.assertEquals(1, portingNumbers.size(), 'One LG_PortingNUmber__c exists for this Opportunity');
	}
}
