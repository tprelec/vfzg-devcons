global class CalloutResponsePEALRFSCheck extends csbb.CalloutResponseManagerExt {
    
    private String availabilityResponse {get; set;}
    private String rfsCheckFailureMessage {get; set;}
    
    global CalloutResponsePEALRFSCheck (Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {
        this.setData(mapCR, productCategory, productResponse);
    }
    
    global CalloutResponsePEALRFSCheck () {
    }
    
    global void setData(Map<String, csbb.CalloutResponse> mapCR, csbb.ProductCategory productCategory, csbb.CalloutProduct.ProductResponse productResponse) {
        this.service = 'PEALRFSCheck';
        this.productCategoryId = productCategory.productCategoryId;
        this.mapCR = mapCR;
        this.productCategory = productCategory;
        this.productResponse = productResponse;
        this.setPrimaryCalloutResponse();
    }
    
    global Map<String, Object> processResponseRaw (Map<String, Object> inputMap) {
        system.debug('+++processResponseRaw inputMap: ' + inputMap);
        String httpStatusCode = (String) inputMap.get('httpStatusCode');
        
        if (httpStatusCode != '200')
        {
            String respMessage = 'The RFS check failed. PEAL is not available, please try later or use offer with AOP product.';
            if (httpStatusCode != null)
            {
                respMessage = 'The RFS check failed with code ' + httpStatusCode + '.';
                if (httpStatusCode == '400')
                {
                    respMessage = respMessage 
                        + ' One of the mandatory parameters are missing (chl, cty or addressId).';
                }
                else if (httpStatusCode == '500')
                {
                    respMessage = respMessage
                        + ' Internal Server Error.';
                }
                else if (httpStatusCode == '404')
                {
                    respMessage = respMessage
                        + ' No Address found.';
                }
            }
            rfsCheckFailureMessage = respMessage;
        }
        else
        {    
            String response = (String) inputMap.get('availabilityResponseRaw');
            availabilityResponse = response.substring(16,response.length()-1).normalizeSpace();
            
            //In try catch - as we don't want to block making an offer if footprint update has failed,
            //as footprint can always be updated later manually.
            try
            {
                String rfsCommonJson = LG_RfsCheckUtility.getCommonRfsJsonFormat(availabilityResponse);
                System.debug('rfsCommonJson: ' + rfsCommonJson);
                LG_RfsCheckUtility.RfsCommon rfsCommon = (LG_RfsCheckUtility.RfsCommon) JSON.deserialize(rfsCommonJson, LG_RfsCheckUtility.RfsCommon.class);
                
                //Set the footprint on the address record.
                LG_Util.updatePremiseFootprint(rfsCommon.addressId, rfsCommon.footprint);
            } 
            catch (Exception ex)
            {
                System.debug('Setting the footprint from the PEAL RFS response failed. Exception is: ' + ex);
            }
        }
        return new Map<String, Object>();
    }
    
    global Map<String, Object> getDynamicRequestParameters (Map<String, Object> inputMap) {
        
        if (inputMap.containsKey('POSTCODE')) {
            
            String postcode = ((String)inputMap.get('POSTCODE')).toUpperCase();
            
            inputMap.put('POSTCODE', (Object)LG_Util.trimAll(postcode));
        }
        
        // return new Map<String, Object>();
        return inputMap;
    }
    
    global void runBusinessRules (String categoryIndicator) {        
        Boolean internetAvailable = false;
        String upspeed;
        String downspeed = '0';
        
        //CATGOV-1052 start
        String gigaAvailable = '';
        //CATGOV-1052 end
        
        if (rfsCheckFailureMessage != null)
        {
            this.productResponse.available = 'false';
            this.productResponse.fields.put('rfsCheckFailureMessage', rfsCheckFailureMessage);
            this.productResponse.displayMessage = rfsCheckFailureMessage;
        }
        else
        {
            this.productResponse.available = 'true';
            String rfsCommonJson = LG_RfsCheckUtility.getCommonRfsJsonFormat(availabilityResponse);
            System.debug('rfsCommonJson: ' + rfsCommonJson);
            LG_RfsCheckUtility.RfsCommon rfsCommon = (LG_RfsCheckUtility.RfsCommon) JSON.deserialize(rfsCommonJson, LG_RfsCheckUtility.RfsCommon.class);
            system.debug('the value in rfscommon.availability is'+rfsCommon.availability);
            //FootPrint
            //String footprint = rfsCommon.footprint;
            for (LG_RfsCheckUtility.common_Availability avail : rfsCommon.availability)
            {
                if (avail.capability.equals('Data') && avail.technology.equals('HFC'))
                {
                    internetAvailable = true;
                    
                    for (LG_RfsCheckUtility.Limits lim : avail.limits)
                    {
                        upspeed = lim.up;
                        downspeed = lim.down;
                    }
                }
                else if (avail.capability.equals('Voice') && avail.technology.equals('Mobile'))
                {
                    //mobile
                }
                else if (avail.capability.equals('TV') && avail.technology.equals('Digital'))
                {
                    //dtv
                }
                else if (avail.capability.equals('TV') && avail.technology.equals('Analogue'))
                {
                    //catv
                }
                else if (avail.capability.equals('Voice') && avail.technology.equals('Digital'))
                {
                    //telephony
                }
            }
            this.productResponse.displayMessage = ('Top speed for this site is ' + downspeed);
            this.productResponse.fields.put('topspeed', downspeed);
            this.crPrimary.mapDynamicFields.put('topspeed', downspeed);
            
            /*Below line map the response of gigaAvailable variable from RFSCheckUtility class.In second line it will map the gigaAvailable value to the giga attribute 
			which is already defined in Callout Response Mapper with the giga Product available attribute of Zakelijk Internet PD.(CATGOV-1052 Start)*/
            gigaAvailable = LG_RfsCheckUtility.gigaAvailable;
            this.crPrimary.mapDynamicFields.put('giga', gigaAvailable); 
            //CATGOV-1052 end            
        }
        
        //Map<String, String> resultMap = (Map<String, String>)JSON.deserialize(resultJson, Map<String, String>.class);
        
        //system.debug('resultMap: ' + resultMap);no need for this as you're not using resultMap anymore
        system.debug('this.crPrimary.mapDynamicFields is ' +  this.crPrimary.mapDynamicFields);
        system.debug('this.productResponse.fields is ' + this.productResponse.fields);
        
    }
    
    global csbb.Result canOffer (Map<String, String> attMap, Map<String, String> responseFields, csbb.CalloutProduct.ProductResponse productResponse) {
        csbb.Result canOfferResult = new csbb.Result();
        
        system.debug('responseFields: ' + responseFields);
        system.debug('productResponse: ' + productResponse);
        system.debug('attMap: ' + attMap);
        
        String ignoreRFSCheck;
        if (attMap.containsKey('Ignore RFS Check')) {
            ignoreRFSCheck = attMap.get('Ignore RFS Check');
        }
        else {
            ignoreRFSCheck = 'No';
        }
        
        if (responseFields.containsKey('rfsCheckFailureMessage'))
        {
            if (ignoreRFSCheck == 'Yes')
            {
                canOfferResult.status = 'OK';
            }
            else
            {
                canOfferResult.status = responseFields.get('rfsCheckFailureMessage');    
            }
        }
        else
        {
            /*
* If topspeed is higher than 0 i.e. there is connection, make non-AOP offers available and AOP ones unavailable.
* Opposite applies.
*/
            String topSpeed;
            if (responseFields.containsKey('topspeed')) {
                topSpeed = responseFields.get('topspeed');
            }
            else {
                topSpeed = '0';
            }
            
            if (topSpeed == '0') {
                if (ignoreRFSCheck == 'Yes') {
                    canOfferResult.status = 'OK';
                }
                else {
                    canOfferResult.status = 'There is no connection at this address';
                }
            }
            else {
                if (ignoreRFSCheck == 'Yes') {
                    canOfferResult.status = 'Address is online so product with AOP is not available';
                }
                else {
                    canOfferResult.status = 'OK';
                }
            }
        }
        
        return canOfferResult;
    }
}