/**
 * @description         This is the trigger handler for the Event object
 */
public with sharing class EventTriggerHandler extends TriggerHandler {
	/**
	 * @description			This handles the before insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeInsert() {
		List<Event> newEvents = (List<Event>) this.newList;

		updatePublicCalendar(newEvents);
		updateLeadFields(newEvents);
	}

	/**
	 * @description         This method updates the EventId on the parent record, if appropriate
	 * @author              Guy Clairbois
	 */
	private void updatePublicCalendar(List<Event> newEvents) {
		for (Event e : newEvents) {
			if (e.Public_Calendar__c != null) {
				e.OwnerId = Public_Calendar__c.getInstance(e.Public_Calendar__c).PublicCalendarId__c;
				e.IsVisibleInSelfService = true;
			}
		}
	}

	private void updateLeadFields(List<Event> newEvents) {
		Set<Id> leadIds = new Set<Id>();
		for (Event e : newEvents) {
			if (e.Subject == 'D2D Event' && String.isNotBlank(e.WhoId) && e.WhoId.getSobjectType() == Schema.Lead.SObjectType) {
				leadIds.add(e.WhoId);
			}
		}
		if (!leadIds.isEmpty()) {
			Map<Id, Lead> leadsById = new Map<Id, Lead>([SELECT Id, LG_VisitAddress__c, LG_FollowUpDescription__c FROM Lead WHERE Id IN :leadIds]);
			for (Event e : newEvents) {
				if (e.Subject == 'D2D Event' && leadsById.containsKey(e.WhoId)) {
					e.D2D_Lead_Visit_Address__c = leadsById.get(e.WhoId).LG_VisitAddress__c;
					e.D2D_Lead_Follow_Up_Description__c = leadsById.get(e.WhoId).LG_FollowUpDescription__c;
				}
			}
		}
	}
}
