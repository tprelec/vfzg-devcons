public with sharing class AddPbxController {
	// Fetching all Pbx types
	@AuraEnabled
	public static List<PBX_Type__c> getPbx() {
		List<PBX_Type__c> pbxTypes = [
			SELECT
				Id,
				Vendor__c,
				Product_Name__c,
				Software_version__c,
				Office_Voice_approved__c,
				Name,
				SIP_Certification__c,
				CNoIP_approved__c,
				VF_CPE__c,
				Remarks__c
			FROM PBX_Type__c
		];

		return pbxTypes;
	}
	// Saving selected Pbx for a Site
	@AuraEnabled
	public static void savePbx(Competitor_Asset__c pbx) {
		Competitor_Asset__c newPbx = new Competitor_Asset__c();
		Id recordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByDeveloperName().get('PABX').getRecordTypeId();
		newPbx.Account__c = pbx.Account__c;
		newPbx.Site__c = pbx.Site__c;
		newPbx.PBX_type__c = pbx.PBX_type__c;
		newPbx.Maintenance_company__c = pbx.Maintenance_company__c;
		newPbx.Maintenance_contact__c = pbx.Maintenance_contact__c;
		newPbx.Maintenance_contact_phone__c = pbx.Maintenance_contact_phone__c;
		newPbx.recordTypeId = recordTypeId;
		insert newPbx;

		Site__c site = [SELECT Id, PBX__c FROM Site__c WHERE Id = :pbx.Site__c];
		site.PBX__c = newPbx.Id;
		update site;
	}
}
