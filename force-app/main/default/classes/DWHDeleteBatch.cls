global class DWHDeleteBatch implements Database.Batchable<sObject>, Schedulable{
    static Date today30 = Date.today().addDays(-7);

    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('Select ID from DWH_Account_SF_Interface__c where Processed__c = true AND LastModifiedDate < :today30');
    }

    public void execute(Database.BatchableContext context, List<SObject> records) {
        system.debug('##records: '+records.size());
        delete records;
    }

    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new DWHDeleteBatch());
    }

    global void finish(Database.BatchableContext BC) {
        Database.executeBatch(new DWHDeleteAssetBatch());
    }
}