public with sharing class COM_O2UDecomposition {
	public static void orderDecomposition(Map<Id, csord__Order__c> newOrdersMap, Map<Id, csord__Order__c> oldOrdersMap) {
		Map<Id, csord__Order__c> readyForDeliveryOrderMap = getReadyForDeliveryOrderMap(newOrdersMap.values(), oldOrdersMap);
		COM_OrderGenerationObserverHandler comOrderGenerationObserverHandler = new COM_OrderGenerationObserverHandler();
		COM_MetadataInformation metadataInformation = new COM_MetadataInformation();

		if (!readyForDeliveryOrderMap.isEmpty()) {
			List<Id> serviceIds = getServiceIdList(readyForDeliveryOrderMap.keySet());

			List<csord__Service__c> servicesList = COM_Helper.getServiceList(serviceIds);

			/**
			 * Decomposition of Commercial Components into Delivery Components
			 */
			List<COM_Delivery_Order__c> createdParentDeliveryOrderList = comOrderGenerationObserverHandler.createParentDeliveryOrder(servicesList);
			List<COM_Delivery_Order__c> createdDeliveryOrderList = comOrderGenerationObserverHandler.createDeliveryOrders(
				servicesList,
				createdParentDeliveryOrderList,
				null
			);
			List<csord__Service__c> updatedServices = comOrderGenerationObserverHandler.linkServiceToDeliveryOrder(
				servicesList,
				createdDeliveryOrderList
			);
			comOrderGenerationObserverHandler.linkVFOrderToDeliveryOrder(servicesList, createdParentDeliveryOrderList);

			List<COM_MetadataInformation.DeliveryComponent> deliveryComponents = new List<COM_MetadataInformation.DeliveryComponent>();

			COM_DeliveryComponentsGenerator generator = new COM_DeliveryComponentsGenerator();
			metadataInformation = generator.prepareDataToGenerateComponents(serviceIds);

			COM_MACDController comMACDController = new COM_MACDController();

			try {
				List<Asset> assetsToInsert = new List<Asset>();
				servicesList = COM_Helper.getServiceList(serviceIds);
				COM_MACDResult macdResult = new COM_MACDResult();
				macdResult = comMACDController.executeBusinessLogic(
					servicesList,
					metadataInformation,
					generator.generateDeliveryComponents(metadataInformation),
					createdParentDeliveryOrderList
				);
				deliveryComponents = macdResult.deliveryComponents;
				updatedServices = macdResult.servicesList;

				for (csord__Service__c service : updatedServices) {
					Set<String> deliveryComponentNames = new Set<String>();
					Boolean hasDeliveryComponents = false;

					for (COM_MetadataInformation.DeliveryComponent delCom : deliveryComponents) {
						if (delCom.serviceId == service.Id) {
							if (delCom.installationRequired) {
								service.Installation_Required__c = true;
							}

							if (delCom.provisioningRequired) {
								service.Provisioning_Required__c = true;
							}

							hasDeliveryComponents = true;
							deliveryComponentNames.add(delCom.Name);

							assetsToInsert.addAll(COM_Helper.prepareCOMAssets(service, delCom, metadataInformation.getDeliveryArticleMaterials()));
						}
					}

					service.Has_Delivery_Components__c = hasDeliveryComponents;
					service.Delivery_Components__c = String.join(new List<String>(deliveryComponentNames), COM_Constants.COMMA_SIGN);
				}

				update updatedServices;
				insert assetsToInsert;
			} catch (Exception e) {
				throw new COM_CustomException(COM_Constants.CUSTOM_EXCEPTION_MESSAGE_BATCH_EXECUTION + e.getMessage());
			} finally {
				o2uDecompositionFinallyLogic(readyForDeliveryOrderMap);
			}

			/**
			 * Grouping of the Delivery Components / Creation of the Delivery Orders
			 */
			generator.updateDeliveryOrder(
				metadataInformation.getServiceList(),
				metadataInformation.getDeliveryArticleMaterials(),
				metadataInformation.getDeliveryComponents(),
				deliveryComponents
			);

			/**
			 * Orchestrator Process Creation
			 */
			orchestrationProcessCreation(readyForDeliveryOrderMap.values());
		}
	}

	private static void o2uDecompositionFinallyLogic(Map<Id, csord__Order__c> readyForDeliveryOrderMap) {
		List<COM_Delivery_Order__c> deliveryOrders = [
			SELECT Id, Status__c
			FROM COM_Delivery_Order__c
			WHERE Order__c IN :readyForDeliveryOrderMap.keySet() AND Parent_Delivery_Order__c = NULL
		];

		for (COM_Delivery_Order__c deliveryOrder : deliveryOrders) {
			deliveryOrder.Status__c = 'Decomposed';
		}

		update deliveryOrders;

		List<csord__Order__c> ordersToUpdate = new List<csord__Order__c>();

		for (Id orderId : readyForDeliveryOrderMap.keySet()) {
			ordersToUpdate.add(new csord__Order__c(Id = orderId, csord__status2__c = 'In Delivery'));
		}

		update ordersToUpdate;
	}

	private static void orchestrationProcessCreation(List<csord__Order__c> deliveryOrders) {
		try {
			List<COM_Delivery_Order__c> deliveryOrderRecords = COM_Helper.fetchParentDeliveryOrders(deliveryOrders);
			CS_ComProcessCreator comProcessCreator = new CS_ComProcessCreator(deliveryOrderRecords);
			comProcessCreator.run();
		} catch (Exception e) {
			throw new COM_CustomException(COM_Constants.CUSTOM_EXCEPTION_MESSAGE_ORCHESTRATION_PROCESS_CREATION + e.getMessage());
		}
	}

	private static Map<Id, csord__Order__c> getReadyForDeliveryOrderMap(List<csord__Order__c> newOrderList, Map<Id, csord__Order__c> oldOrdersMap) {
		Map<Id, csord__Order__c> readyForDeliveryOrderMap = new Map<Id, csord__Order__c>();

		for (csord__Order__c newOrderIterate : newOrderList) {
			if (
				newOrderIterate.csord__status2__c == COM_Constants.READY_FOR_DELIVERY_STATUS &&
				oldOrdersMap.get(newOrderIterate.Id).csord__status2__c != COM_Constants.READY_FOR_DELIVERY_STATUS
			) {
				readyForDeliveryOrderMap.put(newOrderIterate.Id, newOrderIterate);
			}
		}

		return readyForDeliveryOrderMap;
	}

	private static List<Id> getServiceIdList(Set<Id> orderIdSet) {
		List<Id> serviceIdList = new List<Id>();
		List<csord__Service__c> serviceRecordList = getServiceList(orderIdSet);

		for (csord__Service__c serviceRecord : serviceRecordList) {
			serviceIdList.add(serviceRecord.Id);
		}

		return serviceIdList;
	}

	private static List<sObject> getServiceList(Set<Id> orderIds) {
		return [SELECT id FROM csord__Service__c WHERE csord__Order__c IN :orderIds];
	}
}
