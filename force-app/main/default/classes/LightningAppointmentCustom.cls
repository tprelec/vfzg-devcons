/**
 * @description       : provides Invocable method to be used by `Inbound Lightning Appointment Custom` flow
 * @last modified on  : 05-16-2022
 **/
public with sharing class LightningAppointmentCustom {
	/**
    * @description takes in a string parameter, and return its decoded version, so it can be used to update the Lead Source field on Lead object.
                   a per the requirements on https://jiranl.vodafoneziggo.com/browse/SFLO-86
    * @param encodedValues
    * @return List<String>
    **/
	@InvocableMethod(label='Get Decoded String' description='Returns the list of decoded values corresponding to the specified input list.')
	public static List<String> getDecodedValues(List<String> encodedValues) {
		List<String> decodedValues = new List<String>();
		if (!encodedValues.isEmpty() && String.isNotBlank(encodedValues.get(0))) {
			decodedValues.add(EncodingUtil.urlDecode(encodedValues.get(0), 'UTF-8'));
		}
		return decodedValues;
	}
}
