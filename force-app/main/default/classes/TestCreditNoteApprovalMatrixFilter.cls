/**
 * @description:    Test class for CreditNoteApprovalMatrixFilter
 **/
@isTest
public class TestCreditNoteApprovalMatrixFilter {
	private static final String TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_REFUND = Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_REFUND;
	private static final String TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL = Constants.CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL;
	private static final String TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE = Constants.CREDIT_NOTE_SUBSCRIPTION_MOBILE;
	private static final String TEST_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE;
	private static final String TEST_MATRIX_QUEUE_DEVELOPER_NAME = 'TestCreditNoteApprovalMatrixFilterQueue';

	private static final String TEST_NOT_CREATED_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_NOTE_INDIRECT = Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT;
	private static final String TEST_NOT_CREATED_MATRIX_CREDIT_NOTE_CREDIT_TYPE_COULANCE = Constants.CREDIT_NOTE_CREDIT_TYPE_COULANCE;
	private static final String TEST_NOT_CREATED_MATRIX_CREDIT_NOTE_SUBSCRIPTION_FIXED = Constants.CREDIT_NOTE_SUBSCRIPTION_FIXED;

	private static final Integer TEST_MATRIX_LEVEL_KEY = 1;

	private static final Integer TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT = 1000;

	private static final Integer NUMBER_OF_MATRICES_TO_CREATE = 5;

	/**
	 * @description:    Tests returning empty map when no parameters
	 *                  or matrices are provided
	 **/
	@isTest
	static void shouldReturnEmptyMapWhenNoParametersOrMatricesAreProvided() {
		// prepare data
		CreditNoteApprovalMatrixFilter creditNoteApprovalMatrixFilter = new CreditNoteApprovalMatrixFilter(new List<CreditNote_Approval_Matrix__c>());

		// perform testing
		Test.startTest();
		Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredApprovalMatricesPerLevel = creditNoteApprovalMatrixFilter.getFilteredApprovalMatricesPerLevel();
		Test.stopTest();

		// verify results
		System.assertEquals(0, filteredApprovalMatricesPerLevel.size(), '0 records should be returned');
	}

	/**
	 * @description:    Tests returning empty map when no parameters
	 *                  are provided
	 **/
	@isTest
	static void shouldReturnEmptyMapWhenNoParametersAreProvided() {
		// prepare data
		CreditNoteApprovalMatrixFilter creditNoteApprovalMatrixFilter = new CreditNoteApprovalMatrixFilter(
			createTestCreditNoteApprovalMatrices(NUMBER_OF_MATRICES_TO_CREATE, null)
		);

		// perform testing
		Test.startTest();
		Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredApprovalMatricesPerLevel = creditNoteApprovalMatrixFilter.getFilteredApprovalMatricesPerLevel();
		Test.stopTest();

		// verify results
		System.assertEquals(0, filteredApprovalMatricesPerLevel.size(), '0 records should be returned');
	}

	/**
	 * @description:    Tests returning empty map when wrong parameters
	 *                  are provided with correct amount
	 **/
	@isTest
	static void shouldReturnEmptyMapWhenWrongParametersAreProvidedWithCorrectAmount() {
		// prepare data
		CreditNoteApprovalMatrixFilter creditNoteApprovalMatrixFilter = new CreditNoteApprovalMatrixFilter(
			createTestCreditNoteApprovalMatrices(NUMBER_OF_MATRICES_TO_CREATE, null)
		);

		// perform testing
		Test.startTest();
		Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredApprovalMatricesPerLevel = creditNoteApprovalMatrixFilter
			.withCreditNoteRecordType(TEST_NOT_CREATED_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_NOTE_INDIRECT)
			.withCreditNoteCreditType(TEST_NOT_CREATED_MATRIX_CREDIT_NOTE_CREDIT_TYPE_COULANCE)
			.withCreditNoteSubscription(TEST_NOT_CREATED_MATRIX_CREDIT_NOTE_SUBSCRIPTION_FIXED)
			.withAmount(TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT + 1)
			.getFilteredApprovalMatricesPerLevel();
		Test.stopTest();

		// verify results
		System.assertEquals(0, filteredApprovalMatricesPerLevel.size(), '0 records should be returned');
	}

	/**
	 * @description:    Tests returning empty map when correct parameters
	 *                  are provided without amount
	 **/
	@isTest
	static void shouldReturnEmptyMapWhenCorrectParametersAreProvidedWithoutAmount() {
		// prepare data
		CreditNoteApprovalMatrixFilter creditNoteApprovalMatrixFilter = new CreditNoteApprovalMatrixFilter(
			createTestCreditNoteApprovalMatrices(NUMBER_OF_MATRICES_TO_CREATE, null)
		);

		// perform testing
		Test.startTest();
		Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredApprovalMatricesPerLevel = creditNoteApprovalMatrixFilter
			.withCreditNoteRecordType(TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_REFUND)
			.withCreditNoteCreditType(TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL)
			.withCreditNoteSubscription(TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE)
			.getFilteredApprovalMatricesPerLevel();
		Test.stopTest();

		// verify results
		System.assertEquals(0, filteredApprovalMatricesPerLevel.size(), '0 records should be returned');
	}

	/**
	 * @description:    Tests returning map with levels when correct parameters
	 *                  are provided with correct amount which is greater than
	 *                  all matrix values
	 **/
	@isTest
	static void shouldReturnMapWithLevelsWhenCorrectParametersAreProvidedWithAmountThatIsGreaterThanAllMatrixValues() {
		// prepare data
		CreditNoteApprovalMatrixFilter creditNoteApprovalMatrixFilter = new CreditNoteApprovalMatrixFilter(
			createTestCreditNoteApprovalMatrices(NUMBER_OF_MATRICES_TO_CREATE, null)
		);

		Decimal amountGreaterThanAllMatrixValues = TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT * NUMBER_OF_MATRICES_TO_CREATE + 1;

		// perform testing
		Test.startTest();
		Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredApprovalMatricesPerLevel = creditNoteApprovalMatrixFilter
			.withCreditNoteRecordType(TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_REFUND)
			.withCreditNoteCreditType(TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL)
			.withCreditNoteSubscription(TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE)
			.withAmount(amountGreaterThanAllMatrixValues)
			.getFilteredApprovalMatricesPerLevel();
		Test.stopTest();

		// verify results
		System.assertEquals(NUMBER_OF_MATRICES_TO_CREATE, filteredApprovalMatricesPerLevel.size(), 'All created matrix records should be returned');
	}

	/**
	 * @description:    Tests returning map with levels when correct parameters
	 *                  are provided with correct amount which is greater than
	 *                  some matrix values
	 **/
	@isTest
	static void shouldReturnMapWithLevelsWhenCorrectParametersAreProvidedWithAmountThatIsGreaterThanSomeMatrixValues() {
		// prepare data
		List<CreditNote_Approval_Matrix__c> creditNoteApprovalMatrices = createTestCreditNoteApprovalMatrices(NUMBER_OF_MATRICES_TO_CREATE, null);

		CreditNoteApprovalMatrixFilter creditNoteApprovalMatrixFilter = new CreditNoteApprovalMatrixFilter(creditNoteApprovalMatrices);

		Decimal amountGreaterThanSomeMatrixValues = TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT * (NUMBER_OF_MATRICES_TO_CREATE - 2) + 1;

		// perform testing
		Test.startTest();
		Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredApprovalMatricesPerLevel = creditNoteApprovalMatrixFilter
			.withCreditNoteRecordType(TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_REFUND)
			.withCreditNoteCreditType(TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL)
			.withCreditNoteSubscription(TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE)
			.withAmount(amountGreaterThanSomeMatrixValues)
			.getFilteredApprovalMatricesPerLevel();
		Test.stopTest();

		// verify results
		Integer expectedNumberOfMatricesToBeReturned = getExpectedNumberOfMatricesForAmount(
			creditNoteApprovalMatrices,
			amountGreaterThanSomeMatrixValues
		);

		System.assertEquals(
			expectedNumberOfMatricesToBeReturned,
			filteredApprovalMatricesPerLevel.size(),
			'Proper number of created matrix records should be returned'
		);
	}

	/**
	 * @description:    Tests returning map with levels with multiple matrices
	 *                  on the same level when they exist
	 **/
	@isTest
	static void shouldReturnMapWithLevelsWithMultipleMatricesOnTheSameLevelWhenTheyExist() {
		// prepare data
		CreditNoteApprovalMatrixFilter creditNoteApprovalMatrixFilter = new CreditNoteApprovalMatrixFilter(
			createTestCreditNoteApprovalMatrices(NUMBER_OF_MATRICES_TO_CREATE, TEST_MATRIX_LEVEL_KEY)
		);

		Decimal amountGreaterThanAllMatrixValues = TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT * NUMBER_OF_MATRICES_TO_CREATE + 1;

		// perform testing
		Test.startTest();
		Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredApprovalMatricesPerLevel = creditNoteApprovalMatrixFilter
			.withCreditNoteRecordType(TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_REFUND)
			.withCreditNoteCreditType(TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL)
			.withCreditNoteSubscription(TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE)
			.withAmount(amountGreaterThanAllMatrixValues)
			.getFilteredApprovalMatricesPerLevel();
		Test.stopTest();

		// verify results
		System.assertEquals(1, filteredApprovalMatricesPerLevel.keySet().size(), 'Only 1 level should be returned');
		System.assertEquals(
			NUMBER_OF_MATRICES_TO_CREATE,
			filteredApprovalMatricesPerLevel.get(TEST_MATRIX_LEVEL_KEY).size(),
			'Key should contain all created matrix records'
		);
	}

	/**
	 * @description:    Creates test credit note approval matrices
	 * @param           numberOfMatricesToCreate - number of matrices to create
	 * @param           level - level of the matrix
	 * @return          List<CreditNote_Approval_Matrix__c> - created credit note approval matrices
	 **/
	private static List<CreditNote_Approval_Matrix__c> createTestCreditNoteApprovalMatrices(Integer numberOfMatricesToCreate, Integer level) {
		List<CreditNote_Approval_Matrix__c> createdCreditNoteApprovalMatrices = new List<CreditNote_Approval_Matrix__c>();

		for (Integer i = 0; i < numberOfMatricesToCreate; i++) {
			createdCreditNoteApprovalMatrices.add(
				new CreditNote_Approval_Matrix__c(
					CreditNoteRecordType__c = TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_REFUND,
					CreditNoteCreditType__c = TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL,
					CreditNoteSubscription__c = TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE,
					AmountFrom__c = i * TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT,
					AmountTo__c = (i + 1) * TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT,
					ApprovalAssignmentRule__c = TEST_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE,
					QueueApprover__c = TEST_MATRIX_QUEUE_DEVELOPER_NAME,
					Level__c = level != null ? level : i + 1
				)
			);
		}

		return createdCreditNoteApprovalMatrices;
	}

	/**
	 * @description:    Gets expected number of matrices for amount
	 * @param           creditNoteApprovalMatrices - credit note approval matrices
	 * @param           amount - amount
	 * @return          Integer - expected number of matrices for amount
	 **/
	private static Integer getExpectedNumberOfMatricesForAmount(
		List<CreditNote_Approval_Matrix__c> creditNoteApprovalMatrices,
		Decimal amountGreaterThanSomeMatrixValues
	) {
		Integer expectedNumberOfMatricesToBeReturned = 0;

		for (CreditNote_Approval_Matrix__c creditNoteApprovalMatrix : creditNoteApprovalMatrices) {
			if (amountGreaterThanSomeMatrixValues > creditNoteApprovalMatrix.AmountFrom__c) {
				expectedNumberOfMatricesToBeReturned++;
			}
		}

		return expectedNumberOfMatricesToBeReturned;
	}
}
