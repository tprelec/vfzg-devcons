global without sharing class COM_InflightChangeAppliedCustomStep implements CSPOFA.ExecutionHandler {
    
    public List<SObject> process(List<sObject> steps) {
        List<SObject> result = new List<sObject>();
        List<CSPOFA__Orchestration_Step__c> stepList = CS_OrchestratorStepUtility.getStepList(steps);   
            
        try {
            updateDeliveryOrders(stepList);

            for (CSPOFA__Orchestration_Step__c step : stepList) {
                result.add(CS_OrchestratorStepUtility.setStepToComplete(step));
            }                
        } catch (Exception ex) {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                result.add(CS_OrchestratorStepUtility.setStepToError(step,ex));
            }
        }
        
        return result;
    }
    
    
    @TestVisible
    private static void updateDeliveryOrders(List<CSPOFA__Orchestration_Step__c> stepList) {
        Map<Id,String> processNameByDeliveryOrderId = new Map<Id,String>();
        for (CSPOFA__Orchestration_Step__c step : stepList) {
            processNameByDeliveryOrderId.put(step.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c, step.CSPOFA__Orchestration_Process__r.Name);
        }
    
        Map<Id,COM_Delivery_Order__c> deliveryOrders = new Map<Id,COM_Delivery_Order__c> ([
            SELECT Id, On_Hold__c
            FROM COM_Delivery_Order__c
            WHERE Id in :processNameByDeliveryOrderId.keySet()]);

    }
}