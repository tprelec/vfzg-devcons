public with sharing class OrderEntryTestDataFactory {
	/**
	 * Create Opportunity with User, Account, Contact, Ban, Site, Products, OLI
	 */
	public static void createOpportunityWithAllRelatedRecords() {
		User owner = new User(Id = UserInfo.getUserId());
		Account acct = TestUtils.theAccount == null ? TestUtils.createAccount(owner) : TestUtils.theAccount;
		Contact c = new Contact(
			AccountId = acct.Id,
			LastName = 'TestLastname',
			FirstName = 'TestFirstname',
			Marketing_Achiever__c = 'Nee',
			Marketing_Status__c = 'Uit dienst',
			Email = 'test@test.com'
		);
		insert c;
		acct.Authorized_to_sign_1st__c = c.Id;
		update acct;

		TestUtils.theBan = TestUtils.theBan == null ? TestUtils.createBan(acct) : TestUtils.theBan;
		Ban__c ban = TestUtils.theBan == null ? TestUtils.createBan(acct) : TestUtils.theBan;
		OrderType__c ot = TestUtils.createOrderType();

		OrderType__c ot2 = new OrderType__c(
			Name = 'Ziggo',
			ExportSystem__c = 'BOP',
			Status__c = 'Assigned to Inside Sales',
			ExternalId__c = 'OD-05-' + 1
		);
		insert ot2;

		TestUtils.theOrderType = ot2;

		Pricebook2 pricebook = new Pricebook2(
			Id = Test.getStandardPricebookId(),
			Name = 'Standard Price Book',
			IsActive = true,
			Description = 'Test Pricebook'
		);
		update pricebook;

		Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Ziggo').getRecordTypeId();
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, pricebook.Id, ban);

		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		opp.RecordTypeId = recordTypeId;
		update opp;
		Site__c site = TestUtils.createSite(acct);
		TestUtils.autoCommit = false;
		Integer nrOfRows = 2;

		List<Product2> products = new List<Product2>();

		Product2 prod = new Product2(
			Name = 'Test',
			Product_Line__c = 'fZiggo Only',
			OrderType__c = ot2.Id,
			ProductCode = 'PD-00061',
			Quantity_type__c = 'Monthly',
			IsActive = true
		);
		products.add(prod);

		Product2 prod1 = new Product2(
			Name = 'Test',
			Product_Line__c = 'fZiggo Only',
			OrderType__c = ot2.Id,
			ProductCode = 'PD-00060',
			CLC__c = 'Ret'
		);
		products.add(prod1);

		Product2 prod2 = new Product2(
			Name = 'Test',
			Product_Line__c = 'fZiggo Only',
			OrderType__c = ot2.Id,
			ProductCode = 'PD-00095',
			CLC__c = 'Ret'
		);
		products.add(prod2);
		insert products;

		List<PricebookEntry> pbEntries = new List<PricebookEntry>();
		for (Product2 p : products) {
			pbEntries.add(TestUtils.createPricebookEntry(pricebook.Id, p));
		}

		insert pbEntries;

		Decimal totalAmount = 0.0;
		List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
		for (Integer i = 0; i < nrOfRows; i++) {
			OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp, pbEntries.get(i));
			oppLineItem.Site_List__c = site.Id;
			oppLineItems.add(oppLineItem);
			totalAmount += oppLineItem.Product_Arpu_Value__c * oppLineItem.Duration__c * oppLineItem.Quantity;
		}

		insert oppLineItems;
		TestUtils.autoCommit = true;
	}

	/**
	 * Create Order Enty Products with validity Period
	 */

	public static List<OE_Product__c> createOrderEntryProducts() {
		List<OE_Product__c> oeProducts = new List<OE_Product__c>();
		oeProducts.add(new OE_Product__c(Name = 'Main', Type__c = 'Main'));
		oeProducts.add(new OE_Product__c(Name = 'Internet', Type__c = 'Internet'));
		oeProducts.add(new OE_Product__c(Name = 'TV', Type__c = 'TV'));
		oeProducts.add(new OE_Product__c(Name = 'Red Pro', Type__c = 'Mobile'));
		insert oeProducts;

		createValidityPeriod(oeProducts, null, null, null);

		return oeProducts;
	}

	/**
	 * Create Order Entry AddOns with validity period
	 */
	public static List<OE_Add_On__c> createOrderEntryAddOns() {
		List<OE_Add_On__c> oeAddons = new List<OE_Add_On__c>();

		oeAddons.add(new OE_Add_On__c(Name = 'Safe Online', Product_Code__c = 'PD-00060', Type__c = 'Internet Security'));
		oeAddons.add(new OE_Add_On__c(Name = 'Extra internetpunt via stopcontact', Product_Code__c = 'PD-00095', Type__c = 'Internet Additional'));
		insert oeAddons;

		createValidityPeriod(null, null, null, oeAddons);

		return oeAddons;
	}

	/**
	 * Create Order Entry Promotions with validity period
	 */
	public static List<OE_Promotion__c> createPromotions() {
		List<OE_Promotion__c> promotions = new List<OE_Promotion__c>();

		OE_Promotion__c promo = new OE_Promotion__c(
			Promotion_Name__c = 'De eerste maand Ziggo Sport Totaal gratis',
			Customer_Type__c = 'New',
			Type__c = 'Automatic'
		);
		promotions.add(promo);

		OE_Promotion__c promo1 = new OE_Promotion__c(
			Promotion_Name__c = 'De eerste maand Ziggo Sport Totaal gratis',
			Type__c = 'Special',
			Active__c = true,
			Customer_Type__c = 'Existing',
			Contract_Term__c = '1',
			Connection_Type__c = 'Off-Net',
			Off_net_Type__c = 'MTC'
		);
		promotions.add(promo1);

		OE_Promotion__c promo2 = new OE_Promotion__c(
			Promotion_Name__c = 'De eerste maand 100% korting op uw Zakelijk Start abonnement',
			Type__c = 'Standard',
			Active__c = true,
			Customer_Type__c = 'Existing',
			Contract_Term__c = '1',
			Connection_Type__c = 'Off-Net',
			Off_net_Type__c = 'MTC'
		);
		promotions.add(promo2);

		insert promotions;

		createValidityPeriod(null, null, promotions, null);

		return promotions;
	}

	/**
	 * Crete order entry product bundle
	 * */
	public static void createOEProductBundle(Id mainProductId, Id internetProductId, Id tvPrductId) {
		OE_Product_Bundle__c oeProductBundle = new OE_Product_Bundle__c(
			Main_Product__c = mainProductId,
			Internet_Product__c = internetProductId,
			TV_Product__c = tvPrductId,
			Bundle_Product_Code__c = 'PD-00061',
			Contract_Term__c = '1',
			Default__c = true
		);

		insert oeProductBundle;

		cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(
			cspmb__One_Off_Charge__c = 50,
			cspmb__Is_Active__c = true,
			cspmb__Effective_Start_Date__c = Date.today(),
			cspmb__Effective_End_Date__c = Date.today().addDays(7),
			cspmb__Recurring_Charge_External_Id__c = 'PD-00061',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master'
		);

		insert priceItem;
	}

	/**
	 * Create order entry mobile product bundle
	 */
	public static void createOEMobileProductBundle(Id mainProductId, Id mobileProductId, String contractTerm) {
		OE_Product_Bundle__c oeProductBundle = new OE_Product_Bundle__c(
			Main_Product__c = mainProductId,
			Mobile_Product__c = mobileProductId,
			Contract_Term__c = contractTerm,
			Bundle_Product_Code__c = '2222',
			Default__c = true
		);
		insert oeProductBundle;
	}

	public static void createOrderEntryProductAddOn(Id productId, Id addOnId) {
		List<OE_Product_Add_On__c> addons = new List<OE_Product_Add_On__c>();

		addons.add(new OE_Product_Add_On__c(Product__c = productId, Add_On__c = addOnId));

		insert addons;

		createValidityPeriod(null, addons, null, null);
	}

	/**
	 * Inser Mock State
	 */
	public static void insertMockJsonData(Boolean insertRelatedRecord) {
		if (insertRelatedRecord) {
			createOpportunityWithAllRelatedRecords();
			createOrderEntryProducts();
			createOrderEntryAddOns();
		}
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Contact con = [SELECT Id FROM Contact LIMIT 1];
		Account acc = [SELECT Id FROM Account LIMIT 1];
		List<OE_Add_On__c> addOns = [SELECT Id FROM OE_Add_On__c];
		List<OE_Product__c> products = [SELECT Id FROM OE_Product__c];
		List<Site__c> sites = [SELECT Id FROM Site__c];

		Site__c site = sites.size() > 0 ? sites[0] : TestUtils.createSite(acc);
		String var =
			'{"opportunityId":"' +
			opp.Id +
			'",' +
			'"primaryContactId":"' +
			con.Id +
			'",' +
			'"accountId":"' +
			acc.Id +
			'",' +
			'"b2cInternetCustomer":false,' +
			'"sites": [' +
			'{' +
			'"id": 1,' +
			' "type": "Installation",' +
			'"siteId": "' +
			site.Id +
			'",' +
			'"siteCheck": {' +
			'"availability": [' +
			'{ "available": true, "name": "dtv-horizon" },' +
			'{ "available": false, "name": "packages" },' +
			'{ "available": false, "name": "mobile_internet" },' +
			'{ "available": true, "name": "fp200" },' +
			'{ "available": true, "name": "internet" },' +
			'{ "available": true, "name": "dtv" },' +
			'{ "available": true, "name": "catv" },' +
			'{ "available": true, "name": "telephony" },' +
			'{ "available": false, "name": "mobile" },' +
			'{ "available": false, "name": "catvfee" },' +
			'{ "available": true, "name": "fp500" },' +
			'{ "available": true, "name": "fp1000" }' +
			'],' +
			'"city": "DELFT",' +
			'"footprint": "ZIGGO",' +
			'"houseNumber": "5",' +
			'"houseNumberExt": "",' +
			'"status": [],' +
			'"street": "NOORDEINDE",' +
			'"zipCode": "2611KE"' +
			'},' +
			'"addressCheckResult": "On-Net",' +
			' "offNetType": ""' +
			'}' +
			'],' +
			'"lastStep":"Product Configuration",' +
			'"notes": "",' +
			'"bundles": [{' +
			'"bundle":{"bundleName":"Zakelijk Internet Start","code":"PD-00061","id":"aFQ5r0000004C9YGAU","name":"Zakelijk Internet Start","quantity":1,"recurring":42,"totalRecurring":42,"type":"Internet"},' +
			' "contractTerm": "3",' +
			'"addons":[{"bundleName":"Safe Online","code":"PD-00060","id":"' +
			addOns[0].Id +
			'","name":"Safe Online","oneOff":0,"parentProduct":"' +
			products[1].Id +
			'","parentType":"Internet","quantity":1,"recurring":0,"totalOneOff":0,"totalRecurring":0,"type":"Internet Security"},' +
			' {"bundleName":"Extra internetpunt via stopcontact","code":"PD-00095","id":"' +
			addOns[1].Id +
			'","name":"Extra internetpunt via stopcontact","oneOff":53.72,"parentProduct":"' +
			products[1].Id +
			'","parentType":"Internet","quantity":1,"recurring":0,"totalOneOff":0,"totalRecurring":0,"type":"Internet Additional"}],' +
			'"products":[{"id":"' +
			products[0].Id +
			'","type":"Main"},{"id":"' +
			products[1].Id +
			'","type":"Internet"},{"id":"' +
			products[2].Id +
			'","type":"TV"}],' +
			'"telephony": {' +
			'"Telephony": {' +
			'"portingEnabled": true,' +
			'"portingNumber": "1212121212"' +
			'}' +
			'},' +
			'"installation": {' +
			'"earliestInstallationDate": "2022-02-18",' +
			'"customerInformed": false' +
			'},' +
			'"operatorSwitch": {' +
			'"currentContractNumber": "",' +
			'"currentProvider": "",' +
			' "potentialFeeAccepted": false,' +
			'"requested": false' +
			'},' +
			'"porting": {' +
			'"contractNumber": "123",' +
			'"currentSimType": "Subscription",' +
			'"mobileNumber": "0611111111",' +
			'"type": "Business"' +
			'},' +
			'"promos":[{"connectionType":"On-net","contractTerms":"1;2;3","customerType":"New","name": "Test Promo", "discounts":[{"duration":1,"id":"aFT5r000000CaRcGAK","level":"Bundle","name":"De eerste maand 100% korting op uw Zakelijk Start - abonnement","product":"' +
			products[1].Id +
			'","type":"Percentage","value":100}]' +
			'}]}],' +
			'"payment": {' +
			'"bankAccountHolder": "V. Kapoor",' +
			' "billingChannel": "Electronic Bill",' +
			' "iban": "",' +
			' "paymentType": "Bank Transfer",' +
			' "postalCode": "2611KE",' +
			' "street": "NOORDEINDE",' +
			' "houseNumber": "20",' +
			' "houseNumberSuffix": "",' +
			' "city": "DELFT"' +
			'}}';

		insert new Attachment(Name = 'OrderEntryData.json', Body = Blob.valueOf(var), ParentId = opp.Id);
	}

	public static void createValidityPeriod(
		List<OE_Product__c> products,
		List<OE_Product_Add_On__c> addons,
		List<OE_Promotion__c> promotions,
		List<OE_Add_On__c> oeAddonsList
	) {
		List<OE_Validity_Period__c> peroiods = new List<OE_Validity_Period__c>();

		if (products != null) {
			for (OE_Product__c product : products) {
				peroiods.add(new OE_Validity_Period__c(From__c = Date.today(), To__c = Date.today(), Order_Entry_Product__c = product.Id));
			}
		}

		if (addons != null) {
			for (OE_Product_Add_On__c addon : addons) {
				peroiods.add(new OE_Validity_Period__c(From__c = Date.today(), To__c = Date.today(), Order_Entry_Add_On__c = addon.Add_On__c));
			}
		}

		if (promotions != null) {
			for (OE_Promotion__c promotion : promotions) {
				peroiods.add(new OE_Validity_Period__c(From__c = Date.today(), To__c = Date.today(), Order_Entry_Promotion__c = promotion.Id));
			}
		}
		if (OEAddonsList != null) {
			for (OE_Add_On__c addon : OEAddonsList) {
				peroiods.add(new OE_Validity_Period__c(From__c = Date.today(), To__c = Date.today(), Order_Entry_Add_On__c = addon.Id));
			}
		}
		insert peroiods;
	}
}
