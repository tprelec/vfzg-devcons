@isTest
public with sharing class TestExportUtils {
    @isTest
    static void testOblicoUpdate() {
        String initialId = '08e2p000' + '01X7JSy';
        Insert new ExportBatchSchedule__c(SetupOwnerId=UserInfo.getOrganizationId(), Scheduled_Id__c = initialId);
        
        Test.startTest();
        ExportUtils.startNextJob('OlbicoUpdate');
        
        ExportBatchSchedule__c bo = ExportBatchSchedule__c.getOrgDefaults();
        System.assertNotEquals(initialId, bo.Scheduled_Id__c, 'Id should be different from the initial one.');
        
        ExportUtils.startNextJob('Users');
        
        ExportBatchSchedule__c bu = ExportBatchSchedule__c.getOrgDefaults();
        System.assertNotEquals(initialId, bu.Scheduled_Id__c, 'Id should be different from the initial one.');
        System.assertNotEquals(bo.Scheduled_Id__c, bu.Scheduled_Id__c, 'Id should be different from the last one.');
        
        ExportUtils.startNextJob('ContactRoles');
        ExportUtils.startNextJob('Contacts');
        ExportUtils.startNextJob('Sites');
        ExportUtils.startNextJob('Bans');
        
        ExportUtils.startNextJob('Accounts');
        ExportBatchSchedule__c ba = ExportBatchSchedule__c.getOrgDefaults();
        System.assertNotEquals(initialId, ba.Scheduled_Id__c, 'Id should be different from the initial one.');
        System.assertNotEquals(bo.Scheduled_Id__c, ba.Scheduled_Id__c, 'Id should be different from any previus one.');
        System.assertNotEquals(bu.Scheduled_Id__c, ba.Scheduled_Id__c, 'Id should be different from any previus one.');
        
        Test.stopTest();
    }
    
    @isTest
    static void testKillSwitch() {
        String initialId = '08e2p000' + '01X7JSy';
        Insert new ExportBatchSchedule__c(SetupOwnerId=UserInfo.getOrganizationId(), Scheduled_Id__c = initialId, Halt_schedule__c=true);
        
        Test.startTest();
        ExportUtils.startNextJob('OlbicoUpdate');
        Test.stopTest();
        
        ExportBatchSchedule__c b = ExportBatchSchedule__c.getOrgDefaults();
        
        System.assertEquals(initialId, b.Scheduled_Id__c, 'Id should be the same. Kill switch prevents update of id.');
    }
}