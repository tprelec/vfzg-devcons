@isTest
private class CS_CustomPostConfiguringControllerTest {

	private static testMethod void test() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        System.runAs (simpleUser) { 
        
        Test.startTest();
        
        No_Triggers__c notriggers = new No_Triggers__c();
        notriggers.SetupOwnerId = simpleUser.Id;
        notriggers.Flag__c = true;
        insert notriggers;
        
        PriceReset__c priceResetSettings = new PriceReset__c();
        priceResetSettings.ConfigurationName__c = 'Child Config';
        priceResetSettings.ErrorMessage__c = 'Error message 1';
        priceResetSettings.ErrorMessage2__c = 'Error message 2';
        priceResetSettings.MaxRecurringPrice__c = 1;
        priceResetSettings.RecurringAttributeName__c = 'Recurring Attribute';
        insert priceResetSettings;
        
        DealTypeSettings__c dealTypeSettings = new DealTypeSettings__c();
        dealTypeSettings.Location_Configuration_Name__c = 'Location Config Name';
        dealTypeSettings.Deal_Type_Attribute_Name__c = 'ConfigName';
        insert dealTypeSettings;
        
        Account tmpAcc = CS_DataTest.createAccount('Test Account');
        insert tmpAcc;
        
        Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
        insert tmpOpp;
        
        cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);  
        tmpProductBasket.Error_message__c = 'super errrrrror message';
        tmpProductBasket.Number_of_SIP__c = 20;
        tmpProductBasket.Fixed_Scenario__c = 'One Fixed Enterprise';
        tmpProductBasket.OneNet_Scenario__c = 'One Net Enterprise';
        insert tmpProductBasket;
        
        cscfga__Product_Definition__c pd = CS_DataTest.createProductDefinition('Access Infrastructure');
        pd.Product_Type__c = 'Fixed';
        insert pd;
        
        cscfga__Product_Configuration__c pc = CS_DataTest.createProductConfiguration(pd.Id, 'Access Infrastructure',tmpProductBasket.Id);
        pc.cscfga__Contract_Term__c = 24;
        pc.cscfga__total_one_off_charge__c = 100;
        pc.cscfga__Total_Price__c = 20;
        pc.cspl__Type__c = 'Mobile Voice Services';
        pc.cscfga__Contract_Term_Period__c = 12;
        pc.cscfga__Configuration_Status__c = 'Valid';
        pc.cscfga__Quantity__c = 1;
        pc.cscfga__total_recurring_charge__c = 22;
        pc.RC_Cost__c = 0;
        pc.NRC_Cost__c = 0;
        pc.cscfga__one_off_charge_product_discount_value__c = 0;
        pc.cscfga__recurring_charge_product_discount_value__c = 0;
        pc.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
        insert pc;
        
        cscfga__Product_Configuration__c childPc = CS_DataTest.createProductConfiguration(pd.Id, 'Child Config',tmpProductBasket.Id);
        childPc.cscfga__Parent_Configuration__c = pc.Id;
        insert childPc;
        
        cscfga__Attribute_Definition__c adRecurringAttribute = new cscfga__Attribute_Definition__c(
            Name = 'Recurring Attribute',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Data_Type__c = 'String'
        );
        insert adRecurringAttribute;
        
        cscfga__Attribute__c recurringAttribute = new cscfga__Attribute__c(
            Name = 'Recurring Attribute',
            cscfga__Attribute_Definition__c = adRecurringAttribute.Id,
            cscfga__Product_Configuration__c = childPc.Id,
            cscfga__is_active__c = true,
            cscfga__Value__c = '50',
            cscfga__Price__c = 50
        );
        insert recurringAttribute;
        
        
        cscfga__Attribute_Definition__c locationPD = new cscfga__Attribute_Definition__c(
            Name = 'IPVPN',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Data_Type__c = 'String'
        );
        insert locationPD;
        
        cscfga__Attribute__c locationAttribute = new cscfga__Attribute__c(
            Name = 'IPVPN',
            cscfga__Attribute_Definition__c = locationPD.Id,
            cscfga__Product_Configuration__c = pc.Id,
            cscfga__is_active__c = true,
            cscfga__Value__c = pc.Id,
            cscfga__Price__c = 50
        );
        insert locationAttribute;
        
        cscfga__Attribute_Definition__c configPD = new cscfga__Attribute_Definition__c(
            Name = 'ConfigName',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Data_Type__c = 'String'
        );
        insert configPD;
        
        cscfga__Attribute__c configAttribute = new cscfga__Attribute__c(
            Name = 'ConfigName',
            cscfga__Attribute_Definition__c = configPD.Id,
            cscfga__Product_Configuration__c = pc.Id,
            cscfga__is_active__c = true,
            cscfga__Value__c = 'Location Config Name',
            cscfga__Price__c = 50
        );
        insert configAttribute;

        cscfga__Attribute_Definition__c secondaryPD = new cscfga__Attribute_Definition__c(
            Name = 'Secondary',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Data_Type__c = 'String'
        );
        insert secondaryPD;
        
        cscfga__Attribute__c secondaryAttribute = new cscfga__Attribute__c(
            Name = 'Secondary',
            cscfga__Attribute_Definition__c = secondaryPD.Id,
            cscfga__Product_Configuration__c = pc.Id,
            cscfga__is_active__c = true,
            cscfga__Value__c = 'No',
            cscfga__Price__c = 50
        );
        insert secondaryAttribute;
        
        ApexPages.StandardController stdController = new ApexPages.StandardController(tmpProductBasket);
        CS_CustomPostConfiguringController customPost = new CS_CustomPostConfiguringController(stdController);
        
        CS_CustomPostConfiguringController.resetPrices(tmpProductBasket.Id);
        CS_CustomPostConfiguringController.containsConfigPerName(tmpProductBasket.Id,'Access Infrastructure', new List<cscfga__Product_Configuration__c>{pc, childPc});
        CS_CustomPostConfiguringController.getSumPerBasket(tmpProductBasket.Id,new List<cscfga__Attribute__c>{recurringAttribute},new List<cscfga__Product_Configuration__c>{pc, childPc});
        
        system.debug('****pc.Id: ' + pc.Id);
        CS_CustomPostConfiguringController.propagateLocationDealType(tmpProductBasket.Id,pc.Id);

        tmpProductBasket.Fixed_Scenario__c = '';
        update tmpProductBasket;

        CS_CustomPostConfiguringController.resetPricesInTriggerConfig(new List<cscfga__Product_Configuration__c> { pc, childPc }, false);
        CS_CustomPostConfiguringController.resetPricesInTriggerConfig(new List<cscfga__Product_Configuration__c> { pc, childPc }, true);

        List<cscfga__Product_Basket__c> baskets = [SELECT Id, Has_Configurations__c FROM cscfga__Product_Basket__c WHERE Id = :tmpProductBasket.Id];
        System.assertEquals(true, baskets[0].Has_Configurations__c);
            
        Test.stopTest();    
        }
	}

    private static testMethod void test2(){
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
            
        insert simpleUser;
        System.runAs (simpleUser) { 
        
        Test.startTest();
        
        No_Triggers__c notriggers = new No_Triggers__c();
        notriggers.SetupOwnerId = simpleUser.Id;
        notriggers.Flag__c = true;
        insert notriggers;
        
        PriceReset__c priceResetSettings = new PriceReset__c();
        priceResetSettings.ConfigurationName__c = 'Child Config';
        priceResetSettings.ErrorMessage__c = 'Error message 1';
        priceResetSettings.ErrorMessage2__c = 'Error message 2';
        priceResetSettings.MaxRecurringPrice__c = 160;
        priceResetSettings.RecurringAttributeName__c = 'Recurring Attribute';
        insert priceResetSettings;
        
        Account tmpAcc = CS_DataTest.createAccount('Test Account');
        insert tmpAcc;
        
        Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
        insert tmpOpp;
        
        cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);  
        tmpProductBasket.Error_message__c = 'super errrrrror message';
        tmpProductBasket.Number_of_SIP__c = 20;
        tmpProductBasket.Fixed_Scenario__c = 'One Fixed Enterprise';
        tmpProductBasket.OneNet_Scenario__c = 'One Net Enterprise';
        insert tmpProductBasket;
        
        cscfga__Product_Definition__c pd = CS_DataTest.createProductDefinition('Access Infrastructure');
        pd.Product_Type__c = 'Fixed';
        insert pd;
        
        cscfga__Product_Definition__c pdIpPin = CS_DataTest.createProductDefinition('Price Item IP Pin');
        pdIpPin.Product_Type__c = 'Fixed';
        insert pdIpPin;
   
        cscfga__Product_Configuration__c pc = CS_DataTest.createProductConfiguration(pd.Id, 'Access Infrastructure',tmpProductBasket.Id);
        pc.cscfga__Contract_Term__c = 24;
        pc.cscfga__total_one_off_charge__c = 100;
        pc.cscfga__Total_Price__c = 20;
        pc.cspl__Type__c = 'Mobile Voice Services';
        pc.cscfga__Contract_Term_Period__c = 12;
        pc.cscfga__Configuration_Status__c = 'Valid';
        pc.cscfga__Quantity__c = 1;
        pc.cscfga__total_recurring_charge__c = 22;
        pc.RC_Cost__c = 0;
        pc.NRC_Cost__c = 0;
        pc.cscfga__one_off_charge_product_discount_value__c = 0;
        pc.cscfga__recurring_charge_product_discount_value__c = 0;
        pc.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
        insert pc;
        
        cscfga__Product_Configuration__c childPc = CS_DataTest.createProductConfiguration(pd.Id, 'Child Config',tmpProductBasket.Id);
        childPc.cscfga__Parent_Configuration__c = pc.Id;
        insert childPc;
        
        cscfga__Attribute_Definition__c adRecurringAttribute = new cscfga__Attribute_Definition__c(
            Name = 'Recurring Attribute',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Data_Type__c = 'String'
        );
        insert adRecurringAttribute;
        
        cscfga__Attribute__c recurringAttribute = new cscfga__Attribute__c(
            Name = 'Recurring Attribute',
            cscfga__Attribute_Definition__c = adRecurringAttribute.Id,
            cscfga__Product_Configuration__c = childPc.Id,
            cscfga__is_active__c = true,
            cscfga__Value__c = '50',
            cscfga__Price__c = 50
        );
        insert recurringAttribute;
        
                cscfga__Attribute_Definition__c adBackupAttribute = new cscfga__Attribute_Definition__c(
            Name = 'Recurring Attribute',
            cscfga__Product_Definition__c = pd.Id,
            cscfga__Data_Type__c = 'String'
        );
        insert adBackupAttribute;
        
        cscfga__Attribute__c backupAttribute = new cscfga__Attribute__c(
            Name = 'Recurring Attribute',
            cscfga__Attribute_Definition__c = adRecurringAttribute.Id,
            cscfga__Product_Configuration__c = childPc.Id,
            cscfga__is_active__c = true,
            cscfga__Value__c = '50',
            cscfga__Price__c = 50,
            Backup_price__c = 1
        );
        insert backupAttribute; 
        
        cscfga__Configuration_Offer__c offer = new cscfga__Configuration_Offer__c();
        offer.Name = 'IP Pin Flatt';
        insert offer;
        
        CS_CustomPostConfiguringController.resetPrices(tmpProductBasket.Id);
        CS_CustomPostConfiguringController.resetAndBackupAttributes(tmpProductBasket.Id,new List<cscfga__Attribute__c>{recurringAttribute,backupAttribute},new List<cscfga__Product_Configuration__c>{pc,childPc});
        //CS_CustomPostConfiguringController.createConfigIPPin(tmpProductBasket.Id);
            
        Test.stopTest();    
        }
    }
    
}