/**
 * @description:    Credit note approval matrix filter class
 * @testClass:      TestCreditNoteApprovalMatrixFilter
 **/
public inherited sharing class CreditNoteApprovalMatrixFilter {
	private List<CreditNote_Approval_Matrix__c> creditNoteApprovalMatrices;

	private String creditNoteRecordType;
	private String creditNoteCreditType;
	private String creditNoteSubscription;

	private Decimal amount;

	/**
	 * @description:    Constructor
	 * @param 			creditNoteApprovalMatrices - List of credit note
	 * 					approval matrices
	 */
	public CreditNoteApprovalMatrixFilter(List<CreditNote_Approval_Matrix__c> creditNoteApprovalMatrices) {
		this.creditNoteApprovalMatrices = creditNoteApprovalMatrices;
	}

	/**
	 * @description:    Sets creditNoteRecordType filter
	 * @param 			creditNoteRecordType - value for creditNoteRecordType
	 * @return			CreditNoteApprovalMatrixFilter - instance of this
	 */
	public CreditNoteApprovalMatrixFilter withCreditNoteRecordType(String creditNoteRecordType) {
		this.creditNoteRecordType = creditNoteRecordType;
		return this;
	}

	/**
	 * @description:    Sets creditNoteCreditType filter
	 * @param 			creditNoteCreditType - value for creditNoteCreditType
	 * @return			CreditNoteApprovalMatrixFilter - instance of this
	 */
	public CreditNoteApprovalMatrixFilter withCreditNoteCreditType(String creditNoteCreditType) {
		this.creditNoteCreditType = creditNoteCreditType;
		return this;
	}

	/**
	 * @description:    Sets creditNoteSubscription filter
	 * @param 			creditNoteSubscription - value for creditNoteSubscription
	 * @return			CreditNoteApprovalMatrixFilter - instance of this
	 */
	public CreditNoteApprovalMatrixFilter withCreditNoteSubscription(String creditNoteSubscription) {
		this.creditNoteSubscription = creditNoteSubscription;
		return this;
	}

	/**
	 * @description:    Sets amount filter
	 * @param 			amount - value for amount
	 * @return			CreditNoteApprovalMatrixFilter - instance of this
	 */
	public CreditNoteApprovalMatrixFilter withAmount(Decimal amount) {
		this.amount = amount != null ? amount : 0;
		return this;
	}

	/**
	 * @description:    Retrieves filtered approval matrices per level
	 * @return			Map<Decimal, List<CreditNote_Approval_Matrix__c>>
	 * 					- filtered approval matrices per level
	 */
	public Map<Decimal, List<CreditNote_Approval_Matrix__c>> getFilteredApprovalMatricesPerLevel() {
		Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredCreditNoteApprovalMatricesPerLevel = new Map<Decimal, List<CreditNote_Approval_Matrix__c>>();

		if (this.creditNoteApprovalMatrices != null) {
			for (CreditNote_Approval_Matrix__c creditNoteApprovalMatrix : this.creditNoteApprovalMatrices) {
				if (
					this.isMatrixCreditNoteRecordTypeMatch(creditNoteApprovalMatrix) &&
					this.isMatrixCreditNoteCreditTypeMatch(creditNoteApprovalMatrix) &&
					this.isMatrixCreditNoteSubscriptionMatch(creditNoteApprovalMatrix) &&
					this.isMatrixAmountMatch(creditNoteApprovalMatrix)
				) {
					if (!filteredCreditNoteApprovalMatricesPerLevel.containsKey(creditNoteApprovalMatrix.Level__c)) {
						filteredCreditNoteApprovalMatricesPerLevel.put(creditNoteApprovalMatrix.Level__c, new List<CreditNote_Approval_Matrix__c>());
					}
					filteredCreditNoteApprovalMatricesPerLevel.get(creditNoteApprovalMatrix.Level__c).add(creditNoteApprovalMatrix);
				}
			}
		}

		return filteredCreditNoteApprovalMatricesPerLevel;
	}

	/**
	 * @description:    Checks if credit note record type matches matrix record
	 * @return			Boolean - true if matches, false otherwise
	 */
	private Boolean isMatrixCreditNoteRecordTypeMatch(CreditNote_Approval_Matrix__c creditNoteApprovalMatrix) {
		return this.creditNoteRecordType != null && creditNoteApprovalMatrix.CreditNoteRecordType__c.contains(this.creditNoteRecordType);
	}

	/**
	 * @description:    Checks if credit note credit type matches matrix record
	 * @return			Boolean - true if matches, false otherwise
	 */
	private Boolean isMatrixCreditNoteCreditTypeMatch(CreditNote_Approval_Matrix__c creditNoteApprovalMatrix) {
		return this.creditNoteCreditType != null && creditNoteApprovalMatrix.CreditNoteCreditType__c.contains(this.creditNoteCreditType);
	}

	/**
	 * @description:    Checks if credit note subscription matches matrix record
	 * @return			Boolean - true if matches, false otherwise
	 */
	private Boolean isMatrixCreditNoteSubscriptionMatch(CreditNote_Approval_Matrix__c creditNoteApprovalMatrix) {
		return this.creditNoteSubscription != null && creditNoteApprovalMatrix.CreditNoteSubscription__c.contains(this.creditNoteSubscription);
	}

	/**
	 * @description:    Checks if credit note amount matches matrix record
	 * @return			Boolean - true if matches, false otherwise
	 */
	private Boolean isMatrixAmountMatch(CreditNote_Approval_Matrix__c creditNoteApprovalMatrix) {
		return this.amount != null && this.amount >= creditNoteApprovalMatrix.AmountFrom__c;
	}
}
