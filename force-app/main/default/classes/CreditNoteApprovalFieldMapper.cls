/**
 * @description:    Credit note approval field mapper class
 * @testClass:      TestCreditNoteApprovalFieldMapper
 **/
public inherited sharing class CreditNoteApprovalFieldMapper {
	private enum APPROVAL_STAGE {
		CURRENT,
		NEXT
	}

	private enum APPROVAL_FIELDS {
		USER1,
		USER2,
		USER3,
		QUEUE1,
		ASSIGNMENT_RULE
	}

	private static final Map<String, Map<String, String>> APPROVAL_FIELD_API_NAME_FOR_APPROVAL_STAGE = new Map<String, Map<String, String>>{
		CreditNoteApprovalFieldMapper.APPROVAL_STAGE.CURRENT.name() => new Map<String, String>{
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.USER1.name() => Credit_Note__c.Current_Approver_1__c.getDescribe().getName(),
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.USER2.name() => Credit_Note__c.Current_Approver_2__c.getDescribe().getName(),
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.USER3.name() => Credit_Note__c.Current_Approver_3__c.getDescribe().getName(),
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.QUEUE1.name() => Credit_Note__c.Current_Queue_Approver__c.getDescribe().getName(),
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.ASSIGNMENT_RULE.name() => Credit_Note__c.Current_Approval_Assignment_Rules__c.getDescribe()
				.getName()
		},
		CreditNoteApprovalFieldMapper.APPROVAL_STAGE.NEXT.name() => new Map<String, String>{
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.USER1.name() => Credit_Note__c.Next_Approver_1__c.getDescribe().getName(),
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.USER2.name() => Credit_Note__c.Next_Approver_2__c.getDescribe().getName(),
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.USER3.name() => Credit_Note__c.Next_Approver_3__c.getDescribe().getName(),
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.QUEUE1.name() => Credit_Note__c.Next_Queue_Approver__c.getDescribe().getName(),
			CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.ASSIGNMENT_RULE.name() => Credit_Note__c.Next_Approval_Assignment_Rules__c.getDescribe()
				.getName()
		}
	};

	private static final Map<Integer, String> USER_APPROVAL_FIELD_FOR_CURRENT_USER_NUMBER = new Map<Integer, String>{
		1 => CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.USER1.name(),
		2 => CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.USER2.name(),
		3 => CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.USER3.name()
	};

	private static final String APPROVAL_MATRIX_USER_APPROVER_FIELD =
		CreditNote_Approval_Matrix__c.UserApprover__c.getDescribe().getRelationshipName() +
		'.' +
		CreditNote_Approvals__c.User__c.getDescribe().getName();
	private static final String APPROVAL_MATRIX_QUEUE_APPROVER_FIELD = CreditNote_Approval_Matrix__c.QueueApprover__c.getDescribe().getName();

	@TestVisible
	private static final String APPROVAL_ASSIGNMENT_RULES_SEPARATOR = ';';
	private static final String APPROVAL_MATRIX_FIELD_FROM_SPLIT_STRING_REGEX = '\\.';

	private Credit_Note__c creditNote;
	private Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredCreditNoteApprovalMatricesPerLevel;
	private Integer numberOfUserApproversForCurrentApprovalStage;

	/**
	 * @description:    Constructor
	 * @param           creditNote - credit note
	 * @param           creditNoteApprovalMatrices - credit note approval matrices
	 **/
	public CreditNoteApprovalFieldMapper(
		Credit_Note__c creditNote,
		Map<Decimal, List<CreditNote_Approval_Matrix__c>> filteredCreditNoteApprovalMatricesPerLevel
	) {
		this.creditNote = creditNote;
		this.filteredCreditNoteApprovalMatricesPerLevel = filteredCreditNoteApprovalMatricesPerLevel;
	}

	/**
	 * @description:    Resets credit note current approval fields
	 * @return          CreditNoteApprovalFieldMapper - instance of this
	 **/
	public CreditNoteApprovalFieldMapper resetCreditNoteCurrentApprovalFields() {
		this.creditNote.Current_Approval_Assignment_Rules__c = null;
		this.creditNote.Current_Approver_1__c = null;
		this.creditNote.Current_Approver_2__c = null;
		this.creditNote.Current_Approver_3__c = null;
		this.creditNote.Current_Approver_Manual__c = null;
		this.creditNote.Current_Queue_Approver__c = null;
		this.creditNote.Current_Level_Unanimous_Approval__c = true;

		this.creditNote.Is_Matrix_Approval_Process__c = false;

		return this;
	}

	/**
	 * @description:    Resets credit note next approval fields
	 * @return          CreditNoteApprovalFieldMapper - instance of this
	 **/
	public CreditNoteApprovalFieldMapper resetCreditNoteNextApprovalFields() {
		this.creditNote.Next_Approval_Assignment_Rules__c = null;
		this.creditNote.Next_Approver_1__c = null;
		this.creditNote.Next_Approver_2__c = null;
		this.creditNote.Next_Approver_3__c = null;
		this.creditNote.Next_Approver_Manual__c = null;
		this.creditNote.Next_Queue_Approver__c = null;
		this.creditNote.Next_Level_Unanimous_Approval__c = true;

		this.creditNote.Approved__c = false;

		return this;
	}

	/**
	 * @description:    Copies next approver to current approver
	 * @return          CreditNoteApprovalFieldMapper - instance of this
	 **/
	public CreditNoteApprovalFieldMapper copyNextApproverToCurrentApprover() {
		this.creditNote.Current_Approval_Assignment_Rules__c = creditNote.Next_Approval_Assignment_Rules__c;
		this.creditNote.Current_Approver_1__c = creditNote.Next_Approver_1__c;
		this.creditNote.Current_Approver_2__c = creditNote.Next_Approver_2__c;
		this.creditNote.Current_Approver_3__c = creditNote.Next_Approver_3__c;
		this.creditNote.Current_Approver_Manual__c = creditNote.Next_Approver_Manual__c;
		this.creditNote.Current_Queue_Approver__c = creditNote.Next_Queue_Approver__c;
		this.creditNote.Current_Level_Unanimous_Approval__c = creditNote.Next_Level_Unanimous_Approval__c;

		this.creditNote.Approval_Level__c = creditNote.Approval_Level__c + 1;

		return this;
	}

	/**
	 * @description:    Maps current approver matrix field values
	 * @return          CreditNoteApprovalFieldMapper - instance of this
	 **/
	public CreditNoteApprovalFieldMapper mapCurrentApproverMatrixValues() {
		List<CreditNote_Approval_Matrix__c> currentApprovalLevelMatrices = this.filteredCreditNoteApprovalMatricesPerLevel.get(
			this.creditNote.Approval_Level__c + 1
		);

		Boolean currentAssignmentHappened = this.mapApprovalFieldsForCurrentApprovalLevelMatrices(
			currentApprovalLevelMatrices,
			CreditNoteApprovalFieldMapper.APPROVAL_STAGE.CURRENT
		);

		if (currentAssignmentHappened) {
			this.creditNote.Approval_Level__c++;
			this.creditNote.Is_Matrix_Approval_Process__c = true;
			this.creditNote.Current_Level_Unanimous_Approval__c = this.eligibleForUnanimousApprovalProcess(currentApprovalLevelMatrices);
		}

		return this;
	}

	/**
	 * @description:    Maps next approver matrix field values
	 * @return          CreditNoteApprovalFieldMapper - instance of this
	 **/
	public CreditNoteApprovalFieldMapper mapNextApproverMatrixValues() {
		List<CreditNote_Approval_Matrix__c> currentApprovalLevelMatrices = this.filteredCreditNoteApprovalMatricesPerLevel.get(
			this.creditNote.Approval_Level__c + 1
		);

		Boolean nextAssignmentHappened = this.mapApprovalFieldsForCurrentApprovalLevelMatrices(
			currentApprovalLevelMatrices,
			CreditNoteApprovalFieldMapper.APPROVAL_STAGE.NEXT
		);

		this.creditNote.Next_Level_Unanimous_Approval__c = this.eligibleForUnanimousApprovalProcess(currentApprovalLevelMatrices);

		if (!nextAssignmentHappened) {
			this.creditNote.Is_Final_Approver__c = true;
		}

		return this;
	}

	/**
	 * @description:    Maps approval fields for current level
	 * 					approval matrices and returns if assignment happened
	 * @param           currentApprovalLevelMatrices - current approval level matrices
	 * @param           approvalStage - approval stage
	 * @return          Boolean - true if assignment happened, false otherwise
	 **/
	private Boolean mapApprovalFieldsForCurrentApprovalLevelMatrices(
		List<CreditNote_Approval_Matrix__c> currentApprovalLevelMatrices,
		CreditNoteApprovalFieldMapper.APPROVAL_STAGE approvalStage
	) {
		Boolean assignmentHappened = false;

		if (currentApprovalLevelMatrices != null && !currentApprovalLevelMatrices.isEmpty()) {
			this.numberOfUserApproversForCurrentApprovalStage = 1;

			for (Integer i = 0; i < currentApprovalLevelMatrices.size(); i++) {
				this.mapApprovalFieldsForApprovalStage(currentApprovalLevelMatrices[i], approvalStage);
			}

			assignmentHappened = true;
		}

		return assignmentHappened;
	}

	/**
	 * @description:    Maps approval fields for approval stage
	 * @param           currentApprovalLevelMatrix - current approval level matrix
	 * @param           approvalStage - approval stage
	 **/
	private void mapApprovalFieldsForApprovalStage(
		CreditNote_Approval_Matrix__c currentApprovalLevelMatrix,
		CreditNoteApprovalFieldMapper.APPROVAL_STAGE approvalStage
	) {
		if (currentApprovalLevelMatrix.ApprovalAssignmentRule__c.equalsIgnoreCase(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER)) {
			this.handleMappingForUserAssignmentRule(currentApprovalLevelMatrix, approvalStage);
		} else if (currentApprovalLevelMatrix.ApprovalAssignmentRule__c.equalsIgnoreCase(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE)) {
			this.handleMappingForQueueAssignmentRule(currentApprovalLevelMatrix, approvalStage);
		} else if (
			currentApprovalLevelMatrix.ApprovalAssignmentRule__c.equalsIgnoreCase(
				Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_CHANNEL_MANAGER_AND_SOLUTION_SPECIALIST
			)
		) {
			this.mapAccountTeamMemberRole(Constants.ACCOUNT_TEAM_MEMBER_ROLE_CHANNEL_MANAGER, approvalStage);

			this.mapAccountTeamMemberRole(Constants.ACCOUNT_TEAM_MEMBER_ROLE_SOLUTION_SPECIALIST, approvalStage);
		} else if (
			currentApprovalLevelMatrix.ApprovalAssignmentRule__c.equalsIgnoreCase(
				Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_OPERATIONAL_MANAGER
			)
		) {
			this.mapAccountTeamMemberRole(Constants.ACCOUNT_TEAM_MEMBER_ROLE_OPERATIONAL_PARTNER_MANAGER, approvalStage);
		} else if (
			currentApprovalLevelMatrix.ApprovalAssignmentRule__c.equalsIgnoreCase(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_OWNER_MANAGER)
		) {
			this.handleMappingForOwnerManagerAssignmentRule(approvalStage);
		} else if (
			currentApprovalLevelMatrix.ApprovalAssignmentRule__c.equalsIgnoreCase(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_SALES_MANAGER)
		) {
			this.handleMappingForSalesManagerAssignmentRule(approvalStage);
		}

		this.populateApprovalAssignmentRulesForApprovalStage(currentApprovalLevelMatrix, approvalStage);
	}

	/**
	 * @description:    Handles mapping for user assignment rule
	 * @param           currentApprovalLevelMatrix - current approval level matrix
	 * @param           approvalStage - approval stage
	 **/
	private void handleMappingForUserAssignmentRule(
		CreditNote_Approval_Matrix__c currentApprovalLevelMatrix,
		CreditNoteApprovalFieldMapper.APPROVAL_STAGE approvalStage
	) {
		this.assignFieldValueToCreditNoteFieldFromApprovalMatrix(
			currentApprovalLevelMatrix,
			APPROVAL_MATRIX_USER_APPROVER_FIELD,
			APPROVAL_FIELD_API_NAME_FOR_APPROVAL_STAGE.get(approvalStage.name())
				.get(USER_APPROVAL_FIELD_FOR_CURRENT_USER_NUMBER.get(this.numberOfUserApproversForCurrentApprovalStage))
		);

		this.numberOfUserApproversForCurrentApprovalStage++;
	}

	/**
	 * @description:    Handles mapping for queue assignment rule
	 * @param           currentApprovalLevelMatrix - current approval level matrix
	 * @param           approvalStage - approval stage
	 **/
	private void handleMappingForQueueAssignmentRule(
		CreditNote_Approval_Matrix__c currentApprovalLevelMatrix,
		CreditNoteApprovalFieldMapper.APPROVAL_STAGE approvalStage
	) {
		this.assignFieldValueToCreditNoteFieldFromApprovalMatrix(
			currentApprovalLevelMatrix,
			APPROVAL_MATRIX_QUEUE_APPROVER_FIELD,
			APPROVAL_FIELD_API_NAME_FOR_APPROVAL_STAGE.get(approvalStage.name()).get(CreditNoteApprovalFieldMapper.APPROVAL_FIELDS.QUEUE1.name())
		);
	}

	/**
	 * @description:    Handles mapping of account team member role to credit
	 * 					note approval field
	 * @param           accountTeamMemberRole - account team member role
	 * @param           approvalStage - approval stage
	 **/
	private void mapAccountTeamMemberRole(String accountTeamMemberRoleName, CreditNoteApprovalFieldMapper.APPROVAL_STAGE approvalStage) {
		Id accountTeamMemberRoleUserId = AccountTeamMemberAccessor.getAccountTeamMemberForAccountIdAndTeamMemberRole(
				this.creditNote.Partner__c,
				accountTeamMemberRoleName
			)
			?.UserId;

		if (accountTeamMemberRoleUserId != null) {
			this.creditNote.put(
				APPROVAL_FIELD_API_NAME_FOR_APPROVAL_STAGE.get(approvalStage.name())
					.get(USER_APPROVAL_FIELD_FOR_CURRENT_USER_NUMBER.get(this.numberOfUserApproversForCurrentApprovalStage)),
				accountTeamMemberRoleUserId
			);

			this.numberOfUserApproversForCurrentApprovalStage++;
		}
	}

	/**
	 * @description:    Handles mapping for owner manager assignment rule
	 * @param           approvalStage - approval stage
	 **/
	private void handleMappingForOwnerManagerAssignmentRule(CreditNoteApprovalFieldMapper.APPROVAL_STAGE approvalStage) {
		Id creditNoteOwnerManagerId = GeneralUtils.userMap.get(this.creditNote.OwnerId)?.ManagerId;

		if (creditNoteOwnerManagerId != null) {
			this.creditNote.put(
				APPROVAL_FIELD_API_NAME_FOR_APPROVAL_STAGE.get(approvalStage.name())
					.get(USER_APPROVAL_FIELD_FOR_CURRENT_USER_NUMBER.get(this.numberOfUserApproversForCurrentApprovalStage)),
				creditNoteOwnerManagerId
			);

			this.numberOfUserApproversForCurrentApprovalStage++;
		}
	}

	/**
	 * @description:    Handles mapping for sales manager assignment rule
	 * @param           approvalStage - approval stage
	 **/
	private void handleMappingForSalesManagerAssignmentRule(CreditNoteApprovalFieldMapper.APPROVAL_STAGE approvalStage) {
		if (this.creditNote.Sales_Manager__c != null) {
			this.creditNote.put(
				APPROVAL_FIELD_API_NAME_FOR_APPROVAL_STAGE.get(approvalStage.name())
					.get(USER_APPROVAL_FIELD_FOR_CURRENT_USER_NUMBER.get(this.numberOfUserApproversForCurrentApprovalStage)),
				this.creditNote.Sales_Manager__c
			);

			this.numberOfUserApproversForCurrentApprovalStage++;
		}
	}

	/**
	 * @description:        Populates current approval assignment rules for approval stage
	 * @param               currentApprovalLevelMatrix - current approval level matrix
	 * @param               approvalStage - approval stage
	 **/
	private void populateApprovalAssignmentRulesForApprovalStage(
		CreditNote_Approval_Matrix__c currentApprovalLevelMatrix,
		CreditNoteApprovalFieldMapper.APPROVAL_STAGE approvalStage
	) {
		if (approvalStage == CreditNoteApprovalFieldMapper.APPROVAL_STAGE.CURRENT) {
			String currentApprovalAssignmentRules = this.creditNote?.Current_Approval_Assignment_Rules__c;

			this.creditNote.Current_Approval_Assignment_Rules__c = String.isNotBlank(currentApprovalAssignmentRules)
				? currentApprovalAssignmentRules + APPROVAL_ASSIGNMENT_RULES_SEPARATOR + currentApprovalLevelMatrix.ApprovalAssignmentRule__c
				: currentApprovalLevelMatrix.ApprovalAssignmentRule__c;
		} else if (approvalStage == CreditNoteApprovalFieldMapper.APPROVAL_STAGE.NEXT) {
			String nextApprovalAssignmentRules = this.creditNote?.Next_Approval_Assignment_Rules__c;

			this.creditNote.Next_Approval_Assignment_Rules__c = String.isNotBlank(nextApprovalAssignmentRules)
				? nextApprovalAssignmentRules + APPROVAL_ASSIGNMENT_RULES_SEPARATOR + currentApprovalLevelMatrix.ApprovalAssignmentRule__c
				: currentApprovalLevelMatrix.ApprovalAssignmentRule__c;
		}
	}

	/**
	 * @description:        Assigns field value to credit note field from approval matrix
	 * @param               creditNoteApprovalMatrix - credit note approval matrix
	 * @param               fieldFrom - field from value should be taken
	 * @param               fieldForAssignment - field for assignment
	 **/
	private void assignFieldValueToCreditNoteFieldFromApprovalMatrix(
		CreditNote_Approval_Matrix__c creditNoteApprovalMatrix,
		String fieldFrom,
		String fieldForAssignment
	) {
		Object fieldValue;
		List<String> fieldFromParts = fieldFrom.split(APPROVAL_MATRIX_FIELD_FROM_SPLIT_STRING_REGEX);

		if (fieldFromParts.size() == 2) {
			fieldValue = creditNoteApprovalMatrix?.getSobject(fieldFromParts[0])?.get(fieldFromParts[1]);
		} else {
			fieldValue = creditNoteApprovalMatrix.get(fieldFrom);
		}

		this.creditNote.put(fieldForAssignment, (String) fieldValue);
	}

	/**
	 * @description:        Checks if matrices are eligible for unanimous approval process
	 * @param               approvalLevelMatrices - current approval level matrices
	 * @return              Boolean - true if if matrices are eligible for unanimous approval process
	 * 						, false otherwise
	 **/
	private Boolean eligibleForUnanimousApprovalProcess(List<CreditNote_Approval_Matrix__c> approvalLevelMatrices) {
		Boolean isUnanimousApproval = false;

		if (approvalLevelMatrices != null && approvalLevelMatrices.size() > 0) {
			for (CreditNote_Approval_Matrix__c approvalLevelMatrix : approvalLevelMatrices) {
				isUnanimousApproval = isUnanimousApproval || approvalLevelMatrix?.Is_Unanimous_Approval__c;

				if (isUnanimousApproval) {
					break;
				}
			}
		}

		return isUnanimousApproval;
	}
}
