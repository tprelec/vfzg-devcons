@isTest
public with sharing class ContractTermUpdatePluginTest {
    @TestSetup
	private static void testSetup()
	{
	    List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            PriceReset__c priceResetSetting = new PriceReset__c();
            
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
            
            insert priceResetSetting;

            Account testAccount = CS_DataTest.createAccount('Test Account 1');
            Account testAccount2 = CS_DataTest.createAccount('Test Account 2');
            List<Account> accList = new List<Account>{testAccount,testAccount2};
            insert accList;
            
            Contact contact1 = new Contact(
                AccountId = testAccount.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'   
            );
            insert contact1;
            
            
            
            testAccount.Authorized_to_sign_1st__c = contact1.Id;
            testAccount.Contract_rule_no_mailing__c = true;
            testAccount.Frame_Work_Agreement__c = 'VFZA-2018-8';
            testAccount.Version_FWA__c = 4;
            testAccount.Framework_agreement_date__c = Date.today();
            update testAccount;
            
            Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Contract opportunity',simpleUser.id);
            Opportunity opp2 = new Opportunity();
            opp2.Name = 'testName';
            opp2.Account = testAccount2;
            opp2.StageName= 'Qualification';
            opp2.CloseDate = Date.Today()+10;
            
            List<Opportunity> oppList = new List<Opportunity>{opp, opp2};
            insert oppList;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Contract Basket');
            basket.Name = 'Contract Basket';
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = testAccount.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 24;
            basket.OneNet_Scenario__c = 'One Net Enterprise';
            basket.Fixed_Scenario__c = 'One Fixed Enterprise';
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 02, 02);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 02, 02);
            basket.Number_of_SIP__c = 12;
            basket.Approved_Date__c = Date.today() - 4;

            insert basket;
            
            cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
            accessInfraDef.Product_Type__c = 'Fixed';

            cscfga__Product_Definition__c oneFixedDef = CS_DataTest.createProductDefinition('One Fixed');
            oneFixedDef.Product_Type__c = 'Fixed';
            
            List<cscfga__Product_Definition__c> prodDefList = new List<cscfga__Product_Definition__c>{accessInfraDef,oneFixedDef};
            insert prodDefList;
            
            Site__c testSite = CS_DataTest.createSite('testSite', testAccount, '1234AA', 'teststreet', 'testTown', 3);
            insert testSite;
            
            Site_Availability__c testSA = new Site_Availability__c();
            testSA.Name = 'testSA';
            testSA.Site__c = testSite.Id;
            testSA.Vendor__c = 'ZIGGO'; 
            testSA.Premium_Vendor__c = 'TELECITY'; 
            testSA.Access_Infrastructure__c ='Coax'; 
            testSA.Bandwith_Down_Entry__c = 1; 
            testSA.Bandwith_Up_Entry__c = 10; 
            testSA.Bandwith_Down_Premium__c = 1; 
            testSA.Bandwith_Up_Premium__c = 10;
            insert testSA;
            
            PBX_Type__c PBXType = new PBX_Type__c();
            PBXType.Name = 'Test PBX Type';
            PBXType.Software_version__c = '3.5';
            PBXType.Vendor__c = 'ZIGGO';
            PBXType.Product_Name__c = 'test';
            insert PBXType;
    
            cscfga__Product_Configuration__c pcAccessInf = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
            pcAccessInf.Site_Availability_Id__c = testSA.Id;
            pcAccessInf.cscfga__Contract_Term__c = 24;
            pcAccessInf.cscfga__total_one_off_charge__c = 100;
            pcAccessInf.cscfga__Total_Price__c = 20;
            pcAccessInf.cspl__Type__c = 'Mobile Voice Services';
            pcAccessInf.cscfga__Contract_Term_Period__c = 12;
            pcAccessInf.cscfga__Configuration_Status__c = 'Valid';
            pcAccessInf.cscfga__Quantity__c = 1;
            pcAccessInf.cscfga__total_recurring_charge__c = 22;
            pcAccessInf.RC_Cost__c = 0;
            pcAccessInf.NRC_Cost__c = 0;
            pcAccessInf.OneNet_Scenario__c = 'One Fixed';
            pcAccessInf.Deal_Type__c = 'Acquisition';
            pcAccessInf.cscfga__one_off_charge_product_discount_value__c = 0;
            pcAccessInf.cscfga__recurring_charge_product_discount_value__c = 0;
            pcAccessInf.Mobile_Scenario__c = 'test';
            // pcAccessInf.cscfga__discounts__c = '{"discounts":[{"type":"absolute","source":"Negotiation","discountCharge":"oneOff","description":"22","amount":208},{"type":"absolute","source":"Negotiation","discountCharge":"recurring","description":"22","amount":23}]}';
            pcAccessInf.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
            pcAccessInf.OneFixed__c = 'test';
            pcAccessInf.Fixed_Scenario__c = 'test';
            pcAccessInf.IPVPN__c = 'a7Z2p0000000000000';
            pcAccessInf.cscfga__Product_Family__c = 'Access Infrastructure';
            pcAccessInf.PBXId__c = PBXType.Id;
    
            cscfga__Product_Configuration__c pcOneFixed = CS_DataTest.createProductConfiguration(oneFixedDef.Id, 'One Fixed',basket.Id);
            pcOneFixed.Deal_Type__c = 'Acquisition';
            pcOneFixed.cscfga__Contract_Term__c = 24;
            pcOneFixed.cscfga__total_one_off_charge__c = 100;
            pcOneFixed.cscfga__Total_Price__c = 20;
            pcOneFixed.Number_of_SIP__c = 20;
            pcOneFixed.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcOneFixed.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcOneFixed.cscfga__Contract_Term_Period__c = 12;
            pcOneFixed.cscfga__Configuration_Status__c = 'Valid';
            pcOneFixed.cscfga__Quantity__c = 1;
            pcOneFixed.cscfga__total_recurring_charge__c = 22;
            pcOneFixed.RC_Cost__c = 0;
            pcOneFixed.NRC_Cost__c = 0;
            pcOneFixed.cscfga__one_off_charge_product_discount_value__c = 0;
            pcOneFixed.cscfga__recurring_charge_product_discount_value__c = 0;
            pcOneFixed.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
            pcOneFixed.Mobile_Scenario__c = 'test';
            
            List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>{pcAccessInf, pcOneFixed};
            insert pcList;
            
            pcAccessInf.OneFixed__c = pcOneFixed.id;
            update pcAccessInf;
        	
            }
    }
    
    @isTest
    static void testInvoke() {
        List<cscfga__Product_Configuration__c> configList = [SELECT Id, Name, cscfga__Contract_Term__c, ManagedInternet__c, OneFixed__c, IPVPN__c FROM cscfga__Product_Configuration__c];
        Id aiId;
        for (cscfga__Product_Configuration__c config : configList) {
            if (config.Name == 'Access Infrastructure') aiId = config.Id;
        }
        ContractTermUpdatePlugin testObject = new ContractTermUpdatePlugin();
        Test.startTest();
        String res = (String)testObject.invoke(aiId);
        Test.stopTest();
        System.assertEquals('', res);
    }
}