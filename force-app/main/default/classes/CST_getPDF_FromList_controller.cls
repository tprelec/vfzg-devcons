public class CST_getPDF_FromList_controller {
	public String currentChatId { get; set; }
	public LiveChatTranscript chatTranscript { get; set; }
	private String fileNameName;
	public CST_getPDF_FromList_controller(ApexPages.StandardController controller) {
		currentChatId = ApexPages.CurrentPage().getparameters().get('id');
		System.debug('currentChatId: ' + currentChatId);

		List<LiveChatTranscript> listChat = [SELECT id, caseId, body, name FROM LiveChatTranscript WHERE id = :currentChatId];
		System.debug('complete chats: ' + [SELECT id, caseId, body, name FROM LiveChatTranscript]);
		System.debug('listChat: ' + listChat);

		if (listChat != null && listChat.size() > 0) {
			chatTranscript = listChat[0];
			fileNameName = 'ChatTranscript_' + chatTranscript.name + '.pdf';
			System.debug('fileNameName: ' + fileNameName);
			Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename=' + fileNameName);
		}
	}
}
