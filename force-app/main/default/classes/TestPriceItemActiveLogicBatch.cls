@isTest
public with sharing class TestPriceItemActiveLogicBatch {
	@isTest
	public static void testAllRecords() {
		OrderType__c orderTypeMobile = CS_DataTest.createOrderType();
		insert orderTypeMobile;

		Product2 product1 = CS_DataTest.createProduct('One Business', orderTypeMobile);
		insert product1;

		Category__c mobileCategory = CS_DataTest.createCategory('Mobile');
		insert mobileCategory;

		TriggerHandler.preventRecursiveTrigger('PriceItemTriggerhandler', null, 0);

		cspmb__Price_Item__c item1 = new cspmb__Price_Item__c(
			Name = 'item1',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Effective_Start_Date__c = Date.today()
		);
		insert item1;
		cspmb__Price_Item__c item2 = new cspmb__Price_Item__c(
			Name = 'item2',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Effective_End_Date__c = Date.today()
		);
		insert item2;
		cspmb__Price_Item__c item3 = new cspmb__Price_Item__c(Name = 'item3', cspmb__Type__c = 'Commercial Product', cspmb__Role__c = 'Master');
		insert item3;

		PriceItemActiveLogicBatch batch = new PriceItemActiveLogicBatch(true);

		Test.startTest();
		Database.executeBatch(batch, 200);
		Test.stopTest();

		List<cspmb__Price_Item__c> toVerify = [SELECT Id, cspmb__Is_Active__c FROM cspmb__Price_Item__c WHERE cspmb__Is_Active__c = TRUE];
		System.assertEquals(1, toVerify.size(), '1 cspmb__Price_Item__c should be set to Active');
	}

	@isTest
	public static void testSelectedRecords() {
		OrderType__c orderTypeMobile = CS_DataTest.createOrderType();
		insert orderTypeMobile;

		Product2 product1 = CS_DataTest.createProduct('One Business', orderTypeMobile);
		insert product1;

		Category__c mobileCategory = CS_DataTest.createCategory('Mobile');
		insert mobileCategory;

		TriggerHandler.preventRecursiveTrigger('PriceItemTriggerhandler', null, 0);

		cspmb__Price_Item__c item1 = new cspmb__Price_Item__c(
			Name = 'item1',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Effective_Start_Date__c = Date.today()
		);
		insert item1;
		cspmb__Price_Item__c item2 = new cspmb__Price_Item__c(
			Name = 'item2',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Effective_Start_Date__c = Date.today().addDays(-7)
		);
		insert item2;

		PriceItemActiveLogicBatch batch = new PriceItemActiveLogicBatch(false);

		Test.startTest();
		Database.executeBatch(batch, 200);
		Test.stopTest();

		List<cspmb__Price_Item__c> toVerify = [SELECT Id, cspmb__Is_Active__c FROM cspmb__Price_Item__c WHERE cspmb__Is_Active__c = TRUE];
		System.assertEquals(1, toVerify.size(), '1 cspmb__Price_Item__c should be set to Active');
	}

	@isTest
	public static void testSchedule() {
		PriceItemActiveLogicBatch.SchedulePriceItemActiveLogicBatch scheduleClass = new PriceItemActiveLogicBatch.SchedulePriceItemActiveLogicBatch();
		String schedule = '0 0 1 * * ? *';
		String jobId;
		Test.startTest();
		jobId = system.schedule('TestSchedule', schedule, scheduleClass);
		Test.stopTest();

		List<CronTrigger> jobs = [SELECT Id FROM CronTrigger WHERE Id = :jobId];
		System.assertEquals(1, jobs.size(), 'The class should be scheduled');
	}
}
