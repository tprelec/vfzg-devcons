@SuppressWarnings('PMD.ExcessivePublicCount')
public without sharing class EMP_AddmobileProductController {
	@AuraEnabled
	public static List<WrapperFWATemp> getFWATempFromAPI(String strBANNumber, Id oppId) {
		Opportunity objOPP = [SELECT Id, OwnerId FROM Opportunity WHERE Id = :oppId];
		List<String> lstParamsErrMessage = new List<String>();
		Dealer_Information__c definitiveDealerInfo = getDealerInfo(objOPP.ownerId);
		String strDealerCode = Test.isRunningTest() ? '822033' : definitiveDealerInfo.Dealer_Code__c;
		lstParamsErrMessage.add(strBANNumber);
		lstParamsErrMessage.add('00' + strDealerCode);
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		request.setEndpoint(
			'callout:Unify_Layer7_DE_Credentials_SIT/v1/BSL/rest/order/frameworkAgreement/' +
			strBANNumber +
			'?restrictDealer_flag=false'
		);
		request.setMethod('GET');
		request.setHeader('DealerCode', '00' + strDealerCode);
		request.setHeader('MessageID', String.valueof(System.now()));
		request.setHeader('Content-Type', 'application/json');
		EMP_GetFWASiteVPNProduct.Response lstData;
		HttpResponse response;
		List<WrapperFWATemp> lstWrapper = new List<WrapperFWATemp>();
		String strMessage = String.format(System.Label.EMP_NoContractsonGetFWAId, lstParamsErrMessage);

		response = http.send(request);
		lstData = EMP_GetFWASiteVPNProduct.parse(response.getBody());

		if (response.getStatusCode() == 200) {
			if (!lstData.data.isEmpty() && lstData.data[0].frameworkAgreementOutput != null) {
				lstWrapper = EMP_AddMobileProductUtils.createWrapperLWC(lstData.data[0].frameworkAgreementOutput.frameworkAgreementData);
				// Filter records based on record existence in Sf and sales channel
				lstWrapper = EMP_AddMobileProductUtils.filterInputFWAData(lstWrapper, definitiveDealerInfo);
				lstWrapper.sort();
			} else {
				AuraHandledException e = new AuraHandledException(strMessage);
				e.setMessage(strMessage);
				throw e;
			}
		} else {
			strMessage = lstData?.error?.get(0)?.code.equalsIgnoreCase('BSL-14216') ? strMessage : System.Label.EMP_ErrorAddMobileController;
			AuraHandledException e = new AuraHandledException(strMessage);
			e.setMessage(strMessage);
			throw e;
		}
		return lstWrapper;
	}

	private static Dealer_Information__c getDealerInfo(String ownerId) {
		List<Dealer_Information__c> dealerInfos = (List<Dealer_Information__c>) Database.query(buildQueryDynamic(ownerId));
		if (Test.isRunningTest()) {
			dealerInfos = [SELECT Id, ContactUserId__c, Sales_Channel__c, Dealer_Code__c FROM Dealer_Information__c LIMIT 1];
		}
		return dealerInfos[0];
	}

	private static String buildQueryDynamic(String ownerId) {
		User u = GeneralUtils.userMap.get(ownerId);
		String dealerInfoQuery = 'SELECT Id, Sales_Channel__c, ContactUserId__c, Dealer_Code__c ';
		dealerInfoQuery += 'FROM Dealer_Information__c WHERE Status__c = \'Active\' ';
		if (GeneralUtils.isPartnerUser(u) && u.Account?.Dealer_Code__c != null) {
			dealerInfoQuery += 'AND Dealer_Code__c = \'' + String.escapeSingleQuotes(u.Account.Dealer_Code__c) + '\'';
		} else {
			dealerInfoQuery += 'AND ContactUserId__c = \'' + String.escapeSingleQuotes(u.Id) + '\'';
		}
		return dealerInfoQuery;
	}

	@AuraEnabled
	public static String createOLI(String strJSONToDeserialize, Id oppId) {
		List<oliByFwaTemp> lstVFA = (List<oliByFwaTemp>) JSON.deserialize(strJSONToDeserialize, List<oliByFwaTemp>.class);

		List<OpportunityLineItem> oliToInsert = new List<OpportunityLineItem>();

		Map<String, PriceBookEntry> mapConTypePBE = getMapPricebookEntry();
		Integer modelNumber = 0;
		for (oliByFwaTemp tmpVFA : lstVFA) {
			//create Opportunity Line Item
			modelNumber++;
			OpportunityLineItem oli = new OpportunityLineItem();
			oli.OpportunityId = oppId;
			oli.PricebookEntryId = tmpVFA.connectionType.equalsIgnoreCase('Voice') ? mapConTypePBE.get('Voice').Id : mapConTypePBE.get('Data').Id;
			oli.Quantity = Integer.valueof(tmpVFA.quantity);
			oli.Framework_Agreement_Id__c = tmpVFA.fwaId;
			oli.Template_Id__c = tmpVFA.templateId;
			oli.Model_Number__c = modelNumber;
			oli.Resource_Category__c = tmpVFA.resourceCat;
			oli.Description = tmpVFA.baseplan;
			oli.CLC__c = tmpVFA.type == 'New' || tmpVFA.type == 'Portin' ? 'Acq' : 'Ret';
			oli.Deal_Type__c = tmpVFA.type == 'Portin' ? 'Porting' : tmpVFA.type;
			oli.Product_Arpu_Value__c = 0;
			oli.TotalPrice = 0;
			oli.Gross_List_Price__c = 0;
			oli.DiscountNew__c = 0;
			oli.Duration__c = 1;
			oli.recalculateFormulas();
			oliToInsert.add(oli);
		}
		Savepoint sp = Database.setSavepoint();
		try {
			delete [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :oppId];
			insert oliToInsert;
			createNPIandCTNs(oppId);
		} catch (Exception e) {
			Database.rollback(sp);
			throw new AuraHandledException(e.getMessage() + ' - ' + e.getCause() + ' - ' + e.getStackTraceString());
		}
		return null;
	}

	@AuraEnabled
	public static List<OpportunityLineItem> getOpportunityLineItems(Id opportunityId) {
		return [
			SELECT Template_Id__c, Framework_Agreement_Id__c, Deal_Type__c, Quantity
			FROM OpportunityLineItem
			WHERE OpportunityId = :opportunityId
		];
	}

	/**
	 * @description Creates NetProfit Information and CTNs for given Opportunity.
	 * @param  oppId Opportunity ID.
	 */
	@TestVisible
	private static void createNPIandCTNs(Id oppId) {
		Opportunity opp = getOpportunity(oppId);
		// Validate that NPI and CTNs are created when Mobile and Deepsell (Add Mobile Products)
		if (opp.Type_of_service__c.equalsIgnoreCase('Mobile') && opp.Select_Journey__c.equalsIgnoreCase('Add Mobile Product')) {
			deleteExistingNetprofitData(oppId);
			NetProfit_Information__c npi = new NetProfit_Information__c(Opportunity__c = oppId, BAN__c = opp.BAN__c, Customer__c = opp.AccountId);
			User owner = GeneralUtils.userMap.get(opp.OwnerId);
			if (GeneralUtils.isPartnerUser(owner)) {
				npi.Partner_Name__c = opp.Owner.Contact.Account.Name;
				npi.Paid_Dealer_Code__c = (new DealerService())
					.getDealersByUserIds(new Set<Id>{ owner.Id })
					?.get(owner.Id)
					?.dealerInfo.Dealer_Code__c;
			}
			insert npi;
			NetProfitCTNGenerator generator = new NetProfitCTNGenerator(oppId);
			generator.npInfoId = npi.Id;
			List<NetProfit_CTN__c> ctns = generator.generateCTNInfo();
			insert ctns;
		}
	}

	/**
	 * @description Gets Opportunity.
	 * @param  oppId Opportunity ID.
	 * @return       Opportunity.
	 */
	private static Opportunity getOpportunity(Id oppId) {
		return [
			SELECT Id, Segment__c, Type_of_service__c, Select_Journey__c, BAN__c, AccountId, OwnerId, Owner.Contact.Account.Name
			FROM Opportunity
			WHERE Id = :oppId
		];
	}

	/**
	 * @description Deletes existing NetProfit Data from Opportunity.
	 *				Only NetProfit Information is deleted. CTNs are deleted in cascade.
	 * @param  oppId Opportunity ID.
	 */
	private static void deleteExistingNetprofitData(Id oppId) {
		List<NetProfit_Information__c> npis = [SELECT Id FROM NetProfit_Information__c WHERE Opportunity__c = :oppId];
		if (!npis.isEmpty()) {
			delete npis;
		}
	}

	private static Map<String, PriceBookEntry> getMapPricebookEntry() {
		Map<String, PriceBookEntry> returnMap = new Map<String, PriceBookEntry>();
		returnMap.put('Data', null);
		returnMap.put('Voice', null);
		//get the VF product
		Set<Id> setIdVFProd = new Set<Id>();

		for (VF_Product__c objVF : [
			SELECT Id, ProductCode__c
			FROM VF_Product__c
			WHERE ProductCode__c = 'DUMMYADDMOBILEPRODUCT_VOICE' OR ProductCode__c = 'DUMMYADDMOBILEPRODUCT_DATA'
		]) {
			setIdVFProd.add(objVF.Id);
		}
		//get the price book enty
		for (PricebookEntry objPBE : [
			SELECT Id, Product2.VF_Product__c, Product2.Family
			FROM PriceBookEntry
			WHERE Product2.VF_Product__c IN :setIdVFProd
		]) {
			if (objPBE.Product2.Family.equalsIgnoreCase('Voice')) {
				returnMap.put('Voice', objPBE);
			} else if (objPBE.Product2.Family.equalsIgnoreCase('MBB')) {
				returnMap.put('Data', objPBE);
			}
		}
		return returnMap;
	}

	public class WrapperFWATemp implements Comparable {
		@AuraEnabled
		public String fwaId;
		@AuraEnabled
		public String templateId;
		@AuraEnabled
		public String contractNumber;
		@AuraEnabled
		public String templateDescription;
		@AuraEnabled
		public String basePlanName;
		@AuraEnabled
		public String keyId;
		@AuraEnabled
		public Integer quantityPortin;
		@AuraEnabled
		public Integer quantityAcquisition;
		@AuraEnabled
		public Integer quantityRetention;
		@AuraEnabled
		public Date contractSignedDate;
		@AuraEnabled
		public String connectionType;
		@AuraEnabled
		public String resourceCategory;
		@AuraEnabled
		public String creatorDealerCode;
		@AuraEnabled
		public Boolean isRowDisabled;
		@AuraEnabled
		public String cssRowClass;

		public Integer compareTo(Object objToCompare) {
			WrapperFWATemp that = (WrapperFWATemp) objToCompare;
			if (this.contractSignedDate > that.contractSignedDate) {
				return -1;
			}
			if (this.contractSignedDate < that.contractSignedDate) {
				return 1;
			}
			return 0;
		}

		public void rowShouldBeDisabled(Date dtSigned) {
			this.isRowDisabled = dtSigned <= date.today() ? true : false;
			this.cssRowClass = dtSigned <= date.today() ? '' : 'row-disabled-css';
		}
	}

	public class OliByFwaTemp {
		public String fwaId;
		public String templateId;
		public String quantity;
		public String type;
		public String baseplan;
		public String connectionType;
		public String resourceCat;
	}
}
