/*******************************************************************************************************************************************
* File Name     :  LG_PremiseTriggerHandler
* Description   :  This is a utility class for trigger on Premise object

* @author       :   Shreyas
* Modification Log
===================================================================================================
* Ver.    Date          Author              Modification
---------------------------------------------------------------------------------------------------
* 1.0     15th-Jun-16    Shreyas             Created the class for release R1

********************************************************************************************************************************************/
public class LG_PremiseTriggerHandler {
	/*
		Name: beforeInsertHandler
		Purpose: handles all the logic that should execute on before insert
		Argument: list<cscrm__Address__c>
		Return type: null
	*/
	public static void beforeInsertHandler(list<cscrm__Address__c> premiseList) {
		populatePremiseName(premiseList);
		populatePremiseUniqueKey(premiseList);
	}

	/*
		Name: beforeUpdateHandler
		Purpose: handles all the logic that should execute on before update
		Argument: list<cscrm__Address__c>
		Return type: null
	*/
	public static void beforeUpdateHandler(list<cscrm__Address__c> premiseList) {
		populatePremiseName(premiseList);
	}

	/*
		Name: beforeUpdateHandler
		Purpose: handles all the logic that should execute on before update
		Argument: list<cscrm__Address__c>
		Return type: null
	*/
	public static void afterInsertHandler(list<cscrm__Address__c> premiseList) {
		List<string> premiseIdList = new List<string>();
		for (cscrm__Address__c address : premiseList) {
			premiseIdList.add(address.Id);
		}

		system.debug('### premiseIdList: ' + premiseIdList);

		// future method. Will not be called when invoked from batch apex.
		if (premiseIdList.size() > 0 && !System.isBatch() && !System.isFuture()) {
			validateAddresses(premiseIdList);
		}
	}

	/*
		Name: populatePremiseName
		Purpose: Auto-populate the name of the premise record with "Postal Code + House Number + House Number extension"
		Argument: list<cscrm__Address__c>
		Return type: null
	*/
	public static void populatePremiseName(list<cscrm__Address__c> premiseList) {
		// cscrm__Street__c LG_HouseNumber__c, cscrm__Zip_Postal_Code__c cscrm__City__c

		for (cscrm__Address__c address : premiseList) {
			string premiseName = '';

			if (address.cscrm__Street__c != null) {
				premiseName = address.cscrm__Street__c;
			}

			if (address.LG_HouseNumber__c != null) {
				premiseName = premiseName + ' ' + address.LG_HouseNumber__c;
			}

			if (address.cscrm__Zip_Postal_Code__c != null) {
				premiseName = premiseName + ', ' + address.cscrm__Zip_Postal_Code__c;
			}

			if (address.cscrm__City__c != null) {
				premiseName = premiseName + ' ' + address.cscrm__City__c;
			}

			/*
			if (address.cscrm__Zip_Postal_Code__c !=  null) {
				premiseName = address.cscrm__Zip_Postal_Code__c;
			}
			if (address.LG_HouseNumber__c !=  null) {
				premiseName = premiseName  + '-' + address.LG_HouseNumber__c;
			}
			if (address.LG_HouseNumberExtension__c !=  null) {
				premiseName = premiseName  + '-' + address.LG_HouseNumberExtension__c;
			}
			*/

			address.Name = premiseName;
		}
	}

	public static void populatePremiseUniqueKey(list<cscrm__Address__c> premiseList) {
		for (cscrm__Address__c address : premiseList) {
			string uniqueKey = LG_Util.checkIdNull(address.cscrm__Account__c);
			uniqueKey += LG_Util.checkNull(address.cscrm__Zip_Postal_Code__c);
			uniqueKey += LG_Util.checkNull(address.LG_HouseNumber__c);
			uniqueKey += LG_Util.checkNull(address.LG_HouseNumberExtension__c);
			address.LG_Uniquekey__c = uniqueKey;
		}
	}
	/*
		Name: validateAddresses
		Purpose: to hit the peal service and get the address Id for the address.
		Argument: list<String>
		Return type: null
	*/
	@Future(callout=true)
	public static void validateAddresses(list<string> premiseIdList) {
		List<cscrm__Address__c> premiseListFetched = new List<cscrm__Address__c>();
		List<cscrm__Address__c> premiseListToUpdate = new List<cscrm__Address__c>();

		premiseListFetched = [
			SELECT Id, LG_HouseNumber__c, cscrm__Zip_Postal_Code__c, LG_AddressID__c
			FROM cscrm__Address__c
			WHERE Id IN :premiseIdList AND LG_AddressID__c = NULL
		];

		LG_EndpointResolver resolver = LG_EndpointResolver.getInstance();

		string errorMessage = '';
		for (cscrm__Address__c address : premiseListFetched) {
			// If test is running this is used as a mock address
			String requestURL = 'Test';

			// If test is not running retrieve real address
			if (!Test.isRunningTest()) {
				requestURL = resolver.getAddressCheckEndpoint(
					address.LG_HouseNumber__c,
					null,
					address.cscrm__Zip_Postal_Code__c
				);
			}

			HttpRequest req = new HttpRequest();
			HttpResponse res = new HttpResponse();
			Http httpObj = new Http();

			req.setMethod('GET');
			req.setEndpoint(requestURL);
			req.setTimeout(30000);

			system.debug('###: requestURL: ' + requestURL);
			system.debug('###: premise Id: ' + address.Id);

			// Mock - Set the response to a hard-coded JSON string in case of Developer sandboxes because of absense of connectivity of SFDC and PEAL.
			if (Test.isRunningTest() || LG_Util.getSandboxInstanceName().equals('dev')) {
				res.setStatusCode(200);
				res.setStatus('OK');

				// Mimick the response - if houseFlatExt included, mimick response of only one address, else return two addresses
				res.setBody(LG_AddressResponse.buildAddressResponse(true));
			} else {
				try {
					res = httpObj.send(req);
				} catch (System.CalloutException e) {
					system.debug('###: Error Message: ' + e.getMessage());
				} catch (Exception e) {
					system.debug('###: Error Message: ' + e.getMessage());
				}
			}

			if (res.getStatusCode() == 200 && res.getStatus() == 'OK') {
				LG_AddressResponse addressResponse = new LG_AddressResponse();
				addressResponse.addView = new List<LG_AddressResponse.addView>();

				try {
					addressResponse.addView = (List<LG_AddressResponse.addView>) JSON.deserialize(
						res.getBody(),
						List<LG_AddressResponse.addView>.class
					);
					string addressId = '';
					if (addressResponse.addView != null && addressResponse.addView.size() == 1) {
						addressId = addressResponse.addView[0].addressId;
					} else if (addressResponse.addView.size() > 1) {
						system.debug('### More than one addresses found');
					} else {
						system.debug(Label.LG_UnknownApexError);
					}
					system.debug('### addressId: ' + addressId);
					address.LG_AddressID__c = addressId;
					system.debug('###: address.LG_AddressID__c: ' + address.LG_AddressID__c);
					premiseListToUpdate.add(address);
				} catch (System.JSONException ex) {
					system.debug(Label.LG_UnknownApexError);
				}
			} else if (res.getStatusCode() == 404) {
				system.debug(
					'###: Status Code - 404: ' + Label.LG_NoServiceAvailabilityInformation
				);
			} else if (res.getStatusCode() == 2000) {
				system.debug('###: Status Code - 2000: ' + Label.LG_CalloutTimedOut);
			} else {
				system.debug('###: Error Message: ' + Label.LG_UnknownApexError);
			}
		}

		system.debug('### premiseListToUpdate: ' + premiseListToUpdate);
		if (premiseListToUpdate.size() > 0) {
			update premiseListToUpdate;
		}
	}

	// Below logic is managed by CS team at some other place, commenting out this code - Omkar

	/*public static void populateAddress(list<cscrm__Address__c> addressList, map<id,cscrm__Address__c> addressOldMap)
	{
		if(!addressList.isEmpty())
		{
			set<string> postalCodeSet = new set<string>();
			set<Id> addressSet = new set<Id>();
			for(cscrm__Address__c objAddress : addressList)
			{
				if(trigger.isInsert && objAddress.cscrm__Zip_Postal_Code__c != NULL)
				{
					postalCodeSet.add(objAddress.cscrm__Zip_Postal_Code__c.replaceAll(' ',''));
					addressSet.add(objAddress.Id);
				}
				else if(trigger.isUpdate && objAddress.cscrm__Zip_Postal_Code__c != NULL)
				{
					cscrm__Address__c objAddressOld = addressOldMap.get(objAddress.Id);
					if(objAddress.cscrm__Zip_Postal_Code__c != objAddressOld.cscrm__Zip_Postal_Code__c || objAddress.LG_HouseNumber__c != objAddressOld.LG_HouseNumber__c
						|| objAddress.cscrm__Street__c != objAddressOld.cscrm__Street__c || objAddress.cscrm__City__c != objAddressOld.cscrm__City__c
						|| objAddress.cscrm__Country__c != objAddressOld.cscrm__Country__c)
					{
						postalCodeSet.add(objAddress.cscrm__Zip_Postal_Code__c.replaceAll(' ',''));
						addressSet.add(objAddress.Id);
					}
				}
			}

			if(!addressSet.isEmpty() && !postalCodeSet.isEmpty())
			{
				map<string,map<integer,LG_PostalCode__c>> postalCodeHouseNoAddressMap = LG_Util.fetchPostalCodeAddresses(postalCodeSet);

				if(!postalCodeHouseNoAddressMap.isEmpty())
				{
					for(cscrm__Address__c objAddress : addressList)
					{
						if(addressSet.contains(objAddress.Id) && objAddress.cscrm__Zip_Postal_Code__c != NULL && postalCodeHouseNoAddressMap.containsKey(objAddress.cscrm__Zip_Postal_Code__c.replaceAll(' ','')))
						{
							if(objAddress.LG_HouseNumber__c != NULL)
							{
								if(postalCodeHouseNoAddressMap.get(objAddress.cscrm__Zip_Postal_Code__c.replaceAll(' ','')).containsKey(integer.valueof(objAddress.LG_HouseNumber__c)))
								{
									LG_PostalCode__c address = postalCodeHouseNoAddressMap.get(objAddress.cscrm__Zip_Postal_Code__c.replaceAll(' ','')).get(integer.valueof(objAddress.LG_HouseNumber__c));
									objAddress.cscrm__Street__c = address.LG_StreetName__c;
									objAddress.cscrm__City__c = address.LG_City__c;
									objAddress.cscrm__Country__c = address.LG_Country__c;
								}
								else
									objAddress.LG_HouseNumber__c.addError(Label.LG_Address_Entered_House_Number_Is_Not_Valid);
							}
							else
								objAddress.LG_HouseNumber__c.addError(Label.LG_Address_Enter_House_Number);
						}
					}
				}
			}
		}
	}*/
}