@isTest
public class LG_Exception_Test {

    static final String EXCEPTION_MESSAGE = 'Test Exception';

    @isTest static void testEmptyException() {

        Test.startTest();

        try {
            throw new LG_Exception(EXCEPTION_MESSAGE);
        } catch (LG_Exception e) {
            System.assertEquals(EXCEPTION_MESSAGE, e.getMessage());
        }

        Test.stopTest();
    }
    @isTest static void testCoverOverriddenConstructor() {

        Test.startTest();

        try {
            // call overridden method
            throw new LG_Exception('', EXCEPTION_MESSAGE);
        } catch (LG_Exception e) {
            System.assertEquals(EXCEPTION_MESSAGE, e.getMessage());
        }

        Test.stopTest();
    }

}