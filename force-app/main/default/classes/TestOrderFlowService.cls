@IsTest
public class TestOrderFlowService {
	@TestSetup
	static void makeData() {
		insert new Framework__c(Framework_Sequence_Number__c = 99997, VodafoneZiggo_Framework_Sequence_Number__c = 99998);

		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.KVK_number__c = '12345678';
		acc.Visiting_City__c = 'Utrecht';
		acc.Visiting_Housenumber1__c = 69;
		acc.Visiting_Housenumber_Suffix__c = 'A';
		acc.Visiting_Postal_Code__c = '1234AB';
		acc.Visiting_street__c = 'Balistraat';

		Contact con = TestUtils.createContact(acc);
		con.Email = 'mostUniqueEmailInTheWorld@vodafoneziggo.com';
		update con;

		acc.Authorized_to_sign_1st__c = con.Id;
		update acc;

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
			Name = 'New Basket',
			Primary__c = false,
			OwnerId = UserInfo.getUserId(),
			Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]',
			ProfitLoss_JSON__c = '{"KPI Direct Capex / Net Revenue":"0,00","KPI Payback Period(Months)":"0,00","KPI NPV":"0,00",' +
				'"KPI Net Incremental Billed Revenue":"0,00","KPI Free cash flow / Net revenue":"1,50","KPI EBITDA / Net revenue":"1,56",' +
				'"KPI Sales Margin / Net revenue":"1,71","NET RESULT":"56.993,07","NET RESULT first":"58.996,33","Capex":"1.228,43","Network Depreciation":"774,83",' +
				'"Direct Capex":"0,00","EBITDA":"58.996,33","EBITDA first":"62.203,26","Overhead allocation":"3.206,93","Network Opex":"2.819,96",' +
				'"Incremental EBITDA":"65.023,22","Incremental OPEX":"0,00","Total Gross Margin":"65.023,22","Total costs of sales":"10.816,78","A&R":"0,00",' +
				'"Revenue share":"0,00","Other Costs":"0,00","Opportunity cost":"0,00","Opportunity cost Fixed":"0,00","Opportunity cost Mobile":"0,00",' +
				'"Total cost of sales":"10.816,78","Total Net Revenue":"75.840,00","Total discounts":"0,00","Benchmark":"0,00",' +
				'"Total credit amount / loyalty bonus":"0,00","Benchmark Fixed":"0,00","Benchmark Mobile":"0,00",' +
				'"Operator other costs / other migration costs":"0,00","Operator other costs / other migration costs Fixed":"0,00",' +
				'"Operator other costs / other migration costs Mobile":"0,00","Discounts":"0,00","Total Gross Revenue (SUM)":"75.840,00",' +
				'"Total Gross Revenue for Operator other costs / other migration costs":"0,00","Total Gross Revenue":"75.840,00"}'
		);
		insert basket;
	}

	@isTest
	static void testParseDataWithMavenDocumentsBusinessMobile() {
		MavenDocumentsTestFactory.createSettings();
		mmdoc__Document_Solution__c mdSolution = MavenDocumentsTestFactory.createSolution('Agreements');
		insert mdSolution;
		mmdoc__Document_Template__c mdTemplate = MavenDocumentsTestFactory.createTemplate(mdSolution.Id, 'Business Mobile', 'Google Doc');
		insert mdTemplate;

		cscfga__Product_Basket__c basket = [SELECT Id, csbb__Account__r.Create_VZ_Framework_Agreement__c FROM cscfga__Product_Basket__c LIMIT 1];

		cscfga__Product_Definition__c def = CS_DataTest.createProductDefinition('Business Mobile');
		def.Product_Type__c = 'Mobile';
		insert def;

		cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
		config.cscfga__Product_Definition__c = def.Id;
		insert config;

		Account acc = [
			SELECT
				Id,
				Name,
				Create_VZ_Framework_Agreement__c,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];

		String jsonStr = '{"order":{"payment":{"paymentMethod":"invoice","IBAN":"NL91ABNA0417164300"}},"customer":{"identification":{"nationality":"NL","identificationType":"Passport","identificationNumber":"00456S655","identificationExpirationDate":"2022-04-25"}},"locations":[]}';

		Test.startTest();
		OrderFlowService.parseData(jsonStr, acc.Id, basket.Id);
		Test.stopTest();

		List<Opportunity> opps = [SELECT Id FROM Opportunity];
		List<Ban__c> bans = [SELECT Id FROM Ban__c];
		List<Opportunity_Attachment__c> attachments = [SELECT Id FROM Opportunity_Attachment__c];
		List<mmdoc__Document_Request__c> docRequests = [
			SELECT Id, mmdoc__Document_Template__c, mmdoc__Document_Solution__c
			FROM mmdoc__Document_Request__c
		];
		List<Credit_Check__c> creditCheck = [SELECT Id, Name FROM Credit_Check__c LIMIT 1];
		List<OpportunityLineItem> olis = [SELECT Id FROM OpportunityLineItem];
		System.assert(opps.size() == 1, 'There should be one new opportunity created.');
		System.assert(bans.size() == 1, 'There should be one new ban created.');
		System.assert(attachments.size() == 3, 'There should be three new opportunity attachments created.');
		/* System.assert(creditCheck.size() != 0, 'Credit check should be created.');
		if(creditCheck !=null){
			if(creditCheck[0].Credit_Check_Status_Number__c == 2){
				System.assert(docRequests.size() == 1, 'There should be only one document request created.');
			} else {
				System.assert(docRequests.size() == 0, 'There should be no documents created');
			}
		} */
	}

	/* 	@IsTest
	static void testParseDataWithMavenDocumentsBusinessMobileWithMasterAgreement() {
		MavenDocumentsTestFactory.createSettings();
		mmdoc__Document_Solution__c mdSolution = MavenDocumentsTestFactory.createSolution('Agreements');
		insert mdSolution;
		mmdoc__Document_Template__c mdTemplate = MavenDocumentsTestFactory.createTemplate(mdSolution.Id, 'Business Mobile', 'Google Doc');
		insert mdTemplate;
		mmdoc__Document_Template__c mdTemplate2 = MavenDocumentsTestFactory.createTemplate(mdSolution.Id, 'Mantelovereenkomst', 'Google Doc');
		insert mdTemplate2;

		cscfga__Product_Basket__c basket = [SELECT Id, csbb__Account__r.Create_VZ_Framework_Agreement__c FROM cscfga__Product_Basket__c LIMIT 1];

		cscfga__Product_Definition__c def = CS_DataTest.createProductDefinition('Business Mobile');
		def.Product_Type__c = 'Mobile';
		insert def;

		cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
		config.cscfga__Product_Definition__c = def.Id;
		insert config;

		Account acc = [
			SELECT
				Id,
				Name,
				Create_VZ_Framework_Agreement__c,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];

		acc.Create_VZ_Framework_Agreement__c = true;
		update acc;

		String jsonStr = '{"order":{"payment":{"paymentMethod":"invoice","IBAN":"NL91ABNA0417164300"}},"customer":{"identification":{"nationality":"NL","identificationType":"Passport","identificationNumber":"00456S655","identificationExpirationDate":"2022-04-25"}},"locations":[]}';

		Test.startTest();
		OrderFlowService.parseData(jsonStr, acc.Id, basket.Id);
		Test.stopTest();

		List<Opportunity> opps = [SELECT Id FROM Opportunity];
		List<Ban__c> bans = [SELECT Id FROM Ban__c];
		List<Opportunity_Attachment__c> attachments = [SELECT Id FROM Opportunity_Attachment__c];
		List<mmdoc__Document_Request__c> docRequests = [
			SELECT Id, mmdoc__Document_Template__c, mmdoc__Document_Solution__c
			FROM mmdoc__Document_Request__c
		];
		System.assert(opps.size() == 1, 'There should be one new opportunity created.');
		System.assert(bans.size() == 1, 'There should be one new ban created.');
		System.assert(attachments.size() == 3, 'There should be three new opportunity attachments created.');
		System.assert(docRequests.size() == 2, 'There should be two document requests created.');
	} */

	@IsTest
	static void testCreateBan() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		OrderFlowService.accId = acc.Id;
		OrderFlowService.acc = acc;

		Test.startTest();
		Ban__c ban = OrderFlowService.createBan();
		Test.stopTest();

		System.assert(ban.Id != null, 'Ban should have an Id.');
		System.assertEquals(ban.Account__c, acc.Id, 'Ban should be assigned to account.');
		System.assertEquals(ban.Unify_Customer_Type__c, 'B', 'Wrong unify customer type.');
	}

	@IsTest
	static void testCreateFinancialAccount() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		OrderFlowService.Payment payment = new OrderFlowService.Payment();
		payment.IBAN = 'NL91ABNA0417164300';
		payment.paymentMethod = 'invoice';
		OrderFlowService.Order order = new OrderFlowService.Order();
		order.payment = payment;
		OrderFlowService.CustomOrderData cod = new OrderFlowService.CustomOrderData();
		cod.order = order;

		OrderFlowService.data = cod;
		OrderFlowService.accId = acc.Id;
		OrderFlowService.acc = acc;

		Test.startTest();
		OrderFlowService.createBan();
		Financial_Account__c finAcc = OrderFlowService.createFinancialAccount();
		Test.stopTest();

		System.assert(finAcc.Id != null, 'Financial account should have an Id.');
		System.assertEquals(finAcc.Bank_Account_Number__c, 'NL91ABNA0417164300', 'Wrong iban on the financial account.');
		System.assertEquals(finAcc.Payment_method__c, 'invoice', 'Wrong payment method.');
	}

	@IsTest
	static void testCreateBillingArrangment() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		OrderFlowService.Payment payment = new OrderFlowService.Payment();
		payment.IBAN = 'NL91ABNA0417164300';
		payment.paymentMethod = 'invoice';
		OrderFlowService.Order order = new OrderFlowService.Order();
		order.payment = payment;
		OrderFlowService.CustomOrderData cod = new OrderFlowService.CustomOrderData();
		cod.order = order;

		OrderFlowService.data = cod;
		OrderFlowService.accId = acc.Id;
		OrderFlowService.acc = acc;

		Test.startTest();
		OrderFlowService.createBan();
		OrderFlowService.createFinancialAccount();
		Billing_Arrangement__c billArr = OrderFlowService.createBillingArrangment();
		Test.stopTest();

		System.assert(billArr.Id != null, 'Financial account should have an Id.');
		System.assertEquals(billArr.Bank_Account_Number__c, 'NL91ABNA0417164300', 'Wrong iban on the financial account.');
		System.assertEquals(billArr.Payment_method__c, 'invoice', 'Wrong payment method.');
		System.assertEquals(billArr.Bill_Format__c, 'EB', 'Wrong bill format.');
		System.assertEquals(billArr.Billing_City__c, 'Utrecht', 'Wrong billing city.');
	}

	@IsTest
	static void testcreateOpportunity() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];

		OrderFlowService.Payment payment = new OrderFlowService.Payment();
		payment.IBAN = 'NL91ABNA0417164300';
		payment.paymentMethod = 'invoice';
		OrderFlowService.Order order = new OrderFlowService.Order();
		order.payment = payment;
		OrderFlowService.Identification identification = new OrderFlowService.Identification();
		identification.identificationType = 'Passport';
		identification.identificationNumber = '123456789';
		OrderFlowService.Customer customer = new OrderFlowService.Customer();
		customer.identification = identification;
		OrderFlowService.CustomOrderData cod = new OrderFlowService.CustomOrderData();
		cod.order = order;
		cod.customer = customer;

		OrderFlowService.data = cod;
		OrderFlowService.accId = acc.Id;
		OrderFlowService.acc = acc;

		Test.startTest();
		OrderFlowService.createBan();
		OrderFlowService.createFinancialAccount();
		OrderFlowService.createBillingArrangment();
		Opportunity opp = OrderFlowService.createOpportunity();
		Test.stopTest();

		System.assert(opp.Id != null, 'Opportunity should have an Id.');
		System.assertEquals(opp.StageName, 'Offer', 'Wrong stage name.');
		System.assertEquals(opp.Type, 'New Business', 'Wrong Type.');
		System.assertEquals(opp.Deal_Type__c, 'Acquisition', 'Wrong deal type.');
	}

	@IsTest
	static void testcreateOpportunityAttachment() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		OrderFlowService.Payment payment = new OrderFlowService.Payment();
		payment.IBAN = 'NL91ABNA0417164300';
		payment.paymentMethod = 'invoice';
		OrderFlowService.Order order = new OrderFlowService.Order();
		order.payment = payment;
		OrderFlowService.Identification identification = new OrderFlowService.Identification();
		identification.identificationType = 'Passport';
		identification.identificationNumber = '123456789';
		OrderFlowService.Customer customer = new OrderFlowService.Customer();
		customer.identification = identification;
		OrderFlowService.CustomOrderData cod = new OrderFlowService.CustomOrderData();
		cod.order = order;
		cod.customer = customer;

		OrderFlowService.data = cod;
		OrderFlowService.accId = acc.Id;
		OrderFlowService.acc = acc;

		Test.startTest();
		OrderFlowService.createBan();
		OrderFlowService.createFinancialAccount();
		OrderFlowService.createBillingArrangment();
		Opportunity opp = OrderFlowService.createOpportunity();
		Opportunity_Attachment__c oppAtt = OrderFlowService.createOpportunityAttachment('Copy KvK');
		Test.stopTest();

		System.assert(oppAtt.Id != null, 'Opportunity should have an Id.');
		System.assertEquals(oppAtt.Opportunity__c, opp.Id, 'Wrong opportunity.');
		System.assertEquals(oppAtt.Attachment_Type__c, 'Copy KvK', 'Wrong attachment type.');
	}

	@IsTest
	static void testCreateVFContract() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		OrderFlowService.Payment payment = new OrderFlowService.Payment();
		payment.IBAN = 'NL91ABNA0417164300';
		payment.paymentMethod = 'invoice';
		OrderFlowService.Order order = new OrderFlowService.Order();
		order.payment = payment;
		OrderFlowService.Identification identification = new OrderFlowService.Identification();
		identification.identificationType = 'Passport';
		identification.identificationNumber = '123456789';
		OrderFlowService.Customer customer = new OrderFlowService.Customer();
		customer.identification = identification;
		OrderFlowService.CustomOrderData cod = new OrderFlowService.CustomOrderData();
		cod.order = order;
		cod.customer = customer;

		OrderFlowService.data = cod;
		OrderFlowService.accId = acc.Id;
		OrderFlowService.acc = acc;

		Test.startTest();
		OrderFlowService.createBan();
		OrderFlowService.createFinancialAccount();
		OrderFlowService.createBillingArrangment();
		Opportunity opp = OrderFlowService.createOpportunity();
		VF_Contract__c vfContr = OrderFlowService.createVFContract();
		Test.stopTest();

		System.assert(vfContr.Id != null, 'Contract should have an Id.');
		System.assertEquals(vfContr.Opportunity__c, opp.Id, 'Wrong opportunity.');
		System.assertEquals(vfContr.Document_Type__c, 'Passport', 'Wrong document type.');
		System.assertEquals(vfContr.Document_Number__c, identification.identificationNumber, 'Wrong document number.');
	}

	/* @IsTest
	static void testParseData() {
		Contract_Generation_Settings__c contractGenSettings = new Contract_Generation_Settings__c();
		contractGenSettings.Document_template_name_direct__c = 'Direct Sales Template';
		contractGenSettings.Document_template_name_direct_2__c = 'Direct Sales Template 2';
		contractGenSettings.Document_template_name_BPA__c = 'BPA Template new';
		insert contractGenSettings;

		csclm__Document_Definition__c testDocumentDefinition = CS_DataTest.createDocumentDefinition();
		testDocumentDefinition.ExternalID__c = 'SomeId41';
		insert testDocumentDefinition;

		csclm__Document_Template__c docTemplate = CS_DataTest.createDocumentTemplate('Direct Sales Template 2');
		docTemplate.csclm__Document_Definition__c = testDocumentDefinition.Id;
		docTemplate.csclm__Valid__c = true;
		docTemplate.csclm__Active__c = true;
		docTemplate.ExternalID__c = 'SomeId4';
		insert docTemplate;

		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		String jsonStr = '{"order": { "payment": { "paymentMethod": "invoice", "IBAN": "NL91ABNA0417164300" } }, "customer": { "identification": { "nationality" : "NL", "identificationType": "Passport", "identificationNumber": "00456S655", "identificationExpirationDate" : "2022-04-25" } }, "locations": [ { "id": "PAID-177.261.799", "address": { "street": "Stieltjesweg", "houseNumber": "516", "houseNumberAddition": "", "room": "", "postcode": "2628CK", "city": "Delft" }, "contact": { "initials": "M.A", "middleName": "", "lastName": "Meshcheriakov", "email": "mykyta@hacknet.ua", "phone": "555-45895689", "comment": "be aware of dog" } } ] }';
		cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];

		Test.startTest();
		OrderFlowService.parseData(jsonStr, acc.Id, basket.Id);
		Test.stopTest();

		List<Opportunity> opps = [SELECT Id FROM Opportunity];
		List<Ban__c> bans = [SELECT Id FROM Ban__c];
		List<Opportunity_Attachment__c> attachments = [SELECT Id FROM Opportunity_Attachment__c];

		System.assert(opps.size() == 1, 'There should be one new opportunity created.');
		System.assert(bans.size() == 1, 'There should be one new ban created.');
		System.assert(attachments.size() == 3, 'There should be three new opportunity attachments created.');
	} */
}
