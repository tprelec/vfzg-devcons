@isTest
public with sharing class TestAfterInflightAppliedObserver {
    @isTest
    private static void test() {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
    
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;
        System.runAs (simpleUser) { 
            AfterInflightAppliedObserver inflightObserver = new AfterInflightAppliedObserver();
    
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;
    
            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;
    
            Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112',testAccount, '1111AA', 'Breestraat','LEIDEN', 112); 
            insert site;
        
            Site_Postal_Check__c spc = CS_DataTest.createSite(site);
            insert spc;
        
            Site__c site2 = CS_DataTest.createSite('Eindhoven, Esp 130',testAccount,'2222AA', 'Esp','Eindhoven', 130); 
            insert site2;
        
            Site_Postal_Check__c spc2 = CS_DataTest.createSite(site2); 
            insert spc2;
            Site__c site3 = CS_DataTest.createSite('Eindhoven, Esp 123',testAccount,'2232AA', 'Esp','Eindhoven', 123); 
            insert site3;
        
            Site_Postal_Check__c spc3 = CS_DataTest.createSite(site3);
            insert spc3;
            Site_Availability__c siteAvailability = CS_DataTest.createSiteAvailability('LEIDEN, Breestraat 112', site,spc); 
            insert siteAvailability;
            Site_Availability__c siteAvailability2 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 130', site,spc2); 
            insert siteAvailability2;
            Site_Availability__c siteAvailability3 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 123', site,spc3);
            siteAvailability3.Access_Infrastructure__c = 'ADSL';
            insert siteAvailability3;
            Site_Availability__c siteAvailability4 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 123', site,spc3);
            siteAvailability4.Access_Infrastructure__c = 'EthernetOverFiber';
            insert siteAvailability4;
            Site_Availability__c siteAvailability5 = CS_DataTest.createSiteAvailability('Eindhoven, Esp 123', site,spc3);
            siteAvailability5.Access_Infrastructure__c = 'Coax';
            insert siteAvailability5;
        
            List<Site_Postal_Check__c> spcs = [SELECT Id, Access_Active__c from Site_Postal_Check__c where Id = :spc.Id or Id = :spc2.Id or Id = :spc3.Id];
        
            Competitor_Asset__c ca = new Competitor_Asset__c();
            ca.RecordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByName().get('PABX').getRecordTypeId();
            ca.Account__c = testAccount.Id;
            ca.Site__c = site.Id;
            insert ca;
        
            Competitor_Asset__c ca2 = new Competitor_Asset__c();
            ca2.RecordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByName().get('PABX').getRecordTypeId();
            ca2.Site__c = site2.Id;
            ca2.Account__c = testAccount.Id;
            insert ca2;
        
            Competitor_Asset__c ca3 = new Competitor_Asset__c();
            ca3.RecordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByName().get('PABX').getRecordTypeId();
            ca3.Site__c = site2.Id;
            ca3.Account__c = testAccount.Id;
            insert ca3;
                    
            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;
        
            csord__Order_Request__c coreq = new csord__Order_Request__c(csord__Module_Name__c = 'Test', csord__Module_Version__c = '1.0');
            insert coreq;
        
            csord__Order__c order = new csord__Order__c(
                Name = 'Test Order',
                csord__Account__c = testAccount.Id,
                csord__Status2__c = 'Order Submitted',
                csord__Order_Request__c = coreq.Id,
                csord__Identification__c = 'DWHTestBatchOn_' + system.now(),
                csordtelcoa__Opportunity__c = testOpp.Id
            );
            insert order;
                
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;
                
            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
            cscfga__Product_Definition__c accessDef = CS_DataTest.createProductDefinition('Access Infrastructure');
            accessDef.RecordTypeId = productDefinitionRecordType;
            accessDef.Product_Type__c = 'Fixed';
            insert accessDef;
        
            cscfga__Product_Configuration__c accessConf = CS_DataTest.createProductConfiguration(accessDef.Id, 'Access Infrastructure',basket.Id);
            accessConf.ClonedSiteIds__c = '{"0000":"'+siteAvailability.Id+'", "0002":"'+siteAvailability2.Id+'"}';
            accessConf.ClonedPBXIds__c = '{"0000":"'+ca.Id+'", "0001":"'+ca2.Id+'"}';
            insert accessConf;
        
            csord__Subscription__c subscription= CS_DataTest.createSubscription(accessConf.Id);
            subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            subscription.csord__Order__c = order.Id;
            insert subscription;
            
            csord__Service__c service= CS_DataTest.createService(accessConf.Id,subscription);
            service.csordtelcoa__Product_Configuration__c = accessConf.Id;
            service.csord__Identification__c = 'testSubscription';
            service.csord__Subscription__c = subscription.Id;
            service.csord__Status__c = 'Test';
            service.csord__Order__c = order.Id;
            insert service;
        
            List<Id> serviceIds = new List<Id>();
            serviceIds.add(service.Id);

            Map<Id, Id> orderByBasketMap = new Map<Id, Id>();
            orderByBasketMap.put(order.Id, basket.Id);
        
            csordtelcoa.InflightObservable observable = new csordtelcoa.InflightObservable('',orderByBasketMap);
            inflightObserver.execute(observable, null);

            csord__Order__c updatedOrder = [
                SELECT Id, Inflight_Change_Applied__c
                FROM csord__Order__c
                LIMIT 1
            ];
            System.assertEquals(true, updatedOrder.Inflight_Change_Applied__c);
        }
    }
}