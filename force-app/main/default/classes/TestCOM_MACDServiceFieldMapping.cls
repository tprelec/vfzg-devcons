@isTest
public with sharing class TestCOM_MACDServiceFieldMapping {
	@TestSetup
	static void testSetup() {
		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasketOnly('Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinition('TestDef');
		prodDef.RecordTypeId = productDefinitionRecordType;
		prodDef.Product_Type__c = 'Fixed';
		insert prodDef;

		cscfga__Product_Configuration__c prodConfig = CS_DataTest.createProductConfiguration(prodDef.Id, 'TestConfig', basket.Id);
		insert prodConfig;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(prodConfig.Id);
		subscription.csord__Identification__c = 'testSubscription_helpertest';
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service.csord__Identification__c = 'testService_helpertest';
		service.csord__Status__c = 'Test';
		service.CDO_Umbrella_Service_Id__c = 'TestUmbrellaServId';
		insert service;

		csord__Service__c service2 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service2.csord__Identification__c = 'testService_helpertest_new';
		service2.csord__Status__c = 'Test';
		service2.CDO_Umbrella_Service_Id__c = '';
		service2.csordtelcoa__Replaced_Service__c = service.Id;
		insert service2;

		COM_Delivery_Order__c delOrd = new COM_Delivery_Order__c();
		delOrd.Name = 'DelOrdName';
		delOrd.MACD_move__c = true;
		insert delOrd;

		csord__Service__c service3 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service3.csord__Identification__c = 'testService_helpertest_new3';
		service3.csord__Status__c = 'Test';
		service3.CDO_Umbrella_Service_Id__c = '';
		service3.csordtelcoa__Replaced_Service__c = service.Id;
		service3.COM_Delivery_Order__c = delOrd.Id;
		insert service3;
	}

	@isTest
	private static void testFetchFieldSetFields() {
		Test.startTest();
		COM_MACDServiceFieldMapping msfm = new COM_MACDServiceFieldMapping();
		List<Schema.FieldSetMember> result = msfm.fetchFieldSetFields(COM_Constants.MACD_FIELD_MAPPING_FIELDSET_TEST_NAME);
		Test.stopTest();

		System.assertEquals(1, result.size(), 'Size should be 1.');
		System.assertEquals('csord__Activation_Date__c', result[0].getFieldPath(), 'Field is incorrect.');
	}

	@isTest
	private static void testConstructor() {
		Test.startTest();
		COM_MACDServiceFieldMapping msfm = new COM_MACDServiceFieldMapping();
		Test.stopTest();

		System.assertNotEquals(0, msfm.fieldList.size(), 'List should not be empty.');
		System.assertNotEquals('', msfm.commaSeparatedFieldsWithStartingComma, 'String should not be empty.');
	}

	@isTest
	private static void testMapFieldsFromReplacedToNewServices() {
		List<csord__Service__c> serviceList = [SELECT Id FROM csord__Service__c WHERE csord__Identification__c = 'testService_helpertest_new'];
		COM_MACDServiceFieldMapping msfm = new COM_MACDServiceFieldMapping();
		List<csord__Service__c> updatedServiceList = COM_Helper.getServiceList_CustomQuery(serviceList, msfm.commaSeparatedFieldsWithStartingComma);

		Test.startTest();
		msfm.mapFieldsFromReplacedToNewServices(updatedServiceList);
		Test.stopTest();

		List<csord__Service__c> resultServiceList = [
			SELECT Id, CDO_Umbrella_Service_Id__c
			FROM csord__Service__c
			WHERE csord__Identification__c = 'testService_helpertest_new'
		];
		System.assertEquals('TestUmbrellaServId', resultServiceList[0].CDO_Umbrella_Service_Id__c, 'Field has not been mapped properly.');
	}

	@isTest
	private static void testMapFieldsFromReplacedToNewServices_Move() {
		List<csord__Service__c> serviceList = [SELECT Id FROM csord__Service__c WHERE csord__Identification__c = 'testService_helpertest_new3'];
		COM_MACDServiceFieldMapping msfm = new COM_MACDServiceFieldMapping();
		List<csord__Service__c> updatedServiceList = COM_Helper.getServiceList_CustomQuery(serviceList, msfm.commaSeparatedFieldsWithStartingComma);

		Test.startTest();
		msfm.mapFieldsFromReplacedToNewServices(updatedServiceList);
		Test.stopTest();

		List<csord__Service__c> resultServiceList = [
			SELECT Id, CDO_Umbrella_Service_Id__c
			FROM csord__Service__c
			WHERE csord__Identification__c = 'testService_helpertest_new3'
		];
		System.assertNotEquals('TestUmbrellaServId', resultServiceList[0].CDO_Umbrella_Service_Id__c, 'Field shouldnt be mapped.');
	}
}
