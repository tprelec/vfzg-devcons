@IsTest
private class TestQueueUserSharingTriggerHandler {

    @isTest
    public static void setUpNameForQueueUserRecordOnInsert() {

        Queue_User_Sharing__c qusObject = new Queue_User_Sharing__c(OwnerId = Userinfo.getUserId());

        Test.startTest();

        insert qusObject;

        Test.stopTest();

        System.assertEquals([SELECT Id, Name FROM Queue_User_Sharing__c WHERE Id =: qusObject.Id].Name, [SELECT Id, Name FROM User WHERE Id =: Userinfo.getUserId()].Name);
    }

    @isTest
    public static void setUpNameForQueueUserRecordOnUpdate() {

        Profile p = [SELECT Name, Id FROM Profile WHERE Name = 'System Administrator'];
        UserRole r = [SELECT Name, Id FROM UserRole WHERE DeveloperName = 'Administrator'];

        User usr = TestUtils.generateTestUser('Testing', 'QueueUser' , p.Id, r.Id);

        Queue_User_Sharing__c qusObject = new Queue_User_Sharing__c(OwnerId = Userinfo.getUserId());

        Test.startTest();

        System.runAs(usr) {

            insert qusObject;
            qusObject.ownerId = usr.Id;
            update qusObject;
        }

        Test.stopTest();

        System.assertEquals('Testing QueueUser', [SELECT Id, Name FROM Queue_User_Sharing__c WHERE Id =: qusObject.Id].Name);
    }

}