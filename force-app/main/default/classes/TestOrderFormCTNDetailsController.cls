@isTest
private class TestOrderFormCTNDetailsController {
	private static final Integer MAX_CTNS_PER_PAGE_TEST = 5;

	@isTest(SeeAllData=false)
	static void testNpiPageOrderform() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('NetProfitInformationTriggerHandler', null, 0);
		NetProfit_Settings__c settings = TestUtils.createNetProfitCustomSettings();
		settings.Show_in_File_Ban__c = false;
		settings.Show_in_File_Porting__c = true;
		settings.Show_in_File_Contract_Enddate__c = true;
		settings.Show_in_File_APN__c = true;
		settings.Show_in_File_Apn2__c = true;
		settings.Show_in_File_Apn3__c = true;
		settings.Show_in_File_Apn4__c = true;
		settings.Show_in_File_SMS_Premium__c = true;
		settings.Show_in_File_Data_Block__c = true;
		settings.Show_in_File_Roaming_Spending_Limit__c = true;
		settings.Show_in_File_Third_Party_Content__c = true;
		settings.Show_in_File_Premium_Destinations__c = true;
		settings.Show_in_File_Outgoing_Service__c = true;
		settings.Show_in_Fil_SimcardNo_CurrentProvider__c = true;
		settings.Show_in_File_Simcard_number_VF__c = true;
		settings.Show_in_File_Service_Provider_Name__c = true;
		settings.Show_in_File_Current_Provider_Name__c = true;
		settings.Show_in_File_ContrNo_CurrentProvider__c = true;
		settings.Show_in_File_Cancellation_Known__c = true;
		settings.Show_in_File_Salutation__c = true;
		settings.Show_in_File_First_Name__c = true;
		settings.Show_in_File_Middle_Name__c = true;
		settings.Show_in_File_Last_Name__c = true;
		settings.Show_in_File_Initials__c = true;
		settings.Show_in_File_Email__c = true;
		settings.Show_in_File_Allocation_Code__c = true;
		settings.Inline_Edit_Alternative_Arrangements__c = true;
		settings.Inline_Edit_APN1__c = true;
		settings.Inline_Edit_APN2__c = true;
		settings.Inline_Edit_APN3__c = true;
		settings.Inline_Edit_APN4__c = true;
		settings.Inline_Edit_Cancellation_Known__c = true;
		settings.Inline_Edit_Contract_Enddate__c = true;
		settings.Inline_Edit_ContrNo_CurrentProvider__c = true;
		settings.Inline_Edit_CTN_Number__c = true;
		settings.Inline_Edit_Current_Provider_Name__c = true;
		settings.Inline_Edit_Data_Block__c = true;
		settings.Inline_Edit_Outgoing_Service__c = true;
		settings.Inline_Edit_Premium_Destinations__c = true;
		settings.Inline_Edit_Roaming_Spending_Limit__c = true;
		settings.Inline_Edit_Service_Provider_Name__c = true;
		settings.Inline_Edit_Sharing__c = true;
		settings.Inline_Edit_SimcardNo_CurrentProvider__c = true;
		settings.Inline_Edit_Simcard_number_VF__c = true;
		settings.Inline_Edit_SMS_Premium__c = true;
		settings.Inline_Edit_Third_Party_Content__c = true;
		settings.Inline_Edit_Salutation__c = true;
		settings.Inline_Edit_First_Name__c = true;
		settings.Inline_Edit_Middle_Name__c = true;
		settings.Inline_Edit_Last_Name__c = true;
		settings.Inline_Edit_Initials__c = true;
		settings.Inline_Edit_Email__c = true;
		settings.Inline_Edit_Allocation_Code__c = true;
		settings.Show_in_File_Data_Limit__c = true;
		settings.Order_Summary_Button_Is_Visible__c = true;
		settings.Show_in_File_Sharing__c = true;
		settings.Show_in_File_Data_Blockage__c = true;
		settings.Show_in_File_Ban__c = true;
		update settings;

		Order_Form_Settings__c orderFormSettings = Order_Form_Settings__c.getOrgDefaults();
		orderFormSettings.MAX_CTNS_PER_PAGE__c = MAX_CTNS_PER_PAGE_TEST;
		upsert orderFormSettings;

		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();

		Test.startTest();
		TestUtils.createCompleteContract();
		VF_Contract__c contr = TestUtils.theContract;
		Test.stopTest();

		TestUtils.autoCommit = true;
		Order__c order = TestUtils.createOrder(contr);

		String contractId = contr.Id;
		Contracted_Products__c cp = [SELECT Id FROM Contracted_Products__c WHERE VF_Contract__c = :contractId LIMIT 1];
		cp.Order__c = order.Id;
		cp.Product_Family__c = 'Voice';
		update cp;

		NetProfit_Information__c npi = new NetProfit_Information__c();
		npi.Order__c = order.Id;
		insert npi;

		TestUtils.autoCommit = false;
		NetProfit_CTN__c ctn = TestUtils.createNetProfitCTN(npi);
		TestUtils.autoCommit = true;
		ctn.Commission_Total__c = 100;
		ctn.Action__c = 'New';
		ctn.Product_Group__c = 'Priceplan';
		ctn.Product_Quantity_type__c = 'CTN';
		ctn.Duration__c = 2;
		ctn.Discount__c = 0.1;
		ctn.Commission_Regular__c = 3;
		ctn.Data_Limit__c = '200';
		ctn.Data_Blockage__c = '200';
		ctn.Sharing__c = 'No';
		ctn.Alternative_Arrangements__c = 'test';
		ctn.Cancellation_Known__c = 'Nee';
		ctn.Contract_Number_Current_Provider__c = 'ANT';
		ctn.Current_Provider_Name__c = 'VZT';
		ctn.Quote_Profile__c = '2';
		ctn.Price_Plan_Class__c = 'Voice';
		ctn.Contract_Enddate__c = Date.today().addDays(5);
		insert ctn;

		npi = [SELECT Id FROM NetProfit_Information__c LIMIT 1];

		PageReference pageRef = Page.OrderFormCTNDetails;
		pageRef.getParameters().put('contractId', contr.Id);
		pageRef.getParameters().put('orderId', order.Id);
		Test.setCurrentPage(pageRef);

		OrderFormCTNDetailsController controller = new OrderFormCTNDetailsController();

		String separator = controller.separator;
		List<SelectOption> separators = controller.getSeparators();
		String count = controller.newCtnCount;
		List<SelectOption> ctnCountOptions = controller.getCtnCountOptions();
		String selProfile = controller.selectedProfile;

		List<SelectOption> profileChoices = controller.getProfileChoices();
		String selectedProfileX = controller.selectedProfile;

		controller.saveCTNs();

		// handlecsv without adding a document
		controller.handleCSV();
		controller.saveNetProfit();
		controller.rejectBack();

		// go through statuses
		controller.npi.Status__c = 'New';
		controller.submitNextStep();
		controller.submitNextStep();
		controller.submitNextStep();
		controller.submitNextStep();
		controller.submitNextStep();

		controller.refresh();
		controller.getExportHeader();
		controller.getExportData();
		controller.exportToCSV();

		controller.saveCurrentOrder();
		controller.gatherSelectedPricePlan();
		controller.saveChangeSettings();

		controller.getShowApproveButton();
		controller.recall();
		controller.getShowModifyCTNButtons();
		controller.cancel();
		controller.getPriceplanValues();
		controller.buttonGetOrderSummaryBulk();
		// handlecsv with updating a CTN
		controller.separator = ';';
		String csvContent = getCSVContentNoErrors(ctn.Id);
		controller.csvBody = blob.valueOf(csvContent);
		controller.csvName = 'testCSV';
		controller.handleCSV();

		//for CSV 2
		controller.separator = ';';
		String csvContent2 = getCSVContentToInsert();
		controller.csvBody = blob.valueOf(csvContent2);
		controller.csvName = 'testCSV2';
		controller.handleCSV();

		//for csv 4
		controller.separator = ';';
		String csvContent4 = getCSVContentToDeleteInsertError(ctn.Id);
		controller.csvBody = blob.valueOf(csvContent4);
		controller.csvName = 'testCSV4';
		controller.handleCSV();
		//for csv 3
		controller.separator = ';';
		String csvContent3 = getCSVContentToDeleteWithoutError(ctn.Id);
		controller.csvBody = blob.valueOf(csvContent3);
		controller.csvName = 'testCSV3';
		controller.handleCSV();

		// //for csv 5
		controller.separator = ';';
		String csvContent5 = getCSVContentErrorWishDate();
		controller.csvBody = blob.valueOf(csvContent5);
		controller.csvName = 'testCSV5';
		controller.handleCSV();

		// //for csv 6
		controller.separator = ';';
		String csvContent6 = getCSVContentToInsertSameNumber(ctn.Id);
		controller.csvBody = blob.valueOf(csvContent6);
		controller.csvName = 'testCSV6';
		controller.handleCSV();

		System.assertNotEquals(null, controller, 'Controller should not be null');
		System.assertEquals(
			MAX_CTNS_PER_PAGE_TEST,
			OrderFormCTNDetailsController.MAX_CTNS_PER_PAGE,
			'MAX_CTNS_PER_PAGE should be ' + MAX_CTNS_PER_PAGE_TEST
		);
		System.assertEquals(null, controller.errorFound, 'No error should be found');
		System.assertEquals(null, controller.tooManyCtns, 'Too many CTNs should not be set');
		System.assert(String.isBlank(controller.rejectionComment), 'Rejection comment should not be set');
	}

	private static String getCSVContentToInsert() {
		Map<String, Object> csvRow2 = new Map<String, Object>{
			'"ID"' => '',
			'Quote Profile' => '2',
			'CTN Number' => '31625641646',
			'Action' => 'New',
			'Price Plan Class' => 'Mobile Voice',
			'Simcard number VF' => '000011112',
			'Simcard number current provider' => '11110000',
			'Current Provider name' => 'Orange',
			'Service Provider Name' => 'I provide service',
			'Contract Number Current Provider' => '%$YREH2',
			'Billing Arrangement' => 'What?',
			'Alternative Arrangements' => 'I am so alternative',
			'Data Blockage' => 'Call a data plumber',
			'Sharing' => 'Nee',
			'APN1' => 'Nee',
			'apn2' => 'blackberry.net',
			'apn3' => 'BLGG.NL',
			'apn4' => 'HAAGNET.NL',
			'SMS Premium' => 'Enabled',
			'Internet Access' => 'Yes',
			'roamingspendingLimit' => '50',
			'thirdPartyContent' => 'Yes',
			'premiumDestinations' => 'No_erotic_and_premium_destinations',
			'outgoingService' => 'All_outgoing_disabled',
			'Cancellation Known' => 'Nee',
			'Data Limit' => '200',
			'Porting Wishdate' => '10-10-2050',
			'Salutation' => 'Mr.',
			'First Name' => 'TestFirst2',
			'Middle Name' => 'TestMiddle2',
			'Last Name' => 'TestLast2',
			'Initials' => 'Initials2',
			'Email' => 'test2@example.com',
			'Allocation code' => 'code'
		};
		List<String> csvHeaders = new List<String>(csvRow2.keySet());
		String csvContent = String.join(csvHeaders, ';') + '\n' + String.join(csvRow2.values(), ';') + '\n';
		return csvContent;
	}

	private static String getCSVContentToInsertSameNumber(Id ctnId) {
		Map<String, Object> csvRow1 = new Map<String, Object>{
			'Id' => ctnId,
			'Quote Profile' => '2',
			'CTN Number' => '31625641646',
			'Action' => 'New',
			'Price Plan Class' => 'Mobile Voice',
			'Simcard number VF' => '000011112',
			'Simcard number current provider' => '11110000',
			'Current Provider name' => 'Orange',
			'Service Provider Name' => 'I provide service',
			'Contract Number Current Provider' => '%$YREH2',
			'Billing Arrangement' => 'What?',
			'Alternative Arrangements' => 'I am so alternative',
			'Data Blockage' => 'Call a data plumber',
			'Sharing' => 'Nee',
			'APN1' => 'Nee',
			'apn2' => 'blackberry.net',
			'apn3' => 'BLGG.NL',
			'apn4' => 'HAAGNET.NL',
			'SMS Premium' => 'Enabled',
			'Internet Access' => 'Yes',
			'roamingspendingLimit' => '50',
			'thirdPartyContent' => 'Yes',
			'premiumDestinations' => 'No_erotic_and_premium_destinations',
			'outgoingService' => 'All_outgoing_disabled',
			'Cancellation Known' => 'Nee',
			'Data Limit' => '200',
			'Porting Wishdate' => '10-10-2050',
			'Salutation' => 'Mr.',
			'First Name' => 'TestFirst2',
			'Middle Name' => 'TestMiddle2',
			'Last Name' => 'TestLast2',
			'Initials' => 'Initials2',
			'Email' => 'test2@example.com',
			'Allocation code' => 'code'
		};

		Map<String, Object> csvRow2 = new Map<String, Object>{
			'Id' => '',
			'Quote Profile' => '2',
			'CTN Number' => '31625641646',
			'Action' => 'New',
			'Price Plan Class' => 'Mobile Voice',
			'Simcard number VF' => '000011112',
			'Simcard number current provider' => '11110000',
			'Current Provider name' => 'Orange',
			'Service Provider Name' => 'I provide service',
			'Contract Number Current Provider' => '%$YREH2',
			'Billing Arrangement' => 'What?',
			'Alternative Arrangements' => 'I am so alternative',
			'Data Blockage' => 'Call a data plumber',
			'Sharing' => 'Nee',
			'APN1' => 'Nee',
			'apn2' => 'blackberry.net',
			'apn3' => 'BLGG.NL',
			'apn4' => 'HAAGNET.NL',
			'SMS Premium' => 'Enabled',
			'Internet Access' => 'Yes',
			'roamingspendingLimit' => '50',
			'thirdPartyContent' => 'Yes',
			'premiumDestinations' => 'No_erotic_and_premium_destinations',
			'outgoingService' => 'All_outgoing_disabled',
			'Cancellation Known' => 'Nee',
			'Data Limit' => '200',
			'Porting Wishdate' => '10-10-2050',
			'Salutation' => 'Mr.',
			'First Name' => 'TestFirst2',
			'Middle Name' => 'TestMiddle2',
			'Last Name' => 'TestLast2',
			'Initials' => 'Initials2',
			'Email' => 'test2@example.com',
			'Allocation code' => 'code'
		};

		List<String> csvHeaders = new List<String>(csvRow1.keySet());
		String csvContent =
			String.join(csvHeaders, ';') +
			'\n' +
			String.join(csvRow1.values(), ';') +
			'\n' +
			String.join(csvRow2.values(), ';') +
			'\n';
		return csvContent;
	}
	private static String getCSVContentToDeleteWithoutError(Id ctnId) {
		Map<String, Object> csvRow3 = new Map<String, Object>{
			'"ID"' => ctnId,
			'Quote Profile' => '3',
			'CTN Number' => 'DELETE',
			'Action' => 'New',
			'Price Plan Class' => 'Mobile Voice',
			'Simcard number VF' => '000011112',
			'Simcard number current provider' => '11110000',
			'Current Provider name' => 'Orange',
			'Service Provider Name' => 'I provide service',
			'Contract Number Current Provider' => '%$YREH2',
			'Billing Arrangement' => 'What?',
			'Alternative Arrangements' => 'I am so alternative',
			'Data Blockage' => 'Call a data plumber',
			'Sharing' => 'Nee',
			'APN1' => 'Nee',
			'apn2' => 'blackberry.net',
			'apn3' => 'BLGG.NL',
			'apn4' => 'HAAGNET.NL',
			'SMS Premium' => 'Enabled',
			'Internet Access' => 'Yes',
			'roamingspendingLimit' => '50',
			'thirdPartyContent' => 'Yes',
			'premiumDestinations' => 'No_erotic_and_premium_destinations',
			'outgoingService' => 'All_outgoing_disabled',
			'Cancellation Known' => 'Nee',
			'Data Limit' => '200',
			'Porting Wishdate' => '10-10-2050',
			'Salutation' => 'Mr.',
			'First Name' => 'TestFirst3',
			'Middle Name' => 'TestMiddle3',
			'Last Name' => 'TestLast3',
			'Initials' => 'Initials3',
			'Email' => 'test2@example.com',
			'Allocation code' => 'code'
		};
		List<String> csvHeaders = new List<String>(csvRow3.keySet());
		String csvContent = String.join(csvHeaders, ';') + '\n' + String.join(csvRow3.values(), ';') + '\n';
		return csvContent;
	}
	private static String getCSVContentNoErrors(Id ctnId) {
		//Do not change the order of the keys in the map, you're making a csv
		Map<String, Object> csvRow1 = new Map<String, Object>{
			'ID' => ctnId,
			'Quote Profile' => '1',
			'CTN Number' => '31625641645',
			'Action' => 'Retention',
			'Price Plan Class' => 'Mobile Voice',
			'Simcard number VF' => '00001111',
			'Simcard number current provider' => '11110000',
			'Current Provider name' => 'Spark',
			'Service Provider Name' => 'I provide service',
			'Contract Number Current Provider' => '%$YREH',
			'Billing Arrangement' => 'What?',
			'Alternative Arrangements' => 'I am so alternative',
			'Data Blockage' => 'Call a data plumber',
			'Sharing' => 'Nee',
			'APN1' => 'Nee',
			'apn2' => 'blackberry.net',
			'apn3' => 'BLGG.NL',
			'apn4' => 'HAAGNET.NL',
			'SMS Premium' => 'Enabled',
			'Internet Access' => 'Yes',
			'roamingspendingLimit' => '50',
			'thirdPartyContent' => 'Yes',
			'premiumDestinations' => 'No_erotic_and_premium_destinations',
			'outgoingService' => 'All_outgoing_disabled',
			'Cancellation Known' => 'Nee',
			'Data Limit' => '200',
			'Porting Wishdate' => '10-10-2050',
			'Salutation' => 'Mr.',
			'First Name' => 'TestFirst',
			'Middle Name' => 'TestMiddle',
			'Last Name' => 'TestLast',
			'Initials' => 'Initials',
			'Email' => 'test2@example.com',
			'Allocation code' => 'code'
		};

		List<String> csvHeaders = new List<String>(csvRow1.keySet());
		String csvContent = String.join(csvHeaders, ';') + '\n' + String.join(csvRow1.values(), ';') + '\n';
		return csvContent;
	}
	private static String getCSVContentToDeleteInsertError(Id ctnId) {
		//Do not change the order of the keys in the map, you're making a csv
		Map<String, Object> csvRow1 = new Map<String, Object>{
			'ID' => ctnId,
			'Quote Profile' => '1',
			'CTN Number' => 'DELETE',
			'Action' => 'Retention',
			'Price Plan Class' => 'Mobile Voice',
			'Simcard number VF' => '00001111',
			'Simcard number current provider' => '11110000',
			'Current Provider name' => 'Spark',
			'Service Provider Name' => 'I provide service',
			'Contract Number Current Provider' => '%$YREH',
			'Billing Arrangement' => 'What?',
			'Alternative Arrangements' => 'I am so alternative',
			'Data Blockage' => 'Call a data plumber',
			'Sharing' => 'Nee',
			'APN1' => 'Nee',
			'apn2' => 'blackberry.net',
			'apn3' => 'BLGG.NL',
			'apn4' => 'HAAGNET.NL',
			'SMS Premium' => 'Enabled',
			'Internet Access' => 'Yes',
			'roamingspendingLimit' => '50',
			'thirdPartyContent' => 'Yes',
			'premiumDestinations' => 'No_erotic_and_premium_destinations',
			'outgoingService' => 'All_outgoing_disabled',
			'Cancellation Known' => 'Nee',
			'Data Limit' => '200',
			'Porting Wishdate' => '10-10-2050',
			'Salutation' => 'Mr.',
			'First Name' => 'TestFirst',
			'Middle Name' => 'TestMiddle',
			'Last Name' => 'TestLast',
			'Initials' => 'Initials',
			'Email' => 'test2@example.com',
			'Allocation code' => 'code'
		};
		Map<String, Object> csvRow2 = new Map<String, Object>{
			'"ID"' => '',
			'Quote Profile' => '2',
			'CTN Number' => '31625641646',
			'Action' => 'New',
			'Price Plan Class' => 'Mobile Voice',
			'Simcard number VF' => '000011112',
			'Simcard number current provider' => '11110000',
			'Current Provider name' => 'Orange',
			'Service Provider Name' => 'I provide service',
			'Contract Number Current Provider' => '%$YREH2',
			'Billing Arrangement' => 'What?',
			'Alternative Arrangements' => 'I am so alternative',
			'Data Blockage' => 'Call a data plumber',
			'Sharing' => 'Nee',
			'APN1' => 'Nee',
			'apn2' => 'blackberry.net',
			'apn3' => 'BLGG.NL',
			'apn4' => 'HAAGNET.NL',
			'SMS Premium' => 'Enabled',
			'Internet Access' => 'Yes',
			'roamingspendingLimit' => '50',
			'thirdPartyContent' => 'Yes',
			'premiumDestinations' => 'No_erotic_and_premium_destinations',
			'outgoingService' => 'All_outgoing_disabled',
			'Cancellation Known' => 'Nee',
			'Data Limit' => '200',
			'Porting Wishdate' => '10-10-2050',
			'Salutation' => 'Mr.',
			'First Name' => 'TestFirst2',
			'Middle Name' => 'TestMiddle2',
			'Last Name' => 'TestLast2',
			'Initials' => 'Initials2',
			'Email' => 'test2@example.com',
			'Allocation code' => 'code'
		};

		List<String> csvHeaders = new List<String>(csvRow1.keySet());
		String csvContent =
			String.join(csvHeaders, ';') +
			'\n' +
			String.join(csvRow1.values(), ';') +
			'\n' +
			String.join(csvRow2.values(), ';') +
			'\n';
		return csvContent;
	}
	private static String getCSVContentErrorWishDate() {
		//Do not change the order of the keys in the map, you're making a csv
		Map<String, Object> csvRow1 = new Map<String, Object>{
			'ID' => '4',
			'Quote Profile' => '1',
			'CTN Number' => '31625641646',
			'Action' => 'Retention',
			'Price Plan Class' => 'Mobile Voice',
			'Simcard number VF' => '00001111',
			'Simcard number current provider' => '11110000',
			'Current Provider name' => 'Spark',
			'Service Provider Name' => 'I provide service',
			'Contract Number Current Provider' => '%$YREH',
			'Billing Arrangement' => 'What?',
			'Alternative Arrangements' => 'I am so alternative',
			'Data Blockage' => 'Call a data plumber',
			'Sharing' => 'Nee',
			'APN1' => 'Nee',
			'apn2' => 'blackberry.net',
			'apn3' => 'BLGG.NL',
			'apn4' => 'HAAGNET.NL',
			'SMS Premium' => 'Enabled',
			'Internet Access' => 'Yes',
			'roamingspendingLimit' => '50',
			'thirdPartyContent' => 'Yes',
			'premiumDestinations' => 'No_erotic_and_premium_destinations',
			'outgoingService' => 'All_outgoing_disabled',
			'Cancellation Known' => 'Nee',
			'Data Limit' => '200',
			'Porting Wishdate' => 'errorwishdate',
			'Salutation' => 'Mr.',
			'First Name' => 'TestFirst',
			'Middle Name' => 'TestMiddle',
			'Last Name' => 'TestLast',
			'Initials' => 'Initials',
			'Email' => 'test2@example.com',
			'Allocation code' => 'code'
		};

		List<String> csvHeaders = new List<String>(csvRow1.keySet());
		String csvContent = String.join(csvHeaders, ';') + '\n' + String.join(csvRow1.values(), ';') + '\n';
		return csvContent;
	}
}
