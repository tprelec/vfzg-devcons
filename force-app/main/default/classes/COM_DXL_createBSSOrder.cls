global with sharing class COM_DXL_createBSSOrder {
	@InvocableMethod(label='DXL_CreateBSSOrder' description='Send order to DXL for billing')
	global static void sendToBilling(List<Id> orderIds) {
		sendToBillingFuture(orderIds);
	}

	@future(callout=true)
	public static void sendToBillingFuture(List<Id> orderIds) {
		sendToBillingSync(orderIds);
	}

	public static void sendToBillingSync(List<Id> orderIds) {
		String transactionId = COM_WebServiceConfig.getUUID();
		COM_DXL_Helper.BillingData billingData = COM_DXL_Helper.retrieveBillingData(orderIds[0]);
		billingData.transactionId = transactionId;
		IntegrationSettings settings = getIntegrationSettings();
		if (billingData != null) {
			Map<String, List<COM_DXL_Helper.OrderLineItem>> orderLineItemsPerSite = COM_DXL_Helper.getOrderLineItemsPerSite(billingData);
			if (orderLineItemsPerSite != null && orderLineItemsPerSite.size() > 0) {
				String requestBody = generateCreateBSSOrderRequestBody(orderLineItemsPerSite, billingData);

				HTTPResponse response = sendCreateBSSOrderRequest(settings, requestBody, billingData);
				Id parentId = parseResponse(response, billingData, settings);
				if (settings.generateAttachment && (parentId != null)) {
					COM_DXL_Helper.createRequestAttachment(parentId, requestBody, String.valueOf(COM_DXL_Constants.ATTACHMENT_NAME));
				}
			}
		}
	}

	private static IntegrationSettings getIntegrationSettings() {
		IntegrationSettings settings = new IntegrationSettings();

		COM_Integration_setting__mdt createBSSOrderSetting = COM_Integration_setting__mdt.getInstance(
			COM_DXL_Constants.COM_DXL_BSS_INTEGRATION_SETTING_NAME
		);
		settings.executeMock = createBSSOrderSetting != null ? createBSSOrderSetting.Execute_mock__c : true;

		settings.mockExecuteResult = createBSSOrderSetting != null ? createBSSOrderSetting.Mock_Execution_result__c : true;

		settings.generateAttachment = createBSSOrderSetting != null ? createBSSOrderSetting.Generate_Attachment__c : false;

		settings.successStatus = createBSSOrderSetting != null
			? createBSSOrderSetting.Sync_success_status__c
			: COM_DXL_Constants.COM_DXL_BSS_ACTIVE_STATUS;
		settings.intermittentStatus = createBSSOrderSetting != null
			? createBSSOrderSetting.Sync_intermittent_status__c
			: COM_DXL_Constants.COM_DXL_BSS_REQUESTED_STATUS;
		settings.failedStatus = createBSSOrderSetting != null
			? createBSSOrderSetting.Sync_failed_status__c
			: COM_DXL_Constants.COM_DXL_BSS_FAILED_STATUS;

		settings.namedCredentials = createBSSOrderSetting != null ? createBSSOrderSetting.NamedCredential__c : '';

		return settings;
	}

	public static Id parseResponse(HTTPResponse response, COM_DXL_Helper.BillingData billData, IntegrationSettings settings) {
		List<csord__Service_Line_Item__c> sliList = COM_DXL_Helper.retrieveSliList(billData.mapServiceServiceLineItem, billData.replacedServices);

		for (csord__Service_Line_Item__c sli : sliList) {
			sli.Assigned_product_id__c = COM_DXL_Helper.getComponentAssignedProductId(sli.csord__Service__c, billData, sli);
		}
		if (response.getStatusCode() == 200) {
			updateSLI(true, sliList, settings);
		} else {
			updateSLI(false, sliList, settings);
		}

		COM_DXL_Integration_Log__c log = COM_DXL_Helper.createBillingTransactionLog(billData, '');
		if (log != null) {
			insert log;
			return log.Id;
		}
		return null;
	}

	public static HTTPResponse sendCreateBSSOrderRequest(IntegrationSettings settings, String requestBody, COM_DXL_Helper.BillingData billData) {
		String namedCredentials = settings.namedCredentials;

		HttpRequest reqData = new HttpRequest();
		Http http = new Http();

		String endpoint = COM_WebServiceConfig.getEndpoint(namedCredentials, COM_DXL_Constants.DXL_BSS_WEB_PARAM);

		reqData.setMethod('POST');
		reqData.setHeader(COM_DXL_Constants.BUSINESS_TRANSACTION_ID, billData.transactionId);
		reqData.setHeader('Content-Type', 'application/json');
		reqData.setHeader('Accept', '*/*');
		reqData.setEndpoint(endpoint);
		reqData.setBody(requestBody);
		HTTPResponse response = new HttpResponse();
		if (settings.executeMock && !Test.isRunningTest()) {
			response.setHeader('Content-Type', 'application/json');
			if (settings.mockExecuteResult) {
				response.setBody(COM_DXL_Constants.COM_DXL_BSS_SYNC_MOCK_RESPONSE_SUCCESS);
				response.setStatusCode(200);
			} else {
				response.setBody(COM_DXL_Constants.COM_DXL_BSS_SYNC_MOCK_RESPONSE_FAILED);
				response.setStatusCode(400);
			}
		} else {
			response = http.send(reqData);
		}
		return response;
	}

	private static String generateCreateBSSOrderRequestBody(
		Map<String, List<COM_DXL_Helper.OrderLineItem>> orderLineItemsPerSite,
		COM_DXL_Helper.BillingData billingData
	) {
		String requestString;

		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeFieldName(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_CUSTOMER_DETAILS);
		generator.writeStartObject();
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BAID, billingData.customerInformation.baid);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BCID, billingData.customerInformation.bcid);
		generator.writeFieldName(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BILLING_ORDER);

		generator.writeStartArray();
		for (String siteAPID : orderLineItemsPerSite.keySet()) {
			generator.writeStartObject();
			generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_SLSAPID, siteAPID);
			generator.writeFieldName(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_ORDER_LINES);
			generator.writeStartArray();
			for (COM_DXL_Helper.OrderLineItem orderLineItem : orderLineItemsPerSite.get(siteAPID)) {
				if (orderLineItem.activityType == COM_DXL_Constants.COM_DXL_BSS_ACTIVITY_TYPE_PROVIDE) {
					generator = generateNewProvide(generator, orderLineItem, billingData.b2bGPCI);
				} else if (orderLineItem.activityType == COM_DXL_Constants.COM_DXL_BSS_ACTIVITY_TYPE_CHANGE) {
					generator = generateChange(generator, orderLineItem);
				} else if (orderLineItem.activityType == COM_DXL_Constants.COM_DXL_BSS_ACTIVITY_TYPE_CEASE) {
					generator = generateCease(generator, orderLineItem);
				}
			}
			generator.writeEndArray();
			generator.writeEndObject();
		}

		generator.writeEndArray();
		generator.writeEndObject();
		generator.writeEndObject();
		requestString = generator.getAsString();

		return requestString;
	}

	private static JSONGenerator generateCease(JSONGenerator generator, COM_DXL_Helper.OrderLineItem orderLineItem) {
		generator.writeStartObject();
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_SFIBID, orderLineItem.sFIBID);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_BSS_ELEMENT_COMPONENT_APID,
			COM_DXL_Helper.emptyStringInsteadOfNull(orderLineItem.componentAPID)
		);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_ACTIVITY_TYPE, orderLineItem.activityType);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_ACTIVATION_DATE, orderLineItem.activationDate);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_EXTERNAL_REFERENCE_ID, orderLineItem.externalReferenceID);
		generator.writeEndObject();
		return generator;
	}
	private static JSONGenerator generateChange(JSONGenerator generator, COM_DXL_Helper.OrderLineItem orderLineItem) {
		generator.writeStartObject();
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_SFIBID, orderLineItem.sFIBID);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_BSS_ELEMENT_COMPONENT_APID,
			COM_DXL_Helper.emptyStringInsteadOfNull(orderLineItem.componentAPID)
		);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_ACTIVITY_TYPE, orderLineItem.activityType);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_ACTIVATION_DATE, orderLineItem.activationDate);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_INVOICING_START_DATE, orderLineItem.invoicingStartDate);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_TYPE_OF_CHARGE, orderLineItem.typeOfCharge);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_CHARGE_DESCRIPTION, orderLineItem.chargeDescription);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_BSS_ELEMENT_CHARGE_AMOUNT,
			COM_DXL_Helper.emptyStringInsteadOfNull(orderLineItem.chargeAmount)
		);
		generator.writeNumberField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_QUANTITY, orderLineItem.quantity);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_BSS_ELEMENT_DISCOUNT_AMOUNT,
			COM_DXL_Helper.emptyStringInsteadOfNull(orderLineItem.discountAmount)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_BSS_ELEMENT_DISCOUNT_DESCRIPTION,
			COM_DXL_Helper.emptyStringInsteadOfNull(orderLineItem.discountDescription)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_BSS_ELEMENT_DISCOUNT_METHOD,
			COM_DXL_Helper.mapDiscountMethodValue(orderLineItem.discountMethod)
		);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_CHARGE_CODE, orderLineItem.chargeCode);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_1, orderLineItem.bItag1);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_2, orderLineItem.bItag2);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_3, orderLineItem.bItag3);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_4, orderLineItem.bItag4);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_5, orderLineItem.bItag5);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_EXTERNAL_REFERENCE_ID, orderLineItem.externalReferenceID);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_CEASE_AUTOMATICALLY, 'FALSE');
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_ALLOCATION_CODE, '');
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_PO_NUMBER, '');
		generator.writeEndObject();
		return generator;
	}

	private static JSONGenerator generateNewProvide(
		JSONGenerator generator,
		COM_DXL_Helper.OrderLineItem orderLineItem,
		String b2bGenericProductCatalogId
	) {
		generator.writeStartObject();
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_SFIBID, orderLineItem.sFIBID);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_COMPONENT_APID, orderLineItem.componentAPID);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_ACTIVITY_TYPE, orderLineItem.activityType);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_ACTIVATION_DATE, orderLineItem.activationDate);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_INVOICING_START_DATE, orderLineItem.invoicingStartDate);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_TYPE_OF_CHARGE, orderLineItem.typeOfCharge);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_CHARGE_DESCRIPTION, orderLineItem.chargeDescription);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_BSS_ELEMENT_CHARGE_AMOUNT,
			COM_DXL_Helper.emptyStringInsteadOfNull(orderLineItem.chargeAmount)
		);
		generator.writeNumberField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_QUANTITY, orderLineItem.quantity);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_BSS_ELEMENT_DISCOUNT_AMOUNT,
			COM_DXL_Helper.emptyStringInsteadOfNull(orderLineItem.discountAmount)
		);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_DISCOUNT_DESCRIPTION, orderLineItem.discountDescription);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_BSS_ELEMENT_DISCOUNT_METHOD,
			COM_DXL_Helper.mapDiscountMethodValue(orderLineItem.discountMethod)
		);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_CHARGE_CODE, orderLineItem.chargeCode);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_1, orderLineItem.bItag1);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_2, orderLineItem.bItag2);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_3, orderLineItem.bItag3);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_4, orderLineItem.bItag4);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_BITAG_5, orderLineItem.bItag5);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_EXTERNAL_REFERENCE_ID, orderLineItem.externalReferenceID);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_B2B_GENERIC_PRODUCT_ID, b2bGenericProductCatalogId);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_CEASE_AUTOMATICALLY, 'FALSE');
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_ALLOCATION_CODE, '');
		generator.writeStringField(COM_DXL_Constants.COM_DXL_BSS_ELEMENT_PO_NUMBER, '');
		generator.writeEndObject();
		return generator;
	}

	public static Boolean updateSLI(Boolean success, List<csord__Service_Line_Item__c> sliList, IntegrationSettings settings) {
		Boolean result = false;
		if (!success) {
			for (csord__Service_Line_Item__c sli : sliList) {
				if (sli.Number_of_failed_attempts__c == null) {
					sli.Number_of_failed_attempts__c = 1;
				} else {
					sli.Number_of_failed_attempts__c = sli.Number_of_failed_attempts__c + 1;
				}
				if (sli.Number_of_failed_attempts__c == 3) {
					sli.Billing_Status__c = settings.failedStatus;
					result = true;
				} else {
					sli.Billing_Status__c = settings.intermittentStatus;
				}

				if (COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE_DISCOUNT.contains(sli.csord__line_item_type__c)) {
					sli.Billing_Status__c = COM_DXL_Constants.COM_DXL_BSS_NA_STATUS;
				}
			}
		}

		update sliList;
		return result;
	}

	public class CreateBSSOrderResponse {
		public String status;
	}

	private class IntegrationSettings {
		private Boolean doNotExecute;
		private Boolean executeMock;
		private Boolean mockExecuteResult;
		private Boolean generateAttachment;
		private String successStatus;
		private String intermittentStatus;
		private String failedStatus;
		private String namedCredentials;
	}
}
