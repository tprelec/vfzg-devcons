@isTest
public with sharing class OrderEntryTestFactory {
	public static OE_Product__c createOEProduct(String type, Boolean insertRecord) {
		if (type == null) {
			type = 'Main';
		}

		OE_Product__c prod = new OE_Product__c(Type__c = type);

		if (!insertRecord == false) {
			insert prod;
		}

		return prod;
	}

	public static List<OE_Product__c> createOEProducts(List<String> types, Boolean insertRecord) {
		List<OE_Product__c> toReturn = new List<OE_Product__c>();

		for (String type : types) {
			OE_Product__c prod = new OE_Product__c(Type__c = type);
			toReturn.add(prod);
		}

		if (!insertRecord == false) {
			insert toReturn;
		}

		return toReturn;
	}

	public static OE_Add_On__c createOEAddOn(
		String type,
		String productCode,
		Boolean insertRecord
	) {
		if (type == null) {
			type = 'Internet Additional';
		}

		if (productCode == null) {
			productCode = '1111';
		}

		OE_Add_On__c addOn = new OE_Add_On__c(Type__c = type, Product_Code__c = productCode);

		if (!insertRecord == false) {
			insert addOn;
		}

		return addOn;
	}

	public static List<OE_Add_On__c> createOEAddOns(
		List<String> types,
		List<String> productCodes,
		Boolean insertRecord
	) {
		List<OE_Add_On__c> toReturn = new List<OE_Add_On__c>();

		for (Integer i = 0; i < types.size(); i++) {
			OE_Add_On__c addOn = new OE_Add_On__c(
				Type__c = types[i],
				Product_Code__c = productCodes[i]
			);
			toReturn.add(addOn);
		}

		if (!insertRecord == false) {
			insert toReturn;
		}

		return toReturn;
	}

	public static OE_Discount__c createOEDiscount(
		String name,
		String type,
		Integer value,
		String level,
		Boolean insertRecord
	) {
		if (name == null) {
			name = 'De eerste maand Safe Online XL gratis';
		}

		if (type == null) {
			type = 'Percentage';
		}

		if (value == null) {
			value = 100;
		}

		if (level == null) {
			level = 'Bundle';
		}

		OE_Discount__c discount = new OE_Discount__c(
			Name = name,
			Type__c = type,
			Value__c = value,
			Level__c = level
		);

		if (!insertRecord == false) {
			insert discount;
		}

		return discount;
	}

	public static OE_Product_Add_On__c createOEProductAddOn(
		OE_Product__c product,
		OE_Add_On__c addOn,
		Boolean insertRecord
	) {
		OE_Product_Add_On__c prodAddon = new OE_Product_Add_On__c(
			Product__c = product.Id,
			Add_On__c = addon.Id
		);

		if (!insertRecord == false) {
			insert prodAddon;
		}

		return prodAddon;
	}

	public static OE_Product_Bundle__c createOEProductBundle(
		OE_Product__c mainProduct,
		OE_Product__c internetProduct,
		String productCode,
		Boolean isDefault,
		Boolean insertRecord
	) {
		if (productCode == null) {
			productCode = '1111';
		}

		if (isDefault == null) {
			isDefault = false;
		}
		OE_Product_Bundle__c productBundle = new OE_Product_Bundle__c(
			Main_Product__c = mainProduct.Id,
			Internet_Product__c = internetProduct.Id,
			Bundle_Product_Code__c = productCode,
			Default__c = isDefault
		);

		if (!insertRecord == false) {
			insert productBundle;
		}

		return productBundle;
	}

	public static Order_Entry_Product_Detail__c createOEProductDetail(
		OE_Product__c product,
		String category,
		String value,
		String type,
		Boolean insertRecord
	) {
		if (category == null) {
			category = 'Category';
		}

		if (value == null) {
			value = 'Value';
		}

		if (type == null) {
			type = 'Type';
		}
		Order_Entry_Product_Detail__c oeProdDetail = new Order_Entry_Product_Detail__c(
			Product__c = product.Id,
			Category__c = category,
			Value__c = value,
			Type__c = type
		);

		if (!insertRecord == false) {
			insert oeProdDetail;
		}

		return oeProdDetail;
	}

	public static OE_Promotion__c createOEPromotion(
		String name,
		String type,
		Boolean isActive,
		String customerType,
		String contractTerm,
		String connectionType,
		String offNetType,
		Boolean insertRecord
	) {
		if (name == null) {
			name = 'De eerste maand 100% korting op uw Zakelijk Start abonnement';
		}

		if (type == null) {
			type = 'Standard';
		}

		if (isActive == null) {
			isActive = true;
		}

		if (customerType == null) {
			customerType = 'Existing';
		}

		if (contractTerm == null) {
			contractTerm = '1';
		}

		if (connectionType == null) {
			connectionType = 'Off-Net';
		}

		if (offNetType == null) {
			offNetType = 'MTC';
		}
		OE_Promotion__c promotion = new OE_Promotion__c(
			Promotion_Name__c = name,
			Type__c = type,
			Active__c = isActive,
			Customer_Type__c = customerType,
			Contract_Term__c = contractTerm,
			Connection_Type__c = connectionType,
			Off_net_Type__c = offNetType
		);

		if (!insertRecord == false) {
			insert promotion;
		}

		return promotion;
	}

	public static OE_Promotion_Discount__c createOEPromotionDiscount(
		OE_Discount__c discount,
		OE_Promotion__c promotion,
		Boolean insertRecord
	) {
		OE_Promotion_Discount__c promoDiscount = new OE_Promotion_Discount__c(
			Promotion__c = promotion.Id,
			Discount__c = discount.Id
		);

		if (!insertRecord == false) {
			insert promoDiscount;
		}

		return promoDiscount;
	}

	public static OE_Validity_Period__c createOEValidityPeriod(
		Date dateFrom,
		Date dateTo,
		OE_Product__c product,
		Boolean insertRecord
	) {
		if (dateFrom == null) {
			dateFrom = Date.today();
		}

		if (dateTo == null) {
			dateTo = Date.today().addDays(356);
		}

		OE_Validity_Period__c validityPeriod = new OE_Validity_Period__c(
			From__c = dateFrom,
			To__c = dateTo,
			Order_Entry_Product__c = product.Id
		);

		if (!insertRecord == false) {
			insert validityPeriod;
		}

		return validityPeriod;
	}
}