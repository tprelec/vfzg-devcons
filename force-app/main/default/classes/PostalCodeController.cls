public with sharing class PostalCodeController {

	private final Site__c site;
    //
    public String redirectUrl {public get; private set;}
    public Boolean shouldRedirect {public get; private set;}
    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public PostalCodeController(ApexPages.StandardController stdController) {
        
        Site__c mySite = (Site__c)stdController.getRecord();
        redirectUrl = stdController.view().getUrl();
        Id siteId = mySite.Id;
        this.site = [select Id,
                            Sla__c,
                            Last_dsl_check__c,
                            Last_fiber_check__c,
                            Last_dsl_Canvas_check_c__c,
                            Last_fiber_Canvas_check_c__c,
                            Blocked_checks__c,                            
                            Postal_Code_Check_Status__c,
                            Site_Street__c, 
                            Site_Postal_Code__c, 
                            Site_House_Number__c, 
                            Site_House_Number_Suffix__c, 
                            Canvas_Postal_Code__c,          //Marcel:W-000032
                            Canvas_House_Number__c,         //Marcel:W-000032
                            Canvas_House_Number_Suffix__c   //Marcel:W-000032
                        from Site__c
                        where Id =: siteId];
    }


    public void doDSLCheck(){

        //system.debug('doDSLCheck');
        //Check on canvas adres   
        //system.debug('rediredt:' + site.Blocked_checks__c);
        //system.debug('doDSLCheck');
        
        if (site.Blocked_checks__c != null)
        {
            if (!site.Blocked_checks__c.Contains('CanvasD'))
            {
                //system.debug('Vreuls-doDSLCheck');
                if( site.Canvas_Postal_Code__c != null && site.Canvas_House_Number__c != null)
                {
                    system.debug('doDSLCheck');
                    List<Site__c> sites = new List<Site__c>(); 
                    sites.add(this.site);   
                    PostalcodeCheckController.doPostalCodeCheckForSites(sites, 'CanvasD');
                }
            }
        }      
        else
        {
                //system.debug('Vreuls-doDSLCheck');
                if( site.Canvas_Postal_Code__c != null && site.Canvas_House_Number__c != null)
                {
                    system.debug('doDSLCheck');
                    List<Site__c> sites = new List<Site__c>(); 
                    sites.add(this.site);   
                    PostalcodeCheckController.doPostalCodeCheckForSites(sites, 'CanvasD');
                }

        }
        shouldRedirect = true;    
           
    }

   public void doFiberCheck(){

        if (site.Blocked_checks__c != null)
        {
            if (!site.Blocked_checks__c.Contains('CanvasF'))
            {
                //system.debug('Vreuls-doFiberCheck');
                //Check on canvas adres
                if( site.Canvas_Postal_Code__c != null && site.Canvas_House_Number__c != null)
                {
                    system.debug('dofiberCheck');
                    List<Site__c> sites = new List<Site__c>();
                    sites.add(this.site);                             
                    PostalcodeCheckController.doPostalCodeCheckForSites(sites, 'CanvasF');
                }
            }   
        } 
        else
        {
             if( site.Canvas_Postal_Code__c != null && site.Canvas_House_Number__c != null)
                {
                    system.debug('dofiberCheck');
                    List<Site__c> sites = new List<Site__c>();
                    sites.add(this.site);                             
                    PostalcodeCheckController.doPostalCodeCheckForSites(sites, 'CanvasF');
                }
        }
        shouldRedirect = true;   

   }
}