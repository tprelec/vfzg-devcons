/*
 * @author Luka Maric
 * @date 27-10-2022
 *
 * @description Controller for com_ApplyInflightChangesOpp LWC component
 */
public with sharing class COM_ApplyInflightChangeController {
	/**
	 * @description get product basket id in LWC
	 * @param  recordId Opportunity Id
	 * @return          id of product basket
	 */
	@AuraEnabled(cacheable=true)
	public static Id getProductBasketId(String recordId) {
		Id recordTypeId = Schema.SObjectType.cscfga__Product_Basket__c.getRecordTypeInfosByDeveloperName().get('Inflight_Change').getRecordTypeId();

		return [
			SELECT Id
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c = :recordId AND Primary__c = TRUE AND RecordTypeId = :recordTypeId
			LIMIT 1
		]
		.Id;
	}

	/**
	 * @description invoked from LWC to perform CS API call for Applying Inflight Change
	 * @param  basketId     Inflight Product Basket Id
	 */
	@AuraEnabled
	public static void performAction(Id basketId) {
		try {
			if (Test.isRunningTest()) {
				throw new AuraHandledException('perform action exception');
			} else {
				csordtelcoa.API_V1.applyInflightChanges(basketId, false);
			}
		} catch (Exception e) {
			System.debug(LoggingLevel.DEBUG, e);
			throw new AuraHandledException(e.getMessage());
		}
	}
}
