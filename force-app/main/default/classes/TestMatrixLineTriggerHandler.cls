/**
 * @description       : Apex Test Class for the MatrixLineTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   20-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestMatrixLineTriggerHandler {
	@testSetup
	static void setup() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'Matrix_Lines',
			External_Number__c = 1,
			Object_Prefix__c = 'ML-10-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;

		appro__Matrix__c objMatrix = new appro__Matrix__c(
			Name = 'TestMatrix',
			appro__Object__c = 'cscfga__Product_Basket__c',
			appro__Child_Object__c = 'CS_Basket_Snapshot_Transactional__c',
			appro__Child_Link_Field__c = 'Product_Basket__c'
		);
		insert objMatrix;
	}

	@isTest
	static void testSetExternalIds() {
		appro__Matrix__c objMatrix = [SELECT Id FROM appro__Matrix__c LIMIT 1];
		appro__Matrix_Line__c objMatrixLine = new appro__Matrix_Line__c(
			appro__Matrix__c = objMatrix.Id,
			appro__Active__c = true,
			appro__Active_Today__c = 'Yes',
			Approval_Step__c = 'level2',
			Commercial_Terms__c = '*',
			Proposition__c = '*',
			Valid_From__c = System.today().addDays(-7),
			Valid_To__c = System.today().addDays(7),
			Duration_From__c = 2,
			Duration_To__c = 3,
			Treshold_Group__c = 'group1'
		);

		Test.startTest();
		insert objMatrixLine;
		Test.stopTest();

		appro__Matrix_Line__c objMatrixLineUpdated = [SELECT Id, ExternalID__c FROM appro__Matrix_Line__c WHERE Id = :objMatrixLine.Id LIMIT 1];

		System.assertEquals('ML-10-000002', objMatrixLineUpdated.ExternalID__c, 'The External ID on Matrix Line was not set correctly.');
	}
}
