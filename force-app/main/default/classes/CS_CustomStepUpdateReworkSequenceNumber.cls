/**
 * @description Orchestrator Step that needs to have the global modifier 
 * in order to work properly because of the different namespaces.
 */
global without sharing class CS_CustomStepUpdateReworkSequenceNumber implements CSPOFA.ExecutionHandler {

    global List<SObject> process(List<SObject> stepListParameter) {
        List<SObject> result = new List<SObject>();
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) stepListParameter;

        List<Id> processIds = new List<Id>();
        Set<Id> vfContractIds = new Set<Id>();

        List<VF_Contract__c> vfContractsToUpdate = new List<VF_Contract__c>();

        try {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                processIds.add(step.CSPOFA__Orchestration_Process__c);
            }

            Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c> ([
                SELECT id,
                    csordtelcoa__Service__c,
                    CSPOFA__Process_Type__c,
                    Contract_VF__c
                FROM CSPOFA__Orchestration_Process__c
                WHERE id in :processIds
            ]);

            for (CSPOFA__Orchestration_Process__c p : processesMap.values()) {
                vfContractIds.add(p.Contract_VF__c);
            }

            Map<Id, VF_Contract__c> vfContractMap = new Map<Id, VF_Contract__c>([
                SELECT Id,
                    Name,
                    Rework_Sequence_Nr__c
                FROM VF_Contract__c
                WHERE Id IN :vfContractIds
            ]);

            for (CSPOFA__Orchestration_Step__c step : stepList) {
                Id vfContractId = processesMap.get(step.CSPOFA__Orchestration_Process__c).Contract_VF__c;

                VF_Contract__c vfContract = vfContractMap.get(vfContractId);

                if (vfContract != null) {
                    vfContractsToUpdate.add(
                        new VF_Contract__c(
                            Id = vfContractId,
                            Rework_Sequence_Nr__c = vfContract.Rework_Sequence_Nr__c == null ? 1 : vfContract.Rework_Sequence_Nr__c + 1
                        )
                    );
                }
            }

            Set<VF_Contract__c> vfContractSet = new Set<VF_Contract__c>();
            List<VF_Contract__c> vfContractsUpdate = new List<VF_Contract__c>();
            vfContractSet.addAll(vfContractsToUpdate);
            vfContractsUpdate.addAll(vfContractSet);

            if (vfContractsUpdate.size() > 0) {
                update vfContractsUpdate;
            }

            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_COMPLETE;
                step.CSPOFA__Completed_Date__c = Date.today();
                step.CSPOFA__Message__c = 'Custom step succeeded';
                result.add(step);
            }

        } catch (Exception ex) {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
                step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
                result.add(step);
            }
        }
        return result;
    }
}