public class CST_getChatTranscript {
	public String currentCaseId { get; set; }
	public String fileNameName { get; set; }
	public LiveChatTranscript chatTranscript { get; set; }

	public CST_getChatTranscript(ApexPages.StandardController controller) {
		currentCaseId = ApexPages.CurrentPage().getparameters().get('id');

		List<LiveChatTranscript> listChat = [SELECT id, caseId, body, name FROM LiveChatTranscript WHERE caseid = :currentCaseId];

		System.debug('listChat: ' + listChat);

		if (listChat != null && listChat.size() > 0) {
			chatTranscript = listChat[0];
			fileNameName = 'ChatTranscript_' + chatTranscript.name + '.pdf';
			System.debug('fileNameName: ' + fileNameName);
			Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename=' + fileNameName);
		}
	}
}
