/**
 * Copyright (c) 2012, FinancialForce.com, inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 * - Neither the name of the FinancialForce.com, inc nor the names of its contributors 
 *      may be used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

/**
 * This is a dummy test class to obtain 100% coverage for the generated WSDL2Apex code, it is not a funcitonal test class
 **/ 
@isTest  
public class MetadataServiceTest
{    
    /**
     * Dummy Metadata API web service mock class (see MetadataCreateJobTest.cls for a better example)
     **/
    public class WebServiceMockImpl implements WebServiceMock 
    {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType) 
        {
            if(request instanceof MetadataService.retrieve_element)
                response.put('response_x', new MetadataService.retrieveResponse_element());
            else if(request instanceof MetadataService.checkRetrieveStatus_element)
                response.put('response_x', new MetadataService.checkRetrieveStatusResponse_element());
            else if(request instanceof MetadataService.describeMetadata_element)
                response.put('response_x', new MetadataService.describeMetadataResponse_element());
            else if(request instanceof  MetadataService.deleteMetadata_element)
                response.put('response_x', new MetadataService.deleteMetadataResponse_element());
            else if(request instanceof  MetadataService.upsertMetadata_element)
                response.put('response_x', new MetadataService.upsertMetadataResponse_element());
            else if(request instanceof  MetadataService.createMetadata_element)
                response.put('response_x', new MetadataService.createMetadataResponse_element());
            return;
        }
    }    
        
    @IsTest
    private static void coverGeneratedCodeCRUDOperations()
    {   
        // Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations         
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
    }
    
    @IsTest
    private static void coverGeneratedCodeFileBasedOperations1()
    {       
        // Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations         
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
        metaDataPort.retrieve(null);
        metaDataPort.describeMetadata(null);
    }

    @IsTest
    private static void coverGeneratedCodeFileBasedOperations2()
    {       
        // Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations         
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
        metaDataPort.deleteMetadata(null, null);
        metaDataPort.upsertMetadata(null);
        metaDataPort.createMetadata(null);
        metaDataPort.checkRetrieveStatus(null, null);
    }
        
    @IsTest
    private static void coverGeneratedCodeTypes()
    {              
        // Reference types
        new MetadataService();
        new MetadataService.DescribeMetadataResult();

        new MetadataService.LogInfo();



        new MetadataService.CallOptions_element();


        new MetadataService.describeMetadataResponse_element();




        new MetadataService.describeMetadata_element();


        new MetadataService.RetrieveResult();

        new MetadataService.retrieve_element();
        new MetadataService.DescribeMetadataObject();
        new MetadataService.AsyncResult();
        
        new MetadataService.checkRetrieveStatus_element();
        new MetadataService.RetrieveRequest();

        new MetadataService.SharingRules();
        
        new MetadataService.SharedTo();
        
        new MetadataService.Metadata();
        
        
        new MetadataService.RetrieveMessage();
        new MetadataService.ProfileObjectPermissions();


        
        new MetadataService.SessionHeader_element();
        new MetadataService.DebuggingHeader_element();
        
        
        new MetadataService.PackageTypeMembers();
        
        
        new MetadataService.retrieveResponse_element();
        
        
        new MetadataService.checkRetrieveStatusResponse_element();
        new MetadataService.FileProperties();
        new MetadataService.FilterItem();
        
        new MetadataService.DebuggingInfo_element();
        
        new MetadataService.Package_x();
        
        new MetadataService.SessionSettings();
        
        new MetadataService.deleteMetadataResponse_element();
        
        new MetadataService.Error();
        
        
        new MetadataService.SaveResult();
        
        new MetadataService.readMetadataResponse_element();
        
        
        new MetadataService.deleteMetadata_element();
        
        
        new MetadataService.createMetadataResponse_element();
        
        new MetadataService.createMetadata_element();
        
        
        new MetadataService.readMetadata_element();
        
        
        new MetadataService.readSharingRulesResponse_element();
        
        new MetadataService.ReadResult();
        
        
        new MetadataService.DeleteResult();
        
        
        new MetadataService.upsertMetadata_element();
        
        
        new MetadataService.upsertMetadataResponse_element();
        
        
        new MetadataService.UpsertResult();
        
        
        new MetaDataService.SharingCriteriaRule();
        
        
        new MetaDataService.SharingBaseRule();
        
        
        new MetaDataService.SharingTerritoryRule();
        
        new MetaDataService.AccountSharingRuleSettings();
        
        new MetaDataService.SharingOwnerRule();
        
        
        new MetaDataService.AllOrNoneHeader_element();
        
    }

    @IsTest
    private static void elfMissingGetRecordsTest() { // elf patch
        new MetadataService.ReadSharingRulesResult().getRecords();
        
        
        new MetadataService.ReadSharingOwnerRuleResult().getRecords();
        
    }

    @IsTest
    private static void elfMissingGetResultTest() { // elf patch

        new MetadataService.readSharingOwnerRuleResponse_element().getResult();
        
    }    
}