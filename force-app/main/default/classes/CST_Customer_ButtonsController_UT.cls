@isTest
private class CST_Customer_ButtonsController_UT {
	private static Case c;

	@isTest
	static void confirmSolution() {
		createTestData();
		updateCaseSequentially(c);
		Test.startTest();
		CST_Customer_ButtonsController.showComponent(c.Id);
		try {
			CST_Customer_ButtonsController.updateCloseCase(c.Id);
		} catch (Exception e) {
			System.debug('exc -> ' + e.getMessage());
		}
		Test.stopTest();
	}

	@isTest
	static void rejectSolution() {
		createTestData();
		updateCaseSequentially(c);
		Test.startTest();
		CST_Customer_ButtonsController.updateCaseToInProgress(c.Id);
		Test.stopTest();
	}

	@isTest
	static void testCloseCase() {
		createTestData();
		updateCaseSequentially(c);
		Test.startTest();
		CST_Customer_ButtonsController.caseClosed(c.Id, 'Case Closed by TestClass');
		Test.stopTest();
	}

	private static void updateCaseSequentially(Case c) {
		c.Status = 'Assessing';
		update c;

		c.Status = 'In progress';
		c.CST_Sub_Status__c = 'Assigned';
		c.CST_Case_is_with_Team__c = 'Internal - Advisor';
		update c;

		c.CST_Product__c = 'CST_Standard';
		c.Status = 'Waiting for Customer Validation';
		c.OwnerId = UserInfo.getUserId();
		c.CST_End2End_Owner__c = UserInfo.getUserId();
		update c;
	}

	private static void createTestData() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		Contact caseContact = TestUtils.createContact(acc);
		caseContact.Email = '123456abcde@mail.com';
		update caseContact;
		RecordType questionRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CST_Question'];
		Id questionRecordTypeId = (questionRecordType != null) ? questionRecordType.Id : null;
		c = new Case(AccountId = acc.Id, ContactId = caseContact.Id, Remedy_Ticket_No__c = '123', RecordTypeId = questionRecordTypeId);
		insert c;

		CST_Questionnaire__c questionnaire = new CST_Questionnaire__c(CST_Case__c = c.Id);

		insert questionnaire;

		CST_Questionnaire_Line_Item__c lineItem = new CST_Questionnaire_Line_Item__c(
			CST_Questionnaire__c = questionnaire.Id,
			CST_Case__c = c.Id,
			CST_Question__c = 'Question here',
			CST_Answer__c = 'Answer here'
		);
		insert lineItem;

		CST_ExternalSystem_Credentials__c remedyCredentials = new CST_ExternalSystem_Credentials__c();
		remedyCredentials.Name = 'Remedy';
		remedyCredentials.CST_Endpoint__c = 'https://vodafoneziggo.remedy.com';
		remedyCredentials.CST_Password__c = 'pass';
		remedyCredentials.CST_Username__c = 'user01';
		insert remedyCredentials;

		CST_External_Ticket__c remedy = new CST_External_Ticket__c(
			CST_Case__c = c.id,
			CST_IncidentID__c = '565656',
			CST_Summary__c = 'test case',
			CST_Status__c = 'Pending'
		);
		insert remedy;
	}
}
