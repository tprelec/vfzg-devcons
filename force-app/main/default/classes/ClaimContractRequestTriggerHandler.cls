public with sharing class ClaimContractRequestTriggerHandler extends TriggerHandler {
    public static final String STATUS_APPROVED = 'Approved';
    public static final String STATUS_PENDING = 'Pending';
    public static final String STATUS_REJECTED = 'Rejected';
    public static final String STATUS_RECALLED = 'Recalled';

    private Map<Id, Claimed_Contract_Approval_Request__c> ccMap;
    private Map<Id, Claimed_Contract_Approval_Request__c> ccOldMap;

    Map<Id, Dealer_Information__c> dealersInfo;
    Map<Id, List<VF_Contract__c>> reqContracts;
    Map<Id, User> users;

    private void init() {
        ccMap = (Map<Id, Claimed_Contract_Approval_Request__c>) this.newMap;
        ccOldMap = (Map<Id, Claimed_Contract_Approval_Request__c>) this.oldMap;
    }

    public override void beforeUpdate() {
        init();
        handleRecall();
    }

    public override void afterUpdate() {
        init();
        handleApprovals();
    }

    private void handleApprovals() {
        dealersInfo = getDealerInfo();
        reqContracts = getContractsPerReq(ccMap.keySet());
        users = getUserMap();

        List<Claimed_Contract__c> ccToInsert = new List<Claimed_Contract__c>();
        Map<Id, VF_Contract__c> contractsToUpdate = new Map<Id, VF_Contract__c>();

        for (Claimed_Contract_Approval_Request__c ccReq : ccMap.values()) {
            approveRequest(ccReq, ccToInsert, contractsToUpdate);
            recallRequest(ccReq, contractsToUpdate);
        }

        insert ccToInsert;
        update contractsToUpdate.values();
    }

    private void approveRequest(
        Claimed_Contract_Approval_Request__c ccReq,
        List<Claimed_Contract__c> ccToInsert,
        Map<Id, VF_Contract__c> contractsToUpdate
    ) {
        if (checkIfApproved(ccReq)) {
            for (VF_Contract__c contract : reqContracts.get(ccReq.Id)) {
                Claimed_Contract__c cc = new Claimed_Contract__c(
                    Claimed_Contract_Approval_Request__c = ccReq.id,
                    Contract_VF__c = contract.id,
                    New_Owner__c = ccReq.OwnerId,
                    Old_Owner__c = ccReq.Old_Owner__c
                );
                ccToInsert.add(cc);

                User u = users.get(ccReq.OwnerId);
                Dealer_Information__c di = getDealerInformation(u);
                contract.Dealer_Information__c = setContractDealerInfo(di);
                contract.Dealer_code_responsible__c = setContractDealerCodeResp(di);
                contract.OwnerId = ccReq.OwnerId;
                contract.Claimed_Contract_Approval_Request__c = null;
                contractsToUpdate.put(contract.Id, contract);
            }
        }
    }

    private void recallRequest(
        Claimed_Contract_Approval_Request__c ccReq,
        Map<Id, VF_Contract__c> contractsToUpdate
    ) {
        if (
            ccReq.Recall_Approval_Request__c &&
            !reqContracts.isEmpty() &&
            reqContracts.containsKey(ccReq.Id)
        ) {
            for (VF_Contract__c contract : reqContracts.get(ccReq.Id)) {
                contract.Claimed_Contract_Approval_Request__c = null;
                contractsToUpdate.put(contract.Id, contract);
            }
        }
    }

    private void handleRecall() {
        for (Claimed_Contract_Approval_Request__c ccReq : ccMap.values()) {
            // recall approval & remove related contracts
            if (ccReq.Recall_Approval_Request__c) {
                if (ccReq.First_Approval__c == STATUS_PENDING) {
                    ccReq.First_Approval__c = STATUS_RECALLED;
                }
                if (ccReq.Second_Approval__c == STATUS_PENDING) {
                    ccReq.Second_Approval__c = STATUS_RECALLED;
                }
                if (ccReq.Third_Approval_Status__c == STATUS_PENDING) {
                    ccReq.Third_Approval_Status__c = STATUS_RECALLED;
                }
            }
        }
    }

    private Map<Id, Dealer_Information__c> getDealerInfo() {
        Set<Id> ccReqOwnerIds = getUserIds();
        Map<Id, Dealer_Information__c> userDealerInfoMap = new Map<Id, Dealer_Information__c>();

        List<Dealer_Information__c> diList = [
            SELECT Id, Dealer_Code__c, Contact__c, Contact__r.UserId__c
            FROM Dealer_Information__c
            WHERE Contact__r.UserId__c IN :ccReqOwnerIds
        ];

        for (Dealer_Information__c di : diList) {
            userDealerInfoMap.put(di.Contact__r.UserId__c, di);
        }

        return userDealerInfoMap;
    }

    // get ccreqs owner info
    private Map<Id, User> getUserMap() {
        Set<Id> ccReqOwnerIds = getUserIds();

        Map<Id, User> users = new Map<Id, User>(
            [SELECT Id, Ziggo__c, Profile.Name, UserType FROM User WHERE Id IN :ccReqOwnerIds]
        );

        return users;
    }

    // get contracts for claim contract requests
    private Map<Id, List<VF_Contract__c>> getContractsPerReq(Set<Id> ccReqIds) {
        List<VF_Contract__c> contracts = [
            SELECT
                Id,
                OwnerId,
                Claimed_Contract_Approval_Request__c,
                Direct_Indirect__c,
                Account__c,
                Account__r.Mobile_Dealer__c,
                Account__r.Mobile_Dealer__r.Dealer_Code__c
            FROM VF_Contract__c
            WHERE Claimed_Contract_Approval_Request__c IN :ccReqIds
        ];

        Map<Id, List<VF_Contract__c>> requestContracts = new Map<Id, List<VF_Contract__c>>();

        for (VF_Contract__c cc : contracts) {
            if (!requestContracts.containsKey(cc.Claimed_Contract_Approval_Request__c)) {
                requestContracts.put(
                    cc.Claimed_Contract_Approval_Request__c,
                    new List<VF_Contract__c>()
                );
            }
            requestContracts.get(cc.Claimed_Contract_Approval_Request__c).add(cc);
        }

        return requestContracts;
    }

    private Set<Id> getUserIds() {
        Set<Id> ccReqOwnerIds = new Set<Id>();

        for (Claimed_Contract_Approval_Request__c ccReq : ccMap.values()) {
            ccReqOwnerIds.add(ccReq.OwnerId);
        }

        return ccReqOwnerIds;
    }

    private Dealer_Information__c getDealerInformation(User usr) {
        return dealersInfo.containsKey(usr.Id) ? dealersInfo.get(usr.Id) : null;
    }

    private Id setContractDealerInfo(Dealer_Information__c dealerInfo) {
        return dealerInfo != null ? dealerInfo.Id : null;
    }

    private String setContractDealerCodeResp(Dealer_Information__c dealerInfo) {
        return dealerInfo != null ? dealerInfo.Dealer_Code__c : null;
    }

    private Boolean checkIfApproved(Claimed_Contract_Approval_Request__c ccReq) {
        return !reqContracts.isEmpty() &&
            ((ccReq.First_Approval__c == STATUS_APPROVED &&
            ccOldMap.get(ccReq.Id).First_Approval__c == STATUS_PENDING) ||
            (ccReq.First_Approval__c == STATUS_REJECTED &&
            ccReq.Second_Approval__c == STATUS_APPROVED &&
            ccOldMap.get(ccReq.Id).Second_Approval__c == STATUS_PENDING) ||
            (ccReq.First_Approval__c == STATUS_REJECTED &&
            ccReq.Second_Approval__c == STATUS_REJECTED &&
            ccReq.Third_Approval_Status__c == STATUS_APPROVED &&
            ccOldMap.get(ccReq.Id).Third_Approval_Status__c == STATUS_PENDING));
    }
}