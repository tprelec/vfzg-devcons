public with sharing class ZipDeliveryPlanningController {
    public static final String QUEUE_TYPE = 'Queue';
    public static final String WRITE_PERMISSION = 'Write';
    public static final String READ_PERMISSION = 'Read';
    public static final String NO_PERMISSION = 'None';
    public static final String CUSTOM_PERMISSION_WRITE = 'COM_Delivery_Planning_Write';
    public static final String CUSTOM_PERMISSION_READ = 'COM_Delivery_Planning_Read';
    public static final String KEY_REMINDER_DAYS_BEFORE = 'reminderDaysBefore';
    public static final String KEY_REMINDER_HOUR = 'reminderHour';
    public static final Integer MAXIMUM_NUMBER_OF_DELIVERY_ORDERS = 30;
    public static final Integer MAX_SIZE_NAME_FIELD = 80;
    static final String EXCEPTION_DELETE_APPT = 'Errors trying to delete. ';
    public static final String FIELD_ENGINEER_PROF_NAME = 'fieldEngineerProfile';

    @AuraEnabled(cacheable=true)
    public static String getCurrentUserPermission() {
        if (FeatureManagement.checkPermission(CUSTOM_PERMISSION_WRITE)) {
            return WRITE_PERMISSION;
        } else if (FeatureManagement.checkPermission(CUSTOM_PERMISSION_READ)) {
            return READ_PERMISSION;
        } else {
            return NO_PERMISSION;
        }
    }

    @AuraEnabled(cacheable=true)
    public static Map<String, String> getSettings() {
        Map<String, String> appSettings = New Map<String, String>();
        for (COM_Delivery_Planning_Setting__mdt stng: [
            SELECT DeveloperName, Value__c
            FROM COM_Delivery_Planning_Setting__mdt
            WHERE DeveloperName != :KEY_REMINDER_HOUR
            AND DeveloperName != :KEY_REMINDER_HOUR
        ]) {
            appSettings.put(stng.DeveloperName, stng.Value__c);
        }
        return appSettings;
    }

    @AuraEnabled(cacheable=true)
    public static List<Group> getRegions() {
        Set<String> sRegions = new Set<String>();
        Id currUserId = UserInfo.getUserId();
        for (COM_Region_Setting__mdt crs_Setting: [SELECT Queue_API_Name__c FROM COM_Region_Setting__mdt]) {
            sRegions.add(crs_Setting.Queue_API_Name__c);
        }
        List<Group> regionList = [
            SELECT Id, Name
            FROM Group
            WHERE Type = :QUEUE_TYPE
            AND DeveloperName IN :sRegions
            AND Id IN (SELECT GroupId FROM GroupMember WHERE UserOrGroupId = :currUserId)
        ];
        if (regionList.size() == 0) {
            throw new AuraHandledException('You need to have access to some region to see anything here.');
        }
        return regionList;
    }


    @AuraEnabled(cacheable=true)
    public static List<User> getUsersByRegion(String regionId) {
        String sFieldEngineerProfileName = [
            SELECT Value__c
            FROM COM_Delivery_Planning_Setting__mdt
            WHERE DeveloperName = :FIELD_ENGINEER_PROF_NAME
        ][0].Value__c;
        try {
            Id fieldEngineerProfileId = [SELECT Id FROM Profile WHERE Name = :sFieldEngineerProfileName][0].Id;
            return [
                SELECT Id, Name
                FROM User
                WHERE Id IN (
                    SELECT UserOrGroupId
                    FROM GroupMember
                    WHERE GroupId = :regionId
                ) AND ProfileId = :fieldEngineerProfileId];
        } catch (Exception e) {
            String errorText = e.getMessage();
            throw new AuraHandledException(formatFinalUserMessage(errorText));
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<COM_Delivery_Order__c> getUnplannedDeliveryOrders(String regionId) {
        return [
            SELECT Id,
            Site__r.Site_Account__r.Name,
            Site__r.Site_Street__c,
            Site__r.Site_House_Number__c,
            Site__r.Site_House_Number_Suffix__c,
            Site__r.Site_Postal_Code__c,
            Site__r.Site_City__c,
            Order__r.csord__Account__r.Name,
            Technical_Contact__r.Name,
            Technical_Contact__r.Phone,
            Wish_Date__c,
            Products__c,
            Provisioned__c
            FROM COM_Delivery_Order__c
            WHERE Confirmed_Installation__c = false
            AND OwnerId = :regionId
            AND Id NOT IN (SELECT Related_Delivery_Order__c FROM Delivery_Appointment__c WHERE Start__c >= TODAY AND IsDeleted = false)
            AND Onsite_Visit_Required__c = true
            AND Site__c != ''
            AND Order__c != null
            AND Order__r.csord__Account__c != null
            AND Provisioned__c = true
            LIMIT :MAXIMUM_NUMBER_OF_DELIVERY_ORDERS
        ];
    }

    @AuraEnabled
    public static List<Delivery_Appointment__c> getAppointments(String regionId, String engineerId, String since, String to) {
        DateTime dt = (DateTime) JSON.deserialize('"' + since + '"', DateTime.class);
        DateTime dtSince = DateTime.newInstance( dt.getTime());
        dt = (DateTime) JSON.deserialize('"' + to + '"', DateTime.class);
        DateTime dtTo = DateTime.newInstance( dt.getTime());
        List<Delivery_Appointment__c> lCalendarAppointments = [
            SELECT Id,
            Name,
            Start__c,
            Duration__c,
            Location__c,
            Contact__c,
            Contact__r.Name,
            Contact__r.Phone,
            Field_Engineer__c,
            Related_Delivery_Order__c,
            Confirmed_Installation__c,
            Related_Delivery_Order__r.Wish_Date__c,
            Related_Delivery_Order__r.Site__r.Site_Account__r.Name,
            Related_Delivery_Order__r.Order__r.csord__Account__r.Name,
            Related_Delivery_Order__r.Products__c,
            Related_Delivery_Order__r.Provisioned__c
            FROM Delivery_Appointment__c
            WHERE Start__c >= :dtSince
            AND Start__c <= :dtTo
            AND Field_Engineer__c = :engineerId
            AND OwnerId = :regionId
        ];
        return lCalendarAppointments;
    }

    @AuraEnabled
    public static void moveAppointment(String appointmentId, Datetime dtStart, Datetime dtEnd) {
        Decimal dMinutes = (Decimal.valueOf(dtEnd.getTime() - dtStart.getTime())) / 1000 / 60;
        Delivery_Appointment__c deliAppt = new Delivery_Appointment__c();
        deliAppt.Id = Id.valueOf(appointmentId);
        deliAppt.Start__c = dtStart;
        deliAppt.Duration__c = Integer.valueOf(dMinutes);
        System.debug(deliAppt);
        try {
            update deliAppt;
        } catch (DmlException e) {
            String errorText = e.getMessage();
            throw new AuraHandledException(formatFinalUserMessage(errorText));
        }
    }

    @AuraEnabled
    public static Delivery_Appointment__c saveAppointment(
        String appointmentId,
        String subject,
        Datetime start,
        Integer duration,
        String location,
        String contactId,
        String description,
        String fieldEngineerId,
        String relatedDeliveryOrderId,
        String regionId
    ) {
        Delivery_Appointment__c deliAppt = new Delivery_Appointment__c();
        if (appointmentId != '') {
            deliAppt.Id = Id.valueOf(appointmentId);
        }
        if (regionId != '') {
            deliAppt.OwnerId = Id.valueOf(regionId);
        }
        deliAppt.Name = subject.abbreviate(MAX_SIZE_NAME_FIELD);
        deliAppt.Start__c = start;
        deliAppt.Duration__c = duration;
        deliAppt.Location__c = location;
        deliAppt.Contact__c = contactId == ''? null: contactId;
        deliAppt.Description__c = description;
        deliAppt.Related_Delivery_Order__c = relatedDeliveryOrderId;
        deliAppt.Field_Engineer__c = fieldEngineerId;
        try {
            upsert deliAppt;
        } catch(DmlException e) {
            throw new AuraHandledException( formatFinalUserMessage(e.getMessage()) );
        }
        return deliAppt;
    }

    private static String formatFinalUserMessage(String errorText) {
        String sErrorToFound = 'FIELD_CUSTOM_VALIDATION_EXCEPTION';
        Integer iFoundCustomVal = errorText.indexOf('FIELD_CUSTOM_VALIDATION_EXCEPTION');
        if (iFoundCustomVal > -1) {
            errorText = errorText.substring(iFoundCustomVal + sErrorToFound.length());
        }
        return errorText;
    }

    @AuraEnabled
    public static void deleteAppointment(String appointmentId) {
        try {
            if (appointmentId != '') {
                Delivery_Appointment__c deliAppt = new Delivery_Appointment__c();
                if (appointmentId != '') {
                    deliAppt.Id = Id.valueOf(appointmentId);
                }
                delete deliAppt;
            }
        } catch (DmlException e) {
            String errorText = e.getMessage();
            throw new AuraHandledException(EXCEPTION_DELETE_APPT + formatFinalUserMessage(errorText));
        }
    }

    @AuraEnabled
    public static void confirmAppointment(String appointmentId) {
        Delivery_Appointment__c deliAppt = getAppointment(appointmentId);

        Map<String, String> mpSettings = new Map<String, String>();
        for(COM_Delivery_Planning_Setting__mdt stng: [
            SELECT DeveloperName, Value__c
            FROM COM_Delivery_Planning_Setting__mdt
            WHERE DeveloperName = :KEY_REMINDER_DAYS_BEFORE
            OR DeveloperName = :KEY_REMINDER_HOUR
        ]) {
            mpSettings.put(stng.DeveloperName, stng.Value__c);
        }


        Integer iDaysBefore = Integer.valueOf(mpSettings.get(KEY_REMINDER_DAYS_BEFORE));
        Datetime dtReminder = deliAppt.Start__c.addDays(iDaysBefore * -1);

        Integer iMinutes = Integer.valueOf(deliAppt.Duration__c);
        Datetime dtEnd = deliAppt.Start__c.addMinutes(iMinutes);
        String[] sHour = mpSettings.get(KEY_REMINDER_HOUR).split('\\:');
        Datetime calculatedDateTime = Datetime.newInstance(
            dtReminder.year(),
            dtReminder.month(),
            dtReminder.day(),
            Integer.valueOf(sHour[0]),
            Integer.valueOf(sHour[1]),
            0
        );

        COM_Delivery_Order__c deliveryOrder = new COM_Delivery_Order__c();
        deliveryOrder.Id = deliAppt.Related_Delivery_Order__c;
        deliveryOrder.Confirmed_Installation__c = true;
        deliveryOrder.Installation_Start__c = deliAppt.Start__c;
        if (!(deliAppt.Contact__c == null)) {
            deliveryOrder.Technical_Contact__c = deliAppt.Contact__c;
        }
        deliveryOrder.Installation_End__c = dtEnd;
        try {
            update deliveryOrder;
        } catch (DmlException e) {
            String errorText = e.getMessage();
            throw new AuraHandledException(formatFinalUserMessage(errorText));
        }
    }

    @AuraEnabled
    public static void unconfirmAppointment(String appointmentId) {
        Delivery_Appointment__c deliAppt = getAppointment(appointmentId);
        COM_Delivery_Order__c deliveryOrder = new COM_Delivery_Order__c();
        deliveryOrder.Id = deliAppt.Related_Delivery_Order__c;
        deliveryOrder.Confirmed_Installation__c = false;
        deliveryOrder.Installation_Start__c = null;
        deliveryOrder.Installation_End__c = null;
        deliveryOrder.Appointment_Changed__c = true;
        try {
            update deliveryOrder;
        } catch (DmlException e) {
            String errorText = e.getMessage();
            throw new AuraHandledException(formatFinalUserMessage(errorText));
        }
    }

    private static Delivery_Appointment__c getAppointment(String appointmentId) {
        Delivery_Appointment__c deliAppt;
        List<Delivery_Appointment__c> lDeliAppt = [
            SELECT Id, Start__c, Duration__c, Related_Delivery_Order__c, Contact__c
            FROM Delivery_Appointment__c
            WHERE Id = :appointmentId
        ];
        if (lDeliAppt.size() > 0) {
            deliAppt = lDeliAppt.get(0);
        }
        return deliAppt;
    }
}