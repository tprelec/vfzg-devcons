@isTest
public class DeleteProductConfigurationsTest {

  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        
        List<User> listOfExistingUsers = [SELECT UserRoleId, ProfileId, Alias, Email, EmailEncodingKey, LastName, LanguageLocaleKey, LocaleSidKey, TimeZoneSidKey, UserName FROM User WHERE UserName =: simpleUser.UserName];
        if(listOfExistingUsers.size() == 0) {
            insert simpleUser;    
        } else {
            simpleUser = listOfExistingUsers[0];
        }
        
        System.runAs (simpleUser) { 
      
     
        Framework__c frameworkSetting = new Framework__c();
        frameworkSetting.Framework_Sequence_Number__c = 2;
        insert frameworkSetting;
      
        PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
        insert priceResetSetting;
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;
      
      
      
        Sales_Settings__c ssettings = new Sales_Settings__c();
        ssettings.Postalcode_check_validity_days__c = 2;
        ssettings.Max_Daily_Postalcode_Checks__c = 2;
        ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
        ssettings.Postalcode_check_block_period_days__c = 2;
        ssettings.Max_weekly_postalcode_checks__c = 15;
        insert ssettings;
      
        Test.startTest();
        
        Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
        insert testOpp;
        
        
        cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        basket.Primary__c = true;
        basket.cscfga__Basket_Status__c = 'Valid';
        insert basket;
        
        
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
        
        cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        accessInfraDef.Product_Type__c = 'Fixed';
        accessInfraDef.RecordTypeId = productDefinitionRecordType;
        insert accessInfraDef;
        
        cscfga__Product_Configuration__c managedInternetConf = CS_DataTest.createProductConfiguration(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
        managedInternetConf.cscfga__Root_Configuration__c = null;
        managedInternetConf.cscfga__Parent_Configuration__c = null;
        insert managedInternetConf;
        
       
        
        Id batchJobId = Database.executeBatch(new DeleteProductConfigurations(), 200);
        system.debug('Batch Job Id == '+batchJobId);
        
        
        DeleteProductConfigurations.deleteStart();
        
        Test.stopTest();
     }
  }
  
}