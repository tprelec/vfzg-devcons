/**
 * @tests AuraTest
 */
public without sharing class AuraField
{
    @AuraEnabled
    public String name { get; private set; }


    @AuraEnabled
    public String label { get; private set; }


    @AuraEnabled
    public String type { get; private set; }


    @AuraEnabled
    public String value { get; private set; }


    public AuraFieldType auraFieldType { get; private set; }


    @AuraEnabled
    public List<AuraSelectOption> options { get; private set; }


    public AuraField(String name, String value)
    {
        this.name = name;
        this.value = value;
    }


    public AuraField(String name, String value, String label,
                     AuraFieldType auraFieldType, List<AuraSelectOption> options)
    {
        this(name, value);
        this.label = label;
        this.auraFieldType = auraFieldType;
        this.type = auraFieldType.name();
        this.options = options;
    }
}