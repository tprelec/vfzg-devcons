@IsTest
private class TestCS_COMInstallComponentController {
    @IsTest
    static void test_update_delivery_components_implemented() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;


            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            insert ord;

            COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', ord.Id, false);
            deliveryOrder1.Product_Abbreviations__c = 'ACCESS,ZIPRO';
            insert deliveryOrder1;

            csord__Subscription__c subscription= CS_DataTest.createSubscription(testConfConf.Id);
            subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            insert subscription;

            csord__Service__c parentService= CS_DataTest.createService(testConfConf.Id,subscription, 'Service Created');
            parentService.csord__Identification__c = 'testSubscription';
            parentService.COM_Delivery_Order__c = deliveryOrder1.Id;
            parentService.VFZ_Product_Abbreviation__c = 'ZIPRO';
            insert parentService;

            List<Delivery_Component__c> deliveryComponents = CS_DataTest.createDeliveryComponents(parentService,'Layer_2_3_internet_service', 3, true);
            
            List<CS_COMDeliveryComponentUpdateWrapper> deliveryComponentUpdateWrappers = new List<CS_COMDeliveryComponentUpdateWrapper>();
            deliveryComponentUpdateWrappers.addAll(createDeliveryComponentUpdateWrappers(deliveryComponents, Datetime.now()));

            for (CS_COMDeliveryComponentUpdateWrapper wrapper : deliveryComponentUpdateWrappers) {
                wrapper.value = Datetime.now();
            }

            String wrappers = JSON.serialize(deliveryComponentUpdateWrappers);
            CS_COMInstallComponentController.updateDeliveryComponents(wrappers);
            
            deliveryComponentUpdateWrappers.addAll(createDeliveryComponentUpdateWrappers(deliveryComponents, Datetime.now()));

            for (CS_COMDeliveryComponentUpdateWrapper wrapper : deliveryComponentUpdateWrappers) {
                wrapper.value = Datetime.now();
            }

            wrappers = JSON.serialize(deliveryComponentUpdateWrappers);
            CS_COMInstallComponentController.updateDeliveryComponents(wrappers);

            List<Delivery_Component__c> newDeliveryComponents = [SELECT Id, Implemented_Date__c
            FROM Delivery_Component__c
            WHERE Id IN :deliveryComponents];

            for (Delivery_Component__c deliveryComponent : newDeliveryComponents){
                System.debug('*** newDeliveryComponents: '+ JSON.serializePretty(newDeliveryComponents));
                System.assert(deliveryComponent.Implemented_Date__c != null);
            }

        }
    }

    public static List<CS_ComDeliveryComponentUpdateWrapper> createDeliveryComponentUpdateWrappers(
        List<Delivery_Component__c> deliveryComponents,
        Datetime value
    ) {
        List<CS_ComDeliveryComponentUpdateWrapper> returnValue = new List<CS_ComDeliveryComponentUpdateWrapper>();

        for (Delivery_Component__c deliveryComponent : deliveryComponents) {
            CS_ComDeliveryComponentUpdateWrapper wrapper = new CS_ComDeliveryComponentUpdateWrapper();
            wrapper.deliveryComponentId = deliveryComponent.Id;
            wrapper.value = value;
            returnValue.add(wrapper);
        }
        return returnValue;
    }
}