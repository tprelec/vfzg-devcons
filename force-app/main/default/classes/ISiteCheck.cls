public interface ISiteCheck {
    List<Object> getSites(Map<String, String> searchFields, String productDefinitionID, Id[] excludeIds, Integer pageOffset, Integer pageLimit);
}