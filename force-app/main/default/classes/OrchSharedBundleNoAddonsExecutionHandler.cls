//This is the custom controller class for the CS Orchestrator custom step 'Validate Shared Bundle'.
global class OrchSharedBundleNoAddonsExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable{
    private Map<Id,List<OrchCalloutsWrapperCls.calloutData>> calloutResultsMap = new Map<Id,List<OrchCalloutsWrapperCls.calloutData>>();
    private Map<Id,String> mapbillingCustomer;
    private Map<Id,String> mapOrderId;
    
    public Boolean performCallouts(List<SObject> data) {
        // this always runs first
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        calloutResultsMap = new Map<Id,List<OrchCalloutsWrapperCls.calloutData>>();
        Boolean calloutsPerformed = false;
        try{
            Set<Id> resultIds = new Set<Id>();
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                resultIds.add( step.CSPOFA__Orchestration_Process__c );
            }
            OrchUtils.getProcessDetails(resultIds);
            mapbillingCustomer = OrchUtils.mapbillingCustomer;
            mapOrderId = OrchUtils.mapOrderId;          
            for(CSPOFA__Orchestration_Step__c step : stepList) {
                String orderId = mapOrderId.get( step.CSPOFA__Orchestration_Process__c );
                String billingCustomerId = mapbillingCustomer.get( step.CSPOFA__Orchestration_Process__c );
                calloutResultsMap.put(step.Id,EMP_BSLintegration.submitAllGLs(orderId,billingCustomerId,null));       
                calloutsPerformed = true;          
            }
        } catch (exception e) {
            system.debug(e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString());
        }
        return calloutsPerformed;
    }

    public List<sObject> process(List<sObject> data) {
        List<sObject> result = new List<sObject>();
        List<Contracted_Products__c> contractedProductsToUpdate = new List<Contracted_Products__c>();
        List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data;
        for (CSPOFA__Orchestration_Step__c step : stepList) {
            Boolean success = false;
            String strRequest = '';
            String strResponse = '';
            if(calloutResultsMap.containsKey(step.Id)) {
                List<OrchCalloutsWrapperCls.calloutData> calloutResult = calloutResultsMap.get(step.Id);
                for(OrchCalloutsWrapperCls.calloutData tempCalloutResult : calloutResult) {
                    if(tempCalloutResult != null && (Boolean) tempCalloutResult.success == true ) {
                        success = true;
                    }
                    strRequest += tempCalloutResult.strReq != null ? tempCalloutResult.strReq + '\r\n' : '';
                    strResponse += tempCalloutResult.strRes != null ? tempCalloutResult.strRes.abbreviate(131000) + '\r\n' : '';
                }
                if(success) { 
                    String strMessage = calloutResult[0].errorMessage.equalsIgnoreCase( 'Callout not Needed' ) ?  calloutResult[0].errorMessage : 'Validate order for products step completed';
                    step = OrchUtils.setStepRecord( step , false , strMessage);
                } else {  
                    step = OrchUtils.setStepRecord( step , true , 'Error occurred: ' + (String) calloutResult[0].errorMessage );	
                }
                step.Request__c = strRequest;
                step.Response__c = strResponse.abbreviate(131000);
            } else { 
                step = OrchUtils.setStepRecord( step , true , 'Error occurred: Callout results not received.' ); 	                
            }
            result.add(step);
        }
        return result;
    }
}