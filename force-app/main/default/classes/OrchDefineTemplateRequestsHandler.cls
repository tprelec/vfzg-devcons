@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class OrchDefineTemplateRequestsHandler implements CSPOFA.ExecutionHandler {
	List<sObject> data;
	List<CSPOFA__Orchestration_Step__c> steps;
	private Map<Id, Id> processToOrderIdMap;
	private Map<Id, List<Contracted_Products__c>> orderIdToContractedProductsPricePlan;
	private Map<Id, Map<Integer, List<Contracted_Products__c>>> orderIdToModelNumberToContractedProduct;
	private Map<Id, Map<Integer, List<Contracted_Products__c>>> orderIdToDurationToContractedProduct;

	/**
	 * Sets Contracted Product.Request New Template based on the Model Number
	 */
	public List<sObject> process(List<sObject> data) {
		this.data = data;
		this.steps = (List<CSPOFA__Orchestration_Step__c>) data;
		String errMsg;
		try {
			this.init();
			this.setUniqueContractedProducts();
		} catch (Exception ex) {
			errMsg = ex.getMessage() + ' ' + ex.getStackTraceString();
		}
		List<sObject> result = new List<sObject>();
		Boolean isError = String.isNotBlank(errMsg);
		for (CSPOFA__Orchestration_Step__c step : this.steps) {
			step = OrchUtils.setStepRecord(step, isError, (isError ? 'Error occurred: ' + errMsg : ''));
			result.add(step);
		}
		return result;
	}

	/**
	 * Initates Orchetrator steps and contracted products
	 */
	private void init() {
		// Get Orders Related to Orchestartor Steps
		Set<Id> processIds = new Set<Id>();
		for (CSPOFA__Orchestration_Step__c step : steps) {
			processIds.add(step.CSPOFA__Orchestration_Process__c);
		}
		OrchUtils.getProcessDetails(processIds);
		this.processToOrderIdMap = OrchUtils.mapOrderId;

		// Get Contracted Products
		this.getContractedProducts();
	}

	/**
	 * Gets Priceplan Contracted Products
	 */
	private void getContractedProducts() {
		Set<Id> orderIds = new Set<Id>(processToOrderIdMap.values());
		List<Contracted_Products__c> conProducts = [
			SELECT
				Id,
				Order__c,
				Model_Number__c,
				Line_Description__c,
				Price_Plan_Class__c,
				Deal_Type__c,
				Duration__c,
				Unify_Template_Description_Name__c,
				Product__r.Is_an_Addon_for_template_description__c,
				Product__r.Product_group__c,
				Product__r.Unify_Template_Description__c,
				product__r.Name,
				product__r.Description
			FROM Contracted_Products__c
			WHERE Order__c IN :orderIds
			//AND Product__r.Product_group__c = 'Priceplan'
		];

		this.orderIdToModelNumberToContractedProduct = this.create2LevelNestedMapList(conProducts, 'Order__c', 'Model_Number__c');
		this.orderIdToDurationToContractedProduct = this.create2LevelNestedMapList(conProducts, 'Order__c', 'Duration__c');
		//add records with only priceplans to list and create the map
		List<Contracted_Products__c> lstPricePlanCPs = new List<Contracted_Products__c>();
		for (Contracted_Products__c record : conProducts) {
			if (record.Product__r.Product_Group__c.equalsIgnoreCase('Priceplan')) {
				lstPricePlanCPs.add(record);
			}
		}
		this.orderIdToContractedProductsPricePlan = GeneralUtils.groupByIDField(lstPricePlanCPs, 'Order__c');
	}

	/**
	 * Sets unique Contracted Products based on Model Number
	 */
	private void setUniqueContractedProducts() {
		List<Contracted_Products__c> conProducts = new List<Contracted_Products__c>();
		for (Id orderId : this.orderIdToContractedProductsPricePlan.keySet()) {
			Set<Integer> modelNumbers = new Set<Integer>();
			Set<Integer> durationOfContratedProduct = new Set<Integer>();
			for (Contracted_Products__c conProduct : this.orderIdToContractedProductsPricePlan.get(orderId)) {
				if (!modelNumbers.contains((Integer) conProduct.Model_Number__c)) {
					conProduct.Request_New_Template__c = true;
					//set template description for this Contracted Product with group Priceplan iterating over the list of all CPs in the same ModelNumber
					conProduct.Unify_Template_Description_Name__c = this.setDescriptionTemplate(orderId, (Integer) conProduct.Model_Number__c);
				}
				if (!durationOfContratedProduct.contains((Integer) conProduct.Duration__c)) {
					conProduct.Request_New_Framework__c = true;
				}
				conProducts.add(conProduct);
				modelNumbers.add((Integer) conProduct.Model_Number__c);
				durationOfContratedProduct.add((Integer) conProduct.Duration__c);
			}
		}
		//itearate over conProducts to set
		if (!conProducts.isEmpty()) {
			update conProducts;
		}
	}

	private Map<Id, Map<Integer, List<SObject>>> create2LevelNestedMapList(List<SObject> records, String keyOnefield, String keyTwoField) {
		Map<Id, Map<Integer, List<sObject>>> mainMap = new Map<Id, Map<Integer, List<sObject>>>();
		for (SObject record : records) {
			Id fieldValueOne = (Id) record.get(keyOnefield);
			Integer fieldValueTwo = Integer.valueOf(record.get(keyTwoField));
			Map<Integer, List<SObject>> innerMap = mainMap.containsKey(fieldValueOne)
				? mainMap.get(fieldValueOne)
				: new Map<Integer, List<SObject>>();

			if (innerMap.containsKey(fieldValueTwo)) {
				innerMap.get(fieldValueTwo).add(record);
			} else {
				innerMap.put(fieldValueTwo, new List<SObject>{ record });
			}
			mainMap.put(fieldValueOne, innerMap);
		}
		return mainMap;
	}

	private String setDescriptionTemplate(Id keyOuter, Integer keyInner) {
		String strUnformated = System.Label.EMP_UnformatTemplateDescription;
		List<String> lstParamsTemplate = new String[3];
		lstParamsTemplate.set(0, '');
		lstParamsTemplate.set(1, '');
		lstParamsTemplate.set(2, '');

		for (Contracted_Products__c objContractedProd : this.orderIdToModelNumberToContractedProduct.get(keyOuter).get(keyInner)) {
			if (objContractedProd.Product__r.Product_Group__c.equalsIgnoreCase('Priceplan')) {
				lstParamsTemplate.set(
					0,
					String.isBlank(objContractedProd.Product__r.Unify_Template_Description__c)
						? objContractedProd.Line_Description__c
						: objContractedProd.Product__r.Unify_Template_Description__c
				);
				lstParamsTemplate.set(2, String.valueOf(objContractedProd.Duration__c));
				continue;
			}

			if (objContractedProd.Product__r.Is_an_Addon_for_template_description__c.equalsIgnoreCase('Yes')) {
				lstParamsTemplate.set(1, (objContractedProd.Product__r.Description).left(20));
				continue;
			}
		}
		//normalize parameter.
		String strAbb = String.isBlank(lstParamsTemplate.get(1)) ? lstParamsTemplate.get(0).left(44) : lstParamsTemplate.get(0).left(20);
		lstParamsTemplate.set(0, strAbb);
		return String.format(strUnformated, lstParamsTemplate)
			.replace(System.Label.EMP_ReplaceDescriptionChar, System.Label.EMP_ReplaceWithDescriptionChar);
	}
}
