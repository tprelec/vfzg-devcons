public without sharing class PP_SearchAccountController {
	private static final Integer SEARCH_ACCOUNTS_LIMIT = 15;

	public static String account_fields =
		' Id, Name, BillingPostalCode, Visiting_Postal_Code__c, ' + ' Visiting_Housenumber1__c, Visiting_Housenumber_suffix__c, KVK_number__c ';

	@AuraEnabled
	public static AuraActionResult createAccountSharing(String accountId) {
		try {
			if (GeneralUtils.currentUserIsPartnerUser()) {
				BanManagerData.createAccountSharing(accountId, UserInfo.getUserId());
			}
		} catch (Exception exc) {
			return new AuraActionResult(false, new AuraMessage(exc.getMessage(), AuraMessageSeverity.ERROR));
		}

		return new AuraActionResult(true);
	}

	@AuraEnabled
	public static SearchAccountResult searchAccount(SearchAccountParams params) {
		//System.debug(':::searchAccount; params = ' + params);
		Boolean banMatch = false;

		VerifyParamsResult verifyResult = verifyInput(params);

		if (!verifyResult.valid) {
			SearchAccountResult result = new SearchAccountResult();
			result.success = false;
			result.errorMessages = verifyResult.errorMessages;
			return result;
		}

		Boolean exactMatch = false;

		String queryLimitAndOffset = ' LIMIT ' + SEARCH_ACCOUNTS_LIMIT + ' OFFSET ' + params.searchOffset + ' ';

		List<Account> accounts = new List<Account>();
		String queryStart =
			'SELECT ' +
			account_fields +
			' FROM Account WHERE RecordTypeId = \'' +
			GeneralUtils.recordTypeMap.get('Account').get('VF_Account') +
			'\' AND ';

		// first search only on BAN Number
		if (String.isNotBlank(params.banNumber)) {
			Set<Id> acctIds = new Set<Id>();
			for (Ban__c b : [SELECT Id, Name, BAN_Number__c, Account__c FROM Ban__c WHERE BAN_Number__c = :params.banNumber]) {
				acctIds.add(b.Account__c);
			}

			String queryString = queryStart + ' Id IN :acctIds ';
			queryString += queryLimitAndOffset;

			accounts = Database.query(queryString);
			if (accounts.size() > 0) {
				exactMatch = true;
				banMatch = true;
			}
		}

		// second search only on KvK Number
		if (!exactMatch && String.isNotBlank(params.kvkNumber)) {
			String queryString = queryStart + ' KvK_Number__c = \'' + params.kvkNumber + '\' ';
			queryString += queryLimitAndOffset;

			accounts = Database.query(queryString);
			if (accounts.size() > 0) {
				exactMatch = true;
			} else {
				CreateAccountResult createAccountResult = createAccount(params);
				if (!createAccountResult.success) {
					SearchAccountResult result = new SearchAccountResult();
					result.success = false;
					result.errorMessages = createAccountResult.errorMessages;
					result.infoMessages = createAccountResult.infoMessages;
					//System.debug(':::searchAccount; result = ' + result);
					return result;
				}

				//System.debug('::: createdAccountId = ' + createAccountResult.newAccountId);
				return searchAccount(params);
				//accounts.add( createAccountResult.newAccount );
				//exactMatch = true;
			}
		}

		String searchString = params.accountName.replace(' ', '%');

		// third search on the other (or partial) criteria
		Set<Id> acctIds = new Set<Id>();
		if (!exactMatch) {
			String queryString = queryStart + 'Name LIKE \'%' + searchString + '%\' AND ( CreatedDate != null ';
			if (String.isNotBlank(params.banNumber)) {
				for (Ban__c b : [SELECT Id, Name, BAN_Number__c, Account__c FROM Ban__c WHERE BAN_Number__c = :params.banNumber]) {
					acctIds.add(b.Account__c);
				}
				queryString += ' OR Id IN :acctIds ';
			}

			if (String.isNotBlank(params.kvkNumber)) {
				queryString += ' OR KvK_Number__c LIKE \'%' + params.kvkNumber + '%\' ';
			}

			if (String.isNotBlank(params.zipCode)) {
				queryString += ' OR BillingPostalCode LIKE \'%' + params.zipCode + '%\' ';
				queryString += ' OR Visiting_Postal_Code__c LIKE \'%' + params.zipCode + '%\' ';
			}
			queryString += ') ';

			queryString += queryLimitAndOffset;

			accounts = Database.query(queryString);
		}

		SearchAccountResult result = new SearchAccountResult();
		result.success = true;

		Set<Id> accountIdsRetrieved = new Set<Id>();

		for (Account acc : accounts) {
			accountIdsRetrieved.add(acc.Id);

			SearchResult sr = new SearchResult();
			sr.account = acc;
			sr.score = 0;

			// assign scores
			if (String.isNotBlank(params.banNumber) && (acctIds.contains(acc.Id))) {
				sr.score += 10;
			}

			if (String.isNotBlank(params.kvkNumber) && acc.KVK_number__c != null && (acc.KVK_number__c.contains(params.kvkNumber))) {
				sr.score += 8;
			}

			// name always scores, as it is required and is checked in the query
			sr.score += 4;

			// billing addres can only be checked on postalcode, as housenumber is part of the street field..
			if (String.isNotBlank(params.zipCode) && acc.BillingPostalCode != null) {
				if (acc.BillingPostalCode.replace(' ', '').contains(params.zipCode.replace(' ', ''))) {
					sr.score += 4;
				}
			}

			if (String.isNotBlank(params.zipCode) && acc.Visiting_Postal_Code__c != null) {
				if (acc.Visiting_Postal_Code__c.replace(' ', '').contains(params.zipCode.replace(' ', ''))) {
					// increase the score and also enable housenumber scoring
					sr.score += 4;
					if (
						String.isNotBlank(params.houseNumber) &&
						acc.Visiting_Housenumber1__c != null &&
						String.valueOf(acc.Visiting_Housenumber1__c) == params.houseNumber
					) {
						sr.score += 2;
					}
				}
			}

			result.searchResults.add(sr);
		}

		result.moreAvailable = result.searchResults.size() == SEARCH_ACCOUNTS_LIMIT;

		result.sortAndFilter();

		return result;
	}

	private static VerifyParamsResult verifyInput(SearchAccountParams params) {
		VerifyParamsResult result = new VerifyParamsResult();
		result.valid = true;
		result.errorMessages = new List<String>();

		if (String.isNotBlank(params.zipCode)) {
			String zipWithoutSpaces = params.zipCode.replaceAll(' ', '');
			// ZIP should be 4- or 6-characters long
			if (zipWithoutSpaces.length() != 4 && zipWithoutSpaces.length() != 6) {
				result.valid = false;
				result.errorMessages.add(Label.ERROR_Zipcode_should_be_correct);
			}
		}

		if (String.isNotBlank(params.banNumber)) {
			if (params.banNumber.length() != 9) {
				result.valid = false;
				result.errorMessages.add(Label.ERROR_Ban_Number_9_chars);
			}
			if (!params.banNumber.isNumeric()) {
				result.valid = false;
				result.errorMessages.add(Label.ERROR_Ban_Number_numeric);
			}
		}

		if (String.isNotBlank(params.kvkNumber)) {
			// TODO: later it checks for exactly 8-digits long
			if (params.kvkNumber.length() > 8) {
				result.valid = false;
				result.errorMessages.add(Label.ERROR_KvK_8_chars);
			}
			if (!params.kvkNumber.isNumeric()) {
				result.valid = false;
				result.errorMessages.add(Label.ERROR_KvK_numeric);
			}
		}

		if (params.searchOffset < 0 || params.searchOffset > 100) {
			result.valid = false;
			result.errorMessages.add(Label.pp_Invalid_Search_Request);
		}

		return result;
	}

	public class UpdateAccountResult {
		public Id accountId { get; set; }

		public Boolean success { get; set; }

		public List<String> errorMessages { get; set; }

		public List<String> infoMessages { get; set; }
	}

	public class CreateAccountResult {
		public Id newAccountId { get; set; }

		public Boolean success { get; set; }

		public List<String> errorMessages { get; set; }

		public List<String> infoMessages { get; set; }
	}

	public static CreateAccountResult createAccount(SearchAccountParams params) {
		CreateAccountResult result = new CreateAccountResult();
		result.infoMessages = new List<String>();
		result.errorMessages = new List<String>();

		if (Test.isRunningTest()) {
			result.success = false;
			result.errorMessages.add('TODO: HttpMock for OlbicoService???');
			return result;
		}

		OlbicoServiceJSON jService = new OlbicoServiceJSON();

		jService.setRequestRestJson(params.kvkNumber);
		jService.makeReqestRestJson(params.kvkNumber);

		if (jService.getAccId() == null) {
			result.success = false;
			result.errorMessages.add(jService.getResponse());
			return result;
		}

		result.success = true;
		result.newAccountId = jService.getAccId();
		return result;
	}

	public static UpdateAccountResult updateAccount(SearchAccountParams params) {
		UpdateAccountResult result = new UpdateAccountResult();
		result.infoMessages = new List<String>();
		result.errorMessages = new List<String>();

		if (Test.isRunningTest()) {
			result.success = false;
			result.errorMessages.add('TODO: HttpMock for OlbicoService???');
			return result;
		}

		OlbicoServiceJSON jService = new OlbicoServiceJSON();

		jService.setRequestRestJson(params.kvkNumber);
		jService.makeReqestRestJsonUpdate(params.kvkNumber, params.accountName);

		if (jService.getAccId() == null) {
			result.success = false;
			result.errorMessages.add(jService.getResponse());
			return result;
		}

		result.success = true;
		result.accountId = jService.getAccId();
		return result;
	}

	@AuraEnabled
	public static SearchAccountResult searchAccountsServer(String input) {
		SearchAccountParams params = cleanSearchAccountParams(deserializeSearchAccountParams(input));

		return searchAccount(params);
	}

	private static SearchAccountParams deserializeSearchAccountParams(String stringInput) {
		return (SearchAccountParams) JSON.deserialize(stringInput, SearchAccountParams.class);
	}

	private static SearchAccountParams cleanSearchAccountParams(SearchAccountParams params) {
		params.accountName = String.escapeSingleQuotes(avoidNull(params.accountName));
		params.kvkNumber = String.escapeSingleQuotes(avoidNull(params.kvkNumber));
		params.banNumber = String.escapeSingleQuotes(avoidNull(params.banNumber));
		params.zipCode = String.escapeSingleQuotes(avoidNull(params.zipCode));
		params.houseNumber = String.escapeSingleQuotes(avoidNull(params.houseNumber));

		params.searchOffset = params.searchOffset == null ? 0 : params.searchOffset;

		return params;
	}

	private static String avoidNull(String str) {
		return str == null ? '' : str;
	}

	public class VerifyParamsResult {
		Boolean valid { get; set; }
		List<String> errorMessages { get; set; }
	}

	public class SearchAccountParams {
		@AuraEnabled
		public String accountName { get; set; }

		@AuraEnabled
		public String banNumber { get; set; }
		@AuraEnabled
		public String kvkNumber { get; set; }

		@AuraEnabled
		public String zipCode { get; set; }

		@AuraEnabled
		public String houseNumber { get; set; }

		@AuraEnabled
		public Integer searchOffset { get; set; }
	}

	public class SearchAccountResult {
		@AuraEnabled
		public List<SearchResult> searchResults {
			get {
				if (searchResults == null) {
					searchResults = new List<SearchResult>();
					return searchResults;
				}

				return searchResults;
			}
			set;
		}

		@AuraEnabled
		public Boolean success { get; set; }

		@AuraEnabled
		public Boolean moreAvailable { get; set; }

		@AuraEnabled
		public List<String> errorMessages { get; set; }

		@AuraEnabled
		public List<String> infoMessages { get; set; }

		@AuraEnabled
		public Account account { get; set; }

		public void sortAndFilter() {
			this.searchResults.sort();

			List<SearchResult> filteredResult = new List<SearchResult>();
			Integer c = 0;

			for (SearchResult searchResult : searchResults) {
				if (searchResult.score > 0) {
					filteredResult.add(searchResult);
				}
			}
			this.searchResults = filteredResult;
		}
	}

	public class SearchResult implements Comparable {
		@AuraEnabled
		public Account account { get; set; }

		@AuraEnabled
		public Integer score { get; set; }

		// Implement the compareTo() method to enable sorting
		public Integer compareTo(Object compareTo) {
			SearchResult compareToSR = (SearchResult) compareTo;
			if (score == compareToSR.score) {
				return 0;
			}
			if (score > compareToSR.score) {
				return -1;
			}
			return 1;
		}
	}

	@AuraEnabled(cacheable=true)
	public static List<Account> getRecentlyViewedAccounts() {
		return [
			SELECT Id, Name, Phone, LG_VisitAddress__c, KVK_number__c, Location_Status__c
			FROM Account
			WHERE LastViewedDate != NULL AND KVK_number__c != ''
			ORDER BY LastViewedDate DESC
			LIMIT 5
		];
	}

	public static List<Account> searchAccountByKvkNumber(String kvk) {
		return [SELECT Id, Name, Phone, LG_VisitAddress__c, KVK_number__c, Location_Status__c FROM Account WHERE KVK_number__c = :kvk LIMIT 1];
	}

	@AuraEnabled
	public static SearchAccountResult searchAccountByKvk(String kvk) {
		List<Account> accountsByKvk = searchAccountByKvkNumber(kvk);

		if (!accountsByKvk.isEmpty()) {
			SearchAccountParams params = new SearchAccountParams();
			params.kvkNumber = kvk;
			params.accountName = accountsByKvk[0].Id;
			UpdateAccountResult updateAccountResult = updateAccount(params);
			if (!updateAccountResult.success) {
				SearchAccountResult result = new SearchAccountResult();
				result.success = false;
				result.errorMessages = updateAccountResult.errorMessages;
				result.infoMessages = updateAccountResult.infoMessages;
				result.account = searchAccountByKvkNumber(kvk)[0];
				return result;
			} else {
				SearchAccountResult result = new SearchAccountResult();
				result.success = true;
				result.errorMessages = null;
				result.infoMessages = null;
				result.account = searchAccountByKvkNumber(kvk)[0];
				return result;
			}
		} else {
			SearchAccountParams params = new SearchAccountParams();
			params.kvkNumber = kvk;
			CreateAccountResult createAccountResult = createAccount(params);
			if (!createAccountResult.success) {
				SearchAccountResult result = new SearchAccountResult();
				result.success = false;
				result.errorMessages = createAccountResult.errorMessages;
				result.infoMessages = createAccountResult.infoMessages;
				result.account = null;

				return result;
			}

			return searchAccountByKvk(kvk);
		}
	}

	@AuraEnabled(cacheable=true)
	public static List<BPP_Journey> getBppJourneys(String accountId) {
		List<BPP_Journey> bppJourneys = new List<BPP_Journey>();

		List<BPP_Journey__mdt> bppJourneysMtd = [
			SELECT Id, DeveloperName, MasterLabel, Active__c, HandlerName__c, Next_Step__c, Sort_Order__c
			FROM BPP_Journey__mdt
			WHERE Active__c = TRUE
			ORDER BY Sort_Order__c ASC
		];

		for (BPP_Journey__mdt bppJourneyMtd : bppJourneysMtd) {
			try {
				String handlerName = bppJourneyMtd.HandlerName__c;
				if (!String.isEmpty(handlerName)) {
					if (!IJourneyHandler.checkVisibility(handlerName, accountId)) {
						continue;
					}
				}

				BPP_Journey bppJourney = new BPP_Journey();
				bppJourney.id = bppJourneyMtd.Id;
				bppJourney.label = StringUtils.translateLabel(bppJourneyMtd.MasterLabel, null).substringBetween('"', '"');
				bppJourney.nextStep = bppJourneyMtd.Next_Step__c;
				bppJourney.handlerName = handlerName;
				bppJourneys.add(bppJourney);
			} catch (Exception e) {
				LoggerService.log(e);
			}
		}

		return bppJourneys;
	}

	public class BPP_Journey {
		@AuraEnabled
		public String id { get; set; }

		@AuraEnabled
		public String label { get; set; }

		@AuraEnabled
		public String nextStep { get; set; }

		@AuraEnabled
		public String handlerName { get; set; }
	}
}
