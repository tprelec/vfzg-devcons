public class UnicaBaseAllocationBatchExecuteCntrlr{
  
  public UnicaBaseAllocationBatchExecuteCntrlr(ApexPages.StandardSetController ignored) {

  }

  public PageReference callBatch(){
    if([SELECT Id FROM Unica_Base_Allocation__c].size() > 0){
      UnicaBaseAllocationBatch controller = new UnicaBaseAllocationBatch();
      database.executebatch(controller);
    }
    Schema.DescribeSObjectResult prefix = Unica_Base_Allocation__c.SObjectType.getDescribe();
    return new pageReference('/' + prefix.getKeyPrefix());
  }
}