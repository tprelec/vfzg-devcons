@isTest
public with sharing class TestDocumentDefinitionTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Document Definition';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'PBDD-13-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		csclm__Document_Definition__c testDd = new csclm__Document_Definition__c();
		testDd.csclm__Linked_Object__c = 'cscfga__Product_Basket__c';
		insert testDd;

		csclm__Document_Definition__c queryRecord = [SELECT Id, ExternalID__c FROM csclm__Document_Definition__c LIMIT 1];
		System.assert(queryRecord.ExternalID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
