/*
 * @author Rahul Sharma
 *
 * @description
 *     Controller for billingInformation LWC component
 *     the component works for Opportunity and Product basket, CS order objects
 */
// PMD.ExcessivePublicCount: contains business logic hence supress is required
// PMD.CyclomaticComplexity: methods are broken down to smaller functions, but complexity is higher because of business logic
@SuppressWarnings('PMD.ExcessivePublicCount, PMD.CyclomaticComplexity')
public without sharing class BillingInformationController {
	public static final String BAN_NAME_REQUEST_NEW_BAN = 'Requested New Unify BAN';
	public static final String BAN_STATUS_REQUESTED = 'Requested';
	public static final String REQUEST_NEW_BAN = 'Requested New Unify BAN';

	@AuraEnabled
	public static LightningResponse getData(Id recordId) {
		try {
			BillingInformation billingInformation;

			Ban__c ban = new Ban__c();
			Financial_Account__c financialAccount = new Financial_Account__c();
			Billing_Arrangement__c billingArrangement = new Billing_Arrangement__c();
			if (recordId?.getSobjectType() == Schema.cscfga__Product_Basket__c.SObjectType) {
				cscfga__Product_Basket__c basket = [
					SELECT
						Id,
						csbb__Account__c,
						cscfga__Opportunity__c,
						cscfga__Opportunity__r.BAN__c,
						cscfga__Opportunity__r.BAN__r.Name,
						cscfga__Opportunity__r.BAN__r.Account__c,
						cscfga__Opportunity__r.Financial_Account__c,
						cscfga__Opportunity__r.Financial_Account__r.Name,
						cscfga__Opportunity__r.Billing_Arrangement__c,
						cscfga__Opportunity__r.Billing_Arrangement__r.Name
					FROM cscfga__Product_Basket__c
					WHERE Id = :recordId
				];

				billingInformation = new BillingInformation(basket);

				ban = new Ban__c(Account__c = basket.csbb__Account__c);
				financialAccount = new Financial_Account__c(BAN__c = basket.cscfga__Opportunity__r.BAN__c);
				billingArrangement = new Billing_Arrangement__c(Financial_Account__c = basket.cscfga__Opportunity__r.Financial_Account__c);
			} else if (recordId?.getSobjectType() == Schema.csord__Order__c.SObjectType) {
				csord__Order__c basket = [
					SELECT
						Id,
						csord__Account__c,
						csordtelcoa__Opportunity__c,
						BAN_Information__c,
						BAN_Information__r.Name,
						BAN_Information__r.Account__c,
						Financial_Account__c,
						Financial_Account__r.Name,
						Billing_Arrangement__c,
						Billing_Arrangement__r.Name
					FROM csord__Order__c
					WHERE Id = :recordId
				];

				billingInformation = new BillingInformation(basket);

				ban = new Ban__c(Account__c = basket.csord__Account__c);
				financialAccount = new Financial_Account__c(BAN__c = basket.BAN_Information__c);
				billingArrangement = new Billing_Arrangement__c(Financial_Account__c = basket.Financial_Account__c);
			} else if (recordId?.getSobjectType() == Schema.Opportunity.SObjectType) {
				Opportunity opp = [
					SELECT
						Id,
						AccountId,
						Account.Visiting_street__c,
						Account.Visiting_Postal_Code__c,
						Account.Visiting_Country__c,
						Account.Visiting_Housenumber1__c,
						Account.Visiting_Housenumber_Suffix__c,
						Account.Visiting_City__c,
						BAN__c,
						BAN__r.Name,
						BAN__r.Account__c,
						Financial_Account__c,
						Financial_Account__r.Name,
						Billing_Arrangement__c,
						Billing_Arrangement__r.Name
					FROM Opportunity
					WHERE Id = :recordId
				];

				billingInformation = new BillingInformation(opp);

				ban = new Ban__c(Account__c = opp.AccountId);
				financialAccount = new Financial_Account__c(BAN__c = opp.BAN__c);
				billingArrangement = new Billing_Arrangement__c(
					Financial_Account__c = opp.Financial_Account__c,
					Billing_Street__c = opp.Account.Visiting_street__c,
					Billing_Postal_Code__c = opp.Account.Visiting_Postal_Code__c,
					Billing_Country__c = opp.Account.Visiting_Country__c,
					Billing_City__c = opp.Account.Visiting_City__c
				);
				if (opp.Account.Visiting_Housenumber_Suffix__c != null) {
					billingArrangement.Billing_Housenumber__c =
						String.valueOf(opp.Account.Visiting_Housenumber1__c) +
						' ' +
						opp.Account.Visiting_Housenumber_Suffix__c;
				} else {
					billingArrangement.Billing_Housenumber__c = String.valueOf(opp.Account.Visiting_Housenumber1__c);
				}
			}
			if (billingInformation == null) {
				throw new BillingInformationException(System.label.BillingInformation_ObjectNotSupported);
			}

			billingInformation.banCreateNewConfigFields = getBanConfiguration(ban);

			billingInformation.financialAccountCreateNewConfigFields = getFinancialAccountConfiguration(financialAccount);

			billingInformation.billingArrangementCreateNewConfigFields = getBillingArrangementConfiguration(billingArrangement);

			billingInformation.banFieldset = getBanFieldset();
			billingInformation.financialAccountFieldset = getFinancialAccountFieldset();
			billingInformation.billingArrangementFieldset = getBillingArrangementFieldset();

			return new LightningResponse().setBody(billingInformation);
		} catch (Exception objEx) {
			return setError(null, objEx.getMessage());
		}
	}

	@AuraEnabled
	public static List<Ban__c> getBanForAccountId(String accId) {
		try {
			List<Ban__c> bans = [
				SELECT Id, Name, BAN_Name__c, BAN_Status__c, Unify_Customer_Type__c, Unify_Customer_SubType__c, OwnerId
				FROM Ban__c
				WHERE Account__c = :accId AND BAN_Status__c != 'Closed'
			];
			return bans;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static List<Financial_Account__c> getFAForBanId(String banId) {
		try {
			List<Financial_Account__c> finAccounts = [
				SELECT Id, Name, Financial_Contact__c, Status__c, Financial_Contact__r.Name
				FROM Financial_Account__c
				WHERE BAN__c = :banId AND Status__c = 'Open'
			];
			return finAccounts;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static List<Billing_Arrangement__c> getBillingArrangementForBanId(String finAccountId) {
		try {
			List<Billing_Arrangement__c> billingArrangements = [
				SELECT
					Id,
					Name,
					Billing_Arrangement_Alias__c,
					Payment_method__c,
					Bank_Account_Name__c,
					Bank_Account_Number__c,
					Billing_Street__c,
					Billing_Housenumber__c,
					Billing_Postal_Code__c,
					Billing_City__c,
					Billing_Country__c,
					Bill_Format__c,
					Bill_Production_Indicator__c,
					Status__c,
					Unify_Ref_Id__c
				FROM Billing_Arrangement__c
				WHERE Financial_Account__c = :finAccountId AND Status__c = 'Open'
			];
			return billingArrangements;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static LightningResponse save(SaveBillingInformation saveData) {
		Savepoint savepoint = Database.setSavePoint();
		try {
			// update ban
			/*
			Map<String, Object> mapBanFields = saveData.banUpdatedFields;
			if (saveData.banId != null && mapBanFields?.size() > 0) {
				Ban__c ban = new Ban__c(Id = saveData.banId);
				for (String fieldName : mapBanFields.keySet()) {
					ban.put(fieldName, mapBanFields.get(fieldName));
				}
				update ban;
			}
			// update ban
			Map<String, Object> mapBarFields = saveData.barUpdatedFields;
			if (saveData.billingArrangementId != null && mapBarFields?.size() > 0) {
				Billing_Arrangement__c billingArrangement = new Billing_Arrangement__c(
					Id = saveData.billingArrangementId
				);
				for (String fieldName : mapBarFields.keySet()) {
					billingArrangement.put(fieldName, mapBarFields.get(fieldName));
				}
				update billingArrangement;
			}
			*/
			updateBan(saveData);
			updateBillingArrangement(saveData);
			if (saveData.recordId?.getSobjectType() == Schema.Opportunity.SObjectType) {
				Opportunity opp = new Opportunity(
					Id = saveData.recordId,
					BAN__c = saveData.banId,
					Financial_Account__c = saveData.financialAccountId,
					Billing_Arrangement__c = saveData.billingArrangementId
				);
				update opp;
				return new LightningResponse().setSuccess('Billing information is saved successfully to Opportunity!');
			} else if (saveData.recordId?.getSobjectType() == Schema.cscfga__Product_Basket__c.SObjectType) {
				cscfga__Product_Basket__c basket = [SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :saveData.recordId];
				Opportunity opp = new Opportunity(
					Id = basket.cscfga__Opportunity__c,
					BAN__c = saveData.banId,
					Financial_Account__c = saveData.financialAccountId,
					Billing_Arrangement__c = saveData.billingArrangementId
				);
				update opp;

				List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();
				for (cscfga__Product_Configuration__c pc : [
					SELECT Id
					FROM cscfga__Product_Configuration__c
					WHERE cscfga__Product_Basket__c = :saveData.recordId
				]) {
					productConfigurations.add(
						new cscfga__Product_Configuration__c(
							Id = pc.Id,
							BAN_Information__c = saveData.banId,
							Financial_Account__c = saveData.financialAccountId,
							Billing_Arrangement__c = saveData.billingArrangementId
						)
					);
				}
				update productConfigurations;

				return new LightningResponse().setSuccess('Billing information is saved successfully to basket!');
			} else {
				csord__Order__c order = new csord__Order__c(
					Id = saveData.recordId,
					BAN_Information__c = saveData.banId,
					Financial_Account__c = saveData.financialAccountId,
					Billing_Arrangement__c = saveData.billingArrangementId
				);
				update order;

				return new LightningResponse().setSuccess('Billing information is saved successfully to order!');
			}
		} catch (DMLException objEx) {
			return setError(savepoint, objEx.getDmlMessage(0));
		} catch (Exception objEx) {
			return setError(savepoint, objEx.getMessage());
		}
	}

	private static void updateBan(SaveBillingInformation saveData) {
		Map<String, Object> mapBanFields = saveData.banUpdatedFields;
		if (saveData.banId != null && mapBanFields?.size() > 0) {
			Ban__c ban = new Ban__c(Id = saveData.banId);
			for (String fieldName : mapBanFields.keySet()) {
				ban.put(fieldName, mapBanFields.get(fieldName));
			}
			update ban;
		}
	}
	private static void updateBillingArrangement(SaveBillingInformation saveData) {
		Map<String, Object> mapBarFields = saveData.barUpdatedFields;
		if (saveData.billingArrangementId != null && mapBarFields?.size() > 0) {
			Billing_Arrangement__c billingArrangement = new Billing_Arrangement__c(Id = saveData.billingArrangementId);
			for (String fieldName : mapBarFields.keySet()) {
				billingArrangement.put(fieldName, mapBarFields.get(fieldName));
			}
			update billingArrangement;
		}
	}

	@AuraEnabled
	public static Map<String, String> requestNew(String recordId, String currentBanFieldName, String accountId) {
		try {
			Account acc = [SELECT Name FROM Account WHERE Id = :accountId];

			// create Unify BAN placeholder
			Ban__c banToInsert = new Ban__c(
				Name = REQUEST_NEW_BAN + ' ' + system.now(),
				Account__c = accountId,
				BAN_Status__c = BAN_STATUS_REQUESTED,
				BAN_Name__c = acc.Name
			);
			//Portal users don't have access to change the type or subtype fields so it needs to be prepopulatd for them, otherwise the order validation error will fire
			if (GeneralUtils.currentUserIsPartnerUser()) {
				banToInsert.Unify_Customer_Type__c = 'C';
				banToInsert.Unify_Customer_SubType__c = 'A';
			}
			insert banToInsert;
			Map<String, String> mapResponse = new Map<String, String>();

			mapResponse.put('Id', banToInsert.Id);
			mapResponse.put('Name', banToInsert.Name);

			BanVerifierController.save(recordId, banToInsert.Id, currentBanFieldName, accountId);

			return mapResponse;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	public class BillingInformationException extends Exception {
	}

	private static List<FieldSet> getBanFieldset() {
		return new List<FieldSet>{
			new FieldSet().setName('BAN_Name__c'),
			new FieldSet().setName('Unify_Customer_Type__c'),
			new FieldSet().setName('Unify_Customer_SubType__c')
		};
	}

	private static List<FieldSet> getFinancialAccountFieldset() {
		return new List<FieldSet>{ new FieldSet().setName('Financial_Contact__c'), new FieldSet().setName('Status__c') };
	}

	private static List<FieldSet> getBillingArrangementFieldset() {
		return new List<FieldSet>{
			new FieldSet().setName('Billing_Arrangement_Alias__c').setEditAllowed(true),
			new FieldSet().setName('Payment_method__c').setEditAllowed(true),
			new FieldSet().setName('Bank_Account_Name__c').setEditAllowed(true),
			new FieldSet().setName('Bank_Account_Number__c').setEditAllowed(true),
			new FieldSet().setName('Billing_Street__c').setEditAllowed(true),
			new FieldSet().setName('Billing_Housenumber__c').setEditAllowed(true),
			new FieldSet().setName('Billing_Postal_Code__c').setEditAllowed(true),
			new FieldSet().setName('Billing_City__c').setEditAllowed(true),
			new FieldSet().setName('Billing_Country__c').setEditAllowed(true),
			new FieldSet().setName('Bill_Format__c').setEditAllowed(true),
			new FieldSet().setName('Bill_Production_Indicator__c').setEditAllowed(true),
			new FieldSet().setName('Status__c').setEditAllowed(true),
			new FieldSet().setName('Unify_Ref_Id__c')
		};
	}

	private static List<LookupController.LookupCreateNewConfiguration> getBanConfiguration(Ban__c ban) {
		String banName = BAN_NAME_REQUEST_NEW_BAN + ' ' + system.now();
		List<LookupController.LookupCreateNewConfiguration> configs = new List<LookupController.LookupCreateNewConfiguration>{
			new LookupController.LookupCreateNewConfiguration().setName('Account__c').setValue(ban.Account__c).setDisabled(true),
			new LookupController.LookupCreateNewConfiguration().setName('BAN_Status__c').setValue(BAN_STATUS_REQUESTED).setDisabled(true),
			new LookupController.LookupCreateNewConfiguration().setName('Name').setValue(banName).setDisabled(true),
			new LookupController.LookupCreateNewConfiguration().setName('BAN_Name__c').setValue(banName).setDisabled(true),
			new LookupController.LookupCreateNewConfiguration().setName('Unify_Customer_Type__c').setValue(ban.Unify_Customer_Type__c),
			new LookupController.LookupCreateNewConfiguration().setName('Unify_Customer_SubType__c').setValue(ban.Unify_Customer_SubType__c)
		};
		return configs;
	}

	private static List<LookupController.LookupCreateNewConfiguration> getFinancialAccountConfiguration(Financial_Account__c financialAccount) {
		List<LookupController.LookupCreateNewConfiguration> configs = new List<LookupController.LookupCreateNewConfiguration>{
			new LookupController.LookupCreateNewConfiguration().setName('BAN__c').setValue(financialAccount.Ban__c).setDisabled(true),
			new LookupController.LookupCreateNewConfiguration()
				.setName('Financial_Contact__c')
				.setValue(financialAccount.Financial_Contact__c)
				.setRequired(true)
		};
		return configs;
	}

	private static List<LookupController.LookupCreateNewConfiguration> getBillingArrangementConfiguration(Billing_Arrangement__c billingArrangement) {
		List<LookupController.LookupCreateNewConfiguration> configs = new List<LookupController.LookupCreateNewConfiguration>{
			new LookupController.LookupCreateNewConfiguration()
				.setName('Financial_Account__c')
				.setValue(billingArrangement.Financial_Account__c)
				.setDisabled(true),
			new LookupController.LookupCreateNewConfiguration()
				.setName('Billing_Arrangement_Alias__c')
				.setValue(billingArrangement.Billing_Arrangement_Alias__c),
			new LookupController.LookupCreateNewConfiguration().setName('Payment_method__c').setValue(billingArrangement.Payment_method__c),
			new LookupController.LookupCreateNewConfiguration().setName('Bank_Account_Name__c').setValue(billingArrangement.Bank_Account_Name__c),
			new LookupController.LookupCreateNewConfiguration().setName('Bank_Account_Number__c').setValue(billingArrangement.Bank_Account_Number__c),
			new LookupController.LookupCreateNewConfiguration().setName('Billing_Street__c').setValue(billingArrangement.Billing_Street__c),
			new LookupController.LookupCreateNewConfiguration().setName('Billing_Housenumber__c').setValue(billingArrangement.Billing_Housenumber__c),
			new LookupController.LookupCreateNewConfiguration().setName('Billing_Postal_Code__c').setValue(billingArrangement.Billing_Postal_Code__c),
			new LookupController.LookupCreateNewConfiguration().setName('Billing_City__c').setValue(billingArrangement.Billing_City__c),
			new LookupController.LookupCreateNewConfiguration().setName('Billing_Country__c').setValue(billingArrangement.Billing_Country__c),
			new LookupController.LookupCreateNewConfiguration().setName('Bill_Format__c').setValue(billingArrangement.Bill_Format__c),
			new LookupController.LookupCreateNewConfiguration()
				.setName('Bill_Production_Indicator__c')
				.setValue(billingArrangement.Bill_Production_Indicator__c),
			new LookupController.LookupCreateNewConfiguration().setName('Status__c').setValue(billingArrangement.Status__c)
		};
		return configs;
	}

	@TestVisible
	private static LightningResponse setError(Savepoint savepoint, String message) {
		if (savepoint != null) {
			Database.rollback(savepoint);
		}
		return new LightningResponse().setError(message);
	}

	@SuppressWarnings('PMD.ExcessivePublicCount') // because of business logic we have many methods
	public class BillingInformation {
		@AuraEnabled
		public Id opportunityId { get; set; }
		@AuraEnabled
		public Id accountId { get; set; }
		@AuraEnabled
		public Id banId { get; set; }
		@AuraEnabled
		public String banName { get; set; }
		@AuraEnabled
		public Id financialAccountId { get; set; }
		@AuraEnabled
		public String financialAccountName { get; set; }
		@AuraEnabled
		public Id billingArrangementId { get; set; }
		@AuraEnabled
		public String billingArrangementName { get; set; }

		@AuraEnabled
		public List<LookupController.LookupCreateNewConfiguration> banCreateNewConfigFields { get; set; }

		@AuraEnabled
		public List<LookupController.LookupCreateNewConfiguration> financialAccountCreateNewConfigFields { get; set; }

		@AuraEnabled
		public List<LookupController.LookupCreateNewConfiguration> billingArrangementCreateNewConfigFields { get; set; }
		@AuraEnabled
		public List<FieldSet> banFieldset { get; set; }
		@AuraEnabled
		public List<FieldSet> financialAccountFieldset { get; set; }
		@AuraEnabled
		public List<FieldSet> billingArrangementFieldset { get; set; }

		public BillingInformation(cscfga__Product_Basket__c basket) {
			this.accountId = basket.csbb__Account__c;
			this.opportunityId = basket.cscfga__Opportunity__c;

			if (String.isNotEmpty(basket.cscfga__Opportunity__r.BAN__c)) {
				this.banId = basket.cscfga__Opportunity__r.BAN__c;
				this.banName = basket.cscfga__Opportunity__r.BAN__r.Name;
			} else {
				this.banId = basket.cscfga__Opportunity__r?.BAN__c;
			}
			if (String.isNotEmpty(basket.cscfga__Opportunity__r.Financial_Account__c)) {
				this.financialAccountId = basket.cscfga__Opportunity__r.Financial_Account__c;
				this.financialAccountName = basket.cscfga__Opportunity__r.Financial_Account__r.Name;
			}
			if (String.isNotEmpty(basket.cscfga__Opportunity__r.Billing_Arrangement__c)) {
				this.billingArrangementId = basket.cscfga__Opportunity__r.Billing_Arrangement__c;
				this.billingArrangementName = basket.cscfga__Opportunity__r.Billing_Arrangement__r.Name;
			}
		}
		// without empty constructor sometimes LWC doesn't work properly
		@SuppressWarnings('PMD.EmptyStatementBlock')
		public BillingInformation() {
		}

		public BillingInformation(csord__Order__c order) {
			this.accountId = order.csord__Account__c;
			this.opportunityId = order.csordtelcoa__Opportunity__c;

			if (String.isNotEmpty(order.BAN_Information__c)) {
				this.banId = order.BAN_Information__c;
				this.banName = order.BAN_Information__r.Name;
			}
			if (String.isNotEmpty(order.Financial_Account__c)) {
				this.financialAccountId = order.Financial_Account__c;
				this.financialAccountName = order.Financial_Account__r.Name;
			}
			if (String.isNotEmpty(order.Billing_Arrangement__c)) {
				this.billingArrangementId = order.Billing_Arrangement__c;
				this.billingArrangementName = order.Billing_Arrangement__r.Name;
			}
		}

		public BillingInformation(Opportunity opp) {
			this.accountId = opp.AccountId;
			this.opportunityId = opp.Id;

			if (String.isNotEmpty(opp.BAN__c)) {
				this.banId = opp.BAN__c;
				this.banName = opp.BAN__r.Name;
			}
			if (String.isNotEmpty(opp.Financial_Account__c)) {
				this.financialAccountId = opp.Financial_Account__c;
				this.financialAccountName = opp.Financial_Account__r.Name;
			}
			if (String.isNotEmpty(opp.Billing_Arrangement__c)) {
				this.billingArrangementId = opp.Billing_Arrangement__c;
				this.billingArrangementName = opp.Billing_Arrangement__r.Name;
			}
		}
	}

	public class FieldSet {
		@AuraEnabled
		public String name { get; set; }
		@AuraEnabled
		public Boolean isEditAllowed { get; set; }

		public FieldSet() {
			this.isEditAllowed = false;
		}

		public FieldSet setName(String name) {
			this.name = name;
			return this;
		}

		public FieldSet setEditAllowed(Boolean isEditAllowed) {
			this.isEditAllowed = isEditAllowed;
			return this;
		}
	}

	public class SaveBillingInformation {
		@AuraEnabled
		public Id recordId { get; set; }
		@AuraEnabled
		public Id banId { get; set; }
		@AuraEnabled
		public Map<String, Object> banUpdatedFields { get; set; }
		@AuraEnabled
		public Map<String, Object> barUpdatedFields { get; set; }
		@AuraEnabled
		public Id financialAccountId { get; set; }
		@AuraEnabled
		public Id billingArrangementId { get; set; }

		// without empty constructor sometimes LWC doesn't work properly
		@SuppressWarnings('PMD.EmptyStatementBlock')
		public SaveBillingInformation() {
		}

		@SuppressWarnings('PMD.ExcessiveParameterList')
		public SaveBillingInformation(
			Id recordId,
			Id banId,
			Map<String, Object> banUpdatedFields,
			Map<String, Object> barUpdatedFields,
			Id financialAccountId,
			Id billingArrangementId
		) {
			this.recordId = recordId;
			this.banId = banId;
			this.banUpdatedFields = banUpdatedFields;
			this.barUpdatedFields = barUpdatedFields;
			this.financialAccountId = financialAccountId;
			this.billingArrangementId = billingArrangementId;
		}
	}

	@AuraEnabled(cacheable=true)
	public static List<BAN__c> getRecord(String recordId) {
		try {
			List<Ban__c> ban = [
				SELECT Id, BAN_Name__c, Unify_Customer_Type__c, Unify_Customer_SubType__c, BAN_Status__c
				FROM Ban__c
				WHERE Id = :recordId
			];
			return ban;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static Map<String, String> getBANUnifyCustomerTypeOptions() {
		try {
			Schema.DescribeFieldResult fieldResult = Ban__c.Unify_Customer_Type__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

			Map<String, String> returnMap = new Map<String, String>();

			for (Schema.PicklistEntry f : ple) {
				if (f.isActive()) {
					returnMap.put(f.getLabel(), f.getValue());
				}
			}
			return returnMap;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static Map<String, String> getBANUnifyCustomerSubTypeOptions() {
		try {
			Schema.DescribeFieldResult fieldResult = Ban__c.Unify_Customer_SubType__c.getDescribe();
			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			Map<String, String> returnMap = new Map<String, String>();

			for (Schema.PicklistEntry f : ple) {
				if (f.isActive()) {
					returnMap.put(f.getLabel(), f.getValue());
				}
			}
			return returnMap;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static Map<String, String> getActivePicklistValues(String field) {
		try {
			List<String> splitString = field.split('\\.');
			String objectName = splitString[0];
			String fieldName = splitString[1];
			// get the SObjectType
			Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
			// get the fields on the object
			Map<String, SObjectField> fieldMap = objectType.getDescribe().fields.getMap();
			// The key to the map is the api name of the field
			Schema.SobjectField theField = fieldMap.get(fieldName);
			Schema.DescribeFieldResult fieldResult = theField.getDescribe();

			List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
			Map<String, String> returnMap = new Map<String, String>();

			for (Schema.PicklistEntry f : ple) {
				if (f.isActive()) {
					returnMap.put(f.getLabel(), f.getValue());
				}
			}
			return returnMap;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	//provides dependant picklist filtered on master picklist values
	//input format example Ban__c.Unify_Customer_SubType__c
	@AuraEnabled
	@SuppressWarnings('PMD.CyclomaticComplexity')
	public static Map<String, List<String>> getDependentPicklistValues(String field) {
		List<String> splitString = field.split('\\.');
		String objectName = splitString[0];
		String fieldName = splitString[1];
		// get the SObjectType
		Schema.SObjectType objectType = Schema.getGlobalDescribe().get(objectName);
		// get the fields on the object
		Map<String, SObjectField> fieldMap = objectType.getDescribe().fields.getMap();
		// The key to the map is the api name of the field
		Schema.SobjectField theField = fieldMap.get(fieldName);

		Schema.DescribeFieldResult depend = theField.getDescribe();
		Schema.sObjectField controlToken = depend.getController();
		if (controlToken == null) {
			return new Map<String, List<String>>();
		}

		Schema.DescribeFieldResult control = controlToken.getDescribe();
		List<Schema.PicklistEntry> controlEntries;
		if (control.getType() != Schema.DisplayType.Boolean) {
			controlEntries = control.getPicklistValues();
		}

		String base64map = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
		Map<String, List<String>> dependentPicklistValues = new Map<String, List<String>>();
		for (Schema.PicklistEntry entry : depend.getPicklistValues()) {
			if (
				entry.isActive() &&
				String.isNotEmpty(String.valueOf(((Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor')))
			) {
				List<String> base64chars = String.valueOf(((Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(entry))).get('validFor'))
					.split('');
				for (Integer index = 0; index < (controlEntries != null ? controlEntries.size() : 2); index++) {
					Object controlValue = (controlEntries == null
						? (Object) (index == 1)
						: (Object) (controlEntries[index].isActive() ? controlEntries[index].getLabel() : null));
					Integer bitIndex = index / 6;
					if (bitIndex > base64chars.size() - 1) {
						break;
					}
					Integer bitShift = 5 - Math.mod(index, 6);
					if (controlValue == null || (base64map.indexOf(base64chars[bitIndex]) & (1 << bitShift)) == 0) {
						continue;
					}
					if (!dependentPicklistValues.containsKey((String) controlValue)) {
						dependentPicklistValues.put((String) controlValue, new List<String>());
					}
					dependentPicklistValues.get((String) controlValue).add(entry.getLabel());
				}
			}
		}
		return dependentPicklistValues;
	}

	@AuraEnabled
	public static boolean isUserPartnerUser() {
		try {
			return GeneralUtils.currentUserIsPartnerUser();
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}
}
