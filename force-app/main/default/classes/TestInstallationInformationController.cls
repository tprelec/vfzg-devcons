@isTest
private class TestInstallationInformationController {
	@testSetup
	static void prepareCommonTestData() {
		TestUtils.createAccount(GeneralUtils.currentUser);
		TestUtils.createContact(TestUtils.theAccount);
		TestUtils.createOpportunity(TestUtils.theAccount, Test.getStandardPricebookId());
		Site__c oldSite = TestUtils.createSite(TestUtils.theAccount);
		Site__c newSite = TestUtils.createSite(TestUtils.theAccount);
		List<cscfga__Product_Basket__c> listProductBasketToInsert = new List<cscfga__Product_Basket__c>();
		cscfga__Product_Basket__c productBasketWithSites = new cscfga__Product_Basket__c(
			cscfga__Opportunity__c = TestUtils.theOpportunity.Id,
			csbb__Account__c = TestUtils.theAccount.Id
		);
		cscfga__Product_Basket__c productBasketWithNoSites = new cscfga__Product_Basket__c(
			cscfga__Opportunity__c = TestUtils.theOpportunity.Id,
			csbb__Account__c = TestUtils.theAccount.Id
		);
		cscfga__Product_Basket__c productBasketForMove = new cscfga__Product_Basket__c(
			cscfga__Opportunity__c = TestUtils.theOpportunity.Id,
			csbb__Account__c = TestUtils.theAccount.Id
		);
		listProductBasketToInsert.add(productBasketForMove);
		listProductBasketToInsert.add(productBasketWithSites);
		listProductBasketToInsert.add(productBasketWithNoSites);
		insert listProductBasketToInsert;
		List<cscfga__Product_Configuration__c> listProductConfigurationToInsert = new List<cscfga__Product_Configuration__c>();
		listProductConfigurationToInsert.add(new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = productBasketWithNoSites.Id));
		cscfga__Product_Configuration__c oldProductConf = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = productBasketWithSites.Id,
			Site__c = oldSite.Id
		);
		listProductConfigurationToInsert.add(oldProductConf);
		insert listProductConfigurationToInsert;
		cscfga__Product_Configuration__c newProductConf = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = productBasketForMove.Id,
			Site__c = newSite.Id,
			csordtelcoa__Replaced_Product_Configuration__c = oldProductConf.Id
		);
		insert newProductConf;

		csord__Order__c order = new csord__Order__c(csord__Identification__c = 'Test123', csord__Account__c = TestUtils.theAccount.Id);
		insert order;
		csord__Subscription__c subscription = new csord__Subscription__c(csord__Status__c = 'New', csord__Identification__c = '0101');
		insert subscription;
		csord__Service__c service = new csord__Service__c(
			Site__c = oldSite.Id,
			csord__Order__c = order.Id,
			csord__Subscription__c = subscription.Id,
			csord__Identification__c = '0101',
			csordtelcoa__Product_Configuration__c = oldProductConf.Id
		);
		insert service;
	}

	@isTest
	static void testGetSitesWithNoInput() {
		Test.startTest();
		LightningResponse response = InstallationInformationController.getSites(null);
		Test.stopTest();
		System.assertEquals(
			System.Label.InstallationInformation_UnexpectedComponentBehavior,
			response.message,
			'The exepected value is ' +
			System.Label.InstallationInformation_UnexpectedComponentBehavior +
			' is different to the actual value ' +
			response.message
		);
		System.assertEquals(
			LightningResponse.getErrorVariant(),
			response.variant,
			'The exepected value is ' +
			LightningResponse.getErrorVariant() +
			' is different to the actual value ' +
			response.variant
		);
	}

	@isTest
	static void testGetSitesWithNoData() {
		cscfga__Product_Configuration__c producproductConfiguration = [
			SELECT Id, cscfga__Product_Basket__c, Site__c
			FROM cscfga__Product_Configuration__c
			WHERE Site__c = NULL
			LIMIT 1
		];
		Test.startTest();
		LightningResponse response = InstallationInformationController.getSites(producproductConfiguration.cscfga__Product_Basket__c);
		Test.stopTest();
		System.assertEquals(null, response.body, 'The exepected value is null is different to the actual value ' + response.body);
	}

	@isTest
	static void testGetSitesWithBasketData() {
		cscfga__Product_Configuration__c producproductConfiguration = [
			SELECT Id, cscfga__Product_Basket__c, Site__c
			FROM cscfga__Product_Configuration__c
			WHERE Site__c != NULL
			LIMIT 1
		];
		Test.startTest();
		LightningResponse response = InstallationInformationController.getSites(producproductConfiguration.cscfga__Product_Basket__c);
		Test.stopTest();
		List<InstallationInformationController.InstallationSite> sitesResponse = (List<InstallationInformationController.InstallationSite>) response.body;
		System.assertEquals(1, sitesResponse.size(), 'The exepected value is 1 is different to the actual value ' + response.body);
		System.assertEquals(
			producproductConfiguration.Site__c,
			sitesResponse[0].siteId,
			'The exepected value is ' +
			producproductConfiguration.Site__c +
			' is different to the actual value ' +
			sitesResponse[0].siteId
		);
	}

	@isTest
	static void testSaveProductConfigurationSiteRecords() {
		Map<String, InstallationInformationController.installationSite> installationSitesMap = new Map<String, InstallationInformationController.installationSite>();
		cscfga__Product_Configuration__c oldProducproductConfiguration = [
			SELECT
				Id,
				cscfga__Product_Basket__c,
				cscfga__Product_Basket__r.csbb__Account__c,
				cscfga__Product_Basket__r.Discount_Approver__c,
				cscfga__Product_Basket__r.cscfga__Opportunity__c,
				Termination_Wish_Date__c,
				Termination_Reason__c,
				Termination_Sub_Reason__c,
				Termination_Fee_Discount_Type__c,
				Termination_Fee_Discount__c,
				Termination_Fee_Calculation__c,
				Installation_Wish_Date__c,
				cscfga__Contract_Term_Period__c,
				cscfga__total_recurring_charge__c,
				LG_InstallationPlannedDate__c,
				Site__c,
				Site__r.Technical_Contact__c,
				Site__r.Unify_Site_Name__c,
				csordtelcoa__Cancelled_By_Change_Process__c,
				cscfga__Description__c,
				cscfga__Parent_Configuration__c,
				cscfga__Contract_Term__c,
				csordtelcoa__Replaced_Service__c,
				(SELECT Id FROM cscfga__Related_Configurations__r),
				(SELECT Id, csord__Activation_Date__c FROM csordtelcoa__Services__r)
			FROM cscfga__Product_Configuration__c
			WHERE Site__c != NULL
			LIMIT 1
		];
		Contact technicalContact = [
			SELECT Id
			FROM Contact
			WHERE AccountId = :oldProducproductConfiguration.cscfga__Product_Basket__r.csbb__Account__c
			LIMIT 1
		];
		installationSitesMap.put(
			oldProducproductConfiguration.Id,
			new InstallationInformationController.installationSite(oldProducproductConfiguration)
		);
		for (InstallationInformationController.installationSite site : installationSitesMap.values()) {
			site.wishDate = Date.today();
			site.technicalContact = technicalContact.Id;
		}
		Test.startTest();
		LightningResponse response = InstallationInformationController.save(
			oldProducproductConfiguration.cscfga__Product_Basket__c,
			JSON.serialize(installationSitesMap)
		);
		Test.stopTest();
		System.assertEquals(
			LightningResponse.getSuccessVariant(),
			response.variant,
			'The exepected value is ' +
			LightningResponse.getSuccessVariant() +
			' is different to the actual value ' +
			response.variant
		);
		System.assertEquals(
			System.Label.InstallationInformation_InformationSuccessfullyUpdated,
			response.message,
			'The exepected value is ' +
			System.Label.InstallationInformation_InformationSuccessfullyUpdated +
			' is different to the actual value ' +
			response.message
		);
		cscfga__Product_Configuration__c newProducproductConfiguration = [
			SELECT Id, Installation_Wish_Date__c, Site__c, Site__r.Technical_Contact__c
			FROM cscfga__Product_Configuration__c
			WHERE Site__c != NULL
			LIMIT 1
		];
		System.assertNotEquals(
			oldProducproductConfiguration.Installation_Wish_Date__c,
			newProducproductConfiguration.Installation_Wish_Date__c,
			'The unexepected value is ' +
			oldProducproductConfiguration.Installation_Wish_Date__c +
			' is equal to the actual value ' +
			newProducproductConfiguration.Installation_Wish_Date__c
		);
		System.assertEquals(
			newProducproductConfiguration.Installation_Wish_Date__c,
			installationSitesMap.values()[0].wishDate,
			'The exepected value is ' +
			newProducproductConfiguration.Installation_Wish_Date__c +
			' is different to the actual value ' +
			installationSitesMap.values()[0].wishDate
		);
		System.assertNotEquals(
			oldProducproductConfiguration.Site__r.Technical_Contact__c,
			newProducproductConfiguration.Site__r.Technical_Contact__c,
			'The unexepected value is ' +
			oldProducproductConfiguration.Site__r.Technical_Contact__c +
			' is equal to the actual value ' +
			newProducproductConfiguration.Site__r.Technical_Contact__c
		);
		System.assertEquals(
			newProducproductConfiguration.Site__r.Technical_Contact__c,
			installationSitesMap.values()[0].technicalContact,
			'The exepected value is ' +
			newProducproductConfiguration.Site__r.Technical_Contact__c +
			' is different to the actual value ' +
			installationSitesMap.values()[0].technicalContact
		);
	}

	@isTest
	static void testSaveServiceSiteRecords() {
		Map<String, InstallationInformationController.installationSite> installationSitesMap = new Map<String, InstallationInformationController.installationSite>();
		csord__Service__c serviceBeforeUpdate = [
			SELECT
				Id,
				csord__Order__c,
				csord__Order__r.csord__Account__c,
				Installation_Wishdate__c,
				Site__c,
				Site__r.Technical_Contact__c,
				Site__r.Unify_Site_Name__c,
				csordtelcoa__Cancelled_By_Change_Process__c,
				VFZ_Commercial_Article_Name__c,
				csord__Service__c,
				Contract_End_Date__c,
				csordtelcoa__Product_Configuration__c,
				csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__c,
				csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__r.Id,
				csordtelcoa__Product_Configuration__r.cscfga__Product_Basket__r.Discount_Approver__c,
				csordtelcoa__Product_Configuration__r.Termination_Wish_Date__c,
				csordtelcoa__Product_Configuration__r.Termination_Reason__c,
				csordtelcoa__Product_Configuration__r.Termination_Sub_Reason__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Discount_Type__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Discount__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Calculation__c,
				csordtelcoa__Product_Configuration__r.cscfga__Contract_Term__c,
				csordtelcoa__Product_Configuration__r.cscfga__total_recurring_charge__c
			FROM csord__Service__c
			WHERE Site__c != NULL
			LIMIT 1
		];
		Contact technicalContact = [SELECT Id FROM Contact WHERE AccountId = :serviceBeforeUpdate.csord__Order__r.csord__Account__c LIMIT 1];
		installationSitesMap.put(serviceBeforeUpdate.Id, new InstallationInformationController.installationSite(serviceBeforeUpdate));
		for (InstallationInformationController.installationSite site : installationSitesMap.values()) {
			site.wishDate = Date.today();
			site.technicalContact = technicalContact.Id;
		}
		Test.startTest();
		LightningResponse response = InstallationInformationController.save(
			serviceBeforeUpdate.csord__Order__c,
			JSON.serialize(installationSitesMap)
		);
		Test.stopTest();
		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant);
		System.assertEquals(System.Label.InstallationInformation_InformationSuccessfullyUpdated, response.message);
		csord__Service__c updatedService = [
			SELECT Id, Installation_Wishdate__c, Site__c, Site__r.Technical_Contact__c
			FROM csord__Service__c
			WHERE Site__c != NULL
			LIMIT 1
		];
		System.assertNotEquals(serviceBeforeUpdate.Installation_Wishdate__c, updatedService.Installation_Wishdate__c);
		System.assertEquals(installationSitesMap.values()[0].wishDate, updatedService.Installation_Wishdate__c);
		System.assertNotEquals(serviceBeforeUpdate.Site__r.Technical_Contact__c, updatedService.Site__r.Technical_Contact__c);
		System.assertEquals(installationSitesMap.values()[0].technicalContact, updatedService.Site__r.Technical_Contact__c);
	}

	@isTest
	static void testMoveSiteScenario() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		cscfga__Product_Configuration__c productConfiguration = [
			SELECT Id, cscfga__Product_Basket__c, Site__c
			FROM cscfga__Product_Configuration__c
			WHERE Site__c != NULL AND csordtelcoa__Replaced_Product_Configuration__c != NULL
			LIMIT 1
		];
		csord__Order__c order = new csord__Order__c(csord__Identification__c = 'Test1234', csord__Account__c = acc.Id);
		insert order;
		csord__Subscription__c subscription = new csord__Subscription__c(csord__Status__c = 'New', csord__Identification__c = '0104');
		insert subscription;
		List<csord__Service__c> serviceToUpsert = new List<csord__Service__c>();
		csord__Service__c service = new csord__Service__c(
			Site__c = productConfiguration.Site__c,
			csord__Order__c = order.Id,
			csord__Subscription__c = subscription.Id,
			csord__Identification__c = '0102',
			csordtelcoa__Product_Configuration__c = productConfiguration.Id
		);
		serviceToUpsert.add(service);
		Test.startTest();
		upsert serviceToUpsert;
		LightningResponse response = InstallationInformationController.getSites(productConfiguration.cscfga__Product_Basket__c);
		LightningResponse responseOrder = InstallationInformationController.getSites(order.Id);
		List<InstallationInformationController.InstallationSite> sitesResponse = (List<InstallationInformationController.InstallationSite>) response.body;
		List<InstallationInformationController.InstallationSite> orderResponse = (List<InstallationInformationController.InstallationSite>) responseOrder.body;
		Test.stopTest();
		System.assertEquals(2, sitesResponse.size(), 'The exepected value is 2 is different to the actual value ' + sitesResponse.size());
		System.assertEquals(1, orderResponse.size(), 'The exepected value is 1 is different to the actual value ' + orderResponse.size());
	}
}
