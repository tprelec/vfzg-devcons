@isTest
public class CST_getPDF_FromList_controller_test {
	@testSetup
	public static void testSetup() {
		LiveChatVisitor lcv = new LiveChatVisitor();

		try {
			insert lcv;
		} catch (Exception e) {
		}

		// Blob bodyBlob=Blob.valueOf('<p>stuff</p>');

		LiveChatTranscript lct = new LiveChatTranscript();
		lct.LiveChatVisitorid = lcv.id;
		lct.body = '<p>stuff</p>';

		try {
			insert lct;
		} catch (Exception e) {
		}
		system.debug('lct: ' + lct);

		LiveChatTranscriptEvent le = new LiveChatTranscriptEvent();
		le.LiveChatTranscriptId = lct.id;
		le.type = 'ChatRequest';
		le.time = system.now();

		try {
			insert le;
		} catch (Exception e) {
		}
		system.debug('le: ' + le);
	}

	@isTest
	public static void test1() {
		Test.startTest();

		LiveChatTranscript chatTranscript = [SELECT id FROM LiveChatTranscript LIMIT 1];
		system.debug('chatTranscript: ' + chatTranscript);

		PageReference pageRef = Page.CST_getPDF_FromList;
		pageRef.getParameters().put('Id', String.valueOf(chatTranscript.Id));
		system.debug('pageRef: ' + pageRef);

		Test.setCurrentPage(pageRef);

		ApexPages.StandardController sc = new ApexPages.StandardController(chatTranscript);
		System.debug('sc: ' + sc);

		CST_getPDF_FromList_controller testAccPlan = new CST_getPDF_FromList_controller(sc);
		System.debug('testAccPlan: ' + testAccPlan);

		Test.stopTest();
	}
}
