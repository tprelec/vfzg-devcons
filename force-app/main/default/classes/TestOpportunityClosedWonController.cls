@isTest
private class TestOpportunityClosedWonController {
	@isTest
	static void testDirectSales() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		Contact c = new Contact(LastName = 'TestName', AccountId = TestUtils.theAccount.Id, Authorized_to_sign__c = true);
		insert c;
		PageReference pageRef = Page.OpportunityClosedWon;
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController sc = new ApexPages.StandardController(TestUtils.theOpportunity);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);

		Test.startTest();
		controller.theOpp.Confirm_Mixed_Opportunity__c = true;
		controller.confirmMixedOpportunity();
		controller.createNewContact();
		controller.loadContacts();
		controller.saveQuickNewContact();
		controller.QuickNewContact.LastName = 'QuickCreate';
		controller.saveQuickNewContact();
		controller.checkNoneSelected();
		System.assert(controller.noneSelected, 'Controller none selected');
		for (OpportunityClosedWonController.ContactWrapper cw : controller.contacts) {
			cw.selected = true;
		}
		controller.ccToCurrentUser = true;
		try {
			controller.sendWelcomeEmail(controller.contacts, true, TestUtils.theOpportunity.Id);
		} catch (Exception e) {
			System.debug(System.LoggingLevel.ERROR, e);
		}
		controller.checkNoneSelected();
		System.assert(!controller.noneSelected, 'Controller none selected');

		controller.confirmMissingType();
		controller.cancelClosedWon();
		controller.confirmClosedWon();
		Boolean oppHasZiggoProduct = controller.oppHasZiggoProduct;
		controller.sendMessageToParentWindowToCloseFrame();
		Boolean customerReferenceOptional = controller.customerReferenceOptional;
		System.assertEquals(
			controller.templateId,
			[SELECT Id FROM EmailTemplate WHERE DeveloperName = 'Welcome_Email_EBU']
			.Id,
			'Email template is same'
		);
		controller.backToForm();
		controller.getBaseUrl();
		controller.getContentUrl();
		// again to trigger 'already closed' error
		controller.confirmClosedWon();

		Test.stopTest();
	}

	@isTest
	static void testClosingBySAG() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		TestUtils.theAdministrator = new User(Id = UserInfo.getUserId());
		User accManager = new User(
			Id = TestUtils.theAccountManager.Id,
			ManagerId = TestUtils.theAdministrator.Id,
			UserRoleId = [SELECT ID FROM UserRole WHERE Name = 'MLE Sales Support' LIMIT 1]
			.Id
		);

		System.runAs(GeneralUtils.currentUser) {
			update accManager;
		}

		Test.startTest();
		PageReference pageRef = Page.OpportunityClosedWon;
		pageRef.getParameters().put('Id', String.valueOf(TestUtils.theOpportunity.Id));
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController sc = new ApexPages.StandardController(TestUtils.theOpportunity);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
		System.runAs(accManager) {
			controller.confirmClosingBySAG();
			System.assertEquals(controller.theOpp.StageName, 'Closing By SAG', 'Stage name is Closing By SAG');
			controller.confirmClosingBySAG();
			System.assertEquals(controller.theOpp.StageName, 'Closed Won', 'Stage name is Closed Won');
		}
		controller.theOpp.StageName = 'Closing';
		controller.theOpp.Primary_Basket__c = null;
		controller.confirmClosingBySAG();
		System.assertEquals(controller.theOpp.StageName, 'Closing By SAG', 'Stage name is Closing By SAG');
		Test.stopTest();
	}

	@isTest
	static void testLightningClose() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOpportunityFieldMappings();
		OrderType__c ot = TestUtils.createOrderType();
		Product2 p = TestUtils.createProduct();
		PriceBookEntry pbEntry = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), p);
		VF_Product__c vfp = new VF_Product__c(
			Name = 'tst',
			Family__c = 'tst',
			Product_Line__c = 'fVodafone',
			OrderType__c = ot.Id,
			ExternalID__c = 'VFP-02-1234567'
		);
		insert vfp;
		p.VF_Product__c = vfp.Id;
		p.productCode = '123123';
		update p;

		TestUtils.autoCommit = false;
		Account partnerAcc = TestUtils.createPartnerAccount();
		partnerAcc.Frame_Work_Agreement__c = '24234235235';
		partnerAcc.Total_Licenses__c = 5;
		insert partnerAcc;
		Ban__c ban = TestUtils.createBan(partnerAcc);
		ban.Dealer_Code__c = '543654';
		insert ban;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, null);
		insert fa;
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);
		insert bar;
		TestUtils.autoCommit = true;

		Site__c site = TestUtils.createSite(partnerAcc);
		User u = TestUtils.createPortalUser(partnerAcc);

		Test.startTest();
		// run as portal user
		//System.runAs(u) {
		TestUtils.autoCommit = false;
		Opportunity opp = TestUtils.createOpportunityWithBan(partnerAcc, Test.getStandardPricebookId(), null, u); //ban
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		insert opp;
		System.runAs(u) {
			OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp, pbEntry);
			oppLineItem.Site_List__c = site.Id;
			oppLineItem.Source_system__c = 'Manual';
			insert oppLineItem;
			TestUtils.autoCommit = true;

			PageReference pageRef = Page.OpportunityClosedWon;
			pageRef.getParameters().put('Id', String.valueOf(opp.Id));
			Test.setCurrentPage(pageRef);
			ApexPages.StandardController sc = new ApexPages.StandardController(opp);
			OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
			TestUtils.isPartnerCommunity = true;
			controller.cancelClosedWon();
			controller.confirmMixedOpportunity();
			controller.confirmClosingBySAG();

			partnerAcc.Channel__c = 'BP';
			update partnerAcc;
			ApexPages.StandardController sc2 = new ApexPages.StandardController(opp);
			OpportunityClosedWonController controller2 = new OpportunityClosedWonController(sc2);
			controller2.confirmClosingBySAG();
			controller2.showClosedWonWithEmail = false;
			controller2.confirmClosedWon();
			controller2.showClosedWonWithEmail = true;
			controller2.dummyContact = new Contact();
			controller2.dummyContact.Welcome_Email_Date__c = System.today() - 1;
			controller2.confirmClosedWon();
			Boolean hasMsg = false;
			for (ApexPages.Message msg : ApexPages.getMessages()) {
				if (msg.getDetail() == ('Please specify the solution sales contact')) {
					hasMsg = true;
				}
			}
			System.assert(hasMsg, 'Has message is true');
		}
		Test.stopTest();
	}

	@isTest
	static void testLightningCloseSAG() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		insert TestUtils.ov('Opportunity60', 'Opportunity', 'Vodafone_Products__c');

		TestUtils.theAdministrator = new User(Id = UserInfo.getUserId());
		User accManager = new User(
			Id = TestUtils.theAccountManager.Id,
			ManagerId = TestUtils.theAdministrator.Id,
			UserRoleId = [SELECT ID FROM UserRole WHERE Name = 'MLE Sales Support' LIMIT 1]
			.Id
		);
		System.runAs(GeneralUtils.currentUser) {
			List<User> users = new List<User>();
			users.add(new User(Id = TestUtils.theAdministrator.Id, Signature_Document_Id__c = '3452345235'));

			users.add(accManager);
			update users;
		}

		PageReference pageRef = Page.OpportunityClosedWon;
		pageRef.getParameters().put('Id', String.valueOf(TestUtils.theOpportunity.Id));
		Test.setCurrentPage(pageRef);
		Test.startTest();
		System.runAs(accManager) {
			ApexPages.StandardController sc = new ApexPages.StandardController(TestUtils.theOpportunity);
			OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
			TestUtils.isPartnerCommunity = true;
			controller.confirmClosingBySAG();
			System.assertEquals(controller.theOpp.StageName, 'Closing By SAG', 'Stage name is Closing By SAG');
			controller.confirmClosedWon();
			System.assertEquals(controller.theOpp.StageName, 'Closed Won', 'Stage name is Closed Won');
			controller.confirmClosingBySAG();
			Boolean hasMsg = false;
			for (ApexPages.Message msg : ApexPages.getMessages()) {
				if (msg.getDetail() == ('')) {
					hasMsg = true;
				}
			}
			System.assert(hasMsg, 'Has message is true');
		}
		Test.stopTest();
	}

	@isTest
	static void testLightningCloseWon() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		PageReference pageRef = Page.OpportunityClosedWon;
		Test.setCurrentPage(pageRef);
		Test.startTest();
		TestUtils.theOpportunity.No_Contract__c = true;
		TestUtils.theOpportunity.Confirm_Mixed_Opportunity__c = true;
		TestUtils.theOpportunity.Escalation__c = false;
		update TestUtils.theOpportunity;
		ApexPages.StandardController sc = new ApexPages.StandardController(TestUtils.theOpportunity);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
		TestUtils.isPartnerCommunity = true;
		controller.confirmClosedWon();
		System.assertEquals(controller.theOpp.StageName, 'Closed Won', 'Stage name is Closed Won');
		Test.stopTest();
	}

	@isTest
	static void testControllerError() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();
		TestUtils.createOpportunityFieldMappings();
		PageReference pageRef = Page.OpportunityClosedWon;
		pageRef.getParameters().put('Id', String.valueOf(TestUtils.theOpportunity.Id));
		pageRef.getParameters().put('showBooleans', '1');
		pageRef.getParameters().put('isClosed', '1');
		Test.setCurrentPage(pageRef);
		Test.startTest();
		TestUtils.theOpportunity.No_Contract__c = true;
		TestUtils.theOpportunity.Escalation__c = false;
		TestUtils.theOpportunity.Confirm_Mixed_Opportunity__c = true;
		TestUtils.theOpportunity.StageName = 'Closing by SAG';
		update TestUtils.theOpportunity;
		ApexPages.StandardController sc = new ApexPages.StandardController(TestUtils.theOpportunity);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
		controller.confirmClosingBySAG();
		controller.confirmClosedWon();
		Boolean hasMsg = false;
		for (ApexPages.Message msg : ApexPages.getMessages()) {
			if (msg.getDetail().contains('Only Inside Sales can close this opportunity')) {
				hasMsg = true;
			}
		}
		System.assert(hasMsg, 'Has message is true ');
		Test.stopTest();
	}

	@isTest
	static void testControllerNoOlis() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOpportunityFieldMappings();
		User owner = TestUtils.createAdministrator();
		//TestUtils.createAccount(owner);

		Account acc = TestUtils.createAccount(owner);
		acc.Frame_Work_Agreement__c = 'VF-001';
		acc.VZ_Framework_Agreement__c = 'VZ-001';
		update acc;

		Ban__c ban = TestUtils.createBan(TestUtils.TheAccount);
		upsert ban;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, TestUtils.createContact(acc).Id);
		upsert fa;
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);
		upsert bar;

		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Closing',
			CloseDate = system.today(),
			AccountId = TestUtils.TheAccount.Id,
			Type = 'New Business',
			Bespoke__c = false,
			Pricebook2Id = Test.getStandardPricebookId(),
			Project_Comments__c = 'Test opp without opp lines',
			BAN__c = ban.Id,
			Financial_Account__c = fa.Id,
			Billing_Arrangement__c = bar.Id
		);
		insert opp;
		PageReference pageRef = Page.OpportunityClosedWon;
		pageRef.getParameters().put('Id', String.valueOf(opp.Id));
		Test.setCurrentPage(pageRef);
		Test.startTest();
		ApexPages.StandardController sc = new ApexPages.StandardController(opp);
		OpportunityClosedWonController controller = new OpportunityClosedWonController(sc);
		controller.confirmClosingBySAG();
		System.assertEquals(controller.theOpp.StageName, 'Closed Won', 'Stage name is Closed Won');
		Test.stopTest();
	}
}
