@isTest
private class TestCS_ServiceTriggerHandler {
	@isTest
	private static void testCreateProcess() {
		csord__Subscription__c subscription = CS_DataTest.createSubscription(null);
		subscription.csord__Identification__c = 'Test1';
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(null, null);
		service.csord__Status__c = 'Created';
		service.csord__Identification__c = 'Test1';
		service.csord__Subscription__c = subscription.Id;
		service.csord__Status__c = 'Implemented';
		insert service;

		Test.startTest();
		List<csord__Subscription__c> subscriptions = [SELECT Id FROM csord__Subscription__c];

		List<csord__Service__c> services = [
			SELECT Id, csord__Status__c, csord__Identification__c, csord__Subscription__c
			FROM csord__Service__c
			WHERE csord__Subscription__c = :subscriptions[0].Id
		];

		Test.stopTest();

		System.assert(services.size() > 0, 'Services should exist.');
		System.assertEquals(services[0].csord__Status__c, 'Implemented', 'Status sholud have value Implemented.');
	}

	@isTest
	private static void testCalculateEndDateInsert() {
		List<Profile> profiles = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole WHERE ParentRoleId = NULL];

		User user = CS_DataTest.createUser(profiles, roleList);
		insert user;

		System.runAs(user) {
			Account testAccount = CS_DataTest.createAccount('Test Account 1');
			insert testAccount;

			Contact contact1 = new Contact(
				AccountId = testAccount.id,
				LastName = 'Last',
				FirstName = 'First',
				Contact_Role__c = 'Consultant',
				Email = 'test@vf.com'
			);
			insert contact1;

			Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Service opportunity', user.Id);
			insert opp;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Service Basket');
			basket.cscfga__Basket_Status__c = 'Valid';
			basket.Primary__c = true;
			basket.csbb__Synchronised_with_Opportunity__c = true;
			basket.csbb__Account__c = testAccount.Id;
			insert basket;

			cscfga__Product_Definition__c internetDefinition = CS_DataTest.createProductDefinition('Internet');
			insert internetDefinition;

			cscfga__Product_Configuration__c internetConfig = CS_DataTest.createProductConfiguration(internetDefinition.Id, 'Internet', basket.Id);
			internetConfig.Contract_Number_Group__c = 'BI';
			internetConfig.cscfga__Contract_Term__c = 24;
			insert internetConfig;

			csord__Subscription__c subscription1 = CS_DataTest.createSubscription(internetConfig.Id);
			subscription1.csord__Identification__c = 's1';
			insert subscription1;

			csord__Service__c service1 = CS_DataTest.createService(internetConfig.Id, subscription1.Id, 'serv1');
			service1.csord__Activation_Date__c = Date.newInstance(2022, 4, 1);
			insert service1;

			Test.startTest();
			csord__Service__c testedService = [SELECT Id, Contract_End_Date__c FROM csord__Service__c WHERE Id = :service1.Id];
			System.assertEquals(Date.newInstance(2024, 4, 1), testedService.Contract_End_Date__c, 'Dates should be equal.');

			Test.stopTest();
		}
	}
}
