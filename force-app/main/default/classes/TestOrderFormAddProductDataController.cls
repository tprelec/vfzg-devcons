@isTest
private class TestOrderFormAddProductDataController {
	@isTest
	static void test_method_one() {
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);

		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;
		Test.startTest();
		// OrderType__c ot = TestUtils.createOrderType();
		Order__c ord = TestUtils.createOrder(TestUtils.theContract);
		ord.Status__c = 'Accepted';
		update ord;

		VF_Product__c p = new VF_Product__c();
		p.Name = 'Test productVF';
		p.Brand__c = 'Testing Brand';
		p.Cost_Price__c = 11;
		p.Description__c = 'This is a test product';
		p.Active__c = true;
		p.ProductCode__c = 'TEST_PRODUCT_CODE_VF';
		p.Product_Group__c = 'Priceplan';
		p.Quantity_type__c = 'Monthly';
		p.SDC_Product_matrix__c = 'Fixed - Data';
		p.Product_Line__c = 'fZiggo Only';
		p.ExternalID__c = 'VF-01-3983732';
		// p.OrderType__c=ot.id;
		p.OrderType__c = [SELECT Id FROM OrderType__c LIMIT 1].id;
		insert p;
		System.debug('@@@@LIMITS.getQueries: ' + LIMITS.getQueries());

		Product2 prod = new Product2();
		prod = TestUtils.createProduct();
		prod.VF_Product__c = p.id;
		prod.CLC__c = 'Acq';
		prod.role__c = 'Numberporting';
		update prod;

		PricebookEntry pbEntry = new PricebookEntry();
		pbEntry = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
		update pbEntry;

		Account acc = [SELECT id FROM account WHERE id = :TestUtils.theContract.Account__c];

		// Test.startTest();

		Site__c site = TestUtils.createSite(acc);

		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = 1,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Product__c = prod.Id,
			VF_Contract__c = TestUtils.theContract.Id,
			Site__c = site.Id,
			Order__c = ord.id
		);
		insert cp;

		PBX_Type__c pbx = new PBX_Type__c();
		pbx.name = 'Onenet Virtual PBX';
		pbx.vendor__c = 'Onenet';
		pbx.Product_Name__c = 'Virtual PBX';
		//pbx.Software_version__c='1';
		insert pbx;
		System.Debug('PBX:' + pbx.name);

		Competitor_Asset__c ca = new Competitor_Asset__c();
		ca = PBXSelectionWizardController.createAsset(site, ca, pbx.id);
		insert ca;

		PageReference pageRef = Page.OrderFormAddProduct;
		Test.setCurrentPage(pageRef);

		ApexPages.currentPage().getParameters().put('contractId', TestUtils.theContract.id);
		ApexPages.currentPage().getParameters().put('orderId', ord.id);

		OrderFormAddProductDataController controller = new OrderFormAddProductDataController();

		controller.theOrder = ord;
		controller.product = p.Id;
		controller.cp.CLC__c = 'Acq';
		controller.selectedSites = new List<ID>();
		controller.selectedSites.add(site.id);

		controller.create();
		Test.stopTest();
	}
}
