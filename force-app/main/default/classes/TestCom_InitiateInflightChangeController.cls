/*
 * @author Rahul Sharma
 * @date 18-10-2022
 *
 * @author Neno Dodig
 * @date 21-10-2022
 *
 * @description Test class for Com_InitiateInflightChangeController
 */
@IsTest
private class TestCom_InitiateInflightChangeController {
	@TestSetup
	private static void createTestData() {
		Account account = CS_DataTest.createAccount('Test Account');
		insert account;

		Opportunity opportunity = CS_DataTest.createOpportunity(account, 'Test Opp', UserInfo.getUserId());
		insert opportunity;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opportunity, 'Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c productDefinition = CS_DataTest.createProductDefinition('Product Definition');
		productDefinition.RecordTypeId = productDefinitionRecordType;
		productDefinition.Product_Type__c = 'Fixed';
		insert productDefinition;

		cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(productDefinition.Id, 'Test Conf', basket.Id);
		insert productConfiguration;

		csord__Order__c order = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
		insert order;

		Site__c testSite = CS_DataTest.createSite('Test Site', account, '1032AB', 'Street', 'City', 10.0);
		testSite.Footprint__c = null;
		insert testSite;

		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrder('Parent DeliveryOrder', order.Id, true);

		List<COM_Delivery_Order__c> deliveryOrders = CS_DataTest.createMultipleDeliveryOrders(
			'DeliveryOrder 1',
			order.Id,
			deliveryOrder.Id,
			2,
			false
		);
		deliveryOrders[0].Status__c = 'In progress';
		deliveryOrders[1].PONR_Reached__c = true;
		deliveryOrders[1].Status__c = 'In progress';
		insert deliveryOrders;

		COM_Delivery_Order__c deliveryOrder1 = deliveryOrders[0];

		csord__Subscription__c subscription = CS_DataTest.createSubscription(productConfiguration.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		insert subscription;

		csord__Service__c parentService = CS_DataTest.createService(productConfiguration.Id, subscription, 'Service Created');
		parentService.csord__Identification__c = 'testSubscription';
		parentService.COM_Delivery_Order__c = deliveryOrder1.Id;
		parentService.csord__Order__c = order.Id;
		parentService.VFZ_Product_Abbreviation__c = 'ZIPRO';
		insert parentService;

		CSPOFA__Orchestration_Process_Template__c orchestrationProcessTemplate = CS_DataTest.createOrchProcessTemplate(
			'Test process Template',
			'5',
			true
		);
		CSPOFA__Orchestration_Step_Template__c orchestrationStepTemplate = CS_DataTest.createOrchStepTemplate(
			orchestrationProcessTemplate.Id,
			'Step 1',
			'3',
			false
		);
		insert orchestrationStepTemplate;

		CSPOFA__Orchestration_Process__c orchestrationProcess = CS_DataTest.createOrchProcess(
			orchestrationProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		orchestrationProcess.COM_Delivery_Order__c = deliveryOrder1.Id;
		insert orchestrationProcess;

		createSolution(order.Id, basket.Id);
	}

	@IsTest
	private static void testGetDataForCSOrder() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c LIMIT 1];

		Test.startTest();

		LightningResponse response = Com_InitiateInflightChangeController.getData(order.Id);

		System.assertEquals(null, response.variant);
		System.assertEquals(null, response.message);

		Com_InitiateInflightChangeController.InflightChangeData data = (Com_InitiateInflightChangeController.InflightChangeData) response.body;

		Test.stopTest();

		System.assertEquals(1, data.availableDeliveries.size());
		System.assertEquals(1, data.unavailableDeliveries.size());
		System.assertEquals(2, data.columns.size());
	}

	@IsTest
	private static void testSaveDataNoSolutionFoundForCSOrder() {
		csord__Order__c order = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568980');
		insert order;

		Id basketId = getBasketId();

		// set the basket Id like we get it generated from the CS API: cssmgnt.API_1.createInflightRequest
		Com_InitiateInflightChangeController.jsonResponse = getBasketIdMapForMockingCsApiResponse(basketId);

		Test.startTest();

		LightningResponse response = Com_InitiateInflightChangeController.performInflightChangeForOrder(order.Id, new List<String>{});

		Test.stopTest();

		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
		System.assertEquals(System.Label.Com_InitiateInflightChange_NoSolutionFoundMessage, response.message);
	}

	@IsTest
	private static void testSaveDataSimulateCSAPICallFailure() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c LIMIT 1];

		Test.startTest();

		List<COM_Delivery_Order__c> availableDeliveries = [
			SELECT Id, Name, Status__c, PONR_Reached__c, Products__c
			FROM COM_Delivery_Order__c
			WHERE Order__c = :order.Id AND Parent_Delivery_Order__c != NULL AND PONR_Reached__c = FALSE
		];

		System.assertEquals(1, availableDeliveries.size());

		LightningResponse response = Com_InitiateInflightChangeController.performInflightChangeForOrder(
			order.Id,
			new List<String>{ availableDeliveries[0].Id }
		);

		Test.stopTest();

		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
		System.assertEquals(System.Label.Com_InitiateInflightChange_DeserializationErrorMessage, response.message);
	}

	@IsTest
	private static void testSaveDataWithDeliveryRecordsForCSOrder() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c LIMIT 1];

		Id basketId = getBasketId();

		// set the basket Id like we get it generated from the CS API: cssmgnt.API_1.createInflightRequest
		Com_InitiateInflightChangeController.jsonResponse = getBasketIdMapForMockingCsApiResponse(basketId);

		Test.startTest();

		List<COM_Delivery_Order__c> availableDeliveries = [
			SELECT Id, Name, Status__c, PONR_Reached__c, Products__c
			FROM COM_Delivery_Order__c
			WHERE Order__c = :order.Id AND Parent_Delivery_Order__c != NULL AND PONR_Reached__c = FALSE
		];

		System.assertEquals(1, availableDeliveries.size());

		LightningResponse response = Com_InitiateInflightChangeController.performInflightChangeForOrder(
			order.Id,
			new List<String>{ availableDeliveries[0].Id }
		);

		Test.stopTest();

		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant);
		System.assertEquals(System.Label.Com_InitiateInflightChange_SuccessMessage, response.message);
	}

	@IsTest
	private static void testSaveDataWithoutDeliveryRecordsForCSOrder() {
		List<Opportunity> opportunities = [SELECT Id FROM Opportunity LIMIT 1];
		System.assertNotEquals(0, opportunities.size(), 'Opportunity is not created');
		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opportunities[0], 'Test Basket');
		insert basket;

		csord__Order__c order = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568980');
		insert order;

		// set the basket Id like we get it generated from the CS API: cssmgnt.API_1.createInflightRequest
		Com_InitiateInflightChangeController.jsonResponse = getBasketIdMapForMockingCsApiResponse(basket.Id);

		createSolution(order.Id, basket.Id);

		Test.startTest();

		LightningResponse response = Com_InitiateInflightChangeController.performInflightChangeForOrder(order.Id, new List<String>());

		Test.stopTest();

		System.assertEquals(System.Label.Com_InitiateInflightChange_SuccessMessage, response.message);

		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant);
	}

	@IsTest
	private static void testInvalidBasketIdForCSOrder() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c LIMIT 1];

		// set the basket Id like we get it generated from the CS API: cssmgnt.API_1.createInflightRequest
		Com_InitiateInflightChangeController.jsonResponse = getBasketIdMapForMockingCsApiResponse('Invalid basketId');

		Test.startTest();

		LightningResponse response = Com_InitiateInflightChangeController.performInflightChangeForOrder(order.Id, new List<String>());

		Test.stopTest();

		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
		System.assertEquals(System.Label.Com_InitiateInflightChange_CS_API_CallFailed, response.message);
	}

	@IsTest
	private static void testGetDataForDeliveryOrder() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c LIMIT 1];

		List<COM_Delivery_Order__c> availableDeliveries = [
			SELECT Id, Name, Status__c, PONR_Reached__c, Products__c
			FROM COM_Delivery_Order__c
			WHERE Order__c = :order.Id AND Parent_Delivery_Order__c != NULL AND PONR_Reached__c = FALSE
		];

		System.assertEquals(1, availableDeliveries.size());

		Test.startTest();

		LightningResponse response = Com_InitiateInflightChangeController.getData(availableDeliveries[0].Id);

		Test.stopTest();

		System.assertEquals(null, response.variant);
		System.assertEquals(null, response.message);
		System.assertEquals(null, response.body);
	}

	@IsTest
	private static void testSaveDataWithInflightChangeForDeliveryOrder() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c LIMIT 1];

		Id basketId = getBasketId();

		// set the basket Id like we get it generated from the CS API: cssmgnt.API_1.createInflightRequest
		Com_InitiateInflightChangeController.jsonResponse = getBasketIdMapForMockingCsApiResponse(basketId);

		Test.startTest();

		List<COM_Delivery_Order__c> availableDeliveries = [
			SELECT Id, Name, Status__c, PONR_Reached__c, Products__c
			FROM COM_Delivery_Order__c
			WHERE Order__c = :order.Id AND Parent_Delivery_Order__c != NULL AND PONR_Reached__c = FALSE
		];

		System.assertEquals(1, availableDeliveries.size());

		LightningResponse response = Com_InitiateInflightChangeController.performInflightChangeForDeliveryOrder(availableDeliveries[0].Id);

		Test.stopTest();

		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant);
		System.assertEquals(System.Label.Com_InitiateInflightChange_SuccessMessage, response.message);
	}

	@IsTest
	private static void testInvalidBasketIdForDeliveryOrder() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c LIMIT 1];

		// set the basket Id like we get it generated from the CS API: cssmgnt.API_1.createInflightRequest
		Com_InitiateInflightChangeController.jsonResponse = getBasketIdMapForMockingCsApiResponse('Invalid basketId');

		Test.startTest();

		List<COM_Delivery_Order__c> availableDeliveries = [
			SELECT Id, Name, Status__c, PONR_Reached__c, Products__c
			FROM COM_Delivery_Order__c
			WHERE Order__c = :order.Id AND Parent_Delivery_Order__c != NULL AND PONR_Reached__c = FALSE
		];

		System.assertEquals(1, availableDeliveries.size());

		LightningResponse response = Com_InitiateInflightChangeController.performInflightChangeForDeliveryOrder(availableDeliveries[0].Id);

		Test.stopTest();

		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
		System.assertEquals(System.Label.Com_InitiateInflightChange_CS_API_CallFailed, response.message);
	}

	private static Id getBasketId() {
		List<cscfga__Product_Basket__c> baskets = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
		System.assertEquals(1, baskets.size());
		cscfga__Product_Basket__c basket = baskets[0];
		return basket.Id;
	}

	public static void createSolution(Id orderId, Id basketId) {
		cscfga__Product_Definition__c oneMobileProductDefinition = CS_DataTest.createProductDefinition('OneMobile');
		oneMobileProductDefinition.Product_Type__c = 'Mobile';

		cscfga__Product_Definition__c oneMobileSolutionProductDefinition = CS_DataTest.createProductDefinition('OneMobile Solution');
		oneMobileSolutionProductDefinition.Product_Type__c = 'Mobile';

		List<cscfga__Product_Definition__c> productDefinitions = new List<cscfga__Product_Definition__c>{
			oneMobileProductDefinition,
			oneMobileSolutionProductDefinition
		};
		insert productDefinitions;

		csutil__JSON_Data__c schemaMobileSolution = new csutil__JSON_Data__c(
			Name = 'Mobile Solution',
			csutil__value__c = '{"attributes":[{"type":"String","name":"GUID"},{"type":"String","name":"SolutionId"},{"required":false,"showInUI":true,"readOnly":false,"name":"SolutionName","other":"__ConfigName__","label":"Solution Name","options":[],"type":"String","customCSSClasses":"","referencedAttributes":""},{"required":true,"showInUI":true,"readOnly":false,"name":"ContractDuration","label":"Contract Duration","options":[{"value":"1","label":"1"},{"value":"12","label":"12"},{"value":"24","label":"24"},{"value":"36","label":"36"}],"type":"Picklist","customCSSClasses":"","referencedAttributes":""},{"required":false,"showInUI":false,"readOnly":false,"name":"isMainComponent","label":"isMainComponent","value":true,"options":[],"type":"Boolean","customCSSClasses":"","referencedAttributes":""},{"required":false,"showInUI":false,"readOnly":false,"name":"Today","label":"Today","type":"String","helpText":" Field used to capture the current date as a string. Value set using JS.","customCSSClasses":"","referencedAttributes":""},{"required":false,"showInUI":false,"readOnly":false,"name":"SalesChannel","label":"Sales Channel","type":"String","customCSSClasses":"","referencedAttributes":""}],"attributeDefinitions":[],"visible":true,"disabled":false,"name":"Mobile Solution","buttonLabel":"Mobile"}'
		);
		insert schemaMobileSolution;

		cssdm__Solution_Definition__c solutionDefinition = new cssdm__Solution_Definition__c(
			Name = 'Mobile',
			cssdm__component_type__c = 'Mobile Solution',
			cssdm__create_pcr__c = false,
			cssdm__max__c = 1,
			cssdm__min__c = 1,
			cssdm__product_definition__c = oneMobileSolutionProductDefinition.Id,
			cssdm__schema__c = schemaMobileSolution.Id,
			cssdm__type__c = 'Main'
		);
		insert solutionDefinition;

		csord__Solution__c solution = new csord__Solution__c(
			csord__Status__c = 'Completed',
			cssdm__product_basket__c = basketId,
			Name = 'OneMobile Solution',
			cssdm__solution_definition__c = solutionDefinition.Id,
			csord__Identification__c = 'Solution Console 2021-04-09 09:04:00',
			csord__Order__c = orderId
		);
		insert solution;
	}

	private static String getBasketIdMapForMockingCsApiResponse(String basketId) {
		Map<String, Object> jsonResponse = new Map<String, Object>{
			Com_InitiateInflightChangeController.TARGET_BASKET_ID_ATTRIBUTE_NAME => basketId
		};
		return JSON.serialize(jsonResponse);
	}
}
