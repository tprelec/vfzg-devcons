@isTest
private class TestCOM_DXLHttpService {
	@TestSetup
	static void makeData() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		User owner = TestUtils.createAdministrator();

		Account acct = TestUtils.createAccount(owner);
		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test', Email = 'contact@test.com', Phone = '+31 111111789');
		insert c;

		Opportunity testOpportunity = CS_DataTest.createOpportunity(acct, 'Test Opportunity', owner.Id);
		testOpportunity.Main_Contact_Person__c = c.Id;
		insert testOpportunity;
	}

	@isTest
	static void testMethod1() {
		String payload = '{"statusCode": 200,"statusMessage": "OK"}';
		COM_Integration_setting__mdt dxlIntegrationSettings = COM_Integration_setting__mdt.getInstance(
			COM_DXL_Constants.COM_DXL_INTEGRATION_SETTING_NAME
		);
		COM_DXLHttpService httpService = new COM_DXLHttpService(payload);

		COM_DXL_settings__mdt businessTransactionIdSetting = COM_DXL_settings__mdt.getInstance(
			COM_DXL_Constants.CREATECUSTOMER_SYNC_RESPONSE_TRANSACTION_ID
		);
		String businessTransactionIdKey = businessTransactionIdSetting != null
			? businessTransactionIdSetting.Value__c
			: COM_DXL_Constants.BUSINESS_TRANSACTION_ID;

		List<Opportunity> oppList = [SELECT Id, Account.Unify_Dealer_Market_Indicator__c, Account.Mobile_Dealer__r.Dealer_Code__c FROM Opportunity];

		HttpRequest req = httpService.createDxlHttpRequest(dxlIntegrationSettings, businessTransactionIdKey, oppList);

		System.assertEquals(payload, req.getBody(), 'Expexted: ' + payload + ' Actual: ' + req.getBody());

		System.assertEquals(
			oppList[0].Account.Unify_Dealer_Market_Indicator__c,
			req.getHeader('dealerMarket'),
			'Expexted: ' +
			oppList[0].Account.Unify_Dealer_Market_Indicator__c +
			' Actual: ' +
			req.getHeader('dealerMarket')
		);
	}

	@isTest
	static void testMethod2() {
		String businessTransactionId = COM_WebServiceConfig.getUUID();
		Integer statusCode = 400;
		Boolean success = false;
		String payload = '{"statusCode": 200,"statusMessage": "OK"}';
		COM_DXLHttpService httpService = new COM_DXLHttpService(payload);

		COM_DXL_settings__mdt businessTransactionIdSetting = COM_DXL_settings__mdt.getInstance(
			COM_DXL_Constants.CREATECUSTOMER_SYNC_RESPONSE_TRANSACTION_ID
		);
		String businessTransactionIdKey = businessTransactionIdSetting != null
			? businessTransactionIdSetting.Value__c
			: COM_DXL_Constants.BUSINESS_TRANSACTION_ID;

		COM_DXL_CreateCustomer.CreateCustomerResponseData customerResponseData = new COM_DXL_CreateCustomer.CreateCustomerResponseData();
		customerResponseData.businessTransactionIdKey = businessTransactionIdKey;
		customerResponseData.businessTransactionId = businessTransactionId;
		customerResponseData.success = success;
		customerResponseData.statusCode = statusCode;

		HttpResponse response = httpService.createDxlHttpResponse(customerResponseData);

		System.assertEquals(statusCode, response.getStatusCode(), 'Expected: ' + statusCode + ' Actual: ' + response.getStatusCode());
	}
}
