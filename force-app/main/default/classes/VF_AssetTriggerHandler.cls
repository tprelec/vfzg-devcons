@SuppressWarnings('PMD')
public with sharing class VF_AssetTriggerHandler extends TriggerHandler {
	List<VF_Asset__c> newAssets;
	Map<Id, VF_Asset__c> newAssetMap;
	List<VF_Asset__c> oldAssets;
	Map<Id, VF_Asset__c> oldAssetMap;

	private void init() {
		this.newAssets = (List<VF_Asset__c>) this.newList;
		this.newAssetMap = (Map<Id, VF_Asset__c>) this.newMap;
		this.oldAssets = (List<VF_Asset__c>) this.oldList;
		this.oldAssetMap = (Map<Id, VF_Asset__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setOwnerByDealerCode();
		linkAssetsToContract();
		linkAssetsToOrder();
		updateOwnership();
	}

	public override void afterInsert() {
		init();
		updateAccountData();
		updateSharingRules();
		countAssets();
	}

	public override void beforeUpdate() {
		init();
		setOwnerByDealerCode();
		linkAssetsToContract();
		linkAssetsToOrder();
		updateOwnership();
	}

	public override void afterUpdate() {
		init();
		updateSharingRules();
		countAssets();
		removeContractClaims();
	}

	public override void afterDelete() {
		init();
		updateSharingRules();
	}

	private void linkAssetsToOrder() {
		List<VF_Asset__c> assetsToProcess = new List<VF_Asset__c>();
		Set<String> agreementNumbers = new Set<String>();
		Set<String> ctnNumbers = new Set<String>();

		for (VF_Asset__c asset : newAssets) {
			if (asset.Agreement_Number__c == null) {
				continue;
			}
			assetsToProcess.add(asset);
			agreementNumbers.add(asset.Agreement_Number__c);
			ctnNumbers.add(asset.CTN__c);
		}

		List<NetProfit_CTN__c> netProfitCTNList = [
			SELECT id, CTN_Number__c, Framework_Agreement_Id__c, Order__c
			FROM NetProfit_CTN__c
			WHERE
				CTN_Number__c IN :ctnNumbers
				AND Framework_Agreement_Id__c IN :agreementNumbers
				AND Contracted_Product__r.Framework_Id__c IN :agreementNumbers
			ORDER BY CreatedDate DESC
		];
		Map<String, List<NetProfit_CTN__c>> netProfitCTNMap = GeneralUtils.groupByStringField(netProfitCTNList, 'CTN_Number__c');

		for (VF_Asset__c asset : assetsToProcess) {
			if (netProfitCTNMap.containsKey(asset.CTN__c)) {
				List<NetProfit_CTN__c> filteredNetProfitCTNList = netProfitCTNMap.get(asset.CTN__c);
				Map<String, List<NetProfit_CTN__c>> frameworkAgreementMap = GeneralUtils.groupByStringField(
					filteredNetProfitCTNList,
					'Framework_Agreement_Id__c'
				);

				if (frameworkAgreementMap.containsKey(asset.Agreement_Number__c)) {
					asset.Order__c = frameworkAgreementMap.get(asset.Agreement_Number__c)[0].Order__c;
				}
			}
		}
	}

	/**
	 * Searches for existing Contracts or creates new ones.
	 * Links Contracts with Assets.
	 */
	private void linkAssetsToContract() {
		// Check which Assets need to be linked to the Contract
		List<VF_Asset__c> assetsToProcess = new List<VF_Asset__c>();
		Set<String> contractNumbers = new Set<String>();
		Set<String> dealerCodes = new Set<String>();
		for (VF_Asset__c asset : newAssets) {
			Boolean doProcessContract =
				GeneralUtils.isRecordFieldChanged(asset, oldAssetMap, 'BAN__c') ||
				GeneralUtils.isRecordFieldChanged(asset, oldAssetMap, 'Account__c') ||
				GeneralUtils.isRecordFieldChanged(asset, oldAssetMap, 'Contract_Number__c') ||
				GeneralUtils.isRecordFieldChanged(asset, oldAssetMap, 'CTN_Status__c');
			if (asset.Contract_Number__c == null || !doProcessContract) {
				continue;
			}
			if (asset.CTN_Status__c == Constants.VF_ASSET_CTN_STATUS_CANCELLED) {
				asset.Contract_VF__c = null;
				continue;
			}
			assetsToProcess.add(asset);
			contractNumbers.add(asset.Contract_Number__c);
			dealerCodes.add(getDealerCode(asset));
		}

		if (assetsToProcess.isEmpty()) {
			return;
		}

		// Find Contracts based on Contract Number
		Map<String, VF_Contract__c> contractsByContractName = (new ContractService()).getContractsByNumber(contractNumbers);

		// Find Dealers based on Dealer Codes
		dealerCodes.remove(null);
		Map<String, DealerService.Dealer> dealersByCode = (new DealerService()).getDealersByCode(dealerCodes);

		Map<String, VF_Contract__c> newContractsMap = new Map<String, VF_Contract__c>();
		Map<Id, VF_Contract__c> updateContractsMap = new Map<Id, VF_Contract__c>();

		for (VF_Asset__c asset : assetsToProcess) {
			if (contractsByContractName.containsKey(asset.Contract_Number__c) || newContractsMap.containsKey(asset.Contract_Number__c)) {
				VF_Contract__c contractFound = contractsByContractName.containsKey(asset.Contract_Number__c)
					? contractsByContractName.get(asset.Contract_Number__c)
					: newContractsMap.get(asset.Contract_Number__c);
				if (asset.Contract_Number__c != contractFound.Contract_Number__c) {
					asset.Error__c = 'Asset Contract_Number does not match with existing Contract_VF.Contract_Number';
					continue;
				} else {
					asset.Error__c = null;
				}
				if (contractFound.Id != null) {
					asset.Contract_VF__c = contractFound.Id;
					if (String.isNotBlank(asset.CTN__c)) {
						contractFound.Type_of_Service__c = Constants.VF_CONTRACT_MOBILE_SERVICE;
					}
					if (!updateContractsMap.containsKey(contractFound.Id)) {
						updateContractsMap.put(contractFound.Id, contractFound);
					}
				}
				continue;
			}
			if (asset.CTN_Status__c == Constants.VF_ASSET_CTN_STATUS_ACTIVE || asset.CTN_Status__c == Constants.VF_ASSET_CTN_STATUS_SUSPENDED) {
				VF_Contract__c newContract = (new ContractService()).createContractFromAsset(asset);
				String dealerCode = getDealerCode(asset);
				DealerService.Dealer dealer = dealersByCode.get(dealerCode);
				if (dealer != null) {
					newContract.Dealer_code_responsible__c = dealer.dealerInfo.Dealer_Code__c;
					newContract.Dealer_Information__c = dealer.dealerInfo.Id;
				}
				newContractsMap.put(asset.Contract_Number__c, newContract);
			} else if (asset.CTN_Status__c == Constants.VF_ASSET_CTN_STATUS_CANCELLED) {
				asset.Contract_VF__c = null;
			}
		}
		upsertContracts(newContractsMap, updateContractsMap);
	}

	/**
	 * Gets Dealer Code that should be used to define the Contract Ownership.
	 * If Retention Code is available, then it is used. Otherwise Initial Dealer Code is used.
	 * @param  asset 	Asset
	 * @return 			Dealer Code
	 */
	private String getDealerCode(VF_Asset__c asset) {
		if (asset.Retention_Dealer_code__c != null) {
			return DealerService.cleanDealerCode(asset.Retention_Dealer_code__c);
		}
		if (asset.Initial_dealer_code__c != null) {
			return DealerService.cleanDealerCode(asset.Initial_dealer_code__c);
		}
		return null;
	}

	public void upsertContracts(Map<String, VF_Contract__c> newContractsMap, Map<Id, VF_Contract__c> updateContractsMap) {
		List<VF_Asset__c> newAssets = (List<VF_Asset__c>) this.newList;
		Map<String, List<VF_Asset__c>> contractToAsset = GeneralUtils.groupByStringField(newAssets, 'Contract_Number__c');

		if (!newContractsMap.values().isEmpty()) {
			List<Database.UpsertResult> irList = Database.upsert(newContractsMap.values(), VF_Contract__c.Fields.Contract_Number__c, false);

			for (Integer i = 0; i < newContractsMap.values().size(); i++) {
				if (!irList[i].isSuccess()) {
					for (VF_Asset__c a : contractToAsset.get(newContractsMap.values()[i].Contract_Number__c)) {
						a.Error__c = String.valueOf(irList[i].getErrors()[0].getMessage()).left(255);
					}
				} else {
					for (VF_Asset__c a : contractToAsset.get(newContractsMap.values()[i].Contract_Number__c)) {
						a.Contract_VF__c = irList[i].getId();
						a.Error__c = a?.Error__c;
					}
				}
			}
		}

		if (!updateContractsMap.isEmpty()) {
			List<Database.SaveResult> urList = Database.update(updateContractsMap.values(), false);
			for (Integer i = 0; i < updateContractsMap.values().size(); i++) {
				if (!urList[i].isSuccess()) {
					for (VF_Asset__c a : contractToAsset.get(updateContractsMap.values()[i].Contract_Number__c)) {
						a.Error__c = String.valueOf(urList[i].getErrors()[0].getMessage()).left(255);
					}
				}
			}
		}
	}

	/**
	 * @author      : Jefferson Absalon
	 * @description : After the Assets have been moved we verify pending claimed contracts approval request
	 * are not left just with contracts without assets, if that the case the claim is removed and the owner
	 * is notified by an email
	 */
	private void removeContractClaims() {
		Set<Id> oldContractsIDs = new Set<Id>();
		Set<Id> claimedContractsToCheck = new Set<Id>();
		Set<Id> claimedContractsToReject = new Set<Id>();
		List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
		List<Claimed_Contract_Approval_Request__c> claimedContractsToUpdate = new List<Claimed_Contract_Approval_Request__c>();
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
		List<Approval.ProcessWorkitemRequest> listToReject = new List<Approval.ProcessWorkitemRequest>();

		for (VF_Asset__c tempOldAsset : oldAssets) {
			if (tempOldAsset.Contract_VF__c != null) {
				oldContractsIDs.add(tempOldAsset.Contract_VF__c);
			}
		}
		if (!oldContractsIDs.isEmpty()) {
			for (VF_Contract__c tempContract : [
				SELECT Id, Name, Claimed_Contract_Approval_Request__c, (SELECT Id, Name FROM Assets__r)
				FROM VF_Contract__c
				WHERE Id IN :oldContractsIDs AND Claimed_Contract_Approval_Request__c != NULL
			]) {
				Boolean hasAssets = false;
				for (VF_Asset__c a : tempContract.Assets__r) {
					hasAssets = true;
					break;
				}
				if (!hasAssets) {
					claimedContractsToCheck.add(tempContract.Claimed_Contract_Approval_Request__c);
					VF_Contract__c updateContract = new VF_Contract__c(Id = tempContract.Id, Claimed_Contract_Approval_Request__c = null);
					contractsToUpdate.add(updateContract);
				}
			}

			if (contractsToUpdate.isEmpty()) {
				return;
			}

			update contractsToUpdate;

			for (Claimed_Contract_Approval_Request__c tempContractApproval : [
				SELECT Id, Name, (SELECT Id, Name, Claimed_Contract_Approval_Request__c, Account__c, Account__r.Name FROM Contracts_VF__r)
				FROM Claimed_Contract_Approval_Request__c
				WHERE Id IN :claimedContractsToCheck
			]) {
				Boolean hasContracts = false;
				for (VF_Contract__c c : tempContractApproval.Contracts_VF__r) {
					hasContracts = true;
					break;
				}
				if (!hasContracts) {
					claimedContractsToReject.add(tempContractApproval.Id);
				}
			}
			if (!claimedContractsToReject.isEmpty()) {
				for (ProcessInstance tempProcessInstance : [
					SELECT Id, (SELECT Id, ActorId, ProcessInstanceId FROM Workitems)
					FROM ProcessInstance
					WHERE Status = 'Pending' AND TargetObjectId IN :claimedContractsToReject
				]) {
					for (ProcessInstanceWorkitem tempPIW : tempProcessInstance.Workitems) {
						Approval.ProcessWorkitemRequest rejectThis = new Approval.ProcessWorkitemRequest();
						rejectThis.setComments('Auto Reject, contracts to be claimed have no Assets left.');
						rejectThis.setAction('Reject'); //to approve use 'Approve'
						rejectThis.setWorkitemId(tempPIW.Id);
						listToReject.add(rejectThis);
					}
				}
				if (!listToReject.isEmpty()) {
					Approval.ProcessResult[] resultsFromRejections = Approval.process(listToReject);
				}
			}
		}
	}

	// set contract owner as asset owner if asset is related to contract
	private void updateOwnership() {
		Set<Id> contractIds = new Set<Id>();
		for (VF_Asset__c a : newAssets) {
			if (
				a.Contract_VF__c != null && (oldAssetMap == null || (oldAssetMap != null && a.Contract_VF__c != oldAssetMap.get(a.Id).Contract_VF__c))
			) {
				contractIds.add(a.Contract_VF__c);
			}
		}

		if (!contractIds.isEmpty()) {
			Map<Id, VF_Contract__c> contractsMap = new Map<Id, VF_Contract__c>([SELECT Id, OwnerId FROM VF_Contract__c WHERE Id IN :contractIds]);
			for (VF_Asset__c a : newAssets) {
				if (a.Contract_VF__c != null && contractsMap.containsKey(a.Contract_VF__c)) {
					a.OwnerId = contractsMap.get(a.Contract_VF__c).OwnerId;
				}
			}
		}
	}

	/**
	 * @author      : Marcel Vreuls
	 * @description : W-000683 Account Rentainablity
	 */
	private void countAssets() {
		List<AccountToUpdate__c> accountToUpdateList = new List<AccountToUpdate__c>();
		AccountToUpdate__c atu;

		for (VF_Asset__c a : newAssets) {
			atu = new AccountToUpdate__c();
			atu.AccountId__c = a.Account__c;
			accountToUpdateList.add(atu);
		}

		if (oldAssets != null) {
			for (VF_Asset__c a : oldAssets) {
				atu = new AccountToUpdate__c();
				atu.AccountId__c = a.Account__c;
				accountToUpdateList.add(atu);
			}
		}

		Database.insert(accountToUpdateList, false);
	}

	/*
	 * Description:     If this is the first asset for an account, possibly change the account type to 'customer'
	 */
	private void updateAccountData() {
		// collect accountids
		Set<Id> accountIds = new Set<Id>();
		for (VF_Asset__c vfa : newAssets) {
			accountIds.add(vfa.Account__c);
		}

		if (!accountIds.isEmpty()) {
			List<Account> accountsToUpdate = new List<Account>();
			for (Account a : [SELECT Id, Type FROM Account WHERE Id IN :accountIds AND Type = 'Prospect' AND IsPartner = FALSE]) {
				// update account type where needed
				a.Type = 'Customer';
				accountsToUpdate.add(a);
			}
			update accountsToUpdate;
		}
	}

	/**
	 * Updates the owner of the Asset based on the Dealer Code
	 */
	private void setOwnerByDealerCode() {
		// Check if Owner change is required
		Set<String> dealerCodes = new Set<String>();
		List<VF_Asset__c> assetsWithNewOwner = new List<VF_Asset__c>();
		for (VF_Asset__c asset : newAssets) {
			if (asset.Dealer_Code__c != null && asset.Contract_VF__c == null) {
				dealerCodes.add(DealerService.cleanDealerCode(asset.Dealer_Code__c));
				assetsWithNewOwner.add(asset);
			}
		}
		if (dealerCodes.isEmpty()) {
			return;
		}

		// Get Dealers
		DealerService dealerSvc = new DealerService();
		Map<String, DealerService.Dealer> dealersByCode = dealerSvc.getDealersByCode(dealerCodes);

		// Set Owners
		for (VF_Asset__c asset : assetsWithNewOwner) {
			String dealerCode = DealerService.cleanDealerCode(asset.Dealer_Code__c);
			DealerService.Dealer dealer = dealersByCode.get(dealerCode);
			if (dealer != null && dealer.dealerUser.IsActive) {
				asset.OwnerId = dealer.dealerUser.Id;
			} else {
				asset.OwnerId = GeneralUtils.vodafoneUser.Id;
			}
		}
	}

	/**
	 * @description         This method updates the sharing rules on both Asset and Account if applicable
	 * @author              Guy Clairbois
	 */
	private void updateSharingRules() {
		List<VF_Asset__share> assetSharingRulesToInsert = new List<VF_Asset__share>();
		List<AccountShare> accountSharingRulesToInsert = new List<AccountShare>();

		Set<Id> assetIds = new Set<Id>();
		Map<Id, Id> accountToAssetOwner = new Map<Id, Id>();
		Map<Id, Id> banToAssetOwner = new Map<Id, Id>();
		Map<Id, Id> accountToDeletedAssetOwner = new Map<Id, Id>();

		if (oldAssetMap == null) {
			// insert. Always add
			for (VF_Asset__c vfa : newAssets) {
				assetIds.add(vfa.Id);
				// also possibly create view rights on account if owner is different and rights do not exist yet
				accountToAssetOwner.put(vfa.Account__c, vfa.OwnerId);
				banToAssetOwner.put(vfa.Ban__c, vfa.OwnerId);
			}
		} else if (newAssets == null) {
			// delete. do nothing (cleanup will be handled by household jobs)
		} else {
			//update. check if account changed
			for (VF_Asset__c vfa : newAssets) {
				if (vfa.Account__c != oldAssetMap.get(vfa.Id).Account__c || vfa.Ban__c != oldAssetMap.get(vfa.Id).Ban__c) {
					assetIds.add(vfa.Id);
				}
				// also possibly create view rights on account if owner is different and rights do not exist yet
				if (vfa.OwnerId != oldAssetMap.get(vfa.Id).OwnerId) {
					accountToAssetOwner.put(vfa.Account__c, vfa.OwnerId);
					banToAssetOwner.put(vfa.Ban__c, vfa.OwnerId);
				}
			}
		}

		if (!assetIds.isEmpty()) {
			// remove all existing (manual) rules
			new WithoutSharingMethods().deleteSharingRules([SELECT Id FROM VF_Asset__Share WHERE ParentId IN :assetIds AND RowCause = 'Manual']);

			// fetch all assets to create new asset sharing rule if account or ban owner cannot see it yet
			for (VF_Asset__c vfa : [
				SELECT
					Id,
					OwnerId,
					Account__c,
					Account__r.OwnerId,
					Account__r.Owner.IsActive,
					Ban__c,
					Ban__r.OwnerId,
					Ban__r.Owner.IsActive,
					Contract_VF__c
				FROM VF_Asset__c
				WHERE Id IN :assetIds
			]) {
				if (vfa.Contract_VF__c == null) {
					if (vfa.Account__c != null && vfa.OwnerId <> vfa.Account__r.OwnerId && vfa.Account__r.Owner.IsActive) {
						VF_Asset__share vfas = new VF_Asset__share();
						vfas.UserOrGroupId = vfa.Account__r.OwnerId;
						vfas.ParentId = vfa.Id;
						vfas.AccessLevel = 'Read';
						assetSharingRulesToInsert.add(vfas);
					}
					if (
						vfa.Ban__c != null &&
						vfa.OwnerId <> vfa.Ban__r.OwnerId &&
						vfa.Ban__r.OwnerId <> vfa.Account__r.OwnerId &&
						vfa.Ban__r.Owner.IsActive
					) {
						VF_Asset__share vfas = new VF_Asset__share();
						vfas.UserOrGroupId = vfa.Ban__r.OwnerId;
						vfas.ParentId = vfa.Id;
						vfas.AccessLevel = 'Read';
						assetSharingRulesToInsert.add(vfas);
					}
				}
			}
		}

		if (!accountToAssetOwner.isEmpty()) {
			// create sharing rules on Asset for parent partners

			// replace ban owners by their role group and parent role groups, if applicable
			Set<Id> partnerOwnerIds = new Set<Id>();
			for (Id ownerId : accountToAssetOwner.values()) {
				partnerOwnerIds.add(ownerId);
			}

			// add dealercode hierarchy sharing
			// fetch all relevant dealercodes by owner accountid

			// create sharing for the other dealercode contacts (only 1 per parent is necessary, the others will
			// get it automatically because they are all partner superusers)
			Map<Id, Id> parentPartnerUserIds = GeneralUtils.getParentPartnerUserIdsFromUserIds(partnerOwnerIds);
			System.debug(parentPartnerUserIds);
			if (!parentPartnerUserIds.isEmpty()) {
				for (VF_Asset__c vfa : newAssets) {
					if (parentPartnerUserIds.containsKey(vfa.OwnerId)) {
						VF_Asset__share vfas = new VF_Asset__share(RowCause = Schema.VF_Asset__share.RowCause.Dealer_Hierarchy__c);
						vfas.UserOrGroupId = parentPartnerUserIds.get(vfa.OwnerId);
						vfas.ParentId = vfa.Id;
						vfas.AccessLevel = 'Read';
						assetSharingRulesToInsert.add(vfas);
					}
				}

				// add the parent owners to the owner set, preparing for account sharing rules creation
				partnerOwnerIds.addAll(parentPartnerUserIds.values());
			}

			// add account sharing rule if the asset owner cannot view the account yet
			SharingUtils.createAccountSharing(accountToAssetOwner, partnerOwnerIds, null);
		}

		new WithoutSharingMethods().insertSharingRules(assetSharingRulesToInsert);
	}

	/**
	 * @description         A without sharing util class to do the updates/inserts/deletes without interfering with authorizations
	 * @author              Guy Clairbois
	 */
	public without sharing class WithoutSharingMethods {
		public void insertSharingRules(List<sObject> sharingRulestoInsert) {
			insert sharingRulestoInsert;
		}
		public void deleteSharingRules(List<sObject> sharingRulestoDelete) {
			delete sharingRulestoDelete;
		}
	}
}
