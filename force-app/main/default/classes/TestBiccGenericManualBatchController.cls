@isTest
private class TestBiccGenericManualBatchController {
	
	@isTest static void test_method_one() {
        TestUtils.autoCommit = false;

        list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
            fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'DUNS_Number__c', 'DUNS_Number__c'));
            fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'COC_number__c', 'KVK_number__c'));
            fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'Unify_Account_Id__c', 'Unify_Ref_Id__c'));
            fsmlist.add(TestUtils.createSync('BICC_Account__c -> Account', 'Phone__c', 'Phone'));

            insert fsmlist;

		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new list<BICC_Account__c>());		
		BiccGenericManualBatchController controller = new BiccGenericManualBatchController(sc);
		PageReference pr = controller.updateBiccSobjectBatch();
		pr = controller.updateBiccSobjectSelected();
	}

}