public class COM_MACDResult {
	public List<csord__Service__c> replacedServiceList = new List<csord__Service__c>();
	public List<COM_MetadataInformation.DeliveryComponent> deliveryComponents = new List<COM_MetadataInformation.DeliveryComponent>();
	public List<csord__Service__c> serviceListToUpdate = new List<csord__Service__c>();
	public List<csord__Service__c> servicesList = new List<csord__Service__c>();
}
