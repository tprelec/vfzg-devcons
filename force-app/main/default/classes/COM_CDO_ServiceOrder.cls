@SuppressWarnings('PMD.ExcessivePublicCount')
public with sharing class COM_CDO_ServiceOrder {
	public String id;
	public String href;
	public String externalId;
	public String priority;
	public String description;
	public String category;
	public String state;
	public String orderDate;
	public String startDate;
	public String typeVal;
	public OrderItem[] orderItem;
	public class OrderItem {
		public String id;
		public String action;
		public String state;
		public String typeVal;
		public Service service;
	}
	public class Service {
		public String id;
		public String typeVal;
		public String name;
		public String serviceState;
		public ServiceCharacteristic[] serviceCharacteristic;
		public ServiceSpecification serviceSpecification;
	}
	public class ServiceSpecification {
		public String id;
		public String invariantID;
		public String version;
		public String name;
		public String typeVal;
	}
	public class ServiceCharacteristic {
		public String name;
		public String valueType;
		public GenValue value;
	}
	public class GenValue {
		public String typeVal;
		public String valueOf;
	}
}
