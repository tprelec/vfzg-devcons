@isTest
public with sharing class TestCOM_DXL_createBSSOrder {
	@testSetup
	static void setup() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		Account testAccount = CS_DataTest.createAccount('Test Account');
		testAccount.LG_ExternalID__c = '1234456';
		insert testAccount;

		csconta__Billing_Account__c billingAcc = new csconta__Billing_Account__c();
		billingAcc.csconta__Account__c = testAccount.Id;
		billingAcc.LG_PaymentType__c = 'Bank Transfer';
		billingAcc.LG_HouseNumber__c = '12';
		billingAcc.LG_BillingAccountName__c = 'Test Billing Account';
		billingAcc.LG_MandateStartDate__c = System.today();
		insert billingAcc;

		Contact testContact = CS_DataTest.createContact('Test Name', 'TestLastName', 'Developer', 'mail@address.com', testAccount.Id);
		testContact.Language__c = 'English';
		testContact.Title = 'Ms';
		testContact.Phone = '0031622334466';
		insert testContact;

		Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', UserInfo.getUserId());
		testOpp.csordtelcoa__Change_Type__c = 'Change';
		testOpp.Name = 'testOpp';
		testOpp.StageName = 'Ready for Order';
		testOpp.CloseDate = System.today();
		testOpp.AccountId = testAccount.Id;
		testOpp.Main_Contact_Person__c = testContact.Id;
		insert testOpp;

		cscfga__Product_Basket__c testBasket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
		testBasket.Name = 'testBasket';
		testBasket.cscfga__Opportunity__c = testOpp.Id;
		testBasket.LG_Selected_Billing_Account__c = billingAcc.id;
		insert testBasket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c businessInternetDef = CS_DataTest.createProductDefinition('Business Internet');
		businessInternetDef.RecordTypeId = productDefinitionRecordType;
		businessInternetDef.Product_Type__c = 'Fixed';
		insert businessInternetDef;

		cscfga__Product_Definition__c accessDef = CS_DataTest.createProductDefinition('Access');
		accessDef.RecordTypeId = productDefinitionRecordType;
		accessDef.Product_Type__c = 'Fixed';
		insert accessDef;

		cscfga__Product_Configuration__c businessInternetConf = CS_DataTest.createProductConfiguration(
			businessInternetDef.Id,
			'Business Internet',
			testBasket.Id
		);
		insert businessInternetConf;

		cscfga__Product_Configuration__c coax10030Conf = CS_DataTest.createProductConfiguration(
			accessDef.Id,
			'Coax aansluiting 100/30 Mbps',
			testBasket.Id
		);
		insert coax10030Conf;

		csord__Order__c orderProvide = new csord__Order__c();
		orderProvide.csord__Identification__c = 'Order_' + String.valueOf(testBasket.Id);
		insert orderProvide;

		Test.setMock(WebServiceMock.class, new TestUtilMockServices());

		Site__c site = CS_DataTest.createSite('Utrecht Lestraat 12', testAccount, '3572RE', 'Lestraat', 'Utrecht', 12);
		site.Footprint__c = null;
		site.Site_House_Number_Suffix__c = 'A';
		site.Country__c = 'NL';
		insert site;

		COM_Delivery_Order__c deliveryOrderProvide = CS_DataTest.createDeliveryOrder('Delivery order provide', orderProvide.Id, false);
		insert deliveryOrderProvide;

		csord__Subscription__c businessInternetSub = CS_DataTest.createSubscription(businessInternetConf.Id);
		businessInternetSub.csord__Identification__c = 'Business_Internet_Sub_' + String.valueOf(testBasket.Id);
		businessInternetSub.csord__Status__c = 'Subscription created';
		businessInternetSub.csord__Order__c = orderProvide.Id;
		insert businessInternetSub;

		csord__Service__c businessInternetService = CS_DataTest.createService(
			orderProvide.Id,
			deliveryOrderProvide.Id,
			'Business Internet',
			testBasket.Id,
			businessInternetSub.Id,
			site.Id,
			null,
			businessInternetConf.Id
		);
		businessInternetService.Implemented_Date__c = Date.today();
		insert businessInternetService;

		csord__Service__c coaxAansluiting10030Service = CS_DataTest.createService(
			orderProvide.Id,
			deliveryOrderProvide.Id,
			'Coax aansluiting 100/30 Mbps',
			testBasket.Id,
			businessInternetSub.Id,
			site.Id,
			businessInternetService.Id,
			coax10030Conf.Id
		);
		coaxAansluiting10030Service.Implemented_Date__c = Date.today();
		insert coaxAansluiting10030Service;

		csord__Service_Line_Item__c coaxAansluiting10030OneOff = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Coax aansluiting- 100/30 Mbps',
			coaxAansluiting10030Service.Id,
			false,
			0,
			COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE[0],
			'Coax aansluiting- 100/30 Mbps OF'
		);
		coaxAansluiting10030OneOff.Assigned_Product_Id__c = 'APID-OF';

		insert coaxAansluiting10030OneOff;
		csord__Service_Line_Item__c coaxAansluiting10030Rec = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'coax-100-30-24m ATL - recurring',
			coaxAansluiting10030Service.Id,
			true,
			0,
			COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE[0],
			'coax-100-30-24m ATL - recurring'
		);
		coaxAansluiting10030Rec.Assigned_Product_Id__c = 'APID-REC';
		insert coaxAansluiting10030Rec;

		csord__Service_Line_Item__c coaxOFDiscount = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Coax aansluiting discount',
			coaxAansluiting10030Service.Id,
			false,
			5.00,
			COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE_DISCOUNT[0],
			'Coax aansluiting discount'
		);
		insert coaxOFDiscount;

		Attachment att = new Attachment();
		att.ParentId = coaxAansluiting10030Service.Id;
		att.Body = Blob.valueOf(generateJSONAttachment2BillingItems(coaxAansluiting10030Service.Id, coax10030Conf.Id));
		att.Name = COM_DXL_Constants.COM_DXL_BSS_SPECIFICATION_NAME;
		insert att;

		////CHANGE SCENARIO
		Opportunity testOppChange = CS_DataTest.createOpportunity(testAccount, 'Test Opp Change', UserInfo.getUserId());
		testOppChange.csordtelcoa__Change_Type__c = 'Change';
		testOppChange.Name = 'testOpp_Change';
		testOppChange.StageName = 'Ready for Order';
		testOppChange.CloseDate = System.today();
		testOppChange.AccountId = testAccount.Id;
		testOppChange.Main_Contact_Person__c = testContact.Id;
		insert testOppChange;

		cscfga__Product_Basket__c testBasketChange = CS_DataTest.createProductBasket(testOppChange, 'Test Basket Change');
		testBasketChange.Name = 'testBasketChange';
		testBasketChange.cscfga__Opportunity__c = testOppChange.Id;
		testBasketChange.LG_Selected_Billing_Account__c = billingAcc.id;
		insert testBasketChange;

		csord__Order__c orderChange = new csord__Order__c();
		orderChange.csord__Identification__c = 'Order_' + String.valueOf(testBasketChange.Id);
		insert orderChange;

		COM_Delivery_Order__c deliveryOrderChange = CS_DataTest.createDeliveryOrder('ChangeOrder', orderChange.Id, false);
		insert deliveryOrderChange;

		cscfga__Product_Configuration__c internetConfChange = CS_DataTest.createProductConfiguration(
			businessInternetDef.Id,
			'Business Internet',
			testBasketChange.Id
		);
		insert internetConfChange;

		cscfga__Product_Configuration__c coax70030ConfChange = CS_DataTest.createProductConfiguration(
			accessDef.Id,
			'Coax aansluiting 700/30 Mbps',
			testBasketChange.Id
		);
		insert coax70030ConfChange;

		csord__Subscription__c businessInternetSubChange = CS_DataTest.createSubscription(internetConfChange.Id);
		businessInternetSubChange.csord__Identification__c = 'Business_Internet_Sub_' + String.valueOf(testBasketChange.Id);
		businessInternetSubChange.csord__Status__c = 'Subscription created';
		businessInternetSubChange.csord__Order__c = orderChange.Id;
		insert businessInternetSubChange;

		csord__Service__c coax70050ServiceChange = CS_DataTest.createService(
			orderChange.Id,
			deliveryOrderChange.Id,
			'Coax aansluiting 700/50 Mbps',
			testBasketChange.Id,
			businessInternetSubChange.Id,
			site.Id,
			null,
			coax70030ConfChange.Id
		);

		coax70050ServiceChange.csordtelcoa__Replaced_Service__c = coaxAansluiting10030Service.Id;
		coax70050ServiceChange.csordtelcoa__Delta_Status__c = COM_DXL_Constants.COM_DELTA_STATUS_CONTINUING;
		coax70050ServiceChange.Implemented_Date__c = Date.today();
		insert coax70050ServiceChange;

		csord__Service_Line_Item__c coax70050SLIOneOffChange = CS_DataTest.createServiceLineItem(
			testBasketChange.Id,
			'Coax aansluiting- 700/50 Mbps',
			coax70050ServiceChange.Id,
			false,
			0,
			COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE[0],
			'Coax aansluiting- 700/50 Mbps OF'
		);
		insert coax70050SLIOneOffChange;

		csord__Service_Line_Item__c coax70050SLIRecChange = CS_DataTest.createServiceLineItem(
			testBasketChange.Id,
			'coax-700-50-24m ATL - recurring',
			coax70050ServiceChange.Id,
			true,
			0,
			COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE[0],
			'coax-700-50-24m ATL - recurring'
		);
		insert coax70050SLIRecChange;

		Attachment attChange = new Attachment();
		attChange.ParentId = coax70050ServiceChange.Id;
		attChange.Body = Blob.valueOf(generateJSONAttachment2BillingItems(coax70050ServiceChange.Id, coax70030ConfChange.Id));
		attChange.Name = COM_DXL_Constants.COM_DXL_BSS_SPECIFICATION_NAME;
		insert attChange;

		////Cease SCENARIO
		Opportunity testOppCease = CS_DataTest.createOpportunity(testAccount, 'Test Opp Cease', UserInfo.getUserId());
		testOppCease.csordtelcoa__Change_Type__c = 'Cease';
		testOppCease.Name = 'testOpp_Cease';
		testOppCease.StageName = 'Ready for Order';
		testOppCease.CloseDate = System.today();
		testOppCease.AccountId = testAccount.Id;
		testOppCease.Main_Contact_Person__c = testContact.Id;
		insert testOppCease;

		cscfga__Product_Basket__c testBasketCease = CS_DataTest.createProductBasket(testOppCease, 'Test Basket Cease');
		testBasketCease.Name = 'testBasketCease';
		testBasketCease.cscfga__Opportunity__c = testOppCease.Id;
		testBasketCease.LG_Selected_Billing_Account__c = billingAcc.id;
		insert testBasketCease;

		csord__Order__c orderCease = new csord__Order__c();
		orderCease.csord__Identification__c = 'Order_' + String.valueOf(testBasketCease.Id);
		insert orderCease;

		COM_Delivery_Order__c deliveryOrderCease = CS_DataTest.createDeliveryOrder('CeaseOrder', orderCease.Id, false);
		insert deliveryOrderCease;

		cscfga__Product_Configuration__c internetConfCease = CS_DataTest.createProductConfiguration(
			businessInternetDef.Id,
			'Business Internet',
			testBasketCease.Id
		);
		insert internetConfCease;

		cscfga__Product_Configuration__c coax10030ConfCease = CS_DataTest.createProductConfiguration(
			accessDef.Id,
			'Coax aansluiting 100/30 Mbps',
			testBasketCease.Id
		);
		insert coax10030ConfCease;

		csord__Subscription__c businessInternetSubCease = CS_DataTest.createSubscription(internetConfCease.Id);
		businessInternetSubCease.csord__Identification__c = 'Business_Internet_Sub_' + String.valueOf(testBasketCease.Id);
		businessInternetSubCease.csord__Status__c = 'Subscription created';
		businessInternetSubCease.csord__Order__c = orderCease.Id;
		insert businessInternetSubCease;

		csord__Service__c coax10030ServiceCease = CS_DataTest.createService(
			orderCease.Id,
			deliveryOrderCease.Id,
			'Coax aansluiting 100/30 Mbps',
			testBasketCease.Id,
			businessInternetSubCease.Id,
			site.Id,
			null,
			coax10030ConfCease.Id
		);

		coax10030ServiceCease.csordtelcoa__Replaced_Service__c = coaxAansluiting10030Service.Id;
		coax10030ServiceCease.csordtelcoa__Delta_Status__c = COM_DXL_Constants.COM_DELTA_STATUS_DELETION;
		coax10030ServiceCease.csord__Deactivation_Date__c = Date.today();
		insert coax10030ServiceCease;

		csord__Service_Line_Item__c coax10030SLIOneOffCease = CS_DataTest.createServiceLineItem(
			testBasketChange.Id,
			'Coax aansluiting- 100/30 Mbps',
			coax10030ServiceCease.Id,
			false,
			0,
			COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE[0],
			'Coax aansluiting- 100/30 Mbps OF'
		);
		insert coax10030SLIOneOffCease;

		csord__Service_Line_Item__c coax10030SLIRecCease = CS_DataTest.createServiceLineItem(
			testBasketCease.Id,
			'coax-100-30-24m ATL - recurring',
			coax10030ServiceCease.Id,
			true,
			0,
			COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE[0],
			'coax-100-30-24m ATL - recurring'
		);
		insert coax10030SLIRecCease;

		Attachment attCease = new Attachment();
		attCease.ParentId = coax10030ServiceCease.Id;
		attCease.Body = Blob.valueOf(generateJSONAttachment2BillingItems(coax10030ServiceCease.Id, coax10030ConfCease.Id));
		attCease.Name = COM_DXL_Constants.COM_DXL_BSS_SPECIFICATION_NAME;
		insert attCease;
	}

	private static String generateJSONAttachment2BillingItems(Id serviceId, Id productConfigurationId) {
		String jsonText =
			'{ "specifications" : [ { "version" : "1", "status" : "Created", "startDate" : "", "specification" : "' +
			COM_WebServiceConfig.getUUID() +
			'", "productConfigurationId" : "' +
			+productConfigurationId +
			'", "name" : "createBSSOrderInternet", "metadata" : { }, "instanceId" : "", "includeBilling" : false, "guid" : "' +
			+COM_WebServiceConfig.getUUID() +
			'", "endDate" : "", "description" : "Specification for createBSSOrder request to Unify", "code" : "createBSSOrder", "attributes" : { "recurringBITag4" : "recBITag4a", "recurringBITag3" : "recBITag3a", "recurringBITag2" : "recBITag2a", "recurringBITag1" : "recBITag1a", "recurringChargeCode" : "code1234", "recurringChargeDescription" : "Test+ recurring", "oneOffBITag4" : "oneOffBITag4b", "oneOffBITag3" : "oneOffBITag3b", "oneOffChargeDescription" : "codeOF1234", "oneOffChargeCode" : "Test oneOff", "oneOffBITag2" : "oneOffBITag2b", "oneOffBITag1" : "oneOffBITag1b", "quantity" : "1.00", "baseOTC" : "15.00", "baseMRC" : 5.00 }, "additionalAttributes" : { } } ], "serviceId" : "' +
			+serviceId +
			+'", "legacyAttributes" : [ ] }';
		return jsonText;
	}

	@isTest
	static void testProvideRequest() {
		COM_Delivery_Order__c deliveryOrder = [SELECT Id FROM COM_Delivery_Order__c WHERE name = 'Delivery order provide'];
		COM_MockWebService.setTestMockResponse(200, 'OK', '{"status":"Successful"}');
		Test.startTest();
		COM_DXL_createBSSOrder.sendToBilling(new List<Id>{ deliveryOrder.Id });
		Test.stopTest();

		COM_DXL_Integration_Log__c log = [SELECT Error__c, Affected_Objects__c, Type__c FROM COM_DXL_Integration_Log__c];
		System.assertEquals(log.Type__c, 'createBSSOrder', '1 COM DXL Integration should have been created!');
	}

	@isTest
	static void testChangeRequest() {
		COM_Delivery_Order__c deliveryOrderChange = [SELECT Id FROM COM_Delivery_Order__c WHERE name = 'ChangeOrder'];
		COM_MockWebService.setTestMockResponse(200, 'OK', '{"status":"Successful"}');

		Test.startTest();

		COM_DXL_createBSSOrder.sendToBilling(new List<Id>{ deliveryOrderChange.Id });

		Test.stopTest();
		COM_DXL_Integration_Log__c log = [SELECT Error__c, Affected_Objects__c, Type__c FROM COM_DXL_Integration_Log__c];
		System.assertEquals(log.Type__c, 'createBSSOrder', '1 COM DXL Integration should have been created!');

		List<csord__Service_Line_Item__c> sliChange = [
			SELECT Id, Assigned_Product_Id__c, csord__line_item_type__c, csord__Is_Recurring__c
			FROM csord__Service_Line_Item__c
			WHERE csord__Service__r.COM_Delivery_Order__c = :deliveryOrderChange.Id
		];
		for (csord__Service_Line_Item__c sli : sliChange) {
			if (sli.csord__Is_Recurring__c && COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE.contains(sli.csord__line_item_type__c)) {
				System.assertEquals(sli.Assigned_Product_Id__c, 'APID-REC', 'Recurring SLI assigned product mapped incorrectly - CHANGE');
			}
			if (!sli.csord__Is_Recurring__c && COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE.contains(sli.csord__line_item_type__c)) {
				System.assertEquals(sli.Assigned_Product_Id__c, 'APID-OF', 'One Off SLI  assigned product mapped incorrectly - CHANGE');
			}
		}
	}

	@isTest
	static void testCeaseRequest() {
		COM_Delivery_Order__c deliveryOrderCease = [SELECT Id FROM COM_Delivery_Order__c WHERE name = 'CeaseOrder'];

		COM_MockWebService.setTestMockResponse(200, 'OK', '{"status":"Successful"}');

		Test.startTest();

		COM_DXL_createBSSOrder.sendToBilling(new List<Id>{ deliveryOrderCease.Id });

		Test.stopTest();
		COM_DXL_Integration_Log__c log = [SELECT Error__c, Affected_Objects__c, Type__c FROM COM_DXL_Integration_Log__c];
		System.assertEquals(log.Type__c, 'createBSSOrder', '1 COM DXL Integration should have been created!');

		List<csord__Service_Line_Item__c> sliCease = [
			SELECT Id, Assigned_Product_Id__c, csord__line_item_type__c, csord__Is_Recurring__c
			FROM csord__Service_Line_Item__c
			WHERE csord__Service__r.COM_Delivery_Order__c = :deliveryOrderCease.Id
		];
		for (csord__Service_Line_Item__c sli : sliCease) {
			if (sli.csord__Is_Recurring__c && COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE.contains(sli.csord__line_item_type__c)) {
				System.assertEquals(sli.Assigned_Product_Id__c, 'APID-REC', 'Recurring SLI assigned product mapped incorrectly - CEASE');
			}
			if (!sli.csord__Is_Recurring__c && COM_DXL_Constants.COM_DXL_BSS_SLI_CHARGE.contains(sli.csord__line_item_type__c)) {
				System.assertEquals(sli.Assigned_Product_Id__c, 'APID-OF', 'One Off SLI  assigned product mapped incorrectly - CEASE');
			}
		}
	}
}
