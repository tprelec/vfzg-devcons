@isTest
private class TestCS_ServiceInfoProvider {
	@TestSetup
	private static void init() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		Opportunity testOppty = CS_DataTest.createOpportunity(testAccount, 'Test Opportunity', UserInfo.getUserId());
		insert testOppty;

		cscfga__Product_Basket__c testBasket = CS_DataTest.createProductBasket(testOppty, 'My basket');
		testBasket.Block_Expiration__c = false;
		testBasket.Contract_duration_Mobile__c = '12';
		insert testBasket;

		cscfga__Product_Definition__c pdInternet = CS_DataTest.createProductDefinitionRegular('Internet');
		insert pdInternet;

		cscfga__Product_Configuration__c pcInternet = CS_DataTest.createProductConfiguration(pdInternet.Id, 'Internet', testBasket.Id);
		insert pcInternet;

		csord__Subscription__c subscription1 = CS_DataTest.createSubscription(pcInternet.Id);
		subscription1.csord__Identification__c = 's1';
		insert subscription1;

		csord__Service__c service1 = CS_DataTest.createService(pcInternet.Id, subscription1.Id, 'serv1');
		service1.csord__Status__c = 'Service created';
		insert service1;
	}

	@isTest
	private static void testGetData() {
		cscfga__Product_Configuration__c pc = [SELECT Id FROM cscfga__Product_Configuration__c LIMIT 1];
		CS_ServiceInfoProvider.ServiceInfo inputItem = new CS_ServiceInfoProvider.ServiceInfo();
		inputItem.configurationId = pc.Id;
		inputItem.configurationGuid = '3b65dcf2-6ef8-4ce3-aa5f-ce2577e57e74';
		List<CS_ServiceInfoProvider.ServiceInfo> serviceInfos = new List<CS_ServiceInfoProvider.ServiceInfo>{ inputItem };
		Map<String, Object> inputData = new Map<String, Object>();
		inputData.put('configurationInput', JSON.serialize(serviceInfos));

		Test.startTest();
		Map<String, Object> result = CS_ServiceInfoProvider.getData(inputData);
		Test.stopTest();

		Boolean statusResult = (Boolean) result.get('status');
		System.assertEquals(true, statusResult, 'Operation should be successful');
	}
}
