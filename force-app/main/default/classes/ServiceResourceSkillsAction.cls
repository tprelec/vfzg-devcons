/**
 * @description
 * @author       mcubias@vodafoneziggo.com
 * @since        2022.10.05
 */
public without sharing class ServiceResourceSkillsAction {
	/**
	 * @description
	 * @param skills
	 * @return
	 */
	@InvocableMethod(label='Get Service Resources by Skill' description='Return a list of Service Resource Ids as csv' category='Scheduler')
	public static List<String> getServiceResourceIds(List<String> skills) {
		List<String> serviceResourceIds = new List<String>();
		if (!skills.isEmpty()) {
			String skill = skills.get(0);
			for (ServiceResourceSkill srs : [
				SELECT IsDeleted, ServiceResourceId, EffectiveStartDate
				FROM ServiceResourceSkill
				WHERE Skill.DeveloperName = :skill
			]) {
				serviceResourceIds.add(srs.ServiceResourceId);
			}
		}
		return new List<String>{ String.join(serviceResourceIds, ',') };
	}
}
