@isTest
public with sharing class TestUnicaUpdateBatch {
    @testSetup
	static void setup() {
        List<Unica_Update__c> listUU = new List<Unica_Update__c>();
        Unica_Update__c oItem = new Unica_Update__c();
        oItem.Participant_ID__c = '1';
        oItem.Action__c = 'Update';
        oItem.Status__c = 'XXX;Retained;XXX;Retained;XXX;Retained';
        listUU.add(oItem);

        Unica_Update__c oItem1 = new Unica_Update__c();
        oItem1.Participant_ID__c = '2';
        oItem1.Action__c = 'Delete';
        listUU.add(oItem1);

        Unica_Update__c oItem2 = new Unica_Update__c();
        listUU.add(oItem2);

        Unica_Update__c oItem3 = new Unica_Update__c();
        oItem3.Participant_ID__c = '2';
        listUU.add(oItem3);
		insert listUU;

        TestUtils.autoCommit=false;
        List<Lead> leads = new List<Lead>();
        Lead lead = TestUtils.createLead();
        lead.NTA_Id__c = '1';
        leads.add(lead);

        Lead lead1 = TestUtils.createLead();
        lead1.NTA_Id__c = '2';
        leads.add(lead1);

        Lead lead2 = TestUtils.createLead();
        leads.add(lead2);

        insert leads;
    }

    @isTest
    static void testBatch() {
        Test.startTest();
        UnicaUpdateBatch controller = new  UnicaUpdateBatch();
        Database.executeBatch(controller, 100);

        Test.stopTest();
        System.assertEquals(3, [SELECT Id FROM Lead ].size(), '');
    }
}