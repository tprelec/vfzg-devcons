public with sharing class DocumentDefinitionTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<csclm__Document_Definition__c> newProducts = (List<csclm__Document_Definition__c>) this.newList;
		Sequence seq = new Sequence('Document Definition');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (csclm__Document_Definition__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
