public class SAPBatchExecuteController {

	public SAPBatchExecuteController(ApexPages.StandardController ignored) {}

	public PageReference callBatch(){
		database.executebatch(new SAPHardwareAutomation());
		return new pageReference('/a0U');
	}
}