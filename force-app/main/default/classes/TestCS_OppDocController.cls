@isTest
private class TestCS_OppDocController {
	@TestSetup
	static void makeData() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);

		TestUtils.createCompleteContract();

		Opportunity opp = TestUtils.theOpportunity;

		Contract_Conditions__c contractConditionForProduct = createContractCondition(
			false,
			'Dienstbeschrijving',
			'',
			'Dutch'
		);
		List<Contract_Conditions__c> contractConditions = new List<Contract_Conditions__c>{
			contractConditionForProduct,
			createContractCondition(false, 'AVW', '', 'Dutch'),
			createContractCondition(false, 'AVW', '', 'English'),
			createContractCondition(false, 'AVW', 'Test', 'Dutch'),
			createContractCondition(false, 'AVW', 'This proposition does not match', 'Dutch')
		};
		insert contractConditions;
		List<Id> contentDocuments = createContentVersionAndGetContentDocument(opp.Id, 3);
		createContentDocumentLinks(contractConditions, contentDocuments);

		csclm__Document_Definition__c def = new csclm__Document_Definition__c();
		def.csclm__Document_Type__c = 'Contract';
		def.csclm__Linked_Object__c = 'Opportunity';
		def.ExternalID__c = 'SomeId1';
		insert def;

		csclm__Document_Template__c doc = new csclm__Document_Template__c();
		doc.csclm__Effective_From__c = Date.today().addDays(-1);
		doc.csclm__Active__c = true;
		doc.Name = 'Test Doc';
		doc.csclm__Valid__c = true;
		doc.csclm__Document_Generation_Plugin__c = 'Default';
		doc.csclm__Template_Reference__c = 'CustomAgreementPDF';
		doc.csclm__Version__c = '1';
		doc.csclm__Document_Definition__c = def.Id;
		doc.ExternalID__c = 'SomeId1';
		insert doc;

		csclm__Agreement__c agree = new csclm__Agreement__c();
		agree.csclm__Opportunity__c = opp.Id;
		agree.csclm__Output_Format__c = 'pdf';
		agree.Name = 'Test123';
		agree.csclm__Document_Template__c = doc.Id;
		insert agree;

		List<Attachment> attList = new List<Attachment>();
		Attachment att = new Attachment();
		att.ParentId = agree.Id;
		att.Name = 'Test 123';
		att.Body = Blob.valueOf('123');
		attList.add(att);
		Attachment att2 = new Attachment();
		att2.ParentId = agree.Id;
		att2.Name = 'Test BPA';
		att2.Body = Blob.valueOf('BPA');
		attList.add(att2);
		insert attList;

		Product2 productWithContractCondition = [SELECT Id FROM Product2 LIMIT 1];
		insert new Contract_Condition_Product_Association__c(
			Contract_Conditions__c = contractConditionForProduct.Id,
			Product2__c = productWithContractCondition.Id
		);

		List<OpportunityLineItem> oppLineItems = [
			SELECT Id, Proposition__c, Category__c
			FROM OpportunityLineItem
			LIMIT 2
		];
		oppLineItems[0].Proposition__c = 'Test';
		oppLineItems[0].Category__c = 'Test';
		oppLineItems[1].Product2 = productWithContractCondition;
		update oppLineItems;
	}

	@isTest
	static void testGetDocuments() {
		Id oppId = [SELECT Id FROM Opportunity LIMIT 1].Id;
		List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion];

		Test.startTest();
		List<CS_DocItem> docList = CS_OppDocController.getDocuments(oppId);
		Test.stopTest();
		System.assertEquals(5, docList.size(), 'There should be five doc items.');

		Set<Id> contentVersionsInDocItems = new Set<Id>();
		for (CS_DocItem docItem : docList) {
			if (String.isBlank(docItem.serviceDescriptionUrl)) {
				continue;
			}
			String cvId = docItem.serviceDescriptionUrl.split('/download/')[1].split('\\?')[0];
			contentVersionsInDocItems.add(cvId);
		}

		Boolean contentVersionIsInDocList = true;
		for (ContentVersion cv : contentVersions) {
			if (!contentVersionsInDocItems.contains(cv.Id)) {
				contentVersionIsInDocList = false;
			}
		}
		System.assertEquals(
			false,
			contentVersionIsInDocList,
			'Not all ContentVersions should be included in the docItems.'
		);
	}

	@isTest
	static void testGetDocumentsNotDutch() {
		Account acc = new Account(
			Name = 'Test Account'
		);
		insert acc;

		Opportunity opp2 = new Opportunity(
			Name = 'Test Opp',
			AccountId = acc.Id,
			StageName = 'Prospect',
			CloseDate = Date.today()
		);
		insert opp2;

		insert new cscfga__Product_Basket__c(
			cscfga__Opportunity__c = opp2.Id,
			Contract_Language__c = 'English'
		);

		List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion];

		Test.startTest();
		List<CS_DocItem> docList = CS_OppDocController.getDocuments(opp2.Id);
		Test.stopTest();
		System.assertEquals(1, docList.size(), 'One document should exist.');

		Set<Id> contentVersionsInDocItems = new Set<Id>();
		for (CS_DocItem docItem : docList) {
			if (String.isBlank(docItem.serviceDescriptionUrl)) {
				continue;
			}
			String cvId = docItem.serviceDescriptionUrl.split('/download/')[1].split('\\?')[0];
			contentVersionsInDocItems.add(cvId);
		}

		Boolean contentVersionIsInDocList = true;
		for (ContentVersion cv : contentVersions) {
			if (!contentVersionsInDocItems.contains(cv.Id)) {
				contentVersionIsInDocList = false;
			}
		}
		System.assertEquals(
			false,
			contentVersionIsInDocList,
			'Not all ContentVersions should be included in the docItems.'
		);
	}

	private static Contract_Conditions__c createContractCondition(
		Boolean doInsert,
		String type,
		String proposition,
		String language
	) {
		Contract_Conditions__c result = new Contract_Conditions__c(
			Name = 'TestCond123',
			Channel__c = 'Direct/Indirect',
			Proposition1__c = proposition,
			Type__c = type,
			URL_Link__c = 'www.test.com',
			Start_Date__c = Date.today(),
			End_Date__c = Date.today(),
			Version_Number__c = 1.0,
			Language__c = language
		);
		if (doInsert) {
			insert result;
		}
		return result;
	}

	private static List<Id> createContentVersionAndGetContentDocument(Id oppId, Integer n) {
		List<ContentVersion> contentVersions = new List<ContentVersion>();
		for (Integer i = 0; i < n; i++) {
			contentVersions.add(
				new ContentVersion(
					PathOnClient = 'Test.pdf',
					Title = 'Testing',
					OwnerId = UserInfo.getUserId(),
					FirstPublishLocationId = UserInfo.getUserId(),
					VersionData = Blob.valueOf('Test me!')
				)
			);
		}
		contentVersions.add(
			new ContentVersion(
				PathOnClient = 'Test.pdf',
				Title = 'Testing',
				OwnerId = UserInfo.getUserId(),
				FirstPublishLocationId = oppId,
				VersionData = Blob.valueOf('Test me!')
			)
		);
		insert contentVersions;

		List<Id> ret = new List<Id>();
		for (ContentVersion cv : [
			SELECT Id, ContentDocumentId
			FROM ContentVersion
			WHERE Id IN :contentVersions
		]) {
			ret.add(cv.ContentDocumentId);
		}
		return ret;
	}

	private static void createContentDocumentLinks(
		List<Contract_Conditions__c> contractConditions,
		List<Id> contentDocuments
	) {
		List<ContentDocumentLink> cdls = new List<ContentDocumentLink>{
			new ContentDocumentLink(
				LinkedEntityId = contractConditions[0].Id,
				ContentDocumentId = contentDocuments[0]
			),
			new ContentDocumentLink(
				LinkedEntityId = contractConditions[0].Id,
				ContentDocumentId = contentDocuments[1]
			),
			new ContentDocumentLink(
				LinkedEntityId = contractConditions[1].Id,
				ContentDocumentId = contentDocuments[2]
			)
		};
		insert cdls;
	}
}