public with sharing class COM_MACDServiceFieldMapping {
	public String commaSeparatedFieldsWithStartingComma = '';
	public List<String> fieldList = new List<String>();
	public COM_MACDServiceFieldMapping() {
		for (Schema.FieldSetMember fieldSetMember : fetchFieldSetFields(COM_Constants.MACD_FIELD_MAPPING_FIELDSET_NAME)) {
			commaSeparatedFieldsWithStartingComma += COM_Constants.COMMA_SIGN + fieldSetMember.getFieldPath();
			commaSeparatedFieldsWithStartingComma +=
				COM_Constants.COMMA_SIGN +
				COM_Constants.REPLACED_SERVICE_RELATION +
				fieldSetMember.getFieldPath();
			fieldList.add(fieldSetMember.getFieldPath());
		}
	}

	@TestVisible
	private List<Schema.FieldSetMember> fetchFieldSetFields(String fieldSetName) {
		List<Schema.FieldSetMember> result = null;
		Schema.FieldSet fieldSet = Schema.SObjectType.csord__Service__c.fieldSets.getMap().get(fieldSetName);

		if (fieldSet != null) {
			result = fieldSet.getFields();
		}

		return result;
	}

	public void mapFieldsFromReplacedToNewServices(List<csord__Service__c> serviceList) {
		List<csord__Service__c> filteredServiceList = new List<csord__Service__c>();
		for (csord__Service__c service : serviceList) {
			if (
				(service.COM_Delivery_Order__r == null || !service.COM_Delivery_Order__r.MACD_move__c) &&
				service.csordtelcoa__Replaced_Service__r != null
			) {
				filteredServiceList.add(service);
			}
		}

		for (csord__Service__c filteredService : filteredServiceList) {
			for (String field : fieldList) {
				filteredService.put(field, filteredService.csordtelcoa__Replaced_Service__r.get(field));
			}
		}

		update filteredServiceList;
	}
}
