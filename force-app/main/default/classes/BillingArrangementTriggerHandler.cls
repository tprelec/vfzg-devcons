/**
 * @description			This is the trigger handler for the Billing_Arrangement__c sObject.
 * @author				Guy Clairbois
 */
public with sharing class BillingArrangementTriggerHandler extends TriggerHandler {

	/**
	 * @description			This handles the before insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeInsert(){
		List<Billing_Arrangement__c> newBars = (List<Billing_Arrangement__c>) this.newList;

	}

    
	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterInsert(){
		List<Billing_Arrangement__c> newBars = (List<Billing_Arrangement__c>) this.newList;	

		updateFAFields(newBars,null);
	}

	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeUpdate(){
		List<Billing_Arrangement__c> newBars = (List<Billing_Arrangement__c>) this.newList;	
		Map<Id,Billing_Arrangement__c> oldBarMap = (Map<Id,Billing_Arrangement__c>) this.oldMap;
	}	
	
	/**
	 * @description			This handles the after update trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void afterUpdate(){
		List<Billing_Arrangement__c> newBars = (List<Billing_Arrangement__c>) this.newList;	
		Map<Id,Billing_Arrangement__c> oldBarMap = (Map<Id,Billing_Arrangement__c>) this.oldMap;
		
		updateFAFields(newBars,oldBarMap);
	}	

	private void updateFAFields(List<Billing_Arrangement__c> newBars, Map<Id,Billing_Arrangement__c> oldBarMap){
		Map<Id,Financial_Account__c> faToUpdate = new Map<Id,Financial_Account__c>();
		for(Billing_Arrangement__c bar : newBars){
			Financial_Account__c fa = new Financial_Account__c(Id=bar.Financial_Account__c);
			Boolean changeFound = false;
			if(oldBarMap == null || bar.Payment_Method__c != oldBarMap.get(bar.Id).Payment_Method__c){
				fa.Payment_method__c = bar.Payment_Method__c;
				changeFound = true;
			}
			if(oldBarMap == null || bar.Bank_Account_Name__c != oldBarMap.get(bar.Id).Bank_Account_Name__c){
				fa.Bank_Account_Name__c = bar.Bank_Account_Name__c;
				changeFound = true;
			}
			if(oldBarMap == null || bar.Bank_Account_Number__c != oldBarMap.get(bar.Id).Bank_Account_Number__c){
				fa.Bank_Account_Number__c = bar.Bank_Account_Number__c;
				changeFound = true;
			}		
			if(changeFound) faToUpdate.put(fa.Id,fa);
		}
		SharingUtils.updateObjectsWithoutSharingStatic(faToUpdate.values());

	}
}