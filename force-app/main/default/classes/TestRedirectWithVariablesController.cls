@isTest
private class TestRedirectWithVariablesController {
	
		
	@isTest static void test_method_one() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		
		RedirectWithVariablesController rwv = new RedirectWithVariablesController();
		
		PageReference pr = new PageReference('/apex/RedirectWithVariables?object=Opportunity&nooverride=1&accid='+acct.Id+'&cancelURL=/&retURL=/');
		Test.SetCurrentPage(pr);
		
		rwv.redirectUser();

	}

}