/**
 * @description       : The base implementation of an async custom button action.
 *  By extending from this class, your custom button can open up a sales console
 *  dialog, and inform the end user about the status of the action while a long
 *  running process takes place.
 * @author            : marin.mamic@vodafoneziggo.com
 * @last modified on  : 09-21-2022
 * @last modified by  : marin.mamic@vodafoneziggo.com
 **/
@SuppressWarnings('PMD.AvoidGlobalModifier')
global abstract without sharing class CustomButtonBase extends csbb.CustomButtonExt implements Queueable {
	private static final String MESSAGE_WAIT_FOR_BACKGROUND_JOB = 'We\'re waiting for a background job to be processed.';
	private static final String MESSAGE_OPERATION_SUCCESSFULL = 'Operation successfull';
	private static final String MESSAGE_STARTING_ASYNCHRONOUS_PROCESS = 'Starting an asynchronous process';
	private static final String MESSAGE_SOMETHING_IS_WRONG = 'Uh! Oh! Somethings wrong';
	private static final String URL_PRODUCT_BASKET_MODAL_PAGE = '/apex/ProductBasketModalPage?basketId=';

	private ActionPayload payload;
	@TestVisible
	private ExecutionStrategy selectedExecutionStrategy;
	protected Id basketId { get; private set; }

	/**
	 * @description A method that csbb package calls first when an end user clicks on a custom button.
	 * @param String basketId the ID of a basket that triggered the custom button action
	 * @return String a JSON formatted value representing an object based on which a toast message will be shown.
	 **/
	public String performAction(String basketId) {
		this.basketId = Id.valueOf(basketId);

		if (getExecutionStrategy() == ExecutionStrategy.ASYNCHRONOUS) {
			payload = readActionPayloadFrom(basketId);
			if (payload == null) {
				ID jobID = System.enqueueJob(this);
				ActionPayload payload = new ActionPayload(
					ActionStatus.NOT_STARTED,
					MESSAGE_WAIT_FOR_BACKGROUND_JOB,
					whereToRedirectOnSuccess(),
					whereToRedirectOnFailure(),
					jobID
				);
				payload.updatePayloadOn(basketId);
			}

			SalesConsoleUtils.ConsoleMessage consoleMessage = new SalesConsoleUtils.ConsoleMessage(
				SalesConsoleUtils.ConsoleMessageStatus.OK,
				MESSAGE_OPERATION_SUCCESSFULL,
				MESSAGE_STARTING_ASYNCHRONOUS_PROCESS
			);
			consoleMessage.redirectUrl = assembleModalWindowURL();
			consoleMessage.displayInDialog = true;
			consoleMessage.setSize(SalesConsoleUtils.ConsoleDialogSize.S);
			consoleMessage.modalTitle = System.Label.OperationInProgress;

			return consoleMessage.toJSON();
		} else {
			try {
				return this.executeCustomButtonAction();
			} catch (Exception e) {
				SalesConsoleUtils.ConsoleMessage response = new SalesConsoleUtils.ConsoleMessage(
					SalesConsoleUtils.ConsoleMessageStatus.ERROR,
					MESSAGE_SOMETHING_IS_WRONG,
					e.getMessage()
				);
				return response.toJSON();
			}
		}
	}

	/**
	 * @description Custom button action can be executed in multiple ways. Synchronously and asynchronously
	 *  first come to mind and is what this class was developed for. By implementing this method in your
	 *  custom button, you can define a strategy to be used for this process.
	 * @return ExecutionStrategy execution strategy to be used for this process.
	 **/
	global abstract ExecutionStrategy selectAnExecutionStrategy();

	/**
	 * @description The main method of the custom button implementaion. It's where the business logic you're
	 *  trying to execute lives. This No matter what the selected ExecutionStrategy is, this is the code that
	 *  will be executed.
	 * @return String a JSON formatted value representing an object based on which a toast message will be shown.
	 *  This matches the same response that csbb performAction expects to be returned.
	 **/
	global abstract String executeCustomButtonAction();

	/**
	 * @description Executes the same set of action defined under executeCustomButtonAction() but in a queueable job.
	 * @param QueueableContext context Contains the job ID.
	 **/
	public void execute(QueueableContext context) {
		executeCustomButtonAction();
	}

	/**
	 * @description By implementing this method in your custom button, you can define where user
	 *  is redirected to once the asynchronous action has successfully completed
	 * @return String a URL where user will be redirected to.
	 **/
	global abstract String whereToRedirectOnSuccess();

	/**
	 * @description By implementing this method in your custom button, you can define where user
	 *  is redirected to once the asynchronous action has failed.
	 * @return String a URL where user will be redirected to.
	 **/
	global abstract String whereToRedirectOnFailure();

	/**
	 * @description Fetches the payload from the product basket record with the specified ID.
	 * @param Id basketId an ID of the record to be fetched from Salesforce.
	 * @return ActionPayload an instance of the payload parsed from the Custom_Button_Action_Payload__c field.
	 **/
	public static ActionPayload readActionPayloadFrom(Id basketId) {
		cscfga__Product_Basket__c productBasket = [SELECT Custom_Button_Action_Payload__c FROM cscfga__Product_Basket__c WHERE id = :basketId];
		return getActionPayloadFrom(productBasket);
	}

	/**
	 * @description Gets the payload from an existing product basket record. This record needs to be fethced
	 *  with Custom_Button_Action_Payload__c field, otherwise an exception will be thrown.
	 * @param cscfga__Product_Basket__c productBasket a record from which payload will be extracted.
	 * @return ActionPayload an instance of the payload parsed from the Custom_Button_Action_Payload__c field.
	 **/
	public static ActionPayload getActionPayloadFrom(cscfga__Product_Basket__c productBasket) {
		ActionPayload response;
		try {
			response = (ActionPayload) JSON.deserializeStrict(productBasket.Custom_Button_Action_Payload__c, ActionPayload.class);
		} catch (Exception e) {
			response = null;
		}
		return response;
	}

	/**
	 * @description lazy loads the execution strategy.
	 * @return ExecutionStrategy selected execution strategy.
	 **/
	protected ExecutionStrategy getExecutionStrategy() {
		if (this.selectedExecutionStrategy == null) {
			this.selectedExecutionStrategy = selectAnExecutionStrategy();
		}
		return this.selectedExecutionStrategy;
	}

	private String assembleModalWindowURL() {
		return URL.getOrgDomainUrl().toExternalForm() + URL_PRODUCT_BASKET_MODAL_PAGE + this.basketId;
	}

	/**
	 * Defines how custom button actions will be executed.
	 **/
	global enum ExecutionStrategy {
		SYNCHRONOUS,
		ASYNCHRONOUS,
		ASYNCHRONOUS_FOR_LARGE_CONFIGURATIONS
	}

	/**
	 * Defines different statuses a custom button action could have.
	 **/
	public enum ActionStatus {
		NOT_STARTED,
		IN_PROGRESS,
		SUCCESS,
		FAILURE
	}

	/**
	 * There needs to be a way in which a custom button and a dialog page that waits for an action
	 * to complete could interact. We do that with a predefined payload that is persisted on
	 * Custom_Button_Action_Payload__c field on the product basket record. This class represents
	 * that payload, gives ways to protect that payload from being missused and methods to interact
	 * with it.
	 **/
	public without sharing class ActionPayload {
		public ActionStatus status { public get; private set; }
		public String statusMessage { public get; private set; }
		public String redirectToOnSuccess { public get; private set; }
		public String redirectToOnFailure { public get; private set; }
		public Id queueableId { public get; private set; }
		public String siteBaseUrl { public get; private set; }

		/**
		 * @description
		 * @param ActionStatus status this is based on what ProductBasketModalPage determines when
		 *  custom button action is done with execution and the final result of it. Notice how certain
		 *  fields like redirectToOnSuccess, queableId... can only be set when an object is constructed.
		 *  This is intentional to keep the integrity of the payload unaffected during the action process.
		 * @param String statusMessage an additional message to be passed along the status.
		 *  This is shown on ProductBasketModalPage and steers the end user in the right direction
		 *  in case of failure scenarios.
		 * @param String redirectToOnSuccess User will be redirected here when custom button action
		 *  results in a success.
		 * @param String redirectToOnFailure User will be redirected here when custom button action
		 *  results in failure
		 * @param Id queueableId the ID of the apex job executing in the background.
		 **/
		@SuppressWarnings('PMD.ExcessiveParameterList')
		public ActionPayload(ActionStatus status, String statusMessage, String redirectToOnSuccess, String redirectToOnFailure, Id queueableId) {
			this.status = status;
			this.statusMessage = statusMessage;
			this.redirectToOnSuccess = redirectToOnSuccess;
			this.redirectToOnFailure = redirectToOnFailure;
			this.queueableId = queueableId;
			this.siteBaseUrl = System.Site.getBaseUrl();
		}

		/**
		 * @description Runs a query to fetch the basket and updates Custom_Button_Action_Payload__c against it.
		 * @param Id basketId ID of the basket to update ActionPayload on
		 **/
		public void updatePayloadOn(Id basketId) {
			cscfga__Product_Basket__c productBasket = [SELECT Custom_Button_Action_Payload__c FROM cscfga__Product_Basket__c WHERE id = :basketId];
			update setPayloadOn(productBasket);
		}

		/**
		 * @description Updates Custom_Button_Action_Payload__c on an existing product basket record.
		 *  Please make sure the record is queries with that field as it will throw an error otherwise.
		 * @param cscfga__Product_Basket__c productBasket a record to update Custom_Button_Action_Payload__c on.
		 * @return cscfga__Product_Basket__c an updated product basket record.
		 **/
		public cscfga__Product_Basket__c setPayloadOn(cscfga__Product_Basket__c productBasket) {
			productBasket.Custom_Button_Action_Payload__c = this.toJSON();
			return productBasket;
		}

		/**
		 * @description Updates the status and a status message.
		 * @param ActionStatus status updated status
		 * @param String statusMessage updated status message
		 **/
		public void updateStatus(ActionStatus status, String statusMessage) {
			this.status = status;
			this.statusMessage = statusMessage;
		}

		/**
		 * @description Defines which action statuses qualify for an action to be completed.
		 * @return Boolean true if an asynchronous action is completed. False otherwise.
		 **/
		public Boolean isActionCompleted() {
			if (this.status == ActionStatus.FAILURE || this.status == ActionStatus.SUCCESS) {
				return true;
			}
			return false;
		}

		/**
		 * @description serializes this object into a JSON format.
		 * @return String a JSON representation of this ConsoleMessage
		 **/
		public String toJSON() {
			return JSON.serialize(this, true);
		}
	}
}
