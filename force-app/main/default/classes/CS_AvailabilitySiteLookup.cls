global with sharing class CS_AvailabilitySiteLookup extends cscfga.ALookupSearch {

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit){
       
     String configId = searchFields.get('ConfigId');
     
     //get list of alredy selected site checks and sites
     String basketId = searchFields.get('BasketId');
     List<cscfga__Product_Configuration__c> alreadySelectedSitesFromBasket = [SELECT Id, Site_Availability_Id__c, Site_Check_SiteID__c from 
     cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId and Id != :configId and Name = 'Access Infrastructure'];
     
     System.debug('AN --> '+alreadySelectedSitesFromBasket);
     
     Boolean primary = (searchFields.get('Primary checkbox') == 'true' || searchFields.get('Primary checkbox') == 'Yes') ? true : false;
     Boolean secondary = (searchFields.get('Secondary') == 'true' || searchFields.get('Secondary') == 'Yes') ? true : false;
     Set<Id> sitesAvailabilitiesAlreadySelected = new Set<Id>();
     Set<Id> sitesAlreadySelected = new Set<Id>();
      for(cscfga__Product_Configuration__c pc : alreadySelectedSitesFromBasket){
          if(pc.Site_Check_SiteID__c != null){
          sitesAlreadySelected.add(pc.Site_Check_SiteID__c);
          }
          if(pc.Site_Availability_Id__c != null){
          sitesAvailabilitiesAlreadySelected.add(pc.Site_Availability_Id__c);
          }
      }
     
     Set<String> relevantAttributeNames = new Set<String>();
     relevantAttributeNames.add('Access Type');
     relevantAttributeNames.add('Vendor');
     
     Set<Id> pcIds = (new Map<Id,cscfga__Product_Configuration__c>(alreadySelectedSitesFromBasket)).keySet();
     
     List<cscfga__Attribute__c> relAttributes = [SELECT Id, Name, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c in :pcIds and Name in :relevantAttributeNames];
     system.debug('RelATtr =='+relAttributes);
     
     Set<String> alreadySelectedVendors = new Set<String>();
     Set<String> alreadySelectedAccesses = new Set<String>();
     
     for(cscfga__Attribute__c attr : relAttributes){
         if((attr.cscfga__Value__c != '') && (attr.cscfga__Value__c != null)){
             if(attr.Name == 'Vendor'){
                 alreadySelectedVendors.add(attr.cscfga__Value__c);
             }
             if(attr.Name == 'Access Type'){
                 alreadySelectedAccesses.add(attr.cscfga__Value__c);
             }
         }
     }
     
     system.debug('*** searchFields: ' + JSON.serializePretty(searchFields));   
        
     final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1; 
     Integer recordOffset = pageOffset * pageLimit;   
     List<Site_Availability__c> finalSites = new List<Site_Availability__c>();
     Set<Id> siteCheckIds = new Set<Id>();   
     Set<Id> siteCheckIdsToRemove = new Set<Id>();
 
     Decimal minUpBandwidth1 = ((searchFields.get('Min. Upstream Bandwidth') == null)||(searchFields.get('Min. Upstream Bandwidth') == ''))?0:Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth').replace('.','').replace(',','.'));
     Decimal minDownBandwidth1 = ((searchFields.get('Min.Downstream Bandwidth') == null)||(searchFields.get('Min.Downstream Bandwidth') == ''))?0:Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth').replace('.','').replace(',','.'));
     
     if(searchFields.get('Min. Upstream Bandwidth 2') == null){
         system.debug('IS NULL');
     }
     if(searchFields.get('Min. Upstream Bandwidth 2') == ''){
         system.debug('IS EMPTY STRING');
     }
 
     Decimal minUpBandwidth2 = ((searchFields.get('Min. Upstream Bandwidth 2') == null)||(searchFields.get('Min. Upstream Bandwidth 2') == ''))?0:Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth 2').replace('.','').replace(',','.'));
     Decimal minDownBandwidth2 = ((searchFields.get('Min.Downstream Bandwidth 2') == null)||(searchFields.get('Min.Downstream Bandwidth 2') == ''))?0:Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth 2').replace('.','').replace(',','.'));
     Decimal minUpBandwidthInternet = ((searchFields.get('Min. Upstream Bandwidth Internet') == null)||(searchFields.get('Min. Upstream Bandwidth Internet') == ''))?0:Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth Internet').replace('.','').replace(',','.'));
     Decimal minDownBandwidthInternet = ((searchFields.get('Min.Downstream Bandwidth Internet') == null)||(searchFields.get('Min.Downstream Bandwidth Internet') == ''))?0:Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth Internet').replace('.','').replace(',','.'));
     Decimal minUpBandwidthOneFixed = ((searchFields.get('Min Bandwidth OneFixed') == null)||(searchFields.get('Min Bandwidth OneFixed') == ''))?0:Decimal.valueOf(searchFields.get('Min Bandwidth OneFixed')); 
     Decimal minDownBandwidthOneFixed = ((searchFields.get('Min Bandwidth OneFixed') == null)||(searchFields.get('Min Bandwidth OneFixed') == ''))?0:Decimal.valueOf(searchFields.get('Min Bandwidth OneFixed'));
     Decimal minBandwidtOneNet = ((searchFields.get('Min Bandwidth OneNet') == null)||(searchFields.get('Min Bandwidth OneNet') == ''))?0:Decimal.valueOf(searchFields.get('Min Bandwidth OneNet'));
     
     Decimal totalBandwidthEntryUp = 0.0;
     Decimal totalBandwidthEntryDown = 0.0;
     Decimal totalBandwidthPremiumUp = 0.0;
     Decimal totalBandwidthPremiumDown = 0.0;        
     
     system.debug('**** minUpBandwidth1: ' + minUpBandwidth1);
     system.debug('**** minUpBandwidth2: ' + minUpBandwidth2);
     system.debug('**** minDownBandwidth1: ' + minDownBandwidth1);
     system.debug('**** minDownBandwidth2: ' + minDownBandwidth2);
     system.debug('**** minUpBandwidthInternet: ' + minUpBandwidthInternet);
     system.debug('**** minDownBandwidthInternet: ' + minDownBandwidthInternet);
     system.debug('**** minUpBandwidthOneFixed: ' + minUpBandwidthOneFixed);
     system.debug('**** minBandwidtOneNet: ' + minBandwidtOneNet);
     
     
     string accountId = searchFields.get('AccountId');
     Decimal contractDuration = (searchFields.get('Contract Duration') == null||(searchFields.get('Contract Duration') == ''))?0:Decimal.valueOf(searchFields.get('Contract Duration'));
     String searchValue = searchFields.get('searchValue')==''?'%':'%'+searchFields.get('searchValue')+'%';   
     
     Set<String> contractDurationAccessInfrastructure = new Set<String>();
     contractDurationAccessInfrastructure.add('SDSL');
     contractDurationAccessInfrastructure.add('ADSL');
     contractDurationAccessInfrastructure.add('VDSL');
     contractDurationAccessInfrastructure.add('VVDSL');
     contractDurationAccessInfrastructure.add('FTTH');
     contractDurationAccessInfrastructure.add('EthernetOverCopper');
     contractDurationAccessInfrastructure.add('Single Line');
     contractDurationAccessInfrastructure.add('Coax');
     
     Set<String> evcpvcAccessInfrastructure = new Set<String>();
     evcpvcAccessInfrastructure.add('Coax');
     evcpvcAccessInfrastructure.add('SDSL');
     evcpvcAccessInfrastructure.add('VDSL');
     evcpvcAccessInfrastructure.add('VVDSL');
     evcpvcAccessInfrastructure.add('ADSL');
     
     Set<String> dslAccessInfrastructure = new Set<String>();
     dslAccessInfrastructure.add('BVDSL');
     dslAccessInfrastructure.add('BVVDSL');
     dslAccessInfrastructure.add('SDSL');
     dslAccessInfrastructure.add('VDSL');
     dslAccessInfrastructure.add('VVDSL');
     dslAccessInfrastructure.add('ADSL');
     
     //T-2715
     Set<String> wirelessBackupAccessInfrastructure = new Set<String>();
     wirelessBackupAccessInfrastructure.add('EthernetOverFiber');
     wirelessBackupAccessInfrastructure.add('BVDSL');
     wirelessBackupAccessInfrastructure.add('BVVDSL');
     wirelessBackupAccessInfrastructure.add('SDSL');
     wirelessBackupAccessInfrastructure.add('VDSL');
     wirelessBackupAccessInfrastructure.add('VVDSL');
     wirelessBackupAccessInfrastructure.add('ADSL');
     
     Boolean wirelessBackup = (searchFields.get('Wireless backup') == 'false' || searchFields.get('Wireless backup') == '' || searchFields.get('Wireless backup') == null) ? false : true;
     Boolean wirelessBackupInternet = (searchFields.get('Wireless backup internet') == 'false' || searchFields.get('Wireless backup internet') == '' || searchFields.get('Wireless backup internet') == null) ? false : true;
     //end
     
     
     String overbookingData = (searchFields.get('Overbooking Data') == null || (searchFields.get('Overbooking Data') == ''))?'':searchFields.get('Overbooking Data');  
     String overbookingVoip = (searchFields.get('Overbooking Voip') == null ||(searchFields.get('Overbooking Voip') == ''))?'':searchFields.get('Overbooking Voip'); 
     String overbookingInternet = (searchFields.get('Overbooking Internet') == null ||(searchFields.get('Overbooking Internet') == ''))?'':searchFields.get('Overbooking Internet');
     String overbookingOneFixed = (searchFields.get('Overbooking OneFixed') == null ||(searchFields.get('Overbooking OneFixed') == ''))?'':searchFields.get('Overbooking OneFixed');
     String overbookingOneNet = (searchFields.get('Overbooking OneNet') == null ||(searchFields.get('Overbooking OneNet') == ''))?'':searchFields.get('Overbooking OneNet');
     
     String technologyType = (searchFields.get('Technology type') == null || (searchFields.get('Technology type') == ''))?'':searchFields.get('Technology type');
     
     Boolean overbookingEntry = false;
     if(overbookingData.equalsIgnoreCase('Entry') || overbookingVoip.equalsIgnoreCase('Entry') || overbookingInternet.equalsIgnoreCase('Entry') || overbookingOneNet.equalsIgnoreCase('Entry')){
         overbookingEntry = true;
     }
     
     
     //T-02791
     Boolean ipvpn = (searchFields.get('IPVPN') == null ||(searchFields.get('IPVPN') == ''))? false : true;
     Boolean managedInternet = (searchFields.get('Managed Internet') == null ||(searchFields.get('Managed Internet') == ''))? false : true;
     Boolean oneFixed = (searchFields.get('One Fixed') == null || (searchFields.get('One Fixed') == ''))? false : true; 
     Boolean oneNet = (searchFields.get('One Net') == null || (searchFields.get('One Net') == ''))? false : true; 
     //end
 
     //Rule: "If sales has requested 2 Premium EVC/PV..."
     Boolean overbookingPremium = false;
     if((overbookingData.equalsIgnoreCase('Premium') && overbookingVoip.equalsIgnoreCase('Premium')) 
         || (overbookingData.equalsIgnoreCase('Premium') && overbookingInternet.equalsIgnoreCase('Premium'))
         || (overbookingData.equalsIgnoreCase('Premium') && overbookingOneFixed.equalsIgnoreCase('Premium') && oneFixed)
         || (overbookingData.equalsIgnoreCase('Premium') && overbookingOneNet.equalsIgnoreCase('Premium') && oneNet)
         || (overbookingVoip.equalsIgnoreCase('Premium') && overbookingInternet.equalsIgnoreCase('Premium') && managedInternet)
         || (overbookingVoip.equalsIgnoreCase('Premium') && overbookingOneFixed.equalsIgnoreCase('Premium') && oneFixed)
         || (overbookingVoip.equalsIgnoreCase('Premium') && overbookingOneNet.equalsIgnoreCase('Premium') && oneNet)
         || (overbookingInternet.equalsIgnoreCase('Premium') && overbookingOneFixed.equalsIgnoreCase('Premium') && managedInternet)
         || (overbookingInternet.equalsIgnoreCase('Premium') && overbookingOneNet.equalsIgnoreCase('Premium') && oneNet)
         || (overbookingOneFixed.equalsIgnoreCase('Premium') && overbookingOneNet.equalsIgnoreCase('Premium') && oneNet)
         ){
         overbookingPremium = true;
     }   
 
                                 
     String scenario = searchFields.get('Scenario');
     //Integer numOfEVC = scenario == 'Data + VoiP' ? 2 : 1;
     //Integer numOfEVCInternet = overbookingInternet == '' ? 0 : 1;
     Integer totalNumOfEVC = (searchFields.get('Total Num EVCPVC') == null ||(searchFields.get('Total Num EVCPVC') == ''))? 0 : Integer.valueOf(searchFields.get('Total Num EVCPVC'));
     /*
     Boolean ipvpn = (searchFields.get('IPVPN') == null ||(searchFields.get('IPVPN') == ''))? false : true;
     Boolean managedInternet = (searchFields.get('Managed Internet') == null ||(searchFields.get('Managed Internet') == ''))? false : true;
     Boolean oneFixed = (searchFields.get('One Fixed') == null || (searchFields.get('One Fixed') == ''))? false : true;
     */
     Boolean modularPricing = (searchFields.get('Modular pricing') == null ||(searchFields.get('Modular pricing') == ''))? false : ((searchFields.get('Modular pricing') == 'No') || (searchFields.get('Modular pricing') == 'false'))? false : true;
     String targetAccessInfrastructure = (searchFields.get('Target access infrastructure') == null ||(searchFields.get('Target access infrastructure') == ''))? '' : searchFields.get('Target access infrastructure');
     String targetAccessInfrastructureOneNet = (searchFields.get('Target access infra OneNet') == null ||(searchFields.get('Target access infra OneNet') == ''))? '' : searchFields.get('Target access infra OneNet');
     String oneFixedScenario = (searchFields.get('Scenario OneFixed') == null ||(searchFields.get('Scenario OneFixed') == ''))? '' : searchFields.get('Scenario OneFixed');
     String oneNetScenario = (searchFields.get('Scenario OneNet') == null ||(searchFields.get('Scenario OneNet') == ''))? '' : searchFields.get('Scenario OneNet');
     
     //IPVPN Bandwidth
     if(overbookingData == 'Entry'){
         totalBandwidthEntryUp += minUpBandwidth1;   
         totalBandwidthEntryDown += minDownBandwidth1;
     }
     
     if(overbookingVoip == 'Entry'){
         totalBandwidthEntryUp += minUpBandwidth2;
         totalBandwidthEntryDown += minDownBandwidth2;
     }
     
     if(overbookingData == 'Premium'){
         totalBandwidthPremiumUp += minUpBandwidth1;
         totalBandwidthPremiumDown += minDownBandwidth1;
     }
     
     if(overbookingVoip == 'Premium'){
         totalBandwidthPremiumUp += minUpBandwidth2;
         totalBandwidthPremiumDown += minDownBandwidth2;
     }
     //end IPVPN Bandwidth
     
     // adding Managed Internet bandwidth 
     if(overbookingInternet == 'Entry'){
         totalBandwidthEntryUp += minUpBandwidthInternet;    
         totalBandwidthEntryDown += minDownBandwidthInternet;
     }
     
     if(overbookingInternet == 'Premium'){
         totalBandwidthPremiumUp += minUpBandwidthInternet;  
         totalBandwidthPremiumDown += minDownBandwidthInternet;      
     }
     // end adding Managed Internet bandwidth
     
     //add OneFixed bandwidth
     if(overbookingOneFixed == 'Entry'){
         totalBandwidthEntryUp += minUpBandwidthOneFixed;
         totalBandwidthEntryDown += minUpBandwidthOneFixed;
     }
     
     if(overbookingOneFixed == 'Premium'){
         totalBandwidthPremiumUp += minUpBandwidthOneFixed;  
         totalBandwidthPremiumDown += minUpBandwidthOneFixed;
     }
     //end OneFixed bandwidth
     
     //add OneNet bandwidth
     if(overbookingOneNet == 'Entry'){
         totalBandwidthEntryUp += minBandwidtOneNet;
         totalBandwidthEntryDown += minBandwidtOneNet;
     }
     
     if(overbookingOneNet == 'Premium'){
         totalBandwidthPremiumUp += minBandwidtOneNet;  
         totalBandwidthPremiumDown += minBandwidtOneNet;
     }
     //end OneNet bandwidth
     
     Decimal totalBandwidthUp = totalBandwidthEntryUp + totalBandwidthPremiumUp;
     Decimal totalBandwidthDown = totalBandwidthEntryDown + totalBandwidthPremiumDown;
     
     system.debug('totalBandwidthEntryUp ' + totalBandwidthEntryUp);
     system.debug('totalBandwidtPremiumUp ' + totalBandwidthPremiumUp);
     system.debug('totalBandwidtEntryDown ' + totalBandwidthEntryDown);
     system.debug('totalBandwidtPremiumDown ' + totalBandwidthPremiumDown);
     system.debug('totalBandwidthUp ' + totalBandwidthUp);
     system.debug('totalBandwidthDown ' + totalBandwidthDown);
     system.debug('contractDuration ' + contractDuration);
     system.debug('overbookingData ' + overbookingData);
     system.debug('overbookingVoip ' + overbookingVoip);
     system.debug('overbookingInternet ' + overbookingInternet);
     system.debug('overbookingEntry ' + overbookingEntry);
     system.debug('overbookingPremium ' + overbookingPremium);
     system.debug('scenario ' + scenario);
     system.debug('totalNumOfEVC ' + totalNumOfEVC);
     system.debug('ipvpn ' + ipvpn);
     system.debug('managedInternet ' + managedInternet);
     system.debug('oneFixed ' + oneFixed);
     system.debug('modularPricing ' + modularPricing);
     
     system.debug('targetAccessInfrastructure ' + targetAccessInfrastructure);
     system.debug('targetAccessInfrastructureOneNet ' + targetAccessInfrastructureOneNet);
     
     system.debug('oneFixedScenario ' + oneFixedScenario);
     system.debug('oneNetScenario ' + oneNetScenario);
                  
          Map<Id,Site_Availability__c> siteChecks = new Map<Id,Site_Availability__c>([SELECT Id, Name, Access_Infrastructure__c, Bandwith_Down_Entry__c, Bandwith_Down_Premium__c, Bandwith_Sellable_Down__c, Bandwith_Sellable_Up__c, 
                                                                                     Bandwith_Up_Entry__c, Bandwith_Up_Premium__c, Premium_Applicable__c, Premium_Vendor__c, Usable__c, Vendor__c, Site__c, Site__r.Site_Account__c, 
                                                                                     Site__r.Site_PABX_Certified__c, Result_Check__c, Offer_amount__c, Promo__c,Access_Offer_Amount__c, Region__c, Existing_Infra__c
                                                     FROM Site_Availability__c 
                                                      WHERE Site__r.Site_Account__c = :accountId AND Usable__c = true
                                                         AND (Vendor__c LIKE :searchValue OR Name LIKE :searchValue OR Site__r.Site_City__c LIKE :searchValue OR Site__r.Site_Postal_Code__c LIKE :searchValue OR Site__r.Site_Street__c LIKE :searchValue OR Site__r.Building__c LIKE :searchValue  OR Site__r.Name LIKE :searchValue OR Access_Infrastructure__c LIKE :searchValue)]);
         
         for(Site_Availability__c sa : siteChecks.values()){
         
             System.debug('Candidate SA === '+sa);
 
             //modular pricing mode filtering
             if(siteChecks.get(sa.Id).Premium_Applicable__c){
                 if((siteChecks.get(sa.Id).Bandwith_Sellable_Down__c < totalBandwidthEntryDown) || (siteChecks.get(sa.Id).Bandwith_Down_Premium__c < totalBandwidthPremiumDown)
                     || (siteChecks.get(sa.Id).Bandwith_Sellable_Up__c < totalBandwidthEntryUp) || (siteChecks.get(sa.Id).Bandwith_Up_Premium__c < totalBandwidthPremiumUp)){
                     siteCheckIdsToRemove.add(sa.Id);
                     system.debug('Remove Premium_Applicable__c true: ' + sa);
                 }
 
             } else {
                 if(siteChecks.get(sa.Id).Bandwith_Sellable_Down__c < totalBandwidthDown || siteChecks.get(sa.Id).Bandwith_Sellable_Up__c < totalBandwidthUp){
                     siteCheckIdsToRemove.add(sa.Id);
                     system.debug('Remove Premium_Applicable__c false: ' + sa);
                 }
             }
 
         //modular pricing mode filtering
         if(modularPricing){
             
             if(ipvpn && scenario == 'Data + VoiP' && siteChecks.get(sa.Id).Access_Infrastructure__c == 'Coax'){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove scenario == Data + VoiP: ' + sa);
             }
             
             if(totalNumOfEVC > 1 && (siteChecks.get(sa.Id).Access_Infrastructure__c == 'SDSL' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'EthernetOverCopperSingle')){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove numOfEVC > 1: ' + sa);
             }
             
             if(totalNumOfEVC > 2 && (siteChecks.get(sa.Id).Access_Infrastructure__c == 'ADSL' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'VDSL'
                 || siteChecks.get(sa.Id).Access_Infrastructure__c == 'VVDSL' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'Coax'
                 || siteChecks.get(sa.Id).Access_Infrastructure__c == 'EthernetOverCopper')){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove numOfEVC > 2: ' + sa);
             }
             
             if(totalNumOfEVC > 4 && (siteChecks.get(sa.Id).Access_Infrastructure__c == 'EthernetOverFiber' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'FTTH')){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove numOfEVC > 4: ' + sa);
             }
             
             if(overbookingPremium && evcpvcAccessInfrastructure.contains(siteChecks.get(sa.Id).Access_Infrastructure__c)){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove numOfEVC == 2 && overbookingPremium: ' + sa);
             }
             
             //this rule is for standalone Internet - has to be updated with other combinations
             if(managedInternet && !ipvpn && !oneFixed && !oneNet){
                 if(siteChecks.get(sa.Id).Access_Infrastructure__c == 'Coax'){
                    siteCheckIdsToRemove.add(sa.Id);
                     system.debug('Remove standalone managedInternet: ' + sa); 
                 }
             }
             
             if(managedInternet && siteChecks.get(sa.Id).Access_Infrastructure__c == 'SDSL'){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove managedInternet: ' + sa);
             }
             
             if (contractDuration < 12 && contractDurationAccessInfrastructure.contains(siteChecks.get(sa.Id).Access_Infrastructure__c)){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove contractDuration < 12: ' + sa);
             }
             
             if(technologyType == 'IP' && siteChecks.get(sa.Id).Site__r.Site_PABX_Certified__c == ''){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove IPPBX certified: ' + sa);
             }
             
             if(targetAccessInfrastructure != ''){
                  if(siteChecks.get(sa.Id).Access_Infrastructure__c != targetAccessInfrastructure){
                     siteCheckIdsToRemove.add(sa.Id);
                     system.debug('Remove Modular targetAccessInfrastructure selected: ' + sa);
                 }
             }
             
             if(targetAccessInfrastructureOneNet != ''){
                  if(siteChecks.get(sa.Id).Access_Infrastructure__c != targetAccessInfrastructureOneNet){
                     siteCheckIdsToRemove.add(sa.Id);
                     system.debug('Remove Modular targetAccessInfrastructureOneNet selected: ' + sa);
                 }
             }
         } else {
             //bundle mode filtering
             if(targetAccessInfrastructure == ''){
                 if(managedInternet && !oneFixed && !ipvpn && !oneNet){
                     if(siteChecks.get(sa.Id).Access_Infrastructure__c != 'EthernetOverFiber'){
                         siteCheckIdsToRemove.add(sa.Id);
                         system.debug('Remove bundle managedInternet only: ' + sa);
                     }
                 }
                 
                 if(oneFixed && oneFixedScenario == 'One Fixed Express' && !ipvpn){
                     if(!dslAccessInfrastructure.contains(siteChecks.get(sa.Id).Access_Infrastructure__c) && siteChecks.get(sa.Id).Access_Infrastructure__c != 'FTTH' && siteChecks.get(sa.Id).Access_Infrastructure__c != 'Coax'){
                         siteCheckIdsToRemove.add(sa.Id);
                         system.debug('Remove bundle Onefixed Express: ' + sa); 
                     }
                 }
                 
                 if(oneFixed && oneFixedScenario == 'One Fixed Enterprise' && !ipvpn){
                     if(!dslAccessInfrastructure.contains(siteChecks.get(sa.Id).Access_Infrastructure__c) && siteChecks.get(sa.Id).Access_Infrastructure__c != 'FTTH' && siteChecks.get(sa.Id).Access_Infrastructure__c != 'Coax' && siteChecks.get(sa.Id).Access_Infrastructure__c != 'EthernetOverFiber'){
                         siteCheckIdsToRemove.add(sa.Id);
                         system.debug('Remove bundle Onefixed Enterprise: ' + sa); 
                     }
                 }
             } else {
                 if(siteChecks.get(sa.Id).Access_Infrastructure__c != targetAccessInfrastructure){
                     siteCheckIdsToRemove.add(sa.Id);
                     system.debug('Remove bundle targetAccessInfrastructure selected: ' + sa);
                 }
                 
             }
             
             if(targetAccessInfrastructureOneNet != ''){
                 if(siteChecks.get(sa.Id).Access_Infrastructure__c != targetAccessInfrastructureOneNet){
                     siteCheckIdsToRemove.add(sa.Id);
                     system.debug('Remove Modular targetAccessInfrastructureOneNet selected: ' + sa);
                 }
             }
             
             if((oneFixed || (oneFixed && managedInternet)) && !ipvpn){
                 if(!(siteChecks.get(sa.Id).Result_Check__c.contains('ONNET')) && !(siteChecks.get(sa.Id).Result_Check__c.contains('NEARNET')) && !(siteChecks.get(sa.Id).Result_Check__c.contains('OFFNET'))){
                     siteCheckIdsToRemove.add(sa.Id);
                     system.debug('Remove bundle result check: ' + sa);
                 }
                 /*
                 if(siteChecks.get(sa.Id).Result_Check__c != 'ONNET' && siteChecks.get(sa.Id).Result_Check__c != 'NEARNET'){
                     siteCheckIdsToRemove.add(sa.Id);
                     system.debug('Remove bundle result check: ' + sa);
                 }
                 */
             }           
 
         }  
         
         //remove Tele2 Vendor for OneFixed only 
         if(oneFixed && !ipvpn && !managedInternet && !oneNet){
             if(siteChecks.get(sa.Id).Vendor__c.containsIgnoreCase('Tele2')){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove Tele2 for oneFixed only: ' + sa);
             }
         }
         
         //remove EF-FIBER Vendor for OneFixed and ManagedInternet 
         if((oneFixed && managedInternet) && !ipvpn && !oneNet){
             if(siteChecks.get(sa.Id).Vendor__c.containsIgnoreCase('EF-FIBER')){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove EF-FIBER for oneFixed and managedInternet: ' + sa);
             }
         }
         // Region = "Flex" is available only for modular pricing and when scenario is One Net Enterprise
         if(!(modularPricing && oneNetScenario == 'One Net Enterprise')){
             if(siteChecks.get(sa.Id).Region__c != null && siteChecks.get(sa.Id).Region__c.containsIgnoreCase('Flex')){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove site with region FLEX because it is NOT modular One Net Enterprise: ' + sa);
             }
         }
 
         // When One Net Express is selected and modular is true, only sites with Existing_Infra__c = true are allowed
         if(modularPricing && oneNetScenario == 'One Net Express'){
             if(siteChecks.get(sa.Id).Existing_Infra__c == false){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove site availability with Existing_Infra__c == false because configuration is modular One Net Express: ' + sa);
             }
         }
         
         //remove depending on WirelessBackup - no longer required T-04268
         /*if((wirelessBackup == true) || (wirelessBackupInternet== true)){
             if(!wirelessBackupAccessInfrastructure.contains(siteChecks.get(sa.Id).Access_Infrastructure__c)){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('@Remove wireless backup set to true and wrong access type' + siteChecks.get(sa.Id).Access_Infrastructure__c);
             }
         }*/
         
         //remove depending on WirelessBackup --end
 
          System.debug('--Before MEE e-----> '+sa.Id);
         //remove depending on Redundancy
         //if site isn't primary and isn't secondary ----> site availablility results for sites which are already selected should be removed
             if(sitesAlreadySelected!= null && (sa.Site__c!= null) && sitesAlreadySelected.contains(siteChecks.get(sa.Id).Site__c) && !primary && !secondary){
                 System.debug('Removing this one-----> '+sa.Id);
                 siteCheckIdsToRemove.add(sa.Id);
             }
        
         //if site is primary or secondary -> user can chose a different site check result -> different vandor and access type
             //if(sitesAvailabilitiesAlreadySelected!= null && (primary || secondary) && (sitesAvailabilitiesAlreadySelected.contains(siteChecks.get(sa.Id).Id) || (alreadySelectedAccesses.contains(siteChecks.get(sa.Id).Access_Infrastructure__c) && alreadySelectedVendors.contains(siteChecks.get(sa.Id).Vendor__c)))){
             if(sitesAvailabilitiesAlreadySelected!= null && (primary || secondary) && (sitesAvailabilitiesAlreadySelected.contains(siteChecks.get(sa.Id).Id))){
                 siteCheckIdsToRemove.add(sa.Id);
             }
         
         //OneNet SiteCheck rulling
         if(oneNet && !managedInternet){
             if(siteChecks.get(sa.Id).Access_Infrastructure__c == 'ADSL' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'VDSL' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'VVDSL'
                 || siteChecks.get(sa.Id).Access_Infrastructure__c == 'BVDSL' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'BVVDSL' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'Coax'){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove OneNet and no ManagedInternet: ' + sa);
             }
         }
         
         //remove this as per Abdul's suggestion, 4/April
         /*if(oneNet){
             if(siteChecks.get(sa.Id).Access_Infrastructure__c == 'FTTH' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'EthernetOverCopper' || siteChecks.get(sa.Id).Access_Infrastructure__c == 'EthernetOverCopperSingle'){
                 siteCheckIdsToRemove.add(sa.Id);
                 system.debug('Remove OneNet only: ' + sa);
             }
         } */          
         //end OneNet SiteCheck rulling   
         
         }   
         
              
         
        finalSites = [
            SELECT Id, Name, Order__c,Site__c,Site__r.Name, Vendor__c, Bandwith_Sellable_Down__c, Bandwith_Sellable_Up__c, Bandwith_Down_Premium__c, Bandwith_Up_Premium__c, 
                Access_Infrastructure__c, Existing_Infra__c, Region__c, Result_Check__c, Site__r.Site_Account__c, LastModifiedDate,Usable__c,Site__r.Site_City__c, 
                Site__r.Site_Postal_Code__c, Site__r.Site_Street__c, Site__r.Building__c, Premium_Applicable__c, Site__r.Site_PABX_Certified__c, Offer_amount__c, Promo__c, 
                Access_Offer_Amount__c
            FROM Site_Availability__c
            WHERE Id IN :siteChecks.keySet() AND Id NOT IN :siteCheckIdsToRemove
            ORDER BY Site__r.Name, Order__c
            LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset
        ];
                                                      
         System.debug('*****Number of sitecheck results: '+ finalSites.size());
         return finalSites;   
     }
 
 
   public override String getRequiredAttributes(){ 
   
       return '["AccountId", "Min. Upstream Bandwidth","Min.Downstream Bandwidth", "Min. Upstream Bandwidth 2","Min.Downstream Bandwidth 2", "Overbooking Data", '+
       '"Overbooking Voip", "Scenario", "Contract Duration", "Overbooking Internet", "Min.Downstream Bandwidth Internet", "Min. Upstream Bandwidth Internet", "Total Num EVCPVC", '+
       '"Managed Internet", "IPVPN", "Overbooking OneFixed", "Min Bandwidth OneFixed", "One Fixed", "Technology type", "Modular pricing", "Target access infrastructure", '+
       '"Scenario OneFixed","Wireless backup","Wireless backup internet","Primary checkbox","BasketId","Secondary","ConfigId","Min Bandwidth OneNet","One Net","Overbooking OneNet", '+
       '"Target access infra OneNet", "Scenario OneNet"]';
   
   }  
 }