@isTest
@SuppressWarnings('PMD.ExcessiveParameterList')
public with sharing class ECSSOAPLocationMocks {
	public class LocationResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPLocation.locationResponseType theResponse = new ECSSOAPLocation.locationResponseType();
			theResponse.referenceId = 'Test Completed';
			ECSSOAPLocation.locationsResponse_element responseElement = new ECSSOAPLocation.locationsResponse_element();
			responseElement.location = new List<ECSSOAPLocation.locationResponseType>();
			responseElement.location.add(theResponse);
			response.put('response_x', responseElement);
		}
	}

	//created new mock, above old is used in a legacy test class
	public class CreateUpdateLocationsResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			ECSSOAPLocation.locationResponseType[] locationResX = new List<ECSSOAPLocation.locationResponseType>{};
			ECSSOAPLocation.locationResponseType myResponse = new ECSSOAPLocation.locationResponseType();
			myResponse.ban = '12345';
			myResponse.referenceId = 'XYZ';
			myResponse.corporateId = '000randomId';
			myResponse.bopCode = 'ABC';
			myResponse.zipcode = '1111AA';
			myResponse.locationReferenceId = 'test';
			myResponse.housenumber = '1';
			myResponse.housenumberExt = 'A';
			myResponse.errorCode = '0';
			myResponse.errorMessage = 'OK';
			myResponse.dateTime_x = System.now();
			locationResX.add(myResponse);
			ECSSOAPLocation.locationsResponse_element responseX = new ECSSOAPLocation.locationsResponse_element();
			responseX.location = locationResX;
			response.put('response_x', responseX);
		}
	}
}
