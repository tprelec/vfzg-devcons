@isTest
public class TestEMP_UpdateMobileOrder {
	@testSetup
	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, owner);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		update claimerContact;
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(claimerContact.id, '123321');
		contr.Dealer_Information__c = dealerInfo.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true
		);
		insert order;
		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);

		NetProfit_CTN__c ctn1 = new NetProfit_CTN__c(
			Order__c = order.Id,
			Action__c = 'New',
			Product_Group__c = 'Priceplan',
			Price_Plan_Class__c = 'Data',
			NetProfit_Information__c = npi.Id,
			CTN_Number__c = '31612310001',
			SIM_Activation_Order_Id__c = '1234567A',
			Simcard_number_VF__c = Constants.NET_PROFIT_CTN_VALID_SIM_CARD_NUMBER,
			SIM_Activation_Timestamp__c = system.now().addMinutes(-90)
		);
		insert ctn1;

		CSPOFA__Orchestration_Process_Template__c orchTempl = OrchTestDataFactory.createOrchestrationProcessTemplates(null, 1, false)[0];
		orchTempl.Name = 'EMP Status Update';
		insert orchTempl;
	}

	@isTest
	public static void testUpdateMobileOrderGetOrderSumDO() {
		String strCTNId = [SELECT Id FROM NetProfit_CTN__c].Id;
		//creating JSON minify manually because the correlation Id is the id of the ctn and generate randomly in the test class
		String myJSON =
			'{"correlation_Id":\"' +
			strCTNId +
			'\","action":"GetOrderSummary","status":200,"data":[{"orderSummary":[{"orderHeader":{"trackAndTraceId":null,"orderStatus":"DO","orderID":"557474399A","deliveryMethod":null,"daysToExpiration":13,"customerProfileHeader":{"customerName":"JacquelineDersjantPHTGRPHR ITE2","customerID":800050349},"creditVettingStatus":"Status: APPROVED.Aanvraag is goedgekeurd","contactID":null,"cancelPonr":null,"applicationDate":1599092188000,"amendPonr":"No"}}]}],"error":[]}';

		RestRequest request = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		request.requestUri = baseUrl + '/services/apexrest/UpdateMobileOrder';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);

		RestContext.request = request;
		Test.startTest();
		EMP_UpdateMobileOrder.doPost();
		NetProfit_CTN__c objCTN = [SELECT CTN_Status__c, Order_Summary_BSL_Status__c FROM NetProfit_CTN__c];
		System.assertEquals('DO', objCTN.Order_Summary_BSL_Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testUpdateMobileOrderGetOrderSumSB() {
		String strCTNId = [SELECT Id FROM NetProfit_CTN__c].Id;
		//creating JSON minify manually because the correlation Id is the id of the ctn and generate randomly in the test class
		String myJSON =
			'{"correlation_Id":\"' +
			strCTNId +
			'\","action":"GetOrderSummary","status":200,"data":[{"orderSummary":[{"orderHeader":{"trackAndTraceId":null,"orderStatus":"SB","orderID":"557474399A","deliveryMethod":null,"daysToExpiration":13,"customerProfileHeader":{"customerName":"JacquelineDersjantPHTGRPHR ITE2","customerID":800050349},"creditVettingStatus":"Status: APPROVED.Aanvraag is goedgekeurd","contactID":null,"cancelPonr":null,"applicationDate":1599092188000,"amendPonr":"No"}}]}],"error":[]}';

		RestRequest request = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		request.requestUri = baseUrl + '/services/apexrest/UpdateMobileOrder';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);

		RestContext.request = request;
		Test.startTest();
		EMP_UpdateMobileOrder.doPost();
		NetProfit_CTN__c objCTN = [SELECT CTN_Status__c, Order_Summary_BSL_Status__c FROM NetProfit_CTN__c];
		System.assertEquals('SB', objCTN.Order_Summary_BSL_Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testUpdateMobileOrderGetOrderSumError1() {
		String strCTNId = [SELECT Id FROM NetProfit_CTN__c].Id;
		//creating JSON minify manually because the correlation Id is the id of the ctn and generate randomly in the test class
		String myJSON =
			'{"correlation_Id":\"' +
			strCTNId +
			'\","action":"GetOrderSummary","status":500,"data":[],"error":[{"message":"Authorization failed of dealer market restrictions  sourceSystem  BSL","code":"500"}]}';

		RestRequest request = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		request.requestUri = baseUrl + '/services/apexrest/UpdateMobileOrder';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);

		RestContext.request = request;
		Test.startTest();
		EMP_UpdateMobileOrder.doPost();
		NetProfit_CTN__c objCTN = [SELECT CTN_Status__c, Order_Summary_BSL_Status__c FROM NetProfit_CTN__c];
		System.assertEquals('Order Summary failed', objCTN.CTN_Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testUpdateMobileOrderGetOrderSumError2() {
		String strCTNId = [SELECT Id FROM NetProfit_CTN__c].Id;
		//creating JSON minify manually because the correlation Id is the id of the ctn and generate randomly in the test class
		String myJSON =
			'{"correlation_Id":\"' +
			strCTNId +
			'\","action":"GetOrderSummary","status":500,"data":[],"error":[{"message":"Activity timed out","code":"500"}]}';

		RestRequest request = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		request.requestUri = baseUrl + '/services/apexrest/UpdateMobileOrder';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);

		RestContext.request = request;
		Test.startTest();
		EMP_UpdateMobileOrder.doPost();
		NetProfit_CTN__c objCTN = [SELECT CTN_Status__c, Order_Summary_BSL_Status__c, Order_Summary_Error__c FROM NetProfit_CTN__c];
		System.assertEquals(true, objCTN.Order_Summary_Error__c.containsIgnoreCase('Activity timed out'));
		Test.stopTest();
	}

	@isTest
	public static void testUpdateMobileOrderActivateSIM() {
		String strCTNId = [SELECT Id FROM NetProfit_CTN__c].Id;
		//creating JSON minify manually because the correlation Id is the id of the ctn and generate randomly in the test class
		String myJSON =
			'{"correlation_Id":\"' +
			strCTNId +
			'\","action":"ActivateSimForTemplate","status":200,"data":[{"orderID":"557475359A"}],"error":[]}';

		RestRequest request = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		request.requestUri = baseUrl + '/services/apexrest/UpdateMobileOrder';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);

		RestContext.request = request;
		Test.startTest();
		EMP_UpdateMobileOrder.doPost();
		NetProfit_CTN__c objCTN = [SELECT CTN_Status__c, Order_Summary_BSL_Status__c FROM NetProfit_CTN__c];
		System.assertEquals('SIM Activation Order ID received', objCTN.CTN_Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testUpdateMobileOrderActivateSIMError() {
		String strCTNId = [SELECT Id FROM NetProfit_CTN__c].Id;
		//creating JSON minify manually because the correlation Id is the id of the ctn and generate randomly in the test class
		String myJSON =
			'{"correlation_Id":\"' +
			strCTNId +
			'\","action":"ActivateSimForTemplate","status":500,"data":[],"error":[{"message" : "The allocated number [, Simkaartnummer] for  resource is not available. Please allocate new number. sourceSystem  OMS","code" : "500"}]}';

		RestRequest request = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		request.requestUri = baseUrl + '/services/apexrest/UpdateMobileOrder';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);

		RestContext.request = request;
		Test.startTest();
		EMP_UpdateMobileOrder.doPost();
		NetProfit_CTN__c objCTN = [SELECT CTN_Status__c, Order_Summary_BSL_Status__c FROM NetProfit_CTN__c];
		System.assertEquals('SIM Activation failed', objCTN.CTN_Status__c);
		Test.stopTest();
	}

	@isTest
	public static void testUpdateMobileOrderUpdateSettings200() {
		String strCTNId = [SELECT Id FROM NetProfit_CTN__c].Id;
		//creating JSON minify manually because the correlation Id is the id of the ctn and generate randomly in the test class
		String myJSON =
			'{"correlation_Id":\"' +
			strCTNId +
			'\","action":"UpdateProductMultipleSettings","status":200,"data":[{"orderID":"557483381A","quotation_information":{"totalOC":0,"totalRC":33.37,"previousTotalRC":0,"originalTotalOC":0,"discountTotalOC":0,"overrideTotalOC":0,"taxTotalOC":0,"originalTotalRC":33.37,"discountTotalRC":0,"taxTotalRC":7.01,"proratedTotalRC":33.37,"proratedDiscountTotalRC":0,"proratedTaxTotalRC":7.01,"previousTaxTotalRC":0}}],"error":[]}';
		RestRequest request = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		request.requestUri = baseUrl + '/services/apexrest/UpdateMobileOrder';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);

		RestContext.request = request;
		Test.startTest();
		EMP_UpdateMobileOrder.doPost();
		NetProfit_CTN__c objCTN = [SELECT CTN_Status__c, Order_Summary_BSL_Status__c FROM NetProfit_CTN__c];
		System.assertEquals('Update Product Settings confirmed', objCTN.CTN_Status__c);
		Test.stopTest();
	}
	@isTest
	public static void testUpdateMobileOrderUpdateSettingsError() {
		String strCTNId = [SELECT Id FROM NetProfit_CTN__c].Id;
		//creating JSON minify manually because the correlation Id is the id of the ctn and generate randomly in the test class
		String myJSON =
			'{"correlation_Id":\"' +
			strCTNId +
			'\","action":"UpdateProductMultipleSettings","status":500,"data":{},"error":[{"message":"Authorization failed of dealer market restrictions  sourceSystem  BSL","code":"500"}]}';

		RestRequest request = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		request.requestUri = baseUrl + '/services/apexrest/UpdateMobileOrder';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueof(myJSON);

		RestContext.request = request;
		Test.startTest();
		EMP_UpdateMobileOrder.doPost();
		NetProfit_CTN__c objCTN = [SELECT CTN_Status__c, Order_Summary_BSL_Status__c FROM NetProfit_CTN__c];
		System.assertEquals('Update Product Settings failed', objCTN.CTN_Status__c);
		Test.stopTest();
	}
}
