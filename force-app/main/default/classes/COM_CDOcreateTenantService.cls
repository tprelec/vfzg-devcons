@SuppressWarnings('PMD.ExcessivePublicCount')
public with sharing class COM_CDOcreateTenantService {
	public String id;
	public String href;
	public String externalId;
	public String priority;
	public String description;
	public String category;
	public String state;
	public String orderDate;
	public String startDate;
	public String typeVal;
	public OrderItem[] orderItem;
	public class OrderItem {
		public String id;
		public String action;
		public String name;
		public String state;
		public String typeVal;
		public Service service;
	}
	public class Service {
		public String id;
		public String name;
		public String typeVal;
		public String serviceState;
		public ServiceCharacteristic[] serviceCharacteristic;
		public ServiceSpecification serviceSpecification;
	}
	public class ServiceSpecification {
		public String id;
		public String invariantID;
		public String version;
		public String name;
		public String typeVal;
		string test;
	}
	public class ServiceCharacteristic {
		public String name;
		public String valueType;
		public GenericValue value;
	}
	public class GenericValue {
		public String typeVal;
		public String valueOf;
	}

	public static COM_CDOcreateTenantService generatePayload() {
		COM_CDOcreateTenantService req = new COM_CDOcreateTenantService();
		COM_CDO_integration_settings__mdt comSetting = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);

		req.externalId = comSetting.externalId__c;
		req.priority = comSetting.priority__c;
		req.description = comSetting.Description__c;
		req.category = comSetting.Category__c;
		req.typeVal = comSetting.requestType__c;
		req.orderItem = new List<COM_CDOcreateTenantService.OrderItem>();

		COM_CDOcreateTenantService.OrderItem orderitem = new COM_CDOcreateTenantService.OrderItem();
		orderItem.id = '1';
		orderItem.action = COM_CDO_Constants.COM_CDO_ACTION_ADD;
		orderItem.typeVal = comSetting.orderItemType__c;

		COM_CDOcreateTenantService.Service service = new COM_CDOcreateTenantService.Service();

		service.name = comSetting.serviceName__c;
		service.typeVal = comSetting.serviceType__c;
		COM_CDOcreateTenantService.ServiceSpecification specification = new COM_CDOcreateTenantService.ServiceSpecification();
		specification.id = comSetting.serviceSpecificationId__c;
		specification.invariantID = comSetting.serviceSpecification_invariantID__c;
		specification.version = comSetting.serviceSpecification_version__c;
		specification.name = comSetting.serviceSpecification_name__c;
		specification.typeVal = comSetting.serviceSpecificationType__c;

		service.serviceSpecification = specification;

		COM_CDO_Integration_Article_setting__mdt sourceSystem = COM_CDOServiceCharacteristicHelper.getInputServiceCharacteristics(
			COM_CDO_Constants.COM_CDO_SERVICE_CHARACTERISTIC_CREATE_TENANT
		)[0];
		COM_CDOcreateTenantService.ServiceCharacteristic serviceCharacteristic = new COM_CDOcreateTenantService.ServiceCharacteristic();
		serviceCharacteristic.name = sourceSystem.Element_name__c;
		serviceCharacteristic.valueType = comSetting.serviceCharacteristicType__c;

		COM_CDOcreateTenantService.GenericValue genValue = new COM_CDOcreateTenantService.GenericValue();
		genValue.typeVal = comSetting.serviceCharacteristicType__c;
		genValue.valueOf = sourceSystem.ArticleCode__c;
		serviceCharacteristic.value = genValue;
		service.serviceCharacteristic = new List<COM_CDOcreateTenantService.ServiceCharacteristic>();
		service.serviceCharacteristic.add(serviceCharacteristic);
		orderItem.service = service;
		req.orderItem.add(orderItem);
		return req;
	}

	public static String generatePayloadJson(COM_CDOcreateTenantService req) {
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeStringField(COM_CDO_Constants.COM_CDO_EXTERNALID, req.externalId);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_PRIORITY, req.priority);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_DESCRIPTION, req.description);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_CATEGORY, req.category);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, req.typeVal);
		gen.writeFieldName(COM_CDO_Constants.COM_CDO_ORDER_ITEM);
		gen.writeStartArray();
		for (COM_CDOcreateTenantService.OrderItem orderItem : req.orderItem) {
			gen.writeStartObject();
			gen.writeStringField(COM_CDO_Constants.COM_CDO_ID, orderItem.id);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_ACTION, orderItem.action);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, orderItem.typeVal);
			gen.writeFieldName(COM_CDO_Constants.COM_CDO_SERVICE);
			gen.writeStartObject();
			gen.writeStringField(COM_CDO_Constants.COM_CDO_NAME, orderItem.service.name);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, orderItem.service.typeVal);
			gen.writeFieldName(COM_CDO_Constants.COM_CDO_SERVICE_CHARACTERISTICS);
			gen.writeStartArray();
			for (COM_CDOcreateTenantService.ServiceCharacteristic serviceCharacteristic : orderItem.service.serviceCharacteristic) {
				gen.writeStartObject();
				gen.writeStringField(COM_CDO_Constants.COM_CDO_NAME, serviceCharacteristic.name);
				gen.writeStringField(COM_CDO_Constants.COM_CDO_VALUE_TYPE, serviceCharacteristic.value.typeVal);
				gen.writeFieldName(COM_CDO_Constants.COM_CDO_VALUE);
				gen.writeStartObject();
				gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, serviceCharacteristic.value.typeVal);
				gen.writeStringField(COM_CDO_Constants.COM_CDO_SOURCE_SYSTEM, serviceCharacteristic.value.valueOf);
				gen.writeEndObject();
				gen.writeEndObject();
			}
			gen.writeEndArray();

			gen.writeFieldName(COM_CDO_Constants.COM_CDO_SERVICE_SPECIFICATION);
			gen.writeStartObject();
			gen.writeStringField(COM_CDO_Constants.COM_CDO_ID, orderItem.service.serviceSpecification.id);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_INVARIANTID, orderItem.service.serviceSpecification.invariantID);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_VERSION, orderItem.service.serviceSpecification.version);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_NAME, orderItem.service.serviceSpecification.name);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, orderItem.service.serviceSpecification.typeVal);
			gen.writeEndObject();

			gen.writeEndObject();
		}
		gen.writeEndObject();
		gen.writeEndArray();
		gen.writeEndObject();

		return gen.getAsString();
	}
}
