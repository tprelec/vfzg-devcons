/**
 * @description      This is the class that contains logic for key ring lite
 *
 * @author        Ashok
 */
//@SuppressWarnings('PMD.CyclomaticComplexity')
public with sharing class KeyRingService {
	private static final Set<String> BAN_DETAILS_SET = new Set<String>{ 'Id', 'Account__r.BOPCode__c' };

	/**
	 * @description         This method returns set of Accounts
	 */
	public static Set<Id> retrieveAccountIdsWithBopCode(Set<Id> accountIds) {
		Set<Id> accountIdsWithBopCode = new Set<Id>();
		Set<Id> banAccountIds = new Set<Id>();
		Map<Id, Ban__c> banMap = BanDAO.getBanDetailsForRelatedAccounts(BAN_DETAILS_SET, accountIds);
		for (Ban__c b : banMap.values()) {
			if (b.Account__r.BOPCode__c != null) {
				accountIdsWithBopCode.add(b.Account__c);
			} else {
				banAccountIds.add(b.Account__c);
			}
		}
		if (!banAccountIds.isEmpty()) {
			accountIdsWithBopCode.addAll(externalBopAccounts(banAccountIds));
		}
		return accountIdsWithBopCode;
	}

	/**
	 * @description         This method returns set of External account (BOP only)
	 */
	public static Set<Id> externalBopAccounts(Set<Id> accountIds) {
		Set<Id> accountIdsWithBopCode = new Set<Id>();

		for (External_Account__c externalAcc : [
			SELECT Id, Account__c, External_Account_Id__c, External_Source__c
			FROM External_Account__c
			WHERE Account__c IN :accountIds AND External_Source__c = 'BOP'
		]) {
			accountIdsWithBopCode.add(externalAcc.Account__c);
		}

		return accountIdsWithBopCode;
	}

	/**
	 * @description         This method returns list of External account (Unify only)
	 */
	public static List<External_Account__c> externalUnifyAccounts(string accountId) {
		List<External_Account__c> extAcctList = new List<External_Account__c>(
			[SELECT Id, External_Account_Id__c FROM External_Account__c WHERE External_Source__c = 'Unify' AND Account__c = :accountId]
		);

		return extAcctList;
	}
	/**
	 * @description         This method returns map of External account (BOP only)
	 */
	public static Map<string, string> getexternalBopAccount(Set<Id> accountIds) {
		Map<string, string> accountIdsWithBopCodeMap = new Map<string, string>();

		for (External_Account__c externalAcc : [
			SELECT Id, Account__c, External_Account_Id__c, External_Source__c
			FROM External_Account__c
			WHERE Account__c IN :accountIds AND External_Source__c = 'BOP'
		]) {
			accountIdsWithBopCodeMap.put(externalAcc.Account__c, externalAcc.External_Account_Id__c);
		}

		return accountIdsWithBopCodeMap;
	}

	/**
	 * @description         This method returns List of External account
	 */
	public static List<External_Account__c> getExternalAccountsList(Order_Response__c resp, Map<Id, List<External_Account__c>> extAcctMap) {
		List<External_Account__c> extAccountList = new List<External_Account__c>();
		Boolean extAccntMatch = false;
		Id sfdcAccntId = resp.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Account__c;
		if (extAcctMap.containsKey(sfdcAccntId)) {
			for (External_Account__c ea : extAcctMap.get(sfdcAccntId)) {
				if (ea.External_Account_Id__c == resp.Unify_Account_Id__c) {
					extAccntMatch = true;
					break;
				}
			}
		}
		if (!extAccntMatch) {
			External_Account__c newExtAccount = new External_Account__c(
				External_Account_Id__c = resp.Unify_Account_Id__c,
				External_Source__c = 'Unify',
				Account__c = sfdcAccntId
			);
			extAccountList.add(newExtAccount);
		}
		return extAccountList;
	}

	/**
	 * @description         This method returns External site based on order response and site map
	 */

	public static List<External_Site__c> getExternalSitesList(Order_Response__c resp, Map<Id, List<External_Site__c>> extSiteMap) {
		List<External_Site__c> extSiteList = new List<External_Site__c>();
		Boolean extSiteMatch = false;
		if (extSiteMap.keySet().contains(resp.SFDC_Site_Id__c)) {
			for (External_Site__c es : extSiteMap.get(resp.SFDC_Site_Id__c)) {
				if (es.External_Site_Id__c == resp.SFDC_Site_Id__c) {
					es.SLSAPID__c = resp.Unify_Site_AP_Id__c != null ? resp.Unify_Site_AP_Id__c : es.SLSAPID__c;
					extSiteList.add(es);
					extSiteMatch = true;
				}
			}
		}
		if (!extSiteMatch) {
			External_Site__c newExtSite = new External_Site__c(
				External_Site_Id__c = resp.Unify_Site_Id__c,
				External_Source__c = 'Unify',
				Site__c = resp.SFDC_Site_Id__c,
				SLSAPID__c = resp.Unify_Site_AP_Id__c,
				External_Account__r = new External_Account__c(External_Account_Id__c = resp.Unify_Account_Id__c)
			);
			extSiteList.add(newExtSite);
		}
		return extSiteList;
	}

	/**
	 * @description         This method returns list of  External account(BOP) based on order response and  map
	 */
	public static Map<String, List<External_Account__c>> getExternalBOPAccountsList(
		Order_Response__c resp,
		Map<Id, List<External_Account__c>> extAcctMap
	) {
		List<External_Account__c> extAccountListToInsert = new List<External_Account__c>();
		List<External_Account__c> extAccountListToUpdate = new List<External_Account__c>();
		Boolean extAccntMatch = false;
		Id sfdcAccntId = resp.Order__r?.Billing_Arrangement__r?.Financial_Account__r?.Ban__r?.Account__c;
		Id relatedExtAccountId = resp.Order__r?.Billing_Arrangement__r?.Financial_Account__r?.Ban__r?.ExternalAccount__c;
		if (extAcctMap.keySet().contains(sfdcAccntId)) {
			for (External_Account__c ea : extAcctMap.get(sfdcAccntId)) {
				if (ea.External_Account_Id__c == resp.BAN_BopCode__c) {
					extAccntMatch = true;
					break;
				}
			}
		}
		if (!extAccntMatch && sfdcAccntId != null) {
			External_Account__c newExtAccount = new External_Account__c(
				External_Account_Id__c = resp.BAN_BopCode__c,
				External_Source__c = 'BOP',
				Account__c = sfdcAccntId
			);
			extAccountListToInsert.add(newExtAccount);
			// Make sure to connect the unify and bop ext accounts making use of the connection through the identified ban->external account
			if (String.isNotEmpty(relatedExtAccountId)) {
				External_Account__c unifyExtAccount = new External_Account__c(
					Id = relatedExtAccountId,
					Related_External_Account__r = new External_Account__c(External_Account_Id__c = resp.BAN_BopCode__c)
				);
				extAccountListToUpdate.add(unifyExtAccount);
			}
		}
		return new Map<String, List<External_Account__c>>{ 'insert' => extAccountListToInsert, 'update' => extAccountListToUpdate };
	}

	public static String getKeyringKey(String unifyAccountId, String unifyContactId) {
		return unifyAccountId + '-' + unifyContactId;
	}
	/**
	 * @description         This method returns list of  External contact based on order response and  map
	 */

	public static List<External_Contact__c> getExternalContactsList(Order_Response__c resp, Map<Id, List<External_Contact__c>> extConMap) {
		List<External_Contact__c> extContactList = new List<External_Contact__c>();
		Boolean extContMatch = false;

		String unifyAccountId = resp.Unify_Account_Id__c;
		String unifyContactId = resp.Unify_Contact_Id__c;
		String sfdcContactId = resp.SFDC_Contact_Id__c;

		String extContactKey = getKeyringKey(unifyAccountId, unifyContactId);

		if (unifyAccountId == null) {
			return extContactList; // TODO: throw exception instead
		}

		if (extConMap.containsKey(sfdcContactId)) {
			for (External_Contact__c ec : extConMap.get(sfdcContactId)) {
				if (ec.ExternalId__c == extContactKey) {
					// Match on unify account id and unify contact id (key)
					extContMatch = true;
					break;
				}
			}
		}
		if (!extContMatch) {
			extContactList.add(createExternalContact(unifyContactId, new Contact(Id = sfdcContactId), unifyAccountId));
		}
		return extContactList;
	}

	public static External_Contact__c createExternalContact(String unifyContactId, Contact c, String unifyAccountId) {
		String extContactKey = getKeyringKey(unifyAccountId, unifyContactId);

		External_Contact__c newExtContact = new External_Contact__c();
		newExtContact.External_Contact_Id__c = unifyContactId;
		newExtContact.External_Source__c = 'Unify';
		if (c.Id != null) {
			newExtContact.Contact__c = c.Id; // Seems ..__r = new Contact(Id=someId) is not working
		} else {
			newExtContact.Contact__r = c;
		}

		newExtContact.ExternalId__c = extContactKey;
		newExtContact.External_Account__r = new External_Account__c(External_Account_Id__c = unifyAccountId);

		return newExtContact;
	}

	/**
	 * @description         This method returns map of Id and  external sites
	 */
	public static Map<Id, List<External_Site__c>> getExternalSite(Set<Id> siteIds) {
		Map<Id, List<External_Site__c>> extSiteMap = new Map<Id, List<External_Site__c>>();

		if (!siteIds.isEmpty()) {
			List<External_Site__c> extSiteList = [SELECT Id, Site__c, External_Site_Id__c FROM External_Site__c WHERE Site__c IN :siteIds];
			for (External_Site__c es : extSiteList) {
				if (extSiteMap.containsKey(es.Site__c)) {
					extSiteMap.get(es.Site__c).add(es);
				} else {
					extSiteMap.put(es.Site__c, new List<External_Site__c>{ es });
				}
			}
		}

		return extSiteMap;
	}

	/**
	 * @description         This method gets external account
	 */
	public static Map<Id, List<External_Account__c>> getExternalAccount(Set<String> acctIds) {
		Map<Id, List<External_Account__c>> extAcctMap = new Map<Id, List<External_Account__c>>();

		if (!acctIds.isEmpty()) {
			List<External_Account__c> extAcctList = [
				SELECT Id, Account__c, External_Account_Id__c
				FROM External_Account__c
				WHERE External_Account_Id__c IN :acctIds
			];
			for (External_Account__c ea : extAcctList) {
				if (extAcctMap.containsKey(ea.Account__c)) {
					extAcctMap.get(ea.Account__c).add(ea);
				} else {
					extAcctMap.put(ea.Account__c, new List<External_Account__c>{ ea });
				}
			}
		}

		return extAcctMap;
	}

	/**
	 * @description         This method gets external contact
	 */
	public static Map<Id, List<External_Contact__c>> getExternalcontact(Set<String> conIds) {
		Map<Id, List<External_Contact__c>> extConMap = new Map<Id, List<External_Contact__c>>();

		if (!conIds.isEmpty()) {
			List<External_Contact__c> extConList = [
				SELECT Id, Contact__c, External_Contact_Id__c
				FROM External_Contact__c
				WHERE Contact__c IN :conIds
			];
			for (External_Contact__c ec : extConList) {
				if (extConMap.containsKey(ec.Contact__c)) {
					extConMap.get(ec.Contact__c).add(ec);
				} else {
					extConMap.put(ec.Contact__c, new List<External_Contact__c>{ ec });
				}
			}
		}

		return extConMap;
	}

	/**
	 * @description         This method gets external sites
	 */
	public static List<External_Site__c> getExternalsites(String siteId, String extAccId, String extSource) {
		List<External_Site__c> extSiteList = new List<External_Site__c>(
			[
				SELECT Id, SLSAPID__c
				FROM External_Site__c
				WHERE Site__c = :siteId AND External_Account__c = :extAccId AND External_Source__c = :extSource
			]
		);
		return extSiteList;
	}

	/**
	 * @description : get external sites list
	 */
	public static List<External_Site__c> getExternalsitesList(Set<Id> extAcctIds, String extSource) {
		List<External_Site__c> extSiteList = new List<External_Site__c>(
			[
				SELECT Id, SLSAPID__c, Site__c, External_Account__c
				FROM External_Site__c
				WHERE External_Account__c IN :extAcctIds AND External_Source__c = :extSource
			]
		);
		return extSiteList;
	}

	public static Map<String, List<External_Account__c>> buildBopCodeMap(Set<Id> accountIds) {
		Map<String, List<External_Account__c>> bopCodeMap = new Map<String, List<External_Account__c>>();

		for (External_Account__c externalAcc : [
			SELECT Id, Account__c, External_Account_Id__c, External_Source__c
			FROM External_Account__c
			WHERE Account__c IN :accountIds AND External_Source__c = 'BOP'
		]) {
			if (!bopCodeMap.containsKey(externalAcc.Account__c)) {
				bopCodeMap.put(externalAcc.Account__c, new List<External_Account__c>());
			}
			bopCodeMap.get(externalAcc.Account__c).add(externalAcc);
		}

		return bopCodeMap;
	}

	public static Map<Id, String> retrieveBANCodeFromOrders(Set<Order__c> orderSet) {
		Map<Id, String> banCodeMap = new Map<Id, String>();
		for (Order__c o : orderSet) {
			if (
				String.isNotBlank(o?.VF_Contract__r?.Opportunity__r?.BAN__r.ExternalAccount__r?.Related_External_Account__r?.External_Account_Id__c)
			) {
				// todo, make sure this is queried
				banCodeMap.put(o.Id, o.VF_Contract__r.Opportunity__r.BAN__r.ExternalAccount__r.Related_External_Account__r.External_Account_Id__c);
			}
		}

		return banCodeMap;
	}

	public static List<External_Account__c> externalAccountsForBop(string accountId) {
		return getExternalBopAccounts(new Set<Id>{ accountId }).get(accountId);
	}

	public static Map<Id, List<External_Account__c>> getExternalBopAccounts(set<Id> accountIdSet) {
		Map<Id, List<External_Account__c>> extAcctMap = new Map<Id, List<External_Account__c>>();
		List<External_Account__c> extAcctList = new List<External_Account__c>(
			[SELECT Id, External_Account_Id__c, Account__c FROM External_Account__c WHERE External_Source__c = 'BOP' AND Account__c IN :accountIdSet]
		);
		for (External_Account__c ea : extAcctList) {
			if (extAcctMap.containsKey(ea.Account__c)) {
				extAcctMap.get(ea.Account__c).add(ea);
			} else {
				extAcctMap.put(ea.Account__c, new List<External_Account__c>{ ea });
			}
		}

		return extAcctMap;
	}

	public static String getExternalBopCode(string accountId) {
		String bopCode = '';
		List<External_Account__c> extAcctList = new List<External_Account__c>(
			[SELECT Id, External_Account_Id__c FROM External_Account__c WHERE External_Source__c = 'BOP' AND Account__c = :accountId]
		);

		if (extAcctList.size() > 0) {
			bopCode = extAcctList[0].External_Account_Id__c;
		}
		return bopCode;
	}
}
