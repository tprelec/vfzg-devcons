@isTest
public class TestECSLocationService {
	@TestSetup
	static void makeData() {
		TestUtils.autoCommit = true;
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'AFD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		Ban__c ban = TestUtils.createBan(acct);
		ban.BOP_export_datetime__c = System.now();
		update ban;
		TestUtils.autoCommit = false;
		Site__c s1 = TestUtils.createSite(acct);
		s1.Last_dsl_check__c = System.today().addDays(-1);
		insert s1;
	}

	@isTest
	public static void testMakeCreateRequest() {
		String operation = 'create';
		Site__c theSite = [
			SELECT
				Id,
				Name,
				Location_Type__c,
				Location_Alias__c,
				Building__c,
				SLA__r.BOP_Code__c,
				Site_Account__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Site_Postal_Code__c,
				Site_City__c,
				Site_Phone__c,
				Site_Street__c,
				Country__c,
				Canvas_Street__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_City__c,
				Canvas_Country__c,
				Site_Account__r.BOPCode__c,
				BOP_export_Errormessage__c
			FROM Site__c
			WHERE Site_Postal_Code__c = '1234AB'
			LIMIT 1
		];

		ECSLocationService service = new ECSLocationService();
		List<ECSLocationService.Request> requests = new List<ECSLocationService.Request>();
		ECSLocationService.request req = new ECSLocationService.request();

		// reference to company
		req.companyBanNumber = '123456';
		req.companyCorporateId = '000randomId';
		req.companyBopCode = 'AFD';

		// key fields
		req.locationReferenceId = theSite.Id;
		req.housenumber = Integer.valueOf(theSite.Site_House_Number__c);
		req.housenumberExt = theSite.Site_House_Number_Suffix__c;
		req.zipcode = theSite.Site_Postal_Code__c;

		// detail fields
		req.name = theSite.Name;
		req.locationTypeName = theSite.Location_Type__c;
		req.locationAlias = theSite.Location_Alias__c;
		req.building = theSite.Building__c;
		req.sla = theSite?.SLA__r?.BOP_Code__c;
		req.street = theSite.Site_Street__c;
		req.city = theSite.Site_City__c;
		req.countryId = theSite.Country__c;
		req.phone = theSite.Site_Phone__c;
		req.canvasStreet = theSite.Canvas_Street__c;
		req.canvasHousenumber = Integer.valueOf(theSite.Canvas_House_Number__c);
		req.canvasHousenumberExt = theSite.Canvas_House_Number_Suffix__c;
		req.canvasZipcode = theSite.Canvas_Postal_Code__c;
		req.canvasCity = theSite.Canvas_City__c;
		req.canvasCountryId = theSite.Canvas_Country__c;

		requests.add(req);

		Test.setMock(WebServiceMock.class, new ECSSOAPLocationMocks.CreateUpdateLocationsResponse());
		Test.startTest();
		service.setRequest(requests);
		service.makeRequest(operation);
		List<ECSLocationService.response> responseList = service.getResponse();
		System.assertEquals('1111AA', responseList[0].zipcode, 'Bop Code returned: ' + responseList[0].zipcode);
		Test.stopTest();
	}

	@isTest
	public static void testMakeUpdateRequest() {
		String operation = 'update';
		Site__c theSite = [
			SELECT
				Id,
				Name,
				Location_Type__c,
				Location_Alias__c,
				Building__c,
				SLA__r.BOP_Code__c,
				Site_Account__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Site_Postal_Code__c,
				Site_City__c,
				Site_Phone__c,
				Site_Street__c,
				Country__c,
				Canvas_Street__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_City__c,
				Canvas_Country__c,
				Site_Account__r.BOPCode__c,
				BOP_export_Errormessage__c
			FROM Site__c
			WHERE Site_Postal_Code__c = '1234AB'
			LIMIT 1
		];

		ECSLocationService service = new ECSLocationService();
		List<ECSLocationService.Request> requests = new List<ECSLocationService.Request>();
		ECSLocationService.request req = new ECSLocationService.request();

		// reference to company
		req.companyBanNumber = '123456';
		req.companyCorporateId = '000randomId';
		req.companyBopCode = 'AFD';

		// key fields
		req.locationReferenceId = theSite.Id;
		req.housenumber = Integer.valueOf(theSite.Site_House_Number__c);
		req.housenumberExt = theSite.Site_House_Number_Suffix__c;
		req.zipcode = theSite.Site_Postal_Code__c;

		// detail fields
		req.name = theSite.Name;
		req.locationTypeName = theSite.Location_Type__c;
		req.locationAlias = theSite.Location_Alias__c;
		req.building = theSite.Building__c;
		req.sla = theSite?.SLA__r?.BOP_Code__c;
		req.street = theSite.Site_Street__c;
		req.city = theSite.Site_City__c;
		req.countryId = theSite.Country__c;
		req.phone = theSite.Site_Phone__c;
		req.canvasStreet = theSite.Canvas_Street__c;
		req.canvasHousenumber = Integer.valueOf(theSite.Canvas_House_Number__c);
		req.canvasHousenumberExt = theSite.Canvas_House_Number_Suffix__c;
		req.canvasZipcode = theSite.Canvas_Postal_Code__c;
		req.canvasCity = theSite.Canvas_City__c;
		req.canvasCountryId = theSite.Canvas_Country__c;

		requests.add(req);

		Test.setMock(WebServiceMock.class, new ECSSOAPLocationMocks.CreateUpdateLocationsResponse());
		Test.startTest();
		service.setRequest(requests);
		service.makeRequest(operation);
		List<ECSLocationService.response> responseList = service.getResponse();
		System.assertEquals('1111AA', responseList[0].zipcode, 'Bop Code returned: ' + responseList[0].zipcode);
		Test.stopTest();
	}

	@isTest
	public static void testExceptions() {
		Test.startTest();
		ECSLocationService service = new ECSLocationService();
		List<ECSLocationService.request> requests = new List<ECSLocationService.request>();
		ECSLocationService.request req = new ECSLocationService.request();
		requests.add(req);
		service.setRequest(requests);
		try {
			//remove items from list
			requests.clear();
			service.setRequest(requests);
			service.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		try {
			//remove items from list
			requests = null;
			service.setRequest(requests);
			service.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		Test.stopTest();
	}

	@isTest
	public static void testAdditionalErrors() {
		Site__c theSite = [
			SELECT
				Id,
				Name,
				Location_Type__c,
				Location_Alias__c,
				Building__c,
				SLA__r.BOP_Code__c,
				Site_Account__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Site_Postal_Code__c,
				Site_City__c,
				Site_Phone__c,
				Site_Street__c,
				Country__c,
				Canvas_Street__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_City__c,
				Canvas_Country__c,
				Site_Account__r.BOPCode__c,
				BOP_export_Errormessage__c
			FROM Site__c
			WHERE Site_Postal_Code__c = '1234AB'
			LIMIT 1
		];

		ECSLocationService service = new ECSLocationService();
		List<ECSLocationService.Request> requests = new List<ECSLocationService.Request>();
		ECSLocationService.request req = new ECSLocationService.request();

		// reference to company
		req.companyBanNumber = '123456';
		req.companyCorporateId = '000randomId';
		req.companyBopCode = 'AFD';

		// key fields
		req.locationReferenceId = theSite.Id;
		req.housenumber = Integer.valueOf(theSite.Site_House_Number__c);
		req.housenumberExt = theSite.Site_House_Number_Suffix__c;
		req.zipcode = theSite.Site_Postal_Code__c;

		// detail fields
		req.name = theSite.Name;
		req.locationTypeName = theSite.Location_Type__c;
		req.locationAlias = theSite.Location_Alias__c;
		req.building = theSite.Building__c;
		req.sla = theSite?.SLA__r?.BOP_Code__c;
		req.street = theSite.Site_Street__c;
		req.city = theSite.Site_City__c;
		req.countryId = theSite.Country__c;
		req.phone = theSite.Site_Phone__c;
		req.canvasStreet = theSite.Canvas_Street__c;
		req.canvasHousenumber = Integer.valueOf(theSite.Canvas_House_Number__c);
		req.canvasHousenumberExt = theSite.Canvas_House_Number_Suffix__c;
		req.canvasZipcode = theSite.Canvas_Postal_Code__c;
		req.canvasCity = theSite.Canvas_City__c;
		req.canvasCountryId = theSite.Canvas_Country__c;

		requests.add(req);
		service.setRequest(requests);
		Test.startTest();
		try {
			service.makeRequest('create');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to create the location(s): '),
				'when there is no mock Create'
			);
		}
		try {
			service.makeRequest('update');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to update the location(s): '),
				'when there is no mock Update'
			);
		}
		Test.stopTest();
	}
}
