/**
 * @description       : Apex Test Class for the DealItemTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestDealItemTriggerHandler {
	@testSetup
	static void setup() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'Deal Item',
			External_Number__c = 1,
			Object_Prefix__c = 'DI-03-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;
	}

	@isTest
	static void testSetExternalIds() {
		Deal_Item__c objDealItem = new Deal_Item__c();

		Test.startTest();
		insert objDealItem;
		Test.stopTest();

		Deal_Item__c objDealItemUpdated = [SELECT Id, ExternalID__c FROM Deal_Item__c WHERE Id = :objDealItem.Id LIMIT 1];

		System.assertEquals('DI-03-000002', objDealItemUpdated.ExternalID__c, 'The External ID on Deal Item was not set correctly.');
	}
}
