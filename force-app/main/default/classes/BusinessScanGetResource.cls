/**
 * @description       : Apex class for creating Scheduler events for Business Scan
 * @author            : Saurabh Pandey
 * @group             : LeadsUnited
 * @last modified on  : 06-20-2022
 * @last modified by  : Saurabh
 **/
public class BusinessScanGetResource {
	@InvocableMethod(label='Get Resource' description='Get Resource' category='Salesforce Scheduler')
	public static List<string> getResource(List<String> timeslots) {
		// timeslot will be datetime
		List<string> responseList = new List<string>();
		Map<string, Salesforce_Scheduler_Api_Configuration__mdt> fieldwithMdtObjectMap = new Map<string, Salesforce_Scheduler_Api_Configuration__mdt>();
		Event schedulerEvent = new Event();
		List<schedulerWrapper> schWrapper;
		for (String timeslot : timeslots) {
			List<Salesforce_Scheduler_Api_Configuration__mdt> businessScanRecordsList = Salesforce_Scheduler_Api_Configuration__mdt.getAll().values();
			for (Salesforce_Scheduler_Api_Configuration__mdt schMdt : businessScanRecordsList) {
				if (schMdt.MasterLabel == 'Business Scan') {
					fieldwithMdtObjectMap.put(schMdt.DeveloperName, schMdt);
				}
			}

			//replace T from timeslot if it exists
			timeslot = timeslot.replace('T', ' ');
			lxscheduler.WorkType workType = new lxscheduler.WorkTypeBuilder().setId(fieldwithMdtObjectMap.get('WorkType').Value__c).build();

			lxscheduler.GetAppointmentCandidatesInput input = new lxscheduler.GetAppointmentCandidatesInputBuilder()
				.setWorkType(workType)
				.setTerritoryIds(new List<String>{ fieldwithMdtObjectMap.get('TerritoryId').Value__c })
				.setStartTime(datetime.valueOfgmt(timeslot).format('yyyy-MM-dd\'T\'HH:mm:ssZ'))
				.setEndTime(datetime.valueOfgmt(timeslot).addhours(1).format('yyyy-MM-dd\'T\'HH:mm:ssZ'))
				.setAccountId(fieldwithMdtObjectMap.get('AccountId').Value__c)
				.setSchedulingPolicyId(fieldwithMdtObjectMap.get('SchedulingPolicyId').Value__c)
				.setApiVersion(Double.valueOf('54.0'))
				.build();

			String response = lxscheduler.SchedulerResources.getAppointmentCandidates(input);
			//parse response
			schWrapper = (List<schedulerWrapper>) System.JSON.deserialize(response, List<schedulerWrapper>.class);
		}

		//  If a service resource is returned by the Scheduler Service
		if (!schWrapper.isEmpty() && !schWrapper.get(0).resources.isEmpty()) {
			List<ServiceResource> resourceList = new List<ServiceResource>();
			resourceList = [SELECT id, RelatedRecordId FROM ServiceResource WHERE id = :schWrapper[0].resources[0]];
			String dt = timeslots[0].replace('T', ' ');
			String dtDate = dt.split(' ')[0];
			String dtTime = dt.split(' ')[1];
			List<String> dtDateList = dtDate.split('-');
			List<String> dtTimeList = dtTime.split(':');
			Datetime eventDT = Datetime.newInstanceGmt(
				Integer.valueOf(dtDateList[0]),
				Integer.valueOf(dtDateList[1]),
				Integer.valueOf(dtDateList[2]),
				Integer.valueOf(dtTimeList[0]),
				Integer.valueOf(dtTimeList[1]),
				Integer.valueOf(dtTimeList[2].replace('Z', ''))
			);

			schedulerEvent.Subject = 'Business Scan Appointment';
			schedulerEvent.IsAllDayEvent = false;
			schedulerEvent.IsRecurrence = false;
			schedulerEvent.OwnerId = !resourceList.isEmpty()
				? resourceList[0]?.RelatedRecordId
				: fieldwithMdtObjectMap.get('DefaultResource').Value__c;
			schedulerEvent.ShowAs = 'Busy';
			schedulerEvent.StartDateTime = eventDT;
			schedulerEvent.EndDateTime = eventDT.addhours(1); //DateTime.now().addHours(1);
			try {
				insert schedulerEvent;
				responseList.add(schedulerEvent.Id);
			} catch (Exception e) {
				system.debug('Exception Occured while creating the Business Scan Event' + e.getMessage() + ' : ' + e.getLineNumber());
			}
		}

		return responseList;
	}
	public class SchedulerWrapper {
		public String startTime { get; set; }
		public String endTime { get; set; }
		public List<String> resources { get; set; }
		public String territoryId { get; set; }
	}
}
