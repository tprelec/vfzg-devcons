public virtual class JSONGeneratorServiceBase {
	protected JSONGenerator generator;

	public JSONGeneratorServiceBase(JSONGenerator generator) {
		this.generator = generator;
	}

	protected virtual void generateStringOrNullField(String fieldName, String fieldValue, String type) {
		if (type.equals('canvasAddress')) {
			if (String.isBlank(fieldValue)) {
				generator.writeNullField(fieldName);
			} else {
				generator.writeStringField(fieldName, fieldValue);
			}
		} else {
			generator.writeStringField(fieldName, fieldValue);
		}
	}
}
