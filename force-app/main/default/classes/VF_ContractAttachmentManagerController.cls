/*
 *    @Description:    Used for controlling the contract attachments management page. Requires WITHOUT SHARING as other ways to edit ContractAttachments will be disabled
 *    @Author:     Guy Clairbois, Marcel Vreuls
 */
public without sharing class VF_ContractAttachmentManagerController {

  /*
   *    Variables
   */
  public Id contractId {get;set;}
  public VF_Contract__c contract {get;set;}
  public List<AttachmentWrapper> contractAttachments {get;set;}
  public List<AttachmentWrapper> newContractAttachments {get;set;}
  public string SelectedCaId { get; set; }
  //public Boolean signedSLA {get;set;}
  public Boolean signedContract {get;set;}
  public Boolean errorFound {get;set;}
  public Boolean hasEditRightsOnContract {get;set;}

  // the number of new attachments to add to the list when the user clicks 'Add More'
  public static final Integer NUM_ATTACHMENTS_TO_ADD=5;

  /*
   *    Controller
   */

  public VF_ContractAttachmentManagerController() {
    contractId = Apexpages.currentPage().getParameters().get('contractId');
    errorFound = false;
    refreshPage=false;
    if(contractAttachments==null){
      LoadData();
      addMore();
    }

  }

  public VF_ContractAttachmentManagerController(ApexPages.StandardController controller) {
    contractId = controller.getId();
    errorFound = false;
    refreshPage=false;
    if(contractAttachments==null){
      LoadData();
      addMore();
    }
  }  

  private void LoadData(){
    contractAttachments = new List<AttachmentWrapper>();
    newContractAttachments = new List<AttachmentWrapper>();

    // get contract
    List<VF_Contract__c> contracts = [Select 
            Id, 
            Name,
            Account__c,
            Hidden_Sum_of_Attachments__c
          from 
            VF_Contract__c 
          Where 
            Id = :contractId];
    if(!contracts.isEmpty()){
      contract = contracts[0];
    } else {
      ApexPages.addMessage(New ApexPages.Message(ApexPages.Severity.ERROR,'Contract not found'));
      errorFound = true;
      return;
    }

    // check contract access
    UserRecordAccess ua = [SELECT 
                  RecordId, 
                  HasEditAccess 
                FROM 
                  UserRecordAccess
                   WHERE
                     UserId = :System.UserInfo.getUserId()
                     AND RecordId = :contract.Id];
       hasEditRightsOnContract = ua.HasEditAccess;

       // get contract attachments
    Map<Id,AttachmentWrapper> tempMap = new Map<Id,AttachmentWrapper>();
    List<Contract_Attachment__c> tempList = new List<Contract_Attachment__c>();     // use list to maintain sorted order
    Map<Id,AttachmentWrapper> fileMap = new Map<Id,AttachmentWrapper>();        // Maps contentDocumentID to wrapper
    Map<id,Contract_Attachment__c> caMap = new Map<Id,Contract_Attachment__c>();
    Set<ID> contentSet = new Set<ID>();    
    for(Contract_Attachment__c ca :[Select 
                  Id, 
                  Name,
                  Attachment_Type__c, 
                  Description_Optional__c,
                  CreatedById,
                  CreatedDate,
                  Signed_Contract__c,
                  ContentDocumentId__c                  

                From 
                  Contract_Attachment__c 
                Where 
                  Contract_VF__c = :contractId
                Order By Attachment_Type__c
                ])
    {
      if(!GeneralUtils.currentUserIsPartnerUser() || !getHiddenContractTypes().contains(ca.Attachment_Type__c)){
        tempList.add(ca);
        tempMap.put(ca.Id,new AttachmentWrapper(ca,null));
        caMap.put(ca.id,ca);          
      }

    }
    // 1 - Get the ContentDocumentLink Records for the contract attachments
    Set<ID> caSet = caMap.keySet();
    if (caSet.size()>0) {
      for (ContentDocumentLink link : [select ContentDocumentId, LinkedEntityId from ContentDocumentLink where LinkedEntityId in:caSet]) {
        // 2 - Use results to Populate a Map (fileMap) where key is contentDocumentID and result is new AttachmentWrapper
        Contract_Attachment__c ca = caMap.get(link.LinkedEntityId);
        fileMap.put(link.ContentDocumentId,new AttachmentWrapper(ca,null));
        contentSet.add(link.ContentDocumentId);      
        if (ca.ContentDocumentID__c==null) ca.ContentDocumentID__c=link.ContentDocumentId;
      }

      // 3 - Get the contentversion records for the contentDocuments
      // Get the contentversion record for each contract attachment
      for (contentversion file : [select id, ContentDocumentId, pathOnClient from contentversion where contentdocumentid in:contentSet and isLatest=true]) {
        // 4 - Use the fileMap to get the wrapper and populate with the contentversion      
        fileMap.get(file.ContentDocumentId).file = file;
                       
                 
                                
                        
      }

      if(!GeneralUtils.currentUserIsPartnerUser()){
        for(FeedItem fi : [Select
                    Id,
                    ParentId,
                    ContentFileName,
                    RelatedRecordId
                  from
                    FeedItem
                  Where
                    ParentId in :tempMap.keySet()]){
          tempMap.get(fi.ParentId).feed = fi;
        }
      }

      signedContract = false;
              
      for(Contract_Attachment__c ca : tempList){
        // now go through list to maintain sort order
        contractAttachments.add(fileMap.get(ca.ContentDocumentID__c));          
        if(ca.Attachment_Type__c == 'Contract (Signed)' && fileMap.get(ca.ContentDocumentID__c).file != null)
          signedContract = true;
        if(ca.Attachment_Type__c == 'Amendment (Signed)' && ca.Signed_Contract__c != null && fileMap.get(ca.ContentDocumentID__c).file != null)
          signedContract = true;          
                                             
                      
      }
    }

    // if there are no attachments yet, show 5 empty ones to prevent the user from having to click
    if(tempList.isEmpty()) addMore();
  }

  public class AttachmentWrapper {
    public Contract_Attachment__c ca {get;set;}
    public ContentVersion file {get;set;}
        
                  
                 
       
     
    public FeedItem feed {get;set;}


    public AttachmentWrapper(Contract_Attachment__c ca, ContentVersion file){
      this.ca = ca;
      if(file!= null){
        this.file = file;
          
                                 
      }    
    }

    public FeedItem getHiddenFeedItem(){
      if(file != null) {
        //Adding a Content post
          FeedItem post = new FeedItem();
        post.ParentId = ca.Id;
          post.Body = file.pathOnClient;
          post.ContentData = file.VersionData;
          post.ContentFileName = file.pathOnClient;
          post.Visibility = 'InternalUsers';
          return post;
      }
      return null;

    }
  }

  /*
   *    Page References
   */

  public pageReference addMore(){
    for (Integer idx=0; idx<NUM_ATTACHMENTS_TO_ADD; idx++){

      newContractAttachments.add(new AttachmentWrapper(new Contract_Attachment__c(Contract_VF__c = contractId),new ContentVersion()));

    }
                                                                
                     
    return null;
  }

  public pageReference backToContract(){
    return new PageReference('/'+contractId);
  }

  // for the embedded page, this is the link to the actual attachmentmanager
  public pageReference manageAttachments(){
    return new PageReference('/apex/VF_ContractAttachmentManager?contractId='+ContractId);
  }
  public pageReference nothing(){
    refreshPage=true;
    return null;
  }
  public Boolean refreshPage {get; set;}

  // This is void because we don't want to go to anothe page
  // Save attachments can throw an error when the filename is too long
  // Gerhard Newman
  public void saveAttachmentsFromOrderForm(){

       system.debug('got it2');
     
    saveAttachments();

                                                                                                          
  }
  
  // Note: There is two way linking happening here. The native object ContentDocumentLink links to the contract_attachment__c
  // and the contract_attachment__c links to ContentDocument. This second link is populated in ContentDocumentLinkTriggerHandler  
  public void saveAttachments(){
    List<Contract_Attachment__c> contractAttachmentsToInsert = new List<Contract_Attachment__c>();
    List<Contract_Attachment__c> contractAttachmentsToUpdate = new List<Contract_Attachment__c>();    
    List<ContentDocumentLink> contentDocumentLinkToInsert = new List<ContentDocumentLink>();
    List<ContentVersion> contentVersionToInsert = new List<ContentVersion>();
    List<ContentVersion> contentVersionList = new List<ContentVersion>();    
    List<FeedItem> feedItemsToInsert = new List<FeedItem>();
    for(AttachmentWrapper aw : newContractAttachments){
      system.debug(aw);
      if(aw.ca != null && aw.ca.Attachment_Type__c != null) contractAttachmentsToInsert.add(aw.ca);
      
    }
    Savepoint sp = Database.setSavepoint();
    try{
      system.debug(contractAttachmentsToInsert);
      insert contractAttachmentsToInsert;

      Integer counter = 0;
      for(Contract_Attachment__c ca : contractAttachmentsToInsert){

        if(ca.Id != null){
          system.debug(newContractAttachments[counter]);
          if(newContractAttachments[counter].file != null){
            // Hijack this field to store the Opportunity_Attachment__c ID so we can link it back later on
            newContractAttachments[counter].file.ReasonForChange = ca.Id;          

            // for certain attachment types, don't save as attachment but as (hidden) feed item so that partners cannot see it
            if(!getHiddenContractTypes().contains(ca.Attachment_Type__c)){
              contentVersionToInsert.add(newContractAttachments[counter].file);
            } else {
              feedItemsToInsert.add(newContractAttachments[counter].getHiddenFeedItem());
            }
          }
        }
        counter++;
      }
                        
                        
                     
      insert feedItemsToInsert;

      // Insert the files (ContentVersion)
          insert contentVersionToInsert;  

      // Create the links between the content document and the contract attachment
      // and store the content document id on the contract attachment
      contentVersionList = [SELECT ContentDocumentId, ReasonForChange FROM ContentVersion WHERE Id = :contentVersionToInsert];
      for (ContentVersion cv:contentVersionList) {
        ContentDocumentLink cdl = new ContentDocumentLink();        
        cdl.ContentDocumentId = cv.ContentDocumentId;
        cdl.LinkedEntityId = cv.ReasonForChange;
        cdl.ShareType = 'V';  
        cdl.Visibility  = 'Allusers'; //W-001438  
        contentDocumentLinkToInsert.add(cdl);                      
      }  
      insert contentDocumentLinkToInsert;              

      contractAttachments = null;

    } catch (dmlException de){
      Database.rollback(sp);
      Apexpages.addMessages(de);
    }


        //refresh the data
       LoadData();
    newContractAttachments = new List<AttachmentWrapper>();
    
                                                                
    
  }  

  public void DeleteContractAttachment(){
      // if for any reason we are missing the reference 
        if (SelectedCaId == null) {
           return;
        }
     
    Delete New Contract_Attachment__c(Id=SelectedCaId);
      
     
        //refresh the data
        LoadData();
   }    

   public pageReference DeleteContractAttachmentFromOrderform(){
     system.debug('got it');
     system.debug(SelectedCaId);
       DeleteContractAttachment();
       Id OrderId = ApexPages.currentPage().getParameters().get('orderId');
       return new PageReference('/apex/orderformcustomerdetails?contractId='+contractId+'&orderId='+orderId).setRedirect(true);

   }

   public List<SelectOption> signedContractOptions {
       get{
         if(signedContractOptions == null){
           signedContractOptions = new List<SelectOption>();
           signedContractOptions.add(new SelectOption('','none'));

           // first gather all contract_attachments for the particular customer
           Set<Id> caIds = new Set<Id>();
           for(Contract_Attachment__c ca : [Select 
                             Id 
                           From 
                             Contract_Attachment__c 
                           Where 
                             Attachment_Type__c = 'Contract (Signed)'
                           AND
                             Contract_VF__r.Account__c = :contract.Account__c]){
             caIds.add(ca.Id);
           }

           for(Attachment a : [Select Id, ParentId, Name From Attachment Where ParentId in :caIds]){
             signedContractOptions.add(new SelectOption(a.ParentId,a.Name));
           }
         }
         return signedContractOptions;
       }
       private set;
   }

   private Set<String> getHiddenContractTypes(){
       Set<String> returnSet = new Set<String>();

       for(Attachment_Setting__mdt atts : [select DeveloperName, Label, Attachment_types_hidden_from_partners__c from Attachment_Setting__mdt Where Label = 'default']){
         for(String s : atts.Attachment_types_hidden_from_partners__c.split(';')){
           returnSet.add(s);
         }
       }

       return returnSet;
   }   
}