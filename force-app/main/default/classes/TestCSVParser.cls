/**
 * @description			This is the test class for the CSVParser class.
 * @author				Guy Clairbois
 */
@isTest
public with sharing class TestCSVParser {

	private static String TEST_METHOD_NAME;

	public class TestCSVRow extends CSVParser.CSVRow {
		public String string_col { get; set; }
		public Date date_col { get; set; }
		public Double double_col { get; set; }
		public Integer integer_col { get; set; }
		public Decimal decimal_col { get; set; }
		public DateTime datetime_col { get; set; }
	}
	
	
	public class TestParser extends CSVParser {	

		public TestParser(){
			if(TEST_METHOD_NAME == 'shouldLoadWithExplicitColumnHeadingNamesSet'){
				addExplicitColumnHeaderGroupWithName(
					new Set<String>{
						'STRING_COL',
						'DATE_COL',
						'DOUBLE_COL',
						'INTEGER_COL',
						'DECIMAL_COL',
						'DATETIME_COL'
					}, 
				'test_group');
			} else if(TEST_METHOD_NAME == 'shouldNotLoadWithExplicitColumnHeadingNamesSet'){
				addExplicitColumnHeaderGroupWithName(
					new Set<String>{
						'INVALID1',
						'INVALID2'
					}, 
				'invalid_group');
			} else {
				// Cannot test these methods inside a test method as the methods are not visible
				System.assertEquals(true, getHasHeadingRow(), 'Heading row should be defaulted to true');
				setHasHeadingRow(false);
				System.assertEquals(false, getHasHeadingRow(), 'Heading row should have been set to false');
				setHasHeadingRow(true);
				System.assertEquals(true, getHasHeadingRow(), 'Heading row should have been set to true');
			}
		}
		
		
		public override void readFile(Blob file){
			super.readFile(file);
			
				if(TEST_METHOD_NAME == 'shouldLoadCorrectNumberOfRowsWithValuesPopulatedCorrectly'){
					System.assertEquals(true, hasCSVColumnHeadingNames(new Set<String>{
						'STRING_COL',
						'DATE_COL',
						'DOUBLE_COL',
						'INTEGER_COL',
						'DECIMAL_COL',
						'DATETIME_COL'
					}));
				}
		}
		
		
		protected override CSVParser.CSVRow createCSVRow(){
			return TEST_METHOD_NAME == 'shouldNotLoadIfSubClassDoesNotReturnInstanceOfCSVRow'? null : new TestCSVRow();
		}
		
		
		protected override void setRowCellValue(CSVRow csvRow, String columnName, String cellValue){
			TestCSVRow r = (TestCSVRow) csvRow;
				
				if(columnName == 'STRING_COL'){
					r.string_col = cellValue;
				} else if(columnName == 'DATE_COL'){
					r.date_col = convertToDate(cellValue);
				} else if(columnName == 'DOUBLE_COL'){
					r.double_col = convertToDouble(cellValue);
				} else if(columnName == 'INTEGER_COL'){
					r.integer_col = convertToInteger(cellValue);
				} else if(columnName == 'DECIMAL_COL'){
					r.decimal_col = convertToDecimal(cellValue);
				} else if(columnName == 'DATETIME_COL'){
					r.datetime_col = convertToDateTime(cellValue);
					
				}
		}
		
	}
	

	private static Blob getValidCSV(String delimiter){
		String csv = ''
			+ 'STRING_COL' + delimiter + 'DATE_COL' + delimiter + 'DOUBLE_COL' + delimiter + 'INTEGER_COL' + delimiter + 'DECIMAL_COL' + delimiter + 'DATETIME_COL\r\n'
			+ 'Value1' + delimiter + '11012000' + delimiter + '1' + delimiter + '11' + delimiter + '0.1' + delimiter + '01012001 093050\r\n'
			+ 'Value2' + delimiter + '11012001' + delimiter + '2' + delimiter + '21' + delimiter + '0.2' + delimiter + '01012002 093050\r\n'
			+ 'Value3' + delimiter + '11012002' + delimiter + '3' + delimiter + '31' + delimiter + '0.3' + delimiter + '01012003 093050\r\n'
			+ 'Value4' + delimiter + '11012003' + delimiter + '4' + delimiter + '41' + delimiter + '0.4' + delimiter + '01012004 093050\r\n'
			+ 'Value5' + delimiter + '11012004' + delimiter + '5' + delimiter + '51' + delimiter + '0.5' + delimiter + '01012005 093050\r\n'
			+ 'Value6' + delimiter + '11012005' + delimiter + '6' + delimiter + '61' + delimiter + '0.6' + delimiter + '01012006 093050\r\n'
			+ 'Value7' + delimiter + '11012006' + delimiter + '7' + delimiter + '71' + delimiter + '0.7' + delimiter + '01012007 093050\r\n'
			+ 'Value8' + delimiter + '11012007' + delimiter + '8' + delimiter + '81' + delimiter + '0.8' + delimiter + '01012008 093050\r\n'
			+ 'Value9' + delimiter + '11012008' + delimiter + '9' + delimiter + '91' + delimiter + '0.9' + delimiter + '01012009 093050\r\n'
			+ 'Value10' + delimiter + '11012009' + delimiter + '10' + delimiter + '101' + delimiter + '1' + delimiter + '01012010 093050';
		
		return Blob.valueOf(csv);
	}
	
	
	private static Blob getValidDoubleQuotedCSV(){
		String csv = ''
			+ 'STRING_COL,DATE_COL,DOUBLE_COL,INTEGER_COL,DECIMAL_COL,DATETIME_COL\r\n'
			+ '"Value1, Value1","11012000",1,11,0.1,01012001 093050\r\n'
			+ '"Value2, Value2","11012001",2,21,0.2,01012002 093050\r\n'
			+ '"Value3, Value3","11012002",3,31,0.3,01012003 093050\r\n'
			+ '"Value4, Value4","11012003",4,41,0.4,01012004 093050\r\n'
			+ '"Value5, Value5","11012004",5,51,0.5,01012005 093050\r\n'
			+ '"Value6, Value6","11012005",6,61,0.6,01012006 093050\r\n'
			+ '"Value7, Value7","11012006",7,71,0.7,01012007 093050\r\n'
			+ '"Value8, Value8","11012007",8,81,0.8,01012008 093050\r\n'
			+ '"Value9, Value9","11012008",9,91,0.9,01012009 093050\r\n'
			+ '"Value10, Value10","11012009",10,101,1,01012010 093050';
		
		return Blob.valueOf(csv);
	}
	
	
	private static Blob getInvalidDateCSV(){
		String csv = ''
			+ 'STRING_COL,DATE_COL,DOUBLE_COL,INTEGER_COL,DECIMAL_COL,DATETIME_COL\r\n'
			+ 'Value1,11012000,1,11,0.1,01012001 093050\r\n'
			+ 'Value2,11012001,2,21,0.2,01012002 093050\r\n'
			+ 'Value3,11012002,3,31,0.3,01012003 093050\r\n'
			+ 'Value4,!!!!INVALID!!!!!,4,41,0.4,01012004 093050\r\n'
			+ 'Value5,11012004,5,51,0.5,01012005 093050\r\n'
			+ 'Value6,11012005,6,61,0.6,01012006 093050\r\n'
			+ 'Value7,11012006,7,71,0.7,01012007 093050\r\n'
			+ 'Value8,11012007,8,81,0.8,01012008 093050\r\n'
			+ 'Value9,11012008,9,91,0.9,01012009 093050\r\n'
			+ 'Value10,11012009,10,101,1,01012010 093050';
		
		return Blob.valueOf(csv);
	}
	
	
	private static Blob getInvalidDateTimeCSV(){
		String csv = ''
			+ 'STRING_COL,DATE_COL,DOUBLE_COL,INTEGER_COL,DECIMAL_COL,DATETIME_COL\r\n'
			+ 'Value1,11012000,1,11,0.1,01012001 093050\r\n'
			+ 'Value2,11012001,2,21,0.2,01012002 093050\r\n'
			+ 'Value3,11012002,3,31,0.3,01012003 093050\r\n'
			+ 'Value4,11012003,4,41,0.4,01012004 093050\r\n'
			+ 'Value5,11012004,5,51,0.5,!!!!INVALID!!!!!\r\n'
			+ 'Value6,11012005,6,61,0.6,01012006 093050\r\n'
			+ 'Value7,11012006,7,71,0.7,01012007 093050\r\n'
			+ 'Value8,11012007,8,81,0.8,01012008 093050\r\n'
			+ 'Value9,11012008,9,91,0.9,01012009 093050\r\n'
			+ 'Value10,11012009,10,101,1,01012010 093050';
		
		return Blob.valueOf(csv);
	}
	
	
	private static Blob getInvalidDoubleCSV(){
		String csv = ''
			+ 'STRING_COL,DATE_COL,DOUBLE_COL,INTEGER_COL,DECIMAL_COL,DATETIME_COL\r\n'
			+ 'Value1,11012000,1,11,0.1,01012001 093050\r\n'
			+ 'Value2,11012001,2,21,0.2,01012001 093050\r\n'
			+ 'Value3,11012002,3,31,0.3,01012001 093050\r\n'
			+ 'Value4,11012003,4,41,0.4,01012001 093050\r\n'
			+ 'Value5,11012004,5,51,0.5,01012001 093050\r\n'
			+ 'Value6,11012005,6,61,0.6,01012001 093050\r\n'
			+ 'Value7,11012006,7,71,0.7,01012001 093050\r\n'
			+ 'Value8,11012007,8,81,0.8,01012001 093050\r\n'
			+ 'Value9,11012008,9,91,0.9,01012001 093050\r\n'
			+ 'Value10,11012009,!!!!!INVALID!!!!!,101,1,01012001 093050';
		
		return Blob.valueOf(csv);
	}
	
	
	private static Blob getInvalidIntegerCSV(){
		String csv = ''
			+ 'STRING_COL,DATE_COL,DOUBLE_COL,INTEGER_COL,DECIMAL_COL,DATETIME_COL\r\n'
			+ 'Value1,11012000,1,11,0.1,01012001 093050\r\n'
			+ 'Value2,11012001,2,21,0.2,01012001 093050\r\n'
			+ 'Value3,11012002,3,31,0.3,01012001 093050\r\n'
			+ 'Value4,11012003,4,41,0.4,01012001 093050\r\n'
			+ 'Value5,11012004,5,51,0.5,01012001 093050\r\n'
			+ 'Value6,11012005,6,!!!!INVALID!!!!,0.6,01012001 093050\r\n'
			+ 'Value7,11012006,7,71,0.7,01012001 093050\r\n'
			+ 'Value8,11012007,8,81,0.8,01012001 093050\r\n'
			+ 'Value9,11012008,9,91,0.9,01012001 093050\r\n'
			+ 'Value10,11012009,10,101,1,01012001 093050';
		
		return Blob.valueOf(csv);
	}
	
	
	private static Blob getInvalidDecimalCSV(){
		String csv = ''
			+ 'STRING_COL,DATE_COL,DOUBLE_COL,INTEGER_COL,DECIMAL_COL,DATETIME_COL\r\n'
			+ 'Value1,11012000,1,11,0.1,01012001 093050\r\n'
			+ 'Value2,11012001,2,21,!!!!INVALID!!!!,01012001 093050\r\n'
			+ 'Value3,11012002,3,31,0.3,01012001 093050\r\n'
			+ 'Value4,11012003,4,41,0.4,01012001 093050\r\n'
			+ 'Value5,11012004,5,51,0.5,01012001 093050\r\n'
			+ 'Value6,11012005,6,61,0.6,01012001 093050\r\n'
			+ 'Value7,11012006,7,71,0.7,01012001 093050\r\n'
			+ 'Value8,11012007,8,81,0.8,01012001 093050\r\n'
			+ 'Value9,11012008,9,91,0.9,01012001 093050\r\n'
			+ 'Value10,11012009,10,101,1,01012001 093050';
		
		return Blob.valueOf(csv);
	}
	
	
	private static Blob getIncorrectNumberOfValuesOnRowsCSV(){
		String csv = ''
			+ 'STRING_COL,DATE_COL,DOUBLE_COL,INTEGER_COL,DECIMAL_COL,DATETIME_COL\r\n'
			+ 'Value1,11012000,1,11,0.1,01012001 093050\r\n'
			+ 'Value2,11012001,2,21,0.2,01012001 093050\r\n'
			+ 'Value3,11012002,3,31,0.3,01012001 093050\r\n'
			+ 'Value4,11012003,4,41,0.4,01012001 093050\r\n'
			+ 'Value5,11012004,5,51,0.5,01012001 093050,ADDITIONAL VALUE\r\n'
			+ 'Value6,11012005,6,61,0.6,01012001 093050\r\n'
			+ 'Value7,11012006,7,71,0.7,01012001 093050\r\n'
			+ 'Value8,11012007,8,81,0.8,01012001 093050\r\n'
			+ 'Value9,11012008,9,91,0.9,01012001 093050\r\n'
			+ 'Value10,11012009,10,101,1,01012001 093050';
		
		return Blob.valueOf(csv);
	}
	
	private static testMethod void shouldThrowExceptionWhenFileIsNull(){
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(null);
				System.assert(false, 'File should not have been processed, it was null!');
			} catch(CSVParser.ParameterValueException ex){
				System.assertEquals('Cannot parse file when the file is null', ex.getMessage());
			}
	}
	
	
	private static testMethod void shouldLoadCorrectNumberOfRowsWithValuesPopulatedCorrectly(){
		TEST_METHOD_NAME = 'shouldLoadCorrectNumberOfRowsWithValuesPopulatedCorrectly';
		CSVParser testParser = new TestParser();
		testParser.setDelimiter(';');
		testParser.readFile(getValidCSV(testParser.getDelimiter()));
		System.assertEquals(10, testParser.getNumberOfRows(), 'Incorrect number of rows were loaded');
		Integer index = 0;
		
			for(CSVParser.CSVRow csvRow : testParser.getCSVRows()){
				TestCSVRow testRow = (TestCSVRow) csvRow;
				System.assertEquals('Value' + (index + 1), testRow.string_col, 'Incorrect value assigned to string column row cell');
				System.assertEquals(Date.newInstance(2000 + index, 1, 11), testRow.date_col, 'Incorrect value assigned to date column row cell');
				System.assertEquals(index + 1, testRow.double_col, 'Incorrect value assigned to double column row cell');
				System.assertEquals(Integer.valueOf((index + 1) + '1'), testRow.integer_col, 'Incorrect value assigned to integer column row cell');
				index++;
			}
	}
	
	
	private static testMethod void shouldLoadCorrectlyWithValuesSurroundedWithDoubleQuotes(){
		CSVParser testParser = new TestParser();
		testParser.readFile(getValidDoubleQuotedCSV());
		System.assertEquals(10, testParser.getNumberOfRows(), 'Incorrect number of rows were loaded');
		Integer index = 0;
		
			for(CSVParser.CSVRow csvRow : testParser.getCSVRows()){
				TestCSVRow testRow = (TestCSVRow) csvRow;
				System.assertEquals('Value' + (index + 1) + ', ' + 'Value' + (index + 1), testRow.string_col, 'Incorrect value assigned to string column row cell');
				System.assertEquals(Date.newInstance(2000 + index, 1, 11), testRow.date_col, 'Incorrect value assigned to date column row cell');
				System.assertEquals(index + 1, testRow.double_col, 'Incorrect value assigned to double column row cell');
				System.assertEquals(Integer.valueOf((index + 1) + '1'), testRow.integer_col, 'Incorrect value assigned to integer column row cell');
				index++;
			}	
	}
	
	
	private static testMethod void shouldNotLoadWithInvalidDate(){
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(getInvalidDateCSV());
				System.assert(false, 'File should not have been processed due to invalid date being in CSV');
			} catch(CSVParser.ParseException ex){
				System.assertEquals('Unable to process value on row 5 for the column DATE_COL: Date value is invalid', 
											ex.getMessage(),
												'Wrong exception was thrown during parsing');
			}
	}
	
	
	private static testMethod void shouldNotLoadWithInvalidDateTime(){
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(getInvalidDateTimeCSV());
				System.assert(false, 'File should not have been processed due to invalid date time being in CSV');
			} catch(CSVParser.ParseException ex){
				System.assertEquals('Unable to process value on row 6 for the column DATETIME_COL: Date/time value is invalid', 
											ex.getMessage(),
												'Wrong exception was thrown during parsing');
			}
	}
	
	
	private static testMethod void shouldNotLoadWithInvalidDouble(){
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(getInvalidDoubleCSV());
				System.assert(false, 'File should not have been processed due to invalid double being in CSV');
			} catch(CSVParser.ParseException ex){
				System.assertEquals('Unable to process value on row 11 for the column DOUBLE_COL: Cannot convert value to a double', 
											ex.getMessage(),
												'Wrong exception was thrown during parsing');
			}
	}
	
	
	private static testMethod void shouldNotLoadWithInvalidInteger(){
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(getInvalidIntegerCSV());
				System.assert(false, 'File should not have been processed due to invalid integer being in CSV');
			} catch(CSVParser.ParseException ex){
				System.assertEquals('Unable to process value on row 7 for the column INTEGER_COL: Cannot convert value to an integer', 
											ex.getMessage(),
												'Wrong exception was thrown during parsing');
			}
	}
	
	
	private static testMethod void shouldNotLoadWithInvalidDecimal(){
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(getInvalidDecimalCSV());
				System.assert(false, 'File should not have been processed due to invalid decimal being in CSV');
			} catch(CSVParser.ParseException ex){
				System.assertEquals('Unable to process value on row 3 for the column DECIMAL_COL: Cannot convert value to a decimal', 
											ex.getMessage(),
												'Wrong exception was thrown during parsing');
			}
	}
	
	
	private static testMethod void shouldNotLoadWithIncorrectNumberOfValuesOnRow(){
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(getIncorrectNumberOfValuesOnRowsCSV());
				System.assert(false, 'File should not have been processed due to incorrect number of values being on a row');
			} catch(CSVParser.ParseException ex){
				System.debug('###'+ex.getMessage());
				
				System.assertEquals('Number of values do not match number of columns on row: 5 : Column size: 6 : Row size: 7', 
											ex.getMessage(),
												'Wrong exception was thrown during parsing');
			}
	}
	
	
	private static testMethod void shouldLoadWithExplicitColumnHeadingNamesSet(){
		TEST_METHOD_NAME = 'shouldLoadWithExplicitColumnHeadingNamesSet';
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(getValidCSV(testParser.getDelimiter()));
				System.assertEquals('TEST_GROUP', testParser.getExplicitColumnHeaderGroupNameFound(), 'Explicit column header group did not get retrieved correctly');
			} catch(CSVParser.ParseException ex){
				System.assert(false, 'CSV had correct headings, file should have been processed correctly');
			}
	}
	
	
	private static testMethod void shouldNotLoadWithExplicitColumnHeadingNamesSet(){
		TEST_METHOD_NAME = 'shouldNotLoadWithExplicitColumnHeadingNamesSet';
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(getValidCSV(testParser.getDelimiter()));
				System.assert(false, 'File did not contain the correct column heading names');
			} catch(CSVParser.ParseException ex){
				System.assertEquals('Column headers found in CSV do not match any of the explicit column groups expected', ex.getMessage(), 'Wrong exception was thrown');
			}
	}
	
	
	private static testMethod void shouldNotLoadIfSubClassDoesNotReturnInstanceOfCSVRow(){
		TEST_METHOD_NAME = 'shouldNotLoadIfSubClassDoesNotReturnInstanceOfCSVRow';
		CSVParser testParser = new TestParser();
			
			try {
				testParser.readFile(getValidCSV(testParser.getDelimiter()));
				System.assert(false, 'Sub-class returned a null instance of a CSV Row');
			} catch(CSVParser.InvalidConfigurationException ex){
				System.assertEquals('Sub-class must return an instance of CSVRow for the method createCSVRow', ex.getMessage(), 'Wrong exception was thrown');
			}
	}
	
	private static testMethod void shouldGenerateCSV(){
		CSVParser testParser = new TestParser();
		testParser.readFile(getValidCSV(testParser.getDelimiter()));
		testParser.generateCSV();
		System.assert(true,'Generating went ok');
	}
	
}