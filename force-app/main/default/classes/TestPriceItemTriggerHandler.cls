/****/
@isTest
private class TestPriceItemTriggerHandler {
	@isTest
	static void createAddOnInsertSuccess() {
		TestUtils.createOrderType();
		TestUtils.createOneOffProduct();
		TestUtils.createRecurringProduct();

		Product2[] ooprod = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = :TestUtils.theOneOffProduct.ProductCode AND IsActive = TRUE];
		Product2[] recprod = [
			SELECT Id, ProductCode
			FROM Product2
			WHERE ProductCode = :TestUtils.theRecurringProduct.ProductCode AND IsActive = TRUE
		];

		cspmb__Price_Item__c aopi = new cspmb__Price_Item__c();
		aopi.cspmb__One_Off_Charge_External_Id__c = ooprod[0].ProductCode;
		aopi.cspmb__Recurring_Charge_External_Id__c = recprod[0].ProductCode;
		aopi.cspmb__Is_Active__c = true;
		aopi.cspmb__Type__c = 'Commercial Product';
		aopi.cspmb__Role__c = 'Master';
		aopi.Access_Type_Multi__c = 'Coax';
		aopi.Deal_Type_Multi__c = 'Acquisition';
		aopi.Bundle_Services__c = 'Internet';
		aopi.Mobile_Scenario__c = 'Data Only';
		aopi.Mobile_Subscription__c = 'Basic subscription';

		test.startTest();
		insert aopi;
		system.assertEquals(ooprod[0].Id, [SELECT One_Off_Charge_Product__c FROM cspmb__Price_Item__c LIMIT 1].One_Off_Charge_Product__c);
		system.assertEquals(recprod[0].Id, [SELECT Recurring_Charge_Product__c FROM cspmb__Price_Item__c LIMIT 1].Recurring_Charge_Product__c);

		test.stopTest();
	}

	@isTest
	static void createAddOnUpdate() {
		TestUtils.createOrderType();
		TestUtils.createOneOffProduct();
		TestUtils.createRecurringProduct();

		Product2[] ooprod = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = :TestUtils.theOneOffProduct.ProductCode AND IsActive = TRUE];
		Product2[] recprod = [
			SELECT Id, ProductCode
			FROM Product2
			WHERE ProductCode = :TestUtils.theRecurringProduct.ProductCode AND IsActive = TRUE
		];

		cspmb__Price_Item__c aopi = new cspmb__Price_Item__c();
		aopi.cspmb__One_Off_Charge_External_Id__c = ooprod[0].ProductCode;
		aopi.cspmb__Recurring_Charge_External_Id__c = recprod[0].ProductCode;
		aopi.cspmb__Is_Active__c = true;
		aopi.cspmb__Type__c = 'Commercial Product';
		aopi.cspmb__Role__c = 'Master';

		test.startTest();
		insert aopi;
		update aopi;
		system.assertEquals(ooprod[0].Id, [SELECT One_Off_Charge_Product__c FROM cspmb__Price_Item__c LIMIT 1].One_Off_Charge_Product__c);
		system.assertEquals(recprod[0].Id, [SELECT Recurring_Charge_Product__c FROM cspmb__Price_Item__c LIMIT 1].Recurring_Charge_Product__c);

		test.stopTest();
	}

	@isTest
	static void createAddOnInsertError() {
		TestUtils.createOrderType();
		TestUtils.createOneOffProduct();
		TestUtils.createRecurringProduct();

		Boolean blnException = false;

		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'Vendor',
			External_Number__c = 1,
			Object_Prefix__c = 'VEN-09-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;

		Category__c objCategory = CS_DataTest.createCategory('Mobile');
		insert objCategory;

		Vendor__c objVendor = new Vendor__c(Name = 'Test');
		insert objVendor;

		Product2[] ooprod = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = :TestUtils.theOneOffProduct.ProductCode AND IsActive = TRUE];
		Product2[] recprod = [
			SELECT Id, ProductCode
			FROM Product2
			WHERE ProductCode = :TestUtils.theRecurringProduct.ProductCode AND IsActive = TRUE
		];

		cspmb__Price_Item__c aopi = new cspmb__Price_Item__c();
		aopi.cspmb__One_Off_Charge_External_Id__c = ooprod[0].ProductCode;
		aopi.cspmb__Recurring_Charge_External_Id__c = recprod[0].ProductCode;
		aopi.cspmb__Is_Active__c = true;
		aopi.cspmb__Type__c = 'Commercial Product';
		aopi.cspmb__Role__c = 'Master';
		aopi.Category__c = objCategory.Id;
		aopi.Vendor__c = objVendor.Id;

		Test.startTest();

		//Catch error when creating record
		try {
			insert aopi;
		} catch (Exception e) {
			blnException = true;
		}

		System.assertEquals(true, blnException, 'An exception creating the Commercial Product was not thrown.');

		Test.stopTest();
	}

	/*
	@isTest
	public static void testSetIsActive() {
		OrderType__c orderTypeMobile = CS_DataTest.createOrderType();
		insert orderTypeMobile;

		Product2 product1 = CS_DataTest.createProduct('One Business', orderTypeMobile);
		insert product1;

		Category__c mobileCategory = CS_DataTest.createCategory('Mobile');
		insert mobileCategory;

		List<cspmb__Price_Item__c> toInsert = new List<cspmb__Price_Item__c>();

		cspmb__Price_Item__c item1 = new cspmb__Price_Item__c(
			Name = 'item1',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Effective_Start_Date__c = Date.today()
		);
		toInsert.add(item1);
		cspmb__Price_Item__c item2 = new cspmb__Price_Item__c(
			Name = 'item2',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Effective_Start_Date__c = Date.today().addDays(-7)
		);
		toInsert.add(item2);
		cspmb__Price_Item__c item3 = new cspmb__Price_Item__c(
			Name = 'item3',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Effective_Start_Date__c = Date.today().addDays(+7)
		);
		toInsert.add(item3);
		cspmb__Price_Item__c item4 = new cspmb__Price_Item__c(
			Name = 'item4',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Effective_Start_Date__c = Date.today(),
			cspmb__Effective_End_Date__c = Date.today().addDays(7)
		);
		toInsert.add(item4);
		cspmb__Price_Item__c item5 = new cspmb__Price_Item__c(
			Name = 'item5',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master'
		);
		toInsert.add(item5);

		Test.startTest();
		insert toInsert;
		Test.stopTest();

		List<cspmb__Price_Item__c> toVerify = [
			SELECT Id, cspmb__Is_Active__c
			FROM cspmb__Price_Item__c
			WHERE cspmb__Is_Active__c = TRUE
		];
		//System.assertEquals(3, toVerify.size(), '3 cspmb__Price_Item__c should be set to Active');
		System.assertEquals(0, toVerify.size(), 'No commercial products have been inserted in Active status');
	}
	*/

	@isTest
	static void testSetExternalIds() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'Commercial Product',
			External_Number__c = 1,
			Object_Prefix__c = 'CP-01-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;

		cspmb__Price_Item__c objPriceItem = new cspmb__Price_Item__c(
			Name = 'Test Price Item',
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Effective_Start_Date__c = Date.today()
		);

		Test.startTest();
		insert objPriceItem;
		Test.stopTest();

		cspmb__Price_Item__c objPriceItemUpdated = [SELECT Id, External_ID__c FROM cspmb__Price_Item__c WHERE Id = :objPriceItem.Id LIMIT 1];

		System.assertEquals('CP-01-000002', objPriceItemUpdated.External_ID__c, 'The External ID on Commercial Product was not set correctly.');
	}
}
