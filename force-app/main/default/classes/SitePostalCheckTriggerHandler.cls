public without sharing class SitePostalCheckTriggerHandler extends TriggerHandler {
    /**
     * @description         This handles the before insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeInsert() {
        List<Site_Postal_Check__c> newSitePostalCheck = (List<Site_Postal_Check__c>) this.newList;

        setInfrastructure(newSitePostalCheck);
        setOrder(newSitePostalCheck);
    }

    /**
     * @description         This handles the after insert trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterInsert() {
        List<Site_Postal_Check__c> newSitePostalCheck = (List<Site_Postal_Check__c>) this.newList;
        syncToSiteAvailability(newSitePostalCheck, null);
        publishSiteAvailabilityPlatformEvent(newSitePostalCheck);
    }

    /**
     * @description         This handles the before update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void BeforeUpdate() {
        List<Site_Postal_Check__c> newSitePostalCheck = (List<Site_Postal_Check__c>) this.newList;
        List<Site_Postal_Check__c> oldSitePostalCheck = (List<Site_Postal_Check__c>) this.oldList;
        Map<Id, Site_Postal_Check__c> oldSitePostalCheckMap = (Map<Id, Site_Postal_Check__c>) this.oldMap;
    }

    /**
     * @description         This handles the after update trigger event.
     * @param   newObjects  List of new sObjects that are being created.
     */
    public override void AfterUpdate() {
        List<Site_Postal_Check__c> newSitePostalCheck = (List<Site_Postal_Check__c>) this.newList;
        List<Site_Postal_Check__c> oldSitePostalCheck = (List<Site_Postal_Check__c>) this.oldList;
        Map<Id, Site_Postal_Check__c> oldSitePostalCheckMap = (Map<Id, Site_Postal_Check__c>) this.oldMap;

        syncToSiteAvailability(newSitePostalCheck, oldSitePostalCheckMap);
    }

    private void setInfrastructure(
        List<Site_Postal_Check__c> newSitePostalCheck
    ) {
        // loop through the checks and copy the infra from the custom setting
        for (Site_Postal_Check__c spc : newSitePostalCheck) {
            if (
                spc.Technology_Type__c != null &&
                InfrastructureMapping__c.getInstance(spc.Technology_Type__c) !=
                null
            ) {
                spc.Access_Infrastructure__c = InfrastructureMapping__c.getInstance(
                        spc.Technology_Type__c
                    )
                    .AccessInfrastructure__c;
            } else {
                spc.Access_Infrastructure__c = null;
            }
        }
    }

    /*
     *  Description:    a static map containing the preferred supplier reference id's by vendor and by technologytype
     */
    public static Map<String, Map<String, Id>> vendorToTechTypeToPSId {
        get {
            if (vendorToTechTypeToPSId == null) {
                vendorToTechTypeToPSId = new Map<String, Map<String, Id>>();
                for (Preferred_Supplier__c pf : [
                    SELECT Id, Vendor__c, Technology_Type__c, Order__c
                    FROM Preferred_Supplier__c
                ]) {
                    // temporary filter to prevent having to create duplicate mappings for DSL2 types
                    String techTypeTempFilter = pf.Technology_Type__c.replace(
                        '2',
                        ''
                    );
                    if (!vendorToTechTypeToPSId.containsKey(pf.Vendor__c)) {
                        vendorToTechTypeToPSId.put(
                            pf.Vendor__c,
                            new Map<String, Id>()
                        );
                    }
                    if (
                        !vendorToTechTypeToPSId.get(pf.Vendor__c)
                            .containsKey(techTypeTempFilter)
                    ) {
                        vendorToTechTypeToPSId.get(pf.Vendor__c)
                            .put(techTypeTempFilter, pf.Id);
                    }
                }
            }
            return vendorToTechTypeToPSId;
        }
        private set;
    }

    private void setOrder(List<Site_Postal_Check__c> newSitePostalCheck) {
        // loop through the checks and copy the preferred supplier reference id
        for (Site_Postal_Check__c spc : newSitePostalCheck) {
            if (vendorToTechTypeToPSId.containsKey(spc.Access_Vendor__c)) {
                if (
                    vendorToTechTypeToPSId.get(spc.Access_Vendor__c)
                        .containsKey(spc.Access_Infrastructure__c)
                ) {
                    spc.Preferred_Supplier_Reference__c = vendorToTechTypeToPSId.get(
                            spc.Access_Vendor__c
                        )
                        .get(spc.Access_Infrastructure__c);
                }
            }
        }
    }

    public static Map<String, Infrastructure_Vendor__mdt> vendorToDetails {
        get {
            if (vendorToDetails == null) {
                // fetch entry/premium config
                vendorToDetails = new Map<String, Infrastructure_Vendor__mdt>();
                for (Infrastructure_Vendor__mdt iv : [
                    SELECT
                        vendor__c,
                        Is_Premium__c,
                        Generic_Vendor__c,
                        Existing_Infra__c,
                        Entry_Vendor__c
                    FROM Infrastructure_Vendor__mdt
                ]) {
                    vendorToDetails.put(iv.Vendor__c, iv);
                }
            }
            return vendorToDetails;
        }
        private set;
    }

    private void syncToSiteAvailability(
        List<Site_Postal_Check__c> sitePostalCheckList,
        Map<Id, Site_Postal_Check__c> oldSitePostalCheckMap
    ) {
        // create a map of entry and premium by site/vendor/accessInfra
        Map<Id, Map<String, Map<String, Map<String, List<Site_Postal_Check__c>>>>> vendorToInfraToEntryToPostalCheck = new Map<Id, Map<String, Map<String, Map<String, List<Site_Postal_Check__c>>>>>();
        Set<String> vendorInfraToRemove = new Set<String>();
        Map<Id, Id> spcIdToSaId = new Map<Id, Id>();
        for (Site_Postal_Check__c spc : sitePostalCheckList) {
            //if(pc.Access_Active__c && spc.Access_Infrastructure__c!=null && (oldSitePostalCheckMap == null || !oldSitePostalCheckMap.get(spc.Id).Access_Active__c)){
            if (
                spc.Access_Infrastructure__c != null &&
                spc.Access_Vendor__c != null
            ) {
                // prepare removal of existing site availability
                //vendorInfraToRemove.add(spc.Access_Site_ID__c+'|'+spc.Access_Vendor__c+'|'+spc.Access_Infrastructure__c);
                // prepare update of existing site availability
                spcIdToSaId.put(spc.Id, null);

                // is this an entry or a premium result?
                String entryOrPremium = 'entry';
                String entryVendor = spc.Access_Vendor__c;
                if (vendorToDetails.containsKey(spc.Access_Vendor__c)) {
                    if (
                        vendorToDetails.get(spc.Access_Vendor__c).Is_Premium__c
                    ) {
                        entryOrPremium = 'premium';
                        entryVendor = vendorToDetails.get(spc.Access_Vendor__c)
                            .Entry_Vendor__c;
                    }
                }

                // prepare any missing map entries
                if (
                    !vendorToInfraToEntryToPostalCheck.containsKey(
                        spc.Access_Site_ID__c
                    )
                )
                    vendorToInfraToEntryToPostalCheck.put(
                        spc.Access_Site_ID__c,
                        new Map<String, Map<String, Map<String, List<Site_Postal_Check__c>>>>()
                    );
                if (
                    !vendorToInfraToEntryToPostalCheck.get(
                            spc.Access_Site_ID__c
                        )
                        .containsKey(spc.Access_Infrastructure__c)
                )
                    vendorToInfraToEntryToPostalCheck.get(spc.Access_Site_ID__c)
                        .put(
                            spc.Access_Infrastructure__c,
                            new Map<String, Map<String, List<Site_Postal_Check__c>>>()
                        );
                if (
                    !vendorToInfraToEntryToPostalCheck.get(
                            spc.Access_Site_ID__c
                        )
                        .get(spc.Access_Infrastructure__c)
                        .containsKey(entryVendor)
                )
                    vendorToInfraToEntryToPostalCheck.get(spc.Access_Site_ID__c)
                        .get(spc.Access_Infrastructure__c)
                        .put(
                            entryVendor,
                            new Map<String, List<Site_Postal_Check__c>>()
                        );

                // add this record to the map
                // note that if there is premium, the list will always be of size 1. If there is no premium, the list could be longer (different up/down bandwidths)
                if (
                    vendorToInfraToEntryToPostalCheck.get(spc.Access_Site_ID__c)
                        .get(spc.Access_Infrastructure__c)
                        .get(entryVendor)
                        .containsKey(entryOrPremium)
                ) {
                    vendorToInfraToEntryToPostalCheck.get(spc.Access_Site_ID__c)
                        .get(spc.Access_Infrastructure__c)
                        .get(entryVendor)
                        .get(entryOrPremium)
                        .add(spc);
                } else {
                    vendorToInfraToEntryToPostalCheck.get(spc.Access_Site_ID__c)
                        .get(spc.Access_Infrastructure__c)
                        .get(entryVendor)
                        .put(
                            entryOrPremium,
                            new List<Site_Postal_Check__c>{ spc }
                        );
                }
            }
        }
        // fetch existing sa to enable update if applicable
        for (Site_Availability__c sa : [
            SELECT Id, Source_Check_Entry__c
            FROM Site_Availability__c
            WHERE Source_Check_Entry__c IN :spcIdToSaId.keySet()
        ]) {
            spcIdToSaId.put(sa.Source_Check_Entry__c, sa.Id);
        }

        /*if(!vendorInfraToRemove.isEmpty()){
            // remove current set of site availability for this vendor/accessInfra
            String deleteQuery = 'Select Id From Site_Availability__c Where ';
            for(String vi : vendorInfraToRemove){
                String[] splitString = vi.split('\\|');
                deleteQuery += '(Site__c = \''+splitString[0]+'\' AND Vendor__c = \''+splitString[1]+'\' AND Access_Infrastructure__c = \''+splitString[2]+'\') OR ';
            }
            deleteQuery = deleteQuery.subString(0,deleteQuery.length()-3);
            system.debug(deleteQuery);
            delete database.query(deletequery);
        }*/

        // upsert site availability records
        if (!vendorToInfraToEntryToPostalCheck.isEmpty()) {
            List<Site_Availability__c> saToUpsert = new List<Site_Availability__c>();

            for (Id siteId : vendorToInfraToEntryToPostalCheck.keySet()) {
                for (
                    String vendor : vendorToInfraToEntryToPostalCheck.get(
                            siteId
                        )
                        .keySet()
                ) {
                    for (
                        String infra : vendorToInfraToEntryToPostalCheck.get(
                                siteId
                            )
                            .get(vendor)
                            .keySet()
                    ) {
                        if (
                            vendorToInfraToEntryToPostalCheck.get(siteId)
                                .get(vendor)
                                .get(infra)
                                .containsKey('premium')
                        ) {
                            if (
                                vendorToInfraToEntryToPostalCheck.get(siteId)
                                    .get(vendor)
                                    .get(infra)
                                    .containsKey('entry')
                            ) {
                                // haspremium and entry
                                // create 1 site_availability with data of both entry and premium

                                // start with entry
                                Site_Availability__c sa = createSA(
                                    vendorToInfraToEntryToPostalCheck.get(
                                            siteId
                                        )
                                        .get(vendor)
                                        .get(infra)
                                        .get('entry')[0],
                                    spcIdToSaId
                                );
                                // override with values from premium
                                Site_Postal_Check__c premiumspc = vendorToInfraToEntryToPostalCheck.get(
                                        siteId
                                    )
                                    .get(vendor)
                                    .get(infra)
                                    .get('premium')[0];
                                sa.Bandwith_Down_Premium__c = premiumspc.Access_Max_Down_Speed__c;
                                sa.Bandwith_Up_Premium__c = premiumspc.Access_Max_Up_Speed__c;
                                if (
                                    vendorToDetails.containsKey(
                                        premiumspc.Access_Vendor__c
                                    )
                                ) {
                                    sa.Premium_Vendor__c = vendorToDetails.get(
                                            premiumspc.Access_Vendor__c
                                        )
                                        .Generic_Vendor__c;
                                } else {
                                    sa.Premium_Vendor__c = premiumspc.Access_Vendor__c;
                                }
                                sa.Source_Check_Premium__c = premiumspc.Id;
                                saToUpsert.add(sa);
                            } else {
                                // has only premium. error!
                                system.debug('only premium found. not good.');
                            }
                        } else {
                            // does not have premium
                            // create 1 site_availability for all the entry checks
                            for (
                                Site_Postal_Check__c spc : vendorToInfraToEntryToPostalCheck.get(
                                        siteId
                                    )
                                    .get(vendor)
                                    .get(infra)
                                    .get('entry')
                            ) {
                                saToUpsert.add(createSA(spc, spcIdToSaId));
                            }
                        }
                    }
                }
            }
            // insert or update if there is already a site availability for that entry-spc
            system.debug(saToUpsert);
            try {
                database.upsert(saToUpsert, false);
            } catch (dmlException e) {
                // if this fails, just create a debug log. Do not block the transaction
                system.debug(e);
            }
        }
    }

    private void publishSiteAvailabilityPlatformEvent(
        List<Site_Postal_Check__c> sitePostalCheckList
    ) {
        Set<Id> setSiteIds = new Set<Id>();
        for (Site_Postal_Check__c sitePostalCheck : sitePostalCheckList) {
            if (
                sitePostalCheck.Access_Site_ID__c != null &&
                sitePostalCheck.Access_Result_Check__c ==
                ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET &&
                sitePostalCheck.Access_Vendor__c ==
                ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO &&
                sitePostalCheck.Access_Infrastructure__c ==
                ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
            ) {
                setSiteIds.add(sitePostalCheck.Access_Site_ID__c);
            }
        }
        System.debug('@@@@setSiteIds: ' + setSiteIds);
        list<SitePostalCheckEvent__e> lstSitePostalCheckEvents = new list<SitePostalCheckEvent__e>();
        for (Id siteId : setSiteIds) {
            lstSitePostalCheckEvents.add(
                new SitePostalCheckEvent__e(
                    Site_Id__c = siteId
                )
            );
        }
        if (lstSitePostalCheckEvents.size() > 0) {
            EventBus.publish(lstSitePostalCheckEvents);
        }
    }

    //  Description:    generate basic site_availability from postalcodecheck
    private Site_Availability__c createSA(
        Site_Postal_Check__c spc,
        Map<Id, Id> spcIdToSaId
    ) {
        system.debug(spc);
        Site_Availability__c sa;
        // first check if there's an available sa
        if (
            spcIdToSaId.containsKey(spc.Id) && spcIdToSaId.get(spc.Id) != null
        ) {
            sa = new Site_Availability__c(Id = spcIdToSaId.get(spc.Id));
        } else {
            sa = new Site_Availability__c();
            sa.Site__c = spc.Access_Site_ID__c;
        }

        sa.Access_Infrastructure__c = spc.Access_Infrastructure__c;
        sa.Bandwith_Down_Entry__c = spc.Access_Max_Down_Speed__c;
        sa.Bandwith_Up_Entry__c = spc.Access_Max_Up_Speed__c;
        // initially default premium to entry
        sa.Bandwith_Down_Premium__c = spc.Access_Max_Down_Speed__c;
        sa.Bandwith_Up_Premium__c = spc.Access_Max_Up_Speed__c;
        sa.Existing_Infra__c = spc.Existing_Infra__c;
        sa.Preferred_Supplier__c = spc.Preferred_Supplier_Reference__c;
        sa.Access_Offer_Valide_Date__c = spc.Access_Offer_Valide_Date__c;
        sa.Access_Offer_Amount__c = spc.Access_Offer_Amount__c;
        if (vendorToDetails.containsKey(spc.Access_Vendor__c)) {
            sa.Vendor__c = vendorToDetails.get(spc.Access_Vendor__c)
                .Generic_Vendor__c;
            sa.Premium_Vendor__c = vendorToDetails.get(spc.Access_Vendor__c)
                .Generic_Vendor__c;
        } else {
            sa.Vendor__c = spc.Access_Vendor__c;
            sa.Premium_Vendor__c = spc.Access_Vendor__c;
        }
        sa.Source_Check_Entry__c = spc.Id;
        sa.Region__c = spc.Access_Region__c;
        sa.Result_Check__c = spc.Result_Check_Generic__c;

        return sa;
    }
}