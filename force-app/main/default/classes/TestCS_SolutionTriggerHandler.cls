@IsTest
private class TestCS_SolutionTriggerHandler {
    @IsTest
    static void testBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser){
            List<Suborder__c> solutionsToUpdate = new List<Suborder__c>();
            List<Suborder__c> newSolutions = new List<Suborder__c>();
            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            insert ord;

            Suborder__c solution1 = CS_DataTest.createSolution('Solution 1',ord.Id, false);
            solution1.Confirmed_Installation__c = false;
            solution1.Status__c = 'Solution Created';
            solution1.Installation_Required__c = true;
            solutionsToUpdate.add(solution1);
            newSolutions.add(solution1);

            insert newSolutions;

            solution1.Status__c = 'Cancellation Requested';
            update solutionsToUpdate;
        }
    }
}