global with sharing class CustomButtonNewCase extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		String newUrl = CustomButtonNewCase.redirectNewCase(basketId);
		return '{"status":"ok","redirectURL":"' + newUrl + '","target":"1"}';
	}

	@AuraEnabled
	public static cscfga__Product_Basket__c getBasketData(Id basketId) {
		List<cscfga__Product_Basket__c> baskets = new List<cscfga__Product_Basket__c>(
			[
				SELECT Id, Basket_Number__c, Name, cscfga__Opportunity__c, cscfga__Opportunity__r.Name, csbb__Account__c, csbb__Account__r.Name
				FROM cscfga__Product_Basket__c
				WHERE Id = :basketId
			]
		);

		if (baskets != null) {
			return baskets[0];
		} else {
			return null;
		}
	}

	@AuraEnabled
	public static String createNewCase(Map<String, String> newCaseFields, String recordTypeId) {
		Case newCase = new Case();
		for (String newCaseField : newCaseFields.keyset()) {
			if (newCaseField == 'Account') {
				newCase.AccountId = (Id) newCaseFields.get(newCaseField);
			}
			if (newCaseField == 'Opportunity__c') {
				newCase.Opportunity__c = (Id) newCaseFields.get(newCaseField);
			}
			if (newCaseField == 'Subject') {
				newCase.Subject = newCaseFields.get(newCaseField);
			}
			if (newCaseField == 'RecordTypeId') {
				newCase.RecordTypeId = (Id) newCaseFields.get(newCaseField);
			}
			if (newCaseField == 'Description') {
				newCase.Description = newCaseFields.get(newCaseField);
			}
			if (newCaseField == 'Product_Basket__c') {
				newCase.Product_Basket__c = (Id) newCaseFields.get(newCaseField);
			}
		}

		newCase.RecordTypeId = recordTypeId;

		try {
			insert newCase;
			System.debug('newCase=' + newCase);
			return ('SUCCESS-' + newCase.Id);
		} catch (Exception e) {
			// Process exception here
			System.debug(e.getMessage());
			return ('Error');
		}
	}

	// Set url for redirect after action
	public static String redirectNewCase(String basketId) {
		String idCase = GeneralUtils.recordTypeMap.get('Case').get('Salesforce_Case');
		cscfga__Product_Basket__c basket = [SELECT Basket_Number__c, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
		PageReference editPage = new PageReference('/apex/CS_CreateNewCase?basketId=' + basket.Id + '&recordId=' + idCase);
		return editPage.getUrl();
	}
}
