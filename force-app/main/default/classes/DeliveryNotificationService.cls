@RestResource(urlMapping='/deliverynotification/*')
/**
 * @description: Updates Order data in Salesforce
 * @author: Jurgen van Westreenen
 */
global without sharing class DeliveryNotificationService {
	@HttpPost
	global static void updateOrders() {
		updateOrdersService();
	}

	@TestVisible
	private static void updateOrdersService() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		List<Error> returnErrors = new List<Error>();
		try {
			// Deserialize JSON
			DeliveryNotification body = (DeliveryNotification) JSON.deserializeStrict(req.requestbody.tostring(), DeliveryNotification.class);
			system.debug(body);
			// Retrieve Order Id
			String orderId = body.orderId;
			if (String.isNotBlank(orderId)) {
				List<Order__c> ords = [
					SELECT Id, BOP_Order_Status__c, PM_Email__c, PM_First_Name__c, PM_Last_Name__c, PM_Phone__c
					FROM Order__c
					WHERE BOP_Export_Order_Id__c = :orderId
				];
				if (!ords.isEmpty()) {
					// Update order with supplied data
					Order__c ord = ords[0];
					ord.BOP_Order_Status__c = body.status;
					if (body.projectManager != null) {
						ord.PM_Email__c = body.projectManager.email;
						ord.PM_First_Name__c = body.projectManager.firstName;
						ord.PM_Last_Name__c = body.projectManager.lastName;
						ord.PM_Phone__c = body.projectManager.phone;
					}
					update ord;
				} else {
					// No Orders found
					Error err = new Error();
					err.errorCode = 'SFEC-0002';
					err.errorMessage = 'There were no records that could be updated or found in Salesforce.';
					returnErrors.add(err);
				}
			} else {
				// No Order Id supplied
				Error err = new Error();
				err.errorCode = 'SFEC-0001';
				err.errorMessage = 'Missing mandatory Input field: orderId';
				returnErrors.add(err);
			}
		} catch (Exception e) {
			// Any other occuring exceptions
			Error err = new Error();
			err.errorCode = 'SFEC-9999';
			err.errorMessage = 'An unhandled exception occured: ' + e.getMessage();
			returnErrors.add(err);
		} finally {
			// Populate response
			Response resp = new Response();
			if (returnErrors.size() > 0) {
				resp.status = 'FAILED';
				resp.errors = returnErrors;
			} else {
				resp.status = 'OK';
			}
			res.addHeader('Content-Type', 'text/json');
			res.statusCode = 200;
			res.responseBody = Blob.valueOf(JSON.serializePretty(resp));
		}
	}

	global class DeliveryNotification {
		public String orderId;
		public ProjectManager projectManager;
		public String status;
	}
	global class ProjectManager {
		public String firstName;
		public String lastName;
		public String phone;
		public String email;
	}
	global class Error {
		public String errorCode;
		public String errorMessage;
	}
	global class Response {
		public String status;
		public List<Error> errors;
	}
}
