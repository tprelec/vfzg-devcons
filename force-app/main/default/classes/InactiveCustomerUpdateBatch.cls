/**
 * @description       : Apex class to mark the Vodafone Customer as Inactive when all the Blling Arrangments related to a customer are closed.
 * @author            : Saurabh
 * @group             : Leads United
 * @last modified on  : 09-13-2022
 * @last modified by  : Saurabh
 **/
global class InactiveCustomerUpdateBatch implements Database.Batchable<sObject>, Database.Stateful, Schedulable {
	global Map<Id, String> errorMap;
	global Map<Id, SObject> IdToSObjectMap;
	global Map<Id, String> successMap;
	global String baseURL;

	global InactiveCustomerUpdateBatch() {
		errorMap = new Map<Id, String>();
		successMap = new Map<Id, String>();
		IdToSObjectMap = new Map<Id, SObject>();
		baseURL = URL.getSalesforceBaseUrl().toExternalForm();
	}

	global Database.QueryLocator start(Database.BatchableContext bc) {
		// collect the batches of records or objects to be passed to execute
		return Database.getQueryLocator(
			'Select id,Status__c,Financial_Account__r.Ban__r.Account__r.Name,' + 'Financial_Account__r.Ban__r.Account__c from Billing_Arrangement__c'
		);
	}
	global void execute(Database.BatchableContext bc, List<Billing_Arrangement__c> BaList) {
		// process each batch of records
		// Updated_Customer_Status__c field to be updated
		List<Account> accToUpdate = new List<Account>();
		Set<string> accWithOpenBarSet = new Set<string>();
		Set<string> accWithClosedBarSet = new Set<string>();
		for (Billing_Arrangement__c ba : BaList) {
			if (ba.Status__c == 'Open') {
				//this should give us all the unique accounts having open BAR
				accWithOpenBarSet.add(ba.Financial_Account__r.Ban__r.Account__c);
			}
			if (ba.Status__c == 'Closed') {
				Map<string, Billing_Arrangement__c> accVsClosedBarMap = new Map<string, Billing_Arrangement__c>();
				accWithClosedBarSet.add(ba.Financial_Account__r.Ban__r.Account__c);
			}
		}
		// remove the accounts which have at least one billing arrangments open (this set will now have absolute inactive accounts)
		accWithClosedBarSet.removeAll(accWithOpenBarSet);

		//next steps for inactive
		//query the accounts where id in closedSet;
		List<Account> possibleInactiveAcclist = [SELECT id, Updated_Customer_Status__c, Name FROM Account WHERE id IN :accWithClosedBarSet];
		for (Account acc : possibleInactiveAcclist) {
			if (acc.Updated_Customer_Status__c != 'Inactive') {
				acc.Updated_Customer_Status__c = 'Inactive';
				acc.Updated_Customer_Status_Last_Modified__c = String.valueOf(DateTime.now().format('yyyy/MM/dd hh:mm:ss'));
				accToUpdate.add(acc);
			}
		}
		//iterate it and check if they are not already inactive then add them to new list and mark them inactive
		//query the accounts where id in openSet;
		List<Account> possibleActiveAcclist = [SELECT id, Updated_Customer_Status__c, Name FROM Account WHERE id IN :accWithOpenBarSet];
		//iterate it and check if they are not already active then add them to new list and mark them active
		for (Account acc : possibleActiveAcclist) {
			if (acc.Updated_Customer_Status__c != 'Active') {
				acc.Updated_Customer_Status__c = 'Active';
				acc.Updated_Customer_Status_Last_Modified__c = String.valueOf(DateTime.now().format('yyyy/MM/dd hh:mm:ss'));
				accToUpdate.add(acc);
			}
		}
		if (accToUpdate.size() > 0) {
			List<Database.SaveResult> dsrs = Database.update(accToUpdate, false);
			Integer index = 0;
			for (Database.SaveResult dsr : dsrs) {
				if (!dsr.isSuccess()) {
					String errMsg = dsr.getErrors()[0].getMessage();
					errorMap.put(accToUpdate[index].Id, errMsg);
				} else {
					String sucMsg = baseURL + '/' + accToUpdate[index].Id;
					successMap.put(accToUpdate[index].Id, sucMsg);
				}
				IdToSObjectMap.put(accToUpdate[index].Id, accToUpdate[index]);
				index++;
			}
		}
	}
	global void finish(Database.BatchableContext bc) {
		if (!IdToSObjectMap.isEmpty()) {
			//Send an email to the User after your batch completes
			AsyncApexJob a = [
				SELECT id, ApexClassId, JobItemsProcessed, TotalJobItems, NumberOfErrors, CreatedBy.Email
				FROM AsyncApexJob
				WHERE id = :BC.getJobId()
			];
			String body =
				'Your batch job ' +
				'InactiveCustomerUpdateBatch ' +
				'has finished. \n' +
				'There were total : ' +
				IdToSObjectMap.size() +
				' records. Please find the attached success and error records CSV.';
			String subject = 'Account - Apex Batch Result for InactiveCustomerUpdateBatch';
			// Define the email
			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
			// Define email file attachment list
			Messaging.EmailFileAttachment[] emailAttList = new List<Messaging.EmailFileAttachment>();
			if (!errorMap.isEmpty()) {
				// Creating the CSV file for error
				String finalstr = 'Id, Name, Error \n';

				String attName = 'AccountErrors' + system.now().format('YYYYMMDDhhmm') + '.csv';
				for (Id id : errorMap.keySet()) {
					string err = errorMap.get(id);
					Account acct = (Account) IdToSObjectMap.get(id);
					string recordString = '"' + id + '","' + acct.Name + '","' + err + '"\n';
					finalstr = finalstr + recordString;
				}

				// Create the email attachment
				Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
				efa.setFileName(attName);
				efa.setBody(Blob.valueOf(finalstr));
				emailAttList.add(efa);
			}
			if (!successMap.isEmpty()) {
				// Creating the CSV file for successful updates
				String finalstr = 'Id, Name, Link \n';
				String attName = 'AccountSuccess' + system.now().format('YYYYMMDDhhmm') + '.csv';
				for (Id id : successMap.keySet()) {
					string suc = successMap.get(id);
					Account acct = (Account) IdToSObjectMap.get(id);
					string recordString = '"' + id + '","' + acct.Name + '","' + suc + '"\n';
					finalstr = finalstr + recordString;
				}

				// Create the email attachment
				Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
				efa.setFileName(attName);
				efa.setBody(Blob.valueOf(finalstr));
				emailAttList.add(efa);
			}
			// Sets the paramaters of the email
			email.setSubject(subject);
			email.setToAddresses(new List<String>{ System.Label.CustomerStatusUpdateBatchEmailAddress });
			email.setPlainTextBody(body);
			email.setFileAttachments(emailAttList);

			// Sends the email
			if (!Test.isRunningTest()) {
				Messaging.SendEmailResult[] r = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ email });
			}
		}
	}

	// This is the execute to handle a schedule request (just enables the job to be scheduled)
	global void execute(SchedulableContext sc) {
		InactiveCustomerUpdateBatch updateBatch = new InactiveCustomerUpdateBatch();
		Database.executebatch(updateBatch);
	}
}
