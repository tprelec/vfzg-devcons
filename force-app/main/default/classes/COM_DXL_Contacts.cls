public with sharing class COM_DXL_Contacts {
	JSONGenerator generator;
	List<Opportunity> opportunityData;
	Map<Id, List<External_Contact__c>> contactsWithExternalContacts;
	List<Contact_Role__c> contactRoles;

	public COM_DXL_Contacts(
		JSONGenerator generator,
		List<Opportunity> opportunityData,
		Map<Id, List<External_Contact__c>> contactsWithExternalContacts,
		List<Contact_Role__c> contactRoles
	) {
		this.generator = generator;
		this.opportunityData = opportunityData;
		this.contactsWithExternalContacts = contactsWithExternalContacts;
		this.contactRoles = contactRoles;
	}

	public void generateContacts() {
		generator.writeFieldName('contacts');
		generator.writeStartArray();

		if (opportunityData[0].Account.Authorized_to_sign_1st__c != null) {
			generateDirectorContact();
		}

		if (opportunityData[0].Financial_Account__c != null) {
			generateFinancialContact();
		}

		for (Contact_Role__c contactRoleRecord : this.contactRoles) {
			generateOtherContact(contactRoleRecord);
		}

		generator.writeEndArray();
	}

	private void generateDirectorContact() {
		generator.writeStartObject();
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_UNIFY_CONTACT_REFID,
			getExternalContactExternalId(
				contactsWithExternalContacts.get(opportunityData[0].Account.Authorized_to_sign_1st__c),
				COM_DXL_Constants.UNIFY_EXTERNAL_SYSTEM
			)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_EMAIL,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Authorized_to_sign_1st__r.Email)
		);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_CONTACT_ROLE_TYPE, COM_DXL_Constants.DIRECTOR_CONTACT_ROLE);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_PRIMARY_PHONE,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Authorized_to_sign_1st__r.MobilePhone)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_SFDC_ACCOUNT_ID,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Authorized_to_sign_1st__r.AccountId)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_SFDC_CONTACT_ID,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Authorized_to_sign_1st__c)
		);

		generator.writeFieldName(COM_DXL_Constants.COM_DXL_CONTACT_ADDRESS);
		Map<String, String> addressData = generateDirectorAddressData(opportunityData[0]);
		COM_DXL_Address dxlAddress = new COM_DXL_Address(generator, addressData);
		dxlAddress.generateAddress();

		generator.writeFieldName(COM_DXL_Constants.COM_DXL_CONTACT_PERSON_DATA);
		generator.writeStartObject();
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_GENDER,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Authorized_to_sign_1st__r.Gender__c)
		);

		if (String.isNotBlank(String.valueOf(opportunityData[0].Account.Authorized_to_sign_1st__r.BirthDate))) {
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_CONTACT_BIRTHDATE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(String.valueOf(opportunityData[0].Account.Authorized_to_sign_1st__r.BirthDate))
			);
		}

		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_FIRSTNAME,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Authorized_to_sign_1st__r.FirstName)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_LASTNAME,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Authorized_to_sign_1st__r.LastName)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_PHONE_NUMBER,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Authorized_to_sign_1st__r.Phone)
		);

		Map<String, String> identificationDetailsData = new Map<String, String>();
		identificationDetailsData.put(
			COM_DXL_Constants.COM_DXL_CONTACT_ID_TYPE,
			opportunityData[0].Account.Authorized_to_sign_1st__r.Document_Type__c
		);
		identificationDetailsData.put(
			COM_DXL_Constants.COM_DXL_CONTACT_ID_ID,
			opportunityData[0].Account.Authorized_to_sign_1st__r.Document_Number__c
		);
		identificationDetailsData.put(
			COM_DXL_Constants.COM_DXL_CONTACT_ID_COUNTRY,
			opportunityData[0].Account.Authorized_to_sign_1st__r.Jurisdiction_Type__c
		);
		generateIdentificationDetails(identificationDetailsData);

		generator.writeEndObject();
		generator.writeEndObject();
	}

	private void generateFinancialContact() {
		generator.writeStartObject();
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_UNIFY_CONTACT_REFID,
			getExternalContactExternalId(
				contactsWithExternalContacts.get(opportunityData[0].Financial_Account__r.Financial_Contact__c),
				COM_DXL_Constants.UNIFY_EXTERNAL_SYSTEM
			)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_EMAIL,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Financial_Contact__r.Email)
		);
		generator.writeStringField(COM_DXL_Constants.COM_DXL_CONTACT_ROLE_TYPE, COM_DXL_Constants.FINANCIAL_CONTACT_ROLE);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_PRIMARY_PHONE,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Financial_Contact__r.MobilePhone)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_SFDC_ACCOUNT_ID,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Financial_Contact__r.AccountId)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_SFDC_CONTACT_ID,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Financial_Contact__c)
		);

		generator.writeFieldName(COM_DXL_Constants.COM_DXL_CONTACT_ADDRESS);
		Map<String, String> addressData = generateFinancialAddressData(opportunityData[0]);
		COM_DXL_Address dxlAddress = new COM_DXL_Address(generator, addressData);
		dxlAddress.generateAddress();

		generator.writeFieldName(COM_DXL_Constants.COM_DXL_CONTACT_PERSON_DATA);
		generator.writeStartObject();
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_GENDER,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Financial_Contact__r.Gender__c)
		);

		if (String.isNotBlank(String.valueOf(opportunityData[0].Financial_Account__r.Financial_Contact__r.BirthDate))) {
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_CONTACT_BIRTHDATE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(
					String.valueOf(opportunityData[0].Financial_Account__r.Financial_Contact__r.BirthDate)
				)
			);
		}

		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_FIRSTNAME,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Financial_Contact__r.FirstName)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_LASTNAME,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Financial_Contact__r.LastName)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_PHONE_NUMBER,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Financial_Contact__r.Phone)
		);

		Map<String, String> identificationDetailsData = new Map<String, String>();
		identificationDetailsData.put(
			COM_DXL_Constants.COM_DXL_CONTACT_ID_TYPE,
			opportunityData[0].Financial_Account__r.Financial_Contact__r.Document_Type__c
		);
		identificationDetailsData.put(
			COM_DXL_Constants.COM_DXL_CONTACT_ID_ID,
			opportunityData[0].Financial_Account__r.Financial_Contact__r.Document_Number__c
		);
		identificationDetailsData.put(
			COM_DXL_Constants.COM_DXL_CONTACT_ID_COUNTRY,
			opportunityData[0].Financial_Account__r.Financial_Contact__r.Jurisdiction_Type__c
		);
		generateIdentificationDetails(identificationDetailsData);

		generator.writeEndObject();
		generator.writeEndObject();
	}

	private void generateOtherContact(Contact_Role__c contactRoleRecord) {
		generator.writeStartObject();
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_UNIFY_CONTACT_REFID,
			getExternalContactExternalId(contactsWithExternalContacts.get(contactRoleRecord.Contact__c), COM_DXL_Constants.UNIFY_EXTERNAL_SYSTEM)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_EMAIL,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(contactRoleRecord.Contact__r.Email)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_ROLE_TYPE,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(contactRoleRecord.Type__c)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_PRIMARY_PHONE,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(contactRoleRecord.Contact__r.MobilePhone)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_SFDC_ACCOUNT_ID,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(contactRoleRecord.Account__c)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_SFDC_CONTACT_ID,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(contactRoleRecord.Contact__c)
		);

		generator.writeFieldName(COM_DXL_Constants.COM_DXL_CONTACT_ADDRESS);
		Map<String, String> addressData = generateOtherAddressData(contactRoleRecord);
		COM_DXL_Address dxlAddress = new COM_DXL_Address(generator, addressData);
		dxlAddress.generateAddress();

		generator.writeFieldName(COM_DXL_Constants.COM_DXL_CONTACT_PERSON_DATA);
		generator.writeStartObject();
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_GENDER,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(contactRoleRecord.Contact__r.Gender__c)
		);

		if (String.isNotBlank(String.valueOf(contactRoleRecord.Contact__r.Birthdate))) {
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_CONTACT_BIRTHDATE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(String.valueOf(contactRoleRecord.Contact__r.Birthdate))
			);
		}

		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_FIRSTNAME,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(contactRoleRecord.Contact__r.FirstName)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_LASTNAME,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(contactRoleRecord.Contact__r.LastName)
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_CONTACT_PHONE_NUMBER,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(contactRoleRecord.Contact__r.Phone)
		);

		Map<String, String> identificationDetailsData = new Map<String, String>();
		identificationDetailsData.put(COM_DXL_Constants.COM_DXL_CONTACT_ID_TYPE, contactRoleRecord.Contact__r.Document_Type__c);
		identificationDetailsData.put(COM_DXL_Constants.COM_DXL_CONTACT_ID_ID, contactRoleRecord.Contact__r.Document_Number__c);
		identificationDetailsData.put(COM_DXL_Constants.COM_DXL_CONTACT_ID_COUNTRY, contactRoleRecord.Contact__r.Jurisdiction_Type__c);
		generateIdentificationDetails(identificationDetailsData);

		generator.writeEndObject();
		generator.writeEndObject();
	}

	private void generateIdentificationDetails(Map<String, String> identificationDetailsData) {
		if (
			String.isNotBlank(identificationDetailsData.get(COM_DXL_Constants.COM_DXL_CONTACT_ID_TYPE)) &&
			String.isNotBlank(identificationDetailsData.get(COM_DXL_Constants.COM_DXL_CONTACT_ID_ID))
		) {
			generator.writeFieldName('identificationDetails');
			generator.writeStartObject();
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_CONTACT_ID_TYPE,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(identificationDetailsData.get(COM_DXL_Constants.COM_DXL_CONTACT_ID_TYPE))
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_CONTACT_ID_ID,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(identificationDetailsData.get(COM_DXL_Constants.COM_DXL_CONTACT_ID_ID))
			);
			generator.writeStringField(
				COM_DXL_Constants.COM_DXL_CONTACT_ID_COUNTRY,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(
					String.isBlank(
							COM_DXL_CreateCustomerService.getDXLMapping(
								COM_DXL_Constants.COM_DXL_COUNTRY_MAPPING,
								identificationDetailsData.get(COM_DXL_Constants.COM_DXL_CONTACT_ID_COUNTRY)
							)
						)
						? identificationDetailsData.get(COM_DXL_Constants.COM_DXL_CONTACT_ID_COUNTRY)
						: COM_DXL_CreateCustomerService.getDXLMapping(
								COM_DXL_Constants.COM_DXL_COUNTRY_MAPPING,
								identificationDetailsData.get(COM_DXL_Constants.COM_DXL_CONTACT_ID_COUNTRY)
						  )
				)
			);
			generator.writeEndObject();
		}
	}

	private String getExternalContactExternalId(List<External_Contact__c> externalContacts, String externalSystemName) {
		String returnValue = '';

		if (externalContacts != null) {
			for (External_Contact__c externalContact : externalContacts) {
				if (!String.isBlank(externalContact.ExternalId__c) && externalSystemName.equalsIgnoreCase(externalContact.External_Source__c)) {
					returnValue = externalContact.ExternalId__c;
				}
			}
		}

		return returnValue;
	}

	private Map<String, String> generateDirectorAddressData(Opportunity opportunityData) {
		Map<String, String> returnData = new Map<String, String>();
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_STREET, opportunityData.Account.Authorized_to_sign_1st__r.Account.Visiting_Street__c);
		returnData.put(
			COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUMBER,
			String.valueOf(opportunityData.Account.Authorized_to_sign_1st__r.Account.Visiting_Housenumber1__c)
		);
		returnData.put(
			COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUM_EXT,
			opportunityData.Account.Authorized_to_sign_1st__r.Account.Visiting_Housenumber_Suffix__c
		);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_POSTCODE, opportunityData.Account.Authorized_to_sign_1st__r.Account.Visiting_Postal_Code__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_CITY, opportunityData.Account.Authorized_to_sign_1st__r.Account.Visiting_City__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_COUNTRY, opportunityData.Account.Authorized_to_sign_1st__r.Account.Visiting_Country__c);
		return returnData;
	}

	private Map<String, String> generateFinancialAddressData(Opportunity opportunityData) {
		Map<String, String> returnData = new Map<String, String>();
		returnData.put(
			COM_DXL_Constants.COM_DXL_ADDRESS_STREET,
			opportunityData.Financial_Account__r.Financial_Contact__r.Account.Visiting_Street__c
		);
		returnData.put(
			COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUMBER,
			String.valueOf(opportunityData.Financial_Account__r.Financial_Contact__r.Account.Visiting_Housenumber1__c)
		);
		returnData.put(
			COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUM_EXT,
			opportunityData.Financial_Account__r.Financial_Contact__r.Account.Visiting_Housenumber_Suffix__c
		);
		returnData.put(
			COM_DXL_Constants.COM_DXL_ADDRESS_POSTCODE,
			opportunityData.Financial_Account__r.Financial_Contact__r.Account.Visiting_Postal_Code__c
		);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_CITY, opportunityData.Financial_Account__r.Financial_Contact__r.Account.Visiting_City__c);
		returnData.put(
			COM_DXL_Constants.COM_DXL_ADDRESS_COUNTRY,
			opportunityData.Financial_Account__r.Financial_Contact__r.Account.Visiting_Country__c
		);
		return returnData;
	}

	private Map<String, String> generateOtherAddressData(Contact_Role__c contactRoleRecord) {
		Map<String, String> returnData = new Map<String, String>();
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_STREET, contactRoleRecord.Contact__r.Account.Visiting_Street__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUMBER, String.valueOf(contactRoleRecord.Contact__r.Account.Visiting_Housenumber1__c));
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUM_EXT, contactRoleRecord.Contact__r.Account.Visiting_Housenumber_Suffix__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_POSTCODE, contactRoleRecord.Contact__r.Account.Visiting_Postal_Code__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_CITY, contactRoleRecord.Contact__r.Account.Visiting_City__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_COUNTRY, contactRoleRecord.Contact__r.Account.Visiting_Country__c);
		return returnData;
	}
}
