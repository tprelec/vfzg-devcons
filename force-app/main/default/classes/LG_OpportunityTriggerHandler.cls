/**
 * @description       :
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 01-12-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-10-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/
public class LG_OpportunityTriggerHandler {
	public static Boolean isCancellationPerformed = false;
	public static Set<Id> oopWithTaskCreated = new Set<Id>();
	private static Set<Id> mobileOppsBeingProcessed = new Set<Id>();

	// Create log a call activity based on the outcome of D2D agent visit
	public static void createActivity(List<Opportunity> opps, Map<Id, Opportunity> oldMap) {
		Map<String, List<String>> oppMap = new Map<String, List<String>>();

		for (Opportunity o : opps) {
			Boolean quoteFlag = false;
			Boolean salesFlag = false;
			if (oldMap != null) {
				quoteFlag = o.StageName == 'Awareness of interest' && oldMap.get(o.Id).StageName != 'Awareness of interest';
				salesFlag = o.StageName == 'Customer Approved' && oldMap.get(o.Id).StageName != 'Customer Approved';
			} else {
				quoteFlag = o.StageName == 'Awareness of interest';
				salesFlag = o.StageName == 'Customer Approved';
			}

			if (o.LG_CreatedFrom__c == 'Tablet' && quoteFlag) {
				if (!oppMap.containsKey('Quote')) {
					oppMap.put('Quote', new List<String>());
				}
				oppMap.get('Quote').add(o.Id);
			} else if (o.LG_CreatedFrom__c == 'Tablet' && salesFlag) {
				if (!oppMap.containsKey('Sales')) {
					oppMap.put('Sales', new List<String>());
				}
				oppMap.get('Sales').add(o.Id);
			}
		}

		logACallAfterVisit(oppMap);
	}

	public static void logACallAfterVisit(Map<String, List<String>> oppMap) {
		List<Task> tasks = new List<Task>();
		for (String callResultValue : oppMap.keySet()) {
			for (String recordId : oppMap.get(callResultValue)) {
				if (!oopWithTaskCreated.contains(recordId)) {
					Task t = new Task();
					t.Subject = 'D2D Visit';
					t.ActivityDate = System.today();
					t.Type = 'Result of D2D Visit';
					t.LG_Result__c = callResultValue;
					t.Status = 'Completed';
					t.WhatId = recordId;
					t.OwnerId = userinfo.getUserId();
					tasks.add(t);
					oopWithTaskCreated.add(recordId);
				}
			}
		}

		insert tasks;
	}

	public static void calculateD2Ccompenstation(Map<Id, Opportunity> newOpportunityMap) {
		List<Opportunity> opps = new List<Opportunity>();
		Map<Id, List<D2D_Compensation_Item__c>> feesByParentId = new Map<Id, List<D2D_Compensation_Item__c>>();
		Map<Id, D2D_Compensation__c> compensationsByAccount = new Map<Id, D2D_Compensation__c>();
		Map<Id, List<D2D_Compensation_Tier__mdt>> mapD2DCompTiersByD2DCompPeriodId = new Map<Id, List<D2D_Compensation_Tier__mdt>>();
		List<Date> sortedOrderDates = new List<Date>();
		List<D2D_Compensation_Period__mdt> lstCompensationPeriods = new List<D2D_Compensation_Period__mdt>();
		List<D2D_Compensation_Tier__mdt> lstCompensationTiers = new List<D2D_Compensation_Tier__mdt>();
		Set<Id> compareSet = new Set<Id>();

		for (Opportunity opp : newOpportunityMap.values()) {
			if (opp.LG_NEWSalesChannel__c == 'D2D' && opp.IsClosed && !opp.IsWon && !opp.Late_Cancel__c) {
				opp.Shadow_Salesfee__c = 0;
				continue;
			}
			if (opp.LG_NEWSalesChannel__c == 'D2D') {
				opps.add(opp);
			}
		}

		if (opps.isEmpty()) {
			return;
		}

		//SFO-2245
		//Query the proper D2D Compensation period, and then, query all the D2D Compensation tiers related to it
		lstCompensationPeriods = [SELECT Id, Start_Date__c, End_Date__c FROM D2D_Compensation_Period__mdt];

		if (!lstCompensationPeriods.isEmpty()) {
			for (D2D_Compensation_Tier__mdt objD2DCompensationTier : [
				SELECT
					Id,
					Minimum_Delta__c,
					Maximum_Delta__c,
					One_Year_Amount__c,
					(SELECT Id, D2D_Compensation_Period__c FROM D2D_Compensation_Period_Tiers__r)
				FROM D2D_Compensation_Tier__mdt
			]) {
				for (D2D_Compensation_Period_Tier__mdt objD2DCompensationPeriodTier : objD2DCompensationTier.D2D_Compensation_Period_Tiers__r) {
					List<D2D_Compensation_Tier__mdt> lstD2DCompensationTiersTemp = new List<D2D_Compensation_Tier__mdt>();

					if (mapD2DCompTiersByD2DCompPeriodId.containsKey(objD2DCompensationPeriodTier.D2D_Compensation_Period__c)) {
						lstD2DCompensationTiersTemp = mapD2DCompTiersByD2DCompPeriodId.get(objD2DCompensationPeriodTier.D2D_Compensation_Period__c);
					}

					lstD2DCompensationTiersTemp.add(objD2DCompensationTier);
					mapD2DCompTiersByD2DCompPeriodId.put(objD2DCompensationPeriodTier.D2D_Compensation_Period__c, lstD2DCompensationTiersTemp);
				}
			}
		}

		Map<Id, Opportunity> oppByOppId = new Map<Id, Opportunity>();
		for (Opportunity opp : opps) {
			Opportunity oppToUpdate = newOpportunityMap.get(opp.Id);
			oppByOppId.put(oppToUpdate.Id, oppToUpdate);
		}

		Map<Id, List<OpportunityLineItem>> oppLineItemsByOppId = new Map<Id, List<OpportunityLineItem>>();
		for (OpportunityLineItem oppLineItem : [
			SELECT Id, Product2.Name, LG_ContractTerm__c, OpportunityId, Installation_Status__c
			FROM OpportunityLineItem
			WHERE OpportunityId IN :oppByOppId.keySet() AND Bundle_Product__c = TRUE
		]) {
			if (oppLineItemsByOppId.containsKey(oppLineItem.OpportunityId)) {
				oppLineItemsByOppId.get(oppLineItem.opportunityId).add(oppLineItem);
			} else {
				List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
				oppLineItems.add(oppLineItem);
				oppLineItemsByOppId.put(oppLineItem.OpportunityId, oppLineItems);
			}
		}

		Map<Id, D2D_Compensation__c> compensationsById = new Map<Id, D2D_Compensation__c>(
			[
				SELECT
					existing_internet_1_year_multiplier__c,
					existing_internet_2_year_multiplier__c,
					existing_internet_3_year_multiplier__c,
					new_internet_1_year_multiplier__c,
					new_internet_2_year_multiplier__c,
					new_internet_3_year_multiplier__c,
					start_date__c,
					end_date__c
				FROM D2D_Compensation__c
			]
		);

		for (D2D_Compensation_Item__c compItem : [
			SELECT Id, Compensation_Amount__c, D2D_Compensation__c, Order_Entry_Product_Bundle__r.Name, Name
			FROM D2D_Compensation_Item__c
			WHERE D2D_Compensation__c IN :compensationsById.keySet()
		]) {
			if (feesByParentId.containsKey(compItem.D2D_Compensation__c)) {
				feesByParentId.get(compItem.D2D_Compensation__c).add(compItem);
			} else {
				List<D2D_Compensation_Item__c> compList = new List<D2D_Compensation_Item__c>();
				compList.add(compItem);
				feesByParentId.put(compItem.D2D_Compensation__c, compList);
			}
		}

		for (D2D_Compensation_Account__c d2dCompAcc : [
			SELECT Id, D2D_Compensation__c, Account__c
			FROM D2D_Compensation_Account__c
			WHERE D2D_Compensation__c IN :compensationsById.keySet()
		]) {
			compensationsByAccount.put(d2dCompAcc.Account__c, compensationsById.get(d2dCompAcc.D2D_Compensation__c));
			compareSet.add(d2dCompAcc.D2D_Compensation__c);
		}

		compensationsById.keySet().removeAll(compareSet);
		Id noAccountCompId = null;
		for (D2D_Compensation__c compensation : compensationsById.values()) {
			compensationsByAccount.put(compensation.Id, compensation);
			noAccountCompId = compensation.Id;
		}

		for (Opportunity opp : opps) {
			Opportunity oppToUpdate = newOpportunityMap.get(opp.Id);
			Id IdD2DCompensation = null;
			Id IdD2DCompensationPeriod = null;

			//Get D2D Compensation Record that is within the range of the Order Date on Opportunity
			for (D2D_Compensation__c objD2DCompensation : compensationsById.values()) {
				if (objD2DCompensation.start_date__c <= oppToUpdate.Order_Date__c && oppToUpdate.Order_Date__c <= objD2DCompensation.end_date__c) {
					IdD2DCompensation = objD2DCompensation.Id;
					break;
				}
			}

			//Get D2D Compensation Period Record (Custom Metadata) that is within the range of the Order Date on Opportunity
			for (D2D_Compensation_Period__mdt objD2DCompensationPeriod : lstCompensationPeriods) {
				if (
					objD2DCompensationPeriod.Start_Date__c <= oppToUpdate.Order_Date__c &&
					oppToUpdate.Order_Date__c <= objD2DCompensationPeriod.End_Date__c
				) {
					IdD2DCompensationPeriod = objD2DCompensationPeriod.Id;
					break;
				}
			}

			if (feesByParentId.containsKey(idD2DCompensation)) {
				setD2DcompensationValues(
					oppToUpdate,
					compensationsById.get(IdD2DCompensation),
					feesByParentId.get(idD2DCompensation),
					oppLineItemsByOppId.get(oppToUpdate.Id),
					mapD2DCompTiersByD2DCompPeriodId.containsKey(IdD2DCompensationPeriod)
						? mapD2DCompTiersByD2DCompPeriodId.get(IdD2DCompensationPeriod)
						: new List<D2D_Compensation_Tier__mdt>()
				);
			}

			if (
				oppToUpdate.PartnerAccountId != null &&
				compensationsByAccount.containsKey(oppToUpdate.PartnerAccountId) &&
				feesByParentId.containsKey(compensationsByAccount.get(oppToUpdate.PartnerAccountId).Id)
			) {
				setD2DcompensationValues(
					oppToUpdate,
					compensationsByAccount.get(oppToUpdate.PartnerAccountId),
					feesByParentId.get(compensationsByAccount.get(oppToUpdate.PartnerAccountId).Id),
					oppLineItemsByOppId.get(oppToUpdate.Id),
					lstCompensationTiers
				);
			} else if (compensationsByAccount.containsKey(noAccountCompId) && feesByParentId.containsKey(compensationsById.get(noAccountCompId).Id)) {
				setD2DcompensationValues(
					oppToUpdate,
					compensationsById.get(noAccountCompId),
					feesByParentId.get(compensationsById.get(noAccountCompId).Id),
					oppLineItemsByOppId.get(oppToUpdate.Id),
					lstCompensationTiers
				);
			}
		}
	}

	private static Opportunity setD2DcompensationValues(
		Opportunity opp,
		D2D_Compensation__c compensation,
		List<D2D_Compensation_Item__c> fees,
		List<OpportunityLineItem> oppLineItems,
		List<D2D_Compensation_Tier__mdt> lstCompensationTiers
	) {
		if (opp.Journey_type__c == 'Mobile') {
			opp.Shadow_Salesfee__c = 0;
			if (oppLineItems != null) {
				List<OpportunityLineItem> olisToUpdate = new List<OpportunityLineItem>();
				for (OpportunityLineItem oppLineItem : oppLineItems) {
					oppLineItem.Sales_Fee__c = 0;
					String name = '';
					for (D2D_Compensation_Item__c fee : fees) {
						if (oppLineItem.Product2.Name == fee.Order_Entry_Product_Bundle__r.Name) {
							if (name != fee.Order_Entry_Product_Bundle__r.Name) {
								if (oppLineItem.Installation_Status__c != 'Rejected') {
									oppLineItem.Sales_Fee__c = fee.Compensation_Amount__c;
									opp.Shadow_Salesfee__c = opp.Shadow_Salesfee__c + fee.Compensation_Amount__c;
								}
								name = fee.Order_Entry_Product_Bundle__r.Name;
								olisToUpdate.add(oppLineItem);
							}
						}
					}
				}
				update olisToUpdate;
			}
			return opp;
		} else {
			Decimal amount = 0;
			if (opp.Internet_Customer__c && opp.Bundle_Segment__c == 'ZZP') {
				amount = opp.Amount - (opp.Current_Amount_incl_VAT__c / 1.21);
				switch on Integer.valueOf(opp.LG_ContractTermMonths__c) {
					when 12 {
						//SFO-2029
						amount = lstCompensationTiers.size() > 0
							? getBaseCompensationAmount(opp.Amount_Delta__c, lstCompensationTiers) *
							  compensation.existing_internet_1_year_multiplier__c
							: amount * compensation.existing_internet_1_year_multiplier__c;
					}
					when 24 {
						amount = lstCompensationTiers.size() > 0
							? getBaseCompensationAmount(opp.Amount_Delta__c, lstCompensationTiers) *
							  compensation.existing_internet_2_year_multiplier__c
							: amount * compensation.existing_internet_2_year_multiplier__c;
					}
					when 36 {
						amount = lstCompensationTiers.size() > 0
							? getBaseCompensationAmount(opp.Amount_Delta__c, lstCompensationTiers) *
							  compensation.existing_internet_3_year_multiplier__c
							: amount * compensation.existing_internet_3_year_multiplier__c;
					}
				}
			} else {
				for (D2D_Compensation_Item__c fee : fees) {
					if (opp.Bundle_Name__c == fee.Order_Entry_Product_Bundle__r.Name) {
						amount += fee.Compensation_Amount__c;
					}
				}
				switch on Integer.valueOf(opp.LG_ContractTermMonths__c) {
					when 12 {
						amount = amount * compensation.new_internet_1_year_multiplier__c;
					}
					when 24 {
						amount = amount * compensation.new_internet_2_year_multiplier__c;
					}
					when 36 {
						amount = amount * compensation.new_internet_3_year_multiplier__c;
					}
				}
			}
			if (amount < 0) {
				opp.Shadow_Salesfee__c = 0;
			} else {
				opp.Shadow_Salesfee__c = amount;
			}
			return opp;
		}
	}

	/**
	 * @description Process mobile orders using WebShop REST API in future function call.
	 * On success opportunity stage is Installed/Ready for Billing,
	 * otherwise opportunity stage is Need Additional Information.
	 * @param  opps List of opportunities inserted or updated
	 * @param  oldMap Map of old values
	 */
	public static void processMobileOrder(List<Opportunity> opps, Map<Id, Opportunity> oldMap) {
		if (!FeatureFlagging.isActive('OET_ProcessMobileOrders') && !Test.isRunningTest()) {
			return;
		}
		for (Opportunity opp : opps) {
			if (
				opp.StageName == 'Ready for Order' &&
				(oldMap == null ||
				oldMap.get(opp.Id).StageName != 'Ready for Order') &&
				opp.Journey_Type__c == 'Mobile' &&
				!mobileOppsBeingProcessed.contains(opp.Id)
			) {
				mobileOppsBeingProcessed.add(opp.Id);
				processMobileOrder(opp.Id);
			}
		}
	}

	private static Decimal getBaseCompensationAmount(Decimal dcmDeltaAmount, List<D2D_Compensation_Tier__mdt> lstCompensationTiers) {
		for (D2D_Compensation_Tier__mdt objCompensationTier : lstCompensationTiers) {
			if ((Decimal) objCompensationTier.Minimum_Delta__c <= dcmDeltaAmount && dcmDeltaAmount < (Decimal) objCompensationTier.Maximum_Delta__c) {
				return objCompensationTier.One_Year_Amount__c;
			}
		}

		return 0;
	}

	@future(callout=true)
	@testVisible
	private static void processMobileOrder(Id oppId) {
		Opportunity opp = new Opportunity(Id = oppId);
		try {
			String orderNumber = OrderEntryWebCart.processOrder(oppId);
			if (orderNumber != null) {
				// Save opportunity
				opp.StageName = 'Installed/Ready for Billing';
				opp.LG_OrderNumber__c = orderNumber;
			} else {
				opp.StageName = 'Need Additional Information';
			}
		} catch (Exception e) {
			opp.StageName = 'Need Additional Information';
			Log__c log = new Log__c(
				Apex_Class__c = 'OrderEntryWebCart',
				Method__c = 'processOrder',
				Date_Time__c = DateTime.now(),
				Level__c = 'ERROR',
				Message__c = 'Order Entry Tool provisioning failed',
				Stack_Trace__c = e.getStackTraceString(),
				User__c = System.UserInfo.getUserId(),
				Record_ID__c = oppId
			);
			insert log;
		}
		update opp;
	}
}
