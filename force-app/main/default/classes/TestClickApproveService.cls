@isTest
public with sharing class TestClickApproveService {
	@TestSetup
	static void makeData() {
		User u = GeneralUtils.currentUser;
		No_Triggers__c noTriggers = new No_Triggers__c(SetupOwnerId = u.Id, Flag__c = true);
		insert noTriggers;
		MavenDocumentsTestFactory.createMavenDocumentsRecords();
		ClickApproveTestDataFactory.createClickApproveSettings();
	}

	@isTest
	public static void testSendRequests() {
		User u = GeneralUtils.currentUser;
		Test.startTest();

		Account acc = TestUtils.createAccount(u);
		Contact con = TestUtils.createContact(acc);

		TestUtils.autoCommit = false;

		Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		opp.RecordTypeId = GeneralUtils.getRecordTypeIdByName('Opportunity', 'Ziggo');
		opp.LG_AutomatedQuoteDelivery__c = 'Quote Requested';
		opp.OwnerId = u.Id;
		insert opp;

		OpportunityContactRole ocr = new OpportunityContactRole(OpportunityId = opp.Id, ContactId = con.Id, Role = 'Administrative Contact');
		insert ocr;

		Attachment att = new Attachment(
			Name = 'Test Attachment',
			body = Blob.valueOf('this is a test'),
			ContentType = 'pdf',
			IsPrivate = false,
			ParentId = opp.Id
		);
		insert att;

		List<Id> listIds = new List<Id>();
		listIds.add(att.Id);

		List<ClickApproveRequest> crList = new List<ClickApproveRequest>();
		ClickApproveRequest cr = new ClickApproveRequest();
		cr.clickApproveSettingsName = 'Sites Approval setting - Opportunities';
		cr.attachmentIds = listIds;
		cr.recepientId = con.Id;
		cr.relatedToId = opp.Id;
		crList.add(cr);

		CSCAP.API_1.MultipleSendApprovalRequestRecord req = cr.getClickApproveAPIRequest();

		ClickApproveService.sendRequests(crList);
		Test.stopTest();

		System.assertEquals(opp.Id, req.approvalObjectId, 'Approval object is opportunity');
	}
}
