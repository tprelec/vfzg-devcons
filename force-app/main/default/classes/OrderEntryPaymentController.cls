public with sharing class OrderEntryPaymentController {
	private static final String JSON_NAME = 'OrderEntryData.json';

	/**
	 * @dectiption account holder name
	 * @param ContactId
	 * @return name of Contact
	 */
	@AuraEnabled
	public static String getBankAccountHolder(Id contactId) {
		return [SELECT Name FROM Contact WHERE Id = :contactId].Name;
	}

	/**
	 * @description get Iban length
	 * @param countryCode
	 * @return iban length Integer
	 */
	@AuraEnabled
	public static Integer getCountryIbanLength(String countryCode) {
		LG_IBAN__c ibanCountrySpecificConfiguration = LG_IBAN__c.getValues(countryCode);
		if (ibanCountrySpecificConfiguration != null) {
			return Integer.valueOf(ibanCountrySpecificConfiguration.LG_Length__c);
		}
		return null;
	}

	/**
	 * @description Create new Billing Account if it already does not exist
	 * @param opportunityId
	 * @return Updated Payment
	 */
	@AuraEnabled
	public static OrderEntryData.Payment createBillingAccount(Id opportunityId) {
		OrderEntryData data = OrderEntryController.getOrderEntryData(opportunityId);
		Opportunity oppTest = [SELECT Id, LG_InstallationCity__c, LG_InstallationHouseNumber__c FROM Opportunity WHERE Id = :data.opportunityId];
		return createBillingAccountUpdateOpp(data);
	}

	private static OrderEntryData.Payment createBillingAccountUpdateOpp(OrderEntryData data) {
		Contact mainContact = [SELECT Name, Email FROM Contact WHERE Id = :data.primaryContactId];
		String bankAccHolder = String.isNotBlank(data.payment.bankAccountHolder) ? data.payment.bankAccountHolder : mainContact.Name;

		csconta__Billing_Account__c billingAccount;
		if (String.isEmpty(data.payment.billingAccountId)) {
			billingAccount = new csconta__Billing_Account__c();
		} else {
			billingAccount = [SELECT Id FROM csconta__Billing_Account__c WHERE Id = :data.payment.billingAccountId];
		}
		billingAccount.csconta__Account__c = data.accountId;
		billingAccount.LG_PaymentType__c = data.payment.paymentType;
		billingAccount.LG_BankAccountNumberIBAN__c = data.payment.iban;
		billingAccount.csconta__Billing_Channel__c = data.payment.billingChannel;
		billingAccount.csconta__City__c = data.payment.city;
		billingAccount.csconta__Postcode__c = data.payment.postalCode;
		billingAccount.csconta__Street__c = data.payment.street;
		billingAccount.LG_HouseNumber__c = String.valueOf(data.payment.houseNumber);
		billingAccount.LG_HouseNumberExtension__c = data.payment.houseNumberSuffix;
		billingAccount.LG_BillingEmailAddress__c = mainContact.Email;
		billingAccount.LG_BankAccountHolder__c = bankAccHolder;

		upsert billingAccount;
		data.payment.billingAccountId = billingAccount.Id;

		updateOpportunityFields(bankAccHolder, data);
		return data.payment;
	}

	private static void updateOpportunityFields(String bankAccHolder, OrderEntryData data) {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		Opportunity opp = [SELECT Id FROM Opportunity WHERE Id = :data.opportunityId];
		opp.LG_BillingCity__c = data.payment.city;
		opp.LG_BillingHouseNumber__c = String.valueOf(data.payment.houseNumber);
		opp.LG_BillingHouseNumberExtension__c = data.payment.houseNumberSuffix;
		opp.LG_PaymentType__c = data.payment.paymentType;
		opp.LG_BillingPostalCode__c = data.payment.postalCode;
		opp.LG_BillingStreet__c = data.payment.street;
		opp.LG_BankNumber__c = data.payment.iban;
		opp.LG_BankAccountName__c = bankAccHolder;
		opp.Internet_Customer__c = data.b2cInternetCustomer;
		opp.Billing_Channel__c = data.payment.billingChannel;

		if (opp != null) {
			update opp;
		}
	}
}
