@IsTest
private class TestCOM_DeliveryOrdsDataTableController {

	@IsTest
	static void testGetDeliveryOrderList() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            ord.csord__Account__c = testAccount.Id;
			insert ord;

			COM_Delivery_Order__c parentDeliveryOrder = CS_DataTest.createDeliveryOrder('ParentDelOrd', ord.Id, true);

			List<COM_Delivery_Order__c> deliveryOrders = CS_DataTest.createMultipleDeliveryOrders('TestDelOrd', ord.Id, parentDeliveryOrder.Id, 5, true);

			Task taskRec = CS_DataTest.createTask('ProjectStart', 'COM Delivery', simpleUser, false);
			taskRec.WhatId = parentDeliveryOrder.Id;
			insert taskRec;

			System.assertEquals(5, COM_DeliveryOrdersDataTableController.getDeliveryOrderList(taskRec.Id).size(), '5 DeliveryOrder records should be returned' );
		}
	}

	@IsTest
	static void testUpdateDeliveryOrders() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
			insert ord;

			List<COM_Delivery_Order__c> deliveryOrders = CS_DataTest.createMultipleDeliveryOrders('TestDelOrd', ord.Id, null, 1, false);
            deliveryOrders[0].Status__c = 'Created';
            insert deliveryOrders;

			Task taskRec = CS_DataTest.createTask('ProjectStart', 'COM Delivery', simpleUser, false);
			taskRec.WhatId = ord.Id;
			insert taskRec;

			COM_Delivery_Order__c deliveryOrder = [SELECT Id, Name FROM COM_Delivery_Order__c WHERE Id = :deliveryOrders[0].Id];
			System.assertEquals('TestDelOrd', deliveryOrder.Name, 'Initial DeliveryOrder Name should be TestDelOrd' );


			Test.startTest();
			deliveryOrders[0].Name = 'UpdatedName';
			COM_DeliveryOrdersDataTableController.updateDeliveryOrders(deliveryOrders);
			Test.stopTest();

			deliveryOrder = [SELECT Id, Name FROM COM_Delivery_Order__c WHERE Id = :deliveryOrders[0].Id];
			System.assertEquals('UpdatedName', deliveryOrder.Name, 'Updated DeliveryOrder Name should be UpdatedName' );
		}
	}
}