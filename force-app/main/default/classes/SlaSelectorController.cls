public with sharing class SlaSelectorController {

	private final Site__c site;

	public final Boolean useObjectFilter {get;set;}
	public final Boolean useProactiveFilter {get;set;}
	public final Boolean useReactionTimeFilter {get;set;}
	public final Boolean useRepairTimeFilter {get;set;}
	public final Boolean useReplaceByFilter {get;set;}
	public final Boolean useSlaTypeFilter {get;set;}

	public final String enforcedObjectFilter {get;set;}
	public final String enforcedProactiveFilter {get;set;}
	public final String enforcedReactionTimeFilter {get;set;}
	public final String enforcedRepairTimeFilter {get;set;}
	public final String enforcedReplaceByFilter {get;set;}
	public final String enforcedSlaTypeFilter {get;set;}

	public final String defaultObjectFilter;
	public final String defaultProactiveFilter;
	public final String defaultReactionTimeFilter;
	public final String defaultRepairTimeFilter;
	public final String defaultReplaceByFilter;
	public final String defaultSlaTypeFilter;

	public final Boolean slaEditable {get;set;}


	public SlaSelectorController(ApexPages.StandardController stdController) {

		Site__c mySite = (Site__c)stdController.getRecord();
		Id siteId = mySite.Id;
		this.site = [select Id,
							Sla__c							
						from Site__c
						where Id = : siteId];

		Sla_Picker_Settings__c slaSettings = Sla_Picker_Settings__c.getValues('Site Screen');

		Sla_Picker_Visibility__c slaSettingVisibility = Sla_Picker_Visibility__c.getInstance();
		slaEditable = slaSettingVisibility.SLA_Picker_Visible__c;

		useObjectFilter = slaSettings.Use_Object_Filter__c;
		useProactiveFilter = slaSettings.Use_Proactive_Filter__c;
		useReactionTimeFilter = slaSettings.Use_Reaction_Time_Filter__c;
		useRepairTimeFilter = slaSettings.Use_Repair_Time_Filter__c;
		useReplaceByFilter = slaSettings.Use_Replace_By_Filter__c;
		useSlaTypeFilter = slaSettings.Use_Sla_Type_Filter__c;

		enforcedObjectFilter=slaSettings.Enforced_Object_Filter__c;
		enforcedProactiveFilter=slaSettings.Enforced_Proactive_Filter__c;
		enforcedReactionTimeFilter=slaSettings.Enforced_Reaction_Time_Filter__c;
		enforcedRepairTimeFilter=slaSettings.Enforced_Repair_Time_Filter__c;
		enforcedReplaceByFilter=slaSettings.Enforced_Replace_By_Filter__c;
		enforcedSlaTypeFilter=slaSettings.Enforced_Sla_Type_Filter__c;

		defaultObjectFilter=slaSettings.Default_Object_Filter__c;
		defaultProactiveFilter=slaSettings.Default_Proactive_Filter__c;
		defaultReactionTimeFilter=slaSettings.Default_Reaction_Time_Filter__c;
		defaultRepairTimeFilter=slaSettings.Default_Repair_Time_Filter__c;
		defaultReplaceByFilter=slaSettings.Default_Replace_By_Filter__c;
		defaultSlaTypeFilter=slaSettings.Default_Sla_Type_Filter__c;

		resetFilters();
	}

	public String selectedObjectFilter {
		get{
			if(enforcedObjectFilter != null && enforcedObjectFilter != '')
				return enforcedObjectFilter;
			else
				return selectedObjectFilter;
		}
		set;
	}
	public String selectedProactiveFilter {
		get{
			if(enforcedProactiveFilter != null && enforcedProactiveFilter != '')
				return enforcedProactiveFilter;
			else
				return selectedProactiveFilter;
		}
		set;
	}
	public String selectedReactionTimeFilter {
		get{
			if(enforcedReactionTimeFilter != null && enforcedReactionTimeFilter != '')
				return enforcedReactionTimeFilter;
			else
				return selectedReactionTimeFilter;
		}
		set;
	}
	public String selectedRepairTimeFilter {
		get{
			if(enforcedRepairTimeFilter != null && enforcedRepairTimeFilter != '')
				return enforcedRepairTimeFilter;
			else
				return selectedRepairTimeFilter;
		}
		set;
	}
	public String selectedReplaceByFilter {
		get{
			if(enforcedReplaceByFilter != null && enforcedReplaceByFilter != '')
				return enforcedReplaceByFilter;
			else
				return selectedReplaceByFilter;
		}
		set;
	}
	public String selectedSlaTypeFilter {
		get{
			if(enforcedSlaTypeFilter != null && enforcedSlaTypeFilter != '')
				return enforcedSlaTypeFilter;
			else
				return selectedSlaTypeFilter;
		}
		set;
	}

	public String selectedSla {get;set;}
	public List<sObject> filteredSlas;

	public List<SelectOption> filteredSlaOptions{
		get{

			refreshFilters();

			filteredSlaOptions = new List<SelectOption>();
			filteredSlaOptions.add(new SelectOption('', ' - None - '));

		    for(Sobject slaSobject : filteredSlas){
		    	String myId = String.valueOf(slaSobject.get('Id'));
		    	String name = String.valueOf(slaSobject.get('Name'));
		    	String bopCode = String.valueOf(slaSobject.get('Bop_Code__c'));
				filteredSlaOptions.add(new SelectOption(myId, bopCode));
		    }
			return filteredSlaOptions;
		}
		private set;
	}


	public List<SelectOption> objectFilter {
		get{
			if(!useObjectFilter) return null;
			if(objectFilter == null){

				objectFilter = new List<SelectOption>();
				Schema.DescribeFieldResult F = Sla__c.Object__c.getDescribe();
				objectFilter.add(new SelectOption('', ' - None - '));

			    for(Schema.PicklistEntry pentry : F.getPicklistValues()){
					objectFilter.add(new SelectOption(pentry.getValue(), pentry.getLabel()));
			    }
			    
			}
			return objectFilter;
		}
		private set;
	}

	public List<SelectOption> reactiveProactiveFilter {
		get{
			if(!useProactiveFilter) return null;
			if(reactiveProactiveFilter == null){

				reactiveProactiveFilter = new List<SelectOption>();
				Schema.DescribeFieldResult F = Sla__c.Reactive_Proactive__c.getDescribe();
				reactiveProactiveFilter.add(new SelectOption('', ' - None - '));

			    for(Schema.PicklistEntry pentry : F.getPicklistValues()){
					reactiveProactiveFilter.add(new SelectOption(pentry.getValue(), pentry.getLabel()));
			    }
			    
			}
			return reactiveProactiveFilter;
		}
		private set;
	}

	public List<SelectOption> reactionTimeFilter {
		get{
			if(!useReactionTimeFilter) return null;
			if(reactionTimeFilter == null){

				reactionTimeFilter = new List<SelectOption>();
				Schema.DescribeFieldResult F = Sla__c.Reaction_Time__c.getDescribe();
				reactionTimeFilter.add(new SelectOption('', ' - None - '));

			    for(Schema.PicklistEntry pentry : F.getPicklistValues()){
					reactionTimeFilter.add(new SelectOption(pentry.getValue(), pentry.getLabel()));
			    }
			    
			}
			return reactionTimeFilter;
		}
		private set;
	}

	public List<SelectOption> repairTimeFilter {
		get{
			if(!useRepairTimeFilter) return null;
			if(repairTimeFilter == null){

				repairTimeFilter = new List<SelectOption>();
				Schema.DescribeFieldResult F = Sla__c.Repair_Time__c.getDescribe();
				repairTimeFilter.add(new SelectOption('', ' - None - '));

			    for(Schema.PicklistEntry pentry : F.getPicklistValues()){
					repairTimeFilter.add(new SelectOption(pentry.getValue(), pentry.getLabel()));
			    }
			    
			}
			return repairTimeFilter;
		}
		private set;
	}

	public List<SelectOption> replaceByFilter {
		get{
			if(!useReplaceByFilter) return null;
			if(replaceByFilter == null){

				replaceByFilter = new List<SelectOption>();
				Schema.DescribeFieldResult F = Sla__c.Replace_By__c.getDescribe();
				replaceByFilter.add(new SelectOption('', ' - None - '));

			    for(Schema.PicklistEntry pentry : F.getPicklistValues()){
					replaceByFilter.add(new SelectOption(pentry.getValue(), pentry.getLabel()));
			    }
			    
			}
			return replaceByFilter;
		}
		private set;
	}

	public List<SelectOption> slaTypeFilter {
		get{
			if(!useSlaTypeFilter) return null;
			if(slaTypeFilter == null){

				slaTypeFilter = new List<SelectOption>();
				Schema.DescribeFieldResult F = Sla__c.Sla_Type__c.getDescribe();
				slaTypeFilter.add(new SelectOption('', ' - None - '));

			    for(Schema.PicklistEntry pentry : F.getPicklistValues()){
					slaTypeFilter.add(new SelectOption(pentry.getValue(), pentry.getLabel()));
			    }
			    
			}
			return slaTypeFilter;
		}
		private set;
	}

	public void refreshFilters(){


	    if(site != null){

			string soqlQuery = 'select Id, Name, Bop_Code__c from Sla__c';

			List<String> conditions = new List<String>();

			if(useObjectFilter && selectedobjectFilter != null && selectedobjectFilter !=' - None - '){
				conditions.add('Object__c = \'' + selectedobjectFilter + '\'');
			}
			if (useProactiveFilter && selectedProactiveFilter != null && selectedProactiveFilter !=' - None - '){
				conditions.add('Reactive_Proactive__c = \'' + selectedProactiveFilter + '\'');
			}
			if (useReactionTimeFilter && selectedreactionTimeFilter != null && selectedreactionTimeFilter !=' - None - '){
				conditions.add('Reaction_time__c = \'' + selectedreactionTimeFilter + '\'');
			}
			if (useRepairTimeFilter && selectedrepairTimeFilter != null && selectedrepairTimeFilter !=' - None - '){
				conditions.add('Repair_time__c = \'' + selectedrepairTimeFilter + '\'');
			}
			if (useReplaceByFilter && selectedreplaceByFilter != null && selectedreplaceByFilter !=' - None - '){
				conditions.add('Replace_by__c = \'' + selectedreplaceByFilter + '\'');
			}
			if (useSlaTypeFilter && selectedslaTypeFilter != null && selectedslaTypeFilter !=' - None - '){
				conditions.add('SLA_type__c = \'' + selectedslaTypeFilter + '\'');
			}

			if(conditions.size() > 0){
				soqlQuery += ' where ' + String.join(conditions, ' AND ');
			}

			system.debug('***SP*** soqlQuery: ' + soqlQuery); 

			filteredSlas = Database.query(soqlQuery);

		} 
    }


    public void resetFilters(){

    	if(defaultObjectFilter != null && defaultObjectFilter != ''){
			selectedObjectFilter = defaultObjectFilter;
    	}
		else{
			selectedObjectFilter = ' - None - ';
		}

		if(defaultProactiveFilter != null && defaultProactiveFilter != ''){
			selectedProactiveFilter = defaultProactiveFilter;
		}
		else{
			selectedProactiveFilter = ' - None - ';
		}

		if(defaultReactionTimeFilter != null && defaultReactionTimeFilter != ''){
			selectedReactionTimeFilter = defaultReactionTimeFilter;
		}
		else{
			selectedReactionTimeFilter = ' - None - ';
		}

		if(defaultRepairTimeFilter != null && defaultRepairTimeFilter != ''){
			selectedRepairTimeFilter = defaultRepairTimeFilter;
		}
		else{
			selectedRepairTimeFilter = ' - None - ';
		}

		if(defaultReplaceByFilter != null && defaultReplaceByFilter != ''){
			selectedReplaceByFilter = defaultReplaceByFilter;
		}
		else{
			selectedReplaceByFilter = ' - None - ';
		}

		if(defaultSlaTypeFilter != null && defaultSlaTypeFilter != ''){
			selectedSlaTypeFilter = defaultSlaTypeFilter;
		}
		else{
			selectedSlaTypeFilter = ' - None - ';
		}

    }

	public PageReference updateSla(){
		
		Savepoint sp = Database.setSavepoint();

		site.Sla__c = selectedSla;

		try{
			update site;
		} catch (Exception e){
			Database.rollback(sp);
		}
		

		PageReference sitePage = new ApexPages.StandardController(site).view();

        sitePage.setRedirect(true);

        return sitePage;

	}
}