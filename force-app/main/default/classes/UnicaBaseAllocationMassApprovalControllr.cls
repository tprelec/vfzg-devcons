public with sharing class UnicaBaseAllocationMassApprovalControllr {
	

	ApexPages.StandardSetController setContrlr;


	public UnicaBaseAllocationMassApprovalControllr(ApexPages.StandardSetController cntrlr) {
		setContrlr = cntrlr;
	}

	public PageReference ApproveSelected(){
    
	    approveSelected(setContrlr.getSelected());

		Schema.DescribeSObjectResult prefix = Unica_Base_Allocation__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
  	}

  	public static void approveSelected(List<Unica_Base_Allocation__c> selectedRecords){

  		List<Id> selectedRecordsIds = new List<Id>();
  		Map<Id, Unica_Base_Allocation__c> idToUbaMap = new Map<Id, Unica_Base_Allocation__c>();

  		//fetch the error field in case it's not there
  		selectedRecords = [select id, error__c from Unica_Base_Allocation__c where id in :selectedRecords ];

  		for(Unica_Base_Allocation__c uba : selectedRecords){
  			selectedRecordsIds.add(uba.id);
  			idToUbaMap.put(uba.id, uba);
  		}



  		List<ProcessInstanceWorkitem> approvals = [Select Id, ProcessInstance.TargetObjectId from ProcessInstanceWorkitem where ProcessInstance.TargetObjectId in :selectedRecordsIds];

  		for(ProcessInstanceWorkitem myApproval : approvals){

  			Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
			req.setComments('Approved');

			req.setAction('Approve');
			
			Id workItemId = myApproval.id;
			
			if(workItemId == null)
			{
			    idToUbaMap.get(myApproval.ProcessInstance.TargetObjectId).error__c = 'Error Occured in Mass Approval';

			}
			else
			{
			    req.setWorkitemId(workItemId);
			    Approval.ProcessResult result =  Approval.Process(req);
			}
  		}
  		

  	}

}