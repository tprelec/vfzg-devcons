public class SiteCheckFactory {
    
    public ISiteCheck getSiteCheck(String componentType){

        System.debug('Component type: ' + componentType);
        
        switch on componentType {
            when 'Access' {
                return new AccessSiteCheck();
            }	
            when 'Internet' {
                return new InternetSiteCheck();
            }
            // when 'Network ports' {
            //     return new NetworkPortsSiteCheck();
            // }
            when else {
                throw new CS_Exception('Invalid component type!');
            }
        }
    }
}