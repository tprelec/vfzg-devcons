@isTest
public with sharing class TestSitesDatatableController {
	@TestSetup
	static void makeData() {
		Account a = TestUtils.createAccount(null);
		Site__c s = TestUtils.createSite(a);
		TestUtils.createPBXTypes(1);
		TestUtils.createSalesSettings();

		Id recordTypeId = Schema.SObjectType.Competitor_Asset__c.getRecordTypeInfosByDeveloperName().get('PABX').getRecordTypeId();

		PBX_Type__c pbxType = [SELECT Id FROM PBX_Type__c];

		Competitor_Asset__c compAsset = new Competitor_Asset__c();
		compAsset.Site__c = s.Id;
		compAsset.PBX_Type__c = pbxType.Id;
		compAsset.Account__c = a.Id;
		compAsset.recordTypeId = recordTypeId;

		insert compAsset;

		Site_Postal_Check__c spc = new Site_Postal_Check__c();
		spc.Access_Site_ID__c = s.Id;
		spc.Access_Active__c = true;
		spc.Access_Vendor__c = 'IPVPN-FLEX';
		spc.Technology_Type__c = 'Fiber';
		spc.Access_Max_Down_Speed__c = 100;
		spc.Access_Max_Up_Speed__c = 100;
		spc.Existing_Infra__c = true;

		insert spc;
	}

	@IsTest
	public static void testGetData() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Test.startTest();

		List<Site__c> sites = new List<Site__c>();
		sites = SitesDatatableController.getData(acc.Id);
		System.assertEquals(1, sites.size());
		Test.stopTest();
	}

	@IsTest
	public static void testGetAccount() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Test.startTest();

		Account a = SitesDatatableController.getAccount(acc.Id);
		System.assertEquals('Test Account', a.Name);

		Test.stopTest();
	}

	@IsTest
	public static void testActivateSites() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		List<Site__c> sites = [SELECT Id, Active__c FROM Site__c WHERE Site_Account__c = :acc.Id];

		for (Site__c site : sites) {
			site.Active__c = false;
		}
		update sites;

		Test.startTest();

		SitesDatatableController.activateSites(sites);

		List<Site__c> sitesUpdated = [SELECT Id, Active__c FROM Site__c WHERE Site_Account__c = :acc.Id];

		System.assertEquals(true, sitesUpdated.get(0).Active__c);

		Test.stopTest();
	}

	@IsTest
	public static void testDeactivateSites() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		List<Site__c> sites = [SELECT Id, Active__c FROM Site__c WHERE Site_Account__c = :acc.Id];

		for (Site__c site : sites) {
			site.Active__c = true;
		}
		update sites;

		Test.startTest();

		SitesDatatableController.deactivateSites(sites);

		List<Site__c> sitesUpdated = [SELECT Id, Active__c FROM Site__c WHERE Site_Account__c = :acc.Id];

		System.assertEquals(false, sitesUpdated.get(0).Active__c);

		Test.stopTest();
	}

	@IsTest
	public static void testGetExistingInfras() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		List<Site__c> sites = [SELECT Id, Active__c FROM Site__c WHERE Site_Account__c = :acc.Id];

		Test.startTest();

		List<Site_Postal_Check__c> spcs = SitesDatatableController.getExistingInfras(sites);
		System.assertEquals(1, spcs.size());
		Test.stopTest();
	}

	@IsTest
	public static void testIsCommunity() {
		Test.startTest();
		Boolean result = SitesDatatableController.isCommunity();
		Test.stopTest();

		System.assertEquals(false, result);
	}

	@IsTest
	public static void testScheduleDsl() {
		List<Site__c> sites = [SELECT Id, Name, Blocked_Checks__c, Postal_Code_Check_Status__c FROM Site__c];
		sites.get(0).Postal_Code_Check_Status__c = 'Completed';

		Test.startTest();

		SitesDatatableController.scheduleDsl(sites);

		Test.stopTest();

		Site__c site = [SELECT Id, Postal_Code_Check_Status__c FROM Site__c];

		System.assertEquals('DSL check requested', site.Postal_Code_Check_Status__c);
	}

	@IsTest
	public static void testScheduleFiber() {
		List<Site__c> sites = [SELECT Id, Name, Blocked_Checks__c, Postal_Code_Check_Status__c FROM Site__c];
		sites.get(0).Postal_Code_Check_Status__c = 'Completed';
		Test.startTest();

		SitesDatatableController.scheduleFiber(sites);

		Test.stopTest();

		Site__c site = [SELECT Id, Postal_Code_Check_Status__c FROM Site__c];

		System.assertEquals('Fiber check requested', site.Postal_Code_Check_Status__c);
	}

	@IsTest
	public static void testScheduleBoth() {
		List<Site__c> sites = [SELECT Id, Name, Blocked_Checks__c, Postal_Code_Check_Status__c FROM Site__c];
		sites.get(0).Postal_Code_Check_Status__c = 'Completed';

		Test.startTest();

		SitesDatatableController.scheduleBoth(sites);

		Test.stopTest();

		Site__c site = [SELECT Id, Postal_Code_Check_Status__c FROM Site__c];

		System.assertEquals('Combined check requested', site.Postal_Code_Check_Status__c);
	}

	@IsTest
	public static void testBulkImportSite() {
		Account acc = [SELECT Id FROM Account];
		String base64Data = 'POSTALCODE,CITY,STREET,HOUSENUMBER,HOUSENUMBERSUFFIX,PHONE,';
		base64Data = base64Data + '\n';
		base64Data = base64Data + '3711TP,Kelemen,Ludbreska,1,a,0132252520\n';
		base64Data = base64Data + '1822IM,Imocki,Vinska,20,,0202020200';

		Blob tempBlob = Blob.valueOf(base64Data);

		ContentVersion cv = new ContentVersion();
		cv.VersionData = EncodingUtil.base64Decode(EncodingUtil.base64Encode(tempBlob));
		cv.Title = 'fileName';
		cv.PathOnClient = 'filename';
		insert cv;

		ContentVersion cvNew = [SELECT Id, ContentDocumentId FROM ContentVersion];
		Test.startTest();

		String result = SitesDatatableController.bulkImportSite(cvNew.ContentDocumentId, acc.Id, ',');

		Test.stopTest();

		System.assertEquals('SUCCESS', result);
	}

	@IsTest
	public static void testGetSitePbx() {
		Account acc = [SELECT Id FROM Account];

		Test.startTest();

		List<Competitor_Asset__c> compAsset = SitesDatatableController.getSitePbx(acc.Id);

		Test.stopTest();

		System.assertEquals(1, compAsset.size());
	}
}
