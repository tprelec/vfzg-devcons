@isTest
public with sharing class TestPredicateTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Predicate';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'P-18-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		csclm__Predicate__c testPredicate = new csclm__Predicate__c();
		testPredicate.csclm__Subject_Type__c = 'cscfga__Product_Configuration__c';
		testPredicate.csclm__Subject__c = 'Name';
		insert testPredicate;

		csclm__Predicate__c queryRecord = [SELECT Id, ExternalID__c FROM csclm__Predicate__c LIMIT 1];
		System.assert(queryRecord.ExternalID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
