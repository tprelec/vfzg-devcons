public with sharing class OrderEntryAccountController {
	/**
	 * @description get Account data
	 * @param accId Id
	 * @return Account record
	 */
	@AuraEnabled(cacheable=false)
	public static Account getAccountData(Id accId) {
		return [SELECT Id, Name, KVK_number__c, Date_of_Establishment__c, LG_LegalForm__c, VAT_Number__c FROM Account WHERE Id = :accId];
	}

	/**
	 * @description Update account
	 * @param  newAccount Account record
	 */
	@AuraEnabled
	public static Account saveAccountData(Account newAccount) {
		Account acc = newAccount;
		try {
			SharingUtils.updateRecordsWithoutSharing(newAccount);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
		return acc;
	}
}
