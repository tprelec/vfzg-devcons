global with sharing class CustomButtonReviseContract extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>(
			[
				SELECT
					Id,
					Total_Number_of_Expected_Locations__c,
					Error_message__c,
					cscfga__Basket_Status__c,
					Primary__c,
					Used_Snapshot_Objects__c,
					Data_Capping__c,
					cscfga__Products_In_Basket__c,
					Solution_Sales__c,
					Has_Fixed__c,
					DirectIndirect__c,
					Clauses_in_promotion__c,
					Approved_Date__c
				FROM cscfga__Product_Basket__c
				WHERE Id = :basketId
			]
		);

		Id profileId = UserInfo.getProfileId();
		String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;

		String redirectUrl = '/' + basketId;
		if (profileName == 'VF Partner Portal User') {
			redirectUrl = '/partnerportal' + redirectUrl;
		}

		if (basketList != null && basketList[0] != null) {
			//delete agreement
			//chnage basket status

			//CS_ContractsTableHelper.setPromotionClauses(basketList[0]);

			if (basketList[0].cscfga__Basket_Status__c == 'Contract Created') {
				cscfga__Product_Basket__c updateBasket = changeBasketStatus(basketList[0], 'Approved');
				Boolean agreementDeleted = DeleteBasketAgreementWithoutSharing.deleteBasketAgreement(basketId);

				if (agreementDeleted && updateBasket != null) {
					update updateBasket;

					return '{"status":"ok","text":"Contract Revised!","redirectURL":"' + redirectUrl + '"}';
				} else {
					return '{"status":"error","text":"ERROR: Contract not revised, please contact your administrator!"}';
				}
			} else {
				return '{"status":"error","text":"Remove Contract is possible only in Contract Created status"}';
			}
		} else {
			return '{"status":"error","text":"ERROR: Contract not revised, please contact your administrator!"}';
		}
	}

	private cscfga__Product_Basket__c changeBasketStatus(cscfga__Product_Basket__c basket, String basketStatus) {
		basket.cscfga__Basket_Status__c = basketStatus;

		return basket;
	}
}
