/*
 * @author Rahul Sharma
 *
 * @description Test class for CustomButtonBillingInformation
 */
@IsTest
private class TestCustomButtonBillingInformation {
	@IsTest
	static void testPerformAction() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);

		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity').get('Default');
		update opportunity;
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opportunity.id, csbb__Account__c = account.Id);
		insert basket;

		Test.startTest();
		CustomButtonBillingInformation button = new CustomButtonBillingInformation();
		String url = button.performAction(basket.Id);
		Test.stopTest();

		System.assertNotEquals(null, url, 'url must not be null');
		System.assertNotEquals('', url, 'url must not be empty');
	}
}
