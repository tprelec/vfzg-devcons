public without sharing class SiteExistingInfraController {
    public SiteExistingInfraController(ApexPages.StandardController controller) {
        retrieveSitePostalChecks();

        retURL = ApexPages.currentPage().getParameters().get('retURL');
        showIFrame = true;
        if (String.isBlank(retURL) && siteId != null) {
            retURL = '/' + siteId;
            showIFrame = false;
        }

    }

    public void retrieveSitePostalChecks() {
        siteId = ApexPages.currentPage().getParameters().get('id');
        siteIds = ApexPages.currentPage().getParameters().get('ids');

        List<String> siteList = new List<String>();
        if (String.isNotBlank(siteId)) siteList.add(siteId);
        if (String.isNotBlank(siteIds)) siteList.addAll(siteIds.split(','));
        
        existingInfra = new List<spcWrapper>();
        List<Site_Postal_Check__c> spcList = new List<Site_Postal_Check__c>();
        spcList = [
            SELECT Id, Access_Site_ID__c, Access_Vendor__c, Technology_Type__c, Accessclass__c, 
                Access_Max_Down_Speed__c, Access_Max_Up_Speed__c,Total_MB_Existing_EVC__c
            FROM Site_Postal_Check__c
            WHERE Access_Site_ID__c IN :siteList AND Existing_Infra__c = true
        ];

        Set<Id> existingSites = new Set<Id>();
        for (Site_Postal_Check__c spc : spcList) {
            spcWrapper spcw = new spcWrapper();
            spcw.theSpc = spc;
            spcw.totalMB = spc.Total_MB_Existing_EVC__c;
            spcw.vendor = spc.Access_Vendor__c;
            existingInfra.add(spcw);

            existingSites.add(spc.Access_Site_ID__c);
        }

        for (String siteId : siteList) {
            if (!existingSites.contains(siteId)) {
                existingInfra.add(newInfra(siteId));
            }
        }

    }

    public String siteId {get;private set;}
    public String siteIds {get;private set;}
    public List<spcWrapper> existingInfra {get;set;}
    public String retURL {get;set;} 
    public boolean showIFrame {get;set;}    

    public class spcWrapper implements Comparable{
        public Site_Postal_Check__c theSpc {get;set;}
        public Decimal totalMB {get;set;}
        public String vendor {get;set;}
        public Boolean selected {get;set;}

        public Integer compareTo(Object compareTo) {
            spcWrapper o = (spcWrapper) compareTo;
            if (theSpc.Access_Site_ID__c == o.theSpc.Access_Site_ID__c) return 0;
            if (theSpc.Access_Site_ID__c > o.theSpc.Access_Site_ID__c) return 1;
            return -1;  
        }
            
    }

    public Integer numberOfRowToRemove { get; set; }

    public spcWrapper newInfra(String siteId) {
        spcWrapper spcw = new spcWrapper();
        Site_Postal_Check__c newInfra = new Site_Postal_Check__c();
        newInfra.Access_Site_ID__c = siteId;
        newInfra.Technology_Type__c = 'Fiber';
        newInfra.Accessclass__c = 'Fiber';
        newInfra.Access_Max_Down_Speed__c = 1048576;
        newInfra.Access_Max_Up_Speed__c = 1048576;
        newInfra.Access_Result_Check__c = 'Green';
        newInfra.Access_Priority__c = 1;
        newInfra.Access_Active__c = true;
        newInfra.Manual__c = true;
        newInfra.Existing_Infra__c = true;
        spcw.theSpc = newInfra;
        return spcw;
    }

    // The method to add a new item to the list
    public PageReference addInfra() {

        existingInfra.add(newInfra(siteId));
        return null;
    }

    
    public PageReference addInfras() {
        Set<String> selectedSites = new Set<String>();
        for (spcWrapper s : existingInfra) {
            if (s.selected) {
                selectedSites.add(s.theSpc.Access_Site_ID__c);
                s.selected = false;
            }
        }

        for (Id selectedId : selectedSites) {
            existingInfra.add(newInfra(selectedId));
        }
        existingInfra.sort();
        return null;
    }

    // The method to remove an item from the list
    public PageReference removeInfra(){

        spcWrapper s = existingInfra[numberOfRowToRemove];
        existingInfra.remove(numberOfRowToRemove);  
        if(s.theSpc.Id != null) delete s.theSpc;

        return null;
    }
    
    public PageReference removeInfras() {
        List<Site_Postal_Check__c> toDelete = new List<Site_Postal_Check__c>();
        List<spcWrapper> temp = new List<spcWrapper>();
        for (spcWrapper s : existingInfra) {
            if (s.selected) {
                if (s.theSpc.Id != null) toDelete.add(s.theSpc);
            } else {
                temp.add(s);
            }
        }
        delete toDelete;
        existingInfra = temp;
        
        return null;
    }

    public PageReference saveUpdates() {
        system.debug(existingInfra);
        List<Site_Postal_Check__c> spcToUpsert = new List<Site_Postal_Check__c>();
        for (spcWrapper spcw : existingInfra) {
            spcw.theSpc.Access_Vendor__c = spcw.vendor;
            spcw.theSpc.Total_MB_Existing_EVC__c = spcw.totalMB;
            spcToUpsert.add(spcw.theSpc);
        }

        try {
            upsert spcToUpsert;
            return new PageReference(retURL);
        } catch (DmlException e) {
            if (e.getMessage().contains('duplicate value found')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'There can be only one infra per vendor.'));
            } else {
                ApexPages.addMessages(e);
            }
            return null;
        }
    } 

    public PageReference backToSite(){
        return new PageReference(retURL);        
    }

    public List<SelectOption> getVendorPickList(){
        List<SelectOption> vendorPickList = new List<SelectOption>();
        for (Infrastructure_Vendor__mdt iv:[select vendor__c from Infrastructure_Vendor__mdt Where Existing_Infra__c = true]) {
            vendorPickList.add(new SelectOption(iv.vendor__c,iv.vendor__c));
        }
        return vendorPickList;
    }   
}