/**
 * Result class for JSON parsing of Site ID's and PBX for Site decomposition.
 */
public class CS_Site_Pbx_Result {
	
    private Id siteIdInternal;
    private Id pbxIdInternal;
    
    public CS_Site_Pbx_Result(Id siteId) {
        this.siteIdInternal = siteId;
    }
    
    public void setPbxId(Id pbxId) {
        this.pbxIdInternal = pbxId;
    }
    
    public Id getSiteId() {
        return siteIdInternal;
    }
    
    public Id getPbxId() {
        return pbxIdInternal;
    }
}