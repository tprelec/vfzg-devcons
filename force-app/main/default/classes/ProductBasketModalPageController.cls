/**
 * @description       : Controller for ProductBasketModalPage.page
 * @author            : marin.mamic@vodafoneziggo.com
 * @last modified on  : 09-21-2022
 * @last modified by  : marin.mamic@vodafoneziggo.com
 **/
public with sharing class ProductBasketModalPageController {
	public Id basketId;
	public Boolean backgroundProcessCompleted { public get; private set; }
	public Boolean backgroundProcessSuccessfull { public get; private set; }
	public CustomButtonBase.ActionPayload actionButtonPayload { public get; private set; }
	public Boolean isPollerEnabled { public get; private set; }

	/**
	 * @description Class contructor that sets initial values of class instance variables
	 **/
	public ProductBasketModalPageController() {
		isPollerEnabled = true;
		basketId = Id.valueOf(String.escapeSingleQuotes(ApexPages.currentPage().getParameters().get('basketId')));
		backgroundProcessCompleted = false;
	}

	/**
	 * @description ProductBasketModalPage.page polls information from Salesforce every 5 seconds.
	 * This is a method that gets called to execute. It monitors the Custom_Button_Action_Payload__c
	 * field for changes.
	 **/
	public void checkBackgroundProcessStatus() {
		cscfga__Product_Basket__c basket = [SELECT Custom_Button_Action_Payload__c FROM cscfga__Product_Basket__c WHERE id = :basketId];
		actionButtonPayload = CustomButtonBase.getActionPayloadFrom(basket);

		if (actionButtonPayload.isActionCompleted()) {
			isPollerEnabled = false;
			backgroundProcessSuccessfull = actionButtonPayload.status == CustomButtonBase.ActionStatus.SUCCESS ? true : false;
			backgroundProcessCompleted = true;
			basket.Custom_Button_Action_Payload__c = null;
			update basket;
		}
	}
}
