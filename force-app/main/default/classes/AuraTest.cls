@IsTest
private class AuraTest
{
    @IsTest
    static void test_aura_data_classes()
    {
        String errorMsg = 'Oops, somth went wrong';
        AuraActionResult result = new AuraActionResult(false,
                new AuraMessage(errorMsg, AuraMessageSeverity.ERROR));

        System.assertEquals(false, result.success);
        System.assertEquals(errorMsg, result.message.message);
        System.assertEquals('ERROR', result.message.severity);


        List<AuraSelectOption> options = new List<AuraSelectOption>{
                new AuraSelectOption('', '--None--'),
                new AuraSelectOption('VF', 'Vodafone', true, true),
                new AuraSelectOption('zg', 'ziggo', true, false)
        };
        AuraField field = new AuraField('Name', 'VF', 'Company Name',
                AuraFieldType.INPUT_SELECT, options);

        System.assertEquals('VF', field.value);
        System.assertEquals('Name', field.name);
        System.assertEquals('Company Name', field.label);
        System.assertEquals(AuraFieldType.INPUT_SELECT, field.auraFieldType);
        System.assertEquals(3, field.options.size());
    }
}