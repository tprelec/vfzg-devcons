@isTest
private class TestCOM_DXLNotificationListenerService {
	@testSetup
	static void setup() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);

		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test', Email = 'contact@test.com', Phone = '+31 111111789');
		insert c;

		Com_dxl_integration_log__c comdxlintegrationlogcObj = new Com_dxl_integration_log__c(
			Transaction_Id__c = 'TR-123456',
			Type__c = 'createCustomer',
			Status__c = 'Success',
			Affected_Objects__c = 'Test Value',
			Error__c = 'Test Value'
		);
		insert comdxlintegrationlogcObj;

		List<External_Account__c> externalAccounts = new List<External_Account__c>();

		External_account__c externalaccountcObj1 = new External_account__c(Account__c = acct.Id, External_Source__c = 'Unify');

		External_account__c externalaccountcObj2 = new External_account__c(Account__c = acct.Id, External_Source__c = 'BOP');

		externalAccounts.add(externalaccountcObj1);
		externalAccounts.add(externalaccountcObj2);

		insert externalAccounts;

		Opportunity testOpp = CS_DataTest.createOpportunity(acct, 'Test Opp', UserInfo.getUserId());
		testOpp.csordtelcoa__Change_Type__c = 'Change';
		testOpp.Name = 'testOpp';
		testOpp.StageName = 'Ready for Order';
		testOpp.CloseDate = System.today();
		testOpp.AccountId = acct.Id;
		testOpp.Main_Contact_Person__c = c.Id;
		insert testOpp;

		cscfga__Product_Basket__c testBasket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
		testBasket.Name = 'testBasket';
		testBasket.cscfga__Opportunity__c = testOpp.Id;
		insert testBasket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c businessInternetDef = CS_DataTest.createProductDefinition('Business Internet');
		businessInternetDef.RecordTypeId = productDefinitionRecordType;
		businessInternetDef.Product_Type__c = 'Fixed';
		insert businessInternetDef;

		cscfga__Product_Configuration__c businessInternetConf = CS_DataTest.createProductConfiguration(
			businessInternetDef.Id,
			'Business Internet',
			testBasket.Id
		);
		insert businessInternetConf;

		csord__Order__c order = new csord__Order__c();
		order.csord__Identification__c = 'Order_' + CS_DataTest.generateRandomString(6);
		insert order;

		Site__c site = CS_DataTest.createSite('Utrecht Lestraat 12', acct, '3572RE', 'Lestraat', 'Utrecht', 12);
		site.Footprint__c = null;
		site.Site_House_Number_Suffix__c = 'A';
		site.Country__c = 'NL';
		insert site;

		COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', order.Id, false);
		insert deliveryOrder;

		csord__Subscription__c businessInternetSub = CS_DataTest.createSubscription(businessInternetConf.Id);
		businessInternetSub.csord__Identification__c = 'Business_Internet_Sub_' + CS_DataTest.generateRandomString(6);
		businessInternetSub.csord__Status__c = 'Subscription created';
		businessInternetSub.csord__Order__c = order.Id;
		insert businessInternetSub;

		csord__Service__c businessInternetService = CS_DataTest.createService(
			order.Id,
			deliveryOrder.Id,
			'Business Internet',
			testBasket.Id,
			businessInternetSub.Id,
			site.Id,
			null,
			businessInternetConf.Id
		);
		insert businessInternetService;

		csord__Service_Line_Item__c businessInternetSLIOneOff = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet One Off SLI',
			businessInternetService.Id,
			false,
			0,
			'Charge',
			'Internet SLI OF'
		);
		insert businessInternetSLIOneOff;
		csord__Service_Line_Item__c businessInternetSLIRec = CS_DataTest.createServiceLineItem(
			testBasket.Id,
			'Business Internet Recurring SLI',
			businessInternetService.Id,
			true,
			0,
			'Charge',
			'Internet SLI REC'
		);
		insert businessInternetSLIRec;
	}

	@isTest
	static void testMethod1() {
		String logId = 'TR-123456';
		List<Com_dxl_integration_log__c> integrationLogRecordList = COM_DXLNotificationListenerService.getIntegrationLog(logId);

		System.assertEquals(
			logId,
			integrationLogRecordList[0].Transaction_Id__c,
			'Expected: ' +
			logId +
			' Actual: ' +
			integrationLogRecordList[0].Transaction_Id__c
		);
	}

	@isTest
	static void testMethod2() {
		String affectedObjects = '[{"type":"Account","sfdcId":"0013O00000VLejmQAD"},{"type":"BAN__c","sfdcId":"a0s1l000007AcelAAC"},{"type":"Financial_Account__c","sfdcId":"a0H1l000004itTiEAI"},{"type":"Billing_Arrangement__c","sfdcId":"a241l000000WWWmAAO"},{"type":"Site__c","sfdcId":"a063O000003vNFmQAM"},{"type":"Contact","sfdcId":"0033O00000Uk4GbQAJ"},{"type":"Contact","sfdcId":"0031l00000xx6icAAA"}]';
		List<COM_DxlAffectedObjects> affectedObjectsList = COM_DXLNotificationListenerService.getAffectedObjects(affectedObjects);

		system.assertEquals(7, affectedObjectsList.size(), 'Expected 7 records, Actual: ' + affectedObjectsList.size());
	}

	@isTest
	static void testMethod3() {
		String affectedObjects = '[{"type":"Account","sfdcId":"0013O00000VLejmQAD"},{"type":"BAN__c","sfdcId":"a0s1l000007AcelAAC"},{"type":"Financial_Account__c","sfdcId":"a0H1l000004itTiEAI"},{"type":"Billing_Arrangement__c","sfdcId":"a241l000000WWWmAAO"},{"type":"Site__c","sfdcId":"a063O000003vNFmQAM"},{"type":"Contact","sfdcId":"0033O00000Uk4GbQAJ"},{"type":"Contact","sfdcId":"0031l00000xx6icAAA"}]';
		List<COM_DxlAffectedObjects> affectedObjectsList = COM_DXLNotificationListenerService.getAffectedObjects(affectedObjects);
		String type = 'Contact';

		List<COM_DxlAffectedObjects> affectedObjectsByType = COM_DXLNotificationListenerService.getAffectedObjectByType(type, affectedObjectsList);

		System.assertEquals(2, affectedObjectsByType.size(), 'Expected 2, Actual: ' + affectedObjectsByType.size());
	}

	@isTest
	static void testMethod4() {
		List<Account> accList = [SELECT Id FROM Account WHERE Name = 'Test Account'];
		List<External_Account__c> externalAccounts = COM_DXLNotificationListenerService.getExternalAccounts(accList[0].Id);

		Integer expected = 2;
		System.assertEquals(expected, externalAccounts.size(), 'Expected ' + expected + ' Actual: ' + externalAccounts.size());
	}

	@isTest
	static void testMethod5() {
		String type = 'Unify';
		List<Account> accList = [SELECT Id FROM Account WHERE Name = 'Test Account'];
		List<External_Account__c> externalAccounts = COM_DXLNotificationListenerService.getExternalAccounts(accList[0].Id);
		List<Id> externalAccountsByType = COM_DXLNotificationListenerService.getExternalAccountsByType(externalAccounts, type);

		Integer expected = 1;
		System.assertEquals(expected, externalAccountsByType.size(), 'Expected ' + expected + ' Actual: ' + externalAccountsByType.size());
	}

	@isTest
	static void testMethod6() {
		String asyncResponse = COM_DXLNotificationListenerService.generateAsyncReponse('COM_DXL_CREATE_CUSTOMER_ASYNC_RESPONSE');

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(asyncResponse, COM_DXLServiceAsyncResponse.class);

		System.assertEquals(true, response.payload.transactionId.equals('TR-1657110353379'), 'Expected transaction Id to start with TR-');
	}
}
