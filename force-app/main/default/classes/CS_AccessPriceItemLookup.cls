global with sharing class CS_AccessPriceItemLookup extends cscfga.ALookupSearch {

 public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit){
       
    System.debug('**** searchFields: ' + JSON.serializePretty(searchFields));   
    system.debug('productDefinitionID: ' + productDefinitionID);
    system.debug('pageOffset: ' + pageOffset);
    system.debug('pageLimit: ' + pageLimit);
       
    final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1; 
    Integer recordOffset = pageOffset * pageLimit;   
    
    //List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>();
    Set<Id> priceItemIds = new Set<Id>();
    Set<Id> priceItemIdsToRemove = new Set<Id>();
    List<cspmb__Price_Item__c> priceItemsFinal = new List<cspmb__Price_Item__c>();
    Map<Id,cspmb__Price_Item__c> priceItemsInitial = new Map<Id,cspmb__Price_Item__c>();

    String vendor = searchFields.get('Vendor');    
    String accessType = searchFields.get('Access Type Calc');
    String accessTypeSearch = '%' + searchFields.get('Access Type Calc') + '%';
    String region = searchFields.get('Region');
    String infraSLA = searchFields.get('Infra SLA');
    Decimal duration = Decimal.valueOf(searchFields.get('Contract Duration'));
    Decimal availableBandwidthUp = ((searchFields.get('Available bandwidth up') == null)||(searchFields.get('Available bandwidth up') == ''))?0:Decimal.valueOf(searchFields.get('Available bandwidth up'));
    Decimal availableBandwidthDown = ((searchFields.get('Available bandwidth down') == null)||(searchFields.get('Available bandwidth down') == ''))?0:Decimal.valueOf(searchFields.get('Available bandwidth down'));
    String resultCheck = searchFields.get('Result Check');
    String service = searchFields.get('Proposition');
    String lineType = searchFields.get('Line Type');
    
    Decimal minUpBandwidth1 = ((searchFields.get('Min. Upstream Bandwidth') == null) ||(searchFields.get('Min. Upstream Bandwidth') == ''))? 0 : Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth'));
    Decimal minDownBandwidth1 = ((searchFields.get('Min.Downstream Bandwidth') == null) ||(searchFields.get('Min.Downstream Bandwidth') == ''))? 0 : Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth'));
    Decimal minUpBandwidth2 = ((searchFields.get('Min. Upstream Bandwidth 2') == null)||(searchFields.get('Min. Upstream Bandwidth 2') == '')) ? 0 : Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth 2'));
    Decimal minDownBandwidth2 = ((searchFields.get('Min.Downstream Bandwidth 2') == null)||(searchFields.get('Min.Downstream Bandwidth 2') == '')) ? 0 : Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth 2'));
    Decimal minUpBandwidthInternet = ((searchFields.get('Min. Upstream Bandwidth Internet') == null)||(searchFields.get('Min. Upstream Bandwidth Internet') == '')) ? 0 : Decimal.valueOf(searchFields.get('Min. Upstream Bandwidth Internet'));
    Decimal minDownBandwidthInternet = ((searchFields.get('Min.Downstream Bandwidth Internet') == null)||(searchFields.get('Min.Downstream Bandwidth Internet') == '')) ? 0 : Decimal.valueOf(searchFields.get('Min.Downstream Bandwidth Internet'));
    Decimal minUpBandwidthOneFixed = ((searchFields.get('Min Bandwidth OneFixed') == null) ||(searchFields.get('Min Bandwidth OneFixed') == ''))? 0 : Decimal.valueOf(searchFields.get('Min Bandwidth OneFixed')); 
    Decimal minDownBandwidthOneFixed = ((searchFields.get('Min Bandwidth OneFixed') == null)||(searchFields.get('Min Bandwidth OneFixed') == '')) ? 0 : Decimal.valueOf(searchFields.get('Min Bandwidth OneFixed'));
    
    Decimal totalBWDown = ((searchFields.get('Total Bandwidth Down') == null)||(searchFields.get('Total Bandwidth Down') == '')) ? 0 : Decimal.valueOf(searchFields.get('Total Bandwidth Down'));
    Decimal totalBWUp = ((searchFields.get('Total Bandwidth Up') == null) ||(searchFields.get('Total Bandwidth Up') == ''))? 0 : Decimal.valueOf(searchFields.get('Total Bandwidth Up'));
    
    Decimal totalReqBWUp = minUpBandwidth1 + minUpBandwidth2 + minUpBandwidthInternet + minUpBandwidthOneFixed;
    Decimal totalReqBWDown = minDownBandwidth1 + minDownBandwidth2 + minDownBandwidthInternet + minDownBandwidthOneFixed;
    
    system.debug('vendor: ' + vendor);
    system.debug('accessType: ' + accessType);
    system.debug('region: ' + region);
    system.debug('infraSLA: ' + infraSLA);
    system.debug('duration: ' + duration);
    system.debug('availableBandwidthUp: ' + availableBandwidthUp);
    system.debug('availableBandwidthDown: ' + availableBandwidthDown);
    system.debug('resultCheck: ' + resultCheck);
    system.debug('service: ' + service);
    system.debug('duration: ' + duration);
    system.debug('totalReqBWUp: ' + totalReqBWUp);
    system.debug('totalReqBWDown: ' + totalReqBWDown);
    
    String searchValue = searchFields.get('searchValue')==''?'%':'%'+searchFields.get('searchValue')+'%';   
    
    priceItemsInitial = new Map<Id,cspmb__Price_Item__c>([SELECT Id, Name, cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Infra_SLA__c, Overbooking__c, Vendor__c, Vendor_lookup__r.Name, 
                                                            Service__c, Duration__c, cspmb__recurring_charge__c, cspmb__one_off_cost__c, cspmb__recurring_cost__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c, Region__c, SLA_Method__c, 
                                                            CPE_SLA__c, Category__c, Category_lookup__r.Name, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, Number_of_LAN__c, Number_of_LANWAN__c, Number_of_WAN__c, Wireless_compatible__c, 
                                                            Hardware_Toeslag__c,LAN_LANWAN__c,LAN_LANWAN_M_WAN__c, WAN_LANWAN__c, WAN_LANWAN_M_LAN__c,Product__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, One_Off_Charge_Product__r.Name, 
                                                            Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c, Deal_Type_Text__c,Extra_Product__c, ProductConfigNameText__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c, 
                                                            Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,One_Off_Charge_Product__r.ThresholdGroup__c,Max_Quantity__c,Min_Quantity__c
                                                                FROM cspmb__Price_Item__c
                                                                WHERE cspmb__Is_Active__c = true AND Access_Type_Text__c LIKE :accessTypeSearch AND (Infra_SLA__c =:infraSLA OR Infra_SLA__c = '*')
                                                                AND Vendor_lookup__r.Name =:vendor AND Line_Type__c = :lineType
                                                                AND (Result_check__c =:resultCheck OR Result_check__c = '*') AND Name LIKE :searchValue AND Product__c !='' AND Category_lookup__r.Name = 'Access'
                                                                AND Available_bandwidth_down__c >= :totalBWDown
                                                                AND Available_bandwidth_up__c >= :totalBWUp
                                                                AND Max_Duration__c > :duration AND Min_Duration__c <= :duration
                                                                ORDER BY Name]);
         
             
        //System.debug('*****Number of priceItemsInitial results: '+ priceItemsInitial.size());

//business rules defined in ACCESS PD_Business rules file

for(cspmb__Price_Item__c pi : priceItemsInitial.values()){
    
    if(((accessType.equalsIgnoreCase(',EthernetOverFiber,')) && (vendor.containsIgnoreCase('KPN'))) || (accessType.equalsIgnoreCase('Coax'))){
    //region is also a condition for filtering
        if(priceItemsInitial.get(pi.Id).Region__c != region){
            priceItemIdsToRemove.add(pi.Id);
        }
    
    }
    
    if(accessType.equalsIgnoreCase(',EthernetOverCopper,')){
        //bandwidth is also a condition for filtering
        if((priceItemsInitial.get(pi.Id).Available_bandwidth_up__c < totalReqBWUp) || (priceItemsInitial.get(pi.Id).Available_bandwidth_down__c < totalReqBWDown)){
            priceItemIdsToRemove.add(pi.Id);
        }
    }
}




//end business rules defined in ACCESS PD_Business rules file
            
            priceItemsFinal = [SELECT Id, Name, cspmb__Is_Active__c, Access_Type_Text__c, Available_bandwidth_up__c, Available_bandwidth_down__c, Infra_SLA__c, Overbooking__c, Vendor__c, Vendor_lookup__r.Name, Service__c, 
                                Duration__c, cspmb__recurring_charge__c, cspmb__one_off_cost__c, cspmb__recurring_cost__c, cspmb__Contract_Term__c, cspmb__One_Off_Charge__c, Region__c, SLA_Method__c, CPE_SLA__c, Category__c, 
                                Category_lookup__r.Name, Available_bandwidth_QoS_down__c, Available_bandwidth_QoS_up__c, Number_of_LAN__c, Number_of_LANWAN__c, Number_of_WAN__c, Wireless_compatible__c, Hardware_Toeslag__c,LAN_LANWAN__c,
                                LAN_LANWAN_M_WAN__c, WAN_LANWAN__c, WAN_LANWAN_M_LAN__c,Product__c, Line_Type__c, Router_Type__c, Subscription_Profile__c, SFP_Slots__c, One_Off_Charge_Product__r.Name, Recurring_Charge_Product__r.Name, Recurring_Charge_Product__r.ThresholdGroup__c,One_Off_Charge_Product__r.ThresholdGroup__c,
                                Deal_Type_Text__c, Extra_Product__c, ProductConfigNameText__c, OneFixed_Technology_Type__c, OneFixed_Codec__c, OneFixed_SIP_Channels__c, Max_Duration__c, Min_Duration__c, Result_check__c, Bundle_Services_Text__c, Discount_allowed__c, cspmb__Recurring_Charge_External_Id__c, cspmb__One_Off_Charge_External_Id__c, Recurring_Charge_Taxonomy__c, One_Off_Charge_Taxonomy__c,Max_Quantity__c,Min_Quantity__c
                                    FROM cspmb__Price_Item__c
                                    WHERE Id IN :priceItemsInitial.keySet() AND Id NOT IN :priceItemIdsToRemove
                                    ORDER BY Name
                                    LIMIT :SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT OFFSET :recordOffset];
                            
            System.debug('*****PRICE ITEMS FINAL: '+ JSON.serializePretty(priceItemsFinal));                
            
            return priceItemsFinal;

    }


  public override String getRequiredAttributes(){ 
  
      return '["Total Bandwidth Down","Total Bandwidth Up","Vendor","Access Type Calc","Region","Infra SLA","Contract Duration","Available bandwidth up","Available bandwidth down","Result Check", "Proposition", "Overbooking Voip","Contract Duration", "Line Type", "Min. Upstream Bandwidth", "Min.Downstream Bandwidth", "Min. Upstream Bandwidth 2", "Min.Downstream Bandwidth 2", "Min. Upstream Bandwidth Internet", "Min.Downstream Bandwidth Internet","Min Bandwidth OneFixed"]';
      
  }  
}