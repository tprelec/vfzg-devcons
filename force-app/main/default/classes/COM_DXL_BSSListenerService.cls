public with sharing class COM_DXL_BSSListenerService {
	public static List<COM_DXL_Integration_Log__c> getIntegrationLog(String transactionId) {
		return [
			SELECT Id, Name, Transaction_Id__c, Affected_Objects__c, Error__c, Opportunity__c, Status__c, Type__c
			FROM COM_DXL_Integration_Log__c
			WHERE Transaction_Id__c = :transactionId
		];
	}

	public static COM_DXL_Integration_Log__c getLatestIntegrationLog(String logType) {
		return [
			SELECT Id, Name, Transaction_Id__c, Affected_Objects__c, Error__c, Opportunity__c, Status__c, Type__c
			FROM COM_DXL_Integration_Log__c
			WHERE Type__c = :logType
			ORDER BY CreatedDate DESC
			LIMIT 1
		];
	}

	public static Boolean orderLineStatusSuccess(COM_DXLCreateBSSOrderAsyncResponse.OrderLines orderLine) {
		Boolean returnValue = true;
		if (
			orderLine.orderLineStatus == null ||
			(Integer.valueOf(orderLine.orderLineStatus.statusCode) < 200 ||
			Integer.valueOf(orderLine.orderLineStatus.statusCode) > 299)
		) {
			returnValue = false;
		}
		return returnValue;
	}
}
