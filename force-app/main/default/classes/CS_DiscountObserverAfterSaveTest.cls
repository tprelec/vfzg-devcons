@isTest
public class CS_DiscountObserverAfterSaveTest {
	public static cscfga__Product_Configuration__c pcForTest = new cscfga__Product_Configuration__c();
	public static cscfga__Product_Configuration__c pcForTestNotMobile = new cscfga__Product_Configuration__c();
	public static cscfga__Product_Basket__c basketForTest = new cscfga__Product_Basket__c();
	public static String discountString = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
	public static Boolean executeSetup = true;

	@isTest
	private static void setup() {
		if (executeSetup) {
			Framework__c frameworkSetting = new Framework__c();
			frameworkSetting.Framework_Sequence_Number__c = 2;
			upsert frameworkSetting;

			PriceReset__c priceResetSetting = new PriceReset__c();

			priceResetSetting.MaxRecurringPrice__c = 200.00;
			priceResetSetting.ConfigurationName__c = 'IP Pin';

			upsert priceResetSetting;

			Sales_Settings__c ssettings = new Sales_Settings__c();
			ssettings.Postalcode_check_validity_days__c = 2;
			ssettings.Max_Daily_Postalcode_Checks__c = 2;
			ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
			ssettings.Postalcode_check_block_period_days__c = 2;
			ssettings.Max_weekly_postalcode_checks__c = 15;
			upsert ssettings;

			Account account = new Account(OwnerId = UserInfo.getUserId(), Name = 'AccountTest', Type = 'End CustomerTest');
			upsert account;

			Contact contact = new Contact(
				AccountId = account.id,
				LastName = 'LastTest',
				FirstName = 'FirstTest',
				Contact_Role__c = 'Consultant',
				Email = 'test1@vf.com'
			);
			upsert contact;

			//Price Item
			OrderType__c orderType = CS_DataTest.createOrderType();
			insert orderType;
			Product2 product1 = CS_DataTest.createProduct('Access Infrastructure', orderType);
			insert product1;

			Category__c accessCategory = CS_DataTest.createCategory('Access');
			insert accessCategory;

			Vendor__c vendor1 = CS_DataTest.createVendor('KPNWEAS');
			insert vendor1;
			cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(
				product1,
				accessCategory,
				20480.0,
				20480.0,
				vendor1,
				'ONNET',
				'OfficeTTR2BD'
			);
			insert priceItem1;

			priceItem1.Recurring_Discount_Sales_Channel__c = 'Direct;Indirect';
			priceItem1.Recurring_Product_Discount_Allowed__c = true;
			priceItem1.OneOff_Discount_Sales_Channel__c = 'Direct;Indirect';
			priceItem1.One_Off_Product_Discount_Allowed__c = true;
			update priceItem1;

			Opportunity opportunity = new Opportunity(
				Name = 'New OpportunityTest',
				OwnerId = UserInfo.getUserId(),
				StageName = 'Qualification',
				Probability = 0,
				CloseDate = system.today(),
				//ForecastCategoryName = 'Pipeline',
				AccountId = account.id
			);
			upsert opportunity;

			cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
				Name = 'Mobile',
				cscfga__Description__c = 'PD1 Desc',
				Snapshot_Object__c = 'CS_Basket_Snapshot_Transactional__c'
			);
			pd.Product_Type__c = 'Fixed';
			upsert pd;

			cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c(
				Name = 'NotM',
				cscfga__Description__c = 'PD2 Desc',
				Snapshot_Object__c = 'CS_Basket_Snapshot_Transactional__c'
			);
			pd2.Product_Type__c = 'Fixed';
			upsert pd2;

			basketForTest = new cscfga__Product_Basket__c(
				Name = 'New Basket',
				cscfga__Basket_Status__c = 'Approved',
				OwnerId = UserInfo.getUserId(),
				cscfga__Opportunity__c = opportunity.Id,
				Basket_qualification__c = 'SAC SRC',
				Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
			);
			upsert basketForTest;

			cscfga__Product_Configuration__c parentPc = new cscfga__Product_Configuration__c(
				cscfga__Product_Definition__c = pd.Id,
				cscfga__Product_Basket__c = basketForTest.Id,
				cscfga__Quantity__c = 1,
				GroupDiscount__c = null,
				cscfga__total_one_off_charge__c = 50.00,
				cscfga__total_recurring_charge__c = 10.00,
				Name = 'PCParent'
			);
			upsert parentPc;

			pcForTest = new cscfga__Product_Configuration__c(
				cscfga__Product_Definition__c = pd.Id,
				cscfga__Product_Basket__c = basketForTest.Id,
				cscfga__Quantity__c = 1,
				GroupDiscount__c = null,
				cscfga__total_one_off_charge__c = 50.00,
				cscfga__total_recurring_charge__c = 10.00,
				Discount_allowed__c = true,
				Commercial_Product__c = priceItem1.Id,
				csdiscounts__manual_discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}',
				cscfga__Parent_Configuration__c = parentPc.Id,
				cscfga__Root_Configuration__c = parentPc.Id,
				Name = 'PCParent'
			);
			upsert pcForTest;

			pcForTestNotMobile = new cscfga__Product_Configuration__c(
				cscfga__Product_Definition__c = pd2.Id,
				cscfga__Product_Basket__c = basketForTest.Id,
				cscfga__Quantity__c = 1,
				GroupDiscount__c = null,
				cscfga__total_one_off_charge__c = 50.00,
				cscfga__total_recurring_charge__c = 10.00,
				Discount_allowed__c = true,
				Commercial_Product__c = priceItem1.Id,
				csdiscounts__manual_discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}',
				cscfga__Parent_Configuration__c = null,
				Name = 'PCParent'
			);
			upsert pcForTestNotMobile;
			executeSetup = false;

			List<cscfga__Product_Configuration__c> testList = new List<cscfga__Product_Configuration__c>();
			testList.add(pcForTest);
			testList.add(pcForTestNotMobile);
			CS_DiscountObserverAfterSave dsc = new CS_DiscountObserverAfterSave();

			basketForTest.Basket_qualification__c = 'Net profit';
			dsc.basketInNetProfit(basketForTest);
			dsc.roundToFourDecimalPlaces(100.00);
			dsc.calculatePercentage(5.00, 5.00);

			dsc.execute(testList);

			Boolean res = CS_DiscountObserverAfterSave.DiscountIsValid(pcForTest);

			cspmb__Add_On_Price_Item__c testAddOn = new cspmb__Add_On_Price_Item__c();
			testAddOn.cspmb__Account__c = account.id;
			testAddOn.cspmb__Is_One_Off_Discount_Allowed__c = true;
			testAddOn.cspmb__Is_Recurring_Discount_Allowed__c = true;
			testAddOn.OneOff_Discount_Sales_Channel__c = 'Direct';
			testAddOn.Recurring_Discount_Sales_Channel__c = 'Direct';
			insert testAddOn;
			List<cscfga.ProductConfiguration.Discount> discountList = CS_DiscountObserverAfterSave.getDiscountObjectsOOTBStructure(
				pcForTest.csdiscounts__manual_discounts__c,
				new cscfga.ProductConfiguration.Discount()
			);
			List<cscfga.ProductConfiguration.Discount> res2 = CS_DiscountObserverAfterSave.getNotAllowedDiscountsAddon(
				'Direct',
				discountList,
				testAddOn
			);
			List<cscfga.ProductConfiguration.Discount> res3 = CS_DiscountObserverAfterSave.removeNotAllowedDiscounts(discountList, discountList);
			Map<String, Boolean> res8 = CS_DiscountObserverAfterSave.discountTypeAllowedAddon('Direct', testAddOn);

			Decimal testNum1 = 5;
			Decimal testNum2 = 7;

			Decimal res4 = dsc.calculatePercentage(testNum2, testNum1);
			Decimal res5 = dsc.roundToFourDecimalPlaces(testNum2);
			Integer res6 = dsc.roundDecimalNumber(testNum1);
			List<Id> idListPC = new List<Id>{ pcForTest.Id };
			Boolean res7 = dsc.CS_DiscountObserverAfterSave(idListPC);

			cscfga__Product_Definition__c pd3 = new cscfga__Product_Definition__c();
			pd3.Name = 'Test';
			pd3.cscfga__Description__c = 'PD2 Desc';
			pd3.Snapshot_Object__c = 'CS_Basket_Snapshot_Transactional__c';
			pd3.Product_Type__c = 'Fixed';
			insert pd3;

			cspmb__Price_Item__c priceItem2 = CS_DataTest.createPriceItem(
				product1,
				accessCategory,
				20480.0,
				20480.0,
				vendor1,
				'ONNET',
				'OfficeTTR2BD'
			);
			priceItem2.Recurring_Discount_Sales_Channel__c = 'Direct;Indirect';
			priceItem2.Recurring_Product_Discount_Allowed__c = true;
			priceItem2.OneOff_Discount_Sales_Channel__c = 'Indirect';
			priceItem2.One_Off_Product_Discount_Allowed__c = true;
			insert priceItem2;

			cscfga__Product_Configuration__c pcForGDTest = new cscfga__Product_Configuration__c(
				cscfga__Product_Definition__c = pd3.Id,
				cscfga__Product_Basket__c = basketForTest.Id,
				cscfga__Quantity__c = 1,
				cscfga__total_one_off_charge__c = 50.00,
				cscfga__total_recurring_charge__c = 10.00,
				Discount_allowed__c = true,
				Commercial_Product__c = priceItem2.Id,
				csdiscounts__manual_discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}',
				cscfga__Parent_Configuration__c = parentPc.Id,
				cscfga__Root_Configuration__c = parentPc.Id,
				Add_On_Product__c = testAddOn.Id,
				GroupDiscount__c = '{"discounts":[]}',
				Name = 'PCParent'
			);

			insert pcForGDTest;
			List<cscfga__Product_Configuration__c> testList2 = new List<cscfga__Product_Configuration__c>();
			testList2.add(pcForGDTest);
			dsc.execute(testList2);
			System.debug('TEST pcForGDTest ' + pcForGDTest.cscfga__Product_Definition__r + '  ' + pcForGDTest.cscfga__Product_Definition__r.Name);
			//Boolean res9 = dsc.isMobile(pcForGDTest);
		}
	}
	@isTest
	private static void testFlexPortfolio() {
		Framework__c frameworkSetting = new Framework__c();
		frameworkSetting.Framework_Sequence_Number__c = 2;
		insert frameworkSetting;

		PriceReset__c priceResetSetting = new PriceReset__c();

		priceResetSetting.MaxRecurringPrice__c = 200.00;
		priceResetSetting.ConfigurationName__c = 'IP Pin';

		insert priceResetSetting;

		Sales_Settings__c ssettings = new Sales_Settings__c();
		ssettings.Postalcode_check_validity_days__c = 2;
		ssettings.Max_Daily_Postalcode_Checks__c = 2;
		ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
		ssettings.Postalcode_check_block_period_days__c = 2;
		ssettings.Max_weekly_postalcode_checks__c = 15;
		insert ssettings;

		Account account = new Account(OwnerId = UserInfo.getUserId(), Name = 'Account', Type = 'End Customer');
		insert account;

		Contact contact = new Contact(
			AccountId = account.id,
			LastName = 'Last',
			FirstName = 'First',
			Contact_Role__c = 'Consultant',
			Email = 'test@vf.com'
		);
		insert contact;

		Opportunity opportunity = new Opportunity(
			Name = 'New Opportunity',
			OwnerId = UserInfo.getUserId(),
			StageName = 'Qualification',
			Probability = 0,
			CloseDate = system.today(),
			//ForecastCategoryName = 'Pipeline',
			AccountId = account.id
		);
		insert opportunity;

		cscfga__Product_Category__c flexCategory = new cscfga__Product_Category__c();
		flexCategory.Name = 'FLEX Portfolio Services';
		insert flexCategory;

		cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
			Name = 'Access',
			cscfga__Description__c = 'Access Description',
			Snapshot_Object__c = 'CS_Basket_Snapshot_Transactional__c',
			cscfga__Product_Category__c = flexCategory.Id,
			Product_Type__c = 'Fixed'
		);
		insert pd;

		cscfga__Attribute_Definition__c adOneOff = new cscfga__Attribute_Definition__c(
			cscfga__Product_Definition__c = pd.Id,
			Name = 'OneOffPrice',
			Snapshot_Attribute_Value_Field__c = 'OneOffPrice__c',
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = false
		);
		insert adOneOff;

		cscfga__Attribute_Definition__c adRecurring = new cscfga__Attribute_Definition__c(
			cscfga__Product_Definition__c = pd.Id,
			Name = 'RecurringPrice',
			Snapshot_Attribute_Value_Field__c = 'RecurringPrice__c',
			cscfga__Is_Line_Item__c = true,
			cscfga__Recurring__c = true
		);
		insert adRecurring;

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
			Name = 'New Basket',
			cscfga__Basket_Status__c = 'Approved',
			OwnerId = UserInfo.getUserId(),
			cscfga__Opportunity__c = opportunity.Id,
			Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
		);
		insert basket;

		cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
			cscfga__Product_Definition__c = pd.Id,
			cscfga__Product_Basket__c = basket.Id,
			cscfga__Quantity__c = 1,
			cscfga__total_one_off_charge__c = 50.00,
			cscfga__total_recurring_charge__c = 10.00,
			csdiscounts__manual_discounts__c = '{"discounts":[{"version":"3-0-0","type":"absolute","source":"coax-100-30-24m ATL","recurringOffset":0,"recordType":"single","memberDiscounts":null,"evaluationOrder":null,"duration":null,"discountPrice":null,"discountCharge":"__PRODUCT__","description":"coax-100-30-24m ATL","customData":{"discountCode":"coax-100-30-24m-atl"},"chargeType":"recurring","amount":7.50}]}',
			//cscfga__discounts__c = '{"discounts":[{"memberDiscounts":null, "evaluationOrder":null, "version":"3-0-0","type":"percentage","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"recurring","amount":50}]}',
			Name = 'PCParent',
			New_Portfolio__c = true
		);
		insert pc;

		cscfga__Attribute__c recurrinAttr = new cscfga__Attribute__c();
		recurrinAttr.Name = 'RecurringPrice';
		recurrinAttr.cscfga__Is_Line_Item__c = true;
		recurrinAttr.cscfga__Price__c = 50;
		recurrinAttr.cscfga__Value__c = '50';
		recurrinAttr.cscfga__Product_Configuration__c = pc.Id;
		recurrinAttr.cscfga__Is_Active__c = true;
		recurrinAttr.cscfga__Recurring__c = true;
		insert recurrinAttr;

		cscfga__Attribute__c oneOffAttr = new cscfga__Attribute__c();
		oneOffAttr.Name = 'OneOffPrice';
		oneOffAttr.cscfga__Is_Line_Item__c = true;
		oneOffAttr.cscfga__Price__c = 100;
		oneOffAttr.cscfga__Value__c = '100';
		oneOffAttr.cscfga__Product_Configuration__c = pc.Id;
		oneOffAttr.cscfga__Is_Active__c = true;
		oneOffAttr.cscfga__Recurring__c = false;
		insert oneOffAttr;

		Test.startTest();
		CS_DiscountObserverAfterSave obs = new CS_DiscountObserverAfterSave();
		List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>();
		configs.add(pc);
		obs.execute(configs);
		pc.csdiscounts__manual_discounts__c = '{"discounts":[{"version":"3-0-0","type":"absolute","source":"coax-100-30-24m ATL","recurringOffset":0,"recordType":"single","memberDiscounts":null,"evaluationOrder":null,"duration":null,"discountPrice":null,"discountCharge":"__PRODUCT__","description":"coax-100-30-24m ATL","customData":{"discountCode":"coax-100-30-24m-atl"},"chargeType":"recurring","amount":7.50}]}';
		update pc;
		String message = obs.execute(configs);
		System.assertNotEquals(null, message);
		Test.stopTest();
	}
}
