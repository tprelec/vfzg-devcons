@isTest
private class TestZiggoMigrationFieldsFormController {
	@testSetup
	static void prepareCommonTestData() {
		TestUtils.createAccount(GeneralUtils.currentUser);
		TestUtils.createContact(TestUtils.theAccount);
		TestUtils.createOpportunity(TestUtils.theAccount, Test.getStandardPricebookId());
		TestUtils.createSite(TestUtils.theAccount);
		List<cscfga__Product_Basket__c> listProductBasketToInsert = new List<cscfga__Product_Basket__c>();
		cscfga__Product_Basket__c productBasketWithSites = new cscfga__Product_Basket__c(
			cscfga__Opportunity__c = TestUtils.theOpportunity.Id,
			csbb__Account__c = TestUtils.theAccount.Id
		);
		cscfga__Product_Basket__c productBasketWithNoSites = new cscfga__Product_Basket__c(
			cscfga__Opportunity__c = TestUtils.theOpportunity.Id,
			csbb__Account__c = TestUtils.theAccount.Id
		);
		listProductBasketToInsert.add(productBasketWithSites);
		listProductBasketToInsert.add(productBasketWithNoSites);
		insert listProductBasketToInsert;
		List<cscfga__Product_Configuration__c> listProductConfigurationToInsert = new List<cscfga__Product_Configuration__c>();
		listProductConfigurationToInsert.add(
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = productBasketWithSites.Id, Site__c = TestUtils.theSite.Id)
		);
		listProductConfigurationToInsert.add(new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = productBasketWithNoSites.Id));
		insert listProductConfigurationToInsert;
	}

	@isTest
	private static void testGetSitesWithNoData() {
		cscfga__Product_Configuration__c producproductConfiguration = [
			SELECT Id, cscfga__Product_Basket__c, cscfga__Product_Basket__r.Id, Site__c
			FROM cscfga__Product_Configuration__c
			WHERE Site__c = NULL
			LIMIT 1
		];
		Test.startTest();
		LightningResponse response = ZiggoMigrationFieldsFormController.getFields(producproductConfiguration.cscfga__Product_Basket__r.Id);
		Test.stopTest();
		System.assertEquals(null, response.body, 'The exepected value is null is different to the actual value ' + response.body);
	}

	@isTest
	static void testGetSitesWithBasketData() {
		cscfga__Product_Configuration__c productConfiguration = [
			SELECT Id, cscfga__Product_Basket__c, cscfga__Product_Basket__r.Id, Site__c, Site__r.Name
			FROM cscfga__Product_Configuration__c
			WHERE Site__c != NULL
			LIMIT 1
		];
		Test.startTest();
		LightningResponse response = ZiggoMigrationFieldsFormController.getFields(productConfiguration.cscfga__Product_Basket__r.Id);
		Test.stopTest();
		List<AccordionFormController.accordionSection> sitesResponse = (List<AccordionFormController.accordionSection>) response.body;
		System.assertEquals(1, sitesResponse.size(), 'The exepected value is 1 is different to the actual value ' + response.body);
		System.assertEquals(
			productConfiguration.Site__r.Name,
			sitesResponse[0].tittleLabel,
			'The exepected value is ' +
			productConfiguration.Site__r.Name +
			' is different to the actual value ' +
			sitesResponse[0].tittleLabel
		);
	}
}
