@IsTest
private class TestCS_CustStUpdStatusCommercialCmps {
    @IsTest
    static void testBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            List<SObject> objects = new List<SObject>();

            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            insert ord;

            List<Suborder__c> solutions = CS_DataTest.createMultipleSolutions('Solution 1',ord.Id, 2, true);

            List<csord__Subscription__c> subscriptions = CS_DataTest.createMultipleSubscriptions(testConfConf.Id,'Subscription Created', 2, false);
            for (csord__Subscription__c subscription : subscriptions) {
                subscription.Suborder__c = solutions[0].Id;
            }
            insert subscriptions;

            List<csord__Subscription__c> subscriptionsForSecondSolution = CS_DataTest.createMultipleSubscriptions(testConfConf.Id,'Subscription Created', 1, false);
            for (csord__Subscription__c subscription : subscriptionsForSecondSolution) {
                subscription.Suborder__c = solutions[1].Id;
            }
            insert subscriptionsForSecondSolution;

            List<csord__Service__c> servicesForFirstSubscription = CS_DataTest.createMultipleServices(testConfConf.Id,subscriptions[0],'Service Created', 3 , false);
            for (csord__Service__c service : servicesForFirstSubscription) {
                service.Suborder__c = solutions[0].Id;
            }
            insert servicesForFirstSubscription;

            List<csord__Service__c> servicesForSecondSubscription = CS_DataTest.createMultipleServices(testConfConf.Id,subscriptions[1],'Service Created', 3 , false);
            for (csord__Service__c service : servicesForSecondSubscription) {
                service.Suborder__c = solutions[0].Id;
            }
            insert servicesForSecondSubscription;

            List<csord__Service__c> servicesForThirdSubscription = CS_DataTest.createMultipleServices(testConfConf.Id,subscriptionsForSecondSolution[0],'Service Created', 3 , false);
            for (csord__Service__c service : servicesForThirdSubscription) {
                service.Suborder__c = solutions[1].Id;
            }
            insert servicesForThirdSubscription;

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', false);
            insert step1Template;

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.Suborder__c = solutions[0].Id;
            insert testProcess;

            CSPOFA__Orchestration_Process__c testProcess2 = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess2.Suborder__c = solutions[1].Id;
            insert testProcess2;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, false);
            step1.COM_Status__c = 'Ready for MERE';
            step1.COM_Update_Services__c = true;
            step1.COM_Update_Subscriptions__c = true;
            step1.COM_Update_Solutions__c = true;
            insert step1;
            objects.add(step1);

            CSPOFA__Orchestration_Step__c step2 = CS_DataTest.createOrchStep(step1Template.Id, testProcess2.Id, false);
            step2.COM_Status__c = 'Ready for MERE';
            step2.COM_Update_Services__c = true;
            step2.COM_Update_Subscriptions__c = true;
            step2.COM_Update_Solutions__c = true;
            insert step2;
            objects.add(step2);

            CS_CustomStepUpdateStatusCommercialCmps customStep = new CS_CustomStepUpdateStatusCommercialCmps();
            customStep.process(objects);

            List<csord__Service__c> newServices = [SELECT Id,
                                                        csord__Status__c
                                                FROM csord__Service__c
                                                WHERE Id IN :servicesForFirstSubscription
                                                OR Id IN :servicesForSecondSubscription
                                                OR Id IN :servicesForThirdSubscription];

            for (csord__Service__c service : newServices) {
                System.debug('+++ service: ' + JSON.serializePretty(service));
                System.assertEquals(service.csord__Status__c,'Ready for MERE');
            }

            List<csord__Subscription__c> newSubscriptions = [SELECT Id,
                                                                    csord__Status__c
                                                            FROM csord__Subscription__c
                                                            WHERE Id IN :subscriptions OR Id IN :subscriptionsForSecondSolution];

            for (csord__Subscription__c subscription : newSubscriptions) {
                System.debug('+++ subscriptions: ' + JSON.serializePretty(subscription));
                System.assertEquals(subscription.csord__Status__c,'Ready for MERE');
            }

            List<Suborder__c> newSolutions = [SELECT Id,
                                                            Status__c
                                                    FROM Suborder__c
                                                    WHERE Id IN :solutions];

            for (Suborder__c solution : newSolutions) {
                System.debug('+++ solution: ' + JSON.serializePretty(solution));
                System.assertEquals(solution.Status__c,'Ready for MERE');
            }
        }
    }
}