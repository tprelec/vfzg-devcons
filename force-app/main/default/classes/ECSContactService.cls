/**
 * @description			This class is responsible for creating and deleting contacts in ECS.
 * @author				Guy Clairbois
 */
@SuppressWarnings('PMD.ExcessivePublicCount,PMD.TooManyFields')
public class ECSContactService extends ECSSOAPBaseWebService implements IWebService<List<Request>, List<Response>> {
	/**
	 * @description			This is the request which will be sent to ECS create or delete a
	 *						user.
	 */
	private Request[] requests;
	private Response[] responses;
	public class Request {
		// reference to user
		public String email;
		public String userCompanyBanNumber;
		public String userCompanyCorporateId;
		public String userCompanyBopCode;

		// reference to company
		public String companyBanNumber;
		public String companyCorporateId;
		public String companyBopCode;

		// reference to location
		public String houseNumber;
		public String houseNumberExtension;
		public String zipCode;

		// contact fields
		public String type;
		public String service;
	}

	/**
	 * @description			This is the response that will be returned after making a request to ECS.
	 */
	public class Response {
		public String type;
		public String service;

		// company ref
		public String ban;
		public String corporateId;
		public String bopCode;

		// location ref
		public String houseNumber;
		public String houseNumberExtension;
		public String zipCode;

		// user ref
		public String email;
		public String userCompanyBanNumber;
		public String userCompanyCorporateId;
		public String userCompanyBopCode;

		// error fields
		public String errorCode;
		public String errorMessage;
	}

	/**
	 * @description			This will return the response returned by ECS. It will contain the user
	 *						number and optionally a list of messages that may need to be displayed to the user.
	 */
	public Response[] getResponse() {
		return responses;
	}

	/**
	 * @description			The constructor is responsible for setting the default web service configuration.
	 */
	public ECSContactService() {
		if (!Test.isRunningTest()) {
			if (FeatureFlagging.isActive('BOP_User')) {
				setWebServiceConfig(WebServiceConfigLocator.getConfigNamedCredential('ECSSOAPUser'));
			} else {
				setWebServiceConfig(WebServiceConfigLocator.getConfig('ECSSOAPUser'));
			}
		} else {
			setWebServiceConfig(WebServiceConfigLocator.createConfig());
		}
	}

	/**
	 * @description			This sets the user data to create/delete user details in ECS.
	 */
	public void setRequest(Request[] request) {
		this.requests = request;
	}

	public void makeRequest(String requestType) {
		if (requestType == 'create') {
			makeCreateRequest();
		} else if (requestType == 'delete') {
			makeDeleteRequest();
		}
	}

	/**
	 * @description			This will make the request to ECS to create contacts with the request.
	 */
	public void makeCreateRequest() {
		checkExceptions();
		ECSSOAPUser.userSoap service = setServiceSOAP();

		List<ECSSOAPUser.contactType> createRequest = buildContactCreateUsingRequest();
		System.debug(LoggingLevel.INFO, 'createRequest: ' + JSON.serialize(createRequest));

		// create queryResult
		List<ECSSOAPUser.contactResponseType> createResults;

		try {
			system.debug(LoggingLevel.INFO, createRequest);
			createResults = service.createContacts(createRequest);
			system.debug(LoggingLevel.INFO, createResults);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException('Error received from BOP when attempting to create the contact(s): ' + ex.getMessage(), ex);
		}

		parseResponse(createResults);
	}

	/**
	 * @description			This will make the request to ECS to delete contacts with the request.
	 */
	public void makeDeleteRequest() {
		checkExceptions();

		ECSSOAPUser.userSoap service = setServiceSOAP();

		List<ECSSOAPUser.contactType> deleteRequest = buildContactDeleteUsingRequest();
		System.debug(LoggingLevel.INFO, 'deleteRequest: ' + JSON.serialize(deleteRequest));

		// create queryResult
		List<ECSSOAPUser.contactResponseType> deleteResults;

		try {
			deleteResults = service.deleteContacts(deleteRequest);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException('Error received from BOP when attempting to delete the contact(s): ' + ex.getMessage(), ex);
		}

		parseResponse(deleteResults);
	}

	private ECSSOAPUser.userSoap setServiceSOAP() {
		// WebService callout logic
		ECSSOAPUser.userSoap service = new ECSSOAPUser.userSoap();
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPUser.authenticationHeader_element header = new ECSSOAPUser.authenticationHeader_element();
		header.applicationId = 'sfdc';
		header.username = webServiceConfig.getUsername();
		header.password = webServiceConfig.getPassword();
		service.authenticationHeader = header;
		return service;
	}

	/**
	 * @description			This will build the create request object which will be sent to ECS.
	 */
	private List<ECSSOAPUser.contactType> buildContactCreateUsingRequest() {
		List<ECSSOAPUser.contactType> requestList = new List<ECSSOAPUser.contactType>();

		for (Request request : this.requests) {
			ECSSOAPUser.contactType thisContact = new ECSSOAPUser.contactType();

			ECSSOAPUser.companyRefType thisCompany = new ECSSOAPUser.companyRefType();
			thisCompany.banNumber = request.companyBanNumber;
			thisCompany.bopCode = request.companyBopCode;
			thisCompany.corporateId = request.companyCorporateId;

			if (request.zipcode != null) {
				ECSSOAPUser.locationRefType thisLocation = new ECSSOAPUser.locationRefType();
				thisLocation.zipCode = request.zipcode;
				thisLocation.houseNumber = request.houseNumber;
				thisLocation.houseNumberExt = request.houseNumberExtension;
				thisContact.location = thisLocation;

				thisContact.location.company = thisCompany;
			} else {
				thisContact.company = thisCompany;
			}

			ECSSOAPUser.userRefType thisUser = new ECSSOAPUser.userRefType();
			thisUser.email = request.email;
			thisContact.user_x = thisUser;

			ECSSOAPUser.companyRefType thisUserCompany = new ECSSOAPUser.companyRefType();
			thisUserCompany.banNumber = request.userCompanyBanNumber;
			thisUserCompany.bopCode = request.userCompanyBopCode;
			thisUserCompany.corporateId = request.userCompanyCorporateId;
			thisContact.user_x.company = thisUserCompany;

			thisContact.type_x = request.type;
			thisContact.service = request.service;

			requestList.add(thisContact);
		}

		return requestList;
	}

	/**
	 * @description			This will build the delete request object which will be sent to ECS.
	 */
	private List<ECSSOAPUser.contactType> buildContactDeleteUsingRequest() {
		List<ECSSOAPUser.contactType> requestList = new List<ECSSOAPUser.contactType>();

		for (Request request : this.requests) {
			ECSSOAPUser.contactType thisContact = new ECSSOAPUser.contactType();

			ECSSOAPUser.companyRefType thisCompany = new ECSSOAPUser.companyRefType();
			thisCompany.banNumber = request.companyBanNumber;
			thisCompany.bopCode = request.companyBopCode;
			thisCompany.corporateId = request.companyCorporateId;

			if (request.zipcode != null) {
				ECSSOAPUser.locationRefType thisLocation = new ECSSOAPUser.locationRefType();
				thisLocation.zipCode = request.zipcode;
				thisLocation.houseNumber = request.houseNumber;
				thisLocation.houseNumberExt = request.houseNumberExtension;
				thisContact.location = thisLocation;

				thisContact.location.company = thisCompany;
			} else {
				thisContact.company = thisCompany;
			}

			ECSSOAPUser.userRefType thisUser = new ECSSOAPUser.userRefType();
			thisUser.email = request.email;
			thisContact.user_x = thisUser;

			ECSSOAPUser.companyRefType thisUserCompany = new ECSSOAPUser.companyRefType();
			thisUserCompany.banNumber = request.userCompanyBanNumber;
			thisUserCompany.bopCode = request.userCompanyBopCode;
			thisUserCompany.corporateId = request.userCompanyCorporateId;
			thisContact.user_x.company = thisUserCompany;

			thisContact.type_x = request.type;
			thisContact.service = request.service;

			requestList.add(thisContact);
		}

		return requestList;
	}

	/**
	 * @description			This will parse the result returned by ECS.
	 * @param	theResults	The result object returned by ECS which contains messages.
	 */
	private void parseResponse(List<ECSSOAPUser.contactResponseType> theResults) {
		responses = new List<Response>{};
		if (theResults == null) {
			return;
		}
		System.debug(LoggingLevel.INFO, '##### RAW RESPONSE: ' + theResults);

		for (ECSSOAPUser.contactResponseType result : theResults) {
			if (result != null) {
				Response response = new Response();
				if (result.errors != null && result.errors.error.size() > 0) {
					response.errorCode = result.errors.error[0].Code;
					response.errorMessage = result.errors.error[0].Message;
				}

				if (result.location != null) {
					response.zipCode = result.location.zipCode;
					response.houseNumber = result.location.houseNumber;
					response.houseNumberExtension = result.location.houseNumberExt;
					response.ban = result.location.company.banNumber;
					response.bopCode = result.location.company.bopCode;
					response.corporateId = result.location.company.corporateId;
				} else {
					response.ban = result.company.banNumber;
					response.bopCode = result.company.bopCode;
					response.corporateId = result.company.corporateId;
				}

				response.email = result.user_x.email;
				response.userCompanyBanNumber = result.user_x.company.banNumber;
				response.userCompanyBopCode = result.user_x.company.bopCode;
				response.userCompanyCorporateId = result.user_x.company.CorporateId;

				response.type = result.type_x;
				response.service = result.service;

				System.debug(LoggingLevel.INFO, '##### Messages: ' + response.errorMessage);

				responses.add(response);
			}
		}
	}

	@testVisible
	private void checkExceptions() {
		if (webserviceConfig == null) {
			throw new ExWebServiceCalloutException('Missing configuration');
		}
		if (requests == null) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}
		if (requests.isEmpty()) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}
	}
}
