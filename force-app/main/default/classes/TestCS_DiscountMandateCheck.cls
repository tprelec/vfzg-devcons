@isTest
public with sharing class TestCS_DiscountMandateCheck {
	@testSetup
	private static void setup() {
		Account account = CS_DataTest.createAccount('Test Account');
		insert account;

		Opportunity opp = CS_DataTest.createOpportunity(account, 'TestOpportunity', null);

		insert opp;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'TestProductBasket');
		cscfga__Product_Definition__c productDefinition = CS_DataTest.createProductDefinition('Business Mobile');
		insert productDefinition;

		cscfga__Product_Configuration__c productConfig = CS_DataTest.createProductConfiguration(productDefinition.Id, 'DMandate Test', basket.Id);
		productConfig.cscfga__Product_Family__c = 'Business Mobile';
		productConfig.New_Portfolio__c = true;
		productConfig.cscfga__Contract_Term__c = 12;
		productConfig.cscfga__Quantity__c = 21;
		productConfig.Commercial_Product__c = 'a8U1l0000008kNDEAY';
		productConfig.csdiscounts__manual_discounts__c = '{"discounts":[{"version":"3-0-0","recordType":"group","memberDiscounts":[{"version":"3-0-0","type":"percentage","source":"Manual 1","recordType":"single","discountCharge":"__PRODUCT__","description":"sad","chargeType":"recurring","amount":20.0000}],"evaluationOrder":"serial"}]}';
		insert productConfig;

		Discount_Mandate__c discountMandate = new Discount_Mandate__c();
		discountMandate.Max_Duration__c = 23;
		discountMandate.Min_Duration__c = 12;
		discountMandate.Max_Quantity__c = 24;
		discountMandate.Min_Quantity__c = 1;
		discountMandate.Value__c = 15.00;
		discountMandate.Commercial_Product__c = 'a8U1l0000008kNDEAY';
		insert discountMandate;
	}
	@isTest
	static void TestCS_DiscountMandateCheck() {
		List<cscfga__Product_Configuration__c> prodconfigs = [
			SELECT
				Id,
				Name,
				cscfga__Product_Basket__c,
				GroupDiscount__c,
				cscfga__Root_Configuration__c,
				cscfga__Product_Family__c,
				cscfga__Product_Definition__c,
				cscfga__Product_Definition__r.Name,
				csdiscounts__manual_discounts__c,
				cscfga__discounts__c,
				Add_On__c,
				Commercial_Product__c,
				Deal_type__c,
				cscfga__Contract_Term__c,
				New_Portfolio__c,
				cscfga__Quantity__c
			FROM cscfga__Product_Configuration__c
			WHERE Name = 'DMandate Test'
		];
		List<String> outputMessage = new List<String>();
		Test.startTest();
		CS_DiscountMandateCheck.checkDiscountMandate(prodconfigs, prodconfigs, outputMessage);
		Test.stopTest();
		System.assertEquals(
			'Discount mandate check failed for product DMandate Test - maximum allowed discount for given contract duration and quantity is 15.00%',
			outputMessage.toString().remove('(').remove(')')
		);
	}
}
