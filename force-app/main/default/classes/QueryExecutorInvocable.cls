public with sharing class QueryExecutorInvocable {
	@InvocableMethod(label='Query Executor' description='Execute complex queries and filter them by binding variables.')
	public static List<List<SObject>> queryRequest(List<RequestQuery> requests) {
		List<String> criteria1;
		List<String> criteria2;
		List<String> criteria3;
		for (requestQuery request : requests) {
			if (request.criteria1 != null) {
				criteria1 = request.criteria1;
			}
			if (request.criteria2 != null) {
				criteria2 = request.criteria2;
			}
			if (request.criteria3 != null) {
				criteria3 = request.criteria3;
			}
			List<List<SObject>> returnList = new List<List<SObject>>{ Database.query(request.query) };
			if (returnList != null && returnList.isEmpty()) {
				returnList = null;
			}
			return returnList;
		}
		return null;
	}

	public class RequestQuery {
		@InvocableVariable(label='Filter Data 1')
		public List<String> criteria1;

		@InvocableVariable(label='Filter Data 2')
		public List<String> criteria2;

		@InvocableVariable(label='Filter Data 3')
		public List<String> criteria3;

		@InvocableVariable(label='Query Text')
		public String query;
	}
}
