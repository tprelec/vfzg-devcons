@isTest
private class ProductConfigurationTriggerHandlerTest {
   
    @isTest    
    static void testProductConfigurationTriggerHandler()
    {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;

        Test.startTest();
        System.runAs (simpleUser) {
            Account account = LG_GeneralTest.CreateAccount('Account', '12345678', 'Vodafone', true);
            
            Opportunity opp = LG_GeneralTest.CreateOpportunity(account, false);
            opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SOHO_SMALL').getRecordTypeId();
            insert opp;
            
            cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', account, null, opp, false);
            
            basket.LG_CreatedFrom__c = 'Tablet';
            insert basket;
            
            cscfga__Product_Category__c prodCategory = LG_GeneralTest.createProductCategory('Test Category', true);
            
            cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('Phone Numbers', false);	
            
            prodDef.cscfga__Product_Category__c = prodCategory.Id;
            insert prodDef;
    
            Opportunity opp2 = LG_GeneralTest.CreateOpportunity(account, false);
            opp2.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SOHO_SMALL').getRecordTypeId();
            insert opp2;
            
            cscfga__Product_Basket__c basket2 = LG_GeneralTest.createProductBasket('Basket2', account, null, opp2, false);
            
            basket2.LG_CreatedFrom__c = 'NotTablet';
            insert basket2;
    
            cscfga__Product_Configuration__c prodConf = LG_GeneralTest.createProductConfiguration('Phone Numbers 1', 3, basket, prodDef, true);
            cscfga__Product_Configuration__c prodConf2 = LG_GeneralTest.createProductConfiguration('Phone Numbers 2', 3, basket2, prodDef, true);
            prodConf2.LG_MarketSegment__c = 'SoHo2';
            update prodConf2;
            
            prodConf.cscfga__Configuration_Status__c = 'Incomplete';
            prodConf.LG_ChangeType__c = 'Test';
            update prodConf;

            cscfga__Product_Configuration__c testCfg = [SELECT LG_ChangeType__c, cscfga__Configuration_Status__c FROM cscfga__Product_Configuration__c WHERE Id = :prodConf.Id];
            
            System.assertEquals('Test', testCfg.LG_ChangeType__c, 'Change Type should be Test before update');
            System.assertEquals('Incomplete', testCfg.cscfga__Configuration_Status__c, 'Configuration Status should be Test before update');
            
            List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>();
            pcList.add(prodConf);
            delete pcList;

        }

        Test.StopTest();
    }  

    @isTest    
    static void testPendingUsage()
    {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;

        Test.startTest();
        System.runAs (simpleUser) {
            Account account = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'AccountTest',
                Type = 'End CustomerTest'
            );
            upsert account;

            Contact contact = new Contact(
                AccountId = account.id,
                LastName = 'LastTest',
                FirstName = 'FirstTest',
                Contact_Role__c = 'Consultant',
                
                Email = 'test1@vf.com'   
            );
            upsert contact;
            
            Opportunity opp = CS_DataTest.createVodafoneOpportunity(account, 'Test Opp', simpleUser.Id);
            insert opp;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'B1');
            basket.SetPendingUsage__c = true;
            insert basket;
            
            cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinitionRegular('Mobile CTN profile');	
            insert prodDef;        
    
            cscfga__Product_Configuration__c prodConf = CS_DataTest.createProductConfiguration(prodDef.Id, 'Mobile CTN profile', basket.Id);
            insert prodConf;

            prodConf.cscfga__Configuration_Status__c = 'Requires update';
            update prodConf;

            cscfga__Product_Basket__c testBasket = [SELECT Id, cscfga__Basket_Status__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
            System.assertEquals('Pending usage', testBasket.cscfga__Basket_Status__c);
        }

        Test.StopTest();
    }  

    @isTest    
    static void testRevalidateBasket()
    {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;

        Test.startTest();
        System.runAs (simpleUser) {
            Account account = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'AccountTest',
                Type = 'End CustomerTest'
            );
            upsert account;

            Contact contact = new Contact(
                AccountId = account.id,
                LastName = 'LastTest',
                FirstName = 'FirstTest',
                Contact_Role__c = 'Consultant',
                
                Email = 'test1@vf.com'   
            );
            upsert contact;
            
            Opportunity opp = CS_DataTest.createVodafoneOpportunity(account, 'Test Opp', simpleUser.Id);
            insert opp;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'B1');
            basket.cscfga__Basket_Status__c = 'Incomplete';
            insert basket;
            
            cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinitionRegular('Mobile CTN profile');	
            insert prodDef;        
    
            cscfga__Product_Configuration__c prodConf1 = CS_DataTest.createProductConfiguration(prodDef.Id, 'Mobile CTN profile 1', basket.Id);
            insert prodConf1;

            cscfga__Product_Configuration__c prodConf2 = CS_DataTest.createProductConfiguration(prodDef.Id, 'Mobile CTN profile 2', basket.Id);
            insert prodConf2;

            prodConf1.cscfga__Configuration_Status__c = 'Valid';
            update prodConf1;
            prodConf2.cscfga__Configuration_Status__c = 'Valid';
            update prodConf2;

            delete prodConf1;

            cscfga__Product_Basket__c testBasket = [SELECT Id, cscfga__Basket_Status__c FROM cscfga__Product_Basket__c WHERE Id = :basket.Id];
            System.assertEquals('Valid', testBasket.cscfga__Basket_Status__c);
        }

        Test.StopTest();
    }  

}