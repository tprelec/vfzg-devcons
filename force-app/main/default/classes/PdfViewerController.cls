public with sharing class PdfViewerController {
	@AuraEnabled
	public static List<PdfFile> getContent(List<Id> fileIds) {
		Set<Id> attachmentIds = new Set<Id>();
		Set<Id> contentVersionIds = new Set<Id>();

		for (Id fileId : fileIds) {
			if (fileId.getSObjectType().getDescribe().getName() == 'Attachment') {
				attachmentIds.add(fileId);
			} else if (
				fileId.getSObjectType().getDescribe().getName() == 'ContentVersion'
			) {
				contentVersionIds.add(fileId);
			}
		}
		List<PdfFile> results = new List<PdfFile>();

		for (Attachment a : [SELECT Id, Name, Body FROM Attachment WHERE Id IN :attachmentIds]) {
			results.add(new PdfFile(a));
		}

		for (ContentVersion cv : [
			SELECT Id, Title, VersionData
			FROM ContentVersion
			WHERE Id IN :contentVersionIds
		]) {
			results.add(new PdfFile(cv));
		}

		return results;
	}

	public class PdfFile {
		@AuraEnabled
		public String name { get; set; }
		@AuraEnabled
		public String data { get; set; }

		public PdfFile(Attachment att) {
			this(att.Name, att.Body);
		}

		public PdfFile(ContentVersion cv) {
			this(cv.Title, cv.VersionData);
		}

		public PdfFile(String name, Blob content) {
			this.name = name;
			this.data = EncodingUtil.base64Encode(content);
		}
	}
}