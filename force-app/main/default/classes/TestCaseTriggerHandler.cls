@isTest
private class TestCaseTriggerHandler {
	private static Account acc;
	private static Contact con;
	private static Opportunity opp;
	private static VF_Contract__c vfc;
	private static agf__ADM_Scrum_Team__c team;
	private static Case c;
	private static Entitlement ent;

	private static void createTestData() {
		acc = TestUtils.createAccount(GeneralUtils.currentUser);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		con = TestUtils.createContact(acc);
		con.Email = '123456abcde@mail.com';
		update con;
		vfc = TestUtils.createVFContract(acc, opp);
		vfc.Implementation_Manager__c = Userinfo.getUserId();
		update vfc;
		team = TestUtils.createScrumTeams(1, true)[0];
		TestUtils.createThemes(1, true, GeneralUtils.currentUser.Id);

		insert new agf__ADM_Impact__c(Name = 'Malfunctioning');
		insert new agf__ADM_Frequency__c(Name = 'Always');
		insert new agf__ADM_Build__c(Name = 'Production');
		insert new agf__ADM_Sprint__c(agf__Start_Date__c = Date.today(), agf__End_Date__c = Date.today().addDays(1), agf__Scrum_Team__c = team.Id);
		Slaprocess stormSLA = [SELECT Id, NAME FROM Slaprocess WHERE IsActive = TRUE AND name = 'CST SLA Test Class' LIMIT 1];
		System.debug('stormSLA > ' + stormSLA);
		ent = new Entitlement(
			Name = 'EntitlementTest',
			AccountId = acc.Id,
			Type = 'CST_DC01',
			StartDate = System.today(),
			SlaProcessId = stormSLA.Id
		);
		insert ent;
		System.debug('ent > ' + ent);

		List<agf__ADM_Product_Tag__c> productTags = new List<agf__ADM_Product_Tag__c>();

		productTags.add(new agf__ADM_Product_Tag__c(Name = 'Undefined', agf__Team__c = team.Id));
		productTags.add(new agf__ADM_Product_Tag__c(Name = 'Dev SFDC', agf__Team__c = team.Id));

		insert productTags;

		TestUtils.createSync('agf__ADM_Work__c -> Case', 'agf__Subject__c', 'Subject');
		TestUtils.createSync('Case -> agf__ADM_Work__c', 'Subject', 'agf__Subject__c');
		TestUtils.createSync('Case -> agf__ADM_Work__c', 'ContactId', 'Case_Contact__c');
	}

	@isTest
	static void testFillAccount() {
		createTestData();

		Test.startTest();

		c = new Case(Opportunity__c = opp.Id, CST_Case_is_with_Team__c = null);
		c.Status = 'New';
		insert c;

		c.Contract_VF__c = vfc.Id;
		update c;

		Test.stopTest();

		Case resultCase = [SELECT Account__c FROM Case WHERE Id = :c.Id];
		System.assertEquals(acc.Id, resultCase.Account__c, 'Case Account should be the created account.');
	}

	@isTest
	static void testFillContact() {
		createTestData();
		con.Userid__c = UserInfo.getUserId();
		update con;

		Test.startTest();

		c = new Case(CST_Case_is_with_Team__c = null);
		insert c;

		Test.stopTest();

		Case resultCase = [SELECT ContactId FROM Case WHERE Id = :c.Id];
		System.assertEquals(con.Id, resultCase.ContactId, 'Case Contact should be the created contact.');
	}

	@isTest
	static void testCheckOpenChildCasesOnInsert() {
		createTestData();

		Case parentCase = new Case(Open_subcases__c = false, CST_Case_is_with_Team__c = null);
		insert parentCase;

		Test.startTest();

		c = new Case(ParentId = parentCase.Id, CST_Case_is_with_Team__c = null);
		insert c;

		Test.stopTest();

		Case pc = [SELECT Open_subcases__c FROM Case WHERE Id = :parentCase.Id];
		System.assertEquals(true, pc.Open_subcases__c, 'Parent Case Open_subcases__c should be true.');
	}

	@isTest
	static void testCheckOpenChildCasesOnDelete() {
		createTestData();

		Case parentCase = new Case(Open_subcases__c = true, CST_Case_is_with_Team__c = null);
		insert parentCase;

		Test.startTest();

		c = new Case(ParentId = parentCase.Id, CST_Case_is_with_Team__c = null);
		insert c;

		delete c;

		Test.stopTest();

		Case pc = [SELECT Open_subcases__c FROM Case WHERE Id = :parentCase.Id];
		System.assertEquals(false, pc.Open_subcases__c, 'Parent Case Open_subcases__c should be false.');
	}

	@isTest
	static void testCheckOpenChildCasesOnUpdate() {
		createTestData();

		Case parentCase = new Case(Open_subcases__c = true, CST_Case_is_with_Team__c = null);
		insert parentCase;

		Test.startTest();

		c = new Case(ParentId = parentCase.Id, Status = 'New', CST_Case_is_with_Team__c = null);
		insert c;

		c.Status = 'Closed';
		update c;

		Test.stopTest();

		Case pc = [SELECT Open_subcases__c FROM Case WHERE Id = :parentCase.Id];
		System.assertEquals(false, pc.Open_subcases__c, 'Parent Case Open_subcases__c should be false.');
	}

	@isTest
	static void testCheckOpenChildCasesOnUpdateUnclosed() {
		createTestData();

		Case parentCase = new Case(Open_subcases__c = false, CST_Case_is_with_Team__c = null);
		insert parentCase;

		Test.startTest();

		c = new Case(ParentId = parentCase.Id, Status = 'Closed', CST_Case_is_with_Team__c = null);
		insert c;

		c.Status = 'New';
		update c;

		Test.stopTest();

		Case pc = [SELECT Open_subcases__c FROM Case WHERE Id = :parentCase.Id];
		System.assertEquals(true, pc.Open_subcases__c, 'Parent Case Open_subcases__c should be true.');
	}

	@isTest
	static void testSyncToAgileAcceleratorCreateUserStory() {
		createTestData();

		Test.startTest();

		c = new Case(Sprint_Case__c = true, Subject = 'Test', Internal_comments__c = 'Test', CST_Case_is_with_Team__c = null);
		insert c;

		Test.stopTest();

		List<agf__ADM_Work__c> resultWork = [SELECT Id FROM agf__ADM_Work__c];
		System.assertEquals(1, resultWork.size(), 'User story should be created.');
	}

	@isTest
	static void testSyncToAgileAcceleratorCreateBug() {
		createTestData();

		Test.startTest();

		c = new Case(Blocking_Issue__c = true, Subject = 'Test', Internal_comments__c = 'Test', CST_Case_is_with_Team__c = null);
		insert c;

		Test.stopTest();

		List<agf__ADM_Work__c> resultWork = [SELECT Id FROM agf__ADM_Work__c];
		System.assertEquals(1, resultWork.size(), 'Bug should be created.');

		List<agf__ADM_Task__c> resultTasks = [SELECT Id FROM agf__ADM_Task__c];
		System.assertEquals(1, resultTasks.size(), 'Task should be created.');
	}

	@isTest
	static void testMobileFlowTasks() {
		createTestData();

		c = new Case(OwnerId = UserInfo.getUserId(), CST_Case_is_with_Team__c = null);
		insert c;

		Task t = new Task(WhatId = c.Id, Status = 'New');
		insert t;

		Test.startTest();

		User admin = TestUtils.createAdministrator();

		c.OwnerId = admin.Id;
		c.Mobile_Flow_Tasks_Created__c = false;
		update c;

		c.OwnerId = UserInfo.getUserId();
		c.Mobile_Flow_Tasks_Created__c = true;
		update c;

		Test.stopTest();

		Task resultTask = [SELECT OwnerId FROM Task WHERE Id = :t.Id];
		System.assertEquals(UserInfo.getUserId(), resultTask.OwnerId, 'Task owner should be the case owner.');
	}

	@isTest
	static void testUpdateRoleCreatorField() {
		createTestData();
		opp.RoleCreateOrChangeOwner__c = 'Commercial Industries Account Manager';
		update opp;

		Test.startTest();

		c = new Case(Contract_VF__c = vfc.Id, CST_Case_is_with_Team__c = null);
		insert c;

		Test.stopTest();

		Case resultCase = [SELECT RoleCreatorChangeOwnerCreated__c FROM Case WHERE Id = :c.Id];
		System.assertEquals(
			'Enterprise & Hospitality / Service & Industry',
			resultCase.RoleCreatorChangeOwnerCreated__c,
			'Case RoleCreatorChangeOwnerCreated__c should be Enterprise & Hospitality / Service & Industry.'
		);
	}

	@isTest
	static void testSetFinalQualityCheckScore() {
		createTestData();

		Id fqcRecTypeId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName = 'CS_MF_Final_Quality_Check'][0].Id;

		c = new Case(
			Contract_VF__c = vfc.Id,
			RecordTypeId = fqcRecTypeId,
			FQC_Quality_Score__c = 5,
			Responsible_Quality_Officer__c = UserInfo.getUserId(),
			FQC_Result__c = 'Passed',
			CST_Case_is_with_Team__c = null
		);
		insert c;

		Test.startTest();

		c.Status = 'Closed';
		update c;

		Test.stopTest();

		VF_Contract__c resultContract = [SELECT FQC_Quality_Score__c FROM VF_Contract__c WHERE Id = :vfc.Id];
		System.assertEquals(5, resultContract.FQC_Quality_Score__c, 'Conract FQC_Quality_Score__c should be 5.');
	}

	@isTest
	static void testAssociateEntitlementProcess() {
		createTestData();
		RecordType questionRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CST_Question'];
		Id questionRecordTypeId = (questionRecordType != null) ? questionRecordType.Id : null;

		Test.startTest();

		c = new Case(AccountId = acc.Id, CST_Product__c = 'CST_DC01', RecordTypeId = questionRecordTypeId, ContactId = con.id);
		insert c;

		Test.stopTest();

		Case case2verify = [SELECT id, EntitlementId FROM Case WHERE Id = :c.Id];
		System.assertEquals(case2verify.EntitlementId, ent.id, 'Case should have an entitlement associated.');
	}

	@isTest
	static void completeMilestonesTest() {
		createTestData();
		RecordType questionRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CST_Question'];
		Id questionRecordTypeId = (questionRecordType != null) ? questionRecordType.Id : null;
		c = new Case(AccountId = acc.Id, Priority = 'P3', Status = 'Waiting for Customer Validation', ContactId = con.id);
		insert c;
		Test.startTest();
		c.RecordTypeId = questionRecordTypeId;
		c.CST_Product__c = 'CST_DC01';
		update c;
		c.Status = 'Closed';
		c.Reason = 'Solved Immediately';
		update c;
		Test.stopTest();

		CaseMilestone milestone = [SELECT id, caseId, IsCompleted, CompletionDate FROM CaseMilestone WHERE caseId = :c.Id LIMIT 1];
		System.assertEquals(true, milestone.IsCompleted, 'CaseMilestone should be completed.');
	}

	@isTest
	static void testStopMilestoneClock() {
		createTestData();
		RecordType questionRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CST_Question'];
		Id questionRecordTypeId = (questionRecordType != null) ? questionRecordType.Id : null;

		c = new Case(AccountId = acc.Id, CST_Product__c = 'CST_DC01', RecordTypeId = questionRecordTypeId, ContactId = con.id);
		insert c;

		Test.startTest();
		updateCaseSequentially(c);
		Test.stopTest();
		Case case2verify = [SELECT id, IsStopped FROM Case WHERE Id = :c.Id];
		System.assertEquals(case2verify.IsStopped, false, 'Case Entitlement should be running.');
	}

	private static void updateCaseSequentially(Case c) {
		c.Status = 'Assessing';
		update c;

		c.Status = 'In progress';
		c.CST_Sub_Status__c = 'Assigned';
		c.CST_Case_is_with_Team__c = 'Internal - Advisor';
		update c;

		c.Status = 'Waiting for Customer Information';
		update c;

		c.Status = 'In progress';
		update c;
	}
}
