public with sharing class IBANService {
	public enum BankNumberValidationResult {
		IBAN_OK,
		IBAN_INVALID_FORMAT,
		IBAN_INVALID_LENGTH,
		IBAN_SANITY_CHECK_FAILED,
		IBAN_WRONG_COUNTRY_CHECKSUM
	}

	public static final Pattern IBAN_PATTERN = Pattern.compile('^[A-Z]{2}[0-9]{2}[A-Z0-9]+$');

	public static void validateIBAN(List<csconta__Billing_Account__c> data) {
		Map<String, Integer> ibanValues = IbanMap();

		for (csconta__Billing_Account__c o : data) {
			BankNumberValidationResult validationResult = checkIban(
				o.LG_BankAccountNumberIBAN__c,
				ibanValues
			);

			if (validationResult != BankNumberValidationResult.IBAN_OK) {
				o.LG_BankAccountNumberIBAN__c.addError(getErrorMessage(validationResult));
			}
		}
	}

	public static String getErrorMessage(BankNumberValidationResult validationResult) {
		if (validationResult == BankNumberValidationResult.IBAN_INVALID_FORMAT) {
			return Label.LG_IBANInvalidFormat;
		} else if (validationResult == BankNumberValidationResult.IBAN_INVALID_LENGTH) {
			return Label.LG_IBANInvalidLength;
		} else if (validationResult == BankNumberValidationResult.IBAN_SANITY_CHECK_FAILED) {
			return Label.LG_IBANSanityCheckFailed;
		} else if (validationResult == BankNumberValidationResult.IBAN_WRONG_COUNTRY_CHECKSUM) {
			return Label.LG_IBANWrongCountryChecksum;
		}
		return 'Wrong IBAN code.';
	}

	@TestVisible
	private static Map<String, Integer> ibanMap() {
		Map<String, Integer> finalMap = new Map<String, Integer>();
		String alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		Integer counter = 0;

		for (String s : alphabet.split('')) {
			finalMap.put(s, counter);
			counter++;
		}

		return finalMap;
	}
	@TestVisible
	private static BankNumberValidationResult checkIban(
		String iban,
		Map<String, Integer> ibanValues
	) {
		if (String.isEmpty(iban)) {
			return BankNumberValidationResult.IBAN_OK;
		}

		String code = iban.replaceAll('(\\s+)', '').toUppercase();

		if (code.length() < 5 || !IBAN_PATTERN.matcher(code).matches()) {
			return BankNumberValidationResult.IBAN_INVALID_FORMAT;
		}

		String countryCode = code.substring(0, 2);
		LG_IBAN__c ibanCountrySpecificConfiguration = LG_IBAN__c.getValues(countryCode);

		if (
			(ibanCountrySpecificConfiguration != null) &&
			(ibanCountrySpecificConfiguration.LG_Length__c != code.length())
		) {
			return BankNumberValidationResult.IBAN_INVALID_LENGTH;
		}

		Long total = ibanSenityCheck(code, ibanValues);

		return ((Math.mod(total, 97) == 1)
			? BankNumberValidationResult.IBAN_OK
			: BankNumberValidationResult.IBAN_SANITY_CHECK_FAILED);
	}

	@TestVisible
	private static Long ibanSenityCheck(String code, Map<String, Integer> ibanValues) {
		String reformattedCode = code.substring(4) + code.substring(0, 4);
		Long total = 0;
		Long max = 999999999;
		Integer charValue;

		for (String s : reformattedCode.split('')) {
			charValue = ibanValues.get(s);
			total = (charValue > 9 ? total * 100 : total * 10) + charValue;
			
			if (total > max) {
				total = Math.mod(total, 97);
			}
		}
		return total;
	}
}