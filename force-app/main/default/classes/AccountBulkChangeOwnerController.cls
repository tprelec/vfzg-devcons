/**
 * @description         Enables the bulk update of accounts General Owner 
 * @author              Gerhard Newman
 */
 public class AccountBulkChangeOwnerController {
    private final Account acct;  

    private Boolean isRestricted = true;

    ApexPages.StandardSetController con;

    public AccountBulkChangeOwnerController(ApexPages.StandardSetController controller) {
        // Default the claimed__c value to true to enable the trigger logic to transfer ownership correctly
        this.acct = (Account)controller.getRecord();
        this.acct.claimed__c=true;
        
        con = controller;
    }

    public integer getMySelectedSize() {
        return con.getSelected().size();
    }

    public Boolean getIsRestricted(){
        // Get the profile of the current user
        // This page is restricted to System admins only
        Profile currentProfile = [Select Name from Profile where Id =: UserInfo.getProfileId()];
        if (currentProfile.Name != 'System Administrator') {
            return true;
        } else {
            return false;
        }
    }
}