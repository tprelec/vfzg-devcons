/*
	Author: Rahul Sharma
	Description: Order Overview API, Salesforce commerce cloud uses this API to retrieve the open orders related to an account
	Jira ticket: COM-3241
*/

@RestResource(urlMapping='/orders/getOverview')
global with sharing class SFCC_OrderOverviewApi {
	private static final String DATE_TIME_FORMAT = 'yyyy-MM-dd\'T\'HH:mm:ss\'Z\'';
	private static final String DATE_FORMAT = 'yyyy-MM-dd';
	@TestVisible
	private static final Integer SUCCESS_CODE_200 = 200;
	@TestVisible
	private static final Integer SUCCESS_CODE_400 = 400;
	@TestVisible
	private static final String ORDER_STATUS_IN_PROGRESS = 'in-progress';
	@TestVisible
	private static final String ORDER_STATUS_CANCELLED = 'cancelled';
	@TestVisible
	private static final String ORDER_STATUS_COMPLETED = 'completed';
	@TestVisible
	private static final String ORDER_STATUS_ERROR = 'error';

	private static final Set<String> SET_DEFAULT_FILTERED_STATUS = new Set<String>{
		ORDER_STATUS_IN_PROGRESS,
		ORDER_STATUS_CANCELLED,
		ORDER_STATUS_COMPLETED,
		ORDER_STATUS_ERROR
	};

	/*
	 * HTTP post method to get CS and legacy orders based on accountId passed via RestContext.request
	 * status, startDate and endDate are optional filter parameters
	 */
	@HttpPost
	global static void getOverview() {
		RequestWrapper request = parseRequestData();

		// in case of bad request, send appropriate status code
		if (request == null) {
			addDataToResponse(SUCCESS_CODE_400, new ResponseWrapper(System.Label.SFCC_OrderApi_UnableToParseRequestMessage));
			return;
		}

		try {
			// send error message when accountId is invalid
			if (
				String.isEmpty(request.accountId) ||
				!(request.accountId instanceof Id) ||
				Id.valueOf(request.accountId).getSObjectType() != Account.SObjectType
			) {
				addDataToResponse(SUCCESS_CODE_400, new ResponseWrapper(System.label.SFCC_OrderApi_InvalidAccountIdMessage));
				return;
			}

			// get orders based on request parameters and send in respose body
			List<OrderWrapper> orders = getOrders(request);
			ResponseWrapper responseWrapper = new ResponseWrapper(orders);
			addDataToResponse(SUCCESS_CODE_200, responseWrapper);
		} catch (Exception objException) {
			LoggerService.log(LoggingLevel.DEBUG, '@@@@objException.getStackTraceString(): ' + objException.getStackTraceString());
			addDataToResponse(SUCCESS_CODE_400, new ResponseWrapper(System.label.SFCC_OrderApi_UnhandledExceptionMessage));
		}
	}

	private static RequestWrapper parseRequestData() {
		try {
			String requestString = RestContext.request.requestBody.toString();
			LoggerService.log(LoggingLevel.DEBUG, '@@@@requestString: ' + requestString);
			return (RequestWrapper) JSON.deserialize(requestString, RequestWrapper.class);
		} catch (Exception objException) {
			return null;
		}
	}

	/*
	 * Add and send status code and body to RestContext.response
	 */
	private static void addDataToResponse(Integer statusCode, ResponseWrapper responseWrapper) {
		String data = JSON.serialize(responseWrapper);

		if (statusCode != SUCCESS_CODE_200) {
			LoggerService.log(LoggingLevel.ERROR, RestContext.request.requestBody.toString() + ' - ' + data);
		}

		RestContext.response.statusCode = statusCode;
		RestContext.response.responseBody = Blob.valueOf(data);
	}

	/*
	 * get orders response based on request parameters
	 */
	private static List<OrderWrapper> getOrders(RequestWrapper request) {
		List<OrderWrapper> allOrders = new List<OrderWrapper>();
		List<OrderWrapper> legacyOrders = getLegacyOrders(request);
		List<OrderWrapper> csOrders = getCSOrders(request);
		// combine legacy and CS orders and perform custom sort (based on orderDate: look at compareTo in OrderWrapper for the implementation)
		allOrders.addAll(legacyOrders);
		allOrders.addAll(csOrders);
		allOrders.sort();
		return allOrders;
	}

	/*
	 * return response for legacy orders
	 */
	private static List<OrderWrapper> getLegacyOrders(RequestWrapper request) {
		List<OrderWrapper> legacyOrders = new List<OrderWrapper>();
		String legacyOrdersSoql = getLegacyOrdersSoql(request);
		LoggerService.log(LoggingLevel.DEBUG, '@@@@legacyOrdersSoql: ' + legacyOrdersSoql);
		for (Order__c order : Database.query(legacyOrdersSoql)) {
			legacyOrders.add(new OrderWrapper(order));
		}
		return legacyOrders;
	}

	/*
	 * return response for CS orders
	 */
	private static List<OrderWrapper> getCSOrders(RequestWrapper request) {
		List<OrderWrapper> csOrders = new List<OrderWrapper>();
		String csOrdersSoql = getCsOrdersSoql(request);
		LoggerService.log(LoggingLevel.DEBUG, '@@@@csOrdersSoql: ' + csOrdersSoql);
		for (csord__Order__c order : Database.query(csOrdersSoql)) {
			csOrders.add(new OrderWrapper(order));
		}
		return csOrders;
	}

	/*
	 * using request parameters, prepare a SOQL for legacy orders
	 */
	private static String getLegacyOrdersSoql(RequestWrapper request) {
		List<String> queryFields = new List<String>{
			'Id',
			'Name',
			'Account__c',
			'SFCC_Status__c',
			'Order_Date__c',
			'VF_Contract__r.Opportunity__c',
			'VF_Contract__r.Opportunity__r.Channel__c'
		};

		List<String> expr = new List<String>();
		expr.add('SELECT ' + String.join(queryFields, ', '));
		expr.add('FROM Order__c');
		expr.add('WHERE VF_Contract__r.Opportunity__c != null AND');
		expr.add('Account__c = \'' + String.escapeSingleQuotes(request.accountId) + '\'');

		// optional filter for status
		expr.add('AND SFCC_Status__c IN ' + getStatusStringFilter(request.status));

		// optional filter on Order_Date__c between startDate and endDate
		String dateClauseString = getDateClauseString('Order_Date__c', request.startDate, request.endDate);
		if (String.isNotEmpty(dateClauseString)) {
			expr.add('AND ' + dateClauseString);
		}

		expr.add('ORDER BY CreatedDate DESC');

		String queryString = String.join(expr, ' ');
		return queryString;
	}

	/*
	 * using request parameters, prepare a SOQL for CS orders
	 */
	private static String getCsOrdersSoql(RequestWrapper request) {
		List<String> queryFields = new List<String>{
			'Id',
			'csord__Order_Number__c',
			'Ecommerce_Order_Reference_Id__c',
			'csord__Account__c',
			'SFCC_Status__c',
			'CreatedDate',
			'csordtelcoa__Opportunity__c',
			'csordtelcoa__Opportunity__r.Channel__c'
		};

		List<String> expr = new List<String>();
		expr.add('SELECT ' + String.join(queryFields, ', '));
		expr.add('FROM csord__Order__c');
		expr.add('WHERE csordtelcoa__Opportunity__c != null AND');
		expr.add('csord__Account__c = \'' + request.accountId + '\'');

		// optional filter for status
		expr.add('AND SFCC_Status__c IN ' + getStatusStringFilter(request.status));

		// optional filter on createdDate between startDate and endDate
		String dateClauseString = getDateClauseString('CreatedDate', request.startDate, request.endDate);
		if (String.isNotEmpty(dateClauseString)) {
			expr.add('AND ' + dateClauseString);
		}

		expr.add('ORDER BY CreatedDate DESC');

		String queryString = String.join(expr, ' ');
		return queryString;
	}

	/*
	 * prepare status value that will be used in where clause to filter status
	 * @return		comma separated status parameters, example: ('cancelled', 'in-progress')
	 */
	private static String getStatusStringFilter(List<String> requestStatus) {
		// filter out request.status list with allowed statuses
		List<String> filteredStatuses = new List<String>();
		if (requestStatus != null) {
			for (String value : requestStatus) {
				if (SET_DEFAULT_FILTERED_STATUS.contains(value)) {
					filteredStatuses.add(value);
				}
			}
		}
		String statusCsvString = arrayToCsvFilterValue(filteredStatuses);
		if (String.isNotEmpty(statusCsvString)) {
			// return the allowed filtered statuses based on default filter
			return statusCsvString;
		} else {
			// return the default filter when there are no allowed filtered statuses
			List<String> valueList = new List<String>();
			valueList.addAll(SET_DEFAULT_FILTERED_STATUS);
			return arrayToCsvFilterValue(valueList);
		}
	}

	/*
	 * convert array of string to comma separated CSV values, example: ('value 1', 'value 2')
	 */
	private static String arrayToCsvFilterValue(List<String> values) {
		if (values.isEmpty()) {
			return '';
		}
		List<String> valueList = new List<String>();
		for (String value : values) {
			valueList.add('\'' + String.escapeSingleQuotes(value) + '\'');
		}
		return '(' + String.join(valueList, ', ') + ')';
	}

	/*
	 * prepare the where clause filter based on start and end date respectively
	 */
	private static String getDateClauseString(String dateFieldApiName, DateTime startDate, DateTime endDate) {
		String dateClauseString;
		if (startDate != null || endDate != null) {
			List<String> orderDateClauseList = new List<String>();
			if (startDate != null) {
				orderDateClauseList.add(dateFieldApiName + ' >= ' + getFormattedDateString(dateFieldApiName, startDate));
			}
			if (endDate != null) {
				orderDateClauseList.add(dateFieldApiName + ' <= ' + getFormattedDateString(dateFieldApiName, endDate));
			}
			dateClauseString = String.join(orderDateClauseList, ' AND ');
		}
		return dateClauseString;
	}

	// formats the date time value to a string
	private static String getFormattedDateString(String dateFieldApiName, DateTime dateParam) {
		if (dateFieldApiName == 'CreatedDate') {
			return dateParam.formatGmt(DATE_TIME_FORMAT);
		}
		return dateParam.formatGmt(DATE_FORMAT);
	}

	public class RequestWrapper {
		public String accountId;
		public List<String> status;
		public DateTime startDate;
		public DateTime endDate;

		@SuppressWarnings('PMD.ExcessiveParameterList') // parameters are defined by API
		public RequestWrapper(String accountId, List<String> status, DateTime startDate, DateTime endDate) {
			this.accountId = accountId;
			this.status = status;
			this.startDate = startDate;
			this.endDate = endDate;
		}
	}

	public class ResponseWrapper {
		public List<OrderWrapper> orders;
		public String errorMessage;
		public ResponseWrapper(List<OrderWrapper> orders) {
			this.orders = orders;
		}
		public ResponseWrapper(String errorMessage) {
			this.errorMessage = errorMessage;
		}
	}

	global class OrderWrapper implements Comparable {
		public String orderId;
		public String orderNumber;
		public String ecomReference;
		public String accountId;
		public String orderStatus;
		public Datetime orderDate;
		public String channel;

		public OrderWrapper(Order__c order) {
			this.orderId = order.Id;
			this.orderNumber = order.Name;
			this.ecomReference = null;
			this.accountId = order.Account__c;
			this.orderStatus = order.SFCC_Status__c;
			this.orderDate = order.Order_Date__c;
			Opportunity opportunity = order?.VF_Contract__r?.Opportunity__r;
			this.channel = opportunity?.Channel__c;
		}

		public OrderWrapper(csord__Order__c order) {
			this.orderId = order.Id;
			this.orderNumber = order.csord__Order_Number__c;
			this.ecomReference = order.Ecommerce_Order_Reference_Id__c;
			this.accountId = order.csord__Account__c;
			this.orderStatus = order.SFCC_Status__c;
			this.orderDate = order.CreatedDate;
			Opportunity opportunity = order?.csordtelcoa__Opportunity__r;
			this.channel = opportunity?.Channel__c;
		}

		public Integer compareTo(Object compareTo) {
			// Cast argument to OrderWrapper
			OrderWrapper orderWrapper = (OrderWrapper) compareTo;

			if (orderDate > orderWrapper.orderDate) {
				return -1;
			} else if (orderDate < orderWrapper.orderDate) {
				return 1;
			}
			// The return value of 0 indicates that both elements are equal.
			return 0;
		}
	}
}
