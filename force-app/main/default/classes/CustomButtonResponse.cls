public with sharing class CustomButtonResponse {

    public String status {get; set;}
    public String title {get; set;}
    public String text {get; set;}
    public Integer timeout {get; set;}

    public CustomButtonResponse(String status, String title, String text) {
        this.status = status;
        this.title = title;
        this.text = text;
        if (this.status == 'error') {
            this.timeout = 1000;
        }
    }
}