/*
 * @author Rahul Sharma
 *
 * @description Helper class for performing Billing information custom button's functionality from product basket
 */
public with sharing class BasketBillingInformationHelper {
	public static String validateFieldsOnOpportunity(Opportunity opportunity) {
		try {
			checkIfBillingFieldsArePopulated(opportunity);
			checkIfBillingFieldsAreValid(opportunity);

			// return empty message when billing information is valid
			return null;
		} catch (BasketBillingInformationException basketBillingInformationException) {
			return basketBillingInformationException.getMessage();
		} catch (Exception objEx) {
			LoggerService.log(objEx);
			return System.Label.BasketBillingInformation_GenericExceptionMessage;
		}
	}

	public static void checkIfBillingFieldsArePopulated(Opportunity opportunity) {
		if (
			String.isEmpty(opportunity.BAN__c) ||
			String.isEmpty(opportunity.Financial_Account__c) ||
			String.isEmpty(opportunity.Billing_Arrangement__c)
		) {
			throw new BasketBillingInformationException(System.Label.BasketBillingInformation_FieldsAreRequired);
		}
	}

	public static void checkIfBillingFieldsAreValid(Opportunity opportunity) {
		if (
			!OrderBillingValidation.getAllErrors(opportunity.BAN__c, opportunity.Id).isEmpty() ||
			!OrderBillingValidation.getAllErrors(opportunity.Financial_Account__c, opportunity.Id).isEmpty() ||
			!OrderBillingValidation.getAllErrors(opportunity.Billing_Arrangement__c, opportunity.Id).isEmpty()
		) {
			throw new BasketBillingInformationException(System.Label.BasketBillingInformation_FieldsMustBeValid);
		}
	}

	public static void copyFieldsFromOpportunityToPCs(Id basketId, List<cscfga__Product_Configuration__c> allProductConfigurations) {
		cscfga__Product_Basket__c basket = [
			SELECT Id, cscfga__Opportunity__r.BAN__c, cscfga__Opportunity__r.Financial_Account__c, cscfga__Opportunity__r.Billing_Arrangement__c
			FROM cscfga__Product_Basket__c
			WHERE Id = :basketId
		];
		Opportunity opportunity = basket.cscfga__Opportunity__r;

		List<cscfga__Product_Configuration__c> productConfigurations = new List<cscfga__Product_Configuration__c>();

		for (cscfga__Product_Configuration__c productConfiguration : allProductConfigurations) {
			if (
				opportunity != null &&
				(productConfiguration.BAN_Information__c != opportunity.BAN__c ||
				productConfiguration.Financial_Account__c != opportunity.Financial_Account__c ||
				productConfiguration.Billing_Arrangement__c != opportunity.Billing_Arrangement__c)
			) {
				productConfigurations.add(
					new cscfga__Product_Configuration__c(
						Id = productConfiguration.Id,
						BAN_Information__c = opportunity.BAN__c,
						Financial_Account__c = opportunity.Financial_Account__c,
						Billing_Arrangement__c = opportunity.Billing_Arrangement__c
					)
				);
			}
		}

		//update productConfigurations;
		//SF-920 problem with saving BAN when partner user has no access to BAN record
		SharingUtils.updateRecordsWithoutSharing(productConfigurations);
	}

	public class BasketBillingInformationException extends Exception {
	}
}
