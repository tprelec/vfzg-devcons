@isTest
private class TestProductBasketOverrideController {

    @isTest static void testCheckUserAccess() {
        PageReference pageRef = Page.ProposalPage;
        Test.setCurrentPage(pageRef);
        TestUtils.createOrderValidationOpportunity();
        TestUtils.createCompleteOpportunity();

        Test.startTest();
        ProductBasketOverrideController.checkUserAccess(TestUtils.theOpportunity.Id);
        Test.stopTest();
    }
}