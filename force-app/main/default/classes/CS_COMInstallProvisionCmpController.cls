public with sharing class CS_COMInstallProvisionCmpController {
    static final String MATERIAL_SELECT_TYPE = 'Material Select';
    static final String PORTING_DELIVERY_COMPNENT = 'Port Number Block';

    @AuraEnabled
    public static List<CS_ComDeliveryComponentWrapper> getData(Id recordId) {
        List<CS_ComDeliveryComponentWrapper> returnValue = new List<CS_ComDeliveryComponentWrapper>();

        List<Task> taskList = [SELECT 
                id, WhatId, 
                CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.CSPOFA__Orchestration_Process_Template__r.MACD_Flow_Type__c, 
                CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.CSPOFA__Parent_Process__r.CSPOFA__Orchestration_Process_Template__r.MACD_Flow_Type__c,
                CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.CSPOFA__Parent_Process__c,
                Record_Type_Text__c, Type, Owner.Name 
                FROM Task 
                WHERE id =:recordId];
        
        returnValue = getDeliveryComponents(taskList[0], taskList[0].WhatId);

        return returnValue;
    }

    @AuraEnabled
    public static String getDeliveryTaskType(Id recordId) {
        List<Task> taskList = [SELECT 
                id, Type 
                FROM Task 
                WHERE id =:recordId AND Record_Type_Text__c = 'COM_Delivery'];
        
        String type = taskList.isEmpty()?'':taskList[0].Type;
            
        return type;
    }

    private static List<CS_ComDeliveryComponentWrapper> getDeliveryComponents(Task taskRecord, Id deliveryOrderId){
        
        System.debug('Task: ' + taskRecord);
        System.debug('deliveryOrderId: ' + deliveryOrderId);
        
        // TODO
        List<COM_Delivery_Order__c> deliveryOrder = [SELECT id, Site__r.Footprint__c FROM COM_Delivery_Order__c WHERE id=:deliveryOrderId];
        
        if(deliveryOrder.size() == 0) {
            return new List<CS_ComDeliveryComponentWrapper>();
        }
        
        List<Delivery_Component__c> allDeliveryComponent;
        List<Delivery_Component__c> deliveryComponents;
        List<COM_Delivery_Component__mdt> componentMtd;
        Map<String,COM_Delivery_Component__mdt> componentMtdMap;
        List<COM_Delivery_Component_Article_Materials__mdt> articleMaterials;
        String activityType = (taskRecord.Record_Type_Text__c.equals('COM_Delivery') && (taskRecord.Type.equals('COM_Provisioning') || taskRecord.Type.equals('COM_Suspend Service'))) ? 'Provisioning' : 'Installation';
        Set<String> deliveryComponentNamesFromActivites;
        Set<String> allDeliveryComponentNames;
        Set<String> allDeliveryComponentArticleNames;
        List<CS_ComDeliveryComponentWrapper> deliveryComponentWrapperList;
        Map<String,COM_Delivery_Component__mdt> deliveryComponentsMtdMap = new Map<String,COM_Delivery_Component__mdt>();
        Set<Id> subscriptionIds = new Set<Id>();
        Map<String,String> subscriptionSpecifications;
        Set<Id> dependentPCIds = new Set<Id>();
        List<csord__Service__c> dependentServices = new List<csord__Service__c>();
        Set<Id> replacedServiceIds = new Set<Id>();
        Map<Id,Id> dependentPCIdServiceId = new Map<Id,Id>();
        Map<Id,Delivery_Component__c> dependentServicesDeliveryComponentsMap = new Map<Id,Delivery_Component__c>();
        Map<Id,List<Delivery_Component__c>> dependentParentServiceIdDeliveryComponents = new Map<Id,List<Delivery_Component__c>>();
        List<Delivery_Component__c> replcedDeliveryComponents = new List<Delivery_Component__c>();
        Map<Id,List<Delivery_Component__c>> replacedParentServiceIdDeliveryComponents = new Map<Id,List<Delivery_Component__c>>();
        Map<Id,List<Delivery_Component__c>> replacedServiceIdDeliveryComponents = new Map<Id,List<Delivery_Component__c>>();
        Map<String,COM_Delivery_Component__mdt> deliveryComponentMetadata = new Map<String,COM_Delivery_Component__mdt>();

        allDeliveryComponent = getAllDeliveryComponents(deliveryOrderId);
        System.debug('***allDeliveryComponent: ' + allDeliveryComponent.size());
        deliveryComponentMetadata = getDeliveryComponentMetadata(allDeliveryComponent);
        subscriptionIds = getSubscriptionsIdsFromDeliveryComponents(allDeliveryComponent);
        System.debug('***subscriptionIds: ' + subscriptionIds);
        // TODO: Rahul to fix below commented line
        subscriptionSpecifications = null; // CS_ComHelper.getSubscriptionSpecifications(new List<Id>(subscriptionIds));
        // 09.06.2021 Commented out following line because causing NULL pointer exception
        // System.debug('***subscriptionSpecifications: ' + subscriptionSpecifications.size());
        dependentPCIds = getDependentPcIdsFromDeliveryComponents(allDeliveryComponent);
        System.debug('***dependentPCIds: ' + dependentPCIds);
        replacedServiceIds = getReplacedServiceIdsFromDeliveryComponents(allDeliveryComponent);
        System.debug('***replacedServiceIds: ' + replacedServiceIds.size());
        dependentServices = getDependentServices(dependentPCIds);
        System.debug('***dependentServices: ' + dependentServices.size());
        dependentPCIdServiceId = getDependentPcIdServiceIdMap(dependentServices);
        System.debug('***dependentPCIdServiceId: ' + dependentPCIdServiceId.size());
        dependentServicesDeliveryComponentsMap = getDependentServicesDeliveryComponentsMap(dependentServices);
        System.debug('***dependentServicesDeliveryComponentsMap: ' + dependentServicesDeliveryComponentsMap.size());
        dependentParentServiceIdDeliveryComponents = getDependentParentServiceIdDeliveryComponents(dependentServicesDeliveryComponentsMap.values());
        System.debug('***dependentServiceIdDeliveryComponents: ' + dependentParentServiceIdDeliveryComponents.size());
        replcedDeliveryComponents = getReplacedDeliveryComponents(replacedServiceIds);
        System.debug('***replcedDeliveryComponents: ' + replcedDeliveryComponents.size());
        replacedParentServiceIdDeliveryComponents = getReplacedParentServiceIdDeliveryComponents(replcedDeliveryComponents);
        System.debug('***replacedParentServiceIdDeliveryComponents: ' + replacedParentServiceIdDeliveryComponents.size());
        replacedServiceIdDeliveryComponents = getReplacedServiceIdDeliveryComponents(replcedDeliveryComponents);
        System.debug('***getReplacedServiceIdDeliveryComponents: ' + replacedParentServiceIdDeliveryComponents.size());
        allDeliveryComponentNames = getAlldeliveryComponentNames(allDeliveryComponent);
        System.debug('***allDeliveryComponentNames: ' + allDeliveryComponentNames);
        allDeliveryComponentArticleNames = getAllDeliveryComponentsArticleNames(allDeliveryComponent);
        System.debug('***allDeliveryComponentArticleNames: ' + allDeliveryComponentArticleNames);
        articleMaterials = getAllArticleMaterials(allDeliveryComponentArticleNames);
        System.debug('***getAllArticleMaterials: ' + articleMaterials);
        componentMtd = getComponentMtd(taskRecord.Record_Type_Text__c, allDeliveryComponentNames, taskRecord.Type);
        System.debug('***getComponentMtd: ' + componentMtd.size());
        componentMtdMap = getDeliveryComponentMtdMap(componentMtd);
        System.debug('***componentMtdMap: ' + componentMtdMap.size());

        for (COM_Delivery_Component__mdt deliveryComponentsMtd :componentMtd) {
            deliveryComponentsMtdMap.put(deliveryComponentsMtd.Label,deliveryComponentsMtd);
        }

        if (taskRecord.Record_Type_Text__c.equals('COM_Delivery') && (taskRecord.Type.equals('COM_Installation') || taskRecord.Type.equals('COM_Installation Jeopardy') || taskRecord.Type.equals('COM_Returnbox'))) {
            deliveryComponentNamesFromActivites = getDeliveryComponentNames(componentMtd);
        } else {
            String processType = taskRecord.CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.CSPOFA__Orchestration_Process_Template__r.MACD_Flow_Type__c;
            if(processType == null && taskRecord.CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.CSPOFA__Parent_Process__c != null) {
                processType = taskRecord.CSPOFA__Orchestration_Step__r.CSPOFA__Orchestration_Process__r.CSPOFA__Parent_Process__r.CSPOFA__Orchestration_Process_Template__r.MACD_Flow_Type__c;
            }

            deliveryComponentNamesFromActivites = CS_ComHelper.getDeliveryComponentNamesFromActivitiesForProvisioning(deliveryComponentsMtdMap,allDeliveryComponent);
        }
        System.debug('*** deliveryComponentNamesFromActivites: ' + JSON.serializePretty(deliveryComponentNamesFromActivites));

        deliveryComponents = getDeliveryComponentRecords( deliveryOrderId, deliveryComponentNamesFromActivites);
        System.debug('*** deliveryComponents: ' + JSON.serializePretty(deliveryComponents));
        deliveryComponentWrapperList = getDeliveryComponentWrappers(deliveryComponents,articleMaterials,replacedParentServiceIdDeliveryComponents,subscriptionSpecifications,dependentPCIdServiceId, dependentParentServiceIdDeliveryComponents, dependentServicesDeliveryComponentsMap, componentMtdMap, deliveryComponentMetadata, replacedServiceIdDeliveryComponents);
        System.debug('*** deliveryComponentWrapperList: ' + JSON.serializePretty(deliveryComponentWrapperList));

        return deliveryComponentWrapperList;
    }

    private static Set<Id> getSubscriptionsIdsFromDeliveryComponents(List<Delivery_Component__c> deliveryComponents){
        Set<Id> returnValue = new Set<Id>();

        for (Delivery_Component__c deliveryComponentRecord : deliveryComponents) {
            if (deliveryComponentRecord.Name == PORTING_DELIVERY_COMPNENT) {
                if (deliveryComponentRecord.Service__r.csord__Service__r != null) {
                    returnValue.add(deliveryComponentRecord.Service__r.csord__Service__r.csord__Subscription__c);
                } else {
                    returnValue.add(deliveryComponentRecord.Service__r.csord__Subscription__c);
                }
            }
        }

        return returnValue;
    }

    private static Set<Id> getDependentPcIdsFromDeliveryComponents(List<Delivery_Component__c> deliveryComponents) {
        Set<Id> returnValue = new Set<Id>();

        for (Delivery_Component__c deliveryComponent : deliveryComponents) {
            if (deliveryComponent.Service__r.VFZ_Depends_On_These_Service_Identifiers__c != null) {
                returnValue.addAll((List<Id>)deliveryComponent.Service__r.VFZ_Depends_On_These_Service_Identifiers__c.split(','));
            } else if (deliveryComponent.Service__r.csord__Service__r.VFZ_Depends_On_These_Service_Identifiers__c != null){
                returnValue.addAll((List<Id>)deliveryComponent.Service__r.csord__Service__r.VFZ_Depends_On_These_Service_Identifiers__c.split(','));
            }
        }

        return returnValue;
    }

    private static Set<Id> getReplacedServiceIdsFromDeliveryComponents(List<Delivery_Component__c> deliveryComponents){
        Set<Id> returnValue = new Set<Id>();

        for (Delivery_Component__c deliveryComponent : deliveryComponents) {
            if (deliveryComponent.Service__r.csord__Service__r != null) {
                returnValue.add(deliveryComponent.Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c);
            } else if (deliveryComponent.Service__r.csord__Service__r == null){
                returnValue.add(deliveryComponent.Service__r.csordtelcoa__Replaced_Service__c);
            }
        }

        if (returnValue.contains(null)) {
            returnValue.remove(null);
        }

        return returnValue;
    }

    private static List<csord__Service__c> getDependentServices(Set<Id> dependentPCIds){
        List<csord__Service__c> dependentServices = new List<csord__Service__c>();
        if (!dependentPCIds.isEmpty()) {
            dependentServices = [SELECT Id,
                    Name,
                    csordtelcoa__Product_Configuration__c
            FROM csord__Service__c
            WHERE csordtelcoa__Product_Configuration__c IN :dependentPCIds];
        }

        return dependentServices;
    }

    private static Map<Id,Id> getDependentPcIdServiceIdMap(List<csord__Service__c> dependentServices){
        Map<Id,Id> dependentPCIdServiceId = new Map<Id,Id>();

        for(csord__Service__c serviceRecord : dependentServices){
            dependentPCIdServiceId.put(serviceRecord.csordtelcoa__Product_Configuration__c, serviceRecord.Id);
        }

        return dependentPCIdServiceId;
    }

    private static Map<Id,Delivery_Component__c> getDependentServicesDeliveryComponentsMap(List<csord__Service__c> dependentServices){
        Map<Id,Delivery_Component__c> dependentServicesDeliveryComponentsMap = new Map<Id,Delivery_Component__c>([SELECT Id,
                Name,
                Service__c,
                Service__r.csord__Service__c,
                Service__r.csord__Subscription__r.Name
            FROM Delivery_Component__c
            WHERE (Service__c IN :dependentServices)
                OR (Service__r.csord__Service__c IN :dependentServices)]);

        return dependentServicesDeliveryComponentsMap;
    }

    @TestVisible
    private static Map<Id,List<Delivery_Component__c>> getDependentParentServiceIdDeliveryComponents(List<Delivery_Component__c> deliveryComponents){
        Map<Id,List<Delivery_Component__c>> dependentParentServiceIdDeliveryComponents = new Map<Id,List<Delivery_Component__c>>();

        for (Delivery_Component__c dc : deliveryComponents){
            Id parentServiceId;
            if (dc.Service__r.csord__Service__c != null) {
                parentServiceId = dc.Service__r.csord__Service__c;
            } else {
                parentServiceId = dc.Service__c;
            }

            if (dependentParentServiceIdDeliveryComponents.get(parentServiceId) != null) {
                dependentParentServiceIdDeliveryComponents.get(parentServiceId).add(dc);
            } else {
                dependentParentServiceIdDeliveryComponents.put(parentServiceId,new List<Delivery_Component__c>{dc});
            }
        }

        return dependentParentServiceIdDeliveryComponents;
    }

    private static List<Delivery_Component__c> getReplacedDeliveryComponents(Set<Id> replacedServiceIds){
        List<Delivery_Component__c> replcedDeliveryComponents = [SELECT Id,
                    Name,
                    Article_Name__c,
                    Article_Code__c,
                    Service__r.csord__Service__c,
                    Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c,
                    Service__r.csordtelcoa__Replaced_Service__c
            FROM Delivery_Component__c
            WHERE Service__c IN :replacedServiceIds OR Service__r.csord__Service__c IN:replacedServiceIds];

        return replcedDeliveryComponents;
    }

    private static Map<Id,List<Delivery_Component__c>> getReplacedServiceIdDeliveryComponents(List<Delivery_Component__c> deliveryComponents){
        Map<Id,List<Delivery_Component__c>> replacedServiceIdDeliveryComponents = new Map<Id,List<Delivery_Component__c>>();

        for (Delivery_Component__c deliveryComponent : deliveryComponents) {
            Id parentServiceId = deliveryComponent.Service__c;

            if (parentServiceId != null) {
                if (replacedServiceIdDeliveryComponents.get(parentServiceId) != null) {
                    replacedServiceIdDeliveryComponents.get(parentServiceId).add(deliveryComponent);
                } else {
                    replacedServiceIdDeliveryComponents.put(parentServiceId, new List<Delivery_Component__c>{
                            deliveryComponent
                    });
                }
            }
        }

        return replacedServiceIdDeliveryComponents;
    }
    private static Map<Id,List<Delivery_Component__c>> getReplacedParentServiceIdDeliveryComponents(List<Delivery_Component__c> deliveryComponents){
        Map<Id,List<Delivery_Component__c>> replacedParentServiceIdDeliveryComponents = new Map<Id,List<Delivery_Component__c>>();

        for (Delivery_Component__c deliveryComponent : deliveryComponents) {
            Id parentServiceId;
            if (deliveryComponent.Service__r.csord__Service__c != null) {
                parentServiceId = deliveryComponent.Service__r.csord__Service__c;
            } else if (deliveryComponent.Service__r.csord__Service__c == null){
                parentServiceId = deliveryComponent.Service__c;
            }

            if (parentServiceId != null) {
                if (replacedParentServiceIdDeliveryComponents.get(parentServiceId) != null) {
                    replacedParentServiceIdDeliveryComponents.get(parentServiceId).add(deliveryComponent);
                } else {
                    replacedParentServiceIdDeliveryComponents.put(parentServiceId, new List<Delivery_Component__c>{
                            deliveryComponent
                    });
                }
            }
        }

        return replacedParentServiceIdDeliveryComponents;
    }

    private static List<Delivery_Component__c> getAllDeliveryComponents(Id deliveryOrderId){
        List<Delivery_Component__c> deliveryComponents = [SELECT Id,
                                                                Name,
                                                                Service__c,
                                                                Service__r.csordtelcoa__Replaced_Service__c,
                                                                Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c,
                                                                Service__r.csord__Subscription__c,
                                                                Service__r.csord__Service__r.csord__Subscription__c,
                                                                Service__r.VFZ_Depends_On_These_Service_Identifiers__c,
                                                                Service__r.csord__Service__r.VFZ_Depends_On_These_Service_Identifiers__c,
                                                                Article_Name__c,
                                                                Article_Code__c,
                                                                Service__r.Name,
                                                                Implemented_Date__c,
                                                                MACD_Action__c
                                                        FROM Delivery_Component__c
                                                        WHERE COM_Delivery_Order__c = :deliveryOrderId
                                                        AND COM_Delivery_Order__c != null
                                                        ];
        return deliveryComponents;
    }

    private static Set<String> getAlldeliveryComponentNames(List<Delivery_Component__c> deliveryComponents){
        Set<String> deliveryComponentNames = new Set<String>();

        for (Delivery_Component__c deliveryComponent : deliveryComponents) {
            deliveryComponentNames.add(deliveryComponent.Name);
        }

        return deliveryComponentNames;
    }

    private static List<COM_Delivery_Component__mdt> getComponentMtd(String taskRecordType, Set<String> deliveryComponentNames, String taskType){
        List<COM_Delivery_Component__mdt> returnValue;
        if (taskRecordType.equals('COM_Delivery') && taskType.equals('Provisioning')) {
            returnValue = [SELECT Id,
                    DeveloperName,
                    Label,
                    Installation__c,
                    Provisioning__c
            FROM COM_Delivery_Component__mdt
            WHERE Provisioning__c = true
            AND Label IN :deliveryComponentNames];
        } else {
            returnValue = [SELECT Id,
            DeveloperName,
            Label,
            Installation__c,
            Provisioning__c
            FROM COM_Delivery_Component__mdt
            WHERE Installation__c = true
            AND Label IN :deliveryComponentNames];
        }

        return returnValue;
    }

    private static Map<String,COM_Delivery_Component__mdt> getDeliveryComponentMtdMap(List<COM_Delivery_Component__mdt> componentMtdList){
        Map<String,COM_Delivery_Component__mdt> returnValue = new Map<String,COM_Delivery_Component__mdt>();

        for (COM_Delivery_Component__mdt deliveryComponentsMtd : componentMtdList) {
            returnValue.put(deliveryComponentsMtd.Label, deliveryComponentsMtd);
        }

        return returnValue;
    }
    
    @TestVisible
    private static Set<String> getDeliveryComponentNames(List<COM_Delivery_Component__mdt> componentMtd){
        Set<String> returnValue = new Set<String>();

        for (COM_Delivery_Component__mdt mtd : componentMtd) {
            returnValue.add(mtd.Label);
        }

        return returnValue;
    }

    private static List<Delivery_Component__c> getDeliveryComponentRecords(Id deliveryOrderId, Set<String> deliveryComponentNamesFromActivites){
        List<Delivery_Component__c> returnValue;

        // TODO: Rahul to fix below query
        returnValue = [SELECT Id,
                            Name,
                            Service__c,
                            Service__r.csord__Subscription__r.Name,
                            Service__r.csordtelcoa__Replaced_Service__c,
                            Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c,
                            Service__r.VFZ_Depends_On_These_Service_Identifiers__c,
                            Service__r.csord__Service__r.VFZ_Depends_On_These_Service_Identifiers__c,
                            Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
                            Article_Name__c,
                            Article_Code__c,
                            Service__r.Name,
                            CDO_Service_ID__c,
                            Implemented_Date__c,
                            MACD_Action__c
                    FROM Delivery_Component__c
                    WHERE COM_Delivery_Order__c = :deliveryOrderId
                    AND COM_Delivery_Order__c != null
                    AND Name IN :deliveryComponentNamesFromActivites
            // WHERE Name IN :deliveryComponentNamesFromActivites
        ];

        return returnValue;
    }

    @TestVisible 
    private static List<CS_ComDeliveryComponentWrapper> getDeliveryComponentWrappers(List<Delivery_Component__c> deliveryComponents, List<COM_Delivery_Component_Article_Materials__mdt> articleMaterials, Map<Id,List<Delivery_Component__c>> replacedParentServiceIdDeliveryComponents,Map<String,String> subscriptionSpecifications,Map<Id,Id> dependentPCIdServiceId, Map<Id,List<Delivery_Component__c>> dependentParentServiceIdDeliveryComponents, Map<Id,Delivery_Component__c> dependentServicesDeliveryComponentsMap, Map<String,COM_Delivery_Component__mdt> deliveryComponentActivitiesMap, Map<String,COM_Delivery_Component__mdt> deliveryComponentMdt, Map<Id,List<Delivery_Component__c>> replacedServiceIdDeliveryComponents) {
        List<CS_ComDeliveryComponentWrapper> returnValue = new List<CS_ComDeliveryComponentWrapper>();
        for (Delivery_Component__c deliveryComponentRecord : deliveryComponents) {
            CS_ComDeliveryComponentWrapper deliveryComponentWrapper = new CS_ComDeliveryComponentWrapper();
            deliveryComponentWrapper.deliveryComponentId = deliveryComponentRecord.Id;
            deliveryComponentWrapper.productName = deliveryComponentRecord.Service__r.Name;
            deliveryComponentWrapper.componentName = deliveryComponentRecord.Name;
            deliveryComponentWrapper.articleName = deliveryComponentRecord.Article_Name__c;
            deliveryComponentWrapper.articleCode = deliveryComponentRecord.Article_Code__c;
            deliveryComponentWrapper.replacedArticleCode = getReplacedArticleCode(deliveryComponentRecord,replacedParentServiceIdDeliveryComponents,deliveryComponentMdt,replacedServiceIdDeliveryComponents);
            deliveryComponentWrapper.macdAction = deliveryComponentRecord.MACD_Action__c;
            deliveryComponentWrapper.implementedDate = deliveryComponentRecord.Implemented_Date__c;
            deliveryComponentWrapper.editable = (deliveryComponentRecord.MACD_Action__c == Constants.DELIVERY_COMPONENT_NO_CHANGE_STATUS || deliveryComponentRecord.MACD_Action__c == Constants.DELIVERY_COMPONENT_TERMINATED_STATUS)?false:true;
            deliveryComponentWrapper.cdoServiceId = deliveryComponentRecord.CDO_Service_ID__c;
            System.debug('+++ deliveryComponentRecord: ' + deliveryComponentRecord);

            deliveryComponentWrapper.portingDetails = CS_ComHelper.getPortingDetailsFromSubscriptionSpecifications(subscriptionSpecifications,deliveryComponentRecord.Service__c);
            deliveryComponentWrapper.dependantDataDetails = getDependantData(deliveryComponentRecord, dependentPCIdServiceId, dependentParentServiceIdDeliveryComponents, dependentServicesDeliveryComponentsMap);

            returnValue.add(deliveryComponentWrapper);
        }
        return returnValue;
    }

    private static String getReplacedArticleCode(Delivery_Component__c deliveryComponent, Map<Id,List<Delivery_Component__c>> replacedParentServiceIdDeliveryComponents, Map<String,COM_Delivery_Component__mdt> deliveryComponentMdt, Map<Id,List<Delivery_Component__c>> replacedServiceIdDeliveryComponents){
        String returnValue = '';
        
        if(deliveryComponentMdt == null || deliveryComponentMdt.get(deliveryComponent.Name) == null) {
            System.debug('[Warn] Atribute is NULL. Returning from getReplacedArticleCode().');
            return returnValue;
        }        
        
        returnValue = getReplacedArticleCodeParent(deliveryComponent, replacedParentServiceIdDeliveryComponents);

        return returnValue;
    }

    @TestVisible
    private static String getReplacedArticleCodeChild(Delivery_Component__c deliveryComponent, Map<Id,List<Delivery_Component__c>> replacedServiceIdDeliveryComponents){
        String returnValue = '';
        Id parentServiceId;

        parentServiceId = deliveryComponent.Service__r.csordtelcoa__Replaced_Service__c;

        if (replacedServiceIdDeliveryComponents.get(parentServiceId) != null) {
            for (Delivery_Component__c replacedDeliveryComponent : replacedServiceIdDeliveryComponents.get(parentServiceId)) {
                if (deliveryComponent.Name == replacedDeliveryComponent.Name) {
                    returnValue = replacedDeliveryComponent.Article_Code__c;
                }
            }
        }

        return returnValue;
    }

    @TestVisible
    private static String getReplacedArticleCodeParent(Delivery_Component__c deliveryComponent, Map<Id,List<Delivery_Component__c>> replacedParentServiceIdDeliveryComponents){
        String returnValue = '';
        Id parentServiceId;

        if(deliveryComponent.Service__r.csord__Service__r != null) {
            parentServiceId = deliveryComponent.Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c;
        } else if (deliveryComponent.Service__r.csord__Service__r == null) {
            parentServiceId = deliveryComponent.Service__r.csordtelcoa__Replaced_Service__c;
        }

        if (replacedParentServiceIdDeliveryComponents.get(parentServiceId) != null) {
            for (Delivery_Component__c replacedDeliveryComponent : replacedParentServiceIdDeliveryComponents.get(parentServiceId)) {
                if (deliveryComponent.Name == replacedDeliveryComponent.Name) {
                    returnValue = replacedDeliveryComponent.Article_Code__c;
                }
            }
        }

        return returnValue;
    }

    private static Set<String> getAllDeliveryComponentsArticleNames(List<Delivery_Component__c> deliveryComponents){
        Set<String> deliveryComponentArticleNames = new Set<String>();

        for (Delivery_Component__c deliveryComponent : deliveryComponents) {
            deliveryComponentArticleNames.add(deliveryComponent.Article_Name__c);
        }

        return deliveryComponentArticleNames;
    }

    private static List<COM_Delivery_Component_Article_Materials__mdt> getAllArticleMaterials(Set<String> articleNames){
        List<COM_Delivery_Component_Article_Materials__mdt> returnValue = [SELECT Id,
                                                                                    Label,
                                                                                    DeveloperName,
                                                                                    COM_Delivery_Article__r.DeveloperName,
                                                                                    COM_Delivery_Article__r.Delivery_Article_Name__c,
                                                                                    COM_Delivery_Article__r.Label,
                                                                                    COM_Delivery_Materials__r.DeveloperName,
                                                                                    COM_Delivery_Materials__r.Label,
                                                                                    COM_Delivery_Materials__r.Model_Name__c
                                                                            FROM COM_Delivery_Component_Article_Materials__mdt
                                                                            WHERE COM_Delivery_Article__r.Delivery_Article_Name__c IN :articleNames];

        return returnValue;
    }

    @testVisible
    private static List<CS_ComDependantDataWrapper> getDependantData(Delivery_Component__c deliveryComponent, Map<Id,Id> dependentPCIdServiceId, Map<Id,List<Delivery_Component__c>> dependentParentServiceIdDeliveryComponents, Map<Id,Delivery_Component__c> dependentServicesDeliveryComponentsMap){
        List<CS_ComDependantDataWrapper> returnValue = new List<CS_ComDependantDataWrapper>();
        List<Id> dependentPcIds = new List<Id>();

        if (deliveryComponent.Service__r.VFZ_Depends_On_These_Service_Identifiers__c != null) {
            dependentPcIds = (List<Id>)deliveryComponent.Service__r.VFZ_Depends_On_These_Service_Identifiers__c.split(',');
        } else if (deliveryComponent.Service__r.csord__Service__r.VFZ_Depends_On_These_Service_Identifiers__c != null){
            dependentPcIds = (List<Id>)deliveryComponent.Service__r.csord__Service__r.VFZ_Depends_On_These_Service_Identifiers__c.split(',');
        }
        if (!String.isEmpty(deliveryComponent.Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c)
                && deliveryComponent.Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c != 'New') {
            for (Id dependentPcId : dependentPcIds){
                for (Delivery_Component__c dc : dependentParentServiceIdDeliveryComponents.get(dependentPCIdServiceId.get(dependentPcId))) {
                    CS_ComDependantDataWrapper dependantDataWrapperRecord = new CS_ComDependantDataWrapper();
                    dependantDataWrapperRecord.productName = dc.Service__r.csord__Subscription__r.Name;
                    returnValue.add(dependantDataWrapperRecord);
                }
            }
        }

        return returnValue;
    }

    private static Map<String,COM_Delivery_Component__mdt> getDeliveryComponentMetadata(List<Delivery_Component__c> deliveryComponents){
        Set<String> deliveryComponentNames = new Set<String>();
        Map<String,COM_Delivery_Component__mdt> deliveryComponentMdtMap = new Map<String,COM_Delivery_Component__mdt>();

        for (Delivery_Component__c deliveryComponentRecord : deliveryComponents) {
            deliveryComponentNames.add(deliveryComponentRecord.Name);
        }

        List<COM_Delivery_Component__mdt> deliveComponentMdt = [SELECT Id,
                                                                        Label,
                                                                        DeveloperName
                                                                FROM COM_Delivery_Component__mdt
                                                                WHERE
                                                                Label IN :deliveryComponentNames];

        for (COM_Delivery_Component__mdt mdtRecord : deliveComponentMdt) {
            deliveryComponentMdtMap.put(mdtRecord.Label, mdtRecord);
        }
        return deliveryComponentMdtMap;
    }
}