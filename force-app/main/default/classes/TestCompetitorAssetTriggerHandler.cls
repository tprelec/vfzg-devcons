@isTest
public with sharing class TestCompetitorAssetTriggerHandler {
	final static private String MOCK_URL = 'http://example.com/example/test';

	@TestSetup
	static void makeData() {
		setConfig();

		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, null);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test');
		insert c;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, c.Id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);

		VF_Family_Tag__c familyTag = new VF_Family_Tag__c(
			Name = Constants.PRODUCT2_FAMILYTAG_PBX_Trunking_SIP_Charging,
			ExternalID__c = 'FT-06-ABCDEF'
		);
		insert familyTag;

		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct(
			new Map<String, Object>{
				'Billing_Type__c' => Constants.PRODUCT2_BILLINGTYPE_STANDARD,
				'Family_Tag__c' => familyTag.Id,
				'ProductCode' => 'C106929'
			}
		);
		insert product;

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = contr.Id;
		ord.O2C_Order__c = true;
		insert ord;

		Customer_Asset__c ca = new Customer_Asset__c();
		ca.Billing_Arrangement__c = ba.Id;
		ca.Order__c = ord.Id;
		ca.Site__c = site.Id;
		insert ca;

		Competitor_Asset__c compAsset = new Competitor_Asset__c();
		compAsset.Assigned_Product_Id__c = '23468934345';
		insert compAsset;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = 'Acq'; // Required and fixed for BOP
		cp.VF_Contract__c = contr.Id;
		cp.Customer_Asset__c = ca.Id;
		cp.PBX__c = compAsset.Id;
		cp.Product__c = product.Id;
		cp.Delivery_Status__c = Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED;
		cp.Billing_Offer_synced_to_Unify_timestamp__c = null;
		cp.PBX_Change_Error_Info__c = null;
		insert cp;

		setHttpMock();
	}

	@isTest
	private static void testPbxTrunkingChange() {
		Competitor_Asset__c ca = getCompetitorAsset();

		Test.startTest();
		ca.PBX_Trunking_Done__c = true;
		update ca;
		Test.stopTest();

		Competitor_Asset__c caFetch = getCompetitorAsset();

		System.assertEquals(true, caFetch.PBX_Trunking_Done__c, 'PBX Trunking must be true');
	}

	private static void setConfig() {
		External_WebService_Config__c config = new External_WebService_Config__c(
			Name = SiasService.INTEGRATION_SETTING_NAME,
			URL__c = MOCK_URL,
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;
	}

	private static void setHttpMock() {
		Contracted_Products__c cp = getContractedProduct();
		Competitor_Asset__c ca = getCompetitorAsset();

		Map<String, HttpCalloutMock> endpoint2TestResp = new Map<String, HttpCalloutMock>();
		endpoint2TestResp.put(
			MOCK_URL,
			new TestUtilMultiRequestMock.SingleRequestMock(
				200,
				'Complete',
				'{"changePBXOrderResponse": {"orderLinesList": [{"APID": "' +
				ca.Assigned_Product_Id__c +
				'","externalReferenceID": "' +
				cp.PBX__c +
				'"}]}}',
				null
			)
		);
		HttpCalloutMock multiCalloutMockObj = new TestUtilMultiRequestMock(endpoint2TestResp);
		Test.setMock(HttpCalloutMock.class, multiCalloutMockObj);
	}

	private static Contracted_Products__c getContractedProduct() {
		return [SELECT Id, Billing_Offer_synced_to_Unify_timestamp__c, PBX_Change_Error_Info__c, PBX__c FROM Contracted_Products__c LIMIT 1];
	}

	private static Competitor_Asset__c getCompetitorAsset() {
		return [SELECT Id, PBX_Trunking_Done__c, Billing_Offer_synced_to_Unify_timestamp__c, Assigned_Product_Id__c FROM Competitor_Asset__c LIMIT 1];
	}
}
