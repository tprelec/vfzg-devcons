/**
 * @description       : provieds test and use cases coverage for methods on `LightningAppointmentBypass` apex class
 * @last modified on  : 06-21-2022
 **/
@IsTest
public class TestLightningAppointmentBypass {
	@TestSetup
	static void initData() {
		WorkTypeGroup hybrideWerkenType = new WorkTypeGroup(Name = 'Hybride werken');
		insert hybrideWerkenType;
	}

	@IsTest
	static void testgetWorkTypeGroupId() {
		List<String> workTypeNames = new List<String>{ 'Hybride%20werken' };
		Test.startTest();
		List<String> workTypeGroupIds = LightningAppointmentBypass.getWorkTypeGroupId(workTypeNames);
		Test.stopTest();
		WorkTypeGroup hybrideWerken = [SELECT Id FROM WorkTypeGroup WHERE Name = 'Hybride werken'];
		System.assert(!workTypeGroupIds.isEmpty(), 'Work Type Group Id was not returned for Hybride werken');
		System.assertEquals(hybrideWerken.Id, workTypeGroupIds.get(0), 'returned Id does not match Work Type Group Id for Hybride Werken');
	}

	@IsTest
	static void testgetWorkTypeGroupIdWhenNotFound() {
		List<String> workTypeNames = new List<String>{ 'Vaste%20connectiviteit' };
		Test.startTest();
		List<String> workTypeGroupIds = LightningAppointmentBypass.getWorkTypeGroupId(workTypeNames);
		Test.stopTest();
		System.assert(workTypeGroupIds.isEmpty(), 'name list should be empty, no Work Type Group named Vaste connectiviteit should be found');
	}

	@IsTest
	static void testgetWorkTypeGroupIdWhenEmpty() {
		List<String> workTypeNames = new List<String>();
		Test.startTest();
		List<String> workTypeGroupIds = LightningAppointmentBypass.getWorkTypeGroupId(workTypeNames);
		Test.stopTest();
		System.assert(workTypeGroupIds.isEmpty(), 'No input string was sent, an empty response is expected');
	}
}
