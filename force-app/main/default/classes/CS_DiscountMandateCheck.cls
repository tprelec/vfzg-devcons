public with sharing class CS_DiscountMandateCheck {
	public static Boolean checkDiscountMandate(
		List<cscfga__Product_Configuration__c> savedConfigurations,
		List<cscfga__Product_Configuration__c> allConfigurations,
		List<String> outputMessages
	) {
		Boolean hasErrors = false;

		Map<Id, List<cscfga__Product_Configuration__c>> configsPerCommercialProduct = new Map<Id, List<cscfga__Product_Configuration__c>>();
		Map<Id, Decimal> quantityPerProduct = new Map<Id, Decimal>();

		for (cscfga__Product_Configuration__c config : allConfigurations) {
			if (config.New_Portfolio__c && config.cscfga__Product_Family__c == 'Business Mobile') {
				if (configsPerCommercialProduct.containsKey(config.Commercial_Product__c)) {
					configsPerCommercialProduct.get(config.Commercial_Product__c).add(config);
					Decimal quantity = quantityPerProduct.get(config.Commercial_Product__c) + config.cscfga__Quantity__c;
					quantityPerProduct.put(config.Commercial_Product__c, quantity);
				} else {
					List<cscfga__Product_Configuration__c> mapConfigurations = new List<cscfga__Product_Configuration__c>{ config };
					configsPerCommercialProduct.put(config.Commercial_Product__c, mapConfigurations);

					quantityPerProduct.put(config.Commercial_Product__c, config.cscfga__Quantity__c);
				}
			}
		}

		if (quantityPerProduct.keySet().isEmpty()) {
			System.debug('off');
			// we don't need to check discount mandates as there no products which require that check
			return true;
		}

		List<Discount_Mandate__c> discountMandates = [
			SELECT Id, Min_Quantity__c, Min_Duration__c, Max_Quantity__c, Max_Duration__c, Value__c, Commercial_Product__c
			FROM Discount_Mandate__c
			WHERE Commercial_Product__c IN :quantityPerProduct.keySet()
		];

		Map<Id, List<Discount_Mandate__c>> discountMandatePerProduct = new Map<Id, List<Discount_Mandate__c>>();

		for (Discount_Mandate__c discountMandate : discountMandates) {
			if (discountMandatePerProduct.containsKey(discountMandate.Commercial_Product__c)) {
				discountMandatePerProduct.get(discountMandate.Commercial_Product__c).add(discountMandate);
			} else {
				List<Discount_Mandate__c> listDiscountMandates = new List<Discount_Mandate__c>{ discountMandate };
				discountMandatePerProduct.put(discountMandate.Commercial_Product__c, listDiscountMandates);
			}
		}

		for (cscfga__Product_Configuration__c savedConfiguration : savedConfigurations) {
			if (savedConfiguration.New_Portfolio__c && savedConfiguration.cscfga__Product_Definition__r.Name == 'Business Mobile') {
				Decimal productQuantity = quantityPerProduct.get(savedConfiguration.Commercial_Product__c);
				// find discount mandate for given quantity and contract duration (and user level??)
				if (discountMandatePerProduct.containsKey(savedConfiguration.Commercial_Product__c)) {
					List<Discount_Mandate__c> mandates = discountMandatePerProduct.get(savedConfiguration.Commercial_Product__c);

					if (checkSingleConfiguration(savedConfiguration, productQuantity, mandates, outputMessages)) {
						hasErrors = true;
					}
				}
			}
		}

		return !hasErrors;
	}

	private static Boolean checkSingleConfiguration(
		cscfga__Product_Configuration__c savedConfiguration,
		Decimal productQuantity,
		List<Discount_Mandate__c> mandates,
		List<String> outputMessages
	) {
		Boolean hasErrors = false;
		for (Discount_Mandate__c discountMandate : mandates) {
			if (
				discountMandate.Min_Quantity__c <= productQuantity &&
				discountMandate.Max_Quantity__c >= productQuantity &&
				discountMandate.Min_Duration__c <= savedConfiguration.cscfga__Contract_Term__c &&
				discountMandate.Max_Duration__c >= savedConfiguration.cscfga__Contract_Term__c
			) {
				List<cscfga.ProductConfiguration.Discount> discountList = CS_DiscountObserverAfterSave.getDiscountObjectsOOTBStructure(
					savedConfiguration.csdiscounts__manual_discounts__c,
					new cscfga.ProductConfiguration.Discount()
				);

				for (cscfga.ProductConfiguration.Discount discountElement : discountList) {
					if (discountElement.type == CS_ConstantsCOM.DM_TYPE_PERCENTAGE) {
						if (discountElement.amount > discountMandate.Value__c) {
							String message =
								'Discount mandate check failed for product ' +
								savedConfiguration.Name +
								' - maximum allowed discount for given contract duration and quantity is ' +
								discountMandate.Value__c +
								'%';

							outputMessages.add(message);
							hasErrors = true;
						}
					}
				}
			}
		}

		return hasErrors;
	}
}
