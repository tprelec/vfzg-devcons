/**
 * @description			This exception should be thrown if a parameter value is illegal.
 * @author				Guy Clairbois
 */
public class ExInvalidParameterException extends Exception {}