/**
 * @description			This is an extension to the account standardcontroller, enabling the BOA process (account transferring between owners).
 * @author				Guy Clairbois
 */
public without sharing class AccountChangeOwnerController {
        
    public Account acc{get;set;}
    public Account dummyAccount{get;set;}
    public String ownershipLevelSelected{get;set;}
    public String typeOfChangeSelected{get;set;}
    public Date lastOwnershipSnapshotDate{get;private set;}
    public Date nextOwnershipSnapshotDate{get;private set;}

    
    ApexPages.Standardcontroller controller;
    
    public AccountChangeOwnerController(ApexPages.StandardController stdController){
        controller = stdController;
        if (!Test.isRunningTest()) controller.addFields(new List<String>{'OwnerId', 'Fixed_Owner__c'});
        acc = (Account)stdController.getRecord();
		dummyAccount = new Account();

		// initially just set the current values
        setAccountOwnerManager(acc.Fixed_Dealer__c, dummyAccount, null);
        setAccountOwnerManager(acc.Mobile_Dealer__c, acc, null);        

        lastOwnershipSnapshotDate = System.today().toStartOfMonth().addDays(-1); // TODO: make dynamic based on last BICC load?
        nextOwnershipSnapshotDate = System.today().addMonths(1).toStartOfMonth().addDays(-1);
    }

	public Boolean getIsLocked() {
		return Approval.isLocked(acc.Id);
	}
	
	public Boolean getIsSuperUser() {
		Set<String> profileNames = new Set<String>{'VF Business Partner Manager', 'System Administrator', 'VF Indirect Inside Sales', 'VF Direct Inside Sales'};
		return profileNames.contains(GeneralUtils.currentUser.Profile.Name);
	}
        
    public PageReference SaveAndSubmit() {
        
        if(acc.New_Dealer__c == null) {
            ApexPages.Message message = new ApexPages.message(ApexPages.severity.ERROR,'Please select the New Owner.');     
            ApexPages.addMessage(message);
            return null;
        }

        // set the manager for approval, depending on the dealer that was selected
        // if handover is selected, set the new owner's manager, if claim is selected, set the current owner's manager
        if (typeOfChangeSelected == 'Claim') {
			if (!setAccountOwnerManager(acc.New_Dealer__c,acc,'claim')) {
				return null;
			}
		} else if (typeOfChangeSelected == 'Hand over') {
			// set manager to new owner's manager and set 'managedbypartner' if applicable
			if (!setAccountOwnerManager(acc.New_Dealer__c,acc,'handover')) {
				return null;
			}
		}

        // set typeOfChange
        system.debug(typeOfChangeSelected);
        system.debug(ownershipLevelSelected);
        acc.Type_of_Change__c = typeOfChangeSelected; //+ ' ' + ownershipLevelSelected;
        acc.x1st_Approval_Status__c = 'Not Started';
        acc.x2nd_Approval_Status__c = 'Not Started';
        acc.x3rd_Approval_Status__c = 'Not Started';
        Savepoint sp = database.setSavepoint();
    	try {
			update acc;
    	} catch(Exception ex) {
      		ApexPages.addMessages(ex);
      		Database.rollback(sp);
      		return null;
    	}  
        
        PageReference acctPage = new ApexPages.StandardController(acc).view();
        acctPage.setRedirect(true);
        return acctPage;
    }


	/**
	 * Method to fill the field AccountOwner Manager. This is used for the approval process.
	 * For Partners, the Business Partner Manager is selected.
	 */
	public Boolean setAccountOwnerManager(Id dealerId, Account acct, String typeOfChange) {
		
		User owner = [SELECT ManagerId, IsActive, UserType, ContactId FROM User WHERE Id = :acc.OwnerId];
		User fixedOwner;
		if (acc.Fixed_Owner__c != null) fixedOwner = [SELECT ManagerId, IsActive, UserType, ContactId FROM User WHERE Id = :acc.Fixed_Owner__c];
		Dealer_Information__c di;
		if (dealerId != null) {
			di = [
				SELECT Id, 
					Contact__r.UserId__r.ManagerId,
					Contact__r.UserId__r.IsActive, 
					Contact__r.UserId__r.UserType,
					Contact__r.UserId__c,
					Contact__r.Account.OwnerId,
					Contact__c  
				FROM Dealer_Information__c 
				WHERE Id = :dealerId
			];
		}

		if (di != null && di.Contact__r != null) {
			acc.User__c = UserInfo.getUserId();
			// if dealercode has a contact/user in SFDC, ok
			acct.New_Dealer__c = dealerId;
			acct.User__c = di.Contact__r.UserId__c;
			if (GeneralUtils.isPartnerUser(di.Contact__r.UserId__r)) {
				acct.Managed_by_Partner__c = true;
			} else {
				acct.Managed_by_Partner__c = false; 
			} 
		} else {
			// What to do if dealercode has no contact/user in SFDC?
			// use the non-active owner approval process
			// also set the manager to be the current owner's manager (legacy owner field)
			if (owner.ManagerId != null) {
				acct.Account_Owner_Manager__c = owner.ManagerId;
				if (fixedOwner != null) acct.Fixed_Owner_Manager__c = fixedOwner.ManagerId;
			} else {
				acct.Account_Owner_Manager__c = null;
				acct.Fixed_Owner_Manager__c = null;
			}
		}
		
		return true;
	}
}