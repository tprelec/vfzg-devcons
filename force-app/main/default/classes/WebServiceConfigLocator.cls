/**
 * @description      This class is responsible for retrieving the configuration for external webservices.
 * @author        Guy Clairbois
 */
public class WebServiceConfigLocator {
	/**
	 * @description      This inner class holds all of the information for connecting to the external
	 *            web service.
	 * @author        Guy Clairbois
	 */
	private static final string BOP_LAYER7_BASE_URL = 'callout:BOP_Layer7_NL_Credentials';

	private class WebServiceConfig implements IWebServiceConfig {
		private String username;

		private String password;

		private String endpoint;

		private String certificateName;

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getPassword() {
			return password;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public String getEndpoint() {
			return endpoint;
		}

		public void setEndpoint(String endpoint) {
			this.endpoint = endpoint;
		}

		public String getCertificateName() {
			return certificateName;
		}

		public void setCertificateName(String certificateName) {
			this.certificateName = certificateName;
		}
	}

	/**
	 * @description      This holds all of the stored configuration held in custom settings. The reason
	 *            it is a static property is because we don't want to keep retrieving the data unneccessarily.
	 */
	private static Map<String, External_WebService_Config__c> webserviceConfig {
		get {
			if (webserviceConfig == null) {
				webserviceConfig = External_WebService_Config__c.getAll();
			}
			return webserviceConfig;
		}
		set;
	}

	/**
	 * @description      This will retrieve from custom settings a data set with a matching name and will
	 *            instantiate a new web service configuration object and will populate it with the stored
	 *            configuration.
	 * @param  name    The name of the data set to use in custom settings.
	 * @return        New instance of a web service configuration class.
	 */
	public static IWebServiceConfig getConfig(String name) {
		return new WebServiceConfigLocator().getStoredConfig(name);
	}

	public static IWebServiceConfig getConfigNamedCredential(String name) {
		return new WebServiceConfigLocator().getNamedCredentialStoredConfig(name);
	}

	/**
	 * @description      This creates a new instance of the webservice config class. This then allows the
	 *            calling method to configure the object with requiring to fetch data from custom settings.
	 */
	public static IWebServiceConfig createConfig() {
		return new WebServiceConfig();
	}

	/**
	 * @description      This class should not be instantiated, this constructor is private to prevent
	 *            developers from doing this.
	 */
	private WebServiceConfigLocator() {
		// Class should not be instantiated
	}

	/**
	 * @description      This is an instance method to allow us to override this functionality in the future
	 *            if the need be ;)
	 * @param  name    The name of the data set to use in custom settings.
	 * @return        New instance of a web service configuration class.
	 */
	public IWebServiceConfig getStoredConfig(String name) {
		// Retrieve the stored config
		External_WebService_Config__c storedConfig = webserviceConfig.get(name);
		if (storedConfig == null) {
			throw new ExMissingDataException('Invalid data set name provided');
		}

		// Retrieve the prod ip and check if this is a production environment
		// This is done to double check and prevent sandbox messages from going to production
		External_WebService_Config__c prodIP = webserviceConfig.get('ECSProduction_IP'); // TODO: replace this by a map, to enable multiple prod ip's for different systems
		if (prodIP == null) {
			throw new ExMissingDataException('No custom setting for Production_IP provided');
		}

		if (
			storedConfig.URL__c.startsWith(prodIP.URL__c) &&
			// the 3 first characters identify the target system (e.g. 'ECS')
			storedConfig.Name.subString(0, 3) == prodIP.Name.substring(0, 3) &&
			!GeneralUtils.IsProduction()
		) {
			throw new ExInvalidConfigurationException('Attempt to export to production IP from non-production instance!');
			return null;
		}
		if (
			!storedConfig.URL__c.startsWith(prodIP.URL__c) &&
			// the 3 first characters identify the target system (e.g. 'ECS')
			storedConfig.Name.subString(0, 3) == prodIP.Name.substring(0, 3) &&
			GeneralUtils.IsProduction()
		) {
			throw new ExInvalidConfigurationException('Attempt to export to non-production IP from production instance!');
			return null;
		}

		// Create the web service config instance
		IWebServiceConfig config = new WebServiceConfig();

		config.setUsername(storedConfig.username__c);
		config.setPassword(storedConfig.password__c);
		config.setEndpoint(storedConfig.URL__c);
		config.setCertificateName(storedConfig.certificate_name__c);

		return config;
	}

	/*
	 * @description This method is replacing the getStoredConfig because is using a custom setting to configure endpoints.
	 *  by introduciny a compliance of security risk fix, the need is to add a security layer in the middle (Layer7)
	 *  so this method is using the new guidlines with Layer7 named credentials and Custom Metadata to configure the endpoint.
	 *  by using this new method config.setCertificateName are not needed
	 *  because this setting is using named credentials.
	 * @param the method will receive the name in string of the service to configure.
	 * @example the method will receive ('ECSSOAPCompany') and will return the class IWebServiceConfig containing complete url in String format.
	 *  in the endpoint attribute.
	 */
	public IWebServiceConfig getNamedCredentialStoredConfig(string name) {
		List<WebServices_Param_Settings__mdt> paramSettingWebservices = [
			SELECT Id, Label, DeveloperName, Partial_URL__c
			FROM WebServices_Param_Settings__mdt
			WHERE DeveloperName = :name
		];

		checkMetadataParams(paramSettingWebservices);
		//means vaidations passed
		String servicePath = paramSettingWebservices[0].Partial_URL__c;
		//create the endpoint with named credentials & custom metadata
		IWebServiceConfig config = new WebServiceConfig();
		config.setEndpoint(getServiceURL(servicePath));
		config.setUsername('{!$Credential.UserName}');
		config.setPassword('{!$Credential.Password}');
		return config;
	}

	private static String getServiceURL(String servicePath) {
		return BOP_LAYER7_BASE_URL + servicePath;
	}

	@testVisible
	private static void checkMetadataParams(List<WebServices_Param_Settings__mdt> mdtWebservice) {
		if (mdtWebservice.isEmpty()) {
			throw new ExInvalidConfigurationException('Attempted to configure an endpoint without settings');
		}

		//up to this point we should pick only one setting in the list
		if (String.isBlank(mdtWebservice[0].Partial_URL__c)) {
			throw new ExInvalidConfigurationException('There is no partial URL to configure endpoint');
		}
	}
}
