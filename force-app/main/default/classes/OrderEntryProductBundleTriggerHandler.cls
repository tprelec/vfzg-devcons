public with sharing class OrderEntryProductBundleTriggerHandler extends TriggerHandler {
	private List<OE_Product_Bundle__c> newOEProductBundles;
	private Map<Id, OE_Product_Bundle__c> newOEProductBundlesMap;
	private Map<Id, OE_Product_Bundle__c> oldOEProductBundlesMap;

	private void init() {
		newOEProductBundles = (List<OE_Product_Bundle__c>) this.newList;
		newOEProductBundlesMap = (Map<Id, OE_Product_Bundle__c>) this.newMap;
		oldOEProductBundlesMap = (Map<Id, OE_Product_Bundle__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	public void setExternalIds() {
		Sequence seq = new Sequence('OE_Product_Bundle');

		if (seq.canBeUsed()) {
			seq.startMutex();
			for (OE_Product_Bundle__c o : newOEProductBundles) {
				o.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}