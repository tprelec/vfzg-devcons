public with sharing class CustomerExportSias {

    public static Set<Id> ordersInExport {
        get {
            if (ordersInExport == null) {
                ordersInExport = new Set<Id>();
            }
            return ordersInExport;
        }
        set;
    }

    @future(callout=true)
    public static void sendCustomerNotificationOffline(Id orderId) {
        sendCustomerNotification(orderId);
    }

    public static void sendCustomerNotification(Id orderId) {
        // fetch the relevant order
        Order__c o = [
            SELECT
                Name,
                Account__c,
                VF_Contract__c,
                Mobile_Fixed__c,
                OrderType__r.ExportSystem__c
            FROM
                Order__c
            WHERE
                Id = :orderId
        ];

        if (o == null) { return; }

        // replacing the SOAP message generation by a simple XML string, as SFDC does not support SIAS's multiple namespaces
        IWebServiceConfig iwsc;
        if (!Test.isRunningTest()) {
            iwsc =  WebServiceConfigLocator.getConfig('AmdocsSiasCustomer');
        } else {
            iwsc = WebServiceConfigLocator.createConfig();
            iwsc.setEndpoint('test.example.com');
        }

        String soapString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Header/><soapenv:Body><cust:CreateCustomer xmlns:cust="http://amdocs/sias/cust"> <sias:ROOT  xmlns:sias="http://amdocs/sias/cust/SiasCreateCustomerInput"><Event></Event><DATA>';
        soapString += '<AccountID>' + o.Account__c + '</AccountID>';
        soapString += '<ContractID>' + o.VF_Contract__c + '</ContractID>';
        soapString += '<OrderID>' + o.Name + '</OrderID>';
        // only export fixed customers to BOPc
        soapString += '<OssCode>' + (o.OrderType__r.ExportSystem__c == 'BOP' ? o.OrderType__r.ExportSystem__c : 'SIAS') + '</OssCode>';
        soapString += '</DATA></sias:ROOT></cust:CreateCustomer></soapenv:Body></soapenv:Envelope>';

        HttpRequest req = new HttpRequest();
        System.debug(LoggingLevel.DEBUG, soapString);
        req.setBody(soapString);
        req.setMethod('POST');
        req.setEndpoint(iwsc.getEndpoint());
        if (iwsc.getCertificateName() != null) {
            req.setClientCertificateName(iwsc.getCertificateName());
        }
        req.setHeader('Content-Type', 'application/soap+xml');
        req.setTimeout(120000);

        Http http = new Http();
        HttpResponse res;
        Boolean success = false;

        // having problems with callouts in tests..
        try {
            if (Test.isRunningTest()) {
                TestUtilMultiRequestMock.SingleRequestMock sm = new TestUtilMultiRequestMock.SingleRequestMock(
                    200, 'Complete', '', null
                );
                res = sm.respond(req);
            } else {
                res = http.send(req);
            }
            System.debug(LoggingLevel.DEBUG, res);
            System.debug(LoggingLevel.DEBUG, res.getBody());
            success = true;
        } catch (ExWebServiceCalloutException ce) {
            // put any errors in the errormessage field as a 'dump' to provie as much info as possible..
            o.Unify_Customer_Export_Errormessage__c = (res + res.getBody()).abbreviate(255);
        }

        if (success) {
            String resultString = res.getBody();
            if (resultString.contains('<Code>')) {
                String description = resultString.subString(resultString.indexOf('<Description>') + 13, resultString.indexOf('</Description>'));
                String errorCode = resultString.subString(resultString.indexOf('<Code>') + 6, resultString.indexOf('</Code>'));

                if (errorCode != 'SIAS000') {
                    o.Unify_Customer_Export_Errormessage__c = errorCode + description.abbreviate(245);
                }
            } else if (resultString.contains('<Status>')) {
                o.Unify_Customer_Export_Errormessage__c = resultString.subString(resultString.indexOf('<Status>') + 8, resultString.indexOf('</Status>')).abbreviate(255);
            } else {
                // in case no expected structure is returned, assume an error happened
                // put any errors in the errormessage field as a 'dump' to provie as much info as possible..
                o.Unify_Customer_Export_Errormessage__c = (res + res.getBody()).abbreviate(255);
            }
        }

        SharingUtils.updateRecordsWithoutSharing(new List<Order__c>{o});
    }

}