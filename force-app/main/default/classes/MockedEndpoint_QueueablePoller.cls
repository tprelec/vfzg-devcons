public class MockedEndpoint_QueueablePoller implements Queueable, Database.AllowsCallouts {
	public enum AsyncImplementation {
		CDO_ASYNC,
		CDO_DATAPROP,
		DXL
	}

	private AsyncImplementation asyncImpl;
	private Integer delay;
	private DateTime startingTimer;
	private RestRequest originalRequest;

	/**
	 * @description MockedEndpoint_QueueablePoller description
	 * @param  asyncImplementation asyncImplementation which async system needs to be triggered
	 * @param  delay               delay how many seconds need to pass before triggering the async notification
	 */
	public MockedEndpoint_QueueablePoller(AsyncImplementation asyncImpl, Integer delay) {
		this.asyncImpl = asyncImpl;
		this.delay = delay;
		this.startingTimer = DateTime.now();
	}

	/**
	 * @description MockedEndpoint_QueueablePoller description
	 * @param  asyncImpl asyncImplementation which async system needs to be triggered
	 * @param  delay               delay how many seconds need to pass before triggering the async notification
	 * @param  originalRequest     original request sent to external system, usually the external system just updates the same as async.
	 */
	public MockedEndpoint_QueueablePoller(AsyncImplementation asyncImpl, Integer delay, RestRequest originalRequest) {
		this(asyncImpl, delay);
		this.originalRequest = originalRequest;
	}

	/**
	 * @description MockedEndpoint_QueueablePoller description
	 * @param  asyncImpl            asyncImplementation which async system needs to be triggered
	 * @param  delay               delay how many seconds need to pass before triggering the async notification
	 * @param  startingTimer       to be used for calculation if async needs to be triggered
	 */
	public MockedEndpoint_QueueablePoller(AsyncImplementation asyncImpl, Integer delay, RestRequest originalRequest, DateTime startingTimer) {
		this(asyncImpl, delay, originalRequest);
		this.startingTimer = startingTimer;
	}

	public void execute(QueueableContext qc) {
		if (delayExceeded()) {
			triggerAsyncImplementation(asyncImpl);
		} else {
			System.enqueueJob(new MockedEndpoint_QueueablePoller(asyncImpl, delay, originalRequest, startingTimer));
		}
	}

	private Boolean delayExceeded() {
		return DateTime.now() > startingTimer.addSeconds(delay);
	}

	private void triggerAsyncImplementation(AsyncImplementation asyncImpl) {
		switch on asyncImpl {
			when CDO_ASYNC {
				sendRequest('mockedEndpoints/tboCdoNotificationsListener', cdoAsyncRequest());
				System.enqueueJob(new MockedEndpoint_QueueablePoller(AsyncImplementation.CDO_DATAPROP, 120));
			}
			when CDO_DATAPROP {
				sendRequest('mockedEndpoints/tboCdoNotificationsListener', cdoDataPropagationRequest());
			}
			when DXL {
				sendRequest('mockedEndpoints/tboCdoNotificationsListener', dxlAsyncRequest());
			}
		}
	}

	private void sendRequest(String notificationEndpoint, String requestBody) {
		String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm();
		String restAPIURL = sfdcURL + '/services/apexrest/' + notificationEndpoint;
		HttpRequest httpRequest = new HttpRequest();
		httpRequest.setMethod('POST');
		httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
		httpRequest.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
		httpRequest.setEndpoint(restAPIURL);
		httpRequest.setBody(requestBody);
		Http http = new Http();
		HttpResponse httpResponse = http.send(httpRequest);
	}

	private String cdoAsyncRequest() {
		//TODO implemente processing the originalRequest
		return '{returned COMPLETE SUCCESS}';
	}

	private String cdoDataPropagationRequest() {
		//TODO implement
		return '{returned IP, SERVICEID, ACCESS_ID, MAC_ADDRESS}';
	}

	private String dxlAsyncRequest() {
		//TODO implement
		return '{CUSTOMER PROVISIONED}';
	}
}
