/**
 * Test class for DeliveryExport.cls
 *
 * @author: Jurgen van Westreenen
 */
@IsTest
public class TestDeliveryExport {
	@IsTest
	private static void validateSuccessResponse() {
		TriggerHandler.preventRecursiveTrigger('OrderTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);

		String orderId = prepareTestData();
		Set<Id> orderIds = new Set<Id>();
		orderIds.add(orderId);

		Test.startTest();
		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = orderId;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = 'Acq'; // Required and fixed for BOP
		cp.VF_Contract__c = [SELECT Id FROM VF_Contract__c LIMIT 1].Id;
		cp.Customer_Asset__c = [SELECT Id FROM Customer_Asset__c LIMIT 1].Id;
		cp.Product__c = [SELECT Id FROM Product2 LIMIT 1].Id;
		cp.PBX__c = [SELECT Id FROM Competitor_Asset__c LIMIT 1].Id;
		insert cp;

		Map<Id, Contracted_Products__c> cpMap = new Map<Id, Contracted_Products__c>([SELECT Id FROM Contracted_Products__c LIMIT 1]);

		// create custom setting with integration data
		External_WebService_Config__c config = new External_WebService_Config__c(
			Name = 'ECSSOAPDelivery',
			URL__c = 'http://example.com/example/test',
			Username__c = 'username',
			Password__c = 'password'
		);
		insert config;

		Test.setMock(WebServiceMock.class, new ECSSOAPDeliveryMock());
		// DeliveryExport.exportDeliveries(orderIds, 'update');
		DeliveryExport deliveryExportObject = new DeliveryExport(orderIds, cpMap.keySet(), 'update');
		deliveryExportObject.exportDeliveries();
		Test.stopTest();
	}

	private static String prepareTestData() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);

		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test');
		insert c;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, c.Id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);

		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct();
		insert product;

		Opportunity newOpp = [SELECT Id, Ban__c FROM Opportunity LIMIT 1];
		newOpp.Ban__c = ban.Id;
		update newOpp;

		Order__c ord = new Order__c();
		ord.O2C_Order__c = true;
		ord.Sales_Order_Id__c = '12345';
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = contr.Id;
		ord.Account__c = acct.Id;
		insert ord;

		Customer_Asset__c ca = new Customer_Asset__c();
		ca.Billing_Arrangement__c = ba.Id;
		ca.Order__c = ord.Id;
		ca.Site__c = site.Id;
		insert ca;

		Competitor_Asset__c compAsset = new Competitor_Asset__c();
		insert compAsset;

		return ord.Id;
	}
}
