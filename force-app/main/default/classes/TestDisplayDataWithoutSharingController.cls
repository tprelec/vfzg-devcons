@isTest
private class TestDisplayDataWithoutSharingController {
	@isTest
	static void testFetchingData() {
		Pagereference dataWithoutSharingPage = Page.DisplayDataWithoutSharing;
		String lowerCaseField = 'id';

		Test.startTest();
		Test.setCurrentPageReference(dataWithoutSharingPage);
		ApexPages.currentPage().getParameters().put('dwsname', 'unitTest');
		ApexPages.currentPage().getParameters().put('field', lowerCaseField);

		DisplayDataWithoutSharingController dataWithoutSharing = new DisplayDataWithoutSharingController();
		Test.stopTest();

		System.assert(String.isNotEmpty(dataWithoutSharing.columnNamesJson), 'Column name should exist for unitTest DataWithoutSharing');
		System.assert(String.isNotEmpty(dataWithoutSharing.columnReferencesJson), 'Column reference should exist for unitTest DataWithoutSharing');
		System.assert(String.isNotEmpty(dataWithoutSharing.objectsJson), 'Data should exist for unitTest DataWithoutSharing (user)');
		List<Object> userDataJson = (List<Object>) JSON.deserializeUntyped(dataWithoutSharing.objectsJson);
		Map<String, Object> firstRecord = (Map<String, Object>) userDataJson[0];
		System.assertEquals(1, userDataJson.size(), 'Expected to select exactly 1 user using unitTest DataWithoutSharing custom metadata type.');
		Boolean containsSelectedField = false;
		for (String userDataJsonKey : firstRecord.keySet()) {
			if (lowerCaseField == userDataJsonKey.toLowerCase()) {
				containsSelectedField = true;
				break;
			}
		}
		System.assert(containsSelectedField, 'User data should contain field: ' + lowerCaseField);
	}
}
