@IsTest
private class TestCS_COMProvisionComponentController {
@IsTest
    static void testCloseTask() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(simpleUser, false);
            vfContract.Contract_Cleaning_Result__c = 'Clean';
            vfContract.Responsible_Quality_Officer__c = simpleUser.Id;
            insert vfContract;

            Task testTask = CS_DataTest.createTask('FQC', 'COM Delivery', simpleUser, false);
            testTask.Status = 'Open';
            insert testTask;

            CS_COMProvisionComponentController.closeTask(testTask.Id);

            List<Task> taskAfter = new List<Task>([
                    SELECT Id, Status, Delivery_Task_Finished__c
                    FROM Task
                    WHERE Id = :testTask.Id
            ]);

            System.assertEquals('Completed', taskAfter[0].Status);
            System.assertEquals(true, taskAfter[0].Delivery_Task_Finished__c);
        }
    }
}