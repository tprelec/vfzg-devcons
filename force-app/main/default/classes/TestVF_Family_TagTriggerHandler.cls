@isTest
private class TestVF_Family_TagTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'VF Family Tag';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'VFFT-08-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		VF_Family_Tag__c ft = new VF_Family_Tag__c();
		ft.Name = 'Test';
		insert ft;

		VF_Family_Tag__c queryRecord = [SELECT Id, ExternalID__c FROM VF_Family_Tag__c LIMIT 1];
		System.assert(queryRecord.ExternalID__c != null, 'Family Tag External Id should exist.');

		Test.stopTest();
	}
}
