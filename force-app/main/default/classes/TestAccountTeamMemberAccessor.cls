/**
 * @description:    Test class for AccountTeamMemberAccessor
 **/
@isTest
public class TestAccountTeamMemberAccessor {
	private static final String TEST_FAKE_ACCOUNT_TEAM_MEMBER_ID = TestUtils.getFakeId(AccountTeamMember.SObjectType);
	private static final String TEST_FAKE_ACCOUNT_TEAM_MEMBER_USER_ID = TestUtils.getFakeId(User.SObjectType);
	private static final String TEST_FAKE_ACCOUNT_TEAM_MEMBER_ACCOUNT_ID = TestUtils.getFakeId(Account.SObjectType);
	private static final String TEST_ACCOUNT_TEAM_MEMBER_TEAM_MEMBER_ROLE_CHANNEL_MANAGER = Constants.ACCOUNT_TEAM_MEMBER_ROLE_CHANNEL_MANAGER;

	private static final Integer NUMBER_OF_TIMES_TO_CALL_ACCESSOR_IN_SAME_TRANSACTION = 5;

	/**
	 * @description:    Tests retrieving AccountTeamMember for account id and
	 *                  team member role when it exists
	 **/
	@isTest
	static void shouldRetrieveAccountTeamMemberForAccountIdAndTeamMemberRoleWhenItExists() {
		// prepare data
		AccountTeamMember createdAccountTeamMember = createTestAccountTeamMember();

		// perform testing
		Test.startTest();
		AccountTeamMemberAccessor.initAccountTeamMemberMapping(new List<AccountTeamMember>{ createdAccountTeamMember });

		AccountTeamMember retrievedAccountTeamMember = AccountTeamMemberAccessor.getAccountTeamMemberForAccountIdAndTeamMemberRole(
			createdAccountTeamMember.AccountId,
			TEST_ACCOUNT_TEAM_MEMBER_TEAM_MEMBER_ROLE_CHANNEL_MANAGER
		);
		Test.stopTest();

		// verify results
		System.assertEquals(createdAccountTeamMember.Id, retrievedAccountTeamMember.Id, 'AccountTeamMember with proper Id field should be retrieved');
		System.assertEquals(
			createdAccountTeamMember.UserId,
			retrievedAccountTeamMember.UserId,
			'AccountTeamMember with proper UserId field should be retrieved'
		);
		System.assertEquals(
			createdAccountTeamMember.AccountId,
			retrievedAccountTeamMember.AccountId,
			'AccountTeamMember with proper AccountId field should be retrieved'
		);
		System.assertEquals(
			createdAccountTeamMember.TeamMemberRole,
			retrievedAccountTeamMember.TeamMemberRole,
			'AccountTeamMember with proper TeamMemberRole field should be retrieved'
		);
	}

	/**
	 * @description:    Tests retrieving null AccountTeamMember for account id and
	 *                  team member role when it does not exist
	 **/
	@isTest
	static void shouldRetrieveNullAccountTeamMemberForAccountIdAndTeamMemberRoleWhenItDoesNotExist() {
		// prepare data

		// perform testing
		Test.startTest();
		AccountTeamMember retrievedAccountTeamMember = AccountTeamMemberAccessor.getAccountTeamMemberForAccountIdAndTeamMemberRole(
			TEST_FAKE_ACCOUNT_TEAM_MEMBER_ACCOUNT_ID,
			TEST_ACCOUNT_TEAM_MEMBER_TEAM_MEMBER_ROLE_CHANNEL_MANAGER
		);
		Test.stopTest();

		// verify results
		System.assertEquals(null, retrievedAccountTeamMember, 'Null value AccountTeamMember should be retrieved');
	}

	/**
	 * @description:    Tests doing only 1 SOQL when accessor is called
	 *                  multiple times in same transaction
	 **/
	@isTest
	static void shouldDoOneSOQLWhenCalledMultipleTimesInSameTransaction() {
		// prepare data
		AccountTeamMember createdAccountTeamMember = createTestAccountTeamMember();

		// perform testing
		Test.startTest();
		AccountTeamMemberAccessor.initAccountTeamMemberMapping(new List<AccountTeamMember>{ createdAccountTeamMember });

		for (Integer i = 0; i < NUMBER_OF_TIMES_TO_CALL_ACCESSOR_IN_SAME_TRANSACTION; i++) {
			AccountTeamMember retrievedAccountTeamMember = AccountTeamMemberAccessor.getAccountTeamMemberForAccountIdAndTeamMemberRole(
				createdAccountTeamMember.AccountId,
				TEST_ACCOUNT_TEAM_MEMBER_TEAM_MEMBER_ROLE_CHANNEL_MANAGER
			);
		}

		Integer queries = Limits.getQueries();
		Test.stopTest();

		// verify results
		System.assertEquals(1, queries, 'Only 1 SOQL query should be done');
	}

	/**
	 * @description:    Creates test AccountTeamMember
	 * @return          AccountTeamMember - created AccountTeamMember
	 **/
	private static AccountTeamMember createTestAccountTeamMember() {
		return new AccountTeamMember(
			Id = TEST_FAKE_ACCOUNT_TEAM_MEMBER_ID,
			UserId = TEST_FAKE_ACCOUNT_TEAM_MEMBER_USER_ID,
			AccountId = TEST_FAKE_ACCOUNT_TEAM_MEMBER_ACCOUNT_ID,
			TeamMemberRole = TEST_ACCOUNT_TEAM_MEMBER_TEAM_MEMBER_ROLE_CHANNEL_MANAGER
		);
	}
}
