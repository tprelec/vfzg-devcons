public with sharing class CS_MFStatusUpdateLeadTime {
    private static final String CASE_RECORD_TYPE_CS_MF_PROJECT_INTAKE_AND_PREPARATON = 'CS_MF_Project_Intake_and_Preparation';
    private static final string CASE_RECORD_TYPE_CS_MF_FINAL_QUALITY_CHECK = 'CS_MF_Final_Quality_Check';

    @InvocableMethod(label = 'Calc KPI Lead Time' description='Calculates the KPI Lead time for Mobile business' category='Mobile')
    public static void calculateLeadTime(List<Id> caseList) {
        if (!caseList.isEmpty()) {
            Set<Id> contractIdsSet = new Set<Id>();
            for (Case objCase: [SELECT Contract_VF__c FROM Case WHERE Id IN :caseList]) {

                if(objCase.Contract_VF__c != null) {
                    contractIdsSet.add(objCase.Contract_VF__c);
                }
            }

            for(Case objCase: [
                SELECT Contract_VF__c FROM Case
                WHERE RecordType.DeveloperName = :CASE_RECORD_TYPE_CS_MF_FINAL_QUALITY_CHECK
                AND Contract_VF__c IN :contractIdsSet
                AND Id NOT IN :caseList]) {

                contractIdsSet.remove(objCase.Contract_VF__c);
            }

            if(!contractIdsSet.isEmpty()) {
                List<VF_Contract__c> lContracts = new List<VF_Contract__c>();
                for(Case objCase: [
                    SELECT Id, Contract_VF__c, Case_Start_Date__c
                    FROM Case WHERE Contract_VF__c IN :contractIdsSet
                    AND RecordType.DeveloperName = :CASE_RECORD_TYPE_CS_MF_PROJECT_INTAKE_AND_PREPARATON
                ]) {
                    Date dToday = Date.today();
                    Integer iLeadTime = objCase.Case_Start_Date__c.daysBetween(dToday);
                    lContracts.add(new VF_Contract__c(Id = objCase.Contract_VF__c, LeadTime__c = iLeadTime, Leadtime_Date__c = dToday));
                }
                update lContracts;
            }
        }
    }
}