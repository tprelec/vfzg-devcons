@IsTest
private class AddMobileProductsJourneyHandlerTest {
	private static User portalUser;
	private static Account accountVisible;
	private static Account accountNotVisible;

	private static void createTestData() {
		portalUser = PP_TestUtils.createPortalUser();

		TestUtils.autoCommit = false;

		List<Account> accountsToInsert = new List<Account>();
		accountVisible = TestUtils.createAccount(portalUser);
		accountVisible.Name = 'Customer';
		accountVisible.Count_Active_Assets__c = 1;
		accountsToInsert.add(accountVisible);

		accountNotVisible = TestUtils.createAccount(portalUser);
		accountNotVisible.Name = 'CustomerTest';
		accountNotVisible.Count_Active_Assets__c = 0;
		accountsToInsert.add(accountNotVisible);
		insert accountsToInsert;
	}

	@IsTest
	static void testCheckVisibilityTrue() {
		createTestData();

		Boolean checkVisibilityResult;

		System.runAs(portalUser) {
			AddMobileProductsJourneyHandler addMobilProductsJourneyHandler = new AddMobileProductsJourneyHandler();
			checkVisibilityResult = addMobilProductsJourneyHandler.checkVisibility(
				accountVisible.Id
			);
		}

		System.assertEquals(true, checkVisibilityResult, 'Expected checkVisibility to be true.');
	}

	@IsTest
	static void testCheckVisibilityFalse() {
		createTestData();

		Boolean checkVisibilityResult;

		System.runAs(portalUser) {
			AddMobileProductsJourneyHandler addMobilProductsJourneyHandler = new AddMobileProductsJourneyHandler();
			checkVisibilityResult = addMobilProductsJourneyHandler.checkVisibility(
				accountNotVisible.Id
			);
		}

		System.assertEquals(false, checkVisibilityResult, 'Expected checkVisibility to be false.');
	}

	@IsTest
	static void testHandleClick() {
		createTestData();

		Map<String, Object> handleClickResult = new Map<String, Object>();

		System.runAs(portalUser) {
			AddMobileProductsJourneyHandler addMobilProductsJourneyHandler = new AddMobileProductsJourneyHandler();
			handleClickResult = addMobilProductsJourneyHandler.handleClick(accountVisible.Id);
		}

		System.assertEquals(
			true,
			handleClickResult.keySet().size() > 0,
			'Expected handleClickResult to have some results in Map.'
		);
	}
}