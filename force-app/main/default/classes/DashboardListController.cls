/**
 * @description       :
 * @author            : mcubias
 * @group             : leads united
 * @last modified on  : 23-09-2022
 * @last modified by  :
 **/
public with sharing class DashboardListController {
	/**
	 * @description
	 * @author mcubias | 23-09-2022
	 * @param queryLimit
	 * @param offset
	 * @param folderId
	 * @return List<Dashboard>
	 **/
	@AuraEnabled
	public static List<Dashboard> getAllDashboards(Integer queryLimit, Integer offset, String folderId) {
		List<String> soql = new List<String>{ 'SELECT Id, FolderName, Title, Description, LastViewedDate FROM Dashboard', 'WHERE IsDeleted = FALSE' };
		if (String.isNotBlank(folderId)) {
			soql.add('AND FolderId = :folderId');
		}
		soql.add('ORDER BY Title LIMIT :queryLimit OFFSET :offset');
		return Database.query(String.join(soql, ' '));
	}

	/**
	 * @description
	 * @author mcubias | 26-09-2022
	 * @param folderId
	 * @return Integer
	 **/
	@AuraEnabled
	public static Integer getTotalDashboards(String folderId) {
		List<String> soql = new List<String>{ 'SELECT COUNT() FROM Dashboard WHERE IsDeleted = FALSE' };
		if (String.isNotBlank(folderId)) {
			soql.add('AND FolderId = :folderId');
		}
		return Database.countQuery(String.join(soql, ' '));
	}

	/**
	 * @description
	 * @author mcubias | 26-09-2022
	 * @return List<Folder>
	 **/
	@AuraEnabled
	public static List<Folder> getAllDashboardFolders() {
		return [SELECT Id, Name, DeveloperName FROM Folder WHERE Type IN ('Dashboard') ORDER BY Name];
	}
}
