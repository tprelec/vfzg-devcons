/**
 * @description       : Test class for Batch apex to mark the Vodafone Customer as Inactive when all the Blling Arrangments related to a customer are closed.
 * @author            : Saurabh
 * @group             : Leads United
 * @last modified on  : 09-13-2022
 * @last modified by  : Saurabh
 **/

@isTest
public class TestInactiveCustomerUpdateBatch {
	@testSetup
	static void setup() {
		User owner = TestUtils.createAdministrator();
		Account acct1 = TestUtils.createAccount(owner);
		Contact cont1 = TestUtils.createContact(acct1);
		Ban__c ban1 = TestUtils.createBan(acct1);
		Financial_Account__c fa1 = TestUtils.createFinancialAccount(ban1, cont1.Id);
		Billing_Arrangement__c bar1 = TestUtils.createBillingArrangement(fa1);
		Billing_Arrangement__c bar2 = TestUtils.createBillingArrangement(fa1);
	}
	@isTest
	static void testActiveCustomer() {
		List<Billing_Arrangement__c> listToUpdate = new List<Billing_Arrangement__c>();
		List<Billing_Arrangement__c> barlist = [SELECT id, Status__c FROM Billing_Arrangement__c];
		for (Billing_Arrangement__c ba : barlist) {
			ba.Status__c = 'Open';
			listToUpdate.add(ba);
		}
		update listToUpdate;
		test.startTest();
		InactiveCustomerUpdateBatch updateAcct = new InactiveCustomerUpdateBatch();
		Id batchApexCommId = Database.executeBatch(updateAcct);
		test.stopTest();
		Account Acc = [SELECT Id, Updated_Customer_Status__c FROM Account WHERE Updated_Customer_Status__c = 'Active'];
		system.assertEquals('Active', acc.Updated_Customer_Status__c);
	}
	@isTest
	static void testInActiveCustomer() {
		List<Billing_Arrangement__c> listToUpdate = new List<Billing_Arrangement__c>();
		List<Billing_Arrangement__c> barlist = [SELECT id, Status__c FROM Billing_Arrangement__c];
		for (Billing_Arrangement__c ba : barlist) {
			ba.Status__c = 'Closed';
			listToUpdate.add(ba);
		}
		update listToUpdate;
		test.startTest();
		InactiveCustomerUpdateBatch updateAcct = new InactiveCustomerUpdateBatch();
		Id batchApexCommId = Database.executeBatch(updateAcct);
		test.stopTest();
		Account Acc = [SELECT Id, Updated_Customer_Status__c FROM Account WHERE Updated_Customer_Status__c = 'Inactive'];
		system.assertEquals('Inactive', acc.Updated_Customer_Status__c);
	}
	@isTest
	static void testScheduleJob() {
		String jobId = System.schedule('InactiveCustomerUpdateBatchTest', '0 0 23 * * ?', new InactiveCustomerUpdateBatch());
		CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
		// Verify the expressions are the same
		System.assertEquals('0 0 23 * * ?', ct.CronExpression);
	}
}
