/**
 * @description       : SFLO : 200 Test class Trigger Util Class
 * @author            : Saurabh
 * @group             : LeadsUnited
 * @last modified on  : 03-07-2022
 * @last modified by  : Saurabh
 **/
@isTest
public class CreateWinbackLeadFromOpportunityTest {
	@TestSetup
	private static void createTestData() {
		TestUtils.autoCommit = true;
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.LG_PostalPostalCode__c = '3082CW';
		update acc;
		TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
	}
	@isTest
	private static void testWinbackLeads() {
		Opportunity opp = [
			SELECT
				id,
				Name,
				StageName,
				LG_AccntPostalPostalCode__c,
				LG_NEWSalesChannel__c,
				DTV_Customer__c,
				AccountId,
				recordTypeId,
				Internet_Customer__c,
				Telephony_Customer__c
			FROM Opportunity
			LIMIT 1
		];
		Account acc = [SELECT id FROM Account LIMIT 1];
		acc.Vodafone_Customer__c = true;
		acc.LG_ReadyForService__c = 'True';
		acc.LG_PostalCity__c = 'Amsterdam';
		acc.Billing_Street__c = 'Test Street';
		//acc.LG_PostalPostalCode__c = '3082CW';
		update acc;
		system.debug('Updated account is');
		List<Opportunity> oppList = new List<Opportunity>();
		if (opp != null) {
			opp.Name += ' Winback';
			opp.StageName = 'Closed Lost';
			opp.LG_NEWSalesChannel__c = 'D2D';
			opp.DTV_Customer__c = true;
			opp.Internet_Customer__c = true;
			opp.Telephony_Customer__c = true;
			opp.AccountId = acc.Id;

			update opp;
			oppList.add(opp);
		}
		Test.startTest();
		CreateWinbackLeadFromOpportunity.createWinbackLeads(oppList);
		Test.stopTest();
		List<Lead> winbackLead = [SELECT id FROM Lead WHERE RecordType.DeveloperName = 'CSADetails' LIMIT 1];
		system.assert(winbackLead != null, 'WinbackLead Created');
	}
}
