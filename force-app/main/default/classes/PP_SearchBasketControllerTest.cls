@isTest
private class PP_SearchBasketControllerTest {
	
	@isTest static void test_method_one() {

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());

        Framework__c f = Framework__c.getOrgDefaults();
        f.Framework_Sequence_Number__c = 1;
        upsert f;        

        String basketId = PP_SearchBasketController.createBasket(opp.Id);
        List<cscfga__Product_Basket__c> baskets = PP_SearchBasketController.getBaskets(opp.Id);
        System.assertEquals(baskets.size(), 1);

        String basketError = PP_SearchBasketController.createBasket(null);
        System.assertEquals(basketError, 'Error');
	}
	
}