public with sharing class BatchErrorEventTriggerHandler {
	public void handleBatchErrors(List<BatchApexErrorEvent> batchErrorList) {
		new ExceptionHandlingService().handleBatchErrors(batchErrorList);
	}
}