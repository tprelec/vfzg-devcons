public with sharing class COM_DXLCreateCustomerListener {
	private COM_DXLServiceAsyncResponse payload;
	public Boolean payloadValid {
		get {
			return payloadIsValid();
		}
	}

	public COM_DXLCreateCustomerListener(COM_DXLServiceAsyncResponse payload) {
		this.payload = payload;
	}

	public void processPayload() {
		List<COM_DXL_Integration_Log__c> integrationLogRecordList = COM_DXLNotificationListenerService.getIntegrationLog(
			this.payload.payload.transactionId
		);

		processNewProvide(this.payload, integrationLogRecordList[0]);
		updateTransactionLogRecord(integrationLogRecordList[0], COM_DXL_Constants.COM_DXL_PROCESSED_STATUS, '');
		updateOrderStatus(this.payload, integrationLogRecordList[0]);
	}

	@TestVisible
	private void processNewProvide(COM_DXLServiceAsyncResponse response, COM_DXL_Integration_Log__c integrationLogRecord) {
		List<COM_DxlAffectedObjects> affectedObjects = COM_DXLNotificationListenerService.getAffectedObjects(
			integrationLogRecord.Affected_Objects__c
		);

		List<SObject> dataList = new List<SObject>();

		dataList.addAll(createExternalAccounts(response, affectedObjects));
		dataList.add(updateAccountRecord(affectedObjects));
		dataList.add(updateBanRecord(response, affectedObjects));
		dataList.add(updateBillingArangement(response, affectedObjects));
		dataList.add(updateFinancialAccount(response, affectedObjects));

		if (!dataList.isEmpty()) {
			upsert dataList;
		}

		List<External_Account__c> externalAccounts = COM_DXLNotificationListenerService.getExternalAccounts(
			COM_DXLNotificationListenerService.getAffectedObjectByType(COM_DXL_Constants.COM_DXL_ACCOUNT_API_NAME, affectedObjects)[0].sfdcId
		);

		dataList.clear();
		dataList.addAll(createExternalContacts(response, externalAccounts));
		dataList.addAll(createExternalSites(response, externalAccounts));
		if (!dataList.isEmpty()) {
			insert dataList;
		}
	}

	@TestVisible
	private List<External_Account__c> createExternalAccounts(COM_DXLServiceAsyncResponse response, List<COM_DxlAffectedObjects> affectedObjects) {
		List<External_Account__c> returnValue = new List<External_Account__c>();

		External_Account__c externalAccountUnify = new External_Account__c();
		externalAccountUnify.Account__c = COM_DXLNotificationListenerService.getAffectedObjectByType(
				COM_DXL_Constants.COM_DXL_ACCOUNT_API_NAME,
				affectedObjects
			)[0]
			.sfdcId;
		externalAccountUnify.External_Source__c = COM_DXL_Constants.COM_DXL_EXTERNAL_SOURCE_UNIFY;
		externalAccountUnify.External_Account_Id__c = response.payload.customerDetails.companyDetails.unifyAccountRefId;
		returnValue.add(externalAccountUnify);

		External_Account__c externalAccountBop = new External_Account__c();
		externalAccountBop.Account__c = COM_DXLNotificationListenerService.getAffectedObjectByType(
				COM_DXL_Constants.COM_DXL_ACCOUNT_API_NAME,
				affectedObjects
			)[0]
			.sfdcId;
		externalAccountBop.External_Source__c = COM_DXL_Constants.COM_DXL_EXTERNAL_SOURCE_BOP;
		externalAccountBop.External_Account_Id__c = response.payload.customerDetails.companyDetails.bopCompanyCode;
		returnValue.add(externalAccountBop);

		return returnValue;
	}

	@TestVisible
	private Account updateAccountRecord(List<COM_DxlAffectedObjects> affectedObjects) {
		Account accountRecord = new Account();
		accountRecord.Id = COM_DXLNotificationListenerService.getAffectedObjectByType(COM_DXL_Constants.COM_DXL_ACCOUNT_API_NAME, affectedObjects)[0]
			.sfdcId;
		accountRecord.BOP_Export_Datetime__c = Datetime.now();

		return accountRecord;
	}

	@TestVisible
	private Ban__c updateBanRecord(COM_DXLServiceAsyncResponse response, List<COM_DxlAffectedObjects> affectedObjects) {
		Ban__c banRecord = new Ban__c();
		banRecord.Id = COM_DXLNotificationListenerService.getAffectedObjectByType(COM_DXL_Constants.COM_DXL_BAN_API_NAME, affectedObjects)[0].sfdcId;
		banRecord.Unify_Ref_Id__c = response.payload.customerDetails.billingCustomer.unifyBillingCustomerRefId;
		return banRecord;
	}

	@TestVisible
	private Billing_Arrangement__c updateBillingArangement(COM_DXLServiceAsyncResponse response, List<COM_DxlAffectedObjects> affectedObjects) {
		Billing_Arrangement__c billingArrangement = new Billing_Arrangement__c();
		billingArrangement.Id = COM_DXLNotificationListenerService.getAffectedObjectByType(
				COM_DXL_Constants.COM_DXL_BILLING_ARRANGEMENT_API_NAME,
				affectedObjects
			)[0]
			.sfdcId;
		billingArrangement.Unify_Ref_Id__c = response.payload.billingEntities.unifyBillingAccountRefId;

		return billingArrangement;
	}

	@TestVisible
	private Financial_Account__c updateFinancialAccount(COM_DXLServiceAsyncResponse response, List<COM_DxlAffectedObjects> affectedObjects) {
		Financial_Account__c financialAccount = new Financial_Account__c();
		financialAccount.id = COM_DXLNotificationListenerService.getAffectedObjectByType(
				COM_DXL_Constants.COM_DXL_FINANCIAL_ACCOUNT_API_NAME,
				affectedObjects
			)[0]
			.sfdcId;
		financialAccount.Unify_Ref_Id__c = response.payload.billingEntities.unifyFinancialAccountRefId;

		return financialAccount;
	}

	@TestVisible
	private List<External_Contact__c> createExternalContacts(COM_DXLServiceAsyncResponse response, List<External_Account__c> externalAccounts) {
		List<External_Contact__c> returnValue = new List<External_Contact__c>();

		for (COM_DXLServiceAsyncResponse.Contact contactRecord : response.payload.contacts) {
			External_Contact__c externalContact = new External_Contact__c();
			externalContact.ExternalId__c = contactRecord.unifyContactRefId;
			externalContact.External_Source__c = COM_DXL_Constants.COM_DXL_EXTERNAL_SOURCE_UNIFY;
			externalContact.Contact__c = contactRecord.salesforceContactId;
			externalContact.External_Account__c = COM_DXLNotificationListenerService.getExternalAccountsByType(
				externalAccounts,
				COM_DXL_Constants.COM_DXL_EXTERNAL_SOURCE_UNIFY
			)[0];
			returnValue.add(externalContact);
		}

		return returnValue;
	}

	@TestVisible
	private List<External_Site__c> createExternalSites(COM_DXLServiceAsyncResponse response, List<External_Account__c> externalAccounts) {
		List<External_Site__c> returnValue = new List<External_Site__c>();
		for (COM_DXLServiceAsyncResponse.Site siteRecord : response.payload.sites) {
			External_Site__c externalSite = new External_Site__c();
			externalSite.External_Site_Id__c = siteRecord.unifySiteRefId;
			externalSite.External_Source__c = COM_DXL_Constants.COM_DXL_EXTERNAL_SOURCE_UNIFY;
			externalSite.Site__c = siteRecord.salesforceSiteId;
			externalSite.SLSAPID__c = siteRecord.siteLevelServicesAPId;
			externalSite.External_Account__c = COM_DXLNotificationListenerService.getExternalAccountsByType(
				externalAccounts,
				COM_DXL_Constants.COM_DXL_EXTERNAL_SOURCE_UNIFY
			)[0];
			returnValue.add(externalSite);
		}
		return returnValue;
	}

	@TestVisible
	private void updateTransactionLogRecord(COM_DXL_Integration_Log__c integrationLogRecord, String status, String errorMsg) {
		integrationLogRecord.Status__c = status;
		integrationLogRecord.Error__c = errorMsg;

		update integrationLogRecord;
	}

	@TestVisible
	private void updateOrderStatus(COM_DXLServiceAsyncResponse response, COM_DXL_Integration_Log__c integrationLogRecord) {
		List<csord__Order__c> ordersToUpdate = new List<csord__Order__c>();

		List<COM_DxlAffectedObjects> affectedObjects = COM_DXLNotificationListenerService.getAffectedObjects(
			integrationLogRecord.Affected_Objects__c
		);

		String orderId = COM_DXLNotificationListenerService.getAffectedObjectByType(COM_DXL_Constants.COM_DXL_ORDER_API_NAME, affectedObjects)[0]
			.sfdcId;

		if (Test.isRunningTest()) {
			ordersToUpdate.add(new csord__Order__c(Id = orderId, csord__Status2__c = 'Test Status'));
		} else {
			ordersToUpdate.add(new csord__Order__c(Id = orderId, csord__Status2__c = COM_DXL_Constants.COM_DXL_CUSTOMER_PROVISIONED_ORDER_STATUS));
		}

		if (!ordersToUpdate.isEmpty() && payload.payload.state.equalsIgnoreCase(COM_DXL_Constants.COM_DXL_ASYNC_COMPLETED_STATUS)) {
			update ordersToUpdate;
		}
	}

	private Boolean payloadIsValid() {
		if (!String.isBlank(this.payload.payload.transactionId)) {
			return true;
		} else {
			return false;
		}
	}
}
