@IsTest
public with sharing class TestCreateSitesQueueable {
	@TestSetup
	static void makeData() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.KVK_number__c = '12345678';
		update acc;
		Contact con = TestUtils.createContact(acc);
		con.Email = 'mostUniqueEmailInTheWorld@vodafoneziggo.com';
		con.AccountId = acc.Id;
		update con;
		Site__c site = TestUtils.createSite(acc);
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
			Name = 'New Basket',
			Primary__c = false,
			OwnerId = UserInfo.getUserId(),
			Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]',
			ProfitLoss_JSON__c = '{"KPI Direct Capex / Net Revenue":"0,00","KPI Payback Period(Months)":"0,00","KPI NPV":"0,00",' +
				'"KPI Net Incremental Billed Revenue":"0,00","KPI Free cash flow / Net revenue":"1,50","KPI EBITDA / Net revenue":"1,56",' +
				'"KPI Sales Margin / Net revenue":"1,71","NET RESULT":"56.993,07","NET RESULT first":"58.996,33","Capex":"1.228,43","Network Depreciation":"774,83",' +
				'"Direct Capex":"0,00","EBITDA":"58.996,33","EBITDA first":"62.203,26","Overhead allocation":"3.206,93","Network Opex":"2.819,96",' +
				'"Incremental EBITDA":"65.023,22","Incremental OPEX":"0,00","Total Gross Margin":"65.023,22","Total costs of sales":"10.816,78","A&R":"0,00",' +
				'"Revenue share":"0,00","Other Costs":"0,00","Opportunity cost":"0,00","Opportunity cost Fixed":"0,00","Opportunity cost Mobile":"0,00",' +
				'"Total cost of sales":"10.816,78","Total Net Revenue":"75.840,00","Total discounts":"0,00","Benchmark":"0,00",' +
				'"Total credit amount / loyalty bonus":"0,00","Benchmark Fixed":"0,00","Benchmark Mobile":"0,00",' +
				'"Operator other costs / other migration costs":"0,00","Operator other costs / other migration costs Fixed":"0,00",' +
				'"Operator other costs / other migration costs Mobile":"0,00","Discounts":"0,00","Total Gross Revenue (SUM)":"75.840,00",' +
				'"Total Gross Revenue for Operator other costs / other migration costs":"0,00","Total Gross Revenue":"75.840,00"}'
		);
		insert basket;
	}

	@IsTest
	static void testCreateSite() {
		Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout());

		OlbicoSettings__c oblico = new OlbicoSettings__c();
		oblico.Olbico_JSon_Endpoint__c = 'Fake endpoint';
		oblico.Olbico_Json_Username__c = 'Fake username';
		oblico.Olbico_Json_Password__c = 'Fake password';
		oblico.Olbico_Json_BAG_Streets__c = 'Fake bag street';
		insert oblico;

		Account acc = [SELECT Id, KVK_number__c FROM Account LIMIT 1];
		cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
		List<Site__c> preSites = [
			SELECT
				Id,
				Site_Street__c,
				Site_Postal_Code__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c
			FROM Site__c
		];

		Map<String, String> siteMap = new Map<String, String>{
			Constants.POSTCODE => preSites[0].Site_Postal_Code__c,
			Constants.HOUSENUMBER => String.valueOf(preSites[0].Site_House_Number__c),
			Constants.HOUSENRSUFFIX => preSites[0].Site_House_Number_Suffix__c
		};

		Map<String, String> siteMap2 = new Map<String, String>{
			Constants.POSTCODE => '1086VE',
			Constants.HOUSENUMBER => '14',
			Constants.HOUSENRSUFFIX => ''
		};
		List<Map<String, String>> siteData = new List<Map<String, String>>{ siteMap, siteMap2 };

		System.assertEquals(1, preSites.size(), 'There should be only one site in the system.');

		Test.startTest();
		CreateSitesQueueable csq = new CreateSitesQueueable(siteData, acc.Id, basket.Id);
		System.enqueueJob(csq);
		Test.stopTest();

		List<Site__c> postSites = [
			SELECT
				Id,
				Site_Street__c,
				Site_Postal_Code__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c
			FROM Site__c
		];

		System.assertEquals(
			2,
			postSites.size(),
			'There should be two sites in total in the system now.'
		);
	}
}