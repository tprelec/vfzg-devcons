/*
 * @author Juan Cardona
 * @date 06-05-2022
 *
 * @description Test class for CustomButtonInstallationInformation
 */
@isTest
private class TestCustomButtonInstallationInformation {
	@testSetup
	static void prepareCommonTestData() {
		cscfga__Product_Basket__c productBasketWithSites = TestUtils.createCSProductBasket();
		List<cscfga__Product_Configuration__c> listProductConfigurationToInsert = new List<cscfga__Product_Configuration__c>();
		listProductConfigurationToInsert.add(
			new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = productBasketWithSites.Id, Site__c = TestUtils.theSite.Id)
		);
		insert listProductConfigurationToInsert;
	}
	@isTest
	private static void testPerformAction() {
		cscfga__Product_Basket__c productBasket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];
		Test.startTest();
		CustomButtonInstallationInformation button = new CustomButtonInstallationInformation();
		String url = button.performAction(String.valueOf(productBasket.Id));

		System.assertNotEquals(null, url, 'The URL cannot be null.');
		System.assertNotEquals('', url, 'The URL cannot be blank.');
		Test.stopTest();
	}
}
