/*
    Author: Rahul Sharma
    Description: Tests class for SFCC_OrderOverviewApi.cls
    Jira ticket: COM-3241
*/

/*
{
  "accountId": "0011l000018AokgAAC",
  "status": [
    "Ford",
    "BMW",
    "Fiat"
  ],
  "channel": [
    "123"
  ],
  "startDate": "2022-07-01T00:00:00.000Z",
  "endDate": "2022-07-29T00:00:00.000Z"
}
*/

@IsTest
private class SFCC_OrderOverviewApiTest {
	@IsTest
	static void testBadRequestDataFormat() {
		setRequest('Invalid JSON Request Body');

		Test.startTest();

		SFCC_OrderOverviewApi.getOverview();

		Test.stopTest();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_400, RestContext.response.statusCode);

		String responseString = RestContext.response.responseBody.toString();
		SFCC_OrderOverviewApi.ResponseWrapper responseWrapper = (SFCC_OrderOverviewApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderOverviewApi.ResponseWrapper.class
		);
		System.assertEquals('The system is unable to parse the request, please check the request format.', responseWrapper.message);
		System.assertEquals(null, responseWrapper.orders);
	}

	@IsTest
	static void testMandatoryAccountId() {
		setRequest('{"a": 1}');

		Test.startTest();

		SFCC_OrderOverviewApi.getOverview();

		Test.stopTest();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_400, RestContext.response.statusCode);

		String responseString = RestContext.response.responseBody.toString();
		SFCC_OrderOverviewApi.ResponseWrapper responseWrapper = (SFCC_OrderOverviewApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderOverviewApi.ResponseWrapper.class
		);
		System.assertEquals('accountId is required in request and must be a valid salesforce record id', responseWrapper.message);
		System.assertEquals(null, responseWrapper.orders);
	}

	@IsTest
	static void testOrdersSorting() {
		SFCC_OrderOverviewApi.OrderWrapper order1 = new SFCC_OrderOverviewApi.OrderWrapper(new Order__c(), '');
		SFCC_OrderOverviewApi.OrderWrapper order2 = new SFCC_OrderOverviewApi.OrderWrapper(new Order__c(), '');
		SFCC_OrderOverviewApi.OrderWrapper order3 = new SFCC_OrderOverviewApi.OrderWrapper(new Order__c(), '');
		Date orderDate = Date.today().addDays(10);

		Test.startTest();

		List<SFCC_OrderOverviewApi.OrderWrapper> orders = new List<SFCC_OrderOverviewApi.OrderWrapper>{ order3, order1, order2 };
		orders.sort();

		Test.stopTest();

		// Verify the sort order
		System.assertEquals(order1.orderId, orders[0].orderId);
		System.assertEquals(order1.orderNumber, orders[0].orderNumber);
		System.assertEquals(order1.orderDate, orders[0].orderDate);
		System.assertEquals(order2.orderId, orders[1].orderId);
		System.assertEquals(order2.orderNumber, orders[1].orderNumber);
		System.assertEquals(order2.orderDate, orders[1].orderDate);
		System.assertEquals(order3.orderId, orders[2].orderId);
		System.assertEquals(order3.orderNumber, orders[2].orderNumber);
		System.assertEquals(order3.orderDate, orders[2].orderDate);
	}

	private static void setRequest(String requestBody) {
		RestRequest req = new RestRequest();
		req.requestURI = '/orders/getOverview';
		req.httpMethod = 'POST';
		RestContext.request = req;
		req.requestBody = Blob.valueOf(requestBody);
		RestResponse res = new RestResponse();
		RestContext.response = res;
	}
}
