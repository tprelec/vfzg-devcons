public without sharing class VF_ContractTriggerHandler extends TriggerHandler {

    public override void beforeInsert() {
        List<VF_Contract__c> newContracts = (List<VF_Contract__c>) this.newList;

        setDealerInfo(newContracts, null);

    }

    public override void afterInsert() {
        List<VF_Contract__c> newContracts = (List<VF_Contract__c>) this.newList;
        Map<Id, VF_Contract__c> newContractsMap = (Map<Id, VF_Contract__c>) this.newMap;

        createSharingRulesFromOpportunity(newContracts);
        updateFrameworkAgreementDateAndVersionOnAccount(newContracts, null);
        adjustAccountShare(newContracts);
        CS_ComHelper.createMobileFlowProcess(null, newContractsMap);
        setContractNumber(newContracts);
    }

    public override void beforeUpdate() {
        List<VF_Contract__c> newContracts = (List<VF_Contract__c>) this.newList;
        Map<Id, VF_Contract__c> oldContractsMap = (Map<Id, VF_Contract__c>) this.oldMap;

        setDealerInfo(newContracts, oldContractsMap);
        checkContractStatus(newContracts, oldContractsMap);
    }

    public override void afterUpdate() {
        List<VF_Contract__c> newContracts = (List<VF_Contract__c>) this.newList;
        Map<Id, VF_Contract__c> oldContractsMap = (Map<Id, VF_Contract__c>) this.oldMap;
        Map<Id, VF_Contract__c> newContractsMap = (Map<Id, VF_Contract__c>) this.newMap;

        updateAssetOwnership(newContracts, oldContractsMap);
        updateFrameworkAgreementDateAndVersionOnAccount(newContracts, oldContractsMap);
        adjustAccountShare(newContracts);
        CS_ComHelper.createMobileFlowProcess(oldContractsMap, newContractsMap);
        CS_ComHelper.orchestratorSubscribeToEvent(oldContractsMap, newContractsMap);
    }

    public override void beforeDelete() {
        List<VF_Contract__c> oldContracts = (List<VF_Contract__c>) this.oldList;
        
        revertContractAttachments(oldContracts);
    }
    
    private static Dealer_Information__c cvmOwner {
        get {
            if (cvmOwner == null) {
                cvmOwner = [
                    SELECT
                        Id,
                        Contact__r.UserId__c,
                        Dealer_Code__c
                    FROM
                        Dealer_Information__c
                    WHERE Dealer_Code__c = :Label.Dealer_Code_CVM_Owner
                    LIMIT 1
                ];
            }
            return cvmOwner;
        }
        private set;
    }

    private void setContractNumber(List<VF_Contract__c> contracts) {
        List<VF_Contract__c> contractToSetNumber = new List<VF_Contract__c>();
        for (VF_Contract__c c : contracts) {
            if (String.isBlank(c.Contract_Number__c) && c.Opportunity__c != null) {
                VF_Contract__c contract = new VF_Contract__c();
                contract.Id = c.Id;
                contract.Contract_Number__c = c.Name;
                contractToSetNumber.add(contract);
            }
        }
        if (!contractToSetNumber.isEmpty()) {
            update contractToSetNumber;
        }
    }

    private void setDealerInfo(List<VF_Contract__c> contracts, Map<Id, VF_Contract__c> oldContractsMap) {
        Set<Id> dealerInfo = new Set<Id>();
        for (VF_Contract__c rec : contracts) {
            VF_Contract__c oldRec = oldContractsMap != null ? oldContractsMap.get(rec.Id) : null;
            if ((oldRec == null && rec.Dealer_Information__c != null) || (oldRec != null && rec.Dealer_Information__c != oldRec.Dealer_Information__c)
                || (oldRec != null && rec.OwnerId != oldRec.OwnerId && rec.Dealer_Information__c == oldRec.Dealer_Information__c)
            ) {
                dealerInfo.add(rec.Dealer_Information__c);
            }
        }

        if (!dealerInfo.isEmpty()) {
            Map<Id, Id> contractOwnerMap = new Map<Id, Id>();
            Map<Id, Dealer_Information__c> dealers = new Map<Id, Dealer_Information__c>([
                SELECT Id, Dealer_Code__c, Contact__r.UserId__c, Contact__r.UserId__r.IsActive
                FROM Dealer_Information__c
                WHERE Id IN :dealerInfo
            ]);

            for (VF_Contract__c c : contracts) {
                if (c.Dealer_Information__c == null) {
                    continue;
                }

                Dealer_Information__c d = dealers.get(c.Dealer_Information__c);
                if (d.Contact__r.UserId__c == null || (d.Contact__r.UserId__c != null && !d.Contact__r.UserId__r.IsActive)) {
                    c.OwnerId = cvmOwner.Contact__r.UserId__c;
                    continue;
                }
                if (oldContractsMap != null && c.OwnerId != oldContractsMap.get(c.Id).OwnerId && c.Dealer_Information__c == oldContractsMap.get(c.Id).Dealer_Information__c) {
                    c.OwnerId = d.Contact__r.UserId__c;
                }
                if (oldContractsMap == null || (oldContractsMap != null && c.Dealer_Information__c != oldContractsMap.get(c.Id).Dealer_Information__c)) {
                    c.OwnerId = d.Contact__r.UserId__c;
                    c.Dealer_code_responsible__c = d.Dealer_Code__c;
                }
            }
        }
    }

    private void checkContractStatus(List<VF_Contract__c> contracts, Map<Id, VF_Contract__c> oldContractsMap) {
        Set<String> contractStatuses = new Set<String>{'Implementation', 'Active'};
        Set<Id> notSignedUpdate = new Set<Id>();
        for (VF_Contract__c c : contracts) {
            if (c.Contract_Status__c == 'Not Signed' && contractStatuses.contains(oldContractsMap.get(c.Id).Contract_Status__c)) {
                notSignedUpdate.add(c.Id);
            }
            Boolean forUpdate = String.isNotBlank(c.Contract_Number__c) && c.Type_of_Service__c == Constants.VF_CONTRACT_MOBILE_SERVICE && c.No_of_CTN_Assets__c != oldContractsMap.get(c.Id).No_of_CTN_Assets__c;
            if (!forUpdate) {
                continue;
            }
            
            if (forUpdate && c.Contract_Status__c != Constants.VF_CONTRACT_STATUS_TERMINATED && c.No_of_CTN_Assets__c == 0) {
                c.Contract_Status__c = Constants.VF_CONTRACT_STATUS_TERMINATED;
                continue;
            }
            if (forUpdate && c.Contract_Status__c != Constants.VF_CONTRACT_STATUS_ACTIVE && c.No_of_CTN_Assets__c > 0) {
                c.Contract_Status__c = Constants.VF_CONTRACT_STATUS_ACTIVE;
            }
        }
        
        updateContractsToNotSigned(contracts, oldContractsMap, notSignedUpdate);
    }

    private void updateContractsToNotSigned(List<VF_Contract__c> contracts, Map<Id, VF_Contract__c> oldContractsMap, Set<Id> notSignedUpdate) {
        if (notSignedUpdate.isEmpty()) {
            return;
        }

        Map<Id, List<Order__c>> contractOrders = GeneralUtils.groupByIDField(
            [SELECT Id, VF_Contract__c
            FROM Order__c
            WHERE VF_Contract__c IN :notSignedUpdate 
                AND Block_cancellation__c = TRUE], 
            'VF_Contract__c'
        );

        for (VF_Contract__c c : contracts) {
            if (!contractOrders.containsKey(c.Id) || contractOrders.get(c.Id).isEmpty()) {
                continue;
            }
            c.Contract_status__c = oldContractsMap.get(c.Id).Contract_status__c;
        }
    }

    private void updateAssetOwnership(List<VF_Contract__c> contracts, Map<Id, VF_Contract__c> oldContractsMap) {
        Map<Id, Id> contractOwnerMap = new Map<Id, Id>();
        for (VF_Contract__c c : contracts) {
            if (oldContractsMap != null && GeneralUtils.isRecordFieldChanged(c, oldContractsMap, 'OwnerId')) {
                contractOwnerMap.put(c.Id, c.OwnerId);
            }
        }
        if (contractOwnerMap.isEmpty()) { return; }

        List<VF_Asset__c> assetList = [
            SELECT Id, OwnerId, Contract_VF__c, Contract_VF__r.OwnerId
            FROM VF_Asset__c
            WHERE Contract_VF__c IN :contractOwnerMap.keySet()
        ];
        List<VF_Asset__c> assetsForUpdate = new List<VF_Asset__c>();
        for (VF_Asset__c vfa : assetList) {
            if (vfa.OwnerId != contractOwnerMap.get(vfa.Contract_VF__c)) {
                vfa.OwnerId = contractOwnerMap.get(vfa.Contract_VF__c);
                assetsForUpdate.add(vfa);
            }
        }
        if (!assetsForUpdate.isEmpty()) {
            update assetsForUpdate;
        }
    }

    private void updateFrameworkAgreementDateAndVersionOnAccount(List<VF_Contract__c> contracts, Map<Id, VF_Contract__c> oldContractsMap) {
        Map<Id, Date> accountToChangedSignedDate = new Map<Id, Date>();

        for (VF_Contract__c contract : contracts) {
            if (contract.Contract_Sign_Date__c == null) {
                continue;
            }
            if (oldContractsMap == null || contract.Contract_Sign_Date__c != oldContractsMap.get(contract.Id).Contract_Sign_Date__c) {
                accountToChangedSignedDate.put(contract.Account__c, contract.Contract_Sign_Date__c);
            }
        }
        if (!accountToChangedSignedDate.isEmpty()) {
            AccountService.getInstance().updateFrameworkAgreementDateAndVersionForNewContract(accountToChangedSignedDate);
        }
    }

    private static void createSharingRulesFromOpportunity(List<VF_Contract__c> newContracts) {
        Set<Id> oppIds = new Set<Id>();
        for (VF_Contract__c vfc : newContracts) {
            if (vfc.Opportunity__c != null) {
                oppIds.add(vfc.Opportunity__c);
            }
        }

        if (!oppIds.isEmpty()) {
            // fetch opportunity info
            List<VF_Contract__c> contractsWithOpp = [
                SELECT
                    Id,
                    Opportunity__c,
                    Opportunity__r.OwnerId
                FROM VF_Contract__c
                WHERE Opportunity__c IN :oppIds
            ];

            // collect opp team
            List<VF_Contract__Share> contractSharingRulesToInsert = new List<VF_Contract__Share>();
            Map<Id, List<OpportunityTeamMember>> newOppIdsToMember = new Map<Id, List<OpportunityTeamMember>>();
            Set<Id> allTeamMemberIds = new Set<Id>();
            List<OpportunityTeamMember> oppTeamMembers = new WithoutSharingMethods().otmquery(oppIds);

            // Build map
            if (!oppTeamMembers.isEmpty()) {
                for (OpportunityTeamMember otm : oppTeamMembers) {
                    if (otm.User.IsActive == false) {
                        for (VF_Contract__c vfc : newContracts) {
                            vfc.addError('Opportunity Team Member '+ otm.User.Name +' is inactive. Please replace by an active User first.');
                        }
                        return;
                    }
                    // if for any reason the opp owner is a team member, don't create an extra sharing rule
                    if (otm.userId != otm.Opportunity.OwnerId) {
                        if (newOppIdsToMember.containsKey(otm.OpportunityId)) {
                            newOppIdsToMember.get(otm.OpportunityId).add(otm);
                        } else {
                            newOppIdsToMember.put(otm.OpportunityId,new List<OpportunityTeamMember>{otm});
                        }
                    }
                    allTeamMemberIds.add(otm.userId);
                }
            }

            if (!contractsWithOpp.isEmpty()) {
                for (VF_Contract__c vfc : contractsWithOpp) {
                    allTeamMemberIds.add(vfc.Opportunity__r.OwnerId);
                }
            }

            // add parent owners
            Map<Id, Id> parentPartnerUserIds = GeneralUtils.getParentPartnerUserIdsFromUserIds(allTeamMemberIds);

            if (!newOppIdsToMember.isEmpty()) {
                // create new sharing rules for all new contracts for all opp teammembers
                for (VF_Contract__c contract : newContracts) {
                    for (OpportunityTeamMember otm : newOppIdsToMember.get(contract.Opportunity__c)) {
                        VF_Contract__Share vcs = new VF_Contract__Share(RowCause = Schema.VF_Contract__Share.RowCause.Opportunity_Team__c);
                        vcs.parentId = contract.Id;
                        vcs.UserOrGroupId = otm.UserId;

                        // accesslevel 'None' is assigned to the Opportunity owner, so should also result in Edit rights
                        if (otm.OpportunityAccessLevel == 'All' || otm.OpportunityAccessLevel == 'None') {
                            vcs.AccessLevel = 'Edit';
                        } else {
                            vcs.AccessLevel = otm.OpportunityAccessLevel;
                        }

                        contractSharingRulestoInsert.add(vcs);

                        // add sharing rule for parent partners
                        if (parentPartnerUserIds.containsKey(otm.UserId)) {
                            VF_Contract__Share vcsParent = vcs.clone();
                            vcsParent.RowCause = Schema.VF_Contract__Share.RowCause.Opportunity_Team_Parent__c;
                            vcsParent.UserOrGroupId = parentPartnerUserIds.get(otm.UserId);
                            contractSharingRulestoInsert.add(vcsParent);
                        }
                    }
                }
            }
            // create new sharing rules for opp owner parents (if any)
            if (!contractsWithOpp.isEmpty()) {
                for (VF_Contract__c vfc : contractsWithOpp) {
                    if (parentPartnerUserIds.containsKey(vfc.Opportunity__r.OwnerId)) {
                        VF_Contract__Share vcs = new VF_Contract__Share(RowCause = Schema.VF_Contract__Share.RowCause.Opportunity_Owner_Parent__c);
                        vcs.parentId = vfc.Id;
                        vcs.UserOrGroupId = parentPartnerUserIds.get(vfc.Opportunity__r.OwnerId);
                        vcs.AccessLevel = 'Edit';
                        contractSharingRulestoInsert.add(vcs);
                    }
                }
            }

            Savepoint sp = Database.setSavepoint();
            try {
                new WithoutSharingMethods().insertSharingRules(contractSharingRulestoInsert);
            } catch (dmlException de) {
                Database.rollback(sp);
                throw new ExInvalidStateException('Error while creating Contract sharing rules: ' + de);
            }
        }
    }

    /*
     *  Description:    this method will copy contract attachments to the opportunity if the contract is deleted
     */
    private void revertContractAttachments(List<VF_Contract__c> oldContracts) {
        Map<Id, Id> contractIdToOppIdMap = new Map<Id, Id>();
        for (VF_Contract__c vfc : oldContracts) {
            contractIdToOppIdMap.put(vfc.Id, vfc.Opportunity__c);
        }

        // for each opportunity that is set back from closedwon to closing, we migrate the contract attachments to the opportunity
        List<Contract_Attachment__c> caToDelete = new List<Contract_Attachment__c>();
        List<Opportunity_Attachment__c> oaToInsert = new List<Opportunity_Attachment__c>();
        List<Contract_Attachment__c> caList = new List<Contract_Attachment__c>();
        Map<Id, Id> caIdToContractId = new Map<Id, Id>();
        for (Contract_Attachment__c ca : [
            SELECT
                Id,
                Name,
                Attachment_Type__c,
                Description_Optional__c,
                Contract_VF__c,
                Signed_Contract__c,
                ContentDocumentId__c
            FROM Contract_Attachment__c
            WHERE Contract_VF__c IN :contractIdToOppIdMap.keySet()
        ]) {
            caList.add(ca);
            caIdToContractId.put(ca.Id, ca.Contract_VF__c);
            caToDelete.add(ca);
        }

        List<ContentDocumentLink> contentDocumentLinkToInsert = new List<ContentDocumentLink>();

        // Get all the content document links for the contracts
        Map<Id, ContentDocumentLink> caIdToContentDocumentLink = new Map<Id, ContentDocumentLink>();
        Set<Id> caIdSet = caIdToContractId.keySet();
        if (caIdSet.size() > 0) {
            for (ContentDocumentLink cdl : [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :caIdSet]) {
                caIdToContentDocumentLink.put(cdl.LinkedEntityId, cdl);
            }
        }

        List<FeedItem> feedItemsToInsert = new List<FeedItem>();
        Map<Id, FeedItem> caIdToFeedItem = new Map<Id, FeedItem>();
        for (FeedItem fi : [SELECT Id, ParentId, ContentFileName, ContentData FROM FeedItem WHERE ParentId IN :caIdToContractId.keySet()]) {
            caIdToFeedItem.put(fi.ParentId,fi);
        }

        for (Contract_Attachment__c ca : caList) {
            Opportunity_Attachment__c oa = new Opportunity_Attachment__c();
            oa.Attachment_Type__c = ca.Attachment_Type__c;
            oa.Description_Optional__c = ca.Description_Optional__c;
            oa.Signed_Contract__c = ca.Signed_Contract__c;
            oa.Opportunity__c = contractIdToOppIdMap.get(ca.Contract_VF__c);
            oa.Source_Contract_Attachment_Id__c = ca.Id;
            oa.ContentDocumentId__c = ca.ContentDocumentId__c;
            oaToInsert.add(oa);
        }
        insert oaToInsert;

        // go through inserted oa's so we have id's
        for (Opportunity_Attachment__c oa : oatoInsert) {
            if (caIdToContentDocumentLink.containsKey(oa.Source_Contract_Attachment_Id__c)) {
                ContentDocumentLink cacdl = caIdToContentDocumentLink.get(oa.Source_Contract_Attachment_Id__c);
                ContentDocumentLink cdl = new ContentDocumentLink();
                cdl.ContentDocumentId = cacdl.ContentDocumentId;
                cdl.LinkedEntityId = oa.id;
                cdl.ShareType = 'V';
                cdl.Visibility  = 'Allusers'; //W-001438
                contentDocumentLinkToInsert.add(cdl);
            }
            if (caIdToFeedItem.containsKey(oa.Source_Contract_Attachment_Id__c)) {
                FeedItem fi = new FeedItem();
                FeedItem contrFi = caIdToFeedItem.get(oa.Source_Contract_Attachment_Id__c);
                fi.ParentId = oa.Id;
                fi.ContentFileName = contrFi.contentFileName;
                fi.ContentData = contrFi.contentData;
                feedItemsToInsert.add(fi);
            }
        }
        delete caToDelete;
        insert contentDocumentLinkToInsert;
        insert feedItemsToInsert;
    }

    // check account visibility for portal users
    private void adjustAccountShare(List<VF_Contract__c> newContracts) {
        Map<Id, Set<Id>> ownerRoleToAccounts = new Map<Id, Set<Id>>();
        Set<Id> accounts = new Set<Id>();
        for (VF_Contract__c c : newContracts) {
            User owner = GeneralUtils.userMap.get(c.OwnerId);
            if (GeneralUtils.isPartnerUser(owner) && owner.IsActive) {
                if (!ownerRoleToAccounts.containsKey(owner.UserRoleId)) {
                    ownerRoleToAccounts.put(owner.UserRoleId, new Set<Id>());
                }
                ownerRoleToAccounts.get(owner.UserRoleId).add(c.Account__c);
                accounts.add(c.Account__c);
            }
        }

        if (!ownerRoleToAccounts.isEmpty()) {
            Map<Id, Id> roleToGroupId = getRoleGroups(ownerRoleToAccounts.keySet());
            Map<Id, Set<Id>> existingRoleAccountShares = getExistingSharing(ownerRoleToAccounts.keySet(), accounts);
            Map<Id, Set<Id>> accShareToAdd = new Map<Id, Set<Id>>();

            for (Id userRoleId : ownerRoleToAccounts.keySet()) {
                Id groupId = roleToGroupId.get(userRoleId);
                Set<Id> accSharetoToCreate = ownerRoleToAccounts.get(userRoleId);
                if (existingRoleAccountShares.containsKey(groupId)) {
                    for (Id accId : existingRoleAccountShares.get(groupId)) {
                        if (accSharetoToCreate.contains(accId)) {
                            accSharetoToCreate.remove(accId);
                        }
                    }
                }
                accShareToAdd.put(groupId, accSharetoToCreate);
            }

            List<AccountShare> accShareToInsert = new List<AccountShare>();
            for (Id groupId : accShareToAdd.keySet()) {
                for (Id accId : accShareToAdd.get(groupId)) {
                    AccountShare accountShare = new AccountShare();
                    accountShare.AccountId = accId;
                    accountShare.UserOrGroupId = groupId;
                    accountShare.AccountAccessLevel = 'read';
                    accountShare.CaseAccessLevel = 'None';
                    accountShare.OpportunityAccessLevel = 'None';
                    accShareToInsert.add(accountShare);
                }
            }

            if (!accShareToInsert.isEmpty()) {
                SharingUtils.insertRecordsWithoutSharing(accShareToInsert);
            }
        }

    }

    private Map<Id, Id> getRoleGroups(Set<Id> ownerRoleIds) {
        Map<Id, Id> roleToGroupId = new Map<Id, Id>();
        for (Group g : [SELECT Id, RelatedId FROM Group WHERE RelatedId IN :ownerRoleIds AND Type = 'Role']) {
            roleToGroupId.put(g.RelatedId, g.Id);
        }
        return roleToGroupId;
    }

    private Map<Id, Set<Id>> getExistingSharing(Set<Id> ownerRoleIds, Set<Id> accounts) {
        Map<Id, Set<Id>> existingRoleAccountShares = new Map<Id, Set<Id>>();
        List<AccountShare> accShares = [
            SELECT Id, AccountId, UserOrGroupId
            FROM AccountShare
            WHERE RowCause = 'Manual' AND AccountId IN :accounts
                AND UserOrGroupId IN :ownerRoleIds
        ];

        for (AccountShare accShare : accShares) {
            if (existingRoleAccountShares.containsKey(accShare.UserOrGroupId)) {
                existingRoleAccountShares.put(accShare.UserOrGroupId, new Set<Id>());
            }
            existingRoleAccountShares.get(accShare.UserOrGroupId).add(accShare.AccountId);
        }

        return existingRoleAccountShares;
    }

    /**
     * @description         A without sharing util class to do the updates/inserts/deletes without interfering with authorizations
     * @author              Guy Clairbois
     */
    public without sharing class WithoutSharingMethods{
        public void insertSharingRules(List<sObject> sharingRulestoInsert) {
            insert sharingRulestoInsert;
        }

        public list<OpportunityTeamMember> otmquery(set<Id> oppIds) {
            return [
                SELECT
                    Id,
                    UserId,
                    User.IsActive,
                    User.Name,
                    OpportunityId,
                    OpportunityAccessLevel,
                    TeamMemberRole,
                    Opportunity.OwnerId
                FROM OpportunityTeamMember
                WHERE OpportunityId IN :oppIds
            ];
        }
    }
}