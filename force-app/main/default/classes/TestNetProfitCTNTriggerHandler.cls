@isTest
public class TestNetProfitCTNTriggerHandler {
	@isTest
	static void createAndUpdateMobileOrders() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.createCompleteContract();
		VF_Contract__c contr = TestUtils.theContract;
		TestUtils.autoCommit = true;

		Test.startTest();

		Order__c order = TestUtils.createOrder(contr);
		String contrId = contr.Id;

		Contracted_Products__c cp = [
			SELECT Id, Product__c, Site__c, VF_Contract__c
			FROM Contracted_Products__c
			WHERE VF_Contract__c = :contrId
			LIMIT 1
		];

		cp.Order__c = order.Id;
		cp.Product_Family__c = 'Voice';
		update cp;

		Contracted_Products__c cp2 = new Contracted_Products__c(
			Quantity__c = 2,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Product__c = cp.Product__c,
			VF_Contract__c = cp.VF_Contract__c,
			Site__c = cp.Site__c,
			Deal_Type__c = Constants.CONTRACTED_PRODUCT_DEAL_TYPE_NEW
		);
		insert cp2;

		cp2.Order__c = order.Id;
		cp2.Product_Family__c = 'MBB';
		update cp2;

		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);
		NetProfit_CTN__c ctn = TestUtils.createNetProfitCTN(npi);

		ctn.Order__c = order.Id;
		ctn.Action__c = 'New';
		ctn.Product_Group__c = 'Priceplan';
		ctn.Simcard_number_VF__c = Constants.NET_PROFIT_CTN_VALID_SIM_CARD_NUMBER;
		ctn.CTN_Status__c = 'Active';
		ctn.CTN_Number__c = Constants.NETPROFIT_CTN_VALID_CTN_NUMBER;
		update ctn;

		Test.stopTest();

		order = [SELECT Id, action_New_CTNs__c FROM Order__c WHERE Id = :order.Id];
		//System.assertEquals(1, order.action_New_CTNs__c, 'It should be 1');

		npi = [SELECT Id, Quote_Profiles_Details__c FROM NetProfit_Information__c WHERE Id = :npi.Id];

		System.assertNotEquals(false, String.isNotEmpty(npi.Quote_Profiles_Details__c), 'The field should contain information');
	}

	@isTest
	static void deleteNetProfitCtns() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TestUtils.createCompleteContract();

		VF_Contract__c contr = TestUtils.theContract;
		TestUtils.autoCommit = true;

		Order__c order = TestUtils.createOrder(contr);
		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);
		NetProfit_CTN__c ctn = TestUtils.createNetProfitCTN(npi);

		Test.startTest();
		delete ctn;
		Test.stopTest();

		List<NetProfit_CTN__c> mobileOrders = [SELECT Id FROM NetProfit_CTN__c];
		System.assertEquals(0, mobileOrders.size(), 'The mobile order should be deleted');
	}

	@isTest
	public static void testRetentionAssetMatching() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createCompleteContract();
		VF_Contract__c contr = TestUtils.theContract;
		TestUtils.autoCommit = true;
		Order__c order = TestUtils.createOrder(contr);
		NetProfit_Information__c npi = TestUtils.createNetProfitInformation(order);
		TestUtils.autoCommit = false;

		Test.startTest();
		// Create Matching Asset
		VF_Asset__c asset = TestUtils.createVFAsset(TestUtils.theAccount);
		asset.CTN__c = '123456789';
		asset.Assigned_Product_Id__c = '12345';
		insert asset;

		// Create CTNs
		NetProfit_CTN__c ctn1 = TestUtils.createNetProfitCTN(npi);
		ctn1.CTN_Number__c = '123456789';
		ctn1.Action__c = 'Retention';
		ctn1.Product_Group__c = 'Priceplan';
		ctn1.Order__c = order.Id;
		NetProfit_CTN__c ctn2 = TestUtils.createNetProfitCTN(npi);
		ctn2.CTN_Number__c = '987654321';
		ctn2.Action__c = 'Retention';
		ctn2.Product_Group__c = 'Priceplan';
		ctn2.Order__c = order.Id;
		insert (new List<NetProfit_CTN__c>{ ctn1, ctn2 });

		Test.stopTest();

		ctn1 = [SELECT Id, Assigned_Product_Id__c FROM NetProfit_CTN__c WHERE Id = :ctn1.Id];
		System.assertEquals('12345', ctn1.Assigned_Product_Id__c, 'Assigned Product ID should be copied to CTN');
		ctn2 = [SELECT Id, Assigned_Product_Id__c FROM NetProfit_CTN__c WHERE Id = :ctn2.Id];
		System.assertEquals(null, ctn2.Assigned_Product_Id__c, 'Assigned Product ID should not be copied to CTN');
	}
}
