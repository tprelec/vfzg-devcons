public with sharing class CS_SiteUtility {

    /**
     * @param configIds - set of configuration Ids
     * @return list of configurations with fields used in methods of this helper class
     */
    public static List<cscfga__Product_Configuration__c> getConfigData(Set<Id> configIds) {
        List<cscfga__Product_Configuration__c> result = new List<cscfga__Product_Configuration__c>();
        result = [SELECT Id, Name, Site_Availability_Id__c,Site_Check_SiteID__c, ClonedSiteIds__c, ManagedInternet__c, IPVPN__c, OneNet__c, OneFixed__c,Mobile_Scenario__c, Fixed_Scenario__c,
                         OneNet_Scenario__c,cscfga__Product_Definition__r.Name, SkypeForBusiness_Scenario__c, ClonedPBXIds__c, PBXId__c, Secondary__c, cscfga__Parent_Configuration__c
                         FROM cscfga__Product_Configuration__c WHERE id IN :configIds];
        
        return result;
    }

    /**
     * auxiliary
     * @param jsonString - json containing site availabilities
     * @return list of site availabity Ids
     */
    private static List<String> parseJson(String jsonString) {
        Map<String, Object> siteAvailabilitiesMap = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        List<String> result = new List<String>();
        for (String key : siteAvailabilitiesMap.keySet()) {
            result.add((String)siteAvailabilitiesMap.get(key));
        }
       
        return result;
    }

    /**
     * @description         Returns Access configuration from the same package which productConfigId is part of
                            If no such configuration available in the basket then it returns null
     * @param productConfigId - configuration which is part of the package
     * @param configs - basket configurations to search for Access Infrastructure configuration
     * @return Access Infrastructure configuration from package if found, null otherwise
     */
    public static cscfga__Product_Configuration__c getAssociatedAccess(Id productConfigId, List<cscfga__Product_Configuration__c> configs) {
        cscfga__Product_Configuration__c result = null;
        
        for (cscfga__Product_Configuration__c conf : configs) {
            if (conf.Id != productConfigId && ((conf.ManagedInternet__c == productConfigId) || (conf.OneNet__c == productConfigId) || (conf.OneFixed__c == productConfigId)
            || (conf.IPVPN__c == productConfigId))) {
                result = conf;
            }
        }
        return result;
    }

    /**
     * @param configs - basket configurations whose site availabilities are retrieved
     * @return map of site availabilities with configuration Id as a map key
     */
    public static Map<Id, List<String>> getSiteAvailabilityForConfig(List<cscfga__Product_Configuration__c> configs) {
        Map<Id, List<String>> result = new Map<Id, List<String>>();
        
        for (cscfga__Product_Configuration__c config : configs) {
            if ((config.ClonedSiteIds__c == null) && (config.Site_Availability_Id__c != null)) {
                result.put(config.Id, new List<String>{config.Site_Availability_Id__c});
            }
            else if (config.ClonedSiteIds__c != null) {
                 result.put(config.Id, parseJson(config.ClonedSiteIds__c));
            }
        }
        return result;
    }
    
    /**
     * auxiliary
     * @param siteAvailabilityMap - site availabilites mapped on respective configuration Ids
     * @return map of sites mapped on respective configuration Ids
     */
    private static Map<Id, List<String>> getSiteLocationSitesFromSiteAvailability(Map<Id, List<String>> siteAvailabilityMap) {
        Map<Id, List<String>> result = new Map<Id, List<String>>();
        Set<Id> siteAvailabilityIds = new Set<Id>();
        for (Id key : siteAvailabilityMap.keySet()) {
            List<String> sas = siteAvailabilityMap.get(key);
            
            for (String sa : siteAvailabilityMap.get(key)) {
                siteAvailabilityIds.add((Id)sa);
            }
        }
        Map<Id,Site_Availability__c> saMap = new Map<Id,Site_Availability__c>([SELECT Id, Site__c FROM Site_Availability__c WHERE Id in :siteAvailabilityIds]);
    
        for (Id key : siteAvailabilityMap.keySet()) {
            Set<String> sites = new Set<String>();
            for (String sa : siteAvailabilityMap.get(key)) {
                if (saMap.get((Id)sa).Site__c != null) {
                    sites.add(saMap.get((Id)sa).Site__c);
                }
            }
            List<String> listStrings = new List<String>(sites);
            result.put(key, listStrings);
        }
        return result;
    }
    
    /**
     * @param siteAvailabilityMap - site availabilites mapped on respective configuration Ids
     * @return map of sites mapped on respective configuration Ids
     */
    public static  Map<Id, List<String>> getSiteLocationsForConfig(List<cscfga__Product_Configuration__c> configs) {
        Map<Id, List<String>> result = new Map<Id, List<String>>();
        
        Set<Id> siteAvailabilityIds = new Set<Id>();
        
        for (cscfga__Product_Configuration__c config : configs) {
            if ((config.ClonedSiteIds__c == null) && (config.Site_Availability_Id__c != null)) {
                result.put(config.Id, new List<String>{config.Site_Availability_Id__c});
            }
            else if (config.ClonedSiteIds__c!=null) {
                 result.put(config.Id, parseJson(config.ClonedSiteIds__c));
            }
        }
        
        result = getSiteLocationSitesFromSiteAvailability(result);
        
        //add sites without site availability to results
        for (cscfga__Product_Configuration__c config : configs) {
            
            if (config.Site_Check_SiteID__c != null && config.ClonedSiteIds__c == null && config.Site_Availability_Id__c == null) {
                result.put(config.Id, new List<String>{config.Site_Check_SiteID__c});
            }
        }
        return result;
    }
  
    /**
     * @param sitesPerConfigMap - sites mapped on respective configuration Ids
     * @return list of unique sites (siteIds)
     */
    private static List<String> getAllSites(Map<Id, List<String>> sitesPerConfigMap) {
      String result = '';
      Set<String> allSites = new Set<String>();
  
      for (Id key : sitesPerConfigMap.keySet()) {
        List<String> spc = sitesPerConfigMap.get(key);
      
        for (String site : spc) {
            allSites.add(site);
        }
      }
  
      return new List<String>(allSites);
    }

    public static Map<Id, Integer> getConfigurationSiteQuantity(Set<Id> configurationIds) {

        // notice that siteCountForConfig will contain only configurations associated with some site
        // in translation, mobile configurations will not be part of the map
        Map<Id, Integer> siteCountForConfig = new Map<Id, Integer>(); 

        List<cscfga__Product_Configuration__c> configurations = CS_SiteUtility.getConfigData(configurationIds);
        Map<Id, List<String>> siteLocationsForConfig = CS_SiteUtility.getSiteLocationsForConfig(configurations);

        for (cscfga__Product_Configuration__c currentConfig : configurations) {
            if (currentConfig.cscfga__Parent_Configuration__c != null) {
                //get from parent
                if (siteLocationsForConfig.containsKey(currentConfig.cscfga__Parent_Configuration__c)) {
                    if (siteLocationsForConfig.get(currentConfig.cscfga__Parent_Configuration__c) != null && 
                    siteLocationsForConfig.get(currentConfig.cscfga__Parent_Configuration__c).size() > 0) {
                        siteCountForConfig.put(currentConfig.Id, siteLocationsForConfig.get(currentConfig.cscfga__Parent_Configuration__c).size());
                    }
                    else {
                        siteCountForConfig.put(currentConfig.Id, 1);
                    }
                }
                else{
                    cscfga__Product_Configuration__c associatedAccess = CS_SiteUtility.getAssociatedAccess(currentConfig.Id, configurations);
                    if (associatedAccess != null && siteLocationsForConfig.containsKey(associatedAccess.Id)) {
                        if (siteLocationsForConfig.get(associatedAccess.Id) != null && siteLocationsForConfig.get(associatedAccess.Id).size() > 0) {
                            siteCountForConfig.put(currentConfig.Id, siteLocationsForConfig.get(associatedAccess.Id).size());    
                        }
                        else {
                            siteCountForConfig.put(currentConfig.Id, 1);
                        }
                    }
                }
            }
            else {
                //get from config (no parent)
                if (siteLocationsForConfig.containsKey(currentConfig.Id)) {
                    if (siteLocationsForConfig.get(currentConfig.Id) != null && siteLocationsForConfig.get(currentConfig.Id).size() > 0) {
                        siteCountForConfig.put(currentConfig.Id, siteLocationsForConfig.get(currentConfig.Id).size());
                    }
                    else {
                        siteCountForConfig.put(currentConfig.Id, 1);
                    }
                }
            }
        }

        return siteCountForConfig;
    }
}