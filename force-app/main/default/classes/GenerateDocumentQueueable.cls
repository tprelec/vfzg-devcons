public class GenerateDocumentQueueable implements Queueable, Database.AllowsCallouts {
	@testVisible
	private Id parentId;
	public GenerateDocumentQueueable(Id parentId) {
		this.parentId = parentId;
	}
	public void execute(QueueableContext context) {
		csclmcb.AgreementExtensionController.generateDocument(parentId);
		/*
		PageReference ref = new PageReference(
			'/apex/csclmcb__GenerateDocument?id=' +
			parentId +
			'&s=1'
		);
		if (Test.isRunningTest()) {
			System.debug(System.LoggingLevel.INTERNAL, 'Bypassing get content call limitation.');
		} else {
			ref.getContent();
		}*/
	}
}
