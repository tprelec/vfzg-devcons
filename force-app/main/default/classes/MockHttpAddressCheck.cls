@isTest
public class MockHttpAddressCheck implements HttpCalloutMock {
	public HTTPResponse respond(HTTPRequest req) {
		// Create a fake response
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody(
			'{"street":"STREET","houseNumber":"1","houseNumberExt":"","zipCode":"3818EE","city":"AMERSFOORT","status":[],"availability":[{"name":"dtv-horizon","available":true},{"name":"packages","available":false},{"name":"mobile_internet","available":false},{"name":"fp200","available":true},{"name":"internet","available":true},{"name":"dtv","available":true},{"name":"catv","available":true},{"name":"telephony","available":true},{"name":"mobile","available":false},{"name":"catvfee","available":false},{"name":"fp500","available":true},{"name":"fp1000","available":true}],"footprint":"ZIGGO"}'
		);
		res.setStatusCode(200);
		return res;
	}
}