public with sharing class ClickApproveReminderService {
	@InvocableMethod(label='Send ClickApprove Reminder' description='Sends ClickApprove Reminder' category='ClickApprove')
	public static void sendReminder(List<ClickApproveReminderRequest> requests) {
		for (ClickApproveReminderRequest request : requests) {
			EmailService.EmailRequest emailReq = new EmailService.EmailRequest();
			emailReq.recipientId = request.approverId;
			emailReq.relatedToId = request.approvalId;
			emailReq.templateId = request.templateId;
			emailReq.emailHandler = 'ClickApproveEmailHandler';
			EmailService.sendEmail(new List<EmailService.EmailRequest>{ emailReq });
		}
	}

	public class ClickApproveReminderRequest {
		@InvocableVariable(label='Approval ID' description='ID of the Customer Approval Record' required=true)
		public Id approvalId;

		@InvocableVariable(label='Approver ID' description='ID of the Approver Contact' required=true)
		public Id approverId;

		@InvocableVariable(label='Template ID' description='ID of the Email Template Record' required=true)
		public Id templateId;
	}
}
