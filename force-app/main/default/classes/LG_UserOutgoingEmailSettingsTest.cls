@isTest
public class LG_UserOutgoingEmailSettingsTest {

    @isTest
    public static void runTestAsAdministrator() {
        Profile sysProfile = [SELECT Name, Id FROM Profile WHERE Name = 'System Administrator'];
        UserRole sysRole = [SELECT Name, Id FROM UserRole WHERE DeveloperName = 'Administrator'];
        User adminUser = TestUtils.generateTestUser('oes1', 'Testing' , sysProfile.Id, sysRole.Id);

        Profile salesProfile = [SELECT p.Id FROM Profile p WHERE p.Name = 'LG_NL Sales Management User'];
        UserRole salesRole = [SELECT r.Id FROM UserRole r WHERE r.Name = 'LG_NL D2D Manager'];
        User salesUser = TestUtils.generateTestUser('oes2', 'Testing' , salesProfile.Id, salesRole.Id);
        Database.insert(salesUser, true);

        Test.startTest();
        System.runAs(adminUser) {
            LG_UserOutgoingEmailSettingsExtension extension = new LG_UserOutgoingEmailSettingsExtension(new ApexPages.StandardController(salesUser));
            System.assertEquals(false, extension.editing);
            extension.startEditing();
            System.assertEquals(true, extension.editing);
            extension.cancelEditing();
            System.assertEquals(false, extension.editing);
            extension.startEditing();
            System.assertEquals(true, extension.editing);
            extension.selectedUser.SenderName = 'New Test Sender Name';
            extension.selectedUser.SenderEmail = 'new.test.sender.email@example.com';
            extension.confirm();
        }
        Test.stopTest();

        salesUser = [SELECT u.SenderName, u.SenderEmail FROM User u WHERE u.Id = :salesUser.Id];
        System.assertEquals('New Test Sender Name', salesUser.SenderName);
        System.assertEquals('new.test.sender.email@example.com', salesUser.SenderEmail);
    }

    @isTest
    public static void runTestAsOrdinaryUser() {
        Profile salesProfile = [SELECT p.Id FROM Profile p WHERE p.Name = 'LG_NL Sales Management User'];
        UserRole salesRole = [SELECT r.Id FROM UserRole r WHERE r.Name = 'LG_NL D2D Manager'];
        User salesUser = TestUtils.generateTestUser('oes2', 'Testing' , salesProfile.Id, salesRole.Id);

        User salesUser2 = TestUtils.generateTestUser('oes1', 'Testing' , salesProfile.Id, salesRole.Id);
        Database.insert(salesUser2, true);

        Test.startTest();
        System.runAs(salesUser) {
            LG_UserOutgoingEmailSettingsExtension extension = new LG_UserOutgoingEmailSettingsExtension(new ApexPages.StandardController(salesUser2));
            System.assertEquals(false, extension.editing);
            extension.startEditing();
            // Cannot switch to editing mode - no edit access.
            System.assertEquals(false, extension.editing);
            extension.cancelEditing();
            System.assertEquals(false, extension.editing);
            extension.startEditing();
            System.assertEquals(false, extension.editing);
            extension.selectedUser.SenderName = 'New Test Sender Name';
            extension.selectedUser.SenderEmail = 'new.test.sender.email@example.com';
            extension.confirm();
        }
        Test.stopTest();

        salesUser2 = [SELECT u.SenderName, u.SenderEmail FROM User u WHERE u.Id = :salesUser2.Id];
        System.assertEquals(null, salesUser2.SenderName);
        System.assertEquals(null, salesUser2.SenderEmail);
    }
}