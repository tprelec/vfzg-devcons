@isTest
public class TestSectionClauseDefAssTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Section/Clause Definition Ass';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'SCDA-23-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		csclm__Section_Clause_Definition_Association__c test = new csclm__Section_Clause_Definition_Association__c();
		insert test;

		csclm__Section_Clause_Definition_Association__c queryRecord = [
			SELECT Id, ExternalID__c
			FROM csclm__Section_Clause_Definition_Association__c
			LIMIT 1
		];
		System.assert(queryRecord.ExternalID__c != null, 'External Id should exist.');
		//Test.stopTest();
	}
}
