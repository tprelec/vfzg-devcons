@isTest
private with sharing class TestCS_ContractEndDateCalculator {
	@TestSetup
	private static void init() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			Account testAccount = CS_DataTest.createAccount('Test Account 1');
			insert testAccount;

			Contact contact1 = new Contact(
				AccountId = testAccount.id,
				LastName = 'Last',
				FirstName = 'First',
				Contact_Role__c = 'Consultant',
				Email = 'test@vf.com'
			);
			insert contact1;

			Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Service opportunity', simpleUser.id);
			insert opp;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Service Basket');
			basket.Name = 'Service Basket';
			basket.cscfga__Basket_Status__c = 'Valid';
			basket.Primary__c = true;
			basket.csbb__Synchronised_with_Opportunity__c = true;
			basket.csbb__Account__c = testAccount.Id;
			insert basket;

			cscfga__Product_Definition__c internetDefinition = CS_DataTest.createProductDefinition('Internet');
			internetDefinition.Product_Type__c = 'Fixed';
			insert internetDefinition;

			cscfga__Product_Configuration__c internetConf = CS_DataTest.createProductConfiguration(internetDefinition.Id, 'Internet', basket.Id);
			internetConf.Contract_Number_Group__c = 'BI';
			internetConf.cscfga__Contract_Term__c = 24;
			insert internetConf;

			CS_ContractEndDateCalculator.disableCalculator();

			csord__Subscription__c subscription1 = CS_DataTest.createSubscription(internetConf.Id);
			subscription1.csord__Identification__c = 's1';
			insert subscription1;

			csord__Subscription__c subscription2 = CS_DataTest.createSubscription(internetConf.Id);
			subscription2.csord__Identification__c = 's2';
			insert subscription2;

			csord__Service__c service1 = CS_DataTest.createService(internetConf.Id, subscription1.Id, 'serv1');
			service1.csord__Activation_Date__c = Date.newInstance(2022, 4, 1);
			insert service1;

			csord__Service__c service2 = CS_DataTest.createService(internetConf.Id, subscription2.Id, 'serv2');
			service2.csordtelcoa__Replaced_Service__c = service1.Id;
			insert service2;

			CS_ContractEndDateCalculator.enableCalculator();
		}
	}

	@isTest
	private static void testSetServiceEndDateAcquisition() {
		csord__Service__c targetService = [
			SELECT Id, csord__Activation_Date__c, csordtelcoa__Replaced_Service__c, csordtelcoa__Product_Configuration__c, Contract_End_Date__c
			FROM csord__Service__c
			WHERE csordtelcoa__Replaced_Service__c = NULL
		];

		cscfga__Product_Configuration__c config = [
			SELECT Id, cscfga__Contract_Term__c, Contract_Number_Group__c
			FROM cscfga__Product_Configuration__c
			WHERE Id = :targetService.csordtelcoa__Product_Configuration__c
		];

		Test.startTest();
		Boolean res = CS_ContractEndDateCalculator.setServiceEndDateAcquisition(targetService, config);
		Test.stopTest();

		System.assertEquals(true, res, 'End Date should be set');
	}

	@isTest
	private static void testSetServiceEndDateMacd() {
		csord__Service__c targetService = [
			SELECT Id, csord__Activation_Date__c, csordtelcoa__Replaced_Service__c, csordtelcoa__Product_Configuration__c, Contract_End_Date__c
			FROM csord__Service__c
			WHERE csordtelcoa__Replaced_Service__c != NULL
		];

		csord__Service__c replacedService = [
			SELECT Id, Contract_End_Date__c
			FROM csord__Service__c
			WHERE Id = :targetService.csordtelcoa__Replaced_Service__c
		];

		Test.startTest();
		Boolean res = CS_ContractEndDateCalculator.setServiceEndDateMacd(targetService, replacedService);

		Test.stopTest();

		System.assertEquals(true, res, 'End Date should be set');
	}

	@isTest
	private static void testRevalidateContractEndDate() {
		csord__Service__c targetService = [
			SELECT Id, csord__Activation_Date__c, csordtelcoa__Replaced_Service__c, csordtelcoa__Product_Configuration__c, Contract_End_Date__c
			FROM csord__Service__c
			WHERE csordtelcoa__Replaced_Service__c != NULL
		];

		cscfga__Product_Configuration__c config = [
			SELECT Id, cscfga__Contract_Term__c, Contract_Number_Group__c
			FROM cscfga__Product_Configuration__c
			WHERE Id = :targetService.csordtelcoa__Product_Configuration__c
		];

		csord__Service__c replacedService = [
			SELECT Id, Contract_End_Date__c
			FROM csord__Service__c
			WHERE Id = :targetService.csordtelcoa__Replaced_Service__c
		];

		Test.startTest();
		Boolean res = CS_ContractEndDateCalculator.revalidateServiceContractEndDate(targetService, null, config, replacedService);

		Test.stopTest();

		System.assertEquals(true, res, 'End Date should be set');
	}

	@isTest
	private static void testRevalidateContractEndDate2() {
		csord__Service__c targetService = [
			SELECT Id, csord__Activation_Date__c, csordtelcoa__Replaced_Service__c, csordtelcoa__Product_Configuration__c, Contract_End_Date__c
			FROM csord__Service__c
			WHERE csordtelcoa__Replaced_Service__c = NULL
		];

		csord__Service__c oldService = new csord__Service__c();
		targetService.csord__Activation_Date__c = Date.newInstance(2022, 4, 1);

		cscfga__Product_Configuration__c config = [
			SELECT Id, cscfga__Contract_Term__c, Contract_Number_Group__c
			FROM cscfga__Product_Configuration__c
			WHERE Id = :targetService.csordtelcoa__Product_Configuration__c
		];

		Test.startTest();
		Boolean res = CS_ContractEndDateCalculator.revalidateServiceContractEndDate(targetService, oldService, config, null);

		Test.stopTest();

		System.assertEquals(true, res, 'End Date should be set');
	}

	@isTest
	private static void testSetSubscriptionEndDate() {
		csord__Service__c targetService = [
			SELECT
				Id,
				csord__Activation_Date__c,
				csordtelcoa__Replaced_Service__c,
				csordtelcoa__Product_Configuration__c,
				Contract_End_Date__c,
				csord__Subscription__c
			FROM csord__Service__c
			WHERE csordtelcoa__Replaced_Service__c = NULL
		];

		csord__Service__c oldService = CS_DataTest.createService(
			targetService.csordtelcoa__Product_Configuration__c,
			targetService.csord__Subscription__c,
			'serv3'
		);
		insert oldService;

		targetService.Contract_End_Date__c = Date.newInstance(2022, 4, 1);

		List<csord__Service__c> services = new List<csord__Service__c>{ targetService };
		Map<Id, csord__Service__c> oldMap = new Map<Id, csord__Service__c>();
		oldMap.put(targetService.Id, oldService);

		Test.startTest();
		Boolean res = CS_ContractEndDateCalculator.setSubscriptionEndDate(services, oldMap);
		System.assertEquals(false, res, 'No changes on subscription');
		Test.stopTest();
	}
}
