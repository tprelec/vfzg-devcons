//@SuppressWarnings('PMD.CyclomaticComplexity')
public with sharing class SiteTriggerHandler extends TriggerHandler {
	List<Site__c> newSites;
	Map<Id, Site__c> oldSitesMap;
	List<Site__c> oldSites;

	Set<Id> siteExportNewSiteIds;
	Set<Id> siteExportAccountIds;
	Set<Id> siteExportChangedSiteIds;

	private void init() {
		newSites = (List<Site__c>) this.newList;
		oldSitesMap = (Map<Id, Site__c>) this.oldMap;
		oldSites = (List<Site__c>) this.oldList;
	}

	public override void beforeInsert() {
		init();
		preparePostalCodeCheck();
		setAccId();
	}

	public override void afterInsert() {
		init();
		triggerPostalCodeCheck();
		triggerSiteExport('insert');
	}

	public override void beforeUpdate() {
		init();
		resetPostalCodeCheck();
		setAccId();
	}

	public override void afterUpdate() {
		init();
		triggerSiteExport('update');
	}

	// set acc id for duplicate rule check
	private void setAccId() {
		for (Site__c s : newSites) {
			s.Acc_Id__c = s.Site_Account__c;
		}
	}

	private void preparePostalCodeCheck() {
		if (newSites.size() == 1 && newSites[0].Postal_Code_Check_Status__c == null) {
			// check if the amount of checks this week is still under 200. Otherwise do not automatically check
			Integer counter = [
				SELECT COUNT()
				FROM Site__c
				WHERE
					LastModifiedDate = LAST_N_DAYS:7
					AND (Last_dsl_check__c = LAST_N_DAYS:7
					OR Last_fiber_check__c = LAST_N_DAYS:7)
			];

			if (counter > Sales_Settings__c.getInstance().Max_weekly_postalcode_checks__c) {
				for (Site__c s : newSites) {
					s.Postal_Code_Check_Status__c =
						'Weekly maximum (' +
						Sales_Settings__c.getInstance().Max_weekly_postalcode_checks__c.intValue() +
						') reached. Please request manually.';
				}
			} else {
				for (Site__c s : newSites) {
					s.Postal_Code_Check_Status__c = 'Requested';
				}
			}
		}
	}

	private void postalCodeCheck(Set<Id> newSiteIds) {
		if (!System.isFuture()) {
			try {
				postalcodeCheckController.doOfflinePostalCodeCheckForSites(newSiteIds, 'dsl');
				postalcodeCheckController.doOfflinePostalCodeCheckForSites(newSiteIds, 'fiber');
			} catch (ExWebServiceCalloutException we) {
				for (Site__c s : newSites) {
					s.addError(we.getMessage());
				}
			}
		} else {
			try {
				postalcodeCheckController.doOnlinePostalCodeCheckForSites(newSiteIds, 'dsl');
				postalcodeCheckController.doOnlinePostalCodeCheckForSites(newSiteIds, 'fiber');
			} catch (ExWebServiceCalloutException we) {
				for (Site__c s : newSites) {
					s.addError(we.getMessage());
				}
			}
		}
	}

	private void triggerPostalCodeCheck() {
		Set<Id> newSiteIds = new Set<Id>();
		for (Site__c s : newSites) {
			if (s.Postal_Code_Check_Status__c == 'Requested') {
				newSiteIds.add(s.Id);
			}
		}
		if (!newSiteIds.isEmpty() && !System.isBatch()) {
			postalCodeCheck(newSiteIds);
		}
	}

	// This resets the last postalcode check dates if address is changed
	private void resetPostalCodeCheck() {
		for (Site__c s : newSites) {
			Site__c oldSite = oldSitesMap.get(s.Id);
			if (
				s.Site_Postal_Code__c != oldSite.Site_Postal_Code__c ||
				s.Site_House_Number__c != oldSite.Site_House_Number__c ||
				s.Site_House_Number_Suffix__c != oldSite.Site_House_Number_Suffix__c
			) {
				s.Last_dsl_check__c = null;
				s.Last_fiber_check__c = null;
			}
		}
	}

	private void updateSite() {
		siteExportNewSiteIds = new Set<Id>();
		siteExportAccountIds = new Set<Id>();
		siteExportChangedSiteIds = new Set<Id>();

		for (Site__c s : newSites) {
			Site__c oldSite = oldSitesMap.get(s.Id);
			List<Boolean> relevantFieldsChanged = new List<Boolean>();
			relevantFieldsChanged.add(s.Site_Account__c != oldSite.Site_Account__c);
			relevantFieldsChanged.add(s.Site_House_Number__c != oldSite.Site_House_Number__c);
			relevantFieldsChanged.add(
				s.Site_House_Number_Suffix__c != oldSite.Site_House_Number_Suffix__c
			);
			relevantFieldsChanged.add(s.Site_Postal_Code__c != oldSite.Site_Postal_Code__c);
			relevantFieldsChanged.add(s.Name != oldSite.Name);
			relevantFieldsChanged.add(s.Site_City__c != oldSite.Site_City__c);
			relevantFieldsChanged.add(s.Site_Phone__c != oldSite.Site_Phone__c);
			relevantFieldsChanged.add(s.Site_Street__c != oldSite.Site_Street__c);
			relevantFieldsChanged.add(s.Country__c != oldSite.Country__c);
			relevantFieldsChanged.add(s.Location_Type__c != oldSite.Location_Type__c);
			relevantFieldsChanged.add(s.Location_Alias__c != oldSite.Location_Alias__c);
			relevantFieldsChanged.add(s.Building__c != oldSite.Building__c);
			relevantFieldsChanged.add(s.SLA__c != oldSite.SLA__c);
			relevantFieldsChanged.add(s.Canvas_Street__c != oldSite.Canvas_Street__c);
			relevantFieldsChanged.add(s.Canvas_House_Number__c != oldSite.Canvas_House_Number__c);
			relevantFieldsChanged.add(
				s.Canvas_House_Number_Suffix__c != oldSite.Canvas_House_Number_Suffix__c
			);
			relevantFieldsChanged.add(s.Canvas_Postal_Code__c != oldSite.Canvas_Postal_Code__c);
			relevantFieldsChanged.add(s.Canvas_City__c != oldSite.Canvas_City__c);

			if (
				// check if relevant fields changed
				relevantFieldsChanged.contains(true) == true
			) {
				// filter out updates that are actually inserts but failed the first time
				if (s.BOP_export_datetime__c == null) {
					siteExportNewSiteIds.add(s.Id);
				} else {
					siteExportChangedSiteIds.add(s.Id);
				}
				siteExportAccountIds.add(s.Site_Account__c);
			}
		}
	}

	private void insertSite() {
		siteExportNewSiteIds = new Set<Id>();
		siteExportAccountIds = new Set<Id>();
		siteExportChangedSiteIds = new Set<Id>();

		for (Site__c s : newSites) {
			siteExportNewSiteIds.add(s.Id);
			siteExportAccountIds.add(s.Site_Account__c);
		}
	}

	private void siteExport(Set<Id> ids, Boolean isInsert) {
		if (!ids.isEmpty()) {
			ids.removeAll(SiteExport.sitesInExport);
			SiteExport.sitesInExport.addAll(ids);
			if (!ids.isEmpty()) {
				if (System.isFuture() || System.isBatch()) {
					SiteExport.scheduleSiteExportFromSiteIds(ids);
				} else {
					if (isInsert) {
						SiteExport.exportNewSitesOffline(ids);
					} else {
						SiteExport.exportUpdatedSitesOffline(ids);
					}
				}
			}
		}
	}

	// This triggers the export to BOP of sites.
	private void triggerSiteExport(String action) {
		if (action == 'update') {
			updateSite();
		} else {
			insertSite();
		}

		Set<Id> siteIdsForExport = new Set<Id>();
		if (!siteExportNewSiteIds.isEmpty() || !siteExportChangedSiteIds.isEmpty()) {
			// check if Contact's account or one of the BANs has BOPCode (otherwise do not export)
			// Key ring change -start
			Set<Id> accountIdsWithBopCode = KeyRingService.retrieveAccountIdsWithBopCode(
				siteExportAccountIds
			);

			// Key ring change -End
			for (Site__c s : [
				SELECT Id
				FROM Site__c
				WHERE
					(Id IN :siteExportChangedSiteIds
					OR Id IN :siteExportNewSiteIds)
					AND Site_Account__c IN :accountIdsWithBopCode
			]) {
				siteIdsForExport.add(s.Id);
			}
		}

		if (!siteIdsForExport.isEmpty()) {
			siteExportNewSiteIds.retainAll(siteIdsForExport);
			siteExport(siteExportNewSiteIds, true);
			siteExportChangedSiteIds.retainAll(siteIdsForExport);
			siteExport(siteExportChangedSiteIds, false);
		}
	}
}