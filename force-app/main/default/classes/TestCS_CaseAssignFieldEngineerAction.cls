@IsTest
public class TestCS_CaseAssignFieldEngineerAction {
    @isTest
    private static void assignFieldEngineerActionPositive() {
        // Crear Solucion, Delivery_Appointment y un Field_Engineer valido (usuario)
        // Luego crear un Case asignado a la cola
        User fieldEngineer = CS_DataTest.createSystemAdministratorUser();
        System.runAs(fieldEngineer) {
            Group newGroup = [SELECT Id FROM Group WHERE DeveloperName='CS_Test_Queue_for_Cases' LIMIT 1];

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            insert ord;

            Suborder__c solution = CS_DataTest.createSolution('ZIP Solution Test', ord.Id, true);

            Delivery_Appointment__c testAppointment = new Delivery_Appointment__c();
            testAppointment.OwnerId = UserInfo.getUserId();
            testAppointment.Name = 'Test Appointment';
            testAppointment.Start__c = Datetime.now();
            testAppointment.Duration__c = 120;
            testAppointment.Location__c = 'testLocation';
            testAppointment.Contact__c = null;
            testAppointment.Description__c = 'just a test';
            testAppointment.Related_Suborder__c = solution.Id;
            testAppointment.Field_Engineer__c = fieldEngineer.Id;
            insert testAppointment;

            Test.startTest();
            confirmAppointment(testAppointment);

            Case csComInstallSolutionCase = new Case();
            csComInstallSolutionCase.Subject = 'CS COM Install Solution Case';
            csComInstallSolutionCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CS COM Install Solution').getRecordTypeId();
            csComInstallSolutionCase.OwnerId = newGroup.Id;
            csComInstallSolutionCase.Case_Start_Date__c = Date.today();
            csComInstallSolutionCase.Suborder__c = solution.Id;
            insert csComInstallSolutionCase;

            Test.stopTest();

            Case theOnlyCase = [SELECT Id, OwnerId, Owner.Name FROM Case WHERE Subject = 'CS COM Install Solution Case' LIMIT 1];
            System.debug('The Owner ... ');
            System.debug(theOnlyCase.Owner.Name);
            System.assertEquals(fieldEngineer.Id, theOnlyCase.OwnerId);
        }
    }

    private static void confirmAppointment(Delivery_Appointment__c deliAppt) {
        Integer iMinutes = Integer.valueOf(deliAppt.Duration__c);
        Datetime dtEnd = deliAppt.Start__c.addMinutes(iMinutes);

        Suborder__c solution = new Suborder__c();
        solution.Id = deliAppt.Related_Suborder__c;
        solution.Confirmed_Installation__c = true;
        solution.Installation_Start__c = deliAppt.Start__c;
        solution.Installation_Start_Reminder__c = DateTime.now();
        if (!(deliAppt.Contact__c == null)) {
            solution.Technical_Contact__c = deliAppt.Contact__c;
        }
        solution.Installation_End__c = dtEnd;
        update solution;
    }
}