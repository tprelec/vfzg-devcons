public with sharing class OrderValidationFlagController {

	public OrderValidationFlagController(){
		result = null;

	}

	public sObject valObject {get;set;}
	//public OrderFormCache__c valCache {get;set;}
	public Order__c valOrder {get;set;}

	public String result {get;set;}

	public Boolean valid {
		get{
			result = OrderValidation.CheckObject(valObject, valOrder);
			system.debug(valObject);
			system.debug(result);
			if(result == ''){
				return true;
			} 

			return false;
		}
		set;
	}

}