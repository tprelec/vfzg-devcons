/**
 * @File Name          : TestBdsTestCheckPostalCodeKVK.cls
 * @Description        : Test class for BdsTestCheckPostalCodeKVK
 * @Author             : sebastian.ortiz@vodafoneziggo.com
 * @Group              :
 * @Last Modified By   : ChangeMeIn@UserSettingsUnder.SFDoc
 * @Last Modified On   : 4/15/2020, 7:25:54 PM
 * @Modification Log   :
 * Ver       Date            Author      		    Modification
 * 1.0    4/14/2020   sebastian.ortiz@vodafoneziggo.com     Initial Version
 **/
@isTest
public class TestBdsTestCheckPostalCodeKVK {
	private static final String FIELDS_MISSING_ERROR_MESSAGE = 'Field(s) missing or incorrect format';

	@isTest
	private static void returnSPCtoFlexWithoutBOP() {
		Account adminAccount = createAdminUserAccount();

		Site__c site = TestUtils.createSite(adminAccount);
		site.Site_House_Number__c = 3;
		site.Site_House_Number_Suffix__c = '';
		insert site;

		Site_Postal_Check__c sitePostalCheck = TestUtils.createSitePostalCheck(site.id);
		sitePostalCheck.Technology_Type__c = 'hfc';
		sitePostalCheck.Accessclass__c = 'hfc';
		sitePostalCheck.Access_Result_Check__c = 'ONNET';
		sitePostalCheck.Access_Infrastructure__c = 'ZIGGO';
		sitePostalCheck.Access_Active__c = true;
		insert sitePostalCheck;

		Test.startTest();
		RestContext.response = new RestResponse();

		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('1234AB', '3', '', '12345678');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().containsIgnoreCase('\"technologyType\" : \"hfc\"'),
			'Response body does not contain technologyType hfc'
		);
	}

	@isTest
	private static void returnSPCtoFlexWithoutBOPWhenUsingHTTPPostWithMock() {
		Account adminAccount = createAdminUserAccount();

		Site__c site = TestUtils.createSite(adminAccount);
		site.Site_House_Number__c = 3;
		site.Site_House_Number_Suffix__c = '';
		insert site;

		Site_Postal_Check__c sitePostalCheck = TestUtils.createSitePostalCheck(site.id);
		sitePostalCheck.Technology_Type__c = 'hfc';
		sitePostalCheck.Accessclass__c = 'hfc';
		sitePostalCheck.Access_Result_Check__c = 'ONNET';
		sitePostalCheck.Access_Infrastructure__c = 'ZIGGO';
		sitePostalCheck.Access_Active__c = true;
		insert sitePostalCheck;

		String successMockBody =
			'[ {' +
			'"vendor" : "DLM Entry",' +
			'"technologyType" : "hfc",' +
			'"remarks" : null,' +
			'"createdDate" : "2022-08-09T10:51:36.000Z",' +
			'"active" : true,' +
			'"accessResultCheck" : "ONNET",' +
			'"accessRegion" : null,' +
			'"accessPriority" : 2,' +
			'"accessMaxUpSpeed" : 232323,' +
			'"accessMaxDownSpeed" : 676767,' +
			'"accessInfrastructure" : null,' +
			'"accessClass" : "hfc"' +
			' }]';

		TestUtilMultiRequestMock.SingleRequestMock successMock = new TestUtilMultiRequestMock.SingleRequestMock(200, '', successMockBody, null);

		Test.setMock(HttpCalloutMock.class, successMock);

		Test.startTest();
		RestContext.response = new RestResponse();

		BdsTestCheckPostalCodeKVK.doPost('1234AB', '3', '', '12345678');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().containsIgnoreCase('\"technologyType\" : \"hfc\"'),
			'Response body does not contain technologyType hfc'
		);
	}

	@isTest
	private static void returnSPCtoFlexWithBOP() {
		Account adminAccount = createAdminUserAccount();

		Site__c site = TestUtils.createSite(adminAccount);
		site.Site_House_Number__c = 3;
		site.Site_House_Number_Suffix__c = '';
		insert site;

		Test.startTest();
		RestContext.response = new RestResponse();

		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('1234AB', '3', '', '12345678');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().contains(Label.Label_FLEX_NoPostalChecksTryLater),
			'Response body does not contain ' + Label.Label_FLEX_NoPostalChecksTryLater
		);
		System.assertEquals(400, RestContext.response.statusCode, 'Returned status code is not 400');
	}

	@isTest
	private static void returnSPCtoFlexWithBOPandNoAccount() {
		createOlbicoSettings();

		//create BAG_Mapping__c mapping
		List<BAG_Mapping__c> bagMappingList = new List<BAG_Mapping__c>();
		bagMappingList.add(new BAG_Mapping__c(Account_Field__c = 'Name', Name = 'bedrijfsnaam'));
		bagMappingList.add(new BAG_Mapping__c(Account_Field__c = 'NumberOfEmployees', Name = 'aantalmedewerkers'));
		bagMappingList.add(new BAG_Mapping__c(Account_Field__c = 'KVK_number__c', Name = 'kvk_8'));
		bagMappingList.add(new BAG_Mapping__c(Contact_Field__c = 'FirstName', Name = 'bevoegd_functionaris_voornaam'));
		bagMappingList.add(new BAG_Mapping__c(Contact_Field__c = 'LastName', Name = 'bevoegd_functionaris_achternaam'));
		bagMappingList.add(new BAG_Mapping__c(Contact_Field__c = 'MobilePhone', Name = 'mobielnummer'));
		insert bagMappingList;

		// Set mock callout class
		// This causes a fake response to be sent
		// from the class that implements HttpCalloutMock.
		// Used to mock response from OlbicoServiceJSON
		Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout());

		Test.startTest();
		RestContext.response = new RestResponse();

		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('1234AB', '3', '1', '12345678');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().contains(Label.LABEL_FLEX_NoKvk),
			'Response body does not contain ' + Label.LABEL_FLEX_NoKvk
		);
		System.assertEquals(400, RestContext.response.statusCode, 'Returned status code is not 400');
	}

	@isTest
	private static void returnErrorWhenNoDataFromFlex() {
		Test.startTest();
		RestContext.response = new RestResponse();

		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('', null, '', '');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().containsIgnoreCase(FIELDS_MISSING_ERROR_MESSAGE),
			'Response body does not contain ' + FIELDS_MISSING_ERROR_MESSAGE
		);
		System.assertEquals(400, RestContext.response.statusCode, 'Returned status code is not 400');
	}

	@isTest
	private static void returnErrorNoRestContext() {
		Test.startTest();
		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('', null, '', '');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().containsIgnoreCase(FIELDS_MISSING_ERROR_MESSAGE),
			'Response body does not contain ' + FIELDS_MISSING_ERROR_MESSAGE
		);
		System.assertEquals(400, RestContext.response.statusCode, 'Returned status code is not 400');
	}

	@isTest
	private static void returnSPCtoFlexWithoutBOPandNoSite() {
		createOlbicoSettings();
		createAdminUserAccount();

		// Set mock callout class
		// This causes a fake response to be sent
		// from the class that implements HttpCalloutMock.
		// Used to mock response from OlbicoServiceJSON
		Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout());

		Test.startTest();
		RestContext.response = new RestResponse();

		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('1234AB', '3', '', '12345678');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().contains(LABEL.Label_FLEX_NoPostalChecksTryLater),
			'Response body does not contain ' + Label.Label_FLEX_NoPostalChecksTryLater
		);
	}

	@isTest
	private static void returnSPCtoFlexWithoutBOPandExistingCheck() {
		final String technologyTypeFiber = '"technologyType" : "Fiber",';

		Account adminAccount = createAdminUserAccount();

		Site__c site = TestUtils.createSite(adminAccount);
		site.Site_Postal_Code__c = '1234AB';
		site.Site_House_Number__c = 5;
		site.Site_House_Number_Suffix__c = 'a';
		insert site;

		Site_Postal_Check__c sitePostalCheck = TestUtils.createSitePostalCheck(site.id);
		sitePostalCheck.Technology_Type__c = 'Fiber';
		sitePostalCheck.Accessclass__c = 'fiber';
		sitePostalCheck.Access_Result_Check__c = 'Groen';
		sitePostalCheck.Access_Infrastructure__c = 'ZIGGO';
		sitePostalCheck.Access_Max_Down_Speed__c = 2048;
		sitePostalCheck.Access_Max_Up_Speed__c = 2048;
		sitePostalCheck.Access_Vendor__c = null;
		sitePostalCheck.Manual__c = true;
		insert sitePostalCheck;

		Test.setCreatedDate(sitePostalCheck.Id, Date.today().addDays(-35));

		// Set mock callout class
		// Used to mock response from OlbicoServiceJSON
		Test.setMock(WebServiceMock.class, new TestUtilMockServices());

		Test.startTest();
		RestContext.response = new RestResponse();

		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('1234AB', '5', 'a', '12345678');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().contains(technologyTypeFiber),
			'Response body does not contain ' + technologyTypeFiber
		);
	}

	@isTest
	private static void returnErrorInvalidKVK() {
		Test.startTest();
		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('1234AB', '5', 'a', '123');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().containsIgnoreCase(Label.LABEL_FLEX_InvalidKVK),
			'Response body does not contain ' + Label.LABEL_FLEX_InvalidKVK
		);
		System.assertEquals(400, RestContext.response.statusCode, 'Returned status code is not 400');
	}

	@isTest
	private static void returnErrorInvalidPostalCode() {
		Test.startTest();
		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('A', '5', 'a', '12345678');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().containsIgnoreCase(Label.LABEL_FLEX_InvalidPostalCode),
			'Response body does not contain ' + Label.LABEL_FLEX_InvalidPostalCode
		);
		System.assertEquals(400, RestContext.response.statusCode, 'Returned status code is not 400');
	}

	@isTest
	private static void returnErrorInvalidHouseNumber() {
		Test.startTest();
		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('1234AB', 'AAA', 'a', '12345678');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().containsIgnoreCase(Label.LABEL_FLEX_InvalidHouseNo),
			'Response body does not contain ' + Label.LABEL_FLEX_InvalidHouseNo
		);
		System.assertEquals(400, RestContext.response.statusCode, 'Returned status code is not 400');
	}

	@isTest
	private static void returnErrorNoAddress() {
		createOlbicoSettings();
		createAdminUserAccount();

		Test.startTest();
		BdsTestCheckPostalCodeKVK.checkPostalCodeKVK('1234AB', '3', '', '12345678');
		Test.stopTest();

		System.assert(
			RestContext.response.responseBody.toString().contains(LABEL.LABEL_FLEX_NoAddress),
			'Response body does not contain ' + Label.LABEL_FLEX_NoAddress
		);
		System.assertEquals(400, RestContext.response.statusCode, 'Returned status code is not 400');
	}

	private static void createOlbicoSettings() {
		//create OlbicoSettings__c mapping
		List<OlbicoSettings__c> olbicoSettings = new List<OlbicoSettings__c>();
		olbicoSettings.add(
			new OlbicoSettings__c(
				Olbico_JSon_Endpoint__c = 'https://api.dqbox.com/api/search',
				Olbico_Json_Username__c = 'usr_vod2325sitf',
				Olbico_Json_Password__c = 'testpass',
				Olbico_Json_BAG_Streets__c = 'ef62f95b-30fc-4520-9be1-8af222e57ef6'
			)
		);
		insert olbicoSettings;
	}

	private static Account createAdminUserAccount() {
		TestUtils.autoCommit = true;
		User adminUser = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;

		Account adminAccount = TestUtils.createAccount(adminUser);
		adminAccount.KVK_number__c = '12345678';
		insert adminAccount;

		return adminAccount;
	}
}
