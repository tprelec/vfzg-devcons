@isTest
public class CS_BusinessMobileDeviceLookupTest {
	@TestSetup
	static void makeData() {
		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c bmDefinition = CS_DataTest.createProductDefinition('Business Mobile Device');
		bmDefinition.RecordTypeId = productDefinitionRecordType;
		insert bmDefinition;

		cspmb__Price_Item__c priceItem1 = new cspmb__Price_Item__c(
			Name = 'Apple Iphone',
			cspmb__Is_Active__c = true,
			cspmb__Effective_Start_Date__c = Date.today().addDays(-30),
			cspmb__Effective_End_Date__c = Date.today().addDays(300),
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Product_Definition_Name__c = bmDefinition.Name,
			Min_Duration__c = 1,
			Max_Duration__c = 9999,
			Mobile_Scenario__c = 'BM Scenario',
			Mobile_Add_on_category__c = 'Mobile hardware',
			SimType__c = 'PSIM'
		);

		cspmb__Price_Item__c priceItem2 = new cspmb__Price_Item__c(
			Name = 'Samsung',
			cspmb__Is_Active__c = true,
			cspmb__Effective_Start_Date__c = Date.today().addDays(-30),
			cspmb__Effective_End_Date__c = Date.today().addDays(-20),
			cspmb__Type__c = 'Commercial Product',
			cspmb__Role__c = 'Master',
			cspmb__Product_Definition_Name__c = bmDefinition.Name,
			Min_Duration__c = 1,
			Max_Duration__c = 9999,
			Mobile_Scenario__c = 'BM Scenario',
			Mobile_Add_on_category__c = 'Mobile hardware',
			SimType__c = 'ESIM'
		);

		List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>{ priceItem1, priceItem2 };
		insert priceItems;
	}

	@isTest
	private static void testRequiredAttributes() {
		CS_BusinessMobileDeviceLookup caLookup = new CS_BusinessMobileDeviceLookup();
		String result = caLookup.getRequiredAttributes();
		System.assertNotEquals(null, result, 'Return string must not be null');
	}

	@isTest
	public static void lookupTest() {
		cscfga__Product_Definition__c bmDefinition = [SELECT Id FROM cscfga__Product_Definition__c WHERE Name = 'Business Mobile Device' LIMIT 1];
		CS_BusinessMobileDeviceLookup bmLookup = new CS_BusinessMobileDeviceLookup();

		Map<String, String> searchFields1 = new Map<String, String>();
		searchFields1.put('ContractTermDevice', '12');
		searchFields1.put('MobileScenarioDevice', 'BM Scenario');
		searchFields1.put('SIMtype', 'PSIM');
		searchFields1.put('Today', string.valueOf(Date.today()));

		List<Object> result = bmLookup.doLookupSearch(searchFields1, String.valueOf(bmDefinition.Id), null, 0, 0);
		System.assertEquals(1, result.size(), 'Item should exist.');
	}
}
