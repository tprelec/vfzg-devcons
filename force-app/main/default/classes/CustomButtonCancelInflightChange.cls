global without sharing class CustomButtonCancelInflightChange extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		try {
			System.debug('Start CustomButtonCancelInflightChange');
			Boolean result = cancelInflightChange(basketId);

			if (!result) {
				return '{"status":"error","text":"Cannot cancel Inflight change on non-Inflight basket " }';
			}

			cscfga__Product_Basket__c basket = [
				SELECT Id, Name, csordtelcoa__Order_Under_Change__c, Primary__c
				FROM cscfga__Product_Basket__c
				WHERE Id = :basketId
			];

			PageReference redirectPage = new PageReference('/' + basket.csordtelcoa__Order_Under_Change__c);

			return '{"status":"ok","text":"Inflight canceled!","redirectURL":"' + redirectPage.getUrl() + '"}';
		} catch (Exception e) {
			return '{"status":"error","text":"Error happened while cancelling infligth change: " ' + e.getMessage() + '"}';
		}
	}
	@TestVisible
	private static Boolean cancelInflightChange(String basketId) {
		cscfga__Product_Basket__c basket = [
			SELECT Id, Name, csordtelcoa__Order_Under_Change__c, Primary__c
			FROM cscfga__Product_Basket__c
			WHERE Id = :basketId
		];

		if (basket.csordtelcoa__Order_Under_Change__c == null) {
			return false;
		}

		csord__Order__c order = [
			SELECT csordtelcoa__Participating_in_Inflight_Change__c, Id
			FROM csord__Order__c
			WHERE Id = :basket.csordtelcoa__Order_Under_Change__c
		];

		if (order == null) {
			return false;
		}

		order.csordtelcoa__Participating_in_Inflight_Change__c = false;

		List<csord__Service__c> allServices = [
			SELECT Id, cssdm__solution_association__c, Name, Applicable_for_inflight_change__c
			FROM csord__Service__c
			WHERE csord__Order__c = :basket.csordtelcoa__Order_Under_Change__c
		];

		for (csord__Service__c service : allServices) {
			service.Applicable_for_inflight_change__c = false;
		}

		List<COM_Delivery_Order__c> allDeliveryOrders = [
			SELECT Id, On_Hold__c
			FROM COM_Delivery_Order__c
			WHERE Order__c = :basket.csordtelcoa__Order_Under_Change__c
		];

		Set<Id> deliveryOrdersIds = new Set<Id>();

		for (COM_Delivery_Order__c delOrd : allDeliveryOrders) {
			delOrd.On_Hold__c = false;
			deliveryOrdersIds.add(delOrd.Id);
		}

		if (!allDeliveryOrders.isEmpty()) {
			List<CSPOFA__Orchestration_Process__c> orchProcesses = [
				SELECT Id, CSPOFA__Process_On_Hold__c
				FROM CSPOFA__Orchestration_Process__c
				WHERE COM_Delivery_Order__c IN :deliveryOrdersIds
			];
			for (CSPOFA__Orchestration_Process__c orchProcess : orchProcesses) {
				orchProcess.CSPOFA__Process_On_Hold__c = false;
			}
			if (!orchProcesses.isEmpty()) {
				update orchProcesses;
			}
		}

		if (!allServices.isEmpty()) {
			update allServices;
		}

		if (!allDeliveryOrders.isEmpty()) {
			update allDeliveryOrders;
		}

		update order;

		return true;
	}
}
