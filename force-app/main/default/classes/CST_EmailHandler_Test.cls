@isTest
public with sharing class CST_EmailHandler_Test {
	public CST_EmailHandler_Test() {
	}
	// @testSetup
	@isTest
	static void runTest() {
		Id accVFRecTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('VF_Account').getRecordTypeId();
		Id caseQuestionRecTypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CST_Question').getRecordTypeId();
		Id templateId = [SELECT id, name FROM EmailTemplate WHERE developername = 'CST_Generic_or_empty_email_template'].id;

		System.debug(caseQuestionRecTypeId);

		Account accVF = new Account(Name = 'test1', RecordTypeId = accVFRecTypeId);
		insert accVF;

		Case cstQuestion = new Case(AccountId = accVF.id, Origin = 'Phone', Status = 'New', Priority = 'P1');

		insert cstQuestion;
		Test.startTest();

		System.debug('Start');

		QuickAction.SendEmailQuickActionDefaults sendEmailDefaults = Test.newSendEmailQuickActionDefaults(cstQuestion.Id, null);

		sendEmailDefaults.setTemplateId(templateId);
		sendEmailDefaults.setInsertTemplateBody(true); // apply template above original email content
		sendEmailDefaults.setIgnoreTemplateSubject(false);

		EmailMessage emailMessage = (EmailMessage) sendEmailDefaults.getTargetSObject();
		emailMessage.RelatedToId = cstQuestion.id;
		emailMessage.ParentId = cstQuestion.id;
		emailMessage.ValidatedFromAddress = 'customerservices@vodafone.nl';
		emailMessage.FromAddress = 'customerservices@vodafone.nl';
		emailMessage.ToAddress = 'customerservices@vodafone.nl';
		insert emailMessage;

		//create QuickActionDefaults
		List<Map<String, Object>> defaultSettingAsObject = new List<Map<String, Object>>{
			new Map<String, Object>{
				'targetSObject' => new EmailMessage(),
				'contextId' => cstQuestion.Id,
				'ToAddress' => emailMessage.ToAddress,
				'CCAddress' => emailMessage.CcAddress,
				'actionType' => 'Email',
				'actionName' => 'EmailMessage._Reply',
				'fromAddressList' => new List<String>{ 'salesforce@test.com' },
				'Reply-To' => 'salesforce@test.com',
				'In-Reply-To' => 'salesforce@test.com',
				'emailTemplateId' => templateId,
				'insertTemplateBody' => true,
				'ReplyToEmailMessageId' => emailMessage.Id,
				'replyToId' => emailMessage.Id,
				'ignoreTemplateSubject' => false,
				'Subject' => '123'
			}
		};

		List<QuickAction.SendEmailQuickActionDefaults> defaultsSettings = (List<QuickAction.SendEmailQuickActionDefaults>) JSON.deserialize(
			JSON.serialize(defaultSettingAsObject),
			List<QuickAction.SendEmailQuickActionDefaults>.class
		);

		//Do the tests

		(new CST_EmailHandler()).onInitDefaults(defaultsSettings);
		System.debug('defaultsSettings after: ' + defaultsSettings);
		Test.stopTest();
		//System.assertEquals(null, defaultsSettings.QuickActionDefaults.targetSObject.ToAddress);
	}
}
