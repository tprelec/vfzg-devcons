@isTest
public class TestOrderFormComponentsListController {
	@isTest
	public static void constructAndSetOrder() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		TestUtils.createCompleteContract();
		VF_Contract__c contr = TestUtils.theContract;

		Test.startTest();
		Order__c order = TestUtils.createOrder(contr);
		OrderFormComponentsListController controller = new OrderFormComponentsListController();
		controller.theCurrentOrder = order;
		OrderFormComponentsListController.orderFormComponent component1 = controller.customerComponent;
		OrderFormComponentsListController.orderFormComponent component2 = controller.billingComponent;
		OrderFormComponentsListController.orderFormComponent component3 = controller.orderComponent;
		OrderFormComponentsListController.orderFormComponent component4 = OrderFormComponentsListController.deliveryComponent;
		OrderFormComponentsListController.orderFormComponent component5 = controller.ctnComponent;
		OrderFormComponentsListController.orderFormComponent component6 = controller.numberportingComponent;
		OrderFormComponentsListController.orderFormComponent component7 = controller.phonebookRegistrationComponent;
		OrderFormComponentsListController.orderFormComponent component8 = controller.orderProgressComponent;
		OrderFormComponentsListController.orderFormComponent component9 = controller.contractedProductsComponent;
		//Added Asserts to avoid suppressing PMD warnings for unused variables
		System.assertEquals('Customer', component1.componentType, 'Customer component not created');
		System.assertEquals('Billing', component2.componentType, 'Billing component not created');
		System.assertEquals('Order', component3.componentType, 'Order component not created');
		System.assertEquals('Delivery', component4.componentType, 'Delivery component not created');
		System.assertEquals(null, component5, 'CTN component created for fixed order');
		System.assertEquals(null, component6, 'Numberblocks component created');
		System.assertEquals(null, component7, 'PhonebookRegistration component created');
		System.assertEquals('Progress', component8.componentType, 'Progress component not created');
		System.assertEquals('ContractedProducts', component9.componentType, 'ContractedProducts component not created');
		Test.stopTest();
	}
}
