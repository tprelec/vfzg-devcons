@isTest
private class TestCOM_VfOrderGenerationFlowAction {
	@isTest
	private static void testGenerateVfOrders() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();

		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;

		VF_Contract__c contract = [SELECT Id FROM VF_Contract__c LIMIT 1];

		COM_VfOrderGenerationFlowAction.GenerationRequest gr = new COM_VfOrderGenerationFlowAction.GenerationRequest();
		gr.contractId = contract.Id;

		Test.startTest();
		List<COM_VfOrderGenerationFlowAction.GenerationResult> result = COM_VfOrderGenerationFlowAction.generateVfOrders(
			new List<COM_VfOrderGenerationFlowAction.GenerationRequest>{ gr }
		);
		Test.stopTest();

		System.AssertEquals(contract.Id, result[0].contractId, 'Contract Id is correct.');
		System.AssertEquals(true, result[0].generationSuccess, 'Execution should have succeeded.');
	}

	@isTest
	private static void testGenerateVfOrders_failure() {
		TestUtils.createEMPCustomSettings();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();

		List<OrderValidationField__c> ovForInsert = new List<OrderValidationField__c>();
		ovForInsert.add(TestUtils.ov('Order__c10000', 'Order__c', 'Account__r.One_Net__c'));
		ovForInsert.add(TestUtils.ov('Order__c10001', 'Order__c', 'Account__r.One_Net_Enterprise__c'));
		ovForInsert.add(TestUtils.ov('Order__c10002', 'Order__c', 'Account__r.One_Net_Express__c'));
		ovForInsert.add(TestUtils.ov('Order__c10003', 'Order__c', 'Account__r.One_Fixed__c'));
		insert ovForInsert;

		TestUtils.createCompleteContract();
		TestUtils.autoCommit = true;

		VF_Contract__c contract = [SELECT Id FROM VF_Contract__c LIMIT 1];

		COM_VfOrderGenerationFlowAction.GenerationRequest gr = new COM_VfOrderGenerationFlowAction.GenerationRequest();
		gr.contractId = contract.Id;

		Test.startTest();
		List<COM_VfOrderGenerationFlowAction.GenerationResult> result = COM_VfOrderGenerationFlowAction.generateVfOrders(
			new List<COM_VfOrderGenerationFlowAction.GenerationRequest>{ gr }
		);
		Test.stopTest();

		System.AssertEquals(false, result[0].generationSuccess, 'Excecution should have failed.');
	}
}
