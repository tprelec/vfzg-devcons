public class DataFactoryBatch implements Database.Batchable<SObject>{

    public Integer numberOfAccountsToPrepare;
    public Id customerRecordTypeId;
    Dealer_Information__c dealerInfo;
    User owner;

    public DataFactoryBatch(Integer numberOfAccountsToSetup, Id customerRecordTypeId){
        numberOfAccountsToPrepare = numberOfAccountsToSetup;
        customerRecordTypeId = customerRecordTypeId;
        dealerInfo = [select Id from Dealer_Information__c where name = 'Test Account Manager' limit 1];
        owner = [select Id from User where name = 'Test Account Manager' limit 1];

    }

    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([select id, Unify_Account_Type__c, Unify_Account_SubType__c, SBI_Main_Description__c, SBI_Description__c, NumberOfEmployees,
                                   Legal_Structure_Code__c, Legal_Structure__c, DUNS_Number__c, Date_of_Establishment__c, Current_Fixed_Owner__c,
                                   BIK_Code__c, type, Visiting_Country__c, Visiting_Housenumber1__c, visiting_City__c, visiting_Postal_Code__c,
                                   visiting_Street__c, Vat_Number__c,  phone, Mobile_Dealer__c, ownerId, recordTypeId, name, KVK_number__c
                                   from Account where type = 'Prospect' and (NOT Name like '%ITE2') and KVK_number__c != null limit :numberOfAccountsToPrepare]);
    }

    public void execute(Database.BatchableContext bc, List<SObject> scope){
        Datafactory.processData((List<Account>)scope, customerRecordTypeId, dealerInfo, owner);
    }

    public void finish(Database.BatchableContext bc){

    }
}