/**
 * @description       : Apex Test Class for the CategoryTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestCategoryTriggerHandler {
	@testSetup
	static void setup() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'Category',
			External_Number__c = 1,
			Object_Prefix__c = 'CAT-06-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;
	}

	@isTest
	static void testSetExternalIds() {
		Category__c objCategory = CS_DataTest.createCategory('Access');

		Test.startTest();
		insert objCategory;
		Test.stopTest();

		Category__c objCategoryUpdate = [SELECT Id, ExternalID__c FROM Category__c WHERE Id = :objCategory.Id LIMIT 1];

		System.assertEquals('CAT-06-000002', objCategoryUpdate.ExternalID__c, 'The External ID on Category was not set correctly.');
	}
}
