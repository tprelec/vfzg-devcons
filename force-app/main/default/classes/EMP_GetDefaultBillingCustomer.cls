public class EMP_GetDefaultBillingCustomer {
	public class Request {
		public String billingCustomerId;
		public String isHomeAccount;
		public Request(String billingCustomerId) {
			this.billingCustomerId = billingCustomerId;
			this.isHomeAccount = 'true';
		}
	}

	public class Response {
		public Integer status;
		public List<Data> data;
		public List<EMP_ResponseError.Error> error;
	}

	private class Data {
		public List<DefaultBillingCustomerId> defaultBillingCustomerId;
	}

	public class DefaultBillingCustomerId {
		public String defaultBillingCustomerId;
		public String isHomeAccount;
	}

	public static Response parse(String json) {
		return (Response) System.JSON.deserialize(json, Response.class);
	}
}