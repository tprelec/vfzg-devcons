public class ScheduledUsersDeactivator implements Schedulable {
	private final Scheduled_Users_Deactivation_Settings__c deactivationSetting = Scheduled_Users_Deactivation_Settings__c.getOrgDefaults();
	private final Integer FREEZING_DELAY = Integer.valueOf(deactivationSetting.Freezing_Delay__c);
	private final Integer FIRST_EMAIL_DELAY = Integer.valueOf(
		deactivationSetting.First_Email_Alert_Delay__c
	);
	private final Integer SECOND_EMAIL_DELAY = Integer.valueOf(
		deactivationSetting.Second_Email_Alert_Delay__c
	);

	private OrgWideEmailAddress ORG_WIDE_ADDRESS_EMAIL {
		get {
			if (ORG_WIDE_ADDRESS_EMAIL == null) {
				ORG_WIDE_ADDRESS_EMAIL = EmailService.getDefaultOrgWideEmailAddress();
			}
			return ORG_WIDE_ADDRESS_EMAIL;
		}
		private set;
	}

	private final String FIRST_EMAIL_TEMPLATE_NAME = 'First_Scheduled_User_Freezing_Alert';
	private final String SECOND_EMAIL_TEMPLATE_NAME = 'Second_Scheduled_User_Freezing_Alert';
	private final Set<String> EMAIL_TEMPLATE_NAMES = new Set<String>{
		FIRST_EMAIL_TEMPLATE_NAME,
		SECOND_EMAIL_TEMPLATE_NAME
	};

	private Map<Id, UserLogin> userIdToUserLogin;

	/**
	 *@description Freeze users after configurable number of inactive days, notifity via email before configurable number of days
	*/
	public void execute(SchedulableContext sc) {
		Map<String, Id> emailTemplateNameToId = new Map<String, Id>();
		for (EmailTemplate template : [
			SELECT Id, DeveloperName, Body
			FROM EmailTemplate
			WHERE DeveloperName IN :EMAIL_TEMPLATE_NAMES
		]) {
			emailTemplateNameToId.put(template.DeveloperName, template.Id);
		}
		List<Messaging.SingleEmailMessage> emailsToSend = new List<Messaging.SingleEmailMessage>();
		List<UserLogin> userLoginsToUpdate = new List<UserLogin>();

		for (User userForFreezing: getUsersForFreezing()) {
			// LastLoginDate field is not writable, mocking the return for test class
			Date lastUserLoginDate = Test.isRunningTest()
				? Date.today().addDays(-FREEZING_DELAY)
				: userForFreezing.LastLoginDate?.date();

			if (lastUserLoginDate == Date.today().addDays(-FREEZING_DELAY)) {
				UserLogin userLoginToUpdate = userIdToUserLogin.get(userForFreezing.Id);
				userLoginToUpdate.IsFrozen = true;
				userLoginsToUpdate.add(userLoginToUpdate);
			} else if (
				lastUserLoginDate == Date.today().addDays(-(FREEZING_DELAY - FIRST_EMAIL_DELAY))
			) {
				emailsToSend.add(
					createEmailAlertByEmailTemplateId(
						emailTemplateNameToId.get(FIRST_EMAIL_TEMPLATE_NAME),
						userForFreezing
					)
				);
			} else if (
				lastUserLoginDate == Date.today().addDays(-(FREEZING_DELAY - SECOND_EMAIL_DELAY))
			) {
				emailsToSend.add(
					createEmailAlertByEmailTemplateId(
						emailTemplateNameToId.get(SECOND_EMAIL_TEMPLATE_NAME),
						userForFreezing
					)
				);
			}
		}

		if (EmailService.checkDeliverability()) {
			Messaging.SendEmailResult[] results = Messaging.sendEmail(emailsToSend);
		}
		Database.update(userLoginsToUpdate, false);
	}

	private List<User> getUsersForFreezing() {
		userIdToUserLogin = new Map<Id, UserLogin>();
		for (UserLogin nonFrozenUserLogin : [
			SELECT Id, UserId
			FROM UserLogin
			WHERE IsFrozen = FALSE
		]) {
			userIdToUserLogin.put(nonFrozenUserLogin.UserId, nonFrozenUserLogin);
		}
		Set<Id> usersIdsToSelect = userIdToUserLogin.keySet();

		String usersSelect =
			'SELECT Id, LastLoginDate, Name' +
							 ' FROM User' +
							 ' WHERE Id IN :usersIdsToSelect' +
								   ' AND IsActive = TRUE ' +
								   ' AND IsPortalEnabled = FALSE' +
			' AND LastLoginDate = LAST_N_DAYS:' +
			FREEZING_DELAY +
			' AND LastLoginDate < LAST_N_DAYS:' +
			(FREEZING_DELAY - FIRST_EMAIL_DELAY);
		// LastLoginDate field is not writable, mocking the return for test class
		return Test.isRunningTest()
			? [SELECT Id, LastLoginDate, Name FROM User WHERE Email = 'testuser@vodafone.com']
			: Database.query(usersSelect);
	}

	@TestVisible
	private Messaging.SingleEmailMessage createEmailAlertByEmailTemplateId(
		Id emailTemplateId,
		User targetUser
	) {
		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		message.setTemplateId(emailTemplateId);
		message.setTargetObjectId(targetUser.Id);
		message.setOrgWideEmailAddressId(ORG_WIDE_ADDRESS_EMAIL.Id);
		message.saveAsActivity = false;
		return message;
	}
}