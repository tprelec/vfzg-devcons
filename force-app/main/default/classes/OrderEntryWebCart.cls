@SuppressWarnings('PMD.ExcessiveParameterList')
public without sharing class OrderEntryWebCart {
	private static final String REST_API = 'callout:WebShopApi' + '/shop/v2/rest/';

	private static List<Log__c> logs = new List<Log__c>();

	private static final Map<String, Integer> LEGAL_FORM = new Map<String, Integer>{
		'B.V.' => 2,
		'Buitenlandse Onderneming' => 6,
		'C.V.' => 6,
		'Co-operatieve vennootschap' => 8,
		'Eenmanszaak' => 1,
		'Maatschap' => 5,
		'N.V.' => 3,
		'Overheid' => 11,
		'Private Limited Company (LTD)' => 9,
		'Stichting' => 13,
		'V.O.F.' => 7,
		'Vereniging' => 10,
		'Anders' => 14
	};

	private static final Map<String, String> DOCUMENT_TYPE = new Map<String, String>{
		'Passport' => 'PASSPORT',
		'ID Card' => 'ID_CARD',
		'Driver License' => 'DUTCH_DRIVERS_LICENCE'
	};

	/**
	 * @description Callout to the WebShop REST API
	 * @param  endpoint REST API endpoint
	 * @param  method 	Request Method
	 * @param  body 	JSON request body
	 * @return REST API response string
	 */
	private static String callout(String endpoint, String method, String body, Map<String, String> headers) {
		HttpRequest request = new HttpRequest();
		request.setEndpoint(endpoint);
		request.setMethod(method);
		request.setHeader('Content-Type', 'application/json');
		if (headers != null) {
			for (String header : headers.keySet()) {
				request.setHeader(header, headers.get(header));
			}
		}
		request.setBody(body);
		Http http = new Http();
		HttpResponse response = http.send(request);
		Boolean isError = false;
		if (response.getStatusCode() < 200 || response.getStatusCode() >= 300) {
			isError = true;
		}
		createCalloutLog(request, headers, response, isError);
		if (isError) {
			throw new OrderEntryWebCartCalloutException(response.getBody());
		}
		return response.getBody();
	}

	/**
	 * @description Creates Log for every callout. Logs will be used for easier tracking of issues.
	 * @param  request        Http Request.
	 * @param  requestHeaders Http Request Headers.
	 * @param  response       Http Response.
	 * @param  isError        Indicates if response is an error.
	 */
	private static void createCalloutLog(HttpRequest request, Map<String, String> requestHeaders, HttpResponse response, Boolean isError) {
		Map<String, String> responseHeaders = new Map<String, String>();
		for (String header : response.getHeaderKeys()) {
			responseHeaders.put(header, response.getHeader(header));
		}
		logs.add(
			new Log__c(
				Apex_Class__c = 'OrderEntryWebCart',
				Method__c = 'callout',
				Date_Time__c = DateTime.now(),
				Level__c = isError ? 'ERROR' : 'INFO',
				Message__c = isError ? 'Order Entry Tool Provisioning Request Error' : 'Order Entry Tool Provisioning Request Success',
				Stack_Trace__c = String.join(
					new List<String>{
						'REQUEST:',
						request.getMethod() +
						' ' +
						request.getEndpoint(),
						JSON.serializePretty(requestHeaders),
						request.getBody(),
						'\n',
						'RESPONSE:',
						String.valueOf(response.getStatusCode()),
						JSON.serializePretty(responseHeaders),
						response.getBody()
					},
					'\n'
				),
				User__c = UserInfo.getUserId()
			)
		);
	}

	/**
	 * @description Create WebShop cart
	 * @param  existingCustomer True if customer has Fixed Interget from Ziggo
	 * @return Cart ID
	 */
	public static String createCart(Boolean existingCustomer) {
		OrderEntryWebCartJSON.CreateCart createCart = new OrderEntryWebCartJSON.CreateCart();
		createCart.converged = existingCustomer;
		String response = callout(REST_API + 'shopping-carts', 'POST', JSON.serialize(createCart), null);
		OrderEntryWebCartJSON.Cart cart = (OrderEntryWebCartJSON.Cart) JSON.deserialize(response, OrderEntryWebCartJSON.Cart.class);
		return cart.cartId;
	}

	/**
	 * @description Set chosen order type
	 * @param  cartId     Cart ID
	 * @param  isBusiness Always True in our case
	 * @return REST response string
	 */
	public static String setChosenOrderType(String cartId, Boolean isBusiness) {
		OrderEntryWebCartJSON.ChosenOrderType chosenOrderType = new OrderEntryWebCartJSON.ChosenOrderType();
		chosenOrderType.chosenOrderTypeIsBusiness = isBusiness;
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/chosen-order-type', 'PUT', JSON.serialize(chosenOrderType), null);
		return response;
	}

	/**
	 * @description Set personse data to cart
	 * @param  cartId         Cart ID
	 * @param  birthdate      Date of birth
	 * @param  gender         MALE or FEMALE
	 * @param  initials       Customer Initials
	 * @param  lastNamePrefix Last Name Prefix
	 * @param  lastName       Last Name
	 * @return REST response string
	 */
	public static String setPersonalData(String cartId, Date birthdate, String gender, String initials, String lastNamePrefix, String lastName) {
		OrderEntryWebCartJSON.Personal personal = new OrderEntryWebCartJSON.Personal();
		personal.powerOfAttorneyConsent = false;
		personal.birthdate = birthdate;
		personal.gender = gender;
		personal.initials = initials;
		personal.lastNamePrefix = lastNamePrefix;
		personal.lastName = lastName;
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/customer/personal', 'PUT', JSON.serialize(personal), null);
		return response;
	}

	/**
	 * @description Set contact data to cart
	 * @param  cartId Cart ID
	 * @param  email  Email
	 * @param  mobile Mobile Phone
	 * @param  phone  Phone
	 * @return REST response string
	 */
	public static String setContactData(String cartId, String email, String mobile, String phone) {
		OrderEntryWebCartJSON.Contact contact = new OrderEntryWebCartJSON.Contact();
		contact.email = email;
		contact.phone1 = mobile;
		contact.phone2 = phone;
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/customer/contact', 'PUT', JSON.serialize(contact), null);
		return response;
	}

	/**
	 * @description Set identification data to cart
	 * @param  cartId         Cart ID
	 * @param  expiryDate     Document Expiry Date
	 * @param  documentNumber Document Number
	 * @param  type           Documebt Type (ID_CARD, PASSPORT, DUCH_DRIVER_LICENSE)
	 * @param  nationality    Nationality
	 * @return REST response string
	 */
	public static String setIdentificationData(String cartId, Date expiryDate, String documentNumber, String type, String nationality) {
		OrderEntryWebCartJSON.Identification identification = new OrderEntryWebCartJSON.Identification();
		identification.expiryDate = expiryDate;
		identification.documentNumber = documentNumber;
		identification.type = type;
		identification.nationality = nationality;
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/customer/identification', 'PUT', JSON.serialize(identification), null);
		return response;
	}

	/**
	 * @description Set company data to cart
	 * @param  cartId              Cart ID
	 * @param  city                City
	 * @param  street              Street
	 * @param  houseNumber         House Number
	 * @param  houseNumberAddition House Number Suffix
	 * @param  postcode            Postal Code
	 * @param  kvkNumber           KVK Number
	 * @param  establishDate       Date of Establishment
	 * @param  name                Company Name
	 * @param  vatNumber           VAT Number
	 * @param  legalForm           Legal Form
	 * @return REST response string
	 */
	public static String setCompanyData(
		String cartId,
		String city,
		String street,
		String houseNumber,
		String houseNumberAddition,
		String postcode,
		String kvkNumber,
		Date establishDate,
		String name,
		String vatNumber,
		Integer legalForm
	) {
		OrderEntryWebCartJSON.Company company = new OrderEntryWebCartJSON.Company();
		company.address = new OrderEntryWebCartJSON.Address();
		company.address.country = 'NLD';
		company.address.city = city;
		company.address.street = street;
		company.address.houseNumber = houseNumber;
		company.address.houseNumberAddition = houseNumberAddition;
		company.address.postcode = postcode;
		company.cocNumber = kvkNumber;
		company.establishDate = establishDate;
		company.name = name;
		company.vatNumber = vatNumber;
		company.legalForm = legalForm;
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/customer/company', 'PUT', JSON.serialize(company), null);
		return response;
	}

	/**
	 * @description Set delivery address to cart
	 * @param  cartId              Cart ID
	 * @param  city                City
	 * @param  street              Street
	 * @param  houseNumber         House Number
	 * @param  houseNumberAddition House Number Suffix
	 * @param  postcode            Postal Code
	 * @return REST response string
	 */
	public static String setDeliveryAddressData(
		String cartId,
		String city,
		String street,
		String houseNumber,
		String houseNumberAddition,
		String postcode
	) {
		OrderEntryWebCartJSON.DeliveryAddress deliveryAddress = new OrderEntryWebCartJSON.DeliveryAddress();
		deliveryAddress.deliverToInvoiceAddress = false;
		deliveryAddress.deliveryAddress = new OrderEntryWebCartJSON.Address();
		deliveryAddress.deliveryAddress.country = 'NLD';
		deliveryAddress.deliveryAddress.city = city;
		deliveryAddress.deliveryAddress.street = street;
		deliveryAddress.deliveryAddress.houseNumber = houseNumber;
		deliveryAddress.deliveryAddress.houseNumberAddition = houseNumberAddition;
		deliveryAddress.deliveryAddress.postcode = postcode;
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/delivery-address', 'PUT', JSON.serialize(deliveryAddress), null);
		return response;
	}

	/**
	 * @description Set company data to cart
	 * @param  cartId Cart ID
	 * @param  items  Items that will be added to cart
	 * @return REST response string
	 */
	public static String addItemsToCart(String cartId, List<OrderEntryWebCartJSON.Item> items) {
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/items', 'POST', JSON.serialize(items), null);
		return response;
	}

	/**
	 * @description Set terms agreed
	 * @param  cartId Cart ID
	 * @return REST response string
	 */
	public static String setTermsAgreed(String cartId) {
		OrderEntryWebCartJSON.TermsAgreed termsAgreed = new OrderEntryWebCartJSON.TermsAgreed();
		termsAgreed.termsAgreed = true;
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/terms', 'PUT', JSON.serialize(termsAgreed), null);
		return response;
	}

	/**
	 * @description Set summary terms agreed
	 * @param  cartId Cart ID
	 * @return REST response string
	 */
	public static String setSummaryTermsAgreed(String cartId) {
		OrderEntryWebCartJSON.SummaryTermsAgreed summaryTermsAgreed = new OrderEntryWebCartJSON.SummaryTermsAgreed();
		summaryTermsAgreed.summaryTermsAgreed = true;
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/summary-terms', 'PUT', JSON.serialize(summaryTermsAgreed), null);
		return response;
	}

	/**
	 * @description Set payment data to cart
	 * @param  cartId Cart ID
	 * @param  bankAccountHolder Bank Account Holder Name
	 * @param  iban              IBAN
	 * @return REST response string
	 */
	public static String setPaymentData(String cartId, String bankAccountHolder, String iban) {
		OrderEntryWebCartJSON.PaymentInfo paymentInfo = new OrderEntryWebCartJSON.PaymentInfo();
		paymentInfo.powerOfAttorneyConsent = false;
		paymentInfo.accountHolder = bankAccountHolder;
		paymentInfo.iban = iban;
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/payment-info', 'PUT', JSON.serialize(paymentInfo), null);
		return response;
	}

	/**
	 * @description Set payment data to cart
	 * @param  cartId         Cart ID
	 * @param  contractNumber Current Contract Number
	 * @param  currentNumber  Current Mobile Number
	 * @param  shoppingCartItemPackageId Shopping Cart Package ID
	 * @return REST response string
	 */
	public static String setMobileNumberPorting(
		String cartId,
		String cartItem,
		String contractNumber,
		String currentNumber,
		Integer shoppingCartItemPackageId
	) {
		OrderEntryWebCartJSON.MobileNumberPorting mnp = new OrderEntryWebCartJSON.MobileNumberPorting();
		mnp.contractNumber = contractNumber;
		mnp.passCode = null;
		mnp.currentNumber = currentNumber;
		mnp.shoppingCartItemPackageId = shoppingCartItemPackageId;
		mnp.validationType = 'CUSTOMER_NUMBER';
		String response = callout(REST_API + 'shopping-carts/' + cartId + '/mobile-number-portings/' + cartItem, 'POST', JSON.serialize(mnp), null);
		return response;
	}

	/**
	 * @description Place order for cart
	 * @param  cartId Cart ID
	 * @return REST response string
	 */
	public static String placeOrder(String cartId) {
		if (true) {
			return null;
		}
		String response = callout(REST_API + '/orders/' + cartId + '/place', 'POST', '', null);
		OrderEntryWebCartJSON.Order order = (OrderEntryWebCartJSON.Order) JSON.deserialize(response, OrderEntryWebCartJSON.Order.class);
		return order.orderId;
	}

	/**
	 * @description Get legal form enum for WebShop
	 * @param  legalForm
	 * @return legal form enum
	 */
	public static Integer legalFormToEnum(String legalForm) {
		return LEGAL_FORM.get(legalForm);
	}

	/**
	 * @description Get document type for WebShop
	 * @param  documentType
	 * @return WebShop document type
	 */
	public static String documentType(String type) {
		return DOCUMENT_TYPE.get(type);
	}

	/**
	 * @description Sends Order Entry Tool Order using multiple Web Shop API requests
	 * @param  con       Salesforce Contact
	 * @param  acc       Salesforce Account
	 * @param  billAcc   Salesforce Billing Account
	 * @param  oeData    Order Entry Data
	 * @param  site      Installation Site
	 * @param  bundleMap Product Bundles Map
	 * @return Order Number
	 */
	public static String sendOrder(
		Contact con,
		Account acc,
		csconta__Billing_Account__c billAcc,
		OrderEntryData oeData,
		Site__c site,
		Map<Id, OE_Product_Bundle__c> bundleMap
	) {
		String cartId = createCart(oeData.b2cInternetCustomer);
		setChosenOrderType(cartId, true);
		setPersonalData(
			cartId,
			con.Birthdate,
			con.Gender__c != null ? con.Gender__c.toUpperCase() : null,
			(con.FirstName != null ? con.FirstName.substring(0, 1) : '') + (con.LastName != null ? con.LastName.substring(0, 1) : ''),
			con.LG_SalutationLocal__c,
			con.LastName
		);
		setContactData(cartId, con.Email, con.MobilePhone, con.Phone);
		setIdentificationData(
			cartId,
			con.Document_Expiration_Date__c,
			con.Document_Number__c,
			documentType(con.Document_Type__c),
			con.Document_Issuing_Country__c
		);
		setCompanyData(
			cartId,
			billAcc.csconta__Billing_City__c,
			billAcc.csconta__Street__c,
			billAcc.LG_HouseNumber__c,
			billAcc.LG_HouseNumberExtension__c,
			billAcc.csconta__PostCode__c,
			acc.KVK_Number__c,
			acc.Date_of_Establishment__c,
			acc.Name,
			acc.VAT_Number__c,
			legalFormToEnum(acc.LG_LegalForm__c)
		);
		setDeliveryAddressData(
			cartId,
			site.Site_City__c,
			site.Site_Street__c,
			String.valueOf(site.Site_House_Number__c),
			site.Site_House_Number_Suffix__c,
			site.Site_Postal_Code__c
		);
		// list of items for processing
		List<OrderEntryWebCartJSON.Item> items = new List<OrderEntryWebCartJSON.Item>();
		for (OrderEntryData.OrderEntryBundle bundle : oeData.bundles) {
			OrderEntryWebCartJSON.Item item = new OrderEntryWebCartJSON.Item();
			item.quantity = 1;
			item.action = 'ADD';
			item.withSim = false;
			item.sku = bundle.bundle.code;
			items.add(item);
		}
		// table of bundle index and item Id
		String response = addItemsToCart(cartId, items);
		Map<Integer, String> itemMap = new Map<Integer, String>();
		OrderEntryWebCartJSON.Cart cart = (OrderEntryWebCartJSON.Cart) JSON.deserialize(response, OrderEntryWebCartJSON.Cart.class);
		Integer j = 0;
		for (OrderEntryWebCartJSON.CartItem item : cart.shoppingCartItems) {
			if (item.parentId == null) {
				itemMap.put(j, item.id);
				j++;
			}
		}

		// add SIM cards
		j = -1;
		List<OrderEntryWebCartJSON.Item> simItems = new List<OrderEntryWebCartJSON.Item>();
		for (OrderEntryData.OrderEntryBundle bundle : oeData.bundles) {
			j++;
			if (bundleMap.get(bundle.bundle.id).Default__c == true) {
				continue;
			}
			OrderEntryWebCartJSON.Item item = new OrderEntryWebCartJSON.Item();
			item.quantity = 1;
			item.action = 'ADD';
			item.withSim = false;
			item.sku = bundleMap.get(bundle.bundle.id).SIM_Card_Product__r.Product_Code__c;
			item.parentId = Integer.valueOf(itemMap.get(j));
			simItems.add(item);
		}
		if (simItems.size() > 0) {
			addItemsToCart(cartId, simItems);
		}

		// number porting
		j = -1;
		for (OrderEntryData.OrderEntryBundle bundle : oeData.bundles) {
			j++;
			if (bundle.porting == null) {
				continue;
			}
			setMobileNumberPorting(cartId, itemMap.get(j), bundle.porting.contractNumber, bundle.porting.mobileNumber, j + 1);
		}
		setTermsAgreed(cartId);
		setSummaryTermsAgreed(cartId);
		setPaymentData(cartId, billAcc.LG_BankAccountHolder__c, billAcc.LG_BankAccountNumberIBAN__c);
		return placeOrder(cartId);
	}

	/**
	 * @description Prepares Cart and Places Order
	 * @param  oppId opportunityId
	 * @return Order Number
	 */
	public static String processOrder(Id oppId) {
		OrderEntryData oeData = OrderEntryController.getOrderEntryData(oppId);
		Id accId = oeData.accountId;
		Id conId = oeData.primaryContactId;
		Id billingAccId = oeData.payment.billingAccountId;
		Id siteId = oeData.sites[0].siteId;
		Account acc = [SELECT Id, Name, KVK_Number__c, Date_of_Establishment__c, VAT_Number__c, LG_LegalForm__c FROM Account WHERE Id = :accId];
		Contact con = [
			SELECT
				Id,
				Birthdate,
				Gender__c,
				FirstName,
				LastName,
				Email,
				MobilePhone,
				Phone,
				LG_SalutationLocal__c,
				Document_Expiration_Date__c,
				Document_Number__c,
				Document_Type__c,
				Document_Issuing_Country__c
			FROM Contact
			WHERE Id = :conId
		];
		csconta__Billing_Account__c billAcc = [
			SELECT
				Id,
				csconta__Billing_City__c,
				csconta__Street__c,
				LG_HouseNumber__c,
				LG_HouseNumberExtension__c,
				csconta__Postcode__c,
				LG_BankAccountHolder__c,
				LG_BankAccountNumberIBAN__c
			FROM csconta__Billing_Account__c
			WHERE Id = :billingAccId
		];
		Site__c site = [
			SELECT Id, Site_City__c, Site_Street__c, Site_House_Number__c, Site_House_Number_Suffix__c, Site_Postal_Code__c
			FROM Site__c
			WHERE Id = :siteId
		];
		Set<String> bundleIds = new Set<String>();
		for (OrderEntryData.OrderEntryBundle bundle : oeData.bundles) {
			bundleIds.add(bundle.bundle.id);
		}
		Map<Id, OE_Product_Bundle__c> bundleMap = new Map<Id, OE_Product_Bundle__c>(
			[SELECT Id, Default__c, SIM_Card_Product__r.Product_Code__c FROM OE_Product_Bundle__c WHERE Id IN :bundleIds]
		);

		String orderNumber;
		try {
			orderNumber = sendOrder(con, acc, billAcc, oeData, site, bundleMap);
		} catch (OrderEntryWebCartCalloutException ex) {
			// Do nothing. This is just to prevent following requests.
		}
		for (Log__c log : logs) {
			log.Record_ID__c = oppId;
		}
		insert logs;
		return orderNumber;
	}

	public class OrderEntryWebCartCalloutException extends Exception {
	}
}
