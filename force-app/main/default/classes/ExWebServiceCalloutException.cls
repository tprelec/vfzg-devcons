/**
 * @description			This exception should be thrown if a web service callout fails.
 * @author				Guy Clairbois
 */
public class ExWebServiceCalloutException extends Exception {

	public String get255CharMessage(){
		// special method to ensure that the error message returned fits the standard sf text field
		String shortMsg = this.getMessage().replace('Web service callout failed: ','');
		if(shortMsg.length() > 250){
			return this.getMessage().replace('Web service callout failed: ','').subString(0,250);
		} else {
			return shortMsg;
		}
	}

}