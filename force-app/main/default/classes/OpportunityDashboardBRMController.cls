public without sharing class OpportunityDashboardBRMController {

    public Set<Id> visibleRoleIds {get;set;}
    public List<String> visibleRoleNames {get;set;}
    public Integer fiscalYearInteger {get;set;}
    public Boolean variablesInitialized {get;set;}
    public Boolean showSecondaryTabs{get;set;}
    public Boolean viewAsCEO{get;set;}
    public String getSegmentOrVertical() {
         //return viewAsCEO?'Segment':'Vertical';
         return 'Segment';
    }
    public String getHideSegmentOrVertical() {
         //return viewAsCEO?'Vertical':'Segment';
         return 'Vertical';
    }

    public void activateSecondaryTabs(){
        showSecondaryTabs = true;
    }

    // these are used in dynamic query syntax so need to be explicitly declared locally
    public List<String> solSalesProductFamilies = OpportunityDashboardUtils.solSalesProductFamilies;
    public List<String> ctnProductFamilies = OpportunityDashboardUtils.ctnProductFamilies;
    //public List<String> hiddenProductFamilies = OpportunityDashboardUtils.hiddenProductFamilies;
    public List<String> enterpriseServiceLicenseProductSubFamilies = OpportunityDashboardUtils.enterpriseServiceLicenseProductSubFamilies;

    public Set<String> solSalesRoles {
        get{
            return OpportunityDashboardUtils.solSalesRoles;
        }
        set;
    }

    public String currentFiscalYear {
        get{
            if(currentFiscalYear==null) currentFiscalYear = [SELECT FiscalYearSettings.Name FROM Period WHERE Type = 'Year' AND StartDate <= TODAY AND EndDate >= TODAY].FiscalYearSettings.Name;
            return currentFiscalYear;
        }
        set;
    }
    public String fiscalYearSelected {
        get{
            // initialize with current fiscal year
            if(fiscalYearSelected==null) fiscalYearSelected = currentFiscalYear;
            return fiscalYearSelected;
        }
        set;
    }
    public String dataTypeSelected {
        get{
            // initialize with TCV
            if(dataTypeSelected==null) dataTypeSelected = 'MRR';
            return dataTypeSelected;
        }
        set;
    }
    public Set<Id> visibleHeadsOfRoleIds {
        get{
            if(visibleHeadsOfRoleIds == null){
                // get current user's manager
                Id currentUserManager = [Select Id, ParentRoleId From UserRole Where Id = :UserInfo.getUserRoleId()].ParentRoleId;
                // if the current user's role has no parent, use the current user's role (probably admin)
                if(currentUserManager == null){
                    currentUserManager = UserInfo.getUserRoleId();
                }

                // get all headsof roles
                //Set<Id> headOfRoleIds = GeneralUtils.getAllHeadsOfRoleIds();
                Set<Id> headOfRoleIds = GeneralUtils.getAllCommercialDirectorRoleIds();

                system.debug(currentUserManager);
                system.debug(headOfRoleIds);

                // determine which roles the current user's manager can see
                Set<Id> visibleRoleIdsParent = GeneralUtils.getAllSubRoleIds(new Set<Id>{currentUserManager});
                // and add the manager role itself
                if(headOfRoleIds.contains(currentUserManager))
                    visibleRoleIdsParent.add(currentUserManager);
                system.debug(visibleRoleIdsParent);

                visibleRoleIdsParent.retainAll(headOfRoleIds);
                visibleHeadsOfRoleIds = visibleRoleIdsParent;

                system.debug(visibleHeadsOfRoleIds);

                if(Special_Authorizations__c.getInstance().View_CEO_BRM_Dashboard__c){
                    if(GeneralUtils.roleNameToRoleId.containsKey(OpportunityDashboardUtils.ceoRole))
                        visibleHeadsOfRoleIds.add(GeneralUtils.roleNameToRoleId.get(OpportunityDashboardUtils.ceoRole));
                }

                system.debug(visibleHeadsOfRoleIds);


            }
            return visibleHeadsOfRoleIds;
        }
        set;
    }
    public String runAsUserRoleIdSelected {
        get{
            // initialize with current user's role
            if(runAsUserRoleIdSelected==null) {

                // if the current user is a head of, default to that view.
                // if the current user has CEO rights, default to that
                // Else default to the first head of
                if(visibleHeadsOfRoleIds.contains(UserInfo.getUserRoleId())){
                    runAsUserRoleIdSelected = UserInfo.getUserRoleId();
                } else if(Special_Authorizations__c.getInstance().View_CEO_BRM_Dashboard__c) {
                    runAsUserRoleIdSelected = GeneralUtils.roleNameToRoleId.get(OpportunityDashboardUtils.ceoRole);
                    viewAsCEO = true;
                } else {
                    for(Id urId : visibleHeadsOfRoleIds){
                        runAsUserRoleIdSelected = urId;
                        break;
                    }
                }
            }
            return runAsUserRoleIdSelected;
        }
        set{
            runAsUserRoleIdSelected = value;
            if(runAsUserRoleIdSelected == GeneralUtils.roleNameToRoleId.get(OpportunityDashboardUtils.ceoRole)){
                viewAsCEO = true;
            } else {
                viewAsCEO = false;
            }

        }
    }

    public OpportunityDashboardBRMController() {

        //if(ApexPages.currentpage().getParameters().get('showHiddenData') == '1'){
            // empty hidden product families so that they are shown
            //hiddenProductFamilies = new List<String>();
        //}
        //showSecondaryTabs = false;
        viewAsCEO = false;
        dashboardTypeSelected = 'director';

        // get stored values from cookie in order to continue with the view that the user used last
        Cookie brmSettings = ApexPages.currentPage().getCookies().get('brmSettingsDirector');
        if (brmSettings == null) {
            brmSettings = new Cookie('brmSettingsDirector', OpportunityDashboardUtils.getBrmSettingCookieString(dashboardTypeSelected, runAsUserRoleIdSelected, fiscalYearSelected, dataTypeSelected), null,-1,true, 'Strict');
        } else {
            String[] cookieData = OpportunityDashboardUtils.getBrmSettingCookie(brmSettings.getValue());
            system.debug(cookieData);
            try{
                if(cookieData.size() == 4 && cookieData[0] == dashboardTypeSelected) {
                    system.debug(cookieData);
                    if(cookieData[1] != null && cookieData[1] != 'null') runAsUserRoleIdSelected = cookieData[1];
                    if(cookieData[2] != null && cookieData[2] != 'null') fiscalYearSelected = cookieData[2];
                    if(cookieData[3] != null && cookieData[3] != 'null') dataTypeSelected = cookieData[3];
                }
            } catch (Exception e){
                // if this fails for any reason, re-initiate the cookie
                brmSettings = new Cookie('brmSettingsDirector', OpportunityDashboardUtils.getBrmSettingCookieString(dashboardTypeSelected, runAsUserRoleIdSelected, fiscalYearSelected, dataTypeSelected), null, -1, true, 'Strict');
            }
        }

        loadDatapoints();
    }


    public void initializeVariables(){
        //system.debug(hiddenProductFamilies);
        system.debug(runAsUserRoleIdSelected);
        //visibleRoleIds = GeneralUtils.getAllSubRoleIds(new Set<Id>{runAsUserRoleIdSelected});
        //visibleRoleIds.add(runAsUserRoleIdSelected);
        visibleRoleNames = new List<String>(GeneralUtils.getAllSubRoleNames(new Set<Id>{runAsUserRoleIdSelected}));
        visibleRoleNames.add(GeneralUtils.RoleIdToRoleName.get(runAsUserRoleIdSelected));

        fiscalYearInteger = Integer.valueOf(fiscalYearSelected);

        variablesInitialized = true;
        system.debug(visibleRoleNames);
    }

    public pageReference loadDatapoints(){
        showSecondaryTabs = false;

        initializeVariables();
        // reset the json strings so they will be requeried
        mainAcqDatapointsJSON = null;
        mainRetDatapointsJSON = null;
        //solSalesDatapointsJSON = null;
        //itwDatapointsJSON = null;
        //msDatapointsJSON = null;

        system.debug(runAsUserRoleIdSelected);
        system.debug(visibleRoleNames);

        // update the cookie with the latest selection
        String cookieString = OpportunityDashboardUtils.getBrmSettingCookieString(dashboardTypeSelected,runAsUserRoleIdSelected,fiscalYearSelected,dataTypeSelected);
        Cookie brmSettings = new Cookie('brmSettingsDirector', cookieString, null, -1, true, 'Strict');
        ApexPages.currentPage().setCookies(new Cookie[]{brmSettings});

        return null;
    }

    public String basicQueryFields{
        get{
            //if(basicQueryFields == null){
                basicQueryFields = 'SELECT CLC__c,';
                //basicQueryFields += 'Product_Family__c,';
                basicQueryFields += 'PricebookEntry.Product2.Taxonomy__r.Product_Family__c,';
                basicQueryFields += 'Opportunity.StageName,';
                //basicQueryFields += 'Opportunity.Segment__c Segment,';
                basicQueryFields += 'FISCAL_YEAR(Opportunity.CloseDate) fy,';
                basicQueryFields += 'FISCAL_QUARTER(Opportunity.CloseDate) fq,';

                if(dataTypeSelected=='CTN' || dataTypeSelected=='License') {
                    basicQueryFields += 'SUM(Quantity) SumAmount,';//SumQuantity,';
                } else if(dataTypeSelected=='MRR'){
                    basicQueryFields += 'SUM(MRR1__c) SumAmount,';//SumMRR,';
                } else {
                    basicQueryFields += 'SUM(TotalPrice) SumAmount,';//SumTotalPrice,';
                }

                basicQueryFields += 'Opportunity.RoleCreateOrChangeOwner__c';
            //}
            return basicQueryFields;
        }
        set;
    }

    public String basicQueryFilters{
        get{
            basicQueryFilters = ' FROM OpportunityLineItem WHERE ';
            //basicQueryFilters += 'Opportunity.StageName != \'Closed Lost\' AND ';
            basicQueryFilters += ' FISCAL_YEAR(Opportunity.CloseDate) =  :fiscalYearInteger';
            //basicQueryFilters += ' AND Opportunity.Account.VGE__c = false ';
            basicQueryFilters += ' AND Opportunity.RoleCreateOrChangeOwner__c in :visibleRoleNames ';
            basicQueryFilters += ' AND Opportunity.RoleCreateOrChangeOwner__c NOT in :solSalesRoles ';
            if (dataTypeSelected == 'CTN'){
                basicQueryFilters += ' AND Product_Family__c IN :ctnProductFamilies'; // TAXONOMY HAZARD!
            } else if (dataTypeSelected == 'License') {
                basicQueryFilters += ' AND PricebookEntry.Product2.Model__c IN :enterpriseServiceLicenseProductSubFamilies';
            } else {
                 //basicQueryFilters += ' AND Product_Family__c NOT IN :hiddenProductFamilies'; // TAXONOMY HAZARD!
            }
            return basicQueryFilters;
        }
        set;
    }

    public String basicQueryGroupBy{
        get{
            if(basicQueryGroupBy == null){
                basicQueryGroupBy = ' GROUP BY CLC__c,';
                //basicQueryGroupBy += 'Product_Family__c,';
                basicQueryGroupBy += 'PricebookEntry.Product2.Taxonomy__r.Product_Family__c,';
                basicQueryGroupBy += 'Opportunity.StageName,';
                //basicQueryGroupBy += 'Opportunity.Segment__c,';
                basicQueryGroupBy += 'FISCAL_YEAR(Opportunity.CloseDate),';
                basicQueryGroupBy += 'FISCAL_QUARTER(Opportunity.CloseDate),';
                basicQueryGroupBy += 'Opportunity.RoleCreateOrChangeOwner__c';

            }
            return basicQueryGroupBy;
        }
        set;
    }

    public String mainAcqDatapointsSOQLFilter{
        get {
            // add filters to public string, so they can be reused in drilldown
            mainAcqDatapointsSOQLFilter = basicQueryFilters;
            //mainAcqDatapointsSOQLFilter += ' AND Opportunity.Owner.UserType != \'PowerPartner\'';
            mainAcqDatapointsSOQLFilter += ' AND CLC__c = \'Acq\' ';
            //mainAcqDatapointsSOQLFilter += ' AND Opportunity.RoleCreateOrChangeOwner__c in :visibleRoleNames';
            return mainAcqDatapointsSOQLFilter;
        }
        set;
    }

    public String mainAcqDatapointsJSON {
        get{
            //if(mainAcqDatapointsJSON == null){
                if(!variablesInitialized) initializeVariables();

                // load opportunity lineitems for Main tab
                String mainAcqQuery = basicQueryFields;
                // add filters
                mainAcqQuery += mainAcqDatapointsSOQLFilter;
                // add groupby
                mainAcqQuery += basicQueryGroupBy;
                system.debug(mainAcqQuery);
                //List<AggregateResult> mainAcqars = Database.query(mainAcqQuery);
                //mainAcqDatapointsJSON = JSON.serialize(generateDatapointList(mainAcqQuery,'Acquisition Sales'));
            //}
            //return mainAcqDatapointsJSON;
            return JSON.serialize(generateDatapointList(mainAcqQuery,'Acquisition Sales'));
        }
        set;
    }
    public String mainRetDatapointsSOQLFilter{
        get {
            // add filters to public string, so they can be reused in drilldown
            mainRetDatapointsSOQLFilter = basicQueryFilters;
            //mainRetDatapointsSOQLFilter += ' AND Opportunity.Owner.UserType != \'PowerPartner\'';
            mainRetDatapointsSOQLFilter += ' AND CLC__c = \'Ret\' ';
            //mainRetDatapointsSOQLFilter += ' AND Opportunity.RoleCreateOrChangeOwner__c in :visibleRoleNames';
            return mainRetDatapointsSOQLFilter;
        }
        set;
    }

    public String mainRetDatapointsJSON {
        get{
            //if(mainRetDatapointsJSON == null){
                if(!variablesInitialized) initializeVariables();

                // load opportunity lineitems for mainRet tab
                String mainRetQuery = basicQueryFields;
                // add filters
                mainRetQuery += mainRetDatapointsSOQLFilter;
                // add groupby
                mainRetQuery += basicQueryGroupBy;

            //}
            return JSON.serialize(generateDatapointList(mainRetQuery,'Retention Sales'));
        }
        set;
    }


    public List<Datapoint> generateDatapointList(String query, String targetType){

        List<Datapoint> datapoints = new List<Datapoint>();
        transient Map<String,Map<String,Map<String,Map<String,Map<String,Datapoint>>>>> datapointMap = new Map<String,Map<String,Map<String,Map<String,Map<String,Datapoint>>>>>();

        if(Test.isRunningTest()) query += ' LIMIT 10';

        transient List<AggregateResult> ars = Database.query(query); // need to refrain from using querymore, otherwise we will get this error at some point:
                                                            // Aggregate query does not support queryMore(), use LIMIT to restrict the results to a single batch
        // load opportunityProducts
        for(AggregateResult ar : ars){
            Datapoint dp = new Datapoint();
            dp.fy = (Integer) ar.get('fy');
            dp.fq = (Integer) ar.get('fq');

            if(targetType == 'Mite Segment'){
                String acctMgr = (String)ar.get('AccountManager');
                system.debug(acctMgr);
                if(acctMgr == 'null' || acctMgr == null || acctMgr == ''){
                    dp.accountManager = 'Other';
                } else {
                    dp.accountManager = 'Mite';
                }
            } else {
                dp.accountManager = '';//acctMgr;
            }
            String role = (String)ar.get('RoleCreateOrChangeOwner__c');
            //dp.segment = (String)ar.get('Segment');
            if(viewAsCEO){
                if(GeneralUtils.roleToMainSegment.get(role)==null){
                    if(role.endsWith('Partner User')){
                        dp.Segment = 'Business Partner Sales';
                    } else {
                        dp.Segment = 'Other';
                    }
                } else {
                    dp.Segment = GeneralUtils.roleToMainSegment.get(role);
                }
            } else {
                if(GeneralUtils.roleToSegment.get(role)==null){
                    if(role.endsWith('Partner User')){
                        dp.Segment = 'Business Partners';
                    } else {
                        dp.Segment = 'Other';
                    }
                } else {
                    dp.Segment = GeneralUtils.roleToSegment.get(role);
                }
            }
            // skip any segments mapped to solution sales (done in soql now)
            //if(dp.Segment == null || dp.Segment.endsWith('Solution Sales')){
            //    continue;
            //}
            dp.productFamily = (String)ar.get('Product_Family__c');
            //dp.productFamily = dp.productFamily.replace('Acq ','').replace('Ret ','');

            /*if(dataTypeSelected=='CTN' || dataTypeSelected=='License') {
                dp.amount = (Decimal)ar.get('SumQuantity');
            } else if(dataTypeSelected=='MRR'){
                dp.amount = (Decimal)ar.get('SumTotalPrice');
            } else {
                dp.amount = (Decimal)ar.get('SumTotalPrice');
            }*/
            dp.amount = (Decimal)ar.get('SumAmount');
            dp.Type = (String)ar.get('StageName');


            addToDatapointMapping(
                    dp.fq+''+dp.fy,
                    dp.vertical,
                    dp.productFamily,
                    dp.segment,
                    dp.Type,
                    dp,
                    datapointMap
            );
        }

        // load targets
        for(AggregateResult ar : [SELECT
                                    UserRoleName__c roleName,
                                    Product_Family__c,
                                    Segment__c,
                                    FISCAL_QUARTER(Period_startdate__c) fq,
                                    FISCAL_YEAR(Period_startdate__c) fy,
                                    //Fiscal_Quarter__c,
                                    //Fiscal_Year__c,
                                    SUM(Revenue__c) sumRevenue
                                From
                                    Sales_Target__c
                                Where
                                    FISCAL_YEAR(Period_startdate__c) = :Integer.valueOf(fiscalYearSelected)
                                AND(
                                    UserRoleName__c in :visibleRoleNames
                                //OR
                                   // User__c = :UserInfo.getUserId()
                                )
                                AND
                                    User__c = null
                                AND
                                    Segment__c = :targetType
                                AND
                                    Data_type__c = :dataTypeSelected
                                GROUP BY
                                    UserRoleName__c,
                                    Product_Family__c,
                                    Segment__c,
                                    FISCAL_QUARTER(Period_startdate__c),
                                    FISCAL_YEAR(Period_startdate__c)
                                ])
        {

            Datapoint dp = new Datapoint();
            dp.fy = Integer.valueOf( ar.get('fy'));
            dp.fq = Integer.valueOf( ar.get('fq'));
            String role = (String)ar.get('RoleName');
            if(targetType == 'Mite Segment'){
                dp.accountManager = 'Mite';

            } else {
                dp.accountManager = 'Other';
            }

            if(viewAsCEO){
                //dp.segment = (String)ar.get('Segment__c');
                dp.segment  = GeneralUtils.roleToMainSegment.get(role)==null?'Other':GeneralUtils.roleToMainSegment.get(role);
            } else {
                dp.segment  = GeneralUtils.roleToSegment.get(role)==null?'Other':GeneralUtils.roleToSegment.get(role);
            }
            dp.productFamily = (String)ar.get('Product_Family__c');
            //dp.productFamily = dp.productFamily.replace('Acq ','').replace('Ret ','');
            dp.amount = (Decimal)ar.get('sumRevenue');
            dp.Type = 'Target';


            addToDatapointMapping(
                    dp.fq+''+dp.fy,
                    dp.vertical,
                    dp.productFamily,
                    dp.segment,
                    'Target',
                    dp,
                    datapointMap
            );


        }
        system.debug(datapoints);
        system.debug(datapointMap);

        // make sure all types exist in the map, so that we always have all columns in the pivot
        Set<String> allTypes = new Set<String>{'Commit','Closed Won','Target','Gap','Offer','Funnel','Closed Lost','Coverage'};
        for(String p : datapointMap.keySet()){
            for(String v : datapointMap.get(p).keySet()){
                for(String pf : datapointMap.get(p).get(v).keySet()){
                    for(String s : datapointMap.get(p).get(v).get(pf).keySet()){

                        Datapoint dp = new Datapoint();
                        dp.fq = Integer.valueOf(p.subString(0,1));
                        dp.fy = Integer.valueOf(p.substring(1,5));
                        dp.Amount = 0;
                        dp.Vertical = v;
                        dp.ProductFamily = pf;
                        dp.Segment = s;

                        for(String theType : allTypes){
                            if(!datapointMap.get(p).get(v).get(pf).get(s).containsKey(theType)) {
                                Datapoint dpCopy = dp.clone();
                                //dpCopy.sType = theType;
                                dpCopy.Type = theType;
                                datapointMap.get(p).get(v).get(pf).get(s).put(theType,dpCopy);
                            }
                        }
                    }
                }
            }
        }

        for(String p : datapointMap.keySet()){
            for(String v : datapointMap.get(p).keySet()){
                for(String pf : datapointMap.get(p).get(v).keySet()){
                    for(String s : datapointMap.get(p).get(v).get(pf).keySet()){
                        for(String t : datapointMap.get(p).get(v).get(pf).get(s).keySet()){
                            datapoints.add(datapointMap.get(p).get(v).get(pf).get(s).get(t));
                            datapointMap.get(p).get(v).get(pf).get(s).remove(t);
                            //system.debug(datapoints);
                        }
                    }
                }
            }
        }

        return datapoints;
    }

    public void addToDatapointMapping(String period, String vertical, String productFamily, String segment, String type, Datapoint dp,Map<String,Map<String,Map<String,Map<String,Map<String,Datapoint>>>>> datapointMap){

        if(datapointMap.containsKey(period)){
            Map<String,Map<String,Map<String,Map<String,Datapoint>>>> tempMap4 = datapointMap.get(period);
            if(tempMap4.containsKey(vertical)){
                Map<String,Map<String,Map<String,Datapoint>>> tempMap3 = tempMap4.get(vertical);
                if(tempMap3.containsKey(productFamily)){
                    Map<String,Map<String,Datapoint>> tempMap2 = tempMap3.get(productFamily);
                    if(tempMap2.containsKey(segment)){
                        Map<String,Datapoint> tempMap = tempMap2.get(segment);
                        tempMap = addTypeToMap(type,dp,tempMap);
                        tempMap2.put(segment,tempMap);
                    } else {
                        Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>());
                        tempMap2.put(segment,tempMap);
                    }
                    tempMap3.put(productFamily,tempMap2);
                } else {
                    Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>());
                    Map<String,Map<String,Datapoint>> tempMap2 = new Map<String,Map<String,Datapoint>>{segment=>tempMap};
                    tempMap3.put(productFamily,tempMap2);
                }
                tempMap4.put(vertical,tempMap3);
            } else {
                Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>());
                Map<String,Map<String,Datapoint>> tempMap2 = new Map<String,Map<String,Datapoint>>{segment=>tempMap};
                Map<String,Map<String,Map<String,Datapoint>>> tempMap3 = new Map<String,Map<String,Map<String,Datapoint>>>{productFamily=>tempMap2};
                tempMap4.put(vertical,tempMap3);
            }
            datapointMap.put(period,tempMap4);
        } else {
            Map<String,Datapoint> tempMap = addTypeToMap(type,dp,new Map<String,Datapoint>());
            Map<String,Map<String,Datapoint>> tempMap2 = new Map<String,Map<String,Datapoint>>{segment=>tempMap};
            Map<String,Map<String,Map<String,Datapoint>>> tempMap3 = new Map<String,Map<String,Map<String,Datapoint>>>{productFamily=>tempMap2};
            Map<String,Map<String,Map<String,Map<String,Datapoint>>>> tempMap4 = new Map<String,Map<String,Map<String,Map<String,Datapoint>>>>{vertical=>tempMap3};
            datapointMap.put(period,tempMap4);
        }

    }

    public Map<String,Datapoint> addTypeToMap(String type,Datapoint dp,Map<String,Datapoint> theMap){
        // logic for assigning types to BRM categories
        if(type == 'Prospect') {
            theMap = addTypeAmountToMap('Funnel',dp,theMap);
            theMap = addTypeAmountToMap('Coverage',dp,theMap);
        } else if(type == 'Qualify') {
            theMap = addTypeAmountToMap('Funnel',dp,theMap);
            theMap = addTypeAmountToMap('Coverage',dp,theMap);
        } else if(type == 'Offer') {
            theMap = addTypeAmountToMap('Offer',dp,theMap);
            theMap = addTypeAmountToMap('Coverage',dp,theMap);
        } else if(type == 'Closing' || type =='Closing by SAG') {
            theMap = addTypeAmountToMap('Commit',dp,theMap);
            theMap = addTypeAmountToMap('Gap',dp,theMap);
            theMap = addTypeAmountToMap('Coverage',dp,theMap);
        } else if(type == 'Closed Won') {
            theMap = addTypeAmountToMap('Commit',dp,theMap);
            theMap = addTypeAmountToMap('Closed Won',dp,theMap);
            theMap = addTypeAmountToMap('Gap',dp,theMap);
            theMap = addTypeAmountToMap('Coverage',dp,theMap);
        } else if(type == 'Target') {
            theMap = addTypeAmountToMap('Target',dp,theMap);
            theMap = addTypeAmountToMap('TargetGap',dp,theMap);
        } else if(type == 'Closed Lost') {
            theMap = addTypeAmountToMap('Closed Lost',dp,theMap);
        }
        return theMap;
    }

    public Map<String,Datapoint> addTypeAmountToMap(String type,Datapoint dpSource,Map<String,Datapoint> theMap){
        Datapoint dp = dpSource.clone();
        if(type=='TargetGap'){
            type='Gap';
            // for gap, the negative value needs to be taken
            dp.Amount = -1 * dp.Amount;
        }
        dp.Type = type;
        if(theMap.containsKey(type)){
            // if exact type already exists, only add to the amount
            theMap.get(type).amount = theMap.get(type).amount + dp.amount;
        } else {
            theMap.put(type,dp);
        }
        return theMap;

    }

    public class Datapoint {
        public Decimal Amount {get;set;}
        private Integer fy;
        private Integer fq;
        public String AccountManager {get;set;}
        public String Vertical {get;set;} // ownerrole? health / I&S / public
        public String Segment {get;set;} // department op opp
        public String ProductFamily {get;set;}
        //public String teamRoles {get;set;}
        public String Period{
            get{
                return 'Q'+fq+' FY'+fy;
            }
            set;
        }

        public String Type {get;set;}
        //public String sType {get;set;}

    }


    public List<SelectOption> getFiscalYearOptions(){
        List<SelectOption> fiscalYears = new List<SelectOption>();
        String previousFiscalYear = String.valueOf(Integer.valueOf(currentFiscalYear)-1);
        fiscalYears.add(New SelectOption(previousFiscalYear,previousFiscalYear));
        fiscalYears.add(New SelectOption(currentFiscalYear,currentFiscalYear));
        String nextFiscalYear = String.valueOf(Integer.valueOf(currentFiscalYear)+1);
        fiscalYears.add(New SelectOption(nextFiscalYear,nextFiscalYear));

        return fiscalYears;
    }

    /*
     *  Description: this method fetches the current user's role and it's sibling roles to enable switching
     */
    public List<SelectOption> getRunAsUserOptions(){
        List<SelectOption> runAsUsers = new List<SelectOption>();
        for(Id roleId : visibleHeadsOfRoleIds){
            system.debug(roleId);
            runAsUsers.add(New SelectOption(roleId,GeneralUtils.RoleIdToRoleName.get(roleId)));
        }
        return runAsUsers;
    }


    public List<SelectOption> getDataTypeOptions(){
        List<SelectOption> dataTypes = new List<SelectOption>();

        dataTypes.add(New SelectOption('MRR','MRR'));
        dataTypes.add(New SelectOption('TCV','TCV'));
        dataTypes.add(New SelectOption('CTN','CTN'));
        dataTypes.add(New SelectOption('License','License'));

        return dataTypes;
    }

    public String fiscalQuarterSelected {get;set;}

    public String verticalSelected{get;set;}
    public List<SelectOption> getVerticalOptions() {
        List<SelectOption> returnList = new List<SelectOption>();
        returnList.add(new SelectOption('all','-All-'));

        Set<String> verticals = new Set<String>();

        for(String role : visibleRoleNames){
            if(viewAsCEO){
                verticals.add(GeneralUtils.roleToMainSegment.get(role)==null?'Other':GeneralUtils.roleToMainSegment.get(role));
            } else {
                verticals.add(GeneralUtils.roleToSegment.get(role)==null?'Other':GeneralUtils.roleToSegment.get(role));
            }
        }

        for(String s : verticals){
            returnList.add(new SelectOption(s,s));
        }
        return returnList;

    }


    public String getRunasUserLabel(){
        system.debug(runAsUserRoleIdSelected);
        UserRole[] usrRoles = [Select Name From UserRole Where Id = :runAsUserRoleIdSelected];
        if(!usrRoles.isEmpty()){
            return (usrRoles[0].Name).replace(' Sales Manager','').replace('Head of ','').replace(' Sales','').replace('Director ','');
        } else {
            return 'Main';
        }
    }

    public String dashboardTypeSelected{get;set;}
    public List<SelectOption> getDashboardTypeOptions(){
        return OpportunityDashboardUtils.getDashboardTypeOptions();
    }
    public pageReference loadDashboard(){
        return OpportunityDashboardUtils.dashboardVFPage(dashboardTypeSelected);
    }

}