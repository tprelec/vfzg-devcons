global class BiccVodacomDataBanBatch implements Database.Batchable <sObject>, Database.Stateful{

	global map<String,String> BiccBanFieldMapping = SyncUtil.fullMapping('BICC_Vodacom_Data_BAN__c -> Account_Revenue__c').get('BICC_Vodacom_Data_BAN__c -> Account_Revenue__c');
	global final Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
	global final Schema.SObjectType arSchema = schemaMap.get('Account_Revenue__c');
	global final Schema.SObjectType biccSchema = schemaMap.get('BICC_Vodacom_Data_BAN__c');
	global final map<String, Schema.SObjectField> fieldMap = arSchema.getDescribe().fields.getMap();
	global final map<String, Schema.SObjectField> biccFieldMap = biccSchema.getDescribe().fields.getMap();
	global final set<String> queriedFields = SyncUtil.getQueriedFields(BiccBanFieldMapping);
	global final list<String> sortList = SyncUtil.getSortedList(biccFieldMap);
	global String header = SyncUtil.getHeader(sortList, queriedFields);
	global String data = '';
	global String dataError = '';
	global Integer count = 0;
	global Boolean chain = false;

	global Database.QueryLocator start(Database.BatchableContext BC){
		
		//Create a dynamic query with info from the Field Mapping Object
		String Query = 'SELECT ';
		for (String BiccField : queriedFields) {
			Query += BiccField + ',';
		}
		Query = Query.subString(0, Query.length() - 1);
		Query += ' FROM BICC_Vodacom_Data_BAN__c LIMIT 20000';

		return Database.getQueryLocator(Query);
	}

	global void execute(Database.BatchableContext BC, List<BICC_Vodacom_Data_BAN__c> scope){

		if(Job_Management__c.getOrgDefaults().Cancel_Batch__c == true){
			System.abortJob(BC.getJobId());
		}

		map<String, BICC_Vodacom_Data_BAN__c> scopeMap = new map<String, BICC_Vodacom_Data_BAN__c>();
		list<BICC_Vodacom_Data_BAN__c> deleteList = new list<BICC_Vodacom_Data_BAN__c>();
		map<String, Account_Revenue__c> accountRevenueMap = new map<String, Account_Revenue__c>();
		List<BICC_Vodacom_Data_BAN__c> biccErrorList = new List<BICC_Vodacom_Data_BAN__c>();
		map<String, Ban__c> banToAccount = new map<String, Ban__c>();
		map<String, list<Id>> banToAR = new map<String, list<Id>>();

		count += scope.size();

		//Put scope in a map for deletion
		for(BICC_Vodacom_Data_BAN__c bicc : scope){
			//Put data in csv
			for(String field : sortList){
				if(queriedFields.contains(field)){
					data += '"' + bicc.get(field) + '",';
				}
			}
			data = data.subString(0, data.length() - 1) + '\n';

			Boolean match = false;
			if(bicc.Billing_Account_Num__c != null){
				match = StringUtils.checkBan(bicc.Billing_Account_Num__c);
				//match = Pattern.matches('^3[0-9]{8}', bicc.Billing_Account_Num__c);
			}
			if(!match){
				bicc.Error__c = 'The BAN number is not valid';
				biccErrorList.add(bicc);
			} else if(bicc.Year_Month_Num__c == null){
				bicc.Error__c = 'The field Year_Month_Num__c is needed for identification to delete BICC data';
				biccErrorList.add(bicc);
			} else if(scopeMap.containsKey(bicc.Billing_Account_Num__c + bicc.Year_Month_Num__c)){
				bicc.Error__c = 'A duplicate for the Ban and Year_Month_Num__c exists in the data';
				biccErrorList.add(bicc);
			} else {
				scopeMap.put(bicc.Billing_Account_Num__c + bicc.Year_Month_Num__c, bicc);
				banToAccount.put(bicc.Billing_Account_Num__c, null);
			}
		}

		//Make mapping between Ban and Account
		for(Ban__c ban : [select Id, Name, Account__c from Ban__c where Name in: banToAccount.keySet()]){
			banToAccount.put(ban.Name, ban);
		}

		//Search for Existing Account revenues
		for(Account_Revenue__c ar : [select Id, Ban_and_YMN__c from Account_Revenue__c where Ban_and_YMN__c in: scopeMap.keySet()]){
			if(banToAR.get(ar.Ban_and_YMN__c) == null){
				banToAR.put(ar.Ban_and_YMN__c, new list<Id>());
			}
			banToAR.get(ar.Ban_and_YMN__c).add(ar.Id);
		}

		//Create account revenues from BICC
		for (BICC_Vodacom_Data_BAN__c biccBan : scopeMap.values()) {
			Account_Revenue__c ar = new Account_Revenue__c();
			for (String accountField : BiccBanFieldMapping.keySet()) {
				String biccBanField = BiccBanFieldMapping.get(accountField);
				Object biccBanValue;
				if(fieldMap.get(accountField).getDescribe().getType().name() == 'CURRENCY' && biccBan.get(biccBanField) != null){
					biccBanValue = decimal.valueOf(String.valueOf(biccBan.get(biccBanField)));
				} else if(fieldMap.get(accountField).getDescribe().getType().name() == 'DOUBLE' && biccBan.get(biccBanField) != null){
					biccBanValue = decimal.valueOf(String.valueOf(biccBan.get(biccBanField)));
				} else {
					biccBanValue = biccBan.get(biccBanField);
				}
				ar.put(accountField,biccBanValue);
			}
			if(banToAccount.get(biccBan.Billing_Account_Num__c) != null){
				if(banToAR.get(ar.AR_Billing_Account_Num__c + ar.AR_Year_Month_Num__c) != null){
					if(banToAR.get(ar.AR_Billing_Account_Num__c + ar.AR_Year_Month_Num__c).size() == 1){
						ar.Id = banToAR.get(ar.AR_Billing_Account_Num__c + ar.AR_Year_Month_Num__c)[0];
						ar.Ban__c = banToAccount.get(biccBan.Billing_Account_Num__c).Id;
						ar.Account__c = banToAccount.get(biccBan.Billing_Account_Num__c).Account__c;
						accountRevenueMap.put(ar.AR_Billing_Account_Num__c + ar.AR_Year_Month_Num__c, ar);
					} else {
						biccBan.Error__c = 'There is more than one match on existing Account Revenues';
						biccErrorList.add(biccBan);
					}
				} else {
					ar.Ban__c = banToAccount.get(biccBan.Billing_Account_Num__c).Id;
					ar.Account__c = banToAccount.get(biccBan.Billing_Account_Num__c).Account__c;
					accountRevenueMap.put(ar.AR_Billing_Account_Num__c + ar.AR_Year_Month_Num__c, ar);
				}
			} else {
				biccBan.Error__c = 'There is no matching Account';
				biccErrorList.add(biccBan);
			}
		}

		//Upsert account revenues in the database
		list<Database.UpsertResult> SR = Database.upsert(accountRevenueMap.values(), Account_Revenue__c.Fields.Id, false);
		for (Integer i = 0; i < SR.size(); i++) {
			if(SR[i].isSuccess() ){
				deleteList.add(scopeMap.get(accountRevenueMap.values()[i].AR_Billing_Account_Num__c + accountRevenueMap.values()[i].AR_Year_Month_Num__c));
			} else {
				scopeMap.get(accountRevenueMap.values()[i].AR_Billing_Account_Num__c + accountRevenueMap.values()[i].AR_Year_Month_Num__c).Error__c = 'Error while creating Account Revenue';
				biccErrorList.add(scopeMap.get(accountRevenueMap.values()[i].AR_Billing_Account_Num__c + accountRevenueMap.values()[i].AR_Year_Month_Num__c));
			}
		}
		
		for(BICC_Vodacom_Data_BAN__c error : biccErrorList){
			for(String field : sortList){
				if(queriedFields.contains(field)){
					dataError += '"' + error.get(field) + '",';
				}
			}
			dataError += '"' + error.Error__c + '"\n';
		}

		//Delete succesful matches and errors from BICC
		delete deleteList;
		delete biccErrorList;
	}

	global void finish(Database.BatchableContext BC){

		//Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT
							Status,
							NumberOfErrors,
							TotalJobItems
						  FROM
							AsyncApexJob
						  WHERE
							Id =: BC.getJobId()];

		// Send an email to InsideSalesSystems.nl@vodafone.com notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		
		Blob b = blob.valueOf(header + data);
		Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
		efa.setFileName('BiccVodacomDataBanBatch.csv');
		efa.setBody(b);

		header = header.subString(0, header.length() - 1) + ',"Error__c"\n';
		Blob b2 = blob.valueOf(header + dataError);
		Messaging.EmailFileAttachment efa2 = new Messaging.EmailFileAttachment();
		efa2.setFileName('BiccVodacomDataBanBatchErrors.csv');
		efa2.setBody(b2);

		String[] toAddresses = new String[] {'EBUSalesForce.nl@vodafone.com'};
		//String[] toAddresses = new String[] {'ferdinandb@nncourage.com'};
		mail.setToAddresses(toAddresses);
		mail.setSubject('Bicc Vodacom Data Ban database import ' + a.Status);
		mail.setPlainTextBody('The Bicc Vodacom Data Ban database import job processed ' + a.TotalJobItems +
							  ' batches with '+ a.NumberOfErrors + ' failures.');
		mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa, efa2});
		Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

		if(count == 20000){
			Database.executeBatch(new BiccVodacomDataBanBatch(), 100);
		} else if(chain == true){
			BiccBanCustomerValueBatch controller = new BiccBanCustomerValueBatch();
			controller.chain = true;
			database.executebatch(controller);
		}
	}
}