@isTest
public with sharing class CST_TransformToCollectionVariable_UT {
	@TestSetup
	static void makeData() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
	}
	@isTest
	public static void test2sites() {
		Account acc = [SELECT id FROM Account LIMIT 1];

		// 18:15:34.38 (52805972)|USER_DEBUG|[7]|DEBUG|values -> (a065r000002w3HXAAY)
		List<Site__c> sites = new List<Site__c>();

		Site__c site1 = new Site__c(
			name = 'Nice St',
			Active__c = true,
			Site_Account__c = acc.Id,
			Site_Street__c = 'test',
			Site_City__c = 'testCity',
			Site_Postal_Code__c = '1234AB',
			Site_House_Number__c = system.now().millisecond(),
			Site_House_Number_Suffix__c = 'a'
		);

		sites.add(site1);

		Site__c site2 = new Site__c(
			name = 'Bad St',
			Active__c = true,
			Site_Account__c = acc.Id,
			Site_Street__c = 'St 2',
			Site_City__c = 'testCity 2',
			Site_Postal_Code__c = '1234AB',
			Site_House_Number__c = system.now().millisecond(),
			Site_House_Number_Suffix__c = 'a'
		);

		sites.add(site2);

		insert sites;
		Test.StartTest();
		CST_TransformToCollectionVariable.CheckValues(new List<String>{ '(abc)' });
		Test.StopTest();
	}

	@isTest
	public static void test1site() {
		Account acc = [SELECT id FROM Account LIMIT 1];

		// 18:15:34.38 (52805972)|USER_DEBUG|[7]|DEBUG|values -> (a065r000002w3HXAAY)
		List<Site__c> sites = new List<Site__c>();

		Site__c site1 = new Site__c(
			name = 'Nice St',
			Active__c = true,
			Site_Account__c = acc.Id,
			Site_Street__c = 'test',
			Site_City__c = 'testCity',
			Site_Postal_Code__c = '1234AB',
			Site_House_Number__c = system.now().millisecond(),
			Site_House_Number_Suffix__c = 'a'
		);

		sites.add(site1);

		insert sites;
		Test.StartTest();
		CST_TransformToCollectionVariable.CheckValues(new List<String>{ '(abc)' });
		Test.StopTest();
	}

	@isTest
	public static void testNullsite() {
		Test.StartTest();
		CST_TransformToCollectionVariable.CheckValues(new List<String>());
		Test.StopTest();
	}
}
