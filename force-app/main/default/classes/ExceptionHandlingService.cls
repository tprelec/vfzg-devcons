public inherited sharing class ExceptionHandlingService {

    public void handleBatchErrors(List<BatchApexErrorEvent> batchErrorList) {
		Map<Id, AsyncApexJob> asyncJobsMap = getAsyncJobs(batchErrorList);

		List<BatchErrorInformation> batchErrorInformationList = new List<BatchErrorInformation>();
		for (BatchApexErrorEvent apexErrorEvent : batchErrorList) {
			AsyncApexJob asyncJob = asyncJobsMap.containsKey(apexErrorEvent.AsyncApexJobId) ? asyncJobsMap.get(apexErrorEvent.AsyncApexJobId) : new AsyncApexJob();
			batchErrorInformationList.add(new BatchErrorInformation(apexErrorEvent, asyncJob));
		}

		LoggerService.log(batchErrorInformationList);
	}

	private Map<Id, AsyncApexJob> getAsyncJobs(List<BatchApexErrorEvent> batchErrorList) {
		Set<Id> asyncApexJobIds = new Set<Id>();
		for (BatchApexErrorEvent apexErrorEvent : batchErrorList) {
			asyncApexJobIds.add(apexErrorEvent.AsyncApexJobId);
		}
		return new Map<Id, AsyncApexJob>(
			[
				SELECT Id, CreatedById, CreatedDate, ExtendedStatus, JobItemsProcessed, JobType, MethodName, NumberOfErrors, Status, TotalJobItems, ApexClass.Name
				FROM AsyncApexJob
				WHERE Id IN :asyncApexJobIds
			]
		);
	}

	public class BatchErrorInformation {
		public BatchApexErrorEvent apexErrorEvent;
		public AsyncApexJob apexJobInformation;

		private BatchErrorInformation(BatchApexErrorEvent apexErrorEvent, AsyncApexJob apexJobInformation) {
			this.apexErrorEvent = apexErrorEvent;
			this.apexJobInformation = apexJobInformation;
		}
	}
}