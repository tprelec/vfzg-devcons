@isTest
@SuppressWarnings('PMD')
public with sharing class TestSiteExport {
	static testMethod void siteExport() {
		Sales_Settings__c ss = new Sales_Settings__c(
			Max_weekly_postalcode_checks__c = 0,
			Max_Daily_Postalcode_Checks__c = 1,
			Postalcode_check_validity_days__c = 3
		);
		insert ss;

		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'AFD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		Ban__c ban = TestUtils.createBan(acct);
		ban.BOP_export_datetime__c = System.now();
		update ban;
		TestUtils.autoCommit = false;
		Site__c s1 = TestUtils.createSite(acct);
		s1.Last_dsl_check__c = System.today().addDays(-1);
		insert s1;
		TestUtils.autoCommit = true;

		try {
			Site__c s2 = TestUtils.createSite(acct);
		} catch (Exception e) {
			System.debug(e.getMessage());
			System.assertEquals(
				'Weekly maximum (0) reached. Please request manually.',
				e.getMessage()
			);
		}

		Test.startTest();
		ss.Max_weekly_postalcode_checks__c = 2;
		update ss;

		try {
			s1.Site_Postal_Code__c = '1234CC';
			s1.Canvas_Country__c = null;
			update s1;
		} catch (Exception e) {
		}

		ban.BOPCode__c = 'ABC';
		update ban;
		Set<Id> banIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();
		Set<Id> siteIds = new Set<Id>();
		banIds.add(ban.Id);
		accountIds.add(acct.Id);
		siteIds.add(s1.Id);
		Site__c s = TestUtils.createSite(acct);
		SiteExport.sitesInExport.clear();
		s.BOP_export_datetime__c = System.now();
		s.Site_Phone__c = '321321321';
		update s;
		SiteExport.scheduleSiteExportFromBanIds(banIds);
		SiteExport.scheduleSiteExportFromAccountIds(accountIds);
		SiteExport.scheduleSiteExportFromSiteIds(siteIds);
		String exportType = 'create';
		List<Site__c> siteList = SiteExport.exportSites(siteIds, exportType);

		Test.stopTest();
	}

	static testMethod void siteExport1() {
		Sales_Settings__c ss = new Sales_Settings__c(
			Max_weekly_postalcode_checks__c = 0,
			Max_Daily_Postalcode_Checks__c = 1,
			Postalcode_check_validity_days__c = 3
		);
		insert ss;

		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'AFD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		Ban__c ban = TestUtils.createBan(acct);
		ban.BOP_export_datetime__c = System.now();
		update ban;
		TestUtils.autoCommit = false;
		Site__c s1 = TestUtils.createSite(acct);
		s1.Last_dsl_check__c = System.today().addDays(-1);
		insert s1;
		TestUtils.autoCommit = true;

		try {
			Site__c s2 = TestUtils.createSite(acct);
		} catch (Exception e) {
			System.debug(e.getMessage());
			System.assertEquals(
				'Weekly maximum (0) reached. Please request manually.',
				e.getMessage()
			);
		}

		Test.startTest();
		ss.Max_weekly_postalcode_checks__c = 2;
		update ss;

		try {
			s1.Site_Postal_Code__c = '1234CC';
			s1.Canvas_Country__c = null;
			update s1;
		} catch (Exception e) {
		}

		ban.BOPCode__c = 'ABC';
		update ban;
		Set<Id> banIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();
		Set<Id> siteIds = new Set<Id>();
		banIds.add(ban.Id);
		accountIds.add(acct.Id);
		siteIds.add(s1.Id);
		Site__c s = TestUtils.createSite(acct);
		SiteExport.sitesInExport.clear();
		s.BOP_export_datetime__c = System.now();
		s.Site_Phone__c = '321321321';
		update s;
		try {
			SiteExport.scheduleSiteExportFromBanIds(null);
			SiteExport.scheduleSiteExportFromAccountIds(null);
			SiteExport.scheduleSiteExportFromSiteIds(null);
		} catch (Exception e) {
			System.debug(e.getMessage());
			//System.assertEquals('Weekly maximum (0) reached. Please request manually.', e.getMessage());
		}

		Test.stopTest();
	}
}