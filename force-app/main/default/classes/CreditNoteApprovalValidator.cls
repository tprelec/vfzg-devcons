/**
 * @description:    Credit note approval validator class
 * @testClass:      TestCreditNoteApprovalValidator
 **/
public inherited sharing class CreditNoteApprovalValidator {
	private static final List<String> MATRIX_RULES_THAT_ARE_USING_NEXT_APPROVER_FIELDS = new List<String>{
		Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER,
		Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_CHANNEL_MANAGER_AND_SOLUTION_SPECIALIST,
		Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_OPERATIONAL_MANAGER,
		Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_OWNER_MANAGER,
		Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_SALES_MANAGER
	};

	@TestVisible
	private static final String NEXT_APPROVER_MANUAL_FIELD_NAME = Credit_Note__c.Next_Approver_Manual__c.getDescribe().getName();
	@TestVisible
	private static final String NEXT_QUEUE_APPROVER_FIELD_NAME = Credit_Note__c.Next_Queue_Approver__c.getDescribe().getName();

	@TestVisible
	private static final String ERROR_MESSAGE_YOU_NEED_TO_POPULATE_APPROVER_FIELD = '{0} field needs to be populated on next approval step';
	@TestVisible
	private static final String ERROR_MESSAGE_YOU_NEED_TO_POPULATE_ANY_NEXT_APPROVER_FIELD = 'At least 1 Next_Approver_n__c field needs to be populated on next approval step';

	private Credit_Note__c creditNote;

	/**
	 * @description:    Constructor
	 * @param           creditNote - credit note record
	 */
	public CreditNoteApprovalValidator(Credit_Note__c creditNote) {
		this.creditNote = creditNote;
	}

	/**
	 * @description:    Validates credit note for approval and adds error message
	 *                  if it is not valid
	 * @return          Boolean - true if credit note is valid for approval
	 *                  , false else
	 */
	public Boolean validate() {
		String errorMessage;
		Boolean isValid = true;

		if (this.isNextApproverManualEmptyOnManualAssignmentRule()) {
			errorMessage = String.format(ERROR_MESSAGE_YOU_NEED_TO_POPULATE_APPROVER_FIELD, new List<String>{ NEXT_APPROVER_MANUAL_FIELD_NAME });
		} else if (this.areNextApproverFieldsEmptyForRulesThatAreUsingIt()) {
			errorMessage = ERROR_MESSAGE_YOU_NEED_TO_POPULATE_ANY_NEXT_APPROVER_FIELD;
		} else if (this.isNextQueueApproverEmptyOnQueueAssignmentRule()) {
			errorMessage = String.format(ERROR_MESSAGE_YOU_NEED_TO_POPULATE_APPROVER_FIELD, new List<String>{ NEXT_QUEUE_APPROVER_FIELD_NAME });
		}

		if (String.isNotBlank(errorMessage)) {
			creditNote.addError(errorMessage);
			isValid = false;
		}

		return isValid;
	}

	/**
	 * @description:    Checks if Next_Approver_Manual__c is empty
	 *                  on manual assignment rule
	 * @return          Boolean - true if Next_Approver_Manual__c is empty
	 *                  on manual assignment rule, false else
	 */
	private Boolean isNextApproverManualEmptyOnManualAssignmentRule() {
		return this.creditNote.Next_Approval_Assignment_Rules__c.contains(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_MANUAL) &&
			this.creditNote.Next_Approver_Manual__c == null;
	}

	/**
	 * @description:    Checks if all Next_Approver_n__c are empty on rules that
	 *                  are using it
	 * @return          Boolean - true if all Next_Approver_n__c are empty on rules that
	 *                  are using it, false else
	 */
	private Boolean areNextApproverFieldsEmptyForRulesThatAreUsingIt() {
		return this.nextApprovalAssignmentRulesContainAnyMatrixRuleThatUseNextApprover() && this.areAllNonManualNextApproverFieldsEmpty();
	}

	/**
	 * @description:    Checks if Next_Queue_Approver__c is empty on
	 *                  queue assignment rule
	 * @return          Boolean - true if Next_Queue_Approver__c is empty
	 *                  on queue assignment rule, false else
	 */
	private Boolean isNextQueueApproverEmptyOnQueueAssignmentRule() {
		return this.creditNote.Next_Approval_Assignment_Rules__c.contains(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE) &&
			this.creditNote.Next_Queue_Approver__c == null;
	}

	/**
	 * @description:    Checks if all non manual next approver fields are empty
	 * @return          Boolean - true if all non manual next approver fields are empty
	 *                  , false else
	 */
	private Boolean areAllNonManualNextApproverFieldsEmpty() {
		return this.creditNote.Next_Approver_1__c == null && this.creditNote.Next_Approver_2__c == null && this.creditNote.Next_Approver_3__c == null;
	}

	/**
	 * @description:    Checks if next approval assignment rules contain any matrix rule that use next approver
	 * @return          Boolean - true if next approval assignment rules contain any matrix rule that use next approver
	 *                  , false else
	 */
	private Boolean nextApprovalAssignmentRulesContainAnyMatrixRuleThatUseNextApprover() {
		Boolean doesContain = false;

		for (String matrixRuleThatIsUsingNextApproverFields : MATRIX_RULES_THAT_ARE_USING_NEXT_APPROVER_FIELDS) {
			if (this.creditNote.Next_Approval_Assignment_Rules__c.contains(matrixRuleThatIsUsingNextApproverFields)) {
				doesContain = true;
				break;
			}
		}

		return doesContain;
	}
}
