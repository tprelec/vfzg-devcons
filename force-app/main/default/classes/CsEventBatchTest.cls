@isTest
public class CsEventBatchTest {
    @testSetup static void setup(){
        Account acct = TestUtils.createPartnerAccount();
        acct.KVK_number__c = '12345678';
        update acct;

        Lead theLead = TestUtils.createLead();
        theLead.KVK_number__c = '87654321';
        update theLead;

        List<DWH_Events_SF_Interface__c> eventList = new List<DWH_Events_SF_Interface__c>();
        DWH_Events_SF_Interface__c eventLead = new DWH_Events_SF_Interface__c();
        eventLead.KVKNummer__c = '87654321';
        eventList.add(eventLead);
        DWH_Events_SF_Interface__c eventAcc = new DWH_Events_SF_Interface__c();
        eventAcc.KVKNummer__c = '12345678';
        eventList.add(eventAcc);
        insert eventList;
    }

    @isTest static void checkSchedule(){
        Test.startTest();
        SchedulableContext sc = null;
        CsEventBatch tsc = new CsEventBatch();
        tsc.execute(sc);
        Test.stopTest();

    }

    @isTest static void checkCsEventRun() {
        Test.startTest();
        CsEventBatch x = new CsEventBatch();
        database.executeBatch(x);
        Test.stopTest();

        List<DWH_Events_SF_Interface__c> eventList = [Select ID, Account__c, Lead__c, Processed__c, KVKNummer__c FROM DWH_Events_SF_Interface__c];
        for (DWH_Events_SF_Interface__c eventRec : eventList) {
            System.assertEquals(eventRec.Processed__c, true);
            if (eventRec.Account__c == null) {
                System.assertNotEquals(eventRec.Lead__c, null);
            }
            if (eventRec.Lead__c == null) {
                System.assertNotEquals(eventRec.Account__c, null);
            }
        }
    }
}