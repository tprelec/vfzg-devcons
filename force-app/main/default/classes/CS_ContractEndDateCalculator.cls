public with sharing class CS_ContractEndDateCalculator {
	private static Boolean calculatorEnabled = true;

	public static void enableCalculator() {
		calculatorEnabled = true;
	}

	public static void disableCalculator() {
		calculatorEnabled = false;
	}

	public static Boolean revalidateServiceContractEndDate(
		csord__Service__c targetService,
		csord__Service__c oldInstance,
		cscfga__Product_Configuration__c configuration,
		csord__Service__c replacedService
	) {
		if (!calculatorEnabled) {
			return false;
		}

		System.debug('targetService ' + JSON.serializePretty(targetService));
		System.debug('oldInstance ' + JSON.serializePretty(oldInstance));
		System.debug('configuration ' + JSON.serializePretty(configuration));
		System.debug('replacedService ' + JSON.serializePretty(replacedService));

		if (targetService.csordtelcoa__Product_Configuration__c == null) {
			System.debug(Logginglevel.DEBUG, 'csordtelcoa__Product_Configuration__c missing on service ' + targetService.Id);
			return false;
		}

		if (configuration.Contract_Number_Group__c != 'BI') {
			return false;
		}

		if (oldInstance == null) {
			// on insert
			if (targetService.csordtelcoa__Replaced_Service__c != null) {
				return setServiceEndDateMacd(targetService, replacedService);
			} else {
				return setServiceEndDateAcquisition(targetService, configuration);
			}
		} else if (
			oldInstance.csord__Activation_Date__c == null &&
			targetService.csord__Activation_Date__c != null &&
			targetService.csordtelcoa__Replaced_Service__c == null
		) {
			// on update
			return setServiceEndDateAcquisition(targetService, configuration);
		}

		return false;
	}

	public static Boolean setServiceEndDateAcquisition(csord__Service__c targetService, cscfga__Product_Configuration__c config) {
		System.debug('targetService.csord__Activation_Date__c ' + targetService.csord__Activation_Date__c);
		System.debug('targetService.Contract_End_Date__c ' + targetService.Contract_End_Date__c);
		if (targetService.csord__Activation_Date__c != null && targetService.Contract_End_Date__c == null) {
			targetService.Contract_End_Date__c = targetService.csord__Activation_Date__c.addMonths((Integer) config.cscfga__Contract_Term__c);
			return true;
		}
		return false;
	}

	public static Boolean setServiceEndDateMacd(csord__Service__c targetService, csord__Service__c replacedService) {
		System.debug('targetService.Contract_End_Date__c ' + targetService.Contract_End_Date__c);
		System.debug('replacedService.Contract_End_Date__c ' + replacedService.Contract_End_Date__c);
		// in case of MACD, replaced service should have its Contract_End_Date__c set
		if (targetService.Contract_End_Date__c == null) {
			targetService.Contract_End_Date__c = replacedService.Contract_End_Date__c;
			return true;
		}
		return false;
	}

	public static Boolean setSubscriptionEndDate(List<csord__Service__c> services, Map<Id, csord__Service__c> oldServices) {
		if (!calculatorEnabled) {
			return false;
		}

		Set<Id> subscriptionIds = new Set<Id>();

		for (csord__Service__c service : services) {
			if (oldServices == null) {
				subscriptionIds.add(service.csord__Subscription__c);
				continue;
			}
			csord__Service__c oldService = oldServices.get(service.Id);
			if (oldService == null) {
				subscriptionIds.add(service.csord__Subscription__c);
				continue;
			}
			if (service.Contract_End_Date__c != null && service.Contract_End_Date__c != oldService.Contract_End_Date__c) {
				subscriptionIds.add(service.csord__Subscription__c);
			}
		}

		List<csord__Service__c> allServices = [
			SELECT Id, Contract_End_Date__c, csord__Subscription__c
			FROM csord__Service__c
			WHERE csord__Subscription__c IN :subscriptionIds
		];

		Map<Id, List<csord__Service__c>> allServicesMapped = new Map<Id, List<csord__Service__c>>();

		for (csord__Service__c service : allServices) {
			if (allServicesMapped.containsKey(service.csord__Subscription__c)) {
				allServicesMapped.get(service.csord__Subscription__c).add(service);
			} else {
				List<csord__Service__c> serviceList = new List<csord__Service__c>{ service };
				allServicesMapped.put(service.csord__Subscription__c, serviceList);
			}
		}

		List<csord__Subscription__c> subscriptions = [
			SELECT Id, Contract_End_Date__c, csordtelcoa__Product_Configuration__c, csordtelcoa__Product_Configuration__r.Contract_Number_Group__c
			FROM csord__Subscription__c
			WHERE Id IN :subscriptionIds
		];

		List<csord__Subscription__c> subscriptionsToUpdate = new List<csord__Subscription__c>();
		for (csord__Subscription__c subscription : subscriptions) {
			if (
				subscription.Contract_End_Date__c == null &&
				subscription.csordtelcoa__Product_Configuration__c != null &&
				subscription.csordtelcoa__Product_Configuration__r.Contract_Number_Group__c == 'BI'
			) {
				Date minServiceDate = minDate(allServicesMapped.get(subscription.Id));
				if (minServiceDate != null) {
					subscription.Contract_End_Date__c = minServiceDate;
					subscriptionsToUpdate.add(subscription);
				}
			}
		}
		if (subscriptionsToUpdate.size() > 0) {
			update subscriptionsToUpdate;
			return true;
		}

		return false;
	}

	@TestVisible
	private static Date minDate(List<csord__Service__c> services) {
		if (services == null) {
			return null;
		}

		Date minDate = Date.newInstance(10000, 1, 1);

		for (csord__Service__c service : services) {
			if (service.Contract_End_Date__c != null && service.Contract_End_Date__c < minDate) {
				minDate = service.Contract_End_Date__c;
			}
		}

		return minDate == Date.newInstance(10000, 1, 1) ? null : minDate;
	}
}
