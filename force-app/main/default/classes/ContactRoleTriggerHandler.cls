/**
 * @description			This is the trigger handler for the ContactRole
 */
public with sharing class ContactRoleTriggerHandler extends TriggerHandler {

	/**
	 * @description			This handles the before insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeInsert(){
		
	}

    
	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterInsert(){
		List<Contact_Role__c> newContactRoles = (List<Contact_Role__c>) this.newList;	
		Map<Id,Contact_Role__c> newContactRolesMap = (Map<Id,Contact_Role__c>) this.newMap;

		
		deactivatePreviousActiveContactRole(newContactRolesMap, null);
		triggerContactRoleExport(newContactRoles, null);		
	}

	/**
	 * @description			This handles the after insert trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void BeforeUpdate(){
	}
	

	/**
	 * @description			This handles the after update trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */
	public override void AfterUpdate(){
		List<Contact_Role__c> newContactRoles = (List<Contact_Role__c>) this.newList;	
		List<Contact_Role__c> oldContactRoles = (List<Contact_Role__c>) this.oldList;
		Map<Id,Contact_Role__c> newContactRolesMap = (Map<Id,Contact_Role__c>) this.newMap;
		Map<Id,Contact_Role__c> oldContactRolesMap = (Map<Id,Contact_Role__c>) this.oldMap;
		
		deactivatePreviousActiveContactRole(newContactRolesMap, oldContactRolesMap);
		triggerContactRoleExport(newContactRoles, oldContactRolesMap);
	}	

	/**
	 * @description			This handles the before delete trigger event.
	 * @param	newObjects	List of new sObjects that are being created.
	 */	
    public override void BeforeDelete(){
		Map<Id,Contact_Role__c> oldContactRolesMap = (Map<Id,Contact_Role__c>) this.oldMap;
    	
		//triggerContactRoleExport(null, oldContactRolesMap); // not relevant. Delete export happens when contactrole is deactivated
		blockDeleteOfActiveRoles(oldContactRolesMap);
    }   	


	/**
	 * @description			This makes sure that currently active roles get deactivated if a new role of the same type is created
	 */	
	private void deactivatePreviousActiveContactRole(Map<Id,Contact_Role__c> newContactRoles, Map<Id,Contact_Role__c> oldContactRolesMap){
		// collect any new active or activated roles
		Map<Id,Set<String>> accountRolesToDeactivate = new Map<Id,Set<String>>();
		for(Contact_Role__c cr : newContactRoles.values()){
			if((Trigger.isInsert && cr.Active__c)
				|| (Trigger.isUpdate && cr.Active__c && !oldContactRolesMap.get(cr.Id).Active__c) ){

				// check for overlaps within the trigger
				if(accountRolesToDeactivate.containsKey(cr.Account__c)){
					if(accountRolesToDeactivate.get(cr.Account__c).contains(cr.Type__c)){
						cr.addError('Found overlapping active account role for account '+cr.Account__c+' and role \''+cr.Type__c+'\' within this insert/update batch.');
					} else {
						accountRolesToDeactivate.get(cr.Account__c).add(cr.Type__c);
					}
				} else {
					accountRolesToDeactivate.put(cr.Account__c,new Set<String>{cr.Type__c});
				}
				
			}
		}
		if(!accountRolesToDeactivate.isEmpty()){
			List<Contact_Role__c> crToDeactivate = new List<Contact_Role__c>();
			for(Contact_Role__c cr : [Select Id, Account__c, Type__c From Contact_Role__c where Active__c = true AND Account__c in :accountRolesToDeactivate.keySet() AND Id NOT IN :newContactRoles.keySet()]){
				if(accountRolesToDeactivate.get(cr.Account__c).contains(cr.Type__c)){
					cr.Active__c = false;
					crToDeactivate.add(cr);
				}
			}
			SharingUtils.updateRecordsWithoutSharing(crToDeactivate);
		}

	}


	/**
	 * @description			This triggers the export to BOP of ContactRoles.
	 */	
	private void blockDeleteOfActiveRoles(Map<Id,Contact_Role__c> oldContactRolesMap){
		for(Contact_Role__c cr : oldContactRolesMap.values()){
			if(cr.Active__c) cr.addError('Cannot delete active contactroles. Please deactivate contactrole first (so it is removed from BOP) and try again.');
		}

	}

	/**
	 * @description			This triggers the export to BOP of ContactRoles.
	 */	
	private void triggerContactRoleExport(List<Contact_Role__c> newContactRoles, Map<Id,Contact_Role__c> oldContactRolesMap){
		Set<Id> newContactRoleIds = new Set<Id>();
		Set<Id> deleteContactRoleIds = new Set<Id>();
		Set<Id> contactIds = new Set<Id>();
		
		if(oldContactRolesMap == null){
			// insert
			for(Contact_Role__c c : newContactRoles){
				newContactRoleIds.add(c.Id);	
				contactIds.add(c.Contact__c);
			}
		} else {
			// update
			for(Contact_Role__c c : newContactRoles){
				// deactivation (= delete from BOP)
				if(c.Active__c == false && oldContactRolesMap.get(c.Id).Active__c == true){
					deleteContactRoleIds.add(c.Id);	
					contactIds.add(c.Contact__c);
				} else if(c.Active__c == true && oldContactRolesMap.get(c.Id).Active__c != true){
				// activation (= create in BOP)
					newContactRoleIds.add(c.Id);	
					contactIds.add(c.Contact__c);
				}				
			}
		}
		
		Set<Id> contactRoleIdsForExport = new Set<Id>();
		if(!newContactRoleIds.isEmpty() || !deleteContactRoleIds.isEmpty()){
			// check if Contact_Role__c's account or one of the BANs has BOPCode (otherwise do not export)
			Set<Id> syncedContacts = new Set<Id>();
			for(Contact c : [Select BOP_Export_Datetime__c, Id From Contact Where Id in :contactIds AND BOP_Export_Datetime__c != null]){
				syncedContacts.add(c.Id);
			}
			for(Contact_Role__c c : [Select 
								Id
							From 
								Contact_Role__c 
							Where 
								(Id in :deleteContactRoleIds OR Id in :newContactRoleIds) 
								AND
								Contact__c in :syncedContacts
								AND
								Type__c != 'chooser']
			){
				contactRoleIdsForExport.add(c.Id);
			}

		}
		
		if(!contactRoleIdsForExport.isEmpty()){
			newContactRoleIds.retainAll(contactRoleIdsForExport);
			if(!newContactRoleIds.isEmpty()){
				newContactRoleIds.removeAll(ContactExport.ContactRolesInExport);
				ContactExport.ContactRolesInExport.addAll(newContactRoleIds);
				if(!newContactRoleIds.isEmpty()){
					if(system.isFuture() || System.isBatch()){
						ContactExport.scheduleContactRoleExportFromContactIds(newContactRoleIds);
					} else {
						ContactExport.exportNewContactRolesOffline(newContactRoleIds);
					}
				}
			}
			deleteContactRoleIds.retainAll(contactRoleIdsForExport);
			if(!deleteContactRoleIds.isEmpty()){
				deleteContactRoleIds.removeAll(ContactExport.ContactRolesInExport);
				ContactExport.ContactRolesInExport.addAll(deleteContactRoleIds);
				if(!deleteContactRoleIds.isEmpty()){		
					if(system.isFuture() || System.isBatch()){
						ContactExport.scheduleContactRoleExportFromContactIds(deleteContactRoleIds);
					} else {
						ContactExport.exportDeletedContactRolesOffline(deleteContactRoleIds);
					}
				}
			}
		}
	}

}