public class OrderEntryWebCartJSON {
	public class Cart {
		public CartCustomer customer;
		public CartStatus ilt;
		public CartSimStatus delivery;
		public CartStatus fixedNubmerPorting;
		public CartStatus mobileNumberPorting;
		public CartStatus fixedPrivacy;
		public CartStatus mobilePrivacy;
		public CartStatus paymentInfo;
		public CartStatus transferService;
		public CartStatus paymentOption;
		public CartStatus termsAgreed;
		public String cartId;
		public Boolean isBusiness;
		public Boolean chosenOrderTypeIdBusiness;
		public Boolean hasHardwareImpact;
		public List<CartItem> shoppingCartItems;
		public OrderEntryWebJSON.Price ziggoTotal;
		public OrderEntryWebJSON.Price vodafoneTotal;
		public String errors;
	}

	public class CartItem {
		public String id;
		public String parentId;
		public Boolean isMandatory;
		public String action;
		public String priority;
		public Integer quantity;
		public CartProduct product;
		public Date wishDate;
		public Date contractStartDate;
		public Date contractEndDate;
		public CartNumberPorting numberPorting; // TDB
		public String msisdn;
		public String produtConditions;
		public List<CartProductPromotion> promotions; // TBD
		// TBD provjeri koje podatke trebamo
	}

	public class CartProduct {
		public String sku;
		public String hawaiiId;
		public String name;
		public String descriptionShort;
		public String descriptionLong;
		public List<String> usps; // TBD
		public CartProductPrice price;
		public String productType;
		public String addOnType;
		public String salesStatus;
		public String loan;
		public List<CartAttribute> attributes;
	}

	public class CartProductPrice {
		public OrderEntryWebJSON.Charge oneTimeCharge;
		public OrderEntryWebJSON.Charge immediateOneTimeCharge;
		public OrderEntryWebJSON.Charge recurringCharge;
	}

	public class CartNumberPorting {
		public List<String> fixedNumberPorting; // TBD
		public String mobileNumberPorting;
		public Boolean eligibleFormMobileNumberPort;
	}

	public class CartProductPromotion {
		public String sku;
		public String name;
		public String promotionType;
		public CartPeriod period; // TBD
		public Decimal discount;
		public Decimal newPrice;
	}

	public class CartPeriod {
		public String commitmentLength;
		public String commitmentPeriod;
	}

	public class CartAttribute {
		public String key;
		public String value;
	}

	public class CartCustomer {
		public String billingCustomerId;
		public String customerId;
		public CartStatus billingAddress;
		public CartStatus installationAddress;
		public CartStatus companyDetails;
		public CartStatus identification;
		public CartStatus personalDetails;
		public CartStatus contactDetails;
		public String shoppingCartItemPackageIds;
	}

	public class CartStatus {
		public String required;
		public Boolean valid;
	}

	public class CartSimStatus {
		public String required;
		public Boolean valid;
		public Boolean smsVerificationRequired;
	}

	public class CreateCart {
		public Boolean converged;
	}

	public class ChosenOrderType {
		public Boolean chosenOrderTypeIsBusiness;
	}

	public class Personal {
		public Boolean powerOfAttorneyConsent;
		public Date birthdate;
		public String gender;
		public String initials;
		public String lastNamePrefix;
		public String lastName;
	}

	public class Contact {
		public String email;
		public String phone1;
		public String phone2;
	}

	public class Identification {
		public Date expiryDate;
		public String documentNumber;
		public String type;
		public String nationality;
	}

	public class Company {
		public Address address;
		public String cocNumber;
		public Date establishDate;
		public Integer legalForm;
		public String name;
		public String vatNumber;
	}

	public class DeliveryAddress {
		public Boolean deliverToInvoiceAddress;
		public Address deliveryAddress;
	}

	public class Address {
		public String addressId;
		public String flatAddressId;
		public String country;
		public String city;
		public String street;
		public String houseNumber;
		public String houseNumberAddition;
		public String postcode;
		public String room;
	}

	public class Item {
		public Integer quantity;
		public String action;
		public Boolean withSim;
		public String sku;
		public Integer parentId;
	}

	public class TermsAgreed {
		public Boolean termsAgreed;
	}

	public class SummaryTermsAgreed {
		public Boolean summaryTermsAgreed;
	}

	public class PaymentInfo {
		public Boolean powerOfAttorneyConsent;
		public String accountHolder;
		public String iban;
	}

	public class MobileNumberPorting {
		public String contractNumber;
		public String passCode;
		public String currentNumber;
		public Integer shoppingCartItemPackageId;
		public String validationType;
	}

	public class Order {
		public String orderId;
		public String paymentReference;
		public String hawaiiOrderId;
		public String dealerCode;
		public String signature;
	}
}
