public without sharing class ClickApproveConstants {
	public static final String CLICKAPPROVE_DOCUMENT_SIGNATURE = 'ClickApprove';
	public static final String CLICKAPPROVE_REQUEST_DELIVERED_STATUS = 'Quotation Delivered';
}
