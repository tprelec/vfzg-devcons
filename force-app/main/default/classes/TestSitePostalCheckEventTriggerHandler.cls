/**
 * @author: Rahul Sharma (07-09-2021)
 * @description: Test class for SitePostalCheckEventTrigger and SitePostalCheckEventTriggerHandler
 * Also covers ISWSiteCheckService and ISWSiteCheckAPI
 * Jira ticket reference: COM-1960
 */

@IsTest
private class TestSitePostalCheckEventTriggerHandler {
    private static final String SPEED_SETTING_NAME_GIGA = 'Giga_Speed';
    private static final String SPEED_SETTING_NAME_HIGH = 'High_Speed';

    @IsTest
    private static void test_without_mock_to_illustrate_http_failure() {
        // verify default state
        System.assertEquals(
            false,
            SitePostalCheckEventTriggerHandler.isIswCheckEnabled()
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        System.assertEquals(
            true,
            SitePostalCheckEventTriggerHandler.isIswCheckEnabled()
        );

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );
        site.Site_House_Number_Suffix__c = null;
        update site;

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_FAILED,
            site.Checked_With_ISW__c
        );
        System.assertEquals(null, site.Footprint__c);
        System.assertNotEquals(null, site.ISW_Error_Message__c);
        System.assertEquals(
            0,
            [
                SELECT COUNT()
                FROM Site_Postal_Check__c
                WHERE
                    Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                    AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                    AND Access_Site_ID__c = :site.Id
                    AND Access_Active__c = TRUE
            ]
        );
    }

    @IsTest
    private static void test_http_failure() {
        Integer statusCode_400 = 400;

        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            statusCode_400,
            ''
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );
        site.Site_House_Number_Suffix__c = null;
        update site;

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_FAILED,
            site.Checked_With_ISW__c
        );
        System.assertEquals(null, site.Footprint__c);
        System.assertEquals(
            ISWSiteCheckAPI.getHttpFailureMessage(statusCode_400),
            site.ISW_Error_Message__c
        );
        System.assertEquals(
            0,
            [
                SELECT COUNT()
                FROM Site_Postal_Check__c
                WHERE
                    Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                    AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                    AND Access_Site_ID__c = :site.Id
                    AND Access_Active__c = TRUE
            ]
        );
    }

    @IsTest
    private static void test_unexpected_response_format() {
        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            'invalid XML request body'
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );
        site.Site_House_Number_Suffix__c = null;
        update site;

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_FAILED,
            site.Checked_With_ISW__c
        );
        System.assertEquals(null, site.Footprint__c);
        System.assertEquals(
            System.Label.ISWSiteCheck_ResponseFormatNotValid,
            site.ISW_Error_Message__c
        );
        System.assertEquals(
            0,
            [
                SELECT COUNT()
                FROM Site_Postal_Check__c
                WHERE
                    Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                    AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                    AND Access_Site_ID__c = :site.Id
                    AND Access_Active__c = TRUE
            ]
        );
    }

    @IsTest
    private static void test_unexpected_response_format_with_enqueue_another_job() {
        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            'invalid XML request body'
        );

        Test.startTest();

        // skip handling of platform event
        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = false;
        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );
        site.Site_House_Number_Suffix__c = null;
        update site;

        // explicitly unit test enqueueNextJob()
        ISWSiteCheckQueueable objISWSiteCheckQueueable = new ISWSiteCheckQueueable(
            new List<String>{ site.Id }
        );
        objISWSiteCheckQueueable.enqueueNextJob();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_FAILED,
            site.Checked_With_ISW__c
        );
        System.assertEquals(null, site.Footprint__c);
        System.assertEquals(
            System.Label.ISWSiteCheck_ResponseFormatNotValid,
            site.ISW_Error_Message__c
        );
        System.assertEquals(
            0,
            [
                SELECT COUNT()
                FROM Site_Postal_Check__c
                WHERE
                    Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                    AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                    AND Access_Site_ID__c = :site.Id
                    AND Access_Active__c = TRUE
            ]
        );
    }

    @IsTest
    private static void test_onnet_response_failure_message() {
        String errorCode = '2001';
        String response = new MockISWResponse().setErrorCode(errorCode).build();
        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            response
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_FAILED,
            site.Checked_With_ISW__c
        );

        System.assertEquals(
            ISWSiteCheckAPI.getResponseFailureMessage(errorCode),
            site.ISW_Error_Message__c
        );
        System.assertEquals(null, site.Footprint__c);
        System.assertEquals(
            0,
            [
                SELECT COUNT()
                FROM Site_Postal_Check__c
                WHERE
                    Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                    AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                    AND Access_Site_ID__c = :site.Id
                    AND Access_Active__c = TRUE
            ]
        );
    }

    @IsTest
    private static void test_onnet_no_tcp_lines() {
        String response = new MockISWResponse().setTcpLinesToEmpty().build();

        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            response
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_FAILED,
            site.Checked_With_ISW__c
        );

        System.assertEquals('No TCP Lines found', site.ISW_Error_Message__c);
        System.assertEquals(null, site.Footprint__c);
        System.assertEquals(
            0,
            [
                SELECT COUNT()
                FROM Site_Postal_Check__c
                WHERE
                    Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                    AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                    AND Access_Site_ID__c = :site.Id
                    AND Access_Active__c = TRUE
            ]
        );
    }

    @IsTest
    private static void test_onnet_internet_not_available() {
        String response = new MockISWResponse()
            .setFunctionalOrderingPossibleInternet('N')
            .build();
        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            response
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(
            'Internet not available on the location',
            site.ISW_Error_Message__c
        );

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_FAILED,
            site.Checked_With_ISW__c
        );
        System.assertEquals(null, site.Footprint__c);

        System.assertEquals(
            0,
            [
                SELECT COUNT()
                FROM Site_Postal_Check__c
                WHERE
                    Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                    AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                    AND Access_Site_ID__c = :site.Id
                    AND Access_Active__c = TRUE
            ]
        );
    }

    @IsTest
    private static void test_onnet_site_with_giga_speed_and_active() {
        String footprint = 'Fziggo_no';
        String response = new MockISWResponse()
            .setTcpService('false')
            .setFootprint(footprint)
            .setFunctionalOrderingPossibleGiga('Y')
            .build();
        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            response
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(null, site.ISW_Error_Message__c);

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_SUCCESS,
            site.Checked_With_ISW__c
        );
        System.assertEquals(footprint, site.Footprint__c);

        List<Site_Postal_Check__c> sitePostalChecks = [
            SELECT
                Access_Active__c,
                Access_Max_Down_Speed__c,
                Access_Max_Up_Speed__c
            FROM Site_Postal_Check__c
            WHERE
                Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                AND Access_Site_ID__c = :site.Id
        ];

        System.assertEquals(1, sitePostalChecks.size());

        System.assertEquals(true, sitePostalChecks[0].Access_Active__c);

        ISW_Service_Speed__mdt speedData = getspeedSetting(
            SPEED_SETTING_NAME_GIGA
        );
        System.assertEquals(
            speedData.Download_Speed__c,
            sitePostalChecks[0].Access_Max_Down_Speed__c
        );

        System.assertEquals(
            speedData.Upload_Speed__c,
            sitePostalChecks[0].Access_Max_Up_Speed__c
        );
    }

    @IsTest
    private static void test_onnet_site_with_high_speed_and_active() {
        String footprint = 'Fziggo_no';
        String response = new MockISWResponse()
            .setTcpService('false')
            .setFootprint(footprint)
            .setFunctionalOrderingPossibleInternet('Y')
            .build();
        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            response
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(null, site.ISW_Error_Message__c);

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_SUCCESS,
            site.Checked_With_ISW__c
        );
        System.assertEquals(footprint, site.Footprint__c);

        List<Site_Postal_Check__c> sitePostalChecks = [
            SELECT
                Access_Active__c,
                Access_Max_Down_Speed__c,
                Access_Max_Up_Speed__c
            FROM Site_Postal_Check__c
            WHERE
                Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                AND Access_Site_ID__c = :site.Id
        ];

        System.assertEquals(1, sitePostalChecks.size());

        System.assertEquals(true, sitePostalChecks[0].Access_Active__c);

        ISW_Service_Speed__mdt speedData = getspeedSetting(
            SPEED_SETTING_NAME_HIGH
        );

        System.assertEquals(
            speedData.Download_Speed__c,
            sitePostalChecks[0].Access_Max_Down_Speed__c
        );

        System.assertEquals(
            speedData.Upload_Speed__c,
            sitePostalChecks[0].Access_Max_Up_Speed__c
        );
    }

    @IsTest
    private static void test_onnet_site_with_giga_speed_and_already_active() {
        String footprint = 'Fziggo_no';
        String response = new MockISWResponse()
            .setTcpService('true')
            .setFootprint(footprint)
            .setFunctionalOrderingPossibleGiga('Y')
            .build();
        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            response
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(
            'Product is already active on this site',
            site.ISW_Error_Message__c
        );

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_FAILED,
            site.Checked_With_ISW__c
        );
        System.assertEquals(footprint, site.Footprint__c);

        List<Site_Postal_Check__c> sitePostalChecks = [
            SELECT
                Access_Active__c,
                Access_Max_Down_Speed__c,
                Access_Max_Up_Speed__c
            FROM Site_Postal_Check__c
            WHERE
                Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                AND Access_Site_ID__c = :site.Id
        ];

        System.assertEquals(1, sitePostalChecks.size());

        System.assertEquals(false, sitePostalChecks[0].Access_Active__c);

        ISW_Service_Speed__mdt speedData = getspeedSetting(
            SPEED_SETTING_NAME_GIGA
        );
        System.assertEquals(
            speedData.Download_Speed__c,
            sitePostalChecks[0].Access_Max_Down_Speed__c
        );

        System.assertEquals(
            speedData.Upload_Speed__c,
            sitePostalChecks[0].Access_Max_Up_Speed__c
        );
    }

    @IsTest
    private static void test_onnet_site_with_high_speed_and_already_active() {
        String footprint = 'Fziggo_no';
        String response = new MockISWResponse()
            .setTcpService('true')
            .setFootprint(footprint)
            .setFunctionalOrderingPossibleInternet('Y')
            .build();
        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            response
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_ONNET
        );

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(
            'Product is already active on this site',
            site.ISW_Error_Message__c
        );

        System.assertEquals(
            ISWSiteCheckService.CHECKED_WITH_ISW_FAILED,
            site.Checked_With_ISW__c
        );
        System.assertEquals(footprint, site.Footprint__c);

        List<Site_Postal_Check__c> sitePostalChecks = [
            SELECT
                Access_Active__c,
                Access_Max_Down_Speed__c,
                Access_Max_Up_Speed__c
            FROM Site_Postal_Check__c
            WHERE
                Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                AND Access_Site_ID__c = :site.Id
        ];

        System.assertEquals(1, sitePostalChecks.size());

        System.assertEquals(false, sitePostalChecks[0].Access_Active__c);

        ISW_Service_Speed__mdt speedData = getspeedSetting(
            SPEED_SETTING_NAME_HIGH
        );

        System.assertEquals(
            speedData.Download_Speed__c,
            sitePostalChecks[0].Access_Max_Down_Speed__c
        );

        System.assertEquals(
            speedData.Upload_Speed__c,
            sitePostalChecks[0].Access_Max_Up_Speed__c
        );
    }

    @IsTest
    private static void test_offnet_no_tcp_lines() {
        String response = new MockISWResponse().setTcpLinesToEmpty().build();

        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            response
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_OFFNET
        );

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(null, site.Checked_With_ISW__c);

        System.assertEquals(null, site.ISW_Error_Message__c);
        System.assertEquals(null, site.Footprint__c);
        System.assertEquals(
            0,
            [
                SELECT COUNT()
                FROM Site_Postal_Check__c
                WHERE
                    Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                    AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                    AND Access_Site_ID__c = :site.Id
                    AND Access_Active__c = TRUE
            ]
        );
    }

    @IsTest
    private static void test_offnet_site_with_tcp_high_speed() {
        String footprint = 'Fziggo_no';
        String response = new MockISWResponse()
            .setTcpService('true')
            .setFootprint(footprint)
            .setFunctionalOrderingPossibleInternet('Y')
            .build();
        TestUtilMultiRequestMock.setSingleMock(
            ISWSiteCheckAPI.ENDPOINT_URL,
            ISWSiteCheckAPI.HTTP_STATUS_CODE_200,
            response
        );

        Test.startTest();

        SitePostalCheckEventTriggerHandler.ISW_CHECK_ENABLED = true;

        Site__c site = createSite(
            ISWSiteCheckService.ACCESS_RESULT_CHECK_OFFNET
        );

        Test.getEventBus().deliver();

        Test.stopTest();

        site = [
            SELECT Id, Footprint__c, Checked_With_ISW__c, ISW_Error_Message__c
            FROM Site__c
            WHERE Id = :site.Id
        ];

        System.assertEquals(null, site.Checked_With_ISW__c);

        System.assertEquals(null, site.ISW_Error_Message__c);
        System.assertEquals(null, site.Footprint__c);
        System.assertEquals(
            0,
            [
                SELECT COUNT()
                FROM Site_Postal_Check__c
                WHERE
                    Access_Vendor__c = :ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO
                    AND Access_Infrastructure__c = :ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
                    AND Access_Site_ID__c = :site.Id
                    AND Access_Active__c = TRUE
            ]
        );
    }

    private static Site__c createSite(String accessResultCheck) {
        Account acct = TestUtils.createAccount(null);

        Site__c site = new Site__c(
            Site_Account__c = acct.Id,
            Site_Street__c = 'existingSiteStreet',
            Site_City__c = 'Testville',
            Site_Postal_Code__c = '3511WR',
            Site_House_Number__c = 128,
            Site_House_Number_Suffix__c = 'a'
        );
        insert site;

        // required setting to populate Access infrastructure based on mapping
        InfrastructureMapping__c infrastructureMapping = new InfrastructureMapping__c(
            Name = 'hfc',
            AccessInfrastructure__c = ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX
        );
        insert infrastructureMapping;

        List<Site_Postal_Check__c> sitePostalChecks = new List<Site_Postal_Check__c>{
            new Site_Postal_Check__c(
                Access_Site_ID__c = site.Id,
                Access_Vendor__c = ISWSiteCheckService.SPA_ACCESS_VENDOR_ZIGGO,
                Access_Infrastructure__c = ISWSiteCheckService.SPA_ACCESS_INFRASTRUCTURE_COAX,
                Technology_Type__c = infrastructureMapping.Name,
                Access_Result_Check__c = accessResultCheck
            ),
            new Site_Postal_Check__c(Access_Site_ID__c = site.Id)
        };
        insert sitePostalChecks;

        return site;
    }

    private static ISW_Service_Speed__mdt getspeedSetting(
        String speedSettingName
    ) {
        ISW_Service_Speed__mdt speedData = ISW_Service_Speed__mdt.getInstance(
            speedSettingName
        );
        System.assertNotEquals(
            null,
            speedData,
            'ISW_Service_Speed__mdt with name (' +
            speedSettingName +
            ') not found'
        );
        return speedData;
    }

    public class MockISWResponse {
        private String tcpService = 'false';
        private String footprint = 'ziggo';
        private String errorCode = ISWSiteCheckAPI.ERROR_CODE_0;
        private String lineStatus = 'INSERVICE';
        private Boolean isTcpLinesEmpty = false;
        private String functionalOrderingPossibleGiga = 'N';
        private String functionalOrderingPossibleInternet = 'N';
        private String serviceabilityRestrictions = 'false';

        public MockISWResponse() {
        }

        public MockISWResponse setTcpService(String tcpService) {
            this.tcpService = tcpService;
            return this;
        }

        public MockISWResponse setTcpLinesToEmpty() {
            this.isTcpLinesEmpty = true;
            return this;
        }

        public MockISWResponse setFootprint(String footprint) {
            this.footprint = footprint;
            return this;
        }

        public MockISWResponse setLineStatus(String lineStatus) {
            this.lineStatus = lineStatus;
            return this;
        }

        public MockISWResponse setFunctionalOrderingPossibleGiga(
            String functionalOrderingPossibleGiga
        ) {
            this.functionalOrderingPossibleInternet = 'Y';
            this.functionalOrderingPossibleGiga = functionalOrderingPossibleGiga;
            return this;
        }

        public MockISWResponse setFunctionalOrderingPossibleInternet(
            String functionalOrderingPossibleInternet
        ) {
            this.functionalOrderingPossibleInternet = functionalOrderingPossibleInternet;
            return this;
        }

        public MockISWResponse setErrorCode(String errorCode) {
            this.errorCode = errorCode;
            this.isTcpLinesEmpty = true;
            return this;
        }

        public String build() {
            String requestBody =
                '<?xml version="1.0" encoding="UTF-8"?>' +
                '<s:Envelope xmlns:s="http://www.w3.org/2003/05/soap-envelope">' +
                '<s:Body>' +
                '<GetServiceAvailabilityRequest_2Response xmlns="http://xy.spatial-eye.com/soap" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">' +
                '<Header></Header>' +
                '<Body>' +
                '<ErrorCode>' +
                this.errorCode +
                '</ErrorCode>' +
                '<ErrorMessage />';

            if (!isTcpLinesEmpty) {
                requestBody += '<TCPLINES>';
                requestBody += '<TcpLine_2>';

                if (String.isNotEmpty(this.lineStatus)) {
                    requestBody +=
                        '<LINESTATUS>' +
                        this.lineStatus +
                        '</LINESTATUS>';
                }
                if (String.isNotEmpty(this.serviceabilityRestrictions)) {
                    requestBody +=
                        '<SERVICEABILITYRESTRICTIONS>' +
                        this.serviceabilityRestrictions +
                        '</SERVICEABILITYRESTRICTIONS>';
                }

                if (String.isNotEmpty(this.tcpService)) {
                    requestBody +=
                        '<TCPSERVICE>' +
                        this.tcpService +
                        '</TCPSERVICE>';
                }
                if (String.isNotEmpty(this.footprint)) {
                    requestBody +=
                        '<FOOTPRINT>' +
                        this.footprint +
                        '</FOOTPRINT>';
                }

                requestBody += '<TCPSERVICELINES>';

                if (String.isNotEmpty(this.functionalOrderingPossibleGiga)) {
                    requestBody +=
                        '<TcpServiceLine>' +
                        '<FUNCTIONALORDERINGPOSSIBLE>' +
                        this.functionalOrderingPossibleGiga +
                        '</FUNCTIONALORDERINGPOSSIBLE>' +
                        '<TCPPOSSIBLESERVICETYPE>' +
                        ISWSiteCheckAPI.SERVICE_TYPE_GIGA +
                        '</TCPPOSSIBLESERVICETYPE>' +
                        '</TcpServiceLine>';
                }

                if (
                    String.isNotEmpty(this.functionalOrderingPossibleInternet)
                ) {
                    requestBody +=
                        '<TcpServiceLine>' +
                        '<FUNCTIONALORDERINGPOSSIBLE>' +
                        this.functionalOrderingPossibleInternet +
                        '</FUNCTIONALORDERINGPOSSIBLE>' +
                        '<TCPPOSSIBLESERVICETYPE>' +
                        ISWSiteCheckAPI.SERVICE_TYPE_INTERNET +
                        '</TCPPOSSIBLESERVICETYPE>' +
                        '</TcpServiceLine>';
                }

                requestBody += '</TCPSERVICELINES>';
                requestBody += '</TcpLine_2>';
                requestBody += '</TCPLINES>';
            }

            requestBody +=
                '</Body>' +
                '</GetServiceAvailabilityRequest_2Response>' +
                '</s:Body>' +
                '</s:Envelope>';
            return requestBody;
        }
    }
}