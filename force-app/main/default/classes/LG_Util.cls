public class LG_Util {
	public static Set<Id> oopWithTaskCreated = new Set<Id>();

	/*
	 * Standard GUID regex pattern, used by Configurator for temporary IDs
	 */
	private static string guid {
		get {
			return '([a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-4[a-zA-Z0-9]{3}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12})';
		}
	}

	public static String getRecordTypeNameById(String objectName, Id strRecordTypeId) {
		return Schema.getGlobalDescribe()
			.get(objectName)
			.getDescribe()
			.getRecordTypeInfosById()
			.get(strRecordTypeId)
			.getName();
	}

	private static Boolean validateGUID(String input) {
		input = checkNull(input);
		return Pattern.matches(GUID, input);
	}

	public static List<String> getAdminEmailAddresses() {
		List<String> toAddresses = new List<String>();
		LG_EnvironmentVariables__c envSettings = LG_EnvironmentVariables__c.getInstance(
			UserInfo.getUserId()
		);

		if (EnvSettings == null) {
			toAddresses.add('davor.dubokovic@cloudsensesolutions.com');
		} else {
			if (EnvSettings.LG_Admin_Emails__c == '' || EnvSettings.LG_Admin_Emails__c == null) {
				toAddresses.add('davor.dubokovic@cloudsensesolutions.com');
			} else {
				toAddresses = EnvSettings.LG_Admin_Emails__c.split(',');
			}
		}

		return toAddresses;
	}

	public static void sendAdminEmail(Exception ex, String place) {
		string errorMessage =
			'Line Number: ' +
			ex.getLineNumber() +
			' Message: ' +
			ex.getMessage() +
			'\n';

		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		mail.setToAddresses(getAdminEmailAddresses());
		string subject = place + ' - Exception!!!';

		mail.setSubject(Subject);
		mail.setPlainTextBody(ErrorMessage);

		if (!Test.isRunningTest()) {
			Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ mail });
		}
	}

	public static String getVisualForceBaseUrl() {
		String baseUrl = '';

		LG_EnvironmentVariables__c envVariables = LG_EnvironmentVariables__c.getOrgDefaults();
		if (UserInfo.getUserType() == 'Standard') {
			if (envVariables != null && String.isNotBlank(envVariables.LG_VisualForceBaseURL__c)) {
				baseUrl = envVariables.LG_VisualForceBaseURL__c;
			}
		} else if (UserInfo.getUserType() == 'PowerPartner') {
			if (
				envVariables != null &&
				String.isNotBlank(envVariables.LG_VisualForcePartnerBaseURL__c)
			) {
				baseUrl = envVariables.LG_VisualForcePartnerBaseURL__c;
			}
		}
		return baseUrl;
	}

	public static String getSalesforceBaseUrl() {
		String baseUrl = '';

		LG_EnvironmentVariables__c envVariables = LG_EnvironmentVariables__c.getOrgDefaults();

		if (UserInfo.getUserType() == 'Standard') {
			if (envVariables != null && String.isNotBlank(envVariables.LG_SalesforceBaseURL__c)) {
				baseUrl = envVariables.LG_SalesforceBaseURL__c;
			}
		} else if (UserInfo.getUserType() == 'PowerPartner') {
			if (
				envVariables != null &&
				String.isNotBlank(envVariables.LG_SalesforcePartnerBaseURL__c)
			) {
				baseUrl = envVariables.LG_SalesforcePartnerBaseURL__c;
			}
		}
		return baseUrl;
	}

	public static String getSubscriptionRequestDeactivateStatus() {
		String subscriptionDeactivateStatus = '';

		csordtelcoa__Orders_Subscriptions_Options__c osOptions = csordtelcoa__Orders_Subscriptions_Options__c.getOrgDefaults();
		if (
			osOptions != null &&
			String.isNotBlank(osOptions.LG_SubscriptionRequestDeactivateStatus__c)
		) {
			subscriptionDeactivateStatus = osOptions.LG_SubscriptionRequestDeactivateStatus__c;
		}

		return subscriptionDeactivateStatus;
	}

	public static String getServiceRequestDeactivateStatus() {
		String serviceDeactivateStatus = '';

		csordtelcoa__Orders_Subscriptions_Options__c osOptions = csordtelcoa__Orders_Subscriptions_Options__c.getOrgDefaults();
		if (
			osOptions != null && String.isNotBlank(osOptions.LG_ServiceRequestDeactivateStatus__c)
		) {
			serviceDeactivateStatus = osOptions.LG_ServiceRequestDeactivateStatus__c;
		}

		return serviceDeactivateStatus;
	}

	public static String getSubscriptionClosedReplacedStatus() {
		String subscriptionClosedReplacedStatus = '';

		csordtelcoa__Orders_Subscriptions_Options__c osOptions = csordtelcoa__Orders_Subscriptions_Options__c.getOrgDefaults();
		if (
			osOptions != null &&
			String.isNotBlank(osOptions.csordtelcoa__Subscription_Closed_Replaced_State__c)
		) {
			subscriptionClosedReplacedStatus = osOptions.csordtelcoa__Subscription_Closed_Replaced_State__c;
		}

		return subscriptionClosedReplacedStatus;
	}

	//Returns the List of Statuses not Allowing a Change of the Subscription
	//Read from the Orders & Subscriptions Options custom settings
	public static List<String> getOSStatusesNotAllowingChange() {
		List<String> statusesNotAllowingChange = new List<String>();

		csordtelcoa__Orders_Subscriptions_Options__c osOptions = csordtelcoa__Orders_Subscriptions_Options__c.getOrgDefaults();
		if (
			osOptions != null &&
			String.isNotBlank(osOptions.csordtelcoa__Statuses_Not_Allowing_Change__c)
		) {
			for (
				String status : osOptions.csordtelcoa__Statuses_Not_Allowing_Change__c.split(',')
			) {
				statusesNotAllowingChange.add(status);
			}
		}

		return statusesNotAllowingChange;
	}

	//Returns the loookup field Id from the 'New' page of some object.
	//Lookup field Id used for 'Url hacks' to prepopulate a lookup field on the new object
	//page.
	//param page - PageReference object of the new object page
	//param objectPrefix - prefix of the object related to the lookup field we want to populate
	public static String getLookupFieldReferenceId(PageReference page, String objectPrefix) {
		//used in unit test
		String testPageContent =
			'<input type="hidden" id="testId_lktp" value="' +
			objectPrefix +
			'"/></tr>';

		String referenceId = 'noId';
		String pageContent = Test.IsRunningTest() ? testPageContent : page.getContent().toString();

		Pattern p = Pattern.compile('<input.+value="' + objectPrefix + '".*/>');

		for (String line : pageContent.split('/tr>')) {
			Matcher pm = p.matcher(line);

			if (pm.find()) {
				for (String str : pm.group(0).split('/>')) {
					if (str.contains('value="' + objectPrefix + '"') && str.contains('_lktp')) {
						p = Pattern.compile('id=".+_lktp"');
						pm = p.matcher(str);
						if (pm.find()) {
							referenceId = pm.group(0).replace('id="', '').replace('_lktp"', '');
						}
					}
				}
			}
		}
		return referenceId;
	}

	public static String getSandboxInstanceName() {
		String instanceName = '';

		LG_EnvironmentVariables__c envVariables = LG_EnvironmentVariables__c.getOrgDefaults();
		if (envVariables != null && String.isNotBlank(envVariables.LG_SandboxInstance__c)) {
			instanceName = envVariables.LG_SandboxInstance__c;
		}

		return instanceName;
	}

	public static Boolean isValidId(String strId) {
		return ((strId instanceof ID) || ValidateGUID(strId));
	}

	public static Boolean isValidSFId(String strId) {
		strId = checkNull(strId);
		return ((strId instanceof ID) || ValidateGUID(strId));
	}

	public static Boolean isValidConfiguratorId(String strId) {
		strId = checkNull(strId);
		return ValidateGUID(strId);
	}

	public static String checkNull(String input) {
		return String.isBlank(input) ? '' : input;
	}

	/*
	 * Return check for null or empty and return padded value (added sufix, prefix)
	 */
	public static String checkNullPadded(String input, String prefix, String sufix) {
		string temp = prefix == null ? '' : prefix;
		temp += checkNull(input);
		temp += sufix == null ? '' : sufix;

		if (String.isEmpty(temp)) {
			temp = ' ';
		}

		return temp;
	}

	/*
	 * Return empty string if string is null. If not return 15 digit ID
	 */
	public static String checkIdNull(String input) {
		return String.isBlank(input) ? '' : input.substring(0, 15);
	}

	public static String getFormattedAddress(
		String street,
		String houseNumber,
		String houseNumberExt,
		String postcode,
		String city
	) {
		if (
			String.isBlank(street) &&
			String.isBlank(houseNumber) &&
			String.isBlank(houseNumberExt) &&
			String.isBlank(postcode) &&
			String.isBlank(city)
		) {
			return '';
		} else {
			string first = (checkNull(street) +
				checkNullPadded(houseNumber, ' ', ' ') +
				checkNull(houseNumberExt))
				.trim();
			string second = checkNull(postcode) + checkNullPadded(city, ' ', null);
			return (!String.isBlank(first) && !String.isBlank(second))
				? first + ', ' + second
				: first + second;
		}
	}

	/**
	 * Create new Site record
	 * Params: product basket and address ID
	 */
	public static cscrm__Site__c createNewSite(cscfga__Product_Basket__c pb, Id premiseId) {
		cscrm__Site__c site = new cscrm__Site__c();
		site.cscrm__Installation_Address__c = premiseId;
		site.LG_COAXConnectionLocation__c = pb.LG_COAXConnectionLocation__c;
		site.cscrm__Suite_Room__c = pb.LG_SuiteRoom__c;
		return site;
	}

	/*
	 * Resolves Sites data and upserts. Insert where it is a new record or updates if it is an old one
	 */
	public static void resolveSiteDuplicatesAndUpsert(List<cscrm__Site__c> sites) {
		if (!sites.isEmpty()) {
			Set<Id> premiseIds = new Set<Id>();
			Set<String> roomNumbers = new Set<String>();

			for (cscrm__Site__c site : sites) {
				premiseIds.add(site.cscrm__Installation_Address__c);
				roomNumbers.add(site.cscrm__Suite_Room__c);
			}

			List<cscrm__Site__c> existingSites = [
				SELECT Id, cscrm__Installation_Address__c, cscrm__Suite_Room__c
				FROM cscrm__Site__c
				WHERE
					cscrm__Installation_Address__c IN :premiseIds
					AND cscrm__Suite_Room__c IN :roomNumbers
			];

			for (cscrm__Site__c site : sites) {
				for (cscrm__Site__c existingSite : existingSites) {
					if (
						site.cscrm__Installation_Address__c ==
						existingSite.cscrm__Installation_Address__c &&
						site.cscrm__Suite_Room__c == existingSite.cscrm__Suite_Room__c
					) {
						site.Id = existingSite.Id;
						break;
					}
				}
			}
			upsert sites;
		}
	}

	public static cscrm__Address__c createNewPremise(cscfga__Product_Basket__c pb) {
		cscrm__Address__c a = new cscrm__Address__c();

		a.cscrm__City__c = pb.LG_InstallationCity__c;
		a.cscrm__Country__c = pb.LG_InstallationCountry__c;
		a.LG_HouseNumber__c = pb.LG_InstallationHouseNumber__c;
		a.cscrm__Street__c = pb.LG_InstallationStreet__c;
		a.cscrm__Zip_Postal_Code__c = pb.LG_InstallationPostalCode__c;
		a.LG_HouseNumberExtension__c = pb.LG_InstallationHouseNumberExtension__c;
		a.cscrm__Account__c = pb.csbb__Account__c;
		a.LG_SharedOfficeBuilding__c = pb.LG_SharedOfficeBuilding__c;
		a.LG_COAXConnectionLocation__c = pb.LG_COAXConnectionLocation__c;

		return a;
	}

	public static cscrm__Address__c createNewPremise(
		cscfga__Product_Basket__c pb,
		Opportunity tmpOpportunity
	) {
		cscrm__Address__c a = new cscrm__Address__c();

		a.cscrm__City__c = pb.LG_InstallationCity__c;
		a.cscrm__Country__c = pb.LG_InstallationCountry__c;
		a.LG_HouseNumber__c = pb.LG_InstallationHouseNumber__c;
		a.cscrm__Street__c = pb.LG_InstallationStreet__c;
		a.cscrm__Zip_Postal_Code__c = pb.LG_InstallationPostalCode__c;
		a.LG_HouseNumberExtension__c = pb.LG_InstallationHouseNumberExtension__c;
		a.cscrm__Account__c = tmpOpportunity.AccountID;
		a.LG_SharedOfficeBuilding__c = pb.LG_SharedOfficeBuilding__c;
		a.LG_COAXConnectionLocation__c = pb.LG_COAXConnectionLocation__c;

		return a;
	}

	public static csconta__Billing_Account__c createNewBillingAccount(
		cscfga__Product_Basket__c tmpPB,
		Opportunity tmpOpportunity
	) {
		csconta__Billing_Account__c a = new csconta__Billing_Account__c();

		a.csconta__City__c = tmpPB.LG_BillingSameAsInstallationAddress__c
			? tmpPB.LG_InstallationCity__c
			: tmpPB.LG_BillingCity__c;
		a.csconta__Country__c = tmpPB.LG_BillingSameAsInstallationAddress__c
			? tmpPB.LG_InstallationCountry__c
			: tmpPB.LG_BillingCountry__c;
		a.LG_HouseNumber__c = tmpPB.LG_BillingSameAsInstallationAddress__c
			? tmpPB.LG_InstallationHouseNumber__c
			: tmpPB.LG_BillingHouseNumber__c;
		a.csconta__Street__c = tmpPB.LG_BillingSameAsInstallationAddress__c
			? tmpPB.LG_InstallationStreet__c
			: tmpPB.LG_BillingStreet__c;
		a.csconta__PostCode__c = tmpPB.LG_BillingSameAsInstallationAddress__c
			? tmpPB.LG_InstallationPostalCode__c
			: tmpPB.LG_BillingPostalCode__c;
		a.LG_HouseNumberExtension__c = tmpPB.LG_BillingSameAsInstallationAddress__c
			? tmpPB.LG_InstallationHouseNumberExtension__c
			: tmpPB.LG_BillingHouseNumberExtension__c;
		a.csconta__Account__c = tmpOpportunity.AccountID;
		a.LG_CustomerReference__c = tmpPB.LG_CustomerReference__c;
		a.LG_PaymentType__c = tmpPB.LG_PaymentType__c;
		a.LG_BankAccountHolder__c = tmpPB.LG_BankAccountName__c;
		a.LG_BankAccountNumberIBAN__c = tmpPB.LG_BankNumber__c;

		if (String.isNotBlank(tmpPB.LG_BillingEmailAddress__c)) {
			a.LG_BillingEmailAddress__c = tmpPB.LG_BillingEmailAddress__c;
		} else {
			a.LG_BillingEmailAddress__c = tmpPB.LG_AdminContactEmail__c;
		}

		return a;
	}

	public static void resolveAndInsertPremiseDuplicates(List<cscrm__Address__c> addresses) {
		if (!addresses.isEmpty()) {
			List<cscrm__Address__c> newAddresses = new List<cscrm__Address__c>();

			Set<String> uniqueKeys = new Set<String>();

			for (cscrm__Address__c a : addresses) {
				uniqueKeys.add(
					LG_Util.checkIdNull(a.cscrm__Account__c) +
					LG_Util.checkNull(a.cscrm__Zip_Postal_Code__c) +
					LG_Util.checkNull(a.LG_HouseNumber__c) +
					LG_Util.checkNull(a.LG_HouseNumberExtension__c)
				);
			}

			List<cscrm__Address__c> addressesByUnique = [
				SELECT Id, LG_Uniquekey__c, LG_UniqueKeyForm__c
				FROM cscrm__Address__c
				WHERE LG_Uniquekey__c IN :uniqueKeys
			];

			for (cscrm__Address__c a : addresses) {
				string unique =
					LG_Util.checkIdNull(a.cscrm__Account__c) +
					LG_Util.checkNull(a.cscrm__Zip_Postal_Code__c) +
					LG_Util.checkNull(a.LG_HouseNumber__c) +
					LG_Util.checkNull(a.LG_HouseNumberExtension__c);

				for (cscrm__Address__c au : addressesByUnique) {
					if (au.LG_Uniquekey__c == unique || au.LG_UniqueKeyForm__c == unique) {
						a.Id = au.Id;
						break;
					}
				}

				if (String.isBlank(a.Id)) {
					newAddresses.add(a);
				}
			}

			if (!newAddresses.isEmpty()) {
				insert newAddresses;
			}
		}
	}

	public static void resolveAndUpsertPremiseDuplicates(List<cscrm__Address__c> addresses) {
		if (!addresses.isEmpty()) {
			Set<String> uniqueKeys = new Set<String>();

			for (cscrm__Address__c a : addresses) {
				uniqueKeys.add(
					LG_Util.checkIdNull(a.cscrm__Account__c) +
					LG_Util.checkNull(a.cscrm__Zip_Postal_Code__c) +
					LG_Util.checkNull(a.LG_HouseNumber__c) +
					LG_Util.checkNull(a.LG_HouseNumberExtension__c)
				);
			}
			List<cscrm__Address__c> addressesByUnique = [
				SELECT Id, LG_Uniquekey__c, LG_UniqueKeyForm__c
				FROM cscrm__Address__c
				WHERE LG_Uniquekey__c IN :uniqueKeys
			];

			for (cscrm__Address__c a : addresses) {
				string unique =
					LG_Util.checkIdNull(a.cscrm__Account__c) +
					LG_Util.checkNull(a.cscrm__Zip_Postal_Code__c) +
					LG_Util.checkNull(a.LG_HouseNumber__c) +
					LG_Util.checkNull(a.LG_HouseNumberExtension__c);

				for (cscrm__Address__c au : addressesByUnique) {
					if (au.LG_Uniquekey__c == unique || au.LG_UniqueKeyForm__c == unique) {
						a.Id = au.Id;
						break;
					}
				}
			}
			upsert addresses;
		}
	}

	/**
	 *
	 * Method will update the Address/Premise records not having a valid footprint
	 *
	 * @param  String addressId - AddressId from Peal, to be queried with
	 * @param  String footprint - footprint value to upsert premises with
	 * @author Tomislav Blazek
	 * @ticket SFDT-1019
	 * @since  24/5/2016
	 */
	public static void updatePremiseFootprint(String addressId, String footprint) {
		List<cscrm__Address__c> premises = [
			SELECT Id, LG_Footprint__c, LG_AddressID__c
			FROM cscrm__Address__c
			WHERE LG_AddressID__c = :addressId AND LG_Footprint__c != :footprint
		];

		if (!premises.isEmpty()) {
			for (cscrm__Address__c premise : premises) {
				premise.LG_Footprint__c = footprint;
			}
			update premises;
		}
	}

	/**
	 * SEPA Mandate, Creates entry in CSCAP__Customer_Approval__c and sends out Approval Request to the Contact Id
	 *
	 * @param approvalObjectId - Approval object (Opportunity)
	 * @param recipientContactId - Recipient, approver (Contact)
	 * @param clickApproveSettingId - Click Approve setting (CSCAP__Click_Approve_Setting__c)
	 *
	 * @author Petar Miletic
	 * @ticket SFDT-902
	 * @since  26/05/2016
	 */
	public static CSCAP.API_1.MultipleSendApprovalRequestRecord createApprovalRequestRecord(
		Id approvalObjectId,
		Id recipientContactId,
		Id clickApproveSettingId,
		Boolean sendImmediately
	) {
		CSCAP.API_1.MultipleSendApprovalRequestRecord obj = new CSCAP.API_1.MultipleSendApprovalRequestRecord();

		// In order to use this field a lookup i needed that links object to the CSCAP__Customer_Approval__c
		obj.approvalObjectId = approvalObjectId;

		obj.recipientContactId = recipientContactId;
		obj.clickApproveSettingId = clickApproveSettingId;
		obj.attachmentIds = null;
		obj.documentIds = null;
		obj.cc = null;
		obj.bcc = null;

		if (sendImmediately) {
			List<CSCAP.API_1.MultipleSendApprovalRequestRecord> approvalRequests = new List<CSCAP.API_1.MultipleSendApprovalRequestRecord>();
			approvalRequests.add(obj);
			CSCAP.API_1.MultipleSendApprovalRequest(approvalRequests);
		}

		return obj;
	}

	public static void sendApprovalRequests(
		List<CSCAP.API_1.MultipleSendApprovalRequestRecord> approvalRequests
	) {
		CSCAP.API_1.MultipleSendApprovalRequest(approvalRequests);
	}

	public static Task createTask(
		String subject,
		String taskType,
		String result,
		Id ownerId,
		Date taskDate,
		String status,
		String priority,
		Id whatId,
		Boolean insertImmediately
	) {
		Task t = new Task();
		t.Subject = subject;
		t.Type = taskType;
		t.LG_Result__c = result;
		t.OwnerId = ownerId;
		t.ActivityDate = taskDate == null ? date.today() : taskDate;
		t.Status = status;
		t.Priority = priority;
		t.WhatId = whatId;

		if (insertImmediately) {
			insert t;
		}

		return t;
	}

	public static Messaging.SingleEmailMessage createEmail(
		String to,
		String subject,
		Id templateId,
		Boolean sendImmediately
	) {
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		String[] toAddresses = new List<String>{ to };
		mail.setToAddresses(toAddresses);
		mail.setSubject(subject);
		mail.setTemplateId(templateId);

		if (sendImmediately) {
			Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ mail });
		}
		return mail;
	}

	public static string getRecordTypebyDevName(String rtName) {
		string rTypeId;
		Map<string, string> recTypeMap = new Map<string, string>();
		List<RecordType> lstRecordType = [
			SELECT Id, Name, DeveloperName
			FROM recordType
			WHERE developerName = :rtName
		];
		if (!lstRecordType.isEmpty()) {
			for (RecordType rt : lstRecordType) {
				recTypeMap.put(rt.DeveloperName, rt.Id);
			}
		}
		if (recTypeMap.containsKey(rtName)) {
			rTypeId = recTypeMap.get(rtName);
		}
		return rTypeId;
	}

	/**
	 *
	 * Resolve Billing Account data and upserts. Insert where it is a new record or updates if it is an old one
	 * Billing account is not automatically assigned to the product
	 * basket, when order is uploaded from D2D app
	 *
	 * @author Petar Miletic
	 * @ticket SFDT-1182, SFDT-1341
	 * @since  17/06/2016
	 */
	public static void resolveAndUpsertBillingAccountDuplicates(
		List<csconta__Billing_Account__c> billingAccounts
	) {
		if (!billingAccounts.isEmpty()) {
			Set<String> iBANs = new Set<String>();
			Set<ID> accountIds = new Set<ID>();

			for (csconta__Billing_Account__c b : billingAccounts) {
				if (String.isNotBlank(b.LG_BankAccountNumberIBAN__c)) {
					IBANs.add(b.LG_BankAccountNumberIBAN__c);
				}

				accountIds.add(b.csconta__Account__c);
			}

			// Retrieve Billing Accounts by IBAN
			List<csconta__Billing_Account__c> billingAccountsFiltered = [
				SELECT
					Id,
					Name,
					LG_BankAccountNumberIBAN__c,
					LG_BillingAccountIdentifier__c,
					LG_ExternalId__c,
					csconta__Account__c
				FROM csconta__Billing_Account__c
				WHERE LG_BankAccountNumberIBAN__c IN :IBANs AND csconta__Account__c IN :accountIds
			];

			for (csconta__Billing_Account__c billingAccount : billingAccounts) {
				for (csconta__Billing_Account__c baFiltered : billingAccountsFiltered) {
					if (
						baFiltered.LG_BankAccountNumberIBAN__c ==
						billingAccount.LG_BankAccountNumberIBAN__c &&
						baFiltered.csconta__Account__c == billingAccount.csconta__Account__c
					) {
						billingAccount.Id = baFiltered.Id;
						break;
					}
				}
			}
			upsert billingAccounts;
		}
	}

	/**
	 * Sending SoHo orders to PEAL
	 *
	 * @author Petar Miletic
	 * @story SFDT-1076, SFDT-1076
	 * @since 29/06/2016
	 */
	public static HttpResponse httpRequestPost(String endpoint, String payload) {
		Http http = new Http();
		HttpRequest request = new HttpRequest();
		HttpResponse response = new HttpResponse();

		request.setEndpoint(endpoint);
		request.setMethod('POST');
		request.setBody(EncodingUtil.urlEncode(payload, 'UTF-8'));
		request.setCompressed(true);

		try {
			response = http.send(request);
		} catch (System.CalloutException e) {
			response.setStatusCode(400);
			response.setStatus('Bad request');
		}

		return response;
	}

	public static String generateRandomNumberString(Integer len) {
		final String chars = '012345678998746546849684984356367598435131321654987';
		String randString = '';

		while (randString.length() < len) {
			Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
			randString += chars.substring(idx, idx + 1);
		}

		return randString;
	}

	public static String generateRandomCharacterString(Integer len) {
		final String chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		String randString = '';

		while (randString.length() < len) {
			Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
			randString += chars.substring(idx, idx + 1);
		}

		return randString;
	}

	public static String generateGUID() {
		String newGUID = String.format(
			'{0}-{1}-4{2}-{3}-{4}',
			new List<String>{
				LG_Util.generateRandomCharacterString(8),
				LG_Util.generateRandomCharacterString(4),
				LG_Util.generateRandomCharacterString(3),
				LG_Util.generateRandomCharacterString(4),
				LG_Util.generateRandomCharacterString(12)
			}
		);

		if (!ValidateGUID(newGUID)) {
			throw new LG_Exception('Invalid GUID generation)');
		}

		return newGUID;
	}

	public static String getCurrentUserSalesChannel() {
		Id currentUserId = UserInfo.getUserId();
		User currentUser = [SELECT Id, LG_SalesChannel__c FROM User WHERE Id = :currentUserId];

		return currentUser.LG_SalesChannel__c;
	}

	public static List<String> getSalesChannelsForTerminate() {
		List<String> salesChannelList = new List<String>();
		LG_MACDSpecificVariables__c macdSpecifics = LG_MACDSpecificVariables__c.getInstance(
			UserInfo.getUserId()
		);

		if (
			macdSpecifics != null &&
			String.isNotBlank(macdSpecifics.LG_SalesChannelsAllowedForTerminate__c)
		) {
			for (
				String salesChannel : macdSpecifics.LG_SalesChannelsAllowedForTerminate__c.split(
					','
				)
			) {
				salesChannelList.add(salesChannel);
			}
		}

		return salesChannelList;
	}

	public static String getCountryCodeFromCountryName(String countryName) {
		String countryCode = '';

		if (countryName == 'Netherlands') {
			countryCode = 'NL';
		} else if (countryName == 'Germany') {
			countryCode = 'DE';
		} else if (countryName == 'Auatria') {
			countryCode = 'AT';
		} else if (countryName == 'Poland') {
			countryCode = 'PL';
		} else {
			countryCode = 'ISO';
		}

		return countryCode;
	}

	/**
	 * Compare two trigger Maps (works with insert and update)
	 *
	 * @param fieldName - field for comparison
	 * @param newMap - new trigger map
	 * @param oldMap - old trigger map
	 *
	 * @author Petar Miletic
	 * @story SFDT-1188, SFDT-1296
	 * @since 12/07/2016
	 */
	public static Set<Id> compareTwoTriggerMaps(
		String fieldName,
		Map<Id, sObject> newMap,
		Map<Id, sObject> oldMap
	) {
		Set<Id> objIds = new Set<Id>();

		// If Insert take all new object into consideration
		if (oldMap == null) {
			objIds = newMap.keySet();
		} else {
			// If !Insert filter values by field name
			for (sObject o : newMap.values()) {
				if (o.get(fieldName) != oldMap.get(o.Id).get(fieldName)) {
					objIds.add(o.Id);
				}
			}
		}

		return objIds;
	}

	public static Set<Id> getKeySetByPropertyName(String fieldName, List<sObject> objList) {
		Set<Id> objIds = new Set<Id>();

		for (sObject o : objList) {
			objIds.add((Id) o.get(fieldName));
		}

		return objIds;
	}

	// formats the date in the dd-mm-yyyy format
	public static String getFormattedDate(Date value) {
		String formattedDate = '';

		if (value != null) {
			formattedDate = value.day() + '-' + value.month() + '-' + value.year();
		}

		return formattedDate;
	}

	public static String trimAll(String inputString) {
		if (inputString == null) {
			return '';
		}

		return inputString.replaceAll('\\s+', '');
	}

	public static Map<String, LG_PostalCode__c> fetchPostalCodeAddresses(
		Set<String> postalCodeSet
	) {
		Map<String, LG_PostalCode__c> postalCodeNameAddressMap = new Map<String, LG_PostalCode__c>();
		if (!postalCodeSet.isEmpty()) {
			for (LG_PostalCode__c pCode : [
				SELECT
					Id,
					Name,
					LG_AreaCode__c,
					LG_City__c,
					LG_Country__c,
					LG_HouseNumber__c,
					LG_PostalCode__c,
					LG_Postal_Code__c,
					LG_StreetName__c
				FROM LG_PostalCode__c
				WHERE Name IN :postalCodeSet
			]) {
				postalCodeNameAddressMap.put(pCode.Name, pCode);
			}
		}

		return postalCodeNameAddressMap;
	}

	//Util method to avoid unwanted updates.
	public static boolean validateUpdateFields(String oldValue, String newValue) {
		Boolean nullReplacingNull = false;
		if (oldValue == null && newValue == null) {
			nullReplacingNull = true;
		}
		Boolean nullGettingReplacedByEmptyString = false;
		if (oldValue == null && newValue != null && String.isBlank(newValue)) {
			nullGettingReplacedByEmptyString = true;
		}

		Boolean emptyStringGettingReplacedByNull = false;
		if (oldValue != null && String.isBlank(oldValue) && newValue == null) {
			emptyStringGettingReplacedByNull = true;
		}
		Boolean bothEqual = false;
		if (oldValue != null && oldValue.equals(newValue)) {
			bothEqual = true;
		}
		if (
			!bothEqual &&
			!nullReplacingNull &&
			!nullGettingReplacedByEmptyString &&
			!emptyStringGettingReplacedByNull
		) {
			return true;
		} else {
			return false;
		}
	}

	public static Date createInstallationLeadDateForPhoneNumber(Integer installationLeadTime) {
		Integer installationLeadDays = 11;
		if (installationLeadTime != null) {
			installationLeadDays = installationLeadTime + 1;
		}

		Date fromtodayDate = Date.today();
		Integer counter = 0;

		while (counter < installationLeadDays) {
			Date dateB = fromtodayDate.toStartofWeek();
			Integer dayOfWeek = DateB.daysBetween(fromtodayDate);

			if (dayOfWeek != 5 && dayOfWeek != 6) {
				counter++;
			}

			fromtodayDate = fromtodayDate.addDays(1);
		}
		return fromtodayDate;
	}

	public static void logACallAfterVisit(Map<String, List<String>> oppMap) {
		List<Task> tasks = new List<Task>();
		for (String callResultValue : oppMap.keySet()) {
			for (String recordId : oppMap.get(callResultValue)) {
				if (!oopWithTaskCreated.contains(recordId)) {
					Task t = new Task();
					t.Subject = 'D2D Visit';
					t.ActivityDate = System.today();
					t.Type = 'Result of D2D Visit';
					t.LG_Result__c = callResultValue;
					t.Status = 'Completed';
					t.WhatId = recordId;
					t.OwnerId = userinfo.getUserId();
					tasks.add(t);
					oopWithTaskCreated.add(recordId);
				}
			}
		}

		insert tasks;
	}
}