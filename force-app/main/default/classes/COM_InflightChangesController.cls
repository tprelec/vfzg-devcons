public class COM_InflightChangesController {
	/*
	 * As part of COM-2593, commercial order was marked with Inflight-changes applied.
	 */
	public void executeBusinessLogic(List<csord__Service__c> serviceList) {
		List<Id> deliveryOrdersWithMacdTypeTermination = new List<Id>();
		List<Id> deliveryOrdersWithMacdTypeNotTermination = new List<Id>();

		for (csord__Service__c serviceIterate : serviceList) {
			if (serviceIterate.COM_Delivery_Order__r.MACD_Type__c != COM_Constants.MACD_TYPE_TERMINATION) {
				deliveryOrdersWithMacdTypeNotTermination.add(serviceIterate.COM_Delivery_Order__r.MACD_Type__c);
			} else {
				deliveryOrdersWithMacdTypeTermination.add(serviceIterate.COM_Delivery_Order__r.MACD_Type__c);
			}
		}

		if (deliveryOrdersWithMacdTypeTermination.size() > 0) {
			markDeliveryOrderInflightChangeApplied(deliveryOrdersWithMacdTypeTermination);
		}

		if (deliveryOrdersWithMacdTypeNotTermination.size() > 0) {
			// regroup
		}

		Map<Id, List<csord__Service__c>> regroupedDeliveryOrderServices = getDeliveryOrderServicesMap(deliveryOrdersWithMacdTypeNotTermination);
		List<Id> listOfDeliveryOrdersWithAllCancelledServices = new List<Id>();
		List<Id> listOfDeliveryOrdersWithNotAllCancelledServices = new List<Id>();

		for (Id deliveryOrderId : regroupedDeliveryOrderServices.keySet()) {
			Integer numberOfServicesMarkedWithCancelledByChangeProcess = 0;
			for (csord__Service__c service : regroupedDeliveryOrderServices.get(deliveryOrderId)) {
				if (service.csordtelcoa__Cancelled_By_Change_Process__c == true) {
					numberOfServicesMarkedWithCancelledByChangeProcess++;
				}
			}

			if (numberOfServicesMarkedWithCancelledByChangeProcess == regroupedDeliveryOrderServices.get(deliveryOrderId).size()) {
				listOfDeliveryOrdersWithAllCancelledServices.add(deliveryOrderId);
			} else {
				listOfDeliveryOrdersWithNotAllCancelledServices.add(deliveryOrderId);
			}
		}

		//List<COM_Delivery_Order__c> regroupedDeliveryOrders = new List<COM_Delivery_Order__c>();

		/*
         for(COM_Delivery_Order__c deliveryOrder : regroupedDeliveryOrders) {
             
         }
         */
	}

	private Map<Id, List<csord__Service__c>> getDeliveryOrderServicesMap(List<Id> listOfDeliveryOrdersIds) {
		Map<Id, List<csord__Service__c>> deliveryOrderServicesMap = new Map<Id, List<csord__Service__c>>();
		List<csord__Service__c> listOfServices = [
			SELECT id, COM_Delivery_Order__c, csordtelcoa__Cancelled_By_Change_Process__c
			FROM csord__Service__c
			WHERE COM_Delivery_Order__c IN :listOfDeliveryOrdersIds
		];

		for (csord__Service__c service : listOfServices) {
			if (deliveryOrderServicesMap.get(service.COM_Delivery_Order__c) == null) {
				List<csord__Service__c> newServiceList = new List<csord__Service__c>();
				newServiceList.add(service);
				deliveryOrderServicesMap.put(service.COM_Delivery_Order__c, newServiceList);
			} else {
				deliveryOrderServicesMap.get(service.COM_Delivery_Order__c).add(service);
			}
		}

		return deliveryOrderServicesMap;
	}

	private void markDeliveryOrderInflightChangeApplied(List<id> listOfDeliveryOrdersIds) {
		List<COM_Delivery_Order__c> listOfDeliveryOrders = [
			SELECT id, Inflight_Change_Applied__c
			FROM COM_Delivery_Order__c
			WHERE id IN :listOfDeliveryOrdersIds
		];

		for (COM_Delivery_Order__c deliveryOrder : listOfDeliveryOrders) {
			deliveryOrder.Inflight_Change_Applied__c = true;
		}

		update listOfDeliveryOrders;
	}

	private void markOnHoldFlags(List<id> listOfDeliveryOrdersIds) {
		List<COM_Delivery_Order__c> listOfDeliveryOrders = [
			SELECT id, Inflight_Change_Applied__c
			FROM COM_Delivery_Order__c
			WHERE id IN :listOfDeliveryOrdersIds
		];

		for (COM_Delivery_Order__c deliveryOrder : listOfDeliveryOrders) {
			if (deliveryOrder.On_Hold__c == true) {
				deliveryOrder.On_Hold__c = false;
			}
		}

		List<CSPOFA__Orchestration_Process__c> listOfOrchestrationProcess = [
			SELECT id, COM_Delivery_Order__c, CSPOFA__Process_On_Hold__c
			FROM CSPOFA__Orchestration_Process__c
			WHERE COM_Delivery_Order__c IN :listOfDeliveryOrders
		];

		for (CSPOFA__Orchestration_Process__c process : listOfOrchestrationProcess) {
			process.CSPOFA__Process_On_Hold__c = false;
		}

		update listOfOrchestrationProcess;
		update listOfDeliveryOrders;
	}

	private void markDeliveryOrderTypeAction(List<id> listOfDeliveryOrdersIds, Set<String> macdActions) {
		List<COM_Delivery_Order__c> listOfDeliveryOrders = [
			SELECT id, Inflight_Change_Applied__c, MACD_Type__c, MACD_Actions__c
			FROM COM_Delivery_Order__c
			WHERE id IN :listOfDeliveryOrdersIds
		];

		for (COM_Delivery_Order__c deliveryOrder : listOfDeliveryOrders) {
			deliveryOrder.Inflight_Change_Applied__c = true;
			deliveryOrder.MACD_Type__c = COM_Constants.DEFAULT_MACD_TYPE;
			deliveryOrder.MACD_Actions__c = String.join((Iterable<String>) macdActions, ', ');
		}
		update listOfDeliveryOrders;
	}

	/**
	 * if any delivery component, of a On Hold Delivery Order, has been changed, mark the Delivery Order record with Inflight_Change_Applied = true
	 */
	private void markDeliveryOrderWithInflightChangeApplied() {
	}

	/**
      * 3. resume the running processes by

         clearing the On Hold flag on all the related Delivery Order records that have had the On Hold flag set to true.
         clearing the On Hold flag on all the related Delivery Order Process records.
      */
	private void clearOnHoldFlag() {
		clearOnHoldFlagOnAllRelatedDeliveryOrders();
		clearOnHoldFlagOnAllRelatedDeliveryOrderProcess();
	}

	private void clearOnHoldFlagOnAllRelatedDeliveryOrders() {
	}

	private void clearOnHoldFlagOnAllRelatedDeliveryOrderProcess() {
	}

	private void regenerateDeliveryComponents(List<csord__Service__c> serviceList) {
		List<csord__Service__c> serviceNewProvideList = new List<csord__Service__c>();
		List<csord__Service__c> serviceNotNewProvideList = new List<csord__Service__c>();

		for (csord__Service__c serviceIterate : serviceList) {
			if (serviceIterate.csordtelcoa__Replaced_Service__c == null) {
				serviceNewProvideList.add(serviceIterate);
			} else {
				serviceNotNewProvideList.add(serviceIterate);
			}
		}

		System.debug('serviceNewProvideList: ' + serviceNewProvideList);
		System.debug('serviceNotNewProvideList: ' + serviceNotNewProvideList);

		processNewProvideBusinessLogic(serviceNewProvideList);
		processNotNewProvideBusinessLogic(serviceNotNewProvideList);
	}

	private void processNewProvideBusinessLogic(List<csord__Service__c> serviceList) {
		// COM_Delivery_Order__c
		List<csord__Service__c> serviceDeliveryOrderAlreadyProvisionedList = new List<csord__Service__c>();
		List<csord__Service__c> serviceDeliveryOrderNotProvisionedList = new List<csord__Service__c>();

		for (csord__Service__c serviceIterate : serviceList) {
			if (serviceIterate.COM_Delivery_Order__r.Provisioned__c) {
				serviceDeliveryOrderAlreadyProvisionedList.add(serviceIterate);
			} else {
				serviceDeliveryOrderNotProvisionedList.add(serviceIterate);
			}
		}

		System.debug('serviceDeliveryOrderAlreadyProvisionedList: ' + serviceDeliveryOrderAlreadyProvisionedList);
		System.debug('serviceDeliveryOrderNotProvisionedList: ' + serviceDeliveryOrderNotProvisionedList);

		// processDeliveryOrderAlreadyProvisionedLogic(serviceDeliveryOrderAlreadyProvisionedList);
		// processDeliveryOrderNotProvisionedLogic(serviceDeliveryOrderNotProvisionedList);
	}

	private void processNotNewProvideBusinessLogic(List<csord__Service__c> serviceList) {
		// COM_Delivery_Order__c
		List<csord__Service__c> serviceDeliveryOrderAlreadyProvisionedList = new List<csord__Service__c>();
		List<csord__Service__c> serviceDeliveryOrderNotProvisionedList = new List<csord__Service__c>();

		for (csord__Service__c serviceIterate : serviceList) {
			if (serviceIterate.COM_Delivery_Order__r.Provisioned__c) {
				serviceDeliveryOrderAlreadyProvisionedList.add(serviceIterate);
			} else {
				serviceDeliveryOrderNotProvisionedList.add(serviceIterate);
			}
		}

		System.debug('serviceDeliveryOrderAlreadyProvisionedList: ' + serviceDeliveryOrderAlreadyProvisionedList);
		System.debug('serviceDeliveryOrderNotProvisionedList: ' + serviceDeliveryOrderNotProvisionedList);

		// processDeliveryOrderAlreadyProvisionedLogic(serviceDeliveryOrderAlreadyProvisionedList);
		// processDeliveryOrderNotProvisionedLogic(serviceDeliveryOrderNotProvisionedList);
	}
	/*
     private void processDeliveryOrderAlreadyProvisionedLogic(List<csord__Service__c> serviceList) {
         List<csord__Service__c> replacedServiceList = getReplacedServiceList(serviceList);
         Map<Id, List<Delivery_Component__c>> serviceIdDeliveryComponentMap = getServiceDeliveryComponentMap(serviceList);
         Map<Id, List<Delivery_Component__c>> replacedServiceIdDeliveryComponentMap = getReplacedServiceDeliveryComponentMap(replacedServiceList);
        
         System.debug('Service list: ' + serviceList);
         System.debug('replacedServiceList list: ' + replacedServiceList);
         System.debug('serviceIdDeliveryComponentMap: ' + serviceIdDeliveryComponentMap);
         System.debug('replacedServiceIdDeliveryComponentMap: ' + replacedServiceIdDeliveryComponentMap);
        
         List<Delivery_Component__c> deliveryComponentToUpdateList = new List<Delivery_Component__c>();
         List<Delivery_Component__c> deliveryComponentToDeleteList = new List<Delivery_Component__c>();
        
         if(serviceIdDeliveryComponentMap != null && replacedServiceIdDeliveryComponentMap != null) {
             for(csord__Service__c parentService : serviceList) {
                 if(serviceIdDeliveryComponentMap.get(parentService.Id) != null) {
                     for (Delivery_Component__c deliveryComponent : serviceIdDeliveryComponentMap.get(parentService.Id)) {
                         if(replacedServiceIdDeliveryComponentMap.get(parentService.csordtelcoa__Replaced_Service__c) != null) {
                             for (Delivery_Component__c replacedDeliveryComponent : replacedServiceIdDeliveryComponentMap.get(parentService.csordtelcoa__Replaced_Service__c)) {
                                 if(deliveryComponent.Name == replacedDeliveryComponent.Name && deliveryComponent.Article_Code__c == replacedDeliveryComponent.Article_Code__c) {
                                     deliveryComponent = setDeliveryComponentMACDActionNoChange(replacedDeliveryComponent);
                                 } else if (deliveryComponent.Name == replacedDeliveryComponent.Name && deliveryComponent.Article_Code__c != replacedDeliveryComponent.Article_Code__c) {
                                     setDeliveryComponentMACDActionChange(replacedDeliveryComponent);
                                     deliveryComponent = cloneCDOServiceID(replacedDeliveryComponent, deliveryComponent);
                                 } 
                                
                                
                                 if(!serviceIdDeliveryComponentMap.get(parentService.Id).contains(replacedDeliveryComponent)) {
                                     deliveryComponent = setDeliveryComponentMACDActionAdd(replacedDeliveryComponent);
                                 }
                                
                                 if(parentService.csordtelcoa__Cancelled_By_Change_Process__c) {
                                     deliveryComponent = setDeliveryComponentMACDActionDelete(replacedDeliveryComponent);
                                 }
                                
                                 deliveryComponentToUpdateList.add(deliveryComponent);
                             }
                         }
                     }
                 } 
             }
         }
        
         update deliveryComponentToUpdateList;
     }
    
     private void processDeliveryOrderNotProvisionedLogic(List<csord__Service__c> serviceList) {
         // COPY / PASTED processDeliveryOrderNotProvisionedLogic
         List<csord__Service__c> replacedServiceList = getReplacedServiceList(serviceList);
         Map<Id, List<Delivery_Component__c>> serviceIdDeliveryComponentMap = getServiceDeliveryComponentMap(serviceList);
         Map<Id, List<Delivery_Component__c>> replacedServiceIdDeliveryComponentMap = getReplacedServiceDeliveryComponentMap(replacedServiceList);
        
         System.debug('Service list: ' + serviceList);
         System.debug('replacedServiceList list: ' + replacedServiceList);
         System.debug('serviceIdDeliveryComponentMap: ' + serviceIdDeliveryComponentMap);
         System.debug('replacedServiceIdDeliveryComponentMap: ' + replacedServiceIdDeliveryComponentMap);
        
         List<Delivery_Component__c> deliveryComponentToUpdateList = new List<Delivery_Component__c>();
         List<Delivery_Component__c> deliveryComponentToDeleteList = new List<Delivery_Component__c>();
        
         if(serviceIdDeliveryComponentMap != null && replacedServiceIdDeliveryComponentMap != null) {
             for(csord__Service__c parentService : serviceList) {
                 if(serviceIdDeliveryComponentMap.get(parentService.Id) != null) {
                     for (Delivery_Component__c deliveryComponent : serviceIdDeliveryComponentMap.get(parentService.Id)) {
                         if(replacedServiceIdDeliveryComponentMap.get(parentService.csordtelcoa__Replaced_Service__c) != null) {
                             for (Delivery_Component__c replacedDeliveryComponent : replacedServiceIdDeliveryComponentMap.get(parentService.csordtelcoa__Replaced_Service__c)) {
                                 if(deliveryComponent.Name == replacedDeliveryComponent.Name && deliveryComponent.Article_Code__c == replacedDeliveryComponent.Article_Code__c) {
                                     deliveryComponent = setDeliveryComponentMACDActionNoChange(replacedDeliveryComponent);
                                 } else if (deliveryComponent.Name == replacedDeliveryComponent.Name && deliveryComponent.Article_Code__c != replacedDeliveryComponent.Article_Code__c) {
                                     setDeliveryComponentMACDActionChange(replacedDeliveryComponent);
                                     deliveryComponent = cloneCDOServiceID(replacedDeliveryComponent, deliveryComponent);
                                 } 
                                
                                
                                 if(!serviceIdDeliveryComponentMap.get(parentService.Id).contains(replacedDeliveryComponent)) {
                                     deliveryComponent = setDeliveryComponentMACDActionAdd(replacedDeliveryComponent);
                                 }
                                
                                 if(parentService.csordtelcoa__Cancelled_By_Change_Process__c) {
                                     deliveryComponent = setDeliveryComponentMACDActionDelete(replacedDeliveryComponent);
                                 }
                                
                                 deliveryComponentToUpdateList.add(deliveryComponent);
                             }
                         }
                     }
                 } 
             }
         }
        
         update deliveryComponentToUpdateList;
     }
    */

	private void deleteOldDeliveryComponents(csord__Service__c service) {
	}

	private void linkNewDeliveryComponents(csord__Service__c service) {
	}

	private Set<csord__Service__c> getParentServiceSet(List<csord__Service__c> serviceList) {
		Set<csord__Service__c> parentServiceSet = new Set<csord__Service__c>();

		for (csord__Service__c serviceIterate : serviceList) {
			if (serviceIterate.csord__Service__c == null) {
				parentServiceSet.add(serviceIterate);
			}
		}

		return parentServiceSet;
	}

	/*
     public Map<Id, List<Delivery_Component__c>> getReplacedServiceDeliveryComponentMap(List<csord__Service__c> serviceList) {
         Map<Id, List<Delivery_Component__c>> returnServiceDeliveryComponentMap = new Map<Id, List<Delivery_Component__c>>();
         Map<Id, List<Delivery_Component__c>> replacedChildServiceIdDeliveryComponents = new Map<Id, List<Delivery_Component__c>>();
         Map<Id, List<Delivery_Component__c>> replacedServiceIdDeliveryComponents = new Map<Id, List<Delivery_Component__c>>();
        
         List<Delivery_Component__c> replacedDeliveryComponents = [
                 SELECT Id,
                         Name,
                         Article_Code__c,
                         Article_Name__c,
                         Service__c,
                         MACD_Action__c,
                         COM_Delivery_Order__c,
                         COM_Delivery_Order__r.Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
                         Service__r.COM_Delivery_Order__c,
             			Service__r.Site__c,
                         Service__r.csord__Service__c,
                         Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c,
                         Service__r.csord__Service__r.csordtelcoa__Replacement_Service__c,
                         Service__r.csordtelcoa__Replacement_Service__r.csord__Service__c,
                         Service__r.csordtelcoa__Replaced_Service__c,
             			Service__r.csordtelcoa__Replaced_Service__r.Site__c,
                         Service__r.csordtelcoa__Replacement_Service__c,
                         Service__r.csordtelcoa__Replacement_Service__r.Site__c,
                         Service__r.csordtelcoa__Cancelled_By_Change_Process__c,
                         Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
                         Service__r.csord__Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
                         Installation_Required__c,
                         Implemented_Date__c,
                         Status__c,
                         CDO_Service_ID__c
                 FROM Delivery_Component__c
                 WHERE (Service__c IN :serviceList OR Service__r.csord__Service__c IN :serviceList)
         ];

         for (Delivery_Component__c deliveryComponent : replacedDeliveryComponents) {

             if (replacedChildServiceIdDeliveryComponents.get(deliveryComponent.Service__c) != null) {
                 replacedChildServiceIdDeliveryComponents.get(deliveryComponent.Service__c).add(deliveryComponent);
             }
             else {
                 replacedChildServiceIdDeliveryComponents.put(deliveryComponent.Service__c, new List<Delivery_Component__c>{
                         deliveryComponent
                 });
             }

             Id parentServiceId;
             if (deliveryComponent.Service__r.csord__Service__c != null) {
                 parentServiceId = deliveryComponent.Service__r.csord__Service__c;
             }
             else if (deliveryComponent.Service__r.csord__Service__c == null) {
                 parentServiceId = deliveryComponent.Service__c;
             }

             if (replacedServiceIdDeliveryComponents.get(parentServiceId) != null) {
                 replacedServiceIdDeliveryComponents.get(parentServiceId).add(deliveryComponent);
             }
             else {
                 replacedServiceIdDeliveryComponents.put(parentServiceId, new List<Delivery_Component__c>{
                         deliveryComponent
                 });
             }
         }
        
         returnServiceDeliveryComponentMap.putAll(replacedChildServiceIdDeliveryComponents);
         returnServiceDeliveryComponentMap.putAll(replacedServiceIdDeliveryComponents);
        
         return returnServiceDeliveryComponentMap;
     }
     */

	/*
     private Map<Id, List<Delivery_Component__c>> getServiceDeliveryComponentMap(List<csord__Service__c> serviceList) {
         List<Delivery_Component__c> deliveryComponentList = new List<Delivery_Component__c>();
         Map<Id, List<Delivery_Component__c>> serviceIdDeliveryComponentMap = new Map<Id, List<Delivery_Component__c>>();
        
         deliveryComponentList = [
                 SELECT Id,
                         Name,
                         Article_Code__c,
                         Article_Name__c,
                         Service__c,
                         COM_Delivery_Order__c,
                         MACD_Action__c,
                         COM_Delivery_Order__r.Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
             			Service__r.Site__c,
                         Service__r.COM_Delivery_Order__c,
                         Service__r.csord__Service__r.COM_Delivery_Order__c,
                         Service__r.csord__Service__c,
             			Service__r.csord__Service__r.Site__c,
                         Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c,
                         Service__r.csord__Service__r.csordtelcoa__Replacement_Service__c,
                         Service__r.csordtelcoa__Replaced_Service__c,
                         Service__r.csordtelcoa__Replaced_Service__r.Site__c,
                         Service__r.csordtelcoa__Replacement_Service__r.Site__c,
                         Service__r.csordtelcoa__Replacement_Service__c,
                         Service__r.csordtelcoa__Cancelled_By_Change_Process__c,
                         Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
                         Service__r.csord__Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
                         Installation_Required__c,
                         CDO_Service_ID__c,
                         Status__c,
                         Implemented_Date__c
                 FROM Delivery_Component__c
                 WHERE (Service__c IN :serviceList)
         ];
        
         for (Delivery_Component__c deliveryComponent : deliveryComponentList) {
             Id parentServiceId;
             if (deliveryComponent.Service__r.csord__Service__c != null) {
                 parentServiceId = deliveryComponent.Service__r.csord__Service__c;
             }
             else if (deliveryComponent.Service__r.csord__Service__c == null) {
                 parentServiceId = deliveryComponent.Service__c;
             }

             if (serviceIdDeliveryComponentMap.get(parentServiceId) != null) {
                 serviceIdDeliveryComponentMap.get(parentServiceId).add(deliveryComponent);
             }
             else {
                 serviceIdDeliveryComponentMap.put(parentServiceId, new List<Delivery_Component__c>{
                         deliveryComponent
                 });
             }
         }
        
         return serviceIdDeliveryComponentMap;
     }
     */

	private List<csord__Service__c> getReplacedServiceList(List<csord__Service__c> serviceList) {
		Set<Id> replcedServiceIds = new Set<Id>();

		for (csord__Service__c service : serviceList) {
			replcedServiceIds.add(service.csordtelcoa__Replaced_Service__c);
		}
		/*
         List<csord__Service__c> replacedServiceList = [
                 SELECT Id,
                         Name,
                         csord__Service__c,
                         Site__c,
                         (
                         SELECT Id,
                                 Name
                         FROM Delivery_Components__r
                         ),
                         csordtelcoa__Cancelled_By_Change_Process__c
                 FROM csord__Service__c
                 WHERE Id IN :replcedServiceIds 
         ];
        
         return replacedServiceList;
         */
		return null;
	}
	/*
     private Delivery_Component__c setDeliveryComponentMACDActionNoChange(Delivery_Component__c deliveryComponent) {
         return setDeliveryComponentMACDAction(deliveryComponent, 'N/A');
     }
    
     private Delivery_Component__c setDeliveryComponentMACDActionChange(Delivery_Component__c deliveryComponent) {
         return setDeliveryComponentMACDAction(deliveryComponent, 'Change');
     }
    
     private Delivery_Component__c setDeliveryComponentMACDActionAdd(Delivery_Component__c deliveryComponent) {
         return setDeliveryComponentMACDAction(deliveryComponent, 'Add');
     }
    
     private Delivery_Component__c setDeliveryComponentMACDActionDelete(Delivery_Component__c deliveryComponent) {
         return setDeliveryComponentMACDAction(deliveryComponent, 'Delete');
     }
    
     private Delivery_Component__c setDeliveryComponentMACDAction(Delivery_Component__c deliveryComponent, String actionMACD) {
         if(deliveryComponent != null) {
             deliveryComponent.MACD_Action__c = actionMACD;
         }
         return deliveryComponent;
     }
    
     private Delivery_Component__c cloneCDOServiceID(Delivery_Component__c replacedDeliveryComponent, Delivery_Component__c replacementDeliveryComponent) {
         replacementDeliveryComponent.CDO_Service_ID__c  = replacedDeliveryComponent.CDO_Service_ID__c;
         return replacementDeliveryComponent;
     }
     */
}
