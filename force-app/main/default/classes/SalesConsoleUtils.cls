/**
 * @description       : Sales console or otherwise called product basket is a CloudSense
 *  package that is used to enable selling of the products. This class serves as a repository
 *  of common activity around it.
 * @author            : marin.mamic@vodafoneziggo.com
 * @last modified on  : 09-21-2022
 * @last modified by  : marin.mamic@vodafoneziggo.com
 **/
public with sharing class SalesConsoleUtils {
	@SuppressWarnings('PMD.EmptyStatementBlock')
	private SalesConsoleUtils() {
	}

	/**
	 * When Sales console custom button returns from an execution,
	 * a small toast message is displayed to the user. This enum
	 * represents a type of message returned to the user
	 **/
	public enum ConsoleMessageStatus {
		ERROR,
		OK
	}

	/**
	 * When Sales console custom button returns from an execution,
	 * a small modal window can be opened up to the user where a
	 * visualforce page could be rendered. This enum represents a
	 * size od that window.
	 **/
	public enum ConsoleDialogSize {
		L,
		M,
		S
	}

	/**
	 * When Sales console custom button returns from an execution,
	 * a small toast message is displayed to the user. This enum
	 * represents a wrapper around that message
	 **/
	public class ConsoleMessage {
		private String status;
		private String title;
		private String text;
		private String size;
		public String redirectURL { private get; set; }
		public Boolean displayInDialog { private get; set; }
		public String modalTitle { private get; set; }

		/**
		 * @description Constructs a sales console message
		 * @param ConsoleMessageStatus status the outcome of the operation;
		 *  valid values are “ok” and “error”; the resulting message balloon
		 *  shown to the User will then be green or red, respectively
		 * @param String title the header of the message that will be shown to the User
		 * @param String text the body of the message that will be shown to the User
		 **/
		public ConsoleMessage(ConsoleMessageStatus status, String title, String text) {
			this.status = String.valueOf(status).toLowerCase();
			this.title = title;
			this.text = text;
		}

		/**
		 * @description for dialogs only, the size of the dialog containing the
		 *  linked page; supported options are “s”, “m” and “l” for small (e.g.
		 *  a message), medium (e.g. a confirmation dialog) and large (full screen)
		 * @param ConsoleDialogSize size the size of the dialog.
		 **/
		public void setSize(ConsoleDialogSize size) {
			this.size = String.valueOf(size).toLowerCase();
		}

		/**
		 * @description serializes this object into a JSON format.
		 * @return String a JSON representation of this ConsoleMessage
		 **/
		public String toJSON() {
			return JSON.serialize(this, true);
		}
	}
}
