@IsTest
private class TestCS_MobileFlowCaseValidator {
    @IsTest
    static void testUpdateVfContractImplementationStatus() {
        Map<Id, Case> oldCaseMap;
        Map<Id, Case> newCaseMap;

        List<Case> initialCaseList = new List<Case>();
        List<Case> newCaseList = new List<Case>();
        
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Account a = CS_DataTest.createAccount('Test Account');
            a.OwnerId = simpleUser.Id;
            insert a;

            VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(simpleUser, true);

            initialCaseList.add(CS_DataTest.createCase('Order Cleaning', 'CS MF Order Cleaning', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Project Intake and Preparation', 'CS MF Project Intake and Preparation', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Small Implementation', 'CS MF Small Implementation', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Customer Setup', 'CS MF Customer Setup', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Product Setup', 'CS MF Product Setup', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('In Progress Check', 'CS MF In Progress Check', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Provide Mobile Subscribers', 'CS MF Provide Mobile Subscribers', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Migration Preparation', 'CS MF Migration Preparation', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Migration Execution', 'CS MF Migration Execution', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Product Cleanup', 'CS MF Product Cleanup', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Final Quality Check', 'CS MF Final Quality Check', simpleUser, false, 'Open', vfContract.Id, a.Id));
            initialCaseList.add(CS_DataTest.createCase('Process Rework', 'CS MF Process Rework', simpleUser, false, 'Open', vfContract.Id, a.Id));

            insert initialCaseList;
            oldCaseMap = new Map<Id, Case>(initialCaseList);

            newCaseList = initialCaseList.deepClone(true);
            for (Case c : newCaseList) {
                c.Status = 'On Hold';
            }

            newCaseMap = new Map<Id, Case>(newCaseList);

            CS_MobileFlowCaseValidator mfv = new CS_MobileFlowCaseValidator(oldCaseMap, newCaseMap);
            mfv.run();

            List<VF_Contract__c> vfContractAfter = [SELECT Id, Implementation_Status__c FROM VF_Contract__c WHERE Id = :vfContract.Id];
            System.assertEquals('On Hold', vfContractAfter[0].Implementation_Status__c);
        }
    }
}