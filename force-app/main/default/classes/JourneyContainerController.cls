public with sharing class JourneyContainerController {
	@AuraEnabled
	public static String getJourneySteps(String oppId) {
		List<Opportunity> opp = [SELECT Id, Journey_Steps__c FROM Opportunity WHERE Id = :oppId];
		if (opp.size() > 0 && opp[0].Journey_Steps__c != null && !String.isBlank(opp[0].Journey_Steps__c)) {
			return opp[0].Journey_Steps__c;
		}
		return JSON.serialize(AddMobileProductsJourneyHandler.getInitialJourneySteps());
	}

	@AuraEnabled
	public static Boolean updateJorneySteps(String oppId, String steps) {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		List<Opportunity> opp = [SELECT Id, Journey_Steps__c FROM Opportunity WHERE Id = :oppId];
		if (opp.size() > 0) {
			opp[0].Journey_Steps__c = steps;
			update opp;
			return true;
		}
		return false;
	}
}
