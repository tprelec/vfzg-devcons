global with sharing class FieldValues {
    
    @AuraEnabled
    public String value;
    @AuraEnabled
    public String label;
    @AuraEnabled
    public String name;
    @AuraEnabled
    public Boolean visibleInUI;

    
    global FieldValues(String value, String label, String name, Boolean visibleInUI) {
        this.value = value;
        this.label = label;
        this.name = name;
        this.visibleInUI = visibleInUI;

    }
    
     global FieldValues(String value, String label, Boolean visibleInUI) {
        this.value = value;
        this.label = label;
        this.name = value;
        this.visibleInUI = visibleInUI;

    }
    
    global FieldValues(String name, String label, String visibleInUI) {
        this.value = '';
        this.label = label;
        this.name = value;
        if(visibleInUI.touppercase() == 'TRUE'){
            this.visibleInUI = true;
        }
        else{
            this.visibleInUI = false;
        }
        

    }


     global FieldValues(String value, Boolean visibleInUI) {
            this.value = value;
            this.label = value;
            this.label = this.label.removeEnd('__c');
            this.name = value;
            this.visibleInUI = visibleInUI;

    }
}