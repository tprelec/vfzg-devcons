public without sharing class SiteCheckEnrichDataController {

    private transient Map<Id,FeedItem> spcIdToFeedItem {get;set;}

    public SiteCheckEnrichDataController(ApexPages.StandardController controller) {
        
        siteId = ApexPages.currentPage().getParameters().get('id');

        activeChecks = new List<pcCheckWrapper>();
        //Map<Id,Attachment> spcIdToAttachment = new Map<Id,Attachment>();
        spcIdToFeedItem = new Map<Id,FeedItem>();

        for(Site_Postal_Check__c check : [Select 
                            Id, 
                            Access_Active__c,
                            CreatedDate,
                            Access_Site_ID__r.Last_fiber_check__c,
                            Access_Vendor__c, 
                            Technology_Type__c, 
                            Accessclass__c, 
                            Access_Max_Down_Speed__c, 
                            Access_Max_Up_Speed__c,
                            Access_Offer_Amount__c,
                            Access_Offer_Valide_Date__c
                        From 
                            Site_Postal_Check__c
                        Where
                        	Access_Site_ID__c = :siteId
                        AND
                        	// for now this is only applicable to Fiber checks 
                            Technology_Type__c = 'Fiber'
                        AND
                            Existing_Infra__c = false
                        AND
                            Is_Latest_Check__c = true
						]){
            // only use the most recent check's inactive results
        	pcCheckWrapper pcw = new pcCheckWrapper();
        	pcw.spc = check;
        	//spcIdToAttachment.put(check.Id,null);
            spcIdToFeedItem.put(check.Id,null);

        	activeChecks.add(pcw);
        }
        // add attachments (order by createddate to only show the newest one)
        /*for(Attachment a : [Select Id, 
        						Name, 
        						ParentId, 
        						Body 
        					From 
        						Attachment 
        					Where 
        						ParentId in :spcIdToAttachment.keySet() 
        					ORDER BY 
        						CreatedDate ASC]){
        	spcIdToAttachment.put(a.ParentId,a);
        }*/
        for(FeedItem fi : [Select
                                Id,
                                ParentId,
                                ContentData,
                                ContentFileName,
                                RelatedRecordId
                            from
                                FeedItem
                            Where
                                ParentId in :spcIdToFeedItem.keySet()
                            ORDER BY
                                CreatedDate ASC]){
            spcIdToFeedItem.put(fi.ParentId,fi);
        }        
        for(pcCheckWrapper pcw : activeChecks){
        	/*if(spcIdToAttachment.get(pcw.spc.Id) != null){
        		pcw.a = spcIdToAttachment.get(pcw.spc.Id);
        	} else {
        		pcw.a = new Attachment(parentId = pcw.spc.Id);
        	}*/
            if(spcIdToFeedItem.get(pcw.spc.Id) != null){
                // create a copy since this will be a new version if it is changed
                pcw.f = new FeedItem(parentId = pcw.spc.Id, 
                                    Visibility = 'InternalUsers',
                                    //ContentData = spcIdToFeedItem.get(pcw.spc.Id).ContentData,
                                    ContentFileName = spcIdToFeedItem.get(pcw.spc.Id).ContentFileName);

                //spcIdToFeedItem.get(pcw.spc.Id).clone();
            } else {
                pcw.f = new FeedItem(parentId = pcw.spc.Id, Visibility = 'InternalUsers');
            }            
        }
        system.debug(activeChecks);

    }

    public Id siteId {get;private set;}

    public List<pcCheckWrapper> activeChecks {get;set;}
    public List<pcCheckWrapper> newChecks {
        get{
            if(newChecks == null) newChecks = new List<pcCheckWrapper>();
            return newChecks;
        }
        set;}

    public class pcCheckWrapper {
    	public Site_Postal_Check__c spc {
            get;
            set{
                spc = value;
                dummyTask = new Task();
                dummyTask.dummy_Date__c = spc.Access_Offer_Valide_Date__c;
                dummyTask.dummy_Number__c = spc.Access_Offer_Amount__c;
            }
        }
    	//public Attachment a {get;set;}
        public  FeedItem f {get;set;}
        public Task dummyTask{get;set;}
        public String datename {get;set;}

        public String upSpeedSelected{
            get {return String.valueOf(spc.Access_Max_Up_Speed__c);}
            set {spc.Access_Max_Up_Speed__c = Integer.valueOf(value);}
        }

        public String downSpeedSelected{
            get {return String.valueOf(spc.Access_Max_Down_Speed__c);}
            set {spc.Access_Max_Down_Speed__c = Integer.valueOf(value);}
        }

        public List<SelectOption> getSpeeds() {
            List<SelectOption> speeds = new List<SelectOption>();
            speeds.add(new SelectOption('100000','100,000'));
            speeds.add(new SelectOption('1048576','1,048,576'));

            return speeds;
        }

        public List<SelectOption> getResultChecks() {
            List<SelectOption> resultChecks = new List<SelectOption>();
            resultChecks.add(new SelectOption('ONNET','ONNET'));
            resultChecks.add(new SelectOption('NEARNET','NEARNET'));
            resultChecks.add(new SelectOption('OFFNET','OFFNET'));

            return resultChecks;
        }        
    }

    public PageReference saveUpdates(){
        List<Site_Postal_Check__c> spcToUpsert = new List<Site_Postal_Check__c>();
        //List<Attachment> attToUpsert = new List<Attachment>();
        List<FeedItem> fiToInsert = new List<FeedItem>();
        List<FeedItem> fiToDelete = new List<FeedItem>();
        

        activeChecks.addAll(newChecks);

        for(pcCheckWrapper pcw : activeChecks){
        	system.debug(pcw);
            // copy values from dummy object
            pcw.spc.Access_Offer_Valide_Date__c = pcw.dummyTask.Dummy_Date__c;
            pcw.spc.Access_Offer_Amount__c = pcw.dummyTask.Dummy_Number__c;

            // only activate the ones that have an amount and a date filled in
            if(pcw.spc.Access_Offer_Amount__c != null && pcw.spc.Access_Offer_Amount__c > 0 && pcw.spc.Access_Offer_Valide_Date__c !=null){
                pcw.spc.Access_Active__c = true;
                //pcw.spc.Access_Result_Check__c = 'Green';
            }
            
        	spcToUpsert.add(pcw.spc);
        	/*if(pcw.a.Body != null)
        		attToUpsert.add(pcw.a);        	
            */
            if(pcw.f.ContentData != null){
                //Boolean notChanged = false;
                /*if(spcIdToFeedItem.containsKey(pcw.spc.Id)){
                    if(spcIdToFeedItem.get(pcw.spc.Id) != null)
                        if(pcw.f.ContentData == spcIdToFeedItem.get(pcw.spc.Id).ContentData)
                            // content has not changed. No need to update
                            notChanged = true;
                }*/
                
                //if(!notChanged)
                fiToInsert.add(pcw.f);  
            }
        }

        try{
            upsert spcToUpsert;
            //upsert attToUpsert;
            insert fiToInsert;
        } catch (Exception e){
            Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,e.getMessage()));
            return null;
        }

        return new PageReference('/'+siteId);
    } 

    public PageReference addMoreInfra(){
        pcCheckWrapper pcw = new pcCheckWrapper();
        Site_Postal_Check__c spc = new Site_Postal_Check__c();
        spc.Access_Vendor__c = 'ZIGGO';
        spc.Technology_Type__c = 'Fiber';
        spc.Accessclass__c = 'Fiber';
        spc.Access_Site_ID__c = siteId;
        spc.Existing_Infra__c = false;
        spc.Access_Priority__c = 1;
        // spc.Access_Result_Check__c TODO ONNET NEARNET OFFNET
        //spc.Access_Max_Down_Speed__c = 0;
        //spc.Access_Max_Up_Speed__c = 0;

        pcw.spc = spc;
        newChecks.add(pcw);
        return null;
    }

    public PageReference backToSite(){
        return new PageReference('/'+siteId);
    }
 
}