@isTest
public class LG_GeneralTest {
	//create Account

	public static Account createAccount(
		String name,
		String chamberofCommerceNumber,
		String footprint,
		Boolean insertAccount
	) {
		Account account = new Account(
			Name = name,
			KVK_number__c = chamberofCommerceNumber,
			LG_Footprint__c = footprint
		);

		if (insertAccount) {
			insert account;
		}

		return account;
	}

	//catgov-830

	public static Account createAccount(
		String name,
		String chamberofCommerceNumber,
		String footprint,
		Date compDate,
		Boolean insertAccount
	) {
		Account account = new Account(
			Name = name,
			KVK_number__c = chamberofCommerceNumber,
			LG_Footprint__c = footprint,
			LG_CompetitorContractEndDate__c = date.today()
		);

		if (insertAccount) {
			insert account;
		}

		return account;
	}

	//catgov-830 end
	public static Account createAccount(
		String accountName,
		String type,
		String segment,
		Integer numberOfEmployees,
		String leadStatus,
		String phone,
		String chamberofCommerceNumber,
		String footPrint,
		String visitStreet,
		String visitHouseNumber,
		String visitHouseNumberExtension,
		String visitPostalCode,
		String visitCity,
		String visitCountry,
		Boolean withInsert
	) {
		Account tmpAccount = new Account(Name = accountName);
		tmpAccount.Type = type;
		tmpAccount.LG_Segment__c = segment;
		tmpAccount.NumberOfEmployees = numberOfEmployees;
		tmpAccount.LG_AccountLeadStatus__c = leadStatus;
		tmpAccount.Phone = phone;
		tmpAccount.KVK_number__c = chamberofCommerceNumber;
		tmpAccount.LG_Footprint__c = footPrint;
		tmpAccount.LG_VisitStreet__c = visitStreet;
		tmpAccount.LG_VisitHouseNumber__c = visitHouseNumber;
		tmpAccount.LG_VisitHouseNumberExtension__c = visitHouseNumberExtension;
		tmpAccount.LG_VisitPostalCode__c = visitPostalCode;
		tmpAccount.LG_VisitCity__c = visitCity;
		tmpAccount.LG_VisitCountry__c = visitCountry;

		if (withInsert) {
			insert tmpAccount;
		}

		return tmpAccount;
	}

	public static Account createAccount(
		String accountName,
		String type,
		String segment,
		Integer numberOfEmployees,
		String leadStatus,
		String phone,
		String chamberofCommerceNumber,
		String footPrint,
		String visitStreet,
		String visitHouseNumber,
		String visitHouseNumberExtension,
		String visitPostalCode,
		String visitCity,
		String visitCountry
	) {
		return createAccount(
			accountName,
			type,
			segment,
			numberOfEmployees,
			leadStatus,
			phone,
			chamberofCommerceNumber,
			footPrint,
			visitStreet,
			visitHouseNumber,
			visitHouseNumberExtension,
			visitPostalCode,
			visitCity,
			visitCountry,
			true
		);
	}

	//create Opportunity
	public static Opportunity createOpportunity(Account acc, Boolean withInsert) {
		Opportunity tmpOpportunity = new Opportunity(
			Name = 'Test',
			AccountId = acc.Id,
			StageName = 'Awareness of interest',
			CloseDate = Date.today() + 5
		);

		if (withInsert) {
			insert tmpOpportunity;
		}

		return tmpOpportunity;
	}

	public static Opportunity createOpportunity(Account acc) {
		return CreateOpportunity(acc, true);
	}

	//create Contact
	public static Contact createContact(
		Account acc,
		String firstname,
		String lastName,
		String salutation,
		String phone,
		String mobilePhone,
		String email,
		String role,
		Date birthDate,
		String mailingCity,
		String mailingCountry,
		String mailingPostalCode,
		String mailingStreet,
		Boolean withInsert
	) {
		Contact tmpContact = new Contact();
		tmpContact.AccountId = acc.Id;
		tmpContact.Firstname = firstname;
		tmpContact.LastName = lastName;
		tmpContact.Salutation = salutation;
		tmpContact.Phone = phone;
		tmpContact.MobilePhone = mobilePhone;
		tmpContact.Email = email;
		tmpContact.LG_Role__c = role;
		tmpContact.BirthDate = birthDate;
		tmpContact.MailingCity = mailingCity;
		tmpContact.MailingCountry = mailingCountry;
		tmpContact.MailingPostalCode = mailingPostalCode;
		tmpContact.MailingStreet = mailingStreet;
		tmpContact.Marketing_Achiever__c = 'Nee';
		tmpContact.Marketing_Status__c = 'Actief';

		if (withInsert) {
			insert tmpContact;
		}

		return tmpContact;
	}

	public static Contact createContact(
		Account acc,
		String firstname,
		String lastName,
		String salutation,
		String phone,
		String mobilePhone,
		String email,
		String role,
		Date birthDate,
		String mailingCity,
		String mailingCountry,
		String mailingPostalCode,
		String mailingStreet
	) {
		return createContact(
			acc,
			firstname,
			lastName,
			salutation,
			phone,
			mobilePhone,
			email,
			role,
			birthDate,
			mailingCity,
			mailingCountry,
			mailingPostalCode,
			mailingStreet,
			true
		);
	}

	//create Product Definition
	public static cscfga__Product_Definition__c createProductDefinition(
		String name,
		Boolean withInsert
	) {
		cscfga__Product_Definition__c prodDef = new cscfga__Product_Definition__c();
		prodDef.Name = Name;
		prodDef.cscfga__Description__c = name;
		prodDef.Product_Type__c = 'Fixed';

		if (withInsert) {
			insert prodDef;
		}

		return prodDef;
	}

	public static cscfga__Product_Definition__c createProductDefinition(String name) {
		return createProductDefinition(name, true);
	}

	//create Attribute Definition
	public static cscfga__Attribute_Definition__c createAttributeDefinition(
		string name,
		cscfga__Product_Definition__c productDef,
		String attribType,
		String dataType,
		String optKey,
		String oliType,
		String productDetailType,
		Boolean withInsert
	) {
		cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c();
		attDef.Name = name;
		attDef.cscfga__Type__c = attribType;
		attDef.cscfga__Data_Type__c = dataType;
		attDef.LG_OPTKey__c = optKey;
		attDef.LG_OLIType__c = oliType;
		attDef.LG_ProductDetailType__c = productDetailType;
		attDef.cscfga__Product_Definition__c = productDef.Id;

		if (withInsert) {
			insert attDef;
		}

		return attDef;
	}

	public static cscfga__Attribute_Definition__c createAttributeDefinition(
		string name,
		cscfga__Product_Definition__c productDef,
		String attribType,
		String dataType,
		String optKey,
		String oliType,
		String productDetailType
	) {
		return createAttributeDefinition(
			name,
			productDef,
			attribType,
			dataType,
			optKey,
			oliType,
			productDetailType,
			true
		);
	}

	//create Product Configuration
	public static cscfga__Product_Configuration__c createProductConfiguration(
		String name,
		integer contractTerm,
		cscfga__Product_Basket__c productBasket,
		cscfga__Product_Definition__c productDefinition,
		Boolean withInsert
	) {
		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c();
		ProductConfiguration.Name = name;
		ProductConfiguration.cscfga__Contract_Term__c = contractTerm;
		ProductConfiguration.cscfga__Product_Basket__c = productBasket.Id;
		ProductConfiguration.cscfga__Product_Definition__c = productDefinition.Id;
		ProductConfiguration.cscfga__Quantity__c = 1;

		if (withInsert) {
			insert ProductConfiguration;
		}

		return ProductConfiguration;
	}

	public static cscfga__Product_Configuration__c createProductConfiguration(
		String name,
		Integer contractTerm,
		cscfga__Product_Basket__c productBasket,
		cscfga__Product_Definition__c productDefinition
	) {
		return createProductConfiguration(
			name,
			contractTerm,
			productbasket,
			productDefinition,
			true
		);
	}

	//create Attribute
	public static cscfga__Attribute__c createAttribute(
		String name,
		cscfga__Attribute_Definition__c attributeDefinition,
		Boolean isLineItem,
		Double price,
		cscfga__Product_Configuration__c productConfiguration,
		Boolean recurring,
		String value,
		Boolean withInsert
	) {
		cscfga__Attribute__c attribute = new cscfga__Attribute__c();
		Attribute.Name = name;
		Attribute.cscfga__Attribute_Definition__c = attributeDefinition.Id;
		Attribute.cscfga__Is_Line_Item__c = isLineItem;
		Attribute.cscfga__Price__c = price;
		Attribute.cscfga__Product_Configuration__c = productConfiguration.Id;
		Attribute.cscfga__Recurring__c = recurring;
		Attribute.cscfga__Value__c = value;
		Attribute.cscfga__Line_Item_Description__c = name;

		if (withInsert) {
			insert attribute;
		}

		return attribute;
	}

	public static cscfga__Attribute__c createAttribute(
		String name,
		cscfga__Attribute_Definition__c attributeDefinition,
		Boolean isLineItem,
		Double price,
		cscfga__Product_Configuration__c productConfiguration,
		Boolean recurring,
		String value
	) {
		return createAttribute(
			name,
			attributeDefinition,
			isLineItem,
			price,
			productConfiguration,
			recurring,
			value,
			true
		);
	}

	public static cscfga__Product_Basket__c createProductBasket(
		String name,
		Account acc,
		Lead pLead,
		Opportunity opp,
		Boolean withInsert
	) {
		cscfga__Product_Basket__c productBasket = new cscfga__Product_Basket__c();
		ProductBasket.Name = name;
		// if (Acc!=null) ProductBasket.cfgoffline__Account__c=Acc.Id;
		if (Acc != null) {
			ProductBasket.csbb__Account__c = acc.Id;
		}

		if (pLead != null) {
			ProductBasket.Lead__c = pLead.Id;
		}

		if (Opp != null) {
			ProductBasket.cscfga__Opportunity__c = opp.Id;
		}

		ProductBasket.cscfga__Basket_Status__c = 'Valid';

		if (withInsert) {
			insert productBasket;
		}

		return ProductBasket;
	}

	//CATGOV-830 start
	public static cscfga__Product_Basket__c createProductBasket(
		String name,
		Account acc,
		Lead pLead,
		Opportunity opp,
		Boolean withInsert,
		Date compDate,
		Boolean compDateUkn
	) {
		cscfga__Product_Basket__c productBasket = new cscfga__Product_Basket__c();
		ProductBasket.Name = name;
		if (Acc != null) {
			ProductBasket.csbb__Account__c = acc.Id;
		}
		if (pLead != null) {
			ProductBasket.Lead__c = pLead.Id;
		}

		if (Opp != null) {
			ProductBasket.cscfga__Opportunity__c = opp.Id;
		}

		ProductBasket.cscfga__Basket_Status__c = 'Valid';
		ProductBasket.LG_MobileCompetitorContractEndDate__c = system.today();
		ProductBasket.LG_Mobile_Contract_End_Date_Unknown__c = true;

		if (withInsert) {
			insert ProductBasket;
		}

		return ProductBasket;
	}

	public static cscfga__Product_Basket__c createProductBasket(
		String name,
		Account acc,
		Lead pLead,
		Opportunity opp,
		Date compDate,
		Boolean compDateUkn
	) {
		return createProductBasket(name, acc, pLead, opp, true, compDate, compDateUkn);
	}

	public static cscfga__Product_Basket__c createProductBasket(
		String name,
		Account acc,
		Lead pLead,
		Opportunity opp
	) {
		return createProductBasket(name, acc, pLead, opp, true);
	}

	//create billing accounts
	public static csconta__Billing_Account__c createBillingAccount(
		String finNumber,
		Id accountId,
		Boolean defaultBillingAccount,
		Boolean insertAccount
	) {
		csconta__Billing_Account__c billAcc = new csconta__Billing_Account__c(
			csconta__Financial_Account_Number__c = finNumber,
			csconta__Account__c = accountId,
			csconta__Billing_Channel__c = 'Paper bill',
			LG_PaymentType__c = 'Bank Transfer',
			LG_Default__c = defaultBillingAccount,
			/*CRQ000000769283*/ LG_HouseNumber__c = '1' /*CRQ000000769283*/
		);

		if (insertAccount) {
			insert billAcc;
		}

		return billAcc;
	}

	//create product category
	public static cscfga__Product_Category__c createProductCategory(
		String name,
		Boolean insertCategory
	) {
		cscfga__Product_Category__c prodCategory = new cscfga__Product_Category__c(
			Name = String.isBlank(name) ? 'TestCategory' : name
		);

		if (insertCategory) {
			insert prodCategory;
		}

		return prodCategory;
	}
}