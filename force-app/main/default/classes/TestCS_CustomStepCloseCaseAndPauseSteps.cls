@IsTest
private class TestCS_CustomStepCloseCaseAndPauseSteps {
    @IsTest
    static void testBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            List<SObject> objects = new List<SObject>();
            String comCaseStatus = 'Closed - Planning Rescheduled';

            Case caseRecord1 = CS_DataTest.createCase('Install Solution', 'CS COM Install Solution', simpleUser, true);

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', false);
            insert step1Template;

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), true);
            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, false);
            step1.CSPOFA__Case_Record_Type__c = 'CS COM Install Solution';
            step1.CSPOFA__Status__c = 'In Progress';
            step1.CSPOFA__Related_Object_ID__c = caseRecord1.Id;
            step1.COM_Case_Status__c = comCaseStatus;
            insert step1;

            objects.add(step1);

            CS_CustomStepCloseCaseAndPauseSteps csCustomCloseCaseAndUpdateSteps= new CS_CustomStepCloseCaseAndPauseSteps();
            csCustomCloseCaseAndUpdateSteps.process(objects);

            List<Case> caseList = [SELECT Id, Status FROM Case WHERE Id = :caseRecord1.Id];

            for (Case caseRecord : caseList) {
                System.debug('*** caseRecord: ' + caseRecord.Status);
                System.assertEquals(caseRecord.Status,comCaseStatus);
            }
        }
    }
}