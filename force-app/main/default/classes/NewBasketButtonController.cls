public with sharing class NewBasketButtonController {
    
    @AuraEnabled
    public static String getPathPrefix(){
        return Site.getPathPrefix();
    }
}