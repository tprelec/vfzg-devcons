public with sharing class CS_OpportunityLineItemHelper {
	/*
	 * @description This method will create "ceased" OLIs based on replaced attributes/configurations referenced in csordtelcoa__Replaced_Product_Configuration__c field on
	 * cscfga__Product_Configuration__c and put them in input parameter ceasedOlis
	 * Also, attributtes used for creation of ceased OLIs will be put in ceasedAttributes
	 *
	 */
	public static void createCeasedOlis(
		List<ReplacedConfigurationDetail> replacedConfigurations,
		Map<Id, OpportunityLineItem> outputMapAttributeToCeasedOli,
		Map<Id, cscfga__Attribute__c> mapCeasedConfigurationToAttribute
	) {
		Set<Id> replacedConfigurationIds = new Set<Id>();
		Map<Id, Id> mapOldPcNewBasketId = new Map<Id, Id>();
		Set<String> setProductBasketId = new Set<String>();

		for (ReplacedConfigurationDetail configurationDetail : replacedConfigurations) {
			replacedConfigurationIds.add(configurationDetail.replacedConfigurationId);
			setProductBasketId.add(configurationDetail.replacingConfigurationAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c);
			mapOldPcNewBasketId.put(
				configurationDetail.replacedConfigurationId,
				configurationDetail.replacingConfigurationAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c
			);
		}

		Map<String, String> mapProductBasketIdPriceBookId = new AssignPriceBookToProductBasketImpl().AssignPriceBook(setProductBasketId);

		// mapProductBasket here contains new baskets, not the old ones which are associated with ceased configurations
		// when creating ceased items, we will need to know which new Oppty ID to properly connect OLI
		// ceased attribute doesn't have info about that Oppty, so we'll prepare proper map

		Map<Id, cscfga__Product_Basket__c> mapNewBaskets = new Map<Id, cscfga__Product_Basket__c>(
			[SELECT Id, cscfga__Opportunity__c FROM cscfga__Product_Basket__c WHERE Id IN :mapOldPcNewBasketId.values()]
		);
		// Id is Id of the configuration ceased attribute is linked to
		Map<Id, cscfga__Product_Basket__c> mapProductBasket = new Map<Id, cscfga__Product_Basket__c>();

		for (Id oldConfigurationId : mapOldPcNewBasketId.keySet()) {
			Id basketId = mapOldPcNewBasketId.get(oldConfigurationId);
			mapProductBasket.put(oldConfigurationId, mapNewBaskets.get(basketId));
		}

		System.debug(LoggingLevel.DEBUG, 'mapProductBasket ' + JSON.serializePretty(mapProductBasket));

		List<cscfga__Attribute__c> replacedAttributes = [
			SELECT
				Id,
				Name,
				cscfga__is_active__c,
				cscfga__Is_Line_Item__c,
				cscfga__Line_Item_Description__c,
				cscfga__Line_Item_Sequence__c,
				cscfga__Price__c,
				cscfga__List_Price__c,
				cscfga__Recurring__c,
				cscfga__Product_Configuration__c,
				cscfga__Product_Configuration__r.cscfga__Product_Basket__c,
				cscfga__Product_Configuration__r.Name,
				cscfga__Product_Configuration__r.cscfga__Product_Family__c,
				cscfga__Product_Configuration__r.Recurring_Charge_Product__c,
				cscfga__Product_Configuration__r.One_Off_Charge_Product__c,
				cscfga__Attribute_Definition__r.cscfga__Line_Item_Sequence__c,
				cscfga__Product_Configuration__r.cscfga__Quantity__c,
				cscfga__Product_Configuration__r.cscfga__Root_Configuration__c,
				cscfga__Product_Configuration__r.cscfga__Contract_Term__c,
				cscfga__Product_Configuration__r.Deal_Type__c,
				cscfga__Product_Configuration__r.Proposition__c,
				cscfga__Product_Configuration__r.Mobile_Scenario__c,
				cscfga__Product_Configuration__r.Model_Number__c,
				cscfga__Product_Configuration__r.cscfga__Recurring_Charge__c,
				cscfga__Product_Configuration__r.RC_Cost__c,
				cscfga__Product_Configuration__r.cscfga__One_Off_Charge__c,
				cscfga__Product_Configuration__r.NRC_Cost__c,
				cscfga__Product_Configuration__r.Contract_Length__c,
				cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Name,
				cscfga__Product_Configuration__r.cscfga__discounts__c,
				cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c,
				cscfga__Product_Configuration__r.Site_Availability_Id__c,
				cscfga__Product_Configuration__r.Proposition_Component__c,
				cscfga__Product_Configuration__r.Overbooking_type__c,
				cscfga__Product_Configuration__r.Category__c,
				cscfga__Product_Configuration__r.Add_On_Product__c,
				cscfga__Product_Configuration__r.Commercial_Product__c,
				cscfga__Product_Configuration__r.cscfga__Product_Basket__r.DirectIndirect__c,
				cscfga__Product_Configuration__r.cscfga__Product_Definition__r.Product_Type__c,
				cscfga__Product_Configuration__r.Site__c,
				cscfga__Product_Configuration__r.Contract_Number__c,
				cscfga__Product_Configuration__r.ContractNumber_JSON__c,
				cscfga__Product_Configuration__r.New_Delivery_Model__c,
				cscfga__Product_Configuration__r.New_Portfolio__c
			FROM cscfga__Attribute__c
			WHERE
				cscfga__Is_Line_Item__c = TRUE
				AND cscfga__is_active__c = TRUE
				AND name != '__Discount_One_Off__'
				AND name != '__Discount_Recurring__'
				AND name != '__Price_Adjustment_Recurring__'
				AND name != '__Price_Adjustment_One_Off__'
				AND cscfga__Product_Configuration__c IN :replacedConfigurationIds
		];

		System.debug(LoggingLevel.DEBUG, 'replacedAttributes: ' + JSON.serializePretty(replacedAttributes));

		Map<String, ProductUtility.Product> mapProduct2IdProduct = ProductUtility.createProducts2(replacedAttributes);

		System.debug(LoggingLevel.DEBUG, 'mapProduct2IdProduct: ' + JSON.serializePretty(mapProduct2IdProduct));

		Map<String, Map<String, ProductUtility.PBEntry>> mapPriceBookIdMapPBEntry = ProductUtility.MakePriceBookPBEntriesMap(
			mapProductBasketIdPriceBookId,
			replacedAttributes,
			mapProduct2IdProduct,
			mapOldPcNewBasketId
		);
		ProductUtility.CreatePriceBookEntries(mapPriceBookIdMapPBEntry);

		System.debug(Logginglevel.DEBUG, 'HELPER: mapPriceBookIdMapPBEntry ' + JSON.serializePretty(mapPriceBookIdMapPBEntry));

		for (cscfga__Attribute__c oldAttribute : replacedAttributes) {
			OpportunityLineItem ceasedItem = ProductUtility.createCeasedOpportunityLineItem(
				oldAttribute,
				mapProductBasketIdPriceBookId,
				mapProduct2IdProduct,
				mapPriceBookIdMapPBEntry,
				mapProductBasket
			);
			if (ceasedItem != null) {
				outputMapAttributeToCeasedOli.put(oldAttribute.Id, ceasedItem);
				mapCeasedConfigurationToAttribute.put(oldAttribute.cscfga__Product_Configuration__c, oldAttribute);
			}
		}
		System.debug(LoggingLevel.DEBUG, 'Number of ceased OLIs created: ' + outputMapAttributeToCeasedOli.size());
	}

	public static cscfga__Product_Configuration__c getReplacingConfiguration(
		Id replacedConfigurationId,
		List<ReplacedConfigurationDetail> replacedConfigurations
	) {
		for (ReplacedConfigurationDetail currentConfiguration : replacedConfigurations) {
			if (currentConfiguration.replacedConfigurationId == replacedConfigurationId) {
				return currentConfiguration.replacingConfigurationAttribute.cscfga__Product_Configuration__r;
			}
		}
		throw new System.ListException(
			String.format('Id {0} not found in list of replacedConfigurations', new List<Object>{ replacedConfigurationId })
		);
	}

	public static void eliminateRedundantCeasedOlis(
		Map<Id, OpportunityLineItem> mapAttributeToCeasedOli,
		Map<Id, cscfga__Attribute__c> mapCeasedConfigurationToAttribute,
		List<OpportunityLineItem> addedOlis,
		List<cscfga__Attribute__c> addedAttributes
	) {
		Map<Id, cscfga__Attribute__c> mapAddedAttributes = new Map<Id, cscfga__Attribute__c>(addedAttributes);

		System.debug(LoggingLevel.DEBUG, 'mapAttributeToCeasedOli ' + JSON.serializePretty(mapAttributeToCeasedOli));
		System.debug(LoggingLevel.DEBUG, 'mapCeasedConfigurationToAttribute ' + JSON.serializePretty(mapCeasedConfigurationToAttribute));
		System.debug(LoggingLevel.DEBUG, 'addedOlis ' + JSON.serializePretty(addedOlis));
		System.debug(LoggingLevel.DEBUG, 'mapAddedAttributes ' + JSON.serializePretty(mapAddedAttributes));

		for (Integer i = 0; i < addedOlis.size(); i++) {
			cscfga__Attribute__c addedAttribute = mapAddedAttributes.get(addedOlis[i].cscfga__Attribute__c);

			if (
				addedAttribute.cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c != null &&
				addedAttribute.cscfga__Product_Configuration__r.csordtelcoa__cancelled_by_change_process__c == false
			) {
				cscfga__Attribute__c ceasedAttribute = mapCeasedConfigurationToAttribute.get(
					addedAttribute.cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c
				);

				Boolean sameSite =
					ceasedAttribute.cscfga__Product_Configuration__r.Site__c == addedAttribute.cscfga__Product_Configuration__r.Site__c;
				Boolean sameCommercialProduct =
					ceasedAttribute.cscfga__Product_Configuration__r.Commercial_Product__c != null &&
					(ceasedAttribute.cscfga__Product_Configuration__r.Commercial_Product__c ==
					addedAttribute.cscfga__Product_Configuration__r.Commercial_Product__c);
				Boolean sameAddon =
					ceasedAttribute.cscfga__Product_Configuration__r.Add_On_Product__c != null &&
					(ceasedAttribute.cscfga__Product_Configuration__r.Add_On_Product__c ==
					addedAttribute.cscfga__Product_Configuration__r.Add_On_Product__c);

				if (sameSite && (sameCommercialProduct || sameAddon)) {
					// we have the same Add and Cease OLI
					// both can be removed from respective collections
					addedOlis.remove(i);
					i--;
					OpportunityLineItem deletedItem = mapAttributeToCeasedOli.remove(ceasedAttribute.Id);
					System.debug(LoggingLevel.DEBUG, 'REMOVED ITEM:  ' + JSON.serializePretty(deletedItem));
				}
			}
		}

		System.debug(LoggingLevel.DEBUG, 'FINAL STATE ' + JSON.serializePretty(mapAttributeToCeasedOli));
		System.debug(LoggingLevel.DEBUG, 'mapAttributeToCeasedOli ' + JSON.serializePretty(mapAttributeToCeasedOli));
		System.debug(LoggingLevel.DEBUG, 'addedOlis ' + JSON.serializePretty(addedOlis));
	}

	public class ReplacedConfigurationDetail {
		/*
		 * @description Id of the OLD/REPLACED configuration
		 */
		public Id replacedConfigurationId;
		/*
		 * @description attribute (with all the necessary fields) on the NEW/REPLACING configuration
		 */
		public cscfga__Attribute__c replacingConfigurationAttribute;

		public ReplacedConfigurationDetail(Id replacedConfigurationId, cscfga__Attribute__c replacingConfigurationAttribute) {
			this.replacedConfigurationId = replacedConfigurationId;
			this.replacingConfigurationAttribute = replacingConfigurationAttribute;
		}
	}
}
