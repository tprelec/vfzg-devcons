@RestResource(urlMapping='/accountkeys/*')
global with sharing class AccountKeysWebService {
	@HttpPost
	global static AccountKeysWebServiceResponse runRequest() {
		String jsonString = RestContext.request.requestBody.toString();
		AccountKeysWebServiceResponse response = new AccountKeysWebServiceResponse();
		AccountKey body = new AccountKey(
			String.valueOf(
				((Map<String, Object>) JSON.deserializeUntyped(jsonString)).get('sourceSystem')
			),
			String.valueOf(((Map<String, Object>) JSON.deserializeUntyped(jsonString)).get('value'))
		);
		RestResponse res = new RestResponse();
		res.addHeader('Content-Type', 'application/json');

		try {
			AccountKeysService service = new AccountKeysService();
			List<AccountKey> accountKeys = service.getAccountKeys(body);
			if (accountKeys != null && accountKeys.size() > 0) {
				Boolean kvkNotExist = true;
				Boolean unifyNotExist = true;
				for (AccountKey accountKey : accountKeys) {
					if (accountKey.sourceSystem.toLowerCase() == 'kvk') {
						kvkNotExist = false;
					}
					if (accountKey.sourceSystem.toLowerCase() == 'unify') {
						unifyNotExist = false;
					}
				}
				if (kvkNotExist == true) {
					AccountKeysWebServiceError error = new AccountKeysWebServiceError(
						'KVK',
						'The requested resource is missing a key.',
						'MISSING_KEY'
					);
					response.errors.add(error);
				}

				if (unifyNotExist == true) {
					AccountKeysWebServiceError error = new AccountKeysWebServiceError(
						'Unify',
						'The requested resource is missing a key.',
						'MISSING_KEY'
					);
					response.errors.add(error);
				}
				response.accountKeys = accountKeys;
				res.responseBody = Blob.valueOf(JSON.serialize(response));
			} else {
				AccountKeysWebServiceError error = new AccountKeysWebServiceError(
					body.sourceSystem,
					'The requested resource matching system key of \'' +
					body.sourceSystem +
					' ' +
					body.value +
					'\' does not exist.',
					'NOT_FOUND'
				);
				response.errors.add(error);
				res.responseBody = Blob.valueOf(JSON.serialize(response));
			}
		} catch (Exception e) {
			res.statuscode = 400;
			res.responseBody = Blob.valueOf('errorResponse');
		}
		return response;
	}

	global class AccountKeysWebServiceResponse {
		global List<AccountKey> accountKeys;
		global List<AccountKeysWebServiceError> errors;

		global AccountKeysWebServiceResponse() {
			accountKeys = new List<AccountKey>();
			errors = new List<AccountKeysWebServiceError>();
		}
	}

	global class AccountKeysWebServiceError {
		global String message { get; set; }
		global String sourceSystem { get; set; }
		global String errorCode { get; set; }

		global AccountKeysWebServiceError(string sourceSystem, String message, String errorCode) {
			this.sourceSystem = sourceSystem;
			this.message = message;
			this.errorCode = errorCode;
		}
	}
}