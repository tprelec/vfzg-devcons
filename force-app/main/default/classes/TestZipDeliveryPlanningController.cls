@IsTest
private class TestZipDeliveryPlanningController {
    @isTest
    private static void confirmAppointment() {
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;

        Contact testContact = CS_DataTest.createContact(null, null, null, null, testAccount.Id);
        insert testContact;

        csord__Order__c testOrder = new csord__Order__c(csord__Identification__c = 'testOrder', csord__Account__c = testAccount.Id);
        insert testOrder;

        List<Site__c> lSites = TestComZipHelper.getSiteRecords(testAccount);
        insert lSites;

        List<COM_Delivery_Order__c> lDeliveryOrders = TestComZipHelper.getDeliveryOrders(testContact.Id, lSites, testOrder);
        insert lDeliveryOrders;

        List<COM_Delivery_Order__c> restrictedSolList = new List<COM_Delivery_Order__c>();
        restrictedSolList.add(lDeliveryOrders.get(0));
        List<Delivery_Appointment__c> lDeliveryAppointments = TestComZipHelper.getDeliveryAppointments(restrictedSolList);
        insert lDeliveryAppointments;

        Delivery_Appointment__c currAppt = lDeliveryAppointments.get(0);
        try {
            ZipDeliveryPlanningController.confirmAppointment(currAppt.Id);
        } catch (Exception e) {
            System.assert(false, e.getMessage());
        } finally {
            COM_Delivery_Order__c currSolConfirmed = [
                SELECT Id, Installation_Start__c, Technical_Contact__c, Confirmed_Installation__c, Installation_End__c
                FROM COM_Delivery_Order__c
                WHERE Id = :currAppt.Related_Delivery_Order__c
                LIMIT 1
            ];
            System.assertEquals(true, currSolConfirmed.Confirmed_Installation__c);
            System.assertEquals(currAppt.Start__c, currSolConfirmed.Installation_Start__c);
            System.assertEquals(currAppt.Contact__c, currSolConfirmed.Technical_Contact__c);
            Integer iMinutes = Integer.valueOf(currAppt.Duration__c);
            Datetime dtEnd = currAppt.Start__c.addMinutes(iMinutes);
            System.assertEquals(dtEnd, currSolConfirmed.Installation_End__c);
        }
    }

    @isTest
    private static void confirmAppointmentNullContact() {
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;

        csord__Order__c testOrder = new csord__Order__c(
            csord__Identification__c = 'testOrder',
            csord__Account__c = testAccount.Id
        );
        insert testOrder;

        List<Site__c> lSites = TestComZipHelper.getSiteRecords(testAccount);
        insert lSites;

        List<COM_Delivery_Order__c> lDeliveryOrders = TestComZipHelper.getDeliveryOrders(null, lSites, testOrder);
        insert lDeliveryOrders;

        List<COM_Delivery_Order__c> restrictedSolList = new List<COM_Delivery_Order__c>();
        restrictedSolList.add(lDeliveryOrders.get(0));
        List<Delivery_Appointment__c> lDeliveryAppointments = TestComZipHelper.getDeliveryAppointments(restrictedSolList);
        insert lDeliveryAppointments;

        Delivery_Appointment__c currAppt = lDeliveryAppointments.get(0);
        try {
            ZipDeliveryPlanningController.confirmAppointment(currAppt.Id);
        } catch (Exception e) {
            System.assert(false, e.getMessage());
        } finally {
            COM_Delivery_Order__c currSolConfirmed = [
                SELECT Id, Installation_Start__c, Technical_Contact__c, Confirmed_Installation__c, Installation_End__c
                FROM COM_Delivery_Order__c
                WHERE Id = :currAppt.Related_Delivery_Order__c
                LIMIT 1
            ];
            System.assertEquals(true, currSolConfirmed.Confirmed_Installation__c);
            System.assertEquals(currAppt.Start__c, currSolConfirmed.Installation_Start__c);
            System.assertEquals(currAppt.Contact__c, currSolConfirmed.Technical_Contact__c);
            Integer iMinutes = Integer.valueOf(currAppt.Duration__c);
            Datetime dtEnd = currAppt.Start__c.addMinutes(iMinutes);
            System.assertEquals(dtEnd, currSolConfirmed.Installation_End__c);
        }
    }

    @isTest
    private static void saveAppointment() {
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;
        csord__Order__c testOrder = new csord__Order__c(
            csord__Identification__c = 'testOrder',
            csord__Account__c = testAccount.Id
        );
        insert testOrder;
        List<Site__c> lSites = TestComZipHelper.getSiteRecords(testAccount);
        insert lSites;
        List<COM_Delivery_Order__c> lDeliveryOrders = TestComZipHelper.getDeliveryOrders(null, lSites, testOrder);
        insert lDeliveryOrders;
        Delivery_Appointment__c deliAppt;
        try {
            deliAppt = ZipDeliveryPlanningController.saveAppointment(
                '',
                'Test',
                Datetime.now(),
                120,
                'testLocation',
                null,
                'testDescription',
                Userinfo.getUserId(),
                lDeliveryOrders.get(0).Id,
                UserInfo.getUserId()
            );
        } catch (Exception e) {
            System.assert(false, e.getMessage());
        } finally {
            System.assertNotEquals(null, deliAppt.Id);
        }
    }

    @isTest
    private static void unconfirmAppointment() {
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;

        csord__Order__c testOrder = new csord__Order__c(
            csord__Identification__c = 'testOrder',
            csord__Account__c = testAccount.Id
        );
        insert testOrder;

        List<Site__c> lSites = TestComZipHelper.getSiteRecords(testAccount);
        insert lSites;

        List<COM_Delivery_Order__c> lDeliveryOrders = TestComZipHelper.getDeliveryOrders(null, lSites, testOrder);
        insert lDeliveryOrders;
        COM_Delivery_Order__c currSol = lDeliveryOrders.get(0);

        List<COM_Delivery_Order__c> restrictedSolList = new List<COM_Delivery_Order__c>();
        restrictedSolList.add(currSol);
        List<Delivery_Appointment__c> lDeliveryAppointments = TestComZipHelper.getDeliveryAppointments(restrictedSolList);
        insert lDeliveryAppointments;

        Delivery_Appointment__c currAppt = lDeliveryAppointments.get(0);
        currSol.Confirmed_Installation__c = true;
        currSol.Installation_Start__c = Datetime.now();
        update currSol;

        try {
            ZipDeliveryPlanningController.unconfirmAppointment(currAppt.Id);
        } catch (Exception e) {
            System.assert(false, e.getMessage());
        } finally {
            COM_Delivery_Order__c currSolConfirmed = [
                SELECT Id, Confirmed_Installation__c, Installation_Start__c,
                       Installation_End__c
                  FROM COM_Delivery_Order__c
                 WHERE Id = :currAppt.Related_Delivery_Order__c LIMIT 1
                ];
            System.assertEquals(false, currSolConfirmed.Confirmed_Installation__c);
            System.assertEquals(null, currSolConfirmed.Installation_Start__c);
            System.assertEquals(null, currSolConfirmed.Installation_End__c);
        }
    }

    @isTest
    private static void deleteAppointment() {
        Integer durationInMins = 120;
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;
        csord__Order__c testOrder = new csord__Order__c(
            csord__Identification__c = 'testOrder',
            csord__Account__c = testAccount.Id
        );
        insert testOrder;
        List<Site__c> lSites = TestComZipHelper.getSiteRecords(testAccount);
        insert lSites;
        List<COM_Delivery_Order__c> lDeliveryOrders = TestComZipHelper.getDeliveryOrders(null, lSites, testOrder);
        insert lDeliveryOrders;
        Delivery_Appointment__c deliAppt;
        Datetime today = Datetime.now();
        try {
            deliAppt = ZipDeliveryPlanningController.saveAppointment(
                '',
                'Test',
                today,
                durationInMins,
                'testLocation',
                null,
                'testDescription',
                Userinfo.getUserId(),
                lDeliveryOrders.get(0).Id,
                UserInfo.getUserId()
            );
            ZipDeliveryPlanningController.deleteAppointment(deliAppt.Id);

        } catch (Exception e) {
            System.assert(false, e.getMessage());

        } finally {
            List<Delivery_Appointment__c> lResults = [
                SELECT Id
                FROM Delivery_Appointment__c
                WHERE Id = :deliAppt.Id
                LIMIT 1
            ];
            if (lResults.size() > 0) {
                System.assert(false, 'There is no deletion on Delivery_Appointment__c');
            } else {
                System.assertEquals(0, lResults.size(), 'There was no deletion.');
            }
        }
    }

    @isTest
    private static void getAppointments() {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs(simpleUser){
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            csord__Order__c testOrder = new csord__Order__c(csord__Identification__c = 'testOrder', csord__Account__c = testAccount.Id);
            insert testOrder;

            List<Site__c> lSites = TestComZipHelper.getSiteRecords(testAccount);
            insert lSites;

            List<COM_Delivery_Order__c> lDeliveryOrders = TestComZipHelper.getDeliveryOrders(null, lSites, testOrder);
            insert lDeliveryOrders;

            List<Delivery_Appointment__c> lDeliveryAppointments = TestComZipHelper.getDeliveryAppointments(lDeliveryOrders);
            insert lDeliveryAppointments;

            Test.startTest();
            List<Delivery_Appointment__c> lResults = ZipDeliveryPlanningController.getAppointments(
                simpleUser.Id,
                simpleUser.Id,
                TestComZipHelper.lAppointmentDates.get(0),
                TestComZipHelper.lAppointmentDates.get(TestComZipHelper.lAppointmentDates.size() - 1)
            );
            Test.stopTest();
            System.assertEquals(lResults.size(), lDeliveryAppointments.size());
        }
    }

    @isTest
    private static void currentGetUserPermissionsWrite() {
        String deservedPermission = ZipDeliveryPlanningController.WRITE_PERMISSION;
        String resultUserPermission;
        List<Profile> profList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null LIMIT 1];
        User testUser = CS_DataTest.createUser(profList, roleList);
        insert testUser;

        PermissionSet ps = [SELECT Id FROM PermissionSet WHERE Name = 'Delivery_Appointments_Administration'];
        insert new PermissionSetAssignment(AssigneeId = testUser.id, PermissionSetId = ps.Id);

        System.runAs(testUser){
            resultUserPermission = ZipDeliveryPlanningController.getCurrentUserPermission();
            System.debug(resultUserPermission);
        }
        Boolean assertion = deservedPermission.contains(resultUserPermission);
        System.assert(assertion);
    }

    @isTest
    private static void getSettingsFromBackend() {
        List<String> lSettings = new List<String>();
        for(COM_Delivery_Planning_Setting__mdt	cmtSetting: [
            SELECT DeveloperName, Value__c
            FROM COM_Delivery_Planning_Setting__mdt
        ]) {
            lSettings.add(cmtSetting.DeveloperName);
        }
        try {
            Map<String, String> mpResult = ZipDeliveryPlanningController.getSettings();
            for(String sStn: lSettings) {
                if (sStn != ZipDeliveryPlanningController.KEY_REMINDER_DAYS_BEFORE && sStn != ZipDeliveryPlanningController.KEY_REMINDER_HOUR
                ) {
                    System.assertEquals(true, mpResult.containsKey(sStn));
                }
            }
        } catch (Exception e) {
            System.assert(false, e.getMessage());
        }
    }

    @isTest
    private static void getRegionsFromBackend() {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null LIMIT 1];
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        List<COM_Region_Setting__mdt> lSettings = [SELECT DeveloperName, Queue_API_Name__c FROM COM_Region_Setting__mdt];

        String sFirstQueueName = lSettings[0].Queue_API_Name__c;
        Group queueRecord = [SELECT Id FROM Group WHERE DeveloperName = :sFirstQueueName AND Type = 'Queue' LIMIT 1];
        GroupMember member = new GroupMember();
        member.UserOrGroupId = simpleUser.Id;
        member.GroupId = queueRecord.Id;
        insert member;

        List<Group> lRegions = new List<Group>();
        System.runAs(simpleUser){
            try {
                lRegions = ZipDeliveryPlanningController.getRegions();
            } catch(Exception e) {
                System.assert(false, e.getMessage());
            } finally {
                System.assertEquals(1, lRegions.size());
            }
        }
    }

    @isTest
    private static void getUsersByRegion() {
        COM_Delivery_Planning_Setting__mdt sProfileName = [
            SELECT Value__c
            FROM COM_Delivery_Planning_Setting__mdt
            WHERE DeveloperName = :ZipDeliveryPlanningController.FIELD_ENGINEER_PROF_NAME
        ];
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = :sProfileName.Value__c LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null LIMIT 1];
        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        Id userId = simpleUser.Id;
        Id iRegionQueueId;
        List<String> lsQueueNames = new List<String>();
        for(COM_Region_Setting__mdt sQueueName:[SELECT Queue_API_Name__c FROM COM_Region_Setting__mdt LIMIT 1]) {
            lsQueueNames.add(sQueueName.Queue_API_Name__c);
        }
        for(Group grp:[SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName IN :lsQueueNames LIMIT 1]) {
            iRegionQueueId = grp.Id;
            GroupMember member = new GroupMember();
            member.UserOrGroupId = userId;
            member.GroupId = iRegionQueueId;
            insert member;
        }
        List<User> lResUsers = ZipDeliveryPlanningController.getUsersByRegion(iRegionQueueId);
        Boolean currUserExists = false;
        for(User currUser: lResUsers) {
            if(currUser.Id == userId) {
                currUserExists = true;
            }
        }
        System.assertEquals(true, currUserExists);
    }

    @isTest
    private static void getUnplannedDeliveryOrders() {
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;

        csord__Order__c testOrder = new csord__Order__c(
            csord__Identification__c = 'testOrder',
            csord__Account__c = testAccount.Id
        );
        insert testOrder;

        List<COM_Delivery_Order__c> lResults = ZipDeliveryPlanningController.getUnplannedDeliveryOrders(UserInfo.getUserId());
        System.assertEquals(lResults.size(), 0);
    }

    @isTest
    private static void moveAppointment() {
        Integer durationInMins = 120;
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;
        csord__Order__c testOrder = new csord__Order__c(
            csord__Identification__c = 'testOrder',
            csord__Account__c = testAccount.Id
        );
        insert testOrder;
        List<Site__c> lSites = TestComZipHelper.getSiteRecords(testAccount);
        insert lSites;
        List<COM_Delivery_Order__c> lDeliveryOrders = TestComZipHelper.getDeliveryOrders(null, lSites, testOrder);
        insert lDeliveryOrders;
        Delivery_Appointment__c deliAppt;
        Datetime today = Datetime.now();
        Datetime tomorrow = today.addDays(1);
        try {
            deliAppt = ZipDeliveryPlanningController.saveAppointment(
                '',
                'Test',
                today,
                durationInMins,
                'testLocation',
                null,
                'testDescription',
                Userinfo.getUserId(),
                lDeliveryOrders.get(0).Id,
                UserInfo.getUserId()
            );
            Datetime tomorrow2 = tomorrow.addMinutes(durationInMins);
            ZipDeliveryPlanningController.moveAppointment(deliAppt.Id, tomorrow, tomorrow2);

        } catch (Exception e) {
            System.assert(false, e.getMessage());
        } finally {
            List<Delivery_Appointment__c> lResults = [
                SELECT Start__c, Duration__c
                FROM Delivery_Appointment__c
                WHERE Id = :deliAppt.Id
                LIMIT 1
            ];
            if(lResults.size() > 0) {
                Delivery_Appointment__c result = lResults.get(0);
                System.assertEquals(
                    durationInMins,
                    result.Duration__c,
                    'The duration is not what is expected ' + String.valueOf(durationInMins)
                );
                System.assertEquals(tomorrow, result.Start__c, 'The start is not what is expected ' + String.valueOf(durationInMins));
            } else {
                System.assert(false, 'There was no records on Delivery_Appointment__c');
            }
        }
    }
}