@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class COM_CustomStepCDOCallout implements CSPOFA.ExecutionHandler {
	public List<SObject> process(List<sObject> steps) {
		List<SObject> result = new List<sObject>();
		List<CSPOFA__Orchestration_Step__c> stepList = CS_OrchestratorStepUtility.getStepList(steps);

		try {
			calloutToCDO(stepList);

			for (CSPOFA__Orchestration_Step__c step : stepList) {
				result.add(CS_OrchestratorStepUtility.setStepToComplete(step));
			}
		} catch (Exception ex) {
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				result.add(CS_OrchestratorStepUtility.setStepToError(step, ex));
			}
		}

		return result;
	}

	@TestVisible
	private static void calloutToCDO(List<CSPOFA__Orchestration_Step__c> stepList) {
		Map<Id, String> processNameByDeliveryOrderId = new Map<Id, String>();
		for (CSPOFA__Orchestration_Step__c step : stepList) {
			processNameByDeliveryOrderId.put(step.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c, step.CSPOFA__Orchestration_Process__r.Name);
		}

		Map<Id, COM_Delivery_Order__c> deliveryOrders = new Map<Id, COM_Delivery_Order__c>(
			[SELECT Id, Account__c FROM COM_Delivery_Order__c WHERE Id IN :processNameByDeliveryOrderId.keySet()]
		);

		for (Id deliveryOrderId : processNameByDeliveryOrderId.keySet()) {
			String processName = processNameByDeliveryOrderId.get(deliveryOrderId);

			if (processName == COM_Constants.CDO_PROVISION_PROCESS_NAME) {
				COM_CDO_ProvisionService.provisionServices(deliveryOrderId);
			} else if (processName == COM_Constants.CDO_DEPROVISION_PROCESS_NAME) {
				COM_CDO_DeleteServices.deleteServices(deliveryOrderId);
			} else if (processName == COM_Constants.CDO_CREATE_TENANT_PROCESS_NAME) {
				COM_Delivery_Order__c deliveryOrder = deliveryOrders.get(deliveryOrderId);
				COM_CDOcreateTenant.createTenantService(deliveryOrder.Account__c);
			}
		}
	}
}
