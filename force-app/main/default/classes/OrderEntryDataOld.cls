public with sharing class OrderEntryDataOld {
	@AuraEnabled
	public Id accountId { get; set; }

	@AuraEnabled
	public Id opportunityId { get; set; }

	@AuraEnabled
	public Id siteId { get; set; }

	@AuraEnabled
	public Id primaryContactId { get; set; }

	@AuraEnabled
	public String lastStep { get; set; }

	@AuraEnabled
	public String contractTerm { get; set; }

	@AuraEnabled
	public List<OrderEntryProduct> products { get; set; }

	@AuraEnabled
	public List<OrderEntryAddon> addons { get; set; }

	@AuraEnabled
	public OrderEntryBundle bundle { get; set; }

	@AuraEnabled
	public SiteCheck siteCheck { get; set; }

	@AuraEnabled
	public String addressCheckResult { get; set; }

	@AuraEnabled
	public String offNetType { get; set; }

	@AuraEnabled
	public Boolean b2cInternetCustomer { get; set; }

	@AuraEnabled
	public Map<String, Telephony> telephony { get; set; }

	@AuraEnabled
	public List<Promotion> promos { get; set; }

	@AuraEnabled
	public Payment payment { get; set; }

	@AuraEnabled
	public Installation installation { get; set; }

	@AuraEnabled
	public OperatorSwitch operatorSwitch { get; set; }

	@AuraEnabled
	public String notes { get; set; }

	public void init() {
	}

	public class OrderEntryProduct {
		@AuraEnabled
		public Id id { get; set; }

		@AuraEnabled
		public String type { get; set; }
	}

	public class OrderEntryAddon {
		@AuraEnabled
		public Id id { get; set; }

		@AuraEnabled
		public String type { get; set; }

		@AuraEnabled
		public String name { get; set; }

		@AuraEnabled
		public String code { get; set; }

		@AuraEnabled
		public Integer quantity { get; set; }

		@AuraEnabled
		public Decimal recurring { get; set; }

		@AuraEnabled
		public Decimal oneOff { get; set; }

		@AuraEnabled
		public Decimal totalRecurring { get; set; }

		@AuraEnabled
		public Decimal totalOneOff { get; set; }

		@AuraEnabled
		public Id parentProduct { get; set; }

		@AuraEnabled
		public String bundleName { get; set; }

		@AuraEnabled
		public string parentType { get; set; }
	}

	public class OrderEntryBundle {
		@AuraEnabled
		public Id id { get; set; }

		@AuraEnabled
		public String type { get; set; }

		@AuraEnabled
		public String name { get; set; }

		@AuraEnabled
		public String code { get; set; }

		@AuraEnabled
		public Integer quantity { get; set; }

		@AuraEnabled
		public Decimal recurring { get; set; }

		@AuraEnabled
		public Decimal oneOff { get; set; }

		@AuraEnabled
		public Decimal totalRecurring { get; set; }

		@AuraEnabled
		public Decimal totalOneOff { get; set; }

		@AuraEnabled
		public Id parentProduct { get; set; }

		@AuraEnabled
		public String bundleName { get; set; }

		@AuraEnabled
		public String promoDescription { get; set; }
	}

	public class SiteCheck {
		@AuraEnabled
		public String street { get; set; }

		@AuraEnabled
		public String houseNumber { get; set; }

		@AuraEnabled
		public String houseNumberExt { get; set; }

		@AuraEnabled
		public String zipCode { get; set; }

		@AuraEnabled
		public String city { get; set; }

		@AuraEnabled
		public String[] status { get; set; }

		@AuraEnabled
		public List<Availability> availability { get; set; }

		@AuraEnabled
		public String footprint { get; set; }
	}

	public class Availability {
		@AuraEnabled
		public String name { get; set; }

		@AuraEnabled
		public Boolean available { get; set; }
	}

	public class Telephony {
		@AuraEnabled
		public Boolean enabled { get; set; }

		@AuraEnabled
		public Boolean portingEnabled { get; set; }

		@AuraEnabled
		public String portingNumber { get; set; }
	}

	public class Promotion {
		@AuraEnabled
		public Id id { get; set; }

		@AuraEnabled
		public String name { get; set; }

		@AuraEnabled
		public String type { get; set; }

		@AuraEnabled
		public String contractTerms { get; set; }

		@AuraEnabled
		public String customerType { get; set; }

		@AuraEnabled
		public String connectionType { get; set; }

		@AuraEnabled
		public String offNetType { get; set; }

		@AuraEnabled
		public List<Discount> discounts { get; set; }
	}

	public class Discount {
		@AuraEnabled
		public Id id { get; set; }

		@AuraEnabled
		public String name { get; set; }

		@AuraEnabled
		public String type { get; set; }

		@AuraEnabled
		public Decimal value { get; set; }

		@AuraEnabled
		public Decimal duration { get; set; }

		@AuraEnabled
		public String level { get; set; }

		@AuraEnabled
		public Id product { get; set; }

		@AuraEnabled
		public Id addon { get; set; }
	}

	public class Payment {
		@AuraEnabled
		public String bankAccountHolder { get; set; }

		@AuraEnabled
		public String paymentType { get; set; }

		@AuraEnabled
		public String iban { get; set; }

		@AuraEnabled
		public String billingChannel { get; set; }
	}

	public class Installation {
		@AuraEnabled
		public Date earliestInstallationDate { get; set; }

		@AuraEnabled
		public Boolean assignTechnician { get; set; }

		@AuraEnabled
		public PreferredDate preferredDate1 { get; set; }

		@AuraEnabled
		public PreferredDate preferredDate2 { get; set; }

		@AuraEnabled
		public PreferredDate preferredDate3 { get; set; }
	}

	public class PreferredDate {
		@AuraEnabled
		public Date selectedDate { get; set; }

		@AuraEnabled
		public String dayPeriod { get; set; }
	}

	public class OperatorSwitch {
		@AuraEnabled
		public Boolean requested { get; set; }

		@AuraEnabled
		public String currentProvider { get; set; }

		@AuraEnabled
		public String currentContractNumber { get; set; }

		@AuraEnabled
		public Boolean potentialFeeAccepted { get; set; }
	}
}
