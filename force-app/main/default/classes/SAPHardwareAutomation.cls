// Contains logic to copy data from SAP into Salesforce using an excel export to insert SAP_Hardware_Information__c records.
public class SAPHardwareAutomation implements Database.Batchable <sObject>, Database.Stateful, Schedulable {
    // The following variables are stateful in order to prevent problems, should the data they retrieve be changed during a batch job execution.
    private static final Map<String, String> sapToVFProductFieldMapping = SyncUtil.fullMapping('SAP_Hardware_Information__c -> VF_Product__c').get('SAP_Hardware_Information__c -> VF_Product__c');
    private static final Map<String, String> sapToCommercialProductFieldMapping = SyncUtil.fullMapping('SAP_Hardware_Information__c -> cspmb__Price_Item__c').get('SAP_Hardware_Information__c -> cspmb__Price_Item__c');
    private static final Map<String, Field_Sync_Mapping__c> sapFieldToVFProductFieldMappingRecord = SyncUtil.fullMappingPlus('SAP_Hardware_Information__c -> VF_Product__c').get('SAP_Hardware_Information__c -> VF_Product__c');
    private static final Map<String, Field_Sync_Mapping__c> sapFieldToCommercialProductFieldMappingRecord = SyncUtil.fullMappingPlus('SAP_Hardware_Information__c -> cspmb__Price_Item__c').get('SAP_Hardware_Information__c -> cspmb__Price_Item__c');
    private static final Id commercialProductRecordTypeIdMobile = [SELECT Id FROM RecordType WHERE SObjectType = 'cspmb__Price_Item__c' AND DeveloperName = 'Mobile' LIMIT 1].Id;
    @testVisible
    private static Id orderTypeMobile = getOrderTypeMobileId();

    public Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id, Status__c, Item_Number_Vanilla__c, Item_Number_VF__c, ';
        Set<String> fieldsToQuery = new Set<String>();
        fieldsToQuery.addAll(sapToVFProductFieldMapping.values());
        fieldsToQuery.addAll(sapToCommercialProductFieldMapping.values());
        fieldsToQuery.remove(null);
        query += String.join(new List<String>(fieldsToQuery), ',');
        query += ' FROM SAP_Hardware_Information__c WHERE Processed__c = false';

        if (Test.isRunningTest()) { query += ' Limit 50'; }
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<SAP_Hardware_Information__c> scope) {
        List<VF_Product__c> vfProductsToUpsert = new List<VF_Product__c>();
        List<cspmb__Price_Item__c> commercialProductsToUpsert = new List<cspmb__Price_Item__c>();

        try {
            for (SAP_Hardware_Information__c sap : scope) {
                vfProductsToUpsert.addAll(getVFProduct(sap));
                commercialProductsToUpsert.addAll(getCommercialProduct(sap));
                sap.Processed__c = true;
            }
            upsert vfProductsToUpsert ProductCode__c;
            upsert commercialProductsToUpsert cspmb__One_Off_Charge_External_Id__c;
            update scope;
        } catch (exception e) {
            throw new SAPHardwareAutomationException(e.getmessage() + e.getStackTraceString());
        }
    }

    public void execute(SchedulableContext context) {
        Database.executeBatch(this);
    }

    private List<VF_Product__c> getVFProduct(SAP_Hardware_Information__c sap) {
        List<VF_Product__c> ret = new List<VF_Product__c>();

        VF_Product__c product = (VF_Product__c) SyncUtil.createSObjectWithDefaults(sap, sapFieldToVFProductFieldMappingRecord, new VF_Product__c());
        product.ProductCode__c = sap.Item_Number_Vanilla__c != null ? sap.Item_Number_Vanilla__c : sap.Item_Number_VF__c;
        product.Active__c = sap.Status__c == 'Active';
        if (product.OrderType__c == null && orderTypeMobile != null) { product.OrderType__c = orderTypeMobile; }

        // If both item numbers are provided, create 2 products
        if (sap.Item_Number_Vanilla__c != null && sap.Item_Number_VF__c != null) {
            VF_Product__c anotherProduct = product.clone(false, true, false, false);
            product.ProductCode__c = sap.Item_Number_Vanilla__c;
            product.Name += ' Vanilla';
            anotherProduct.ProductCode__c = sap.Item_Number_VF__c;
            anotherProduct.Name += ' VF';
            ret.add(anotherProduct);
        }
        if (String.isBlank(product.ProductCode__c)) {
            throw new SAPHardwareAutomationException('Both the Item_Number_Vanilla__c and Item_Number_VF__c are missing on the SAP_Hardware_Information__c record');
        }

        ret.add(product);
        return ret;
    }

    private List<cspmb__Price_Item__c> getCommercialProduct(SAP_Hardware_Information__c sap) {
        List<cspmb__Price_Item__c> ret = new List<cspmb__Price_Item__c>();

        cspmb__Price_Item__c product = (cspmb__Price_Item__c) SyncUtil.createSObjectWithDefaults(sap, sapFieldToCommercialProductFieldMappingRecord, new cspmb__Price_Item__c());
        product.RecordTypeId = commercialProductRecordTypeIdMobile;
        product.cspmb__Price_Item_Code__c = sap.Item_Number_Vanilla__c != null ? sap.Item_Number_Vanilla__c : sap.Item_Number_VF__c;
        if (String.isBlank(product.cspmb__Price_Item_Code__c)) {
            throw new SAPHardwareAutomationException('Both the Item_Number_Vanilla__c and Item_Number_VF__c are missing on the SAP_Hardware_Information__c record');
        }
        product.cspmb__One_Off_Charge_Code__c = product.cspmb__Price_Item_Code__c;
        product.cspmb__One_Off_Charge_External_Id__c = product.cspmb__Price_Item_Code__c;
        product.cspmb__Is_Active__c = sap.Status__c == 'Active';

        // If both item numbers are provided, create 2 products
        if (sap.Item_Number_Vanilla__c != null && sap.Item_Number_VF__c != null) {
            cspmb__Price_Item__c anotherProduct = product.clone(false, true, false, false);
            product.Name += ' Vanilla';
            anotherProduct.Name += ' VF';
            anotherProduct.cspmb__Price_Item_Code__c = sap.Item_Number_VF__c;
            anotherProduct.cspmb__One_Off_Charge_Code__c = sap.Item_Number_VF__c;
            anotherProduct.cspmb__One_Off_Charge_External_Id__c = sap.Item_Number_VF__c;
            ret.add(anotherProduct);
        }
        ret.add(product);
        return ret;
    }

    public void finish(Database.BatchableContext BC) {}

    private static Id getOrderTypeMobileId() {
        List<OrderType__c> orderType = [SELECT Id FROM OrderType__c WHERE Name = 'Mobile' LIMIT 1];
        if (orderType.size() == 1) { return orderType[0].Id; }
        return null;
    }

    public class SAPHardwareAutomationException extends Exception {}
}