public with sharing class CS_Constants {
	public static final Map<String, String> DEAL_TYPES = new Map<String, String>{
		'ACQUISITION' => 'Acquisition',
		'RETENTION' => 'Retention',
		'MIGRATION' => 'Migration',
		'PORTING' => 'Porting',
		'DISCONNECT' => 'Disconnect'
	};

	// Group__c field on Commercial Products used to differentiate between BMS products.
	public static final String LOOKOUT_GROUP = 'Lookout_VFBM';
	public static final String MANAGED_SERVICE_1_GROUP = 'Managed Service 1';
	public static final String MANAGED_SERVICE_2_GROUP = 'Managed Service 2';
	public static final String MANAGED_SERVICE_EXPRESS_GROUP = 'Managed Service Express';
	public static final String MANAGED_SERVICE_OPTIE_GROUP = 'Managed Service Optie';
	public static final String MICROSOFT_EMS_INTUNE_GROUP = 'Microsoft EMS-Intune';
	public static final String MOBILE_IRON_GROUP = 'MobileIron_VFBM';
	public static final String PROFESSIONAL_SERVICES_GROUP = 'Professional Services';
	public static final String SERVICE_MANAGEMENT_GROUP = 'Service Management';
	public static final String TREND_MICRO_GROUP = 'Trend Micro';
	public static final String VERVANGENDE_TOESTEL_SERVICE_GROUP = 'Vervangende Toestel Service';
	public static final String VSDM_GROUP = 'VSDM_VFBM';
	public static final String WANDERA_GROUP = 'Wandera';
	public static final String CISCO_UMBRELLA_GROUP = 'Cisco Umbrella';
	public static final String CYBER_EXPOSURE_DIAGNOSTIC_GROUP = 'Cyber Exposure Diagnostic';
	public static final String PHISHING_AWARENESS_GROUP = 'Phishing Awareness';
	public static final String VULNERABILITY_ASSESSMENT_GROUP = 'Vulnerability Assessment';
	public static final String PENETRATION_TESTING_GROUP = 'Penetration Testing';
	public static final String MANAGED_DETECTION_RESPONSE_GROUP = 'Managed Detection & Response';
	public static final String BRECH_RESPONSE_FORENSICS_GROUP = 'Breach Response & Forensics';
	public static final String MICROSOFT_BUSINESS_MARKETPLACE_GROUP = 'Microsoft_BM';
	public static final String TREND_MICRO_MARKETPLACE_GROUP = 'TrendMicro_BM';
	public static final String IVANTI_MOBILEIRON_MARKETPLACE_GROUP = 'Ivanti / MobileIron_BM';
	public static final String VODAFONE_SECURE_DEVICE_MANAGER_MARKETPLACE_GROUP = 'VSDM_BM';
	public static final String LOOKOUT_MARKETPLACE_GROUP = 'Lookout_BM';
	public static final String VODAFONE_CLOUD_BACKUP_MARKETPLACE_GROUP = 'Skykick_BM';

	//new RedPro scenarios
	public static final String REDPRO_SCENARIO_NEW_PORTING = 'New-Porting RedPro';
	public static final String REDPRO_SCENARIO_RETENTION_MIGRATION = 'Retention-Migration RedPro';

	// One Net Harmonization
	public static final String ACCESS_INFRASTRUCTURE = 'Access Infrastructure';
	public static final String MANAGED_INTERNET = 'Managed Internet';
	public static final List<String> ONE_NET_SEAT_LICENCES = new List<String>{ 'ONE Combi 2', 'One Flex', 'One Mobiel', 'One Vast' };
	public static final List<String> TRIGGER_LICENCE_NAMES = new List<String>{ 'One Call Centre Premium Licentie', 'One Integrate Premium' };
	public static final String ONSITE_PROJECT_MANAGEMENT = 'Onsite of telefonische project management ondersteuning';
	public static final String ONE_NET_ENTERPRISE_BASIS_PM = 'One Net Enterprise Basis Projectmanagement';
	public static final String ONE_NET_FLEX_PRODUCT_IN_BASKET = '[ONE NET (Flex Only)]';
	public static final String ONE_NET_PRODUCT_IN_BASKET = '[One Net]';
	public static final String ONE_CALL_CENTRE_LICENTIE = 'One Call Centre Premium Licentie';
	public static final String IVR_KEUZEMENU = 'IVR / Keuzemenu';
	public static final String ONE_INTEGRATE_PREMIUM = 'One Integrate Premium';
	public static final String ONE_NET_BASIS_PROJECTCOORDINATIE = 'One Net Basis Projectcoördinatie';
	public static final String ONE_NET_STANDAARD_PM = 'One Net Standaard Projectmanagement';
	public static final String ONE_NET_ENTERPRISE = 'One Net Enterprise';
	public static final String LICENCE_ONE_MOBIEL = 'One Mobiel';
	public static final String VODAFONE_CALLING = 'Vodafone Calling';

	// Business Mobile
	public static final List<String> BM_ABBONNEMENTEN = new List<String>{
		'RCBOBM10GBSO',
		'RCBOBM25GBSO',
		'RCBOBMUNLSO',
		'RCBOBMDOSO10G',
		'RCBOBMDOSO25G',
		'RCBOBMDOSOUNL',
		'RCBOBM10GB1MND',
		'RCBOBM25GB1MND',
		'RCBOBMDO1MNDUN',
		'RCBOBMDO1M10G',
		'RCBOBMDO1M25G',
		'RCBOBMUNL1MND',
		'RCBOBM10GBBE',
		'RCBOBM25GBBE',
		'RCBOBMUNLBE',
		'RCBOBMDOBE10G',
		'RCBOBMDOBE25G',
		'RCBOBMDOBEUNL',
		'RCBOBM10GBREG',
		'RCBOBM25GBREG',
		'RCBOBMUNLREG',
		'RCBOBMDORE10G',
		'RCBOBMDORE25G',
		'RCBOBMDOREGUN'
	};

	//Mobile CTN/BAN Profile, Customer Services validations
	public static final String MOBILE_CTN_PROFILE_IN_BASKET = '[Mobile CTN profile]';
	public static final String MOBILE_BAN_PROFILE_IN_BASKET = '[Mobile BAN profile]';
	public static final String CUSTOMER_SERVICES_IN_BASKET = '[Customer Services]';

	// GDSP Validation
	public static final String GDSP_IN_BASKET = '[GDSP]';

	// theUndersignedClauseGenerator
	public static final String PRIVATE_COMPANY_NL = 'De besloten ';
	public static final String PUBLIC_COMPANY_NL = 'De naamloze ';
	public static final String ITS_NL = ' haar ';
	public static final String AND_NL = ', en';
	public static final String PRIVATE_COMPANY_EN = 'The private limited company ';
	public static final String PUBLIC_COMPANY_EN = 'The public limited company ';
	public static final String ITS_EN = ' its ';
	public static final String AND_EN = ', and';
	public static final String SALUTATION_EN_MR = 'Mr. ';
	public static final String SALUTATION_EN_MRS = 'Mrs. ';
	public static final String SALUTATION_NL_MR = 'de heer ';
	public static final String SALUTATION_NL_MRS = 'mevrouw ';
}
