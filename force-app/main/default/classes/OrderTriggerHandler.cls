public with sharing class OrderTriggerHandler extends TriggerHandler {
	private static final List<String> VALID_LOWERCASE_SEGMENTS_FOR_CTN_READY = new List<String>{ 'soho', 'sme ids' };
	public Map<Id, VF_Contract__c> contractMap = new Map<Id, VF_Contract__c>();

	public override void beforeInsert() {
		List<Order__c> newOrders = (List<Order__c>) this.newList;
		setAccountLookup(newOrders, null);
		setUnifyPropositionType(newOrders);
		setExportSystem(newOrders, null);
		setMainContact(newOrders);
		setSAGandSolutionSalesContact(newOrders);
		setFinancialInformationAndBillingArrangement(newOrders);
		setIsPartner(newOrders);
		setOrderStatus(newOrders, null);
		setCTNReadyForValidSegments(newOrders);
	}

	public override void afterInsert() {
		List<Order__c> newOrders = (List<Order__c>) this.newList;
		copyPropositionToAccount(newOrders, null);
	}

	public override void beforeUpdate() {
		List<Order__c> newOrders = (List<Order__c>) this.newList;
		Map<Id, Order__c> oldOrdersMap = (Map<Id, Order__c>) this.oldMap;
		Map<Id, Order__c> newOrderMap = (Map<Id, Order__c>) this.newMap;
		setAccountLookup(newOrders, oldOrdersMap);
		setUnifyPropositionType(newOrders);
		setExportSystem(newOrders, oldOrdersMap);
		updateBOPProductFields(newOrders, oldOrdersMap);
		setTitleToBOP(newOrderMap);
		setOrderStatus(newOrders, oldOrdersMap);
	}

	public override void afterUpdate() {
		List<Order__c> newOrders = (List<Order__c>) this.newList;
		Map<Id, Order__c> newOrderMap = (Map<Id, Order__c>) this.newMap;
		Map<Id, Order__c> oldOrdersMap = (Map<Id, Order__c>) this.oldMap;
		copyStatusToContract(newOrders, oldOrdersMap);
		updateImplementationReadyOnContract(newOrders, oldOrdersMap);
		// commenting recursion check as it does not work when order form is submitted, form must be updating the order before changing status to submitting.
		// I only want this code running once
		BillingLogicHelper.createAssets(newOrders, oldOrdersMap);
		if (!TriggerHandler.isAlreadyModified()) {
			TriggerHandler.setAlreadyModified();
			triggerOrderPabx(newOrders, oldOrdersMap);
		}
		triggerOrderExport(newOrders, oldOrdersMap);
		copyPropositionToAccount(newOrders, oldOrdersMap);
		copyFixedOrderDateToBAN(newOrders, oldOrdersMap);
		emitOrchestratorUpdateEvent(newOrderMap, oldOrdersMap);
	}

	public override void beforeDelete() {
		List<Order__c> oldOrders = (List<Order__c>) this.oldList;
		Map<Id, Order__c> oldOrdersMap = (Map<Id, Order__c>) this.oldMap;
		preventSyncedOrderDelete(oldOrders);
		copyPropositionToAccount(null, oldOrdersMap);
	}

	// Sets the initial order status
	private void setOrderStatus(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		Map<Id, OrderType__c> orderTypeMap = GeneralUtils.orderTypeMap;
		Set<String> salesProfiles = new Set<String>{ Constants.PROFILE_INDIRECT_SALES, Constants.PROFILE_DIRECT_SALES };

		for (Order__c order : newOrders) {
			if (oldOrdersMap != null) {
				if (
					GeneralUtils.isRecordFieldChanged(order, oldOrdersMap, 'Status__c') &&
					oldOrdersMap.get(order.Id).Status__c == 'Accepted' &&
					order.Block_cancellation__c
				) {
					order.Status__c = oldOrdersMap.get(order.Id).Status__c;
				}
				continue;
			}

			Ordertype__c ot = orderTypeMap.get(order.OrderType__c);

			// Set the initial order status based on the configuration in the ordertype__c object
			order.Status__c = ot.Status__c;
			Opportunity opp = order.VF_Contract__c != null && contractMap.containsKey(order.VF_Contract__c)
				? contractMap.get(order.VF_Contract__c).Opportunity__r
				: null;
			if (
				ot.Name == 'MAC' &&
				opp != null &&
				opp.RecordType.Name == 'Default' &&
				!salesProfiles.contains(GeneralUtils.currentUser.Profile.Name)
			) {
				order.Status__c = Constants.ORDER_STATUS_NEW;
			}
			if (order.Status__c == 'Assigned to Inside Sales') {
				// legacy order can be direcly assigned to SAG if it originates from a MAC opportunity
				order.Ready_for_Inside_Sales__c = true;
				order.Ready_for_Inside_Sales_Datetime__c = System.now();
			}
		}
	}

	// Fills the export system, based on some fields on the order
	private void setExportSystem(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		Map<ID, OrderType__c> orderTypeMap = GeneralUtils.orderTypeMap;
		for (Order__c o : newOrders) {
			// If status is 'accepted' or if Override Export Systems then don't change it anymore
			if (o.Status__c != 'Accepted' && !o.Override_Export_Systems__c) {
				// Set customer export system
				if (o.Customer_migrated_To_Unify__c) {
					o.Export_system_Customerdata__c = 'SIAS';
				} else {
					o.Export_system_Customerdata__c = 'BOP';
				}
				// Set order export system based on ordertype object settings
				if (o.OrderType__c != null) {
					OrderType__c orderType = orderTypeMap.get(o.ordertype__c);
					o.Export__c = orderType.ExportSystem__c;
					// When ordertype is Fixed/OneNet, orders normally are sent to SIAS
					// However when there are OneNet products on the order it should instead be sent to BOP
					if (orderType.Name == 'Fixed/OneNet' && !o.Is_Unify_Proposition__c) {
						o.Export__c = 'BOP';
					}
				} else {
					// This code can be removed once all open orders have an ordertype__c set
					// set order export system. For now only export fixed orders.
					if (o.Mobile_Fixed__c == 'Fixed') {
						if (o.Customer_migrated_To_Unify__c && o.Is_Unify_Proposition__c) {
							o.Export__c = 'SIAS';
						} else {
							o.Export__c = 'BOP';
						}
					} else {
						o.Export__c = null;
					}
				}
			}
		}
	}

	// Sets Contract status depending on the Order status.
	private void copyStatusToContract(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		// Check which Orders have changed Status, update Contract Status if there are some Orders that have changed Status
		List<Order__c> ordersChangedStatus = GeneralUtils.filterChangedRecords(newOrders, oldOrdersMap, 'Status__c');
		if (!ordersChangedStatus.isEmpty()) {
			List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
			Set<Id> contractIds = GeneralUtils.getIDSetFromList(ordersChangedStatus, 'VF_Contract__c');
			Map<Id, List<Order__c>> contractOrders = GeneralUtils.groupByIDField(
				[SELECT Id, VF_Contract__c, Contract_Ready__c, Status__c, RecordTypeId FROM Order__c WHERE VF_Contract__c = :contractIds],
				'VF_Contract__c'
			);

			for (Order__c o : ordersChangedStatus) {
				Boolean isValidStatus = true;

				// Check if this order's contract had more orders in it.
				// if yes check on status != accepted and/ or !='processed manually' for all the orders on the contract
				// if all status are set to accepted or processed manually then the contract status should be updated
				for (Order__c ox : contractOrders.get(o.VF_Contract__c)) {
					if (ox.Status__c != 'Accepted' && ox.Status__c != 'Processed manually') {
						isValidStatus = false;
					}
				}
				if (isValidStatus) {
					contractsToUpdate.add(new VF_Contract__c(Id = o.VF_Contract__c, Order_Status__c = o.Status__c));
				}
			}
			SharingUtils.updateRecordsWithoutSharing(contractsToUpdate);
		}
	}

	// Fills the Ready for Implementation checkbox on contract if all orders under the contract are 'contract ready'
	private void updateImplementationReadyOnContract(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		Set<Id> contractIdsToCheck = GeneralUtils.getIDSetFromList(
			GeneralUtils.filterChangedRecords(newOrders, oldOrdersMap, 'Contract_Ready__c'),
			'VF_Contract__c'
		);
		if (!contractIdsToCheck.isEmpty()) {
			List<VF_Contract__c> contractsToUpdate = new List<VF_Contract__c>();
			for (Order__c o : [SELECT Id, VF_Contract__c, Contract_Ready__c FROM Order__c WHERE VF_Contract__c IN :contractIdsToCheck]) {
				if (!o.Contract_Ready__c) {
					contractIdsToCheck.remove(o.VF_Contract__c);
				}
			}
			for (Id contractId : contractIdsToCheck) {
				contractsToUpdate.add(new VF_Contract__c(Id = contractId, Ready_for_Implementation__c = true));
			}
			SharingUtils.updateRecordsWithoutSharing(contractsToUpdate);
		}
	}

	// Fills the IsPartner field on Order
	private void setIsPartner(List<Order__c> newOrders) {
		for (Order__c o : newOrders) {
			o.IsPartner__c = o.Direct_Indirect__c == 'Indirect';
		}
	}

	// Fills the unify proposition type on Order.
	private void setUnifyPropositionType(List<Order__c> newOrders) {
		for (Order__c o : newOrders) {
			if (o.Mobile_Fixed__c != 'Mobile') {
				o.Unify_Proposition_Type__c = o.Propositions__c;
			} else {
				o.Unify_Proposition_Type__c = 'Mobile';
			}
		}
	}

	// Updates the BOP Project fields.
	private void updateBOPProductFields(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		for (Order__c o : newOrders) {
			if (GeneralUtils.isRecordFieldChanged(o, oldOrdersMap, 'BOP_Order_Status__c') && o.BOP_Order_Status__c == 'Project Created') {
				o.BOP_Project_Number__c = o.Sales_Order_Id__c;
				o.BOP_Start_Project_Date__c = System.now();
			}
		}
	}

	// Fills the account id lookup on Order.
	private void setAccountLookup(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		// Check if any Order has set/changed Contract
		List<Order__c> ordersChangedContract = GeneralUtils.filterChangedRecords(newOrders, oldOrdersMap, 'VF_Contract__c');
		if (ordersChangedContract.isEmpty()) {
			return;
		}
		// Get all Contracts. Query owner and owner's manager related to filtered VF contract
		Set<Id> contractIds = GeneralUtils.getIDSetFromList(ordersChangedContract, 'VF_Contract__c');
		Map<Id, VF_Contract__c> contractsMap = new Map<Id, VF_Contract__c>(
			[SELECT Id, Owner__c, Owner__r.ManagerId, Account__c FROM VF_Contract__c WHERE Id IN :contractIds]
		);
		// Apply values to Orders
		for (Order__c order : ordersChangedContract) {
			VF_Contract__c contract = contractsMap.get(order.VF_Contract__c);
			if (contract != null) {
				order.Account__c = contract.Account__c;
				if (contract.Owner__c != null) {
					order.Manager__c = contract.Owner__r.ManagerId;
				}
			}
		}
	}

	// This triggers the Order export, if applicable
	private void triggerOrderExport(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		Set<Id> exportOrderIds = new Set<Id>();
		Set<Id> cancelOrderIds = new Set<Id>();
		Set<Id> exportLegacyOrderIds = new Set<Id>();
		Set<Id> siasExportOrderIds = new Set<Id>();
		List<Order__c> ordersToUpdate = new List<Order__c>();
		List<CSPOFA__Orchestration_Process__c> copToInsert = new List<CSPOFA__Orchestration_Process__c>();

		// check status is ok and if relevant fields changed
		for (Order__c o : newOrders) {
			// check if customer requires separate export
			if (o.Export_system_Customerdata__c == 'SIAS') {
				if (
					o.Status__c == 'Submitted' &&
					(o.Unify_Customer_Export_Datetime__c != oldOrdersMap.get(o.Id).Unify_Customer_Export_Datetime__c ||
					o.Status__c != oldOrdersMap.get(o.Id).Status__c)
				) {
					if (o.Unify_Customer_Export_Datetime__c == null) {
						// if customer export is not completed yet, kick off customer export (preventing duplicate trigger)

						if (!CustomerExportSias.ordersInExport.contains(o.Id)) {
							CustomerExportSias.ordersInExport.add(o.Id);
							CustomerExportSias.sendCustomerNotificationOffline(o.Id);
						}
					} else {
						// if customer export is completed, kick off order export
						if (o.Export__c == 'BOP') {
							if (o.Propositions__c != 'Legacy') {
								if (o.Status__c == 'Submitted') {
									exportOrderIds.add(o.Id);
								} else if (o.Status__c == 'Cancellation Requested') {
									cancelOrderIds.add(o.Id);
								}
							} else {
								if (o.Status__c == 'Submitted') {
									exportLegacyOrderIds.add(o.Id);
								}
							}
						} else if (o.Export__c == 'SIAS' && o.EMP_Automated_Mobile_order__c) {
							if (!CustomerExportSias.ordersInExport.contains(o.Id)) {
								// we can reuse this variable since it will never be in the same context twice
								CustomerExportSias.ordersInExport.add(o.Id);
								// only mobile orders have BSL
								// create a new instance of the EMP Ordering process
								if (empOrderProcessTemplateId == null) {
									o.addError('No orchestration process template found with name \'EMP Order Provisioning\' ');
								} else {
									CSPOFA__Orchestration_Process__c cop = new CSPOFA__Orchestration_Process__c(
										CSPOFA__Orchestration_Process_Template__c = empOrderProcessTemplateId,
										Name = 'EMP Order Process for order ' + o.Name,
										VZ_Order__c = o.Id
									);
									copToInsert.add(cop);
								}
							}
						} else if (o.Export__c == 'SIAS' || o.Export__c == null) {
							// this interface was locked down. Just mark the order as 'Accepted'
							// siasExportOrderIds.add(o.Id);
							ordersToUpdate.add(new Order__c(Id = o.Id, Status__c = 'Accepted'));
							system.debug('sias');
						}
					}
				}
			} else {
				// non-sias customer. BOP Order export can be started immediately
				if (o.Propositions__c != 'Legacy') {
					if (o.Status__c == 'Submitted') {
						if (oldOrdersMap.get(o.Id).Status__c != o.Status__c) {
							exportOrderIds.add(o.Id);
						}
					}
					if (o.Status__c == 'Cancellation Requested') {
						if (oldOrdersMap.get(o.Id).Status__c != o.Status__c) {
							cancelOrderIds.add(o.Id);
						}
					}
				} else {
					if (o.Status__c == 'Submitted') {
						if (oldOrdersMap.get(o.Id).Status__c != o.Status__c) {
							exportLegacyOrderIds.add(o.Id);
						}
					}
				}
			}
		}

		update ordersToUpdate; // set mobile orders to 'accepted'
		if (!copToInsert.isEmpty()) {
			insert copToInsert;
		}

		// prevent double export triggers if order is already in export
		exportOrderIds.removeAll(OrderExport.ordersInExport);
		if (!exportOrderIds.isEmpty()) {
			OrderExport.ordersInExport.addAll(exportOrderIds);
			OrderExport.exportOrdersOffline(exportOrderIds);
		}
		cancelOrderIds.removeAll(OrderExport.ordersInExport);
		if (!cancelOrderIds.isEmpty()) {
			OrderExport.ordersInExport.addAll(cancelOrderIds);
			OrderExport.cancelOrdersOffline(cancelOrderIds);
		}
		exportLegacyOrderIds.removeAll(OrderExport.ordersInExport);
		if (!exportLegacyOrderIds.isEmpty()) {
			OrderExport.ordersInExport.addAll(exportLegacyOrderIds);
			OrderExport.exportLegacyOrdersOffline(exportLegacyOrderIds);
		}
		siasExportOrderIds.removeAll(OrderExport.ordersInExport);
		if (!siasExportOrderIds.isEmpty()) {
			OrderExport.ordersInExport.addAll(siasExportOrderIds);
			OrderExportSias.sendOrderNotificationsOffline(siasExportOrderIds);
		}
	}

	// This sets the main contact on Order level (if it exists on Account). It is stored there to enable easier order validation..
	private void setMainContact(List<Order__c> newOrders) {
		// collect accounts
		Map<Id, Id> accountIdToMainContactId = new Map<Id, Id>();
		for (Order__c newOrder : newOrders) {
			accountIdToMainContactId.put(newOrder.Account__c, null);
		}
		for (Contact_Role__c cr : [
			SELECT Id, Contact__c, Account__c
			FROM Contact_Role__c
			WHERE Type__c = 'Main' AND Active__c = TRUE AND Account__c IN :accountIdToMainContactId.keySet()
		]) {
			accountIdToMainContactId.put(cr.Account__c, cr.Contact__c);
		}
		for (Order__c newOrder : newOrders) {
			if (accountIdToMainContactId.get(newOrder.Account__c) != null) {
				newOrder.Customer_Main_Contact__c = accountIdToMainContactId.get(newOrder.Account__c);
			}
		}
	}

	// This sets the SAG contact on Order level (if it exists on Opportunity)
	// It also sets the Solution Sales contact on Order level
	private void setSAGandSolutionSalesContact(List<Order__c> newOrders) {
		Map<Id, Id> contractIdToSagUserId = new Map<Id, Id>();
		Map<Id, Id> contractIdToSolutionSalesID = new Map<Id, Id>();

		for (Order__c o : newOrders) {
			contractIdToSagUserId.put(o.VF_Contract__c, null);
		}

		List<VF_Contract__c> contractsList = [
			SELECT
				Id,
				Opportunity__r.Assigned_SAG_User__c,
				Opportunity__r.Solution_Sales__c,
				Opportunity__r.RecordType.Name,
				Opportunity__r.Segment__c,
				Opportunity__r.NetProfit_Complete__c,
				Opportunity__r.Financial_Account__c,
				Opportunity__r.Billing_Arrangement__c
			FROM VF_Contract__c
			WHERE Id IN :contractIdToSagUserId.keySet()
		];
		contractMap.putAll(contractsList);

		for (VF_Contract__c vfc : contractsList) {
			if (vfc.Opportunity__r.Assigned_SAG_User__c != null) {
				contractIdToSagUserId.put(vfc.Id, vfc.Opportunity__r.Assigned_SAG_User__c);
			}
			if (vfc.Opportunity__r.Solution_Sales__c != null) {
				contractIdToSolutionSalesID.put(vfc.id, vfc.Opportunity__r.Solution_Sales__c);
			}
		}
		for (Order__c o : newOrders) {
			if (contractIdToSagUserId.get(o.VF_Contract__c) != null) {
				o.Inside_Sales_Owner__c = contractIdToSagUserId.get(o.VF_Contract__c);
			}
			if (contractIdToSolutionSalesID.get(o.VF_Contract__c) != null) {
				o.Solution_Sales__c = contractIdToSolutionSalesID.get(o.VF_Contract__c);
			}
		}
	}

	private void preventSyncedOrderDelete(List<Order__c> orders) {
		for (Order__c o : orders) {
			Boolean preventDelete =
				o.Sales_Order_Id__c != null ||
				o.BOP_Order_Id__c != null ||
				o.BOP_export_datetime__c != null ||
				o.Block_cancellation__c;
			if (preventDelete) {
				o.addError('Order has been synced to BOP already so cannot be deleted. Please contact your administrator.');
			}
		}
	}

	// This copies the order proposition to account (see case 00005936)
	private void copyPropositionToAccount(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		Set<Id> accIds = new Set<Id>();
		Boolean isDelete = false;
		List<Account> accsToUpdate = new List<Account>();

		// Insert/Update
		if (newOrders != null) {
			accIds.addAll(GeneralUtils.getIDSetFromList(GeneralUtils.filterChangedRecords(newOrders, oldOrdersMap, 'Propositions__c'), 'Account__c'));
		}
		// Delete
		if (oldOrdersMap != null && newOrders == null) {
			isDelete = true;
			for (Order__c o : oldOrdersMap.values()) {
				if (o.VF_Contract__c != null) {
					accIds.add(o.Account__c);
				}
			}
		}

		if (!accids.isEmpty()) {
			Map<Id, Account> accsMap = new Map<Id, Account>([SELECT Id, Active_ONE_ONX__c FROM Account WHERE Id IN :accIds]);
			List<Order__c> allOrders = [
				SELECT Id, VF_Contract__c, Account__c, Propositions__c
				FROM Order__c
				WHERE Account__c IN :accIds AND VF_Contract__c != NULL
			];
			Map<Id, List<Order__c>> ordersToAccountsMap = GeneralUtils.groupByIDField(allOrders, 'Account__c');

			//update these fields on account
			//Active_ONE_ONX__c (values: One Net Express, One Net Enterprise, ONX + ONE)
			for (Id accId : ordersToAccountsMap.keySet()) {
				Account acc = accsMap.get(accId);
				List<Order__c> allOrdersForAcc = ordersToAccountsMap.get(accId);

				Boolean foundOnxOne = false;
				Boolean foundOnx = false;
				Boolean foundOne = false;

				for (Order__c o : allOrdersForAcc) {
					// skip counting if it's the one being deleted
					if (isDelete && oldOrdersMap.get(o.id) != null) {
						continue;
					}
					if (
						o.Propositions__c != null &&
						o.Propositions__c.contains('One Net Enterprise') &&
						o.Propositions__c.contains('One Net Express')
					) {
						foundOnxOne = true;
						break;
					} else if (o.Propositions__c != null && o.Propositions__c.contains('One Net Enterprise')) {
						foundOne = true;
					} else if (o.Propositions__c != null && o.Propositions__c.contains('One Net Express')) {
						foundOnx = true;
					}
				}
				if (foundOnxOne || (foundOne && foundOnx)) {
					acc.Active_ONE_ONX__c = 'ONX + ONE';
				} else if (foundOne) {
					acc.Active_ONE_ONX__c = 'One Net Enterprise';
				} else if (foundOnx) {
					acc.Active_ONE_ONX__c = 'One Net Express';
				} else {
					acc.Active_ONE_ONX__c = '';
				}
				accsToUpdate.add(acc);
			}
		}
		// user doesn't necessarily have read/write access to Account
		SharingUtils.updateRecordsWithoutSharing(accsToUpdate);
	}

	// Sends the order Ids to batch for calling OrderPABXService
	// OrderPABXService generates PBX Id required for BOP to make the changes in salesforce
	private void triggerOrderPabx(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		Set<Id> orderIdsForCreatePbx = new Set<Id>();
		for (Order__c newOrder : newOrders) {
			Order__c oldOrder = oldOrdersMap.get(newOrder.Id);
			// all orders for which BOP Order status is project created and O2C_Order__c checked
			if (
				newOrder.BOP_Order_Status__c == Constants.ORDER_BOP_ORDER_STATUS_PROJECT_CREATED &&
				oldOrder.BOP_Order_Status__c != newOrder.BOP_Order_Status__c &&
				newOrder.O2C_Order__c
			) {
				orderIdsForCreatePbx.add(newOrder.Id);
			}
		}

		if (!orderIdsForCreatePbx.isEmpty()) {
			invokeOrderPabxBatch(orderIdsForCreatePbx);
		}
	}

	private void invokeOrderPabxBatch(Set<Id> orderIdsForCreatePbx) {
		// As 'Database.executeBatch' is treated as DML due to which callout fails, run the batch's code in future context
		if (Test.isRunningTest()) {
			OrderPABXBatch.executeCalloutAsFuture(orderIdsForCreatePbx);
		} else {
			// batch size is 1 to avoid 'System.calloutException' as the service processes one contracted product at a time
			// batch will query all contracted products where competitor assets does not have PBX Id
			//      those records would sent to SIAS to generate the PBX Id
			Database.executeBatch(new OrderPABXBatch(orderIdsForCreatePbx), 1);
		}
	}

	// This fills the order status on contract
	private void copyFixedOrderDateToBAN(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		Map<Id, Ban__c> bansToUpdate = new Map<Id, Ban__c>();
		Set<Id> contractIds = new Set<Id>();
		for (Order__c o : newOrders) {
			if (o.Status__c == 'Accepted' && o.Mobile_Fixed__c == 'Fixed' && o.Status__c != oldOrdersMap.get(o.id).Status__c) {
				contractIds.add(o.VF_Contract__c);
			}
		}
		if (!contractIds.isEmpty()) {
			for (VF_Contract__c vfc : [
				SELECT Id, Opportunity__r.Ban__c
				FROM VF_Contract__C
				WHERE Opportunity__r.Ban__c != NULL AND Id IN :contractIDs
			]) {
				bansToUpdate.put(vfc.Opportunity__r.Ban__c, new BAN__c(Id = vfc.Opportunity__r.Ban__c, Datetime_of_Fixed_order__c = System.Now()));
			}

			SharingUtils.updateRecordsWithoutSharing(bansToUpdate.values());
		}
	}

	private Id empOrderProcessTemplateId {
		get {
			if (empOrderProcessTemplateId == null) {
				List<CSPOFA__Orchestration_Process_Template__c> coptList = [
					SELECT Id
					FROM CSPOFA__Orchestration_Process_Template__c
					WHERE Name = 'EMP Order Provisioning'
				];
				if (!coptList.isEmpty()) {
					return coptList[0].Id;
				} else {
					return null;
				}
			}
			return empOrderProcessTemplateId;
		}
		set;
	}

	// Sets the initial NetProfit_Complete__c
	private void setCTNReadyForValidSegments(List<Order__c> newOrders) {
		for (Order__c order : newOrders) {
			// Set the initial NetProfit_Complete__c as true if it comes from SOHO
			if (order.VF_Contract__c != null && contractMap.containsKey(order.VF_Contract__c)) {
				String segmentValue = contractMap.get(order.VF_Contract__c).Opportunity__r.Segment__c;
				if (segmentValue != null) {
					order.CTNReady__c = VALID_LOWERCASE_SEGMENTS_FOR_CTN_READY.contains(segmentValue.toLowerCase())
						? contractMap.get(order.VF_Contract__c).Opportunity__r.NetProfit_Complete__c
						: false;
				}
			}
		}
	}

	private void setTitleToBOP(Map<Id, Order__c> newOrdersMap) {
		List<Order__c> lstOrder = new List<Order__c>();
		for (Order__c objOrder : newOrdersMap.values()) {
			if (
				objOrder.Status__c.equalsIgnoreCase('Clean') &&
				String.isBlank(objOrder.BOP_Order_Id__c) &&
				objOrder.Export__c.equalsIgnoreCase('BOP') &&
				objOrder.Propositions__c.equalsIgnoreCase('Legacy')
			) {
				//filter orders to change title
				lstOrder.add(objOrder);
			}
		}
		//invoke service for changing title only when list is not empty
		if (lstOrder.isEmpty()) {
			return;
		}
		OrderService serviceOrder = new OrderService();
		serviceOrder.initInfoTitle(lstOrder);
		//add values of modified map into original newOrdersMap
		for (Order__c objOrder : newOrdersMap.values()) {
			if (serviceOrder.retOrder.titleBopByOrderId.containsKey(objOrder.Id)) {
				objOrder.Title__c = serviceOrder.retOrder.titleBopByOrderId.get(objOrder.Id);
			}
		}
	}

	//get values from opp and place them on order when it gets created. contractMap is populated in setSAGandSolutionSalesContact method
	private void setFinancialInformationAndBillingArrangement(List<Order__c> newOrders) {
		for (Order__c order : newOrders) {
			if (order.VF_Contract__c != null && contractMap.containsKey(order.VF_Contract__c)) {
				order.Financial_Account__c = contractMap.get(order.VF_Contract__c).Opportunity__r.Financial_Account__c;
				order.Billing_Arrangement__c = contractMap.get(order.VF_Contract__c).Opportunity__r.Billing_Arrangement__c;
			}
		}
	}

	@TestVisible
	private void emitOrchestratorUpdateEvent(Map<Id, Order__c> newOrdersMap, Map<Id, Order__c> oldOrdersMap) {
		Set<Id> changedOrders = new Set<Id>();
		for (Id orderId : newOrdersMap.keySet()) {
			if (
				(newOrdersMap.get(orderId).Status__c != oldOrdersMap.get(orderId).Status__c) ||
				(newOrdersMap.get(orderId).ProvisioningProcessStatus__c != oldOrdersMap.get(orderId).ProvisioningProcessStatus__c)
			) {
				changedOrders.add(orderId);
			}
		}

		if (changedOrders.size() > 0) {
			CSPOFA.Events.emit('update', changedOrders);
		}
	}
}
