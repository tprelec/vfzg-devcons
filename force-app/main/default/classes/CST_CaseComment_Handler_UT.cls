@isTest
public with sharing class CST_CaseComment_Handler_UT {
	private static Account acc;
	private static Contact con;
	private static Opportunity opp;
	private static VF_Contract__c vfc;
	private static agf__ADM_Scrum_Team__c team;
	private static Case c;
	private static Entitlement ent;
	private static User advisorUsr;
	private static User customerUser;

	@TestSetup
	static void makeData() {
		String incidentRecordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CST_Incident').getRecordTypeId();
		String usrId = UserInfo.getUserId();

		acc = TestUtils.createAccount(GeneralUtils.currentUser);
		con = TestUtils.createContact(acc);
		con.Email = '123456abcde@mail.com';
		update con;

		advisorUsr = [SELECT id FROM User WHERE isActive = TRUE AND profile.name = 'VF CST Basic' LIMIT 1];

		customerUser = [SELECT id FROM User WHERE isActive = TRUE AND profile.name = 'VF CST Customer' LIMIT 1];

		List<Case> cases = new List<Case>();

		String loggedUsrId = UserInfo.getUserId();

		Case CustomerCase = new Case(
			contactId = con.id,
			CST_End2End_Owner__c = loggedUsrId,
			recordTypeId = incidentRecordtypeId,
			status = 'New',
			CST_Sub_Status__c = 'Working on it',
			CST_Case_is_with_Team__c = 'Internal - Advisor',
			CST_CaseCategory__c = 'CST_Incident'
		);

		cases.add(CustomerCase);

		insert cases;

		CustomerCase.ownerId = advisorUsr.id;

		update CustomerCase;
	}
	@isTest
	static void advisorComment_test() {
		Case cs = [SELECT id, status, CST_Sub_Status__c, CST_End2End_Owner__c, OwnerId, CST_Case_is_with_Team__c FROM case LIMIT 1];

		String loggedUsrId = UserInfo.getUserId();

		USer usr = [SELECT id FROM user WHERE id = :loggedUsrId];

		System.debug('cs: ' + cs);

		CaseComment comm = new CaseComment(ParentId = cs.id, IsPublished = true, CommentBody = 'new info');

		Test.startTest();

		List<Case> caseList = [SELECT id FROM case LIMIT 1];
		insert comm;
		Test.stopTest();
	}

	@isTest
	static void customerComment_test() {
		Case cs = [
			SELECT id, status, CST_Sub_Status__c, recordtype.developerName, CST_End2End_Owner__c, OwnerId, CST_Case_is_with_Team__c
			FROM case
			LIMIT 1
		];

		User customerUser = [SELECT id FROM User WHERE isActive = TRUE AND profile.name = 'VF CST Customer' LIMIT 1];

		String loggedUsrId = UserInfo.getUserId();
		CaseComment comm = new CaseComment(ParentId = cs.id, IsPublished = true, CommentBody = 'new info from customerComment_test');

		Test.startTest();

		insert comm;

		Test.stopTest();
	}

	@isTest
	static void deleteComment_test() {
		Case cs = [
			SELECT id, status, CST_Sub_Status__c, recordtype.developerName, CST_End2End_Owner__c, OwnerId, CST_Case_is_with_Team__c
			FROM case
			LIMIT 1
		];

		CaseComment comm = new CaseComment(ParentId = cs.id, IsPublished = true, CommentBody = 'new info');

		insert comm;

		Test.startTest();

		delete comm;

		Test.stopTest();
	}
	@isTest
	static void editComment_test() {
		Case cs = [
			SELECT id, status, CST_Sub_Status__c, recordtype.developerName, CST_End2End_Owner__c, OwnerId, CST_Case_is_with_Team__c
			FROM case
			LIMIT 1
		];

		CaseComment comm = new CaseComment(ParentId = cs.id, IsPublished = true, CommentBody = 'new info');

		insert comm;

		Test.startTest();

		try {
			comm.CommentBody = '[WorkLog] hello';
			update comm;
		} catch (Exception e) {
			System.debug('exc -> ' + e.getMessage());
		}

		Test.stopTest();
	}
}
