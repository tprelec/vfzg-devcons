/**
 * Used by Autocomplete Visualforce component and LWC Autocomplete
 */
@SuppressWarnings('PMD.ExcessiveParameterList')
public without sharing class AutocompleteController {
    public static final Integer TOTAL_RECORDS = 20;

    @RemoteAction
    public static SObject[] findSObjects(String obj, String qry, String addFields, String parentField, String parentValue, Boolean bothSides) {       
        List<sObject> resultList = new List<sObject>();
        String soqlQuery = buildSoqlQuery(obj, qry, addFields, parentField, parentValue, bothSides);
        try {
            resultList = Database.query(soqlQuery);
        } catch (QueryException e) {
            return null;
        }
        return resultList;
    }

    @AuraEnabled(cacheable=true)
    public static SObject[] findSObjectsLwc(String sObjectApiName, String key, String addFields, String parentField, String parentValue, Boolean bothSides) {
        List<sObject> resultList = new List<sObject>();
        String soqlQuery = buildSoqlQuery(sObjectApiName, key, addFields, parentField, parentValue, bothSides);
        try {
            resultList = Database.query(soqlQuery);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
        return resultList;
    }

    @SuppressWarnings('PMD.ApexSOQLInjection')
    @AuraEnabled(cacheable=true)
    public static sObject fetchDefaultRecord(String recordId) {
        try {
            Id objectId = Id.valueOf(recordId);
            String sObjectApiName = objectId.getSObjectType().getDescribe().getName();        
            string sQuery = 'SELECT Id, Name FROM ' + sObjectApiName + ' WHERE Id = :recordId LIMIT 1';
            for (sObject obj: database.query(sQuery)) {
                return obj;
            }
            return null;
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    private static String buildSoqlQuery(String obj, String key, String addFields, String parentField, String parentValue, Boolean bothSides) {
        List<String> fieldList = new List<String>();
        if (addFields != '') {
            fieldList = addFields.split(',');
        }

        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) {
            return null;
        }
        String leftWildcard = '';
        if (bothSides) {
            leftWildcard = '%';
        }

        String soql = 'SELECT Name';
        String filter = ' LIKE \'' + leftWildcard + String.escapeSingleQuotes(key) + '%\'';
        
        if (fieldList.size() > 0) {
            for (String s : fieldList) {
                soql += ', ' + s;
            }
        }
        
        if(String.isNotEmpty(parentField)) {
            filter += ' AND ' + String.escapeSingleQuotes(parentField) + '= \'' + String.escapeSingleQuotes(parentValue) + '\'';
        }

        soql += ' FROM ' + obj + ' WHERE Name' + filter;
        soql += ' ORDER BY Name LIMIT ' + TOTAL_RECORDS;
        return soql;
    }
}