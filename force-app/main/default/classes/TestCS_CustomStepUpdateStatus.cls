@IsTest
private class TestCS_CustomStepUpdateStatus {
    @IsTest
    static void testBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            insert ord;

            COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', ord.Id, false);
            deliveryOrder1.Product_Abbreviations__c = 'ACCESS,ZIPRO';
            deliveryOrder1.Status__c = 'Created';
            insert deliveryOrder1;

            csord__Subscription__c subscription= CS_DataTest.createSubscription(testConfConf.Id);
            subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            insert subscription;

            csord__Service__c service = CS_DataTest.createService(testConfConf.Id, subscription, 'Service Created');
            service.csord__Identification__c = 'testSubscription';
            service.COM_Delivery_Order__c = deliveryOrder1.Id;
            service.VFZ_Product_Abbreviation__c = 'ZIPRO';
            insert service;

            List<Delivery_Component__c> deliveryComponents = CS_DataTest.createDeliveryComponents(service,'TestComponent', 3, false);
            for (Delivery_Component__c deliveryComponent : deliveryComponents) {
                deliveryComponent.Installation_Required__c = true;
                deliveryComponent.COM_Delivery_Order__c = deliveryOrder1.Id;
            }
            insert deliveryComponents;

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', false);
            insert step1Template;

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.COM_Delivery_Order__c = deliveryOrder1.Id;
            insert testProcess;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, false);
            step1.COM_Status__c = 'Provisioned';
            step1.COM_Provisioning_Queue__c = 'Provisioning Robot';
            step1.COM_Update_Delivery_Components__c = true;
            insert step1;
            List<CSPOFA__Orchestration_Step__c> stepList = [SELECT Id,
                    CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c,
                    COM_Provisioning_Queue__c,
                    COM_Status__c
            FROM CSPOFA__Orchestration_Step__c
            WHERE Id = :step1.Id];

            CS_CustomStepUpdateStatus customStepUpdateStatus = new CS_CustomStepUpdateStatus();
            customStepUpdateStatus.process(stepList);

            List<Delivery_Component__c> newDeliveryComponents = [SELECT Id, Status__c
            FROM Delivery_Component__c
            WHERE Id IN :deliveryComponents];

            for (Delivery_Component__c deliveryComponent : newDeliveryComponents) {
                System.debug('*** deliveryComponent: ' + JSON.serializePretty(deliveryComponent));
                System.assertEquals('Provisioned',deliveryComponent.Status__c);
            }
        }
    }
}