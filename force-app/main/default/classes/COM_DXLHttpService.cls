public with sharing class COM_DXLHttpService {
	static final String DEFAULT_ERROR_MESSAGE = '{"statusCode": 500,"statusMessage": "Default Error"}';
	private String dxlRequestBody;

	public COM_DXLHttpService(String requestBody) {
		this.dxlRequestBody = requestBody;
	}

	public HttpRequest createDxlHttpRequest(
		COM_Integration_setting__mdt dxlIntegrationSettings,
		String businessTransactionIdKey,
		List<Opportunity> opportunityData
	) {
		String namedCredentials = dxlIntegrationSettings != null ? dxlIntegrationSettings.NamedCredential__c : COM_DXL_Constants.COM_DXL_WEB_PARAM;

		HttpRequest returnValue = new HttpRequest();
		returnValue.setMethod('POST');
		returnValue.setHeader(businessTransactionIdKey, COM_WebServiceConfig.getUUID());
		returnValue.setHeader('ossCode', COM_DXL_Constants.COM_DXL_OSS_CODE);
		returnValue.setHeader('Content-Type', 'application/json');
		returnValue.setHeader('Accept', '*/*');
		returnValue.setHeader(
			'dealerMarket',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Unify_Dealer_Market_Indicator__c)
		);
		returnValue.setHeader(
			'dealerCode',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Account.Mobile_Dealer__r.Dealer_Code__c)
		);
		returnValue.setEndpoint(COM_WebServiceConfig.getEndpoint(namedCredentials, COM_DXL_Constants.COM_DXL_WEB_PARAM));
		returnValue.setBody(this.dxlRequestBody);

		return returnValue;
	}

	public HttpResponse createDxlHttpResponse(COM_DXL_CreateCustomer.CreateCustomerResponseData customerResponseData) {
		HttpResponse returnValue = new HttpResponse();
		returnValue.setHeader('requestId', 'some_random_value');
		returnValue.setHeader('transactionId', customerResponseData.businessTransactionId);
		returnValue.setHeader(customerResponseData.businessTransactionIdKey, customerResponseData.businessTransactionId);
		returnValue.setStatusCode(customerResponseData.statusCode);

		if (!customerResponseData.success) {
			returnValue.setBody(getErrorMessages(customerResponseData.statusCode).get(customerResponseData.statusCode));
		}

		return returnValue;
	}

	private Map<Integer, String> getErrorMessages(Integer statusCode) {
		Map<Integer, String> returnValue = new Map<Integer, String>();
		Set<String> errorStaticResources = new Set<String>{ 'COM_DXL_SYNC_ERROR_' + String.valueOf(statusCode) };

		List<StaticResource> staticResources = [SELECT id, Name, body FROM StaticResource WHERE Name IN :errorStaticResources];

		if (!Test.isRunningTest()) {
			for (StaticResource staticResourceRecord : staticResources) {
				returnValue.put(statusCode, staticResourceRecord.body.toString());
			}
		} else {
			returnValue.put(400, DEFAULT_ERROR_MESSAGE);
		}

		return returnValue;
	}
}
