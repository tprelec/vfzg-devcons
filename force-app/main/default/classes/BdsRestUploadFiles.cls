@RestResource(urlMapping='/opportunityfiles/*')
global without sharing class BdsRestUploadFiles {
	@HttpPost
	global static void attachFile() {
		internalAttachFile();
	}

	@TestVisible
	private static void internalAttachFile() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		if (res == null) {
			res = new RestResponse();
			RestContext.response = res;
			res.addHeader('Content-Type', 'text/json');
			res.statusCode = 200;
		}

		String oppId = req.params.get('oppId');
		String docType = req.params.get('docType');
		String docExt = req.params.get('docExt');
		String fileName = req.params.get('fileName');

		Blob blobBody = req.requestBody;

		String docname = 'Document';
		if (!String.isEmpty(fileName)) {
			docname = fileName;
		} else {
			if (docType == 'kvk') {
				docname = 'Copy KvK';
			} else if (docType == 'contract') {
				docname = 'Contract';
			}
		}

		/** Extension */
		if (!docname.contains('.')) {
			if (docExt == 'pdf') {
				docname += '.pdf';
			} else if (docExt == 'doc') {
				docname += '.doc';
			} else if (docExt == 'docx') {
				docname += '.docx';
			} else if (docExt == 'xls') {
				docname += '.xls';
			} else if (docExt == 'xlsx') {
				docname += '.xlsx';
			}
		}

		/*Attachment a = new Attachment(ParentId = oppId,
                Body = blobBody,
                //ContentType = 'image/jpg',
                Name = docname);
		*/
		ContentVersion v = new ContentVersion();
		if (Test.isRunningTest()) {
			v.versionData = Blob.valueOf('123');
		} else {
			v.versionData = blobBody;
		}
		v.title = docname;
		v.pathOnClient = docname;
		insert v;

		v = [SELECT id, ContentDocumentID FROM ContentVersion WHERE id = :v.id];
		/** Return class */
		BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
		//BdsRestUploadFiles.CreateFileReturnClass returnClass = new BdsRestUploadFiles.CreateFileReturnClass();
		if (v != null) {
			try {
				/** To save this record, the user needs read rights on the account */
				Opportunity opp = [SELECT AccountId, Account.OwnerId FROM Opportunity WHERE Id = :oppId];
				if (opp != null) {
					Boolean needSharing = false;
					if (opp.Account.OwnerId != UserInfo.getUserId()) {
						/** Check if has sharing rights */

						List<AccountShare> accShareList = [
							SELECT Id, AccountAccessLevel, OpportunityAccessLevel
							FROM AccountShare
							WHERE AccountId = :opp.AccountId AND UserOrGroupId = :UserInfo.getUserId()
						];
						if (accShareList.size() == 0) {
							needSharing = true;
						}
					}
					if (needSharing) {
						AccountShare accShare = new AccountShare();
						accShare.AccountId = opp.AccountId;
						accShare.UserOrGroupId = UserInfo.getUserId();
						accShare.CaseAccessLevel = 'None';
						accShare.OpportunityAccessLevel = 'Read';
						accShare.AccountAccessLevel = 'Read';
						SharingUtils.insertSharingRulesWithoutSharingStatic(new List<AccountShare>{ accShare });
					}
				}

				Opportunity_Attachment__c oa = new Opportunity_Attachment__c();
				oa.Opportunity__c = oppId;
				oa.Description_Optional__c = docname;
				if (docType == 'kvk') {
					oa.Attachment_Type__c = 'Copy KvK';
				} else if (docType == 'contract') {
					oa.Attachment_Type__c = 'Contract (Signed)';
				}
				insert oa;

				/*a.ParentId = oa.Id;
                insert a;
				*/
				ContentDocumentLink cl = new ContentDocumentLink();
				cl.ContentDocumentId = v.ContentDocumentId;
				cl.LinkedEntityId = oa.Id;
				cl.ShareType = 'V';
				cl.Visibility = 'AllUsers';
				insert cl;
				returnClass.isSuccess = true;
			} catch (Exception e) {
				res.statusCode = 400;
				returnClass.errorMessage = e.getMessage();
			}
		}

		res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
	}
}
