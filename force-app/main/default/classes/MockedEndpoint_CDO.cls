@RestResource(urlMapping='/mockedEndpoints/cdoEndpoint')
global with sharing class MockedEndpoint_CDO {
	@HttpPost
	global static void processRequest() {
		RestRequest request = RestContext.request;
		System.debug('request ' + request);
		RestResponse response = RestContext.response;
		response.statusCode = 200;
		response.responseBody = Blob.valueOf(formulateResponse(request.requestBody.toString()));
		insert new COM_Delivery_Order__c(Name = 'testinsert' + DateTime.now());
		mockAsync(response.responseBody.toString());
	}

	private static String formulateResponse(String requestBody) {
		//TODO check if createTenant or create/modify/deleteService
		return requestBody;
	}

	@future(callout=true)
	private static void mockAsync(String requestToProcess) {
		String sfdcURL = URL.getSalesforceBaseUrl().toExternalForm();
		String restAPIURL = sfdcURL + '/services/apexrest/notificationListener';
		HttpRequest httpRequest = new HttpRequest();
		httpRequest.setMethod('POST');
		httpRequest.setHeader('Authorization', 'OAuth ' + UserInfo.getSessionId());
		httpRequest.setHeader('Authorization', 'Bearer ' + UserInfo.getSessionID());
		httpRequest.setEndpoint(restAPIURL);
		httpRequest.setBody(requestToProcess);
		Http http = new Http();
		HttpResponse httpResponse = http.send(httpRequest);
	}
}
