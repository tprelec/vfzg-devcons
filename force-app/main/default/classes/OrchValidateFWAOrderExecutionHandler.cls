//This is the custom controller class for the CS Orchestrator custom step 'Validate FWA Order'.
@SuppressWarnings('PMD.AvoidGlobalModifier') //Orchestrator needs by force a global Modifier.
global without sharing class OrchValidateFWAOrderExecutionHandler implements CSPOFA.ExecutionHandler, CSPOFA.Calloutable {
	private Map<Id, List<OrchCalloutsWrapperCls.calloutData>> calloutResultsMap = new Map<Id, List<OrchCalloutsWrapperCls.calloutData>>();
	private String calloutError;
	private Map<Id, String> mapContractedProductId;
	private Map<Id, String> processToOrderIdMap;
	private Map<Id, Integer> mapcontractedProductDuration;
	private Map<Id, Map<Integer, List<Contracted_Products__c>>> orderIdToDurationToContractedProduct;
	public Boolean performCallouts(List<SObject> data) {
		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;
		calloutResultsMap = new Map<Id, List<OrchCalloutsWrapperCls.calloutData>>();
		Boolean calloutsPerformed = false;
		try {
			Set<Id> resultIds = new Set<Id>();
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				resultIds.add(step.CSPOFA__Orchestration_Process__c);
			}
			OrchUtils.getProcessDetails(resultIds);
			mapContractedProductId = OrchUtils.mapContractedProductId;
			mapcontractedProductDuration = OrchUtils.mapcontractedProductDuration;
			this.processToOrderIdMap = OrchUtils.mapcontractedProductOrderId;
			// Get Contracted Products
			this.getContractedProducts();
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				String contractedProductId = mapContractedProductId.get(step.CSPOFA__Orchestration_Process__c);
				calloutResultsMap.put(step.Id, EMP_BSLintegration.createFramework(contractedProductId));
				calloutsPerformed = true;
			}
		} catch (exception e) {
			system.debug(LoggingLevel.INFO, (e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString()).abbreviate(255));
		}
		return calloutsPerformed;
	}
	@SuppressWarnings('PMD.CyclomaticComplexity')
	public List<sObject> process(List<sObject> data) {
		List<sObject> result = new List<sObject>();
		List<Contracted_Products__c> contractedProductsToUpdate = new List<Contracted_Products__c>();
		List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>) data;
		for (CSPOFA__Orchestration_Step__c step : stepList) {
			if (calloutResultsMap.containsKey(step.Id)) {
				List<OrchCalloutsWrapperCls.calloutData> calloutResult = calloutResultsMap.get(step.Id);
				if (calloutResult != null && (Boolean) calloutResult[0].success == true) {
					step = OrchUtils.setStepRecord(step, false, 'Validate Create Framework step completed');
					String orderIdOfCP = processToOrderIdMap.get(step.CSPOFA__Orchestration_Process__c);
					Integer durationOfCP = mapcontractedProductDuration.get(step.CSPOFA__Orchestration_Process__c);

					for (
						Contracted_Products__c objCPToUpdate : (List<Contracted_Products__c>) orderIdToDurationToContractedProduct.get(orderIdOfCP)
							.get(durationOfCP)
					) {
						contractedProductsToUpdate.add(
							new Contracted_Products__c(
								Id = objCPToUpdate.Id,
								Unify_FW_Order_Id__c = (String) calloutResult[0].infoToCOM.get('orderId'),
								Framework_Id__c = (String) calloutResult[0].infoToCOM.get('FrameworkId')
							)
						);
					}
				} else {
					step = OrchUtils.setStepRecord(step, true, 'Error occurred: ' + (String) calloutResult[0].errorMessage);
				}
				step.Request__c = calloutResult != null &&
					!calloutResult.isEmpty() &&
					calloutResult[0].strReq != null
					? calloutResult[0].strReq.abbreviate(131000)
					: '';
				step.Response__c = calloutResult != null &&
					!calloutResult.isEmpty() &&
					calloutResult[0].strRes != null
					? calloutResult[0].strRes.abbreviate(131000)
					: '';
			} else {
				step = OrchUtils.setStepRecord(step, true, 'Error occurred: Callout results not received.');
			}
			result.add(step);
		}

		result = this.tryUpdateRelatedRecord(stepList, result, contractedProductsToUpdate);
		return result;
	}
	private List<sObject> tryUpdateRelatedRecord(
		List<CSPOFA__Orchestration_Step__c> stepList,
		List<sObject> result,
		List<Contracted_Products__c> contractedProductsToUpdate
	) {
		try {
			Map<Id, String> failedContractedProductUpdateMap = new Map<Id, String>();
			List<Database.SaveResult> updateResults = Database.update(contractedProductsToUpdate, false);
			for (Integer i = 0; i < updateResults.size(); i++) {
				if (!updateResults.get(i).isSuccess()) {
					Database.Error error = updateResults.get(i).getErrors().get(0);
					failedContractedProductUpdateMap.put(contractedProductsToUpdate.get(i).Id, error.getMessage());
				}
			}
			if (!failedContractedProductUpdateMap.isEmpty()) {
				for (CSPOFA__Orchestration_Step__c step : stepList) {
					String contractedProductId = this.mapContractedProductId.get(step.CSPOFA__Orchestration_Process__c);
					if (failedContractedProductUpdateMap.containsKey(contractedProductId)) {
						result.add(
							OrchUtils.setStepRecord(
								step,
								true,
								'Error occurred: Contracted Product update failed with error: ' +
								failedContractedProductUpdateMap.get(contractedProductId)
							)
						);
					}
				}
			}
		} catch (Exception e) {
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				result.add(
					OrchUtils.setStepRecord(
						step,
						true,
						('Error occurred: ' + e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString()).abbreviate(255)
					)
				);
			}
		}
		return result;
	}

	private void getContractedProducts() {
		Set<String> orderIds = new Set<String>(processToOrderIdMap.values());
		List<Contracted_Products__c> conProducts = [
			SELECT
				Id,
				Order__c,
				Model_Number__c,
				Line_Description__c,
				Price_Plan_Class__c,
				Deal_Type__c,
				Duration__c,
				Unify_Template_Description_Name__c,
				Product__r.Is_an_Addon_for_template_description__c,
				Product__r.Product_group__c,
				Product__r.Unify_Template_Description__c,
				product__r.Name,
				product__r.Description
			FROM Contracted_Products__c
			WHERE Order__c IN :orderIds AND Product__r.Product_group__c = 'Priceplan'
		];

		this.orderIdToDurationToContractedProduct = this.create2LevelNestedMapList(conProducts, 'Order__c', 'Duration__c');
	}

	private Map<Id, Map<Integer, List<SObject>>> create2LevelNestedMapList(List<SObject> records, String keyOnefield, String keyTwoField) {
		Map<Id, Map<Integer, List<sObject>>> mainMap = new Map<Id, Map<Integer, List<sObject>>>();
		for (SObject record : records) {
			Id fieldValueOne = (Id) record.get(keyOnefield);
			Integer fieldValueTwo = Integer.valueOf(record.get(keyTwoField));
			Map<Integer, List<SObject>> innerMap = mainMap.containsKey(fieldValueOne)
				? mainMap.get(fieldValueOne)
				: new Map<Integer, List<SObject>>();

			if (innerMap.containsKey(fieldValueTwo)) {
				innerMap.get(fieldValueTwo).add(record);
			} else {
				innerMap.put(fieldValueTwo, new List<SObject>{ record });
			}
			mainMap.put(fieldValueOne, innerMap);
		}
		return mainMap;
	}
}
