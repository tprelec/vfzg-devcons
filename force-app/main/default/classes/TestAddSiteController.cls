@IsTest
public with sharing class TestAddSiteController {
	@TestSetup
	static void makeData() {
		Account a = TestUtils.createAccount(null);
	}

	@IsTest
	public static void testOblicoSearch() {
		OlbicoSettings__c oblico = new OlbicoSettings__c();
		oblico.Olbico_JSon_Endpoint__c = 'Fake endpoint';
		oblico.Olbico_Json_Username__c = 'Fake username';
		oblico.Olbico_Json_Password__c = 'Fake password';
		oblico.Olbico_Json_BAG_Streets__c = 'Fake bag street';
		insert oblico;

		Test.startTest();
		Map<String, String> result = AddSiteController.oblicoSearch('1111XX');
		Test.stopTest();

		System.assertEquals(result.size(), 0, 'There shuld be no options returned.');
	}

	@IsTest
	public static void testSaveSite() {
		Account acc = [SELECT Id FROM Account LIMIT 1];

		Test.startTest();
		AddSiteController.saveSite('Medanstraat | 1 |  |  | 3531EC | Utrecht', acc.Id);
		Test.stopTest();
		Site__c newSIte = [SELECT Id, Site_Street__c, Site_Postal_Code__c, Site_House_Number__c FROM Site__c];
		List<Site__c> postSites = [SELECT Id FROM Site__c WHERE Site_Account__c = :acc.Id];
		System.assert(postSites.size() > 0, 'There should be one new site.');
		System.assertEquals('Medanstraat', newSIte.Site_Street__c.trim(), 'There should be one new site.');
		System.assertEquals(1, newSIte.Site_House_Number__c, 'There should be one new site.');
		System.assertEquals('3531EC', newSIte.Site_Postal_Code__c.trim(), 'There should be one new site.');
	}
}
