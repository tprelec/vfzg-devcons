/**
 * @description		This is the test class for ECSSOAPPostalCodeCheck class. Because of a bug in mockservices, we have to cover this specifically
 * @author        	Guy Clairbois
 */
@isTest
private class TestPostalCodeCheckSOAPData {

    static testMethod void dataTest() {
    	ECSSOAPPostalCodeCheck pcc = new ECSSOAPPostalCodeCheck();
    	ECSSOAPPostalCodeCheck.authenticationHeader_element ah = new ECSSOAPPostalCodeCheck.authenticationHeader_element();
    	ECSSOAPPostalCodeCheck.bandwidthType bt = new ECSSOAPPostalCodeCheck.bandwidthType();
    	ECSSOAPPostalCodeCheck.lineoptionsType lot = new ECSSOAPPostalCodeCheck.lineoptionsType();
    	ECSSOAPPostalCodeCheck.locationsRequestType lrqt = new ECSSOAPPostalCodeCheck.locationsRequestType();
    	ECSSOAPPostalCodeCheck.locationsResponseType lrst = new ECSSOAPPostalCodeCheck.locationsResponseType();
    	ECSSOAPPostalCodeCheck.locationTypeRequest ltrq = new ECSSOAPPostalCodeCheck.locationTypeRequest();
    	ECSSOAPPostalCodeCheck.locationTypeResponse ltrs = new ECSSOAPPostalCodeCheck.locationTypeResponse();
    	ECSSOAPPostalCodeCheck.optionType ot = new ECSSOAPPostalCodeCheck.optionType();
    	ECSSOAPPostalCodeCheck.pcavailability pca = new ECSSOAPPostalCodeCheck.pcavailability();
 		
 	
      	System.assert(true, 'dummy assertion');
    }
    
}