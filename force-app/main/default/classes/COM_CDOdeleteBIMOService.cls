public with sharing class COM_CDOdeleteBIMOService {
	/*
	public static final String COM_CDO_INTEGRATION_SETTING_NAME = 'deleteBIMOService';
	public static final String COM_CDO_INTEGRATION_FAILED_STATUS = 'Failure';
	public static final String COM_CDO_INTEGRATION_COMPLETE_STATUS = 'Complete';
	public static final String MOCKSYNCSUCCESS = '{ "id": "b0d13c8a-5d77-4e19-96f1-2405ee2f34dg", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2f34dg", "externalId": "COM", "priority": "1", "description": "Internet Modem Only Service Order", "category": "Internet Modem Only Service", "state": "acknowledged", "orderDate": "2022-02-08T10:37:15.97221Z", "@type": "standard", "orderItem": [ { "id": "1", "action": "delete", "state": "acknowledged", "@type": "standard", "service": { "id": "4ed3dece-d5bd-442f-a1f9-687c0a1c52221" } } ] }';
	public static final String MOCKSYNCFAILURE = '{ "code": 40020, "reason": "SERVICE_ORDER_CHARACTERISTICS_VALIDATION_ERROR", "message": "Request validation has failed due the following issue(s) - [ Input parameter accessL1Article violates constraint, Invalid value ]" }';

	public static String deleteBIMOService(Id deliveryOrderId) {
		String responseBody = deleteBIMOServiceReq(deliveryOrderId);
		return responseBody;
	}

	private static String deleteBIMOServiceReq(Id deliveryOrderId) {
		String result = '';
		COM_CDO_BIMOService request = getRequest(deliveryOrderId);

		COM_Integration_setting__mdt deleteBIMOServiceSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_INTEGRATION_SETTING_NAME
		);
		Boolean doNotExecuteReq = deleteBIMOServiceSetting != null
			? deleteBIMOServiceSetting.Execute_mock__c
			: true;
		Boolean doNotExecuteStatus = deleteBIMOServiceSetting != null
			? deleteBIMOServiceSetting.Mock_Execution_result__c
			: true;

		HttpResponse response = sendDeleteBIMOServiceRequest(
			request,
			doNotExecuteReq,
			doNotExecuteStatus
		);

		//if failed response
		if (failedResponse(response)) {
			DeleteBIMOServiceSyncErrorResponse responseDeserialized = (DeleteBIMOServiceSyncErrorResponse) JSON.deserializeStrict(
				response.getBody(),
				DeleteBIMOServiceSyncErrorResponse.class
			);

			COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
			updates.deliveryOrderId = deliveryOrderId;
			updates.status = COM_CDO_INTEGRATION_FAILED_STATUS;
			updates.errorMessage = responseDeserialized.message;
			updates.success = false;

			COM_CDOHelper.performOrderUpdatesAfterResponse(updates, null);

			result = response.getBody();
		} else {
			String reqBody = COM_CDOServiceCharacteristicHelper.modifyJsonLiteToFitDeserializeClass(
				response.getBody()
			);
			result = reqBody;
			COM_CDO_BIMOService body = (COM_CDO_BIMOService) JSON.deserializeStrict(
				reqBody,
				COM_CDO_BIMOService.class
			);

			COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
			updates.deliveryOrderId = deliveryOrderId;
			updates.status = '';
			updates.errorMessage = '';
			updates.success = true;

			COM_CDOHelper.performOrderUpdatesAfterResponse(updates, body);
		}

		return result;
	}

	public static COM_CDO_BIMOService getRequest(Id deliveryOrderId) {
		List<String> umbrellaServices = COM_CDOHelper.getUmbrellaServices(deliveryOrderId);

		COM_CDO_BIMOService request = new COM_CDO_BIMOService();

		COM_CDO_integration_settings__mdt comSetting = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_INTEGRATION_SETTING_NAME
		);

		request.externalId = comSetting.externalId__c;
		request.priority = comSetting.priority__c;
		request.description = comSetting.Description__c;
		request.category = comSetting.Category__c;
		request.typeVal = comSetting.requestType__c;
		request.orderItem = new List<COM_CDO_BIMOService.OrderItem>();

		Integer index = 1;
		for (String umbrellaService : umbrellaServices) {
			COM_CDO_BIMOService.OrderItem orderitem = new COM_CDO_BIMOService.OrderItem();
			orderItem.id = String.valueOf(index);
			orderItem.action = 'delete';
			orderItem.typeVal = 'standard';

			COM_CDO_BIMOService.Service service = new COM_CDO_BIMOService.Service();

			service.id = umbrellaService;

			orderItem.service = service;

			request.orderItem.add(orderitem);
			index++;
		}

		return request;
	}

	private static String generatePayloadJson(COM_CDO_BIMOService req) {
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeStringField('externalId', req.externalId);
		gen.writeStringField('priority', req.priority);
		gen.writeStringField('description', req.description);
		gen.writeStringField('category', req.category);
		gen.writeStringField('@type', req.typeVal);
		gen.writeFieldName('orderItem');
		gen.writeStartArray();
		for (COM_CDO_BIMOService.OrderItem orderItem : req.orderItem) {
			gen.writeStartObject();
			gen.writeStringField('id', orderItem.id);
			gen.writeStringField('action', orderItem.action);
			gen.writeStringField('@type', orderItem.typeVal);
			gen.writeFieldName('service');
			gen.writeStartObject();
			gen.writeStringField('id', orderItem.service.id);
			gen.writeEndObject();
			gen.writeEndObject();
		}
		gen.writeEndArray();
		gen.writeEndObject();

		return gen.getAsString();
	}

	private static HTTPResponse sendDeleteBIMOServiceRequest(
		COM_CDO_BIMOService req,
		Boolean doNotExecute,
		Boolean doNotExecuteStatus
	) {
		HttpRequest reqData = new HttpRequest();
		Http http = new Http();
		reqData.setEndpoint('callout:CDO_createTenant?format=json');

		String requestBody = generatePayloadJson(req);

		System.debug(LoggingLevel.DEBUG, '### Request body: ' + requestBody);
		reqData.setBody(requestBody);
		reqData.setMethod('POST');
		System.debug(LoggingLevel.DEBUG, '@@@@ reqData: ' + reqData);
		HTTPResponse response = new HttpResponse();
		if (doNotExecute && !Test.isRunningTest()) {
			response.setHeader('Content-Type', 'application/json');
			if (doNotExecuteStatus) {
				String mockSuccess = getMockResponseSuccess();
				mockSuccess = mockSuccess != null ? mockSuccess : MOCKSYNCSUCCESS;
				response.setBody(mockSuccess);
				response.setStatusCode(200);
			} else {
				String mockFailed = getMockResponseFailed();
				mockFailed = mockFailed != null ? mockFailed : MOCKSYNCFAILURE;
				response.setBody(mockFailed);

				response.setStatusCode(400);
			}
		} else {
			response = http.send(reqData);
		}

		System.debug(LoggingLevel.DEBUG, '@@@@ response: ' + response.getBody());
		return response;
	}

	private static String getMockResponseSuccess() {
		String result = null;
		StaticResource sr = [
			SELECT id, body
			FROM StaticResource
			WHERE Name = 'COM_CDO_deleteBIMOServiceSyncSuccess'
		];
		if (sr != null) {
			result = sr.body.toString();
		}

		return result;
	}

	private static String getMockResponseFailed() {
		String result = null;
		StaticResource sr = [
			SELECT id, body
			FROM StaticResource
			WHERE Name = 'COM_CDO_deleteBIMOServiceSyncFailed'
		];

		if (sr != null) {
			result = sr.body.toString();
		}

		return result;
	}

	private static Boolean failedResponse(HTTPResponse response) {
		Boolean result = false;
		try {
			DeleteBIMOServiceSyncErrorResponse responseDeserialized = (DeleteBIMOServiceSyncErrorResponse) JSON.deserializeStrict(
				response.getBody(),
				DeleteBIMOServiceSyncErrorResponse.class
			);
			if (responseDeserialized.code != null) {
				result = true;
			}
		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	public class DeleteBIMOServiceSyncErrorResponse {
		public Integer code;
		public String reason;
		public String message;
	}
	*/
}
