/*
	Author: Juan Cardona
	Description: component that retrieves products marked as "Cease" and prepare a wrapper object.
	Jira ticket: COM-3181
*/

public with sharing class COM_CancelledSitesController {
	private static final String CEASE_ACTIVITY_TYPE = 'Cease';
	@testVisible
	private static final String BUSINESS_INTERNET_PROPOSITION = 'Business Internet';

	public string opportunityId { get; set; }

	/*
	 * Prepare the information related to "Business Internet" Ceased products.
	 * The logic checks whether there are business internet products marked as
	 * "Ceased" and groups them based on the location (Site).
	 *
	 * @return	A map that contanis Terminated products per Site.
	 */
	public Map<String, List<TerminatedProduct>> getCancelledProducts() {
		TerminatedProduct product;
		Set<string> businessInternetCeasedSites = new Set<string>();
		Map<String, List<OpportunityLineItem>> productsBySite = new Map<String, List<OpportunityLineItem>>();
		Map<String, List<TerminatedProduct>> cancelledProductsBySite = new Map<String, List<TerminatedProduct>>();

		for (OpportunityLineItem oppLineItem : getProducts(opportunityId)) {
			if (oppLineItem.Location__c != null && oppLineItem.Location__r.Unify_Site_Name__c != null) {
				if (
					oppLineItem.Proposition__c == BUSINESS_INTERNET_PROPOSITION &&
					!businessInternetCeasedSites.contains(oppLineItem.Location__r.Unify_Site_Name__c)
				) {
					businessInternetCeasedSites.add(oppLineItem.Location__r.Unify_Site_Name__c);
				}

				if (productsBySite.containsKey(oppLineItem.Location__r.Unify_Site_Name__c)) {
					productsBySite.get(oppLineItem.Location__r.Unify_Site_Name__c).add(oppLineItem);
				} else {
					productsBySite.put(oppLineItem.Location__r.Unify_Site_Name__c, new List<OpportunityLineItem>{ oppLineItem });
				}
			}
		}

		for (String site : businessInternetCeasedSites) {
			for (OpportunityLineItem p : productsBySite.get(site)) {
				product = new TerminatedProduct(p);

				if (cancelledProductsBySite.containsKey(product.siteKey)) {
					cancelledProductsBySite.get(product.siteKey).add(product);
				} else {
					cancelledProductsBySite.put(product.siteKey, new List<TerminatedProduct>{ product });
				}
			}
		}

		return cancelledProductsBySite;
	}

	/*
	 * Method that perform the SOQL that retrieves the Ceased
	 * products.
	 */
	private List<OpportunityLineItem> getProducts(Id opportunityId) {
		return [
			SELECT
				Id,
				Name,
				Proposition__c,
				Location__c,
				Location__r.Unify_Site_Name__c,
				Product_Name__c,
				Contract_Number__c,
				Financial_Account__c,
				Financial_Account__r.Bank_Account_Number__c
			FROM OpportunityLineItem
			WHERE
				OpportunityId = :opportunityId
				AND Activity_Type__c = :CEASE_ACTIVITY_TYPE
				AND cscfga__Attribute__c != NULL
				AND cscfga__Attribute__r.cscfga__Product_Configuration__c != NULL
				AND cscfga__Attribute__r.cscfga__Product_Configuration__r.csordtelcoa__cancelled_by_change_process__c = TRUE
		];
	}

	/*
	 * Wrapper Object that gruop attrubittes related
	 * to termination process.
	 */
	public class TerminatedProduct {
		public String siteKey { get; set; }
		public String productName { get; set; }
		public String contractNumber { get; set; }
		public String debitNumber { get; set; }

		public TerminatedProduct(OpportunityLineItem product) {
			siteKey = product.Location__r.Unify_Site_Name__c;
			productName = product.Product_Name__c != null ? product.Product_Name__c : '-';
			contractNumber = product.Contract_Number__c != null ? product.Contract_Number__c : '-';
			debitNumber = product.Financial_Account__c != null ? product.Financial_Account__r.Bank_Account_Number__c : '-';
		}
	}
}
