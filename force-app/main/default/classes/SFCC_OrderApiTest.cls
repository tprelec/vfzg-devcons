/*
    Author: Juan Cardona/Rahul Sharma
    Description: Tests class for SFCC_OrderApi.cls
    Jira ticket: COM-1542
*/

@IsTest
private class SFCC_OrderApiTest {
	private static String validOrderNumber = 'test-1234';

	@IsTest
	static void test_invalid_bad_request_data() {
		setRequest('{"a": 1}');

		Test.startTest();

		SFCC_OrderApi.getOrderStatus();

		Test.stopTest();

		System.assertEquals(SFCC_OrderApi.ORDER_API_REQUEST_JSON_PARSER_ERROR_CODE, RestContext.response.statusCode);
		System.assertEquals(SFCC_OrderApi.ORDER_API_REQUEST_JSON_PARSER_ERROR, RestContext.response.responseBody.toString());
	}

	@IsTest
	static void test_empty_order_number_in_request() {
		String orderNumber = '';
		String accountId = 'fake account id';
		List<SFCC_OrderApi.RequestWrapper> requestWrapperList = new List<SFCC_OrderApi.RequestWrapper>{
			new SFCC_OrderApi.RequestWrapper(orderNumber, accountId)
		};
		setRequest(JSON.serialize(requestWrapperList));

		Test.startTest();

		SFCC_OrderApi.getOrderStatus();

		Test.stopTest();

		System.assertEquals(SFCC_OrderApi.ORDER_API_REQUEST_SUCCESS_CODE, RestContext.response.statusCode);
		String responseBody = RestContext.response.responseBody.toString();
		System.assertNotEquals(null, responseBody);

		List<SFCC_OrderApi.ResponseWrapper> responseWrapperList = (List<SFCC_OrderApi.ResponseWrapper>) JSON.deserialize(
			responseBody,
			List<SFCC_OrderApi.ResponseWrapper>.class
		);
		System.assertEquals(1, responseWrapperList.size());

		SFCC_OrderApi.ResultWrapper resultWrapper = new SFCC_OrderApi.ResultWrapper(false, SFCC_OrderApi.ORDER_API_REQUEST_JSON_EMPTY_ATTRIBUTES);
		SFCC_OrderApi.OwnerWrapper owner = new SFCC_OrderApi.OwnerWrapper(null, null, null);
		SFCC_OrderApi.ResponseWrapper expectedResponseWrapper = new SFCC_OrderApi.ResponseWrapper(orderNumber, accountId, null, owner, resultWrapper);
		System.assertEquals(JSON.serialize(expectedResponseWrapper), JSON.serialize(responseWrapperList[0]));
	}

	@IsTest
	static void test_order_not_found() {
		String orderNumber = 'fake order number';
		String accountId = 'fake account id';
		List<SFCC_OrderApi.RequestWrapper> requestWrapperList = new List<SFCC_OrderApi.RequestWrapper>{
			new SFCC_OrderApi.RequestWrapper(orderNumber, accountId)
		};
		setRequest(JSON.serialize(requestWrapperList));

		Test.startTest();

		SFCC_OrderApi.getOrderStatus();

		Test.stopTest();

		System.assertEquals(SFCC_OrderApi.ORDER_API_REQUEST_SUCCESS_CODE, RestContext.response.statusCode);
		String responseBody = RestContext.response.responseBody.toString();
		System.assertNotEquals(null, responseBody);

		List<SFCC_OrderApi.ResponseWrapper> responseWrapperList = (List<SFCC_OrderApi.ResponseWrapper>) JSON.deserialize(
			responseBody,
			List<SFCC_OrderApi.ResponseWrapper>.class
		);
		System.assertEquals(1, responseWrapperList.size());

		SFCC_OrderApi.ResultWrapper resultWrapper = new SFCC_OrderApi.ResultWrapper(false, SFCC_OrderApi.ORDER_API_REQUEST_ORDER_NOT_FOUND);
		SFCC_OrderApi.OwnerWrapper owner = new SFCC_OrderApi.OwnerWrapper(null, null, null);
		SFCC_OrderApi.ResponseWrapper expectedResponseWrapper = new SFCC_OrderApi.ResponseWrapper(orderNumber, accountId, null, owner, resultWrapper);
		System.assertEquals(JSON.serialize(expectedResponseWrapper), JSON.serialize(responseWrapperList[0]));
	}

	@IsTest
	static void test_order_not_status_found() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		// create a test data
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOpportunityFieldMappings();
		TestUtils.createCompleteOpportunity();

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(TestUtils.theOpportunity, 'Test Basket');
		basket.Primary__c = true;
		basket.cscfga__Basket_Status__c = 'Contract Created';
		insert basket;

		VF_Contract__c contr = TestUtils.createVFContract(TestUtils.theAccount, TestUtils.theOpportunity);
		OrderType__c ot = TestUtils.createOrderType();
		System.assertNotEquals(null, ot.Id, 'Order Type shouldnt be null');

		TestUtils.autoCommit = false;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			ProvisioningProcessStatus__c = 'Pending'
		);
		insert order;
		System.assertNotEquals(null, order.Id, 'Order Id shouldnt be null');

		csb2c__Inbound_Ecommerce_Order_Request__c ecommerceOrderRequest = new csb2c__Inbound_Ecommerce_Order_Request__c(
			csb2c__Product_Basket__c = basket.id,
			csb2c__API_Order_Reference__c = validOrderNumber
		);
		insert ecommerceOrderRequest;

		Credit_Check__c cc = new Credit_Check__c();
		cc.Opportunity__c = TestUtils.theOpportunity.Id;
		cc.Credit_Check_Status_Number__c = 2;
		insert cc;

		update new Opportunity(Id = TestUtils.theOpportunity.Id, StageName = 'Closed Won');
		System.assertEquals([SELECT Id, IsWon FROM Opportunity WHERE Id = :TestUtils.theOpportunity.Id].IsWon, true, 'Opp is Closed Won');

		// prepare the request
		String accountId = 'dummy account id';
		List<SFCC_OrderApi.RequestWrapper> requestWrapperList = new List<SFCC_OrderApi.RequestWrapper>{
			new SFCC_OrderApi.RequestWrapper(validOrderNumber, accountId)
		};
		setRequest(JSON.serialize(requestWrapperList));

		// set the custom metadata
		SFCC_OrderApi.orderStatusMetaDataList = new List<COM_B2B_Online_Order_Status__mdt>{
			new COM_B2B_Online_Order_Status__mdt(
				DeveloperName = 'In_Process_Order_Initializing_Status',
				MasterLabel = 'In Process Order Initializing Status',
				Commerce_Cloud_Status__c = 'In process',
				Source_Object__c = 'Order__c',
				Source_Field__c = 'ProvisioningProcessStatus__c',
				Value__c = 'Initializing'
			)
		};

		Test.startTest();

		SFCC_OrderApi.getOrderStatus();

		Test.stopTest();

		System.assertEquals(SFCC_OrderApi.ORDER_API_REQUEST_SUCCESS_CODE, RestContext.response.statusCode);
		String responseBody = RestContext.response.responseBody.toString();
		System.assertNotEquals(null, responseBody);

		List<SFCC_OrderApi.ResponseWrapper> responseWrapperList = (List<SFCC_OrderApi.ResponseWrapper>) JSON.deserialize(
			responseBody,
			List<SFCC_OrderApi.ResponseWrapper>.class
		);
		System.assertEquals(1, responseWrapperList.size());

		System.assertEquals(validOrderNumber, responseWrapperList[0].orderNumber);
		System.assertEquals(accountId, responseWrapperList[0].accountId);
		System.assertEquals(null, responseWrapperList[0].orderStatus);
		System.assertNotEquals(null, responseWrapperList[0].owner);
		System.assertEquals(false, responseWrapperList[0].result.isSuccess);
		System.assertEquals(SFCC_OrderApi.ORDER_API_REQUEST_ORDER_STATUS_NOT_FOUND, responseWrapperList[0].result.message);
	}

	@IsTest
	static void test_order_status_in_progress() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		// create a test data
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOpportunityFieldMappings();
		TestUtils.createCompleteOpportunity();

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(TestUtils.theOpportunity, 'Test Basket');
		basket.Primary__c = true;
		basket.cscfga__Basket_Status__c = 'Contract Created';
		insert basket;

		VF_Contract__c contr = TestUtils.createVFContract(TestUtils.theAccount, TestUtils.theOpportunity);
		OrderType__c ot = TestUtils.createOrderType();
		System.assertNotEquals(null, ot.Id, 'Order Type shouldnt be null');

		TestUtils.autoCommit = false;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			ProvisioningProcessStatus__c = 'Initializing'
		);
		insert order;
		System.assertNotEquals(null, order.Id, 'Order Id shouldnt be null');

		csb2c__Inbound_Ecommerce_Order_Request__c ecommerceOrderRequest = new csb2c__Inbound_Ecommerce_Order_Request__c(
			csb2c__Product_Basket__c = basket.id,
			csb2c__API_Order_Reference__c = validOrderNumber
		);
		insert ecommerceOrderRequest;

		Credit_Check__c cc = new Credit_Check__c();
		cc.Opportunity__c = TestUtils.theOpportunity.Id;
		cc.Credit_Check_Status_Number__c = 2;
		insert cc;

		update new Opportunity(Id = TestUtils.theOpportunity.Id, StageName = 'Closed Won');
		System.assertEquals([SELECT Id, IsWon FROM Opportunity WHERE Id = :TestUtils.theOpportunity.Id].IsWon, true, 'Opp is Closed Won');

		// prepare the request
		String accountId = 'dummy account id';
		List<SFCC_OrderApi.RequestWrapper> requestWrapperList = new List<SFCC_OrderApi.RequestWrapper>{
			new SFCC_OrderApi.RequestWrapper(validOrderNumber, accountId)
		};
		setRequest(JSON.serialize(requestWrapperList));

		// set the custom metadata
		SFCC_OrderApi.orderStatusMetaDataList = new List<COM_B2B_Online_Order_Status__mdt>{
			new COM_B2B_Online_Order_Status__mdt(
				DeveloperName = 'In_Process_Order_Initializing_Status',
				MasterLabel = 'In Process Order Initializing Status',
				Commerce_Cloud_Status__c = 'In process',
				Source_Object__c = 'Order__c',
				Source_Field__c = 'ProvisioningProcessStatus__c',
				Value__c = 'Initializing'
			)
		};

		Test.startTest();

		SFCC_OrderApi.getOrderStatus();

		Test.stopTest();

		System.assertEquals(SFCC_OrderApi.ORDER_API_REQUEST_SUCCESS_CODE, RestContext.response.statusCode);
		String responseBody = RestContext.response.responseBody.toString();
		System.assertNotEquals(null, responseBody);

		List<SFCC_OrderApi.ResponseWrapper> responseWrapperList = (List<SFCC_OrderApi.ResponseWrapper>) JSON.deserialize(
			responseBody,
			List<SFCC_OrderApi.ResponseWrapper>.class
		);
		System.assertEquals(1, responseWrapperList.size());

		System.assertEquals(validOrderNumber, responseWrapperList[0].orderNumber);
		System.assertEquals(accountId, responseWrapperList[0].accountId);
		System.assertEquals('In process', responseWrapperList[0].orderStatus);
		System.assertNotEquals(null, responseWrapperList[0].owner);
		System.assertEquals(true, responseWrapperList[0].result.isSuccess);
		System.assertEquals(null, responseWrapperList[0].result.message);
	}

	@IsTest
	static void test_opportunity_status_in_treatment_covers() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		// setup test data
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Test Basket');
		basket.Primary__c = true;
		basket.cscfga__Basket_Status__c = 'Valid';
		insert basket;

		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		System.assertNotEquals(null, ot.Id);

		TestUtils.autoCommit = false;

		csb2c__Inbound_Ecommerce_Order_Request__c ecommerceOrderRequest = new csb2c__Inbound_Ecommerce_Order_Request__c(
			csb2c__Product_Basket__c = basket.id,
			csb2c__API_Order_Reference__c = validOrderNumber
		);
		insert ecommerceOrderRequest;

		// prepare the request
		String accountId = 'dummy account id';
		List<SFCC_OrderApi.RequestWrapper> requestWrapperList = new List<SFCC_OrderApi.RequestWrapper>{
			new SFCC_OrderApi.RequestWrapper(validOrderNumber, accountId)
		};
		setRequest(JSON.serialize(requestWrapperList));

		// set the custom metadata
		SFCC_OrderApi.orderStatusMetaDataList = new List<COM_B2B_Online_Order_Status__mdt>{
			new COM_B2B_Online_Order_Status__mdt(
				DeveloperName = 'In_Process_Order_Initializing_Status',
				MasterLabel = 'In Process Order Initializing Status',
				Commerce_Cloud_Status__c = 'In process',
				Source_Object__c = 'Order__c',
				Source_Field__c = 'ProvisioningProcessStatus__c',
				Value__c = 'Initializing'
			),
			new COM_B2B_Online_Order_Status__mdt(
				DeveloperName = 'In_Treatment_Covers_Status',
				MasterLabel = 'In Treatment Covers Status',
				Commerce_Cloud_Status__c = 'In treatment covers',
				Source_Object__c = 'Opportunity',
				Source_Field__c = 'IsClosed',
				Value__c = 'false'
			)
		};

		System.assertEquals([SELECT Id, IsWon FROM Opportunity WHERE Id = :opp.Id].IsWon, false);

		Test.startTest();

		SFCC_OrderApi.getOrderStatus();

		Test.stopTest();

		System.assertEquals(SFCC_OrderApi.ORDER_API_REQUEST_SUCCESS_CODE, RestContext.response.statusCode);
		String responseBody = RestContext.response.responseBody.toString();
		System.assertNotEquals(null, responseBody);

		List<SFCC_OrderApi.ResponseWrapper> responseWrapperList = (List<SFCC_OrderApi.ResponseWrapper>) JSON.deserialize(
			responseBody,
			List<SFCC_OrderApi.ResponseWrapper>.class
		);
		System.assertEquals(1, responseWrapperList.size());

		System.assertEquals(validOrderNumber, responseWrapperList[0].orderNumber);
		System.assertEquals(accountId, responseWrapperList[0].accountId);
		System.assertEquals('In treatment covers', responseWrapperList[0].orderStatus);
		System.assertNotEquals(null, responseWrapperList[0].owner);

		System.assertEquals(true, responseWrapperList[0].result.isSuccess);
		System.assertEquals(null, responseWrapperList[0].result.message);
	}

	@IsTest
	static void test_opportunity_status_cancelled() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		// setup test data
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Test Basket');
		basket.Primary__c = true;
		basket.cscfga__Basket_Status__c = 'Valid';
		insert basket;

		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		System.assertNotEquals(null, ot.Id);

		TestUtils.autoCommit = false;

		csb2c__Inbound_Ecommerce_Order_Request__c ecommerceOrderRequest = new csb2c__Inbound_Ecommerce_Order_Request__c(
			csb2c__Product_Basket__c = basket.id,
			csb2c__API_Order_Reference__c = validOrderNumber
		);
		insert ecommerceOrderRequest;

		opp.StageName = 'Closed Lost';
		update opp;

		// prepare the request
		String accountId = 'dummy account id';
		List<SFCC_OrderApi.RequestWrapper> requestWrapperList = new List<SFCC_OrderApi.RequestWrapper>{
			new SFCC_OrderApi.RequestWrapper(validOrderNumber, accountId)
		};
		setRequest(JSON.serialize(requestWrapperList));

		// set the custom metadata
		SFCC_OrderApi.orderStatusMetaDataList = new List<COM_B2B_Online_Order_Status__mdt>{
			new COM_B2B_Online_Order_Status__mdt(
				DeveloperName = 'Cancelled_Order_Closed_Lost_Status',
				MasterLabel = 'Cancelled Order Closed Lost Status',
				Commerce_Cloud_Status__c = 'cancelled',
				Source_Object__c = 'Opportunity',
				Source_Field__c = 'StageName',
				Value__c = 'Closed Lost'
			)
		};

		System.assertEquals([SELECT Id, IsWon FROM Opportunity WHERE Id = :opp.Id].IsWon, false);

		Test.startTest();

		SFCC_OrderApi.getOrderStatus();

		Test.stopTest();

		System.assertEquals(SFCC_OrderApi.ORDER_API_REQUEST_SUCCESS_CODE, RestContext.response.statusCode);
		String responseBody = RestContext.response.responseBody.toString();
		System.assertNotEquals(null, responseBody);

		List<SFCC_OrderApi.ResponseWrapper> responseWrapperList = (List<SFCC_OrderApi.ResponseWrapper>) JSON.deserialize(
			responseBody,
			List<SFCC_OrderApi.ResponseWrapper>.class
		);
		System.assertEquals(1, responseWrapperList.size());

		System.assertEquals(validOrderNumber, responseWrapperList[0].orderNumber);
		System.assertEquals(accountId, responseWrapperList[0].accountId);
		System.assertEquals('cancelled', responseWrapperList[0].orderStatus);
		System.assertNotEquals(null, responseWrapperList[0].owner);
		System.assertEquals(true, responseWrapperList[0].result.isSuccess);
		System.assertEquals(null, responseWrapperList[0].result.message);
	}

	private static void setRequest(String requestBody) {
		RestRequest req = new RestRequest();
		req.requestURI = '/orders/getStatus';
		req.httpMethod = 'POST';
		RestContext.request = req;
		req.requestBody = Blob.valueOf(requestBody);
		RestResponse res = new RestResponse();
		RestContext.response = res;
	}
}
