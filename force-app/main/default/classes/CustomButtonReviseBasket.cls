global with sharing class CustomButtonReviseBasket extends csbb.CustomButtonExt {

    public String performAction (String basketId) {

        Id profileId = UserInfo.getProfileId();
        String profileName = [Select Id, Name from Profile where Id =: profileId].Name;
        String retValue = '';

        List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>([
            SELECT Id, Name, cscfga__Basket_Status__c, Basket_approval_status__c, Basket_qualification__c, Mobile_Usage_Available__c
            FROM cscfga__Product_Basket__c
            WHERE Id = :basketId
        ]);

        String redirectUrl = '/' + basketId;
        if (profileName == 'VF Partner Portal User') {
            redirectUrl = '/partnerportal' + redirectUrl;
        }

        // always delete agreement when basket is revised
        // basket update is executed at the method end (if not locked by approval)
        Boolean agreementDeleted = deleteBasketAgreement(basketId);
        Boolean ctnInfoDeleted = DeleteCTNInformation.deleteCtnInformation(basketId);
        basketList[0].Basket_qualification__c = 'SAC SRC';

        if (basketList[0].cscfga__Basket_Status__c == 'Pending approval') {
            try {
                appro.ApprovalUtilsController.apexRecallRecord(new Map<String, Object>{basketId => null});
                return '{"status":"ok","text":"Basket status updated!","redirectURL":"' + redirectUrl + '"}';
            } catch (Exception e) {
                return '{"status":"error","text":"Error happened while recalling record:"+e.getErrorMessage(),"redirectURL":"' + redirectUrl + '"}';
            }
        } else if (basketList[0].cscfga__Basket_Status__c == 'Pending usage' || basketList[0].cscfga__Basket_Status__c == 'Approved' || basketList[0].cscfga__Basket_Status__c == 'Contract created' || basketList[0].cscfga__Basket_Status__c == 'Rejected') {
            basketList[0].cscfga__Basket_Status__c = 'Valid';
            basketList[0].Basket_approval_status__c = null;
            basketList[0].NetProfit_Approval_Status__c = null;

            Boolean isLocked = Approval.isLocked(basketList[0].Id);
            if (!isLocked) {
                return updateBasket(basketList, redirectUrl);
            } else {
                return '{"status":"error","text":"Basket is locked!"}';
            }
        }
        return '{"status":"ok","text":"No basket status update applicable.","redirectURL":"' + redirectUrl + '"}';
    }


    private String updateBasket(List<cscfga__Product_Basket__c> basketList, String redirectUrl) {
        Boolean updateOk = false;
        String errorMesssage = '';

        Database.SaveResult[] srList =  Database.update(basketList, true);

        for (Database.SaveResult sr : srList) {
            if (sr.isSuccess()) {
                updateOk = true;
            } else {
                updateOk = false;
                for (Database.Error err : sr.getErrors()) {
                    errorMesssage = err.getStatusCode() + ': ' + err.getMessage();
                }
            }
        }

        if (updateOk) {
            return '{"status":"ok","text":"Basket status updated!","redirectURL":"'+redirectUrl+'"}';
        } else {
            return '{"status":"error","text":"ERROR: "' + errorMesssage + '}';
        }
    }

    private Boolean deleteBasketAgreement(Id basketId) {
       List<csclm__Agreement__c> basketAgreements = [SELECT Id, Product_Basket__c from csclm__Agreement__c where Product_Basket__c = :basketId];

       if (basketAgreements!=null) {
           try{
               delete basketAgreements;
               return true;
           }
           catch(exception e) {
               return false;
           }
       }
       else{
           return false;
       }

    }
}