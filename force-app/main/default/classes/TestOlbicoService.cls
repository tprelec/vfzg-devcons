@isTest
public class TestOlbicoService {
	@isTest
	static void testMakeRequestRestJson() {
		// Set mock callout class
		Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout());
		// This causes a fake response to be sent
		// from the class that implements HttpCalloutMock.
		OlbicoServiceJSON testClass = new OlbicoServiceJSON();
		//create OlbicoSettings__c mapping
		List<OlbicoSettings__c> olbicoSettings = new List<OlbicoSettings__c>();
		olbicoSettings.add(
			new OlbicoSettings__c(
				Olbico_JSon_Endpoint__c = 'https://api.dqbox.com/api/search',
				Olbico_Json_Username__c = 'usr_vod2325sitf',
				Olbico_Json_Password__c = 'testpass',
				Olbico_Json_BAG_Streets__c = 'ef62f95b-30fc-4520-9be1-8af222e57ef6'
			)
		);
		insert olbicoSettings;
		//create BAG_Mapping__c mapping
		List<BAG_Mapping__c> bagmappingList = new List<BAG_Mapping__c>();
		bagmappingList.add(new BAG_Mapping__c(Account_Field__c = 'Name', Name = 'bedrijfsnaam'));
		bagmappingList.add(new BAG_Mapping__c(Account_Field__c = 'NumberOfEmployees', Name = 'aantalmedewerkers'));
		bagmappingList.add(new BAG_Mapping__c(Account_Field__c = 'KVK_number__c', Name = 'kvk_8'));
		bagmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'FirstName', Name = 'bevoegd_functionaris_voornaam'));
		bagmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'LastName', Name = 'bevoegd_functionaris_achternaam'));
		bagmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'MobilePhone', Name = 'mobielnummer'));
		insert bagmappingList;
		test.startTest();
		testClass.setRequestRestJson('12345678'); //Param is a kvk code
		testClass.makeReqestRestJson('12345678'); //Param is a kvk code
		test.stopTest();
	}
	@isTest
	static void testMakeRequestRestJsonStreet() {
		// Set mock callout class
		Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout());
		// This causes a fake response to be sent
		// from the class that implements HttpCalloutMock.
		OlbicoServiceJSON testClass = new OlbicoServiceJSON();
		//create OlbicoSettings__c mapping
		List<OlbicoSettings__c> olbicoSettings = new List<OlbicoSettings__c>();
		olbicoSettings.add(
			new OlbicoSettings__c(
				Olbico_JSon_Endpoint__c = 'https://api.dqbox.com/api/search',
				Olbico_Json_Username__c = 'usr_vod2325sitf',
				Olbico_Json_Password__c = 'testpass',
				Olbico_Json_BAG_Streets__c = 'ef62f95b-30fc-4520-9be1-8af222e57ef6'
			)
		);
		insert olbicoSettings;
		test.startTest();
		testClass.setRequestRestJsonStreet('1234ab'); //Param is a zip code
		testClass.makeRequestRestJsonStreet('1234ab'); //Param is a zip code
		test.stopTest();
	}

	@isTest
	static void testMakeRequestRestJsonUpdate() {
		// Set mock callout class
		Test.setMock(HttpCalloutMock.class, new TestUtilMockCallout());
		// This causes a fake response to be sent
		// from the class that implements HttpCalloutMock.
		OlbicoServiceJSON testClass = new OlbicoServiceJSON();
		//create OlbicoSettings__c mapping
		List<OlbicoSettings__c> olbicoSettings = new List<OlbicoSettings__c>();
		olbicoSettings.add(
			new OlbicoSettings__c(
				Olbico_JSon_Endpoint__c = 'https://api.dqbox.com/api/search',
				Olbico_Json_Username__c = 'usr_vod2325sitf',
				Olbico_Json_Password__c = 'testpass',
				Olbico_Json_BAG_Streets__c = 'ef62f95b-30fc-4520-9be1-8af222e57ef6'
			)
		);
		insert olbicoSettings;
		//create BAG_Mapping__c mapping
		List<BAG_Mapping__c> bagmappingList = new List<BAG_Mapping__c>();
		bagmappingList.add(new BAG_Mapping__c(Account_Field__c = 'Name', Name = 'bedrijfsnaam'));
		bagmappingList.add(new BAG_Mapping__c(Account_Field__c = 'NumberOfEmployees', Name = 'aantalmedewerkers'));
		bagmappingList.add(new BAG_Mapping__c(Account_Field__c = 'KVK_number__c', Name = 'kvk_8'));
		bagmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'FirstName', Name = 'bevoegd_functionaris_voornaam'));
		bagmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'LastName', Name = 'bevoegd_functionaris_achternaam'));
		bagmappingList.add(new BAG_Mapping__c(Contact_Field__c = 'MobilePhone', Name = 'mobielnummer'));
		insert bagmappingList;

		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		account.KVK_number__c = '12345678';
		update account;

		test.startTest();
		testClass.setRequestRestJson('12345678'); //Param is a kvk code
		testClass.makeReqestRestJsonUpdate('12345678', account.Id); //Param is a kvk code
		test.stopTest();
		System.assertEquals(true, account.Id != null, 'Account was not created correctly');
	}
}
