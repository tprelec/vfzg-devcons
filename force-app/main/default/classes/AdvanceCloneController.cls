public with sharing class AdvanceCloneController {

    @AuraEnabled
    public static AddressCloneObject getSiteAddresses(Id pcrId){
        Id accountId;
        
        Id configToCloneId;
        
        Id packageId;
        
        //get AccountId
        List<csbb__Product_Configuration_Request__c> pcr = [Select csbb__Product_Configuration__c from csbb__Product_Configuration_Request__c where Id = :pcrId];

        if(pcr.size()>0){
            configToCloneId = (Id)pcr[0].csbb__Product_Configuration__c;
        }
        
        packageId = configToCloneId;

        //get AccountId
        List<cscfga__Product_Configuration__c> pc = [Select cscfga__Product_Definition__r.RecordTypeId, Name,cscfga__Product_Basket__c, Id from cscfga__Product_Configuration__c where Id = :configToCloneId];

        if(pc.size()>0){
            Id basketId = (Id)pc[0].cscfga__Product_Basket__c;
            List<cscfga__Product_Basket__c> pb = [Select Id,cscfga__Opportunity__c,cscfga__Opportunity__r.AccountId from cscfga__Product_Basket__c  where Id = :basketId];

            if(pb.size()>0){
                accountId = (Id)pb[0].cscfga__Opportunity__r.AccountId;
                
                System.debug('accountId ==='+accountId);
                
                Id pcAId = findAccessConfig(pcrId);
                
                //String configType = checkIfPackageOrDefinition(pc[0]);
                if(pcAId!=null){
                    return querySites(pb[0].Id, accountId, pcAId, packageId);
                }
                else{
                    return null;
                }
            }
            else{
                return null;
            }
            
        }
        else{
            return null;
        }
        
        
    }

    @AuraEnabled
    public static String performAdvanceClone(Map<String, String> sites, String basketId, String pcrId, String referenceAccessId, String originSiteId, String originPBX, String packageConfigId){
       
        List<cscfga__Product_Configuration__c> pcList = [SELECT Number_of_SIP_Clone_Multiplicator__c,cscfga__Configuration_Status__c, cscfga__Quantity__c, RequiresReClone__c, AdvanceCloneIds__c,ClonedSiteIds__c, ClonedPBXIds__c,AdvanceCloned__c, Id, 
        ManagedInternet__c, OneNet__c, OneFixed__c, IPVPN__c from cscfga__Product_Configuration__c where Id = :referenceAccessId or Id = :packageConfigId];
        
        if(pcList.size()>1){
            Map<String,String> siteKeys = getSiteKeyMap(originSiteId, sites);
            
            Boolean advanceCloned = false;
            if(siteKeys.size()>1){
                advanceCloned = true;
            }
            
            
            Boolean quantityUpdated = false;
            
            for(cscfga__Product_Configuration__c pc: pcList){
                pc.Number_of_SIP_Clone_Multiplicator__c = siteKeys.size();
                if(String.valueOf(pc.Id) == referenceAccessId){
                    pc.ClonedSiteIds__c = getCloneSiteVal(siteKeys,originSiteId, sites);
                    pc.ClonedPBXIds__c = getClonePBXVal(siteKeys,originPBX, sites);
                    
                    pc.AdvanceCloned__c = advanceCloned;
                    
                    
                    System.debug('PBX='+pc.ClonedPBXIds__c);
                    
                    pc.RequiresReClone__c = '';

                    if(pc.ManagedInternet__c != null)
                    {
                        advanceClonePackageComponentConfig(pc.ManagedInternet__c, pc.ClonedSiteIds__c);
                    }
                    if(pc.OneNet__c != null)
                    {
                        advanceClonePackageComponentConfig(pc.OneNet__c, pc.ClonedSiteIds__c);
                    }
                    if(pc.OneFixed__c != null)
                    {
                        advanceClonePackageComponentConfig(pc.OneFixed__c, pc.ClonedSiteIds__c);
                    }
                    if(pc.IPVPN__c != null)
                    {
                        advanceClonePackageComponentConfig(pc.IPVPN__c, pc.ClonedSiteIds__c);
                    }
                }
                else if(String.valueOf(pc.Id) == packageConfigId){
                    if(pc.cscfga__Quantity__c!=siteKeys.size()){
                        pc.cscfga__Quantity__c = siteKeys.size();
                        //pc.AdvanceCloneIds__c = 'hehehehe';
                        quantityUpdated = true;
                    }
                }
            }
            if(pcList.size()>0){
                //update pcList;
                
                 //attribute value change
                String nameRequiresReclone = 'RequiresReClone';
                String attrQS = 'select name, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c = :referenceAccessId and name = :nameRequiresReclone';
                List<sObject> attrsObjects = Database.query(attrQS);
                
                List<cscfga__Attribute__c> attrListUpdate = new List<cscfga__Attribute__c>();
                
                Boolean setToValid = false;
                
                if((attrsObjects!=null) && attrsObjects.size()>0){
                    for(sObject so : attrsObjects){
                        cscfga__Attribute__c attr = (cscfga__Attribute__c)so;
                        if(attr.cscfga__Value__c == null){
                            attr.cscfga__Value__c = 'cloned';
                        }
                        else if(attr.cscfga__Value__c == 'needsReclone'){
                            attr.cscfga__Value__c = 'cloned';
                            setToValid = true;
                        }
                        
                        attrListUpdate.add(attr);
                    }
                }
                
                if(attrListUpdate.size()>0){
                    update attrListUpdate;
                }
                
                
                if(setToValid){
                    //change status of access
                     for(cscfga__Product_Configuration__c pc: pcList){
                        if((String.valueOf(pc.Id) == referenceAccessId) || (String.valueOf(pc.Id) == packageConfigId)){
                            pc.cscfga__Configuration_Status__c = 'Valid';
                        }
                     }
                }
                //end attribute
                update pcList;
                
                System.debug('AN--UPDATED == '+sites);
                System.debug('AN---- -> '+pcList);
                
                if(quantityUpdated){
                    cscfga.ProductConfigurationBulkActions.calculateTotals(new Set<Id>{(Id)basketId});
                    
                    pcList = [SELECT Number_of_SIP_Clone_Multiplicator__c, cscfga__Parent_Configuration__c, Codec__c, Id, Number_of_SIP__c, cscfga__Product_Basket__c,Contract_Length__c,cscfga__Contract_Term__c, Name,cspl__Type__c,OneNet_Scenario__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId];
                    cscfga__Product_Basket__c basket = [SELECT Id, Number_of_SIP__c, Number_of_SIP_4__c, Number_of_SIP_8__c
                                                    from cscfga__Product_Basket__c where Id = :basketId];
                    CS_PCPackageSubscriber.getBasketNumOfSIP(pcList, basketId, basket);
                }
                return 'SUCCESS';
            }
            else{
                return 'Failure';
            }
        }
        else{
            return 'Failure';
        }
        
    }

    private static void advanceClonePackageComponentConfig(Id pcId, String clonedSites)
    {
        List<cscfga__Product_Configuration__c> pcList = [SELECT Name, cscfga__Quantity__c, RequiresReClone__c, AdvanceCloneIds__c,ClonedSiteIds__c, ClonedPBXIds__c,AdvanceCloned__c, Id 
        from cscfga__Product_Configuration__c where Id = :pcId];

        if(pcList.size() > 0)
        {
            pcList[0].ClonedSiteIds__c = clonedSites;
            System.debug('Advance cloned package config ' + pcList[0].Name);
        }

        update pcList;
    }
    
    private static Map<String,String> getSiteKeyMap(String originSiteId, Map<String, String> sites){
        Map<String,String> result = new Map<String,String>();
        result.put('0000', originSiteId);
        
        for(String siteId : sites.keySet()){
            result.put(keyGen(4),siteId);
        }
        
        return result;
    }

    private static String getCloneSiteVal(Map<String,String> siteKeyMap, String originSiteId, Map<String, String> sites){
        String result = '{';
        
        for(String key : siteKeyMap.keySet()){
            result += '"'+key+'":"'+siteKeyMap.get(key)+'",';
        }
        
        result = result.removeEnd(',');
        result +='}';
        
        return result;
    }
    
    
    private static String getClonePBXVal(Map<String,String> siteKeyMap, String originPBX, Map<String, String> sites){
        String result = '{';
        
        for(String key : siteKeyMap.keySet()){
            String val = sites.get(siteKeyMap.get(key));
            if(val==null || val==''){
                val='null';
            }
            
            if(key == '0000'){
                val = originPBX;
            }
            result += '"'+key+'":"'+val+'",';
        }
        
        result = result.removeEnd(',');
        result +='}';
        
        return result;
    }
    
    private static String keyGen(Integer numChar) {
        String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789abcdefghijklmnopqrstuvwxyz';
        String key = '';
        while (key.length() < numChar) {
           Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
           key += chars.substring(idx, idx+1);
        }
        return key; 
    }
    
    @AuraEnabled
    public static Id findAccessConfig(Id pcrId){
        Id pcId;
        List<csbb__Product_Configuration_Request__c> pcr = [select csbb__Product_Configuration__c from csbb__Product_Configuration_Request__c where Id = :pcrId];
        if(pcr.size()>0){
            List<cscfga__Attribute__c> attrs = [select Id from cscfga__Attribute__c where Name Like '%Access Infrastructure%' and cscfga__Product_Configuration__c = :pcr[0].csbb__Product_Configuration__c];
            System.debug('AN --->> '+attrs);
            if(attrs.size()>0){
                List<cscfga__Product_Configuration__c> pcS = [select Id from cscfga__Product_Configuration__c where cscfga__package_slot__c = :attrs[0].Id];
                if(pcS.size()>0){
                    pcId = pcS[0].Id;
                }
            }
        }
        
        return pcId;
    }

    @AuraEnabled
    public static List<FieldValues> GetDataFromConfig(String configName){
        List<CS_Advance_Clone_Configuration__c> advCloneConfField = [
            select name, value__c
            from CS_Advance_Clone_Configuration__c
            where name = :configName
        ];

        List<FieldValues> objectFields = (List<FieldValues>)JSON.deserialize(advCloneConfField[0].value__c, List<FieldValues>.class);

        List<FieldValues> result = new List<FieldValues>();
        for(FieldValues fv :objectFields){
            fv.value = '';
            FieldValues fa =  new FieldValues(fv.name, fv.label, fv.visibleInUI);
            result.add(fa);
        }
        System.debug('Field values ==');
        System.debug(result);
        return result;

    }


    @AuraEnabled
    public static AddressCloneObject querySites(Id basketId, Id accountId, Id pcId, Id packageId){

        System.debug('IN QUERY');

        List<FieldValues> fieldsToQuery = new List<FieldValues>();

        
        fieldsToQuery = GetDataFromConfig('Fields');

      
        List<FieldValues> fl = GetDataFromConfig('Fields');
        System.debug('AN---s');
        System.debug(fl);

        String fieldsToQueryString = '';

        for(FieldValues fv :fieldsToQuery){
            fieldsToQueryString+=fv.Name+',';
        }

        fieldsToQueryString = fieldsToQueryString.removeEnd(',');

        String queryString = 'select ' + fieldsToQueryString + ' from ' + 'Site_Availability__c' +  ' where Site__r.Site_Account__c = :accountId AND Usable__c = true';
        //String queryString = 'select ' + fieldsToQueryString + ' from ' + 'Site_Availability__c' +  ' where Usable__c = true';
        
        System.debug('QS ==== '+ queryString);
        
        Set<Id> sites = new Set<Id>();
        
        
        
        List<FieldValues> attributesToQuery = GetDataFromConfig('Attributes');
        
        List<cscfga__Product_Configuration__c> basketConfigs = [SELECT Id, ClonedSiteIds__c, ClonedPBXIds__c, Site_Check_SiteID__c, PBXId__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c = :basketId];
        
        List<FieldValues> comparissonAttributes = getComparissonAttributes(pcId, attributesToQuery);
        
        List<String> siteRelevantAttrNames = new List<String>();
        siteRelevantAttrNames.add('Site Check Id');
        siteRelevantAttrNames.add('PBX');
        Map<String,String> sitePbxParameters = getSiteFromOriginConfigId(pcId, siteRelevantAttrNames);
        
        System.debug('AN--->sitePbxParameters '+sitePbxParameters);
        //String originSiteId = getSiteFromOriginConfigId(pcId, 'Site Check Id');
        
        cscfga__Product_Configuration__c pcAcc;
        for(cscfga__Product_Configuration__c pcA : basketConfigs){
            if(pcA.Id == pcId){
                pcAcc = pcA;
            }
        }
        Map<String, Object> meta;
        
        if(pcAcc.ClonedSiteIds__c!=null){
            meta = (Map<String, Object>) JSON.deserializeUntyped(pcAcc.ClonedSiteIds__c);
        }
        Map<String,String> prevSiteAvSelected = new Map<String,String>();
        
        if(meta!=null){
            for(String key : meta.keySet()){
                prevSiteAvSelected.put(key, String.valueOf(meta.get(key)));
            }
        }
        System.debug('AN---->  JSON UNTY -->'+meta);
        System.debug('AN---->  JSON TYPED -->'+prevSiteAvSelected);
        
        String originSiteId = sitePbxParameters.get('Site Check Id');
        String pbxId = sitePbxParameters.get('PBX');

        System.debug('AN---->  PBXid -->'+pbxId);
        
        List<SiteDetails> siteDetails = new List<SiteDetails>();
        List<sObject> sObjects = Database.query(queryString);
        
        System.debug('FOR TEST +'+sObjects);
        
        
        for (sObject so : sObjects){
            Site_Availability__c sa = (Site_Availability__c)so;
            SiteDetails sd = new SiteDetails();
            sd.siteId = (Id)so.get('Id');
            sd.siteCId = (Id)so.get('Site__c');
            sd.siteFields = new List<FieldValues>();//fieldsToQuery;
            sd.comparissonAttributes = comparissonAttributes;
            
            sd.pbx = 'null';
            
            System.debug('---Added PBX == '+sd.pbx);
            
            for(FieldValues fv :fieldsToQuery){
                sd.siteFields.add(new FieldValues(fv.value, fv.label, fv.name, fv.visibleInUI));
            }
                
            for(FieldValues fv :sd.siteFields){
                if(!fv.name.contains('__r')){
                    fv.value = String.valueOf(sa.get(fv.name));
                }
                if(fv.name == 'Site__c'){
                    sites.add(fv.value);
                }
            }
            
            //System.debug('SD=');
            //System.debug(sd);
            siteDetails.add(sd);

        }
       
        
        
        //query sites and add those values
        if(sites.size()>0){
            
                //get PBAX
                String pbxQueryString = 'Select Id, Site__c, LastModifiedDate From Competitor_Asset__c Where Site__c in :sites AND RecordType.DeveloperName = \'PABX\'';
                List<sObject> pbxObjects = Database.query(pbxQueryString);
                
                Map<Id,List<Competitor_Asset__c>> pbxPerSite = getPBXPerSite(sites, (List<Competitor_Asset__c>) pbxObjects);
                
                //System.debug('Sites =='+sites);
                String siteQueryString = 'select Id, Name, Site_City__c, Building__c, Site_Postal_Code__c, Site_Street__c  from ' + 'Site__c' +  ' where Id in :sites';
                List<sObject> siteObjects = Database.query(siteQueryString);
                
                //System.debug(siteDetails);
         
                for(SiteDetails sd : siteDetails){
                    List<FieldValues> valToAdd = new List<FieldValues>();
                     //System.debug('New Site');
                     //System.debug(sd.siteFields);
                    for (sObject so : siteObjects){
                        Site__c s = (Site__c)so;
                        
                        if(s.Id == sd.siteCId){
                            System.debug('Equal --- '+s);
                            System.debug('sd.siteCId='+sd.siteCId);
                            for(FieldValues fv :sd.siteFields){
                                if(fv.name.contains('Site__r.Site_City__c')){
                                    fv.value = s.Site_City__c;
                                    
                                }
                                else if(fv.name.contains('Site__r.Building__c')){
                                    fv.value = s.Building__c;
                                }
                                else if(fv.name.contains('Site__r.Site_Postal_Code__c')){
                                    fv.value = s.Site_Postal_Code__c;
                                }
                                else if(fv.name.contains('Site__r.Site_Street__c')){
                                    fv.value = s.Site_Street__c;
                                }
                                else if(fv.name.contains('Site__r.Name')){
                                    fv.value = s.Name;
                                }
                            }
                        }
                    }
                    //set Applicable
                    Competitor_Asset__c ca = getNewestPBX(pbxPerSite.get(sd.siteCId));
                    sd.pbx = ca!=null ? String.valueOf(ca.Id) : 'null';
                    sd.applicable = setApplicable(sd);
                    
                    
                    //set if already selected
                    if(prevSiteAvSelected.size()>0){
                    sd.selectedPrev = setSelectedPrev(sd,prevSiteAvSelected);
                    }
                    else{
                       sd.selectedPrev = false; 
                    }
                    //setVisible
                    sd.showInUI = setVisible(sd);
                    
                    if(sd.selectedPrev){
                         sd.showInUI = sd.selectedPrev;
                    }
                    //setOrigin
                    sd.isOrigin = setOrigin(sd,originSiteId);
                    if(sd.isOrigin){
                         sd.showInUI = false;
                         sd.pbx = pbxId;
                    }
                    
                    //IF site has pbx -> cloned site also needs to have pbx
                    if(sd.pbx == 'null' && pbxId!=null){
                        sd.applicable = false;
                        
                    }
                    
                    
                    sd.configExists = setConfigExists(basketConfigs, sd);
                    
                    if(sd.configExists){
                        Id existingId = getConfigExistsId(basketConfigs, sd);
                        if(existingId!=null){
                            sd.rootPCRId = getRootPCRId(existingId);
                        }
                    }

                }
        }
        
        siteDetails.sort();
        
        AddressCloneObject adc = new AddressCloneObject(fieldsToQuery,siteDetails, originSiteId, pbxId, (String)pcId, (String)packageId);

       
        
        //System.debug('IN QUERY - OBJECT');
        //System.debug(adc);
        
        
        return adc;
    }
    
    
    
    private static Map<Id,List<Competitor_Asset__c>> getPBXPerSite(Set<Id> siteIds, List<Competitor_Asset__c> pbxS){
        Map<Id,List<Competitor_Asset__c>> result = new Map<Id,List<Competitor_Asset__c>>();
        
        //get with latest last modifies date
        for(Id siteId : siteIds){
            List<Competitor_Asset__c> sitePBX = getPBXPerSite(siteId, pbxS);
            result.put(siteId, sitePBX);
        }
        
        return result;
    }
    
    private static Competitor_Asset__c getNewestPBX(List<Competitor_Asset__c> pbxS){
        
        if(pbxS!=null && pbxS.size()>0){
            DateTime newest = pbxS[0].LastModifiedDate;
            Competitor_Asset__c newCA= pbxS[0];
            
            for(Competitor_Asset__c ca : pbxS){
                if(ca.LastModifiedDate >newest){
                    newCA = ca;
                    newest = ca.LastModifiedDate;
                }
            }
            
            return newCA;
        }
        else{
            return null;
        }
    }
    
    private static List<Competitor_Asset__c> getPBXPerSite(Id siteId, List<Competitor_Asset__c> allPBXS){
        
        List<Competitor_Asset__c> result = new List<Competitor_Asset__c>();
        
        for(Competitor_Asset__c ca : allPBXS){
            if(ca.Site__c == siteId){
                result.add(ca);
            }
        }
        return result;
    }
    
    /*
    private static String checkIfPackageOrDefinition(cscfga__Product_Configuration__c pc){
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        if(pc!=null){
            //check if package
            if(pc.cscfga__Product_Definition__r.RecordTypeId == productDefinitionRecordType){
                return 'Product Definition';
            }
            else{
                return 'Package Definition';
            }
        }
        return null;
    }
    */
    private static Map<String,String> getSiteFromOriginConfigId(Id pcId, List<String> attNames){
        List<cscfga__Attribute__c> result;
        
        Map<String,String> attrValPairs = new Map<String,String>();
        
        String attrQS = 'select Id, name, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c = :pcId and name in :attNames';
        List<sObject> attrsObjects = Database.query(attrQS);
        if((attrsObjects!=null) && attrsObjects.size()>0){
            result = (List<cscfga__Attribute__c>)attrsObjects;
            for(cscfga__Attribute__c att : result){
                attrValPairs.put(att.Name, att.cscfga__Value__c);
            }
            return attrValPairs;
        }
        else{
            return null;
        }
    }
    
    private static List<FieldValues> getComparissonAttributes(Id pcId, List<FieldValues> attrConf){//List<String> attrNames){
        
        List<String> attrNames = new List<String>();
        
        for(FieldValues fv :attrConf){
            attrNames.add(fv.name);
        }
        
        
        //List<FieldValues> comparissonAttr = new List<FieldValues>();
        
        String attrQS = 'select name, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c = :pcId and name in :attrNames';
        List<sObject> attrsObjects = Database.query(attrQS);
        
        if((attrsObjects!=null) && attrsObjects.size()>0){
            for(sObject so : attrsObjects){
                 cscfga__Attribute__c attr = (cscfga__Attribute__c)so;
                 for(FieldValues fl :attrConf){
                     if(fl.name == attr.name){
                         fl.value = attr.cscfga__Value__c;
                     }
                 }
            }
        }
        
        return attrConf;
        
    }
    
    private static Id getConfigExistsId(List<cscfga__Product_Configuration__c> configsInBasket, SiteDetails site){
        Id result;
        
        for(cscfga__Product_Configuration__c pc : configsInBasket){
            if (pc.Site_Check_SiteID__c!=null){
                if(pc.Site_Check_SiteID__c == site.siteCId){
                    return pc.Id;
                }
            }
        }
        
        return null;
    }
    
    private static Boolean setConfigExists(List<cscfga__Product_Configuration__c> configsInBasket, SiteDetails site){
        Boolean result = false;
        
        for(cscfga__Product_Configuration__c pc : configsInBasket){
            if (pc.Site_Check_SiteID__c!=null){
                if(pc.Site_Check_SiteID__c == site.siteCId){
                    result = true;
                }
            }
        }
        
        return result;
    }
    
    private static String getRootPCRId(Id referenceConfigInPackage){
        String result = '';
        
        //find PCR of referenced package
        //assumption -> Access is in a package -> this package needs to be removed and built as new
        //get package slot
        List<cscfga__Product_Configuration__c> pc =[select cscfga__package_slot__c, Id from cscfga__Product_Configuration__c  where id = :referenceConfigInPackage];
        if(pc.size()>0){
            List<cscfga__Attribute__c> attr = [select cscfga__Product_Configuration__c, Id from cscfga__Attribute__c  where id = :pc[0].cscfga__package_slot__c];
            if(attr.size()>0){
                List<csbb__Product_Configuration_Request__c> pcrList = [select Id from csbb__Product_Configuration_Request__c where csbb__Product_Configuration__c = :attr[0].cscfga__Product_Configuration__c];
                if(pcrList.size()>0){
                    result = pcrList[0].Id;
                }
            }
         }
        
        return result;
    }
    
    
    private static Boolean setOrigin(SiteDetails site, String originSiteId){
        Boolean result = false;
        
        if((site.siteId!=null)&&(originSiteId!=null)){
            if(site.siteId == originSiteId){
                result = true;
            }
        }
        
        return result;
    }
    
    
    private static Boolean setVisible(SiteDetails site){
    Boolean result = true;
        //visible
        String siteVendor = getFieldStringValue(site.siteFields,'Vendor__c');
        String configVendor = getFieldStringValue(site.comparissonAttributes,'Vendor');

        String siteAccessInfrastructure = getFieldStringValue(site.siteFields,'Access_Infrastructure__c');
        String configAccessInfrastructure = getFieldStringValue(site.comparissonAttributes,'Access Type');
        
        //vendor
        if((siteVendor!=null)&&(configVendor!=null)){
            if(siteVendor!=configVendor){
                result = false;
            }
        }

        //access
        if((siteAccessInfrastructure!=null)&&(configAccessInfrastructure!=null)){
            if(siteAccessInfrastructure!=configAccessInfrastructure){
                result = false;
            }
        }
        
        String siteRegion = getFieldStringValue(site.siteFields,'Region__c');
        String configRegion = getFieldStringValue(site.comparissonAttributes,'Region');
        //region
        //access
        if((siteRegion!=null)&&(configRegion!=null)){
            if(siteRegion!=configRegion){
                result = false;
            }
        }
        
        return result;
    }
    
    private static Boolean setSelectedPrev(SiteDetails site, Map<String,String> selectedSites){
        Boolean result = false;
        
        for(String key : selectedSites.keySet()){
            
            if(selectedSites.get(key) == site.siteId){
                result = true;
            }
        }
        
        return result;
    }
    
    
    private static Boolean setApplicable(SiteDetails site){
        Boolean result = true;
        //applicable:
        String sitePremiumApplicable = getFieldStringValue(site.siteFields,'Premium_Applicable__c');
        String configPremiumApplicable = getFieldStringValue(site.comparissonAttributes,'Premium Applicable');
        
        Decimal siteTotalUp = getFieldDecimalValue(site.siteFields,'Bandwith_Sellable_Up__c');
        Decimal siteTotalDown = getFieldDecimalValue(site.siteFields,'Bandwith_Sellable_Down__c');
        Decimal totalEntryUp = getFieldDecimalValue(site.comparissonAttributes,'Total Bandwidth Up Entry');
        Decimal totalEntryDown = getFieldDecimalValue(site.comparissonAttributes,'Total Bandwidth Down Entry');

        Decimal totalUp = getFieldDecimalValue(site.comparissonAttributes,'Total Bandwidth Up');
        Decimal totalDown = getFieldDecimalValue(site.comparissonAttributes,'Total Bandwidth Down');

        Decimal sitePremiumUp = getFieldDecimalValue(site.siteFields,'Bandwith_Up_Premium__c');
        Decimal sitePremiumDown = getFieldDecimalValue(site.siteFields,'Bandwith_Up_Premium__c');
        Decimal totalPremiumUp = getFieldDecimalValue(site.comparissonAttributes,'Total Bandwidth Up Premium');
        Decimal totalPremiumDown = getFieldDecimalValue(site.comparissonAttributes,'Total Bandwidth Down Premium');

        String siteResultCheck = getFieldStringValue(site.siteFields,'Result_Check__c');
        String configResultCheck = getFieldStringValue(site.comparissonAttributes,'Result Check');

        String siteVendor = getFieldStringValue(site.siteFields,'Vendor__c');
        String configVendor = getFieldStringValue(site.comparissonAttributes,'Vendor');


        String siteAccessInfrastructure = getFieldStringValue(site.siteFields,'Access_Infrastructure__c');
        String configAccessInfrastructure = getFieldStringValue(site.comparissonAttributes,'Access Type');


        //IF Premium Applicable 
            //--> siteBandwidthUpPremium>=configBandwidthUpPremium
            //--> siteBandwidthDownPremium>=configBandwidthDownPremium
            //--> siteBandwidthUpSelable>=configBandwidthUpEntry
            //--> siteBandwidthDownSelable>=configBandwidthDownEntry
            //--> sitePremiumAppllicable == configPremiumApplicable
            //--> sitePremiumVendor == configVendor

        if(configPremiumApplicable=='true'){

            //premium applicable
           
            //entry
            if((siteTotalUp!=null)&&(siteTotalDown!=null)&&(totalEntryUp!=null)&&(totalEntryDown!=null)){
                if((siteTotalUp<totalEntryUp) || (siteTotalDown<totalEntryDown)){
                    result = false;
                }
            }

             //premium
            if((sitePremiumUp!=null)&&(sitePremiumDown!=null)&&(totalPremiumUp!=null)&&(totalPremiumDown!=null)){
                if((sitePremiumUp<totalPremiumUp) || (sitePremiumDown<totalPremiumDown)){
                    result = false;
                }
            }

            //resultCheck
            if((siteResultCheck!=null)&&(configResultCheck!=null)){
                if(siteResultCheck!=configResultCheck){
                    result = false;
                }
            }


            //vendor
            if((siteVendor!=null)&&(configVendor!=null)){
                if(siteVendor!=configVendor){
                    result = false;
                }
            }

            //access
            if((siteAccessInfrastructure!=null)&&(configAccessInfrastructure!=null)){
                if(siteAccessInfrastructure!=configAccessInfrastructure){
                    result = false;
                }
            }


        }

        //NOT Premium Applicable
            //-->siteBandwidthUpSelable>=configBandwidthUp
            //-->siteBandwidthDownSelable>=configBandwidthDown
            //--> vendor == configVendor
        else{

            //total
            if((siteTotalUp!=null)&&(siteTotalDown!=null)&&(totalUp!=null)&&(totalDown!=null)){
                if((siteTotalUp<totalUp) || (siteTotalDown<totalDown)){
                    result = false;
                }
            }


            //result check
            if((siteResultCheck!=null)&&(configResultCheck!=null)){
                if(siteResultCheck!=configResultCheck){
                    result = false;
                }
            }
            //vendor
            if((siteVendor!=null)&&(configVendor!=null)){
                if(siteVendor!=configVendor){
                    result = false;
                }
            }

            if((siteAccessInfrastructure!=null)&&(configAccessInfrastructure!=null)){
                if(siteAccessInfrastructure!=configAccessInfrastructure){
                    result = false;
                }
            }

        }
        
        return result;
    }
    
    private static Decimal getFieldDecimalValue(List<FieldValues> siteFields, String name){
      
        FieldValues result;
        for(FieldValues fv : siteFields){
            if(fv.name == name){
               if(fv.value!=null){
                   result = fv;
               }
               else{
                   result = null;
               }
            }
            
        }
        if((result!=null)&&(result.value!=null)){
            return Decimal.valueOf(result.value);
            
        }
        else{
            return null;
        }
    }

    private static String getFieldStringValue(List<FieldValues> siteFields, String name){
      
        FieldValues result;
        for(FieldValues fv : siteFields){
            if(fv.name == name){
               if(fv.value!=null){
                   result = fv;
               }
               else{
                   result = null;
               }
            }
            
        }
        if((result!=null)&&(result.value!=null)){
            return String.valueOf(result.value);
        }
        else{
            return null;
        }
    }
}