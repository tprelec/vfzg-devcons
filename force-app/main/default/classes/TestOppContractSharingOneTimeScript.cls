@isTest
public class TestOppContractSharingOneTimeScript {
	@isTest
	static void check() {
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);

		TestUtils.createCompleteContract();

		Test.startTest();
		OppContractSharingOneTimeScript x = new OppContractSharingOneTimeScript();
		database.executeBatch(x);
		Test.stopTest();
	}
}
