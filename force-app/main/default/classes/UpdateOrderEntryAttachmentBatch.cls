public class UpdateOrderEntryAttachmentBatch implements Database.Batchable<sObject> {
	public Database.QueryLocator start(Database.BatchableContext bc) {
		return Database.getQueryLocator(
			'SELECT Id, Body FROM Attachment WHERE Name LIKE \'OrderEntryData%\' AND CreatedDate <= TODAY'
		);
	}
	public void execute(Database.BatchableContext bc, List<Attachment> records) {
		List<OE_Product__c> mainProduct = [
			SELECT Id, Type__c, Journey_Type__c, Sort_Order__c, Name
			FROM OE_Product__c
			WHERE Type__c = 'Main' AND Journey_Type__c = 'Fixed'
		];

		for (Attachment att : records) {
			OrderEntryData dataTemp = new OrderEntryData();
			dataTemp = (OrderEntryData) JSON.deserialize(att.Body.toString(), OrderEntryData.class);
			if (dataTemp.journeyType == null) {
				OrderEntryData data = new OrderEntryData();
				OrderEntryDataOld oldData = new OrderEntryDataOld();
				oldData = (OrderEntryDataOld) JSON.deserialize(
					att.Body.toString(),
					OrderEntryDataOld.class
				);
				data.accountId = oldData.accountId;
				data.b2cInternetCustomer = oldData.b2cInternetCustomer;
				data.opportunityId = oldData.opportunityId;
				data.primaryContactId = oldData.primaryContactId;
				data.lastStep = oldData.lastStep;
				data.mainProduct = mainProduct.size() > 0
					? (OE_Product__c) JSON.deserialize(
							JSON.serialize(
								new OE_Product__c(
									Id = mainProduct[0].Id,
									Type__c = mainProduct[0].Type__c,
									Journey_Type__c = mainProduct[0].Journey_Type__c,
									Sort_Order__c = mainProduct[0].Sort_Order__c,
									Name = mainProduct[0].Name
								)
							),
							OE_Product__c.class
					  )
					: null;
				OrderEntryData.OrderEntrySite site = new OrderEntryData.OrderEntrySite();
				site.siteId = oldData.siteId;
				site.type = 'Installation';
				site.siteCheck = (OrderEntryData.SiteCheck) JSON.deserialize(
					JSON.serialize(oldData.siteCheck),
					OrderEntryData.SiteCheck.class
				);
				site.addressCheckResult = oldData.addressCheckResult;
				site.offNetType = oldData.offNetType;
				List<OrderEntryData.OrderEntrySite> sites = new List<OrderEntryData.OrderEntrySite>();
				sites.add(site);
				data.sites = sites;
				OrderEntryData.OrderEntryBundle bundle = new OrderEntryData.OrderEntryBundle();
				bundle.type = 'Fixed';
				bundle.bundle = (OrderEntryData.ProductBundle) JSON.deserialize(
					JSON.serialize(oldData.bundle),
					OrderEntryData.ProductBundle.class
				);
				bundle.contractTerm = oldData.contractTerm;
				bundle.addons = (List<OrderEntryData.OrderEntryAddon>) JSON.deserialize(
					JSON.serialize(oldData.addons),
					List<OrderEntryData.OrderEntryAddon>.class
				);
				bundle.promos = (List<OrderEntryData.Promotion>) JSON.deserialize(
					JSON.serialize(oldData.promos),
					List<OrderEntryData.Promotion>.class
				);
				bundle.products = (List<OrderEntryData.OrderEntryProduct>) JSON.deserialize(
					JSON.serialize(oldData.products),
					List<OrderEntryData.OrderEntryProduct>.class
				);

				Map<String, OrderEntryData.Telephony> telephony = new Map<String, OrderEntryData.Telephony>();
				Map<String, Object> tempTelephony = (Map<String, Object>) JSON.deserializeUntyped(
					JSON.serialize(oldData.telephony)
				);
				if (tempTelephony != null) {
					for (String key : tempTelephony.keySet()) {
						telephony.put(
							key,
							(OrderEntryData.Telephony) JSON.deserialize(
								JSON.serialize(tempTelephony.get(key)),
								OrderEntryData.Telephony.class
							)
						);
					}
					bundle.telephony = telephony;
				} else {
					bundle.telephony = new Map<String, OrderEntryData.Telephony>();
				}

				bundle.operatorSwitch = (OrderEntryData.operatorSwitch) JSON.deserialize(
					JSON.serialize(oldData.operatorSwitch),
					OrderEntryData.operatorSwitch.class
				);
				bundle.installation = (OrderEntryData.Installation) JSON.deserialize(
					JSON.serialize(oldData.installation),
					OrderEntryData.Installation.class
				);
				List<OrderEntryData.OrderEntryBundle> bundles = new List<OrderEntryData.OrderEntryBundle>();
				bundles.add(bundle);
				data.bundles = bundles;
				data.payment = (OrderEntryData.Payment) JSON.deserialize(
					JSON.serialize(oldData.payment),
					OrderEntryData.Payment.class
				);
				data.notes = oldData.notes;
				data.journeyType = 'Fixed';
				data.init();

				att.Body = Blob.valueOf(JSON.serializePretty(data, true));
			}
		}

		update records;
	}

	public void finish(Database.BatchableContext bc) {
	}
}
