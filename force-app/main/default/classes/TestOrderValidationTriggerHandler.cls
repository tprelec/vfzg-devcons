/**
 * @description       : Apex Test Class for the OrderValidationTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   20-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestOrderValidationTriggerHandler {
	@testSetup
	static void setup() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'Order Validation',
			External_Number__c = 1,
			Object_Prefix__c = 'OV-22-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;
	}

	@isTest
	static void testSetExternalIds() {
		Order_Validation__c objOrderValidation = new Order_Validation__c(
			Active__c = true,
			API_Fieldname__c = 'BAN_Name__c',
			Error_message__c = 'Test',
			Export_System__c = 'SIAS',
			Name = 'BAN Name - test1',
			Object__c = 'Ban__c',
			Operator__c = 'isblank',
			Proposition__c = 'One Fixed;One Net;Numberporting;One Fixed Express;IPVPN;Legacy;Internet;Mobile',
			Who__c = 'Inside Sales;'
		);

		Test.startTest();
		insert objOrderValidation;
		Test.stopTest();

		Order_Validation__c objOrderValidationUpdated = [SELECT Id, ExternalID__c FROM Order_Validation__c WHERE Id = :objOrderValidation.Id LIMIT 1];

		System.assertEquals('OV-22-000002', objOrderValidationUpdated.ExternalID__c, 'The External ID on Order Validation was not set correctly.');
	}
}
