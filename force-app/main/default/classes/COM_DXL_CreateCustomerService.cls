public with sharing class COM_DXL_CreateCustomerService {
	public List<Opportunity> opportunityData;
	public Set<Id> siteIds;
	public List<Site__c> sites;
	public List<External_Site__c> externalSites;
	public List<External_Contact__c> externalContacts;
	public List<External_Account__c> externalAccounts;
	public Map<Id, List<External_Site__c>> sitesWithExternalSites;
	public Map<Id, List<External_Contact__c>> contactsWithExternalContacts;
	public Map<Id, List<External_Account__c>> accountsWithExternalAccounts;
	public List<COM_DxlAffectedObjects> dxlAffectedObjects;
	public List<Contact_Role__c> contactRoles;

	public COM_DXL_CreateCustomerService(Id opportunityId, Id orderId) {
		this.opportunityData = getOpportunityData(opportunityId);
		this.contactRoles = getContactRoles(this.opportunityData[0]);
		this.siteIds = getSiteIds(opportunityId);
		this.sites = getSites(this.siteIds);
		this.externalSites = getExternalSites(this.siteIds);
		this.sitesWithExternalSites = getSiteExternalSites(externalSites);
		this.externalContacts = getExternalContacts(getOpportunityContacts(this.opportunityData[0], this.contactRoles));
		this.contactsWithExternalContacts = getContactExternalContacts(externalContacts);
		this.externalAccounts = getExternalAccounts(new Set<Id>{ this.opportunityData[0].AccountId });
		this.accountsWithExternalAccounts = getAccountExternalAccounts(externalAccounts);
		this.dxlAffectedObjects = getAffectedObjects(this.opportunityData[0], this.contactRoles, orderId);
	}

	@TestVisible
	private List<Opportunity> getOpportunityData(Id opportunityId) {
		List<Opportunity> opportunityData = [
			SELECT
				Id,
				AccountId,
				Account.KVK_number__c,
				Account.date_of_establishment__c,
				Account.Legal_Structure_Code__c,
				Account.NumberOfEmployees,
				Account.Name,
				Account.Unify_Account_Type__c,
				Account.Unify_Account_SubType__c,
				Account.Fax,
				Account.duns_number__c,
				Account.BOPCode__c,
				BAN__c,
				BAN__r.BAN_Name__c,
				BAN__r.BAN_Number__c,
				BAN__r.Unify_Ref_Id__c,
				BAN__r.Unify_Customer_Type__c,
				BAN__r.Unify_Customer_SubType__c,
				Account.Visiting_Postal_Code__c,
				Account.Visiting_HouseNumber1__c,
				Account.Visiting_HouseNumber_Suffix__c,
				Account.Visiting_street__c,
				Account.Visiting_City__c,
				Account.Visiting_Country__c,
				Account.Unify_Dealer_Market_Indicator__c,
				Account.Fixed_Dealer__c,
				Account.Fixed_Dealer__r.Contact__r.UserId__r.UserType,
				Account.Fixed_Dealer__r.Contact__r.UserId__r.Name,
				Account.Fixed_Dealer__r.Contact__r.UserId__r.Manager.Name,
				Account.Fixed_Dealer__r.Contact__r.Account.BOPCode__c,
				Account.Mobile_Dealer__c,
				Account.Mobile_Dealer__r.Contact__r.UserId__r.UserType,
				Account.Mobile_Dealer__r.Contact__r.UserId__r.Name,
				Account.Mobile_Dealer__r.Contact__r.UserId__r.Manager.Name,
				Account.Mobile_Dealer__r.Contact__r.Account.BOPCode__c,
				Account.Mobile_Dealer__r.Dealer_Code__c,
				Account.Owner.UserType,
				Account.Owner.Name,
				Account.Owner.Manager.Name,
				Account.Owner.Account.BOPCode__c,
				Account.Authorized_to_Sign_1st__c,
				Account.Authorized_to_Sign_1st__r.Gender__c,
				Account.Authorized_to_Sign_1st__r.Email,
				Account.Authorized_to_Sign_1st__r.Phone,
				Account.Authorized_to_Sign_1st__r.MobilePhone,
				Account.Authorized_to_Sign_1st__r.BirthDate,
				Account.Authorized_to_Sign_1st__r.FirstName,
				Account.Authorized_to_Sign_1st__r.LastName,
				Account.Authorized_to_Sign_1st__r.Document_Type__c,
				Account.Authorized_to_Sign_1st__r.Document_Number__c,
				Account.Authorized_to_Sign_1st__r.Jurisdiction_Type__c,
				Account.Authorized_to_Sign_1st__r.AccountId,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Postal_Code__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Housenumber1__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Housenumber_Suffix__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Street__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_City__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Country__c,
				Financial_Account__c,
				Financial_Account__r.Payment_Method__c,
				Financial_Account__r.Financial_Contact__r.Name,
				Financial_Account__r.Unify_Ref_Id__c,
				Financial_Account__r.Bank_Account_Name__c,
				Financial_Account__r.Bank_Account_Number__c,
				Financial_Account__r.Financial_Contact__c,
				Financial_Account__r.Financial_Contact__r.Gender__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Postal_Code__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Housenumber1__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Housenumber_Suffix__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Street__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_City__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Country__c,
				Financial_Account__r.Financial_Contact__r.Email,
				Financial_Account__r.Financial_Contact__r.Phone,
				Financial_Account__r.Financial_Contact__r.MobilePhone,
				Financial_Account__r.Financial_Contact__r.Birthdate,
				Financial_Account__r.Financial_Contact__r.FirstName,
				Financial_Account__r.Financial_Contact__r.LastName,
				Financial_Account__r.Financial_Contact__r.AccountId,
				Financial_Account__r.Financial_Contact__r.Document_Type__c,
				Financial_Account__r.Financial_Contact__r.Document_Number__c,
				Financial_Account__r.Financial_Contact__r.Jurisdiction_Type__c,
				Billing_Arrangement__c,
				Billing_Arrangement__r.Unify_Ref_Id__c,
				Billing_Arrangement__r.Billing_Arrangement_Alias__c,
				Billing_Arrangement__r.Bill_Format__c,
				Billing_Arrangement__r.Billing_Postal_Code__c,
				Billing_Arrangement__r.Billing_Housenumber__c,
				Billing_Arrangement__r.Billing_Housenumber_Suffix__c,
				Billing_Arrangement__r.Billing_Street__c,
				Billing_Arrangement__r.Billing_City__c,
				Billing_Arrangement__r.Billing_Country__c,
				Contact__c,
				Contact__r.Account.Visiting_Postal_Code__c,
				Contact__r.Account.Visiting_Housenumber1__c,
				Contact__r.Account.Visiting_Housenumber_Suffix__c,
				Contact__r.Account.Visiting_Street__c,
				Contact__r.Account.Visiting_City__c,
				Contact__r.Account.Visiting_Country__c,
				Contact__r.Email,
				Contact__r.Phone,
				Contact__r.MobilePhone,
				Contact__r.BirthDate,
				Contact__r.Document_Type__c,
				Contact__r.Document_Number__c,
				Contact__r.Jurisdiction_Type__c,
				Contact__r.Contact_Role__c,
				Contact__r.FirstName,
				Contact__r.LastName,
				Contact__r.AccountId
			FROM Opportunity
			WHERE Id = :opportunityId
		];
		return opportunityData;
	}

	@TestVisible
	private Set<Id> getSiteIds(Id opportunityId) {
		Set<Id> siteIds = new Set<Id>();
		List<OpportunityLineItem> opportunityProducts = [
			SELECT Id, Name, OpportunityId, Location__c
			FROM OpportunityLineItem
			WHERE OpportunityId = :opportunityId
		];

		for (OpportunityLineItem oli : opportunityProducts) {
			siteIds.add(oli.Location__c);
		}
		return siteIds;
	}

	@TestVisible
	private List<Site__c> getSites(Set<Id> siteIds) {
		List<Site__c> sites = [
			SELECT
				Id,
				Site_Street__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Site_Postal_Code__c,
				Site_City__c,
				Country__c,
				Location_Alias__c,
				Unify_Site_Name__c,
				Canvas_Street__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_City__c,
				Canvas_Country__c,
				SLA__r.BOP_Code__c,
				Location_Type__c,
				Site_Phone__c,
				Building__c,
				Site_Account__c
			FROM Site__c
			WHERE Id IN :siteIds
		];

		return sites;
	}

	@TestVisible
	private List<External_Account__c> getExternalAccounts(Set<Id> accountIds) {
		List<External_Account__c> externalAccounts = [
			SELECT Id, External_Source__c, External_Account_Id__c, Account__c
			FROM External_Account__c
			WHERE Account__c IN :accountIds
		];

		return externalAccounts;
	}

	@TestVisible
	private Map<Id, List<External_Account__c>> getAccountExternalAccounts(List<External_Account__c> externalAccounts) {
		Map<Id, List<External_Account__c>> accountsWithExternalAccounts = new Map<Id, List<External_Account__c>>();

		for (External_Account__c externalAccount : externalAccounts) {
			if (accountsWithExternalAccounts.get(externalAccount.Account__c) != null) {
				accountsWithExternalAccounts.get(externalAccount.Account__c).add(externalAccount);
			} else {
				accountsWithExternalAccounts.put(externalAccount.Account__c, new List<External_Account__c>{ externalAccount });
			}
		}

		return accountsWithExternalAccounts;
	}

	@TestVisible
	private List<External_Contact__c> getExternalContacts(Set<Id> contactIds) {
		List<External_Contact__c> externalContacts = [
			SELECT Id, External_Source__c, External_Contact_Id__c, Contact__c, ExternalId__c
			FROM External_Contact__c
			WHERE Contact__c IN :contactIds
		];

		return externalContacts;
	}

	@TestVisible
	private Map<Id, List<External_Contact__c>> getContactExternalContacts(List<External_Contact__c> externalContacts) {
		Map<Id, List<External_Contact__c>> contactsWithExternalContacts = new Map<Id, List<External_Contact__c>>();

		for (External_Contact__c externalContact : externalContacts) {
			if (contactsWithExternalContacts.get(externalContact.Contact__c) != null) {
				contactsWithExternalContacts.get(externalContact.Contact__c).add(externalContact);
			} else {
				contactsWithExternalContacts.put(externalContact.Contact__c, new List<External_Contact__c>{ externalContact });
			}
		}

		return contactsWithExternalContacts;
	}

	@TestVisible
	private List<External_Site__c> getExternalSites(Set<Id> siteIds) {
		List<External_Site__c> externalSites = [
			SELECT Id, External_Source__c, External_Site_Id__c, Site__c
			FROM External_Site__c
			WHERE Site__c IN :siteIds
		];

		return externalSites;
	}

	@TestVisible
	private Map<Id, List<External_Site__c>> getSiteExternalSites(List<External_Site__c> externalSites) {
		Map<Id, List<External_Site__c>> sitesWithExternalSitesMap = new Map<Id, List<External_Site__c>>();

		for (External_Site__c es : externalSites) {
			if (sitesWithExternalSitesMap.get(es.Site__c) != null) {
				sitesWithExternalSitesMap.get(es.Site__c).add(es);
			} else {
				sitesWithExternalSitesMap.put(es.Site__c, new List<External_Site__c>{ es });
			}
		}

		return sitesWithExternalSitesMap;
	}

	public String generateDxlCreateCustomerPayload() {
		String payloadString = '';
		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();

		COM_DXL_CustomerDetails customerDetails = new COM_DXL_CustomerDetails(generator, this.opportunityData, this.accountsWithExternalAccounts);
		customerDetails.generateCustomerDetails();

		COM_DXL_BillingEntities billingEntities = new COM_DXL_BillingEntities(generator, this.opportunityData);
		billingEntities.generateBillingEntities();

		COM_DXL_Contacts contacts = new COM_DXL_Contacts(generator, this.opportunityData, this.contactsWithExternalContacts, this.contactRoles);
		contacts.generateContacts();

		COM_DXL_Sites dxlSites = new COM_DXL_Sites(generator, this.sites, this.sitesWithExternalSites);
		dxlSites.generateSites();

		generator.writeEndObject();
		payloadString = generator.getAsString();
		return payloadString;
	}

	@TestVisible
	private Set<Id> getOpportunityContacts(Opportunity opportunityRecord, List<Contact_Role__c> contactRoles) {
		Set<Id> contactIds = new Set<Id>();
		contactIds.add(opportunityRecord.Account.Authorized_to_Sign_1st__c);
		contactIds.add(opportunityRecord.Financial_Account__r.Financial_Contact__c);
		for (Contact_Role__c contactRoleRecord : contactRoles) {
			contactIds.add(contactRoleRecord.Contact__c);
		}
		return contactIds;
	}

	@TestVisible
	private List<COM_DxlAffectedObjects> getAffectedObjects(Opportunity opportunityData, List<Contact_Role__c> contactRoles, Id orderId) {
		List<COM_DxlAffectedObjects> returnValue = new List<COM_DxlAffectedObjects>();

		COM_DxlAffectedObjects affectedOrder = new COM_DxlAffectedObjects();
		affectedOrder.type = COM_DXL_Constants.COM_DXL_ORDER_API_NAME;
		affectedOrder.sfdcId = String.valueOf(orderId);
		returnValue.add(affectedOrder);

		COM_DxlAffectedObjects affectedAccount = new COM_DxlAffectedObjects();
		affectedAccount.type = COM_DXL_Constants.COM_DXL_ACCOUNT_API_NAME;
		affectedAccount.sfdcId = String.valueOf(opportunityData.AccountId);
		returnValue.add(affectedAccount);

		COM_DxlAffectedObjects affectedBAN = new COM_DxlAffectedObjects();
		affectedBAN.type = COM_DXL_Constants.COM_DXL_BAN_API_NAME;
		affectedBAN.sfdcId = String.valueOf(opportunityData.BAN__c);
		returnValue.add(affectedBAN);

		COM_DxlAffectedObjects affectedFinancialAccount = new COM_DxlAffectedObjects();
		affectedFinancialAccount.type = COM_DXL_Constants.COM_DXL_FINANCIAL_ACCOUNT_API_NAME;
		affectedFinancialAccount.sfdcId = String.valueOf(opportunityData.Financial_Account__c);
		returnValue.add(affectedFinancialAccount);

		COM_DxlAffectedObjects affectedBillingArrangement = new COM_DxlAffectedObjects();
		affectedBillingArrangement.type = COM_DXL_Constants.COM_DXL_BILLING_ARRANGEMENT_API_NAME;
		affectedBillingArrangement.sfdcId = String.valueOf(opportunityData.Billing_Arrangement__c);
		returnValue.add(affectedBillingArrangement);

		for (Site__c tmpSite : this.sites) {
			COM_DxlAffectedObjects affectedSite = new COM_DxlAffectedObjects();
			affectedSite.type = COM_DXL_Constants.COM_DXL_SITE_API_NAME;
			affectedSite.sfdcId = String.valueOf(tmpSite.Id);
			returnValue.add(affectedSite);
		}

		if (opportunityData.Account.Authorized_to_Sign_1st__c != null) {
			COM_DxlAffectedObjects affectedDirectorContact = new COM_DxlAffectedObjects();
			affectedDirectorContact.type = COM_DXL_Constants.COM_DXL_CONTACT_API_NAME;
			affectedDirectorContact.sfdcId = String.valueOf(opportunityData.Account.Authorized_to_Sign_1st__c);
			returnValue.add(affectedDirectorContact);
		}

		if (opportunityData.Financial_Account__r.Financial_Contact__c != null) {
			COM_DxlAffectedObjects affectedFinancialContact = new COM_DxlAffectedObjects();
			affectedFinancialContact.type = COM_DXL_Constants.COM_DXL_CONTACT_API_NAME;
			affectedFinancialContact.sfdcId = String.valueOf(opportunityData.Financial_Account__r.Financial_Contact__c);
			returnValue.add(affectedFinancialContact);
		}

		for (Contact_Role__c contactRoleRecord : contactRoles) {
			COM_DxlAffectedObjects affectedContact = new COM_DxlAffectedObjects();
			affectedContact.type = COM_DXL_Constants.COM_DXL_CONTACT_API_NAME;
			affectedContact.sfdcId = String.valueOf(contactRoleRecord.Contact__c);
			returnValue.add(affectedContact);
		}

		return returnValue;
	}

	private List<Contact_Role__c> getContactRoles(Opportunity opportunityData) {
		return [
			SELECT
				Id,
				Account__c,
				Contact__c,
				Type__c,
				Active__c,
				Contact__r.Email,
				Contact__r.Account.Visiting_street__c,
				Contact__r.Account.Visiting_Country__c,
				Contact__r.Account.Visiting_City__c,
				Contact__r.Account.Visiting_Postal_Code__c,
				Contact__r.Account.Visiting_Housenumber_Suffix__c,
				Contact__r.Account.Visiting_Housenumber1__c,
				Contact__r.Jurisdiction_Type__c,
				Contact__r.Document_Number__c,
				Contact__r.Document_Type__c,
				Contact__r.Phone,
				Contact__r.MobilePhone,
				Contact__r.LastName,
				Contact__r.FirstName,
				Contact__r.Birthdate,
				Contact__r.Gender__c
			FROM Contact_Role__c
			WHERE Account__c = :opportunityData.AccountId AND Active__c = TRUE
		];
	}

	public void createTransactionLog(HttpResponse dxlSyncResponse, Id opportunityId, String businessTransactionIdKey) {
		List<COM_DXL_Integration_Log__c> dxlIntegrationLogList = new List<COM_DXL_Integration_Log__c>();
		COM_DXL_Integration_Log__c dxlLog = new COM_DXL_Integration_Log__c();

		dxlLog.Transaction_Id__c = COM_WebServiceConfig.retrieveCaseInsensitiveHeader(dxlSyncResponse, businessTransactionIdKey);
		dxlLog.Opportunity__c = opportunityId;
		dxlLog.Type__c = COM_DXL_Constants.TRANSACTION_TYPE;
		if (dxlSyncResponse.getStatusCode() > 199 && dxlSyncResponse.getStatusCode() < 300) {
			dxlLog.Status__c = COM_DXL_Constants.TRANSACTION_STATUS_SUCCESS;
		} else {
			dxlLog.Status__c = COM_DXL_Constants.TRANSACTION_STATUS_ERROR;
			dxlLog.Error__c = dxlSyncResponse.getBody();
		}
		dxlLog.Affected_Objects__c = JSON.serialize(this.dxlAffectedObjects);
		dxlIntegrationLogList.add(dxlLog);

		if (dxlIntegrationLogList.size() > 0) {
			insert dxlIntegrationLogList;
		}
	}

	public void createRequestAttachment(Id parentId, String content) {
		Attachment attachment = new Attachment();
		attachment.Body = Blob.valueOf(content);
		attachment.Name = String.valueOf(COM_DXL_Constants.ATTACHMENT_NAME);
		attachment.ParentId = parentId;
		insert attachment;
	}

	public static String getDXLMapping(String mappingName, String keyName) {
		String returnValue = keyName;
		Map<String, COM_DXL_Mapping__mdt> mdtMap = COM_DXL_Mapping__mdt.getAll();

		if (Test.isRunningTest() && mdtMap.isEmpty()) {
			COM_DXL_Mapping__mdt dxlMapping = new COM_DXL_Mapping__mdt();
			dxlMapping.DeveloperName = 'sole_proprietorship_01';
			dxlMapping.Mapping_Name__c = COM_DXL_Constants.COM_DXL_LEGAL_ENTITY_MAPPING;
			dxlMapping.Key__c = '01';
			dxlMapping.Value__c = 'sole proprietorship';
			mdtMap.put('sole_proprietorship_01', dxlMapping);
		}

		for (String key : mdtMap.keySet()) {
			COM_DXL_Mapping__mdt mdtRecord = mdtMap.get(key);
			if (mdtRecord.Mapping_Name__c.equals(mappingName) && mdtRecord.Key__c.equals(keyName)) {
				returnValue = mdtRecord.Value__c;
			}
		}

		return returnValue;
	}

	public static String checkNullAndEmptyValues(String data) {
		return String.isBlank(data) ? '' : data;
	}
}
