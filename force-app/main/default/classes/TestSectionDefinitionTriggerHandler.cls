@isTest
public with sharing class TestSectionDefinitionTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Section Definition';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'SD-20-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		csclm__Section_Definition__c testSectionDefinition = new csclm__Section_Definition__c();
		insert testSectionDefinition;

		csclm__Section_Definition__c queryRecord = [SELECT Id, ExternalID__c FROM csclm__Section_Definition__c LIMIT 1];
		System.assert(queryRecord.ExternalID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
