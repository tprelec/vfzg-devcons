@isTest
public with sharing class TestOrderEntryPromotionTriggerHandler {
	@isTest
	public static void testAutoNumbering() {
		ExternalIDNumber__c custSetting = new ExternalIDNumber__c(
			Name = 'OE_Promotion',
			External_Number__c = 1,
			Object_Prefix__c = 'OEP-55-',
			blocked__c = false,
			runningOrg__c = '00D1l0000008i1vEAA'
		);
		insert custSetting;

		String orgId = UserInfo.getOrganizationId();

		Test.startTest();
		OrderEntryTestFactory.createOEPromotion(
			'De eerste maand 100% korting op uw Zakelijk Start abonnement',
			'Standard',
			true,
			'Existing',
			'1',
			'Off-Net',
			'MTC',
			true
		);
		Test.stopTest();

		OE_Promotion__c result = [SELECT Id, External_Id__c FROM OE_Promotion__c];

		if (custSetting.runningOrg__c == orgId) {
			System.assertEquals(
				'OEP-55-000002',
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		} else {
			System.assertEquals(
				null,
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		}
	}
}