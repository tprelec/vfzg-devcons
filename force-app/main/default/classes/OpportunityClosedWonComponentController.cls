public with sharing class OpportunityClosedWonComponentController {
	@AuraEnabled
	public static Boolean getProductBasketPortfolioStatus(String opportunityId) {
		List<Opportunity> result = [
			SELECT 
				Id, 
				Primary_Basket__r.New_Portfolio__c
			FROM Opportunity
			WHERE Id = :opportunityId 
			AND Primary_Basket__c != NULL
		];

		if (!result.isEmpty()) {
			return result[0].Primary_Basket__r.New_Portfolio__c;
		} else {
			return null;
		}
	}

	@AuraEnabled
	public static String getOrder(String opportunityId) {
		List<csord__Order__c> orderId = [
			SELECT 
				Id, 
				csordtelcoa__Opportunity__c
			FROM csord__Order__c
			WHERE csordtelcoa__Opportunity__c = :opportunityId
			ORDER BY CreatedDate DESC
			LIMIT 1
		];

		if (!orderId.isEmpty()) {
			return orderId[0].Id;
		} else {
			return null;
		}
	}

	@AuraEnabled
	public static Opportunity updateOppCloseDate(Id recordId, Date fieldDate) {
		Opportunity opp = [SELECT Id, CloseDate FROM Opportunity WHERE Id = :recordId];
		opp.CloseDate = fieldDate;
		opp.StageName = 'Closed Won';

		try {
			update (opp);
			return opp;
		} catch (Exception ex) {
			throw new AuraHandledException(ex.getMessage());
		}
	}

	@AuraEnabled
	public static Boolean isOpportunityClosedWon(Id recordId){
		Opportunity opp = [
			SELECT 
				Id,
				StageName 
			FROM Opportunity 
			WHERE Id = :recordId
		];

		if(opp.StageName == 'Closed Won'){
			return true;
		} else {
			return false;
		}
	}
}