public with sharing class COM_CDO_DeleteServices {
	public static String deleteServices(Id deliveryOrderId) {
		String responseBody = deleteServiceReq(deliveryOrderId);
		return responseBody;
	}

	private static String deleteServiceReq(Id deliveryOrderId) {
		String result = '';
		COM_CDO_ServiceOrder request = getRequest(deliveryOrderId);

		COM_Integration_setting__mdt deleteBIMOServiceSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.CDO_DEPROVISION_BIMO_SETTING_NAME
		);

		HttpResponse response = sendDeleteServiceRequest(request, deleteBIMOServiceSetting, deliveryOrderId);

		if (failedResponse(response)) {
			DeleteServicesSyncErrorResponse responseDeserialized = (DeleteServicesSyncErrorResponse) JSON.deserializeStrict(
				response.getBody(),
				DeleteServicesSyncErrorResponse.class
			);

			COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
			updates.deliveryOrderId = deliveryOrderId;
			updates.status = deleteBIMOServiceSetting.Sync_failed_status__c;
			updates.errorMessage = responseDeserialized.message;
			updates.success = false;

			COM_CDOHelper.performOrderUpdatesAfterResponse(updates, null);

			result = response.getBody();
		} else {
			String reqBody = COM_CDOServiceCharacteristicHelper.modifyJsonLiteToFitDeserializeClass(response.getBody());
			result = reqBody;
			COM_CDO_ServiceOrder body = (COM_CDO_ServiceOrder) JSON.deserializeStrict(reqBody, COM_CDO_ServiceOrder.class);

			COM_CDOHelper.DeliveryOrderUpdates updates = new COM_CDOHelper.DeliveryOrderUpdates();
			updates.deliveryOrderId = deliveryOrderId;
			updates.status = '';
			updates.errorMessage = '';
			updates.success = true;

			COM_CDOHelper.performOrderUpdatesAfterResponse(updates, body);
		}

		return result;
	}

	public static COM_CDO_ServiceOrder getRequest(Id deliveryOrderId) {
		List<String> umbrellaServices = COM_CDOHelper.getUmbrellaServices(deliveryOrderId);

		COM_CDO_ServiceOrder request = new COM_CDO_ServiceOrder();

		COM_CDO_integration_settings__mdt comSetting = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.CDO_DEPROVISION_BIMO_SETTING_NAME
		);

		request.externalId = comSetting.externalId__c;
		request.priority = comSetting.priority__c;
		request.description = comSetting.Description__c;
		request.category = comSetting.Category__c;
		request.typeVal = comSetting.requestType__c;
		request.orderItem = new List<COM_CDO_ServiceOrder.OrderItem>();

		Integer index = 1;
		for (String umbrellaService : umbrellaServices) {
			COM_CDO_ServiceOrder.OrderItem orderitem = new COM_CDO_ServiceOrder.OrderItem();
			orderItem.id = String.valueOf(index);
			orderItem.action = COM_CDO_Constants.COM_CDO_ACTION_DELETE;
			orderItem.typeVal = COM_CDO_Constants.COM_CDO_STANDARD_CONS;

			COM_CDO_ServiceOrder.Service service = new COM_CDO_ServiceOrder.Service();

			service.id = umbrellaService;
			service.typeVal = comSetting.serviceType__c;
			service.name = comSetting.serviceName__c;

			COM_CDO_ServiceOrder.ServiceSpecification specification = new COM_CDO_ServiceOrder.ServiceSpecification();
			specification.id = comSetting.serviceSpecificationId__c;
			specification.invariantID = comSetting.serviceSpecification_invariantID__c;
			specification.version = comSetting.serviceSpecification_version__c;
			specification.name = comSetting.serviceSpecification_name__c;
			specification.typeVal = comSetting.serviceSpecificationType__c;
			service.serviceSpecification = specification;

			orderItem.service = service;

			request.orderItem.add(orderitem);
			index++;
		}

		return request;
	}

	private static String generatePayloadJson(COM_CDO_ServiceOrder req) {
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeStringField(COM_CDO_Constants.COM_CDO_EXTERNALID, req.externalId);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_PRIORITY, req.priority);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_DESCRIPTION, req.description);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_CATEGORY, req.category);
		gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, req.typeVal);
		gen.writeFieldName(COM_CDO_Constants.COM_CDO_ORDER_ITEM);
		gen.writeStartArray();
		for (COM_CDO_ServiceOrder.OrderItem orderItem : req.orderItem) {
			gen.writeStartObject();
			gen.writeStringField(COM_CDO_Constants.COM_CDO_ID, orderItem.id);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_ACTION, orderItem.action);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, orderItem.typeVal);

			gen.writeFieldName(COM_CDO_Constants.COM_CDO_SERVICE);
			gen.writeStartObject();
			gen.writeStringField(COM_CDO_Constants.COM_CDO_ID, orderItem.service.id);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_NAME, orderItem.service.name);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, orderItem.service.typeVal);

			gen.writeFieldName(COM_CDO_Constants.COM_CDO_SERVICE_SPECIFICATION);
			gen.writeStartObject();
			gen.writeStringField(COM_CDO_Constants.COM_CDO_ID, orderItem.service.serviceSpecification.id);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_INVARIANTID, orderItem.service.serviceSpecification.invariantID);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_VERSION, orderItem.service.serviceSpecification.version);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_NAME, orderItem.service.serviceSpecification.name);
			gen.writeStringField(COM_CDO_Constants.COM_CDO_TYPE, orderItem.service.serviceSpecification.typeVal);
			gen.writeEndObject();

			gen.writeEndObject();
			gen.writeEndObject();
		}
		gen.writeEndArray();
		gen.writeEndObject();

		return gen.getAsString();
	}

	private static HTTPResponse sendDeleteServiceRequest(
		COM_CDO_ServiceOrder req,
		COM_Integration_setting__mdt deleteBIMOServiceSetting,
		Id deliveryOrderId
	) {
		Boolean doNotExecuteReq = deleteBIMOServiceSetting != null ? deleteBIMOServiceSetting.Execute_mock__c : true;
		Boolean doNotExecuteStatus = deleteBIMOServiceSetting != null ? deleteBIMOServiceSetting.Mock_Execution_result__c : true;

		Boolean generateAttachment = deleteBIMOServiceSetting != null ? deleteBIMOServiceSetting.Generate_attachment__c : true;

		String namedCredentials = deleteBIMOServiceSetting.NamedCredential__c;

		HttpRequest reqData = new HttpRequest();
		Http http = new Http();

		String endpoint = COM_WebServiceConfig.getEndpoint(namedCredentials, COM_CDO_Constants.COM_WEB_PARAM);

		reqData.setHeader('Content-Type', 'application/json');
		reqData.setEndpoint(endpoint);
		String requestBody = generatePayloadJson(req);

		reqData.setBody(requestBody);
		reqData.setMethod('POST');
		HTTPResponse response = new HttpResponse();
		if (doNotExecuteReq && !Test.isRunningTest()) {
			response.setHeader('Content-Type', 'application/json');
			if (doNotExecuteStatus) {
				String mockSuccess = COM_CDOHelper.getMockResponse(
					COM_CDO_Constants.COM_DEPROVISION_BIMO_SYNC_SUCCESS_MOCK,
					COM_CDO_Constants.CDO_DEPROVISION_BIMO_SETTING_NAME
				);
				response.setBody(mockSuccess);
				response.setStatusCode(200);
			} else {
				String mockFailed = COM_CDOHelper.getMockResponse(
					COM_CDO_Constants.COM_DEPROVISION_BIMO_SYNC_FAILED_MOCK,
					COM_CDO_Constants.CDO_DEPROVISION_BIMO_SETTING_NAME
				);
				response.setBody(mockFailed);

				response.setStatusCode(400);
			}
		} else {
			response = http.send(reqData);
		}

		if (generateAttachment) {
			COM_CDOHelper.createPayloadAttachment(deliveryOrderId, COM_CDO_Constants.COM_CDO_DEPROVISION_PAYLOAD_ATTACHMENT_NAME, requestBody);
		}

		return response;
	}

	private static Boolean failedResponse(HTTPResponse response) {
		Boolean result = false;
		try {
			DeleteServicesSyncErrorResponse responseDeserialized = (DeleteServicesSyncErrorResponse) JSON.deserializeStrict(
				response.getBody(),
				DeleteServicesSyncErrorResponse.class
			);
			if (responseDeserialized.code != null) {
				result = true;
			}
		} catch (Exception e) {
			result = false;
		}

		return result;
	}

	public class DeleteServicesSyncErrorResponse {
		public Integer code;
		public String reason;
		public String message;
	}
}
