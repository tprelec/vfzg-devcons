@IsTest
public with sharing class TestOnlineOrderFlowOrchestration {
	@TestSetup
	static void makeData() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		acc.KVK_number__c = '12345678';
		acc.Visiting_City__c = 'Utrecht';
		acc.Visiting_Housenumber1__c = 69;
		acc.Visiting_Housenumber_Suffix__c = 'A';
		acc.Visiting_Postal_Code__c = '1234AB';
		acc.Visiting_street__c = 'Balistraat';

		Contact con = TestUtils.createContact(acc);
		con.Email = 'mostUniqueEmailInTheWorld@vodafoneziggo.com';
		update con;

		Site__c site = TestUtils.createSite(acc);

		acc.Authorized_to_sign_1st__c = con.Id;
		update acc;

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
			Name = 'New Basket',
			Primary__c = false,
			OwnerId = UserInfo.getUserId(),
			Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]',
			ProfitLoss_JSON__c = '{"KPI Direct Capex / Net Revenue":"0,00","KPI Payback Period(Months)":"0,00","KPI NPV":"0,00",' +
				'"KPI Net Incremental Billed Revenue":"0,00","KPI Free cash flow / Net revenue":"1,50","KPI EBITDA / Net revenue":"1,56",' +
				'"KPI Sales Margin / Net revenue":"1,71","NET RESULT":"56.993,07","NET RESULT first":"58.996,33","Capex":"1.228,43","Network Depreciation":"774,83",' +
				'"Direct Capex":"0,00","EBITDA":"58.996,33","EBITDA first":"62.203,26","Overhead allocation":"3.206,93","Network Opex":"2.819,96",' +
				'"Incremental EBITDA":"65.023,22","Incremental OPEX":"0,00","Total Gross Margin":"65.023,22","Total costs of sales":"10.816,78","A&R":"0,00",' +
				'"Revenue share":"0,00","Other Costs":"0,00","Opportunity cost":"0,00","Opportunity cost Fixed":"0,00","Opportunity cost Mobile":"0,00",' +
				'"Total cost of sales":"10.816,78","Total Net Revenue":"75.840,00","Total discounts":"0,00","Benchmark":"0,00",' +
				'"Total credit amount / loyalty bonus":"0,00","Benchmark Fixed":"0,00","Benchmark Mobile":"0,00",' +
				'"Operator other costs / other migration costs":"0,00","Operator other costs / other migration costs Fixed":"0,00",' +
				'"Operator other costs / other migration costs Mobile":"0,00","Discounts":"0,00","Total Gross Revenue (SUM)":"75.840,00",' +
				'"Total Gross Revenue for Operator other costs / other migration costs":"0,00","Total Gross Revenue":"75.840,00"}'
		);
		insert basket;
	}

	@IsTest
	static void testCreateBan() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		OnlineOrderFlowOrchestration.accId = acc.Id;
		OnlineOrderFlowOrchestration.acc = acc;

		Test.startTest();
		Ban__c ban = OnlineOrderFlowOrchestration.createBan();
		Test.stopTest();

		System.assert(ban.Id != null, 'Ban should have an Id.');
		System.assertEquals(ban.Account__c, acc.Id, 'Ban should be assigned to account.');
		System.assertEquals(ban.Unify_Customer_Type__c, 'B', 'Wrong unify customer type.');
	}

	@IsTest
	static void testCreateFinancialAccount() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		OnlineOrderFlowOrchestration.Payment payment = new OnlineOrderFlowOrchestration.Payment();
		payment.IBAN = 'NL91ABNA0417164300';
		payment.paymentMethod = 'invoice';
		OnlineOrderFlowOrchestration.Order order = new OnlineOrderFlowOrchestration.Order();
		order.payment = payment;
		OnlineOrderFlowOrchestration.CustomOrderData cod = new OnlineOrderFlowOrchestration.CustomOrderData();
		cod.order = order;

		OnlineOrderFlowOrchestration.data = cod;
		OnlineOrderFlowOrchestration.accId = acc.Id;
		OnlineOrderFlowOrchestration.acc = acc;

		Test.startTest();
		OnlineOrderFlowOrchestration.createBan();
		Financial_Account__c finAcc = OnlineOrderFlowOrchestration.createFinancialAccount();
		Test.stopTest();

		System.assert(finAcc.Id != null, 'Financial account should have an Id.');
		System.assertEquals(
			finAcc.Bank_Account_Number__c,
			'NL91ABNA0417164300',
			'Wrong iban on the financial account.'
		);
		System.assertEquals(finAcc.Payment_method__c, 'invoice', 'Wrong payment method.');
	}

	@IsTest
	static void testCreateBillingArrangment() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		OnlineOrderFlowOrchestration.Payment payment = new OnlineOrderFlowOrchestration.Payment();
		payment.IBAN = 'NL91ABNA0417164300';
		payment.paymentMethod = 'invoice';
		OnlineOrderFlowOrchestration.Order order = new OnlineOrderFlowOrchestration.Order();
		order.payment = payment;
		OnlineOrderFlowOrchestration.CustomOrderData cod = new OnlineOrderFlowOrchestration.CustomOrderData();
		cod.order = order;

		OnlineOrderFlowOrchestration.data = cod;
		OnlineOrderFlowOrchestration.accId = acc.Id;
		OnlineOrderFlowOrchestration.acc = acc;

		Test.startTest();
		OnlineOrderFlowOrchestration.createBan();
		OnlineOrderFlowOrchestration.createFinancialAccount();
		Billing_Arrangement__c billArr = OnlineOrderFlowOrchestration.createBillingArrangment();
		Test.stopTest();

		System.assert(billArr.Id != null, 'Financial account should have an Id.');
		System.assertEquals(
			billArr.Bank_Account_Number__c,
			'NL91ABNA0417164300',
			'Wrong iban on the financial account.'
		);
		System.assertEquals(billArr.Payment_method__c, 'invoice', 'Wrong payment method.');
		System.assertEquals(billArr.Bill_Format__c, 'EB', 'Wrong bill format.');
		System.assertEquals(billArr.Billing_City__c, 'Utrecht', 'Wrong billing city.');
	}

	@IsTest
	static void testCreateOpporutnity() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];

		OnlineOrderFlowOrchestration.Payment payment = new OnlineOrderFlowOrchestration.Payment();
		payment.IBAN = 'NL91ABNA0417164300';
		payment.paymentMethod = 'invoice';
		OnlineOrderFlowOrchestration.Order order = new OnlineOrderFlowOrchestration.Order();
		order.payment = payment;
		OnlineOrderFlowOrchestration.Identification identification = new OnlineOrderFlowOrchestration.Identification();
		identification.identificationType = 'Passport';
		identification.identificationNumber = '123456789';
		OnlineOrderFlowOrchestration.Customer customer = new OnlineOrderFlowOrchestration.Customer();
		customer.identification = identification;
		OnlineOrderFlowOrchestration.CustomOrderData cod = new OnlineOrderFlowOrchestration.CustomOrderData();
		cod.order = order;
		cod.customer = customer;

		OnlineOrderFlowOrchestration.data = cod;
		OnlineOrderFlowOrchestration.accId = acc.Id;
		OnlineOrderFlowOrchestration.acc = acc;

		Test.startTest();
		OnlineOrderFlowOrchestration.createBan();
		OnlineOrderFlowOrchestration.createFinancialAccount();
		OnlineOrderFlowOrchestration.createBillingArrangment();
		Opportunity opp = OnlineOrderFlowOrchestration.createOpporutnity();
		Test.stopTest();

		System.assert(opp.Id != null, 'Opportunity should have an Id.');
		System.assertEquals(opp.StageName, 'Closing', 'Wrong stage name.');
		System.assertEquals(opp.Type, 'New Business', 'Wrong ptype.');
		System.assertEquals(opp.Deal_Type__c, 'Acquisition', 'Wrong deal type.');
	}

	@IsTest
	static void testCreateOpporutnityAttachment() {
		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		OnlineOrderFlowOrchestration.Payment payment = new OnlineOrderFlowOrchestration.Payment();
		payment.IBAN = 'NL91ABNA0417164300';
		payment.paymentMethod = 'invoice';
		OnlineOrderFlowOrchestration.Order order = new OnlineOrderFlowOrchestration.Order();
		order.payment = payment;
		OnlineOrderFlowOrchestration.Identification identification = new OnlineOrderFlowOrchestration.Identification();
		identification.identificationType = 'Passport';
		identification.identificationNumber = '123456789';
		OnlineOrderFlowOrchestration.Customer customer = new OnlineOrderFlowOrchestration.Customer();
		customer.identification = identification;
		OnlineOrderFlowOrchestration.CustomOrderData cod = new OnlineOrderFlowOrchestration.CustomOrderData();
		cod.order = order;
		cod.customer = customer;

		OnlineOrderFlowOrchestration.data = cod;
		OnlineOrderFlowOrchestration.accId = acc.Id;
		OnlineOrderFlowOrchestration.acc = acc;

		Test.startTest();
		OnlineOrderFlowOrchestration.createBan();
		OnlineOrderFlowOrchestration.createFinancialAccount();
		OnlineOrderFlowOrchestration.createBillingArrangment();
		Opportunity opp = OnlineOrderFlowOrchestration.createOpporutnity();
		Opportunity_Attachment__c oppAtt = OnlineOrderFlowOrchestration.createOpporutnityAttachment(
			'Copy KvK'
		);
		Test.stopTest();

		System.assert(oppAtt.Id != null, 'Opportunity should have an Id.');
		System.assertEquals(oppAtt.Opportunity__c, opp.Id, 'Wrong opportunity.');
		System.assertEquals(oppAtt.Attachment_Type__c, 'Copy KvK', 'Wrong attachment type.');
	}

	@IsTest
	static void testParseData() {
		Contract_Generation_Settings__c contractGenSettings = new Contract_Generation_Settings__c();
		contractGenSettings.Document_template_name_direct__c = 'Direct Sales Template';
		contractGenSettings.Document_template_name_direct_2__c = 'Direct Sales Template 2';
		contractGenSettings.Document_template_name_BPA__c = 'BPA Template new';
		insert contractGenSettings;

		csclm__Document_Definition__c testDocumentDefinition = CS_DataTest.createDocumentDefinition();
		testDocumentDefinition.ExternalID__c = 'SomeId41';
		insert testDocumentDefinition;

		csclm__Document_Template__c docTemplate = CS_DataTest.createDocumentTemplate(
			'Direct Sales Template 2'
		);
		docTemplate.csclm__Document_Definition__c = testDocumentDefinition.Id;
		docTemplate.csclm__Valid__c = true;
		docTemplate.csclm__Active__c = true;
		docTemplate.ExternalID__c = 'SomeId4';
		insert docTemplate;

		Account acc = [
			SELECT
				Id,
				Name,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			LIMIT 1
		];
		String jsonStr = '{"order": { "payment": { "paymentMethod": "invoice", "IBAN": "NL91ABNA0417164300" } }, "customer": { "identification": { "nationality" : "NL", "identificationType": "Passport", "identificationNumber": "00456S655", "identificationExpirationDate" : "2022-04-25" } }, "locations": [ { "id": "PAID-177.261.799", "address": { "street": "Stieltjesweg", "houseNumber": "516", "houseNumberAddition": "", "room": "", "postcode": "2628CK", "city": "Delft" }, "contact": { "initials": "M.A", "middleName": "", "lastName": "Meshcheriakov", "email": "mykyta@hacknet.ua", "phone": "555-45895689", "comment": "be aware of dog" } } ] }';
		cscfga__Product_Basket__c basket = [SELECT Id FROM cscfga__Product_Basket__c LIMIT 1];

		Test.startTest();
		OnlineOrderFlowOrchestration oofo = new OnlineOrderFlowOrchestration(
			jsonStr,
			acc.Id,
			basket.Id
		);
		OnlineOrderFlowOrchestration.createObjects();
		Test.stopTest();

		System.assert(
			OnlineOrderFlowOrchestration.accId == acc.Id,
			'Account Id not set correctly.'
		);
		System.assert(
			OnlineOrderFlowOrchestration.pbId == basket.Id,
			'Basket Id not set correctly.'
		);
	}

	@IsTest
	static void testFetchExistingSites() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Site__c st = [
			SELECT
				Id,
				Site_Street__c,
				Site_Postal_Code__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c
			FROM Site__c
			LIMIT 1
		];

		Map<String, String> siteMap = new Map<String, String>{
			Constants.POSTCODE => st.Site_Postal_Code__c,
			Constants.HOUSENUMBER => String.valueOf(st.Site_House_Number__c),
			Constants.HOUSENRSUFFIX => st.Site_House_Number_Suffix__c
		};

		List<Map<String, String>> siteData = new List<Map<String, String>>{ siteMap };

		Test.startTest();
		OnlineOrderFlowOrchestration.accId = acc.Id;
		List<Site__c> sites = OnlineOrderFlowOrchestration.fetchExistingSites(siteData);
		Test.stopTest();

		System.assertEquals(
			acc.Id,
			sites[0].Site_Account__c,
			'Site not associated to correct account.'
		);
		System.assertEquals(st.Site_Street__c, sites[0].Site_Street__c, 'Wrong site street.');
	}

	
}