/**
 * @description:    RecordType accessor class
 * @testClass:      TestRecordTypeAccessor
 **/
public inherited sharing class RecordTypeAccessor {
	private static Map<Id, String> recordTypeDeveloperNamePerRecordTypeId;
	private static Map<String, Map<String, Id>> recordTypeIdPerRecordTypeDeveloperNameAndSObjectType;

	static {
		initRecordTypeMapping(getRecordTypes());
	}

	/**
	 * @description:    Retrieves record type developer name for record type Id
	 * @param           recordTypeId - record type id
	 * @return          String - record type developer name for record type Id
	 */
	public static String getRecordTypeDeveloperNameForId(Id recordTypeId) {
		return recordTypeDeveloperNamePerRecordTypeId?.get(recordTypeId);
	}

	/**
	 * @description:    Retrieves record type id for SObject type and developer name
	 * @param           sObjectType - SObject type
	 * @param           recordTypeDeveloperName - record type developer name
	 * @return          String - record type id for SObject type and developer name
	 */
	public static String getRecordTypeIdForSObjectTypeAndDeveloperName(SObjectType sObjectType, String recordTypeDeveloperName) {
		return recordTypeIdPerRecordTypeDeveloperNameAndSObjectType?.get(sObjectType?.getDescribe()?.getName())?.get(recordTypeDeveloperName);
	}

	/**
	 * @description:    Initializes record type mapping
	 * @param           recordTypes - list of record types
	 */
	@TestVisible
	private static void initRecordTypeMapping(List<RecordType> recordTypes) {
		recordTypeDeveloperNamePerRecordTypeId = new Map<Id, String>();
		recordTypeIdPerRecordTypeDeveloperNameAndSObjectType = new Map<String, Map<String, Id>>();

		for (RecordType recordType : recordTypes) {
			recordTypeDeveloperNamePerRecordTypeId.put(recordType.Id, recordType.DeveloperName);

			if (!recordTypeIdPerRecordTypeDeveloperNameAndSObjectType.containsKey(recordType.SObjectType)) {
				recordTypeIdPerRecordTypeDeveloperNameAndSObjectType.put(recordType.SObjectType, new Map<String, Id>());
			}

			recordTypeIdPerRecordTypeDeveloperNameAndSObjectType.get(recordType.SObjectType).put(recordType.DeveloperName, recordType.Id);
		}
	}

	/**
	 * @description:    Retrieves all RecordType records
	 * @return          List<RecordType> - list of RecordType
	 */
	private static List<RecordType> getRecordTypes() {
		return [SELECT SObjectType, DeveloperName, Id FROM RecordType];
	}
}
