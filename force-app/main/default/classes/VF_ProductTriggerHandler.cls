/**
 * @description         This is the trigger handler for the VF_Product__c sObject.
 * @author              Guy Clairbois
 */
public with sharing class VF_ProductTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public override void AfterInsert() {
		List<VF_Product__c> newProducts = (List<VF_Product__c>) this.newList;
		Map<Id, VF_Product__c> newProductsMap = (Map<Id, VF_Product__c>) this.newMap;

		syncWithProduct2(newProductsMap, null);
	}

	public override void BeforeUpdate() {
		List<VF_Product__c> newProducts = (List<VF_Product__c>) this.newList;
	}

	public override void AfterUpdate() {
		List<VF_Product__c> newProducts = (List<VF_Product__c>) this.newList;
		List<VF_Product__c> oldProducts = (List<VF_Product__c>) this.oldList;
		Map<Id, VF_Product__c> newProductsMap = (Map<Id, VF_Product__c>) this.newMap;
		Map<Id, VF_Product__c> oldProductsMap = (Map<Id, VF_Product__c>) this.oldMap;

		syncWithProduct2(newProductsMap, oldProductsMap);
	}

	public override void BeforeDelete() {
		List<VF_Product__c> newProducts = (List<VF_Product__c>) this.newList;
		List<VF_Product__c> oldProducts = (List<VF_Product__c>) this.oldList;
		Map<Id, VF_Product__c> oldProductsMap = (Map<Id, VF_Product__c>) this.oldMap;

		syncWithProduct2(oldProductsMap, null);
	}

	/**
	 * @description         Automatically create/update 1 or more VF_Product__c based on the VF_VF_Product__c
	 */
	private void syncWithProduct2(Map<Id, VF_Product__c> newProductsMap, Map<Id, VF_Product__c> oldProductsMap) {
		// in case of a delete, just try to delete the existing products and return
		if (Trigger.isDelete) {
			delete [SELECT Id, Family, VF_Product__c FROM Product2 WHERE VF_Product__c IN :newProductsMap.keySet()];
			return;
		}

		// in case of insert or update, do further processing

		// first link any non-linked Product2 that have the same ProductCode to this VF_Product
		// (this is for enabling easier environment refreshes)
		if (!GeneralUtils.IsProduction() || Test.isRunningTest()) {
			// collect productcodes in map
			Map<String, Id> productCodeToVFProductId = new Map<String, Id>();
			List<Product2> p2ToUpdate = new List<Product2>();
			for (VF_Product__c vfp : newProductsMap.values()) {
				productCodeToVFProductId.put(vfp.Productcode__c, vfp.Id);
			}
			for (Product2 p : [
				SELECT Id, ProductCode, VF_Product__c
				FROM Product2
				WHERE /*IsActive = true AND*/ VF_Product__r.Id = NULL AND ProductCode IN :productCodeToVFProductId.keySet()
			]) {
				if (productCodeToVFProductId.containsKey(p.ProductCode)) {
					p.VF_Product__c = productCodeToVFProductId.get(p.ProductCode);
					p2ToUpdate.add(p);
				}
			}
			update p2ToUpdate;
		}

		// get all existing Product2 records
		Map<Id, Map<String, Product2>> vfProductIdToCLCToProduct2 = new Map<Id, Map<String, Product2>>();
		for (Id pId : newProductsMap.keySet()) {
			Map<String, Product2> tempMap = new Map<String, Product2>();
			vfProductIdToCLCToProduct2.put(pId, tempMap);
		}
		for (Product2 p : [
			SELECT Id, Family, CLC__c, VF_Product__c
			FROM Product2
			WHERE VF_Product__c IN :newProductsMap.keySet()
			ORDER BY CLC__c ASC
		]) {
			if (vfProductIdToCLCToProduct2.get(p.VF_Product__c).containsKey(p.CLC__c)) {
				newProductsMap.get(p.VF_Product__c)
					.addError('Double CLC entry found in Product table (\'' + p.CLC__c + '\'). Please corrrect and try again.');
			}
			vfProductIdToCLCToProduct2.get(p.VF_Product__c).put(p.CLC__c, p);
		}

		List<Product2> ProductsToUpsert = new List<Product2>();

		Map<String, Schema.SObjectField> fieldmap = Schema.SObjectType.VF_Product__c.fields.getMap();
		Set<String> editableAPIFieldNames = new Set<String>();
		for (String fieldName : fieldmap.keySet()) {
			if (fieldmap.get(fieldName).getDescribe().isUpdateable()) {
				editableAPIFieldNames.add(fieldName);
			}
		}

		// determine new/updated Product2 records
		for (VF_Product__c vfp : newProductsMap.values()) {
			// for updates, do some checks to see if VF_Product__c and Product2 are in sync
			if (Trigger.isUpdate) {
				if (vfp.Split_Product_Family_into_Ret_Acq_Churn__c != oldProductsMap.get(vfp.Id).Split_Product_Family_into_Ret_Acq_Churn__c) {
					if (!vfp.Split_Product_Family_into_Ret_Acq_Churn__c) {
						vfp.addError('This box cannot be unchecked. Please create a new VF_Product__c.');
					}
				}
				// check by the old split-checkbox
				if (oldProductsMap.get(vfp.Id).Split_Product_Family_into_Ret_Acq_Churn__c) {
					//if(vfProductIdToCLCToProduct2.get(vfp.Id).values().size() != 4){
					if (vfProductIdToCLCToProduct2.get(vfp.Id).values().size() != 5) {
						//vfp.addError('VF_Product is out of sync with Product2. Please make sure that there are 4 linked Product2 records.');
						vfp.addError('VF_Product is out of sync with Product2. Please make sure that there are 5 linked Product2 records.');
					}
					if (!vfProductIdToCLCToProduct2.get(vfp.Id).containsKey('Acq')) {
						vfp.addError('Product2 record with CLC Acq missing.');
					}
					if (!vfProductIdToCLCToProduct2.get(vfp.Id).containsKey('Ret')) {
						vfp.addError('Product2 record with CLC Ret missing.');
					}
					if (!vfProductIdToCLCToProduct2.get(vfp.Id).containsKey('Churn')) {
						vfp.addError('Product2 record with CLC Churn missing.');
					}
					if (!vfProductIdToCLCToProduct2.get(vfp.Id).containsKey('Mig')) {
						vfp.addError('Product2 record with CLC Mig missing.');
					}
					if (!vfProductIdToCLCToProduct2.get(vfp.Id).containsKey(null)) {
						vfp.addError('Product2 record with empty CLC missing.');
					}
				} else {
					system.debug(vfProductIdToCLCToProduct2.get(vfp.Id).values());
					if (vfProductIdToCLCToProduct2.get(vfp.Id).values().size() != 1 && !vfp.Split_Product_Family_into_Ret_Acq_Churn__c) {
						vfp.addError('VF_Product is out of sync with Product2. Please make sure that there is only 1 linked Product2 record.');
					}
				}
			}

			Product2 p = new Product2();
			// loop through all fields (minus the ones that should not be synced) and copy over the values
			for (String APIFieldName : editableAPIFieldNames) {
				if (
					APIFieldName != 'id' &&
					//&& APIFieldName != 'thresholdgroup__c'
					//&& APIFieldName != 'lastactivitydate'
					//&& APIFieldName != 'createdbyid'
					//&& APIFieldName != 'createddate'
					//&& APIFieldName != 'lastmodifieddate'
					//&& APIFieldName != 'lastreferenceddate'
					//&& APIFieldName != 'isdeleted'
					APIFieldName != 'ownerid' &&
					//&& APIFieldName != 'systemmodstamp'
					//&& APIFieldName != 'lastvieweddate'
					//&& APIFieldName != 'lastmodifiedbyid'
					APIFieldName != 'family__c' &&
					//&& APIFieldName != 'connectionreceivedid'
					//&& APIFieldName != 'connectionsentid'
					APIFieldName != 'Split_Product_Family_into_Ret_Acq_Churn__c'
				) {
					if (APIFieldName == 'productcode__c') {
						p.put('ProductCode', vfp.get(APIFieldName));
					} else if (APIFieldName == 'active__c') {
						p.put('IsActive', vfp.get(APIFieldName));
					} else if (APIFieldName == 'description__c') {
						p.put('Description', vfp.get(APIFieldName));
					} else {
						p.put(APIFieldName, vfp.get(APIFieldName));
					}
				}
			}
			p.VF_Product__c = vfp.Id;

			// if split is required, create/update 3 prods
			if (vfp.Split_Product_Family_into_Ret_Acq_Churn__c) {
				Product2 p1 = p.clone();
				p1.Family = 'Acq ' + vfp.Family__c;
				p1.CLC__c = 'Acq';
				if (vfProductIdToCLCToProduct2.get(vfp.Id).containsKey(p1.CLC__c)) {
					p1.Id = vfProductIdToCLCToProduct2.get(vfp.Id).get(p1.CLC__c).Id;
				}

				// special case for the scenario where the checkbox is checked (going from 1 to 5 Product2)
				// in that case the existing Product is the acq
				if (Trigger.isUpdate) {
					if (vfp.Split_Product_Family_into_Ret_Acq_Churn__c && !oldProductsMap.get(vfp.Id).Split_Product_Family_into_Ret_Acq_Churn__c) {
						// just take the first Product2 (there can be only 1). CLC can be either null, '' or acq
						// if it's not just 1, it is only cleanup of the checkbox and we don't need to do anything
						if (vfProductIdToCLCToProduct2.get(vfp.Id).values().size() == 1) {
							//p1.Family = vfProductIdToCLCToProduct2.get(vfp.Id).values()[0].Family;
							p1.Id = vfProductIdToCLCToProduct2.get(vfp.Id).values()[0].Id;
						}
					}
				}

				ProductsToUpsert.add(p1);
				Product2 p2 = p.clone();
				p2.Family = 'Ret ' + vfp.Family__c;
				p2.CLC__c = 'Ret';
				if (vfProductIdToCLCToProduct2.get(vfp.Id).containsKey(p2.CLC__c)) {
					p2.Id = vfProductIdToCLCToProduct2.get(vfp.Id).get(p2.CLC__c).Id;
				}
				ProductsToUpsert.add(p2);
				Product2 p3 = p.clone();
				p3.Family = 'Churn ' + vfp.Family__c;
				p3.CLC__c = 'Churn';
				if (vfProductIdToCLCToProduct2.get(vfp.Id).containsKey(p3.CLC__c)) {
					p3.Id = vfProductIdToCLCToProduct2.get(vfp.Id).get(p3.CLC__c).Id;
				}
				ProductsToUpsert.add(p3);
				Product2 p4 = p.clone();
				p4.Family = 'Mig ' + vfp.Family__c;
				p4.CLC__c = 'Mig';
				if (vfProductIdToCLCToProduct2.get(vfp.Id).containsKey(p4.CLC__c)) {
					p4.Id = vfProductIdToCLCToProduct2.get(vfp.Id).get(p4.CLC__c).Id;
				}
				ProductsToUpsert.add(p4);
				Product2 p5 = p.clone();
				p5.Family = vfp.Family__c;
				p5.CLC__c = null;
				if (vfProductIdToCLCToProduct2.get(vfp.Id).containsKey(p5.CLC__c)) {
					p5.Id = vfProductIdToCLCToProduct2.get(vfp.Id).get(p5.CLC__c).Id;
				}
				ProductsToUpsert.add(p5);
			} else {
				p.Family = vfp.Family__c;
				p.CLC__c = '';
				if (!vfProductIdToCLCToProduct2.get(vfp.Id).values().isEmpty()) {
					// just take the first Product2 (there can be only 1). CLC can be either null, '' or acq
					p.Id = vfProductIdToCLCToProduct2.get(vfp.Id).values()[0].Id;
				}
				ProductsToUpsert.add(p);
			}
		}
		upsert ProductsToUpsert;
	}

	public void increaseExternalId() {
		List<VF_Product__c> newProducts = (List<VF_Product__c>) this.newList;
		Sequence seq = new Sequence('VF Product');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (VF_Product__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
