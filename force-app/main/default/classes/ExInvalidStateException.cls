/**
 * @description			This exception should be thrown if during runtime an invalid state occurs, e.g. the
 *						valid from is greater than the valid to date.
 * @author				Guy Clairbois
 */
public class ExInvalidStateException extends Exception {}