@IsTest
public with sharing class TestMapsRecordRedirectController {
	@IsTest
	private static void testRedirect() {
		PageReference reference = Page.MapsRecordRedirect;
		reference.getParameters().put('RecordId', UserInfo.getUserId());
		Test.setCurrentPage(reference);

		Test.startTest();
		MapsRecordRedirectController ctrl = new MapsRecordRedirectController();
		PageReference redirectReference = ctrl.redirectToRecord();
		Test.stopTest();

		System.assertNotEquals(
			null,
			redirectReference,
			'Page should redirect to the record reference.'
		);
	}

	@IsTest
	private static void testNoRedirect() {
		PageReference reference = Page.MapsRecordRedirect;
		Test.setCurrentPage(reference);

		Test.startTest();
		MapsRecordRedirectController ctrl = new MapsRecordRedirectController();
		PageReference redirectReference = ctrl.redirectToRecord();
		Test.stopTest();

		System.assertEquals(
			null,
			redirectReference,
			'Page should not redirect to the record reference.'
		);
	}
}