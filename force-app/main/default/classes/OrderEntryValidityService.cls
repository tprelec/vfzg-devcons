public with sharing class OrderEntryValidityService {
	/**
	 * @description Validates Products/Addons/Promotions based on the Offer Date of the Opportunity
	 * @param  oppId           Opportunity ID
	 * @param  itemsToValidate Items to validate
	 * @return                 Set of valid item IDs
	 */
	public static Set<Id> validatePeriod(Id oppId, List<sObject> itemsToValidate) {
		if (itemsToValidate.isEmpty()) {
			return new Set<Id>();
		}
		Date offerDate = getOfferDate(oppId);
		String sObjName = itemsToValidate[0].Id.getSObjectType().getDescribe().getName();
		String fieldName = getFieldName(sObjName);

		Set<Id> idsToValidate = getIdsToValidate(sObjName, itemsToValidate);
		List<OE_Validity_Period__c> periods = getValidityPeriods(offerDate, fieldName, idsToValidate);
		return filterItems(periods, itemsToValidate, fieldName);
	}

	/**
	 * @description Gets Offer Date for provided Opportunity
	 * @param  oppId Opportunity ID
	 * @return       Offer Date
	 */
	private static Date getOfferDate(Id oppId) {
		Opportunity opp = [SELECT Id, CreatedDate, Offer_Date__c FROM Opportunity WHERE Id = :oppId];
		return opp.Offer_Date__c == null
			? Date.newinstance(opp.CreatedDate.year(), opp.CreatedDate.month(), opp.CreatedDate.day())
			: opp.Offer_Date__c;
	}

	/**
	 * @description Gets field on tha Validity Period that is used for validation
	 * @param  sObjName Object Name
	 * @return          Validation Period Field Name
	 */
	private static String getFieldName(String sObjName) {
		if (sObjName == 'OE_Product_Add_On__c') {
			return 'Order_Entry_Add_On__c';
		} else if (sObjName == 'OE_Promotion__c') {
			return 'Order_Entry_Promotion__c';
		} else {
			return 'Order_Entry_Product__c';
		}
	}

	/**
	 * @description Gets IDs that should be validated
	 * @param  sObjName        Name of the object that is being validated
	 * @param  itemsToValidate Items to validate
	 * @return                 Set of records that should be validated
	 */
	private static Set<Id> getIdsToValidate(String sObjName, List<sObject> itemsToValidate) {
		Set<Id> idsToValidate = new Set<Id>();
		if (sObjName == 'OE_Product_Add_On__c') {
			for (sObject obj : itemsToValidate) {
				idsToValidate.add(((OE_Product_Add_On__c) obj).Add_On__c);
			}
		} else {
			for (sObject obj : itemsToValidate) {
				idsToValidate.add(obj.Id);
			}
		}
		return idsToValidate;
	}

	/**
	 * @description Gets Validity periods
	 * @param  offerDate     Offer Date
	 * @param  fieldName     Name of the field on the Validity Period
	 * @param  idsToValidate Ids to validate
	 * @return               List of validity periods
	 */
	private static List<OE_Validity_Period__c> getValidityPeriods(Date offerDate, String fieldName, Set<Id> idsToValidate) {
		return Database.query(
			'SELECT Id, ' +
			fieldName +
			' FROM OE_Validity_Period__c' +
			' WHERE' +
			' From__c <= :offerDate' +
			' AND To__c >= :offerDate' +
			' AND ' +
			fieldName +
			' IN :idsToValidate'
		);
	}

	/**
	 * @description Filters items based on available periods
	 * @param  periods   Validity Periods
	 * @param  items     Items to validate
	 * @param  fieldName Field Name
	 * @return           Set of valid item IDs
	 */
	private static Set<Id> filterItems(List<OE_Validity_Period__c> periods, List<sObject> items, String fieldName) {
		Set<Id> returnIds = new Set<Id>();
		Map<Id, sObject> productMap = new Map<Id, SObject>(items);
		List<sObject> returnList = new List<sObject>();

		for (OE_Validity_Period__c period : periods) {
			if ('Order_Entry_Add_On__c' == fieldName) {
				for (sObject prd : items) {
					if (((OE_Product_Add_On__c) prd).Add_On__c == period.Order_Entry_Add_On__c) {
						returnIds.add(prd.Id);
					}
				}
			} else if ('Order_Entry_Product__c' == fieldName) {
				returnIds.add(productMap.get(period.Order_Entry_Product__c).Id);
			} else if ('Order_Entry_Promotion__c' == fieldName) {
				returnIds.add(productMap.get(period.Order_Entry_Promotion__c).Id);
			}
		}

		return returnIds;
	}
}
