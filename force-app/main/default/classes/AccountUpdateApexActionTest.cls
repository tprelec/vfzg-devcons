@IsTest
public with sharing class AccountUpdateApexActionTest {
	@IsTest
	static void updateAccountData() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);
		account.KVK_number__c = '12345678';
		update account;

		List<Id> accountIds = AccountUpdateApexAction.updateAccount(new List<Id>{ account.Id });

		System.assertEquals(true, accountIds.size() > 0, 'Accounts should be updated');
	}
}
