@SuppressWarnings('PMD.CyclomaticComplexity')
public with sharing class OppDocumentGenerationController {
	@TestVisible
	private static final String DOCUMENT_TEMPLATE_BUSINESS_INTERNET = 'Business Internet';

	@TestVisible
	private static final String CONTRACT_GENERATION_TOOL_MV = 'MV';

	@TestVisible
	private static final String AGREEMENT_MAVEN_DOC_RECORD_TYPE_NAME = 'Maven_Document';

	@TestVisible
	private static final String AGREEMENT_OUTPUT_FORMAT_PDF = 'pdf';

	@TestVisible
	private static final String AGREEMENT_CONTENT_TYPE = 'contenttype';

	@TestVisible
	public static final String DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED = 'ATTACHMENTS ATTACHED';

	@TestVisible
	private static final String AGREEMENT_APPROVAL_TYPE_NO_APPROVAL = 'No Approval';

	@TestVisible
	private static final String AGREEMENT_APPROVAL_STATUS_APPROVED = 'Approved';

	@TestVisible
	public static final String DOCUMENT_REQUEST_STATUS_NEW = 'New';

	@TestVisible
	public static final String DOCUMENT_REQUEST_DOCUMENT_FORMAT_PDF = 'PDF';

	@TestVisible
	public static final String DOCUMENT_REQUEST_SAVE_AS_ATTACHMENT = 'Attachment';

	@TestVisible
	public static final String DOCUMENT_REQUEST_PROCESSING_TYPE_BATCH = 'Batch';

	@TestVisible
	private static final String BASKET_STATUS_APPROVED = 'Approved';

	@TestVisible
	private static final String CLICK_APPROVE_SETTING = 'COM - New Portfolio Settings';

	@TestVisible
	private static final String OPPORTUNITY_SIGNATURE_DOCUSIGN = 'DocuSign';

	@TestVisible
	private static final String OPPORTUNITY_PROPOSITION_TYPE_FIXED = 'FIXED';

	@TestVisible
	private static final String OPPORTUNITY_SIGNATURE_CLICKAPPROVE = 'ClickApprove';

	@TestVisible
	private static final String OPPORTUNITY_SIGNATURE_STATUS_PENDING_FOR_APPROVAL = 'Pending for Approval';

	@TestVisible
	private static List<Orderform_Proposition__mdt> propositionCMDTs;

	@TestVisible
	private static List<mmdoc__Document_Template__c> documentTemplates;

	private static List<Opportunity> opportunities;

	@TestVisible
	private static List<Orderform_Proposition__mdt> getPropositionSetting() {
		if (propositionCMDTs == null) {
			propositionCMDTs = new List<Orderform_Proposition__mdt>();
			for (Orderform_Proposition__mdt orderProposition : Orderform_Proposition__mdt.getAll().values()) {
				if (orderProposition.Contract_Template__c != null && orderProposition.Contract_Generation_Tool__c == CONTRACT_GENERATION_TOOL_MV) {
					propositionCMDTs.add(orderProposition);
				}
			}
			return propositionCMDTs;
		}
		return propositionCMDTs;
	}

	private static List<mmdoc__Document_Template__c> getDocumentTemplates() {
		if (documentTemplates == null) {
			Set<String> setDocumentTemplate = new Set<String>();
			for (Orderform_Proposition__mdt proposition : getPropositionSetting()) {
				setDocumentTemplate.add(proposition.Contract_Template__c);
				setDocumentTemplate.add(proposition.Amendment_Contract_Template__c);
			}
			return [SELECT Id, Name, mmdoc__Document_Solution__c FROM mmdoc__Document_Template__c WHERE Name IN :setDocumentTemplate];
		}
		return documentTemplates;
	}

	private static List<Opportunity> getOpportunity(Id opportunityId) {
		if (opportunities == null || opportunities.isEmpty() || opportunities[0].Id != opportunityId) {
			return [
				SELECT
					Id,
					Primary_Basket__c,
					Primary_Basket__r.Name,
					Primary_Basket__r.Basket_Approval_Status__c,
					AccountId,
					Document_Signature__c,
					DS_Authorized_to_sign_1st__c,
					Account.Authorized_to_sign_1st__c,
					Credit_Check__c
				FROM Opportunity
				WHERE Id = :opportunityId AND Primary_Basket__c != NULL AND Primary_Basket__r.Primary__c = TRUE
			];
		}
		return opportunities;
	}

	@AuraEnabled(cacheable=true)
	public static LightningResponse getBasket(Id opportunityId) {
		Opportunity opportunity = !getOpportunity(opportunityId).isEmpty() ? getOpportunity(opportunityId)[0] : null;
		return new LightningResponse().setBody(new BasketResponse(opportunity));
	}

	@AuraEnabled
	public static LightningResponse getContracts(Id opportunityId) {
		Set<Id> documentIds = new Set<Id>();
		for (CS_DocItem document : CS_OppDocController.getDocuments(opportunityId)) {
			if (document.docId != null) {
				documentIds.add(document.docId);
			}
		}
		return new LightningResponse().setBody(documentIds);
	}

	@AuraEnabled
	public static LightningResponse createDocument(Id opportunityId) {
		Savepoint savepoint = Database.setSavePoint();
		try {
			return invokeCreateDocument(opportunityId);
		} catch (DMLException objEx) {
			return setError(savepoint, objEx.getDmlMessage(0));
		} catch (Exception objEx) {
			return setError(savepoint, objEx.getMessage());
		}
	}

	@AuraEnabled
	public static LightningResponse sendForApproval(Id opportunityId, String documentSignature) {
		Savepoint savepoint = Database.setSavePoint();
		try {
			if (documentSignature == OPPORTUNITY_SIGNATURE_DOCUSIGN) {
				String url = DocuSignButtonController.prepareOpportunityAndCreateEnvelopeURL(opportunityId, false);
				updateSignatureStatus(opportunityId);
				return new LightningResponse()
					.setSuccess(System.label.OppDocumentGeneration_SentToDocuSignMessage)
					.setBody(new Map<String, String>{ 'url' => url });
			} else if (documentSignature == OPPORTUNITY_SIGNATURE_CLICKAPPROVE) {
				LightningResponse response = sendQuoteApproval(opportunityId);
				if (response != null) {
					return response;
				}
				updateSignatureStatus(opportunityId);
				return new LightningResponse().setSuccess(System.label.OppDocumentGeneration_SentToClickApproveMessage);
			} else {
				return new LightningResponse().setError(System.label.OppDocumentGeneration_SignatureOtherMethodMessage);
			}
		} catch (DMLException objEx) {
			return setError(savepoint, objEx.getDmlMessage(0));
		} catch (Exception objEx) {
			return setError(savepoint, objEx.getMessage());
		}
	}

	public static LightningResponse invokeCreateDocument(Id opportunityId) {
		List<Opportunity> opportunities = getOpportunity(opportunityId);
		if (opportunities.isEmpty()) {
			return new LightningResponse().setError(System.Label.OppDocumentGeneration_BasketNotPrimaryMessage);
		}

		Map<String, mmdoc__Document_Template__c> mapNameWithDocumentTemplate = new Map<String, mmdoc__Document_Template__c>();
		Map<Id, mmdoc__Document_Template__c> mapIdWithDocumentTemplate = new Map<Id, mmdoc__Document_Template__c>();
		List<mmdoc__Document_Template__c> documentTemplate = getDocumentTemplates();
		for (mmdoc__Document_Template__c template : documentTemplate) {
			mapNameWithDocumentTemplate.put(template.Name, template);
			mapIdWithDocumentTemplate.put(template.Id, template);
		}

		List<csclm__Agreement__c> agreements = getAgreementsObjects(opportunityId);
		if (agreements.isEmpty()) {
			return new LightningResponse().setError(System.Label.OppDocumentGeneration_BusinessInternetPropositionMissingOnProduct);
		}

		Id agreementMavenDocRecordTypeId = Schema.SObjectType.csclm__Agreement__c.getRecordTypeInfosByDeveloperName()
			.get(AGREEMENT_MAVEN_DOC_RECORD_TYPE_NAME)
			.getRecordTypeId();

		List<csclm__Agreement__c> agreementsToDelete = getAgreements(opportunityId, agreementMavenDocRecordTypeId);
		insert agreements;

		List<mmdoc__Document_Request__c> documentRequests = new List<mmdoc__Document_Request__c>();
		for (csclm__Agreement__c agreement : agreements) {
			if (mapIdWithDocumentTemplate.containsKey(agreement.Maven_Document_Template__c)) {
				documentRequests.add(
					new mmdoc__Document_Request__c(
						mmdoc__Record_Id__c = agreement.Id,
						Agreement__c = agreement.Id,
						mmdoc__Document_Template__c = agreement.Maven_Document_Template__c,
						mmdoc__Document_Solution__c = mapIdWithDocumentTemplate.get(agreement.Maven_Document_Template__c).mmdoc__Document_Solution__c,
						mmdoc__Status__c = DOCUMENT_REQUEST_STATUS_NEW,
						mmdoc__Document_Format__c = DOCUMENT_REQUEST_DOCUMENT_FORMAT_PDF,
						mmdoc__Save_As__c = DOCUMENT_REQUEST_SAVE_AS_ATTACHMENT,
						mmdoc__Processing_Type__c = DOCUMENT_REQUEST_PROCESSING_TYPE_BATCH
					)
				);
			}
		}

		insert documentRequests;
		deletedCreatedDocument(agreementsToDelete);

		return new LightningResponse().setSuccess(System.Label.OppDocumentGeneration_AgreementCreatedMessage);
	}

	private static String getOriginalContractEndDate(CS_ContractNumber contractNumber) {
		if (contractNumber == null || contractNumber.majorVersion == null || contractNumber.minorVersion == null) {
			return null;
		}
		if (contractNumber.majorVersion == 3) {
			if (contractNumber.minorVersion == 1) {
				contractNumber.majorVersion = 2;
			} else {
				contractNumber.minorVersion = contractNumber.minorVersion - 1;
			}
		}
		return contractNumber.toString();
	}

	public static List<csclm__Agreement__c> getAgreementsObjects(Id opportunityId) {
		List<Opportunity> opportunities = getOpportunity(opportunityId);
		Map<String, mmdoc__Document_Template__c> mapNameWithDocumentTemplate = getDocumentTemplatesMap();
		Map<String, Map<String, CS_ContractNumber>> mapChangeTypeWithProposition = getChangeTypeWithPropostionMap(opportunityId);

		Boolean skipContractSummaryCreation = false;

		Id agreementMavenDocRecordTypeId = Schema.SObjectType.csclm__Agreement__c.getRecordTypeInfosByDeveloperName()
			.get(AGREEMENT_MAVEN_DOC_RECORD_TYPE_NAME)
			.getRecordTypeId();

		List<csclm__Agreement__c> agreements = new List<csclm__Agreement__c>();
		Map<String, Map<String, Boolean>> mapPropositionWithGeneratedContract = new Map<String, Map<String, Boolean>>{};
		for (String changeType : mapChangeTypeWithProposition.keySet()) {
			for (Orderform_Proposition__mdt proposition : getPropositionSetting()) {
				if (
					mapChangeTypeWithProposition.get(changeType).containsKey(proposition.MasterLabel) &&
					mapNameWithDocumentTemplate.containsKey(proposition.Contract_Template__c) ||
					mapNameWithDocumentTemplate.containsKey(proposition.Amendment_Contract_Template__c)
				) {
					for (String propositionOli : mapChangeTypeWithProposition.get(changeType).keySet()) {
						CS_ContractNumber csContractobject = mapChangeTypeWithProposition.get(changeType).get(propositionOli);
						if (
							proposition.MasterLabel == propositionOli &&
							mapPropositionWithGeneratedContract?.get(proposition.Proposition_Category__c)?.get(changeType) == null
						) {
							mapPropositionWithGeneratedContract.put(
								proposition.Proposition_Category__c,
								new Map<String, Boolean>{ changeType => true }
							);
							agreements.add(
								new csclm__Agreement__c(
									RecordTypeId = agreementMavenDocRecordTypeId,
									Original_Contract_Date__c = csContractobject.originalContractDate,
									Name = csContractobject.toString(),
									Contract_Number__c = csContractobject.toString(),
									Maven_Document_Template__c = csContractobject.majorVersion == 2
										? mapNameWithDocumentTemplate.get(proposition.Contract_Template__c).Id
										: mapNameWithDocumentTemplate.get(proposition.Amendment_Contract_Template__c).Id,
									Original_Contract_Number__c = getOriginalContractEndDate(csContractobject),
									Service_Name__c = proposition.Proposition_Category__c,
									csclm__Opportunity__c = opportunities[0].Id,
									Product_Basket__c = opportunities[0].Primary_Basket__c,
									csclm__Account__c = opportunities[0].AccountId,
									csclm__Output_Format__c = AGREEMENT_OUTPUT_FORMAT_PDF,
									csclm__Approval_Type__c = AGREEMENT_APPROVAL_TYPE_NO_APPROVAL,
									csclm__Approval_Status__c = AGREEMENT_APPROVAL_STATUS_APPROVED,
									Skip_Contract_Summary__c = skipContractSummaryCreation
								)
							);
							skipContractSummaryCreation = true;
						}
					}
				}
			}
		}
		return agreements;
	}

	private static Map<String, mmdoc__Document_Template__c> getDocumentTemplatesMap() {
		Map<String, mmdoc__Document_Template__c> mapNameWithDocumentTemplate = new Map<String, mmdoc__Document_Template__c>();
		List<mmdoc__Document_Template__c> documentTemplate = getDocumentTemplates();
		for (mmdoc__Document_Template__c template : documentTemplate) {
			mapNameWithDocumentTemplate.put(template.Name, template);
		}
		return mapNameWithDocumentTemplate;
	}

	private static Map<String, Map<String, CS_ContractNumber>> getChangeTypeWithPropostionMap(Id opportunityId) {
		Map<String, Map<String, CS_ContractNumber>> mapChangeTypeWithProposition = new Map<String, Map<String, CS_ContractNumber>>();
		for (OpportunityLineItem oli : getOpportunityProducts(opportunityId)) {
			CS_ContractNumber csContractobject = (CS_ContractNumber) JSON.deserialize(oli.ContractNumber_JSON__c, CS_ContractNumber.class);
			if (
				csContractobject != null &&
				mapChangeTypeWithProposition.containsKey(
					String.valueOf(csContractobject.majorVersion) +
					'.' +
					String.valueOf(csContractobject.minorVersion)
				)
			) {
				mapChangeTypeWithProposition.get(String.valueOf(csContractobject.majorVersion) + '.' + String.valueOf(csContractobject.minorVersion))
					.put(oli.Proposition__c, csContractobject);
			} else if (csContractobject != null) {
				mapChangeTypeWithProposition.put(
					String.valueOf(String.valueOf(csContractobject.majorVersion) + '.' + String.valueOf(csContractobject.minorVersion)),
					new Map<String, CS_ContractNumber>{ oli.Proposition__c => csContractobject }
				);
			}
		}
		return mapChangeTypeWithProposition;
	}

	private static void deletedCreatedDocument(List<csclm__Agreement__c> agreements) {
		if (!agreements.isEmpty()) {
			delete agreements;
		}
	}

	private static List<OpportunityLineItem> getOpportunityProducts(Id opportunityId) {
		return [
			SELECT Id, Location__c, Proposition__c, Proposition_Type__c, Contract_Number__c, ContractNumber_JSON__c
			FROM OpportunityLineItem
			WHERE OpportunityId = :opportunityId AND Proposition__c != NULL AND Contract_Number__c != NULL
		];
	}

	private static List<csclm__Agreement__c> getAgreements(Id opportunityId, Id recordTypeId) {
		return [
			SELECT Id, csclm__Document_Template__c
			FROM csclm__Agreement__c
			WHERE csclm__Opportunity__c = :opportunityId AND RecordTypeId = :recordTypeId
		];
	}

	private static List<ContentDocumentLink> getListContentDocument(Id opportunityId) {
		return [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :opportunityId LIMIT 1];
	}

	private static LightningResponse sendQuoteApproval(String oppId) {
		List<CSCAP.API_1.MultipleSendApprovalRequestRecord> approvalRequests = new List<CSCAP.API_1.MultipleSendApprovalRequestRecord>();
		Set<Id> setAttachmentIds = new Set<Id>();
		List<Opportunity> oppList = getOpportunity(oppId);
		if (!oppList.isEmpty()) {
			CSCAP__Click_Approve_Setting__c lstCAS = [
				SELECT Id, Name
				FROM CSCAP__Click_Approve_Setting__c
				WHERE Name LIKE :CLICK_APPROVE_SETTING
				LIMIT 1
			];

			deleteAttachmentsRecords(oppId);
			setAttachmentIds = createAttachmentsRecords(oppId);

			if (oppList[0].DS_Authorized_to_sign_1st__c == null) {
				setAuthorizeToSignFirstOnOpportunity(oppList[0]);
				// re-query opportunity after authorize to sign is updated
				oppList = getOpportunity(oppId);
			}

			if (oppList[0].DS_Authorized_to_sign_1st__c != null) {
				CSCAP.API_1.MultipleSendApprovalRequestRecord approvalRequest = new CSCAP.API_1.MultipleSendApprovalRequestRecord();
				approvalRequest.approvalObjectId = oppId;
				approvalRequest.recipientContactId = oppList[0].DS_Authorized_to_sign_1st__c;
				approvalRequest.clickApproveSettingId = lstCAS.Id;
				approvalRequest.attachmentIds = setAttachmentIds;
				approvalRequests.add(approvalRequest);
			} else {
				return new LightningResponse().setError(System.label.OppDocumentGeneration_Authorized_to_Sign_Empty_Message);
			}
			if (!approvalRequests.isEmpty() && !Test.isRunningTest()) {
				CSCAP.API_1.MultipleSendApprovalRequest(approvalRequests);
			}
		}
		return null;
	}

	private static Set<Id> createAttachmentsRecords(Id opportunityId) {
		Set<Id> setAttachmentIds = new Set<Id>();
		Set<Id> documentIds = new Set<Id>();
		List<Attachment> lstAttachment = new List<Attachment>();
		List<CS_DocItem> documents = CS_OppDocController.getDocuments(opportunityId);
		for (CS_DocItem document : documents) {
			if (document.docId != null) {
				documentIds.add(document.docId);
			}
		}

		Attachment newAttachment;
		for (Attachment att : [SELECT Id, Body, Name, Description, ParentId FROM Attachment WHERE Id = :documentIds]) {
			newAttachment = att.clone();
			newAttachment.ParentId = opportunityId;
			lstAttachment.add(newAttachment);
		}

		Set<Id> idsToCheckSObjectType = getServiceIds(documents);
		Set<Id> contentDocumentIds = new Set<Id>();
		Set<Id> contentVersionIds = new Set<Id>();
		for (Id idToCheckSObjectType : idsToCheckSObjectType) {
			String sObjectType = idToCheckSObjectType.getSObjectType().getDescribe().getName();
			if (sObjectType == 'ContentDocument') {
				contentDocumentIds.add(idToCheckSObjectType);
				continue;
			}
			if (sObjectType == 'ContentVersion') {
				contentVersionIds.add(idToCheckSObjectType);
				continue;
			}
		}

		List<ContentVersion> cvList = [
			SELECT Title, VersionData, FileExtension
			FROM ContentVersion
			WHERE Id IN :contentVersionIds AND IsLatest = TRUE
		];

		for (ContentVersion contentVersion : cvList) {
			Blob fileData = contentVersion.VersionData;
			lstAttachment.add(
				new Attachment(
					ParentId = opportunityId,
					body = fileData,
					name = contentVersion.Title + '.' + AGREEMENT_OUTPUT_FORMAT_PDF,
					contenttype = AGREEMENT_CONTENT_TYPE
				)
			);
		}
		insert lstAttachment;

		for (Attachment attch : lstAttachment) {
			setAttachmentIds.add(attch.Id);
		}

		return setAttachmentIds;
	}

	private static void deleteAttachmentsRecords(Id opportunityId) {
		delete [SELECT Id FROM Attachment WHERE ParentId = :opportunityId];
	}

	private static void setAuthorizeToSignFirstOnOpportunity(Opportunity opportunity) {
		Id contactId = opportunity?.Account?.Authorized_to_sign_1st__c;
		List<Contact> contacts = [SELECT Id FROM Contact WHERE Id = :contactId AND AccountId = :opportunity.AccountId];
		// when the authorize to sign first on Account is a contact related to opportunity's account, set authorize to sign first
		if (!contacts.isEmpty()) {
			update new Opportunity(Id = opportunity.Id, DS_Authorized_to_sign_1st__c = contactId);
		}
	}

	private static Set<Id> getServiceIds(List<CS_DocItem> documents) {
		List<String> serviceDescriptionUrls = new List<String>();
		for (CS_DocItem document : documents) {
			if (document.docId == null && !String.isBlank(document.serviceDescriptionUrl)) {
				serviceDescriptionUrls.add(document.serviceDescriptionUrl);
			}
		}

		Set<Id> idsToCheckSObjectType = new Set<Id>();
		for (String serviceDescriptionUrl : serviceDescriptionUrls) {
			List<Id> retrievedIds = new List<Id>();
			retrievedIds.add(Id.valueOf(serviceDescriptionUrl.substringAfterLast('/').substringBefore('?')));
			idsToCheckSObjectType.add(retrievedIds[0]);
		}
		return idsToCheckSObjectType;
	}

	private static void updateSignatureStatus(Id opportunityId) {
		Opportunity opportunity = new Opportunity(
			Id = opportunityId,
			Docusign_Attachment_Status__c = DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED,
			Signature_Status__c = OPPORTUNITY_SIGNATURE_STATUS_PENDING_FOR_APPROVAL
		);
		update opportunity;
	}

	@TestVisible
	private static LightningResponse setError(Savepoint savepoint, String message) {
		if (savepoint != null) {
			Database.rollback(savepoint);
		}
		return new LightningResponse().setError(message);
	}

	public class BasketResponse {
		@AuraEnabled
		public String id;
		@AuraEnabled
		public String name;
		@AuraEnabled
		public Boolean isStatusApproved;
		@AuraEnabled
		public String documentSignature;
		@AuraEnabled
		public Boolean isSignatureDocusign;
		@AuraEnabled
		public Boolean isSignatureClickApprove;
		@AuraEnabled
		public Boolean isSignatureOther;
		@AuraEnabled
		public String sendForApprovalButtonLabel;
		@AuraEnabled
		public boolean creditCheckApproved;

		public BasketResponse(Opportunity opportunity) {
			cscfga__Product_Basket__c basket = opportunity?.Primary_Basket__r;
			if (basket != null) {
				this.id = basket.Id;
				this.name = basket.Name;
				this.isStatusApproved = basket.Basket_Approval_Status__c == BASKET_STATUS_APPROVED;
			}
			this.creditCheckApproved = opportunity?.Credit_Check__c == 'Approved' ? true : false;
			this.documentSignature = opportunity?.Document_Signature__c;
			this.isSignatureDocusign = opportunity?.Document_Signature__c == OPPORTUNITY_SIGNATURE_DOCUSIGN;
			this.isSignatureClickApprove = opportunity?.Document_Signature__c == OPPORTUNITY_SIGNATURE_CLICKAPPROVE;
			this.isSignatureOther = !(this.isSignatureDocusign || this.isSignatureClickApprove);
			this.sendForApprovalButtonLabel = String.format(
				System.Label.OppDocumentGeneration_SendForApprovalButton,
				new List<String>{ this.documentSignature }
			);
		}
	}
}
