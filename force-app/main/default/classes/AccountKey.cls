public with sharing class AccountKey {
	public String sourceSystem { get; set; }
	public String value { get; set; }

    public AccountKey(String sourceSystem, String value) {
        this.sourceSystem = sourceSystem;
        this.value = value;
    }
}