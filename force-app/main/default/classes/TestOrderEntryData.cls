@isTest
public with sharing class TestOrderEntryData {
	@TestSetup
	static void makeData() {
		TestUtils.createCompleteOpportunity();
		OrderEntryTestDataFactory.createOrderEntryProducts();
	}

	@isTest
	static void testDataCreation() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Contact cnt = [SELECT Id FROM Contact LIMIT 1];

		OrderEntryData oeData = new OrderEntryData();
		oeData.accountId = acc.Id;
		oeData.opportunityId = opp.Id;
		oeData.primaryContactId = cnt.Id;

		oeData.lastStep = 'last step';
		oeData.b2cInternetCustomer = false;
		oeData.notes = 'notes';
		oeData.mainProduct = null;
		oeData.journeyType = 'Fixed';

		OrderEntryData.Availability availability = new OrderEntryData.Availability();
		availability.available = true;
		availability.name = 'name';
		OrderEntryData.SiteCheck siteCheck = new OrderEntryData.SiteCheck();
		siteCheck.availability = new List<OrderEntryData.Availability>{ availability };
		siteCheck.city = 'city';
		siteCheck.street = 'street';
		siteCheck.houseNumber = 'houseNumber';
		siteCheck.houseNumberExt = 'houseNumberExt';
		siteCheck.zipCode = 'zipCode';
		siteCheck.status = new List<String>{ 'status1', 'status2' };
		siteCheck.footprint = 'footprint';

		OrderEntryData.OrderEntrySite site = new OrderEntryData.OrderEntrySite();
		site.siteId = acc.Id;
		site.type = 'type';
		site.siteCheck = siteCheck;
		site.addressCheckResult = 'acr';
		site.offNetType = 'ont';
		oeData.sites = new List<OrderEntryData.OrderEntrySite>{ site };

		OrderEntryData.OrderEntryAddon oea = new OrderEntryData.OrderEntryAddon();
		oea.id = acc.Id;
		oea.type = 'type';
		oea.name = 'name';
		oea.code = 'code';
		oea.quantity = 5;
		oea.recurring = 0.5;
		oea.oneOff = 5.0;
		oea.totalRecurring = 5.0;
		oea.totalOneOff = 5.0;
		oea.parentProduct = acc.Id;
		oea.bundleName = 'bundleName';
		oea.parentType = 'parentType';

		OrderEntryData.ProductBundle pbundle = new OrderEntryData.ProductBundle();
		pbundle.id = acc.Id;
		pbundle.type = 'type';
		pbundle.name = 'name';
		pbundle.code = 'code';
		pbundle.quantity = 1;
		pbundle.recurring = 1.1;
		pbundle.oneOff = 1.1;
		pbundle.totalRecurring = 1.1;
		pbundle.totalOneOff = 1.1;
		pbundle.parentProduct = acc.Id;
		pbundle.bundleName = 'bundleName';
		pbundle.promoDescription = 'Promod desc';

		OrderEntryData.OrderEntryProduct oep = new OrderEntryData.OrderEntryProduct();
		oep.id = acc.Id;
		oep.type = 'type';

		OrderEntryData.Telephony tel = new OrderEntryData.Telephony();
		tel.enabled = false;
		tel.portingEnabled = true;
		tel.portingNumber = '13';
		Map<String, OrderEntryData.Telephony> telMap = new Map<String, OrderEntryData.Telephony>();
		telMap.put('tel1', tel);

		OrderEntryData.Discount disc = new OrderEntryData.Discount();
		disc.id = acc.Id;
		disc.name = 'name';
		disc.type = 'type';
		disc.duration = 12.0;
		disc.value = 13.0;
		disc.level = 'level';
		disc.product = acc.Id;
		disc.product = acc.Id;
		disc.addon = acc.Id;

		OrderEntryData.Promotion prom = new OrderEntryData.Promotion();
		prom.id = acc.Id;
		prom.name = 'name';
		prom.type = 'type';
		prom.contractTerms = 'contractTerms';
		prom.customerType = 'customerType';
		prom.connectionType = 'connectionType';
		prom.offNetType = 'offNetType';
		prom.discounts = new List<OrderEntryData.Discount>{ disc };

		OrderEntryData.PreferredDate d = new OrderEntryData.PreferredDate();
		d.selectedDate = Date.today();
		d.dayPeriod = 'night';

		OrderEntryData.Installation install = new OrderEntryData.Installation();
		install.earliestInstallationDate = Date.today();
		install.customerInformed = false;

		OrderEntryData.OperatorSwitch oSwitch = new OrderEntryData.OperatorSwitch();
		oSwitch.requested = true;
		oSwitch.currentProvider = 'currentProvider';
		oSwitch.currentContractNumber = 'currentContractNumber';
		oSwitch.potentialFeeAccepted = true;

		OrderEntryData.OrderEntryBundle bundle = new OrderEntryData.OrderEntryBundle();
		bundle.type = 'type';
		bundle.bundle = pbundle;
		bundle.contractTerm = '1';
		bundle.simCard = 'ESIM';
		bundle.addons = new List<OrderEntryData.OrderEntryAddon>{ oea };
		bundle.promos = new List<OrderEntryData.Promotion>{ prom };
		bundle.products = new List<OrderEntryData.OrderEntryProduct>{ oep };
		bundle.details = new Map<String, String>{ 'Test' => 'Test' };
		bundle.telephony = telMap;
		bundle.operatorSwitch = oSwitch;
		bundle.installation = install;
		oeData.bundles = new List<OrderEntryData.OrderEntryBundle>{ bundle };

		OrderEntryData.Payment payment = new OrderEntryData.Payment();
		payment.bankAccountHolder = 'bankAccountHolder';
		payment.paymentType = 'paymentType';
		payment.iban = 'iban';
		payment.billingChannel = 'billingChannel';
		payment.billingAccountId = acc.id;
		payment.street = 'street';
		payment.postalCode = '1015xx';
		payment.houseNumber = 'hn';
		payment.houseNumberSuffix = 's';
		payment.city = 'Imotski';
		oeData.payment = payment;

		System.assertEquals(oeData.payment.city, 'Imotski', 'Corect value.');
	}
}
