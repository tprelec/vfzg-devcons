@isTest
public with sharing class TestVFAssetsToOrderAction {
	public static NetProfit_Information__c netProfitInformation;
	public static User owner;
	public static Order__c order;
	public static VF_Contract__c contract;
	@isTest
	public static void testgetOrdersWithAllCTNsActivated() {
		setupTestData();
		Test.startTest();
		User dealerUser = TestUtils.createDealer();

		List<Contact> contactsList = new List<Contact>();
		List<VF_Asset__c> assetsList = new List<VF_Asset__c>();

		Account account = new Account();
		account.Name = 'test1';
		account.OwnerId = dealerUser.Id;
		insert account;

		contactsList.add(
			new Contact(AccountId = account.Id, LastName = 'TestLastname', FirstName = 'TestFirstname', Userid__c = dealerUser.Id, isactive__c = true)
		);
		insert contactsList;

		String random4charNumericString = String.valueOf(Math.mod(Math.round(Math.random() * 1000), 1000) + 1000);
		String random4charNumericString2 = String.valueOf(Math.mod(Math.round(Math.random() * 1000), 1000) + 1000);
		Dealer_Information__c newDealer = TestUtils.createDealerInformation(
			contactsList[0].Id,
			'99' + random4charNumericString2,
			'99' + random4charNumericString
		);

		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = 3,
			Duration__c = 1,
			CLC__c = 'Acq',
			VF_Contract__c = contract.Id,
			Order__c = order.Id,
			Framework_Id__c = '11254033501'
		);
		insert cp;

		NetProfit_CTN__c netProfitCTN = new NetProfit_CTN__c(
			CTN_Number__c = '31621505108',
			Framework_Agreement_Id__c = '11254033501',
			NetProfit_Information__c = netProfitInformation.Id,
			Order__c = order.Id,
			Contracted_Product__c = cp.Id,
			Action__c = 'Porting'
		);
		insert netProfitCTN;

		VF_Asset__c vfAsset = createAsset(account.Id, newDealer.Dealer_Code__c, '5', 'Active', '399999932', true);
		vfAsset.Agreement_Number__c = '11254033501';
		vfAsset.CTN__c = '31621505108';
		assetsList.add(vfAsset);
		insert assetsList;

		List<VFAssetsToOrderAction.ReturnValues> returnValuesList = VFAssetsToOrderAction.getOrdersWithAllCTNsActivated();
		System.assertNotEquals(null, returnValuesList[0].orderList, 'VF Asset is not matching NetProfitCTN record');
		Test.stopTest();
	}

	public static VF_Asset__c createAsset(
		Id accId,
		String dealerCode,
		String contractNumber,
		String ctnStatus,
		String banNumber,
		Boolean isRetention
	) {
		VF_Asset__c newAsset = new VF_Asset__c();
		newAsset.Account__c = accId;
		newAsset.Initial_dealer_code__c = dealerCode;
		newAsset.Contract_Number__c = contractNumber;
		newAsset.CTN_Status__c = ctnStatus;
		newAsset.BAN_Number__c = banNumber;
		newAsset.Contract_Start_Date__c = system.today().addMonths(-6);
		newAsset.Contract_End_Date__c = newAsset.Contract_Start_Date__c.addMonths(6);
		if (isRetention) {
			newAsset.Retention_Date__c = system.today();
			newAsset.Retention_Dealer_code__c = dealerCode;
			newAsset.Contract_Start_Date__c = system.today();
			newAsset.Contract_End_Date__c = newAsset.Contract_Start_Date__c.addMonths(12);
		}
		return newAsset;
	}

	public static void setupTestData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, owner);
		opp.Select_Journey__c = 'Sales';
		update opp;
		contract = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		claimerContact.Email = 'a@b.com';
		update claimerContact;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, claimerContact.id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(claimerContact.id, '123321');
		contract.Dealer_Information__c = dealerInfo.Id;
		contract.Unify_Framework_Id__c = '9634563A';
		contract.Signed_by_Customer__c = claimerContact.Id;
		update contract;

		order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contract.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			Billing_Arrangement__c = ba.id
		);
		insert order;

		netProfitInformation = TestUtils.createNetProfitInformation(order);
	}
}
