/**
 * @description:    Test class for RecordTypeAccessor
 **/
@isTest
public class TestRecordTypeAccessor {
	private static final String TEST_FAKE_RECORD_TYPE_ACCOUNT_ID = TestUtils.getFakeId(SObjectType.RecordType.getSObjectType());
	private static final SObjectType TEST_RECORD_TYPE_SOBJECT_TYPE_ACCOUNT = Account.getSObjectType();
	private static final String TEST_FAKE_RECORD_TYPE_DEVELOPER_NAME = 'TestRecordTypeAccessorDeveloperName';

	/**
	 * @description:    Tests retrieving record type developer name for Id
	 *                  when record type with Id exists
	 **/
	@isTest
	static void shouldRetrieveRecordTypeDeveloperNameForIdWhenRecordTypeWithIdExists() {
		// prepare data
		RecordType createdRecordType = createTestRecordType();

		// perform testing
		Test.startTest();
		RecordTypeAccessor.initRecordTypeMapping(new List<RecordType>{ createdRecordType });

		String recordTypeDeveloperName = RecordTypeAccessor.getRecordTypeDeveloperNameForId(createdRecordType.Id);
		Test.stopTest();

		// verify results
		System.assertEquals(createdRecordType.DeveloperName, recordTypeDeveloperName, 'Proper record type developer name for id should be retrieved');
	}

	/**
	 * @description:    Tests retrieving null record type developer name for Id
	 *                  when record type with Id does not exist
	 **/
	@isTest
	static void shouldRetrieveNullRecordTypeDeveloperNameForIdWhenRecordTypeWithIdDoesNotExist() {
		// prepare data

		// perform testing
		Test.startTest();
		String recordTypeDeveloperName = RecordTypeAccessor.getRecordTypeDeveloperNameForId(TEST_FAKE_RECORD_TYPE_ACCOUNT_ID);
		Test.stopTest();

		// verify results
		System.assertEquals(null, recordTypeDeveloperName, 'Null value record type developer name for id should be retrieved');
	}

	/**
	 * @description:    Tests retrieving record type id for SObject type
	 *                  and developer name when it exists
	 **/
	@isTest
	static void shouldRetrieveRecordTypeIdForSObjectTypeAndDeveloperNameWhenItExists() {
		// prepare data
		RecordType createdRecordType = createTestRecordType();

		// perform testing
		Test.startTest();
		RecordTypeAccessor.initRecordTypeMapping(new List<RecordType>{ createdRecordType });

		String recordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
			TEST_RECORD_TYPE_SOBJECT_TYPE_ACCOUNT,
			TEST_FAKE_RECORD_TYPE_DEVELOPER_NAME
		);
		Test.stopTest();

		// verify results
		System.assertEquals(createdRecordType.Id, recordTypeId, 'Proper id for sobject type and record type developer name should be retrieved');
	}
	/**
	 * @description:    Tests retrieving null record type id for SObject type
	 *                  and developer name when it does not exist
	 **/
	@isTest
	static void shouldRetrieveNullRecordTypeIdForSObjectTypeAndDeveloperNameWhenItDoesNotExist() {
		// prepare data

		// perform testing
		Test.startTest();
		String recordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
			TEST_RECORD_TYPE_SOBJECT_TYPE_ACCOUNT,
			TEST_FAKE_RECORD_TYPE_DEVELOPER_NAME
		);
		Test.stopTest();

		// verify results
		System.assertEquals(null, recordTypeId, 'Null value for sobject type and record type developer name should be retrieved');
	}

	/**
	 * @description:    Tests doing only 1 SOQL when accessor is called
	 *                  multiple times in same transaction
	 **/
	@isTest
	static void shouldDoOneSOQLWhenCalledMultipleTimesInSameTransaction() {
		// prepare data
		RecordType createdRecordType = createTestRecordType();

		// perform testing
		Test.startTest();
		RecordTypeAccessor.initRecordTypeMapping(new List<RecordType>{ createdRecordType });

		String recordTypeDeveloperName = RecordTypeAccessor.getRecordTypeDeveloperNameForId(createdRecordType.Id);
		String recordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
			TEST_RECORD_TYPE_SOBJECT_TYPE_ACCOUNT,
			TEST_FAKE_RECORD_TYPE_DEVELOPER_NAME
		);

		Integer queries = Limits.getQueries();
		Test.stopTest();

		// verify results
		System.assertEquals(1, queries, 'Only 1 SOQL query should be done');
	}

	/**
	 * @description:    Creates test record type
	 * @return          RecordType - created record type
	 **/
	private static RecordType createTestRecordType() {
		return new RecordType(
			Id = TEST_FAKE_RECORD_TYPE_ACCOUNT_ID,
			SObjectType = TEST_RECORD_TYPE_SOBJECT_TYPE_ACCOUNT.getDescribe().getName(),
			DeveloperName = TEST_FAKE_RECORD_TYPE_DEVELOPER_NAME
		);
	}
}
