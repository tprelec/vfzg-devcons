@isTest
public with sharing class CS_PlInputsTest {

    @isTest
    static void test() {
        CS_PlInputs inputs = new CS_PlInputs();
        System.assertNotEquals(null, inputs.flexibilityMargins);
        System.assertNotEquals(null, inputs.siteCountForConfig);
    }
}