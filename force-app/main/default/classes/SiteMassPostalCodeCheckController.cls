public with sharing class SiteMassPostalCodeCheckController {
	
	public SiteMassPostalCodeCheckController(ApexPages.StandardSetController controller) {
		accountId = ApexPages.currentPage().getParameters().get('id');
		fetchSites();

							
	}

	public Id accountId {get;set;}
	public List<siteWrapper> siteList {get;set;}

	public class siteWrapper {
		public siteWrapper(Site__c s, Boolean sel){
			theSite = s;
			selected = sel;
			if(s.Postal_Code_Check_Status__c != null){
				if(s.Postal_Code_Check_Status__c == 'Completed'){
					status = 'completed';
				} else {

					status = 'pending';
				} 
			}

		}
		public Site__c theSite {get;set;}
		public Boolean selected {get;set;}
		public String status {get;set;}
	}

	public void fetchSites(){
		siteList = new List<siteWrapper>();
		sitesInProgress = false;
		for(Site__c s : [Select Id, 
							Name, 
							Blocked_Checks__c,
							Last_dsl_check__c,
							Last_fiber_check__c,
							Postal_Code_Check_Status__c,
							Site_Postal_Code__c, 
							Site_House_Number__c, 
							Site_House_Number_Suffix__c,
							Site_City__c
						From 
							Site__c
						Where
							Site_Account__c = :accountId]){
			if(s.Postal_Code_Check_Status__c == 'DSL check requested' 
				|| s.Postal_Code_Check_Status__c == 'Fiber check requested'
				|| s.Postal_Code_Check_Status__c == 'Combined check requested'){
				sitesInProgress = true;
			}
			siteList.add(new siteWrapper(s,false));

		}
	}

	public Boolean sitesInProgress {get;set;}

	public pageReference backToAccount(){
		return new pageReference('/'+accountId);
	}

	/*
	 *	Description:	returns 0 if request is OK. Returns number of allowed checks if requested amount is too high
	 */
	public Integer fiberChecksAllowed(Integer numberOfSitesRequested){
		Integer counter = [ Select count() 
		                    FROM 
		                    	Site__c 
		                    Where 
		                    	LastModifiedDate = LAST_N_DAYS:1
		                    AND
			                	Last_fiber_check__c = LAST_N_DAYS:1
		                    ]; 
        Integer dailyBatchChecksAllowed = Integer.valueOf(Sales_Settings__c.getInstance().Max_daily_postalcode_checks__c) - 50;
		Integer batchChecksAllowedToday = dailyBatchChecksAllowed - counter;

		system.debug(numberOfSitesRequested);
		system.debug(batchChecksAllowedToday);
		system.debug(dailyBatchChecksAllowed);
        if(numberOfSitesRequested > batchChecksAllowedToday){
			return batchChecksAllowedToday;
        } else {	
			return 0;
		}		

	}

	public pageReference scheduleDsl(){

		List<Site__c> sitesToUpdate = new List<Site__c>();

		for(siteWrapper sw : siteList){
			if(sw.selected && (sw.theSite.Postal_Code_Check_Status__c == null || sw.theSite.Postal_Code_Check_Status__c =='Completed') && sw.theSite.Last_dsl_check__c == null){
				sw.theSite.Postal_Code_Check_Status__c = 'DSL check requested';
				sw.status = 'pending';
				sitesToUpdate.add(sw.theSite);
			}
			sw.selected = false;

		}
		scheduleCheck(sitesToUpdate);      		
	    update sitesToUpdate;
	    sitesInProgress = true;
		return null;
	}

	public pageReference scheduleFiber(){
		List<Site__c> sitesToUpdate = new List<Site__c>();

		// first count number of selected sites
		Integer sitesSelected = 0;
		for(siteWrapper sw : siteList){
			if(sw.selected && (sw.theSite.Postal_Code_Check_Status__c == null || sw.theSite.Postal_Code_Check_Status__c =='Completed') && sw.theSite.Last_fiber_check__c == null){
				sitesSelected++;
			}
		}

		Integer checkAllowed = fiberChecksAllowed(sitesSelected);
		if(checkAllowed == 0){
			for(siteWrapper sw : siteList){
				if(sw.selected && (sw.theSite.Postal_Code_Check_Status__c == null || sw.theSite.Postal_Code_Check_Status__c =='Completed') && sw.theSite.Last_fiber_check__c == null){
					sw.theSite.Postal_Code_Check_Status__c = 'Fiber check requested';
					sw.status = 'pending';
					sitesToUpdate.add(sw.theSite);
				}
				sw.selected = false;

			}

			scheduleCheck(sitesToUpdate);     		
		    update sitesToUpdate;
			sitesInProgress = true;
		} else {
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'Daily max reached for batch fiber checks. Only '+checkAllowed+' more checks allowed.'));
		}
		return null;
	}

	public pageReference scheduleBoth(){

		List<Site__c> sitesToUpdate = new List<Site__c>();

		// first count number of selected sites
		Integer sitesSelected = 0;		
		for(siteWrapper sw : siteList){
			if(sw.selected && (sw.theSite.Postal_Code_Check_Status__c == null || sw.theSite.Postal_Code_Check_Status__c =='Completed') && sw.theSite.Last_dsl_check__c == null && sw.theSite.Last_fiber_check__c == null){
				sitesSelected++;
			}
		}

		Integer checkAllowed = fiberChecksAllowed(sitesSelected);
		if(checkAllowed == 0){

			for(siteWrapper sw : siteList){
				if(sw.selected && (sw.theSite.Postal_Code_Check_Status__c == null || sw.theSite.Postal_Code_Check_Status__c =='Completed') && sw.theSite.Last_dsl_check__c == null && sw.theSite.Last_fiber_check__c == null){
					sw.theSite.Postal_Code_Check_Status__c = 'Combined check requested';
					sw.status = 'pending';
					sitesToUpdate.add(sw.theSite);
				}
				sw.selected = false;

			}

			scheduleCheck(sitesToUpdate);     		
		    update sitesToUpdate;
			sitesInProgress = true;
		} else {
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,'Daily max reached for batch fiber checks. Only '+checkAllowed+' more checks allowed.'));
		}
		return null;
	}	

	public void scheduleCheck(List<Site__c> sitesToCheck){
		
		/*DateTime n = datetime.now().addSeconds(5);
		String cron = '';

		cron += n.second();
		cron += ' ' + n.minute();
		cron += ' ' + n.hour();
		cron += ' ' + n.day();
		cron += ' ' + n.month();
		cron += ' ' + '?';
		cron += ' ' + n.year();
		Id scheduleId = System.schedule('Postalcode check '+system.now()+' '+system.currentTimeMillis(), cron, new ScheduleSitePostalCodeCheckBatch());  */

		// instead of scheduling, start a queue
		if(!sitesToCheck.isEmpty()){
			system.debug(sitesToCheck.size());
			// then call the enqueue job with the list of lists
			System.enqueueJob(new QueueablePostalcodeCheck(sitesToCheck));
		}		


	}

}