@RestResource(urlMapping='/UpdateMobileOrder/*')
global without sharing class EMP_UpdateMobileOrder {
	@HttpPost
	global static void doPost() {
		String correlationId;
		String action;
		String status;
		String orderId;
		List<ErrorData> errors;
		List<EMP_ActivateSIMForTemplate.Data> activateSimData;
		List<EMP_UpdateProductMultipleSettings.Data> updateProductSettingsData;
		List<EMP_GetOrderSummary.Data> orderSummaryData;
		List<EMP_ValidateOrderFE.Data> validateOrderData;

		RestRequest req = RestContext.request;
		RestResponse res = Restcontext.response;
		Blob body = req.requestBody;

		JSONParser parser = JSON.createParser(body.toString());
		while (parser.nextToken() != System.JSONToken.END_OBJECT) {
			if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
				String text = parser.getText();
				if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
					switch on text {
						when 'correlation_Id' {
							correlationId = parser.getText();
						}
						when 'action' {
							action = parser.getText();
						}
						when 'status' {
							status = parser.getText();
						}
						when 'orderID' {
							orderId = parser.getText();
						}
						when 'error' {
							errors = (List<ErrorData>) parser.readValueAs(List<ErrorData>.class);
						}
						when 'data' {
							switch on action {
								when 'ActivateSimForTemplate' {
									activateSimData = (List<EMP_ActivateSIMForTemplate.Data>) parser.readValueAs(
										List<EMP_ActivateSIMForTemplate.Data>.class
									);
								}
								when 'UpdateProductMultipleSettings' {
									EMP_UpdateProductMultipleSettings.Data testdata = new EMP_UpdateProductMultipleSettings.Data(parser);
									updateProductSettingsData = new List<EMP_UpdateProductMultipleSettings.Data>();
									updateProductSettingsData.add(testdata);
								}
								when 'GetOrderSummary' {
									orderSummaryData = (List<EMP_GetOrderSummary.Data>) parser.readValueAs(List<EMP_GetOrderSummary.Data>.class);
								}
								when 'ValidateOrderFE' {
									validateOrderData = (List<EMP_ValidateOrderFE.Data>) parser.readValueAs(List<EMP_ValidateOrderFE.Data>.class);
								}
							}
						}
					}
				}
			}
		}

		List<NetProfit_CTN__c> lstCTNstoUpdate = getCTNs(correlationId);
		// Process data according to Action and object type it can be executed one per webservice invoke
		lstCTNstoUpdate = processActivateSIM(lstCTNstoUpdate, action, status, activateSimData, errors);
		lstCTNstoUpdate = processUpdateProduct(lstCTNstoUpdate, action, status, updateProductSettingsData, errors);
		lstCTNstoUpdate = processGetOrderSummary(lstCTNstoUpdate, action, status, orderSummaryData, errors);
		lstCTNstoUpdate = processValidateOrderFE(lstCTNstoUpdate, action, status, validateOrderData, errors);
		update lstCTNstoUpdate;
		// Create Orch process to update status after the CTN has the statuses from BSL
		createStatusOrchProcess(lstCTNstoUpdate, action);
		return;
	}

	private static List<NetProfit_CTN__c> getCTNs(String correlationId) {
		NetProfit_CTN__c objCTNtoUpdate = [SELECT Id, Order__c, CTN_Number__c FROM NetProfit_CTN__c WHERE Id = :correlationId LIMIT 1];
		return [
			SELECT Id, Name, CTN_Number__c, CTNPortingStatus__c, Action__c, Order_Summary_BSL_Status__c, UnifyCreditVettingStatus__c
			FROM NetProfit_CTN__c
			WHERE CTN_Number__c = :objCTNtoUpdate.CTN_Number__c AND Order__c = :objCTNtoUpdate.Order__c
			FOR UPDATE
		];
	}

	private static List<NetProfit_CTN__c> processActivateSIM(
		List<NetProfit_CTN__c> lstCTNstoUpdate,
		String action,
		String status,
		List<EMP_ActivateSIMForTemplate.Data> activateSimData,
		List<ErrorData> errors
	) {
		if (!action.equalsIgnoreCase('ActivateSIMForTemplate')) {
			return lstCTNstoUpdate;
		}
		if (status.equalsIgnoreCase('200')) {
			for (NetProfit_CTN__c tempCTN : lstCTNstoUpdate) {
				tempCTN.CTN_Status__c = 'SIM Activation Order ID received';
				tempCTN.SIM_Activation_Order_Id__c = activateSimData[0].orderId;
				tempCTN.SIM_Activation_Error__c = '';
				tempCTN.SIM_Activation_Timestamp__c = System.now();
			}
		}
		if (!errors.isEmpty()) {
			for (NetProfit_CTN__c tempCTN : lstCTNstoUpdate) {
				tempCTN.CTN_Status__c = 'SIM Activation failed';
				tempCTN.SIM_Activation_Error__c = JSON.serialize(errors).abbreviate(255);
				tempCTN.Error_on_Connect_GLP__c = errors?.get(0).message?.contains(Label.EMP_GLPNotConnected);
			}
		}
		return lstCTNstoUpdate;
	}

	private static List<NetProfit_CTN__c> processUpdateProduct(
		List<NetProfit_CTN__c> lstCTNstoUpdate,
		String action,
		String status,
		List<EMP_UpdateProductMultipleSettings.Data> updateProductSettingsData,
		List<ErrorData> errors
	) {
		if (!action.equalsIgnoreCase('UpdateProductMultipleSettings')) {
			return lstCTNstoUpdate;
		}
		if (status.equalsIgnoreCase('200')) {
			for (NetProfit_CTN__c tempCTN : lstCTNstoUpdate) {
				tempCTN.CTN_Status__c = 'Update Product Settings confirmed';
				tempCTN.Unify_Order_Id_c__c = updateProductSettingsData[0].orderId;
				tempCTN.Update_Product_Settings_Error__c = '';
				tempCTN.Update_Settings_Confirmed_Timestamp__c = System.now();
			}
		}
		if (!errors.isEmpty()) {
			for (NetProfit_CTN__c tempCTN : lstCTNstoUpdate) {
				tempCTN.CTN_Status__c = 'Update Product Settings failed';
				tempCTN.Update_Product_Settings_Error__c = JSON.serialize(errors).abbreviate(255);
			}
		}
		return lstCTNstoUpdate;
	}

	private static List<NetProfit_CTN__c> processGetOrderSummary(
		List<NetProfit_CTN__c> lstCTNstoUpdate,
		String action,
		String status,
		List<EMP_GetOrderSummary.Data> orderSummaryData,
		List<ErrorData> errors
	) {
		if (!action.equalsIgnoreCase('GetOrderSummary')) {
			return lstCTNstoUpdate;
		}
		if (status.equalsIgnoreCase('200')) {
			String trimVettingStatus = orderSummaryData?.get(0)?.orderSummary?.get(0)?.orderHeader?.creditVettingStatus;
			if (trimVettingStatus.containsIgnoreCase('Status:')) {
				//check possible wrong format
				trimVettingStatus = String.isNotBlank(orderSummaryData[0].orderSummary[0].orderHeader.creditVettingStatus)
					? orderSummaryData
							?.get(0)
							?.orderSummary
							?.get(0)
							?.orderHeader
							?.creditVettingStatus
							?.split('Status: ', 2)
							?.get(1)
							?.split('\\.', 2)
							?.get(0)
					: '';
			}
			for (NetProfit_CTN__c tempCTN : lstCTNstoUpdate) {
				tempCTN.Order_Summary_Error__c = '';
				tempCTN.Order_Summary_Timestamp__c = System.now();
				tempCTN.Order_Summary_BSL_Status__c = orderSummaryData?.get(0)?.orderSummary.get(0)?.orderHeader?.orderStatus;
				// For creditvetting
				tempCTN.UnifyCreditVettingStatus__c = trimVettingStatus;
				tempCTN.UnifyCreditVettingStatusReason__c = orderSummaryData.get(0)?.orderSummary.get(0)?.orderHeader?.creditVettingReason;
				// For porting
				tempCTN.CTNPortingStatus__c = orderSummaryData
						?.get(0)
						?.orderSummary
						?.get(0)
						?.provideOrderActionsSummary
						?.get(0)
						?.orderActionSummary
						?.get(0)
						?.rejectionStatus == null
					? orderSummaryData?.get(0)?.orderSummary?.get(0)?.provideOrderActionsSummary?.get(0)?.orderActionSummary?.get(0)?.portingStatus
					: 'Rejected';
				tempCTN.CTNPortingStatusReason__c = orderSummaryData
					?.get(0)
					?.orderSummary
					?.get(0)
					?.provideOrderActionsSummary
					?.get(0)
					?.orderActionSummary
					?.get(0)
					?.rejectionStatus;
				Long longtime = orderSummaryData
					?.get(0)
					?.orderSummary
					?.get(0)
					?.provideOrderActionsSummary
					?.get(0)
					?.orderActionSummary
					?.get(0)
					?.wishDate;
				tempCTN.Contract_Enddate__c = longtime == null ? null : DateTime.newInstance(longtime).date();
			}
		}
		if (!errors.isEmpty()) {
			for (NetProfit_CTN__c tempCTN : lstCTNstoUpdate) {
				tempCTN.CTN_Status__c = errors[0].message.equalsIgnoreCase('Activity timed out') ? '' : 'Order Summary failed';
				tempCTN.Order_Summary_Error__c = JSON.serialize(errors).abbreviate(255);
			}
		}
		return lstCTNstoUpdate;
	}

	private static List<NetProfit_CTN__c> processValidateOrderFE(
		List<NetProfit_CTN__c> lstCTNstoUpdate,
		String action,
		String status,
		List<EMP_ValidateOrderFE.Data> validateOrderData,
		List<ErrorData> errors
	) {
		if (!action.equalsIgnoreCase('ValidateOrderFE')) {
			return lstCTNstoUpdate;
		}
		if (status.equalsIgnoreCase('200')) {
			for (NetProfit_CTN__c tempCTN : lstCTNstoUpdate) {
				tempCTN.CTN_Status__c = 'Retention Order ID Received';
				tempCTN.Unify_Order_Id_c__c = validateOrderData?.get(0)?.orderOutputData?.createdOrderData?.orderHeader?.orderId;
				tempCTN.Retention_Error__c = '';
				tempCTN.Retention_Confirmed_Timestamp__c = System.now();
			}
		}
		if (!errors.isEmpty()) {
			for (NetProfit_CTN__c tempCTN : lstCTNstoUpdate) {
				tempCTN.CTN_Status__c = 'Retention Failed';
				tempCTN.Retention_Error__c = JSON.serialize(errors).abbreviate(255);
				tempCTN.Error_on_Connect_GLP__c = errors?.get(0).message?.contains(Label.EMP_GLPNotConnected);
			}
		}
		return lstCTNstoUpdate;
	}

	private static void createStatusOrchProcess(List<NetProfit_CTN__c> lstCTNstoCreateOrchProcess, String action) {
		if (!action.equalsIgnoreCase('GetOrderSummary')) {
			return;
		}
		List<CSPOFA__Orchestration_Process__c> lstOrchProcess = new List<CSPOFA__Orchestration_Process__c>();
		String empOrderProcessTemplateId = [SELECT Id FROM CSPOFA__Orchestration_Process_Template__c WHERE Name = 'EMP Status Update' LIMIT 1]
			?.get(0)
			?.Id;
		for (NetProfit_CTN__c objCTN : lstCTNstoCreateOrchProcess) {
			lstOrchProcess.add(
				new CSPOFA__Orchestration_Process__c(
					CSPOFA__Orchestration_Process_Template__c = empOrderProcessTemplateId,
					Name = 'EMP Status Check for CTN ' + objCTN.Name,
					CTN__c = objCTN.Id
				)
			);
		}
		insert lstOrchProcess;
	}

	global class ErrorData {
		global String code { get; set; }
		global String message { get; set; }
	}
}
