@isTest
private with sharing class TestCS_ProductConfigurationLookup {
	@isTest
	private static void testRequiredAttributes() {
		CS_ProductConfigurationLookup caLookup = new CS_ProductConfigurationLookup();
		String result = caLookup.getRequiredAttributes();
		System.assertNotEquals(null, result, 'Return string must not be null');
	}

	@isTest
	private static void testLookup() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		Opportunity testOpp = new Opportunity(
			Name = 'New Opportunity',
			OwnerId = UserInfo.getUserId(),
			StageName = 'Qualification',
			Probability = 0,
			CloseDate = system.today(),
			AccountId = testAccount.id
		);
		insert testOpp;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c accessProductDef = CS_DataTest.createProductDefinition('Access Infrastructure');
		accessProductDef.Product_Type__c = 'Fixed';
		accessProductDef.RecordTypeId = productDefinitionRecordType;
		insert accessProductDef;

		Site__c site = CS_DataTest.createSite('Site 1', testAccount, '1111AA', 'Main street', 'Admsterdam', 1.0);
		insert site;

		cscfga__Product_Configuration__c accessProductConf = CS_DataTest.createProductConfiguration(
			accessProductDef.Id,
			'Access Infrastructure',
			basket.Id
		);
		accessProductConf.cscfga__Root_Configuration__c = null;
		accessProductConf.cscfga__Parent_Configuration__c = null;
		accessProductConf.Site__c = site.Id;
		insert accessProductConf;

		csbb__Product_Configuration_Request__c pcr = CS_DataTest.createPCR(accessProductConf);
		pcr.csbb__Product_Configuration__c = accessProductConf.Id;
		insert pcr;

		Map<String, String> searchFields = new Map<String, String>();
		searchFields.put('AccountID', String.valueOf(testAccount.Id));
		searchFields.put('BasketId', String.valueOf(basket.Id));
		searchFields.put('SiteId', String.valueOf(site.Id));

		CS_ProductConfigurationLookup caLookup = new CS_ProductConfigurationLookup();
		List<Object> result = caLookup.doLookupSearch(searchFields, String.valueOf(accessProductDef.Id), null, 0, 0);
		System.assertEquals(0, result.size());
	}
}
