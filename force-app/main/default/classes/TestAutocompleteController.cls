@SuppressWarnings('PMD.ApexAssertionsShouldIncludeMessage')
@isTest
private class TestAutocompleteController {
    @isTest
    static void findSObjectsPossitive() {
        List<Account> acctList = createAccountList();
        List<SObject> resultLst = AutocompleteController.findSObjects('Account', 'The%', '', '', '', false);
        System.assertEquals((acctList.size() - 1), resultLst.size());
    }

    @isTest
    static void findSObjectsLwcAssociatedPossitive() {
        List<Account> acctList = createAccountList();
        List<Opportunity> oppList = createOpportunityList(acctList[0].Id);
        List<SObject> resultLst = AutocompleteController.findSObjectsLwc('Opportunity', 'Holly%', '', 'AccountId', acctList[0].Id, false);
        System.assertEquals(oppList.size(), resultLst.size());
    }

    @isTest
    static void fetchDefaultRecordPossitive() {
        List<Account> acctList = createAccountList();
        SObject resultObj = AutocompleteController.fetchDefaultRecord(acctList[0].Id);
        System.assertEquals(acctList[0].Name, String.valueOf(resultObj.get('Name')));
    }


    private static List<Account> createAccountList() {
        List<Account> acctList = new List<Account>();
        acctList.add(new Account(Name = 'The Hollywood Company'));
        acctList.add(new Account(Name = 'The Schoonmaakt bedrijf'));
        acctList.add(new Account(Name = 'Miscellaneous bedrijft'));
        insert acctList;
        return acctList;
    }

    private static List<Opportunity> createOpportunityList(Id accountId) {
        List<Opportunity> oppList = new List<Opportunity>();
        oppList.add(new Opportunity(AccountId = accountId, Name = 'Hollywood Films', StageName = 'Closing', CloseDate = Date.today()));
        oppList.add(new Opportunity(AccountId = accountId, Name = 'Hollywood Tomatoes', StageName = 'Closing', CloseDate = Date.today()));
        oppList.add(new Opportunity(AccountId = accountId, Name = 'Hollywood Orange juice', StageName = 'Closing', CloseDate = Date.today()));
        insert oppList;
        return oppList;
    }
}