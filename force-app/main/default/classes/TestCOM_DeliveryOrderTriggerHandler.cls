@IsTest
public with sharing class TestCOM_DeliveryOrderTriggerHandler {
	@IsTest
	static void testBehavior() {
		List<COM_Delivery_Order__c> deliveryOrdersToUpdate = new List<COM_Delivery_Order__c>();
		List<COM_Delivery_Order__c> newDeliveryOrders = new List<COM_Delivery_Order__c>();
		csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
		insert ord;

		COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', ord.Id, false);
		deliveryOrder1.Confirmed_Installation__c = false;
		deliveryOrder1.Status__c = 'Created';
		deliveryOrder1.Installation_Required__c = true;
		deliveryOrdersToUpdate.add(deliveryOrder1);
		newDeliveryOrders.add(deliveryOrder1);

		COM_Delivery_Order__c deliveryOrder2 = CS_DataTest.createDeliveryOrder('DeliveryOrder 2', ord.Id, false);
		deliveryOrder2.Confirmed_Installation__c = false;
		deliveryOrder2.Status__c = 'Created';
		deliveryOrder2.Installation_Required__c = true;
		deliveryOrder2.Inflight_Change_Applied__c = false;
		deliveryOrdersToUpdate.add(deliveryOrder2);
		newDeliveryOrders.add(deliveryOrder2);

		Boolean stepPassed;

		try {
			insert newDeliveryOrders;

			deliveryOrder1.Status__c = 'Cancellation Requested';
			deliveryOrder2.Inflight_Change_Applied__c = true;
			update deliveryOrdersToUpdate;
			stepPassed = true;
		} catch (Exception e) {
			stepPassed = false;
		}

		System.assert(true, stepPassed);
	}
}
