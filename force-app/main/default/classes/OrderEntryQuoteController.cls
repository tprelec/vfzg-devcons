public with sharing class OrderEntryQuoteController {
	/**
	 * @description Generate order confirmation document
	 * @param  opportunityId Id
	 * @param  journeyType String
	 * @return           mmdoc__Document_Request__c
	 */
	@AuraEnabled
	public static mmdoc__Document_Request__c generateConfirmation(Id opportunityId, String journeyType) {
		if (journeyType.toLowerCase() == 'fixed') {
			mmdoc__Document_Template__c docTemplate = MavenDocumentsService.getDocumentTemplate('Ziggo Order Confirmation');
			return createDocumentRequest(opportunityId, docTemplate);
		} else if (journeyType.toLowerCase() == 'mobile') {
			mmdoc__Document_Template__c docTemplate = MavenDocumentsService.getDocumentTemplate('Vodafone Order Confirmation');
			return createDocumentRequest(opportunityId, docTemplate);
		}
		return null;
	}

	/**
	 * @description Generate contract summary document
	 * @param  opportunityId Id
	 * @param  journeyType String
	 * @return           mmdoc__Document_Request__c
	 */
	@AuraEnabled
	public static mmdoc__Document_Request__c generateContractSummary(Id opportunityId, String journeyType) {
		if (journeyType.toLowerCase() == 'fixed') {
			mmdoc__Document_Template__c docTemplate = MavenDocumentsService.getDocumentTemplate('Ziggo Contract Summary');
			return createDocumentRequest(opportunityId, docTemplate);
		} else if (journeyType.toLowerCase() == 'mobile') {
			mmdoc__Document_Template__c docTemplate = MavenDocumentsService.getDocumentTemplate('Vodafone Contract Summary (OET)');
			return createDocumentRequest(opportunityId, docTemplate);
		}
		return null;
	}

	/**
	 * @description Get Document request
	 * @param  docRequestId Id
	 * @return           mmdoc__Document_Request__c
	 */
	@AuraEnabled
	public static mmdoc__Document_Request__c getDocumentRequest(Id docRequestId) {
		return MavenDocumentsService.getDocumentRequest(docRequestId);
	}

	/**
	 * @description Update opportunity, set  LG_SignedQuoteAttachmentId__c and LG_SignedQuoteAvailable__c
	 * @param  oppId Id
	 * @param  attachmentId Id
	 * @param  singed Boolean
	 */
	@AuraEnabled
	public static void updateOpportunitySingedQuote(Id oppId, Id attachmentId, Boolean singed) {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		Opportunity opp = [SELECT Id, LG_SignedQuoteAvailable__c, LG_SignedQuoteAttachmentId__c FROM Opportunity WHERE Id = :oppId];

		if (opp != null) {
			opp.LG_SignedQuoteAttachmentId__c = (attachmentId != null) ? attachmentId : opp.LG_SignedQuoteAttachmentId__c;
			opp.LG_SignedQuoteAvailable__c = String.valueOf(singed);

			update opp;
		}
	}

	/**
	 * @description Update attachment
	 * @param  fileId Id
	 * @param  attachmentBodyBlob String
	 * @return  Boolean
	 */
	@AuraEnabled
	public static Boolean updateQuoteAttachment(Id fileId, String attachmentBodyBlob) {
		if (String.valueOf(fileId.getSobjectType()) == 'Attachment') {
			Attachment attachment = [SELECT Id, ParentId, Body FROM Attachment WHERE Id = :fileId];
			attachment.Body = EncodingUtil.base64Decode(attachmentBodyBlob);
			update attachment;
		} else {
			ContentVersion contentVersion = [SELECT Id, VersionData, ContentDocumentId, Title, PathOnClient FROM ContentVersion WHERE Id = :fileId];
			insert new ContentVersion(
				ContentDocumentId = contentVersion.ContentDocumentId,
				Title = contentVersion.Title,
				VersionData = EncodingUtil.Base64Decode(attachmentBodyBlob),
				PathOnClient = contentVersion.PathOnClient,
				ContentLocation = 'S'
			);
		}

		return true;
	}

	/**
	 * @description Update LG_AutomatedQuoteDelivery__c on Opportunity, this field is used to send email
	 * @param  oppId Id
	 * @param  value String
	 */
	@AuraEnabled
	public static void updateOpportunityAutomatedQuoteDelivery(Id oppId, String value) {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		try {
			Opportunity opp = [SELECT Id, LG_AutomatedQuoteDelivery__c FROM Opportunity WHERE Id = :oppId];
			opp.LG_AutomatedQuoteDelivery__c = value;
			update opp;
		} catch (Exception ex) {
			throw new AuraException(ex.getMessage());
		}
	}

	/**
	 * @description Creae Document request
	 * @param  recordId Id
	 * @param  docTemplate mmdoc__Document_Template__c
	 * @return  mmdoc__Document_Request__c
	 */
	private static mmdoc__Document_Request__c createDocumentRequest(Id recordId, mmdoc__Document_Template__c docTemplate) {
		return MavenDocumentsService.createDocRequest(recordId, docTemplate, 'Attachment', 'PDF', 'Queue', true);
	}
}
