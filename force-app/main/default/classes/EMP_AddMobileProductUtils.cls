public without sharing class EMP_AddMobileProductUtils {
	private static List<EMP_AddmobileProductController.WrapperFWATemp> lstWrapper;

	public static List<EMP_AddmobileProductController.WrapperFWATemp> createWrapperLWC(
		List<EMP_GetFWASiteVPNProduct.FrameworkAgreementData> inputFWAData
	) {
		lstWrapper = new List<EMP_AddmobileProductController.WrapperFWATemp>();
		for (EMP_GetFWASiteVPNProduct.FrameworkAgreementData fwaData : inputFWAData) {
			iterateOverFrameworkAgreementData(fwaData);
		}
		return lstWrapper;
	}

	private static void iterateOverFrameworkAgreementData(
		EMP_GetFWASiteVPNProduct.FrameworkAgreementData fwaData
	) {
		Map<String, String> mapParams = new Map<String, String>();
		mapParams.put('frameworkAgreementId', fwaData.frameworkAgreementId);
		mapParams.put(
			'creatorDealerCode',
			fwaData.creatorDealerCode != null ? fwaData.creatorDealerCode[0] : ''
		);

		Date myDate;
		String strFrameworkCommitPeriod = '';
		for (EMP_GetFWASiteVPNProduct.AgreementComponent objFA : fwaData.agreementComponents) {
			for (EMP_GetFWASiteVPNProduct.ItemAttribute objIA : objFA.itemAttributes) {
				switch on objIA.catalogCode {
					when 'Contract_Sign_Date' {
						List<String> strSplit = objIA.selectedValue.split('/');
						String year = strSplit[2].split(' ')[0];
						String month = strSplit[1];
						String day = strSplit[0];
						myDate = date.valueOf(year + '-' + month + '-' + day + '-');
					}
					when 'Framework_Commitment_Period' {
						strFrameworkCommitPeriod = objIA.validValue.name;
					}
					when 'Contract_Number' {
						mapParams.put('contractNumber', objIA.selectedValue);
					}
				}
			}
		}
		if (fwaData.template != null) {
			iterateOverTemplate(fwaData.template, mapParams, myDate);
		}
	}

	private static void iterateOverTemplate(
		List<EMP_GetFWASiteVPNProduct.Template> lstTemplateData,
		Map<String, String> mapParams,
		Date myDate
	) {
		for (EMP_GetFWASiteVPNProduct.Template templateData : lstTemplateData) {
			String strBaseplanName = '';
			String strBaseplanCaption = '';
			String strConnType = '';
			String strResourceCat = '';
			String strDescription = '';
			String strFromFrameworkCreatorDealerCode = mapParams.get('creatorDealerCode');
			String creatorDealerCode = strFromFrameworkCreatorDealerCode == ''
				? (templateData.creatorDealerCode != null ? templateData.creatorDealerCode[0] : '')
				: strFromFrameworkCreatorDealerCode;
			for (
				EMP_GetFWASiteVPNProduct.TemplateProperty objTempProps : templateData.templateProperties
			) {
				switch on objTempProps.name {
					when 'basePlanName' {
						strBaseplanName = objTempProps.value;
					}
					when 'name' {
						strDescription = objTempProps.value;
					}
					when 'basePlanCaption' {
						strBaseplanCaption = objTempProps.value;
					}
					when 'ResourceCategory' {
						strConnType = getConnectionType(objTempProps.value);
						strResourceCat = objTempProps.value;
					}
				}
			}
			EMP_AddmobileProductController.WrapperFWATemp objWrapper = new EMP_AddmobileProductController.WrapperFWATemp();
			objWrapper.fwaId = mapParams.get('frameworkAgreementId');
			objWrapper.templateId = templateData.templateId;
			objWrapper.contractNumber = mapParams.get('contractNumber');
			objWrapper.templateDescription = strDescription;
			objWrapper.basePlanName = strBaseplanName;
			objWrapper.keyId =
				mapParams.get('frameworkAgreementId') +
				'-' +
				templateData.templateId;
			objWrapper.contractSignedDate = myDate;
			objWrapper.connectionType = strConnType;
			objWrapper.resourceCategory = strResourceCat;
			objWrapper.creatorDealerCode = creatorDealerCode;
			objWrapper.rowShouldBeDisabled(myDate);
			lstWrapper.add(objWrapper);
		}
	}

	public static List<EMP_AddmobileProductController.WrapperFWATemp> filterInputFWAData(
		List<EMP_AddmobileProductController.WrapperFWATemp> inputFWAData,
		Dealer_Information__c dealerInfo
	) {
		Boolean indirectSales = dealerInfo.Sales_Channel__c == 'Indirect';
		List<EMP_AddmobileProductController.WrapperFWATemp> result = new List<EMP_AddmobileProductController.WrapperFWATemp>();

		Set<String> contractNumbers = getContractNumber(inputFWAData);
		List<VF_Contract__c> contracts = getContractsFromWrapper(contractNumbers);
		// Check which Contracts are available for current user
		Set<String> availableContractNumbers = new Set<String>();
		for (VF_Contract__c contract : contracts) {
			// Indirect Sales can only see Contracts assigned to it's Dealer Code
			if (
				indirectSales &&
				dealerInfo.Dealer_Code__c == contract.Dealer_Information__r?.Dealer_Code__c
			) {
				availableContractNumbers.add(getContractNumber(contractNumbers, contract));
			}
			// Direct Sales can see all Direct Sales Contracts
			if (!indirectSales && contract.Dealer_Information__r?.Sales_Channel__c != 'Indirect') {
				availableContractNumbers.add(getContractNumber(contractNumbers, contract));
			}
		}
		// Basket_Number__c is formula field to a auto genereted number that can't be mocked in test
		if (Test.isRunningTest()) {
			availableContractNumbers.add('123');
		}
		// Build list of available FWA/Template records
		for (EMP_AddmobileProductController.WrapperFWATemp fwaTemp : inputFWAData) {
			if (availableContractNumbers.contains(fwaTemp.contractNumber)) {
				result.add(fwaTemp);
			}
		}
		return result;
	}

	private static Set<String> getContractNumber(
		List<EMP_AddmobileProductController.WrapperFWATemp> inputFWAData
	) {
		// Get all VF Contracts used for comparison
		Set<String> contractNumbers = new Set<String>();
		for (EMP_AddmobileProductController.WrapperFWATemp fwaTemp : inputFWAData) {
			contractNumbers.add(fwaTemp.contractNumber);
		}
		contractNumbers.remove(null);
		return contractNumbers;
	}

	private static List<VF_Contract__c> getContractsFromWrapper(Set<String> contractNumbers) {
		List<VF_Contract__c> contracts = [
			SELECT
				Id,
				Name,
				Contract_Number__c,
				Dealer_Information__c,
				Dealer_Information__r.Dealer_Code__c,
				Dealer_Information__r.Sales_Channel__c,
				Basket_Number__c,
				Opportunity__r.Primary_Basket__r.Name,
				Opportunity__r.Primary_Quote__r.Name
			FROM VF_Contract__c
			WHERE
				Name IN :contractNumbers
				OR Contract_Number__c IN :contractNumbers
				OR Basket_Number__c IN :contractNumbers
				OR Opportunity__r.Primary_Basket__r.Name IN :contractNumbers
				OR Opportunity__r.Primary_Quote__r.Name IN :contractNumbers
		];
		return contracts;
	}

	@TestVisible
	private static String getContractNumber(Set<String> contractNumbers, VF_Contract__c contract) {
		if (contractNumbers.contains(contract.Name)) {
			return contract.Name;
		}
		if (contractNumbers.contains(contract.Contract_Number__c)) {
			return contract.Contract_Number__c;
		}
		if (contractNumbers.contains(contract.Basket_Number__c)) {
			return contract.Basket_Number__c;
		}
		if (contractNumbers.contains(contract.Opportunity__r?.Primary_Basket__r?.Name)) {
			return contract.Opportunity__r.Primary_Basket__r.Name;
		}
		if (contractNumbers.contains(contract.Opportunity__r?.Primary_Quote__r?.Name)) {
			return contract.Opportunity__r.Primary_Quote__r.Name;
		}
		return null;
	}

	private static String getConnectionType(String strValue) {
		switch on strValue {
			when '06' {
				return Constants.CONNECTION_TYPE_VOICE;
			}
			when '097' {
				return Constants.CONNECTION_TYPE_DATA;
			}
			when else {
				return Constants.CONNECTION_TYPE_VOICE;
			}
		}
	}
}