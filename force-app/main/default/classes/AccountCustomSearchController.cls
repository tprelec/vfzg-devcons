/**
 * @description         This is the controller for the AccountCustomSearch page
 * @author				Marcel Vreuls
 * @History				Okt 2017: adjustments for W-000269..
 */

public without sharing class AccountCustomSearchController {

	public AccountCustomSearchController(){
		inprogress = true;
		Map<String,String> param = ApexPages.currentPage().getParameters();
		if(param.containsKey('bannr')) searchBanNumber = param.get('bannr');
		if(param.containsKey('kvknr')) searchKvkNumber = param.get('kvknr');
		if(param.containsKey('name')) searchName = param.get('name');
		if(param.containsKey('zip')) searchZipcode = param.get('zip');
		if(param.containsKey('hnr')) searchHousenumber = param.get('hnr');
		if(param.containsKey('descr')) description = param.get('descr');
		if(param.containsKey('leadId')) leadId = param.get('leadId');

	} 

	public Boolean showResults {get;set;}
	public Boolean banMatch {get;set;}
	public Static Boolean inprogress {get;set;}
	
	public String searchName {get;set;} // prio 4, 4 points
	public String searchZipcode {get;set;} // prio 3, 4 points for both billing and visiting postalcode
	public String searchHousenumber {get;set;} // prio 5 (only combined with zipcode), 2 points
	public String searchKvkNumber {get;set;} // prio 2, 8 points
	public String searchBanNumber {get;set;} // prio 1, 10 points
	
	public String description {get;set;} // description can be added as parameter to add to the Opportunity

	public String leadId {get;set;}
	
	public class SearchResult implements Comparable {

		AccountCustomSearchController acsc = new AccountCustomSearchController();

		public SearchResult(AccountCustomSearchController acsc){
			this.acsc = acsc;
		}

		public Account acct {get;set;}
		public Integer score {get;set;}
		public BanManagerData bm {get;set;}		
		
		// Implement the compareTo() method to enable sorting
		public Integer compareTo(Object compareTo) {
			SearchResult compareToSR = (SearchResult)compareTo;
			if (score == compareToSR.score) return 0;
			if (score > compareToSR.score) return -1;
			return 1;        
		}	

		public PageReference createOpportunity(){
			Id leadId = (Id) acsc.leadId;
			if(leadId != null){
				Database.LeadConvert lc = new Database.LeadConvert();
				lc.setLeadId(leadId);
				lc.setAccountId(acct.Id);
				LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
				lc.setConvertedStatus(convertStatus.MasterLabel);
				Database.LeadConvertResult lcr = Database.convertLead(lc);
				return New PageReference('/'+ lcr.opportunityId );
			} else {
				if(GeneralUtils.currentUserIsPartnerUser()){
					BanManagerData.createAccountSharing(acct.Id,UserInfo.getUserId());
				}
				
				// then create a pagereference for the opportunity
				inprogress = true;
				String refString = '/apex/RedirectWithVariables';

				// add object reference
				refString += '?object=Opportunity';
				refString += '&cancelURL=/apex/AccountCustomSearch&retURL=/001';
				refString += '&nooverride=1';
				// add accountId (via ugly hack in the redirectwithvariables code)
				refString += '&accid='+acct.Id;				        
				system.debug(refString);
				return new PageReference(refString);
			}						
		}	    	
	}
	
	public List<SearchResult> searchResults {
		get{
			if(searchResults == null){
				searchResults = new List<SearchResult>();
			} else {
				searchResults.sort();
				List<SearchResult> filteredResult = new List<SearchResult>();
				Integer c = 0;
				system.debug(searchResults);
				for(SearchResult rs : searchResults){
					if(rs.score > 0 && c < 10){
						filteredResult.add(rs);
						c++;
					}
				}
				searchResults = filteredResult;
				system.debug(searchResults);
			}
			return searchResults;	
		}	
		set;
	}
	
	public void doSearch(){
		Boolean errorFound = false;
		banMatch = false;

		// clean the search parameters
		String cSearchName = String.escapeSingleQuotes(searchName);
		String cSearchZipcode = String.escapeSingleQuotes(searchZipcode);
		String cSearchHousenumber = String.escapeSingleQuotes(searchHousenumber);
		String cSearchKvkNumber = String.escapeSingleQuotes(searchKvkNumber);
		String cSearchBanNumber = String.escapeSingleQuotes(searchBanNumber);
		
		// some checks
		if(cSearchName == null || cSearchName.length() < 2){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_customer_name_required));
			errorFound = true;
		}
		// verify that at least 2 fields are filled (name is already required, so just check the others)
		if(cSearchZipcode == '' && cSearchBanNumber == '' && cSearchKvkNumber == ''){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_at_least_2_search_fields_filled));
			errorFound = true;
		}
		if(cSearchZipcode != null && cSearchZipcode != ''){
			if( cSearchZipcode.replaceAll(' ','').length() != 4 && cSearchZipcode.replaceAll(' ','').length() != 6){
				Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_Zipcode_should_be_correct));
				errorFound = true;
			}			
		}				
		if((cSearchZipcode == null || cSearchZipcode == '') && cSearchHousenumber != null && cSearchHousenumber != ''){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_housenumber_only_with_zipcode));
			errorFound = true;			
		}
		if(cSearchBanNumber != null && cSearchBanNumber != '' && cSearchBanNumber.length() != 9){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_Ban_Number_9_chars));
			errorFound = true;			
		}		
		if(cSearchBanNumber != null && cSearchBanNumber != '' && !cSearchBanNumber.isNumeric()){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_Ban_Number_numeric));
			errorFound = true;			
		}				
		if(cSearchKvkNumber != null && cSearchKvkNumber != '' && cSearchKvkNumber.length() > 8){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_KvK_8_chars));
			errorFound = true;			
		}
		if(cSearchKvkNumber != null && cSearchKvkNumber != '' && !cSearchKvkNumber.isNumeric()){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_KvK_numeric));
			errorFound = true;			
		}		
		if(errorFound){
			return;
		}
		 
		
		searchResults.clear();
		
		Boolean exactMatch = false;
		List<Account> queryResult = new List<Account>();
		String queryStart = 'Select Id, Name, BillingPostalCode,Visiting_Postal_Code__c,Visiting_Housenumber1__c,Visiting_Housenumber_suffix__c,KVK_number__c from Account Where RecordTypeId = \''+GeneralUtils.recordTypeMap.get('Account').get('VF_Account')+'\' AND '; 
		

		// first search only on BAN Number
		if(cSearchBanNumber != null && cSearchBanNumber != ''){
			Set<Id> acctIds = new Set<Id>();
			for(Ban__c b : [Select Id, Name, BAN_Number__c,Account__c  From Ban__c Where BAN_Number__c = :cSearchBanNumber]){
				acctIds.add(b.Account__c);
			}

			String queryString = queryStart;
			queryString += 'Id in :acctIds ';
			queryResult = Database.query(queryString);
			if(queryResult.size() > 0){
				exactMatch = true;
				banMatch = true;
			}
		}

		// second search only on KvK Number
		if(!exactMatch){
			if(cSearchKvkNumber != null && cSearchKvkNumber != ''){
				String queryString = queryStart;
				queryString += 'KvK_Number__c = \''+cSearchKvkNumber+'\' ';
				queryResult = Database.query(queryString);
				if(queryResult.size() > 0){
					exactMatch = true;
				} else {
					createAccount();
					exactMatch = true;
				}
			}						
		}
		
		queryStart = 'Select Id, Name, BillingPostalCode,Visiting_Postal_Code__c,Visiting_Housenumber1__c,Visiting_Housenumber_suffix__c,KVK_number__c ';
		queryStart += 'from Account Where RecordTypeId = \''+GeneralUtils.recordTypeMap.get('Account').get('VF_Account')+'\' AND '; 

		String searchString = cSearchName.replace(' ','%');

		// third search on the other (or partial) criteria
		Set<Id> acctIds = new Set<Id>();
		if(!exactMatch){
			String queryString = queryStart;
			queryString += 'Name like \'%'+searchString+'%\' AND ( CreatedDate != null ';
			if(cSearchBanNumber != null && cSearchBanNumber != ''){
				
				for(Ban__c b : [Select Id, Name, BAN_Number__c,Account__c From Ban__c Where BAN_Number__c = :cSearchBanNumber]){
					acctIds.add(b.Account__c);
				}				
				queryString += 'OR Id in :acctIds '; 
			}		
			if(cSearchKvkNumber != null && cSearchKvkNumber != ''){
				queryString += 'OR KvK_Number__c like \'%'+cSearchKvkNumber+'%\' ';
			}		
			if(cSearchZipcode != null && cSearchZipcode != ''){
				queryString += 'OR BillingPostalCode like \'%'+cSearchZipcode+'%\' ';
				queryString += 'OR Visiting_Postal_Code__c like \'%'+cSearchZipcode+'%\' ';
				if(cSearchHousenumber != null && cSearchHousenumber != '' && cSearchHousenumber.isNumeric()){
					queryString += 'OR Visiting_Housenumber1__c = '+cSearchHousenumber;
				}
			}
			queryString += ') ';
			
			system.debug(queryString);
			queryResult = Database.query(queryString);
		}		
		system.debug(queryResult);

		Set<Id> accountIdsRetrieved = new Set<Id>();

		for(Account a : queryResult){
			accountIdsRetrieved.add(a.Id);

			SearchResult sr = new SearchResult(this);
			sr.acct = a;
			sr.score = 0;
			// assign scores
			if(cSearchBanNumber != null && (acctIds.contains(a.Id) /*a.BAN_Number__c == cSearchBanNumber*/)) 
				sr.score += 10;
			
			if(cSearchKvkNumber != null && cSearchKvkNumber != '' && a.KVK_number__c != null && (a.KVK_number__c.contains(cSearchKvkNumber))) 
				sr.score += 8;
			
			//if(cSearchName != null && cSearchName != '' && (a.Name.toLowerCase().contains(cSearchName.toLowerCase()) || cSearchName.toLowerCase().contains(a.Name.toLowerCase()))) 
			// name always scores, as it is required and is checked in the query
				sr.score += 4;
			
			// billing addres can only be checked on postalcode, as housenumber is part of the street field..
			if(cSearchZipcode != null && cSearchZipcode != '' && a.BillingPostalCode != null){
				if (a.BillingPostalCode.replace(' ','').contains(cSearchZipcode.replace(' ',''))) 
					sr.score += 4;
			}
			if(cSearchZipcode != null && cSearchZipcode != '' && a.Visiting_Postal_Code__c != null){
				if (a.Visiting_Postal_Code__c.replace(' ','').contains(cSearchZipcode.replace(' ',''))){
					// increase the score and also enable housenumber scoring	
					sr.score += 4;
					if(cSearchHousenumber != null && cSearchHousenumber != '' && a.Visiting_Housenumber1__c != null){
						if (String.valueOf(a.Visiting_Housenumber1__c) == cSearchHousenumber) 
							sr.score += 2;
					}					 
				}
			}	

			searchResults.add(sr);
		}

		showResults = true;
	}
	

	
	public PageReference createAccount(){


		String cSearchKvkNumber = String.escapeSingleQuotes(searchKvkNumber);
		System.debug('--------'+cSearchKvkNumber + '/' + searchKvkNumber);

		List<Account> accountCheck = [select Id from Account where KvK_Number__c =: cSearchKvkNumber];

		if(!accountCheck.isEmpty()){
			Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.INFO,'You found a unique match on KVK number, so please create the opportunity with the button'));
			return null;
		} else {
			if(Pattern.matches('[0-9]{8}', cSearchKvkNumber)){
				
				OlbicoServiceJSON jService = new OlbicoServiceJSON();

				jService.setRequestRestJson(cSearchKvkNumber);
				jService.makeReqestRestJson(cSearchKvkNumber);

				if(jService.getAccId() != null)
				{
					doSearch();
					return null;

				} else {
					Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,jService.getResponse()));
					return null;
				}
			} else {
				Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_KvK_numeric));
				return null;
			}
		}
	}
	
	public PageReference cancel(){
		if(leadId != null){
			return new PageReference('/' + leadId);
		} else {
			return new PageReference('/001/o');
		}		
	}




}