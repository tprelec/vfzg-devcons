global class ITWBatch implements Database.Batchable<sObject> {	

	global Database.QueryLocator start(Database.BatchableContext BC) {
		return Database.getQueryLocator('SELECT Id, BAN_Number__c, ITW_Representative__c FROM ITW_Account_Info__c');
	}

	global void execute(Database.BatchableContext BC, List<ITW_Account_Info__c> scope) {

		Map<String, Ban__c> accMap = new Map<String, Ban__c>();
		Map<String, User> userMap = new Map<String, User>();
		List<Account> accountsToUpdate = new List<Account>();
		List<AccountTeamMember> atmToInsert = new List<AccountTeamMember>();
		List<ITW_Account_Info__c> itwToUpdate = new List<ITW_Account_Info__c>();
		List<ITW_Account_Info__c> itwToDelete = new List<ITW_Account_Info__c>();
		Map<Id, Set<Id>> shareMap = new Map<Id, Set<Id>>();
		List<AccountShare> sharesToUpdate = new List<AccountShare>();

		for(ITW_Account_Info__c itw : scope){
			accMap.put(itw.BAN_Number__c, null);
			userMap.put(itw.ITW_Representative__c, null);
		}

		for(BAN__c ban : [select Id, Name, Account__c, Account__r.ITW_Indicator__c from BAN__c where Name in : accMap.keySet()]){			
			accMap.put(ban.Name, ban);

			if(!ban.Account__r.ITW_Indicator__c){
				ban.Account__r.ITW_Indicator__c = true;
				accountsToUpdate.add(new Account(Id=ban.Account__c,ITW_Indicator__c = true));
			}
		}

		for(User u : [select Id, Name from User where Name in: userMap.keySet()]){
			userMap.put(u.Name, u);
		}

		for(ITW_Account_Info__c itw : scope){
			if(accMap.get(itw.BAN_Number__c) == null){
				itw.Error__c = 'There is no corresponding Account with this Ban Number';
				itwToUpdate.add(itw);
			} else if(userMap.get(itw.ITW_Representative__c) == null){
				itw.Error__c = 'There is no corresponding User with this Name';
				itwToUpdate.add(itw);
			} else {
				if (shareMap.get(accMap.get(itw.BAN_Number__c).Account__c) == null) {
					shareMap.put(accMap.get(itw.BAN_Number__c).Account__c, new Set<Id>());
				}
				shareMap.get(accMap.get(itw.BAN_Number__c).Account__c).add(userMap.get(itw.ITW_Representative__c).Id);
				AccountTeamMember atm = new AccountTeamMember(AccountId = accMap.get(itw.BAN_Number__c).Account__c,
															  TeamMemberRole = 'ITW Sales Rep',
															  UserId = userMap.get(itw.ITW_Representative__c).Id);
				atmToInsert.add(atm);
				itwToDelete.add(itw);
			}
		}

		update accountsToUpdate;
		insert atmToInsert;
		update itwToUpdate;
		delete itwToDelete;

		//Assign correct rights to users
		for(AccountShare acs : [select Id, AccountId, RowCause, UserOrGroupId, AccountAccessLevel, OpportunityAccessLevel, CaseAccessLevel
							   from AccountShare where AccountId in: shareMap.keySet() and RowCause = 'Team']){
			if(shareMap.get(acs.AccountId).contains(acs.UserOrGroupId)){
				acs.AccountAccessLevel = 'Edit';
				acs.OpportunityAccessLevel = 'Edit';
				acs.CaseAccessLevel = 'Read';
				sharesToUpdate.add(acs);
			}
		}
		update sharesToUpdate;
	}

	global void finish(Database.BatchableContext BC) {

	}
}