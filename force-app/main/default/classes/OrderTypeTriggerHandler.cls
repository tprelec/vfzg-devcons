/**
 * @description       : Apex Class Handler of the Order Type Trigger
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   20-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

public class OrderTypeTriggerHandler extends TriggerHandler {
	private List<OrderType__c> lstOrderType;

	private Map<Id, OrderType__c> mapOldOrderType;
	private Map<Id, OrderType__c> mapNewOrderType;

	/**
	 * @description Initialize Class Variables
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	private void init() {
		lstOrderType = (List<OrderType__c>) this.newList;
		mapOldOrderType = (Map<Id, OrderType__c>) this.oldMap;
		mapNewOrderType = (Map<Id, OrderType__c>) this.newMap;
	}

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public void setExternalIds() {
		Sequence objSequence = new Sequence('OrderType');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (OrderType__c objOrderType : lstOrderType) {
				objOrderType.ExternalID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
