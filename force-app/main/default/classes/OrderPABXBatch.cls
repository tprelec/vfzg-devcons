/**
 * @description         This batch class is responsible for calling OrderPABX interface 
 *                          which generates PABXId which is then later used by BOP for making changes to contracted products directly
 * @author              Rahul Sharma
 */
 
public class OrderPABXBatch implements Database.Batchable<SObject>, Database.AllowsCallouts {

    // properties
    public Set<Id> orderSet = new Set<Id>();

    // constructor
    public OrderPABXBatch(Set<Id> orderSet) {
        this.orderSet = orderSet;
    }

    // start method
    // only select applicable cp's. Do not create PBX in Unify for OneNet.
    public Database.QueryLocator start(Database.BatchableContext BC){ 
        return Database.getQueryLocator([
            SELECT Id FROM Contracted_Products__c WHERE 
                Order__c IN :this.orderSet AND
                Order__r.BOP_Order_Status__c = :Constants.ORDER_BOP_ORDER_STATUS_PROJECT_CREATED AND
                Order__r.O2C_Order__c = true AND
                PBX__c != null AND
                PBX__r.PABX_Id__c = null AND
                Product__r.Family_Tag__r.Name = :Constants.PRODUCT2_FAMILYTAG_PBX_Trunking AND
            	Proposition_Contains_OneNet__c = false AND
                Do_not_export__c = false
        ]);
    }

    // execute method
    public void execute(Database.BatchableContext BC, List<Contracted_Products__c> contractedProducts){
        for(Contracted_Products__c contractedProduct: contractedProducts) {
            requestPABXOrderCreate(contractedProduct);
        }
    }

    // makes callout to create order PABX service
    private static void requestPABXOrderCreate(Contracted_Products__c contractedProduct) {
        OrderPABXRest.createOrderPABX(contractedProduct.Id);
    }

    // finish method
    public void finish(Database.BatchableContext BC){ 
        // Error handling if required
    }

    // calls the execute method in context of future
    // this method is intended to be called in context of test method
    @future(callout=true)
    public static void executeCalloutAsFuture(Set<Id> orderSet) {

        OrderPABXBatch batchInstance = new OrderPABXBatch(orderSet);
        // Get an iterator
        Database.QueryLocatorIterator iterator =  batchInstance.start(null).iterator();
        
        List<Contracted_Products__c> contractedProducts = new List<Contracted_Products__c>();
        // Collect the first record
        if(iterator.hasNext()) {
            contractedProducts.add((Contracted_Products__c) iterator.next());
        }
        // call execute method to make callout and process the record
        if(!contractedProducts.isEmpty()) {
            batchInstance.execute(null, contractedProducts);
        }
    }

}