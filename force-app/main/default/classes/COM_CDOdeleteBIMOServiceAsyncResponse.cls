@RestResource(urlMapping='/testCDOdeleteBIMO/*')
global with sharing class COM_CDOdeleteBIMOServiceAsyncResponse {
	/*
	@HttpPost
	global static void updateDeletedServiceDeliveryOrder() {
		RestRequest req = RestContext.request;
		String reqBody = req.requestbody.tostring();
		reqBody = COM_CDOHelper.modifyRequestBody(reqBody);

		try {
			CDODeleteBIMOAsyncResponse response = (CDODeleteBIMOAsyncResponse) JSON.deserializeStrict(
				reqBody,
				CDODeleteBIMOAsyncResponse.class
			);

			COM_CDOHelper.performOrderUpdatesAfterSuccessfulResponse(
				null,
				response.event.serviceOrder,
				COM_CDOdeleteBIMOService.COM_CDO_INTEGRATION_COMPLETE_STATUS
			);
		} catch (Exception e) {
			try {
				CDODeleteBIMOAsyncFailedResponse response = (CDODeleteBIMOAsyncFailedResponse) JSON.deserializeStrict(
					reqBody,
					CDODeleteBIMOAsyncFailedResponse.class
				);

				String errorMessage = getErrorMessage(response.event.errorEvents);
				COM_CDOHelper.performOrderUpdatesAfterFailedResponse(
					null,
					response.event.serviceOrder,
					COM_CDOdeleteBIMOService.COM_CDO_INTEGRATION_FAILED_STATUS,
					errorMessage
				);
			} catch (Exception ex) {
				System.debug(LoggingLevel.Error, '***Failure');
				System.debug(LoggingLevel.Error, ex.getMessage());
			}
		}
	}

	private static String getErrorMessage(CDODeleteBIMOErrorEvents[] errorEvents) {
		String result = '';
		if (errorEvents != null && errorEvents.size() > 0) {
			for (CDODeleteBIMOErrorEvents ef : errorEvents) {
				result += ef.message;
			}
		}
		return result;
	}
	global class CDODeleteBIMOAsyncResponse {
		public String eventId; //deff54ee-ba37-4dba-b1c9-1fe1e554a4e1
		public String eventTime; //2022-02-08T11:30:15.97221Z
		public String eventType;
		public Event event;
	}

	global class Event {
		public COM_CDO_BIMOService serviceOrder;
	}

	global class CDODeleteBIMOAsyncFailedResponse {
		public String eventId; //9577add4-5200-47cb-b787-f6e617b396d9
		public String eventTime; //2022-02-08T10:45:15.97221Z
		public String eventType; //ServiceOrderStateChangeNotification
		public CDODeleteBIMOEventFailed event;
	}

	global class CDODeleteBIMOEventFailed {
		public COM_CDO_BIMOService serviceOrder;
		public CDODeleteBIMOErrorEvents[] errorEvents;
	}

	global class CDODeleteBIMOErrorEvents {
		public Integer code; //510105000
		public String reason; //EXTERNAL_SYSTEM_ERROR
		public String message; //A fulfillment activity failed with error-code '5000', error-message 'some error message'.
	}
	*/
}
