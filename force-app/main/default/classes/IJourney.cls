public interface IJourney {
	Boolean checkVisibility(String accountId);
	Map<String, Object> handleClick(String accountId);
}