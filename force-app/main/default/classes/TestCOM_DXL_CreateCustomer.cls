@isTest
private class TestCOM_DXL_CreateCustomer {
	@testSetup
	static void setup() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		TestUtils.createOrderType();

		Integer nrOfRows = 2;
		User owner = TestUtils.createAdministrator();

		Account acct = TestUtils.createAccount(owner);
		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test', Email = 'contact@test.com', Phone = '+31 111111789');
		insert c;

		TestUtils.autoCommit = false;
		List<Site__c> sites = new List<Site__c>();
		sites.add(TestUtils.createSite(acct));
		sites.add(TestUtils.createSite(acct));
		insert sites;

		Opportunity testOpportunity = CS_DataTest.createOpportunity(acct, 'Test Opportunity', owner.Id);
		testOpportunity.Main_Contact_Person__c = c.Id;
		insert testOpportunity;

		csord__Order__c ord = CS_DataTest.createOrder();
		ord.csord__Status2__c = 'Created';
		ord.csordtelcoa__Opportunity__c = testOpportunity.Id;
		insert ord;

		List<Product2> products = new List<Product2>();
		for (Integer i = 0; i < nrOfRows; i++) {
			products.add(TestUtils.createProduct());
		}
		insert products;

		List<PricebookEntry> pbEntries = new List<PricebookEntry>();
		for (Integer i = 0; i < nrOfRows; i++) {
			pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i)));
		}
		insert pbEntries;

		List<Opportunitylineitem> olis = new List<Opportunitylineitem>();

		for (Integer i = 0; i < nrOfRows; i++) {
			OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(testOpportunity, pbEntries.get(0));
			oppLineItem.Location__c = sites[i].Id;
			olis.add(oppLineItem);
		}

		insert olis;

		External_account__c externalaccountcObj = new External_account__c(
			Account__c = acct.Id,
			External_Account_Id__c = 'test value',
			External_Source__c = 'Unify'
		);
		insert externalaccountcObj;

		External_contact__c externalcontactcObj = new External_contact__c(
			Contact__c = c.Id,
			External_Contact_Id__c = 'test value',
			External_Source__c = 'Unify',
			ExternalId__c = 'test value'
		);
		insert externalcontactcObj;

		External_site__c externalsitecObj = new External_site__c(
			External_Site_Id__c = 'UNIFY123654789',
			External_Source__c = 'Unify',
			Site__c = sites[0].Id
		);
		insert externalsitecObj;
	}

	@isTest
	static void testCallDxlCreateCustomer() {
		List<csord__Order__c> ordList = [SELECT Id, Name, csord__Status2__c, csordtelcoa__Opportunity__c FROM csord__Order__c];
		List<Id> orderIds = new List<Id>();
		orderIds.add(ordList[0].Id);

		Test.startTest();
		COM_MockWebService.setTestMockResponse(500, '500', '{"statusCode": 500,"statusMessage": "Default Error"}');
		COM_DXL_CreateCustomer.callDxlCreateCustomer(orderIds);
		Test.stopTest();

		List<COM_DXL_Integration_Log__c> transactionLogs = [
			SELECT Id
			FROM COM_DXL_Integration_Log__c
			WHERE Opportunity__c = :ordList[0].csordtelcoa__Opportunity__c
		];
		System.assertEquals(false, transactionLogs.isEmpty(), 'Expected: false Actual: ' + String.valueOf(transactionLogs.isEmpty()));
	}
}
