@isTest
public with sharing class TestCOM_CreateBSSOrder {
    @testSetup 
    static void setup() {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;

        Account testAccount = CS_DataTest.createAccount('Test Account');
        testAccount.LG_ExternalID__c  = '1234456';
        insert testAccount;

        csconta__Billing_Account__c billingAcc = new csconta__Billing_Account__c();
        billingAcc.csconta__Account__c = testAccount.Id;
        billingAcc.LG_PaymentType__c = 'Bank Transfer';
        billingAcc.LG_HouseNumber__c = '12';
        billingAcc.LG_BillingAccountName__c = 'Test Billing Account';
        //billingAcc.Mandate_Reference__c = mandate.Id;
        billingAcc.LG_MandateStartDate__c = System.today();
        insert billingAcc;

        Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',UserInfo.getUserId());
        testOpp.csordtelcoa__Change_Type__c = 'Change';
        testOpp.Name = 'testOpp';
        testOpp.StageName = 'Ready for Order';
        testOpp.CloseDate = System.today();
        testOpp.AccountId = testAccount.Id;
        insert testOpp;

        cscfga__Product_Basket__c testBasket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        testBasket.Name = 'testBasket';
        testBasket.cscfga__Opportunity__c = testOpp.Id;
        testBasket.LG_Selected_Billing_Account__c = billingAcc.id;
        insert testBasket;

        Contact testContact = CS_DataTest.createContact('Test Name', 'TestLastName', 'Developer', 'mail@address.com', testAccount.Id);
        testContact.Language__c = 'English';
        testContact.Title = 'Ms';
        insert testContact;

        
            

        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

        cscfga__Product_Definition__c businessInternetDef = CS_DataTest.createProductDefinition('Business Internet');
        businessInternetDef.RecordTypeId = productDefinitionRecordType;
        businessInternetDef.Product_Type__c = 'Fixed';
        insert businessInternetDef;

        cscfga__Product_Definition__c businessInternetChildDef = CS_DataTest.createProductDefinition('Internet');
        businessInternetChildDef.RecordTypeId = productDefinitionRecordType;
        businessInternetChildDef.Product_Type__c = 'Fixed';
        insert businessInternetChildDef;

        cscfga__Product_Configuration__c businessInternetConf = CS_DataTest.createProductConfiguration(businessInternetDef.Id, 'Business Internet',testBasket.Id);
        insert businessInternetConf;

        cscfga__Product_Configuration__c businessInternetChildConf = CS_DataTest.createProductConfiguration(businessInternetChildDef.Id, 'Internet',testBasket.Id);
        insert businessInternetChildConf;
        
        csord__Order__c order = new csord__Order__c();
        order.csord__Identification__c = 'Order_'+String.valueOf(testBasket.Id);
        insert order;
            
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());
    
        Site__c site = CS_DataTest.createSite('Utrecht Lestraat 12',testAccount,'3572RE','Lestraat','Utrecht',12);
        site.Footprint__c = null;
        site.Site_House_Number_Suffix__c = 'A';
        site.Country__c = 'NL';
        insert site;
    
       
        COM_Delivery_Order__c deliveryOrder = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', order.Id, false);
        insert deliveryOrder;

        csord__Subscription__c businessInternetSub = CS_DataTest.createSubscription(businessInternetConf.Id);
        businessInternetSub.csord__Identification__c = 'Business_Internet_Sub_'+String.valueOf(testBasket.Id);
        businessInternetSub.csord__Status__c = 'Subscription created';
        businessInternetSub.csord__Order__c = order.Id;
        insert businessInternetSub;

        csord__Service__c businessInternetService = CS_DataTest.createService(order.Id,deliveryOrder.Id, 'Business Internet',testBasket.Id,businessInternetSub.Id, site.Id, null, businessInternetConf.Id);
        insert businessInternetService;
        
        
        csord__Service_Line_Item__c businessInternetSLIOneOff = CS_DataTest.createServiceLineItem(testBasket.Id, 'Business Internet One Off SLI', businessInternetService.Id, false, 0,COM_CreateBSSOrder.SLI_CHARGE[0], 'Internet SLI OF');
        insert businessInternetSLIOneOff;
        csord__Service_Line_Item__c businessInternetSLIRec = CS_DataTest.createServiceLineItem(testBasket.Id, 'Business Internet Recurring SLI', businessInternetService.Id, true, 0,COM_CreateBSSOrder.SLI_CHARGE[0], 'Internet SLI REC');
        insert businessInternetSLIRec;
        
           
        csord__Service__c internetChildService = CS_DataTest.createService(order.Id, deliveryOrder.Id, 'Internet Subsc child service',testBasket.Id, businessInternetSub.Id, site.Id, businessInternetService.Id, businessInternetChildConf.Id);
        insert internetChildService;

        csord__Service_Line_Item__c internetChildSLIOneOff = CS_DataTest.createServiceLineItem(testBasket.Id, 'Internet One Off SLI', internetChildService.Id, false, 0,COM_CreateBSSOrder.SLI_CHARGE[0], 'Internet Subscription SLI OF');
        insert internetChildSLIOneOff;
        csord__Service_Line_Item__c internetCChildSLIRec = CS_DataTest.createServiceLineItem(testBasket.Id, 'Internet Recurring SLI',internetChildService.Id, true, 0,COM_CreateBSSOrder.SLI_CHARGE[0], 'Internet Subscription SLI REC');
        insert internetCChildSLIRec;

        csord__Service_Line_Item__c internetSLIOFDiscount = CS_DataTest.createServiceLineItem(testBasket.Id, 'Internet One Off SLI', internetChildService.Id, false, 5.00,COM_CreateBSSOrder.SLI_CHARGE_DISCOUNT[0], 'Internet; OF+ Discount');
        insert internetSLIOFDiscount;


        csord__Service__c accessChildService = CS_DataTest.createService(order.Id, deliveryOrder.Id,'Access Subsc child service',testBasket.Id, businessInternetSub.Id, site.Id, businessInternetService.Id, businessInternetChildConf.Id);
        insert accessChildService;

        csord__Service_Line_Item__c accessChilddSLIOneOff = CS_DataTest.createServiceLineItem(testBasket.Id, 'Access One Off SLI', accessChildService.Id, false, 0,COM_CreateBSSOrder.SLI_CHARGE[0], 'Access Subscription SLI OF');
        insert accessChilddSLIOneOff;
        csord__Service_Line_Item__c accessChildSLIRec = CS_DataTest.createServiceLineItem(testBasket.Id, 'Access Recurring SLI',accessChildService.Id, true, 0,COM_CreateBSSOrder.SLI_CHARGE[0], 'Access Subscription SLI REC');
        insert accessChildSLIRec;

        csord__Service_Line_Item__c accessChildSLIRecDiscount = CS_DataTest.createServiceLineItem(testBasket.Id, 'Internet One Off SLI', accessChildService.Id, true, 5.00,COM_CreateBSSOrder.SLI_CHARGE_DISCOUNT[0], 'Access+ REC ;Discount');
        insert accessChildSLIRecDiscount;


        Attachment att = new Attachment();
        att.ParentId = internetChildService.Id;
        att.Body = Blob.valueOf(generateJSONAttachment2BillingItems());
        att.Name = 'ServiceSpecifications.json';
        insert att;


        Attachment att2 = new Attachment();
        att2.ParentId = accessChildService.Id;
        att2.Body = Blob.valueOf(generateJSONAttachment2BillingItemsAccess());
        att2.Name = 'ServiceSpecifications.json';
        insert att2;

        External_WebService_Config__c config = new External_WebService_Config__c(
        Name = COM_CreateBSSOrder.INTEGRATION_SETTING_NAME,
        URL__c = 'https://mock/createBSSOrder/v3/create',
        Username__c = 'username',
        Password__c = 'password'
        );
        insert config;
           
    }

    private static void setSLIToTwoFailures (List<csord__Service_Line_Item__c> slis) {
        for (csord__Service_Line_Item__c sli : slis) {
            sli.Number_of_failed_attempts__c = 2;
        }
        update slis;
    }
    private static String generateJSONAttachment2BillingItems () {
        String jsonText = '{ "specifications" : [ { "version" : "1", "status" : "Created", "startDate" : "", "specification" : "04e859ed-56cc-5266-984b-c49cc0de8d95", "productConfigurationId" : "a7Z1l0000009qevEAA", "name" : "createBSSOrderInternet", "metadata" : { }, "instanceId" : "", "includeBilling" : false, "guid" : "c9cd3c0b-91cb-2172-b7ab-89a63820d5ac", "endDate" : "", "description" : "Specification for createBSSOrder request to Unify", "code" : "createBSSOrder", "attributes" : { "recurringBITag4" : "recBITag4a", "recurringBITag3" : "recBITag3a", "recurringBITag2" : "recBITag2a", "recurringBITag1" : "recBITag1a", "recurringChargeCode" : "code1234", "recurringChargeDescription" : "Test+ recurring", "oneOffBITag4" : "oneOffBITag4b", "oneOffBITag3" : "oneOffBITag3b", "oneOffChargeDescription" : "codeOF1234", "oneOffChargeCode" : "Test oneOff", "oneOffBITag2" : "oneOffBITag2b", "oneOffBITag1" : "oneOffBITag1b", "quantity" : "1.00", "baseOTC" : "15.00", "baseMRC" : 5.00 }, "additionalAttributes" : { } } ], "serviceId" : "a6G1l0000000oDqEAI", "legacyAttributes" : [ ] }';
        return jsonText;
    }

    private static string generateJSONAttachment2BillingItemsAccess () {
        String jsonText = '{ "specifications" : [ { "version" : "1", "status" : "Created", "startDate" : "", "specification" : "04e859ed-56cc-5266-984b-c49cc0de8d95", "productConfigurationId" : "a7Z1l0000009qevEAA", "name" : "createBSSOrderAccess", "metadata" : { }, "instanceId" : "", "includeBilling" : false, "guid" : "c9cd3c0b-91cb-2172-b7ab-89a63820d5ac", "endDate" : "", "description" : "Specification for createBSSOrder request to Unify", "code" : "createBSSOrder", "attributes" : { "recurringBITag4" : "recBITag4aAccess", "recurringBITag3" : "recBITag3access", "recurringBITag2" : "recBITag2access", "recurringBITag1" : "recBITag1access", "recurringChargeCode" : "code1234ccess", "recurringChargeDescription" : "Test recurringccess", "oneOffBITag4" : "oneOffBITag4baccess", "oneOffBITag3" : "oneOffBITag3bccess", "oneOffChargeDescription" : "codeOF1234+ccess+", "oneOffChargeCode" : "Test oneOffccess", "oneOffBITag2" : "oneOffBITag2bccess", "oneOffBITag1" : "oneOffBITag1bccess", "quantity" : "2.00", "baseOTC" : "25.00", "baseMRC" : 15.00 }, "additionalAttributes" : { } } ], "serviceId" : "a6G1l0000000oDqEAI", "legacyAttributes" : [ ] }';
        return jsonText;
    }

    static testMethod void testUnifyCallout() {
        COM_Integration_setting__mdt  unifySetting = COM_Integration_setting__mdt.getInstance(COM_CreateBSSOrder.UNIFY_SETTING_NAME);
        String successStatus = unifySetting!= null ? unifySetting.Sync_success_status__c : COM_CreateBSSOrder.BILLING_STATUS_REQUEST_ACTIVE;
        
        // Set mock callout class 
        COM_Delivery_Order__c deliveryOrder = [SELECT Id 
                                FROM COM_Delivery_Order__c];
        COM_MockWebService.setTestMockResponse(200,'OK','{"status":"Successful"}');
        COM_CreateBSSOrder.sendToBilling(new List<Id>{deliveryOrder.Id});
        
        List<csord__Service_Line_Item__c>  slis = [SELECT csord__Service__r.Name, csord__Line_Description__c 
                                                    FROM csord__Service_Line_Item__c];
        System.debug('Service line items');
        System.debug(slis);
        

        Order_Billing_Transaction__c  obt = [SELECT Service_Line_Items__c, Delivery_Order__c, Account__c, Transaction_Id__c, Error_Info__c 
                                            FROM Order_Billing_Transaction__c];
        System.assertEquals(deliveryOrder.Id, obt.Delivery_Order__c, '1 Order Billing Transaction should have been created!');  
    } 

    static testMethod void testUpdateService() {
        COM_Integration_setting__mdt  unifySetting = COM_Integration_setting__mdt.getInstance(COM_CreateBSSOrder.UNIFY_SETTING_NAME);
        String successStatus = unifySetting!= null ? unifySetting.Sync_success_status__c : COM_CreateBSSOrder.BILLING_STATUS_REQUEST_ACTIVE;
        
        // Set mock callout class 
        COM_Delivery_Order__c order = [SELECT Id 
                                FROM COM_Delivery_Order__c];
        COM_MockWebService.setTestMockResponse(200,'OK','{"status":"Successful"}');
        COM_CreateBSSOrder.sendToBilling(new List<Id>{order.Id});
        
        List<csord__Service__c>  services = [SELECT Id, csord__Status__c 
                                            FROM csord__Service__c];
        
        for (csord__Service__c service : services) {
            System.assertEquals(successStatus, service.csord__Status__c, 'Service should have been active');  
        }
        
    } 

    static testMethod void testUpdateSLI() {
        COM_Integration_setting__mdt  unifySetting = COM_Integration_setting__mdt.getInstance(COM_CreateBSSOrder.UNIFY_SETTING_NAME);
        String successStatus = unifySetting!= null ? unifySetting.Sync_success_status__c : COM_CreateBSSOrder.BILLING_STATUS_REQUEST_ACTIVE;
        
        // Set mock callout class 
        COM_Delivery_Order__c order = [SELECT Id 
                                FROM COM_Delivery_Order__c];
        
        COM_MockWebService.setTestMockResponse(200,'OK','{"status":"Successful"}');          
        COM_CreateBSSOrder.sendToBilling(new List<Id>{order.Id});

        List<csord__Service_Line_Item__c>  items = [SELECT Id, Billing_Status__c, csord__line_item_type__c
                                            FROM csord__Service_Line_Item__c];
        
        for (csord__Service_Line_Item__c sli : items) {
            if (COM_CreateBSSOrder.SLI_CHARGE_DISCOUNT.contains(sli.csord__line_item_type__c)) {
                System.assertEquals(COM_CreateBSSOrder.BILLING_STATUS_REQUEST_DISCOUNT, sli.Billing_Status__c, 'Service should have been active');  
            }
            if (COM_CreateBSSOrder.SLI_CHARGE.contains(sli.csord__line_item_type__c)) {
                System.assertEquals(successStatus, sli.Billing_Status__c, 'Service should have been active');  
            }
        }
        
    } 

    static testMethod void testUpdateSubscription() {
        COM_Integration_setting__mdt  unifySetting = COM_Integration_setting__mdt.getInstance(COM_CreateBSSOrder.UNIFY_SETTING_NAME);
        String successStatus = unifySetting!= null ? unifySetting.Sync_success_status__c : COM_CreateBSSOrder.BILLING_STATUS_REQUEST_ACTIVE;
        
        // Set mock callout class 
        COM_Delivery_Order__c order = [SELECT Id 
                                FROM COM_Delivery_Order__c];
        COM_MockWebService.setTestMockResponse(200,'OK','{"status":"Successful"}');
        
        COM_CreateBSSOrder.sendToBilling(new List<Id>{order.Id});
        
        List<csord__Subscription__c>  subscriptions = [SELECT Id, csord__Status__c 
                                                        FROM csord__Subscription__c];
        
        for (csord__Subscription__c subscription : subscriptions) {
            System.assertEquals(successStatus, subscription.csord__Status__c, 'Subscriptions should have been active');  
        }
        
    } 


    static testMethod void testOneFailure() {
        COM_Integration_setting__mdt  unifySetting = COM_Integration_setting__mdt.getInstance(COM_CreateBSSOrder.UNIFY_SETTING_NAME);
        String intermittentStatus = unifySetting!= null ? unifySetting.Sync_intermittent_status__c : COM_CreateBSSOrder.BILLING_STATUS_REQUESTED;
        
        // Set mock callout class 
        COM_Delivery_Order__c order = [SELECT Id 
                                FROM COM_Delivery_Order__c];
        COM_MockWebService.setTestMockResponse(400,'error','ERROR MESSAGE');
        
        COM_CreateBSSOrder.sendToBilling(new List<Id>{order.Id});
        
        List<csord__Service_Line_Item__c>  slis = [SELECT Id, Billing_Status__c,csord__line_item_type__c 
                                                        FROM csord__Service_Line_Item__c];

        List<csord__Service__c>  services = [SELECT Id, csord__Status__c 
                                                FROM csord__Service__c];

        List<csord__Subscription__c>  subs = [SELECT Id, csord__Status__c
                                                FROM csord__Subscription__c];

        for (csord__Service__c service : services) {
            System.assertEquals(intermittentStatus, service.csord__Status__c, 'service have been requested');  
        }

        for (csord__Subscription__c sub : subs) {
            System.assertEquals(intermittentStatus, sub.csord__Status__c, 'subscription have been requested');  
        }

        for (csord__Service_Line_Item__c sli : slis) {
            if (COM_CreateBSSOrder.SLI_CHARGE.contains(sli.csord__line_item_type__c)) {
                System.assertEquals(intermittentStatus, sli.Billing_Status__c, 'SLI should have been requested');  
            }
        }
        
    } 
}