/**
 * @description			This is the test class that contains the tests for the BICC BOA Customer Value Batch
 * @author				Ferdinand Bondt
 */

@isTest
private class TestBiccBoaInformationBatch {

	static testMethod void myUnitTest() {

		//It is not possible yet to test AccountHistory
		//Data preparation
		TestUtils.autoCommit = false;

		map<String, String> mapping = new map<String, String>{'BAN__c' => 'Name',
															  'Class_1_Name__c' => 'Class_1_Name__c',
															   'Account_owner_cd__c' => 'Dealer_code__c'};
		list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
		for (String field : mapping.keyset()) {
			fsmlist.add(TestUtils.createSync('BICC_BOA_Information__c -> Ban__c', field, mapping.get(field)));
		}
		insert fsmlist;

		list<Account> accountList = new list<Account>{
		new Account(Name = 'Test Account 00', KVK_number__c = '22312351', Visiting_Postal_Code__c = '1491 HV', Ban_Number__c = '312312311')};
		insert accountList;

		Dealer_information__c di = new Dealer_Information__c(Dealer_code__c = '123456');
		insert di;

		Account acc = [select Id from Account where KVK_number__c = '22312351'];

		list<Ban__c> banList = new list<Ban__c>{
		new Ban__c(Name = '312312311', Account__c = acc.Id)}; //Update
		insert banList;

		TestUtils.autoCommit = true;

		TestUtils.createBiccBoa('412312311', 'Class name test'); //Wrong BAN number
		TestUtils.createBiccBoa('312312311', 'Class name test'); //Match existing account
		TestUtils.createBiccBoa('312312312', 'Class name test'); //Ban number doesn't exist

		//Test
		Test.startTest();

		PageReference pageRef = Page.BiccBoaInformationBatchExecute;
		Test.setCurrentPage(pageRef);
		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new list<BICC_BOA_Information__c>());
		BiccBoaInformationBatchExecuteController controller = new BiccBoaInformationBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		//Checks
		//System.assertEquals([select Class_1_Name__c from Ban__c where Name =: '312312311' limit 1].Class_1_Name__c, 'Class name test'); //Succesful update
	}
}