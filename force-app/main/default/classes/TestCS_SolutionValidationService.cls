@isTest
public with sharing class TestCS_SolutionValidationService {
	@testSetup
	private static void setup() {
		Account account = CS_DataTest.createAccount('Test Account');
		insert account;
		Account account2 = CS_DataTest.createAccount('Test Account 2');
		insert account2;

		cssdm__Solution_Definition__c solutionDefinition = CS_DataTest.createSolutionDefinition(
			CS_ConstantsCOM.SOLUTION_DEFINITION_NAME_BIMO,
			CS_ConstantsCOM.SOLUTION_DEFINITION_TYPE_MAIN
		);
		insert solutionDefinition;
		cssdm__Solution_Definition__c solutionDefinition2 = CS_DataTest.createSolutionDefinition(
			CS_ConstantsCOM.SOLUTION_DEFINITION_NAME_BIMO,
			CS_ConstantsCOM.SOLUTION_DEFINITION_TYPE_MAIN
		);
		insert solutionDefinition2;

		csord__Solution__c solution = CS_DataTest.createSolution('BIMO_TestWithAcc');
		solution.csord__Account__c = account.Id;
		solution.cssdm__Solution_Definition__c = solutionDefinition.Id;
		solution.Is_Active__c = true;
		solution.csord__Identification__c = 'SlovojedPapaKnjigu';
		insert solution;

		Opportunity opp = CS_DataTest.createOpportunity(account, 'TestOpportunity', null);
		insert opp;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'TestProductBasket');
		basket.csbb__Account__c = account.Id;
		insert basket;
		cscfga__Product_Basket__c basket2 = CS_DataTest.createProductBasket(opp, 'TestProductBasket2');
		basket2.csbb__Account__c = account.Id;
		insert basket2;
		cscfga__Product_Basket__c basket4 = CS_DataTest.createProductBasket(opp, 'TestProductBasket3');
		basket4.csbb__Account__c = account.Id;
		insert basket4;

		csord__Solution__c solution3 = CS_DataTest.createSolution('BIMO_TestForReplace');
		solution3.Is_Active__c = false;
		solution3.cssdm__product_basket__c = basket.Id;
		solution3.cssdm__Solution_Definition__c = solutionDefinition.Id;
		solution3.csord__Identification__c = 'SlovojedPapaKnjigu';
		insert solution3;

		csord__Solution__c solution4 = CS_DataTest.createSolution('BIMO_TestForReplace');
		solution4.Is_Active__c = true;
		//solution4.csord__Account__c = account.Id;
		solution4.cssdm__product_basket__c = basket2.Id;
		solution4.cssdm__Solution_Definition__c = solutionDefinition.Id;
		solution4.csord__Identification__c = 'SlovojedPapaKnjigu';
		insert solution4;

		csord__Solution__c solution2 = CS_DataTest.createSolution('BIMO_TestForOutdated');
		solution2.csord__Account__c = account.Id;
		solution2.cssdm__product_basket__c = basket.Id;
		solution2.cssdm__Solution_Definition__c = solutionDefinition.Id;
		solution2.cssdm__replaced_solution__c = solution3.Id;
		solution2.csord__Identification__c = 'SlovojedPapaKnjigu';
		solution2.Is_Active__c = false;
		insert solution2;

		Opportunity opp2 = CS_DataTest.createOpportunity(account, 'TestOpportunity2', null);
		insert opp2;

		cscfga__Product_Basket__c basket3 = CS_DataTest.createProductBasket(opp2, 'TestProductBasket3');
		basket3.csordtelcoa__Synchronised_with_Opportunity__c = true;
		basket3.Primary__c = true;
		insert basket3;

		//List<String> LIST_UNIQUE_SOLUTION_PER_ACCOUNT = new List<String>{ CS_ConstantsCOM.SOLUTION_DEFINITION_NAME_BIMO };
	}
	@isTest
	static void testCanAddAcquisitionSolution() {
		Account acc = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		Account acc2 = [SELECT Id, Name FROM Account WHERE Name = 'Test Account 2'];
		//cssdm__Solution_Definition__c solutionDefinition = [SELECT Name, Id FROM cssdm__Solution_Definition__c WHERE Name =:CS_ConstantsCOM.SOLUTION_DEFINITION_NAME_BIMO];
		System.assertEquals(
			false,
			CS_SolutionValidationService.canAddAcquisitionSolution(acc.Id, CS_ConstantsCOM.SOLUTION_DEFINITION_NAME_BIMO),
			'Should not be able to add solution to the account.'
		);
		System.assertEquals(
			true,
			CS_SolutionValidationService.canAddAcquisitionSolution(acc2.Id, CS_ConstantsCOM.SOLUTION_DEFINITION_NAME_BIMO),
			'Should be able to add solution to the account.'
		);
	}

	@isTest
	static void testIsMacdBasketOutdated() {
		Opportunity opp = [SELECT Id, Name FROM Opportunity WHERE Name = 'TestOpportunity'];
		List<cscfga__Product_Basket__c> baskets = [SELECT Id, Name FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c = :opp.Id];
		//cscfga__Product_Basket__c basket = [SELECT Id, Name FROM cscfga__Product_Basket__c WHERE Name='TestProductBasket'];
		//cscfga__Product_Basket__c basket2 = [SELECT Id, Name FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c =: opp.Id AND Name='TestProductBasket2'];
		System.assertEquals(true, CS_SolutionValidationService.isMacdBasketOutdated(baskets[0].Id), 'Macd basket should be outdated.');
		System.assertEquals(false, CS_SolutionValidationService.isMacdBasketOutdated(baskets[1].Id), 'Macd basket should not be outdated.');
	}

	@isTest
	static void testCanSyncBasket() {
		Opportunity opp = [SELECT Id, Name FROM Opportunity WHERE Name = 'TestOpportunity'];
		List<cscfga__Product_Basket__c> baskets = [
			SELECT Id, Name, csbb__Account__c
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c = :opp.Id
		];
		/* cscfga__Product_Basket__c basket = [SELECT Id, Name FROM cscfga__Product_Basket__c WHERE  Name='TestProductBasket'];
		 cscfga__Product_Basket__c basket2 = [SELECT Id, Name FROM cscfga__Product_Basket__c WHERE cscfga__Opportunity__c =: opp.Id AND Name='TestProductBasket2']; */
		System.assertEquals(false, CS_SolutionValidationService.canSyncBasket(baskets[1]), 'Basket should not be synced.');
		System.assertEquals(true, CS_SolutionValidationService.canSyncBasket(baskets[2]), 'Basket can be synchronised.');
	}

	@isTest
	static void testCanCloseWon() {
		Opportunity opp = [SELECT Id, Name FROM Opportunity WHERE Name = 'TestOpportunity'];
		Opportunity opp2 = [SELECT Id, Name FROM Opportunity WHERE Name = 'TestOpportunity2'];
		System.assertEquals(false, CS_SolutionValidationService.canCloseWon(opp.Id), 'Should not be able to close won an opportunity.');
		System.assertEquals(true, CS_SolutionValidationService.canCloseWon(opp2.Id), 'Should be possible to close won an opportunity.');
	}
}
