@isTest
public class TestOrderFormCustomerDataController {
	@isTest
	static void orderFormCustomerDataContrMethod() {
		Set<Id> banIds = new Set<Id>();
		Set<Id> conIds = new Set<Id>();
		Set<Id> contIds = new Set<Id>();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact cont = TestUtils.createContact(acct);
		cont.Email = 'test@gmail1.com';
		cont.userid__c = owner.Id;
		update cont;

		Site__c site = TestUtils.createSite(acct);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, cont.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		Test.startTest();
		Contact_Role__c contRole = new Contact_Role__c();
		contRole.Account__c = acct.Id;
		contRole.Contact__c = cont.Id;
		contRole.Site__c = site.Id;
		contRole.Active__c = true;
		contRole.Type__c = 'main';
		insert contRole;
		Contact_Role__c contRole1 = new Contact_Role__c();
		contRole1.Account__c = acct.Id;
		contRole1.Contact__c = cont.Id;
		contRole1.Site__c = site.Id;
		contRole1.Active__c = true;
		contRole1.Type__c = 'maintenance';
		insert contRole1;
		Contact_Role__c contRole2 = new Contact_Role__c();
		contRole2.Account__c = acct.Id;
		contRole2.Contact__c = cont.Id;
		contRole2.Site__c = site.Id;
		contRole2.Active__c = true;
		contRole2.Type__c = 'incident';
		insert contRole2;
		Contact_Role__c contRole3 = new Contact_Role__c();
		contRole3.Account__c = acct.Id;
		contRole3.Contact__c = cont.Id;
		contRole3.Site__c = site.Id;
		contRole3.Active__c = true;
		insert contRole3;
		contIds.add(contRole.Id);
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		opp.Ban__c = ban.Id;
		update opp;
		Order__c ord = TestUtils.createOrder(contr);
		banIds.add(ban.Id);
		conIds.add(cont.Id);

		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'AFD';
		externalAcc.External_Source__c = 'Unify';
		insert externalAcc;
		TestUtils.createOrderValidationOrder();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationContactRoles();

		VF_Contract__c c = [SELECT Id, Account__c FROM VF_Contract__c LIMIT 1];
		PageReference pageRef = Page.OrderFormCustomerDetails;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('orderId', ord.Id);
		ApexPages.currentPage().getParameters().put('contractId', c.Id);
		OrderFormCustomerDataController orderContCtrl = new OrderFormCustomerDataController();
		orderContCtrl.theContract.Account__c = c.Account__c;
		orderContCtrl.theContract = c;
		orderContCtrl.theOrderWrapper.customerMainContact = contRole;
		orderContCtrl.theOrderWrapper.customerMaintenanceContact = contRole1;
		orderContCtrl.theOrderWrapper.customerIncidentContact = contRole2;
		orderContCtrl.theOrderWrapper.customerChooserContact = contRole3;
		orderContCtrl.theOrderWrapper.theOrder = ord;
		orderContCtrl.mainContactSelected = cont.Id;
		String contactId = cont.Id;
		orderContCtrl.refresh();
		orderContCtrl.saveCustomer();
		Contact_Role__c contactRole = orderContCtrl.createContactRole('maintenance', contactId);
		system.assert(contactRole != null, 'contact role isnot null');
		Test.stopTest();
	}

	@isTest
	static void orderFormCustomerDataContrMethodOne() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		Set<Id> banIds = new Set<Id>();
		Set<Id> conIds = new Set<Id>();
		Set<Id> contIds = new Set<Id>();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact cont = TestUtils.createContact(acct);
		cont.Email = 'test@gmail1.com';
		cont.userid__c = owner.Id;
		update cont;

		Site__c site = TestUtils.createSite(acct);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, cont.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		Contact_Role__c contRole = new Contact_Role__c();
		contRole.Account__c = acct.Id;
		contRole.Contact__c = cont.Id;
		contRole.Site__c = site.Id;
		contRole.Active__c = true;
		contRole.Type__c = 'chooser';
		insert contRole;
		contIds.add(contRole.Id);

		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		Order__c ord = TestUtils.createOrder(contr);
		banIds.add(ban.Id);
		conIds.add(cont.Id);
		Test.startTest();
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'AFD';
		externalAcc.External_Source__c = 'Unify';
		insert externalAcc;
		TestUtils.createOrderValidationOrder();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationContactRoles();

		VF_Contract__c c = [SELECT Id, Account__c FROM VF_Contract__c LIMIT 1];
		PageReference pageRef = Page.OrderFormCustomerDetails;
		Test.setCurrentPage(pageRef);
		ApexPages.currentPage().getParameters().put('orderId', null);
		ApexPages.currentPage().getParameters().put('contractId', c.Id);
		OrderFormCustomerDataController orderContCtrl = new OrderFormCustomerDataController();
		orderContCtrl.theContract.Account__c = c.Account__c;
		orderContCtrl.theContract = c;
		orderContCtrl.showPartnerContacts = false;
		orderContCtrl.theOrderWrapper.customerChooserContact = contRole;
		orderContCtrl.theOrderWrapper.theOrder = ord;

		orderContCtrl.refresh();
		orderContCtrl.saveCustomer();
		system.assert(c != null, 'contact role is not null');
		Test.stopTest();
	}
}
