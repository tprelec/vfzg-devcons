@isTest
private class TestAccountSharingRulesBatch {
    
    @isTest
    static void testShareAsset() {

        // create an account and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Contact cont = TestUtils.createContact(acct);
        User u = TestUtils.createManager();

        VF_Asset__c vfa = new VF_Asset__c(Account__c=acct.id);
        insert vfa;

        List<VF_Asset__c> myTest = [Select 
                                            Id, 
                                            OwnerId, 
                                            Account__c, 
                                            Account__r.OwnerId
                                        From 
                                            VF_Asset__c 
                                        Where 
                                            Account__c =:acct.id];
        
        test.startTest();
        cont.Userid__c = u.Id;
        update cont;

        Dealer_Information__c di = TestUtils.createDealerInformation(cont.Id);
        acct.Mobile_Dealer__c = di.Id;
        acct.Fixed_Dealer__c = di.Id;
        update acct;
        
        Set<id> accountSet = new Set<id>();
        accountSet.add(acct.id);

        AccountSharingRulesBatch bc = new AccountSharingRulesBatch(accountSet);
        Database.executeBatch(bc, 200);

        test.stopTest();
    }
}