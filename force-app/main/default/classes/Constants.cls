public with sharing class Constants {
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_NEW = 'New';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_REQUESTED = 'Requested';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_PROCESSED = 'Processed';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_FAILED = 'Failed';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_NO_BILLING = 'No Billing';
	public static final String CONTRACTED_PRODUCT_BILLING_STATUS_SPECIAL_BILLING = 'Special Billing';

	public static final String DELIVERY_COMPONENT_NO_CHANGE_STATUS = 'No Change';
	public static final String DELIVERY_COMPONENT_TERMINATED_STATUS = 'Terminate';

	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_NEW = 'New';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_IN_DELIVERY = 'In Delivery';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_AWAITING_APPROVAL = 'Awaiting Approval';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED = 'Approved';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_CANCELLED = 'Cancelled';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_OPEN = 'OPEN';
	public static final String CONTRACTED_PRODUCT_DELIVERY_STATUS_ROLLBACK = 'Rollback Requested';

	public static final String CONTRACTED_PRODUCT_DEAL_TYPE_NEW = 'New';

	public static final String CONTRACTED_PRODUCT_BOP_STATUS_CLOSED = 'CLOSED';

	public static final String CONTRACTED_PRODUCT_CLC_ACQ = 'Acq';

	public static final String CONTRACTED_PRODUCT_QUANTITY_TYPE_MONTHLY = 'Monthly';

	public static final String CONTRACTED_PRODUCT_ROW_TYPE_ADD = 'Add';
	public static final String CONTRACTED_PRODUCT_ROW_TYPE_REMOVE = 'Remove';
	public static final String CONTRACTED_PRODUCT_ROW_TYPE_PRICE_CHANGE = 'Price_change';
	//added aditional proposition on ONE NET for SFOD-284 'One Net Enterprise Flex'
	public static final Set<String> CONTRACTED_PRODUCTS_PROPOSITION_ONE_NET = new Set<String>{
		'One Net',
		'One Net Enterprise',
		'One Net Express',
		'One Net Enterprise Flex'
	};
	public final static Set<String> FAMILYTAGSTHATREQUIREPBX = new Set<String>{ 'PBX_Trunking', 'PBX_Number_Charging', 'PBX_Trunking_SIP_CHARGING' };

	public static final String CUSTOMER_ASSET_BILLING_STATUS_NEW = 'New';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_ACTIVE = 'Active';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_COMPLETED = 'Completed';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_CANCELLED = 'Cancelled';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_NO_BILLING = 'No Billing';
	public static final String CUSTOMER_ASSET_BILLING_STATUS_SPECIAL_BILLING = 'Special Billing';

	public static final String CUSTOMER_ASSET_TYPE_NORMAL = 'Normal';

	public static final String ORDER_STATUS_NEW = 'New';
	public static final String ORDER_STATUS_CLEAN = 'Clean';
	public static final String ORDER_STATUS_SUBMITTED = 'Submitted';
	public static final String ORDER_STATUS_ACCEPTED = 'Accepted';
	public static final String ORDER_BOP_ORDER_STATUS_PROJECT_CREATED = 'Project Created';
	public static final String ORDER_STATUS_PROCESSED_MANUALLY = 'Processed manually';
	public static final String ORDER_TYPE_BMS = 'Business Managed Service';

	public static final String PRODUCT2_BILLINGTYPE_SPECIAL = 'Special';
	public static final String PRODUCT2_BILLINGTYPE_STANDARD = 'Standard';

	public static final String PRODUCT2_FAMILYTAG_PBX_TRUNKING_SIP_CHARGING = 'PBX_Trunking_SIP_Charging';
	public static final String PRODUCT2_FAMILYTAG_PBX_TRUNKING = 'PBX_Trunking';

	public static final String ORDERTYPE_EXPORT_SYSTEM_BOP = 'BOP';
	public static final String OPPORTUNITY_DEALTYPE_NEW = 'NEW';
	public static final String OPPORTUNITY_DEALTYPE_ACQUISITION = 'Acquisition';

	public static final String PROFILE_INDIRECT_SALES = 'VF Indirect Inside Sales';
	public static final String PROFILE_DIRECT_SALES = 'VF Direct Inside Sales';
	public static final String PROFILE_BPM = 'VF Business Partner Manager';
	public static final String PROFILE_SYSTEM_ADMINISTRATOR = 'System Administrator';
	public static final String PROFILE_D2D_PARTNER_SALES = 'LG_NL D2D_Partner Sales User';

	public static final String PERMISSION_SET_GROUP_SAG_INDIRECT = 'SAG_Indirect';
	public static final String PERMISSION_SET_GROUP_SAG_DIRECT = 'SAG_Direct';

	public static final String ORCHESTRATOR_STEP_COMPLETE = 'Complete';
	public static final String ORCHESTRATOR_STEP_ERROR = 'Error';

	public static final String DOCUSIGN_ATTACHMENT_STATUS_DEFAULT = 'DEFAULT';
	public static final String DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_ATTACHED = 'ATTACHMENTS ATTACHED';
	public static final String DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_TO_BE_REMOVED = 'ATTACHMENTS TO BE REMOVED';
	public static final String DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_REMOVED = 'ATTACHMENTS REMOVED';

	// Account
	public static final String ACCOUNT_APPROVAL_STATUS_REJECTED = 'Rejected';
	public static final String ACCOUNT_TYPE_DEALER = 'Dealer';

	public static final String CONNECTION_TYPE_VOICE = 'Voice';
	public static final String CONNECTION_TYPE_DATA = 'Data';
	public static final String CONNECTION_TYPE_VOICE_CODE = '06';
	public static final String CONNECTION_TYPE_DATA_CODE = '097';
	public static final String NET_PROFIT_CTN_VALID_SIM_CARD_NUMBER = '893144123';

	public static final String VF_ASSET_CTN_STATUS_ACTIVE = 'Active';
	public static final String VF_ASSET_CTN_STATUS_SUSPENDED = 'Suspended';
	public static final String VF_ASSET_CTN_STATUS_CANCELLED = 'Cancelled';
	public static final String VF_ASSET_CTN_STATUS_INACTIVE = 'Inactive';
	public static final String VF_ASSET_PRICEPLAN_DATAONLY = 'Data Only';
	public static final String VF_ASSET_PRICEPLAN_DATA_ONLY = 'Data_Only';
	public static final String VF_ASSET_PRICEPLAN_MOBILE_ONLY = 'Mobile Voice';

	public static final String VF_CONTRACT_STATUS_ACTIVE = 'Active';
	public static final String VF_CONTRACT_STATUS_UNLOCKED = 'Unlocked';
	public static final String VF_CONTRACT_STATUS_TERMINATED = 'Terminated';
	public static final String VF_CONTRACT_MOBILE_SERVICE = 'Mobile';
	public static final String VF_CONTRACT_STATUS_NOT_SIGNED = 'Not Signed';
	public static final String VF_CONTRACT_STATUS_IMPLEMENTATION = 'Implementation';
	public static final String VF_CONTRACT_MOBILE_IMPLEMENTATION_INDIRECT_SALES = 'Implementation Indirect Sales';

	public static final String SNAPSHOT_CONNECTION_TYPE_DISCONNECT = 'Disconnect';
	public static final String CUSTOMER_ASSET_PENDING_CHANGE = 'Pending Change';

	public static final List<String> DEAL_SUMMARY_PROFILES_FOR_DISCONNECTS = new List<String>{
		'System Administrator',
		'VF EBU/WBU Control',
		'VF Financial Sales Control'
	};

	public static final String POSTCODE = 'Postcode';
	public static final String HOUSENUMBER = 'HouseNumber';
	public static final String HOUSENRSUFFIX = 'HouseNrSuffix';

	public static final String CTN_ASSET_RECORDTYPE_NAME = 'CTN';
	public static final String OPPORTUNITY_SALES_CHANNEL_INDIRECT = 'Indirect';

	public static final String FEATURE_OPTIMIZATIONSETTING_NAME = 'Standard Optimisation';

	// Dealer_Information__c
	public static final String DEALER_INFORMATION_SALES_CHANNEL_SOHO = 'SoHo';
	public static final String DEALER_INFORMATION_SALES_CHANNEL_INDIRECT = 'Indirect';
	public static final String DEALER_INFORMATION_STATUS_ACTIVE = 'Active';
	public static final String DEALER_INFORMATION_STATUS_EXPIRED = 'Expired';

	// BICC_Dealer_Information__c
	public static final Integer BICC_DEALER_INFORMATION_STATUS_ACTIVE = 1;
	public static final Integer BICC_DEALER_INFORMATION_STATUS_EXPIRED = 0;

	// CreditNote_Approval_Matrix__c
	public static final String APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER = 'USER';
	public static final String APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE = 'QUEUE';
	public static final String APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_CHANNEL_MANAGER_AND_SOLUTION_SPECIALIST = 'PARTNER_CHANNEL_MANAGER_AND_SOLUTION_SPECIALIST';
	public static final String APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_PARTNER_OPERATIONAL_MANAGER = 'PARTNER_OPERATIONAL_PARTNER_MANAGER';
	public static final String APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_OWNER_MANAGER = 'OWNER_MANAGER';
	public static final String APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_SALES_MANAGER = 'SALES_MANAGER';
	public static final String APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_MANUAL = 'MANUAL';

	// Credit_Note__c
	public static final String CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_REFUND = 'Credit_Refund';
	public static final String CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT = 'Credit_Note_Indirect';
	public static final String CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_CREDIT_REFUND = 'Credit_Refund';
	public static final String CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_FZIGGO = 'Credit_Note_fZiggo';
	public static final String CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL = 'Contractual';
	public static final String CREDIT_NOTE_CREDIT_TYPE_COULANCE = 'Coulance';
	public static final String CREDIT_NOTE_SUBSCRIPTION_MOBILE = 'Mobile';
	public static final String CREDIT_NOTE_SUBSCRIPTION_FIXED = 'Fixed';
	public static final String CREDIT_NOTE_STATUS_APPROVED = 'Approved';
	public static final String CREDIT_NOTE_STATUS_DRAFT = 'Draft';
	public static final String CREDIT_NOTE_STATUS_PENDING_APPROVAL = 'Pending Approval';
	public static final String CREDIT_NOTE_STATUS_READY_FOR_APPROVAL = 'Ready for Approval';
	public static final String CREDIT_NOTE_CREDIT_NOTE_TO_CUSTOMER = 'Customer';
	public static final String CREDIT_NOTE_FREQUENCY_ONE_TIME = 'One time';

	// AccountTeamMember
	public static final String ACCOUNT_TEAM_MEMBER_ROLE_CHANNEL_MANAGER = 'Channel Manager';
	public static final String ACCOUNT_TEAM_MEMBER_ROLE_SOLUTION_SPECIALIST = 'Solution Specialist';
	public static final String ACCOUNT_TEAM_MEMBER_ROLE_OPERATIONAL_PARTNER_MANAGER = 'Operational Partner Manager';

	// NetProfit_CTN__c - Mobile Order
	public static final String NETPROFIT_CTN_ACTION_NEW = 'New';
	public static final String NETPROFIT_CTN_VALID_CTN_NUMBER = '319799123';

	// OpportunityLineItem
	public static final String OPPORTUNITY_LINE_ITEM_DEAL_TYPE_NEW = 'New';
}
