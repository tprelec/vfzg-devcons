@isTest
public with sharing class TestOrderEntryProductDetailTriggerHandle {
	@isTest
	public static void testAutoNumbering() {
		ExternalIDNumber__c custSetting = new ExternalIDNumber__c(
			Name = 'Order_Entry_Product_Detail',
			External_Number__c = 1,
			Object_Prefix__c = 'OEPD-54-',
			blocked__c = false,
			runningOrg__c = '00D1l0000008i1vEAA'
		);
		insert custSetting;

		String orgId = UserInfo.getOrganizationId();
		OE_Product__c product = OrderEntryTestFactory.createOEProduct('Main', true);

		Test.startTest();
		OrderEntryTestFactory.createOEProductDetail(product, 'Category', 'Value', 'Type', true);
		Test.stopTest();

		Order_Entry_Product_Detail__c result = [
			SELECT Id, External_Id__c
			FROM Order_Entry_Product_Detail__c
		];
		if (custSetting.runningOrg__c == orgId) {
			System.assertEquals(
				'OEPD-54-000002',
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		} else {
			System.assertEquals(
				null,
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		}
	}
}