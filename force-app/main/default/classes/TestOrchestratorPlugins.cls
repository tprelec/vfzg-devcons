@IsTest
private class TestOrchestratorPlugins {
    @IsTest
    static void testSetIdentifier() {
        List<Profile> pList = [
            SELECT Id, Name
            FROM Profile
            WHERE Name = 'System Administrator'
            LIMIT 1
        ];
        List<UserRole> roleList = [
            SELECT Id, Name, DeveloperName
            FROM UserRole u
            WHERE ParentRoleId = null
        ];

        User simpleUser = CS_DataTest.createUser(pList, roleList);
        insert simpleUser;

        System.runAs (simpleUser) {
            List<CSSX__Calendar__c> calendars = new List<CSSX__Calendar__c>();
            CSSX__Calendar__c calendar = new CSSX__Calendar__c();
            calendar.Name = 'test calendar';
            calendars.add(calendar);
            insert calendars;

            String calendarDates = '{"dateTimes":[],"dates":["2020-06-01","2020-06-02"]}';
            Attachment attachment = new Attachment();
            attachment.Body = Blob.valueOf(calendarDates);
            attachment.Name = String.valueOf('CalendarDates');
            attachment.ParentId = calendars[0].Id;
            insert attachment;

            OrchestratorPlugins.OperationalLevelAgreementActivityProviderFactory oapf = new OrchestratorPlugins.OperationalLevelAgreementActivityProviderFactory();
            OrchestratorPlugins.OperationalLevelAgreementActivityProvider oap = (OrchestratorPlugins.OperationalLevelAgreementActivityProvider)oapf.create();
            oap.setIdentifier(calendars[0].Id);

            Test.startTest();
            Long amount = oap.getTimeAmount(Datetime.newInstance(2020,6,1), Datetime.newInstance(2020,6,2));
            Datetime nextTimepoint = oap.getNextTimepoint(Datetime.newInstance(2020,6,1),amount);
            String identifier = oap.getIdentifierType();

            System.assertEquals(Date.newInstance(2020,6,2), nextTimepoint.date());
            Test.stopTest();
        }
    }
}