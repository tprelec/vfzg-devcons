public without sharing class OpportunityContractController {

    public String stage {
        get {
            if (stage == null) {
                stage = 'init';
            }
            return stage;
        }
        private set;
    }

    public Boolean closeToLimit {
        get {
            if (closeToLimit == null) {
                closeToLimit = false;
            }
            return closeToLimit;
        }
        private set;
    }

    public List<Id> lineItemIds {
        get {
            if (lineItemIds == null) {
                lineItemIds = new List<Id>();
            }
            return lineItemIds;
        }
        private set;
    }

    public void updateContract() {
        String contractId = ApexPages.currentPage().getParameters().get('contractId').unescapeHtml4();
        updateContractFromOpportunity(contractId);
        if (!closeToLimit) {
            stage = 'contractUpdated';
        }
    }

    public void updateTaskAtt() {
        String contractId = ApexPages.currentPage().getParameters().get('contractId').unescapeHtml4();
        updateTaskAttFromOpportunity(contractId);
        stage = 'taskAttUpdated';
    }

    public PageReference doRedirect() {
        if (PP_LightningController.isLightningCommunity()) {
            String opportunityId = ApexPages.currentPage().getParameters().get('opportunityId').unescapeHtml4();
            PageReference oppPage = new PageReference('/apex/OpportunityClosedWon');
            oppPage.getParameters().put('opportunityId', opportunityId);
            oppPage.getParameters().put('isClosed', '1');
            oppPage.setRedirect(true);
            return oppPage;
        } else {
            String contractId = ApexPages.currentPage().getParameters().get('contractId').unescapeHtml4();
            PageReference pageRef = new PageReference('/apex/OrderFormPreload?contractId=' + contractId);
            pageRef.setRedirect(true);
            return pageRef;
        }
    }

    private static String macId;
    private static String getMacId() {
        if (String.isBlank(macId)) {
            macId = [SELECT Id FROM Ordertype__c WHERE Name='MAC'].Id;
        }
        return macId;
    }

    private void updateContractFromOpportunity(String contractId) {
        Savepoint svpt = Database.setSavepoint();
        try {
            List<VF_Contract__c> contractList = [SELECT Id, Opportunity__c, Owner__c FROM VF_Contract__c WHERE Id = :contractId];
            Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, Name, Vodafone_Products__c, Dummy_Contract__c FROM Opportunity WHERE Contract_VF__c = :contractId]);
            Set<Id> oppIds = oppMap.keySet();

            if (!contractList.isEmpty()) {
                Map<Id, VF_Contract__c> oppIdToContractMap = new Map<Id, VF_Contract__c>();
                for (VF_Contract__c contract: contractList) {
                    oppIdToContractMap.put(contract.Opportunity__c, contract);
                }
                addProductsToContract(oppIdToContractMap, oppIds);
            }
        } catch (Exception e) {
            // If anything fails perform a rollback and delete the (already created) Contract by changing the status back to Closing
            Database.rollback(svpt);
            if (contractId != null) {
                List<Opportunity> opps = [SELECT Id, StageName FROM Opportunity WHERE Contract_VF__c = :contractId];
                if (!opps.isEmpty()) {
                    opps[0].StageName = 'Closing';
                    update opps[0];
                }
            }
            stage = 'error';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, System.Label.LABEL_Error_DML + e.getMessage(), ''));
        }
    }

    private void addProductsToContract(Map<Id, VF_Contract__c> oppIdToContractMap, Set<Id> oppIds) {
        transient List<Contracted_Products__c> productsToInsert = new List<Contracted_Products__c>();
        // Explictly set to false to avoid looping
        closeToLimit = false;
        String oppLiQuery = 'SELECT ';
        Map<String,String> oppProductFieldMapping = SyncUtil.fullMapping('OpportunityLineItem -> Contracted_Products__c').get('OpportunityLineItem -> Contracted_Products__c');
        for (String oppLiField : oppProductFieldMapping.values()) {
            oppLiQuery += oppLiField + ', ';
        }
        oppLiQuery += ' Opportunity.RecordTypeId, Site_Availability_List__c, PricebookEntry.Product2.OrderType__c,PricebookEntry.Product2.Role__c, PricebookEntry.Product2.Product_Group__c, PricebookEntry.Product2Id, PricebookEntry.ProductCode, PricebookEntry.Product2.Multiplier_Vodacom__c, OpportunityId FROM OpportunityLineItem WHERE OpportunityId IN :oppIds';
        // filter out already processed line items (if applicable)
        if (!lineItemIds.isEmpty()) {
            oppLiQuery += ' AND Id NOT IN :lineItemIds';
        }
        for (List<OpportunityLineItem> lineItems : Database.query(String.escapeSingleQuotes(oppLiQuery))) {
            for (OpportunityLineItem lineItem : lineItems) {
                if (Limits.getCpuTime() > 20000 || productsToInsert.size() > 1000) {
                    closeToLimit = true;
                    break;
                }
                lineItemIds.add(lineItem.Id);
                productsToInsert.addAll(lineItemToProducts(lineItem, oppProductFieldMapping, oppIdToContractMap));
            }
        }
        insert productsToInsert;
    }

    private List<Contracted_Products__c> lineItemToProducts(OpportunityLineItem lineItem, Map<String,String> oppProductFieldMapping, Map<Id, VF_Contract__c> oppIdToContractMap) {
        // Create Product from Line Item
        Contracted_Products__c product = new Contracted_Products__c();
        for (String contractProdField : oppProductFieldMapping.keySet()) {
            String oppProdField = oppProductFieldMapping.get(contractProdField);
            Object oppProdValue = lineItem.get(oppProdField);
            if (oppProdValue != null) {
                product.put(contractProdField, oppProdValue);
            }
        }
        product.OpportunityLineItemId__c = lineItem.Id;
        product.Site_List__c = null;
        product.VF_Contract__c = oppIdToContractMap.get(lineItem.OpportunityId).Id;
        product.Product__c = lineItem.PricebookEntry.Product2Id;
        product.ProductCode__c = lineItem.PricebookEntry.ProductCode;
        product.Multiplier_Vodacom__c = lineItem.PricebookEntry.Product2.Multiplier_Vodacom__c;
        if (product.OrderType__c == null) {
            if (lineItem.Opportunity.RecordTypeId == GeneralUtils.getRecordTypeIdByName(SObjectType.Opportunity.getName(), 'MAC')) {
                product.OrderType__c = getMacId();
            } else {
                product.OrderType__c = lineItem.PricebookEntry.Product2.OrderType__c;
            }
        }
        // Create one product for each site
        List<String> sites = getLineItemSites(lineItem);
        List<String> siteAvailabilityList = getLineItemSitesAvailabilities(lineItem);
        return createSiteProducts(lineItem, product, sites, siteAvailabilityList);
    }

    private List<String> getLineItemSites(OpportunityLineItem lineItem) {
        List<String> sites = new List<String>();
        if (!String.isEmpty(lineItem.Site_List__c)) {
            String sep = ',';
            if (lineItem.Site_List__c.contains(';')) {
                sep = ';';
            }
            sites = lineItem.Site_List__c.split(sep);
        } else {
            sites.add(null);
        }
        return sites;
    }

    private List<String> getLineItemSitesAvailabilities(OpportunityLineItem lineItem) {
        List<String> siteAvailabilityList = new List<String>();
        if (!String.isEmpty(lineItem.Site_Availability_List__c)) {
            siteAvailabilityList = lineItem.Site_Availability_List__c.split(';');
        }
        return siteAvailabilityList;
    }

    private List<Contracted_Products__c> createSiteProducts(OpportunityLineItem lineItem, Contracted_Products__c product, List<String> sites, List<String> siteAvailabilityList) {
        List<Contracted_Products__c> products = new List<Contracted_Products__c>();
        Integer numberOfSites = sites.size();
        Integer countSites = 0;
        for (String siteId : sites) {
            // Sometimes BM puts the string 'null' here, which has to be caught..
            if (siteId == 'null') {
                siteId = null;
            }
            Contracted_Products__c cp = product.clone();
            cp.Quantity__c = product.Quantity__c.divide(numberOfSites, 0);
            if (siteId != null) {
                cp.Site__c = siteId;
            }
            if (!siteAvailabilityList.isEmpty() && countSites <= siteAvailabilityList.size() - 1) {
                cp.Site_Availability__c = siteAvailabilityList[countSites];
                countSites++;
            }
            // Create a separate ContractedProduct for some types of products if quantity is more than 1
            if (cp.Quantity__c > 1) {
                if (lineItem.Proposition__c != 'One Mobile') {
                    if (lineItem.PricebookEntry.Product2.Product_Group__c == 'Infra'
                        || lineItem.PricebookEntry.Product2.Product_Group__c == 'SLA'
                        || lineItem.PricebookEntry.Product2.Product_Group__c == 'CPE'
                        || lineItem.PricebookEntry.Product2.Role__c == 'Numberporting'
                        || lineItem.PricebookEntry.Product2.Role__c == 'Switch'
                    ) {
                        Decimal originalQuantity = cp.Quantity__c;
                        cp.Subtotal__c = product.Subtotal__c.divide(originalQuantity,2);
                        cp.Quantity__c = 1;
                        for (Integer i = 0; i < originalQuantity - 1; i++) {
                            Contracted_Products__c cpCopy = cp.clone();
                            products.add(cpCopy);
                        }
                    }
                }
            }
            products.add(cp);
        }
        return products;
    }

    private Map<Id, Opportunity> opportunityBanNumber {get;set;}
    private void addBanAndCorporateIdToContract(List<VF_Contract__c> contracts, Set<Id> oppIds) {
        opportunityBanNumber = new Map<Id, Opportunity> ([
            SELECT Id, Account.name, BAN__r.Name, BAN__r.Corporate_Id__c
            FROM Opportunity
            WHERE Id IN :oppIds
        ]);
    }

    private void updateTaskAttFromOpportunity(String contractId) {
        Savepoint svpt = Database.setSavepoint();
        try {
            List<VF_Contract__c> contractList = [SELECT Id, Opportunity__c, Owner__c FROM VF_Contract__c WHERE Id = :contractId];
            Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, Name, Vodafone_Products__c, Dummy_Contract__c FROM Opportunity WHERE Contract_VF__c = :contractId]);
            Set<Id> oppIds = oppMap.keySet();

            if (!contractList.isEmpty()) {
                this.addBanAndCorporateIdToContract(contractList, oppIds);
                Map<Id, VF_Contract__c> oppIdToContractMap = new Map<Id, VF_Contract__c>();
                for (VF_Contract__c contract: contractList) {
                    oppIdToContractMap.put(contract.Opportunity__c, contract);
                }
                Database.insert(this.createInitialContractTasks(contractList, oppMap));
                migrateContractAttachments(oppIdToContractMap,oppIds);
            }
        } catch (Exception e) {
            // If anything fails perform a rollback and delete the (already created) Contract by changing the status back to Closing
            Database.rollback(svpt);
            if (contractId != null) {
                List<Opportunity> opps = [SELECT Id, StageName FROM Opportunity WHERE Contract_VF__c = :contractId];
                if (!opps.isEmpty()) {
                    opps[0].StageName = 'Closing';
                    update opps[0];
                }
            }
            stage = 'error';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, System.Label.LABEL_Error_DML + e.getMessage(), ''));
        }
    }

    private List<Task> createInitialContractTasks(List<VF_Contract__c> contracts, Map<Id,Opportunity> opportunityMap) {
        List<Task> taskList = new List<Task>();
        for (VF_Contract__c contract : contracts) {
            if (opportunityMap.get(contract.Opportunity__c).Vodafone_Products__c > 0 && !opportunityMap.get(contract.Opportunity__c).Dummy_Contract__c) {
                String subjectString = Label.Contract_VF_Initial_Task_Subject;
                subjectString += ': '+ opportunityMap.get(contract.Opportunity__c).name;
                subjectString += ' - Account: '+opportunityBanNumber.get(contract.Opportunity__c).Account.name;
                Task newTask = new Task(
                    Subject = subjectString,
                    Description = Label.Contract_VF_Initial_Task_Description,
                    OwnerId = contract.Owner__c,
                    WhatId = contract.Id
                );
                taskList.add(newTask);
            }
        }
        return taskList;
    }

    private void migrateContractAttachments(Map<Id, VF_Contract__c> oppIdToContractMap, Set<Id> oppIds) {
        List<Contract_Attachment__c> caToInsert = new List<Contract_Attachment__c>();
        Map<Id, Opportunity_Attachment__c> oppAttachments = new Map<Id, Opportunity_Attachment__c>([
            SELECT
                Id,
                Name,
                Attachment_Type__c,
                Description_Optional__c,
                Opportunity__c,
                Signed_Contract__c,
                ContentDocumentId__c
            FROM
                Opportunity_Attachment__c
            WHERE
                Opportunity__c IN :oppIds
                AND Attachment_Type__c != 'HBO Attachment'
        ]);
        List<ContentDocumentLink> contentDocumentLinkToInsert = new List<ContentDocumentLink>();
        Map<Id, ContentDocumentLink> oaIdToContentDocumentLink = new Map<Id, ContentDocumentLink>();
        List<FeedItem> feedItemsToInsert = new List<FeedItem>();
        Map<Id,FeedItem> oaIdToFeedItem = new Map<Id,FeedItem>();
        if (!oppAttachments.isEmpty()) {
            for (ContentDocumentLink cdl : [SELECT Id, ContentDocumentId, LinkedEntityId FROM ContentDocumentLink WHERE LinkedEntityId IN :oppAttachments.keySet()]) {
                oaIdToContentDocumentLink.put(cdl.LinkedEntityId, cdl);
            }
            for (FeedItem fi : [SELECT Id, ParentId, ContentFileName, ContentData FROM FeedItem WHERE ParentId IN :oppAttachments.keySet()]) {
                oaIdToFeedItem.put(fi.ParentId,fi);
            }
            for (Opportunity_Attachment__c oa : oppAttachments.values()) {
                Contract_Attachment__c ca = new Contract_Attachment__c(
                    Attachment_Type__c = oa.Attachment_Type__c,
                    Description_Optional__c = oa.Description_Optional__c,
                    Signed_Contract__c = oa.Signed_Contract__c,
                    Contract_VF__c = oppIdToContractMap.get(oa.Opportunity__c).Id,
                    Source_Opportunity_Attachment_Id__c = oa.Id,
                    ContentDocumentId__c = oa.ContentDocumentId__c
                );
                caToInsert.add(ca);
            }
        }
        List<Contract_Attachment__c> hboAttachmentsToCopy = migrateHBOAttachments(oppIdToContractMap, oppIds);
        caToInsert.addAll(hboAttachmentsToCopy);
        insert caToInsert;
        for (Contract_Attachment__c ca : catoInsert) {
            if (oaIdToContentDocumentLink.containsKey(ca.Source_Opportunity_Attachment_Id__c) || ca.Attachment_Type__c == 'HBO Attachment') {
                ContentDocumentLink cdl = new ContentDocumentLink(
                    ContentDocumentId = ca.ContentDocumentId__c,
                    LinkedEntityId = ca.id,
                    ShareType = 'V',
                    Visibility  = 'Allusers'
                );
                contentDocumentLinkToInsert.add(cdl);
            }
            if (oaIdToFeedItem.containsKey(ca.Source_Opportunity_Attachment_Id__c)) {
                FeedItem oppFi = oaIdToFeedItem.get(ca.Source_Opportunity_Attachment_Id__c);
                FeedItem fi = new FeedItem(
                    ParentId = ca.Id,
                    ContentFileName = oppFi.contentFileName,
                    ContentData = oppFi.contentData
                );
                feedItemsToInsert.add(fi);
            }
        }
        delete oppAttachments.values();
        try {
            List<Database.SaveResult> ircontentDocumentLinkToInsert = Database.insert(contentDocumentLinkToInsert, false);
            List<Database.SaveResult> irfeedItemsToInsert = Database.insert(feedItemsToInsert, false);
        } catch(Exception e) {

        }
    }

    private List<Contract_Attachment__c> migrateHBOAttachments(Map<Id, VF_Contract__c> oppIdToContractMap, Set<Id> oppIds) {
        String oppLiQuery = 'SELECT ';
        Map<String,String> oppProductFieldMapping = SyncUtil.fullMapping('OpportunityLineItem -> Contracted_Products__c').get('OpportunityLineItem -> Contracted_Products__c');
        for (String oppLiField : oppProductFieldMapping.values()) {
            oppLiQuery += oppLiField+', ';
        }
        oppLiQuery += ' Opportunity.RecordTypeId, Site_Availability_List__c, PricebookEntry.Product2.OrderType__c, PricebookEntry.Product2.Role__c, PricebookEntry.Product2.Product_Group__c, PricebookEntry.Product2Id, PricebookEntry.ProductCode, PricebookEntry.Product2.Multiplier_Vodacom__c, OpportunityId FROM OpportunityLineItem WHERE OpportunityId IN :oppIds';

        Set<String> availabilityIds = new Set<String>();
        for (List<OpportunityLineItem> lineItems : Database.query(oppLiQuery.unescapeHtml4())) {
            for (OpportunityLineItem lineItem : lineItems) {
                availabilityIds.addAll(getLineItemSitesAvailabilities(lineItem));
            }
        }
        List<Contract_Attachment__c> oppToContractAttachment = new List<Contract_Attachment__c>();
        if (!availabilityIds.isEmpty()) {
            List<HBO__c> hbos = [
                SELECT Id, hbo_opportunity__c,
                    (SELECT Id, LinkedEntityId, ContentDocumentId, ContentDocument.Title FROM ContentDocumentLinks)
                FROM HBO__c
                WHERE hbo_opportunity__c IN :oppIds AND hbo_status__c = 'Approved' AND hbo_postal_check__c IN (
                    SELECT Source_Check_Entry__c FROM Site_Availability__c WHERE Id IN :availabilityIds
                )
            ];
            for (HBO__c hbo : hbos) {
                if (!hbo.ContentDocumentLinks.isEmpty()) {
                    for (ContentDocumentLink cl : hbo.ContentDocumentLinks) {
                        Contract_Attachment__c ca = new Contract_Attachment__c();
                        ca.Attachment_Type__c = 'HBO Attachment';
                        ca.Contract_VF__c = oppIdToContractMap.get(hbo.hbo_opportunity__c).Id;
                        ca.Source_Opportunity_Attachment_Id__c = cl.LinkedEntityId;
                        ca.ContentDocumentId__c = cl.ContentDocumentId;
                        oppToContractAttachment.add(ca);
                    }
                }
            }
        }
        return oppToContractAttachment;
    }
}