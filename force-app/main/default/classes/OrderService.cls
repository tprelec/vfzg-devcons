public with sharing class OrderService {
	public static final String STRPRIVATE = 'Private infra';
	public static final String STRMULTI = 'Multi infra';
	public static final String STRACT = 'Activatie';
	public static final String STRIMP = 'Implementatie';

	private Map<Id, VF_Contract__c> contractMap;
	private Map<Id, List<Contracted_Products__c>> contractedPMap;
	private Set<String> setVFCOInScope;
	private Set<String> setVFCOPrivate;
	private Set<String> setVFCOImp;
	private String mdtTitle;
	public Order retOrder;
	//TO-DO: bulkify method
	public static String getOpportunityNumberWithOrderId(String orderId) {
		Order__c theOrder = [
			SELECT Id, VF_Contract__r.Opportunity__r.Opportunity_Number__c
			FROM Order__c
			WHERE Name = :orderId
		];

		return theOrder.VF_Contract__r.Opportunity__r.Opportunity_Number__c;
	}

	public void initInfoTitle(List<Order__c> ordersInfo) {
		//get metadata parameters
		Automatic_Title_to_BOP__mdt objMDTVFCalling = [
			SELECT
				MasterLabel,
				Developername,
				Product_Codes_in_Scope__c,
				Condition_Codes_1__c,
				Condition_Codes_2__c,
				Result_Title__c
			FROM Automatic_Title_to_BOP__mdt
			WHERE Developername = 'VF_Calling'
		];
		//TO-DO: the trigger needs to updated to at least api version 51 in order to use getInstance
		// Automatic_Title_to_BOP__mdt objMDTVFCalling = Automatic_Title_to_BOP__mdt.getInstance(
		// 	'VF_Calling'
		// );
		mdtTitle = objMDTVFCalling.Result_Title__c;
		setVFCOInScope = stringFieldToSet(objMDTVFCalling.Product_Codes_in_Scope__c);
		setVFCOPrivate = stringFieldToSet(objMDTVFCalling.Condition_Codes_1__c);
		setVFCOImp = stringFieldToSet(objMDTVFCalling.Condition_Codes_2__c);
		// Validate parameters
		if (ordersInfo == null || ordersInfo.isEmpty()) {
			throw new OrderServiceException('Orders not specified.');
		}
		//query Contract info
		Set<Id> contractIds = GeneralUtils.getIDSetFromList(ordersInfo, 'VF_Contract__c');
		contractMap = getContracts(contractIds);
		//query Contract info
		Set<Id> orderIds = GeneralUtils.getIDSetFromList(ordersInfo, 'Id');
		contractedPMap = getContractedProducts(orderIds);
		setVFCallingTitle(ordersInfo);
	}

	private Map<Id, VF_Contract__c> getContracts(Set<Id> contractIds) {
		if (contractIds.isEmpty()) {
			return new Map<Id, VF_Contract__c>();
		}
		return new Map<Id, VF_Contract__c>(
			[SELECT Id, Deal_Type__c FROM VF_Contract__c WHERE Id IN :contractIds]
		);
	}

	private Map<Id, List<Contracted_Products__c>> getContractedProducts(Set<Id> orderIds) {
		Map<Id, List<Contracted_Products__c>> mapReturn = new Map<Id, List<Contracted_Products__c>>();
		if (orderIds.isEmpty()) {
			return mapReturn;
		}
		for (Contracted_Products__c objCP : [
			SELECT Id, ProductCode__c, Site__c, Order__c
			FROM Contracted_Products__c
			WHERE Order__c IN :orderIds
		]) {
			if (mapReturn.containsKey(objCP.Order__c)) {
				mapReturn.get(objCP.Order__c).add(objCP);
			} else {
				mapReturn.put(objCP.Order__c, new List<Contracted_Products__c>{ objCP });
			}
		}
		return mapReturn;
	}

	private void setVFCallingTitle(List<Order__c> ordersInfo) {
		Map<Id, String> titleBopByOrderId = new Map<Id, String>();
		for (Order__c objOrder : ordersInfo) {
			titleBopByOrderId.put(objOrder.Id, executeTitleBOP(objOrder));
		}
		retOrder = new Order(titleBopByOrderId);
	}

	private String executeTitleBOP(Order__c objOrder) {
		List<String> lstParamsTitle = new String[5];
		lstParamsTitle.set(0, OrderService.STRMULTI);
		lstParamsTitle.set(2, contractMap.get(objOrder.VF_Contract__c).Deal_Type__c);
		lstParamsTitle.set(3, objOrder.BOP_Export_Order_Id__c);
		lstParamsTitle.set(4, OrderService.STRACT);

		Boolean blnFoundVFCalling = false;
		Set<String> setLocation = new Set<String>();
		for (Contracted_Products__c cp : contractedPMap.get(objOrder.Id)) {
			if (setVFCOInScope.contains(cp.ProductCode__c)) {
				blnFoundVFCalling = true;
			}
			if (setVFCOPrivate.contains(cp.ProductCode__c)) {
				lstParamsTitle.set(0, OrderService.STRPRIVATE);
			}
			if (setVFCOImp.contains(cp.ProductCode__c)) {
				lstParamsTitle.set(4, OrderService.STRIMP);
			}
			if (cp.Site__c != null) {
				setLocation.add(cp.Site__c);
			}
		}
		lstParamsTitle.set(1, String.valueOf(setLocation.size()));

		if (!blnFoundVFCalling) {
			return null;
		}

		String strTitle = String.format(mdtTitle, lstParamsTitle);
		//removing empty segments of the title
		String regExp = '\\{[a-zA-Z0-9_]+?\\}';
		strTitle = strTitle.replaceAll(regExp, '')
			.replace('/  /', '/')
			.replace('0 Locaties ', 'Geen Locaties');
		//example outcome VCIO / Private / 1 location / Acquisition / O-1234567.1 / Implementatie
		return strTitle;
	}

	private Set<String> stringFieldToSet(String strValue) {
		Set<String> setReturn = new Set<String>(
			strValue != null ? strValue.split(';') : new List<String>()
		);
		return setReturn;
	}

	public class Order {
		public Map<Id, String> titleBopByOrderId;
		public Order(Map<Id, String> titleBopByOrderId) {
			this.titleBopByOrderId = titleBopByOrderId;
		}
	}

	public class OrderServiceException extends Exception {
	}
}