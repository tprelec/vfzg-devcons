public interface IEmailHandler {
	Messaging.SingleEmailMessage handleEmail(EmailService.EmailRequest request);
}
