public with sharing class PP_SearchBasketController {

	// This returns the list of baskets for the opportunity
	@AuraEnabled
	public static List<cscfga__Product_Basket__c> getBaskets(Id opportunityID) {
		return [SELECT id, Name, Basket_Description__c, csbb__Synchronised_With_Opportunity__c, cscfga__Basket_Status__c, Primary__c FROM cscfga__Product_Basket__c where cscfga__Opportunity__c=:opportunityID];
	}	

	// This creates a new basket so that the basket edit VF page has a record to edit
	@AuraEnabled
	public static String createBasket(Id opportunityID) {
		String returnValue = '';
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
		basket.cscfga__Opportunity__c = opportunityID;
		basket.Name = 'New Basket ' + system.now();
        try {
        	insert basket;
			returnValue = basket.Id;
        } catch(Exception e) {
			returnValue = e.getMessage();
			if (returnValue.contains('Oracle')) {
				returnValue = 'oracleError';
			} else {
				returnValue = 'Error';
			}
		}
		
		return returnValue;
	}
    
}