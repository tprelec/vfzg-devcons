public with sharing class EmailTemplateHelper {
	public static final String SERVICES_TABLE_START = '<table border="0" cellspacing="0" cellpadding="0" style="border-collapse:collapse;"><tbody>';
	public static final String SERVICES_TABLE_END = '</tbody></table>';
	public static final String SERVICES_TABLE_ROW_START = '<tr><td valign="top" style="width:206pt;padding:0 5.4pt;"><p style="font-size:14pt; font-family:myfont,Arial; color:rgb(74, 77, 78)"><b><span>';
	public static final String SERVICES_TABLE_ROW_MIDDLE = '</span></b></p></td><td valign="top" style="width:206pt;padding:0 5.4pt;"><p style="font-size:14pt; font-family:myfont,Arial; color:rgb(74, 77, 78)"><b><span>';
	public static final String SERVICES_TABLE_ROW_END = '</span></b></p></td></tr>';
	public static final String SERVICES_FILTERED_ROW_START = '<b>';
	public static final String SERVICES_FILTERED_ROW_END = '</b><br/>';
	public static final String STYLING_TEMPLATE_DEVELOPER_NAME = 'COM_Styling_Template';
	public static final String ORDER_MANAGEMENT_FOLDER_NAME = 'COM Order Management';

	public static String processEmailTemplate(
		EmailTemplate emailTemplate,
		COM_Delivery_Order__c deliveryOrder
	) {
		deliveryOrder = fetchDeliveryOrder(deliveryOrder.Id);
		String mailBody = replaceEmailTemplateHtmlFromMetadata(emailTemplate, deliveryOrder);
		mailBody = replaceEmailTemplateHtmlFromDeliveryOrder(mailBody, deliveryOrder);
		mailBody = styleEmail(mailBody);

		return mailBody;
	}

	@TestVisible
	private static String replaceEmailTemplateHtmlFromMetadata(
		EmailTemplate emailTemplate,
		COM_Delivery_Order__c deliveryOrder
	) {
		List<COM_Delivery_Email_Template__mdt> listOfDeliveryEmailTemplates = [
			SELECT id, Block__c, Content__c, Change_Type__c, COM_Delivery_Component__r.Label
			FROM COM_Delivery_Email_Template__mdt
			WHERE
				Email_Template_Unique_Name__c = :emailTemplate.DeveloperName
				AND Attachment__c = FALSE
		];
		Set<String> unusedTags = new Set<String>();

		String mailBody = emailTemplate.HtmlValue;
		mailBody = mailBody.replace('<![CDATA[', '').replace(']]>', '');

		for (COM_Delivery_Email_Template__mdt template : listOfDeliveryEmailTemplates) {
			if (
				checkForChangeType(template.Change_Type__c, deliveryOrder) &&
				checkForDeliveryComponent(template.COM_Delivery_Component__r.Label, deliveryOrder)
			) {
				mailBody = mailBody.replace(
					'[BLOCK_' +
					template.Block__c +
					']',
					template.Content__c
				);
			} else {
				unusedTags.add(template.Block__c);
			}
		}

		for (String tag : unusedTags) {
			mailBody = mailBody.replace('[BLOCK_' + tag + ']', '');
		}

		return mailBody;
	}

	@TestVisible
	private static String replaceEmailTemplateHtmlFromDeliveryOrder(
		String mailBody,
		COM_Delivery_Order__c deliveryOrder
	) {
		mailBody = mailBody.replace(
			'{DeliveryOrder.Technical_Contact_Name}',
			deliveryOrder.Technical_Contact__c != null
				? deliveryOrder.Technical_Contact__r.Name
				: ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.Installation_Start}',
			deliveryOrder.Installation_Start__c != null
				? deliveryOrder.Installation_Start__c.format()
				: ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.Installation_Start_Time}',
			deliveryOrder.Installation_End__c != null
				? deliveryOrder.Installation_Start__c.format('HH:mm')
				: ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.Installation_End}',
			deliveryOrder.Installation_End__c != null
				? deliveryOrder.Installation_End__c.format()
				: ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.Installation_End_Time}',
			deliveryOrder.Installation_End__c != null
				? deliveryOrder.Installation_End__c.format('HH:mm')
				: ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.Products__c}',
			deliveryOrder.Products__c != null ? deliveryOrder.Products__c : ''
		);
		mailBody = mailBody.replace('{DeliveryOrder.Name}', deliveryOrder.Name);
		mailBody = mailBody.replace(
			'{DeliveryOrder.Installation_Address}',
			deliveryOrder.Site__c != null ? deliveryOrder.Site__r.Name : ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.SiteStreet}',
			deliveryOrder.Site__c != null ? deliveryOrder.Site__r.Site_Street__c : ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.SiteHouseNumber}',
			deliveryOrder.Site__c != null
				? String.valueOf(deliveryOrder.Site__r.Site_House_Number__c)
				: ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.SiteHouseNumberAddition}',
			(deliveryOrder.Site__c != null &&
				deliveryOrder.Site__r.Site_House_Number_Suffix__c != null)
				? deliveryOrder.Site__r.Site_House_Number_Suffix__c
				: ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.SiteZipCode}',
			deliveryOrder.Site__c != null ? deliveryOrder.Site__r.Site_Postal_Code__c : ''
		);
		mailBody = mailBody.replace(
			'{DeliveryOrder.SiteCity}',
			deliveryOrder.Site__c != null ? deliveryOrder.Site__r.Site_City__c : ''
		);
		mailBody = mailBody.replace('{DeliveryOrder.Technical_Details}', '');
		mailBody = mailBody.replace('{DeliveryOrder.Services}', formatServiceData(deliveryOrder));
		mailBody = mailBody.replace(
			'{DeliveryOrder.NewServices}',
			filterServicesByMACDAction(deliveryOrder, 'Add', true)
		);
		mailBody = mailBody.replace('{DeliveryOrder.SoftwareServices}', '');
		mailBody = mailBody.replace('{DeliveryOrder.TerminatedServices}', '');
		mailBody = mailBody.replace(
			'{DeliveryOrder.Termination_Date}',
			deliveryOrder.Termination_Date__c != null
				? deliveryOrder.Termination_Date__c.format()
				: ''
		);

		return mailBody;
	}

	public static String styleEmail(String mailBody) {
		String stylingBody = fetchStylingTemplateHTML();
		if (stylingBody != null) {
			stylingBody = stylingBody.replace('[EMAIL_CONTENT]', mailBody);
		}

		return stylingBody;
	}

	@TestVisible
	private static String fetchStylingTemplateHTML() {
		String result = null;
		List<EmailTemplate> et = [
			SELECT HtmlValue
			FROM EmailTemplate
			WHERE DeveloperName = :STYLING_TEMPLATE_DEVELOPER_NAME
			LIMIT 1
		];

		if (et.Size() != 0) {
			result = et[0].HtmlValue;
		}

		return result;
	}

	@TestVisible
	private static Boolean checkForChangeType(
		String changeType,
		COM_Delivery_Order__c deliveryOrder
	) {
		String opportunityChangeType = deliveryOrder.Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c;
		if (opportunityChangeType == null || opportunityChangeType == 'Add') {
			opportunityChangeType = 'New';
		}

		return changeType == null || changeType == 'All' || changeType == opportunityChangeType;
	}

	@TestVisible
	private static Boolean checkForDeliveryComponent(
		String deliveryComponent,
		COM_Delivery_Order__c deliveryOrder
	) {
		Boolean containsComponent = false;
		if (
			deliveryComponent == null ||
			deliveryOrder.Delivery_Components__c.contains(deliveryComponent)
		) {
			containsComponent = true;
		}
		return containsComponent;
	}

	public static List<Messaging.EmailFileAttachment> fetchTemplateEmailAttachments(
		String emailTemplateName,
		COM_Delivery_Order__c deliveryOrder
	) {
		deliveryOrder = fetchDeliveryOrder(deliveryOrder.Id);
		List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
		List<COM_Delivery_Email_Template__mdt> listOfDeliveryEmailTemplates = [
			SELECT
				id,
				Block__c,
				Content__c,
				Change_Type__c,
				COM_Delivery_Component__c,
				COM_Delivery_Component__r.Label
			FROM COM_Delivery_Email_Template__mdt
			WHERE Email_Template_Unique_Name__c = :emailTemplateName AND Attachment__c = TRUE
		];
		Set<String> attachmentNames = new Set<String>();

		for (COM_Delivery_Email_Template__mdt template : listOfDeliveryEmailTemplates) {
			if (
				checkForChangeType(template.Change_Type__c, deliveryOrder) &&
				checkForDeliveryComponent(template.COM_Delivery_Component__r.Label, deliveryOrder)
			) {
				attachmentNames.add(template.Content__c);
			}
		}

		List<Document> documents = [
			SELECT Name, Id, Type, ContentType, Body
			FROM Document
			WHERE Name IN :attachmentNames
		];
		for (Document doc : documents) {
			Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
			efa.setFileName(doc.Name + '.' + doc.Type);
			efa.setContentType(doc.ContentType);
			efa.setInline(false);
			efa.Body = doc.Body;
			emailAttachments.add(efa);
		}

		return emailAttachments;
	}

	@TestVisible
	private static String formatServiceData(COM_Delivery_Order__c deliveryOrder) {
		String result = SERVICES_TABLE_START;
		for (csord__Service__c serv : deliveryOrder.Services__r) {
			String deltaStatus = 'New';
			if (serv.csordtelcoa__Delta_Status__c == 'Continuing In Subscription') {
				deltaStatus = 'Continuing';
			} else if (serv.csordtelcoa__Delta_Status__c == 'Deleted From Subscription') {
				deltaStatus = 'Removed';
			}

			result +=
				SERVICES_TABLE_ROW_START +
				serv.Name +
				SERVICES_TABLE_ROW_MIDDLE +
				deltaStatus +
				SERVICES_TABLE_ROW_END;
		}
		result += SERVICES_TABLE_END;

		return result;
	}

	@TestVisible
	private static String filterServicesByMACDAction(
		COM_Delivery_Order__c deliveryOrder,
		String macdAction,
		Boolean includeNull
	) {
		String result = '';

		for (csord__Service__c service : deliveryOrder.Services__r) {
			result += SERVICES_FILTERED_ROW_START + service.Name + SERVICES_FILTERED_ROW_END;
		}

		return result;
	}

	public static COM_Delivery_Order__c fetchDeliveryOrder(Id deliveryOrderId) {
		return [
			SELECT
				Id,
				Name,
				Technical_Contact__c,
				Technical_Contact__r.Email,
				Technical_Contact__r.Name,
				Technical_Contact__r.Language__c,
				Site__r.Name,
				Site__r.Site_Street__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_City__c,
				Installation_Start__c,
				Installation_End__c,
				Products__c,
				Delivery_Components__c,
				Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
				Termination_Date__c,
				(SELECT id, Name, csordtelcoa__Delta_Status__c FROM Services__r)
			FROM COM_Delivery_Order__c
			WHERE Id = :deliveryOrderId
			LIMIT 1
		];
	}

	public static String returnBodyIfStylingNeeded(String templateId) {
		String result = null;
		List<EmailTemplate> et = [
			SELECT HtmlValue
			FROM EmailTemplate
			WHERE Id = :templateId AND Folder.Name = :ORDER_MANAGEMENT_FOLDER_NAME
			LIMIT 1
		];

		if (et.Size() != 0) {
			result = et[0].HtmlValue;
		}

		return result;
	}
}
