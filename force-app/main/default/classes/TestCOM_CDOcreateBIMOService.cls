@isTest
public with sharing class TestCOM_CDOcreateBIMOService {
	@TestSetup
	static void setup() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		Account account = CS_DataTest.createAccount('Test Umbrella Account');
		account.CDO_Tenant_Id__c = 'someTenantId';
		insert account;

		Opportunity opportunity = CS_DataTest.createOpportunity(
			account,
			'Test Opp',
			UserInfo.getUserId()
		);
		insert opportunity;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(
			opportunity,
			'Test Basket'
		);
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c productDefinition = CS_DataTest.createProductDefinition(
			'Product Definition'
		);
		productDefinition.RecordTypeId = productDefinitionRecordType;
		productDefinition.Product_Type__c = 'Fixed';
		insert productDefinition;

		cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(
			productDefinition.Id,
			'Test Conf',
			basket.Id
		);
		insert productConfiguration;

		csord__Order__c order = new csord__Order__c(
			Name = 'Order 1',
			csord__Identification__c = 'ID_4568978'
		);
		insert order;

		Site__c testSite = CS_DataTest.createSite(
			'Test Site',
			account,
			'1032AB',
			'Street',
			'City',
			10.0
		);
		testSite.Footprint__c = null;
		insert testSite;

		COM_Delivery_Order__c deliveryOrder = new COM_Delivery_Order__c();
		deliveryOrder.Name = 'Test order';
		deliveryOrder.Order__c = order.Id;
		deliveryOrder.Status__c = 'In progress';
		deliveryOrder.CDO_Service_Order_Request_Id__c = '88888-11111';

		insert deliveryOrder;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(
			productConfiguration.Id
		);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(
			productConfiguration.Id,
			subscription,
			'Service Created'
		);
		service.csord__Identification__c = 'testSubscription';
		service.COM_Delivery_Order__c = deliveryOrder.Id;
		service.csord__Order__c = order.Id;
		service.VFZ_Product_Abbreviation__c = 'ZIPRO';
		service.VFZ_Commercial_Article_Name__c = 'Coax aansluiting 1000/50 Mbps';
		service.VFZ_Commercial_Article_Name__c = 'Coax HFC';
		service.Site__c = testSite.Id;
		insert service;

		csord__Service__c service2 = CS_DataTest.createService(
			productConfiguration.Id,
			subscription,
			'Service Created'
		);
		service2.csord__Identification__c = 'testSubscription2';
		service2.COM_Delivery_Order__c = deliveryOrder.Id;
		service2.csord__Order__c = order.Id;
		service2.VFZ_Product_Abbreviation__c = 'ZIPRO';
		service2.VFZ_Commercial_Article_Name__c = 'Business Internet Standard Fit';
		service2.VFZ_Commercial_Article_Name__c = 'Internet toegang Standard Fit';
		service2.Site__c = testSite.Id;
		insert service2;

		csord__Service__c service3 = CS_DataTest.createService(
			productConfiguration.Id,
			subscription,
			'Service Created'
		);
		service3.csord__Identification__c = 'testSubscription3';
		service3.COM_Delivery_Order__c = deliveryOrder.Id;
		service3.csord__Order__c = order.Id;
		service3.VFZ_Product_Abbreviation__c = 'ZIPRO';
		service3.VFZ_Commercial_Article_Name__c = 'Premium';
		service3.VFZ_Commercial_Article_Name__c = 'Helpdesk';
		service3.Site__c = testSite.Id;
		insert service3;

		csord__Service__c service4 = CS_DataTest.createService(
			productConfiguration.Id,
			subscription,
			'Service Created'
		);
		service4.csord__Identification__c = 'testSubscription4';
		service4.COM_Delivery_Order__c = deliveryOrder.Id;
		service4.csord__Order__c = order.Id;
		service4.VFZ_Product_Abbreviation__c = 'ZIPRO';
		service4.VFZ_Commercial_Article_Name__c = 'Dynamisch IPv4 adres';
		service4.VFZ_Commercial_Article_Name__c = 'IP addressen';
		service4.Site__c = testSite.Id;
		insert service4;
	}

	@isTest
	static void testcreateBIMOSyncSuccessfulRequests() {
		String umbrellaService = '2222-4444-5555';
		String serviceOrderRequest = '2222-4444';
		String mockSyncSuccess =
			'{ "id":"' +
			serviceOrderRequest +
			'", "href":"https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId":"COM", "priority":"1", "description":"Internet Modem Only Service Order", "category":"Internet Modem Only Service", "state":"acknowledged", "orderDate":"2022-02-09T10:37:15.97221Z", "@type":"standard", "orderItem":[ { "id":"1", "action":"add", "state":"acknowledged", "@type":"standard", "service":{ "id":"' +
			umbrellaService +
			'", "serviceCharacteristic":[ { "name":"sourcesystem", "valueType":"string", "value":{ "@type":"string", "sourcesystem":"SF" } }, { "name":"tenantId", "valueType":"string", "value":{ "@type":"string", "tenantId":"Some Tenant Id" } }, { "name":"accessL1PostalCode", "valueType":"string", "value":{ "@type":"string", "accessL1PostalCode":"123456" } }, { "name":"accessL1HouseNumber", "valueType":"integer", "value":{ "@type":"integer", "accessL1HouseNumber":"101" } }, { "name":"accessL1HouseNumberExt", "valueType":"string", "value":{ "@type":"string", "accessL1HouseNumberExt":"9" } }, { "name":"accessL1Article", "valueType":"string", "value":{ "@type":"string", "accessL1Article":"HFC" } }, { "name":"circuitL2Article", "valueType":"string", "value":{ "@type":"string", "circuitL2Article":"int_l2_zz1" } }, { "name":"internetL3Article", "valueType":"string", "value":{ "@type":"string", "internetL3Article":"int_service_l3" } }, { "name":"ipv4BlockIPPool", "valueType":"string", "value":{ "@type":"string", "ipv4BlockIPPool":"/32" } } ], "serviceSpecification":{ "id":"bdab731e-d5de-4135-8849-5c724ea01041", "invariantID":"930d7161-0eb8-444f-8d11-5971812246d5", "version":"1.0.0", "name":"Internet Modem Only Service", "@type":"ServiceSpecification" } } } ] }';
		COM_Delivery_Order__c deliveryOrder = [SELECT Id FROM COM_Delivery_Order__c];
		Id deliveryOrderId = deliveryOrder.Id;

		Test.startTest();
		COM_MockWebService.setTestMockResponse(200, 'OK', mockSyncSuccess);
		COM_CDO_createBIMOService.createBIMOService(deliveryOrderId);
		Test.stopTest();

		deliveryOrder = [
			SELECT Id, CDO_Integration_Status__c, CDO_Service_Order_Request_Id__c
			FROM COM_Delivery_Order__c
			WHERE Id = :deliveryOrderId
		];

		List<csord__Service__c> services = [
			SELECT Id, CDO_Umbrella_Service_Id__c
			FROM csord__Service__c
			WHERE COM_Delivery_Order__c = :deliveryOrderId
		];

		System.assertEquals(
			deliveryOrder.CDO_Service_Order_Request_Id__c,
			serviceOrderRequest,
			'Service order request service not set correctly in sync success BIMO test'
		);

		for (csord__Service__c service : services) {
			System.assertEquals(
				service.CDO_Umbrella_Service_Id__c,
				umbrellaService,
				'Umbrella service not set correctly in Services in sync success BIMO test'
			);
		}
	}

	@isTest
	static void testcreateBIMOSyncFailedRequests() {
		String mockSyncFailed = '{ "code": 40020, "reason": "SERVICE_ORDER_CHARACTERISTICS_VALIDATION_ERROR", "message": "Request validation has failed due the following issue(s) - [ Input parameter accessL1Article violates constraint, Invalid value ]" }';

		COM_Delivery_Order__c deliveryOrder = [SELECT Id FROM COM_Delivery_Order__c];
		Id deliveryOrderId = deliveryOrder.Id;

		Test.startTest();
		COM_MockWebService.setTestMockResponse(200, 'OK', mockSyncFailed);
		COM_CDO_createBIMOService.createBIMOService(deliveryOrderId);
		Test.stopTest();

		deliveryOrder = [
			SELECT Id, CDO_Integration_Status__c
			FROM COM_Delivery_Order__c
			WHERE Id = :deliveryOrderId
		];

		System.assertEquals(
			deliveryOrder.CDO_Integration_Status__c,
			'Failure',
			'Integration status not set correctly in sync success BIMO test'
		);
	}

	@isTest
	static void testcreateBIMOAsyncSuccessRequests() {
		String umbrellaService = '11-22-33-44';
		String serviceOrderRequest = '88888-11111';
		String mockASyncSuccess =
			'{ "eventId": "deff54ee-ba37-4dba-b1c9-1fe1e554a4e1", "eventTime": "2022-02-08T11:30:15.97221Z", "eventType": "ServiceOrderStateChangeNotification", "event": { "serviceOrder": { "id": "' +
			serviceOrderRequest +
			'", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId": "COM", "priority": "1", "description": "Connectivity Service Order", "category": "Connectivity Service", "state": "completed", "orderDate": "2022-02-08T10:37:15.97221Z", "startDate": "2022-02-08T10:37:15.97321Z", "@type": "standard", "orderItem": [ { "id": "1", "action": "add", "state": "completed", "@type": "standard", "service": { "id": "' +
			umbrellaService +
			'", "state": "active", "serviceCharacteristic": [ { "name": "sourcesystem", "valueType": "string", "value": { "@type": "string", "sourcesystem": "COM" } }, { "name": "tenantId", "valueType": "string", "value": { "@type": "string", "tenantId": "1234" } }, { "name": "accessL1PostalCode", "valueType": "string", "value": { "@type": "string", "accessL1PostalCode": "123456" } }, { "name": "accessL1HouseNumber", "valueType": "integer", "value": { "@type": "integer", "accessL1HouseNumber": "101" } }, { "name": "accessL1HouseNumberExt", "valueType": "string", "value": { "@type": "string", "accessL1HouseNumberExt": "9" } }, { "name": "accessL1Article", "valueType": "string", "value": { "@type": "string", "accessL1Article": "HFC" } }, { "name": "circuitL2Article", "valueType": "string", "value": { "@type": "string", "circuitL2Article": "int_l2_zz1" } }, { "name": "internetL3Article", "valueType": "string", "value": { "@type": "string", "internetL3Article": "int_service_l3" } }, { "name": "accessId", "valueType": "string", "value": { "@type": "string", "accessId": "4ed3dece-d5bd-442f-a1f9-687c0a1c5423" } }, { "name": "serviceId", "valueType": "string", "value": { "@type": "string", "serviceId": "4ed3dece-d5bd-442f-a1f9-687c0a1c5424" } } ], "serviceSpecification": { "id": "bdab731e-d5de-4135-8849-5c724ea01041", "invariantID": "930d7161-0eb8-444f-8d11-5971812246d5", "version": "1.0.0", "name": "Business Internet Modem Only Service", "@type": "ServiceSpecification" } } } ] } } }';
		RestRequest request = new RestRequest();
		request.requestUri = '/Salesforce/services/apexrest/notificationListener/*';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(mockASyncSuccess);
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		COM_CDONotificationListener.processNotification();
		Test.stopTest();

		COM_Delivery_Order__c deliveryOrder = [
			SELECT Id, CDO_Integration_Status__c, CDO_Service_Order_Request_Id__c
			FROM COM_Delivery_Order__c
			WHERE CDO_Service_Order_Request_Id__c = :serviceOrderRequest
		];

		System.assertEquals(
			deliveryOrder.CDO_Integration_Status__c,
			'Complete',
			'Integration status not set correctly in async success BIMO test'
		);
	}

	@isTest
	static void testcreateBIMOAsyncFailedRequests() {
		String umbrellaService = '11-22-33-44';
		String serviceOrderRequest = '88888-11111';
		String mockAsyncFailed =
			'{ "eventId": "9577add4-5200-47cb-b787-f6e617b396d9", "eventTime": "2022-02-08T10:45:15.97221Z", "eventType": "ServiceOrderStateChangeNotification", "event": { "serviceOrder": { "id": "' +
			serviceOrderRequest +
			'", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId": "COM", "priority": "1", "description": "Internet Modem Only Service Order", "category": "Internet Modem Only Service", "state": "held", "orderDate": "2022-02-08T10:37:15.97221Z", "startDate": "2022-02-08T10:37:15.97321Z", "@type": "standard", "orderItem": [ { "id": "1", "action": "add", "state": "held", "@type": "standard", "service": { "id": "' +
			umbrellaService +
			'", "state": "inactive", "serviceCharacteristic": [ { "name": "sourcesystem", "valueType": "string", "value": { "@type": "string", "sourcesystem": "SF" } }, { "name": "tenantId", "valueType": "string", "value": { "@type": "string", "tenantId": "Some Tenant Id" } }, { "name": "accessL1PostalCode", "valueType": "string", "value": { "@type": "string", "accessL1PostalCode": "123456" } }, { "name": "accessL1HouseNumber", "valueType": "integer", "value": { "@type": "integer", "accessL1HouseNumber": "101" } }, { "name": "accessL1HouseNumberExt", "valueType": "string", "value": { "@type": "string", "accessL1HouseNumberExt": "9" } }, { "name": "accessL1Article", "valueType": "string", "value": { "@type": "string", "accessL1Article": "HFC" } }, { "name": "circuitL2Article", "valueType": "string", "value": { "@type": "string", "circuitL2Article": "int_l2_zz1" } }, { "name": "internetL3Article", "valueType": "string", "value": { "@type": "string", "internetL3Article": "int_service_l3" } }, { "name": "ipv4BlockIPPool", "valueType": "string", "value": { "@type": "string", "ipv4BlockIPPool": "/32" } } ], "serviceSpecification": { "id": "bdab731e-d5de-4135-8849-5c724ea01041", "invariantID": "930d7161-0eb8-444f-8d11-5971812246d5", "version": "1.0.0", "name": "Internet Modem Only Service", "@type": "ServiceSpecification" } } } ] }, "errorEvents": [ { "code": 510105000, "reason": "EXTERNAL_SYSTEM_ERROR", "message": "A fulfillment activity failed with error-code 5000, error-message some error message." } ] } }';

		RestRequest request = new RestRequest();
		request.requestUri = '/Salesforce/services/apexrest/notificationListener/*';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(mockAsyncFailed);
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		COM_CDONotificationListener.processNotification();
		Test.stopTest();

		COM_Delivery_Order__c deliveryOrder = [
			SELECT Id, CDO_Integration_Status__c
			FROM COM_Delivery_Order__c
			WHERE CDO_Service_Order_Request_Id__c = :serviceOrderRequest
		];

		System.assertEquals(
			deliveryOrder.CDO_Integration_Status__c,
			'Failure',
			'Integration status not set correctly in async success BIMO test'
		);
	}
}
