/**
 * @description    This class contains unit tests for the OrderResponseTriggerHandler class
 * @Author         Guy Clairbois
 */
@isTest
private class TestOrderResponseTriggerHandler {
	private static final String ACCNT_UNIFY_ID = '456',
		CHAMBER_OF_COMMERCE = '12345678',
		BAN_NR = '341324111',
		OPPTY_MARKETING_KEY = 'RANDOMOPP234',
		UNIFY_ORDER_ID = '123456789',
		BOP_ORDER_ID = '123456789',
		SITE_UNIQUE_ID = '123456789',
		CONTACT_EXT_ID = '123456789',
		BILLING_ARRANGEMENT_ID = '123456789',
		BAR_ID = '123456789',
		BAN_BOP_CODE = 'ABC';

	@TestSetup
	static void prepareTests() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.autoCommit = true;
		List<sObject> testDataList = new List<sObject>();

		Account accnt = new Account(
			Name = 'Test Account',
			Mobile_Dealer__c = null,
			Unify_Account_Type__c = 'C',
			Unify_Account_SubType__c = 'CRP',
			Phone = '020-45678 96',
			Frame_Work_Agreement__c = 'VF-0001',
			claimed__c = true,
			Visiting_Country__c = 'NL',
			Unify_Ref_Id__c = ACCNT_UNIFY_ID,
			KVK_number__c = CHAMBER_OF_COMMERCE
		);
		insert accnt;

		Contact cont = new Contact(
			Account = accnt,
			LastName = 'TestLastname',
			FirstName = 'TestFirstname',
			Marketing_Achiever__c = 'Nee',
			Marketing_Status__c = 'Uit dienst',
			Unify_Ref_Id__c = CONTACT_EXT_ID
		);
		insert cont;

		Site__c s = new Site__c();
		//s.Site_Account__r = new Account(KVK_number__c = CHAMBER_OF_COMMERCE);
		s.Site_Account__c = accnt.Id;
		s.Site_Street__c = 'test';
		s.Site_City__c = 'testCity';
		s.Site_Postal_Code__c = '1234AB';
		s.Site_House_Number__c = system.now().millisecond();
		s.Site_House_Number_Suffix__c = 'a';
		s.UniqueId__c = SITE_UNIQUE_ID;
		s.Unify_Ref_Id__c = SITE_UNIQUE_ID;
		insert s;

		/*
        Ban__c b = new Ban__c(
			Account__c = accnt.Id,
            ExternalId__c = BAN_NR,
			Unify_Customer_Type__c = 'C',
			Unify_Customer_SubType__c = 'A',
            Unify_Ref_Id__c = BAN_NR,
			Name = '388888888'
		);
		insert b;
		*/
		Ban__c b = TestUtils.createBan(accnt);
		Financial_Account__c fa = TestUtils.createFinancialAccount(b, cont.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = new Opportunity(
			Name = 'Test Opportunity',
			StageName = 'Closing',
			CloseDate = system.today(),
			Account = accnt,
			Type = 'New Business',
			Bespoke__c = false,
			Pricebook2Id = Test.getStandardPricebookId(),
			//Ban__r = new Ban__c(ExternalId__c = BAN_NR),
			Marketing_Key__c = OPPTY_MARKETING_KEY,
			Ban__c = b.Id,
			Financial_Account__c = fa.Id,
			Billing_Arrangement__c = bar.Id
		);
		insert opp;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		VF_Contract__c contr = new VF_Contract__c(
			Account__r = accnt,
			Opportunity__c = opp.Id,
			Net_Revenues__c = 12345,
			Unify_Order_Id__c = UNIFY_ORDER_ID
		);
		insert contr;

		OrderType__c ot = TestUtils.createOrderType();

		Order__c ord = new Order__c();
		ord.Account__r = accnt;
		ord.VF_Contract__r = contr;
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.id;
		ord.BOP_Order_Id__c = BOP_ORDER_ID;
		ord.Billing_Arrangement__c = bar.Id;
		//insert ord;
	}

	@isTest
	static void testCreateOrder() {
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.disableDlrs = true;
		Account acct = TestUtils.createAccount(null);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contract = TestUtils.createVFContract(acct, opp);
		Order__c order = TestUtils.createOrder(contract);
		order.BOP_Order_Id__c = 'BOP_ORDER_ID';
		update order;

		Test.startTest();

		// customer
		Order_Response__c or3 = new Order_Response__c();
		//or3.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or3.Order__c = order.Id;
		or3.Type__c = 'UnifyAccount';
		or3.Unify_Error_Number__c = 'SIAS000';
		or3.Unify_Account_Id__c = ACCNT_UNIFY_ID;
		or3.Unify_Billing_Customer_Id__c = BAN_NR;
		or3.BAN_Corporate_Id__c = ACCNT_UNIFY_ID;
		or3.Unify_Financial_Account_Id__c = BILLING_ARRANGEMENT_ID;
		or3.Unify_Billing_Arrangement_Id__c = BAR_ID;
		insert or3;

		// bop customer
		Order_Response__c or4 = new Order_Response__c();
		//or4.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or4.Order__c = order.Id;
		or4.Type__c = 'BOPCompany';
		or4.Unify_Billing_Customer_Id__c = BAN_NR;
		or4.BAN_BOPcode__c = BAN_BOP_CODE;
		or4.Unify_Error_Number__c = 'SIAS000';
		insert or4;

		// site
		Order_Response__c or2 = new Order_Response__c();
		//or2.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or2.Order__c = order.Id;
		or2.Type__c = 'UnifySite';
		or2.Unify_Error_Number__c = 'SIAS000';
		or2.SFDC_Site_Id__c = [SELECT Id FROM Site__c WHERE Unify_Ref_Id__c = :SITE_UNIQUE_ID].Id;
		or2.Unify_Site_Id__c = SITE_UNIQUE_ID;
		or2.Unify_Account_Id__c = ACCNT_UNIFY_ID;
		or2.Unify_Billing_Customer_Id__c = BAN_NR;
		or2.BAN_BOPcode__c = BAN_BOP_CODE;
		insert or2;

		// contact
		Order_Response__c or1 = new Order_Response__c();
		//or1.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or1.Order__c = order.Id;
		or1.Type__c = 'UnifyContact';
		or1.Unify_Account_Id__c = ACCNT_UNIFY_ID;
		or1.SFDC_Contact_Id__c = [SELECT Id FROM Contact WHERE Unify_Ref_Id__c = :CONTACT_EXT_ID].Id;
		or1.Unify_Error_Number__c = 'SIAS000';
		or1.Unify_Contact_Id__c = CONTACT_EXT_ID;
		insert or1;

		// start order
		Order_Response__c or5 = new Order_Response__c();
		//or5.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or5.Order__c = order.Id;
		or5.Type__c = 'StartOrder';
		or5.Unify_Error_Number__c = 'SIAS000';
		insert or5;

		// contact - negative
		Order_Response__c or6 = new Order_Response__c();
		//or6.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or6.Order__c = order.Id;
		or6.Type__c = 'UnifyContact';
		or6.Unify_Error_Number__c = 'SIAS001';
		//or6.SFDC_Contact_Id__c = CONTACT_EXT_ID;
		or6.Unify_Contact_Id__c = CONTACT_EXT_ID;
		insert or6;

		// site - negative
		Order_Response__c or7 = new Order_Response__c();
		//or7.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or7.Order__c = order.Id;
		or7.Type__c = 'UnifySite';
		or7.Unify_Error_Number__c = 'SIAS001';
		or7.Unify_Site_Id__c = SITE_UNIQUE_ID;
		or7.Unify_Account_Id__c = ACCNT_UNIFY_ID;
		insert or7;

		// customer - negative
		Order_Response__c or8 = new Order_Response__c();
		//or8.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or8.Order__c = order.Id;
		or8.Type__c = 'UnifyAccount';
		or8.Unify_Error_Number__c = 'SIAS001';
		insert or8;

		// bop customer - negative
		Order_Response__c or9 = new Order_Response__c();
		//or9.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or9.Order__c = order.Id;
		or9.Type__c = 'BOPCompany';
		or9.Unify_Error_Number__c = 'SIAS001';
		insert or9;

		// start order - negative
		Order_Response__c or10 = new Order_Response__c();
		//or10.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or10.Order__c = order.Id;
		or10.Type__c = 'StartOrder';
		or10.Unify_Error_Number__c = 'SIAS001';
		or10.Unify_Account_Id__c = ACCNT_UNIFY_ID;
		insert or10;

		Test.stopTest();

		List<Order_Response__c> orderRespProcessed = [
			SELECT Id, Type__c, Unify_Billing_Customer_Id__c, Unify_Error_Description__c, Unify_Error_Number__c, Processing_result__c
			FROM Order_Response__c
			WHERE Processing_result__c = 'Processed'
		];
		List<Order_Response__c> orderRespNotProcessed = [
			SELECT Id, Type__c, Unify_Billing_Customer_Id__c, Unify_Error_Description__c, Unify_Error_Number__c, Processing_result__c
			FROM Order_Response__c
			WHERE Processing_result__c != 'Processed'
		];

		System.assertEquals(5, orderRespProcessed.size(), 'Expected 5 to be processed, found ' + orderRespProcessed.size() + ' instead');
		System.assertEquals(5, orderRespNotProcessed.size(), 'Expected 5 to not be processed, found ' + orderRespNotProcessed.size() + ' instead');
	}

	@isTest
	static void testBOPOrderCreation() {
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.disableDlrs = true;
		Account acct = TestUtils.createAccount(null);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contract = TestUtils.createVFContract(acct, opp);
		Order__c order = TestUtils.createOrder(contract);
		order.BOP_Order_Id__c = 'BOP_ORDER_ID';
		update order;

		Test.startTest();

		Order_Response__c or12 = new Order_Response__c();
		//or12.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or12.Order__c = order.Id;
		or12.Type__c = 'BopOrderCreation';
		or12.Unify_Error_Number__c = 'SIAS102';
		or12.Unify_Error_Description__c = 'WRONG!';
		insert or12;

		Order_Response__c or13 = new Order_Response__c();
		//or13.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or13.Order__c = order.Id;
		or13.Type__c = 'BopOrderCreation';
		or13.Unify_Error_Number__c = 'SIAS001';
		insert or13;

		Order_Response__c or11 = new Order_Response__c();
		//or11.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or11.Order__c = order.Id;
		or11.Type__c = 'BopOrderCreation';
		or11.Unify_Error_Number__c = '0';
		insert or11;

		Test.stopTest();

		List<Order_Response__c> orderRespProcessed = [SELECT Id FROM Order_Response__c WHERE Processing_result__c = 'Processed'];
		List<Order_Response__c> orderRespNotProcessed = [SELECT Id FROM Order_Response__c WHERE Processing_result__c != 'Processed'];
		System.assertEquals(0, orderRespProcessed.size(), 'Expected 0 to be processed, found ' + orderRespProcessed.size() + ' instead');
		System.assertEquals(3, orderRespNotProcessed.size(), 'Expected 3 to not be processed, found ' + orderRespNotProcessed.size() + ' instead');
	}

	@isTest
	static void testUnifyOrderCreation() {
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.disableDlrs = true;
		Account acct = TestUtils.createAccount(null);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contract = TestUtils.createVFContract(acct, opp);
		Order__c order = TestUtils.createOrder(contract);
		order.BOP_Order_Id__c = 'BOP_ORDER_ID';
		update order;

		Test.startTest();

		Order_Response__c or15 = new Order_Response__c();
		//or15.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or15.Order__c = order.Id;
		or15.Type__c = 'UnifyOrderCreation';
		or15.Unify_Error_Number__c = 'SIAS001';
		insert or15;

		Order_Response__c or16 = new Order_Response__c();
		//or16.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or16.Order__c = order.Id;
		or16.Type__c = 'SendMappingObject';
		or16.Unify_Error_Number__c = 'SIAS000';
		insert or16;

		Order_Response__c or17 = new Order_Response__c();
		//or17.Order__r = new Order__c(BOP_Order_Id__c = BOP_ORDER_ID);
		or17.Order__c = order.Id;
		or17.Type__c = 'SendMappingObject';
		or17.Unify_Error_Number__c = 'SIAS001';
		insert or17;

		Test.stopTest();

		List<Order_Response__c> orderRespProcessed = [SELECT Id FROM Order_Response__c WHERE Processing_result__c = 'Processed'];
		List<Order_Response__c> orderRespNotProcessed = [SELECT Id FROM Order_Response__c WHERE Processing_result__c != 'Processed'];
		System.assertEquals(0, orderRespProcessed.size(), 'Expected 0 to be processed, found ' + orderRespProcessed.size() + ' instead');
		System.assertEquals(3, orderRespNotProcessed.size(), 'Expected 3 to not be processed, found ' + orderRespNotProcessed.size() + ' instead');
	}
}
