@isTest
private class TestBdsRestCreateAccount {

    @isTest
    static void runCreateNewContact() {
        Test.startTest();
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);


        BdsRestCreateAccount.ContactClass cc = new BdsRestCreateAccount.ContactClass();
        cc.email = 'name@example.com';
        cc.firstname = 'John';
        cc.lastname= 'Smith';
        cc.gender = 'Male';
        cc.isprimary = 'True';
        cc.mobilephone = '0623456789';
        cc.phone = '0623456789';
        BdsRestCreateAccount.ContactResult conId = BdsRestCreateAccount.createContact(cc, acct.Id);
        System.assertNotEquals(null, conId.Id);

        Test.stopTest();
    }

    /**
     * Create new Contact, send the same contact again and should receive first 1 Id
     */
    @isTest
    static void runCreateContactAgain() {
        Test.startTest();
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);


        BdsRestCreateAccount.ContactClass cc = new BdsRestCreateAccount.ContactClass();
        cc.email = 'name@example.com';
        cc.firstname = 'John';
        cc.lastname= 'Smith';
        cc.gender = 'Male';
        cc.isprimary = 'True';
        cc.mobilephone = '0123456789';
        cc.phone = '0123456789';
        BdsRestCreateAccount.ContactResult conId = BdsRestCreateAccount.createContact(cc, acct.Id);
		system.debug(conId.errorMsg);
        BdsRestCreateAccount.ContactResult conSecondId = BdsRestCreateAccount.createContact(cc, acct.Id);

        System.assertEquals(conId.Id, conSecondId.Id);

        Test.stopTest();
    }

    @isTest
    static void runValidateMissingKvk() {
        Test.startTest();

        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0123456789';
        conClass.mobilephone = '0123456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        List<BdsResponseClasses.ValidationError> errList = BdsRestCreateAccount.validateValues('', contactList, banList, siteList);
        System.assertEquals(1, errList.size());
        System.assertEquals('kvk', errList[0].field);

        Test.stopTest();
    }

    @isTest
    static void runValidateMissingContact() {
        Test.startTest();

        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        List<BdsResponseClasses.ValidationError> errList = BdsRestCreateAccount.validateValues('01234567', contactList, banList, siteList);
        System.assertEquals(1, errList.size());
        System.assertEquals('contact', errList[0].field);

        Test.stopTest();
    }

    @isTest
    static void runValidateContactIsNull() {
        Test.startTest();

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        List<BdsResponseClasses.ValidationError> errList = BdsRestCreateAccount.validateValues('01234567', null, banList, siteList);
        System.assertEquals(1, errList.size());
        System.assertEquals('contact', errList[0].field);

        Test.stopTest();
    }

    @isTest
    static void runDoPost() {
        Test.startTest();

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;

        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0123456789';
        conClass.mobilephone = '0123456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        Test.stopTest();
    }

    @isTest
    static void runDoPostNoContacts() {
        Test.startTest();


        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';
        update acct;


        List<BdsRestCreateAccount.ContactClass> contactList = new List<BdsRestCreateAccount.ContactClass>();
        BdsRestCreateAccount.ContactClass conClass= new BdsRestCreateAccount.ContactClass();
        conClass.phone = '0123456789';
        conClass.mobilephone = '0123456789';
        conClass.isprimary = 'True';
        conClass.lastname = 'Smith';
        conClass.firstname = 'John';
        conClass.email = 'name@example.com';
        //contactList.add(conClass);

        List<BdsRestCreateAccount.BanClass> banList = new List<BdsRestCreateAccount.BanClass>();
        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        banList.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass siteClass = new BdsRestCreateAccount.SiteClass();
        siteClass.street = 'Mainstreet';
        siteClass.zipcode = '1010AA';
        siteClass.city = 'Amsterdam';
        siteList.add(siteClass);

        BdsRestCreateAccount.doPost('01234567', contactList, banList, siteList);

        Test.stopTest();
    }

    @isTest
    static void testCatchError() {


        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '01234567';

        Site__c s = new Site__c();
		s.Site_Account__c = acct.Id;
		s.Site_Street__c = 'existingSiteStreet';
		s.Site_City__c = 'Testville';
		s.Site_Postal_Code__c = '1234AB';
		s.Site_House_Number__c = 101;
		s.Site_House_Number_Suffix__c = 'a';
        insert s;

        BdsRestCreateAccount.ContactClass cc = new BdsRestCreateAccount.ContactClass();
        cc.email = 'name@example.com';
        cc.firstname = 'John';
        cc.lastname= 'Smith';
        cc.gender = 'Male';
        cc.isprimary = 'True';
        cc.mobilephone = '0623456789';
        cc.phone = '0623456789';
		List<BdsRestCreateAccount.ContactClass> contact = new List<BdsRestCreateAccount.ContactClass>();
        contact.add(cc);

        BdsRestCreateAccount.BanClass banClass = new BdsRestCreateAccount.BanClass();
        banClass.financialcontact = 'name@example.com';
        List<BdsRestCreateAccount.BanClass> ban = new List<BdsRestCreateAccount.BanClass>();
        ban.add(banClass);

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass existingSiteStreet = new BdsRestCreateAccount.SiteClass();
        existingSiteStreet.street = 'existingSiteStreet';
        existingSiteStreet.housenumber = 101;
        existingSiteStreet.housenumberExt = 'a';
        existingSiteStreet.zipcode = '1234AB';
        existingSiteStreet.city = 'Testville';
        siteList.add(existingSiteStreet);
        BdsRestCreateAccount.SiteClass newSiteStreet = new BdsRestCreateAccount.SiteClass();
        newSiteStreet.street = 'test';
        newSiteStreet.housenumber = 30000;
        newSiteStreet.housenumberExt = '';
        newSiteStreet.zipcode = '2345GY';
        newSiteStreet.city = 'Testville';
        siteList.add(newSiteStreet);

        Test.startTest();
            BdsRestCreateAccount.doPost('01234567', contact, ban, siteList);
        Test.stopTest();
    }

    @isTest
    static void runWithExistingAndNewSite() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.KVK_number__c = '78945612';
        update acct;
        Contact contact = TestUtils.createContact(acct);
        Site__c site = TestUtils.createSite(acct);
        site.Site_House_Number__c = 101;
        update site;

        List<BdsRestCreateAccount.SiteClass> siteList = new List<BdsRestCreateAccount.SiteClass>();
        BdsRestCreateAccount.SiteClass existingSiteStreet = new BdsRestCreateAccount.SiteClass();
        existingSiteStreet.street = 'existingSiteStreet';
        existingSiteStreet.housenumber = 101;
        existingSiteStreet.housenumberExt = 'a';
        existingSiteStreet.zipcode = '1234AB';
        existingSiteStreet.city = 'Testville';
        siteList.add(existingSiteStreet);
        BdsRestCreateAccount.SiteClass newSiteStreet = new BdsRestCreateAccount.SiteClass();
        newSiteStreet.street = 'newSiteStreet';
        newSiteStreet.housenumber = 30000;
        newSiteStreet.housenumberExt = '';
        newSiteStreet.zipcode = '2345GY';
        newSiteStreet.city = 'Testville';
        siteList.add(newSiteStreet);

        Test.startTest();
        BdsRestCreateAccount.doPost(acct.KVK_number__c, null, null, siteList);
        Test.stopTest();

        System.assertEquals(2, [SELECT Id FROM Site__c WHERE Site_Account__c = :acct.Id].size());
    }

    @isTest
    static void filterOnExistingSitesForAccount() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Site__c site = TestUtils.createSite(acct);
        site.Site_House_Number__c = 101;
        update site;

        List<Site__c> siteList = new List<Site__c>{
            new Site__c(
                Site_Street__c = 'existingSiteStreet',
                Site_House_Number__c = 101,
                Site_House_Number_Suffix__c = 'a',
                Site_Postal_Code__c = '1234AB'
            ),
            new Site__c(
                Site_Street__c = 'newSiteStreet',
                Site_House_Number__c = 30000,
                Site_House_Number_Suffix__c = '',
                Site_Postal_Code__c = '2345GY'
            )
        };

        System.assertEquals(
            1,
            BdsRestCreateAccount.filterOnExistingSitesForAccount(acct.Id, siteList).size()
        );
    }
}