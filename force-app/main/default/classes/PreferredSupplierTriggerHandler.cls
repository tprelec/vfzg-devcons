public with sharing class PreferredSupplierTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<Preferred_Supplier__c> newProducts = (List<Preferred_Supplier__c>) this.newList;
		Sequence seq = new Sequence('Preferred Supplier');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (Preferred_Supplier__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
