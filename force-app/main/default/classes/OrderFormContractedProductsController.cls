/**
 * @description         This is the controller class for the Contracted Products part of the Order Form. It collects all required data and shows it to the user
 * @author              Jurgen van Westreenne
 */
public with sharing class OrderFormContractedProductsController {

    public Order__c order {get;set;}
    public List<Contracted_Products__c> contractedProducts {get;set;}
    public String contractId{get; private set;}
    public String orderId{get; private set;}
    public Boolean errorFound {get;set;}

    public OrderFormContractedProductsController getApexController() {
        // used for passing the controller between form components
        return this;
    }
    
    public OrderFormContractedProductsController() {
        orderId = ApexPages.currentPage().getParameters().get('orderId');
        
        this.contractedProducts = new List<Contracted_Products__c>();
  
        if(orderId == null){
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'No OrderId provided.'));
            errorFound = true;
        }        
        // fetch the contract details if the order is based on a contract
        if(orderId != null){
            errorFound = retrieveOrder();
            retrieveContractedProducts();
        }        
    }

    /**
     * @description         Retrieve the order data
     */
    public Boolean retrieveOrder(){
        Boolean error = false;
        order = OrderUtils.getOrderDataByContractId(null, new Set<Id>{orderId})[0];
        return error;
    }

    public void retrieveContractedProducts() {
        String contractedProductQueryFormat = 'SELECT {0} FROM Contracted_Products__c WHERE Order__c = \'\'{1}\'\'';
        List<String> fieldsList = getFieldsFromFieldset(
            'Contracted_Products__c',
            'Order_Form_Progress_Step'
        );

        if(String.isNotEmpty(this.orderId) && !fieldsList.isEmpty()) {
            String contractedProductQuery = String.format(contractedProductQueryFormat, new List<String> {
                String.join(fieldsList, ', '),
                this.orderId
            });
            System.debug('@@@@contractedProductQuery: ' + contractedProductQuery);
            this.contractedProducts = Database.query(contractedProductQuery);
        }
    }
    
    private List<String> getFieldsFromFieldset(String objectName, String fieldSetName) {

        List<String> fieldsList = new List<String>();

        List<DescribeSObjectResult> describes = Schema.describeSObjects(new String[] { objectName });

        if(describes != null && describes.size() > 0) {
            Map<String, Schema.FieldSet> mapFieldsets = describes[0].fieldSets.getMap();

            if(mapFieldsets.containsKey(fieldSetName)) {
                Schema.FieldSet fieldset = mapFieldsets.get(fieldSetName); 
                for(Schema.FieldSetMember fieldSetMember: fieldset.getFields()) {
                    fieldsList.add(fieldSetMember.getFieldPath());
                }
            }
        }
        return fieldsList;
    }

    public pageReference refresh(){
        retrieveOrder();
        retrieveContractedProducts();
        return null;    
    }

}