public with sharing class ContractTierGroupWrapper {
	public ContractTierGroupWrapper() {
		
	}

	// the main tier is leading for the type and the initialFee
	public Contract_Tiers__c mainTier {get;set;}	

	public List<Contract_Tiers__c> theTiers {get;set;}

}