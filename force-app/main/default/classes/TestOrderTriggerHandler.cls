/**
 *     @description    This class contains unit tests for the OrderTriggerHandler class
 *    @Author            Guy Clairbois
 */
@isTest
private class TestOrderTriggerHandler {
	@isTest
	static void testCreateOrder() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact con = TestUtils.createContact(acct);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		Test.startTest();
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		Order__c ord = TestUtils.createOrder(contr);
		ord.Propositions__c = 'Legacy';
		ord.BEN_Number__c = '1234';
		ord.Contract_Ready__c = true;
		update ord;
		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}

	@isTest
	static void testUpdateOrder1() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact con = TestUtils.createContact(acct);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		opp.Ban__c = ban.Id;
		update opp;

		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		Order__c ord = TestUtils.createOrder(contr);

		Test.startTest();
		Test.setMock(HttpCalloutMock.class, new TestUtilMultiRequestMock.SingleRequestMock(200, 'Complete', 'Error message', null));
		ord.Propositions__c = 'Legacy';
		ord.Customer_Main_Contact__c = con.Id;
		ord.Status__c = 'Submitted';
		update ord;
		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}

	@isTest
	static void testUpdateOrder2() {
		Test.startTest();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.createOrderValidationOrder();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact con = TestUtils.createContact(acct);
		Ban__c ban = TestUtils.createBan(acct);
		ban.ExternalAccount__c = null;
		update ban;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		Order__c ord = TestUtils.createOrder(contr);
		ord.Export__c = 'BOP';
		ord.Propositions__c = 'Retention';
		ord.Customer_Main_Contact__c = con.Id;
		ord.Status__c = 'Submitted';
		ord.BOP_Order_Status__c = 'Project Created';
		ord.Unify_Customer_Export_Datetime__c = system.now();
		ord.Billing_Arrangement__c = bar.Id;
		update ord;

		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}

	@isTest
	static void testUpdateOrder3() {
		Test.startTest();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.createOrderValidationOrder();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact con = TestUtils.createContact(acct);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		CSPOFA__Orchestration_Process_Template__c orchTemp = new CSPOFA__Orchestration_Process_Template__c(
			Name = 'EMP Order Provisioning',
			CSPOFA__Unique_Name__c = 'EMP Order Provisioning'
		);
		insert orchTemp;

		Order__c ord = TestUtils.createOrder(contr);
		ord.Ordertype__c = null;
		ord.Propositions__c = 'IPVPN';
		ord.Export__c = 'SIAS';
		ord.Customer_Main_Contact__c = con.Id;
		ord.Status__c = 'Submitted';
		ord.BOP_Order_Status__c = 'Project Created';
		ord.Unify_Customer_Export_Datetime__c = system.now();
		ord.EMP_Automated_Mobile_order__c = true;
		update ord;

		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}

	@isTest
	static void testDeleteOrder() {
		Test.startTest();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact con = TestUtils.createContact(acct);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		Order__c ord = TestUtils.createOrder(contr);
		delete ord;

		Test.stopTest();
		System.assert(true, 'dummy assertion');
	}

	@isTest
	public static void testOrderLegacy() {
		Test.startTest();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TestUtils.createOrderValidationOrder();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact con = TestUtils.createContact(acct);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		//create OrderType
		TestUtils.createOrderType();
		//create site
		Site__c site = TestUtils.createSite(acct);

		TestUtils.autoCommit = false;
		List<Product2> lstProdToInsert = new List<Product2>();
		Product2 product = TestUtils.createProduct(new Map<String, Object>{ 'ProductCode' => 'C112312' });
		Product2 product1 = TestUtils.createProduct(new Map<String, Object>{ 'ProductCode' => 'C113309' });
		Product2 productTwo = TestUtils.createProduct(new Map<String, Object>{ 'ProductCode' => 'C113317' });
		lstProdToInsert.add(product);
		lstProdToInsert.add(product1);
		lstProdToInsert.add(productTwo);
		insert lstProdToInsert;
		TestUtils.autoCommit = true;

		Order__c ord = TestUtils.createOrder(contr);

		List<Contracted_Products__c> cpInsert = new List<Contracted_Products__c>();
		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = 1,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Product__c = lstProdToInsert[0].Id,
			VF_Contract__c = contr.Id,
			Site__c = site.Id,
			Order__c = ord.Id,
			Proposition_Component__c = 'Voice; Internet; IPVPN',
			Proposition__c = 'One Net',
			Family_Condition__c = 'ONENETX2014,ONENET2014,ONENET2014SUB30,One Net Enterprise,One Net Express',
			Number_of_sessions__c = 2
		);
		cpInsert.add(cp);
		Contracted_Products__c cp1 = new Contracted_Products__c(
			Quantity__c = 1,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Product__c = lstProdToInsert[1].Id,
			VF_Contract__c = contr.Id,
			Site__c = site.Id,
			Order__c = ord.Id,
			Proposition_Component__c = 'Voice; Internet; IPVPN',
			Proposition__c = 'IPVPN',
			Number_of_sessions__c = 2
		);
		cpInsert.add(cp1);
		Contracted_Products__c cp2 = new Contracted_Products__c(
			Quantity__c = 1,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Product__c = lstProdToInsert[2].Id,
			VF_Contract__c = contr.Id,
			Site__c = site.Id,
			Order__c = ord.Id,
			Proposition_Component__c = 'Voice; Internet; IPVPN',
			Proposition__c = 'One Net',
			Family_Condition__c = 'ONENET2014SUB30,One Net Express',
			Number_of_sessions__c = 2
		);
		cpInsert.add(cp2);

		insert cpInsert;

		ord.Export__c = 'BOP';
		ord.Propositions__c = 'Legacy';
		ord.Customer_Main_Contact__c = con.Id;
		ord.Status__c = 'Clean';
		update ord;

		Test.stopTest();
		//query info to assert
		String strAssertTitle = [SELECT Title__c FROM Order__c WHERE Id = :ord.Id LIMIT 1]?.Title__c;
		System.assertEquals(true, string.isNotEmpty(strAssertTitle), 'validating that the Order gets a title for the VF calling information');
	}

	@isTest
	private static void testEmitOrchestratorUpdateEvent() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', null);
		insert testOpp;

		VF_Contract__c contr = new VF_Contract__c();
		contr.Opportunity__c = testOpp.Id;
		contr.Account__c = testAccount.Id;
		insert contr;

		OrderType__c ot = new OrderType__c();
		ot.Name = 'TestOT2';
		ot.ExportSystem__c = 'BOP';
		ot.Status__c = 'New';
		ot.ExternalID__c = 'Test12';
		insert ot;

		Order__c oldOrd1 = new Order__c();
		oldOrd1.VF_Contract__c = contr.Id;
		oldOrd1.OrderType__c = ot.Id;
		oldOrd1.BEN_Number__c = '66666';
		oldOrd1.Status__c = 'Submitted';
		oldOrd1.ProvisioningProcessStatus__c = 'Error';
		insert oldOrd1;

		Order__c oldOrd2 = new Order__c();
		oldOrd2.VF_Contract__c = contr.Id;
		oldOrd2.OrderType__c = ot.Id;
		oldOrd2.BEN_Number__c = '66666';
		oldOrd2.Status__c = 'Submitted';
		oldOrd2.ProvisioningProcessStatus__c = 'Error';
		insert oldOrd2;

		Order__c newOrd1 = new Order__c();
		newOrd1.Status__c = 'Accepted';
		newOrd1.ProvisioningProcessStatus__c = 'Error';

		Order__c newOrd2 = new Order__c();
		newOrd2.Status__c = 'Submitted';
		newOrd2.ProvisioningProcessStatus__c = 'Failed';

		Map<Id, Order__c> newOrdersMap = new Map<Id, Order__c>{ oldOrd1.Id => newOrd1, oldOrd2.Id => newOrd2 };
		Map<Id, Order__c> oldOrdersMap = new Map<Id, Order__c>{ oldOrd1.Id => oldOrd1, oldOrd2.Id => oldOrd2 };

		Boolean passed = true;
		Test.startTest();
		try {
			OrderTriggerHandler oth = new OrderTriggerHandler();
			oth.emitOrchestratorUpdateEvent(newOrdersMap, oldOrdersMap);
		} catch (Exception e) {
			passed = false;
		}
		Test.stopTest();

		System.assertEquals(true, passed, 'Execution failed.');
	}
}
