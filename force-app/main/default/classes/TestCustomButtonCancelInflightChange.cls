@isTest
public with sharing class TestCustomButtonCancelInflightChange {
	@testSetup
	private static void setup() {
		Account account = CS_DataTest.createAccount('Test Account');
		insert account;

		Opportunity opp = CS_DataTest.createOpportunity(account, 'TestOpportunity', null);

		insert opp;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'TestProductBasket');
		csord__Order__c order = CS_DataTest.createOrder();
		order.csordtelcoa__Participating_in_Inflight_Change__c = true;
		insert order;

		basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
		basket.Primary__c = true;
		basket.csordtelcoa__Order_Under_Change__c = order.Id;
		insert basket;

		cscfga__Product_Definition__c managedInternetDef = CS_DataTest.createProductDefinition('Managed Internet');
		managedInternetDef.Product_Type__c = 'Fixed';

		insert managedInternetDef;

		cscfga__Product_Configuration__c managedInternetConf = CS_DataTest.createProductConfiguration(
			managedInternetDef.Id,
			'Managed Internet',
			basket.Id
		);
		managedInternetConf.cscfga__Root_Configuration__c = null;
		managedInternetConf.cscfga__Parent_Configuration__c = null;
		insert managedInternetConf;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(managedInternetConf.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(order.Id, null, 'TestService', basket.Id, null, null, null, managedInternetConf.Id);
		service.Applicable_for_inflight_change__c = true;
		service.csord__Subscription__c = subscription.Id;
		insert service;

		COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', order.Id, false);
		insert deliveryOrder1;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = deliveryOrder1.Id;
		testProcess.Name = COM_Constants.CDO_CREATE_TENANT_PROCESS_NAME;
		testProcess.CSPOFA__Process_On_Hold__c = true;
		insert testProcess;
	}

	@isTest
	static void customButtonCancelInflightChangeTest() {
		Opportunity opp = [SELECT Id, Name FROM Opportunity WHERE Name = 'TestOpportunity'];
		List<cscfga__Product_Basket__c> baskets = [
			SELECT Id, Name, csbb__Account__c, csordtelcoa__Order_Under_Change__c
			FROM cscfga__Product_Basket__c
			WHERE cscfga__Opportunity__c = :opp.Id
		];
		Test.startTest();
		CustomButtonCancelInflightChange cancelButton = new CustomButtonCancelInflightChange();
		cancelButton.performAction(String.valueOf(baskets[0].Id));
		Test.stopTest();
		csord__Order__c order = [
			SELECT csordtelcoa__Participating_in_Inflight_Change__c, Id
			FROM csord__Order__c
			WHERE Id = :baskets[0].csordtelcoa__Order_Under_Change__c
		];

		csord__Service__c service = [
			SELECT Id, csord__Order__c, Applicable_for_inflight_change__c
			FROM csord__Service__c
			WHERE csord__Order__c = :order.Id
		];

		COM_Delivery_Order__c deliveryOrder1 = [SELECT Id, On_Hold__c FROM COM_Delivery_Order__c WHERE Order__c = :order.Id];

		CSPOFA__Orchestration_Process__c testProcess = [
			SELECT Id, COM_Delivery_Order__c, CSPOFA__Process_On_Hold__c
			FROM CSPOFA__Orchestration_Process__c
			WHERE COM_Delivery_Order__c = :deliveryOrder1.Id
		];

		System.assertEquals(
			false,
			order.csordtelcoa__Participating_in_Inflight_Change__c,
			'Cancel Inflight Change Button should have changed this field to false!'
		);
		System.assertEquals(
			false,
			service.Applicable_for_inflight_change__c,
			'Cancel Inflight Change Button should have changed this field to false!'
		);
		System.assertEquals(false, deliveryOrder1.On_Hold__c, 'Cancel Inflight Change Button should have changed this field to false!');
		System.assertEquals(false, testProcess.CSPOFA__Process_On_Hold__c, 'Cancel Inflight Change Button should have changed this field to false!');
	}
}
