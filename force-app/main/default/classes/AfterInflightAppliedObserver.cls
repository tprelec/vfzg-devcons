global with sharing class AfterInflightAppliedObserver implements csordcb.ObserverApi.IObserver {
	global void execute(csordcb.ObserverApi.Observable o, Object arg) {
		csordtelcoa.InflightObservable observable = (csordtelcoa.InflightObservable) o;
		Map<Id, Id> ordIdByBasketId = observable.getOrderIdByBasketIdMap();
		List<csord__Order__c> orderToUpdate = new List<csord__Order__c>();
		for (String orderId : ordIdByBasketId.values()) {
			orderToUpdate.add(new csord__Order__c(Id = orderId, Inflight_Change_Applied__c = true));
		}
		System.debug('orderToUpdate in AfterInflightAppliedObserver' + orderToUpdate);
		update orderToUpdate;

		cssmgnt.API_1.mergeInflightSolutions(ordIdByBasketId);
	}
}
