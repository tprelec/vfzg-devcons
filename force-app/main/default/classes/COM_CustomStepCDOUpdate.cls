@SuppressWarnings('PMD.AvoidGlobalModifier')
global without sharing class COM_CustomStepCDOUpdate implements CSPOFA.ExecutionHandler {
	public List<SObject> process(List<sObject> steps) {
		List<SObject> result = new List<sObject>();
		List<CSPOFA__Orchestration_Step__c> stepList = CS_OrchestratorStepUtility.getStepList(steps);

		try {
			updateDeliveryOrdersForCDO(stepList);

			for (CSPOFA__Orchestration_Step__c step : stepList) {
				result.add(CS_OrchestratorStepUtility.setStepToComplete(step));
			}
		} catch (Exception ex) {
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				result.add(CS_OrchestratorStepUtility.setStepToError(step, ex));
			}
		}

		return result;
	}

	@TestVisible
	private static void updateDeliveryOrdersForCDO(List<CSPOFA__Orchestration_Step__c> stepList) {
		Map<Id, String> processNameByDeliveryOrderId = new Map<Id, String>();
		for (CSPOFA__Orchestration_Step__c step : stepList) {
			processNameByDeliveryOrderId.put(step.CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c, step.CSPOFA__Orchestration_Process__r.Name);
		}

		Map<Id, COM_Delivery_Order__c> deliveryOrders = new Map<Id, COM_Delivery_Order__c>(
			[
				SELECT Id, CDO_Mock_Option__c, CDO_Integration_Status__c, CDO_Error_Message__c, Account__c
				FROM COM_Delivery_Order__c
				WHERE Id IN :processNameByDeliveryOrderId.keySet()
			]
		);

		Set<Id> deliveryOrderIdsForServiceUpdate = new Set<Id>();

		for (COM_Delivery_Order__c delOrd : deliveryOrders.values()) {
			if (delOrd.CDO_Integration_Status__c == COM_Constants.CDO_INTEGRATION_STATUS_COMPLETE) {
				String processName = processNameByDeliveryOrderId.get(delOrd.Id);
				if (processName != COM_Constants.CDO_CREATE_TENANT_PROCESS_NAME) {
					deliveryOrderIdsForServiceUpdate.add(delOrd.Id);
				}
			}
		}

		if (!deliveryOrderIdsForServiceUpdate.isEmpty()) {
			updateServices(deliveryOrderIdsForServiceUpdate, processNameByDeliveryOrderId);
		}
	}

	@TestVisible
	private static void updateServices(Set<Id> deliveryOrderIdsForServiceUpdate, Map<Id, String> processNameByDeliveryOrderId) {
		List<csord__Service__c> services = [
			SELECT
				Id,
				CDO_Umbrella_Service_Id__c,
				Installation_Required__c,
				COM_Delivery_Order__c,
				COM_Delivery_Order__r.Termination_Date__c,
				COM_Delivery_Order__r.MACD_Type__c,
				Delivery_Components__c,
				OSS10_Service_Id__c,
				csordtelcoa__Cancelled_By_Change_Process__c
			FROM csord__Service__c
			WHERE COM_Delivery_Order__c IN :deliveryOrderIdsForServiceUpdate AND Provisioning_Required__c = TRUE
			//AND Has_Delivery_Components__c = TRUE
		];

		for (csord__Service__c service : services) {
			String processName = processNameByDeliveryOrderId.get(service.COM_Delivery_Order__c);
			if (processName == COM_Constants.CDO_PROVISION_PROCESS_NAME) {
				service = setStatusInformation(service);
			} else if (processName == COM_Constants.CDO_SUSPEND_PROCESS_NAME) {
				service.csord__Status__c = COM_Constants.CDO_STATUS_SUSPENDED;
			} else if (processName == COM_Constants.CDO_DEPROVISION_PROCESS_NAME) {
				service = setStatusInformation(service);
			}
		}

		update services;
	}

	@TestVisible
	private static csord__Service__c setStatusInformation(csord__Service__c service) {
		if (service.COM_Delivery_Order__r.MACD_Type__c == 'Termination' && service.csordtelcoa__Cancelled_By_Change_Process__c == true) {
			service.csord__Status__c = COM_Constants.CDO_STATUS_DEPROVISIONED;
			service.Deprovisioned_Date__c = service.COM_Delivery_Order__r.Termination_Date__c;
		} else if (service.COM_Delivery_Order__r.MACD_Type__c == COM_Constants.DEFAULT_MACD_TYPE) {
			if (service.Installation_Required__c && service.csordtelcoa__Cancelled_By_Change_Process__c == false) {
				service.csord__Status__c = COM_Constants.CDO_STATUS_PROVISIONED;
			} else if (service.Installation_Required__c == false) {
				service.csord__Status__c = COM_Constants.CDO_STATUS_IMPLEMENTED;
				service.Implemented_Date__c = System.now();
			}
		}

		return service;
	}
}
