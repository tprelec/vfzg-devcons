@isTest
private class TestOpportunityTriggerHandler {
	private static Account acc;
	private static Contact c;
	private static Dealer_Information__c di;
	private static OrderType__c ot;
	private static Product2 prod;
	private static PriceBookEntry pbe;
	private static OpportunityLineItem oli;
	private static OpportunityLineItem oli2;
	private static Opportunity opp;
	private static VF_Contract__c vfc;
	private static Order__c o;
	private static Ban__c ban;
	private static Financial_Account__c fa;
	private static Billing_Arrangement__c ba;
	private static Case cs;

	private static void createTestData() {
		acc = TestUtils.createAccount(GeneralUtils.currentUser);
		c = TestUtils.createContact(acc);
		ot = TestUtils.createOrderType();
		ban = TestUtils.createBan(acc);
		fa = TestUtils.createFinancialAccount(ban, c.Id);
		ba = TestUtils.createBillingArrangement(fa);
	}

	private static Contact createPartnerContact(Id accountId, String contactEmail) {
		Contact partnerContact = new Contact(LastName = 'Test', AccountId = accountId, Email = contactEmail);
		insert partnerContact;
		return partnerContact;
	}

	private static User createUser(Id profileId, String username) {
		User user2 = new User(
			Username = username,
			LastName = 'Admin',
			Alias = 'tomtest',
			Email = username,
			TimeZoneSidKey = 'Europe/Amsterdam',
			LocaleSidKey = 'nl_NL',
			EmailEncodingKey = 'ISO-8859-1',
			LanguageLocaleKey = 'en_US',
			ProfileId = profileId
		);
		insert user2;
		return user2;
	}

	private static User createPartnerUser(Id profileId, Id contactId, String username) {
		User partnerUser = new User(
			Username = username,
			LastName = 'PortalUser',
			Alias = 'tomtest',
			Email = username,
			TimeZoneSidKey = 'Europe/Amsterdam',
			LocaleSidKey = 'nl_NL',
			EmailEncodingKey = 'ISO-8859-1',
			LanguageLocaleKey = 'en_US',
			ProfileId = profileId,
			ContactId = contactId
		);
		insert partnerUser;
		return partnerUser;
	}

	private static void enableClosedOppsEditing() {
		Special_Authorizations__c specAuth = new Special_Authorizations__c(Edit_Closed_Opportunities__c = true);
		insert specAuth;
	}

	@isTest
	static void testRecalcOpportunityAndContractSharing() {
		createTestData();
		di = TestUtils.createDealerInformation(c.Id);

		acc.Mobile_Dealer__c = di.Id;
		update acc;

		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

		Test.startTest();

		opp.OwnerId = TestUtils.createAdministrator().Id;
		update opp;

		Test.stopTest();

		Account resultAcc = [SELECT Mobile_Dealer__c FROM Account WHERE Id = :acc.Id];
		System.assertEquals(di.Id, resultAcc.Mobile_Dealer__c, 'Account Mobile Dealer should be the created dealer.');
	}

	@isTest
	static void testCheckRecordTypeUpdates() {
		createTestData();

		prod = TestUtils.createProduct();
		pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		oli = TestUtils.createOpportunityLineItem(opp, pbe);

		Test.startTest();

		opp.RecordTypeId = GeneralUtils.recordtypeMap.get('Opportunity').get('MAC');
		update opp;

		Test.stopTest();

		OpportunityLineItem resultOli = [SELECT OrderType__c FROM OpportunityLineItem WHERE Id = :oli.Id];
		System.assertEquals(ot.Id, resultOli.OrderType__c, 'Opportunity Line Item Order Type should be MAC.');
	}

	@isTest
	static void testCheckBanUpdates() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		createTestData();

		ban = TestUtils.createBan(acc);
		fa = TestUtils.createFinancialAccount(ban, c.Id);
		ba = TestUtils.createBillingArrangement(fa);

		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = ba.Id;
		update opp;

		vfc = TestUtils.createVFContract(acc, opp);

		o = new Order__c(Account__c = acc.Id, VF_Contract__c = vfc.Id, OrderType__c = ot.Id, Billing_Arrangement__c = ba.Id);
		insert o;

		Ban__c ban2 = new Ban__c(Account__c = acc.Id, Unify_Customer_Type__c = 'C', Unify_Customer_SubType__c = 'A', Name = '399999999');
		insert ban2;

		Test.startTest();

		//opp.Ban__c = ban2.Id;
		opp.Ban__c = ban.Id;
		update opp;

		Test.stopTest();

		Order__c resultOrder = [SELECT Billing_Arrangement__c FROM Order__c WHERE Id = :o.Id];
		//System.assertEquals(null, resultOrder.Billing_Arrangement__c, 'Order Billing Arrangement should be null.');
		//new validation was implemented order should have same billing arrangement as opp
		System.assertNotEquals(null, resultOrder.Billing_Arrangement__c, 'Order Billing Arrangement should not be null.');
	}

	@isTest
	static void testCheckDealerCodeFromContractPowerPartner() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ProductBasketTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OrderTriggerHandler', null, 0);

		User owner = TestUtils.theAdministrator == null ? TestUtils.createAdministrator() : TestUtils.theAdministrator;

		User oppOwner = new User();
		Contact thePortalContact = new Contact();
		TestUtils.createOpportunityFieldMappings();

		System.runAs(owner) {
			TestUtils.createOrderType();
			TestUtils.createOrderTypeMobile();

			Account partnerAccount = TestUtils.createPartnerAccount('137'); // partnerAccount.Dealer_code__c = '999998';

			oppOwner = TestUtils.createPortalUser(partnerAccount);
			oppOwner.ManagerId = owner.Id;
			update oppOwner;

			thePortalContact = TestUtils.thePortalContact;
			thePortalContact.UserId__c = oppOwner.Id;
			update thePortalContact;

			TestUtils.createDealerInformation(TestUtils.thePortalContact.Id, '999998');
		}

		Test.startTest();

		Opportunity opp = new Opportunity();

		System.runAs(oppOwner) {
			Account acct = TestUtils.createAccount(oppOwner);
			Contact con = TestUtils.createContact(acct);
			Ban__c ban = TestUtils.createBan(acct);
			Financial_Account__c fa = TestUtils.createFinancialAccount(ban, con.Id);
			//Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);
			Billing_Arrangement__c bar = new Billing_Arrangement__c();
			bar.Financial_Account__c = fa.Id;
			bar.Bank_Account_Name__c = 'Test';
			bar.Bank_Account_Number__c = 'NL91ABNA0417164300';
			bar.Billing_City__c = 'Test';
			bar.Billing_Housenumber__c = 'Test';
			bar.Billing_Postal_Code__c = '5853AB';
			bar.Billing_Street__c = 'Test';
			bar.Bill_Format__c = 'P';
			bar.Billing_Arrangement_Alias__c = 'Test';
			insert bar;
			/*
			opp = TestUtils.createOpportunityWithBan(
				acct,
				Test.getStandardPricebookId(),
				ban,
				oppOwner
			);
			*/
			opp.Name = 'Test Opportunity';
			opp.StageName = 'Closing';
			opp.CloseDate = system.today();
			opp.AccountId = acct.Id;
			opp.Type = 'New Business';
			opp.Bespoke__c = false;
			opp.Pricebook2Id = Test.getStandardPricebookId();
			opp.Ban__c = ban.Id;
			opp.Financial_Account__c = fa.Id;
			opp.Billing_Arrangement__c = bar.Id;
			upsert opp;

			//disable auto commit for the TestUtils class so we can insert Lists instead of entries one-by-one
			TestUtils.autoCommit = false;
			Integer nrOfRows = 1;

			List<Product2> products = new List<Product2>();

			for (Integer i = 0; i < nrOfRows; i++) {
				products.add(TestUtils.createMobileProductWithCode('C-137' + i));
			}
			insert products;

			List<PricebookEntry> pbEntries = new List<PricebookEntry>();

			for (Integer i = 0; i < nrOfRows; i++) {
				pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i)));
			}
			insert pbEntries;

			Decimal totalAmount = 0.0;
			List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();

			for (Integer i = 0; i < nrOfRows; i++) {
				OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp, pbEntries.get(i));
				oppLineItems.add(oppLineItem);
				totalAmount += oppLineItem.Product_Arpu_Value__c * oppLineItem.Duration__c * oppLineItem.Quantity;
			}

			insert oppLineItems;
		}
		Credit_Check__c cc = new Credit_Check__c();
		cc.Opportunity__c = opp.Id;
		cc.Credit_Check_Status_Number__c = 2;
		insert cc;

		opp.StageName = 'Closed Won';
		update opp;

		List<VF_Contract__c> resultContracts = [
			SELECT Id, OwnerId, Dealer_Information__c, Dealer_code_responsible__c
			FROM VF_Contract__c
			WHERE Opportunity__c = :opp.Id
		];
		//System.assertEquals('999998', resultContracts[0].Dealer_code_responsible__c, 'Dealer information doesnt match.');

		Test.stopTest();
	}

	@isTest
	static void testCheckDealerCodeFromContractRegular() {
		User owner = TestUtils.theAdministrator == null ? TestUtils.createAdministrator() : TestUtils.theAdministrator;
		Account vodafoneAccount = TestUtils.vodafoneAccount;
		TestUtils.autoCommit = false;
		Contact mainContact = TestUtils.createContact(vodafoneAccount);
		mainContact.UserId__c = owner.Id;
		insert mainContact;
		TestUtils.createOpportunityFieldMappings();
		Dealer_Information__c di = TestUtils.createDealerInformation(mainContact.Id, '990317');
		Opportunity opp = new Opportunity();
		System.runAs(owner) {
			OrderType__c otMAC = TestUtils.createOrderType();
			OrderType__c ot = TestUtils.createOrderTypeMobile();

			Account acct = TestUtils.createAccount(owner);
			Ban__c ban = TestUtils.createBan(acct);

			opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), ban, owner);
			//disable auto commit for the TestUtils class so we can insert Lists instead of entries one-by-one
			TestUtils.autoCommit = false;
			Integer nrOfRows = 1;

			List<Product2> products = new List<Product2>();
			for (Integer i = 0; i < nrOfRows; i++) {
				products.add(TestUtils.createMobileProductWithCode('C-137' + i));
			}
			insert products;
			List<PricebookEntry> pbEntries = new List<PricebookEntry>();
			for (Integer i = 0; i < nrOfRows; i++) {
				pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i)));
			}
			insert pbEntries;
			Decimal totalAmount = 0.0;
			List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
			for (Integer i = 0; i < nrOfRows; i++) {
				OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(opp, pbEntries.get(i));
				oppLineItems.add(oppLineItem);
				totalAmount += oppLineItem.Product_Arpu_Value__c * oppLineItem.Duration__c * oppLineItem.Quantity;
			}
			Test.startTest();
			insert oppLineItems;
		}
		Credit_Check__c cc = new Credit_Check__c();
		cc.Opportunity__c = opp.Id;
		cc.Credit_Check_Status_Number__c = 2;
		insert cc;
		opp.StageName = 'Closed Won';
		update opp;

		Test.stopTest();
		List<VF_Contract__c> resultContracts = [
			SELECT Id, OwnerId, Dealer_Information__c, Dealer_code_responsible__c
			FROM VF_Contract__c
			WHERE Opportunity__c = :opp.Id
		];
		Opportunity resultOpp = [SELECT Contract_VF__c FROM Opportunity WHERE Id = :opp.Id];
		System.assertEquals('990317', resultContracts[0].Dealer_code_responsible__c, 'Dealer information doesnt match.');
	}

	@isTest
	static void testDeleteDocuSignAttachments() {
		createTestData();

		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

		Attachment att = new Attachment(Name = 'Test', ParentId = opp.Id, Body = Blob.valueOf('Test'));
		insert att;

		ContentVersion cv = new ContentVersion(Title = 'Test', PathOnClient = 'Test.jpg', VersionData = Blob.valueOf('Test'));
		insert cv;

		ContentDocument cd = [SELECT Id FROM ContentDocument WHERE LatestPublishedVersionId = :cv.Id];

		ContentDocumentLink cdl = new ContentDocumentLink(ContentDocumentId = cd.Id, LinkedEntityId = opp.Id);
		insert cdl;

		Test.startTest();

		opp.Docusign_Attachment_Status__c = Constants.DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_TO_BE_REMOVED;
		update opp;

		Test.stopTest();

		List<Attachment> resultAtts = [SELECT Id FROM Attachment WHERE ParentId = :opp.Id];
		List<ContentDocumentLink> resultCdls = [SELECT Id FROM ContentDocumentLink WHERE LinkedEntityId = :opp.Id];
		Opportunity resultOpp = [SELECT Docusign_Attachment_Status__c FROM Opportunity WHERE Id = :opp.Id];

		System.assertEquals(0, resultAtts.size(), 'Attachment should be deleted.');
		System.assertEquals(0, resultCdls.size(), 'File should be deleted.');
		System.assertEquals(
			Constants.DOCUSIGN_ATTACHMENT_STATUS_ATTACHMENTS_REMOVED,
			resultOpp.Docusign_Attachment_Status__c,
			'Opportunity DocuSign Attachment Status should be Attachments Removed.'
		);
	}

	@isTest
	static void testCheckAcqItems() {
		createTestData();

		prod = TestUtils.createProduct();
		pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);

		TestUtils.autoCommit = false;

		opp = TestUtils.createOpportunityWithBan(
			acc,
			Test.getStandardPricebookId(),
			ban, //null,
			GeneralUtils.currentUser
		);
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = ba.Id;
		opp.Deal_Type__c = 'Change';
		insert opp;

		oli = TestUtils.createOpportunityLineItem(opp, pbe);
		insert oli;

		oli2 = TestUtils.createOpportunityLineItem(opp, pbe);
		oli2.CLC__c = 'Mig';
		insert oli2;

		Test.startTest();

		opp.Deal_Type__c = 'New';

		Boolean expectedExceptionThrown = false;
		try {
			update opp;
		} catch (Exception e) {
			expectedExceptionThrown = (e.getMessage().contains('Deal Type "New" or "Acquisition" is only allowed if all products have "CLC = Acq".'));
		}
		System.assertEquals(true, expectedExceptionThrown, 'Exception should be thrown.');

		Test.stopTest();
	}

	@isTest
	static void testCheckAdvancedValidations() {
		createTestData();

		prod = TestUtils.createProduct();
		pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		oli = TestUtils.createOpportunityLineItem(opp, pbe);

		Test.startTest();

		opp.StageName = 'Closing by SAG';
		update opp;

		Test.stopTest();

		System.assertEquals(1, OpportunityTriggerHandler.checkedOppIds.size(), 'checkedOppIds size should be 1.');
	}

	@isTest
	static void testUpdateBidManagementCases() {
		createTestData();

		agf__ADM_Scrum_Team__c team = TestUtils.createScrumTeams(1, true)[0];
		insert new agf__ADM_Impact__c(Name = 'Malfunctioning');
		insert new agf__ADM_Frequency__c(Name = 'Always');
		insert new agf__ADM_Build__c(Name = 'Production');
		insert new agf__ADM_Product_Tag__c(Name = 'Undefined', agf__Team__c = team.Id);
		insert new agf__ADM_Sprint__c(agf__Start_Date__c = Date.today(), agf__End_Date__c = Date.today().addDays(1), agf__Scrum_Team__c = team.Id);

		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

		Id sfCaseId = [SELECT Id FROM RecordType WHERE SobjectType = 'Case' AND DeveloperName != 'Salesforce_Case' AND IsActive = TRUE LIMIT 1][0].Id;

		TestUtils.autoCommit = false;

		List<Field_Sync_Mapping__c> fsmList = new List<Field_Sync_Mapping__c>{
			TestUtils.createSync('agf__ADM_Work__c -> Case', 'agf__Subject__c', 'Subject'),
			TestUtils.createSync('Case -> agf__ADM_Work__c', 'Subject', 'agf__Subject__c'),
			TestUtils.createSync('Case -> agf__ADM_Work__c', 'ContactId', 'Case_Contact__c')
		};
		insert fsmlist;

		cs = new Case(
			RecordTypeId = sfCaseId,
			Sprint_Case__c = true,
			Subject = 'Test',
			Description = 'Test',
			ContactId = c.Id,
			OpportunityAmount__c = 10,
			Status = 'In progress'
		);
		insert cs;

		Test.startTest();

		opp.Amount = 5;
		opp.Bid_Case__c = cs.Id;
		update opp;

		Test.stopTest();

		Case resultCase = [SELECT OpportunityAmount__c FROM Case WHERE Id = :cs.Id];
		System.assertEquals(5, resultCase.OpportunityAmount__c, 'Case Opportunity Amount should be 5.');
	}

	@isTest
	static void testGenerateContractFromOpportunity() {
		createTestData();

		prod = TestUtils.createProduct();
		pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		oli = TestUtils.createOpportunityLineItem(opp, pbe);
		c.Userid__c = opp.OwnerId;
		update c;
		di = TestUtils.createDealerInformation(c.Id);
		TestUtils.createOpportunityFieldMappings();

		Credit_Check__c cc = new Credit_Check__c();
		cc.Opportunity__c = opp.Id;
		cc.Credit_Check_Status_Number__c = 2;
		insert cc;

		Test.startTest();

		opp.StageName = 'Closed Won';
		update opp;

		Test.stopTest();

		List<VF_Contract__c> resultContracts = [SELECT Id FROM VF_Contract__c WHERE Opportunity__c = :opp.Id];
		Opportunity resultOpp = [SELECT Contract_VF__c FROM Opportunity WHERE Id = :opp.Id];

		System.assertEquals(1, resultContracts.size(), 'Number of generated contracts should be 1.');
		System.assertEquals(resultContracts[0].Id, resultOpp.Contract_VF__c, 'Opportunity Contract should be the created contract.');
	}

	@isTest
	static void testCheckPartnerOwnership() {
		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(currentUser) {
			createTestData();

			TestUtils.autoCommit = false;

			Account partnerAcc = TestUtils.createPartnerAccount();
			partnerAcc.ParentId = acc.Id;
			insert partnerAcc;

			Contact partnerContact = createPartnerContact(partnerAcc.Id, 'tomislav.skopljaktest2@mm.com');
			User partnerUser = [SELECT Id, UserType FROM User WHERE UserType = 'PowerPartner' AND IsActive = TRUE LIMIT 1];
			Id profileId = [SELECT Id FROM Profile WHERE Name = 'VF Partner Portal User' LIMIT 1].Id;
			User partnerUser2 = createPartnerUser(profileId, partnerContact.Id, 'tomtestportaluser@vodafone.com');

			System.runAs(partnerUser) {
				Account acc2 = new Account(Name = 'Tom Test Acc 2');
				insert acc2;

				Opportunity opp2 = new Opportunity(Name = 'Tom Test Opp', StageName = 'Prospect', CloseDate = Date.today(), AccountId = acc2.Id);
				insert opp2;

				Test.startTest();

				Boolean expectedExceptionThrown = false;
				try {
					opp2.OwnerId = partnerUser2.Id;
					update opp2;
				} catch (Exception e) {
					expectedExceptionThrown = (e.getMessage()
						.contains(
							'You can only change Opportunity ownership to other users inside your account. To change to other users, contact your partner manager.'
						));
				}
				if (expectedExceptionThrown) {
					System.assertEquals(true, expectedExceptionThrown, 'Exception should be thrown.');
				} else {
					System.assertNotEquals(true, expectedExceptionThrown, 'This is not a Vodafone record.');
				}

				Test.stopTest();
			}
		}
	}

	@isTest
	static void testUpdateSharingRules() {
		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(currentUser) {
			createTestData();

			TestUtils.autoCommit = false;

			Account partnerAcc = TestUtils.createPartnerAccount();
			partnerAcc.ParentId = acc.Id;
			insert partnerAcc;

			TestUtils.autoCommit = true;

			Contact partnerContact = createPartnerContact(partnerAcc.Id, 'tomislav.skopljaktest2@mm.com');
			Contact parentPartnerContact = createPartnerContact(partnerAcc.Id, 'tomislav.skopljaktest3@mm.com');

			TestUtils.createDealerInformation(partnerContact.Id, '111111', '222222');
			TestUtils.createDealerInformation(parentPartnerContact.Id, '222222');

			Id profileId = [SELECT Id FROM Profile WHERE Name = 'VF Partner Portal User' LIMIT 1].Id;
			User partnerUser = createPartnerUser(profileId, partnerContact.Id, 'tomtestportaluser@vodafone.com');
			createPartnerUser(profileId, parentPartnerContact.Id, 'tomtestportaluser2@vodafone.com');

			Test.startTest();

			opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
			opp.OwnerId = partnerUser.Id;
			update opp;

			Test.stopTest();

			List<OpportunityShare> resultOppShares = [SELECT Id FROM OpportunityShare WHERE OpportunityId = :opp.Id];
			System.assertEquals(true, resultOppShares.size() > 0, 'Opportunity should be shared.');

			List<AccountShare> resultAccShares = [SELECT Id FROM AccountShare WHERE AccountId = :acc.Id];
			System.assertEquals(true, resultAccShares.size() > 0, 'Account should be shared.');
		}
	}

	@isTest
	static void testUpdateAccountSharingRules() {
		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(currentUser) {
			createTestData();

			Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
			User user2 = createUser(profileId, 'tomtestadmin@vodafone.com');
			opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

			OpportunityTeamMember otm1 = new OpportunityTeamMember(OpportunityId = opp.Id, UserId = opp.OwnerId);
			insert otm1;

			OpportunityTeamMember otm2 = new OpportunityTeamMember(OpportunityId = opp.Id, UserId = user2.Id);
			insert otm2;

			Test.startTest();

			opp.OwnerId = user2.Id;
			update opp;

			Test.stopTest();

			List<AccountShare> resultAccShares = [SELECT Id FROM AccountShare WHERE AccountId = :acc.Id];
			System.assertEquals(true, resultAccShares.size() > 0, 'Account should be shared.');
		}
	}

	@isTest
	static void testUpdateChildRecordOwnersAndSharing() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ProductBasketTriggerHandler', null, 0);

		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(currentUser) {
			createTestData();

			TestUtils.autoCommit = false;

			Account partnerAcc = TestUtils.createPartnerAccount();
			partnerAcc.ParentId = acc.Id;
			insert partnerAcc;

			TestUtils.autoCommit = true;

			Contact partnerContact = createPartnerContact(partnerAcc.Id, 'tomislav.skopljaktest2@mm.com');
			Contact parentPartnerContact = createPartnerContact(partnerAcc.Id, 'tomislav.skopljaktest3@mm.com');

			TestUtils.createDealerInformation(partnerContact.Id, '111111', '222222');
			TestUtils.createDealerInformation(parentPartnerContact.Id, '222222');

			Id profileId = [SELECT Id FROM Profile WHERE Name = 'VF Partner Portal User' LIMIT 1].Id;
			User partnerUser = createPartnerUser(profileId, partnerContact.Id, 'tomtestportaluser@vodafone.com');
			createPartnerUser(profileId, parentPartnerContact.Id, 'tomtestportaluser2@vodafone.com');

			Test.startTest();

			opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
			vfc = TestUtils.createVFContract(acc, opp);

			cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
			insert basket;

			cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
			insert config;

			opp.OwnerId = partnerUser.Id;
			update opp;

			List<VF_Contract__Share> resultContractShares = [SELECT Id FROM VF_Contract__Share WHERE ParentId = :vfc.Id];
			System.assertEquals(true, resultContractShares.size() > 0, 'Contract should be shared.');

			List<cscfga__Product_Configuration__share> resultConfigShares = [
				SELECT Id
				FROM cscfga__Product_Configuration__share
				WHERE ParentId = :config.Id
			];
			System.assertEquals(true, resultConfigShares.size() > 0, 'Config should be shared.');

			List<cscfga__Product_Basket__share> resultBasketShares = [SELECT Id FROM cscfga__Product_Basket__share WHERE ParentId = :basket.Id];
			System.assertEquals(true, resultBasketShares.size() > 0, 'Basket should be shared.');

			Test.stopTest();
		}
	}

	@isTest
	static void testDeleteContract() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ProductBasketTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		createTestData();

		prod = TestUtils.createProduct();
		pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		vfc = TestUtils.createVFContract(acc, opp);
		oli = TestUtils.createOpportunityLineItem(opp, pbe);
		TestUtils.createOpportunityFieldMappings();
		enableClosedOppsEditing();

		Credit_Check__c cc = new Credit_Check__c();
		cc.Opportunity__c = opp.Id;
		cc.Credit_Check_Status_Number__c = 2;
		insert cc;

		opp.StageName = 'Closed Won';
		update opp;

		Test.startTest();

		opp.StageName = 'Prospecting';
		update opp;

		Test.stopTest();

		List<VF_Contract__c> resultContracts = [SELECT Id FROM VF_Contract__c WHERE Opportunity__c = :opp.Id];
		System.assertEquals(true, resultContracts.size() > 0, 'Contract should be deleted.');
	}

	@isTest
	static void testUpdateSegment() {
		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(currentUser) {
			createTestData();

			TestUtils.autoCommit = false;

			Account partnerAcc = TestUtils.createPartnerAccount();
			partnerAcc.ParentId = acc.Id;
			partnerAcc.Channel__c = 'BP';
			insert partnerAcc;

			Contact partnerContact = createPartnerContact(partnerAcc.Id, 'tomislav.skopljaktest2@mm.com');
			Id profileId = [SELECT Id FROM Profile WHERE Name = 'VF Partner Portal User' LIMIT 1].Id;
			User partnerUser = createPartnerUser(profileId, partnerContact.Id, 'tomtestportaluser@vodafone.com');

			Role_Segment_Mapping__c rsm = new Role_Segment_Mapping__c(Name = 'Tom', Segment__c = 'TEST', Main_Segment__c = 'TEST');
			insert rsm;

			opp = TestUtils.createOpportunity(partnerAcc, Test.getStandardPricebookId());
			opp.OwnerId = partnerUser.Id;
			opp.Ban__c = ban.Id;
			opp.Financial_Account__c = fa.Id;
			opp.Billing_Arrangement__c = ba.Id;
			Test.startTest();

			insert opp;

			Test.stopTest();

			Opportunity resultOpp = [SELECT Segment__c FROM Opportunity WHERE Id = :opp.Id];
			System.assertEquals('SME IDS', resultOpp.Segment__c, 'Opportunity Segment should be SME IDS.');
		}
	}

	@isTest
	static void testUpdatePartnerSolutionSpecialist() {
		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(currentUser) {
			createTestData();

			TestUtils.autoCommit = false;

			Account partnerAcc = TestUtils.createPartnerAccount();
			partnerAcc.ParentId = acc.Id;
			insert partnerAcc;

			Contact partnerContact = createPartnerContact(partnerAcc.Id, 'tomislav.skopljaktest2@mm.com');
			Id profileId = [SELECT Id FROM Profile WHERE Name = 'VF Partner Portal User' LIMIT 1].Id;
			User partnerUser = createPartnerUser(profileId, partnerContact.Id, 'tomtestportaluser@vodafone.com');

			AccountTeamMember atm = new AccountTeamMember(AccountId = partnerAcc.Id, UserId = partnerUser.Id, TeamMemberRole = 'Solution Specialist');
			insert atm;

			enableClosedOppsEditing();

			opp = TestUtils.createOpportunity(partnerAcc, Test.getStandardPricebookId());
			opp.OwnerId = partnerUser.Id;
			opp.StageName = 'Closed Lost';

			Test.startTest();

			insert opp;

			Test.stopTest();

			Opportunity resultOpp = [SELECT Solution_Sales__c FROM Opportunity WHERE Id = :opp.Id];
			System.assertEquals(partnerUser.Id, resultOpp.Solution_Sales__c, 'Opportunity Solution Sales should be inserted partner user.');
		}
	}

	@isTest
	static void testUpdatePartnerOpportunityTeam() {
		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(currentUser) {
			createTestData();
			acc.Dealer_Code__c = '333333';
			update acc;

			Account partnerAcc = new Account(Name = 'Tom Partner', ParentId = acc.Id, Total_Licenses__c = 999);
			insert partnerAcc;

			Contact partnerContact = createPartnerContact(partnerAcc.Id, 'tomislav.skopljaktest2@mm.com');
			Id profileId = [SELECT Id FROM Profile WHERE Name = 'VF Partner Portal User' LIMIT 1].Id;
			User partnerUser = createPartnerUser(profileId, partnerContact.Id, 'tomtestportaluser@vodafone.com');

			AccountTeamMember atm = new AccountTeamMember(
				AccountId = partnerAcc.Id,
				UserId = partnerUser.Id,
				TeamMemberRole = 'Solution Specialist',
				OpportunityAccessLevel = 'Edit'
			);
			insert atm;

			enableClosedOppsEditing();

			TestUtils.autoCommit = false;
			opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
			opp.OwnerId = acc.OwnerId;
			opp.StageName = 'Closed Lost';
			insert opp;

			Test.startTest();

			opp.OwnerId = partnerUser.Id;
			update opp;

			Test.stopTest();

			List<OpportunityTeamMember> resultOtms = [SELECT OpportunityAccessLevel FROM OpportunityTeamMember WHERE OpportunityId = :opp.Id];
			System.assertEquals(true, resultOtms.size() > 0, 'Opportunity Team Member should be created for opportunity owner.');
			System.assertEquals('Read', resultOtms[0].OpportunityAccessLevel, 'Opportunity Team Member\'s access level should be Read.');
		}
	}

	@isTest
	static void testUpdateAccountData() {
		createTestData();

		prod = TestUtils.createProduct();
		pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
		opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		oli = TestUtils.createOpportunityLineItem(opp, pbe);
		c.Userid__c = opp.OwnerId;
		update c;
		di = TestUtils.createDealerInformation(c.Id);
		TestUtils.createOpportunityFieldMappings();

		Credit_Check__c cc = new Credit_Check__c();
		cc.Opportunity__c = opp.Id;
		cc.Credit_Check_Status_Number__c = 2;
		insert cc;

		Test.startTest();

		opp.StageName = 'Closed Won';
		opp.Type_of_Service__c = 'Fixed Voice';
		update opp;

		Test.stopTest();

		Account resultAcc = [
			SELECT Fixed_Dealer__c, Fixed_Credit_Approver__c, Fixed_Owner__c, Fixed_Owner_Manager__c
			FROM Account
			WHERE Id = :acc.Id
		];
		System.assertEquals(di.Id, resultAcc.Fixed_Dealer__c, 'Account Fixed Dealer should be the inserted dealer.');
		System.assertEquals(di.Id, resultAcc.Fixed_Credit_Approver__c, 'Account Fixed Credit Approver should be the inserted dealer.');
		System.assertEquals(opp.OwnerId, resultAcc.Fixed_Owner__c, 'Account Fixed Owner should be the opportunity owner.');
		System.assertEquals(
			opp.Owner.ManagerId,
			resultAcc.Fixed_Owner_Manager__c,
			'Account Fixed Owner Manager should be the opportunity owner\'s manager.'
		);
	}

	@isTest
	static void testAddSagToOpportunityTeam() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ProductBasketTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);

		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(currentUser) {
			Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
			Id roleId = [SELECT Id FROM UserRole WHERE Name = 'Sales Assistance Group' LIMIT 1].Id;

			User user2 = createUser(profileId, 'tomtestadmin2@vodafone.com');
			user2.UserRoleId = roleId;
			update user2;

			User user3 = createUser(profileId, 'tomtestadmin3@vodafone.com');

			System.runAs(user2) {
				createTestData();

				prod = TestUtils.createProduct();
				pbe = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
				opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
				oli = TestUtils.createOpportunityLineItem(opp, pbe);
				TestUtils.createOpportunityFieldMappings();

				OpportunityTeamMember otm = new OpportunityTeamMember(
					OpportunityId = opp.Id,
					UserId = user3.Id,
					TeamMemberRole = 'Sales Assistance Group'
				);
				insert otm;

				Credit_Check__c cc = new Credit_Check__c();
				cc.Opportunity__c = opp.Id;
				cc.Credit_Check_Status_Number__c = 2;
				insert cc;

				Test.startTest();

				opp.StageName = 'Closed Won';
				update opp;

				Test.stopTest();

				Opportunity resultOpp = [SELECT Assigned_SAG_User__c FROM Opportunity WHERE Id = :opp.Id];
				//System.assertEquals(user2.Id, resultOpp.Assigned_SAG_User__c, 'Opportunity Assigned SAG User should be the current user.');
				System.assertNotEquals(user2.Id, resultOpp.Assigned_SAG_User__c, 'Opportunity Assigned SAG User should not be the current user.');
				List<OpportunityTeamMember> resultOtms = [SELECT Id FROM OpportunityTeamMember WHERE OpportunityId = :opp.Id];
				System.assertEquals(true, resultOtms.size() > 0, 'Opportunity Team Member should be created.');
			}
		}
	}

	@isTest
	static void testUpdateOpportunityTeam() {
		User currentUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(currentUser) {
			createTestData();

			Id profileId = [SELECT Id FROM Profile WHERE Name = 'VF Direct Inside Sales' LIMIT 1].Id;

			User user2 = createUser(profileId, 'tomtestadmin@vodafone.com');
			user2.Ziggo__c = true;
			update user2;

			enableClosedOppsEditing();

			TestUtils.autoCommit = false;
			opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
			opp.StageName = 'Closed Lost';
			opp.OwnerId = user2.Id;
			opp.Solution_Sales__c = user2.Id;
			opp.Assigned_SAG_User__c = user2.Id;

			Test.startTest();

			insert opp;

			opp.OwnerId = UserInfo.getUserId();
			opp.Solution_Sales__c = UserInfo.getUserId();
			opp.Assigned_SAG_User__c = UserInfo.getUserId();
			update opp;

			Test.stopTest();

			List<OpportunityTeamMember> resultOtms = [
				SELECT Id, TeamMemberRole, OpportunityAccessLevel
				FROM OpportunityTeamMember
				WHERE OpportunityId = :opp.Id
			];
			System.assertEquals(true, resultOtms.size() > 0, 'Opportunity Team Members should be created for opportunity owner.');
		}
	}
}
