/**
 * @description       : Apex Class Handler of the Applicability Rule Trigger
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

public class ApplicabilityRuleTriggerHandler extends TriggerHandler {
	private List<csclm__Applicability_Rule__c> lstNewApplicabilityRule;

	private Map<Id, csclm__Applicability_Rule__c> mapOldApplicabilityRule;
	private Map<Id, csclm__Applicability_Rule__c> mapNewApplicabilityRule;

	/**
	 * @description Initialize Class Variables
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	private void init() {
		lstNewApplicabilityRule = (List<csclm__Applicability_Rule__c>) this.newList;
		mapOldApplicabilityRule = (Map<Id, csclm__Applicability_Rule__c>) this.oldMap;
		mapNewApplicabilityRule = (Map<Id, csclm__Applicability_Rule__c>) this.newMap;
	}

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public void setExternalIds() {
		Sequence objSequence = new Sequence('Applicability Rule');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (csclm__Applicability_Rule__c objApplicabilityRule : lstNewApplicabilityRule) {
				objApplicabilityRule.ExternalID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
