@IsTest
private class PP_JourneyControllerTest {
	@IsTest
	static void controllerTest() {
		TestUtils.createOrderValidationNetProfitInformation();
		createValidations();
		Test.startTest();
		TestUtils.createCompleteContract();
		Test.stopTest();
		createOrder(TestUtils.theContract);

		List<PP_JourneyController.CustomOpportunityClass> p = PP_JourneyController.retrieveOrdersByView('0', 'incomplete-orders', 'all', 'opp', '');
		p = PP_JourneyController.retrieveOrdersByView('0', 'incomplete-orders', 'basket', 'opp', '');
		p = PP_JourneyController.retrieveOrdersByView('0', 'orders-pending-sag', 'all', 'opp', '');
		p = PP_JourneyController.retrieveOrdersByView('0', 'orders-pending-sag', 'all', 'basket', '');
		p = PP_JourneyController.retrieveOrdersByView('0', 'completed-orders', 'all', 'opp', '');
		p = PP_JourneyController.retrieveOrdersByView('0', 'completed-orders', 'all', 'basket', '');

		PP_JourneyController.getOpportunityAttachments(TestUtils.theOpportunity.Id);
		List<Order__c> orders = PP_JourneyController.getOrders(TestUtils.theContract.Id);
		System.assertEquals(1, orders.size(), 'Orders created.');
		PP_JourneyController.getOpportunity(TestUtils.theOpportunity.Id);
		PP_JourneyController.getAccount(TestUtils.theAccount.Id);
		List<Opportunity> opps = PP_JourneyController.getRecentOpportunities('3');
		System.assertEquals(0, opps.size(), 'Opportunity size is 0');
		PP_JourneyController.opptyRecordTypeIsMac(TestUtils.theAccount.Id);
		PP_JourneyController.getCompleteOrdersQuery2('mine');
	}

	@IsTest
	static void generalTest() {
		System.assertNotEquals(null, PP_JourneyController.getOpportunityDefaultRecordTypeId(), 'Record type id is null');
		System.assertNotEquals(null, PP_JourneyController.getContactGeneralRecordTypeId(), 'Record type id is null');
	}

	@IsTest
	static void customOppTest() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		TestUtils.autoCommit = true;
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), null, owner);

		Test.startTest();
		List<PP_JourneyController.CustomOpportunityClass> p = new List<PP_JourneyController.CustomOpportunityClass>();
		PP_JourneyController.CustomOpportunityClass p1 = new PP_JourneyController.CustomOpportunityClass();
		p1.opp = opp;
		p1.isCompletedOpp = true;
		p.add(p1);
		System.assert(p1.basketName == null, 'Basket name is null');
		System.assert(p1.phaseNumber == 0, 'phase numer is 0');
		System.assert(!p1.isBasket, 'Is not basket');
		cscfga__Product_Basket__c basket = createBasket(TestUtils.theOpportunity);
		basket = [SELECT Id, Name FROM cscfga__Product_Basket__c WHERE Id = :basket.ID];
		PP_JourneyController.CustomOpportunityClass p2 = new PP_JourneyController.CustomOpportunityClass();
		p2.opp = opp;
		p2.isCompletedOpp = true;
		p2.basket = basket;
		p2.phase = '1';
		p.add(p2);
		p.sort();
		System.assert(p2.basketName != null, 'Basket name is not null');
		System.assert(p2.isBasket, 'Is basket');
		Test.stopTest();
	}

	@IsTest
	static void customOppRetrieveIncompleteOrdersTest() {
		createValidations();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact c = TestUtils.createContact(acct);
		acct.Authorized_to_sign_1st__c = c.Id;
		update acct;
		TestUtils.autoCommit = true;
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), null, owner);
		cscfga__Product_Basket__c basket = createBasket(opp);

		Test.startTest();
		List<PP_JourneyController.CustomOpportunityClass> p = PP_JourneyController.retrieveOrdersByView('0', 'incomplete-orders', 'all', 'opp', '');
		p = PP_JourneyController.retrieveOrdersByView('0', 'incomplete-orders', 'all', 'basket', '');
		p = PP_JourneyController.retrieveOrdersByView('0', 'incomplete-orders', 'all', 'opp', 'Test');

		closeOpp(opp);

		p = PP_JourneyController.retrieveOrdersByView('0', 'incomplete-orders', 'all', 'basket', '');
		System.assert(p[0].basket != null, 'Is Basket');
		p = PP_JourneyController.retrieveOrdersByView('0', 'incomplete-orders', 'all', 'opp', 'Test');
		System.assert(p[0].opp.Name.contains('Test'), 'Exist Test Opportunity');
		Test.stopTest();
	}

	@IsTest
	static void customOppRetrieveOrdersPendingSagTest() {
		createValidations();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact c = TestUtils.createContact(acct);
		acct.Authorized_to_sign_1st__c = c.Id;
		update acct;
		TestUtils.autoCommit = true;
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), null, owner);
		opp.StageName = 'Closing by SAG';
		update opp;

		Test.startTest();
		List<PP_JourneyController.CustomOpportunityClass> viewSag = PP_JourneyController.retrieveOrdersByView(
			'0',
			'orders-pending-sag',
			'all',
			'',
			''
		);
		viewSag = PP_JourneyController.retrieveOrdersByView('0', 'orders-pending-sag', 'all', 'basket', 'Test');
		viewSag = PP_JourneyController.retrieveOrdersByView('0', 'orders-pending-sag', 'mine', '', '');
		System.assertEquals(0, viewSag.size(), 'List size is 0');

		closeOpp(opp);

		List<PP_JourneyController.CustomOpportunityClass> viewCW = PP_JourneyController.retrieveOrdersByView(
			'0',
			'orders-pending-sag',
			'all',
			'',
			''
		);
		viewCW = PP_JourneyController.retrieveOrdersByView('0', 'orders-pending-sag', 'all', 'basket', 'Test');
		viewCW = PP_JourneyController.retrieveOrdersByView('0', 'orders-pending-sag', 'mine', '', '');
		// order stat
		Test.stopTest();
	}

	@IsTest
	static void customOppRetrieveCompletedOrdersTest() {
		createValidations();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact c = TestUtils.createContact(acct);
		acct.Authorized_to_sign_1st__c = c.Id;
		update acct;
		TestUtils.autoCommit = true;
		Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), null, owner);

		Test.startTest();
		List<PP_JourneyController.CustomOpportunityClass> viewCO = PP_JourneyController.retrieveOrdersByView('0', 'completed-orders', 'all', '', '');
		viewCO = PP_JourneyController.retrieveOrdersByView('0', 'completed-orders', 'all', 'basket', '');
		viewCO = PP_JourneyController.retrieveOrdersByView('0', 'completed-orders', 'mine', 'basket', '');
		System.assertEquals(0, viewCO.size(), 'List size is 0');

		closeOpp(opp);

		viewCO = PP_JourneyController.retrieveOrdersByView('1', 'completed-orders', 'all', '', '');
		viewCO = PP_JourneyController.retrieveOrdersByView('0', 'completed-orders', 'all', 'basket', '');
		viewCO = PP_JourneyController.retrieveOrdersByView('0', 'completed-orders', 'mine', 'basket', '');
		//order stat
		Test.stopTest();
	}

	@IsTest
	static void customOppGetContactFieldsTest() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Contact cont = TestUtils.createContact(acct);
		List<AuraField> fields = PP_JourneyController.getContactFields(null);
		fields = PP_JourneyController.getContactFields(cont.Id);
		System.assertEquals(3, fields.size(), 'List size is 3');
	}

	// test data //
	public static void createValidations() {
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationOpportunity();
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
	}

	public static cscfga__Product_Basket__c createBasket(Opportunity opp) {
		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
		basket.cscfga__Opportunity__c = opp.Id;
		basket.csbb__Account__c = opp.AccountId;
		basket.Expected_delivery_date_for_Fixed__c = Date.today();
		basket.Expected_delivery_date_for_Mobile__c = Date.today();
		basket.cscfga__Basket_Status__c = 'Contract Created';
		insert basket;
		return basket;
	}

	public static Order__c createOrder(VF_Contract__c c) {
		TestUtils.autoCommit = true;
		Order__c ord = TestUtils.createOrder(c);
		ord.Sales_Order_Id__c = '082495873abc';
		update ord;
		return ord;
	}

	public static void closeOpp(Opportunity opp) {
		opp.StageName = 'Closed Won';
		update opp;
		VF_Contract__c c = new VF_Contract__c(Account__c = opp.AccountId, Opportunity__c = opp.Id);
		insert c;
		Order__c ord = TestUtils.createOrder(c);
		ord.Sales_Order_Id__c = '082495873abc';
		update ord;
	}

	@IsTest
	static void validateAddMobileJourney() {
		createValidations();
		TestUtils.createCompleteOpportunity();
		Opportunity opp = TestUtils.theOpportunity;

		Test.startTest();
		Boolean blnJourneyLocked = PP_JourneyController.getJourneyLock(opp.Id);
		System.assertEquals(true, blnJourneyLocked, 'meaning the journey is locked and cannot be edited and the BP can proceed');
		Opportunity oppAssert = PP_JourneyController.getBANforAddMobileJourney(opp.Id);
		System.assertEquals(false, oppAssert.BAN__r == null, 'meaning the BAN has been select to continue with the journey');
		List<OpportunityLineItem> lstAssert = PP_JourneyController.getOpportunityLineItems(opp.Id);
		System.assertEquals(false, lstAssert.size() == 0, 'meaning OLI are created at this point of the journey and the BP can continue');
		Test.stopTest();
	}

	@IsTest
	static void createDefaultBppOpportunityTest() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);

		Test.startTest();
		Id opportunityId = PP_JourneyController.createDefaultBppOpportunity(account.Id);
		Test.stopTest();

		List<Opportunity> opportunityList = [SELECT Id FROM Opportunity WHERE Id = :opportunityId];

		System.assertEquals(1, opportunityList.size(), 'Opportunity should be created and its Id returned.');
	}

	@IsTest
	static void createDefaultBppOpportunityNullTest() {
		Test.startTest();
		Id opportunityId = PP_JourneyController.createDefaultBppOpportunity('');
		Test.stopTest();

		System.assertEquals(opportunityId, null, 'Returned value should be null.');
	}
}
