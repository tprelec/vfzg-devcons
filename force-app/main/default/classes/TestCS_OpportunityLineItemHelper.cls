@isTest
private class TestCS_OpportunityLineItemHelper {
	@testSetup
	private static void setup() {
		Account account = CS_DataTest.createAccount('Bubamara Inc');
		insert account;

		OrderType__c orderType = CS_DataTest.createOrderType('MAC');
		insert orderType;

		Product2 prod21 = CS_DataTest.createProduct2('Recurring Price Basic 1', 'Access Definition', orderType, true);
		prod21.Quantity_type__c = 'Monthly';

		Product2 prod22 = CS_DataTest.createProduct2('Recurring Price Basic 2', 'Access Definition', orderType, true);
		prod22.Quantity_type__c = 'Monthly';

		List<Product2> products = new List<Product2>{ prod21, prod22 };
		insert products;

		cscfga__Product_Definition__c pdAccess = CS_DataTest.createProductDefinitionRegular('Access');
		insert pdAccess;

		Site__c testSite = CS_DataTest.createSite('testSite', account, '1234AA', 'teststreet', 'testTown', 3);
		Site__c testSite2 = CS_DataTest.createSite('testSite2', account, '1234AB', 'teststreet2', 'testTown2', 3);
		List<Site__c> siteList = new List<Site__c>{ testSite, testSite2 };
		insert siteList;

		// old basket
		Opportunity oldOpportunity = CS_DataTest.createOpportunity(account, 'Bubamara ACQ', System.UserInfo.getUserId());
		insert oldOpportunity;

		cscfga__Product_Basket__c oldBasket = CS_DataTest.createProductBasket(oldOpportunity, 'VZ-Bubamara-01');
		oldBasket.cscfga__Basket_Status__c = 'Valid';
		oldBasket.Primary__c = true;
		oldBasket.csbb__Synchronised_with_Opportunity__c = true;
		insert oldBasket;

		cscfga__Product_Configuration__c oldPcAccess = CS_DataTest.createProductConfiguration(pdAccess.Id, 'Access 1', oldBasket.Id);
		oldPcAccess.cscfga__Contract_Term__c = 24;
		oldPcAccess.Recurring_Charge_Product__c = prod21.Id;
		oldPcAccess.Site__c = testSite.Id;
		insert oldPcAccess;

		// new basket
		Opportunity newOpportunity = CS_DataTest.createOpportunity(account, 'Bubamara ACQ New', System.UserInfo.getUserId());
		insert newOpportunity;

		cscfga__Product_Basket__c newBasket = CS_DataTest.createProductBasket(newOpportunity, 'VZ-Bubamara-02');
		newBasket.cscfga__Basket_Status__c = 'Valid';
		newBasket.Primary__c = true;
		newBasket.New_Portfolio__c = true;
		newBasket.csbb__Synchronised_with_Opportunity__c = true;
		insert newBasket;

		cscfga__Product_Configuration__c newPcAccess = CS_DataTest.createProductConfiguration(pdAccess.Id, 'Access 2', newBasket.Id);
		newPcAccess.cscfga__Contract_Term__c = 24;
		newPcAccess.Recurring_Charge_Product__c = prod22.Id;
		newPcAccess.csordtelcoa__Replaced_Product_Configuration__c = oldPcAccess.Id;
		newPcAccess.Site__c = testSite2.Id;
		insert newPcAccess;

		// attributes
		cscfga__Attribute__c attributeOneOffOld = CS_DataTest.createAttribute(oldPcAccess.Id, 'OneOff', '10');
		attributeOneOffOld.cscfga__Is_Line_Item__c = true;
		attributeOneOffOld.cscfga__Recurring__c = false;
		attributeOneOffOld.cscfga__Price__c = 10;
		attributeOneOffOld.cscfga__Line_Item_Description__c = 'OneOff component';

		cscfga__Attribute__c attributeRecurringOld = CS_DataTest.createAttribute(oldPcAccess.Id, 'Recurring', '20');
		attributeRecurringOld.cscfga__Is_Line_Item__c = true;
		attributeRecurringOld.cscfga__Recurring__c = true;
		attributeRecurringOld.cscfga__Price__c = 20;
		attributeRecurringOld.cscfga__Line_Item_Description__c = 'Recurring component';

		cscfga__Attribute__c attributeOneOffNew = CS_DataTest.createAttribute(newPcAccess.Id, 'OneOff', '10');
		attributeOneOffNew.cscfga__Is_Line_Item__c = true;
		attributeOneOffNew.cscfga__Recurring__c = false;
		attributeOneOffNew.cscfga__Price__c = 10;
		attributeOneOffNew.cscfga__Line_Item_Description__c = 'OneOff component';

		cscfga__Attribute__c attributeRecurringNew = CS_DataTest.createAttribute(newPcAccess.Id, 'Recurring', '20');
		attributeRecurringNew.cscfga__Is_Line_Item__c = true;
		attributeRecurringNew.cscfga__Recurring__c = true;
		attributeRecurringNew.cscfga__Price__c = 20;
		attributeRecurringNew.cscfga__Line_Item_Description__c = 'Recurring component';

		List<cscfga__Attribute__c> attributes = new List<cscfga__Attribute__c>{
			attributeOneOffOld,
			attributeRecurringOld,
			attributeOneOffNew,
			attributeRecurringNew
		};
		insert attributes;
	}

	@isTest
	private static void testCreateCeasedOlis() {
		cscfga__Product_Basket__c newBasket = [SELECT Id, Name FROM cscfga__Product_Basket__c WHERE New_Portfolio__c = TRUE LIMIT 1];
		Set<String> setBasketIds = new Set<String>{ newBasket.Id };
		Test.startTest();
		ProductUtility.CreateOLIs(setBasketIds, null);
		Test.stopTest();

		List<OpportunityLineItem> ceasedItems = [
			SELECT Id, Activity_type__c
			FROM OpportunityLineItem
			WHERE Activity_type__c = :CS_ConstantsCOM.OLI_ACTIVITY_TYPE_CEASE
		];
		System.assertNotEquals(ceasedItems.size(), 0, 'Ceased items should be created!');
	}
}
