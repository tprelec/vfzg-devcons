@IsTest
public with sharing class TestQueueUserSharingService {

    @testsetup
    private static void setupTestData() {

        TestUtils.createLead();
        Account acc =TestUtils.createAccount(GeneralUtils.currentUser);
        Pricebook2 prbk = TestUtils.createPricebook(false);
        Opportunity opp = TestUtils.createOpportunity(acc, prbk.Id);
        TestUtils.createNumberPorting(TestUtils.createOrder(TestUtils.createVFContract(acc, opp)));
    }

    @isTest
    public static void shareAccountsWithUserQueues() {

        Queue_User_Sharing__c qusObject = new Queue_User_Sharing__c();

        Account acc = [SELECT Id, Name, OwnerId FROM Account LIMIT 1];
        Profile p = [SELECT p.Name, p.Id FROM Profile p WHERE p.Name = 'System Administrator'];
        UserRole r = [SELECT r.Name, r.Id FROM UserRole r WHERE r.DeveloperName = 'Administrator'];
        User usr = TestUtils.generateTestUser('Testing' , 'QueueUser' , p.Id, r.Id);

        Test.startTest();

        System.runAs(usr) {

            qusObject.ownerId = usr.Id;
            insert qusObject;

            QueueUserSharingService.shareAccountsWithUserQueues(new map<Id, set<Id>>{qusObject.Id => new set<Id>{acc.Id}});
        }

        Test.stopTest();

        List<AccountShare> lst_accountShare = [SELECT Id,AccountId,UserOrGroupId FROM Accountshare WHERE AccountId =:acc.Id AND UserOrGroupId=:usr.Id AND RowCause=:Schema.AccountShare.RowCause.Manual];

        System.assert(lst_accountShare.size() == 1, 'The amount of manual shared records for the Account should be one.');
    }

    @isTest
    public static void removeShareAccountsWithUserQueues() {

        AccountShare accShare;
        Queue_User_Sharing__c qusObject = new Queue_User_Sharing__c();

        Account acc = [SELECT Id, Name, OwnerId FROM Account LIMIT 1];
        Profile p = [SELECT p.Name, p.Id FROM Profile p WHERE p.Name = 'System Administrator'];
        UserRole r = [SELECT r.Name, r.Id FROM UserRole r WHERE r.DeveloperName = 'Administrator'];
        User usr = TestUtils.generateTestUser('Testing', 'QueueUser', p.Id, r.Id);

        Test.startTest();

        System.runAs(usr) {

            qusObject.ownerId = usr.Id;
            insert qusObject;

            accShare = new AccountShare(UserOrGroupId = usr.Id, RowCause = Schema.AccountShare.RowCause.Manual, AccountAccessLevel = 'Edit', OpportunityAccessLevel = 'None', CaseAccessLevel = 'None', AccountId = acc.Id);
            insert accShare;

            QueueUserSharingService.removeShareAccountsWithUserQueues(new map<Id, set<Id>>{qusObject.Id => new set<Id>{acc.Id}});
        }

        Test.stopTest();

        List<AccountShare> lst_accountShare = [SELECT Id, AccountId, UserOrGroupId FROM Accountshare WHERE AccountId =: acc.Id AND UserOrGroupId =: usr.Id AND RowCause =: Schema.AccountShare.RowCause.Manual];

        System.assert(lst_accountShare.size() == 0, 'The Account share table should have no records for the User that owns the Queue Sharing records.');
    }
}