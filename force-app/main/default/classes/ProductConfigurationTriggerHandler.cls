public with sharing class ProductConfigurationTriggerHandler extends TriggerHandler {
	/**
	 * @description         This handles the after update trigger event.
	 * @param   newObjects  List of new sObjects that are being created.
	 */
	public override void AfterUpdate() {
		List<cscfga__Product_Configuration__c> newProductConfigurations = (List<cscfga__Product_Configuration__c>) this.newList;
		Map<Id, cscfga__Product_Configuration__c> oldProductConfigurationsMap = (Map<Id, cscfga__Product_Configuration__c>) this.oldMap;

		if (isTriggerActive()) {
			if(CS_Brand_Resolver.isVodafoneRecord(newProductConfigurations)) {
				setBasketToPendingUsage(newProductConfigurations);
			}
		}
	}

	/**
	 * @description         This handles the after delete trigger event.
	 * @param   newObjects  List of sObjects that are were deleted.
	 */
	public override void AfterDelete() {
		List<cscfga__Product_Configuration__c> oldProductConfigurations = (List<cscfga__Product_Configuration__c>) this.oldList;
		Map<Id, cscfga__Product_Configuration__c> oldProductConfigurationsMap = (Map<Id, cscfga__Product_Configuration__c>) this.oldMap;

		if (isTriggerActive()) {
			if (CS_Brand_Resolver.isVodafoneRecord(oldProductConfigurations)) {
				CS_CustomPostConfiguringController.resetPricesInTriggerConfig(
					oldProductConfigurations,
					true
				);
				revalidateBasketStatus(oldProductConfigurations);
			}
		}
	}

	private static void revalidateBasketStatus(List<cscfga__Product_Configuration__c> oldList) {
		// Id - basketId
		// Boolean value - true if all configurations from basket with Id are 'Valid', false otherwise

		Map<Id, Boolean> basketConfigurationValidity = new Map<Id, Boolean>();
		Set<Id> basketIds = new Set<Id>();
		for (cscfga__Product_Configuration__c config : oldList) {
			basketIds.add(config.cscfga__Product_Basket__c);
		}

		List<cscfga__Product_Configuration__c> configList = [
			SELECT Id, cscfga__Product_Basket__c, cscfga__Configuration_Status__c
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c IN :basketIds
		];

		for (cscfga__Product_Configuration__c config : configList) {
			if (basketConfigurationValidity.containsKey(config.cscfga__Product_Basket__c)) {
				if (
					basketConfigurationValidity.get(config.cscfga__Product_Basket__c) == true &&
					config.cscfga__Configuration_Status__c != 'Valid'
				) {
					basketConfigurationValidity.put(config.cscfga__Product_Basket__c, false);
				}
			} else {
				basketConfigurationValidity.put(
					config.cscfga__Product_Basket__c,
					config.cscfga__Configuration_Status__c == 'Valid'
				);
			}
		}

		List<cscfga__Product_Basket__c> basketsToUpdate = new List<cscfga__Product_Basket__c>();
		List<cscfga__Product_Basket__c> baskets = [
			SELECT Id, cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
			WHERE Id IN :basketConfigurationValidity.keySet()
		];
		for (cscfga__Product_Basket__c basket : baskets) {
			if (
				basket.cscfga__Basket_Status__c != 'Valid' &&
				basketConfigurationValidity.get(basket.Id) == true
			) {
				basket.cscfga__Basket_Status__c = 'Valid';
				basketsToUpdate.add(basket);
			}
		}

		if (basketsToUpdate.size() > 0) {
			update basketsToUpdate;
		}
	}

	private static void setBasketToPendingUsage(List<cscfga__Product_Configuration__c> newList) {
		Set<Id> basketIds = new Set<Id>();
		List<cscfga__Product_Basket__c> basketsToUpdate = new List<cscfga__Product_Basket__c>();

		for (cscfga__Product_Configuration__c config : newList) {
			if (config.cscfga__Configuration_Status__c == 'Requires update') {
				basketIds.add(config.cscfga__Product_Basket__c);
			}
		}

		if (basketIds.size() == 0) {
			return;
		}

		List<cscfga__Product_Basket__c> baskets = [
			SELECT Id, SetPendingUsage__c, cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
			WHERE Id IN :basketIds
		];
		for (cscfga__Product_Basket__c basket : baskets) {
			if (basket.SetPendingUsage__c == true) {
				basket.SetPendingUsage__c = false;
				basket.cscfga__Basket_Status__c = 'Pending usage';
				basketsToUpdate.add(basket);
			}
		}

		if (basketsToUpdate.size() > 0) {
			update basketsToUpdate;
		}
	}
}