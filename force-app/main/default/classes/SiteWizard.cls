/**
 * @description         This class is responsible for calling, parsing the Olbico JSON Webservice on BAG information
 * @author              Marcel Vreuls
 * @History             Nov 2017: inital creation for W-000286. 
 *                      feb 2018: fixed some issues, changed custom labels in setup 
 * @Defaults            Requires the creation and update of the Olbico Custom settings
 *
 *
 */
public with sharing class SiteWizard {

    private final sObject mysObject;

    public Id accountId {get;set;}
    public Account account {get;set;}
    public string selectedValue {get;set;}
    public List<SelectOption> Addresinformation {get; set;}
    public Boolean showCreateCase {get;set;}
    public boolean showIFrame {get;set;}
    public String retURL {get;set;}     
    public String labelSave;
    public String labelCancel;
    public boolean fromAccountPage {get;set;}
    public Boolean showCreateSite {get;set;}
    //public site__c site {get;set;}
    

    public SiteWizard(ApexPages.StandardController controller){

        this.account=(Account)controller.getrecord();
        accountId = account.id;

        //accountId = ApexPages.currentPage().getParameters().get('id');
        account = [select Id, Name, SicDesc from Account where Id = : accountId];
        account.SicDesc = '';
        showCreateCase = false; 
        showCreateSite = false;
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        String basketId;
        if (!String.isEmpty(ApexPages.currentPage().getParameters().get('basketId'))) {
            basketId = ApexPages.currentPage().getParameters().get('basketId');
            retURL += '&basketId='+basketId;
        }
        showIFrame=true;
        fromAccountPage = false;
        if (retURL==null) {
            fromAccountPage=true;
        } else if (!retURL.tolowercase().contains('sitemanager')) {
            fromAccountPage=true;
        }
        if (fromAccountPage) {
            retURL='/'+accountId;
            showIFrame = false;
        }
        system.debug('retURL:'+retURL);
    }

 

    public String getlabelSave() {
        if (fromAccountPage) {
            labelSave=Label.LABEL_Create_Site;
        } else {
            labelSave=Label.LABEL_Save;
        }
        return labelSave;
    }

    public String getlabelCancel() {
        if (fromAccountPage) {
            labelCancel=Label.LABEL_Back_To+' '+account.name;
        } else {
            labelCancel=Label.LABEL_Cancel;
        }
        return labelCancel;
    }    

    public Boolean getIsPartnerUser(){
        return GeneralUtils.currentUserIsPartnerUser();
    }    

    public pageReference cancel(){
        PageReference p = new PageReference(retURL);        
        p.setRedirect(false);
        return p;
    }

    // Creates a new case pre-filling the account details
    public pageReference createCase() {
        PageReference p = new PageReference('/apex/RedirectWithVariables?object=Case&allowRTS=true&Account__c='+Account.Name+'&ID_Account__c='+Account.Id+'&retURL=/'+Account.Id+'&cancelURL=/'+Account.Id);
        p.setRedirect(false);
        return p;
    }

    public pageReference saveAndNew() {
        // Save the site using the regular method
        save();
        // Clear all input values and selected value to reuse the page
        account.SicDesc=null;
        selectedValue=null;  
        Addresinformation=null;
        return null;      
    }

    public pageReference save(){       
       
        integer i=0;

        string straat;        
        integer huisnummer;
        string  huisletter;
        String  huisnummertoevoeging;
        string postcode;
        string woonplaats;
                       

        String[] arrTest = selectedValue.split('\\|');

        for (String sValue : arrTest) {
            if(i==0)
            {
                straat = sValue;
            }
            if(i==1)
            {
                huisnummer =  Integer.valueof(sValue.Trim());
            }
             if(i==2)
            {
                huisletter = sValue;
            }
            if(i==3)
            {
                huisnummertoevoeging = sValue;
            }
            if(i==4)
            {
                postcode = sValue;
            }
            if(i==5)
            {
                woonplaats = sValue;
            }
            if(i==6)
            {
                woonplaats = woonplaats + ' - ' + sValue;
            }
            i++;
        }
        

        Site__c mySite = new Site__c();
        mySite.Site_Account__c = this.account.id;
        mySite.KVK_Number__c = this.account.KVK_number__c;
        mySite.Site_Street__c = straat;
        mySite.Site_House_Number__c = huisnummer;
        mySite.Site_House_Number_Suffix__c = huisletter + ' ' + huisnummertoevoeging;
        mySite.Site_Postal_Code__c = postcode;
        mySite.Site_City__c = woonplaats;
        mySite.Olbico__c = true;
        mySite.Name = straat;

        try {
            insert mySite;
        } catch (DMLException e) {
            if (e.getDmlType(0)==StatusCode.DUPLICATE_VALUE) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, Label.ERROR_Duplicate));
            } else {
                ApexPages.addMessages(e);                
            }
            return null;
        }

        PageReference nextPage = new PageReference(retURL);
        return nextPage;    
    }

    public void fetchStreets()
    {
            
        OlbicoServiceJSON jService = new OlbicoServiceJSON();

        string Zipcode = account.SicDesc.deleteWhitespace().toUpperCase();
        
        jService.setRequestRestJsonStreet(Zipcode);
        jService.makeRequestRestJsonStreet(Zipcode);

        Addresinformation = jService.AdresInfo();
        if (!jService.addressFound) {
            
            if(GeneralUtils.currentUserIsPartnerUser()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, Label.ERROR_No_Address_Found_Partner));                     
            } else {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, Label.ERROR_No_Address_Found));                   
            }
           
            //Only enable the new site button for SAG users
            Profile p = [Select Name from Profile where Id =: userinfo.getProfileid()];
            if (p.name == 'VF Indirect Inside Sales' || p.Name == 'System Administrator' || p.Name == 'Systeembeheerder')
            {
                showCreateSite = true;
            }      
            else
            {
                showCreateCase = true;  
            } 
        }  
        else
        {
            showCreateCase = false;  
            system.debug('saveSelected' +  selectedValue);
        }
    }
}