global with sharing class CustomButtonManageSites extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		String newUrl = CustomButtonManageSites.redirectToManageSites(basketId);
		return '{"status":"ok","redirectURL":"' + newUrl + '"}';
	}

	// Set url for redirect after action
	public static String redirectToManageSites(String basketId) {
		PageReference editPage = new PageReference('/apex/SiteManager?basketId=' + basketId);

		Id profileId = UserInfo.getProfileId();
		String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;

		if (profileName == 'VF Partner Portal User') {
			editPage = new PageReference('/partnerportal/apex/SiteManager?basketId=' + basketId);
		}

		return editPage.getUrl();
	}
}
