public with sharing class MedalliaFeedbackTriggerHandler extends TriggerHandler {

    public override void BeforeInsert(){

    }

    /**
     * @description			This handles the after insert trigger event.
     * @param	newObjects	List of new sObjects that are being created.
     */
    public override void AfterInsert(){
        List<Medallia_Feedback__c> newFeedbackList = (List<Medallia_Feedback__c>) this.newList;
        MedalliaFeedbackTriggerHandler.rollUpNpsFeedbackRecords(newFeedbackList, false, null);
    }

    public override void BeforeUpdate(){

    }

    /**
     * @description			This handles the after update trigger event.
     * @param	newObjects	List of new sObjects that are being created.
     */
    public override void AfterUpdate(){
        List<Medallia_Feedback__c> newFeedbackList = (List<Medallia_Feedback__c>) this.newList;
        MedalliaFeedbackTriggerHandler.rollUpNpsFeedbackRecords(newFeedbackList, false, null);
    }

    /**
     * Before Delete
     */
    public override void BeforeDelete() {
        List<Medallia_Feedback__c> oldFeedbackList = (List<Medallia_Feedback__c>) this.oldList;
        Map<Id, Medallia_Feedback__c> oldFeedbackMap = (Map<Id, Medallia_Feedback__c>) this.oldmap;
        MedalliaFeedbackTriggerHandler.rollUpNpsFeedbackRecords(oldFeedbackList, true, oldFeedbackMap);
    }

    /**
     * Count the amount of Completed Survey Feedback records
     *
     * @param mfList
     * @param isDelete
     */
    public static void rollUpNpsFeedbackRecords(List<Medallia_Feedback__c> mfList, Boolean isDelete, Map<Id, Medallia_Feedback__c> mfDeleteMap) {

        system.debug('##mfList: '+mfList);
        system.debug('##mfDeleteMap: '+mfDeleteMap);
        Set<Id> accSet = new Set<Id>();
        Map<Id, Integer> accCountMap = new Map<Id, Integer>();
        List<Account> accUpdateList = new List<Account>();

        for (Medallia_Feedback__c mf : mfList) {
            accSet.add(mf.Account_Id__c);
            accCountMap.put(mf.Account_Id__c, 0);
        }

        List<Medallia_Feedback__c> allMfRecord = [Select Id, Account_Id__c, Record_Status__c From Medallia_Feedback__c Where Account_Id__c IN : accSet AND Record_Status__c = 'Completed'];
        for (Medallia_Feedback__c mf : allMfRecord) {
            Integer counter = 0;
            if (accCountMap.containsKey(mf.Account_Id__c)) {
                counter = accCountMap.get(mf.Account_Id__c);
                counter += 1;

            } else {
                counter = 1;
            }
            if (isDelete && mfDeleteMap != null) {
                system.debug('##mfDeleteMap: '+mfDeleteMap);
                system.debug('##mf.Id: '+mf.Id);
                if (mfDeleteMap.containsKey(mf.Id)) {
                    counter = counter - 1;
                }
            }
            accCountMap.put(mf.Account_Id__c, counter);
        }
        for (Id accId : accCountMap.keySet()) {
            Account acc = new Account();
            acc.Id = accId;
            acc.Total_NPS_responses__c = accCountMap.get(accId);
            accUpdateList.add(acc);
        }
        if (accUpdateList.size() > 0) {
            SharingUtils.updateObjectsWithoutSharingStatic(accUpdateList);
        }

    }
}