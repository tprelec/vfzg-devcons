@isTest
public class CST_Questionnaire_UT {
	@testSetup
	public static void testSetup() {
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		Contact caseContact = TestUtils.createContact(acc);
		caseContact.Email = '123456abcde@mail.com';
		update caseContact;
		RecordType questionRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CST_Incident'];
		Id questionRecordTypeId = (questionRecordType != null) ? questionRecordType.Id : null;
		Case c = new Case(
			AccountId = acc.Id,
			CST_Product__c = 'CST_Standard',
			RecordTypeId = questionRecordTypeId,
			ContactId = caseContact.Id,
			Type = 'Internet Services',
			CST_Level1__c = 'Internet (Data)'
		);
		insert c;

		List<CST_Question_Bank__c> questions = new List<CST_Question_Bank__c>();
		for (Integer i = 1; i < 3; i++) {
			CST_Question_Bank__c question = new CST_Question_Bank__c();
			question.CST_Active__c = true;
			question.CST_Case_RecordType__c = 'CST_Incident';
			question.CST_Case_Level1__c = 'Internet (Data)';
			question.CST_Case_Type__c = 'Internet Services';
			question.CST_Product__c = 'CST_Standard';
			question.CST_Question__c = 'Question ' + i;
			question.CST_Question_Order__c = i;
			question.CST_Question_Type__c = 'Text';
			question.CST_Questionnaire_Type__c = 'Advisor';
			question.CST_Questionaire_HelpText__c = 'Hello Help';
			questions.add(question);
		}

		insert questions;
	}

	@isTest
	static void insertLineItems() {
		Case caseTest = [SELECT id FROM Case LIMIT 1];
		List<CST_Questionnaire_Line_Item__c> lineItems = new List<CST_Questionnaire_Line_Item__c>();
		for (Integer i = 1; i < 3; i++) {
			CST_Questionnaire_Line_Item__c line = new CST_Questionnaire_Line_Item__c();
			line.CST_Question__c = 'Question ' + i;
			line.CST_Answer__c = 'Answer ' + i;
			lineItems.add(line);
		}
		Test.startTest();
		CST_Questionnaire__c newQuest = CST_QuestionBank_Handler.handleApexSubmit(lineItems, caseTest.Id);
		CST_QuestionBank_Handler.checkExistingQuestionnaire(caseTest.Id);
		CST_Questionnaire_Handler.getQuestionnaireLineItems(newQuest.Id);
		Test.stopTest();
	}

	@isTest
	static void getQuestionsFromBank() {
		Case caseTest = [SELECT id FROM Case LIMIT 1];
		Test.startTest();
		List<CST_Question_Bank__c> questions = CST_QuestionBank_Handler.CST_GetQuestionsFromBank(caseTest.Id);
		Test.stopTest();
	}
}
