public with sharing class COM_DXL_Address {
	JSONGenerator generator;
	Map<String, String> addressData;

	public COM_DXL_Address(JSONGenerator generator, Map<String, String> addressData) {
		this.generator = generator;
		this.addressData = addressData;
	}

	public void generateAddress() {
		generator.writeStartObject();
		generator.writeStringField(COM_DXL_Constants.COM_DXL_UNIFY_ADDRESS_REFID, '');
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_ADDRESS_STREET,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(this.addressData.get(COM_DXL_Constants.COM_DXL_ADDRESS_STREET))
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUMBER,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(this.addressData.get(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUMBER))
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUM_EXT,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(this.addressData.get(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUM_EXT))
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_ADDRESS_POSTCODE,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(this.addressData.get(COM_DXL_Constants.COM_DXL_ADDRESS_POSTCODE))
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_ADDRESS_CITY,
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(this.addressData.get(COM_DXL_Constants.COM_DXL_ADDRESS_CITY))
		);
		generator.writeStringField(
			COM_DXL_Constants.COM_DXL_ADDRESS_COUNTRY,
			COM_DXL_CreateCustomerService.getDXLMapping(
				COM_DXL_Constants.COM_DXL_COUNTRY_MAPPING,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(this.addressData.get(COM_DXL_Constants.COM_DXL_ADDRESS_COUNTRY))
			)
		);
		generator.writeEndObject();
	}

	private void generateStringOrNullField(String fieldName, String fieldValue, String type) {
		if (type.equals('canvasAddress')) {
			if (String.isBlank(fieldValue)) {
				generator.writeNullField(fieldName);
			} else {
				generator.writeStringField(fieldName, fieldValue);
			}
		} else {
			if (String.isBlank(fieldValue)) {
				generator.writeStringField(fieldName, '');
			} else {
				generator.writeStringField(fieldName, fieldValue);
			}
		}
	}
}
