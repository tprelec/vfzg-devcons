@IsTest
private class PP_SearchAccountControllerTest {
	@IsTest
	static void testCreateAccountSharing() {
		Account partnerAccount = TestUtils.createPartnerAccount();
		Contact con = TestUtils.portalContact(partnerAccount);
		User portalUser = TestUtils.createPortalUser(partnerAccount);
		update con;

		portalUser = PP_TestUtils.getUser(portalUser.Id);

		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);

		System.runAs(portalUser) {
			PP_SearchAccountController.createAccountSharing(acct.Id);
		}

		List<AccountShare> result = [SELECT Id FROM AccountShare];
		System.assertEquals(true, result.size() > 0, 'Accounts should be shared');
	}

	@IsTest
	static void testSearchAccountServer() {
		Account partnerAccount = TestUtils.createPartnerAccount();
		Contact con = TestUtils.portalContact(partnerAccount);
		User portalUser = TestUtils.createPortalUser(partnerAccount);
		update con;

		User owner = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acct = TestUtils.createAccount(owner);
		acct.KVK_number__c = '12345678';
		acct.BAN_Number__c = '123456789';
		acct.Visiting_Postal_Code__c = '1234AB';
		insert acct;

		PP_SearchAccountController.SearchAccountParams params = new PP_SearchAccountController.SearchAccountParams();

		params.accountName = acct.Name;
		params.banNumber = '123456789';
		params.zipCode = '1234AB';
		params.searchOffset = 0;

		System.runAs(portalUser) {
			PP_SearchAccountController.SearchAccountResult result = PP_SearchAccountController.searchAccountsServer(
				JSON.serialize(params)
			);
			System.assertEquals(1, result.searchResults.size(), 'One account should be found');
			System.assertEquals(
				acct.Id,
				result.searchResults.get(0).account.Id,
				'Test account should be found'
			);
		}

		params.accountName = acct.Name;
		params.kvkNumber = '12345678';
		params.zipCode = '1234AB';

		System.runAs(portalUser) {
			PP_SearchAccountController.SearchAccountResult result = PP_SearchAccountController.searchAccountsServer(
				JSON.serialize(params)
			);
			System.assertEquals(1, result.searchResults.size(), 'One account should be found');
		}
	}

	@IsTest
	static void testSearchAccountServerInvalidParams() {
		Account partnerAccount = TestUtils.createPartnerAccount();
		Contact con = TestUtils.portalContact(partnerAccount);
		User portalUser = TestUtils.createPortalUser(partnerAccount);
		update con;

		User owner = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account acct = TestUtils.createAccount(owner);
		acct.KVK_number__c = '12345678';
		acct.BAN_Number__c = 'invalid';
		acct.Visiting_Postal_Code__c = '1';
		acct.BillingPostalCode = '2';
		insert acct;

		PP_SearchAccountController.SearchAccountParams params = new PP_SearchAccountController.SearchAccountParams();
		params.accountName = acct.Name;
		params.banNumber = 'invalid';
		params.kvkNumber = '123456789';
		params.zipCode = '12';
		params.searchOffset = 0;

		System.runAs(portalUser) {
			PP_SearchAccountController.SearchAccountResult result = PP_SearchAccountController.searchAccountsServer(
				JSON.serialize(params)
			);
			System.assertEquals(0, result.searchResults.size(), 'No accounts should be found');
		}
	}

	@IsTest
	static void testSearchAccountServerCreateAccount() {
		Account partnerAccount = TestUtils.createPartnerAccount();
		Contact con = TestUtils.portalContact(partnerAccount);
		User portalUser = TestUtils.createPortalUser(partnerAccount);
		update con;

		PP_SearchAccountController.SearchAccountParams params = new PP_SearchAccountController.SearchAccountParams();

		params.accountName = 'new Account';
		params.kvkNumber = '12345678';

		System.runAs(portalUser) {
			PP_SearchAccountController.SearchAccountResult result = PP_SearchAccountController.searchAccountsServer(
				JSON.serialize(params)
			);
			System.assertEquals(0, result.searchResults.size(), 'No accounts should be found');
		}
	}

	@IsTest
	static void testSearchAccountByKvkSFDCValid() {
		String kvkNumber = '12345678';

		Account partnerAccount = TestUtils.createPartnerAccount();
		Contact contact = TestUtils.portalContact(partnerAccount);
		User portalUser = TestUtils.createPortalUser(partnerAccount);
		update contact;

		User owner = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account account = TestUtils.createAccount(owner);
		account.KVK_number__c = kvkNumber;
		insert account;

		System.runAs(portalUser) {
			PP_SearchAccountController.SearchAccountResult result = PP_SearchAccountController.searchAccountByKvk(
				kvkNumber
			);
			System.assertNotEquals(null, result.account, 'One account should be found');
			System.assertEquals(account.Id, result.account.Id, 'Test account should be found');
		}
	}

	@IsTest
	static void testSearchAccountByKvkOlbico() {
		String kvkNumber = '12345678';

		Account partnerAccount = TestUtils.createPartnerAccount();
		Contact contact = TestUtils.portalContact(partnerAccount);
		User portalUser = TestUtils.createPortalUser(partnerAccount);
		update contact;

		User owner = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account account = TestUtils.createAccount(owner);
		insert account;

		System.runAs(portalUser) {
			PP_SearchAccountController.SearchAccountResult result = PP_SearchAccountController.searchAccountByKvk(
				kvkNumber
			);
			System.assertEquals(null, result.account, 'No account should be found');
		}
	}

	@IsTest
	static void testGetBppJourneys() {
		Account partnerAccount = TestUtils.createPartnerAccount();
		Contact contact = TestUtils.portalContact(partnerAccount);
		User portalUser = TestUtils.createPortalUser(partnerAccount);
		update contact;

		User owner = TestUtils.createAdministrator();
		TestUtils.autoCommit = false;
		Account account = TestUtils.createAccount(owner);
		insert account;

		System.runAs(portalUser) {
			List<PP_SearchAccountController.BPP_Journey> result = PP_SearchAccountController.getBppJourneys(
				account.Id
			);

			System.assertNotEquals(
				true,
				result.size() > 0,
				'There should be some BPP Journey Metadata defined.'
			);
		}
	}
}