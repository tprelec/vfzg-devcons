@isTest
private class CustomBtnSynchWithOppZiggoTest {
    
    @testsetup
    private static void setupTestData()
    {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        upsert noTriggers;
        
        Account account = LG_GeneralTest.CreateAccount('AccountSFDT', '12345678', 'Ziggo', date.today(), true);
        
        Opportunity opp = LG_GeneralTest.CreateOpportunity(account, true);
        
        cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', account, null, opp, false, system.today(), true);
        basket.csbb__Synchronised_With_Opportunity__c = true;
        insert basket;
        
        cscfga__Product_Basket__c basketT = [SELECT Id, name,cscfga__Basket_Status__c, cscfga__Opportunity__r.Id, csbb__Account__r.Id,LG_MobileCompetitorContractEndDate__c,LG_Mobile_Contract_End_Date_Unknown__c
                                            FROM cscfga__Product_Basket__c];
        System.debug('basketT.Name='+basketT.Name);
        noTriggers.Flag__c = false;
        upsert noTriggers;
    }

    private static testmethod void testPerformActionSuccess()
    {
        cscfga__Product_Basket__c basket = [SELECT Id, cscfga__Basket_Status__c, cscfga__Opportunity__r.Id, csbb__Account__r.Id,LG_MobileCompetitorContractEndDate__c,LG_Mobile_Contract_End_Date_Unknown__c
                                            FROM cscfga__Product_Basket__c];// WHERE Name = 'Basket'];
                                            
        Test.startTest();
            CustomButtonSynchronizeWithOpportunity cstBtn = new CustomButtonSynchronizeWithOpportunity();
            String performActionResult = cstBtn.performAction(basket.Id);
        Test.stopTest();
        
        basket = [SELECT Id, cscfga__Basket_Status__c, cscfga__Opportunity__r.Id,csbb__Account__r.Id,LG_MobileCompetitorContractEndDate__c,LG_Mobile_Contract_End_Date_Unknown__c, 
                            csordtelcoa__Synchronised_with_Opportunity__c,
                            csbb__Synchronised_With_Opportunity__c
                                            FROM cscfga__Product_Basket__c ];//WHERE Name = 'Basket'];
                                            
        System.assertEquals(true, basket.csbb__Synchronised_With_Opportunity__c,
                                'Basket should be synced with the opp');
        System.assertEquals(true, basket.csbb__Synchronised_With_Opportunity__c,
                                'Basket should be synced with the opp');
                                
        String oppId = basket.cscfga__Opportunity__r.Id;
        
        /*
        System.assertEquals('{"status":"ok","text":"Basket has been synced successfully","redirectURL":"/' + oppId.substring(0,15) + '"}',
                            performActionResult, 'result should match');
        */
        
    }

    private static testmethod void testPerformActionError()
    {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;
        
        cscfga__Product_Basket__c basket = [SELECT Id, cscfga__Basket_Status__c,cscfga__Products_In_Basket__c 
                                            FROM cscfga__Product_Basket__c]; //WHERE Name = 'Basket'];
        basket.cscfga__Basket_Status__c = 'SomethingDummy';
        update basket;
        
        noTriggers.Flag__c = false;
        upsert noTriggers;
        
        Test.startTest();
        CustomButtonSynchWithOpportunityZiggo cstBtn = new CustomButtonSynchWithOpportunityZiggo();
        String performActionResult = cstBtn.performAction(basket.Id);
        Test.stopTest();
        
        System.assertEquals(CustomButtonSynchWithOpportunityZiggo.errorMsg,
                            performActionResult, 'result should match');
        
        basket.cscfga__Basket_Status__c = 'Valid';
        basket.cscfga__Products_In_Basket__c='Archived';
        update basket;
        
        
        CustomButtonSynchWithOpportunityZiggo cstBtn1 = new CustomButtonSynchWithOpportunityZiggo();
        String performActionResult1 = cstBtn1.performAction(basket.Id);
        
        System.assertEquals(CustomButtonSynchWithOpportunityZiggo.validationMsg,
                            performActionResult1, 'result should match');
    }
    
    private static testmethod void testDeleteAndCreateOLIs()
    {
        No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
        noTriggers.Flag__c = true;
        noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
        upsert noTriggers;
        
        cscfga__Product_Basket__c basket = [SELECT Id, cscfga__Basket_Status__c,cscfga__Products_In_Basket__c 
                                            FROM cscfga__Product_Basket__c]; //WHERE Name = 'Basket'];
        
        noTriggers.Flag__c = false;
        upsert noTriggers;
        
        basket.cscfga__Basket_Status__c = 'Valid';
        basket.cscfga__Products_In_Basket__c='Test123';
        basket.LG_Mobile_Contract_End_Date_Unknown__c = true;
        basket.LG_MobileCompetitorContractEndDate__c=null;
        update basket;
        
        
        CustomButtonSynchWithOpportunityZiggo cstBtn1 = new CustomButtonSynchWithOpportunityZiggo();
        String performActionResult1 = cstBtn1.performAction(basket.Id);
        
        System.assertEquals(CustomButtonSynchWithOpportunityZiggo.validationMessageForCreationOfOLIs,
                            performActionResult1, 'result should match');
    }
}