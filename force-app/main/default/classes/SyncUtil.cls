public with sharing class SyncUtil {
    private static List<Field_Sync_Mapping__c> cacheQueriedMappings;
    private static Map<String, Map<String, String>> cacheFullMapping;
    private static Map<String, Map<String, Field_Sync_Mapping__c>> cacheFullMappingPlus;

    // Source_Target->Targetfield->Sourcefield
    public static Map<String, Map<String, String>> fullMapping(String mapping) {
        if (cacheFullMapping == null) {
            cacheFullMapping = new Map<String, Map<String, String>>();
            for (Field_Sync_Mapping__c fsm : getMappings()) {
                if (cacheFullMapping.containsKey(fsm.Object_Mapping__c)) {
                    cacheFullMapping.get(fsm.Object_Mapping__c).put(fsm.Target_Field_Name__c, fsm.Source_Field_Name__c);
                } else {
                    cacheFullMapping.put(fsm.Object_Mapping__c, new Map<String, String>{fsm.Target_Field_Name__c => fsm.Source_Field_Name__c});
                }
            }
        }
        Map<String, String> retrievedMapping = cacheFullMapping.get(mapping);
        if (retrievedMapping == null || retrievedMapping.isEmpty()) { throw new SyncUtilException('No mapping found for mapping: ' + mapping);}

        return new Map<String, Map<String, String>>{mapping => retrievedMapping};
    }

    public static Map<String, Map<String, Field_Sync_Mapping__c>> fullMappingPlus(String mapping) {
        if (cacheFullMappingPlus == null) {
            cacheFullMappingPlus = new Map<String, Map<String, Field_Sync_Mapping__c>>();
            for (Field_Sync_Mapping__c fsm : getMappings()) {
                if (cacheFullMappingPlus.containsKey(fsm.Object_Mapping__c)) {
                    cacheFullMappingPlus.get(fsm.Object_Mapping__c).put(fsm.Target_Field_Name__c, fsm);
                } else {
                    cacheFullMappingPlus.put(fsm.Object_Mapping__c, new Map<String, Field_Sync_Mapping__c>{fsm.Target_Field_Name__c => fsm});
                }
            }
        }
        Map<String, Field_Sync_Mapping__c> retrievedMapping = cacheFullMappingPlus.get(mapping);
        if (retrievedMapping == null || retrievedMapping.isEmpty()) { throw new SyncUtilException('No full mapping found for mapping: ' + mapping);}

        return new Map<String, Map<String, Field_Sync_Mapping__c>>{mapping => retrievedMapping};
    }

    private static List<Field_Sync_Mapping__c> getMappings() {
        if (cacheQueriedMappings == null) {
            cacheQueriedMappings = [
                SELECT Id, Object_Mapping__c, Source_Field_Name__c, Target_Field_Name__c, Insert_Only__c, Target_Default_Value__c, Target_Default_Value_Type__c
                FROM Field_Sync_Mapping__c
            ];
        }
        return cacheQueriedMappings;
    }

    public static set<String> getQueriedFields(map<String,String> BiccBanFieldMapping) {
        set<String> toBeReturned = new set<String>();
        toBeReturned.addall(BiccBanFieldMapping.values());
        return toBeReturned;
    }

    public static list<String> getSortedList(map<String, Schema.SObjectField> biccFieldMap) {
        List<String> sortList = new List<String>();
        for (Schema.SObjectField fieldDef : biccFieldMap.values()) {
            sortList.add(fieldDef.getDescribe().getName());
        }
        sortList.sort();
        return sortList;
    }

    public static String getHeader(List<String> sortList, set<String> queriedFields) {
        String str = '';
        for (String field : sortList) {
            if(queriedFields.contains(field)) {
                str += '"' + field + '",';
            }
        }
        str = str.subString(0, str.length() - 1) + '\n';
        return str;
    }

    public static sObject populateFields(SObject destinationRecord, SObject sourceRecord, Map<String, Field_Sync_Mapping__c> mapping, Map<String, Schema.SObjectField> destinationObjectSchemaFieldMap, Boolean modify) {
        for (String destinationField : mapping.keySet()) {
            String sourceField = mapping.get(destinationField).Source_Field_Name__c;
            Boolean insertOnly = mapping.get(destinationField).Insert_Only__c;

            if (destinationField.left(3) != 'N/A') {
                String destinationFieldtype = destinationObjectSchemaFieldMap.get(destinationField).getDescribe().getType().name();

                Object sourceValue = sourceRecord.get(sourceField);
                String stringSourceValue = String.valueOf(sourceValue);

                try {
                    if (destinationFieldtype == 'CURRENCY' && sourceValue != null) {
                        sourceValue = Decimal.valueOf(stringSourceValue);
                    } else if (destinationFieldtype == 'DOUBLE' && sourceValue != null) {
                        sourceValue = Decimal.valueOf(stringSourceValue);
                    } else if (destinationFieldtype == 'INTEGER' && sourceValue != null) {
                        sourceValue = Integer.valueOf(stringSourceValue);
                    } else if (destinationFieldtype == 'BOOLEAN' && sourceValue != null) {
                        sourceValue = Boolean.valueOf(stringSourceValue);
                    } else if (destinationFieldtype == 'DATE' && sourceValue != null) {
                        sourceValue = Date.valueOf(stringSourceValue);
                    } else {
                        //it stays what it was
                    }

                    // Don't overwrite values with null
                    if (sourceValue!=null) {
                        // If in modify mode then don't populate field if tagged as Insert Only
                        if (!modify || !insertOnly) {
                            destinationRecord.put(destinationField, sourceValue);
                        }
                    }
                } catch(Exception ex) {
                    sourceRecord.put('Error__c', sourceField + ':' + ex.getMessage());
                    System.debug('***SP*** exception while populating fields: '+ ex.getMessage());
                }
            }
        }
        return destinationRecord;
    }

    public static Map<String, Field_Sync_Mapping__c> generateSingleFullMappingFromObjectName(String objectName) {
        List<String> mappings = getMappingFromObjectName(objectName);
        if(mappings.size() > 1) {
            //too many mappings for that object found
            throw new MultipleMappingFoundException(String.Format(Label.Error_MultipleMappingFoundException, new String[]{objectName}));
        }
        return fullMappingPlus(mappings[0]).get(mappings[0]);
    }

    public static List<String> getMappingFromObjectName(String objectName) {
        Schema.DescribeFieldResult fieldResult = Field_Sync_Mapping__c.Object_Mapping__c.getDescribe();
        List<Schema.PicklistEntry> ples = fieldResult.getPicklistValues();
        Integer foundCounter = 0;
        List<String> mappings = new List<String>();

        for (Schema.PicklistEntry ple : ples) {
            if (ple.getValue().split(' ')[0] == objectName) {
                foundCounter++;
                mappings.add(ple.getValue());
            }
        }

        if (foundCounter == 0) {
            throw new NoMappingFoundException(String.Format(Label.Error_NoMappingFoundException, new String[]{objectName}));
        }
        return mappings;
    }

    public static String getDestinationObjectFromMapping(String objectName) {
        String mapping = getMappingFromObjectName(objectName)[0];
        String[] split = mapping.split(' ');
        String source = split[0];
        String destination = split[2];
        return destination;
    }

    public static SObject createSObjectWithDefaults(SObject source, Map<String, Field_Sync_Mapping__c> fieldToMappingMap, SObject ret) {
        for (String targetFieldName : fieldToMappingMap.keySet()) {
            Field_Sync_Mapping__c mappingRecord = fieldToMappingMap.get(targetFieldName);
            Object targetValue = String.isNotBlank(mappingRecord.Source_Field_Name__c) ? source.get(mappingRecord.Source_Field_Name__c) : null;
            if (targetValue == null && mappingRecord.Target_Default_Value__c != null) {
                targetValue = mappingRecord.Target_Default_Value__c;
                switch on mappingRecord.Target_Default_Value_Type__c {
                    when 'STRING' {
                        targetValue = String.valueOf(targetValue);
                    }
                    when 'BOOLEAN' {
                        targetValue = Boolean.valueOf(targetValue);
                    }
                    when 'INTEGER' {
                        targetValue = Integer.valueOf(targetValue);
                    }
                    when else {
                        throw new SyncUtilException(
                            'The following type is not supported as a default value for Field_Sync_Mapping__c: ' +
                            mappingRecord.Target_Default_Value_Type__c
                        );
                    }
                }
            }
            if (String.isBlank(String.valueOf(targetValue))) { continue;}
            ret.put(targetFieldName, targetValue);
        }
        return ret;
    }

    public class NoMappingFoundException extends Exception {}
    public class MultipleMappingFoundException extends Exception {}
    public class SyncUtilException extends Exception {}
}