/*
 * @author Rahul Sharma
 *
 * @description Controller for orderBillingValidation LWC component
 */

public without sharing class OrderBillingValidation {
	@TestVisible
	private static final String VALIDATION_WHO_ORDER_CHECK = 'Order Check';

	private static final String BASKET_API_NAME = '' + cscfga__Product_Basket__c.SObjectType;
	private static final String OPPORTUNITY_API_NAME = '' + Opportunity.SObjectType;
	private static final String CONTACT_ROLE_API_NAME = '' + Contact_Role__c.SObjectType;
	private static final String BAN_API_NAME = '' + Ban__c.SObjectType;
	private static final String FINANCIAL_ACCOUNT_API_NAME = '' + Financial_Account__c.SObjectType;
	private static final String BILLING_ARRANGEMENT_API_NAME = '' + Billing_Arrangement__c.SObjectType;

	private static final Set<String> SET_SUPPORTED_OBJECTS = new Set<String>{
		BAN_API_NAME,
		FINANCIAL_ACCOUNT_API_NAME,
		BILLING_ARRANGEMENT_API_NAME,
		CONTACT_ROLE_API_NAME
	};

	private static final Map<String, String> MAP_BILLING_OBJECTS_WITH_CONFIG_NAME = new Map<String, String>{
		BAN_API_NAME => 'Ban__r.',
		FINANCIAL_ACCOUNT_API_NAME => 'Financial_Account__r.',
		BILLING_ARRANGEMENT_API_NAME => 'Billing_Arrangement__r.'
	};

	@AuraEnabled
	public static LightningResponse getErrors(String billingObjectRecordId, String sourceRecordId) {
		try {
			List<String> allErrors = getAllErrors(billingObjectRecordId, sourceRecordId);
			return new LightningResponse().setBody(allErrors);
		} catch (Exception objEx) {
			LoggerService.log(objEx);
			return new LightningResponse().setError(objEx.getMessage());
		}
	}

	public static List<String> getAllErrors(String billingObjectRecordId, String sourceRecordId) {
		if (String.isEmpty(billingObjectRecordId)) {
			throw new BillingValidationException(System.Label.OrderBillingValidation_ValidIdRequired);
		}

		String validationObjectApiName = Id.valueOf(billingObjectRecordId).getSObjectType().getDescribe().getName();

		String sourceObjectApiName = Id.valueOf(sourceRecordId).getSObjectType().getDescribe().getName();

		if (!SET_SUPPORTED_OBJECTS.contains(validationObjectApiName)) {
			throw new BillingValidationException(getUnsupportedObjectMessage(billingObjectRecordId));
		}

		List<SObject> record = getBillingRecordToValidate(billingObjectRecordId, validationObjectApiName);

		SObject recordToValidate = record[0];

		List<String> allErrors = new List<String>();
		allErrors.addAll(performRunValidation(recordToValidate, validationObjectApiName, sourceObjectApiName));
		return allErrors;
	}

	/**
	 * @description Gets Opportunity with list of provided field
	 */
	@AuraEnabled
	public static Opportunity getOpportunity(Id oppId, List<String> fields) {
		String query = 'SELECT ';
		query += String.join(fields, ',');
		query += ' FROM Opportunity WHERE Id =: oppId';
		List<Opportunity> opportunityList = Database.query(query);
		if (!opportunityList.isEmpty()) {
			return opportunityList[0];
		}
		return null;
	}

	@TestVisible
	private static String getUnsupportedObjectMessage(Id recordId) {
		return String.format(
			System.Label.OrderBillingValidation_UnsupportedBillingObject,
			new List<String>{ recordId.getSObjectType().getDescribe().getLabel() }
		);
	}

	private static List<String> performRunValidation(sObject obj, String validationObjectApiName, String sourceObjectApiName) {
		List<String> errors = new List<String>();
		for (Order_Validation__c validation : OrderValidation.objectToValidations.get(validationObjectApiName.toLowerCase())) {
			// skip billing information where Who__c contains order check
			Boolean isBillingInformationValidation = ((sourceObjectApiName == BASKET_API_NAME || sourceObjectApiName == OPPORTUNITY_API_NAME) &&
			validation.Who__c?.containsIgnoreCase(VALIDATION_WHO_ORDER_CHECK));

			if (isBillingInformationValidation) {
				continue;
			}

			String error = OrderValidation.runValidation(obj, validation);
			if (String.isNotEmpty(error)) {
				errors.add(error.normalizeSpace());
			}
		}
		return errors;
	}

	// query the record that needs to be validation
	private static List<SObject> getBillingRecordToValidate(Id recordId, String validationObjectApiName) {
		List<String> lstAllFields = getBillingFieldsFromConfiguration(validationObjectApiName);

		String fieldsCsv = String.join(lstAllFields, ', ');

		String query = String.format('SELECT {0} FROM {1} WHERE Id = :recordId', new List<String>{ fieldsCsv, validationObjectApiName });

		List<SObject> recordIdToBillingData = Database.query(query);
		return recordIdToBillingData;
	}

	private static List<String> getBillingFieldsFromConfiguration(String objectName) {
		Set<String> setAllFields = new Set<String>{ 'Id' };
		// Get the fields from OrderValidationField__c
		for (OrderValidationField__c ovf : OrderValidationField__c.getAll().values()) {
			String fieldApiName;

			if (objectName == CONTACT_ROLE_API_NAME && ovf.object__c == CONTACT_ROLE_API_NAME) {
				// when object is contact role, use the field as it is
				fieldApiName = ovf.Field__c;
			} else if (MAP_BILLING_OBJECTS_WITH_CONFIG_NAME.containsKey(objectName)) {
				fieldApiName = removeConfigNameFromFieldApiName(objectName, ovf);
			}
			if (String.isNotEmpty(fieldApiName)) {
				setAllFields.add(fieldApiName);
			}
		}
		List<String> lstAllFields = new List<String>();
		lstAllFields.addAll(setAllFields);
		return lstAllFields;
	}

	private static String removeConfigNameFromFieldApiName(String objectName, OrderValidationField__c ovf) {
		String fieldApiName = '';
		if (ovf.object__c == String.valueOf(Billing_Arrangement__c.SObjectType)) {
			String configName = MAP_BILLING_OBJECTS_WITH_CONFIG_NAME.get(objectName);
			if (String.isNotEmpty(configName)) {
				// this is a billing object, extract the API name by removing config name
				fieldApiName = ovf.Field__c.substringAfter(configName);
			}
		}
		return fieldApiName;
	}

	public class BillingValidationException extends Exception {
	}
}
