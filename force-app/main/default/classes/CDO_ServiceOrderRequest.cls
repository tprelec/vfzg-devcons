public class CDO_ServiceOrderRequest{
	public String externalId;	//COM
	public String priority;	//1
	public String description;	//Internet Modem Only Service Order
	public String category;	//Internet Modem Only Service
	public String at_type;	//standard
	public cls_orderItem[] orderItem;
	public class cls_orderItem {
		public String id;	//1
		public String action;	//add
		public String at_type;	//standard
		public cls_service service;
	}
	public class cls_service {
		public String at_type;	//Service
		public cls_serviceSpecification serviceSpecification;
		public cls_serviceCharacteristic[] serviceCharacteristic;
	}
	public class cls_serviceSpecification {
		public String id;	//bdab731e-d5de-4135-8849-5c724ea01041
		public String invariantID;	//930d7161-0eb8-444f-8d11-5971812246d5
		public String version;	//1.0.0
		public String name;	//Internet Modem Only Service
		public String at_type;	//ServiceSpecification
	}
	public class cls_serviceCharacteristic {
		public String name;	//sourcesystem
		public String valueType;	//string
		public cls_value value;
	}
	public class cls_value {
		public String at_type;	//string
		public String value;	//SF
	}
	public static CDO_ServiceOrderRequest parse(String json){
		return (CDO_ServiceOrderRequest) System.JSON.deserialize(json, CDO_ServiceOrderRequest.class);
	}
    
    
	public static String serializeGoogleCalendarEvent(CDO_ServiceOrderRequest req){
		String json = JSON.serialize(req, true);
		return replacePropsToFitGoogleAPI(json);
	}
    
    	public static String replacePropsToFitGoogleAPI(String str) {
		str = str.replaceAll('"start":', '"start_xx":');
		str = str.replaceAll('"end":', '"end_xx":');
		str = str.replaceAll('"date":', '"date_xx":');
		str = str.replaceAll('"dateTime":', '"dateTime_xx":');
		str = str.replaceAll('"private":', '"private_xx":');
		return str;
	}
}