/**
 * @author: Rahul Sharma (07-09-2021)
 * @description: Perform ISW site check for requested Site record
 *      TestSitePostalCheckEventTriggerHandler covers unit test of this class
 *      Jira ticket reference: COM-1960
 * Note:
 *      WSDLtoApex doesn't support SOAP version 1.1 which was provided by ISW interface team, due to which a REST call is being made.
 */

public with sharing class ISWSiteCheckService {
    public static final String ACCESS_RESULT_CHECK_ONNET = 'ONNET';
    public static final String ACCESS_RESULT_CHECK_OFFNET = 'OFFNET';
    public static final String SPA_ACCESS_VENDOR_ZIGGO = 'ZIGGO';
    public static final String SPA_ACCESS_INFRASTRUCTURE_COAX = 'Coax';
    public static final String CHECKED_WITH_ISW_SUCCESS = 'Success';
    public static final String CHECKED_WITH_ISW_FAILED = 'Failed';

    public static void performISWSiteCheck(Id siteId) {
        List<Site__c> sites = [
            SELECT
                Id,
                Footprint__c,
                Site_Account__c,
                Site_Postal_Code__c,
                Site_House_Number__c,
                Site_House_Number_Suffix__c
            FROM Site__c
            WHERE Id = :siteId
        ];

        // System.debug('@@@@site: ' + sites);
        if (sites.size() == 1) {
            performISWSiteCheck(sites[0]);
        }
    }

    private static void performISWSiteCheck(Site__c site) {
        try {
            ISWSiteCheckServiceInformation objIswSiteCheckServiceInformation = getISWSiteCheckAPI(
                site
            );

            System.debug(
                '@@@@objIswSiteCheckServiceInformation: ' +
                objIswSiteCheckServiceInformation
            );

            new Flow.Interview.ISW_Site_Check_Handler(
                    new Map<String, Object>{
                        'siteId' => site.Id,
                        'siteCheckServiceInformation' => objIswSiteCheckServiceInformation
                    }
                )
                .start();

            // call handling of response
        } catch (Exception objEx) {
            System.debug('@@@@objEx: ' + objEx);
            System.debug(
                '@@@@objEx.getStackTraceString(): ' +
                objEx.getStackTraceString()
            );
            updateFailureOnSite(site.Id, objEx.getMessage());
        }
    }

    public static ISWSiteCheckServiceInformation getISWSiteCheckAPI(
        Site__c site
    ) {
        ISWSiteCheckAPI.ISWSiteRequest request = new ISWSiteCheckAPI.ISWSiteRequest(
            site.Id,
            site.Site_Postal_Code__c,
            String.valueOf(site.Site_House_Number__c),
            site.Site_House_Number_Suffix__c
        );
        // System.debug('@@@@request: ' + request);

        // System.debug('@@@@response: ' + response);
        ISWSiteCheckAPI.ISWSiteResponse iSWSiteResponse = new ISWSiteCheckAPI()
            .getServiceAvailability(request);
        return new ISWSiteCheckServiceInformation(iSWSiteResponse);
    }

    private static void updateFailureOnSite(Id siteId, String failureMessage) {
        update new Site__c(
            Id = siteId,
            Checked_With_ISW__c = CHECKED_WITH_ISW_FAILED,
            ISW_Error_Message__c = failureMessage
        );
    }
}