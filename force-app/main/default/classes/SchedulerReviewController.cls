/**
 *
 * @author      miguel.cubias@vodafoneziggo.com
 * @since       2022.11.01
 * @description provides apex methods for LWC
 */
public without sharing class SchedulerReviewController {
	/**
	 *
	 * @description
	 * @param appointmentTypeValue
	 * @return  `String`
	 * @exception AuraHandledException
	 */
	@AuraEnabled(cacheable=true)
	public static String getServiceAppointmentLabel(String appointmentTypeValue) {
		try {
			List<Schema.PicklistEntry> appointmentTypePicklistEntries = Schema.SObjectType.ServiceAppointment.fields.AppointmentType.getPicklistValues();
			for (Schema.PicklistEntry appointmentTypePicklistEntry : appointmentTypePicklistEntries) {
				if (appointmentTypePicklistEntry.getValue() == appointmentTypeValue) {
					return appointmentTypePicklistEntry.getLabel();
				}
			}
			throw new AuraHandledException(appointmentTypeValue + ' was not found as a valid option on ServiceAppointment.AppointmentType picklist');
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}
}
