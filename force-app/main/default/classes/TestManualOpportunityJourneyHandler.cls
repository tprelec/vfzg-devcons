@IsTest
private class TestManualOpportunityJourneyHandler {
	private static User portalUser;
	private static Account account;

	private static void createTestData() {
		portalUser = PP_TestUtils.createPortalUser();
		account = TestUtils.createAccount(portalUser);
	}

	@IsTest
	static void testCheckVisibilityTrue() {
		createTestData();

		Boolean checkVisibilityResult;

		Test.startTest();
		System.runAs(portalUser) {
			ManualOpportunityJourneyHandler manualOpportunityJourneyHandler = new ManualOpportunityJourneyHandler();
			checkVisibilityResult = manualOpportunityJourneyHandler.checkVisibility(account.Id);
		}
		Test.stopTest();

		System.assertEquals(true, checkVisibilityResult, 'Expected checkVisibility to be true.');
	}

	@IsTest
	static void testHandleClick() {
		createTestData();

		Map<String, Object> handleClickResult = new Map<String, Object>();

		Test.startTest();
		System.runAs(portalUser) {
			ManualOpportunityJourneyHandler manualOpportunityJourneyHandler = new ManualOpportunityJourneyHandler();
			handleClickResult = manualOpportunityJourneyHandler.handleClick(account.Id);
		}
		Test.stopTest();

		List<Opportunity> opportunityList = [SELECT Id FROM Opportunity];

		System.assertEquals(
			opportunityList.size(),
			handleClickResult.keySet().size(),
			'Number of created opportunities should match returned result.'
		);

		System.assert(handleClickResult.keySet().contains(opportunityList[0].Id), 'Returned opportunity must match created opportunity.');
	}
}
