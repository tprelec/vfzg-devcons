@SuppressWarnings('PMD.ExcessivePublicCount')
public with sharing class EmailService {
	private static Boolean isDeliverabilityActive = null;

	public static Boolean checkDeliverability() {
		if (isDeliverabilityActive == null) {
			try {
				Messaging.reserveSingleEmailCapacity(0);
				isDeliverabilityActive = true;
			} catch (System.NoAccessException e) {
				isDeliverabilityActive = false;
			}
		}

		return isDeliverabilityActive;
	}

	@InvocableMethod(label='Send Email' description='Sends email with option to attach files.' category='Email')
	public static void sendEmail(List<EmailRequest> requests) {
		for (EmailRequest request : requests) {
			if (request.emailHandler == null) {
				sendEmail(
					request.recipientId,
					request.relatedToId,
					request.toAddresses,
					request.ccAddresses,
					request.bccAddresses,
					request.templateId,
					request.stylinTemplateId,
					request.subject,
					request.attachmentIds,
					request.orgWideEmailId,
					request.useSignature
				);
			} else {
				Type t = Type.forName(request.emailHandler);
				IEmailHandler handler = (IEmailHandler) t.newInstance();
				Messaging.SingleEmailMessage mail = handler.handleEmail(request);
				checkDeliverabilityAndSend(mail);
			}
		}
	}

	public static void sendEmail(
		Id recipientId,
		Id relatedToId,
		List<String> toAddresses,
		List<String> ccAddresses,
		List<String> bccAddresses,
		Id templateId,
		List<Id> attachmentIds,
		Id orgWideEmailId
	) {
		sendEmail(recipientId, relatedToId, toAddresses, ccAddresses, bccAddresses, templateId, null, null, attachmentIds, orgWideEmailId, true);
	}

	@SuppressWarnings('PMD.ExcessiveParameterList')
	public static void sendEmail(
		Id recipientId,
		Id relatedToId,
		List<String> toAddresses,
		List<String> ccAddresses,
		List<String> bccAddresses,
		Id templateId,
		Id stylinTemplateId,
		String subject,
		List<Id> attachmentIds,
		Id orgWideEmailId,
		Boolean useSignature
	) {
		Boolean isUserRecepient = recipientId != null && recipientId.getSobjectType().getDescribe().getName() == 'User';
		Messaging.SingleEmailMessage mail;
		if (isUserRecepient && templateId != null && relatedToId != null) {
			mail = Messaging.renderStoredEmailTemplate(templateId, recipientId, relatedToId);
		} else {
			mail = new Messaging.SingleEmailMessage();
			mail.setTemplateId(templateId);
			setTargetObject(recipientId, mail);
			setRelatedTo(relatedToId, mail);
		}
		if (isUserRecepient) {
			mail.saveAsActivity = false;
		}
		setToAddresses(toAddresses, mail);
		setCcAddresses(ccAddresses, mail);
		setBccAddresses(bccAddresses, mail);
		setAttachments(attachmentIds, mail);
		setOrgWideEmail(orgWideEmailId, mail);
		setStylinTemplate(templateId, stylinTemplateId, mail);
		setSubject(subject, mail);
		if (useSignature != null) {
			mail.setUseSignature(useSignature);
		}
		checkDeliverabilityAndSend(mail);
	}

	// This method should be removed. Was added because of multiple recurring Copado merge issues regarding prepareEmail method.
	@SuppressWarnings('PMD.ExcessiveParameterList')
	public static Messaging.SingleEmailMessage prepareEmail(
		Id recipientId,
		Id relatedToId,
		List<String> toAddresses,
		List<String> ccAddresses,
		List<String> bccAddresses,
		Id templateId,
		Id stylinTemplateId,
		String subject,
		List<Id> attachmentIds,
		Id orgWideEmailId,
		Boolean useSignature
	) {
		return prepareEmail(
			recipientId,
			relatedToId,
			toAddresses,
			ccAddresses,
			bccAddresses,
			templateId,
			stylinTemplateId,
			subject,
			attachmentIds,
			orgWideEmailId
		);
	}

	@SuppressWarnings('PMD.ExcessiveParameterList')
	public static Messaging.SingleEmailMessage prepareEmail(
		Id recipientId,
		Id relatedToId,
		List<String> toAddresses,
		List<String> ccAddresses,
		List<String> bccAddresses,
		Id templateId,
		Id stylinTemplateId,
		String subject,
		List<Id> attachmentIds,
		Id orgWideEmailId
	) {
		Boolean isUserRecepient = recipientId != null && recipientId.getSobjectType().getDescribe().getName() == 'User';
		Messaging.SingleEmailMessage mail;
		if (isUserRecepient && templateId != null && relatedToId != null) {
			mail = Messaging.renderStoredEmailTemplate(templateId, recipientId, relatedToId);
		} else {
			mail = new Messaging.SingleEmailMessage();
			mail.setTemplateId(templateId);
			setTargetObject(recipientId, mail);
			setRelatedTo(relatedToId, mail);
		}
		if (isUserRecepient) {
			mail.saveAsActivity = false;
		}
		setToAddresses(toAddresses, mail);
		setCcAddresses(ccAddresses, mail);
		setBccAddresses(bccAddresses, mail);
		setAttachments(attachmentIds, mail);
		setOrgWideEmail(orgWideEmailId, mail);
		setStylinTemplate(templateId, stylinTemplateId, mail);
		setSubject(subject, mail);

		return mail;
	}

	private static void checkDeliverabilityAndSend(Messaging.SingleEmailMessage mail) {
		if (checkDeliverability()) {
			List<Messaging.SendEmailResult> result = Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ mail });
			if (!result[0].isSuccess()) {
				throw new EmailException(result[0].getErrors()[0].getMessage());
			}
		}
	}

	private static void setTargetObject(Id recipientId, Messaging.SingleEmailMessage mail) {
		if (recipientId != null) {
			mail.setTargetObjectId(recipientId);
		}
	}

	private static void setRelatedTo(Id relatedToId, Messaging.SingleEmailMessage mail) {
		if (relatedToId != null) {
			mail.setWhatId(relatedToId);
		}
	}

	private static void setToAddresses(List<String> toAddresses, Messaging.SingleEmailMessage mail) {
		if (toAddresses != null) {
			mail.setToAddresses(toAddresses);
		}
	}

	private static void setCcAddresses(List<String> ccAddresses, Messaging.SingleEmailMessage mail) {
		if (ccAddresses != null) {
			mail.setCcAddresses(ccAddresses);
		}
	}

	private static void setBccAddresses(List<String> bccAddresses, Messaging.SingleEmailMessage mail) {
		if (bccAddresses != null) {
			mail.setBccAddresses(bccAddresses);
		}
	}

	private static void setAttachments(List<Id> attachmentIds, Messaging.SingleEmailMessage mail) {
		if (attachmentIds != null) {
			mail.setEntityAttachments(attachmentIds);
		}
	}

	private static void setOrgWideEmail(Id orgWideEmailId, Messaging.SingleEmailMessage mail) {
		if (orgWideEmailId != null) {
			mail.setOrgWideEmailAddressId(orgWideEmailId);
		}
	}

	private static void setStylinTemplate(Id templateId, Id stylinTemplateId, Messaging.SingleEmailMessage mail) {
		EmailTemplate template = fetchTemplate(templateId);
		if (stylinTemplateId != null && template.HtmlValue != null) {
			String stylingBody = fetchTemplate(stylinTemplateId).HtmlValue;
			mail.setTreatBodiesAsTemplate(true);
			if (stylingBody != null) {
				stylingBody = stylingBody.replace('[EMAIL_CONTENT]', template.HtmlValue);
				stylingBody = stylingBody.replace('[EMAIL_COVER_PICTURE]', fetchCoverPictureAndPrepareTag(template.DeveloperName));
				mail.setHtmlBody(stylingBody);
			} else {
				mail.setHtmlBody(template.HtmlValue);
			}
		}
	}

	private static void setSubject(String subject, Messaging.SingleEmailMessage mail) {
		if (subject != null) {
			mail.setSubject(subject);
		}
	}

	@TestVisible
	private static EmailTemplate fetchTemplate(Id templateId) {
		EmailTemplate result = null;
		List<EmailTemplate> et = [SELECT Id, DeveloperName, HtmlValue, Markup FROM EmailTemplate WHERE Id = :templateId LIMIT 1];

		if (et.Size() != 0) {
			result = et[0];
		}

		return result;
	}

	@TestVisible
	private static String fetchCoverPictureAndPrepareTag(String templateName) {
		String result = '';
		if (COM_Constants.EMAIL_TEMPLATE_NAME_FILE_NAME_MAP.get(templateName) == null) {
			templateName = '';
		}

		String pictureName = COM_Constants.EMAIL_TEMPLATE_NAME_FILE_NAME_MAP.get(templateName);
		List<ContentDistribution> cdsList = new EmailService.ContentDistributionWrapper().getContentDistribution(pictureName);

		if (cdsList.size() > 0) {
			result = COM_Constants.COVER_PICTURE_TAG_START + cdsList[0].ContentDownloadUrl + COM_Constants.COVER_PICTURE_TAG_END;
		}

		return result;
	}

	private without sharing class ContentDistributionWrapper {
		private List<ContentDistribution> getContentDistribution(String pictureName) {
			return [
				SELECT Id, ContentDownloadUrl, DistributionPublicUrl
				FROM ContentDistribution
				WHERE ContentVersion.ContentDocument.Title = :pictureName
			];
		}
	}

	public class EmailRequest {
		@InvocableVariable(label='Recipient Id')
		public Id recipientId;

		@InvocableVariable(label='Related Record Id')
		public Id relatedToId;

		@InvocableVariable(label='To Addresses')
		public List<String> toAddresses;

		@InvocableVariable(label='CC Addresses')
		public List<String> ccAddresses;

		@InvocableVariable(label='BCC Addresses')
		public List<String> bccAddresses;

		@InvocableVariable(label='Email Template Id')
		public Id templateId;

		@InvocableVariable(label='Stylin Template Id')
		public Id stylinTemplateId;

		@InvocableVariable(label='Email Subject')
		public String subject;

		@InvocableVariable(label='Attachment Ids')
		public List<Id> attachmentIds;

		@InvocableVariable(label='Org Wide Email Address Id')
		public Id orgWideEmailId;

		@InvocableVariable(label='Email Handler Name')
		public String emailHandler;

		@InvocableVariable(label='Set Use Signature')
		public Boolean useSignature;
	}

	public class EmailException extends Exception {
	}

	public static OrgWideEmailAddress getOrgWideEmailAddress(String email) {
		List<OrgWideEmailAddress> orgWideEmailAddress = [SELECT Id, Address FROM OrgWideEmailAddress WHERE Address = :email];

		if (orgWideEmailAddress.size() > 0) {
			return orgWideEmailAddress[0];
		}

		return null;
	}
	public static OrgWideEmailAddress getDefaultOrgWideEmailAddress() {
		return getOrgWideEmailAddress('ebusalesforce@ext.vodafoneziggo.com');
	}
	public static OrgWideEmailAddress getCaseOrgWideEmailAddress() {
		return getOrgWideEmailAddress('salesforcecases@ext.vodafoneziggo.com');
	}
	public static OrgWideEmailAddress getDeliveryOrgWideEmailAddress() {
		return getOrgWideEmailAddress(COM_Constants.DELIVERY_ORG_WIDE_EMAIL_ADDRESS);
	}
}
