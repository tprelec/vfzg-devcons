@isTest
public with sharing class TestOrderEntryPromotionsController {
	private static List<OE_Product__c> oeProducts;
	private static List<OE_Add_On__c> oeAddons;

	@TestSetup
	static void makeData() {
		TestUtils.theAdministrator = new User(Id = UserInfo.getUserId());
		TestUtils.createCompleteOpportunity();
		MavenDocumentsTestFactory.createMavenDocumentsRecords();
	}

	@isTest
	static void testGetBundlePromotions() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		createProductsAndAddons();
		insertMockJsonData(opp.Id, opp.Id);
		List<OE_Promotion__c> promotions = new List<OE_Promotion__c>();
		OE_Promotion__c promotion = new OE_Promotion__c(
			Promotion_Name__c = 'De eerste maand 100% korting op uw Zakelijk Start abonnement',
			Type__c = 'Standard',
			Active__c = true,
			Customer_Type__c = 'Existing',
			Contract_Term__c = '1;2;3',
			Connection_Type__c = 'Off-Net',
			Off_net_Type__c = 'MTC'
		);

		promotions.add(promotion);
		insert promotions;

		createValidityPeriod(null, null, promotions, null);

		OE_Discount__c discount = new OE_Discount__c(
			Name = 'De eerste maand Safe Online XL gratis',
			Type__c = 'Percentage',
			Value__c = 100,
			Level__c = 'Bundle'
		);
		insert discount;

		insert new OE_Promotion_Discount__c(Promotion__c = promotion.Id, Discount__c = discount.Id);

		OrderEntryData oed = OrderEntryController.getOrderEntryData(opp.Id);
		Test.startTest();

		List<OE_Promotion__c> result = OrderEntryPromotionsController.getBundlePromotions(opp.Id, true, null, null, oed.bundles[0], oed.sites[0]);
		Test.stopTest();

		System.assertEquals(result.size(), 1, 'One bundle promotion should be returned.');
	}

	@isTest
	static void testGetSpecialPromotions() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		createProductsAndAddons();
		insertMockJsonData(opp.Id, opp.Id);
		List<OE_Promotion__c> promotions = new List<OE_Promotion__c>();

		OE_Promotion__c promo = new OE_Promotion__c(
			Promotion_Name__c = 'De eerste maand Ziggo Sport Totaal gratis',
			Type__c = 'Special',
			Active__c = true,
			Customer_Type__c = 'Existing',
			Contract_Term__c = '1;2;3',
			Connection_Type__c = 'Off-Net',
			Off_net_Type__c = 'MTC'
		);

		promotions.add(promo);
		insert promotions;
		List<OrderEntryData.OrderEntryProduct> jsonProducts = new List<OrderEntryData.OrderEntryProduct>();

		OrderEntryData.OrderEntryProduct prod1 = new OrderEntryData.OrderEntryProduct();
		prod1.id = oeProducts[0].Id;
		prod1.type = 'Main';
		jsonProducts.add(prod1);

		OrderEntryData.OrderEntryProduct prod2 = new OrderEntryData.OrderEntryProduct();
		prod2.id = oeProducts[1].Id;
		prod2.type = 'Internet';
		jsonProducts.add(prod2);

		OrderEntryData.OrderEntryProduct prod3 = new OrderEntryData.OrderEntryProduct();
		prod3.id = oeProducts[2].Id;
		prod3.type = 'TV';
		jsonProducts.add(prod3);

		List<OrderEntryData.OrderEntryAddon> jsonAddons = new List<OrderEntryData.OrderEntryAddon>();

		OrderEntryData.OrderEntryAddon addon1 = new OrderEntryData.OrderEntryAddon();
		addon1.bundleName = 'Safe Online';
		addon1.code = 'PD-00060';
		addon1.id = oeAddons[0].Id;
		addon1.name = 'Safe Online';
		addon1.oneOff = 0;
		addon1.parentProduct = oeProducts[1].Id;
		addon1.parentType = 'Internet';
		addon1.quantity = 1;
		addon1.recurring = 0;
		addon1.totalOneOff = 0;
		addon1.totalRecurring = 0;
		addon1.type = 'Internet Security';
		jsonAddons.add(addon1);

		OrderEntryData.OrderEntryAddon addon2 = new OrderEntryData.OrderEntryAddon();
		addon2.bundleName = 'Extra internetpunt via stopcontact';
		addon2.code = 'PD-00095';
		addon2.id = oeAddons[1].Id;
		addon2.name = 'Extra internetpunt via stopcontact';
		addon2.oneOff = 53.72;
		addon2.parentProduct = oeProducts[1].Id;
		addon2.parentType = 'Internet';
		addon2.quantity = 0;
		addon2.recurring = 0;
		addon2.totalOneOff = 0;
		addon2.totalRecurring = 0;
		addon2.type = 'Internet Additional';
		jsonAddons.add(addon2);

		createValidityPeriod(oeProducts, null, promotions, oeAddons);
		OrderEntryData oed = OrderEntryController.getOrderEntryData(opp.Id);
		Test.startTest();
		List<OE_Promotion__c> result = OrderEntryPromotionsController.getSpecialPromotions(opp.Id, true, null, null, oed.bundles[0], oed.sites[0]);
		Test.stopTest();

		System.assertEquals(result.size(), 1, 'One special promotion should be returned.');
	}

	@isTest
	static void testGetAutomaticPromotions() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];

		createProductsAndAddons();
		insertMockJsonData(opp.Id, opp.Id);
		List<OE_Promotion__c> promotions = new List<OE_Promotion__c>();

		OE_Promotion__c promo = new OE_Promotion__c(
			Promotion_Name__c = 'De eerste maand Ziggo Sport Totaal gratis',
			Customer_Type__c = 'New',
			Type__c = 'Automatic'
		);
		promotions.add(promo);
		insert promotions;

		List<OrderEntryData.OrderEntryProduct> jsonProducts = new List<OrderEntryData.OrderEntryProduct>();

		OrderEntryData.OrderEntryProduct prod1 = new OrderEntryData.OrderEntryProduct();
		prod1.id = oeProducts[0].Id;
		prod1.type = 'Main';
		jsonProducts.add(prod1);

		OrderEntryData.OrderEntryProduct prod2 = new OrderEntryData.OrderEntryProduct();
		prod2.id = oeProducts[1].Id;
		prod2.type = 'Internet';
		jsonProducts.add(prod2);

		OrderEntryData.OrderEntryProduct prod3 = new OrderEntryData.OrderEntryProduct();
		prod3.id = oeProducts[2].Id;
		prod3.type = 'TV';
		jsonProducts.add(prod3);

		List<OrderEntryData.OrderEntryAddon> jsonAddons = new List<OrderEntryData.OrderEntryAddon>();

		OrderEntryData.OrderEntryAddon addon1 = new OrderEntryData.OrderEntryAddon();
		addon1.bundleName = 'Safe Online';
		addon1.code = 'PD-00060';
		addon1.id = oeAddons[0].Id;
		addon1.name = 'Safe Online';
		addon1.oneOff = 0;
		addon1.parentProduct = oeProducts[1].Id;
		addon1.parentType = 'Internet';
		addon1.quantity = 1;
		addon1.recurring = 0;
		addon1.totalOneOff = 0;
		addon1.totalRecurring = 0;
		addon1.type = 'Internet Security';
		jsonAddons.add(addon1);

		OrderEntryData.OrderEntryAddon addon2 = new OrderEntryData.OrderEntryAddon();
		addon2.bundleName = 'Extra internetpunt via stopcontact';
		addon2.code = 'PD-00095';
		addon2.id = oeAddons[1].Id;
		addon2.name = 'Extra internetpunt via stopcontact';
		addon2.oneOff = 53.72;
		addon2.parentProduct = oeProducts[1].Id;
		addon2.parentType = 'Internet';
		addon2.quantity = 0;
		addon2.recurring = 0;
		addon2.totalOneOff = 0;
		addon2.totalRecurring = 0;
		addon2.type = 'Internet Additional';
		jsonAddons.add(addon2);

		createValidityPeriod(oeProducts, null, promotions, oeAddons);

		Test.startTest();
		List<OE_Promotion__c> result = OrderEntryPromotionsController.getAutomaticPromotions(opp.Id, false, null, '', '', jsonProducts, jsonAddons);
		Test.stopTest();

		System.assertEquals(result.size(), 1, 'One automatic promotion should be returned.');
	}

	static void createValidityPeriod(
		List<OE_Product__c> products,
		List<OE_Product_Add_On__c> addons,
		List<OE_Promotion__c> promotions,
		List<OE_Add_On__c> oeAddonsList
	) {
		List<OE_Validity_Period__c> peroiods = new List<OE_Validity_Period__c>();

		if (products != null) {
			for (OE_Product__c product : products) {
				peroiods.add(new OE_Validity_Period__c(From__c = Date.today(), To__c = Date.today(), Order_Entry_Product__c = product.Id));
			}
		}

		if (addons != null) {
			for (OE_Product_Add_On__c addon : addons) {
				peroiods.add(new OE_Validity_Period__c(From__c = Date.today(), To__c = Date.today(), Order_Entry_Add_On__c = addon.Add_On__c));
			}
		}

		if (promotions != null) {
			for (OE_Promotion__c promotion : promotions) {
				peroiods.add(new OE_Validity_Period__c(From__c = Date.today(), To__c = Date.today(), Order_Entry_Promotion__c = promotion.Id));
			}
		}
		if (OEAddonsList != null) {
			for (OE_Add_On__c addon : OEAddonsList) {
				peroiods.add(new OE_Validity_Period__c(From__c = Date.today(), To__c = Date.today(), Order_Entry_Add_On__c = addon.Id));
			}
		}
		insert peroiods;
	}

	static void createProductsAndAddons() {
		oeProducts = new List<OE_Product__c>();
		oeProducts.add(new OE_Product__c(Name = 'Main', Type__c = 'Main'));
		oeProducts.add(new OE_Product__c(Name = 'Internet', Type__c = 'Internet'));
		oeProducts.add(new OE_Product__c(Name = 'TV', Type__c = 'TV'));
		insert oeProducts;

		oeAddons = new List<OE_Add_On__c>();
		oeAddons.add(new OE_Add_On__c(Name = 'Safe Online', Product_Code__c = 'PD-00060', Type__c = 'Internet Security'));
		oeAddons.add(new OE_Add_On__c(Name = 'Extra internetpunt via stopcontact', Product_Code__c = 'PD-00095', Type__c = 'Internet Additional'));
		insert oeAddons;
	}

	static void insertMockJsonData(Id oppId, Id contactId) {
		String var =
			'{"opportunityId":"' +
			oppId +
			'",' +
			'"primaryContactId":"' +
			contactId +
			'",' +
			'"accountId":"' +
			[SELECT Id FROM Account][0]
			.Id +
			'",' +
			'"b2cInternetCustomer":false,' +
			'"lastStep":"Product Configuration",' +
			'"notes": "",' +
			'"sites": [{ "id": 1, "type": "Installation", "addressCheckResult": "Off-Net", "offNetType": "MTC", "siteCheck": {' +
			'"availability": [' +
			'{ "available": true, "name": "dtv-horizon" },' +
			'{ "available": false, "name": "packages" },' +
			'{ "available": false, "name": "mobile_internet" },' +
			'{ "available": true, "name": "fp200" },' +
			'{ "available": true, "name": "internet" },' +
			'{ "available": true, "name": "dtv" },' +
			'{ "available": true, "name": "catv" },' +
			'{ "available": true, "name": "telephony" },' +
			'{ "available": false, "name": "mobile" },' +
			'{ "available": false, "name": "catvfee" },' +
			'{ "available": true, "name": "fp500" },' +
			'{ "available": true, "name": "fp1000" }' +
			'],' +
			'"city": "DELFT",' +
			' "footprint": "ZIGGO",' +
			' "houseNumber": "5",' +
			' "houseNumberExt": "",' +
			' "status": [],' +
			'"street": "NOORDEINDE",' +
			'"zipCode": "2611KE"' +
			' },"siteId":"' +
			[SELECT Id FROM Site__c][0]
			.Id +
			'"}],' +
			'"bundles": [{' +
			'"bundle":{"bundleName":"Zakelijk Internet Start","code":"PD-00060","id":"aFQ5r0000004C9YGAU","name":"Zakelijk Internet Start","quantity":1,"recurring":42,"totalRecurring":42,"type":"Internet"},' +
			' "contractTerm": "3",' +
			'"addons":[{"bundleName":"Safe Online","code":"PD-00060","id":"' +
			oeAddons[0].Id +
			'","name":"Safe Online","oneOff":0,"parentProduct":"' +
			oeProducts[1].Id +
			'","parentType":"Internet","quantity":1,"recurring":0,"totalOneOff":0,"totalRecurring":0,"type":"Internet Security"},' +
			' {"bundleName":"Extra internetpunt via stopcontact","code":"PD-00095","id":"' +
			oeAddons[1].Id +
			'","name":"Extra internetpunt via stopcontact","oneOff":53.72,"parentProduct":"' +
			oeProducts[1].Id +
			'","parentType":"Internet","quantity":0,"recurring":0,"totalOneOff":0,"totalRecurring":0,"type":"Internet Additional"}],' +
			'"products":[{"id":"' +
			oeProducts[0].Id +
			'","type":"Main"},{"id":"' +
			oeProducts[1].Id +
			'","type":"Internet"},{"id":"' +
			oeProducts[2].Id +
			'","type":"TV"}],' +
			'"telephony": {},' +
			'"installation": {' +
			'"earliestInstallationDate": "2022-02-18",' +
			'"customerInformed": false' +
			'},' +
			'"operatorSwitch": {' +
			'"currentContractNumber": "",' +
			'"currentProvider": "",' +
			' "potentialFeeAccepted": false,' +
			'"requested": false' +
			'},' +
			'"promos":[{"connectionType":"On-net","contractTerms":"1;2;3","customerType":"New","discounts":[{"duration":1,"id":"aFT5r000000CaRcGAK","level":"Bundle","name":"De eerste maand 100% korting op uw Zakelijk Start abonnement","product":"' +
			oeProducts[1].Id +
			'","type":"Percentage","value":100}]' +
			'}]}],' +
			'"payment": {' +
			'"bankAccountHolder": "V. Kapoor",' +
			' "billingChannel": "Electronic Bill",' +
			' "iban": "",' +
			' "paymentType": "Bank Transfer"' +
			'}}';

		System.debug(var);
		insert new Attachment(Name = 'OrderEntryData.json', Body = Blob.valueOf(var), ParentId = oppId);
	}
}
