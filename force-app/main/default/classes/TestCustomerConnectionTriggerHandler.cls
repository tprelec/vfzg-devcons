/**
 * @description       : Apex Test Class for the CustomerConnectionTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestCustomerConnectionTriggerHandler {
	@testSetup
	static void setup() {
		List<ExternalIDNumber__c> lstExternalIDNumbers = new List<ExternalIDNumber__c>();

		ExternalIDNumber__c objExternalIDNumberCP = new ExternalIDNumber__c(
			Name = 'Commercial Product',
			External_Number__c = 1,
			Object_Prefix__c = 'CP-01-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		lstExternalIDNumbers.add(objExternalIDNumberCP);

		ExternalIDNumber__c objExternalIDNumberDI = new ExternalIDNumber__c(
			Name = 'Deal Item',
			External_Number__c = 1,
			Object_Prefix__c = 'DI-03-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		lstExternalIDNumbers.add(objExternalIDNumberDI);

		insert lstExternalIDNumbers;

		Deal_Item__c objDealItem = new Deal_Item__c();
		insert objDealItem;
	}

	@isTest
	static void testSetExternalIds() {
		Deal_Item__c objDealItem = [SELECT Id FROM Deal_Item__c LIMIT 1];
		CS_Customer_Connection__c objCustomerConnection = new CS_Customer_Connection__c(
			Cost_Type__c = 'Capex',
			Deal_Item__c = objDealItem.Id,
			Proposition__c = 'Data Only',
			Value__c = 150,
			Year__c = '2022'
		);

		Test.startTest();
		insert objCustomerConnection;
		Test.stopTest();

		CS_Customer_Connection__c objCustomerConnectionUpdate = [
			SELECT Id, ExternalID__c
			FROM CS_Customer_Connection__c
			WHERE Id = :objCustomerConnection.Id
			LIMIT 1
		];

		System.assertEquals(
			'CP-01-000002',
			objCustomerConnectionUpdate.ExternalID__c,
			'The External ID on Customer Connection was not set correctly.'
		);
	}
}
