@isTest
public with sharing class TestCOM_CancelDeliveryOrderController {
    
    private static string ORDER_NAME = 'TestOrder1';
    private static string DELIVERY_ORDER_NAME_1 = 'TestDeliveryOrder1';
    private static string DELIVERY_ORDER_NAME_2 = 'TestDeliveryOrder2';
    private static string CANCELLATION_REQUESTED_STATUS = 'Cancellation Requested';
    
    @TestSetup
    static void testSetup(){
        csord__Order__c order = CS_DataTest.createOrder();
        order.Name = ORDER_NAME;
        insert order;

        CS_DataTest.createDeliveryOrder(DELIVERY_ORDER_NAME_1, order.Id, true);
        CS_DataTest.createDeliveryOrder(DELIVERY_ORDER_NAME_2, order.Id, true);
    }
    
    @isTest
    static void testGetDeliveryOrderList() {
        List<csord__Order__c> orderList = [
            SELECT Id
            FROM csord__Order__c
            WHERE Name = :ORDER_NAME
            ];
        
        List<COM_Delivery_Order__c> deliveryOrderList = [
            SELECT Id
            FROM COM_Delivery_Order__c
            WHERE Name in (:DELIVERY_ORDER_NAME_1,:DELIVERY_ORDER_NAME_2)
        ];

        COM_CancelDeliveryOrderController.RequestWrap reqWrap = new COM_CancelDeliveryOrderController.RequestWrap();
        reqWrap.recordId = orderList[0].Id;

        Test.startTest();
        String result = COM_CancelDeliveryOrderController.getDeliveryOrderList(JSON.serialize(reqWrap));
        Test.stopTest();

        COM_CancelDeliveryOrderController.DeliveryOrderListWrap subListWrap = (COM_CancelDeliveryOrderController.DeliveryOrderListWrap) JSON.deserialize(result, COM_CancelDeliveryOrderController.DeliveryOrderListWrap.class);
        System.assertEquals(2, subListWrap.deliveryOrderList.size());
        System.assertEquals(deliveryOrderList[0].Id, subListWrap.deliveryOrderList[0].Id);
        System.assertEquals(deliveryOrderList[1].Id, subListWrap.deliveryOrderList[1].Id);
    }
    
    @IsTest
    static void testBehaviorCancelDeliveryOrder() {
        List<COM_Delivery_Order__c> deliveryOrderList = [
            SELECT Id
            FROM COM_Delivery_Order__c
            WHERE Name in (:DELIVERY_ORDER_NAME_1)
        ];

        COM_CancelDeliveryOrderController.RequestListWrap reqWrap = new COM_CancelDeliveryOrderController.RequestListWrap();
        reqWrap.deliveryOrderList = deliveryOrderList;

        Test.startTest();
        String result = COM_CancelDeliveryOrderController.cancelDeliveryOrder(JSON.serialize(reqWrap));
        Test.stopTest();

        COM_CancelDeliveryOrderController.ResponseWrap resultWrap = (COM_CancelDeliveryOrderController.ResponseWrap) JSON.deserialize(result, COM_CancelDeliveryOrderController.ResponseWrap.class);
        System.assertEquals(true, resultWrap.deliveryOrderCancelled);

        List<COM_Delivery_Order__c> updatedDeliveryOrderList = [
            SELECT Id, Status__c
            FROM COM_Delivery_Order__c
            WHERE Name in (:DELIVERY_ORDER_NAME_1,:DELIVERY_ORDER_NAME_2)
        ];

        System.assertEquals(CANCELLATION_REQUESTED_STATUS, updatedDeliveryOrderList[0].Status__c);
        System.assertNotEquals(CANCELLATION_REQUESTED_STATUS, updatedDeliveryOrderList[1].Status__c);
    }
}