public with sharing class CS_SolutionValidationService {
	@TestVisible
	private static final List<String> LIST_UNIQUE_SOLUTION_PER_ACCOUNT = new List<String>{ CS_ConstantsCOM.SOLUTION_DEFINITION_NAME_BIMO };
	private static final Boolean skipChecks = true;

	/*
	 * @description If already exists such solution on Account which has Is_Active__c = true, then we cannot allow new solutions of the same type if
	 * that solution is in the LIST_UNIQUE_SOLUTION_PER_ACCOUNT
	 */
	public static Boolean canAddAcquisitionSolution(Id accountId, String solutionName) {
		// only decomposed solutions will have csord__Account__c set
		if (skipChecks) {
			return true;
		}

		if (LIST_UNIQUE_SOLUTION_PER_ACCOUNT.contains(solutionName)) {
			List<csord__Solution__c> accountSolutions = [
				SELECT Id, Name, csord__Account__c, Is_Active__c, cssdm__solution_definition__r.Name
				FROM csord__Solution__c
				WHERE csord__Account__c = :accountId AND Is_Active__c = TRUE AND cssdm__solution_definition__r.Name = :solutionName
			];
			System.debug('canAddAcquisitionSolution - solutions ' + JSON.serializePretty(accountSolutions));
			return accountSolutions.isEmpty();
		}
		return true;
	}

	/*
	 * @description Checks whether solution from the basket is trying to replace some other Solution which is not active anymore
	 * if that is the case, this basket is outdated and cannot be synchroinzed with Opportunity
	 */
	public static Boolean isMacdBasketOutdated(String basketId) {
		List<csord__Solution__c> basketSolutions = [
			SELECT
				Id,
				cssdm__product_basket__c,
				cssdm__replaced_solution__c,
				cssdm__replaced_solution__r.Is_Active__c,
				cssdm__solution_definition__r.Name
			FROM csord__Solution__c
			WHERE cssdm__product_basket__c = :basketId
		];

		for (csord__Solution__c basketSolution : basketSolutions) {
			if (basketSolution.cssdm__replaced_solution__c != null && !basketSolution.cssdm__replaced_solution__r.Is_Active__c) {
				return true;
			}
		}
		return false;
	}

	/*
	 * Check whether basket contains solution falling under "unique solution per account" requirement and whether it can be synchronized
	 *
	 */

	public static Boolean canSyncBasket(cscfga__Product_Basket__c basket) {
		Map<String, csord__Solution__c> activeSolutions = new Map<String, csord__Solution__c>();
		System.debug('Calling canSyncBasket with input basket ' + basket);
		if (skipChecks) {
			return true;
		}

		List<csord__Solution__c> accountSolutions = [
			SELECT
				Id,
				Is_Active__c,
				cssdm__product_basket__c,
				csord__Account__c,
				cssdm__replaced_solution__c,
				cssdm__replaced_solution__r.Is_Active__c,
				cssdm__solution_definition__r.Name
			FROM csord__Solution__c
			WHERE
				(csord__Account__c = :basket.csbb__Account__c
				OR cssdm__product_basket__c = :basket.Id)
				AND cssdm__solution_definition__r.Name IN :LIST_UNIQUE_SOLUTION_PER_ACCOUNT
		];

		for (csord__Solution__c solution : accountSolutions) {
			if (solution.Is_Active__c && solution.cssdm__product_basket__c != basket.Id) {
				activeSolutions.put(solution.cssdm__solution_definition__r.Name, solution);
			}
		}
		System.debug('canSyncBasket activeSolutions: ' + JSON.serializePretty(activeSolutions));
		if (!activeSolutions.isEmpty()) {
			// if we have active solution and solution from current basket is not replacing that one, then we cannot sync such basket
			for (csord__Solution__c solution : accountSolutions) {
				csord__Solution__c activeSolution = activeSolutions.get(solution.cssdm__solution_definition__r.Name);
				if (
					activeSolution != null &&
					solution.cssdm__product_basket__c == basket.Id &&
					solution.cssdm__replaced_solution__c != activeSolution.Id
				) {
					return false;
				}
			}
		}
		return true;
	}

	/*
	 * Check whether primary basket of the opportunity we are moving to Close Won contains solution falling under "unique solution per account" requirement
	 * but there is already an account solution of the same type decomposed
	 */
	public static Boolean canCloseWon(Id opportunityId) {
		if (skipChecks) {
			return true;
		}
		// fields on Oppty --> Primary_Basket__c and csbb__Synchronised_with_Opportunity__c
		List<cscfga__Product_Basket__c> baskets = [
			SELECT Id, csbb__Account__c
			FROM cscfga__Product_Basket__c
			WHERE csordtelcoa__Synchronised_with_Opportunity__c = TRUE AND Primary__c = TRUE AND cscfga__Opportunity__c = :opportunityId
		];
		if (baskets.isEmpty()) {
			System.debug('canCloseWon returning false by default');
			return false;
		}
		return canSyncBasket(baskets[0]);
	}

	@InvocableMethod(label='Can close won Oppty with solution' description='Returns true if "unique solution per account" contraint is not violated')
	public static List<FlowOutput> canCloseWonInvocable(List<Id> listOpportunityId) {
		System.debug('Calling canCloseWonInvocable with input ' + listOpportunityId[0]);
		Boolean result = canCloseWon(listOpportunityId[0]);
		System.debug('Result of canCloseWonInvocable ' + result);
		return new List<FlowOutput>{ new FlowOutput(result) };
	}

	public class FlowOutput {
		@InvocableVariable
		public Boolean canCloseResult;

		public FlowOutput(Boolean inputResult) {
			this.canCloseResult = inputResult;
		}
	}
}
