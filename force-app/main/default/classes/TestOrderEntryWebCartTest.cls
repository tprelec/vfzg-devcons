@IsTest
public class TestOrderEntryWebCartTest {
	@TestSetup
	static void makeData() {
		// TBD prepare test opportunity and other data
	}

	@IsTest
	static void testCreateCart() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCreateJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TDB
	}

	@IsTest
	static void testSetChosenOrderType() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCarChosenOrderTypeJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testSetPersonalData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCustomerPersonalJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testSetContactData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCustomerContactJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testSetIdentificationData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartCustomerIdentificationJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testSetCompanyData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCustomerCompanyJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testSetDeliveryAddressData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartDeliveryAddressJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testAddItemsToCart() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartItemsJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testSetTermsAgreed() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartTermsJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testSetSummaryTermsAgreed() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartSummaryTermsJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testSetPaymentData() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebCartPaymentJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		// TBD
	}

	@IsTest
	static void testPlaceOrder() {
		// TBD place order
	}

	@IsTest
	static void testProcessOrders() {
		// TBD test with one opportunity Id
	}
}
