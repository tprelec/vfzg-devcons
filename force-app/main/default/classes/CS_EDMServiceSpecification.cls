/**
 * Container class for CloudSense External Data Mapping (CSEDM) package data
 */
public with sharing class CS_EDMServiceSpecification {

    public String status;
    // Globally Unique Identifier
    public String guid;
    public String startDate;
    public String name;
    public String description;
    public String version;
    public String replacedSpecification;
    public String identifier;
    public String endDate;
    public String code;
    public String instanceId;
    public String productConfigurationId;
    public List<SimpleAttribute> simpleAttributes;
    public Map<String, List<ComplexAttribute>> complexAttributes;
    public List<SimpleAttribute> additionalSimpleAttributes;
    public String serviceId;

    public CS_EDMServiceSpecification() {}

    public class SimpleAttribute {
        public String name;
        public String value;

        public SimpleAttribute(String name, String value) {
            this.name = name;
            this.value = value;
        }
    }

    public class ComplexAttribute {
        public String productConfigurationId;
        public List<SimpleAttribute> simpleAttributes;

        public ComplexAttribute() {}
    }
}