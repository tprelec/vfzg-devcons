public with sharing class ContractRelatedQuestionsController {
	@TestVisible
	private static String featureFlaggingDisableBpaGeneration = 'DisableBPAGeneration';

	private static final Map<String, String> TEMPLATE_NAME_TO_AGREEMENT_SUFFIX = new Map<String, String>{
		'Direct Sales Template 2' => '--2',
		Contract_Generation_Settings__c.getInstance().Document_template_name_BPA__c => '-BPA'
	};

	/**
	 * Gets Product Basket and related details.
	 * @param  basketId Product Basket ID.
	 * @return          Product Basket.
	 */
	@AuraEnabled
	public static cscfga__Product_Basket__c getBasketDetails(String basketId) {
		List<cscfga__Product_Basket__c> basketList = [
			SELECT
				Id,
				Name,
				csordtelcoa__Basket_Stage__c,
				cscfga__Basket_Status__c,
				OwnerAccount__c,
				Primary__c,
				csbb__Synchronised_with_Opportunity__c,
				cscfga__Opportunity__r.Name,
				csbb__Account__r.Authorized_to_sign_1st__r.Name,
				csbb__Account__r.Authorized_to_sign_2nd__r.Name,
				csbb__Account__r.Contract_rule_no_mailing__c,
				csbb__Account__r.Frame_Work_Agreement__c,
				csbb__Account__r.Version_FWA__c,
				csbb__Account__r.Framework_agreement_date__c,
				cscfga__Opportunity__c,
				cscfga__Opportunity__r.Direct_Indirect__c,
				Tracking_purposes__c,
				Sign_on_behalf_of_customer__c,
				Fiber_connection_in_different_agreement__c,
				Existing_Contract_ID__c,
				Existing_Contract_Date__c,
				One_Mobile_install_base__c,
				Existing_Mobile_Contract_ID__c,
				Existing_Mobile_Contract_Date__c,
				Vodafone_fixed_phone_service__c,
				Existing_Fixed_Contract_ID__c,
				Existing_Fixed_Contract_Date__c,
				OneNetE_with_OneMobileOneBusiness__c,
				Mobile_integrated_with_OneFixed__c,
				Create_new_FWA__c,
				Latest_TandC__c,
				Support_use_of_fax__c,
				Sellthrough_connectivity__c,
				cscfga__Opportunity__r.Account.Authorized_to_sign_1st__c,
				MobileScenarios__c,
				cscfga__Products_In_Basket__c,
				cscfga__Opportunity__r.Account.Frame_Work_Agreement__c,
				cscfga__Opportunity__r.Account.VZ_Framework_Agreement__c,
				Contract_Language__c,
				BMS_Products__c,
				DirectIndirect__c,
				Optional_Clauses__c,
				OneNet_Scenario__c,
				OneNet_Flex_Scenario__c,
				(SELECT Id, Name, Mobile_Scenario__c, Fixed_Scenario__c, OneNet_Scenario__c FROM cscfga__Product_Configurations__r)
			FROM cscfga__Product_Basket__c
			WHERE Id = :basketId
		];

		if (!basketList.isEmpty()) {
			return basketList[0];
		} else {
			return null;
		}
	}

	/**
	 * Gets Contract Settings from Metadata.
	 * @param  productBasket Product Basket.
	 * @return               List of relevant settings.
	 */
	@AuraEnabled
	public static List<Contract_Custom_Settings__mdt> getContractCustomSettings(cscfga__Product_Basket__c productBasket) {
		List<Contract_Custom_Settings__mdt> contractSettings = [
			SELECT
				BMS_Grouping__c,
				Document_template__c,
				English_contract__c,
				Mobile_Scenario__c,
				Product_Configuration__c,
				Product_Name__c,
				Product_Code__c
			FROM Contract_Custom_Settings__mdt
		];
		List<cscfga__Product_Configuration__c> configs = getProductsFromBasket(productBasket.Id);
		String productsInBasket = '';

		for (cscfga__Product_Configuration__c config : configs) {
			productsInBasket += '[' + config.Name + ']';
		}

		List<Contract_Custom_Settings__mdt> applicableSettings = new List<Contract_Custom_Settings__mdt>();
		String mobileScenarios = productBasket.MobileScenarios__c;
		String bmsProducts = productBasket.BMS_Products__c;
		String oneNetScenario = productBasket.OneNet_Scenario__c;
		String oneNetFlexScenario = productBasket.OneNet_Flex_Scenario__c;

		for (Contract_Custom_Settings__mdt contractSetting : contractSettings) {
			Boolean isApplicable = false;

			if (contractSetting.Product_Configuration__c != null) {
				List<String> productConfigurations = new List<String>();

				if (contractSetting.Product_Configuration__c.contains(';')) {
					productConfigurations.addAll(contractSetting.Product_Configuration__c.split(';'));
				} else {
					productConfigurations.add(contractSetting.Product_Configuration__c);
				}

				for (String productConfiguration : productConfigurations) {
					if (productsInBasket != '' && productsInBasket.containsIgnoreCase(productConfiguration)) {
						if (
							productConfiguration.equalsIgnoreCase('Mobile CTN Profile') &&
							contractSetting.Mobile_Scenario__c != null &&
							mobileScenarios != null &&
							mobileScenarios.containsIgnoreCase(contractSetting.Mobile_Scenario__c)
						) {
							isApplicable = true;
						}
						if (
							productConfiguration.equalsIgnoreCase('Business Managed Services') &&
							contractSetting.BMS_Grouping__c != null &&
							bmsProducts != null &&
							bmsProducts.containsIgnoreCase(contractSetting.BMS_Grouping__c)
						) {
							isApplicable = true;
						}
						if (productConfiguration.equalsIgnoreCase('One Net')) {
							if (
								oneNetScenario != null &&
								oneNetScenario.equals('One Net Express') &&
								contractSetting.Product_Name__c.equals('One Net Express')
							) {
								isApplicable = true;
							} else if (
								((oneNetScenario != null && oneNetScenario.equals('One Net Enterprise')) ||
								(oneNetFlexScenario != null && oneNetFlexScenario.equals('One Net Enterprise Flex'))) &&
								contractSetting.Product_Name__c.equals('One Net')
							) {
								isApplicable = true;
							}
						}
						if (productConfiguration.equalsIgnoreCase('Managed Internet') && !productsInBasket.containsIgnoreCase('One Net')) {
							isApplicable = true;
						}
						if (
							!productConfiguration.equalsIgnoreCase('One Net') &&
							!productConfiguration.equalsIgnoreCase('Mobile CTN Profile') &&
							!productConfiguration.equalsIgnoreCase('Business Managed Services') &&
							!productConfiguration.equalsIgnoreCase('Managed Internet')
						) {
							isApplicable = true;
						}
					}
				}
			}

			if (isApplicable) {
				applicableSettings.add(contractSetting);
			}
		}

		return applicableSettings;
	}

	/**
	 * Gets Current User details.
	 * @return   Current User.
	 */
	@AuraEnabled
	public static User getCurrentUser() {
		User currentUser = [SELECT Id, Name, Partner_User__c FROM User WHERE Id = :userinfo.getUserId()];
		return currentUser;
	}

	/**
	 * Creates Business Partner Agreement if needed.
	 * @param  basket Product Basket
	 * @return        Agreement if created, null otherwise.
	 */
	@AuraEnabled
	public static csclm__Agreement__c createBPAIfNeeded(cscfga__Product_Basket__c basket) {
		csclm__Agreement__c agreement;
		if (
			!FeatureFlagging.isActive(featureFlaggingDisableBpaGeneration) &&
			basket.cscfga__Opportunity__r.Direct_Indirect__c == 'Indirect' &&
			mobileExists(basket)
		) {
			String templateName = Contract_Generation_Settings__c.getInstance().Document_template_name_BPA__c;
			agreement = getExistingOrNewAgreement(basket, templateName);
		}
		return agreement;
	}

	/**
	 * Checks if Mobile product exists in Basket
	 * @param  basket Product Basket.
	 * @return        True if mobile products exist, false otherwise.
	 */
	public static Boolean mobileExists(cscfga__Product_Basket__c basket) {
		Boolean result = false;
		for (cscfga__Product_Configuration__c config : getProductsFromBasket(String.valueOf(basket.Id))) {
			if (
				config.cscfga__Product_Definition__r.Name == 'Mobile CTN addons' ||
				config.cscfga__Product_Definition__r.Name == 'Mobile CTN subscription'
			) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * Checks if existing Agreement already exists for given Basket and Template.
	 * Creates new Agreement with associated Template if existing is not found.
	 * Sets Agreement Status to Approved
	 * @param  basket       Product Basket.
	 * @param  templateName Template Name.
	 * @return              Agreement.
	 */
	private static csclm__Agreement__c getExistingOrNewAgreement(cscfga__Product_Basket__c basket, String templateName) {
		// Check existing
		csclm__Agreement__c agreement = getExistingAgreement(basket, templateName);
		if (agreement == null) {
			// Create new
			agreement = createNewAgreement(basket, templateName);
		}
		// Always set status to approved (even if newely created)
		agreement.csclm__Approval_Status__c = 'Approved';
		update agreement;
		return agreement;
	}

	/**
	 * Checks if existing Agreement already exists for given Basket and Template.
	 * @param  basket       Product Basket.
	 * @param  templateName Template Name.
	 * @return              Agreement if found, null otherwise.
	 */
	private static csclm__Agreement__c getExistingAgreement(cscfga__Product_Basket__c basket, String templateName) {
		List<csclm__Agreement__c> agreements = [
			SELECT Id, Name, csclm__Document_Template__r.Name, Product_Basket__c
			FROM csclm__Agreement__c
			WHERE Product_Basket__c = :basket.Id AND csclm__Document_Template__r.Name = :templateName
		];
		if (!agreements.isEmpty()) {
			return agreements[0];
		}
		return null;
	}

	/**
	 * Creates new Agreement for given Basket and Template.
	 * Invokes PDF generation.
	 * @param  basket       Product Basket.
	 * @param  templateName Template Name.
	 * @return              Agreement.
	 */
	private static csclm__Agreement__c createNewAgreement(cscfga__Product_Basket__c basket, String templateName) {
		csclm__Document_Template__c documentTemplate = getTemplate(templateName);
		csclm__Agreement__c newAgreement = new csclm__Agreement__c(
			Name = basket.Name,
			RecordTypeId = RecordTypeService.getRecordTypeIdByName('csclm__Agreement__c', 'Standard'),
			Product_Basket__c = basket.Id,
			csclm__Opportunity__c = basket.cscfga__Opportunity__c,
			csclm__Document_Template__c = documentTemplate.Id,
			csclm__Output_Format__c = 'pdf',
			csclm__Requires_Update__c = false,
			csclm__Approval_Status__c = 'Approved'
		);

		if (TEMPLATE_NAME_TO_AGREEMENT_SUFFIX.containsKey(templateName)) {
			newAgreement.Name += TEMPLATE_NAME_TO_AGREEMENT_SUFFIX.get(templateName);
		}

		insert newAgreement;

		GenerateDocumentQueueable generatePdfJob = new GenerateDocumentQueueable(newAgreement.Id);
		Id jobID = System.enqueueJob(generatePdfJob);

		return newAgreement;
	}

	/**
	 * Gets CLM Document Template.
	 * @param  templateName Template Name.
	 * @return              CLM Document Template if found, null otherwise.
	 */
	private static csclm__Document_Template__c getTemplate(String templateName) {
		List<csclm__Document_Template__c> documentTemplates = [
			SELECT Id, Name, csclm__Valid__c, csclm__Active__c
			FROM csclm__Document_Template__c
			WHERE Name = :templateName AND csclm__Valid__c = TRUE AND csclm__Active__c = TRUE
			LIMIT 1
		];
		if (!documentTemplates.isEmpty()) {
			return documentTemplates[0];
		}
		return null;
	}

	/**
	 * Creates new agreement or gets existing for provided Basket.
	 * Depending if it is Direct/Indirect, takes different template.
	 * @param  basket Product Basket.
	 * @return        Agreement.
	 */
	@AuraEnabled
	public static csclm__Agreement__c createAgreement(cscfga__Product_Basket__c basket) {
		return createAgreement2(basket, basket.cscfga__Opportunity__r.Direct_Indirect__c);
	}

	/**
	 * Creates new agreement or gets existing for provided Basket.
	 * Depending on provided template type, takes different template.
	 * @param  basket   Product Basket.
	 * @param  template Template Type
	 * @return          Agreement.
	 */
	@AuraEnabled
	public static csclm__Agreement__c createAgreement2(cscfga__Product_Basket__c basket, String template) {
		String templateName = '';

		switch on template {
			when 'Indirect' {
				templateName = Contract_Generation_Settings__c.getInstance().Document_template_name_indirect__c;
			}
			when 'Direct' {
				templateName = Contract_Generation_Settings__c.getInstance().Document_template_name_direct__c;
			}
			when 'Direct 2' {
				templateName = Contract_Generation_Settings__c.getInstance().Document_template_name_direct_2__c;
			}
			when 'Direct English' {
				templateName = Contract_Generation_Settings__c.getInstance().Document_template_name_direct_English__c;
			}
			when 'Indirect English' {
				templateName = Contract_Generation_Settings__c.getInstance().Document_template_name_indirect_English__c;
			}
			when else {
				templateName = Contract_Generation_Settings__c.getInstance().Document_template_name_direct__c;
			}
		}
		return getExistingOrNewAgreement(basket, templateName);
	}

	@AuraEnabled
	public static Boolean updateBasketStatus(cscfga__Product_Basket__c basket, String value) {
		basket.cscfga__Basket_Status__c = value;

		Database.SaveResult saveResult = Database.Update(basket, false);

		if (saveResult.isSuccess()) {
			return true;
		} else {
			return false;
		}
	}

	@AuraEnabled
	public static Boolean updateBasket(cscfga__Product_Basket__c basket, Map<String, Object> fieldsToUpdate) {
		for (String key : fieldsToUpdate.keySet()) {
			Schema.DisplayType dt = SObjectService.getFieldDisplayType(cscfga__Product_Basket__c.getSobjectType(), key);
			if (dt == Schema.DisplayType.Date) {
				if (fieldsToUpdate.get(key) == null) {
					basket.put(key, null);
				} else {
					String d = fieldsToUpdate.get(key).toString();
					String[] strList = d.split('-');
					Date dte = Date.newInstance(Integer.valueOf(strList[0]), Integer.valueOf(strList[1]), Integer.valueOf(strList[2]));
					basket.put(key, dte);
				}
			} else if (dt == Schema.DisplayType.Boolean) {
				if (fieldsToUpdate.get(key) == null) {
					basket.put(key, false);
				} else {
					basket.put(key, Boolean.valueOf(fieldsToUpdate.get(key)));
				}
			} else {
				if (fieldsToUpdate.get(key) == null) {
					basket.put(key, null);
				} else {
					basket.put(key, fieldsToUpdate.get(key));
				}
			}
		}

		Database.SaveResult saveResult = Database.Update(basket, false);

		if (saveResult.isSuccess()) {
			return true;
		} else {
			return false;
		}
	}

	@AuraEnabled
	public static List<cscfga__Product_Configuration__c> getProductsFromBasket(String basketId) {
		List<cscfga__Product_Configuration__c> pcList = [
			SELECT
				ID,
				Name,
				cscfga__Product_Basket__c,
				Mobile_Scenario__c,
				Fixed_Scenario__c,
				OneNet_Scenario__c,
				cscfga__Product_Definition__c,
				cscfga__Product_Definition__r.Name
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c = :basketId
		];

		return pcList;
	}
}
