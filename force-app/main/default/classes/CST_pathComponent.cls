public with sharing class CST_pathComponent {
	@AuraEnabled
	public static Boolean getUserInfo(String CaseId) {
		String usrId = UserInfo.getUserId();
		String userName = UserInfo.getUserName();

		System.debug('CaseId: ' + CaseId);
		List<UserRecordAccess> ura = [
			SELECT RecordId, HasReadAccess, HasEditAccess, HasTransferAccess, MaxAccessLevel
			FROM UserRecordAccess
			WHERE
				Userid = :usrId // UserInfo.getuserName()
				AND RecordID = :CaseId
		];

		Boolean returnValue = ura != null && ura.size() > 0;

		System.debug('returnValue: ' + returnValue);

		return returnValue;
	}
}
