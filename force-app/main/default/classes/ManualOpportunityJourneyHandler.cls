public with sharing class ManualOpportunityJourneyHandler implements IJourney {
	public Boolean checkVisibility(String accountId) {
		return true;
	}

	public Map<String, Object> handleClick(String accountId) {
		Map<String, Object> opportunityToReturn = new Map<String, Object>();
		Account account = [SELECT Id, Name FROM Account WHERE Id = :accountId];

		Opportunity opportunity = new Opportunity();
		opportunity.Name = account.Name + ' / ' + Label.pp_Enter_Short_Description;
		opportunity.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('MAC').getRecordTypeId();
		opportunity.StageName = 'Offer';
		opportunity.CloseDate = Date.today().addMonths(1);
		opportunity.AccountId = accountId;
		opportunity.Select_Journey__c = 'Sales';
		opportunity.Special_project_type__c = 'Other';
		insert opportunity;

		opportunityToReturn.put(opportunity.Id, opportunity);

		return opportunityToReturn;
	}
}
