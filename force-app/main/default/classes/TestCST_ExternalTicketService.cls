@IsTest
public class TestCST_ExternalTicketService {
	@TestSetup
	static void setupTestData() {
		String recordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CST_Incident').getRecordTypeId();
		String e2eOwnerId = UserInfo.getUserId();
		User advisor = TestUtils.generateTestUser('cst', 'advisor', 'System Administrator');
		insert advisor;

		System.runAs(advisor) {
			Case cstCase = new Case(
				RecordtypeId = recordtypeId,
				Origin = 'Internal',
				Status = 'New',
				CST_End2End_Owner__c = e2eOwnerId,
				OwnerId = advisor.Id,
				Remedy_Ticket_No__c = '123'
			);

			insert cstCase;

			CST_External_Ticket__c externalTicket = new CST_External_Ticket__c(
				CST_Case__c = cstCase.Id,
				CST_IncidentId__c = '565656',
				CST_Summary__c = 'test case',
				CST_Status__c = 'Draft',
				CST_Ticket_Priority__c = 'Low'
			);
			insert externalTicket;
		}
	}

	@IsTest
	static void testAssigned() {
		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = '565656';
		reqData.status = 'Assigned';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(200, resp.statusCode, 'Request should result with success');

		Case cstCase = [SELECT Id, Status, CST_Sub_Status__c FROM Case LIMIT 1];

		System.assertEquals('New', cstCase.Status, 'Case status should be unchanged');

		CST_External_Ticket__c externalTicket = [
			SELECT Id, CST_Status__c, CST_Assignee__c, CST_Ticket_Priority__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			LIMIT 1
		];

		System.assertEquals('Assigned', externalTicket.CST_Status__c, 'ExternalTicket status should be updated');
		System.assertEquals('Luis', externalTicket.CST_Assignee__c, 'ExternalTicket assignee should be updated');
		System.assertEquals('Medium', externalTicket.CST_Ticket_Priority__c, 'ExternalTicket priority should be updated');
	}

	@IsTest
	static void testInProgress() {
		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = '565656';
		reqData.status = 'In Progress';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(200, resp.statusCode, 'Request should result with success');

		Case cstCase = [SELECT Id, Status, CST_Sub_Status__c, CST_End2End_Owner__c, OwnerId FROM Case LIMIT 1];

		System.assertEquals('In progress', cstCase.Status, 'Case status should be updated');
		System.assertEquals('Working on it', cstCase.CST_Sub_Status__c, 'Case substatus should be updated');
		System.assert(cstCase.CST_End2End_Owner__c != cstCase.OwnerId, 'Case owner should be unchanged');

		CST_External_Ticket__c externalTicket = [
			SELECT Id, CST_Status__c, CST_Assignee__c, CST_Ticket_Priority__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			LIMIT 1
		];

		System.assertEquals('In Progress', externalTicket.CST_Status__c, 'ExternalTicket status should be updated');
		System.assertEquals('Luis', externalTicket.CST_Assignee__c, 'ExternalTicket assignee should be updated');
		System.assertEquals('Medium', externalTicket.CST_Ticket_Priority__c, 'ExternalTicket priority should be updated');
	}

	@IsTest
	static void testPending() {
		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = '565656';
		reqData.status = 'Pending';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(200, resp.statusCode, 'Request should result with success');

		Case cstCase = [
			SELECT Id, Status, CST_Sub_Status__c, OwnerId, CST_End2End_Owner__c, CST_Case_Is_With_Team__c, Remedy_Ticket_No__c
			FROM Case
			LIMIT 1
		];

		System.assertEquals('In progress', cstCase.Status, 'Case status should be changed');
		System.assertEquals('Working on it', cstCase.CST_Sub_Status__c, 'Case substatus should be changed');
		System.assert(cstCase.CST_End2End_Owner__c == cstCase.OwnerId, 'Case owner should be updated');
		String e2eOwnerId = UserInfo.getUserId();
		System.assertEquals(e2eOwnerId, cstCase.OwnerId, 'Case owner should be updated to E2E owner');
		System.assertEquals('Internal - Advisor', cstCase.CST_Case_Is_With_Team__c, 'Case team should be changed');

		CST_External_Ticket__c externalTicket = [
			SELECT Id, CST_Status__c, CST_Assignee__c, CST_Ticket_Priority__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			LIMIT 1
		];

		System.assertEquals('Pending', externalTicket.CST_Status__c, 'ExternalTicket status should be updated');
		System.assertEquals('Client Action Required', externalTicket.CST_Status_Reason__c, 'ExternalTicket status reason should be updated');
		System.assertEquals('Luis', externalTicket.CST_Assignee__c, 'ExternalTicket assignee should be updated');
		System.assertEquals('Medium', externalTicket.CST_Ticket_Priority__c, 'ExternalTicket priority should be updated');

		Task newTask = [
			SELECT Id, Subject, OwnerId, WhatId, Status, Type, RecordTypeId, IsREminderSet, ReminderDateTime, ActivityDate
			FROM Task
			LIMIT 1
		];

		System.assertNotEquals(null, newTask, 'New task is created');
		System.assertEquals('Remedy Ticket updated to Pending', newTask.Subject, 'Unexpected Task subject');
		String usrId = UserInfo.getUserId();
		System.assertEquals(usrId, newTask.OwnerId, 'Unexpected Task owner');
		System.assertEquals(cstCase.Id, newTask.WhatId, 'Unexpected Task whatId');
		System.assertEquals('Open', newTask.Status, 'Unexpected Task status');
		System.assertEquals('Other', newTask.Type, 'Unexpected Task type');
		RecordType taskRecordType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'CST_New_Task' LIMIT 1];
		System.assertEquals(taskRecordType.Id, newTask.RecordTypeId, 'Unexpected Task record type');
		System.assertEquals(true, newTask.IsReminderSet, 'Task reminder should be set');
	}

	@IsTest
	static void testResolved() {
		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = '565656';
		reqData.status = 'Resolved';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(200, resp.statusCode, 'Request should result with success');

		Case cstCase = [
			SELECT Id, Status, CST_Sub_Status__c, OwnerId, CST_End2End_Owner__c, CST_Case_Is_With_Team__c, Remedy_Ticket_No__c
			FROM Case
			LIMIT 1
		];

		System.assertEquals('Resolved', cstCase.Status, 'Case status should be changed');
		System.assertEquals(null, cstCase.CST_Sub_Status__c, 'Case substatus should be changed');
		System.assert(cstCase.CST_End2End_Owner__c == cstCase.OwnerId, 'Case owner should be updated');
		String e2eOwnerId = UserInfo.getUserId();
		System.assertEquals(e2eOwnerId, cstCase.OwnerId, 'Case owner should be updated to E2E owner');
		System.assertEquals(null, cstCase.CST_Case_Is_With_Team__c, 'Case team should be changed');

		CST_External_Ticket__c externalTicket = [
			SELECT Id, CST_Status__c, CST_Assignee__c, CST_Ticket_Priority__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			LIMIT 1
		];

		System.assertEquals('Resolved', externalTicket.CST_Status__c, 'ExternalTicket status should be updated');
		System.assertEquals('Luis', externalTicket.CST_Assignee__c, 'ExternalTicket assignee should be updated');
		System.assertEquals('Medium', externalTicket.CST_Ticket_Priority__c, 'ExternalTicket priority should be updated');
	}

	@IsTest
	static void testCancelled() {
		CST_External_Ticket__c externalTicket = [SELECT Id, CST_Status__c FROM CST_External_Ticket__c LIMIT 1];
		externalTicket.CST_Status__c = 'Pending';
		update externalTicket;

		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = '565656';
		reqData.status = 'Cancelled';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(200, resp.statusCode, 'Request should result with success');

		Case cstCase = [
			SELECT Id, Status, CST_Sub_Status__c, OwnerId, CST_End2End_Owner__c, CST_Case_Is_With_Team__c, Remedy_Ticket_No__c
			FROM Case
			LIMIT 1
		];

		System.assertEquals('In progress', cstCase.Status, 'Case status should be changed');
		System.assertEquals('Working on it', cstCase.CST_Sub_Status__c, 'Case substatus should be changed');
		System.assert(cstCase.CST_End2End_Owner__c == cstCase.OwnerId, 'Case owner should be updated');
		String e2eOwnerId = UserInfo.getUserId();
		System.assertEquals(e2eOwnerId, cstCase.OwnerId, 'Case owner should be updated to E2E owner');
		System.assertEquals('Internal - Advisor', cstCase.CST_Case_Is_With_Team__c, 'Case team should be changed');

		externalTicket = [
			SELECT Id, CST_Status__c, CST_Assignee__c, CST_Ticket_Priority__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			LIMIT 1
		];

		System.assertEquals('Cancelled', externalTicket.CST_Status__c, 'ExternalTicket status should be updated');
		System.assertEquals('Luis', externalTicket.CST_Assignee__c, 'ExternalTicket assignee should be updated');
		System.assertEquals('Medium', externalTicket.CST_Ticket_Priority__c, 'ExternalTicket priority should be updated');

		Task newTask = [
			SELECT Id, Subject, OwnerId, WhatId, Status, Type, RecordTypeId, IsREminderSet, ReminderDateTime, ActivityDate
			FROM Task
			LIMIT 1
		];

		System.assertNotEquals(null, newTask, 'New task is created');
		System.assertEquals('Remedy Ticket updated to Cancelled', newTask.Subject, 'Unexpected Task subject');
		String usrId = UserInfo.getUserId();
		System.assertEquals(usrId, newTask.OwnerId, 'Unexpected Task owner');
		System.assertEquals(cstCase.Id, newTask.WhatId, 'Unexpected Task whatId');
		System.assertEquals('Open', newTask.Status, 'Unexpected Task status');
		System.assertEquals('Other', newTask.Type, 'Unexpected Task type');
		RecordType taskRecordType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'CST_New_Task' LIMIT 1];
		System.assertEquals(taskRecordType.Id, newTask.RecordTypeId, 'Unexpected Task record type');
		System.assertEquals(true, newTask.IsReminderSet, 'Task reminder should be set');
	}

	@IsTest
	static void testClosed() {
		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = '565656';
		reqData.status = 'Closed';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(200, resp.statusCode, 'Request should result with success');

		Case cstCase = [
			SELECT Id, Status, CST_Sub_Status__c, OwnerId, CST_End2End_Owner__c, CST_Case_Is_With_Team__c, Remedy_Ticket_No__c
			FROM Case
			LIMIT 1
		];

		System.assertEquals('New', cstCase.Status, 'Case status should be unchanged');

		CST_External_Ticket__c externalTicket = [
			SELECT Id, CST_Status__c, CST_Assignee__c, CST_Ticket_Priority__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			LIMIT 1
		];

		System.assertEquals('Closed', externalTicket.CST_Status__c, 'ExternalTicket status should be updated');
		System.assertEquals('Luis', externalTicket.CST_Assignee__c, 'ExternalTicket assignee should be updated');
		System.assertEquals('Medium', externalTicket.CST_Ticket_Priority__c, 'ExternalTicket priority should be updated');
	}

	@IsTest
	static void testNoStatusChange() {
		CST_External_Ticket__c externalTicket = [SELECT Id, CST_Status__c FROM CST_External_Ticket__c LIMIT 1];
		externalTicket.CST_Status__c = 'In Progress';
		update externalTicket;

		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = '565656';
		reqData.status = 'In Progress';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(200, resp.statusCode, 'Request should result with success');

		Case cstCase = [
			SELECT Id, Status, CST_Sub_Status__c, OwnerId, CST_End2End_Owner__c, CST_Case_Is_With_Team__c, Remedy_Ticket_No__c
			FROM Case
			LIMIT 1
		];

		System.assertEquals('New', cstCase.Status, 'Case status should be unchanged');

		externalTicket = [
			SELECT Id, CST_Status__c, CST_Assignee__c, CST_Ticket_Priority__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			LIMIT 1
		];

		System.assertEquals('In Progress', externalTicket.CST_Status__c, 'ExternalTicket status should be unchanged');
		System.assertEquals('Luis', externalTicket.CST_Assignee__c, 'ExternalTicket assignee should be updated');
		System.assertEquals('Medium', externalTicket.CST_Ticket_Priority__c, 'ExternalTicket priority should be updated');
	}

	@IsTest
	static void testInvalidStatusTransition() {
		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = '565656';
		reqData.status = 'Invalid status';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(409, resp.statusCode, 'Request should result with conflict');
		System.assertEquals(
			'Invalid status transition status=Invalid status. Allowed statuses: Assigned, In Progress, Pending, ' + 'Resolved, Cancelled, Closed',
			resp.responseBody.toString(),
			'Unexpected response body error message'
		);

		Case cstCase = [
			SELECT Id, Status, CST_Sub_Status__c, OwnerId, CST_End2End_Owner__c, CST_Case_Is_With_Team__c, Remedy_Ticket_No__c
			FROM Case
			LIMIT 1
		];

		System.assertEquals('New', cstCase.Status, 'Case status should be unchanged');

		CST_External_Ticket__c externalTicket = [
			SELECT Id, CST_Status__c, CST_Assignee__c, CST_Ticket_Priority__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			LIMIT 1
		];

		System.assertEquals('Draft', externalTicket.CST_Status__c, 'ExternalTicket status should be unchanged');
		System.assertEquals(null, externalTicket.CST_Assignee__c, 'ExternalTicket assignee should be unchanged');
		System.assertEquals('Low', externalTicket.CST_Ticket_Priority__c, 'ExternalTicket priority should be updated');
	}

	@IsTest
	static void testExernalTicketNotFound() {
		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = 'invalid-id';
		reqData.status = 'In Progress';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(404, resp.statusCode, 'Request should result with not found');
		System.assertEquals(
			'External Ticket with incidentId=invalid-id not found',
			resp.responseBody.toString(),
			'Unexpected response body error message'
		);
	}

	@IsTest
	static void testAttemptToUpdateExternalTicketRelatedToCancelledCase() {
		Case cstCase = [SELECT Id, Status FROM Case LIMIT 1];
		cstCase.Status = 'Cancelled'; //or Closed
		update cstCase;

		CST_ExternalTicketService.ExternalTicketRequest reqData = new CST_ExternalTicketService.ExternalTicketRequest();
		reqData.incidentId = '565656';
		reqData.status = 'In Progress';
		reqData.assignee = 'Luis';
		reqData.priority = 'Medium';

		RestRequest req = new RestRequest();
		String baseUrl = URL.getSalesforceBaseUrl().toExternalForm();
		req.requestUri = baseUrl + '/services/apexrest/cst/external-tickets';
		req.httpMethod = 'PATCH';
		req.requestBody = Blob.valueOf(JSON.serializePretty(reqData));
		RestContext.request = req;

		RestResponse resp = new RestResponse();
		RestContext.response = resp;

		Test.startTest();
		CST_ExternalTicketService.doPatch();
		Test.stopTest();

		System.assertEquals(409, resp.statusCode, 'Request should result with conflict');
		System.assertEquals(
			'Not allowed to update external tickets related to closed or cancelled cases',
			resp.responseBody.toString(),
			'Unexpected response body error message'
		);
	}
}
