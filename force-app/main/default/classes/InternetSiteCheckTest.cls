@isTest
public with sharing class InternetSiteCheckTest {
    @isTest
    private static void testSiteAvailability() {

        Test.startTest();

        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;

        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('AccountID', String.valueOf(testAccount.Id));
        searchFields.put('Technology', 'Coax');

        Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112',testAccount, '1111AA', 'Breestraat','LEIDEN', 112); 
        insert site;
        
        Site_Postal_Check__c spc = CS_DataTest.createSite(site);
        insert spc;
        
        Site_Availability__c siteAvailability = CS_DataTest.createSiteAvailability('LEIDEN, Breestraat 112', site, spc); 
        siteAvailability.Access_Infrastructure__c = 'Coax';
        insert siteAvailability;

        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
        cscfga__Product_Definition__c accessPd = CS_DataTest.createProductDefinition('Access');
        accessPd.Product_Type__c = 'Fixed';
        accessPd.RecordTypeId = productDefinitionRecordType;
        insert accessPd;

        AccessSiteCheck accessSiteCheck = new AccessSiteCheck();
        List<Object> result = accessSiteCheck.getSites(searchFields, String.valueOf(accessPd.Id), null, 0, 0);
        System.assertEquals(0, result.size());

        Test.stopTest();


    }
}