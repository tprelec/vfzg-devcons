@IsTest
private class TestCS_MobileFlowService {
    @IsTest
    static void testUpdateTaskOwnership() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();
        User simpleUser2 = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            VF_Contract__c vfContract1 = CS_DataTest.createDefaultVfContract(simpleUser, true);

            List<Case> cases = new List<Case>();
            Case case1 = CS_DataTest.createCase('Product Cleanup Test', 'CS MF Product Cleanup', simpleUser, false);
            case1.Contract_VF__c = vfContract1.Id;
            cases.add(case1);
            Case case2 = CS_DataTest.createCase('Product Cleanup Test 2', 'CS MF Product Cleanup', simpleUser, false);
            case2.Contract_VF__c = vfContract1.Id;
            cases.add(case2);
            insert cases;

            Test.startTest();
            Set<String> caseRecordTypes = new Set<String>();

            for (Case c : cases) {
                String recordTypeName = RecordTypeService.getRecordTypeNameById('Case', c.RecordTypeId);
                if (recordTypeName.startsWith(CS_MobileFlowService.recordTypePrefix)) {
                    caseRecordTypes.add(recordTypeName.substringAfter(CS_MobileFlowService.recordTypePrefix));
                }
            }

            List<String> groups = new List<String>{
                    'G1', 'G2'
            };
            CS_MobileFlowService.mobileFlowMetadata = CS_DataTest.createMobileFlowMetadata(caseRecordTypes, groups);
            CS_MobileFlowService.createTasksForCases(cases);

            case1.OwnerId = simpleUser2.Id;
            upsert case1;

            Map<Id, Case> casesMap = new Map<Id, Case>();
            casesMap.put(case1.Id, case1);

            CS_MobileFlowService.updateTaskOwnership(casesMap);

            List<Task> caseOneTasks = [
                SELECT Id,
                    OwnerId
                FROM Task
                WHERE WhatId = :case1.Id
            ];

            for (Task t : caseOneTasks) {
                System.assertEquals(t.OwnerId, case1.OwnerId);
            }
            Test.stopTest();
        }
    }

    @IsTest
    static void testHandleSmallImplementation() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();

            VF_Contract__c vfContract1 = createVfContractWithAdditionalData(simpleUser, true, 'Mobile', 'Retention', 0);

            List<Case> cases = new List<Case>();
            Case case1 = CS_DataTest.createCase('Small Implementation', 'CS MF Small Implementation', simpleUser, false);
            case1.Contract_VF__c = vfContract1.Id;
            cases.add(case1);
            Case case2 = createCaseWithKtoData(simpleUser, vfContract1, true, 'test@test.com');
            cases.add(case2);
            insert cases;

            case2.Status = 'Closed';
            update case2;

            assertKtoData(case1, case2);
            Test.stopTest();
        }
    }

    @IsTest
    static void testProductCleanup() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();

            VF_Contract__c vfContractPc1 = createVfContractWithAdditionalData(simpleUser, false, 'Mobile', 'Retention', 0);

            List<Case> cases = new List<Case>();
            Case casePc1 = CS_DataTest.createCase('Product Cleanup', 'CS MF Product Cleanup', simpleUser, false);
            casePc1.Contract_VF__c = vfContractPc1.Id;
            cases.add(casePc1);
            Case casePc2 = createCaseWithKtoData(simpleUser, vfContractPc1, true, 'test@test.com');
            cases.add(casePc2);
            insert cases;

            casePc2.Status = 'Closed';
            update casePc2;

            assertKtoData(casePc1, casePc2);
            Test.stopTest();
        }
    }

    @IsTest
    static void testHandleProvideMobileSubscribers() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();

            VF_Contract__c vfContractPc1 = createVfContractWithAdditionalData(simpleUser, false, 'Mobile', 'Acquisition', 0);

            List<Case> cases = new List<Case>();
            Case case1 = CS_DataTest.createCase('Provide Mobile Subscribers', 'CS MF Provide Mobile Subscribers', simpleUser, false);
            case1.Contract_VF__c = vfContractPc1.Id;
            cases.add(case1);
            Case case2 = createCaseWithKtoData(simpleUser, vfContractPc1, true, 'test@test.com');
            cases.add(case2);
            insert cases;

            case2.Status = 'Closed';
            update case2;

            assertKtoData(case1, case2);
            Test.stopTest();
        }
    }

    @IsTest
    static void testProcessRework() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();

            VF_Contract__c vfContract1 = createVfContractWithAdditionalData(simpleUser, false, 'Mobile', 'Retention', 0);

            List<Case> cases = new List<Case>();
            Case case1 = CS_DataTest.createCase('Process Rework', 'CS MF Process Rework', simpleUser, false);
            case1.Contract_VF__c = vfContract1.Id;
            cases.add(case1);
            Case case2 = createCaseWithFqcData(simpleUser, vfContract1, 'Final Quality Check', 'CS MF Final Quality Check', 'FQC Comment', 'Number of Add-ons not correct', 5 , 'Passed');
            cases.add(case2);
            insert cases;

            case2.Status = 'Closed';
            update case2;

            List<Case> caseList = [
                    SELECT Id, Case_Record_Type_Text__c, Contract_VF__c
                    FROM Case
                    WHERE Id = :case1.Id
            ];

            CS_MobileFlowService.setCaseFields(caseList);
            List<Case> newCase = [
                    SELECT Id, FQC_Comment__c, FQC_Failure_Reason__c
                    FROM Case
                    WHERE Id = :case1.Id
            ];

            for (Case c : newCase) {
                System.assertEquals(case2.FQC_Comment__c, c.FQC_Comment__c);
                System.assertEquals(case2.FQC_Failure_Reason__c, c.FQC_Failure_Reason__c);
            }
            Test.stopTest();
        }
    }

    @IsTest
    static void testFqcProvideMobileSubscribers() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();

            VF_Contract__c vfContract1 = createVfContractWithAdditionalData(simpleUser,false,'Mobile','Acquisition',0);

            List<Case> cases = new List<Case>();
            Case case1 = createCaseWithFqcData(simpleUser,vfContract1,'Final Quality Check','CS MF Final Quality Check', null, null, null, null);
            cases.add(case1);
            Case case2 = createCaseCommentsForFqc(simpleUser, vfContract1, 'Provide Mobile Subscribers', 'CS MF Provide Mobile Subscribers');
            cases.add(case2);
            insert cases;

            case2.Status = 'Closed';
            update case2;

            finalQualityCheckAssertCommentsForFqc(case1, case2);

            Test.stopTest();
        }
    }

    @IsTest
    static void testFqcProductCleanup() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();
            VF_Contract__c vfContract1 = createVfContractWithAdditionalData(simpleUser,false,'Mobile','Retention',0);

            List<Case> cases = new List<Case>();
            Case case1 = createCaseWithFqcData(simpleUser, vfContract1, 'Final Quality Check', 'CS MF Final Quality Check', null, null, null, null);
            cases.add(case1);
            Case case2 = createCaseCommentsForFqc(simpleUser, vfContract1, 'Product Cleanup', 'CS MF Product Cleanup');
            cases.add(case2);
            insert cases;

            case2.Status = 'Closed';
            update case2;

            finalQualityCheckAssertCommentsForFqc(case1, case2);

            Test.stopTest();
        }
    }

    @IsTest
    static void testFqcSmallImplementation() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();
            VF_Contract__c vfContract1 = createVfContractWithAdditionalData(simpleUser, true, 'Mobile', 'Retention', 0);

            List<Case> cases = new List<Case>();
            Case case1 = createCaseWithFqcData(simpleUser, vfContract1, 'Final Quality Check', 'CS MF Final Quality Check', null, null, null, null);
            cases.add(case1);
            Case case2 = createCaseCommentsForFqc(simpleUser, vfContract1, 'Small Implementation', 'CS MF Small Implementation');
            cases.add(case2);
            insert cases;

            case2.Status = 'Closed';
            update case2;

            finalQualityCheckAssertCommentsForFqc(case1, case2);

            Test.stopTest();
        }
    }

    @IsTest
    static void testFqcProcessRework() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();
            VF_Contract__c vfContract1 = createVfContractWithAdditionalData(simpleUser, false, 'Mobile', 'Retention', 2);

            List<Case> cases = new List<Case>();
            Case case1 = createCaseWithFqcData(simpleUser, vfContract1, 'Final Quality Check', 'CS MF Final Quality Check', null, null, null, null);
            cases.add(case1);
            Case case2 = createCaseCommentsForFqc(simpleUser, vfContract1,'Process Rework', 'CS MF Process Rework');
            cases.add(case2);
            insert cases;

            case2.Status = 'Closed';
            update case2;

            finalQualityCheckAssertCommentsForFqc(case1, case2);
            Test.stopTest();
        }
    }

    @IsTest
    static void testFqcFinalQualityCheck() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();

            VF_Contract__c vfContract1 = createVfContractWithAdditionalData(simpleUser, false, 'Mobile', 'Retention', 2);

            List<Case> cases = new List<Case>();
            Case case1 = createCaseWithFqcData(simpleUser, vfContract1, 'Final Quality Check', 'CS MF Final Quality Check', null, null, null, null);
            cases.add(case1);
            Case case2 = createCaseWithFqcData(simpleUser, vfContract1, 'Final Quality Check', 'CS MF Final Quality Check', 'FQC Comment 2', 'Number of Add-ons not correct', 50, 'Passed');
            cases.add(case2);
            insert cases;

            case2.Status = 'Closed';
            update case2;

            List<Case> caseList = [
                    SELECT Id, Case_Record_Type_Text__c, Contract_VF__c
                    FROM Case
                    WHERE Id = :case1.Id
            ];

            CS_MobileFlowService.setCaseFields(caseList);
            List<Case> newCase = [
                    SELECT Id,
                            FQC_Comment__c,
                            FQC_Failure_Reason__c,
                            FQC_Quality_Score__c,
                            Responsible_Quality_Officer__c,
                            Contract_VF__r.Responsible_Quality_Officer__c
                    FROM Case
                    WHERE Id = :case1.Id
            ];

            for (Case c : newCase) {
                System.assertEquals(case2.FQC_Comment__c, c.FQC_Comment__c);
                System.assertEquals(case2.FQC_Failure_Reason__c, c.FQC_Failure_Reason__c);
                System.assertEquals(case2.FQC_Quality_Score__c, c.FQC_Quality_Score__c);
                System.assertEquals(case2.Responsible_Quality_Officer__c, c.Contract_VF__r.Responsible_Quality_Officer__c);
            }
            Test.stopTest();
        }
    }

    private static Case createCaseWithKtoData(User simpleUser, VF_Contract__c vfContract, Boolean sendKto, String ktoEmail){
        Case returnCase = CS_DataTest.createCase('Project Intake', 'CS MF Project Intake and Preparation', simpleUser, false);
        returnCase.Contract_VF__c = vfContract.Id;
        returnCase.Send_KTO__c = sendKto;
        returnCase.KTO_Contact_Email__c = ktoEmail;

        return returnCase;
    }

    private static Case createCaseWithFqcData(User simpleUser, VF_Contract__c vfContract, String subject, String recordTypeName, String fqcComment, String fqcFailureReason, Decimal qualityScore, String fqcResult){
        Case returnCase = CS_DataTest.createCase(subject, recordTypeName, simpleUser, false);
        returnCase.Contract_VF__c = vfContract.Id;
        returnCase.FQC_Comment__c = fqcComment == null ? 'FQC Comment' : fqcComment;
        returnCase.FQC_Failure_Reason__c = fqcFailureReason == null ? 'Number of Add-ons not correct' : fqcFailureReason;
        returnCase.Responsible_Quality_Officer__c = simpleUser.Id;
        returnCase.FQC_Quality_Score__c = qualityScore == null ? 5 : qualityScore;
        returnCase.FQC_Result__c = fqcResult == null ? 'Passed' : fqcResult;

        return returnCase;
    }

    private static Case createCaseCommentsForFqc(User simpleUser, VF_Contract__c vfContract, String subject, String recordTypeName){
        Case returnCase = CS_DataTest.createCase(subject, recordTypeName, simpleUser, false);
        returnCase.Contract_VF__c = vfContract.Id;
        returnCase.Comments_for_FQC__c = 'Comments for FQC';

        return returnCase;
    }

    private static VF_Contract__c createVfContractWithAdditionalData(User simpleUser, Boolean isSmallImplementation, String typeOfService, String dealType, Decimal reworkSequence){
        VF_Contract__c returnVfContract = CS_DataTest.createDefaultVfContract(simpleUser, false);
        returnVfContract.Is_Small_Implementation__c = isSmallImplementation;
        returnVfContract.Type_of_Service__c = typeOfService;
        returnVfContract.Deal_Type__c = dealType;
        returnVfContract.Responsible_Quality_Officer__c = simpleUser.Id;
        returnVfContract.Rework_Sequence_Nr__c = reworkSequence;

        insert returnVfContract;
        return returnVfContract;
    }

    private static void finalQualityCheckAssertCommentsForFqc(Case caseBeforeMethodCall, Case caseAfterMethodCall) {
        List<Case> caseList = [
                SELECT Id,
                        Case_Record_Type_Text__c,
                        Contract_VF__c
                FROM Case
                WHERE Id = :caseBeforeMethodCall.Id
        ];

        CS_MobileFlowService.setCaseFields(caseList);
        List<Case> newCase = [
                SELECT Id,
                        Comments_for_FQC__c
                FROM Case
                WHERE Id = :caseBeforeMethodCall.Id
        ];

        for (Case c : newCase) {
            System.assertEquals(caseAfterMethodCall.Comments_for_FQC__c, c.Comments_for_FQC__c);
        }
    }

    private static void assertKtoData(Case caseBeforeMethodCall, Case caseAfterMethodCall) {
        List<Case> caseList = [
                SELECT Id,
                        Case_Record_Type_Text__c,
                        Contract_VF__c
                FROM Case
                WHERE Id = :caseBeforeMethodCall.Id
        ];
        CS_MobileFlowService.setCaseFields(caseList);
        List<Case> newCase = [
                SELECT Id,
                        Send_KTO__c,
                        KTO_Contact_Email__c
                FROM Case
                WHERE Id = :caseBeforeMethodCall.Id
        ];

        for (Case c : newCase) {
            System.assertEquals(caseAfterMethodCall.Send_KTO__c, c.Send_KTO__c);
            System.assertEquals(caseAfterMethodCall.KTO_Contact_Email__c, c.KTO_Contact_Email__c);
        }
    }
}