public with sharing class AttachmentTriggerHandler extends TriggerHandler {
	private List<Attachment> newAtts;
	private List<CSCAP__Click_Approve_Setting__c> lstCAS = null;

	private void init() {
		newAtts = (List<Attachment>) this.newList;
	}

	public override void afterInsert() {
		init();
		sendQuoteApproval();
	}

	private Id getSiteApprovalSettingId() {
		Id casId = null;
		if (lstCAS == null) {
			lstCAS = [
				SELECT Id, Name
				FROM CSCAP__Click_Approve_Setting__c c
				WHERE Name LIKE '%Opportunit%'
				LIMIT 1
			];
		}
		if (lstCAS.size() > 0) {
			casId = lstCAS[0].Id;
		}
		return casId;
	}

	private void sendQuoteApproval() {
		if (getSiteApprovalSettingId() != null) {
			Map<Id, Set<Id>> oppIdsToAttIds = getOppIdToAttIdsMap();
			if (oppIdsToAttIds.size() == 0) {
				return;
			}
			List<CSCAP.API_1.MultipleSendApprovalRequestRecord> approvalRequests = new List<CSCAP.API_1.MultipleSendApprovalRequestRecord>();
			List<Opportunity> opps = [
				SELECT
					Id,
					LG_AutomatedQuoteDelivery__c,
					StageName,
					(SELECT ContactId, OpportunityId, Role, IsPrimary FROM OpportunityContactRoles)
				FROM Opportunity
				WHERE
					LG_AutomatedQuoteDelivery__c = 'Quote Requested'
					AND Id IN :oppIdsToAttIds.keyset()
			];
			Set<Id> flyerDocIds = getFlyerDocumentIds();
			for (Opportunity opp : opps) {
				Id recepientId = getOpportunityRecepient(opp);
				Set<Id> setAttachmentId = oppIdsToAttIds.get(opp.Id);
				if (recepientId != null) {
					CSCAP.API_1.MultipleSendApprovalRequestRecord approvalRequest = new CSCAP.API_1.MultipleSendApprovalRequestRecord();
					approvalRequest.approvalObjectId = opp.Id;
					approvalRequest.recipientContactId = recepientId;
					approvalRequest.clickApproveSettingId = getSiteApprovalSettingId();
					approvalRequest.attachmentIds = setAttachmentId;
					approvalRequest.documentIds = flyerDocIds.isEmpty() ? null : flyerDocIds;
					approvalRequest.cc = null;
					approvalRequest.bcc = null;
					approvalRequests.add(approvalRequest);
				}
				opp.LG_AutomatedQuoteDelivery__c = 'Quote Sent';
			}

			update opps;

			if (approvalRequests.size() > 0 && !Test.isRunningTest()) {
				CSCAP.API_1.MultipleSendApprovalRequest(approvalRequests);
			}
		}
	}

	private Map<Id, Set<Id>> getOppIdToAttIdsMap() {
		Map<Id, Set<Id>> oppIdsToAttIds = new Map<Id, Set<Id>>();
		for (Attachment att : newAtts) {
			if (String.valueOf(att.ParentId).startswith('006')) {
				if (!oppIdsToAttIds.containsKey(att.ParentId)) {
					oppIdsToAttIds.put(att.ParentId, new Set<Id>());
				}
				oppIdsToAttIds.get(att.ParentId).add(att.Id);
			}
		}
		return oppIdsToAttIds;
	}

	private Id getOpportunityRecepient(Opportunity opp) {
		Id primaryId;
		Id adminId;
		Id businesId;
		Id otherId;
		for (OpportunityContactRole ocr : opp.OpportunityContactRoles) {
			if (ocr.Role == 'Business User') {
				businesId = ocr.ContactId;
			}
			if (ocr.Role == 'Administrative Contact') {
				adminId = ocr.ContactId;
			}
			if (ocr.IsPrimary) {
				primaryId = ocr.ContactId;
			}
			otherId = ocr.ContactId;
		}
		if (primaryId != null) {
			return primaryId;
		}
		if (businesId != null) {
			return businesId;
		}
		if (adminId != null) {
			return adminId;
		}
		return otherId;
	}

	private Set<Id> getFlyerDocumentIds() {
		// Custom settings that contains info about potential flyers
		List<FlyerInfo__c> finfo = [SELECT IncludeFlyer__c, DocumentIds__c FROM FlyerInfo__c];
		Set<Id> documentIds = new Set<Id>();
		if (!finfo.isEmpty() && finfo[0].IncludeFlyer__c) {
			// DocumentIds__c contains comma separated Ids of flyer that are stored as Documents
			for (String docId : finfo[0].DocumentIds__c.split(',')) {
				if (docId.startsWith('015')) {
					documentIds.add(docId);
				}
			}
		}
		return documentIds;
	}
}