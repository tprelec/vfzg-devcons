public with sharing class CS_ComDependantDataDetailsWrapper {

    @AuraEnabled
    public String attributeName;

    @AuraEnabled
    public String attributeValue;
}