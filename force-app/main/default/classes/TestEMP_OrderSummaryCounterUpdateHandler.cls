@IsTest
private class TestEMP_OrderSummaryCounterUpdateHandler {
	@TestSetup
	private static void testSetup() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		TestUtils.autoCommit = false;
		User owner = TestUtils.createAdministrator();
		System.runAs(GeneralUtils.currentUser) {
			owner.Username = 'teoscuhtestadmin@vodafone.com';
			insert owner;
		}
		TestUtils.autoCommit = true;

		Account account = TestUtils.createAccount(owner);
		Opportunity opportunity = TestUtils.createOpportunity(account, Test.getStandardPricebookId());
		VF_Contract__c vfContract = TestUtils.createVFContract(account, opportunity);

		OrderType__c orderType = TestUtils.createOrderType();
		Order__c order = new Order__c(OrderType__c = orderType.Id, VF_Contract__c = vfContract.Id, Account__c = account.Id);
		insert order;

		CSPOFA__Orchestration_Process_Template__c processTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);

		CSPOFA__Orchestration_Step_Template__c stepTemplate = CS_DataTest.createOrchStepTemplate(processTemplate.Id, 'Test Step 1', '3', true);

		CSPOFA__Orchestration_Process__c orchestratorProcess = CS_DataTest.createOrchProcess(
			processTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		orchestratorProcess.VZ_Order__c = order.Id;
		insert orchestratorProcess;

		CS_DataTest.createOrchStep(stepTemplate.Id, orchestratorProcess.Id, true);
	}

	@IsTest
	private static void testProcessNonInitializedCounter() {
		List<CSPOFA__Orchestration_Step__c> orchestratorSteps = [SELECT id, CSPOFA__Orchestration_Process__c FROM CSPOFA__Orchestration_Step__c];
		EMP_OrderSummaryCounterUpdateHandler orderSummaryCounterUpdateHandler = new EMP_OrderSummaryCounterUpdateHandler();

		initializeOrderCurrentAndMaxCounter(null, null);

		Test.startTest();
		orderSummaryCounterUpdateHandler.process(orchestratorSteps);
		Test.stopTest();

		List<Order__c> resultingOrders = [SELECT Order_Summary_Current_Counter__c, Order_Summary_Max_Counter__c FROM Order__c];
		System.assertEquals(1, resultingOrders.size(), 'There should be only 1 order.');
		System.assertEquals(
			2,
			resultingOrders[0].Order_Summary_Current_Counter__c,
			'The current counter should be set to 2 after initialization and one pass.'
		);
		System.assertEquals(
			2,
			resultingOrders[0].Order_Summary_Max_Counter__c,
			'The maximum counter should be set to 2 after initialization and one pass.'
		);
	}

	@IsTest
	private static void testProcessEndLoopDoublingCounter() {
		List<CSPOFA__Orchestration_Step__c> orchestratorSteps = [SELECT id, CSPOFA__Orchestration_Process__c FROM CSPOFA__Orchestration_Step__c];
		EMP_OrderSummaryCounterUpdateHandler orderSummaryCounterUpdateHandler = new EMP_OrderSummaryCounterUpdateHandler();

		Integer startingMax = 4;
		initializeOrderCurrentAndMaxCounter(1, startingMax);

		Test.startTest();
		orderSummaryCounterUpdateHandler.process(orchestratorSteps);
		Test.stopTest();

		List<Order__c> resultingOrders = [SELECT Order_Summary_Current_Counter__c, Order_Summary_Max_Counter__c FROM Order__c];
		System.assertEquals(1, resultingOrders.size(), 'There should be only 1 order.');
		System.assertEquals(
			startingMax * 2,
			resultingOrders[0].Order_Summary_Current_Counter__c,
			'The current counter should be set to double value of the starting maximum.'
		);
		System.assertEquals(
			startingMax * 2,
			resultingOrders[0].Order_Summary_Max_Counter__c,
			'The maximum counter should be set to double value of the starting maximum.'
		);
	}

	@IsTest
	private static void testProcessMidLoopDecreaseCounter() {
		List<CSPOFA__Orchestration_Step__c> orchestratorSteps = [SELECT id, CSPOFA__Orchestration_Process__c FROM CSPOFA__Orchestration_Step__c];
		EMP_OrderSummaryCounterUpdateHandler orderSummaryCounterUpdateHandler = new EMP_OrderSummaryCounterUpdateHandler();

		Integer startingCurrent = 2;
		Integer startingMax = 4;
		initializeOrderCurrentAndMaxCounter(startingCurrent, startingMax);

		Test.startTest();
		orderSummaryCounterUpdateHandler.process(orchestratorSteps);
		Test.stopTest();

		List<Order__c> resultingOrders = [SELECT Order_Summary_Current_Counter__c, Order_Summary_Max_Counter__c FROM Order__c];
		System.assertEquals(1, resultingOrders.size(), 'There should be only 1 order.');
		System.assertEquals(
			startingCurrent - 1,
			resultingOrders[0].Order_Summary_Current_Counter__c,
			'The current counter should be decreased by 1.'
		);
		System.assertEquals(startingMax, resultingOrders[0].Order_Summary_Max_Counter__c, 'The maximum counter should remain unchanged.');
	}

	private static void initializeOrderCurrentAndMaxCounter(Integer currentCounter, Integer maxCounter) {
		List<Order__c> orders = [SELECT Order_Summary_Current_Counter__c, Order_Summary_Max_Counter__c FROM Order__c];
		orders[0].Order_Summary_Current_Counter__c = currentCounter;
		orders[0].Order_Summary_Max_Counter__c = maxCounter;
		update orders;
	}
}
