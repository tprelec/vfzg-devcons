@isTest
public with sharing class TestCOM_CDOdeleteBIMOService {
	@TestSetup
	static void setup() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		Account account = CS_DataTest.createAccount('Test Delete BIMO Account');
		account.CDO_Tenant_Id__c = 'someTenantId';
		insert account;

		Opportunity opportunity = CS_DataTest.createOpportunity(
			account,
			'Test Opp Delete BIMO',
			UserInfo.getUserId()
		);
		insert opportunity;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(
			opportunity,
			'Test Basket Delete BIMO'
		);
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c productDefinition = CS_DataTest.createProductDefinition(
			'Product Definition'
		);
		productDefinition.RecordTypeId = productDefinitionRecordType;
		productDefinition.Product_Type__c = 'Fixed';
		insert productDefinition;

		cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(
			productDefinition.Id,
			'Test Conf',
			basket.Id
		);
		insert productConfiguration;

		csord__Order__c order = new csord__Order__c(
			Name = 'Order 1',
			csord__Identification__c = 'ID_4568978'
		);
		insert order;

		Site__c testSite = CS_DataTest.createSite(
			'Test Site',
			account,
			'1032AB',
			'Street',
			'City',
			10.0
		);
		testSite.Footprint__c = null;
		insert testSite;

		COM_Delivery_Order__c deliveryOrder = new COM_Delivery_Order__c();
		deliveryOrder.Name = 'Test order';
		deliveryOrder.Order__c = order.Id;
		deliveryOrder.Status__c = 'In progress';
		deliveryOrder.CDO_Service_Order_Request_Id__c = '222-4444';

		insert deliveryOrder;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(
			productConfiguration.Id
		);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(
			productConfiguration.Id,
			subscription,
			'Service Created'
		);
		service.csord__Identification__c = 'testSubscription';
		service.COM_Delivery_Order__c = deliveryOrder.Id;
		service.csord__Order__c = order.Id;
		service.CDO_Umbrella_Service_Id__c = 'aaaaa-bbbbb-ccccc';
		service.Site__c = testSite.Id;
		insert service;

		csord__Service__c service2 = CS_DataTest.createService(
			productConfiguration.Id,
			subscription,
			'Service Created'
		);
		service2.csord__Identification__c = 'testSubscription2';
		service2.COM_Delivery_Order__c = deliveryOrder.Id;
		service2.csord__Order__c = order.Id;
		service2.CDO_Umbrella_Service_Id__c = 'aaaaa-bbbbb-ccccc';
		service2.Site__c = testSite.Id;
		insert service2;

		csord__Service__c service3 = CS_DataTest.createService(
			productConfiguration.Id,
			subscription,
			'Service Created'
		);
		service3.csord__Identification__c = 'testSubscription3';
		service3.COM_Delivery_Order__c = deliveryOrder.Id;
		service3.csord__Order__c = order.Id;
		service3.CDO_Umbrella_Service_Id__c = 'aaaaa-bbbbb-ccccc';
		service3.Site__c = testSite.Id;
		insert service3;

		csord__Service__c service4 = CS_DataTest.createService(
			productConfiguration.Id,
			subscription,
			'Service Created'
		);
		service4.csord__Identification__c = 'testSubscription4';
		service4.COM_Delivery_Order__c = deliveryOrder.Id;
		service4.csord__Order__c = order.Id;
		service4.CDO_Umbrella_Service_Id__c = 'aaaaa-bbbbb-ccccc';
		service4.Site__c = testSite.Id;
		insert service4;
	}

	@isTest
	static void testdeleteBIMOSyncSuccessfulRequests() {
		String umbrellaService = '222-333';
		String serviceOrderRequest = '000-999';

		String mockSyncSuccess =
			'{ "id": "' +
			serviceOrderRequest +
			'", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2f34dg", "externalId": "COM", "priority": "1", "description": "Internet Modem Only Service Order", "category": "Internet Modem Only Service", "state": "acknowledged", "orderDate": "2022-02-08T10:37:15.97221Z", "@type": "standard", "orderItem": [ { "id": "1", "action": "delete", "state": "acknowledged", "@type": "standard", "service": { "id": "' +
			umbrellaService +
			'" } } ] }';

		COM_Delivery_Order__c deliveryOrder = [SELECT Id FROM COM_Delivery_Order__c];
		Id deliveryOrderId = deliveryOrder.Id;

		Test.startTest();
		COM_MockWebService.setTestMockResponse(200, 'OK', mockSyncSuccess);
		COM_CDOdeleteBIMOService.deleteBIMOService(deliveryOrderId);
		Test.stopTest();

		deliveryOrder = [
			SELECT Id, CDO_Integration_Status__c, CDO_Service_Order_Request_Id__c
			FROM COM_Delivery_Order__c
			WHERE Id = :deliveryOrderId
		];

		System.assertEquals(
			deliveryOrder.CDO_Service_Order_Request_Id__c,
			serviceOrderRequest,
			'Service order request service not set correctly in sync success BIMO test'
		);
	}

	@isTest
	static void testdeleteBIMOSyncFailedRequests() {
		String mockSyncFailed = '{ "code": 40020, "reason": "SERVICE_ORDER_CHARACTERISTICS_VALIDATION_ERROR", "message": "Request for deletion has failed due to invalid value" }';

		COM_Delivery_Order__c deliveryOrder = [SELECT Id FROM COM_Delivery_Order__c];
		Id deliveryOrderId = deliveryOrder.Id;

		Test.startTest();
		COM_MockWebService.setTestMockResponse(200, 'OK', mockSyncFailed);
		COM_CDOdeleteBIMOService.deleteBIMOService(deliveryOrderId);
		Test.stopTest();

		deliveryOrder = [
			SELECT Id, CDO_Integration_Status__c
			FROM COM_Delivery_Order__c
			WHERE Id = :deliveryOrderId
		];

		System.assertEquals(
			deliveryOrder.CDO_Integration_Status__c,
			'Failure',
			'Service order request service not set correctly in sync failure BIMO test'
		);
	}

	@isTest
	static void testdeleteBIMOAsyncSuccessRequests() {
		String umbrellaService = 'aaaaa-bbbbb-ccccc';
		String serviceOrderRequest = '222-4444';

		String mockASyncSuccess =
			'{ "eventId": "deff54ee-ba37-4dba-b1c9-1fe1e554eds1", "eventTime": "2022-02-08T11:30:15.97221Z", "eventType": "ServiceOrderStateChangeNotification", "event": { "serviceOrder": { "id": "' +
			serviceOrderRequest +
			'", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2f34dg", "externalId": "COM", "priority": "1", "description": "Internet Modem Only Service Order", "category": "Internet Modem Only Service", "state": "completed", "orderDate": "2022-02-08T10:37:15.97221Z", "startDate": "2022-02-08T10:37:15.97321Z", "@type": "standard", "orderItem": [ { "id": "1", "action": "modify", "state": " completed", "@type": "standard", "service": { "id": "' +
			umbrellaService +
			'" } } ] } } }';

		RestRequest request = new RestRequest();
		request.requestUri = '/Salesforce/services/apexrest/notificationListener/*';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(mockASyncSuccess);
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		COM_CDONotificationListener.processNotification();
		Test.stopTest();

		COM_Delivery_Order__c deliveryOrder = [
			SELECT Id, CDO_Integration_Status__c, CDO_Service_Order_Request_Id__c
			FROM COM_Delivery_Order__c
			WHERE CDO_Service_Order_Request_Id__c = :serviceOrderRequest
		];

		System.assertEquals(
			deliveryOrder.CDO_Integration_Status__c,
			'Complete',
			'Integration status not set correctly in async success BIMO test'
		);
	}

	@isTest
	static void testdeleteBIMOAsyncFailedRequests() {
		String umbrellaService = 'aaaaa-bbbbb-ccccc';
		String serviceOrderRequest = '222-4444';

		String mockAsyncFailed =
			'{ "eventId": "9577add4-5200-47cb-b787-f6e617b396d9", "eventTime": "2022-02-08T10:45:15.97221Z", "eventType": "ServiceOrderStateChangeNotification", "event": { "serviceOrder": { "id": "' +
			serviceOrderRequest +
			'", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2f34dg", "externalId": "COM", "priority": "1", "description": "Internet Modem Only Service Order", "category": "Internet Modem Only Service", "state": "held", "orderDate": "2022-02-08T10:37:15.97221Z", "startDate": "2022-02-08T10:37:15.97321Z", "@type": "standard", "orderItem": [ { "id": "1", "action": "add", "state": "held", "@type": "standard", "service": { "id": "' +
			umbrellaService +
			'" } } ] }, "errorEvents": [ { "code": 510105000, "reason": "EXTERNAL_SYSTEM_ERROR", "message": "Deletion of service failed with error 5000" } ] } }';

		RestRequest request = new RestRequest();
		request.requestUri = '/Salesforce/services/apexrest/notificationListener/*';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(mockAsyncFailed);
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		COM_CDONotificationListener.processNotification();
		Test.stopTest();

		COM_Delivery_Order__c deliveryOrder = [
			SELECT Id, CDO_Integration_Status__c, CDO_Error_Message__c
			FROM COM_Delivery_Order__c
			WHERE CDO_Service_Order_Request_Id__c = :serviceOrderRequest
		];

		System.assertEquals(
			deliveryOrder.CDO_Integration_Status__c,
			'Failure',
			'Integration status not set correctly in async success BIMO test'
		);

		System.assertEquals(
			deliveryOrder.CDO_Error_Message__c,
			'Deletion of service failed with error 5000',
			'Error message not set correctly in async success BIMO test'
		);
	}
}
