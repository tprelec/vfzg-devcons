public with sharing class COM_CDO_DataPropagation {
	public String eventId;
	public String eventTime;
	public String eventType;
	public COM_CDO_DataPropagationEvent event;
	public class COM_CDO_DataPropagationEvent {
		public COM_CDO_ServiceOrder.Service service;
	}
}
