public without sharing class QueueUserSharingService {
	@TestVisible
	public static map<String, Id> ownersIdsByQueueUser {
		get {
			if (ownersIdsByQueueUser == null) {
				ownersIdsByQueueUser = new Map<String, Id>();
				for (Queue_User_Sharing__c qus : [SELECT Id, OwnerId FROM Queue_User_Sharing__c])
					ownersIdsByQueueUser.put(qus.Id, qus.OwnerId);
			}
			return ownersIdsByQueueUser;
		}
		private set;
	}

	public static map<Id, set<Id>> ownersIdsByQueue {
		get {
			if (ownersIdsByQueue == null) {
				ownersIdsByQueue = new Map<Id, set<Id>>();

				for (GroupMember gm : [SELECT GroupId, UserOrGroupId FROM GroupMember WHERE GroupId IN :ownersIdsByQueueUser.values()]) {
					if (ownersIdsByQueue.containsKey(gm.GroupId))
						ownersIdsByQueue.get(gm.GroupId).add(gm.UserOrGroupId);
					else
						ownersIdsByQueue.put(gm.GroupId, new Set<Id>{ gm.UserOrGroupId });
				}
			}
			return ownersIdsByQueue;
		}
		private set;
	}

	public static void shareAccountsWithUserQueues(map<Id, set<Id>> accountIdsByQueueUserIds) {
		set<Id> ownersId;
		set<Id> accIds = new Set<Id>();
		map<Id, Id> ownersByAccounts = new Map<Id, Id>();
		List<AccountShare> lst_newAccountShares = new List<AccountShare>();

		if (accountIdsByQueueUserIds == null || accountIdsByQueueUserIds.isEmpty())
			return;

		for (set<Id> stId : accountIdsByQueueUserIds.values())
			accIds.addAll(stId);

		for (Account acc : [SELECT Id, OwnerId FROM Account WHERE Id IN :accIds])
			ownersByAccounts.put(acc.Id, acc.OwnerId);

		for (ID qusId : accountIdsByQueueUserIds.keySet()) {
			for (ID accId : accountIdsByQueueUserIds.get(qusId)) {
				ownersId = ownersIdsByQueueUser.containsKey(qusId) && !ownersIdsByQueue.containsKey(ownersIdsByQueueUser.get(qusId))
					? new Set<ID>{ ownersIdsByQueueUser.get(qusId) }
					: ownersIdsByQueue.get(ownersIdsByQueueUser.get(qusId));

				for (ID oId : ownersId) {
					if (ownersByAccounts.containsKey(accId) && ownersByAccounts.get(accId) != oId)
						lst_newAccountShares.add(
							new AccountShare(
								UserOrGroupId = oId,
								RowCause = Schema.AccountShare.RowCause.Manual,
								AccountAccessLevel = 'Edit',
								OpportunityAccessLevel = 'None',
								CaseAccessLevel = 'None',
								AccountId = accId
							)
						);
				}
			}
		}

		if (!lst_newAccountShares.isEmpty())
			insert lst_newAccountShares;
	}

	public static void removeShareAccountsWithUserQueues(map<Id, set<Id>> accountIdsByQueueUserIds_remove) {
		set<Id> ownersId;
		set<Id> accRemoveIds = new Set<Id>();
		List<AccountShare> lst_removeAccountShares = new List<AccountShare>();

		map<Id, map<Id, AccountShare>> currentAccountShares = new Map<Id, map<Id, AccountShare>>();

		if (accountIdsByQueueUserIds_remove == null || accountIdsByQueueUserIds_remove.isEmpty())
			return;

		for (set<Id> stId : accountIdsByQueueUserIds_remove.values())
			accRemoveIds.addAll(stId);

		for (AccountShare accShare : [
			SELECT Id, AccountId, UserOrGroupId
			FROM Accountshare
			WHERE AccountId IN :accRemoveIds AND RowCause = :Schema.AccountShare.RowCause.Manual
		])
			if (currentAccountShares.containsKey(accShare.AccountId))
				currentAccountShares.get(accShare.AccountId).put(accShare.UserOrGroupId, accShare);
			else
				currentAccountShares.put(accShare.AccountId, new Map<Id, AccountShare>{ accShare.UserOrGroupId => accShare });

		for (ID qusId : accountIdsByQueueUserIds_remove.keySet()) {
			for (ID accId : accountIdsByQueueUserIds_remove.get(qusId)) {
				ownersId = ownersIdsByQueueUser.containsKey(qusId) && !ownersIdsByQueue.containsKey(ownersIdsByQueueUser.get(qusId))
					? new Set<ID>{ ownersIdsByQueueUser.get(qusId) }
					: ownersIdsByQueue.get(ownersIdsByQueueUser.get(qusId));

				for (ID oId : ownersId)
					if (currentAccountShares.containsKey(accId) && currentAccountShares.get(accId).containsKey(oId))
						lst_removeAccountShares.add(currentAccountShares.get(accId).get(oId));
			}
		}

		if (!lst_removeAccountShares.isEmpty())
			delete lst_removeAccountShares;
	}
}
