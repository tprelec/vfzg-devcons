@isTest
public with sharing class TestIBANService {
	public enum BankNumberValidationResult {
		IBAN_OK, // Valid IBAN
		IBAN_INVALID_FORMAT, // Base format check failed (must start with country code, ...)
		IBAN_INVALID_LENGTH, // Invalid country specific length
		IBAN_SANITY_CHECK_FAILED, // Failed sanity check
		IBAN_WRONG_COUNTRY_CHECKSUM // Wrong country specific checksum (example: Eleven-check in the Netherlands)
	}

	@isTest
	public static void testValidateIBAN() {
		String accWithBillAccName = 'SFDT-59 W-Def';
		String accWithoutBillAccName = 'SFDT-59 Wo-Def';
		String defaultBillingAccountName = 'SFDT-59 Bill1';

		List<Account> accounts = new List<Account>();
		Account accWithDefaultBill = LG_GeneralTest.CreateAccount(
			accWithBillAccName,
			'12345678',
			'Ziggo',
			false
		);
		Account accWithoutDefaultBill = LG_GeneralTest.CreateAccount(
			accWithoutBillAccName,
			'12345679',
			'Ziggo',
			false
		);
		accounts.add(accWithoutDefaultBill);
		accounts.add(accWithDefaultBill);
		insert accounts;

		csconta__Billing_Account__c ba1 = LG_GeneralTest.createBillingAccount(
			defaultBillingAccountName,
			accWithDefaultBill.Id,
			true,
			false
		);
		ba1.LG_BankAccountNumberIBAN__c = 'GB33BUKB20201555555555';
		csconta__Billing_Account__c ba2 = LG_GeneralTest.createBillingAccount(
			'SFDT-59 Bill2',
			accWithDefaultBill.Id,
			false,
			false
		);
		csconta__Billing_Account__c ba3 = LG_GeneralTest.createBillingAccount(
			'SFDT-59 Bill3',
			accWithoutDefaultBill.Id,
			false,
			false
		);

		List<csconta__Billing_Account__c> billAccounts = new List<csconta__Billing_Account__c>();
		billAccounts.add(ba1);
		billAccounts.add(ba2);
		billAccounts.add(ba3);
		insert billAccounts;

		Test.startTest();
		IBANService.validateIBAN(billAccounts);
		Test.stopTest();
	}
	@isTest
	public static void testGetErrorMessage() {
		Map<String, Integer> ibanValues = IBANService.IbanMap();

		IBANService.BankNumberValidationResult validationResult = IBANService.checkIban(
			'B20201555555555',
			ibanValues
		);

		IBANService.BankNumberValidationResult validationResult1 = IBANService.checkIban(
			'GB33BUKB20201555555555778887777888',
			ibanValues
		);

		Test.startTest();
		String result = IBANService.getErrorMessage(validationResult);
		String result1 = IBANService.getErrorMessage(validationResult1);
		Test.stopTest();
	}
}