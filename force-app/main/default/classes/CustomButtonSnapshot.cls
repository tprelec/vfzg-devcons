global with sharing class CustomButtonSnapshot extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		List<CS_Basket_Snapshot_Transactional__c> oldData = [SELECT Id FROM CS_Basket_Snapshot_Transactional__c WHERE Product_Basket__c = :basketId];
		delete oldData;

		String basketQuery =
			'SELECT Id,Name,Used_Snapshot_Objects__c, cscfga__Opportunity__c FROM cscfga__Product_Basket__c' +
			' WHERE Id = \'' +
			basketId +
			'\'';
		List<cscfga__Product_Basket__c> baskets = Database.query(basketQuery);

		if (
			(baskets[0].Used_Snapshot_Objects__c == '') ||
			(baskets[0].Used_Snapshot_Objects__c == null) ||
			(baskets[0].Used_Snapshot_Objects__c != '[CS_Basket_Snapshot_Transactional__c]')
		) {
			baskets[0].Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]';
			update baskets;
		}
		CS_BasketSnapshotManager.TakeBasketSnapshot(baskets, false);

		return '{"status":"ok","text":"Basket snapshotted, Snapshotted successfully"}';
	}
}
