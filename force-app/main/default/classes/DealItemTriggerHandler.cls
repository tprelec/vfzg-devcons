/**
 * @description       : Apex Class Handler of the Deal Item Trigger
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

public class DealItemTriggerHandler extends TriggerHandler {
	private List<Deal_Item__c> lstNewDealItem;

	private Map<Id, Deal_Item__c> mapOldDealItem;
	private Map<Id, Deal_Item__c> mapNewDealItem;

	/**
	 * @description Initialize Class Variables
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	private void init() {
		lstNewDealItem = (List<Deal_Item__c>) this.newList;
		mapOldDealItem = (Map<Id, Deal_Item__c>) this.oldMap;
		mapNewDealItem = (Map<Id, Deal_Item__c>) this.newMap;
	}

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public void setExternalIds() {
		Sequence objSequence = new Sequence('Deal Item');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (Deal_Item__c objDealItem : lstNewDealItem) {
				objDealItem.ExternalID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
