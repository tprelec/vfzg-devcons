@isTest
private class TestITWBatch {
	@isTest
	static void test_method_one() {
		//Data preparation
		TestUtils.autoCommit = true;

		User testuser = TestUtils.createAdministrator();
		Account acc = TestUtils.createAccount(testuser);
		Ban__c ban = TestUtils.createBan(acc);
		TestUtils.createITW('388888888', 'Test Manager');
		TestUtils.createITW('399999998', 'Test User');
		TestUtils.createITW('388888888', 'Eelco');

		//Test
		Test.startTest();

		PageReference pageRef = Page.ITWBatchExecute;
		Test.setCurrentPage(pageRef);
		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new List<ITW_Account_Info__c>());
		ITWBatchExecuteController controller = new ITWBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		//Checks
		System.assertEquals(
			3,
			[SELECT Id FROM ITW_Account_Info__c WHERE Error__c = :'There is no corresponding Account with this Ban Number'].size()
		);
		System.assertEquals(0, [SELECT Id FROM ITW_Account_Info__c WHERE Error__c = :'There is no corresponding User with this Name'].size());
		System.assertEquals(3, [SELECT Id FROM ITW_Account_Info__c].size());
		System.assertEquals(0, [SELECT Id FROM AccountTeamMember WHERE AccountId = :acc.Id AND TeamMemberRole = 'ITW Sales Rep'].size());
		System.assertEquals(0, [SELECT Id FROM AccountShare WHERE RowCause = 'Team' AND OpportunityAccessLevel = 'Edit'].size());
	}
}
