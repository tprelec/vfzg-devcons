@isTest
public class CS_DealSummaryControllerTest {

    private static cscfga__Product_Configuration__c initConfig(Id productDefId, String productConfigName, Id basketId) {
        cscfga__Product_Configuration__c config = CS_DataTest.createProductConfiguration(productDefId, productConfigName, basketId);

        config.cscfga__Contract_Term__c = 24;
        config.cscfga__total_one_off_charge__c = 100;
        config.cscfga__Total_Price__c = 20;
        config.Number_of_SIP__c = 20;
        config.RC_Cost__c = 0;
        config.NRC_Cost__c = 0;
        config.cscfga__Contract_Term_Period__c = 12;
        config.cscfga__Configuration_Status__c = 'Valid';
        config.cscfga__Quantity__c = 1;
        config.cscfga__total_recurring_charge__c = 22;
        config.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
        config.cscfga__one_off_charge_product_discount_value__c = 0;
        config.cscfga__recurring_charge_product_discount_value__c = 0;

        return config;
    }

    @isTest
    private static void CS_DealSummaryControllerTest() {
        List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
        
        System.runAs (simpleUser) {
            
            Framework__c frameworkSetting = new Framework__c();
            frameworkSetting.Framework_Sequence_Number__c = 2;
            insert frameworkSetting;
            
            PriceReset__c priceResetSetting = new PriceReset__c();
            priceResetSetting.MaxRecurringPrice__c = 200.00;
            priceResetSetting.ConfigurationName__c = 'IP Pin';
            insert priceResetSetting;
            
            Sales_Settings__c ssettings = new Sales_Settings__c();
            ssettings.Postalcode_check_validity_days__c = 2;
            ssettings.Max_Daily_Postalcode_Checks__c = 2;
            ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
            ssettings.Postalcode_check_block_period_days__c = 2;
            ssettings.Max_weekly_postalcode_checks__c = 15;
            insert ssettings;
            
            CS_Basket_Snapshot_Settings__c pcSettings = new CS_Basket_Snapshot_Settings__c();
            pcSettings.Output_Strategy__c = 'Product Configuration';
            pcSettings.Cleanup_Process__c = 'In Transaction';
            pcSettings.Use_Field_Type_Conversion__c = true;
            insert pcSettings;
            Account account = new Account(
                OwnerId = UserInfo.getUserId(),
                Name = 'Account',
                Type = 'End Customer'
            );
            insert account;

            Contact contact = new Contact(
                AccountId = account.id,
                LastName = 'Last',
                FirstName = 'First',
                Contact_Role__c = 'Consultant',
                
                Email = 'test@vf.com'   
            );
            insert contact;

            Opportunity opportunity = new Opportunity(
                Name = 'New Opportunity',
                OwnerId = UserInfo.getUserId(),
                StageName = 'Qualification',
                Probability = 0,
                CloseDate = system.today(),
                //ForecastCategoryName = 'Pipeline',
                AccountId = account.id
            );
            insert opportunity;

            cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
                Name = 'PD1',
                cscfga__Description__c = 'PD1 Desc',
                Snapshot_Object__c ='CS_Basket_Snapshot_Transactional__c'
            );
            pd.Product_Type__c = 'Fixed';
            insert pd;

            cscfga__Attribute_Definition__c ad = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'AD1',
                Snapshot_Attribute_Value_Field__c = 'OneOffPrice__c'
               
            );
            insert ad;
             cscfga__Attribute_Definition__c adQ = new cscfga__Attribute_Definition__c(
                cscfga__Product_Definition__c = pd.Id,
                Name = 'Quantity',
                Snapshot_Attribute_Value_Field__c = 'Quantity__c'
               
            );
            insert adQ;
            
            cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
                Name = 'New Basket',
              
                cscfga__Basket_Status__c = 'Valid',
                OwnerId = UserInfo.getUserId(),
                cscfga__Opportunity__c = opportunity.Id,
                Used_Snapshot_Objects__c = '[CS_Basket_Snapshot_Transactional__c]'
            );
            insert basket;
            
            cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 50.00,
                cscfga__total_recurring_charge__c = 10.00,
                Name='PCParent'
            );
            insert pc;

            cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(
                cscfga__Product_Definition__c = pd.Id,   
                cscfga__Product_Basket__c = basket.Id,
                cscfga__Quantity__c = 1,
                cscfga__total_one_off_charge__c = 40.00,
                cscfga__total_recurring_charge__c = 10.00,
                cscfga__Parent_Configuration__c = pc.Id,
                cscfga__Root_Configuration__c = pc.Id,
                Name='PC1'
            );
            insert pc1;

            cscfga__Attribute__c att = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '10',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = ad.Id,
                cscfga__Price__c = 50.00
            );
            insert att;
            
            cscfga__Attribute__c attQ = new cscfga__Attribute__c(
                cscfga__Product_Configuration__c = pc.Id,
                Name = 'Test',
                cscfga__Value__c = '2',
                cscfga__is_active__c = true,
                cscfga__Attribute_Definition__c = adQ.Id,
                cscfga__Price__c = 50.00
            );
            insert attQ;
            
            cscfga__Product_Definition__c accessInfraDef = CS_DataTest.createProductDefinition('Access Infrastructure');
            accessInfraDef.Product_Type__c = 'Fixed';
    
            cscfga__Product_Definition__c managedInternetDef = CS_DataTest.createProductDefinition('Managed Internet');
            managedInternetDef.Product_Type__c = 'Fixed';
            
            cscfga__Product_Definition__c ipvpnDef = CS_DataTest.createProductDefinition('IPVPN');
            ipvpnDef.Product_Type__c = 'Fixed';
    
            cscfga__Product_Definition__c oneFixedDef = CS_DataTest.createProductDefinition('One Fixed');
            oneFixedDef.Product_Type__c = 'Fixed';
            
            cscfga__Product_Definition__c vodAccessDefn = CS_DataTest.createProductDefinition('Vodafone Access');
            vodAccessDefn.Product_Type__c = 'Fixed';
            
            cscfga__Product_Definition__c mobCTNSubs = CS_DataTest.createProductDefinition('Mobile CTN Subscription');
            mobCTNSubs.Product_Type__c = 'Mobile';
            
            List<cscfga__Product_Definition__c> prodDefList = new List<cscfga__Product_Definition__c>{accessInfraDef, managedInternetDef, ipvpnDef, oneFixedDef, vodAccessDefn, mobCTNSubs};
            insert prodDefList;

            Account testAccount = CS_DataTest.createAccount('Test Account 1');
            insert testAccount;

            Site__c testSite = new Site__c();
        	testSite.Name = 'testSite';
        	testSite.Site_Postal_Code__c = '1234AA';
        	testSite.Site_Account__c = testAccount.Id;
        	testSite.Site_Street__c = 'teststreet';
        	testSite.Site_City__c = 'testTown';
        	testSite.Site_House_Number__c = 3;
        	insert testSite;
        	
        	Site_Availability__c testSA = new Site_Availability__c();
        	testSA.Name = 'testSA';
        	testSA.Site__c = testSite.Id;
        	testSA.Vendor__c = 'ZIGGO'; 
        	testSA.Premium_Vendor__c = 'TELECITY'; 
        	testSA.Access_Infrastructure__c ='Coax'; 
        	testSA.Bandwith_Down_Entry__c = 1; 
        	testSA.Bandwith_Up_Entry__c = 10; 
        	testSA.Bandwith_Down_Premium__c = 1; 
        	testSA.Bandwith_Up_Premium__c = 10;
        	insert testSA;
    
            Site_Availability__c testSA2 = new Site_Availability__c();
        	testSA2.Name = 'testSA2';
        	testSA2.Site__c = testSite.Id;
        	testSA2.Vendor__c = 'ZIGGO'; 
        	testSA2.Premium_Vendor__c = 'TELECITY'; 
        	testSA2.Access_Infrastructure__c ='Coax'; 
        	testSA2.Bandwith_Down_Entry__c = 1; 
        	testSA2.Bandwith_Up_Entry__c = 10; 
        	testSA2.Bandwith_Down_Premium__c = 1; 
        	testSA2.Bandwith_Up_Premium__c = 10;
            insert testSA2;

            String clonedIds = '{"0000":"' + testSA.Id + '","jtE5":"' + testSA2.Id + '"}';
    
            cscfga__Product_Configuration__c pcAccessInf = initConfig(accessInfraDef.Id, 'Access Infrastructure',basket.Id);
            pcAccessInf.cspl__Type__c = 'Mobile Voice Services';
            pcAccessInf.OneNet_Scenario__c = 'One Fixed';
            pcAccessInf.Deal_Type__c = 'Acquisition';
            pcAccessInf.Mobile_Scenario__c = 'test';
            pcAccessInf.cscfga__Product_Family__c = 'Access Infrastructure';
            pcAccessInf.Site_Availability_Id__c = testSA.Id;
            pcAccessInf.ClonedSiteIds__c = clonedIds;
            insert pcAccessInf;
    
            cscfga__Product_Configuration__c pcOneFixed = initConfig(oneFixedDef.Id, 'One Fixed',basket.Id);
            pcOneFixed.Number_of_SIP_Clone_Multiplicator__c = 4;
            pcOneFixed.Mobile_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcSkype = initConfig(oneFixedDef.Id, 'Skype for Business',basket.Id);
            pcSkype.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcSkype.Number_of_SIP_Clone_Multiplicator__c = 4;
            pcSkype.Mobile_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcBMS = initConfig(oneFixedDef.Id, 'Business Managed Services',basket.Id);
            pcBMS.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcBMS.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcBMS.Mobile_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcMobCTN = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN.Deal_Type__c = 'Disconnect';
            pcMobCTN.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN.Mobile_Scenario__c = 'OneBusiness';
            
            cscfga__Product_Configuration__c pcMobCTN2 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN2.Deal_Type__c = 'Disconnect';
            pcMobCTN2.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN2.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN2.Mobile_Scenario__c = 'Data only';
            
            cscfga__Product_Configuration__c pcMobCTN3 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN3.Deal_Type__c = 'Disconnect';
            pcMobCTN3.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN3.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN3.Mobile_Scenario__c = 'RedPro';
            
            cscfga__Product_Configuration__c pcMobCTN4 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN4.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN4.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN4.Mobile_Scenario__c = 'OneBusiness IOT';
            
            cscfga__Product_Configuration__c pcMobCTN5 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN5.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN5.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN5.Mobile_Scenario__c = 'OneMobile';
            
            cscfga__Product_Configuration__c pcMobCTN6 = initConfig(mobCTNSubs.Id, 'Mobile CTN profile',basket.Id);
            pcMobCTN6.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcMobCTN6.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcMobCTN6.Mobile_Scenario__c = 'OneBusiness';
            
            cscfga__Product_Configuration__c pcVodCal = initConfig(vodAccessDefn.Id, 'Vodafone Calling',basket.Id);
            pcVodCal.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcVodCal.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcVodCal.Mobile_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcOneNet = initConfig(vodAccessDefn.Id, 'One Net',basket.Id);
            pcOneNet.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcOneNet.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcOneNet.Mobile_Scenario__c = 'test';
            pcOneNet.OneNet_Scenario__c = 'test';
            
            cscfga__Product_Configuration__c pcWOS = initConfig(vodAccessDefn.Id, 'Wireless Only Standalone',basket.Id);
            pcWOS.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcWOS.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcWOS.Mobile_Scenario__c = 'test';
            pcWOS.OneNet_Scenario__c = 'test';

            cscfga__Product_Configuration__c pcCLFV = initConfig(vodAccessDefn.Id, 'Company Level Fixed Voice',basket.Id);
            pcCLFV.cscfga__Parent_Configuration__c = pcAccessInf.Id;
            pcCLFV.Number_of_SIP_Clone_Multiplicator__c = 2;
            pcCLFV.Mobile_Scenario__c = 'test';
            pcCLFV.OneNet_Scenario__c = 'test';

            List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>{ pcOneFixed, pcSkype, pcBMS, pcMobCTN, pcMobCTN2, 
                pcMobCTN3, pcMobCTN4, pcMobCTN5, pcMobCTN6, pcVodCal, pcOneNet, pcCLFV };
            insert pcList;

            pcAccessInf.OneFixed__c = pcOneFixed.id;
            update pcAccessInf;

            CS_Basket_Snapshot_Transactional__c snap1 = CS_DataTest.createBasketSnapshotTransactional('Snap 1', basket.Id, pcAccessInf.Id, pcOneFixed.Id);
            CS_Basket_Snapshot_Transactional__c snap2 = CS_DataTest.createBasketSnapshotTransactional('Snap 2', basket.Id, pcAccessInf.Id, pcSkype.Id);
            CS_Basket_Snapshot_Transactional__c snap3 = CS_DataTest.createBasketSnapshotTransactional('Snap 3', basket.Id, pcAccessInf.Id, pcBMS.Id);
            CS_Basket_Snapshot_Transactional__c snap4 = CS_DataTest.createBasketSnapshotTransactional('Snap 4', basket.Id, pcAccessInf.Id, pcMobCTN.Id);
            CS_Basket_Snapshot_Transactional__c snap5 = CS_DataTest.createBasketSnapshotTransactional('Snap 5', basket.Id, pcAccessInf.Id, pcMobCTN2.Id);
            CS_Basket_Snapshot_Transactional__c snap6 = CS_DataTest.createBasketSnapshotTransactional('Snap 6', basket.Id, pcAccessInf.Id, pcMobCTN3.Id);
            CS_Basket_Snapshot_Transactional__c snap7 = CS_DataTest.createBasketSnapshotTransactional('Snap 7', basket.Id, pcAccessInf.Id, pcVodCal.Id);
            CS_Basket_Snapshot_Transactional__c snap8 = CS_DataTest.createBasketSnapshotTransactional('Snap 8', basket.Id, pcAccessInf.Id, pcOneNet.Id);
            CS_Basket_Snapshot_Transactional__c snap9 = CS_DataTest.createBasketSnapshotTransactional('Snap 9', basket.Id, pcAccessInf.Id, pcCLFV.Id);

            List<CS_Basket_Snapshot_Transactional__c> snaps = new List<CS_Basket_Snapshot_Transactional__c> { snap1, snap2, snap3, snap4, snap5, snap6, snap7, snap8, snap9 };
            insert snaps;

            Test.startTest();
            CS_DealSummaryController.getDealSummaryData(basket.Id);
            DealSumaryDataObject dt = new DealSumaryDataObject();

            List<CS_Basket_Snapshot_Transactional__c> changedSnaps = [SELECT FinalPriceOneOff__c, FinalPriceRecurring__c 
                                                                      FROM CS_Basket_Snapshot_Transactional__c
                                                                      WHERE Product_Basket__c = :basket.Id];

            System.assertNotEquals(0, changedSnaps[0].FinalPriceOneOff__c);
            Test.stopTest();
        }
    }
    
    @isTest
    private static void testGetProfileName() {
        String result = '';
        Test.startTest();
        result = CS_DealSummaryController.getProfileName();
        Test.stopTest();
        
        System.assertNotEquals('', result);
    }
}