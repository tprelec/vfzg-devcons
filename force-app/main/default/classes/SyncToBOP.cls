/**
 * @description         Syncs Account, Contact, Site, User and BAN to BOP
 *                      See Work W-000192
 * @author              Gerhard Newman
 */
//@SuppressWarnings('PMD')
public without sharing class SyncToBOP {
	// Process starts with an Account ID and syncs the other objects based on this account
	public static void syncToBOP(ID accID) {
		syncAccount(accID);
		syncContact(accID);
		syncSite(accID);
	}

	private static void syncAccount(ID accountID) {
		// Note: Accounts don't export unless they are partner accounts
		Set<ID> accountSet = new Set<ID>();
		accountSet.add(accountID);
		AccountExport.exportAccountsOffline(accountSet);
	}

	private static void syncContact(ID accountID) {
		// Note: Contacts don't export without an email address
		//       Contacts don't export unless the Account or BAN has a BOP code
		//
		// Retrieve all the contacts for this account (but not interal users)
		List<Contact> contactList = [
			SELECT id, BOP_export_datetime__c
			FROM contact
			WHERE
				accountid = :accountID
				AND email != NULL
				AND recordTypeId != :GeneralUtils.recordTypeMap.get('Contact').get('Internal_Users')
		];
		Set<Id> updatedContacts = new Set<Id>();
		Set<Id> newContacts = new Set<Id>();

		// Determine if the contact has been exported before or not
		for (Contact c : contactList) {
			if (c.BOP_export_datetime__c == null) {
				newContacts.add(c.Id);
			} else {
				updatedContacts.add(c.Id);
			}
		}
		if (!newContacts.isEmpty()) {
			ContactExport.exportNewContactsOffline(newContacts);
		}
		if (!updatedContacts.isEmpty()) {
			ContactExport.exportUpdatedContactsOffline(updatedContacts);
		}
	}

	private static void syncSite(ID accountID) {
		Set<Id> siteIDsForUpdate = new Set<Id>();
		Set<Id> newSiteIds = new Set<Id>();
		Set<Id> accountIds = new Set<Id>();

		// Retrieve all the Sites for this account
		List<Site__c> siteList = [
			SELECT id, BOP_export_datetime__c, Site_Account__c
			FROM Site__c
			WHERE Site_Account__c = :accountID
		];

		for (Site__c s : siteList) {
			accountIds.add(s.Site_Account__c);
		}

		// check if the Site's account or one of the BANs has a BOPCode (otherwise do not export)
		Set<Id> accountIdsWithBopCode = KeyRingService.retrieveAccountIdsWithBopCode(accountIds);

		// key ring change -end
		// For all the sites that have an account or ban with a bop code add to a set for processing
		for (Site__c s : siteList) {
			if (accountIdsWithBopCode.contains(s.Site_Account__c)) {
				if (s.BOP_export_datetime__c == null) {
					newSiteIds.add(s.Id);
				} else {
					siteIDsForUpdate.add(s.Id);
				}
			}
		}
		if (!newSiteIds.isEmpty()) {
			SiteExport.exportNewSitesOffline(newSiteIds);
		}

		if (!siteIDsForUpdate.isEmpty()) {
			SiteExport.exportUpdatedSitesOffline(siteIDsForUpdate);
		}
	}
}