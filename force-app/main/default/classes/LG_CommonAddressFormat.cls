/**
* 
* Class for converting different address for common address format
* 
* @author Petar Miletic
* @ticket SFDT-15
* @since  23/02/2016
*/
public class LG_CommonAddressFormat {

    private Map<String, Object> response { get; set; }
    private Map<String, Object> envelope { get; set; }
    private Map<String, Object> body { get; set; }
    private Map<String, Object> getAddresses2Response { get; set; }
    
    private string street { get; set; }
    private string houseNumber { get; set; }
    private string houseNumberExt { get; set; }
    private string postcode { get; set; }
    private string city { get; set; }
    
    private Boolean PEAL { get; set; }
    
    // Readonly property 
    private string displayValue 
    {
        get 
        {
            // Extracted to Util becouse it is used in multiple classes
            return LG_util.getFormattedAddress(this.street, this.houseNumber, this.houseNumberExt, this.postcode, this.city);
        }
    }

     /*
        Constructor
    */
    public LG_CommonAddressFormat(Map<String, Object> inputMap) {
        
        this.PEAL = false;
        string s = (String)inputMap.get('addressResponseRaw');
        
        if (s.contains('Envelope') && s.contains('Body')) {
            this.response = (Map<String, Object>)JSON.deserializeUntyped(s);
            this.envelope = (Map<String, Object>)response.get('Envelope');
            this.body = (Map<String, Object>)envelope.get('Body');
            this.getAddresses2Response = (Map<String, Object>)body.get('getAddressesResponse');
            
            if (this.getAddresses2Response == null) {
                this.getAddresses2Response = (Map<String, Object>)body.get('getCustomAddressesResponse');
            }
            if (this.getAddresses2Response == null) {
                this.getAddresses2Response = (Map<String, Object>)body.get('getSameAddressResponse');
            }
        }
        else {

            this.PEAL = true;
            this.response = (Map<String, Object>)JSON.deserializeUntyped(s);
            this.getAddresses2Response = this.response;            
        }
    }

    /*
        Convert JSON response from internal/external source to common address format for TCS Console
        Parametar order: street, house number, house number extension, postcode, city, ID field
    */
    public List<Object> ToCommonAddressFormat(string street, string houseNumber, string houseNumberExt, string postcode, string city, string idField) {
        
        List<Object> results = new List<Object>();

        if (!this.PEAL) {

            string result = (String)getAddresses2Response.get('result');
            results = (List<Object>)JSON.deserializeUntyped(result);
        }
        else  {
        
            if (Test.isRunningTest()){
    
                string result = (String)getAddresses2Response.get('response');
                results = (List<Object>)JSON.deserializeUntyped(result);
            }
            else {
                
                results = (List<Object>)getAddresses2Response.get('response');                
            }
        }
        
        for (Object resultRow : results) {
            
            Map<String, Object> r = (Map<String, Object>)resultRow;

            this.street = getStringValue(r, street);
            this.houseNumber = getStringValue(r, houseNumber);
            this.houseNumberExt = getStringValue(r, houseNumberExt);
            this.postcode = getStringValue(r, postcode);
            this.city = getStringValue(r, city);
            
            r.put('id', getStringValue(r, idField));
            r.put('LG_Street', this.street);
            r.put('LG_HouseNumber', this.houseNumber);
            r.put('LG_HouseNumberExt', this.houseNumberExt);
            r.put('LG_City', this.city);
            r.put('LG_Postcode', this.postcode);
            r.put('DisplayValue', this.displayValue);
            r.put('LG_AddressID', getStringValue(r, idField));
        }

        return results;
    }

    /*
        Helper method for processing property value
    */
    private string getStringValue(Map<String, Object> objList, string propertyName) {
        string retval = (String)objList.get(propertyName);
        
        if (retval == null) {
            retval = '';
        }
        
        return retval;
    }
}