@IsTest
public with sharing class TestEmailService {
	@TestSetup
	static void makeData() {
		Account acc = TestUtils.createAccount(null);
		TestUtils.autoCommit = false;
		Contact con = TestUtils.createContact(acc);
		con.Email = 'test@test.com';
		insert con;
	}

	@IsTest
	private static void testSendEmailInvocable() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Contact con = [SELECT Id FROM Contact WHERE Email = 'test@test.com' LIMIT 1];
		Attachment att = new Attachment(
			Name = 'Test Attachment',
			body = Blob.valueOf('this is a test'),
			ContentType = 'pdf',
			IsPrivate = false,
			ParentId = acc.Id
		);
		insert att;
		EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];
		Test.startTest();
		EmailService.EmailRequest req = new EmailService.EmailRequest();
		req.recipientId = con.Id;
		req.relatedToId = acc.Id;
		req.toAddresses = new List<String>{ 'test1@test.com' };
		req.ccAddresses = new List<String>{ 'test2@test.com' };
		req.bccAddresses = new List<String>{ 'test3@test.com' };
		req.subject = 'testsubject';
		req.stylinTemplateId = template.Id;
		req.templateId = template.Id;
		req.attachmentIds = new List<Id>{ att.Id };
		req.useSignature = false;

		OrgWideEmailAddress orgWideEmailAddress = EmailService.getOrgWideEmailAddress('zakelijk@vodafone.nl');

		if (orgWideEmailAddress != null) {
			req.orgWideEmailId = orgWideEmailAddress.Id;
		}

		EmailService.sendEmail(new List<EmailService.EmailRequest>{ req });
		Test.stopTest();
		if (EmailService.checkDeliverability()) {
			List<EmailMessage> emails = [SELECT Id FROM EmailMessage];
			System.assert(!emails.isEmpty(), 'Email should be sent');
		}
	}

	@IsTest
	private static void testOrgWideEmailAddress() {
		Test.startTest();
		OrgWideEmailAddress defaultEmail = EmailService.getDefaultOrgWideEmailAddress();
		OrgWideEmailAddress caseEmail = EmailService.getCaseOrgWideEmailAddress();
		OrgWideEmailAddress deliveryEmail = EmailService.getDeliveryOrgWideEmailAddress();
		System.assert(defaultEmail != null, 'Email address exists.');
		System.assert(caseEmail != null, 'Email address exists.');
		System.assert(deliveryEmail != null, 'Email address exists.');
		Test.stopTest();
	}

	@IsTest
	private static void testPrepareEmail() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Contact con = [SELECT Id FROM Contact WHERE Email = 'test@test.com' LIMIT 1];
		EmailTemplate template = [SELECT Id FROM EmailTemplate LIMIT 1];

		Test.startTest();
		Messaging.SingleEmailMessage result = EmailService.prepareEmail(
			con.Id,
			acc.Id,
			new List<String>{ 'test1@test.com' },
			new List<String>{ 'test2@test.com' },
			new List<String>{ 'test3@test.com' },
			template.Id,
			template.Id,
			'testsubject',
			new List<Id>(),
			null
		);
		Test.stopTest();

		System.assertEquals('testsubject', result.getSubject(), 'Result does not contain required text.');
	}

	@isTest
	private static void testFetchCoverPictureAndPrepareTag() {
		ContentVersion cv = new ContentVersion(
			PathOnClient = 'COM-styling.pdf',
			Title = 'COM-styling',
			OwnerId = UserInfo.getUserId(),
			FirstPublishLocationId = UserInfo.getUserId(),
			VersionData = Blob.valueOf('Test me!')
		);
		insert cv;

		ContentDistribution cds = new ContentDistribution(ContentVersion = cv, Name = 'COM-styling 29-6-2022', ContentVersionId = cv.Id);
		insert cds;

		Test.startTest();
		String result = EmailService.fetchCoverPictureAndPrepareTag('test-template');
		Test.stopTest();

		System.assertEquals(
			true,
			(result.contains(COM_Constants.COVER_PICTURE_TAG_START) && result.contains(COM_Constants.COVER_PICTURE_TAG_END)),
			'Result does not contain required tags.'
		);
	}
}
