public with sharing class BiccBanCustValBatchExecuteController {

	public BiccBanCustValBatchExecuteController(ApexPages.StandardSetController ignored) {

	}

	public PageReference callBatch(){
		BiccBanCustomerValueBatch controller = new BiccBanCustomerValueBatch();
		database.executebatch(controller);
		Schema.DescribeSObjectResult prefix = BICC_BAN_CUST_VALUE__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}
}