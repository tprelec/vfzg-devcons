public with sharing class BanService {
	private List<Ban__c> newBans;
	private Map<Id, Ban__c> oldBansMap;
	private List<Ban__Share> newShares;
	private List<Ban__Share> oldShares;
	private List<Ban> bans;

	public BanService(List<Ban__c> newBans, Map<Id, Ban__c> oldBansMap) {
		this.newBans = newBans;
		this.oldBansMap = oldBansMap;
		if (this.oldBansMap == null) {
			this.oldBansMap = new Map<Id, Ban__c>();
		}
	}

	/**
	 * Inits BAN wrappers
	 */
	private void initBans() {
		bans = new List<Ban>();
		Set<Id> accountIds = GeneralUtils.getIDSetFromList(newBans, 'Account__c');
		accountIds.addAll(GeneralUtils.getIDSetFromList(oldBansMap.values(), 'Account__c'));
		Map<Id, Account> accountMap = getAccounts(accountIds);
		for (Ban__c newBan : newBans) {
			Ban ban = new Ban(newBan);
			ban.account = accountMap.get(newBan.Account__c);
			ban.oldBan = oldBansMap.get(newBan.Id);
			if (ban.oldBan != null) {
				ban.oldAccount = accountMap.get(ban.oldBan.Account__c);
			}
			bans.add(ban);
		}
	}

	/**
	 * Gets Accounts with all needed fields based on Account IDs
	 * @param  accountIds Account IDs
	 * @return            Map of Accounts
	 */
	private Map<Id, Account> getAccounts(Set<Id> accountIds) {
		if (accountIds.isEmpty()) {
			return new Map<Id, Account>();
		}
		return new Map<Id, Account>(
			[
				SELECT Id, Fixed_Dealer__c, Fixed_Dealer__r.ContactUserId__c
				FROM Account
				WHERE Id IN :accountIds
			]
		);
	}

	/**
	 * Shares BAN records with Fixed Dealers and Partners
	 */
	public void shareBans() {
		shareBans(null);
	}

	/**
	 * Shares BAN records
	 */
	public void shareBans(Set<BanSharingType> shareTypes) {
		newShares = new List<Ban__Share>();
		oldShares = new List<Ban__Share>();
		initBans();
		if (shareTypes == null || shareTypes.contains(BanSharingType.FIXED_DEALER)) {
			shareWithFixedDealers();
		}
		if (shareTypes == null || shareTypes.contains(BanSharingType.PARTNER)) {
			shareWithPartners();
		}
		if (!newShares.isEmpty()) {
			SharingUtils.insertRecordsWithoutSharing(newShares);
		}
		if (!oldShares.isEmpty()) {
			SharingUtils.deleteRecordsWithoutSharing(oldShares);
		}
	}

	/**
	 * Shares BAN records with new Fixed Dealer, based on the Dealer hierarchy
	 * Unshares BAN records from old Fixed Dealers
	 */
	private void shareWithFixedDealers() {
		// Check which BANs need to be shared with new Fixed Dealer
		// Check which BANS need to be unshared from old Fixed Dealer
		List<Ban> bansWithNewFixedDealer = new List<Ban>();
		List<Ban> bansWithOldFixedDealer = new List<Ban>();
		for (Ban ban : bans) {
			if (ban.isFixedDealerChanged()) {
				if (ban.account.Fixed_Dealer__c != null) {
					bansWithNewFixedDealer.add(ban);
				}
				if (ban.oldAccount?.Fixed_Dealer__c != null) {
					bansWithOldFixedDealer.add(ban);
				}
			}
		}
		shareWithNewFixedDealers(bansWithNewFixedDealer);
		unshareFromOldFixedDealers(bansWithOldFixedDealer);
	}

	/**
	 * Shares BAN records with Fixed Dealer, based on the Dealer hierarchy
	 */
	private void shareWithNewFixedDealers(List<Ban> bansWithNewFixedDealer) {
		if (bansWithNewFixedDealer.isEmpty()) {
			return;
		}
		Set<Id> dealerIds = new Set<Id>();
		for (Ban ban : bansWithNewFixedDealer) {
			dealerIds.add(ban.account.Fixed_Dealer__c);
		}
		// Get Dealers
		DealerService dealerSvc = new DealerService();
		Map<Id, DealerService.Dealer> dealersMap = dealerSvc.getDealersById(dealerIds);
		// Create Shares
		for (Ban ban : bansWithNewFixedDealer) {
			DealerService.Dealer dealer = dealersMap.get(ban.account.Fixed_Dealer__c);
			if (dealer.getGroupOrUserId() != null) {
				newShares.add(createBanShare(ban.ban, dealer.getGroupOrUserId()));
			}
		}
	}

	/**
	 * Unshares BAN records from old Fixed Dealers
	 */
	private void unshareFromOldFixedDealers(List<Ban> bansWithOldFixedDealer) {
		if (bansWithOldFixedDealer.isEmpty()) {
			return;
		}
		Set<Id> banIds = new Set<Id>();
		Set<Id> dealerIds = new Set<Id>();
		for (Ban ban : bansWithOldFixedDealer) {
			banIds.add(ban.ban.Id);
			dealerIds.add(ban.oldAccount.Fixed_Dealer__c);
		}
		// Get Dealers
		DealerService dealerSvc = new DealerService();
		Map<Id, DealerService.Dealer> dealersMap = dealerSvc.getDealersById(dealerIds);
		Set<Id> groupOrUserIds = new Set<Id>();
		for (DealerService.Dealer dealer : dealersMap.values()) {
			groupOrUserIds.add(dealer.getGroupOrUserId());
		}
		oldShares.addAll(
			[
				SELECT Id
				FROM Ban__Share
				WHERE
					ParentId IN :banIds
					AND UserOrGroupId IN :groupOrUserIds
					AND RowCause = :Schema.Ban__share.RowCause.Dealer_Hierarchy__c
			]
		);
	}

	/**
	 * Shares BAN with Partner users
	 */
	private void shareWithPartners() {
		List<Ban> bansWithNewOwner = new List<Ban>();
		Set<Id> newOwnerIds = new Set<Id>();
		for (Ban ban : bans) {
			if (ban.isOwnerChanged()) {
				bansWithNewOwner.add(ban);
				newOwnerIds.add(ban.ban.OwnerId);
			}
		}
		if (bansWithNewOwner.isEmpty()) {
			return;
		}
		DealerService dealerSvc = new DealerService();
		Map<Id, DealerService.Dealer> dealersByOwnerIds = dealerSvc.getDealersByUserIds(
			newOwnerIds
		);
		for (Ban ban : bansWithNewOwner) {
			DealerService.Dealer dealer = dealersByOwnerIds.get(ban.ban.OwnerId);
			if (dealer?.parentDealer != null && dealer.parentDealer.dealerUser.IsActive) {
				newShares.add(createBanShare(ban.ban, dealer.parentDealer.dealerUser.Id));
			}
		}
		//TODO: This part should be refactored in Account Service
		Map<Id, Id> accountToBanOwner = new Map<Id, Id>();
		for (Ban ban : bansWithNewOwner) {
			accountToBanOwner.put(ban.ban.Account__c, ban.ban.OwnerId);
		}
		Map<Id, Id> parentPartnerUserIds = GeneralUtils.getParentPartnerUserIdsFromUserIds(
			newOwnerIds
		);
		SharingUtils.createAccountSharing(accountToBanOwner, newOwnerIds, parentPartnerUserIds);
	}

	/**
	 * Creates Ban Share record
	 * @param  ban           BAN
	 * @param  userOrGroupId User or Group ID
	 * @return               Ban Share Record
	 */
	private Ban__Share createBanShare(Ban__c ban, Id userOrGroupId) {
		return new Ban__Share(
			RowCause = Schema.Ban__Share.RowCause.Dealer_Hierarchy__c,
			ParentId = ban.Id,
			UserOrGroupId = userOrGroupId,
			AccessLevel = 'read'
		);
	}

	public class Ban {
		public Ban__c ban;
		public Ban__c oldBan;
		public Account account;
		public Account oldAccount;

		public Ban(Ban__c ban) {
			this.ban = ban;
		}

		public Boolean isFixedDealerChanged() {
			return oldBan == null || account.Fixed_Dealer__c != oldAccount.Fixed_Dealer__c;
		}

		public Boolean isOwnerChanged() {
			return oldBan == null || ban.OwnerId != oldBan.OwnerId;
		}
	}

	public enum BanSharingType {
		FIXED_DEALER,
		PARTNER
	}
}