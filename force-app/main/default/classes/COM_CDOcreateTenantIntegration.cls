public with sharing class COM_CDOcreateTenantIntegration {
	/*
	@TestVisible
	public static final String COM_CDO_INTEGRATION_SETTING_NAME = 'createTenant';

	private static final String MOCKSYNCSUCCESS = '{ "id":"b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "href":"https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId":"SF", "priority":"1", "description":"Tenant Service Order", "category":"Tenant Service", "state":"acknowledged", "orderDate":"2022-02-09T10:37:15.97221Z", "@type":"standard", "orderItem":[ { "id":"1", "action":"add", "state":"ana2", "@type":"standard", "service":{ "id":"4ed3dece-d5bd-442f-a1f9-687c0a1c52221", "serviceCharacteristic":[ { "name":"sourcesystem", "valueType":"string", "value":{ "@type":"string", "sourcesystem":"SF" } } ], "serviceSpecification":{ "id":"bdab731e-d5de-4135-8849-5c724ea01041", "invariantID":"930d7161-0eb8-444f-8d11-5971812246d5", "version":"1.0.0", "name":"Tenant Service", "@type":"ServiceSpecification" } } } ] }';
	private static final String MOCKSYNCFAILURE = '{ "code": 40020, "reason": "SERVICE_ORDER_CHARACTERISTICS_VALIDATION_ERROR", "message": "Request validation has failed due the following issue(s) - [ Input parameter tenantContactEmail violates constraint, Invalid value ]" }';

	public static void createTenantService(Id accountId) {
		COM_CDOcreateTenantService request = COM_CDOcreateTenantService.generatePayload();
		createTenantService(request, accountId);
	}

	private static void createTenantService(COM_CDOcreateTenantService req, Id accountId) {
		COM_Integration_setting__mdt createTenantSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_INTEGRATION_SETTING_NAME
		);
		Boolean doNotExecuteReq = createTenantSetting != null
			? createTenantSetting.Execute_mock__c
			: true;
		Boolean doNotExecuteStatus = createTenantSetting != null
			? createTenantSetting.Mock_Execution_result__c
			: true;

		HTTPResponse response = sendCreateTenantRequest(req, doNotExecuteReq, doNotExecuteStatus);

		if (failedResponse(response)) {
			CreateTenantSyncErrorResponse responseDeserialized = (CreateTenantSyncErrorResponse) JSON.deserializeStrict(
				response.getBody(),
				CreateTenantSyncErrorResponse.class
			);

			CreateTenantAccountUpdateData accountUpdate = new CreateTenantAccountUpdateData();
			accountUpdate.accountId = accountId;
			accountUpdate.successful = false;
			accountUpdate.tenantId = null;
			accountUpdate.umbrellaService = null;
			accountUpdate.errorMessage = responseDeserialized.message;

			updateAccount(accountUpdate);
		} else {
			String reqBody = COM_CDOcreateTenantService.replaceJsonKeysToFitDeserializeClass(
				response.getBody()
			);
			COM_CDOcreateTenantService body = (COM_CDOcreateTenantService) JSON.deserializeStrict(
				reqBody,
				COM_CDOcreateTenantService.class
			);
			String umbrellaService = COM_CDOcreateTenant.getUmbrellaService(body);

			CreateTenantAccountUpdateData accountUpdate = new CreateTenantAccountUpdateData();
			accountUpdate.accountId = accountId;
			accountUpdate.successful = true;
			accountUpdate.tenantId = null;
			accountUpdate.umbrellaService = umbrellaService;
			accountUpdate.errorMessage = null;

			updateAccount(accountUpdate);
		}
	}

	private static Boolean failedResponse(HTTPResponse response) {
		Boolean result = false;
		try {
			CreateTenantSyncErrorResponse responseDeserialized = (CreateTenantSyncErrorResponse) JSON.deserializeStrict(
				response.getBody(),
				CreateTenantSyncErrorResponse.class
			);
			if (responseDeserialized.code != null) {
				result = true;
			}
		} catch (Exception e) {
			result = false;
		}

		return result;
	}
	public static HTTPResponse sendCreateTenantRequest(
		COM_CDOcreateTenantService req,
		Boolean doNotExecute,
		Boolean doNotExecuteStatus
	) {
		HttpRequest reqData = new HttpRequest();
		Http http = new Http();
		reqData.setEndpoint('callout:CDO_createTenant?format=json');

		String requestBody = COM_CDOcreateTenantService.generatePayloadJson(req);
		System.debug(LoggingLevel.DEBUG, '### Request body: ' + requestBody);
		reqData.setBody(requestBody);
		reqData.setMethod('POST');
		System.debug(LoggingLevel.DEBUG, '@@@@ reqData: ' + reqData);
		HTTPResponse response = new HttpResponse();
		if (doNotExecute && !Test.isRunningTest()) {
			response.setHeader('Content-Type', 'application/json');
			if (doNotExecuteStatus) {
				String mockSuccess = getMockResponseSuccess();
				mockSuccess = mockSuccess != null ? mockSuccess : MOCKSYNCSUCCESS;
				response.setBody(mockSuccess);
				response.setStatusCode(200);
			} else {
				String mockFailed = getMockResponseFailed();
				mockFailed = mockFailed != null ? mockFailed : MOCKSYNCFAILURE;
				response.setBody(mockFailed);

				response.setStatusCode(400);
			}
		} else {
			response = http.send(reqData);
		}

		System.debug(LoggingLevel.DEBUG, '@@@@ response: ' + response.getBody());
		return response;
	}

	public static String getMockResponseSuccess() {
		String result = null;
		StaticResource sr = [
			SELECT id, body
			FROM StaticResource
			WHERE Name = 'COM_CDO_createTenantSyncSuccess'
		];
		if (sr != null) {
			result = sr.body.toString();
		}

		return result;
	}

	public static String getMockResponseFailed() {
		String result = null;
		StaticResource sr = [
			SELECT id, body
			FROM StaticResource
			WHERE Name = 'COM_CDO_createTenantSyncFailed'
		];

		if (sr != null) {
			result = sr.body.toString();
		}

		return result;
	}

	public static Id getAccountIdByUmbrellaService(String umbrellaService) {
		Account acc = [
			SELECT Id
			FROM Account
			WHERE CDO_Umbrella_Service_Id__c = :umbrellaService
			LIMIT 1
		];
		if (acc != null) {
			return acc.Id;
		} else {
			return null;
		}
	}

	public static String getTenantId(COM_CDOcreateTenantService req) {
		String result = '';
		for (COM_CDOcreateTenantService.OrderItem orderItem : req.orderItem) {
			for (
				COM_CDOcreateTenantService.ServiceCharacteristic serviceCharacteristic : orderItem.service.serviceCharacteristic
			) {
				if (serviceCharacteristic.name == 'tenantId') {
					result = serviceCharacteristic.value.valueOf;
				}
			}
		}
		return result;
	}

	public static String getUmbrellaService(COM_CDOcreateTenantService req) {
		String result = '';
		for (COM_CDOcreateTenantService.OrderItem orderItem : req.orderItem) {
			result = orderItem.service.id;
		}
		return result;
	}

	public static void updateAccount(CreateTenantAccountUpdateData accountUpdate) {
		Account acc = new Account();
		acc.CDO_Umbrella_Service_Id__c = accountUpdate.umbrellaService != null
			? accountUpdate.umbrellaService
			: '';
		acc.Id = accountUpdate.accountId;

		if (accountUpdate.successful) {
			if (accountUpdate.tenantId != null) {
				acc.CDO_Tenant_Id__c = accountUpdate.tenantId;
				acc.CDO_Integration_Status__c = 'Complete';
			} else {
				acc.CDO_Integration_Status__c = '';
			}
			acc.CDO_Error_Message__c = '';
		} else {
			acc.CDO_Integration_Status__c = 'Failed';
			acc.CDO_Error_Message__c = accountUpdate.errorMessage;
		}
		update acc;
	}

	public class CreateTenantSyncErrorResponse {
		public Integer code;
		public String reason;
		public String message;
	}

	public class CreateTenantAccountUpdateData {
		public Id accountId;
		public Boolean successful;
		public String tenantId;
		public String umbrellaService;
		public String errorMessage;
	}
	*/
}
