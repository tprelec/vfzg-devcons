@isTest
private class TestCOM_SrvDeliveryComponentsTableCntrl {
	@TestSetup
	static void testSetup() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, true);

		Task testTask = new Task();
		testTask.Subject = 'TestInstallation';
		testTask.COM_Delivery_Order__c = delOrd.Id;
		insert testTask;

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(null, delOrd.Id, 'TestService', null, subscription.Id, null, null, null);
		insert service;

		csord__Service__c childService = CS_DataTest.createService(
			null,
			delOrd.Id,
			'TestServiceChild',
			null,
			subscription.Id,
			null,
			service.Id,
			null
		);
		insert childService;
	}

	@isTest
	private static void testOverviewByDeliveryOrderId() {
		COM_Delivery_Order__c delOrd = [SELECT Id FROM COM_Delivery_Order__c WHERE Name = 'TestDelOrd'];

		Test.startTest();
		List<COM_SrvDeliveryComponentsTableCntrl.ServiceWrapper> wrappers = COM_SrvDeliveryComponentsTableCntrl.getServices(delOrd.Id);
		Test.stopTest();

		System.assertEquals(2, wrappers.size(), 'Size is not the same.');
		System.assertEquals('TestServiceChild', wrappers[1].name, 'Name is not the same.');
		System.assertEquals('TestService', wrappers[0].parentServiceName, 'Name is not the same.');
	}

	@isTest
	private static void testOverviewByTaskId() {
		Task testTask = [SELECT Id FROM Task WHERE Subject = 'TestInstallation'];

		Test.startTest();
		List<COM_SrvDeliveryComponentsTableCntrl.ServiceWrapper> wrappers = COM_SrvDeliveryComponentsTableCntrl.getServices(testTask.Id);
		Test.stopTest();

		System.assertEquals(2, wrappers.size(), 'Size is not the same.');
		System.assertEquals('TestServiceChild', wrappers[1].name, 'Name is not the same.');
		System.assertEquals('TestService', wrappers[0].parentServiceName, 'Name is not the same.');
	}
}
