global with sharing class CS_AfterMacdBasketCreated implements csordcb.ObserverApi.IObserver{

    global void execute(csordcb.ObserverApi.Observable o, Object arg) {
        System.debug('Running CS_AfterMacdBasketCreated observer');
        csordtelcoa.ChangeRequestObservable observable = (csordtelcoa.ChangeRequestObservable)o;
        List<Id> clonedIds = observable.getProductConfigurationIds();
        System.debug('CS_AfterMacdBasketCreated: TOTAL config count ' + clonedIds.size());
        // List <cscfga__Product_Configuration__c> pcList = [
        //     SELECT 
        //     Id, 
        //     Name,
        //     ContractNumber_JSON__c,
        //     Contract_Number__c,
        //     Contract_Number_Group__c 
        //     FROM cscfga__Product_Configuration__c
        //     WHERE Id IN :clonedIds];

        }
}