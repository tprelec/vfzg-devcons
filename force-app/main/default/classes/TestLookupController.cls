/*
 * @author Rahul Sharma
 *
 * @description Test class for LookupController
 */
@IsTest
private class TestLookupController {
	@IsTest
	static void testCreateNewConfig() {
		Test.startTest();
		LookupController.LookupCreateNewConfiguration config = new LookupController.LookupCreateNewConfiguration()
			.setName('a')
			.setValue('b')
			.setDisabled(true)
			.setRequired(true);
		Test.stopTest();
		System.assertEquals('a', config.name, 'The config name should be a.');
		System.assertEquals('b', config.value, 'The config name should be b.');
		System.assertEquals(true, config.isDisabled, 'isDisabled should be true.');
		System.assertEquals(true, config.isRequired, 'isRequired should be true.');
	}

	@IsTest
	static void testGetRecentRecords() {
		User owner = TestUtils.createAdministrator();
		TestUtils.createAccount(owner);
		TestUtils.createAccount(owner);

		Test.startTest();
		LightningResponse response = LookupController.getRecent(new LookupController.LookupRequest('', 'Account', 'Name', 'Phone', '', '', 10));
		Test.stopTest();
		System.assertEquals(null, response.message, 'response.message is not null');
		System.assertEquals(null, response.variant, 'response.variant is not null');
	}
	@IsTest
	static void testGetAccounts() {
		User owner = TestUtils.createAdministrator();
		TestUtils.createAccount(owner);
		TestUtils.createAccount(owner);

		Test.startTest();
		LightningResponse response = LookupController.getRecords(new LookupController.LookupRequest('', 'Account', 'Name', 'Phone', '', '', 10));
		Test.stopTest();

		List<LookupController.LookupResult> result = (List<LookupController.LookupResult>) response.body;
		System.assertEquals(null, response.message, 'Response.message should be null');
		System.assertEquals(null, response.variant, 'Response.variant should be null');
		System.assertNotEquals(0, result.size(), 'Result.size should not be 0');
	}

	@IsTest
	static void testGetAccount() {
		User owner = TestUtils.createAdministrator();
		Account account = TestUtils.createAccount(owner);

		Test.startTest();
		LightningResponse response = LookupController.getRecord(new LookupController.LookupRequest('', 'Account', 'Name', '', '', account.Id, 10));
		Test.stopTest();

		List<LookupController.LookupResult> result = (List<LookupController.LookupResult>) response.body;
		System.assertEquals(null, response.message, 'Response.message should be null');
		System.assertEquals(null, response.variant, 'Response.variant should be null');
		System.assertNotEquals(0, result.size(), 'Result.size should not be 0');
	}
}
