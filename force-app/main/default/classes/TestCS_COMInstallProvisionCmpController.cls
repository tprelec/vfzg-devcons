@IsTest
private class TestCS_COMInstallProvisionCmpController {
    
    @IsTest
    static void testGetData() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            ord.csord__Account__c = testAccount.Id;
            insert ord;

            Site__c testSite = CS_DataTest.createSite('Test Site',testAccount,'1032AB','Street','City',10.0);
            testSite.Footprint__c = null;
            insert testSite;
            
            COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', ord.Id, false);
            deliveryOrder1.Product_Abbreviations__c = 'ACCESS,ZIPRO';
            deliveryOrder1.Site__c = testSite.Id;
            insert deliveryOrder1;

            csord__Subscription__c subscription= CS_DataTest.createSubscription(testConfConf.Id);
            subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            subscription.csord__Order__c = ord.Id;
            insert subscription;

            csord__Service__c parentService= CS_DataTest.createService(testConfConf.Id,subscription, 'Service Created');
            parentService.csord__Identification__c = 'testSubscription';
            parentService.COM_Delivery_Order__c = deliveryOrder1.Id;
            parentService.csord__Order__c = ord.Id;
            parentService.VFZ_Product_Abbreviation__c = 'ZIPRO';
            insert parentService;

            List<Delivery_Component__c> deliveryComponents = CS_DataTest.createDeliveryComponents(parentService,'TestComponent', 3, false);
            for (Delivery_Component__c dc : deliveryComponents) {
                dc.COM_Delivery_Order__c = deliveryOrder1.Id;
                dc.MACD_Action__c = 'Add';
                dc.Article_Name__c = 'Coax/HFC';
                dc.Article_Code__c = 'Coax_HFC';
            }
            insert deliveryComponents;

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', false);
            testProcessTemplate.MACD_Flow_Type__c = 'New Add Provisioning';
            insert testProcessTemplate;
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.COM_Delivery_Order__c = deliveryOrder1.Id;
            testProcess.Order__c = ord.Id;
            insert testProcess;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, false);
            step1.COM_Task_Status__c = 'Closed - Planning Rescheduled';
            //step1.CSPOFA__Case_Record_Type__c = 'CS COM Install DeliveryOrder';
            insert step1;

            Task testTask = CS_DataTest.createTask('Provisioning robot', 'COM Delivery', simpleUser, false);
            testTask.WhatId = deliveryOrder1.Id;
            testTask.Type = 'COM_Provisioning';
            //testCase1.Order__c = ord.Id;
            testTask.CSPOFA__Orchestration_Step__c = step1.Id;
            insert testTask;

            List<Task> taskResult = [SELECT Id, Subject FROM Task WHERE Id = :testTask.Id];

            for (CS_ComDeliveryComponentWrapper wrapper :CS_COMInstallProvisionCmpController.getData(testTask.Id)) {
                System.assertEquals(wrapper.productName,parentService.Id);
            }
        }
    }
    
    @IsTest
    static void testGetDataWitheReplacedDeliveryComponents(){
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            ord.csord__Account__c = testAccount.Id;
            insert ord;

            Site__c testSite = CS_DataTest.createSite('Test Site',testAccount,'1032AB','Street','City',10.0);
            testSite.Footprint__c = null;
            insert testSite;
            
            COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', ord.Id, false);
            deliveryOrder1.Product_Abbreviations__c = 'ACCESS,ZIPRO';
            deliveryOrder1.Site__c = testSite.Id;
            insert deliveryOrder1;
            
            COM_Delivery_Order__c oldDeliveryOrder = CS_DataTest.createDeliveryOrder('OldDeliveryOrder',ord.Id,true);

            csord__Subscription__c subscription= CS_DataTest.createSubscription(testConfConf.Id);
            subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            insert subscription;

            csord__Service__c zipReplacedParentService= CS_DataTest.createService(testConfConf.Id,subscription,'Service Created');
            zipReplacedParentService.csord__Identification__c = 'testSubscription';
            zipReplacedParentService.VFZ_Commercial_Component_Name__c = 'Capacity';
            zipReplacedParentService.VFZ_Commercial_Article_Name__c = 'Max';
            zipReplacedParentService.COM_Delivery_Order__c = oldDeliveryOrder.Id;
            //zipReplacedParentService.csord__Order__c = ordersList[0].Id;
            insert zipReplacedParentService;

            Integer numOfRecords = 2;
            List<Delivery_Component__c> zipReplacedDeliveryComponents = CS_DataTest.createDeliveryComponents(zipReplacedParentService,'',numOfRecords,false);
            for (Integer i = 0; i < numOfRecords; i++) {
                if (i == 0) {
                    zipReplacedDeliveryComponents[i].Name = 'TestComponent';
                    zipReplacedDeliveryComponents[i].Article_Name__c = 'TestComponent';
                    zipReplacedDeliveryComponents[i].Article_Code__c = '200/30';
                } else if (i == 1){
                    zipReplacedDeliveryComponents[i].Name = 'TestComponent';
                    zipReplacedDeliveryComponents[i].Article_Name__c = 'TestComponent';
                    zipReplacedDeliveryComponents[i].Article_Code__c = 'Coax_HFC';
                }
            }
            insert zipReplacedDeliveryComponents;

            csord__Service__c parentService= CS_DataTest.createService(testConfConf.Id,subscription, 'Service Created');
            parentService.csord__Identification__c = 'testSubscription';
            parentService.COM_Delivery_Order__c = deliveryOrder1.Id;
            parentService.VFZ_Product_Abbreviation__c = 'ZIPRO';
            parentService.csordtelcoa__Replaced_Service__c = zipReplacedParentService.Id;
            insert parentService;

            List<Delivery_Component__c> deliveryComponents = CS_DataTest.createDeliveryComponents(parentService,'Coax_HFC', 3, false);
            for (Delivery_Component__c dc : deliveryComponents) {
                dc.COM_Delivery_Order__c = deliveryOrder1.Id;
                dc.MACD_Action__c = 'Add';
                dc.Article_Name__c = 'Coax/HFC';
                dc.Article_Code__c = 'Coax_HFC';
            }
            insert deliveryComponents;

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', false);
            testProcessTemplate.MACD_Flow_Type__c = 'New Add Provisioning';
            insert testProcessTemplate;
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.COM_Delivery_Order__c = deliveryOrder1.Id;
            insert testProcess;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, false);
            step1.COM_Task_Status__c = 'Closed - Planning Rescheduled';
            //step1.CSPOFA__Case_Record_Type__c = 'CS COM Install DeliveryOrder';
            insert step1;

            Task testTask = CS_DataTest.createTask('Provisioning robot', 'COM Delivery', simpleUser, false);
            testTask.WhatId = deliveryOrder1.Id;
            testTask.Type = 'COM_Provisioning';
            //testCase1.Order__c = ord.Id;
            testTask.CSPOFA__Orchestration_Step__c = step1.Id;
            insert testTask;

            List<Task> taskResult = [SELECT Id, Subject FROM Task WHERE Id = :testTask.Id];

            for (CS_ComDeliveryComponentWrapper wrapper :CS_COMInstallProvisionCmpController.getData(testTask.Id)) {
                System.assertEquals(wrapper.productName,parentService.Id);
            }
        }
    }

    @IsTest
    static void testGetDataForInstallationCase(){
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser) {
            Group newGroup = [SELECT Id FROM Group WHERE DeveloperName='CS_Test_Queue_for_Cases' LIMIT 1];

            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            ord.csord__Account__c = testAccount.Id;
            insert ord;

            Site__c testSite = CS_DataTest.createSite('Test Site',testAccount,'1032AB','Street','City',10.0);
            testSite.Footprint__c = null;
            insert testSite;
            
            COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', ord.Id, false);
            deliveryOrder1.Product_Abbreviations__c = 'ACCESS,ZIPRO';
            deliveryOrder1.Confirmed_Installation__c = true;
            deliveryOrder1.Site__c = testSite.Id;
            insert deliveryOrder1;

            Delivery_Appointment__c testAppointment = new Delivery_Appointment__c();
            testAppointment.OwnerId = UserInfo.getUserId();
            testAppointment.Name = 'Test Appointment';
            testAppointment.Start__c = Datetime.now();
            testAppointment.Duration__c = 120;
            testAppointment.Location__c = 'testLocation';
            testAppointment.Contact__c = null;
            testAppointment.Description__c = 'just a test';
            testAppointment.Related_Delivery_Order__c = deliveryOrder1.Id;
            testAppointment.Field_Engineer__c = simpleUser.Id;
            insert testAppointment;

            csord__Subscription__c subscription= CS_DataTest.createSubscription(testConfConf.Id);
            subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            insert subscription;

            csord__Service__c parentService= CS_DataTest.createService(testConfConf.Id,subscription, 'Service Created');
            parentService.csord__Identification__c = 'testSubscription';
            parentService.COM_Delivery_Order__c = deliveryOrder1.Id;
            parentService.VFZ_Product_Abbreviation__c = 'ZIPRO';
            insert parentService;

            List<Delivery_Component__c> deliveryComponents = CS_DataTest.createDeliveryComponents(parentService,'TestComponent', 3, false);
            for (Delivery_Component__c dc : deliveryComponents) {
                dc.COM_Delivery_Order__c = deliveryOrder1.Id;
                dc.MACD_Action__c = 'Add';
                dc.Article_Name__c = 'Coax/HFC';
                dc.Article_Code__c = 'Coax_HFC';
            }
            insert deliveryComponents;

            CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', false);
            testProcessTemplate.MACD_Flow_Type__c = 'New Add Provisioning';
            insert testProcessTemplate;
            CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Step 1', '3', true);

            CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(testProcessTemplate.Id, null, Datetime.newInstance(2020, 3, 27), Datetime.newInstance(2020, 3, 27), false);
            testProcess.COM_Delivery_Order__c = deliveryOrder1.Id;
            insert testProcess;

            CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, false);
            //step1.COM_Task_Status__c = 'Closed - Planning Rescheduled';
            //step1.CSPOFA__Case_Record_Type__c = 'CS COM Install DeliveryOrder';
            insert step1;

            Task testTask = CS_DataTest.createTask('Provisioning robot', 'COM Delivery', simpleUser, false);
            testTask.WhatId = deliveryOrder1.Id;
            testTask.Type = 'COM_Provisioning';
            //testCase1.Order__c = ord.Id;
            testTask.CSPOFA__Orchestration_Step__c = step1.Id;
            //testCase1.OwnerId = newGroup.Id;
            //testCase1.Case_Start_Date__c = Date.today();
            insert testTask;
            
            CS_COMInstallProvisionCmpController.getDeliveryComponentWrappers(deliveryComponents, new List<COM_Delivery_Component_Article_Materials__mdt>(), new Map<Id,List<Delivery_Component__c>>(), new Map<String,String>(), new Map<Id,Id>(), new Map<Id,List<Delivery_Component__c>>(), new Map<Id,Delivery_Component__c>(), new Map<String,COM_Delivery_Component__mdt>(), new Map<String,COM_Delivery_Component__mdt>(), new Map<Id,List<Delivery_Component__c>>());
            
            /*
            for (CS_ComDeliveryComponentWrapper wrapper :CS_COMInstallProvisionCmpController.getData(testCase1.Id)) {
                System.assertEquals(wrapper.productName,parentService.Id);
            }*/
        }
    }

    @isTest
    static void testGetDeliveryComponentNames() {
        List<COM_Delivery_Component__mdt> componentActivities = [select id, Label from COM_Delivery_Component__mdt limit 100];
        
        Test.startTest();
        Set<String> result = CS_COMInstallProvisionCmpController.getDeliveryComponentNames(componentActivities);
        Test.stopTest();

        System.assertNotEquals(0, result.size());
    }

    @isTest
    static void testAdditionalMethods() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();
        System.runAs(simpleUser) {
            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            testOpp.csordtelcoa__Change_Type__c = 'Change';
            insert testOpp;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            ord.csordtelcoa__Opportunity__c = testOpp.Id;
            insert ord;

            Site__c testSite = CS_DataTest.createSite('Test Site',testAccount,'1032AB','Street','City',10.0);
            testSite.Footprint__c = null;
            insert testSite;
            
            COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', ord.Id, false);
            deliveryOrder1.Product_Abbreviations__c = 'ACCESS,ZIPRO';
            deliveryOrder1.Confirmed_Installation__c = true;
            deliveryOrder1.Site__c = testSite.Id;
            insert deliveryOrder1;

            Delivery_Appointment__c testAppointment = new Delivery_Appointment__c();
            testAppointment.OwnerId = UserInfo.getUserId();
            testAppointment.Name = 'Test Appointment';
            testAppointment.Start__c = Datetime.now();
            testAppointment.Duration__c = 120;
            testAppointment.Location__c = 'testLocation';
            testAppointment.Contact__c = null;
            testAppointment.Description__c = 'just a test';
            testAppointment.Related_Delivery_Order__c = deliveryOrder1.Id;
            testAppointment.Field_Engineer__c = simpleUser.Id;
            insert testAppointment;

            csord__Subscription__c subscription= CS_DataTest.createSubscription(testConfConf.Id);
            subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            insert subscription;

            csord__Service__c parentService= CS_DataTest.createService(testConfConf.Id,subscription, 'Service Created');
            parentService.csord__Identification__c = 'testSubscription1';
            parentService.COM_Delivery_Order__c = deliveryOrder1.Id;
            parentService.VFZ_Product_Abbreviation__c = 'ZIPRO';
            parentService.csord__Order__c = ord.Id;
            insert parentService;

            csord__Service__c parentService2= CS_DataTest.createService(testConfConf.Id,subscription, 'Service Created');
            parentService2.csord__Identification__c = 'testSubscription2';
            parentService2.COM_Delivery_Order__c = deliveryOrder1.Id;
            parentService2.VFZ_Product_Abbreviation__c = 'ZIPRO';
            parentService2.VFZ_Depends_On_These_Service_Identifiers__c = parentService.Id;
            parentService2.csord__Order__c = ord.Id;
            insert parentService2;

            List<Delivery_Component__c> deliveryComponents = CS_DataTest.createDeliveryComponents(parentService2,'TestComponent', 1, false);
            for (Delivery_Component__c dc : deliveryComponents) {
                //dc.COM_Delivery_Order__c = deliveryOrder1.Id;
                dc.MACD_Action__c = 'Add';
                dc.Article_Name__c = 'Coax/HFC';
                dc.Article_Code__c = 'Coax_HFC';
            }
            insert deliveryComponents;

            List<Delivery_Component__c> deliveryComponents_Child = CS_DataTest.createDeliveryComponents(parentService2,'TestComponent_Child', 1, false);
            for (Delivery_Component__c dc : deliveryComponents_Child) {
                //dc.COM_Delivery_Order__c = deliveryOrder1.Id;
                dc.MACD_Action__c = 'Add';
                dc.Article_Name__c = 'Coax/HFC';
                dc.Article_Code__c = 'Coax_HFC';
            }
            insert deliveryComponents_Child;

            List<Delivery_Component__c> delComp = [
                                            select id, name, Service__c, Service__r.VFZ_Depends_On_These_Service_Identifiers__c, Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c, Service__r.csord__Subscription__r.Name, Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c, Service__r.csordtelcoa__Replaced_Service__c
                                            from Delivery_Component__c 
                                            where id = :deliveryComponents[0].id];    

            List<Delivery_Component__c> delComp_Child = [
                                            select id, name, Service__c, Service__r.VFZ_Depends_On_These_Service_Identifiers__c, Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c, Service__r.csord__Subscription__r.Name, Service__r.csord__Service__r.csordtelcoa__Replaced_Service__c, Service__r.csordtelcoa__Replaced_Service__c
                                            from Delivery_Component__c 
                                            where id = :deliveryComponents_Child[0].id];

            Map<Id,Id> dependentPCIdServiceId = new Map<Id,Id>{parentService.Id => parentService2.Id};
            Map<Id,List<Delivery_Component__c>> dependentParentServiceIdDeliveryComponents= new Map<Id,List<Delivery_Component__c>>{parentService2.Id => delComp, parentService2.Id => delComp_Child};
            Map<Id,Delivery_Component__c> dependentServicesDeliveryComponentsMap = new Map<Id,Delivery_Component__c>{delComp[0].Id => delComp[0], delComp_Child[0].Id => delComp_Child[0]};
            List<COM_Delivery_Component_Article_Materials__mdt> articleMaterials = new List<COM_Delivery_Component_Article_Materials__mdt>();
            
            List<COM_Delivery_Component__mdt> deliveryComp = [SELECT Id, Label FROM COM_Delivery_Component__mdt WHERE Label = 'TestComponent' OR Label = 'TestComponent_Child'];
            Map<String,COM_Delivery_Component__mdt> deliveryComponentMdt = new Map<String,COM_Delivery_Component__mdt>();
            deliveryComponentMdt.put(deliveryComp[0].label, deliveryComp[0]);
            deliveryComponentMdt.put(deliveryComp[1].label, deliveryComp[1]);

            Test.startTest();
            List<CS_ComDependantDataWrapper> result = CS_COMInstallProvisionCmpController.getDependantData(delComp[0], dependentPCIdServiceId, dependentParentServiceIdDeliveryComponents, dependentServicesDeliveryComponentsMap);            
            String result3 = CS_COMInstallProvisionCmpController.getReplacedArticleCodeParent(delComp[0], dependentParentServiceIdDeliveryComponents);
            Map<Id,List<Delivery_Component__c>> result4 = CS_COMInstallProvisionCmpController.getDependentParentServiceIdDeliveryComponents(delComp);
            String result7 = CS_COMInstallProvisionCmpController.getReplacedArticleCodeChild(delComp[0],  dependentParentServiceIdDeliveryComponents);
            Test.stopTest();
        }
    }

    @isTest
    private static void testGetDeliveryTaskType() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();
        System.runAs(simpleUser) {
            Task testTask_1 = CS_DataTest.createTask('Test', 'COM Delivery', simpleUser, false);
            testTask_1.Type = 'COM_Provisioning';
            insert testTask_1;
            
            Task testTask_2 = CS_DataTest.createTask('Test', 'Task', simpleUser, false);
            testTask_2.Type = 'COM_Provisioning';
            insert testTask_2;

            Test.startTest();
            String result1 = CS_COMInstallProvisionCmpController.getDeliveryTaskType(testTask_1.Id);
            String result2 = CS_COMInstallProvisionCmpController.getDeliveryTaskType(testTask_2.Id);
            Test.stopTest();

            System.assertEquals('COM_Provisioning', result1);
            System.assertEquals('', result2);
        }
    }
}