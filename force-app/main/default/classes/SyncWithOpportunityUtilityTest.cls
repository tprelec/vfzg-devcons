@isTest
public with sharing class SyncWithOpportunityUtilityTest {
	public SyncWithOpportunityUtilityTest() {
	}

	@TestSetup
	private static void testSetup() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			/*
            Account account1 = CS_DataTest.createAccount('Account 1');
            Account account2 = CS_DataTest.createAccount('Account 2');
            List<Account> accList = new List<Account>{account1,account2};
            insert accList;

            Contact contact1 = new Contact(
                AccountId = account1.id,
                LastName = 'Last Name',
                FirstName = 'First Name',
                Contact_Role__c = 'Consultant',
                Email = 'test@vf.com'
            );
            insert contact1;

            Opportunity opp = CS_DataTest.createOpportunity(account1, 'Sync opportunity',simpleUser.id);
            Opportunity opp2 = new Opportunity();
            opp2.Name = 'OPP 2';
            opp2.Account = account2;
            opp2.StageName= 'Qualification';
            opp2.CloseDate = Date.Today()+10;

            List<Opportunity> oppList = new List<Opportunity>{opp, opp2};
            insert oppList;

            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Sync Basket');
            basket.Name = 'Sync Basket';
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = account1.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 24;
            basket.OneNet_Scenario__c = 'One Net Enterprise';
            basket.Fixed_Scenario__c = 'One Fixed Enterprise';
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020,10, 10);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 10, 10);
            basket.Number_of_SIP__c = 12;
            basket.Approved_Date__c = Date.today() - 4;
            basket.PortingPercentage__c = 10;
            basket.ProfitLoss_JSON__c = '{"KPI Direct Capex / Net Revenue":"0,00","KPI Payback Period(Months)":"0,00","KPI NPV":"0,00",' +
            '"KPI Net Incremental Billed Revenue":"0,00","KPI Free cash flow / Net revenue":"1,50","KPI EBITDA / Net revenue":"1,56",' +
            '"KPI Sales Margin / Net revenue":"1,71","NET RESULT":"56.993,07","NET RESULT first":"58.996,33","Capex":"1.228,43","Network Depreciation":"774,83",' +
            '"Direct Capex":"0,00","EBITDA":"58.996,33","EBITDA first":"62.203,26","Overhead allocation":"3.206,93","Network Opex":"2.819,96",' +
            '"Incremental EBITDA":"65.023,22","Incremental OPEX":"0,00","Total Gross Margin":"65.023,22","Total costs of sales":"10.816,78","A&R":"0,00",' +
            '"Revenue share":"0,00","Other Costs":"0,00","Opportunity cost":"0,00","Opportunity cost Fixed":"0,00","Opportunity cost Mobile":"0,00",' +
            '"Total cost of sales":"10.816,78","Total Net Revenue":"75.840,00","Total discounts":"0,00","Benchmark":"0,00",' +
            '"Total credit amount / loyalty bonus":"0,00","Benchmark Fixed":"0,00","Benchmark Mobile":"0,00",' +
            '"Operator other costs / other migration costs":"0,00","Operator other costs / other migration costs Fixed":"0,00",' +
            '"Operator other costs / other migration costs Mobile":"0,00","Discounts":"0,00","Total Gross Revenue (SUM)":"75.840,00",' +
            '"Total Gross Revenue for Operator other costs / other migration costs":"0,00","Total Gross Revenue":"75.840,00"}';
            basket.Contract_end_Mobile__c = Date.newInstance(2022, 10, 10);

            insert basket;*/

			// PriceReset__c priceResetSetting = new PriceReset__c();

			// priceResetSetting.MaxRecurringPrice__c = 200.00;
			// priceResetSetting.ConfigurationName__c = 'IP Pin';

			// insert priceResetSetting;

			Account testAccount = CS_DataTest.createAccount('Test Account 1');
			Account testAccount2 = CS_DataTest.createAccount('Test Account 2');
			List<Account> accList = new List<Account>{ testAccount, testAccount2 };
			insert accList;

			Contact contact1 = new Contact(
				AccountId = testAccount.id,
				LastName = 'Last',
				FirstName = 'First',
				Contact_Role__c = 'Consultant',
				Email = 'test@vf.com'
			);
			insert contact1;

			testAccount.Authorized_to_sign_1st__c = contact1.Id;
			testAccount.Contract_rule_no_mailing__c = true;
			testAccount.Frame_Work_Agreement__c = 'VFZA-2018-8';
			testAccount.Version_FWA__c = 4;
			testAccount.Framework_agreement_date__c = Date.today();
			update testAccount;

			Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Contract opportunity', simpleUser.id);
			Opportunity opp2 = new Opportunity();
			opp2.Name = 'testName';
			opp2.Account = testAccount2;
			opp2.StageName = 'Qualification';
			opp2.CloseDate = Date.Today() + 10;

			List<Opportunity> oppList = new List<Opportunity>{ opp, opp2 };
			insert oppList;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'New basket');
			basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
			basket.cscfga__Basket_Status__c = 'Valid';
			basket.Primary__c = true;
			basket.csbb__Synchronised_with_Opportunity__c = true;
			basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
			basket.csbb__Account__c = testAccount.Id;
			basket.Contract_duration_Mobile__c = '24';
			basket.Contract_duration_Fixed__c = 24;
			basket.OneNet_Scenario__c = 'One Net Enterprise';
			basket.Fixed_Scenario__c = 'One Fixed Enterprise';
			basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 2, 2);
			basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 2, 2);
			basket.Number_of_SIP__c = 12;
			basket.Approved_Date__c = Date.today() - 4;
			basket.PortingPercentage__c = 10;
			basket.New_Portfolio__c = true;
			basket.ProfitLoss_JSON__c =
				'{"KPI Direct Capex / Net Revenue":"0,00","KPI Payback Period(Months)":"0,00","KPI NPV":"0,00",' +
				'"KPI Net Incremental Billed Revenue":"0,00","KPI Free cash flow / Net revenue":"1,50","KPI EBITDA / Net revenue":"1,56",' +
				'"KPI Sales Margin / Net revenue":"1,71","NET RESULT":"56.993,07","NET RESULT first":"58.996,33","Capex":"1.228,43","Network Depreciation":"774,83",' +
				'"Direct Capex":"0,00","EBITDA":"58.996,33","EBITDA first":"62.203,26","Overhead allocation":"3.206,93","Network Opex":"2.819,96",' +
				'"Incremental EBITDA":"65.023,22","Incremental OPEX":"0,00","Total Gross Margin":"65.023,22","Total costs of sales":"10.816,78","A&R":"0,00",' +
				'"Revenue share":"0,00","Other Costs":"0,00","Opportunity cost":"0,00","Opportunity cost Fixed":"0,00","Opportunity cost Mobile":"0,00",' +
				'"Total cost of sales":"10.816,78","Total Net Revenue":"75.840,00","Total discounts":"0,00","Benchmark":"0,00",' +
				'"Total credit amount / loyalty bonus":"0,00","Benchmark Fixed":"0,00","Benchmark Mobile":"0,00",' +
				'"Operator other costs / other migration costs":"0,00","Operator other costs / other migration costs Fixed":"0,00",' +
				'"Operator other costs / other migration costs Mobile":"0,00","Discounts":"0,00","Total Gross Revenue (SUM)":"75.840,00",' +
				'"Total Gross Revenue for Operator other costs / other migration costs":"0,00","Total Gross Revenue":"75.840,00"}';
			basket.Contract_end_Mobile__c = Date.newInstance(2022, 10, 10);

			List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>{ basket };
			insert basketList;

			cscfga__Product_Definition__c accessPd = CS_DataTest.createProductDefinition('Access');
			insert accessPd;

			cscfga__Product_Configuration__c accessConf = CS_DataTest.createProductConfiguration(
				accessPd.Id,
				CS_ConstantsCOM.PRODUCT_DEFINITION_ACCESS,
				basket.Id
			);
			accessConf.New_Delivery_Model__c = true;
			insert accessConf;
		}
	}

	@isTest
	static void testUnSyncProductBasketsAfterInsertUpdate() {
		List<cscfga__Product_Basket__c> baskets = [
			SELECT
				Id,
				New_Portfolio__c,
				Error_message__c,
				Total_Number_of_Expected_Locations__c,
				cscfga__opportunity__c,
				Block_Expiration__c,
				Contract_duration_Mobile__c,
				csbb__Synchronised_with_Opportunity__c,
				cscfga__Basket_Status__c,
				ProfitLoss_JSON__c,
				csordtelcoa__Synchronised_with_Opportunity__c
			FROM cscfga__Product_Basket__c
		];
		Test.startTest();
		SyncWithOpportunityUtility.UnSyncProductBasketsAfterInsertUpdate(baskets, null);
		Test.stopTest();
	}
	@isTest
	static void testInsertOLIsProductDetailsAfterUpdate() {
		List<cscfga__Product_Basket__c> baskets = [
			SELECT
				Id,
				New_Portfolio__c,
				Error_message__c,
				Total_Number_of_Expected_Locations__c,
				cscfga__opportunity__c,
				Block_Expiration__c,
				Contract_duration_Mobile__c,
				csbb__Synchronised_with_Opportunity__c,
				cscfga__Basket_Status__c,
				ProfitLoss_JSON__c,
				csordtelcoa__Synchronised_with_Opportunity__c
			FROM cscfga__Product_Basket__c
		];
		Test.startTest();

		List<Opportunity> opps = [SELECT Id FROM Opportunity];
		List<Account> accs = [SELECT Id FROM Account];

		cscfga__Product_Basket__c oldBasket = CS_DataTest.createProductBasket(opps[0], 'Old basket');
		oldBasket.csordtelcoa__Basket_Stage__c = 'Prospecting';
		oldBasket.cscfga__Basket_Status__c = 'Valid';
		oldBasket.Primary__c = true;
		oldBasket.csbb__Synchronised_with_Opportunity__c = false;
		oldBasket.csordtelcoa__Synchronised_with_Opportunity__c = false;
		oldBasket.csbb__Account__c = accs[0].Id;
		oldBasket.Contract_duration_Mobile__c = '24';
		oldBasket.Contract_duration_Fixed__c = 24;
		oldBasket.OneNet_Scenario__c = 'One Net Enterprise';
		oldBasket.Fixed_Scenario__c = 'One Fixed Enterprise';
		oldBasket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2020, 2, 2);
		oldBasket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2020, 2, 2);
		oldBasket.Number_of_SIP__c = 12;
		oldBasket.Approved_Date__c = Date.today() - 4;
		oldBasket.PortingPercentage__c = 10;
		oldBasket.ProfitLoss_JSON__c =
			'{"KPI Direct Capex / Net Revenue":"0,00","KPI Payback Period(Months)":"0,00","KPI NPV":"0,00",' +
			'"KPI Net Incremental Billed Revenue":"0,00","KPI Free cash flow / Net revenue":"1,50","KPI EBITDA / Net revenue":"1,56",' +
			'"KPI Sales Margin / Net revenue":"1,71","NET RESULT":"56.993,07","NET RESULT first":"58.996,33","Capex":"1.228,43","Network Depreciation":"774,83",' +
			'"Direct Capex":"0,00","EBITDA":"58.996,33","EBITDA first":"62.203,26","Overhead allocation":"3.206,93","Network Opex":"2.819,96",' +
			'"Incremental EBITDA":"65.023,22","Incremental OPEX":"0,00","Total Gross Margin":"65.023,22","Total costs of sales":"10.816,78","A&R":"0,00",' +
			'"Revenue share":"0,00","Other Costs":"0,00","Opportunity cost":"0,00","Opportunity cost Fixed":"0,00","Opportunity cost Mobile":"0,00",' +
			'"Total cost of sales":"10.816,78","Total Net Revenue":"75.840,00","Total discounts":"0,00","Benchmark":"0,00",' +
			'"Total credit amount / loyalty bonus":"0,00","Benchmark Fixed":"0,00","Benchmark Mobile":"0,00",' +
			'"Operator other costs / other migration costs":"0,00","Operator other costs / other migration costs Fixed":"0,00",' +
			'"Operator other costs / other migration costs Mobile":"0,00","Discounts":"0,00","Total Gross Revenue (SUM)":"75.840,00",' +
			'"Total Gross Revenue for Operator other costs / other migration costs":"0,00","Total Gross Revenue":"75.840,00"}';
		oldBasket.Contract_end_Mobile__c = Date.newInstance(2022, 10, 10);
		insert oldBasket;

		Map<Id, Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>>> pcOeAttrMap = new Map<Id, Map<String, List<cssmgnt.ProductProcessingUtility.Attribute>>>();
		SyncWithOpportunityUtility.InsertOLIsProductDetailsAfterUpdate(baskets, new List<cscfga__Product_Basket__c>{ oldBasket }, pcOeAttrMap);
		Test.stopTest();
	}

	@isTest
	static void testDeleteOLIsProductDetailsAfterUpdate() {
		List<cscfga__Product_Basket__c> baskets = [
			SELECT
				Id,
				New_Portfolio__c,
				Error_message__c,
				Total_Number_of_Expected_Locations__c,
				cscfga__opportunity__c,
				Block_Expiration__c,
				Contract_duration_Mobile__c,
				csbb__Synchronised_with_Opportunity__c,
				cscfga__Basket_Status__c,
				ProfitLoss_JSON__c,
				csordtelcoa__Synchronised_with_Opportunity__c
			FROM cscfga__Product_Basket__c
		];
		Test.startTest();
		SyncWithOpportunityUtility.DeleteOLIsProductDetailsAfterUpdate(baskets, baskets);
		Test.stopTest();
	}

	@isTest
	static void testSynchronizeProfitLossOppFieldsAndBlockExpiration() {
		List<cscfga__Product_Basket__c> baskets = [
			SELECT
				Id,
				New_Portfolio__c,
				Error_message__c,
				Total_Number_of_Expected_Locations__c,
				cscfga__opportunity__c,
				Block_Expiration__c,
				Contract_duration_Mobile__c,
				csbb__Synchronised_with_Opportunity__c,
				cscfga__Basket_Status__c,
				ProfitLoss_JSON__c
			FROM cscfga__Product_Basket__c
		];
		List<cscfga__Product_configuration__c> configs = [SELECT Id, Name, New_Delivery_Model__c FROM cscfga__Product_configuration__c];
		Test.startTest();
		SyncWithOpportunityUtility.syncOpportunityFields(baskets[0], configs);
		Test.stopTest();
		Opportunity resultOpportunity = [SELECT Id, No_Contract__c FROM Opportunity WHERE Id = :baskets[0].cscfga__opportunity__c];
		System.assertEquals(true, resultOpportunity.No_Contract__c, 'No contract needed');
	}
}
