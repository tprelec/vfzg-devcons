@isTest
public with sharing class TestProductGroupAssTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Product Group Association';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'PGA-21-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		Product_Group_Association__c testPga = new Product_Group_Association__c();
		insert testPga;

		Product_Group_Association__c queryRecord = [SELECT Id, External_ID__c FROM Product_Group_Association__c LIMIT 1];
		System.assert(queryRecord.External_ID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
