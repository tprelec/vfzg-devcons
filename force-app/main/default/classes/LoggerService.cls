public with sharing class LoggerService {
	private static final String CLASSNAME = LoggerService.class.getName();

	private static final Pattern STACK_LINE = Pattern.compile(
		'^(?:Class\\.)?([^.]+)\\.?([^\\.\\:]+)?[\\.\\:]?([^\\.\\:]*): line (\\d+), column (\\d+)$'
	);

	// Map of Logging Level Names and Enum values
	private static Map<String, LoggingLevel> loggingLevelsByName;

	// All settings for different users
	private static Logger_Setting__mdt[] loggerSettings = [SELECT Logger_Level__c, Logger_User_Id__c FROM Logger_Setting__mdt];

	private static Map<String, LoggingLevel> getLoggingLevelsByName() {
		if (loggingLevelsByName == null) {
			loggingLevelsByName = new Map<String, LoggingLevel>();
			for (LoggingLevel logLevel : LoggingLevel.values()) {
				loggingLevelsByName.put(logLevel.name(), logLevel);
			}
		}
		return loggingLevelsByName;
	}

	/**
	 * Logs message with DEBUG logging level
	 * @param  msg	Message
	 */
	public static void log(String msg) {
		log(LoggingLevel.DEBUG, msg);
	}

	/**
	 * Logs message with provided logging level
	 * @param  logLevel	Logging level
	 * @param  msg	Message
	 */
	public static void log(LoggingLevel logLevel, String msg) {
		if (checkLogSettings(logLevel)) {
			LogEvent__e le = createLogEvent(logLevel, msg);
			publishLogEvents(new List<LogEvent__e>{ le });
		} else {
			System.debug(LoggingLevel.DEBUG, msg);
		}
	}

	/**
	 * Logs exception with ERROR logging level
	 * @param  ex	Exception
	 */
	public static void log(Exception ex) {
		LogEvent__e le = createLogEvent(LoggingLevel.ERROR, ex.getMessage());
		le.StackTrace__c = ex.getStackTraceString();
		publishLogEvents(new List<LogEvent__e>{ le });
	}

	/**
	 * Logs exception with ERROR logging level and recored ids
	 * @param  ex	Exception¸
	 * @param  recordIds Set of record Ids
	 */
	public static void log(Exception ex, Set<Id> recordIds) {
		for (Id recordId : recordIds) {
			LogEvent__e le = createLogEvent(LoggingLevel.ERROR, ex.getMessage());
			le.StackTrace__c = ex.getStackTraceString();
			le.RecordId__c = recordId;
			publishLogEvents(new List<LogEvent__e>{ le });
		}
	}

	/**
	 * Logs the unhandled batch apex exceptions
	 * @param  batchErrorList	List<BatchApexErrorEvent> - List of unhandled batch apex exceptions encapsulated in BatchApexErrorEvents
	 */
	public static void log(List<ExceptionHandlingService.BatchErrorInformation> batchErrorList) {
		List<LogEvent__e> logEventList = new List<LogEvent__e>();
		for (ExceptionHandlingService.BatchErrorInformation batchErrorInfo : batchErrorList) {
			BatchApexErrorEvent apexErrorEvent = batchErrorInfo.apexErrorEvent;
			AsyncApexJob apexJobInformation = batchErrorInfo.apexJobInformation;

			// Id, CreatedBy.Name, CreatedDate, ExtendedStatus, JobItemsProcessed, JobType, MethodName, NumberOfErrors, Status, TotalJobItems, ApexClass.Name

			LogEvent__e logEvent = new LogEvent__e();
			logEvent.ApexClass__c = apexJobInformation.ApexClass.Name;
			logEvent.DateTime__c = DateTime.now();
			logEvent.Level__c = LoggingLevel.ERROR.name();
			//logEvent.LineNumber__c = '';
			logEvent.Message__c = String.valueOf(JSON.serializePretty(batchErrorInfo, true));
			logEvent.Method__c = apexJobInformation.MethodName;
			logEvent.StackTrace__c = apexErrorEvent.StackTrace;
			logEvent.User__c = apexJobInformation.CreatedById;

			logEventList.add(logEvent);
		}
		List<Database.SaveResult> srList = EventBus.publish(logEventList);
		for (Database.SaveResult sr : srList) {
			if (!sr.isSuccess()) {
				System.debug(LoggingLevel.DEBUG, 'Unsuccessfully published event');
			}
		}
	}

	/**
	 * Creates Log Platform Event
	 * @param  logLevel	Logging level
	 * @param  msg	Message
	 */
	private static LogEvent__e createLogEvent(LoggingLevel logLevel, String msg) {
		LogEvent__e le = new LogEvent__e(DateTime__c = System.now(), Level__c = logLevel.name(), Message__c = msg, User__c = UserInfo.getUserId());
		setExactLocation(le);
		return le;
	}

	/**
	 * Sets exact location (Class, Method, Line) for Log
	 * @param  logLevel	Logging level
	 * @param  msg	Message
	 */
	private static void setExactLocation(LogEvent__e le) {
		List<String> stacktrace = new DmlException().getStackTraceString().split('\n');
		for (String line : stacktrace) {
			Matcher matcher = STACK_LINE.matcher(line);
			if (matcher.find() && !line.startsWith('Class.' + CLASSNAME + '.')) {
				Boolean hasNamespace = String.isNotBlank(matcher.group(3));
				if (hasNamespace) {
					le.ApexClass__c = matcher.group(2);
					le.Method__c = prettyMethod(matcher.group(3));
				} else {
					le.ApexClass__c = matcher.group(1);
					le.Method__c = prettyMethod(matcher.group(2));
				}
				le.LineNumber__c = matcher.group(4);
				return;
			}
		}
	}

	/**
	 * Checks if org/users logging settings are matching Logging Level.
	 * @param  logLevel Logging Level.
	 */
	private static Boolean checkLogSettings(LoggingLevel logLevel) {
		Boolean logFlag = false;

		for (Logger_Setting__mdt loggerSetting : loggerSettings) {
			if (loggerSetting.Logger_User_Id__c == UserInfo.getUserId() || String.isBlank(loggerSetting.Logger_User_Id__c)) {
				System.debug(LoggingLevel.DEBUG, loggerSetting.Logger_Level__c);
				System.debug(LoggingLevel.DEBUG, getLoggingLevelsByName().get(loggerSetting.Logger_Level__c));
				LoggingLevel userLogLevel = getLoggingLevelsByName().get(loggerSetting.Logger_Level__c);
				if (logLevel.ordinal() >= userLogLevel.ordinal()) {
					return true;
				}
			}
		}
		return logFlag;
	}

	/**
	 * Inserts a record into the Log object stating the Error or Information
	 * @param	leList	List of Log Events
	 */
	public static void persistLog(List<LogEvent__e> leList) {
		List<Log__c> logs = new List<Log__c>();
		for (LogEvent__e le : leList) {
			logs.add(
				new Log__c(
					Date_Time__c = le.DateTime__c,
					Level__c = le.Level__c,
					Apex_Class__c = le.ApexClass__c,
					Method__c = le.Method__c,
					Trigger__c = le.Trigger__c,
					Record_ID__c = le.RecordId__c,
					Line_Number__c = le.LineNumber__c != null ? Integer.valueOf(le.LineNumber__c) : null,
					Message__c = le.Message__c,
					Stack_Trace__c = le.StackTrace__c,
					User__c = String.isNotBlank(le.User__c) ? le.User__c : null
				)
			);
		}
		Database.insert(logs);
	}

	/**
	 * Publish log events using platform events
	 * @param	logEventList	List of Log Events
	 */
	public static void publishLogEvents(List<LogEvent__e> logEventList) {
		for (LogEvent__e logEvent : logEventList) {
			Database.SaveResult saveResult = EventBus.publish(logEvent);
			if (!saveResult.isSuccess()) {
				System.debug(LoggingLevel.DEBUG, 'Unsuccessfully published event');
			}
		}
	}

	private static String prettyMethod(String method) {
		String result = (method == null) ? 'anonymous' : method;
		return (result.contains('init')) ? 'ctor' : result;
	}
}
