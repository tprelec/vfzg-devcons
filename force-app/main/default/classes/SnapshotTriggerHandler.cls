/**
 * @description         This is the trigger handler for the CS_Basket_Snapshot_Transactional__c sObject. 
 * @author              Guy Clairbois
 */
public with sharing class SnapshotTriggerHandler extends TriggerHandler {

    private List<CS_Basket_Snapshot_Transactional__c> newSnapshots;
    private Map<Id, CS_Basket_Snapshot_Transactional__c> oldSnapshotsMap;

    private void init() {
        newSnapshots = (List<CS_Basket_Snapshot_Transactional__c>) this.newList;
        oldSnapshotsMap = (Map<Id, CS_Basket_Snapshot_Transactional__c>) this.oldMap;
    }

    public override void beforeInsert() {
        init();
        calculateTotalProductsInBasket();
    }

    public override void afterInsert() {
        init();
        generateSnapshotLines();
    }

    public override void beforeUpdate() {
        init();
        calculateTotalProductsInBasket();
    }

    public void calculateTotalProductsInBasket() {
        Set<String> basketIds = GeneralUtils.getStringSetFromList(newSnapshots, 'Product_Basket__c');
        List<CS_Basket_Snapshot_Transactional__c> snaps = [
            SELECT Id, Quantity__c, RecurringProduct__c, Product_Basket__c, ConnectionType__c
            FROM CS_Basket_Snapshot_Transactional__c 
            WHERE Product_Basket__c IN :basketIds
        ];

        List<CS_Basket_Snapshot_Transactional__c> snapToUpdate = new List<CS_Basket_Snapshot_Transactional__c>();
        snaps.addAll(newSnapshots);
        Map<Id, List<CS_Basket_Snapshot_Transactional__c>> basketSnaps = GeneralUtils.groupByIDField(snaps, 'Product_Basket__c');
        for (CS_Basket_Snapshot_Transactional__c newSnap : newSnapshots) {
            if (oldSnapshotsMap != null && !GeneralUtils.isRecordFieldChanged(newSnap, oldSnapshotsMap, 'Quantity__c')) { 
                continue;
            }

            List<CS_Basket_Snapshot_Transactional__c> snapList = basketSnaps.containsKey(newSnap.Product_Basket__c) ? basketSnaps.get(newSnap.Product_Basket__c) : new List<CS_Basket_Snapshot_Transactional__c>();
            Map<Id, List<CS_Basket_Snapshot_Transactional__c>> productSnaps = GeneralUtils.groupByIDField(snapList, 'RecurringProduct__c');
            
            Decimal qty = isDisconnectType(newSnap) ? 0 : newSnap.Quantity__c;
            List<CS_Basket_Snapshot_Transactional__c> ps = productSnaps.containsKey(newSnap.RecurringProduct__c) ? productSnaps.get(newSnap.RecurringProduct__c) : new List<CS_Basket_Snapshot_Transactional__c>();
            for (CS_Basket_Snapshot_Transactional__c s : ps) {
                if ((s.Id == null && s == newSnap) || (s.Id != null && s.Id == newSnap.Id) || isDisconnectType(s)) {
                    continue;
                }
                qty += s.Quantity__c;
            }
            newSnap.Total_Product_Quantity__c = qty;

            for (CS_Basket_Snapshot_Transactional__c s : ps) {
                if (s.Id == null || s.Id == newSnap.Id) {
                    continue;
                }
                snapToUpdate.add(new CS_Basket_Snapshot_Transactional__c(Id = s.Id, Total_Product_Quantity__c = newSnap.Total_Product_Quantity__c));
            }
        }

        if (!snapToUpdate.isEmpty()) {
            update snapToUpdate;
        }
    }

    private Boolean isDisconnectType(CS_Basket_Snapshot_Transactional__c snap) {
        return snap.ConnectionType__c == Constants.SNAPSHOT_CONNECTION_TYPE_DISCONNECT;
    }
    
    public void generateSnapshotLines() {
        // collect basket id's and group the snapshots by basket id
        Map<Id, List<CS_Basket_Snapshot_Transactional__c>> basketIdToSnapshots = new Map<Id, List<CS_Basket_Snapshot_Transactional__c>>();
        for (CS_Basket_Snapshot_Transactional__c ss : newSnapShots) {
            if (!basketIdToSnapshots.containsKey(ss.Product_Basket__c)) {
                basketIdToSnapshots.put(ss.Product_Basket__c, new List<CS_Basket_Snapshot_Transactional__c>{ss});    
            } else {
                basketIdToSnapshots.get(ss.Product_Basket__c).add(ss);       
            }
        }
        
        // fetch basket info
        Map<Id, cscfga__Product_Basket__c> idToBasket = new Map<Id, cscfga__Product_Basket__c>([
            SELECT Id, Commercial_Terms__c, Contract_Duration_Mobile_Numeric__c, Number_of_ctn__c 
            FROM cscfga__Product_Basket__c 
            WHERE Id IN :basketIdToSnapshots.keySet()
        ]);        
        
        // now do the processing per basket.
        // WARNING: contains soql in loops. However, snapshots are normally never created for multiple baskets at a time.
        List<Snapshot_Line__c> slToInsert = new List<Snapshot_Line__c>();

        for (Id basketId : basketIdToSnapshots.keySet()) {
                    
            Set<String> thresholdGroups = new Set<String>();
            List<CS_Basket_Snapshot_Transactional__c> snapshots = basketIDToSnapShots.get(basketId);
            
            for (CS_Basket_Snapshot_Transactional__c ss : snapshots) {
                // collect snapshot-level fields that are used in exact matches
                if(ss.TresholdGroup_OneOff__c!= null)
                  thresholdGroups.add(ss.TresholdGroup_OneOff__c);
                if(ss.TresholdGroup_Recurring__c != null)
                  thresholdGroups.add(ss.TresholdGroup_Recurring__c);
                if (ss.SpecialMandate_OC__c != null && ss.SpecialMandate_OC__c != 'No Special Mandate') {
                    thresholdGroups.add(ss.SpecialMandate_OC__c);
                }
                if (ss.SpecialMandate_RC__c != null && ss.SpecialMandate_RC__c != 'No Special Mandate') {
                    thresholdGroups.add(ss.SpecialMandate_RC__c);
                }
            }
            
            cscfga__Product_Basket__c basket = idToBasket.get(basketId);
        
            // query matrix lines
            String matrixLineQuery = 'SELECT Id, Approval_Step__c, Proposition__c, Duration_From__c, Duration_to__c, Quantity_From__c, Quantity_To__c, Treshold_Group__c, Treshold_Percentage__c, Treshold_Amount__c';
            matrixLineQuery += ' FROM appro__Matrix_Line__c WHERE ';
            matrixLineQuery += ' appro__Active__c = true ';
            matrixLineQuery += ' AND Approval_Step__c != null ';
            matrixLineQuery += ' AND (Commercial_Terms__c = \''+basket.Commercial_Terms__c + '\' OR Commercial_Terms__c = \'*\') ';
            // changes to new index field Active_Today__c and not the date fields
            //matrixLineQuery += ' AND Valid_From__c < TODAY AND Valid_To__c >= TODAY ';
            matrixLineQuery += ' AND appro__Active_Today__c = \'Yes\' ';
            
            // add a filter to snapshot-level exact matches
            matrixLineQuery += ' AND Treshold_Group__c IN :thresholdGroups ';
            
            // loop through all snapshots per basket and, for all matrix level lines found, check if there's a match. If there is one, create a Snapshot_Line record
            system.debug(matrixLineQuery);
            for (appro__Matrix_Line__c ml : Database.Query(matrixLineQuery)) {
                Set<String> mlPropositions = new Set<String>();
                if (ml.Proposition__c != null) {
                    mlPropositions.addAll(ml.Proposition__c.split(';'));
                }            
                for (CS_Basket_Snapshot_Transactional__c ss : snapshots) {
                    Set<String> ssPropositions = new Set<String>();
                    if (ss.CS_Proposition_scenario_matrix__c != null) {
                        ssPropositions.addAll(ss.CS_Proposition_scenario_matrix__c.split(';'));
                    }
                    if (ml.Treshold_Group__c == ss.TresholdGroup_OneOff__c || ml.Treshold_Group__c == ss.SpecialMandate_OC__c) {
                        // one off matching
                        // start with checking duration
                        if (ss.FixedDuration__c >= ml.Duration_From__c && ss.FixedDuration__c < ml.Duration_To__c) {                        
                            // fixed matching
                            // check quantity
                            if ((ml.Quantity_From__c == null || ss.Quantity_for_Thresholds__c >= ml.Quantity_From__c) && (ml.Quantity_To__c == null || ss.Quantity_for_Thresholds__c < ml.Quantity_To__c)) {
                                // check Proposition
                                if (ml.Proposition__c == '*' || GeneralUtils.setsIntersect(mlPropositions, ssPropositions)) {
                                    // found a match
                                    slToInsert.add(generateSnapshotLine(ml,ss));
                                }
                            } 
                        } else if (ss.MobileDuration__c >= ml.Duration_From__c && ss.MobileDuration__c < ml.Duration_To__c ) {
                            // mobile matching
                            // check quantity
                            if ((ml.Quantity_From__c == null || ss.Quantity_for_Thresholds__c >= ml.Quantity_From__c) && (ml.Quantity_To__c == null || ss.Quantity_for_Thresholds__c < ml.Quantity_To__c)) {
                                // (proposition is not relevant for mobile)
                                // found a match
                                slToInsert.add(generateSnapshotLine(ml,ss));
                            } 
                        }
                    }
                    // snapshot can be both one off and recurring at the same time
                    if (ml.Treshold_Group__c == ss.TresholdGroup_Recurring__c || ml.Treshold_Group__c == ss.SpecialMandate_RC__c) {
                        // recurring matching
                        // start with checking duration
                        if (ss.FixedDuration__c >= ml.Duration_From__c && ss.FixedDuration__c < ml.Duration_To__c) {
                            // fixed matching
                            // check quantity
                            if ((ml.Quantity_From__c == null || ss.Quantity_for_Thresholds__c >= ml.Quantity_From__c) && (ml.Quantity_To__c == null || ss.Quantity_for_Thresholds__c < ml.Quantity_To__c)) {
                                // check Proposition
                                if (ml.Proposition__c == '*' || GeneralUtils.setsIntersect(mlPropositions, ssPropositions)) {
                                    // found a match
                                    slToInsert.add(generateSnapshotLine(ml,ss));
                                }
                            }
                        } else if (ss.MobileDuration__c >= ml.Duration_From__c && ss.MobileDuration__c < ml.Duration_To__c) {
                            // mobile matching
                            // check quantity
                            if ((ml.Quantity_From__c == null || ss.Quantity_for_Thresholds__c >= ml.Quantity_From__c) && (ml.Quantity_To__c == null || ss.Quantity_for_Thresholds__c < ml.Quantity_To__c)) {
                                // (proposition is not relevant for mobile)
                                // found a match
                                slToInsert.add(generateSnapshotLine(ml,ss));
                            } 
                        }                        
                    }
                }
            }
        }
        insert slToInsert;
    }
    
    private Snapshot_Line__c generateSnapshotLine(appro__Matrix_Line__c ml, CS_Basket_Snapshot_Transactional__c ss) {
        Snapshot_Line__c sl = new Snapshot_Line__c(
            Matrix_Line__c = ml.Id,
            Snapshot__c = ss.Id,
            Level__c = ml.Approval_Step__c,
            Allowance__c = ml.Treshold_Amount__c,
            Percentage__c = ml.Treshold_Percentage__c
        );
        return sl;
    }
    
}