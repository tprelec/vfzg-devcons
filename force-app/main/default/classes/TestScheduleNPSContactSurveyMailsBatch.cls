@isTest
private class TestScheduleNPSContactSurveyMailsBatch {

    @isTest
    static void runTest() {

        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.NPS_Survey_Date__c = Date.Today().addDays(14);
        update acct;

        Test.startTest();
        ScheduleNPSContactSurveyMailsBatch npsBatch = new ScheduleNPSContactSurveyMailsBatch();
        Database.executeBatch(npsBatch);
        Test.stopTest();
    }

    @isTest
    static void runTestSchedule() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        acct.NPS_Survey_Date__c = Date.Today().addDays(14);
        update acct;

        Test.StartTest();
        ScheduleNPSContactSurveyMailsBatch ssha = new ScheduleNPSContactSurveyMailsBatch();
        String sch = '0 0 23 * * ?';
        system.schedule('Test Batch ScheduleNPSContactSurveyMailsBatch', sch, ssha);
        Test.stopTest();
    }


}