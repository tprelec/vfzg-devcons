@IsTest
public with sharing class TestDocumentInvocableService {
	@TestSetup
	static void makeData() {
		cscfga__Product_Basket__c testBasket = TestUtils.createCSProductBasket();
		Opportunity testOpp = [SELECT Id, AccountId, Amount, Primary_Basket__c, OwnerId FROM Opportunity LIMIT 1];
		testOpp.Amount = 500001;
		testOpp.Primary_Basket__c = testBasket.Id;
		update testOpp;
		csclm__Agreement__c agreement = CS_DataTest.createAgreement('Test Agreement');
		agreement.csclm__Opportunity__c = testOpp.Id;
		agreement.recordTypeId = Schema.SObjectType.csclm__Agreement__c.getRecordTypeInfosByDeveloperName().get('Bespoke').getRecordTypeId();
		insert agreement;
		Attachment attachment = new Attachment(
			Name = 'I\'m testing attachments',
			Body = Blob.valueOf('Some body once told me the world is gonna roll me'),
			ParentId = agreement.Id
		);
		insert attachment;
	}

	@isTest
	private static void getFilesFromAgreements() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		DocumentInvocableService.requestParamater reqParameter = new DocumentInvocableService.requestParamater();
		reqParameter.opportunityId = opp.Id;
		Test.startTest();
		List<List<String>> listResponse = DocumentInvocableService.getFilesFromAgreements(
			new List<DocumentInvocableService.requestParamater>{ reqParameter }
		);
		Test.stopTest();
		System.assert(listResponse != null, 'The response is null.');
		List<String> setFiles = new List<String>();
		List<String> setContentDocumentIds = new List<String>();
		List<ContentDocumentLink> documentLink = [SELECT Id, ContentDocumentId FROM ContentDocumentLink WHERE LinkedEntityId = :opp.Id];
		for (ContentDocumentLink doc : documentLink) {
			setContentDocumentIds.add(doc.ContentDocumentId);
		}

		List<ContentVersion> contentVersions = [SELECT Id FROM ContentVersion WHERE ContentDocumentId IN :setContentDocumentIds];

		for (ContentVersion content : contentVersions) {
			setFiles.add(content.Id);
		}
		for (List<String> lstString : listResponse) {
			for (String fileId : lstString) {
				System.assert(setFiles.contains(fileId), 'The expected Files have not been found.');
			}
		}
	}
}
