public with sharing class COM_IPPDFGeneratorController {
	private final COM_Delivery_Order__c deliveryOrder;
	private final Account acc;
	public COM_IPPDFGeneratorController(ApexPages.StandardController stdController) {
		deliveryOrder = [SELECT Id, Account__c FROM COM_Delivery_Order__c WHERE Id = :((COM_Delivery_Order__c) stdController.getRecord()).Id];

		acc = [SELECT Id, Name FROM Account WHERE Id = :deliveryOrder.Account__c];
	}

	public Account getAccount() {
		return acc;
	}
}
