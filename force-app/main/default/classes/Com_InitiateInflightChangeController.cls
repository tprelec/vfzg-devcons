/*
 * @author Rahul Sharma
 * @date 01-02-2022
 *
 * @author Neno Dodig
 * @date 21-10-2022
 *
 * @description Controller for Com_InitiateInflightChange LWC component
 * Jira ticket reference: COM-2590/COM-3619
 */
public with sharing class Com_InitiateInflightChangeController {
	@TestVisible
	private static string jsonResponse;
	@TestVisible
	private static final string TARGET_BASKET_ID_ATTRIBUTE_NAME = 'targetBasketId';

	/**
	 * @description get initial data on of LWC
	 * @param  recordId Order or delivery order salesforce Id
	 * @return          wrapper class holding available and unavailable deliveries
	 */
	@AuraEnabled(cacheable=true)
	public static LightningResponse getData(Id recordId) {
		// Skip the initial data for delivery order and return empty data as its not needed
		if (recordId?.getSobjectType() == Schema.COM_Delivery_Order__c.SObjectType) {
			return new LightningResponse();
		}

		try {
			LoggerService.log(LoggingLevel.DEBUG, '@@@@recordId: ' + recordId);
			InflightChangeData data = new InflightChangeData();

			data.columns = new List<DataTableColumn>{
				new DataTableColumn(COM_Delivery_Order__c.Name.getDescribe().getLabel(), 'name'),
				new DataTableColumn(COM_Delivery_Order__c.Products__c.getDescribe().getLabel(), 'productName')
			};

			for (COM_Delivery_Order__c objDeliveryOrder : [
				SELECT Id, Name, Status__c, PONR_Reached__c, Products__c
				FROM COM_Delivery_Order__c
				WHERE Order__c = :recordId AND Parent_Delivery_Order__c != NULL
			]) {
				if (objDeliveryOrder.PONR_Reached__c) {
					data.unavailableDeliveries.add(new DeliveryOrder(objDeliveryOrder));
				} else {
					data.availableDeliveries.add(new DeliveryOrder(objDeliveryOrder));
				}
			}

			return new LightningResponse().setBody(data);
		} catch (Exception objEx) {
			return new LightningResponse().setError(objEx.getMessage());
		}
	}

	/**
	 * @description invoked from LWC to perform initiate inflight change for delivery order
	 * @param  deliveryOrderId     delivery order record Id
	 * @return             wrapper class holding either exception in case something went wrong or a basketId in case of successful operation
	 */
	@AuraEnabled
	public static LightningResponse performInflightChangeForDeliveryOrder(Id deliveryOrderId) {
		try {
			LoggerService.log(LoggingLevel.DEBUG, '@@@@deliveryOrderId: ' + deliveryOrderId);

			COM_Delivery_Order__c deliveryOrder = [SELECT Id, Order__c FROM COM_Delivery_Order__c WHERE Id = :deliveryOrderId];
			Id orderId = deliveryOrder.Order__c;
			LoggerService.log(LoggingLevel.DEBUG, '@@@@orderId: ' + orderId);
			String basketId = performInflightChange(orderId);

			update new COM_Delivery_Order__c(Id = deliveryOrderId, On_Hold__c = true, Inflight_by_Delivery_Coordinator__c = true);

			List<Id> selectedDeliveryOrderIds = new List<Id>{ deliveryOrderId };
			updateRelatedDeliveryOrderMainProcess(selectedDeliveryOrderIds);
			updateServicesWithApplicableForInflightChange(orderId, selectedDeliveryOrderIds);

			update new csord__Order__c(
				Id = orderId,
				Inflight_Change_Applied__c = false,
				// as participating in inflight change is not set from CS call cssmgnt.API_1.createInflightRequest(), we mark it as true
				csordtelcoa__Participating_in_Inflight_Change__c = true,
				Inflight_by_Delivery_Coordinator__c = true
			);

			return new LightningResponse().setBody(basketId).setSuccess(System.Label.Com_InitiateInflightChange_SuccessMessage);
		} catch (DMLException objEx) {
			logDmlException(objEx);
			return new LightningResponse().setError(objEx.getDmlMessage(0));
		} catch (Exception objEx) {
			logGenericException(objEx);
			return new LightningResponse().setError(objEx.getMessage());
		}
	}

	/**
	 * @description invoked from LWC to perform initiate inflight change for cs order
	 * @param  orderId     Order record Id
	 * @param  selectedDeliveryOrderIds selected delivery record ids from UI
	 * @return             wrapper class holding either exception in case something went wrong or a basketId in case of successful operation
	 */
	@AuraEnabled
	public static LightningResponse performInflightChangeForOrder(Id orderId, List<Id> selectedDeliveryOrderIds) {
		try {
			LoggerService.log(LoggingLevel.DEBUG, '@@@@orderId: ' + orderId);
			LoggerService.log(LoggingLevel.DEBUG, '@@@@selectedDeliveryOrderIds: ' + selectedDeliveryOrderIds);
			String basketId = performInflightChange(orderId);

			if (!selectedDeliveryOrderIds.isEmpty()) {
				updateDeliveryOrdersToOnHold(selectedDeliveryOrderIds);
				updateRelatedDeliveryOrderMainProcess(selectedDeliveryOrderIds);
			}
			updateServicesWithApplicableForInflightChange(orderId, selectedDeliveryOrderIds);

			clearInFlightChangeFlagOnCommercialOrder(orderId);

			return new LightningResponse().setBody(basketId).setSuccess(System.Label.Com_InitiateInflightChange_SuccessMessage);
		} catch (DMLException objEx) {
			logDmlException(objEx);
			return new LightningResponse().setError(objEx.getDmlMessage(0));
		} catch (Exception objEx) {
			logGenericException(objEx);
			return new LightningResponse().setError(objEx.getMessage());
		}
	}

	@TestVisible
	private static void logDmlException(DMLException objEx) {
		LoggerService.log(LoggingLevel.DEBUG, '@@@@objEx: ' + objEx.getDmlMessage(0));
		LoggerService.log(LoggingLevel.ERROR, objEx.getDmlMessage(0));
		LoggerService.log(objEx);
	}

	@TestVisible
	private static void logGenericException(Exception objEx) {
		LoggerService.log(LoggingLevel.DEBUG, '@@@@objEx: ' + objEx.getMessage());
		LoggerService.log(LoggingLevel.ERROR, objEx.getMessage());
		LoggerService.log(objEx);
	}

	/**
	 * @description For the current order record, perform inflight change change by calling CS API
	 * @param  orderId Order record Id
	 * @return         basket record Id
	 */
	private static String performInflightChange(Id orderId) {
		List<csord__Solution__c> solutions = [SELECT Id FROM csord__Solution__c WHERE csord__Order__c = :orderId ORDER BY CreatedDate DESC LIMIT 1];
		if (solutions.isEmpty()) {
			throw new InflightChangeException(System.Label.Com_InitiateInflightChange_NoSolutionFoundMessage);
		}

		LoggerService.log(LoggingLevel.DEBUG, '@@@@solutions[0].Id: ' + solutions[0].Id);

		String basketId = callCSCreateInflightRequestAPI(solutions[0].Id);
		verifyIfBasketIdIsValid(basketId);
		return basketId;
	}

	private static void verifyIfBasketIdIsValid(String basketId) {
		// verify if the basketId is valid record Id
		if (!(basketId instanceof Id)) {
			throw new InflightChangeException(System.label.Com_InitiateInflightChange_CS_API_CallFailed);
		}
	}

	/**
	 * @description call the CS API with solution record Id to creating an inflight request
	 * @param  solutionId Solution record Id
	 * @return            basket record Id
	 */
	private static String callCSCreateInflightRequestAPI(Id solutionId) {
		// Skip call to CS api
		if (!Test.isRunningTest()) {
			jsonResponse = cssmgnt.API_1.createInflightRequest(solutionId);
		}

		LoggerService.log(LoggingLevel.DEBUG, '@@@@jsonResponse: ' + jsonResponse);
		Map<String, Object> mapResponse;
		try {
			mapResponse = (Map<String, Object>) JSON.deserializeUntyped(jsonResponse);
		} catch (Exception objException) {
			LoggerService.log(LoggingLevel.DEBUG, '@@@@callCSCreateInflightRequestAPI objException: ' + objException);
			throw new InflightChangeException(System.Label.Com_InitiateInflightChange_DeserializationErrorMessage);
		}
		performCallout(jsonResponse);
		return String.valueOf(mapResponse.get(TARGET_BASKET_ID_ATTRIBUTE_NAME));
	}

	/**
	 * @description Call CS API method required for an inflight change
	 * @param  jsonResponse JSON response from cssmgnt.API_1.createInflightRequest
	 */
	@future(callout=true)
	private static void performCallout(String jsonResponse) {
		Map<String, Object> responseObjMap = (Map<String, Object>) JSON.deserializeUntyped(jsonResponse);
		Map<String, String> inputMap = new Map<String, String>();
		for (String objKey : responseObjMap.keyset()) {
			inputMap.put(objKey, (String) responseObjMap.get(objKey));
		}
		// Skip call to CS api
		if (!Test.isRunningTest()) {
			cssmgnt.SolutionMACDController.createHerokuMACDSolution(inputMap);
		}
	}

	private static void updateDeliveryOrdersToOnHold(List<Id> selectedDeliveryOrderIds) {
		List<COM_Delivery_Order__c> deliveryOrders = new List<COM_Delivery_Order__c>();
		for (COM_Delivery_Order__c objDeliveryOrder : [
			SELECT Id
			FROM COM_Delivery_Order__c
			WHERE Id IN :selectedDeliveryOrderIds AND On_Hold__c = FALSE
		]) {
			deliveryOrders.add(new COM_Delivery_Order__c(Id = objDeliveryOrder.Id, On_Hold__c = true));
		}
		update deliveryOrders;
	}

	private static void updateRelatedDeliveryOrderMainProcess(List<Id> selectedDeliveryOrderIds) {
		List<CSPOFA__Orchestration_Process__c> orchestrationProcesss = new List<CSPOFA__Orchestration_Process__c>();
		for (CSPOFA__Orchestration_Process__c objDeliveryOrder : [
			SELECT Id
			FROM CSPOFA__Orchestration_Process__c
			WHERE COM_Delivery_Order__c IN :selectedDeliveryOrderIds AND CSPOFA__Process_On_Hold__c = FALSE
		]) {
			orchestrationProcesss.add(new CSPOFA__Orchestration_Process__c(Id = objDeliveryOrder.Id, CSPOFA__Process_On_Hold__c = true));
		}
		update orchestrationProcesss;
	}

	private static void updateServicesWithApplicableForInflightChange(Id orderId, List<Id> selectedDeliveryOrderIds) {
		String soqlQuery = 'SELECT Id from csord__Service__c WHERE ';
		// When specific delivery orders are selected from UI, use it to narrow down the records
		// otherwise we update all the services for current order, so that CPQ could handle them properly
		if (!selectedDeliveryOrderIds.isEmpty()) {
			soqlQuery += ' COM_Delivery_Order__c IN :selectedDeliveryOrderIds AND';
		}
		soqlQuery += ' csord__Order__c = :orderId AND Applicable_for_inflight_change__c = false';

		List<csord__Service__c> services = new List<csord__Service__c>();
		for (csord__Service__c service : Database.query(soqlQuery)) {
			services.add(new csord__Service__c(Id = service.Id, Applicable_for_inflight_change__c = true));
		}
		update services;
	}

	private static void clearInFlightChangeFlagOnCommercialOrder(Id orderId) {
		update new csord__Order__c(
			Id = orderId,
			Inflight_Change_Applied__c = false,
			// as participating in inflight change is not set from CS call cssmgnt.API_1.createInflightRequest(), we mark it as true
			csordtelcoa__Participating_in_Inflight_Change__c = true
		);
	}

	/**
	 * @description Wrapper class to provide initial data for UI
	 */
	public class InflightChangeData {
		@AuraEnabled
		public List<DeliveryOrder> availableDeliveries { get; set; }
		@AuraEnabled
		public List<DeliveryOrder> unavailableDeliveries { get; set; }
		@AuraEnabled
		public List<DataTableColumn> columns { get; set; }

		public InflightChangeData() {
			availableDeliveries = new List<DeliveryOrder>();
			unavailableDeliveries = new List<DeliveryOrder>();
		}
	}

	/**
	 * @description Holds the column configuration for datatable in the UI
	 */
	public class DataTableColumn {
		@AuraEnabled
		public String label { get; set; }
		@AuraEnabled
		public String fieldName { get; set; }
		public DataTableColumn(String label, String fieldName) {
			this.label = label;
			this.fieldName = fieldName;
		}
	}

	/**
	 * @description Holds the delivery order records
	 */
	public class DeliveryOrder {
		@AuraEnabled
		public String recordId { get; set; }
		@AuraEnabled
		public String name { get; set; }
		@AuraEnabled
		public String productName { get; set; }

		public DeliveryOrder(COM_Delivery_Order__c objDeliveryOrder) {
			this.recordId = objDeliveryOrder.Id;
			this.name = objDeliveryOrder.Name;
			this.productName = objDeliveryOrder.Products__c;
		}
	}

	public class InflightChangeException extends Exception {
	}
}
