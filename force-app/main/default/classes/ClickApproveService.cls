public with sharing class ClickApproveService {
	@InvocableMethod(label='Send ClickApprove Request' description='Sends ClickApprove Request for Approval' category='ClickApprove')
	public static void sendRequests(List<ClickApproveRequest> requests) {
		List<CSCAP.API_1.MultipleSendApprovalRequestRecord> approvalRequests = new List<CSCAP.API_1.MultipleSendApprovalRequestRecord>();

		Set<String> settingNames = new Set<String>();
		for (ClickApproveRequest request : requests) {
			if (request.clickApproveSettingsId == null) {
				settingNames.add(request.clickApproveSettingsName);
			}
		}
		Map<String, CSCAP__Click_Approve_Setting__c> settingsByName = getSettingsByName(settingNames);
		for (ClickApproveRequest request : requests) {
			if (request.clickApproveSettingsId == null) {
				CSCAP__Click_Approve_Setting__c setting = settingsByName.get(request.clickApproveSettingsName);
				request.clickApproveSettingsId = setting.Id;
			}
			approvalRequests.add(request.getClickApproveAPIRequest());
		}
		if (approvalRequests.size() > 0 && !Test.isRunningTest()) {
			CSCAP.API_1.MultipleSendApprovalRequest(approvalRequests);
		}
	}

	/**
	 * Gets all ClickApprove settings by Name
	 * @param  settingNames Setting Names.
	 * @return              Settings by Name.
	 */
	private static Map<String, CSCAP__Click_Approve_Setting__c> getSettingsByName(Set<String> settingNames) {
		Map<String, CSCAP__Click_Approve_Setting__c> settingsByName = new Map<String, CSCAP__Click_Approve_Setting__c>();
		if (settingNames.isEmpty()) {
			return settingsByName;
		}
		List<CSCAP__Click_Approve_Setting__c> settings = [SELECT Id, Name FROM CSCAP__Click_Approve_Setting__c WHERE Name IN :settingNames];
		for (CSCAP__Click_Approve_Setting__c setting : settings) {
			settingsByName.put(setting.Name, setting);
		}
		return settingsByName;
	}
}
