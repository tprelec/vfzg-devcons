public class UnicaCampaignBatchExecuteController {
	
	public UnicaCampaignBatchExecuteController(ApexPages.StandardSetController ignored) {

	}

	public PageReference callBatch(){
		if([SELECT Id FROM Unica_Campaign__c where Processed__c = false LIMIT 1000].size() > 0){
			UnicaCampaignBatch controller = new UnicaCampaignBatch();
			database.executebatch(controller);
		}
		Schema.DescribeSObjectResult prefix = Unica_Campaign__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}
}