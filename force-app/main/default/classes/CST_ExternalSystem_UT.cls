@isTest
public class CST_ExternalSystem_UT {
	@TestSetup
	static void makeData() {
		String recordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CST_Incident').getRecordTypeId();
		String usrId = UserInfo.getUserId();
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		Contact con = TestUtils.createContact(acc);
		con.Email = '123456abcde@mail.com';
		update con;

		RecordType incidentRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CST_Incident'];
		Id incidentRecordTypeId = (incidentRecordType != null) ? incidentRecordType.Id : null;

		Case cs = new Case(
			status = 'New',
			CST_Sub_Status__c = 'Working on it',
			CST_End2End_Owner__c = usrId,
			Subject = 'CST Case',
			Priority = 'P4',
			OwnerId = usrId,
			AccountId = acc.Id,
			ContactId = con.Id,
			CST_Case_is_with_Team__c = 'Internal - Advisor',
			RecordTypeId = incidentRecordTypeId,
			Remedy_Ticket_No__c = '123',
			IsStopped = true
		);

		insert cs;

		//cs.status = 'Accessing';
		//update cs;

		//cs.status = 'In Progress';
		//update cs;

		CST_Questionnaire__c questionnaire = new CST_Questionnaire__c(CST_Case__c = cs.Id);

		insert questionnaire;

		CST_Questionnaire_Line_Item__c lineItem = new CST_Questionnaire_Line_Item__c(
			CST_Questionnaire__c = questionnaire.Id,
			CST_Case__c = cs.Id,
			CST_Question__c = 'Question here',
			CST_Answer__c = 'Answer here'
		);

		insert lineItem;

		CST_ExternalSystem_Credentials__c remedyCredentials = new CST_ExternalSystem_Credentials__c();
		remedyCredentials.Name = 'Remedy';
		remedyCredentials.CST_Endpoint__c = 'https://vodafoneziggo.remedy.com';
		remedyCredentials.CST_Password__c = 'pass';
		remedyCredentials.CST_Username__c = 'user01';
		insert remedyCredentials;

		CSTRemedyData__c remedyData = new CSTRemedyData__c();
		remedyData.Name = 'ExternalSupportGroup';
		remedyData.ExtSupportGroup__c = 'SMC-Mobile & IT';

		insert remedyData;

		CST_External_Ticket__c remedy = new CST_External_Ticket__c(
			CST_Case__c = cs.id,
			CST_IncidentID__c = '565656',
			CST_Summary__c = 'test case',
			CST_Status__c = 'Pending'
		);
		insert remedy;

		String txtFile = 'info';
		ContentVersion docVer1 = new ContentVersion();
		docVer1.ContentLocation = 'S';
		docVer1.PathOnClient = 'Demo.txt';
		docVer1.title = 'Demo';

		Blob txtData = Blob.valueOf(txtFile);
		docVer1.VersionData = txtData;
		insert docVer1;

		ContentVersion docVer2 = [SELECT id, contentDocumentId FROM ContentVersion LIMIT 1];

		ContentDocumentLink docLink = new ContentDocumentLink();
		docLink.linkedEntityId = remedy.id;
		docLink.contentDocumentId = docVer2.contentDocumentId;
		docLink.shareType = 'I';
		docLink.visibility = 'AllUsers';
		insert docLink;
	}
	@isTest
	static void getQuestions() {
		system.debug('getQuestions');
		String testCaseId = [SELECT Id FROM Case LIMIT 1].Id;
		Test.startTest();
		CST_ExternalSystem.getQuestions(testCaseId);
		Test.stopTest();
	}

	@isTest
	static void submitExternalTicket() {
		system.debug('submitExternalTicket');
		String questId = [SELECT Id FROM CST_Questionnaire__c LIMIT 1].Id;
		Test.startTest();
		CST_ExternalSystem.submitExternalTicket(questId);
		Test.stopTest();
	}
	@isTest
	static void getFiles_Test() {
		System.debug('getFiles_Test');
		String csId = [SELECT Id FROM CST_External_Ticket__c LIMIT 1].Id;

		System.debug('csId ' + csId);

		ContentDocumentLink links = [
			SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility
			FROM ContentDocumentLink
			WHERE LinkedEntityId = :csId
			LIMIT 1
		];

		List<ContentVersion> contentVersions = [
			SELECT Id, Title, ContentDocumentId
			FROM ContentVersion
			WHERE ContentDocumentId = :links.ContentDocumentId
			LIMIT 1
		];

		System.debug('links ' + links);

		Test.startTest();
		List<String> idsList = new List<String>{ links.ContentDocumentId };
		System.debug('idsList: ' + idsList);

		String res = CST_ExternalSystem.getFiles(JSON.serialize(idsList));
		System.debug('getFiles_Test res: ' + res);

		Test.stopTest();
	}
	@isTest
	static void futureUpdateEnd2EndOwner_InRemedy_Test() {
		Id csId = [SELECT Id FROM CST_External_Ticket__c LIMIT 1].Id;

		Test.startTest();
		CST_ExternalSystem.futureUpdateEnd2EndOwner_InRemedy(new Set<Id>{ csId });
		Test.stopTest();
	}

	@isTest
	static void deleteFile_Test() {
		String csId = [SELECT Id FROM CST_External_Ticket__c LIMIT 1].Id;

		System.debug('csId ' + csId);

		ContentDocumentLink links = [
			SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility
			FROM ContentDocumentLink
			WHERE LinkedEntityId = :csId
			LIMIT 1
		];

		ContentVersion contentVersions = [
			SELECT Id, Title, ContentDocumentId
			FROM ContentVersion
			WHERE ContentDocumentId = :links.ContentDocumentId
			LIMIT 1
		];
		System.debug('contentVersions ' + contentVersions);

		String ticketId = [SELECT Id FROM CST_External_Ticket__c LIMIT 1].Id;
		System.debug('ticketId ' + ticketId);

		System.debug(contentVersions);
		Test.startTest();
		CST_ExternalSystem.deleteFiles(contentVersions.ContentDocumentId);
		Test.stopTest();
	}

	@isTest
	static void sendTicketToRemedy() {
		system.debug('sendTicketToRemedy');
		StaticResource sr = [SELECT Id, Name, ContentType, BodyLength, Body FROM StaticResource WHERE name = 'sldsOverrides'];

		cst_FileUploadMultiController.uploadInfo fileInfo = new cst_FileUploadMultiController.uploadInfo();
		fileInfo.fileName = '123.jpg';
		fileInfo.fileSize = 5432;
		fileInfo.fileContent = EncodingUtil.base64Encode(sr.Body);

		List<cst_FileUploadMultiController.uploadInfo> fileList = new List<cst_FileUploadMultiController.uploadInfo>();
		fileList.add(fileInfo);

		String jsonString = JSON.serialize(fileList);

		Case testCase = [SELECT id FROM Case];
		String remedyNumber = '';

		Test.startTest();
		remedyNumber = CST_ExternalSystem.submitTicketToRemedy(testCase.Id);
		Test.stopTest();

		System.debug('remedyNumber -> ' + remedyNumber);
		// System.assertNotEquals('',remedyNumber, 'Remedy Number can not be empty.');
	}

	@isTest
	static void updateTest() {
		system.debug('updateTest');
		CST_External_Ticket__c testCase = [SELECT id, CST_Status__c FROM CST_External_Ticket__c];
		testCase.CST_Status__c = 'Assigned';

		Test.startTest();
		update testCase;
		Test.stopTest();
	}
}
