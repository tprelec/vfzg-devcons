global class AccountAssetUpdateBatch implements Database.Batchable<sObject>, Database.AllowsCallouts, Database.Stateful, Schedulable {
	@TestVisible
	public static final Date RETENTION_DATE = Date.today() + 121;

	global Database.QueryLocator start(Database.BatchableContext bc) {
		List<AccountToUpdate__c> accountToUpdateList = new List<AccountToUpdate__c>();
		Map<Id, AggregateResult> results = new Map<id, AggregateResult>(
			[SELECT Account__c Id FROM VF_Asset__c WHERE Contract_End_Date__c = :RETENTION_DATE GROUP BY Account__c]
		);

		// For all accounts returned try and insert into the table for processing
		for (Id id : results.keySet()) {
			AccountToUpdate__c atu = new AccountToUpdate__c();
			atu.AccountId__c = id;
			accountToUpdateList.add(atu);
		}
		// Using insert will fail on any duplicates
		// so by using database.insert we allow the non duplicates to be successfully inserted
		Database.insert(accountToUpdateList, false);

		// Keep the number of records to 100000 and batch size of 2
		// This will result in a max of 50000 executions against the daily limit
		return Database.getQueryLocator('Select AccountID__c from AccountToUpdate__c limit 100000');
	}

	// This is the execute to handle a schedule request (just enables the job to be scheduled)
	global void execute(SchedulableContext sc) {
		AccountAssetUpdateBatch controller = new AccountAssetUpdateBatch();
		database.executebatch(controller, 2);
	}

	// This is the execute for the scope of the batch (ie does the processing)
	global void execute(Database.BatchableContext bc, List<sObject> scope) {
		// Note that we will want to limit the batch size to 1 so that only one account is processed in each execute
		processRecords(scope);
	}

	global void finish(Database.BatchableContext bc) {
		OrgWideEmailAddress orgWideEmailAddress = EmailService.getDefaultOrgWideEmailAddress();

		//Get the batch job for reference in the email.
		AsyncApexJob a = [SELECT Status, NumberOfErrors, TotalJobItems FROM AsyncApexJob WHERE Id = :bc.getJobId()];

		// Send an email to the running user notifying of job completion.
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();

		String[] toAddresses = new List<String>{ UserInfo.getUserEmail() };
		mail.setToAddresses(toAddresses);
		mail.setSubject('Competitor Assests recalculated ' + a.Status);
		mail.setPlainTextBody(
			'Competitor Assests recalculation job processed ' +
			a.TotalJobItems +
			' batches with ' +
			a.NumberOfErrors +
			' failures.'
		);

		if (orgWideEmailAddress != null) {
			mail.setOrgWideEmailAddressId(orgWideEmailAddress.Id);
		}
		if (EmailService.checkDeliverability()) {
			Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{ mail });
		}
	}

	public void processRecords(List<AccountToUpdate__c> accountsToProcess) {
		Integer retainable = 0; //teller to count the retainable assest
		Integer active = 0; //teller to count the Active assets
		Integer retainableDataAssets = 0;
		Integer retainableVoiceAssets = 0;
		List<Account> accountsToUpdate = new List<Account>();

		for (AccountToUpdate__c atu : accountsToProcess) {
			// Added limit 50000 to prevent SOQL error
			// Accounts that have more than 50000 are not valid anyway so the counts are not important for those
			List<VF_Asset__c> allVFAsset = [
				SELECT Id, Retainable__c, CTN_Status__c, Priceplan_Class__c
				FROM VF_Asset__c
				WHERE
					Account__c = :atu.AccountId__c
					AND CTN_Status__c = 'Active'
					AND (Priceplan_Class__c LIKE '%voice%'
					OR Priceplan_Class__c LIKE '%data%')
				LIMIT 49000
			];
			if (!allVFAsset.isEmpty()) {
				for (VF_Asset__c x : allVFAsset) {
					//System.Debug('Retainable__c' + x.Retainable__c);
					if (x.Retainable__c == true) {
						retainable++;
						if (x.Priceplan_Class__c.toLowercase().contains('voice')) {
							retainableVoiceAssets++;
						}
						if (x.Priceplan_Class__c.toLowercase().contains('data')) {
							retainableDataAssets++;
						}
					}
					//System.Debug('CTN_Status__c' + x.CTN_Status__c);
					active++;
				}

				Account a = new Account();
				a.id = atu.AccountID__c;
				a.Count_Retainable_Assets__c = retainable;
				a.Count_Active_Assets__c = active;
				a.Count_Retainable_Assets_Voice__c = retainableVoiceAssets;
				a.Count_Retainable_Assets_Data__c = retainableDataAssets;
				accountsToUpdate.add(a);
			}
			retainable = 0; //zet teller op 0, voor nieuwe Account
			active = 0; //zet teller op 0, voor nieuwe Account
			retainableVoiceAssets = 0;
			retainableDataAssets = 0;
		}

		update accountsToUpdate;

		// This deletes the entry in AccountToUpdate__c, not the actual account
		delete accountsToProcess;
	}
}
