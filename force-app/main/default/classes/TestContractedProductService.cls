@isTest
public class TestContractedProductService {
	@isTest
	static void setRedundancyReferencesAndHbos() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OrderTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);

		Profile p = [SELECT Id FROM Profile WHERE Name = 'VF Indirect Inside Sales' LIMIT 1];
		UserRole ur = [SELECT Id FROM UserRole WHERE DeveloperName = 'Network_Planner' LIMIT 1];
		List<User> userList = new List<User>();
		User owner = TestUtils.generateTestUser('admin', 'user', 'System Administrator');
		User salesUser = TestUtils.generateTestUser('testing', 'is', p.Id, null);
		User networkPlanner = TestUtils.generateTestUser('Network', 'Planner', p.Id, ur.Id);
		userList.add(owner);
		userList.add(salesUser);
		userList.add(networkPlanner);
		insert userList;

		System.runAs(owner) {
			TestUtils.autoCommit = true;
			Account acct = TestUtils.createAccount(owner);
			Contact generalContact = new Contact(
				AccountId = acct.Id,
				LastName = 'Current',
				FirstName = 'User',
				Phone = '+31645061130',
				MobilePhone = '+31645061133',
				Email = 'tstcontact@mail.com'
			);
			insert generalContact;
			Opportunity opp = TestUtils.createOpportunityWithBan(acct, Test.getStandardPricebookId(), null);
			Site__c site = TestUtils.createSite(acct);
			Site_Postal_Check__c sitePosCheck = new Site_Postal_Check__c(
				Access_Site_ID__c = site.Id,
				Access_Active__c = false,
				Access_Vendor__c = 'ZIGGO',
				Access_Result_Check__c = 'OFFNET',
				Access_Redundancy__c = 'Primary'
			);
			insert sitePosCheck;

			Map<String, Id> paramsHbo = new Map<String, Id>{
				'accId' => acct.Id,
				'oppId' => opp.Id,
				'siteId' => site.Id,
				'sitePosCheckId' => sitePosCheck.Id,
				'netwPlannerId' => networkPlanner.Id
			};
			TestContractedProductService.createHBO(paramsHbo);

			List<String> productRoles = new List<String>{
				'Link (Internet)',
				'Link',
				'Carrier',
				'Numberporting',
				'Phone',
				'ERS',
				'Dark Fiber',
				'Router'
			};
			List<String> linkTypesProducts = new List<String>{ 'ADSL', 'EOC', 'FIBER', 'SDSL', 'VDSL', 'WEAS', 'WEAS', 'ADSL' };

			OrderType__c ot = TestUtils.createOrderType();
			TestUtils.autoCommit = false;

			List<Product2> products = new List<Product2>();
			for (Integer i = 0; i < productRoles.size(); i++) {
				Product2 prod = TestUtils.createProduct();
				prod.Role__c = productRoles[i];
				prod.Link_type__c = linkTypesProducts[i];
				prod.Quantity_type__c = 'Monthly';
				prod.Product_Group__c = 'Priceplan';
				if (productRoles[i] == 'Router') {
					prod.Brand__c = 'TestBrand';
					prod.Model__c = 'TestModel';
				}
				products.add(prod);
			}
			insert products;

			// create Contract
			VF_Contract__c c = new VF_Contract__c(Account__c = acct.Id, Opportunity__c = opp.id, Solution_Sales__c = owner.Id);
			insert c;

			Order__c theOrder = new Order__c();
			theOrder.VF_Contract__c = c.Id;
			theOrder.OrderType__c = ot.Id;
			theOrder.Account__c = c.Account__c;
			theOrder.Escalation_Level__c = 'high';
			theOrder.Export__c = 'SIAS';
			theOrder.Status__c = 'Assigned to Inside Sales';
			theOrder.Inside_Sales_Owner__c = salesUser.Id;
			theOrder.Project_Contact__c = generalContact.Id;
			theOrder.Infra_Contact__c = generalContact.Id;
			theOrder.CC_Contact__c = generalContact.Id;
			theOrder.Porting_Contact__c = generalContact.Id;
			theOrder.Propositions__c = 'Internet;IPVPN;One Net;One Fixed;Voice';
			insert theOrder;

			List<Contracted_Products__c> cpInsert = new List<Contracted_Products__c>();
			List<String> linkTypes = new List<String>{ 'EthernetOverFiber', 'FTTH', 'EthernetOverCopper', 'VVDSL', 'COAX' };
			Decimal value = 10;
			Integer quantity = 1;

			Site_Availability__c siteAvail = TestUtils.createSiteAvailability(site.Id, sitePosCheck.Id);
			insert siteAvail;

			Contracted_Products__c cp = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[0].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENETX2014,ONENET2014,ONENET2014SUB30,One Net Enterprise,One Net Express',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp);

			Contracted_Products__c cp1 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[2].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Link_type__c = linkTypes[1],
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'IPVPN',
				Number_of_sessions__c = 2,
				Model_Number__c = 1,
				Access_Redundancy__c = 'Primary123123123',
				Model_Number_Identifier__c = '456456456',
				Site_Availability__c = siteAvail.Id
			);
			cpInsert.add(cp1);

			Contracted_Products__c cp2 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[2].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENET2014SUB30,One Net Express',
				Number_of_sessions__c = 2,
				Model_Number__c = 2,
				Access_Redundancy__c = 'Secondary456456456',
				Model_Number_Identifier__c = '123123123',
				Site_Availability__c = siteAvail.Id
			);
			cpInsert.add(cp2);

			insert cpInsert;

			Test.startTest();
			Set<Id> orderIds = new Set<Id>();
			orderIds.add(theOrder.Id);
			Set<Id> carrierProductIds;
			carrierProductIds = ContractedProductService.setRedundancyReferences(cpInsert);
			ContractedProductService.setHboS(cpInsert, carrierProductIds);
			Test.stopTest();

			Id accessPrimaryReferenceID;
			Id hboId;
			List<Contracted_Products__c> lCoPrResult = [
				SELECT Id, HBO__c, Access_Primary_Reference_ID__c
				FROM Contracted_Products__c
				WHERE Access_Redundancy__c LIKE 'Secondary%'
				LIMIT 1
			];
			accessPrimaryReferenceID = lCoPrResult[0].Access_Primary_Reference_ID__c;
			hboId = lCoPrResult[0].HBO__c;
			System.assertNotEquals(null, accessPrimaryReferenceID, 'Access_Primary_Reference_ID__c must to be disctinct of null');
			System.assertNotEquals(null, hboId, 'HBO__c must to be disctinct of null');
		}
	}

	/**
	 * Creates a complete HBO designed for testing Hbo auto-completion
	 * 'accId': account Id, 'oppId': Opportunity Id, 'siteId': Site Id, 'sitePosCheckId': Site Postal Check Id,
	 * 'netwPlannerId': Network Planner Id (User Id)
	 * @param params
	 */
	private static void createHBO(Map<String, Id> params) {
		HBO__c hbo = new HBO__c(
			hbo_account__c = params.get('accId'),
			hbo_opportunity__c = params.get('oppId'),
			hbo_status__c = 'New',
			hbo_site__c = params.get('siteId'),
			hbo_network_planner__c = params.get('netwPlannerId'),
			hbo_delivery_time__c = 12,
			hbo_digging_distance__c = 100,
			hbo_total_costs__c = 500,
			hbo_redundancy__c = false,
			hbo_availability__c = 'Yes',
			hbo_postal_check__c = params.get('sitePosCheckId'),
			hbo_result_type__c = 'Onnet'
		);
		insert hbo;
		hbo.hbo_status__c = 'Approved';
		update hbo;
	}
}
