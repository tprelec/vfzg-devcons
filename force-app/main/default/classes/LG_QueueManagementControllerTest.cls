@IsTest
private class LG_QueueManagementControllerTest {
	@TestSetup
	private static void setupTestData() {
		Map<String, String> mapDefaults = new Map<String, String>{
			'NL_Default_Zoom_Level' => '1',
			'NL_Default_Latitude' => '52.16812913256766',
			'NL_Default_Longitude' => '5.059651772744132'
		};
		TestUtils.createMapsSetting(mapDefaults);

		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		// this by-passes the mixed dml errors
		User sysUser = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		// Partner Account
		Account account = LG_GeneralTest.createAccount('Account', '12345678', 'Ziggo', false);
		account.Type = 'Partner';
		account.Total_Licenses__c = 2;
		account.Dealer_code__c = '123456';
		insert account;
		account.IsPartner = true;
		update account;

		Profile p = [SELECT Id FROM Profile WHERE Name IN ('LG_NL D2D Partner Manager', 'LG_NL D2D_Partner Manager') LIMIT 1];

		User testUser = new User(
			Alias = 'TestUser',
			Email = 'testUser@ziggo.dev2.com.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'Testing',
			LanguageLocaleKey = 'nl_NL',
			LocaleSidKey = 'nl_NL',
			ProfileId = p.Id,
			CompanyName = 'CS',
			TimeZoneSidKey = 'America/Los_Angeles',
			UserName = 'testUser@ziggo.dev2.com.com'
		);

		System.runAs(sysUser) {
			insert testUser;
		}

		Contact contPortUser = LG_GeneralTest.createContact(
			account,
			'First',
			'Last',
			'Mr.',
			'0031000000000',
			'0600000000',
			'TestPortalUser@ziggo.dev2.com.com',
			null,
			null,
			null,
			null,
			null,
			null
		);

		Contact contPortUser2 = LG_GeneralTest.createContact(
			account,
			'First2',
			'Last2',
			'Mr.',
			'0031000000000',
			'0600000000',
			'TestPortalUser2@ziggo.dev2.com.com',
			null,
			null,
			null,
			null,
			null,
			null
		);

		Profile portalUserProfile = [SELECT Id FROM Profile WHERE usertype = 'PowerPartner' LIMIT 1];

		User portalUser = new User(
			Alias = 'TPUser',
			Email = 'TestPortalUser@ziggo.dev2.com.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'Testing',
			LanguageLocaleKey = 'nl_NL',
			LocaleSidKey = 'nl_NL',
			ProfileId = portalUserProfile.Id,
			CompanyName = 'CS',
			TimeZoneSidKey = 'America/Los_Angeles',
			UserName = 'TestPortalUser@ziggo.dev2.com.com',
			ContactId = contPortUser.Id
		);

		User portalUser2 = new User(
			Alias = 'TUser2',
			Email = 'TestPortalUser2@ziggo.dev2.com.com',
			EmailEncodingKey = 'UTF-8',
			LastName = 'Testing',
			LanguageLocaleKey = 'nl_NL',
			LocaleSidKey = 'nl_NL',
			ProfileId = portalUserProfile.Id,
			CompanyName = 'CS',
			TimeZoneSidKey = 'America/Los_Angeles',
			UserName = 'TestPortalUser2@ziggo.dev2.com.com',
			ContactId = contPortUser2.Id
		);

		System.runAs(sysUser) {
			insert portalUser;
			insert portalUser2;
		}

		LG_PartnerQueueManagement__c partnerQMng = new LG_PartnerQueueManagement__c(LG_PartnerAccount__c = account.Id);
		insert partnerQMng;

		//by-passes the mixed dml errors
		System.runAs(sysUser) {
			setupQueues();
			setupPartnerQmng();
		}

		noTriggers.Flag__c = false;
		upsert noTriggers;
	}

	private static void setupQueues() {
		Group testGroup = new Group(Name = 'TestQueue', Type = 'Queue');
		insert testGroup;

		QueueSobject testQueue = new QueueSObject(QueueId = testGroup.Id, SobjectType = 'LG_PartnerQueueManagement__c');
		insert testQueue;

		User portalUser = [SELECT Id FROM User WHERE Alias = 'TUser2'];

		insert new GroupMember(UserOrGroupId = portalUser.Id, GroupId = testGroup.Id);
	}

	private static void setupPartnerQmng() {
		Group testGroup = [SELECT Id FROM Group WHERE Name = 'TestQueue'];
		LG_PartnerQueueManagement__c partnerQMng = [SELECT Id FROM LG_PartnerQueueManagement__c LIMIT 1];
		partnerQMng.OwnerId = testGroup.Id;
		update partnerQMng;
	}

	@IsTest
	private static void testPartnerAccountNotAssigned() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());

		List<LG_PartnerQueueManagementUser__c> accountUser = [
			SELECT Id, LG_PartnerAccount__c, LG_PartnerAccountName__c
			FROM LG_PartnerQueueManagementUser__c
		];
		delete AccountUser;

		noTriggers.Flag__c = false;
		upsert noTriggers;

		Test.startTest();

		LG_QueueManagementController controller = new LG_QueueManagementController();

		Test.stopTest();

		System.assertEquals(true, controller.selectedUsers.isEmpty(), 'Selected Users list should be empty');
		System.assertEquals(true, controller.allUsers.isEmpty(), 'All Users list should be empty');
		System.assertEquals(null, controller.partnerAccount.Id, 'Partner Account Id should be null');
		System.assertEquals(
			LG_QueueManagementController.USER_ACCOUNT_NOT_LINKED,
			controller.partnerAccount.Name,
			'Partner Account Name should be equal to ' + LG_QueueManagementController.USER_ACCOUNT_NOT_LINKED
		);
	}

	@IsTest
	private static void testSave() {
		User user = [SELECT Id, Name FROM User WHERE Alias = 'TestUser'];
		User portalUser2 = [SELECT Id, Name FROM User WHERE Alias = 'TUser2'];
		User portalUser = [SELECT Id, Name FROM User WHERE Alias = 'TPUser'];
		Group queue = [SELECT Id FROM Group WHERE Name = 'TestQueue'];

		System.runAs(user) {
			Test.startTest();

			LG_QueueManagementController controller = new LG_QueueManagementController();
			controller.selectedQueue = queue.Id;
			controller.queues = new List<group>{ queue };
			controller.requeryUsers();
			controller.selectedUsers.add(new SelectOption(portalUser2.Id, portalUser2.Name));
			controller.save();

			Test.stopTest();
		}

		List<GroupMember> currentMembers = [SELECT Id, UserOrGroupId FROM GroupMember WHERE Group.Type = 'Queue' AND GroupId = :queue.Id];

		system.assertEquals(1, currentMembers.size(), 'Only one user should be selected');
	}

	@IsTest
	private static void partnerUserExistsTest() {
		User user = [SELECT Id, Name FROM User WHERE Alias = 'TestUser'];
		User portalUser2 = [SELECT Id, Name FROM User WHERE Alias = 'TUser2'];
		User portalUser = [SELECT Id, Name FROM User WHERE Alias = 'TPUser'];
		Group queue = [SELECT Id FROM Group WHERE Name = 'TestQueue'];

		LG_PartnerQueueManagement__c pqMngmt = [SELECT Id, LG_QueueAPIName__c, LG_PartnerAccount__c FROM LG_PartnerQueueManagement__c LIMIT 1];

		LG_PartnerQueueManagementUser__c pquser = new LG_PartnerQueueManagementUser__c(
			LG_UserName__c = user.Id,
			LG_PartnerAccount__c = pqMngmt.LG_PartnerAccount__c
		);

		insert pquser;

		System.runAs(user) {
			Test.startTest();

			LG_QueueManagementController controller = new LG_QueueManagementController();
			controller.selectedQueue = queue.Id;
			controller.queues = new List<group>{ queue };
			controller.requeryUsers();
			controller.selectedUsers.add(new SelectOption(portalUser2.Id, portalUser2.Name));
			controller.save();

			Test.stopTest();
		}
	}
}
