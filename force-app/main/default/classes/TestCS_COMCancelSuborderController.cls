@IsTest
private class TestCS_COMCancelSuborderController {
    private static string ORDER_NAME = 'TestOrder1';
    private static string SUBORDER_NAME_1 = 'TestSuborder1';
    private static string SUBORDER_NAME_2 = 'TestSuborder2';
    private static string CANCELLATION_REQUESTED_STATUS = 'Cancellation Requested';
    
    @TestSetup
    static void testSetup(){
        csord__Order__c order = CS_DataTest.createOrder();
        order.Name = ORDER_NAME;
        insert order;

        CS_DataTest.createSuborder(SUBORDER_NAME_1, order.Id, true);
        CS_DataTest.createSuborder(SUBORDER_NAME_2, order.Id, true);
    }
    
    @isTest
    static void testGetSuborderList() {
        List<csord__Order__c> orderList = [
            SELECT Id
            FROM csord__Order__c
            WHERE Name = :ORDER_NAME
            ];
        
        List<Suborder__c> suborderList = [
            SELECT Id
            FROM Suborder__c
            WHERE Name in (:SUBORDER_NAME_1,:SUBORDER_NAME_2)
        ];

        CS_ComCancelSuborderController.RequestWrap reqWrap = new CS_ComCancelSuborderController.RequestWrap();
        reqWrap.recordId = orderList[0].Id;

        Test.startTest();
        String result = CS_ComCancelSuborderController.getSuborderList(JSON.serialize(reqWrap));
        Test.stopTest();

        CS_ComCancelSuborderController.SuborderListWrap subListWrap = (CS_ComCancelSuborderController.SuborderListWrap) JSON.deserialize(result, CS_ComCancelSuborderController.SuborderListWrap.class);
        System.assertEquals(2, subListWrap.suborderList.size());
        System.assertEquals(suborderList[0].Id, subListWrap.suborderList[0].Id);
        System.assertEquals(suborderList[1].Id, subListWrap.suborderList[1].Id);
    }
    
    @IsTest
    static void testBehaviorCancelSuborder() {
        List<Suborder__c> suborderList = [
            SELECT Id
            FROM Suborder__c
            WHERE Name in (:SUBORDER_NAME_1)
        ];

        CS_ComCancelSuborderController.RequestListWrap reqWrap = new CS_ComCancelSuborderController.RequestListWrap();
        reqWrap.suborderList = suborderList;

        Test.startTest();
        String result = CS_ComCancelSuborderController.cancelSuborder(JSON.serialize(reqWrap));
        Test.stopTest();

        CS_ComCancelSuborderController.ResponseWrap resultWrap = (CS_ComCancelSuborderController.ResponseWrap) JSON.deserialize(result, CS_ComCancelSuborderController.ResponseWrap.class);
        System.assertEquals(true, resultWrap.suborderCancelled);

        List<Suborder__c> updatedSuborderList = [
            SELECT Id, Status__c
            FROM Suborder__c
            WHERE Name in (:SUBORDER_NAME_1,:SUBORDER_NAME_2)
        ];

        System.assertEquals(CANCELLATION_REQUESTED_STATUS, updatedSuborderList[0].Status__c);
        System.assertNotEquals(CANCELLATION_REQUESTED_STATUS, updatedSuborderList[1].Status__c);
    }
}