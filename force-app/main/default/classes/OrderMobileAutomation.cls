public without sharing class OrderMobileAutomation {
	private static final Date DEPLOY_DATE = Date.newInstance(2021, 1, 11);
	private static final Boolean ISORDERAUTOMATED = true;
	private OrderWrapper mobileOrderWrapper;
	private List<Contracted_Products__c> contratedProductsList;
	private List<String> orderPropositions;
	private Mobile_Rules_For_Automation__mdt mdtMRA;

	public void init(OrderWrapper mobileOrderWrapper, List<Contracted_Products__c> contratedProductsList) {
		this.mobileOrderWrapper = mobileOrderWrapper;
		this.contratedProductsList = contratedProductsList;
		String mdtInstance = mobileOrderWrapper.theOrder.VF_Contract__r.Opportunity__r.Select_Journey__c == 'Add Mobile Product'
			? 'DeepSell'
			: mobileOrderWrapper.theOrder.VF_Contract__r.Opportunity__r.Select_Journey__c;
		this.mdtMRA = Mobile_Rules_For_Automation__mdt.getInstance(mdtInstance);
		this.orderPropositions = mobileOrderWrapper.theOrder.Propositions__c.split(';');
	}

	public Boolean canMobileOrderdBeAutomated() {
		return markMobileOrderForAutomation();
	}

	private Boolean markMobileOrderForAutomation() {
		//if there is no Metadata return false, no journey = no automation, also could mean the Order is not Mobile (fail check)
		if (mdtMRA == null) {
			return false;
		}

		Boolean isStatusAllow = mdtMRA?.Allowed_Status__c?.split(';')?.contains(mobileOrderWrapper.theOrder.Status__c);
		//pointless to check further if the status is not valid
		if (!isStatusAllow) {
			//return the previous value meaning no change after the order is submitted
			return mobileOrderWrapper.theOrder.EMP_Automated_Mobile_order__c;
		}
		Boolean validInfo = mobileOrderContainsValidInfo();
		Boolean validAccountValues = mobileOrderContainsValidAccountValues();
		Boolean validContractedProducts = mobileOrderContainsValidContractedProducts();
		Boolean validPropositions = mobileOrderContainsValidPropositions();
		//up to this point all validations should pass to Automate the Mobile Order
		return (validInfo && validAccountValues && validContractedProducts && validPropositions);
	}

	private Boolean mobileOrderContainsValidInfo() {
		Boolean isSegmentAllow = mdtMRA
			?.Allowed_Segments__c
			?.split(';')
			?.contains(mobileOrderWrapper.theOrder.VF_Contract__r.Opportunity__r.Segment__c);
		if (mobileOrderWrapper.theOrder.VF_Contract__r.Opportunity__r.CreatedDate < DEPLOY_DATE || !isSegmentAllow) {
			return false;
		}

		return true;
	}

	private Boolean mobileOrderContainsValidAccountValues() {
		Boolean accountValid = true;
		//when the account is marked as one net, then check if allows it
		if (
			mobileOrderWrapper.theOrder.Account__r.One_Net__c ||
			mobileOrderWrapper.theOrder.Account__r.One_Net_Enterprise__c ||
			mobileOrderWrapper.theOrder.Account__r.One_Net_Express__c
		) {
			accountValid = mdtMRA?.Allow_One_Net_Klant__c;
		}

		//when the account is marked as one fixed, then check if allows it
		if (mobileOrderWrapper.theOrder.Account__r.One_Fixed__c) {
			accountValid = mdtMRA?.Allow_One_Fixed_Klant__c;
		}

		return accountValid;
	}

	private Boolean mobileOrderContainsValidContractedProducts() {
		Set<String> setAllowedCLC = new Set<String>(mdtMRA.Allowed_CLC__c.split(';'));
		Set<String> setAllowedProductGroups = new Set<String>(mdtMRA.Include_the_following_product_groups__c.split(';'));

		boolean allowedProductGroups = true;
		boolean allowedCLC = true;
		for (Contracted_Products__c cp : contratedProductsList) {
			if (!setAllowedProductGroups.contains(cp.Product__r.Product_Group__c)) {
				allowedProductGroups = false;
			}

			if (!setAllowedCLC.contains(cp.CLC__c)) {
				allowedCLC = false;
			}
		}

		//only allowed Product Groups will Automate the Mobile Order
		//only allowed CLC will Automate the Mobile Order
		return (allowedProductGroups && allowedCLC);
	}

	private Boolean mobileOrderContainsValidPropositions() {
		Set<String> setAllowedPricePlans = new Set<String>(mdtMRA.Price_Plans__c.split(';'));
		boolean automaticPricePlanIncluded = true;
		//only allowed Propositions(in Order)/Priceplans will Automate the Mobile Order
		for (String individualProposition : orderPropositions) {
			if (!setAllowedPricePlans.contains(individualProposition)) {
				automaticPricePlanIncluded = false;
			}
		}

		return automaticPricePlanIncluded;
	}
}
