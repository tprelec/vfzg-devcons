/**
 * @description       : Apex Class Handler of the Customer Connection Trigger
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

public class CustomerConnectionTriggerHandler extends TriggerHandler {
	private List<CS_Customer_Connection__c> lstNewCustomerConnection;

	private Map<Id, CS_Customer_Connection__c> mapOldCustomerConnection;
	private Map<Id, CS_Customer_Connection__c> mapNewCustomerConnection;

	/**
	 * @description Initialize Class Variables
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	private void init() {
		lstNewCustomerConnection = (List<CS_Customer_Connection__c>) this.newList;
		mapOldCustomerConnection = (Map<Id, CS_Customer_Connection__c>) this.oldMap;
		mapNewCustomerConnection = (Map<Id, CS_Customer_Connection__c>) this.newMap;
	}

	/**
	 * @description Override beforeInsert method from TriggerHandler Class
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	/**
	 * @description Method that calculates the External ID field using the External ID Custom Setting
	 * @author Daniel Guzman (daniel.guzman@vodafoneziggo.com) | 19-07-2022
	 **/
	public void setExternalIds() {
		Sequence objSequence = new Sequence('Commercial Product');

		if (objSequence.canBeUsed()) {
			objSequence.startMutex();

			for (CS_Customer_Connection__c objCustomerConnection : lstNewCustomerConnection) {
				objCustomerConnection.ExternalID__c = objSequence.incr(1, 8, false);
			}

			objSequence.endMutex();
		}
	}
}
