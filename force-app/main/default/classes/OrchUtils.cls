public without sharing class OrchUtils {
	public static Map<Id, CSPOFA__Orchestration_Process__c> mapIdConfigValues { get; set; }

	public static Map<Id, String> mapOrderId {
		get {
			Map<Id, String> retMap = new Map<Id, String>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.vz_order__r.Id);
				}
			}
			return retMap;
		}
	}

	public static Map<Id, String> mapsalesJourney {
		get {
			Map<Id, String> retMap = new Map<Id, String>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.vz_order__r.vf_contract__r.opportunity__r.select_journey__c);
				}
			}
			return retMap;
		}
	}
	public static Map<Id, String> mapcontractId {
		get {
			Map<Id, String> retMap = new Map<Id, String>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.vz_order__r.vf_contract__r.id);
				}
			}
			return retMap;
		}
	}
	public static Map<Id, String> mapbillingCustomer {
		get {
			Map<Id, String> retMap = new Map<Id, String>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.vz_order__r.vf_contract__r.opportunity__r.ban__r.name);
				}
			}
			return retMap;
		}
	}
	public static Map<Id, String> mapcontractedProductId {
		get {
			Map<Id, String> retMap = new Map<Id, String>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.contracted_product__r.id);
				}
			}
			return retMap;
		}
	}
	public static Map<Id, String> mapcpFrameworkId {
		get {
			Map<Id, String> retMap = new Map<Id, String>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.contracted_product__r.Framework_Id__c);
				}
			}
			return retMap;
		}
	}

	public static Map<Id, String> mapUnifyOrderId {
		get {
			Map<Id, String> retMap = new Map<Id, String>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.contracted_product__r.Unify_FW_Order_Id__c);
				}
			}
			return retMap;
		}
	}

	public static Map<Id, String> mapcontractedProductUnifyOrderId {
		get {
			Map<Id, String> retMap = new Map<Id, String>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.contracted_product__r.Unify_Order_Id__c);
				}
			}
			return retMap;
		}
	}
	public static Map<Id, String> mapcontractedProductOrderId {
		get {
			Map<Id, String> retMap = new Map<Id, String>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.contracted_product__r.Order__c);
				}
			}
			return retMap;
		}
	}

	public static Map<Id, Decimal> mapcontractedProductModelNumber {
		get {
			Map<Id, Decimal> retMap = new Map<Id, Decimal>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, objProcess.contracted_product__r.Model_Number__c);
				}
			}
			return retMap;
		}
	}

	public static Map<Id, Integer> mapcontractedProductDuration {
		get {
			Map<Id, Integer> retMap = new Map<Id, Integer>();
			if (!mapIdConfigValues.isEmpty()) {
				for (CSPOFA__Orchestration_Process__c objProcess : mapIdConfigValues.values()) {
					retMap.put(objProcess.Id, Integer.valueOf(objProcess.contracted_product__r.Duration__c));
				}
			}
			return retMap;
		}
	}

	public static void getProcessDetails(Set<Id> setIds) {
		Map<Id, CSPOFA__Orchestration_Process__c> retMapIdOrchStep = new Map<Id, CSPOFA__Orchestration_Process__c>(
			[
				SELECT
					Id,
					vz_order__r.vf_contract__r.opportunity__r.select_journey__c,
					vz_order__r.vf_contract__r.id,
					vz_order__r.vf_contract__r.opportunity__r.ban__r.name,
					vz_order__r.Id,
					contracted_product__r.id,
					contracted_product__r.Framework_Id__c,
					vz_order__r.vf_contract__r.Unify_Order_Id__c,
					contracted_product__r.Unify_Order_Id__c,
					contracted_product__r.Unify_FW_Order_Id__c,
					contracted_product__r.Model_Number__c,
					contracted_product__r.Order__c,
					contracted_product__r.Duration__c
				FROM CSPOFA__Orchestration_Process__c
				WHERE Id IN :setIds
			]
		);

		mapIdConfigValues = retMapIdOrchStep;
	}

	public static CSPOFA__Orchestration_Step__c setStepRecord(CSPOFA__Orchestration_Step__c objStep, Boolean blnIsError, String strMessage) {
		objStep.CSPOFA__Status__c = (blnIsError ? 'Error' : 'Complete');
		objStep.CSPOFA__Completed_Date__c = (blnIsError ? null : Date.today());
		objStep.CSPOFA__Message__c = strMessage.abbreviate(255);
		return objStep;
	}

	public static List<sObject> tryUpdateRelatedRecord(
		List<CSPOFA__Orchestration_Step__c> stepList,
		List<sObject> result,
		List<NetProfit_CTN__c> ctnToUpdate
	) {
		try {
			Map<Id, String> failedCTNUpdateMap = new Map<Id, String>();
			List<Database.SaveResult> updateResults = Database.update(ctnToUpdate, false);
			for (Integer i = 0; i < updateResults.size(); i++) {
				if (!updateResults.get(i).isSuccess()) {
					Database.Error error = updateResults.get(i).getErrors().get(0);
					failedCTNUpdateMap.put(ctnToUpdate.get(i).Id, error.getMessage());
				}
			}

			if (!failedCTNUpdateMap.isEmpty()) {
				for (CSPOFA__Orchestration_Step__c step : stepList) {
					result.add(
						OrchUtils.setStepRecord(
							step,
							true,
							('Error occurred: CTN update failed with error: ' + failedCTNUpdateMap?.values()?.get(0)).abbreviate(255)
						)
					);
				}
			}
		} catch (Exception e) {
			for (CSPOFA__Orchestration_Step__c step : stepList) {
				result.add(
					OrchUtils.setStepRecord(
						step,
						true,
						('Error occurred: ' + e.getMessage() + ' on line ' + e.getLineNumber() + e.getStackTraceString()).abbreviate(255)
					)
				);
			}
		}
		return result;
	}

	public static List<NetProfit_CTN__c> processExistingCTNs(Map<String, List<NetProfit_CTN__c>> mapExistingnNumberListCTN) {
		List<NetProfit_CTN__c> retlstCTN = new List<NetProfit_CTN__c>();
		for (String ctnPor : mapExistingnNumberListCTN.keySet()) {
			for (NetProfit_CTN__c ctn : mapExistingnNumberListCTN.get(ctnPor)) {
				if (ctn.CTN_Status__c.equalsIgnoreCase('Future')) {
					if (!ctn.CTN_Number__c.containsIgnoreCase('XX')) {
						ctn.CTN_Status__c = 'MSISDN Assigned';
					} else {
						ctn.CTN_Status__c = 'MSISDN Not Assigned';
					}
					retlstCTN.add(ctn);
				}
			}
		}
		return retlstCTN;
	}
}
