@SuppressWarnings('PMD') // only cyclomatic complexity
public with sharing class COM_DeliveryEmailTemplatesMerge {
	// Resolve special merge fields for email message body.
	public static String processEmailTemplate(EmailTemplate emailTemplate, COM_Delivery_Order__c deliveryOrder) {
		deliveryOrder = fetchDeliveryOrder(deliveryOrder.Id);
		String mailBody = replaceEmailTemplateHtmlFromMetadata(emailTemplate, deliveryOrder);
		mailBody = replaceEmailTemplateHtmlFromDeliveryOrder(mailBody, deliveryOrder);

		return mailBody;
	}

	// Resolve special merge fields for email message subject.
	public static String processEmailSubject(String subject, COM_Delivery_Order__c deliveryOrder) {
		deliveryOrder = fetchDeliveryOrder(deliveryOrder.Id);
		return replaceEmailTemplateHtmlFromDeliveryOrder(subject, deliveryOrder);
	}

	@TestVisible
	private static String replaceEmailTemplateHtmlFromMetadata(EmailTemplate emailTemplate, COM_Delivery_Order__c deliveryOrder) {
		List<COM_Delivery_Email_Template__mdt> listOfDeliveryEmailTemplates = [
			SELECT id, Block__c, Content__c, Change_Type__c, COM_Delivery_Component__r.Label
			FROM COM_Delivery_Email_Template__mdt
			WHERE Email_Template_Unique_Name__c = :emailTemplate.DeveloperName AND Attachment__c = FALSE
		];
		Set<String> unusedTags = new Set<String>();

		String mailBody;
		if (emailTemplate.TemplateType != 'text') {
			mailBody = emailTemplate.HtmlValue;
		} else {
			mailBody = emailTemplate.Body;
		}

		mailBody = mailBody.replace('<![CDATA[', '').replace(']]>', '');

		for (COM_Delivery_Email_Template__mdt template : listOfDeliveryEmailTemplates) {
			if (
				checkForChangeType(template.Change_Type__c, deliveryOrder) &&
				containsDeliveryComponent(template.COM_Delivery_Component__r.Label, deliveryOrder)
			) {
				mailBody = mailBody.replace('[BLOCK_' + template.Block__c + ']', template.Content__c);
			} else {
				unusedTags.add(template.Block__c);
			}
		}

		for (String tag : unusedTags) {
			mailBody = mailBody.replace('[BLOCK_' + tag + ']', '');
		}

		return mailBody;
	}

	@TestVisible
	private static String replaceEmailTemplateHtmlFromDeliveryOrder(String mailBody, COM_Delivery_Order__c deliveryOrder) {
		csord__Service__c l2Service = findL2Service(deliveryOrder);
		List<COM_SrvDeliveryComponentsTableCntrl.ServiceWrapper> serviceWrappers = COM_SrvDeliveryComponentsTableCntrl.getServices(deliveryOrder.Id);
		DateTime l2Date = getL2Date(l2Service, deliveryOrder.MACD_Type__c);
		Map<String, String> fieldValueMergeMap = new Map<String, String>{
			'{DeliveryOrder.Technical_Contact_Name}' => deliveryOrder.Technical_Contact__c != null ? deliveryOrder.Technical_Contact__r.Name : '',
			'{DeliveryOrder.Technical_Contact_Phone}' => (deliveryOrder.Technical_Contact__c != null &&
				deliveryOrder.Technical_Contact__r.Phone != null)
				? deliveryOrder.Technical_Contact__r.Phone
				: '',
			'{DeliveryOrder.Technical_Contact_Email}' => (deliveryOrder.Technical_Contact__c != null &&
				deliveryOrder.Technical_Contact__r.Email != null)
				? deliveryOrder.Technical_Contact__r.Email
				: '',
			'{DeliveryOrder.Installation_Start}' => deliveryOrder.Installation_Start__c != null ? deliveryOrder.Installation_Start__c.format() : '',
			'{DeliveryOrder.Installation_Start_Time}' => deliveryOrder.Installation_End__c != null
				? deliveryOrder.Installation_Start__c.format('HH:mm')
				: '',
			'{DeliveryOrder.Installation_End}' => deliveryOrder.Installation_End__c != null ? deliveryOrder.Installation_End__c.format() : '',
			'{DeliveryOrder.Installation_End_Time}' => deliveryOrder.Installation_End__c != null
				? deliveryOrder.Installation_End__c.format('HH:mm')
				: '',
			'{DeliveryOrder.Products__c}' => deliveryOrder.Products__c != null ? deliveryOrder.Products__c : '',
			'{DeliveryOrder.Name}' => deliveryOrder.Name,
			'{DeliveryOrder.Installation_Address}' => deliveryOrder.Site__c != null ? deliveryOrder.Site__r.Name : '',
			'{DeliveryOrder.SiteStreet}' => deliveryOrder.Site__c != null ? deliveryOrder.Site__r.Site_Street__c : '',
			'{DeliveryOrder.SiteHouseNumber}' => deliveryOrder.Site__c != null ? String.valueOf(deliveryOrder.Site__r.Site_House_Number__c) : '',
			'{DeliveryOrder.SiteHouseNumberAddition}' => (deliveryOrder.Site__c != null &&
				deliveryOrder.Site__r.Site_House_Number_Suffix__c != null)
				? deliveryOrder.Site__r.Site_House_Number_Suffix__c
				: '',
			'{DeliveryOrder.SiteZipCode}' => deliveryOrder.Site__c != null ? deliveryOrder.Site__r.Site_Postal_Code__c : '',
			'{DeliveryOrder.SiteCity}' => deliveryOrder.Site__c != null ? deliveryOrder.Site__r.Site_City__c : '',
			'{DeliveryOrder.Technical_Details}' => '',
			'{DeliveryOrder.Services}' => formatServiceData(deliveryOrder),
			'{DeliveryOrder.NewServices}' => filterServicesByMACDAction(deliveryOrder, 'Add', true),
			'{DeliveryOrder.SoftwareServices}' => '',
			'{DeliveryOrder.TerminatedServices}' => '',
			'{DeliveryOrder.Termination_Date}' => deliveryOrder.Termination_Date__c != null ? deliveryOrder.Termination_Date__c.format() : '',
			'{DeliveryOrder.Delivery_Order_Number}' => deliveryOrder.Delivery_Order_Number__c,
			'{DeliveryOrder.Account_Name}' => deliveryOrder.Account__c != null ? deliveryOrder.Account__r.Name : '',
			'{DeliveryOrder.Macd_Type}' => deliveryOrder.MACD_Type__c == COM_Constants.MACD_TYPE_TERMINATION
				? COM_Constants.MACD_TYPE_TERMINATION_DUTCH
				: COM_Constants.MACD_TYPE_NEW_DUTCH,
			'{DeliveryOrder.L2_Implemented_Date}' => (l2Service != null && l2Date != null) ? l2Date.format('dd-MM-yyyy') : '',
			'{DeliveryOrder.L2_Implemented_Time}' => (l2Service != null && l2Date != null) ? l2Date.format('HH:mm') : '',
			'{DeliveryOrder.L2_Access_Id}' => (l2Service != null && l2Service.OSS10_Service_Id__c != null) ? l2Service.OSS10_Service_Id__c : '',
			'{DeliveryOrder.NoIPText}' => !containsDeliveryArticle(serviceWrappers, '1DYNIPV4') ? COM_Constants.NO_IP_TEXT : ''
		};

		for (String key : fieldValueMergeMap.keySet()) {
			mailBody = mailBody.replace(key, fieldValueMergeMap.get(key));
		}

		return mailBody;
	}

	@TestVisible
	public static Id fetchStylingTemplateId() {
		Id result = null;
		List<EmailTemplate> et = [SELECT Id FROM EmailTemplate WHERE DeveloperName = :COM_Constants.STYLING_TEMPLATE_DEVELOPER_NAME LIMIT 1];

		if (et.Size() != 0) {
			result = et[0].Id;
		}

		return result;
	}

	//True if opportunity change type is equal to metadata type or if metadata type is null or All.
	@TestVisible
	private static Boolean checkForChangeType(String changeType, COM_Delivery_Order__c deliveryOrder) {
		String opportunityChangeType = deliveryOrder.Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c;
		if (opportunityChangeType == null || opportunityChangeType == 'Add') {
			opportunityChangeType = 'New';
		}

		return changeType == null || changeType == 'All' || changeType == opportunityChangeType;
	}

	@TestVisible
	private static Boolean containsDeliveryComponent(String deliveryComponent, COM_Delivery_Order__c deliveryOrder) {
		Boolean containsComponent = false;
		if (deliveryComponent == null || deliveryOrder.Delivery_Components__c.contains(deliveryComponent)) {
			containsComponent = true;
		}
		return containsComponent;
	}

	@TestVisible
	private static Boolean containsDeliveryArticle(
		List<COM_SrvDeliveryComponentsTableCntrl.ServiceWrapper> serviceWrappers,
		String deliveryArticleName
	) {
		Boolean result = false;

		for (COM_SrvDeliveryComponentsTableCntrl.ServiceWrapper servWrapp : serviceWrappers) {
			if (servWrapp.deliveryArticleName == deliveryArticleName) {
				result = true;
				break;
			}
		}

		return result;
	}

	//Fetch email attachments as defined in the COM_Delivery_Email_Template__mdt metadata.
	public static List<Messaging.EmailFileAttachment> fetchTemplateEmailAttachments(String emailTemplateName, COM_Delivery_Order__c deliveryOrder) {
		deliveryOrder = fetchDeliveryOrder(deliveryOrder.Id);
		List<Messaging.EmailFileAttachment> emailAttachments = new List<Messaging.EmailFileAttachment>();
		List<COM_Delivery_Email_Template__mdt> listOfDeliveryEmailTemplates = [
			SELECT
				Id,
				Block__c,
				Content__c,
				Change_Type__c,
				COM_Delivery_Component__c,
				COM_Delivery_Component__r.Label,
				Generate_Attachment_from_Template__c
			FROM COM_Delivery_Email_Template__mdt
			WHERE Email_Template_Unique_Name__c = :emailTemplateName AND Attachment__c = TRUE
		];
		Set<String> attachmentNames = new Set<String>();
		Set<String> generatorTemplateNames = new Set<String>();

		for (COM_Delivery_Email_Template__mdt template : listOfDeliveryEmailTemplates) {
			if (
				checkForChangeType(template.Change_Type__c, deliveryOrder) &&
				containsDeliveryComponent(template.COM_Delivery_Component__r.Label, deliveryOrder)
			) {
				if (template.Generate_Attachment_from_Template__c) {
					generatorTemplateNames.add(template.Content__c);
				} else {
					attachmentNames.add(template.Content__c);
				}
			}
		}

		List<EmailTemplate> generatorTemplates = [SELECT Id, Name, HtmlValue FROM EmailTemplate WHERE DeveloperName IN :generatorTemplateNames];

		/*for (EmailTemplate generatorTemplate : generatorTemplates) {
			emailAttachments.add(
				prepareEmailFileAttachment(
					generatorTemplate.Name + '.pdf',
					Blob.toPDF(
						replaceEmailTemplateHtmlFromDeliveryOrder(
							generatorTemplate.HtmlValue,
							deliveryOrder
						)
					)
				)
			);
		} */

		List<ContentDocument> contentDocuments = [SELECT Id FROM ContentDocument WHERE Title IN :attachmentNames];

		List<ContentVersion> cvList = [
			SELECT Title, VersionData, FileExtension
			FROM ContentVersion
			WHERE ContentDocumentId IN :contentDocuments AND IsLatest = TRUE
		];

		for (ContentVersion cv : cvList) {
			emailAttachments.add(prepareEmailFileAttachment(cv.Title + '.' + cv.FileExtension, cv.VersionData));
		}

		return emailAttachments;
	}

	@TestVisible
	private static Messaging.EmailFileAttachment prepareEmailFileAttachment(String fileName, Blob content) {
		Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
		efa.setContentType('application/pdf');
		efa.setFilename(fileName);
		efa.setBody(content);

		return efa;
	}

	@TestVisible
	private static String formatServiceData(COM_Delivery_Order__c deliveryOrder) {
		String result = COM_Constants.SERVICES_TABLE_START;
		for (csord__Service__c serv : deliveryOrder.Services__r) {
			String deltaStatus = 'New';
			if (serv.csordtelcoa__Delta_Status__c == 'Continuing In Subscription') {
				deltaStatus = 'Continuing';
			} else if (serv.csordtelcoa__Delta_Status__c == 'Deleted From Subscription') {
				deltaStatus = 'Removed';
			}

			result +=
				COM_Constants.SERVICES_TABLE_ROW_START +
				serv.Name +
				COM_Constants.SERVICES_TABLE_ROW_MIDDLE +
				deltaStatus +
				COM_Constants.SERVICES_TABLE_ROW_END;
		}
		result += COM_Constants.SERVICES_TABLE_END;

		return result;
	}

	@TestVisible
	private static String filterServicesByMACDAction(COM_Delivery_Order__c deliveryOrder, String macdAction, Boolean includeNull) {
		String result = '';

		for (csord__Service__c service : deliveryOrder.Services__r) {
			result += COM_Constants.SERVICES_FILTERED_ROW_START + service.Name + COM_Constants.SERVICES_FILTERED_ROW_END;
		}

		return result;
	}

	//Fetch delivery order with all needed fields.
	public static COM_Delivery_Order__c fetchDeliveryOrder(Id deliveryOrderId) {
		return [
			SELECT
				Id,
				Name,
				Delivery_Order_Number__c,
				Technical_Contact__c,
				Technical_Contact__r.Email,
				Technical_Contact__r.Phone,
				Technical_Contact__r.Name,
				Technical_Contact__r.Language__c,
				Site__r.Name,
				Site__r.Site_Street__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_City__c,
				Installation_Start__c,
				Installation_End__c,
				Products__c,
				Delivery_Components__c,
				Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
				Termination_Date__c,
				MACD_Type__c,
				Account__c,
				Account__r.Name,
				(
					SELECT
						id,
						Name,
						Delivery_Components__c,
						Implemented_Date__c,
						Deprovisioned_Date__c,
						OSS10_Service_Id__c,
						csordtelcoa__Delta_Status__c
					FROM Services__r
				)
			FROM COM_Delivery_Order__c
			WHERE Id = :deliveryOrderId
			LIMIT 1
		];
	}

	@TestVisible
	private static csord__Service__c findL2Service(COM_Delivery_Order__c deliveryOrder) {
		csord__Service__c result = null;
		for (csord__Service__c service : deliveryOrder.Services__r) {
			if (service.Delivery_Components__c != null && service.Delivery_Components__c.contains('L2')) {
				result = service;
				break;
			}
		}

		return result;
	}

	@TestVisible
	private static DateTime getL2Date(csord__Service__c l2Service, String macdType) {
		DateTime dt = null;
		if (l2Service != null) {
			dt = l2Service.Implemented_Date__c;
			if (macdType == COM_Constants.MACD_TYPE_TERMINATION) {
				dt = l2Service.Deprovisioned_Date__c;
			}
		}

		return dt;
	}
}
