global without sharing class CS_CustomStepUpdateStatusCommercialCmps implements CSPOFA.ExecutionHandler{

    public List<SObject> process(List<SObject> steps) {
        List<SObject> result = new List<sObject>();
        List<csord__Service__c> servicesToUpdate = new List<csord__Service__c>();
        List<csord__Subscription__c> subscriptionsToUpdate = new List<csord__Subscription__c>();
        List<Suborder__c> solutionsToUpdate = new List<Suborder__c>();
        Map<Id, CSPOFA__Orchestration_Step__c> processStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
        Set<Id> solutionIds = new Set<Id>();
        List<CSPOFA__Orchestration_Step__c> stepList = [SELECT ID, Name,
                                                                CSPOFA__Orchestration_Process__c,
                                                                CSPOFA__Orchestration_Process__r.Suborder__c,
                                                                CSPOFA__Status__c,
                                                                CSPOFA__Completed_Date__c,
                                                                CSPOFA__Message__c,
                                                                COM_Status__c,
                                                                COM_Update_Services__c,
                                                                COM_Update_Subscriptions__c,
                                                                COM_Update_Solutions__c
                                                        FROM CSPOFA__Orchestration_Step__c
                                                        WHERE Id IN :steps];

        try {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                processStepMap.put(step.CSPOFA__Orchestration_Process__c, step);
                solutionIds.add(step.CSPOFA__Orchestration_Process__r.Suborder__c);
            }

            Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c>([
                                                                                SELECT id,
                                                                                        Suborder__c
                                                                                FROM CSPOFA__Orchestration_Process__c
                                                                                WHERE id in :processStepMap.keySet()
                                                                        ]);

            Map<Id,List<csord__Service__c>> solutionIdServices = getSolutionIdServicesListMap(solutionIds);
            Map<Id,List<csord__Subscription__c>> solutionIdSubscriptions = getSolutionIdSubscriptionsListMap(solutionIds);

            //process steps
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                if (step.COM_Update_Services__c && solutionIdServices.get(step.CSPOFA__Orchestration_Process__r.Suborder__c) != null){
                    for (csord__Service__c service : solutionIdServices.get(step.CSPOFA__Orchestration_Process__r.Suborder__c)){
                        servicesToUpdate.add(new csord__Service__c (Id = service.Id,
                        csord__Status__c = step.COM_Status__c));
                    }
                }

                if (step.COM_Update_Subscriptions__c && solutionIdSubscriptions.get(step.CSPOFA__Orchestration_Process__r.Suborder__c) != null){
                    for (csord__Subscription__c subscription : solutionIdSubscriptions.get(step.CSPOFA__Orchestration_Process__r.Suborder__c)){
                        subscriptionsToUpdate.add(new csord__Subscription__c (Id = subscription.Id,
                                csord__Status__c = step.COM_Status__c));
                    }
                }

                if (step.COM_Update_Solutions__c) {
                    solutionsToUpdate.add(new Suborder__c(Id = step.CSPOFA__Orchestration_Process__r.Suborder__c,
                            Status__c = step.COM_Status__c
                            ));
                }
            }

            if (!servicesToUpdate.isEmpty()) {
                update servicesToUpdate;
            }

            if (!subscriptionsToUpdate.isEmpty()) {
                update subscriptionsToUpdate;
            }

            if (!solutionsToUpdate.isEmpty()) {
                update solutionsToUpdate;
            }

            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_COMPLETE;
                step.CSPOFA__Completed_Date__c = Date.today();
                step.CSPOFA__Message__c = 'Custom step succeeded';
                result.add(step);
            }
        } catch (Exception ex) {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
                step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
                if(step.CSPOFA__Message__c.length() > 255 ){
                    step.CSPOFA__Message__c = step.CSPOFA__Message__c.substring(0, 254);
                }
                result.add(step);
            }
        }

        return result;
    }

    private Map<Id,List<csord__Subscription__c>> getSolutionIdSubscriptionsListMap(Set<Id> solutionIds) {
        Map<Id,List<csord__Subscription__c>> returnValue = new Map<Id,List<csord__Subscription__c>>();
        List<csord__Subscription__c> subscriptions = [SELECT Id, Suborder__c
                                                        FROM csord__Subscription__c
                                                        WHERE Suborder__c IN :solutionIds];

        for (csord__Subscription__c subscription : subscriptions) {
            if (returnValue.get(subscription.Suborder__c) == null) {
                returnValue.put(subscription.Suborder__c,new List<csord__Subscription__c>{subscription});
            } else {
                returnValue.get(subscription.Suborder__c).add(subscription);
            }
        }

        return returnValue;
    }

    private Map<Id,List<csord__Service__c>> getSolutionIdServicesListMap(Set<Id> solutionIds) {
        Map<Id,List<csord__Service__c>> returnValue = new Map<Id,List<csord__Service__c>>();

        List<csord__Service__c> services = [SELECT Id, Suborder__c, csord__Service__c, csord__Service__r.Suborder__c
                                            FROM csord__Service__c
                                            WHERE Suborder__c IN :solutionIds
                                            OR csord__Service__r.Suborder__c IN :solutionIds];

        for (csord__Service__c service : services) {
            if (service.csord__Service__c == null) {
                if (returnValue.get(service.Suborder__c) == null) {
                    returnValue.put(service.Suborder__c,new List<csord__Service__c>{service});
                } else {
                    returnValue.get(service.Suborder__c).add(service);
                }
            } else if (service.csord__Service__c != null) {
                if (returnValue.get(service.csord__Service__r.Suborder__c) == null) {
                    returnValue.put(service.csord__Service__r.Suborder__c ,new List<csord__Service__c>{service});
                } else {
                    returnValue.get(service.csord__Service__r.Suborder__c).add(service);
                }
            }
        }

        return returnValue;
    }
}