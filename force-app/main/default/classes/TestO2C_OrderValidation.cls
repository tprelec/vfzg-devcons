@IsTest
public class TestO2C_OrderValidation {
    @TestSetup
    static void makeData() {
        User owner = TestUtils.createAdministrator();
        Account account = TestUtils.createAccount(owner);
        Contact contact = TestUtils.createContact(account);
        Ban__c ban = TestUtils.createBan(account);
        Financial_Account__c fa = TestUtils.createFinancialAccount(
            ban,
            contact.Id
        );

        Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);

        Opportunity opportunity = TestUtils.createOpportunity(
            account,
            Test.getStandardPricebookId()
        );
        opportunity.RecordTypeId = GeneralUtils.recordTypeMap.get('Opportunity')
            .get('Default');
        update opportunity;
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
            cscfga__Opportunity__c = opportunity.id,
            csbb__Account__c = account.Id
        );
        insert basket;

        TestUtils.createOrderValidationBilling();
        TestUtils.createOrderValidations();
    }

    @IsTest
    private static void testNoRecordId() {
        Test.startTest();

        LightningResponse response = O2C_OrderValidation.getResult(null);
        System.assertNotEquals(null, response.message);
        System.assertEquals(
            LightningResponse.getErrorVariant(),
            response.variant
        );
        Test.stopTest();
    }

    @IsTest
    private static void testIncorrectRecordId() {
        Account account = [SELECT Id FROM Account LIMIT 1];
        Test.startTest();

        LightningResponse response = O2C_OrderValidation.getResult(account.Id);
        System.assertNotEquals(null, response.message);
        System.assertEquals(
            LightningResponse.getErrorVariant(),
            response.variant
        );
        Test.stopTest();
    }

    @IsTest
    private static void testBanValidation() {
        Ban__c ban = [SELECT Id FROM Ban__c LIMIT 1];
        Test.startTest();

        LightningResponse response = O2C_OrderValidation.getResult(ban.Id);

        System.assertNotEquals(null, response);
        System.assertEquals(null, response.message);
        System.assertEquals(null, response.variant);
        System.assertNotEquals(null, response.body);

        List<String> allErrors = (List<String>) response.body;
        System.assertNotEquals(0, allErrors.size());

        Test.stopTest();
    }

    @IsTest
    private static void testFinancialAccountValidation() {
        Financial_Account__c fa = [SELECT Id FROM Financial_Account__c LIMIT 1];
        Test.startTest();
        LightningResponse response = O2C_OrderValidation.getResult(fa.Id);

        System.assertNotEquals(null, response);
        System.assertEquals(null, response.message);
        System.assertEquals(null, response.variant);
        System.assertNotEquals(null, response.body);

        List<String> allErrors = (List<String>) response.body;
        System.assertNotEquals(0, allErrors.size());
        Test.stopTest();
    }

    @IsTest
    private static void testBillingArrangementValidation() {
        Billing_Arrangement__c bar = [
            SELECT Id
            FROM Billing_Arrangement__c
            LIMIT 1
        ];
        Test.startTest();
        LightningResponse response = O2C_OrderValidation.getResult(bar.Id);

        System.assertNotEquals(null, response);
        System.assertEquals(null, response.message);
        System.assertEquals(null, response.variant);
        System.assertNotEquals(null, response.body);

        List<String> allErrors = (List<String>) response.body;
        System.assertNotEquals(0, allErrors.size());
        Test.stopTest();
    }
}