@SuppressWarnings('PMD.StdCyclomaticComplexity,PMD.CyclomaticComplexity')
public with sharing class NetProfitInformationTriggerHandler extends TriggerHandler {
	private List<NetProfit_Information__c> newNPIs;
	private Map<Id, NetProfit_Information__c> newNPIMap;
	private List<NetProfit_Information__c> oldNPIs;
	private Map<Id, NetProfit_Information__c> oldNPIMap;

	private List<Contracted_Products__c> contractedProducts;
	private Map<Id, List<Contracted_Products__c>> contractedProductsByOrder;
	private Map<Id, Order__c> orders;
	private Map<Id, Opportunity> opportunities;
	private Map<Id, Dealer_Information__c> dealerCode;
	private Map<Id, NetProfit_Information__c> npisByOrder;

	// Static variables for the dummy number assignment so they also work for batches that spread across trigger batch size.
	private static Integer voiceNumberCounter = 0;
	private static Integer dataNumberCounter = 0;

	private static final Set<String> CTN_VOICE_PRODUCT_FAMILIES = new Set<String>{
		'Acq Voice',
		'Ret Voice',
		'Mig Voice',
		'Voice',
		'HHBD',
		'Acq HHBD',
		'Ret HHBD'
	};

	private static final Set<String> CTN_DATA_PRODUCT_FAMILIES = new Set<String>{ 'MBB', 'Acq MBB', 'Mig MBB', 'Ret MBB' };

	private static final String PRODUCT_GROUP_PRICEPLAN = 'Priceplan';
	private static final String PRODUCT_GROUP_ADDONS = 'Addons';
	private static final String PRODUCT_GROUP_CONNECTIONCOST = 'ConnectionCosts';

	private static final Set<String> ALLOWED_PRODUCT_GROUPS = new Set<String>{
		PRODUCT_GROUP_ADDONS,
		PRODUCT_GROUP_PRICEPLAN,
		PRODUCT_GROUP_CONNECTIONCOST
	};

	private static final Set<String> ALLOWED_AANSLUITKOSTEN_PRICE_PLANS = new Set<String>{ 'Aansluitkosten' };

	private static final Set<String> ALLOWED_ADDON_CONTRACTED_PRODUCTS = new Set<String>{ 'COMPANYLEVELSERVICES' };

	private static final Map<String, String> DEAL_TYPE_TO_CTN_ACTION = new Map<String, String>{
		'New' => 'New',
		'Retention' => 'Retention',
		'Porting' => 'Porting',
		'New and Porting' => 'Migration',
		'Migration' => 'Migration'
	};

	private static final Map<String, String> DEAL_TYPE_TO_CTN_STATUS = new Map<String, String>{
		'New' => 'Future',
		'Retention' => 'Future',
		'Porting' => 'Future',
		'New and Porting' => 'Future',
		'Migration' => 'Active'
	};

	private static final String CONNECTION_TYPE_DATA = 'Data';
	private static final String CONNECTION_TYPE_VOICE = 'Voice';
	private static final String CONNECTION_TYPE_VOICE_DATA = 'Voice/Data';

	private void init() {
		newNPIs = (List<NetProfit_Information__c>) this.newList;
		newNPIMap = (Map<Id, NetProfit_Information__c>) this.newMap;
		oldNPIs = (List<NetProfit_Information__c>) this.oldList;
		oldNPIMap = (Map<Id, NetProfit_Information__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
	}

	public override void afterInsert() {
		init();
		generateCTNInfo();
		if (!System.isFuture() && !System.isBatch()) {
			updateNPIStatus(newNPIMap.keySet());
		}
	}

	public override void beforeUpdate() {
		init();
	}

	/**
	 * Creates new CTN record
	 * @param  npiID          NPI ID
	 * @param  conProd        Contracted Product
	 * @param  dummyCtnNumber Dummy CTN Number
	 * @return                CTN record
	 */
	private NetProfit_CTN__c newCTN(Id npiID, Contracted_Products__c conProd, String dummyCtnNumber) {
		NetProfit_CTN__c newCtn = new NetProfit_CTN__c(
			NetProfit_Information__c = npiID,
			CTN_Number__c = String.isBlank(conProd.CTN_number__c) ? dummyCtnNumber : conProd.CTN_number__c,
			CTN_Status__c = DEAL_TYPE_TO_CTN_STATUS.get(conProd.Deal_Type__c),
			Action__c = DEAL_TYPE_TO_CTN_ACTION.get(conProd.Deal_Type__c),
			Price_Plan_Class__c = getConnectionType(conProd),
			Duration__c = conProd.Duration__c,
			MAF__c = conProd.Gross_List_Price__c,
			Discount__c = conProd.vMAFDiscountExtraPromo__c,
			Commission_Regular__c = conProd.Allowed_SAC_Per_CTN__c,
			Commission_Total__c = conProd.Total_Dealer_Bonus_per_CTN__c,
			Quote_Profile__c = String.valueOf(conProd.Model_Number__c),
			Product_Group__c = conProd.Product__r.Product_Group__c,
			Product_Quantity_type__c = conProd.Product__r.Quantity_type__c,
			Order__c = conProd.Order__c,
			Contracted_Product__c = conProd.id,
			Assigned_Product_Id__c = conProd.Assigned_Product_Id__c,
			Framework_Agreement_Id__c = conProd.Framework_Id__c,
			Template_Id__c = conProd.Unify_Template_Id__c,
			Resource_Category__c = conProd.Resource_Category__c
		);
		if (String.isNotBlank(conProd.Unify_Template_Id__c) || String.isNotBlank(conProd.Assigned_Product_Id__c)) {
			newCtn.Price_Plan_Description__c = conProd.Line_Description__c;
		} else {
			newCtn.Price_Plan_Description__c = conProd.Product_Name__c;
		}
		if (String.isNotBlank(conProd.Unify_Template_Id__c)) {
			newCtn.Price_Plan_Code__c = conProd.Unify_Template_Id__c;
		} else {
			//newCtn.Price_Plan_Code__c = conProd.Product__r.Gemini_Code__c;
			newCtn.Price_Plan_Code__c = conProd.Product__r.ProductCode;
		}
		return newCtn;
	}

	private List<Contracted_Products__c> getContractedProducts() {
		if (contractedProducts != null) {
			return contractedProducts;
		}
		Set<Id> orderIds = GeneralUtils.getIDSetFromList(newNPIs, 'Order__c');
		List<Contracted_Products__c> contractedProducts = [
			SELECT
				Id,
				Order__c,
				Proposition_Type__c,
				Product_Name__c,
				ProductCode__c,
				Duration__c,
				Group__c,
				Product__r.ProductCode,
				Product__r.Product_Group__c,
				Product__r.Marketing_Description__c,
				Product__r.Description,
				Product__r.Gemini_Code__c,
				Product__r.Quantity_type__c,
				vMAFDiscountExtraPromo__c,
				Allowed_SAC_Per_CTN__c,
				Total_Dealer_Bonus_per_CTN__c,
				Product__r.BAP_SAP__c,
				Gross_List_Price__c,
				Product_Family__c,
				Quantity__c,
				Model_Number__c,
				Deal_Type__c,
				VF_Contract__r.Dummy_Contract__c,
				Unify_Template_Id__c,
				Framework_Id__c,
				Assigned_Product_Id__c,
				Line_Description__c,
				CTN_number__c,
				Resource_Category__c,
				Price_Plan_Class__c
			FROM Contracted_Products__c
			WHERE Order__c IN :orderIds AND Order__c != NULL
		];
		contractedProductsByOrder = GeneralUtils.groupByIDField(contractedProducts, 'Order__c');
		return contractedProducts;
	}

	private Map<Id, Order__c> getOrders() {
		if (orders != null) {
			return orders;
		}
		Set<Id> orderIds = GeneralUtils.getIDSetFromList(newNPIs, 'Order__c');
		orders = new Map<Id, Order__c>(
			[
				SELECT
					Id,
					Account__c,
					VF_Contract__r.Opportunity__r.BAN__c,
					VF_Contract__r.Opportunity__r.Owner.Contact.Account.Name,
					VF_Contract__r.Opportunity__r.Opportunity_Discounts__c,
					VF_Contract__r.Opportunity__r.Opportunity_FUP_Amount__c,
					VF_Contract__r.Opportunity__r.Primary_Quote__c,
					VF_Contract__r.Opportunity__c
				FROM Order__c
				WHERE Id IN :orderIds
			]
		);
		return orders;
	}

	private Map<Id, Opportunity> getOpportunities() {
		if (opportunities != null) {
			return opportunities;
		}
		Set<Id> oppIds = GeneralUtils.getIDSetFromList(newNPIs, 'Opportunity__c');
		opportunities = new Map<Id, Opportunity>(
			[
				SELECT Id, AccountId, BAN__c, Owner.Contact.Account.Name, Opportunity_Discounts__c, Opportunity_FUP_Amount__c, Primary_Quote__c
				FROM Opportunity
				WHERE Id IN :oppIds
			]
		);
		return opportunities;
	}

	/**
	 * Builds Map of Order IDs and related NPI records
	 */
	private void buildNPIsByOrderMap() {
		npisByOrder = new Map<Id, NetProfit_Information__c>();
		for (NetProfit_Information__c npi : newNPIs) {
			if (npi.Order__c != null) {
				npisByOrder.put(npi.Order__c, npi);
			}
		}
	}

	/**
	 * Checks if Contracted Product is Void Product
	 * @param  conProd Contracted Product
	 * @return         True if it is Voice Product, false otherwise
	 */
	private Boolean isVoiceProduct(Contracted_Products__c conProd) {
		return conProd != null && CTN_VOICE_PRODUCT_FAMILIES.contains(conProd.Product_Family__c);
	}

	/**
	 * Checks if Contracted Product is Data Product
	 * @param  conProd Contracted Product
	 * @return         True if it is Data Product, false otherwise
	 */
	private Boolean isDataProduct(Contracted_Products__c conProd) {
		return conProd != null && CTN_DATA_PRODUCT_FAMILIES.contains(conProd.Product_Family__c);
	}

	/**
	 * Gets Connection Type for provided Contracted Product.
	 * @param  conProd Contracted Product.
	 * @return         Connection Type if it is found, null otherwise.
	 */
	private String getConnectionType(Contracted_Products__c conProd) {
		String connType;
		if (conProd.Price_Plan_Class__c == CONNECTION_TYPE_VOICE_DATA) {
			for (Contracted_Products__c cp2 : contractedProductsByOrder.get(conProd.Order__c)) {
				if (cp2.Model_Number__c == conProd.Model_Number__c && cp2.Product__r.Product_Group__c == PRODUCT_GROUP_PRICEPLAN) {
					connType = getConnectionTypeBasedOnProduct(cp2);
					if (connType != null) {
						break;
					}
				}
			}
		} else {
			connType = getConnectionTypeBasedOnProduct(conProd);
		}
		return connType;
	}

	/**
	 * Gets Connection Type for provided Contracted Product based on Product.
	 * @param  conProd Contracted Product.
	 * @return         Connection Type if it is found, null otherwise.
	 */
	private String getConnectionTypeBasedOnProduct(Contracted_Products__c conProd) {
		String connType;
		if (isVoiceProduct(conProd)) {
			connType = CONNECTION_TYPE_VOICE;
		} else if (isDataProduct(conProd)) {
			connType = CONNECTION_TYPE_DATA;
		}
		return connType;
	}

	/**
	 * Gets Product Group for selected Contracted Product
	 * @param  conProd Contracted Product
	 * @return         Product Group
	 */
	private String getProductGroup(Contracted_Products__c conProd) {
		String productGroup = conProd.Product__r.Product_Group__c;
		// Update the connection cost productgroup (only in-memory)
		// We are not updating the CP data but the product2 data
		for (String aansluitkostenPriceplan : ALLOWED_AANSLUITKOSTEN_PRICE_PLANS) {
			if (conProd.Product_Name__c.contains(aansluitkostenPriceplan)) {
				productGroup = PRODUCT_GROUP_CONNECTIONCOST;
				conProd.Product__r.Product_Group__c = PRODUCT_GROUP_CONNECTIONCOST;
			}
		}
		return productGroup;
	}

	/**
	 * Gropus Contracted Products by Order, Model Number, Connection Type, Deal Type and Product Group
	 * @return   Grouped Contracted Products
	 */
	private Map<Id, Map<Decimal, Map<String, Map<String, Map<String, List<Contracted_Products__c>>>>>> groupContractedProducts() {
		Map<Id, Map<Decimal, Map<String, Map<String, Map<String, List<Contracted_Products__c>>>>>> cpsByOrderId = new Map<Id, Map<Decimal, Map<String, Map<String, Map<String, List<Contracted_Products__c>>>>>>();
		for (Contracted_Products__c cp : getContractedProducts()) {
			// Contracted Product Group must be in the allowed set
			if (!ALLOWED_PRODUCT_GROUPS.contains(cp.Product__r.Product_Group__c)) {
				continue;
			}
			// Get Product Group & Connection Type
			String productGroup = getProductGroup(cp);
			String connectionType = getConnectionType(cp);
			if (String.isBlank(connectionType) && cp.Order__c != null) {
				NetProfit_Information__c netProfitInformationToAddError = npisByOrder.get(cp.Order__c);
				netProfitInformationToAddError.addError(
					'Invalid connection type found for order ' +
					cp.Order__c +
					'. Expecting voice or data. Please contact your administrator.'
				);
				continue;
			}
			// Contracted Products by Order
			if (!cpsByOrderId.containsKey(cp.Order__c)) {
				cpsByOrderId.put(cp.Order__c, new Map<Decimal, Map<String, Map<String, Map<String, List<Contracted_Products__c>>>>>());
			}
			// Contracted Products by Model Number
			Map<Decimal, Map<String, Map<String, Map<String, List<Contracted_Products__c>>>>> cpsByModelNumber = cpsByOrderId.get(cp.Order__c);
			if (!cpsByModelNumber.containsKey(cp.Model_Number__c)) {
				cpsByModelNumber.put(cp.Model_Number__c, new Map<String, Map<String, Map<String, List<Contracted_Products__c>>>>());
			}
			// Contracted Products by Connection Type
			Map<String, Map<String, Map<String, List<Contracted_Products__c>>>> cpsByConnectionType = cpsByModelNumber.get(cp.Model_Number__c);
			if (!cpsByConnectionType.containsKey(connectionType)) {
				cpsByConnectionType.put(connectionType, new Map<String, Map<String, List<Contracted_Products__c>>>());
			}
			// Contracted Products by Deal Type
			Map<String, Map<String, List<Contracted_Products__c>>> cpsByDealType = cpsByConnectionType.get(connectionType);
			if (!cpsByDealType.containsKey(cp.Deal_Type__c)) {
				cpsByDealType.put(cp.Deal_Type__c, new Map<String, List<Contracted_Products__c>>());
			}
			// Contracted Products by Product Group
			Map<String, List<Contracted_Products__c>> cpsByProductGroup = cpsByDealType.get(cp.Deal_Type__c);
			if (!cpsByProductGroup.containsKey(productGroup)) {
				cpsByProductGroup.put(productGroup, new List<Contracted_Products__c>());
			}
			cpsByProductGroup.get(productGroup).add(cp);
		}
		return cpsByOrderId;
	}

	/**
	 * Checks if it has company level Product Groups.
	 * @param  cpsByProductGroup Contracted Products grouped by Product Group
	 * @return   True if company level Product Groups are found, false otherwise.
	 */
	private Boolean checkCompanyLevelServices(Map<String, List<Contracted_Products__c>> cpsByProductGroup) {
		Boolean companyLevelServicesFound = false;
		for (String productGroup : cpsByProductGroup.keySet()) {
			List<Contracted_Products__c> cpTmpList = cpsByProductGroup.get(productGroup);
			for (Contracted_Products__c cp : cpTmpList) {
				if (ALLOWED_ADDON_CONTRACTED_PRODUCTS.contains(cp.Proposition_Type__c)) {
					companyLevelServicesFound = true;
				}
			}
		}
		return companyLevelServicesFound;
	}

	/**
	 * Calculates Quantity for Contracted Product
	 * @param  cpsByProductGroup         Contracted Products grouped by Product Group
	 * @param  isCompanyLevelService     Defines if it is company level product
	 * @return                           Quantity
	 */
	private Integer getQuantity(Map<String, List<Contracted_Products__c>> cpsByProductGroup, Boolean companyLevelServicesFound) {
		Integer quantity = 1;
		if (!companyLevelServicesFound) {
			quantity = (Integer) cpsByProductGroup.get(PRODUCT_GROUP_PRICEPLAN)[0].Quantity__c;
		}
		return quantity;
	}

	/**
	 * Calculates Dummy CTN Number
	 * 316xxx[ModelNumber][quantity_processed] for voice
	 * 3197xx[ModelNumber][quantity_processed] for data
	 * @param  connectionType Connection Type
	 * @param  modelNumber    Model Number
	 * @return                Dummy CTN NUmber
	 */
	private String getDummyCtnNumber(String connectionType, Decimal modelNumber) {
		String dummyCtnNumber = '';
		if (connectionType == CONNECTION_TYPE_VOICE) {
			voiceNumberCounter += 1;
			dummyCtnNumber = '316XXX' + modelNumber + String.ValueOf(voiceNumberCounter).leftPad(4, '0');
		} else if (connectionType == CONNECTION_TYPE_DATA) {
			dataNumberCounter += 1;
			dummyCtnNumber = '3197XX' + modelNumber + String.ValueOf(dataNumberCounter).leftPad(4, '0');
		}
		return dummyCtnNumber;
	}

	/**
	 * Creates CTN records for inserted NPIs
	 */
	private void generateCTNInfo() {
		buildNPIsByOrderMap();
		Map<Id, Map<Decimal, Map<String, Map<String, Map<String, List<Contracted_Products__c>>>>>> cpMap = groupContractedProducts();
		// Go through all groups in the map, creating ctn lineitems and assigning ctn numbers
		List<NetProfit_CTN__c> ctnsToInsert = new List<NetProfit_CTN__c>();
		for (Id orderId : cpMap.keySet()) {
			if (orderId != null) {
				Map<Decimal, Map<String, Map<String, Map<String, List<Contracted_Products__c>>>>> cpsByModelNumber = cpMap.get(orderId);
				for (Decimal modelNumber : cpsByModelNumber.keySet()) {
					Map<String, Map<String, Map<String, List<Contracted_Products__c>>>> cpsByConnectionType = cpsByModelNumber.get(modelNumber);
					for (String connectionType : cpsByConnectionType.keySet()) {
						Map<String, Map<String, List<Contracted_Products__c>>> cpsByDealType = cpsByConnectionType.get(connectionType);
						for (String dealType : cpsByDealType.keySet()) {
							Map<String, List<Contracted_Products__c>> cpsByProductGroup = cpsByDealType.get(dealType);
							Boolean companyLevelServicesFound = checkCompanyLevelServices(cpsByProductGroup);
							if (
								!cpsByProductGroup.containsKey(PRODUCT_GROUP_PRICEPLAN) &&
								!companyLevelServicesFound &&
								npisByOrder.get(orderId).order__c != null
							) {
								NetProfit_Information__c netProfitInformationToAddError = npisByOrder.get(orderId);
								netProfitInformationToAddError.addError(
									'Missing priceplan for order ' +
									orderId +
									' modelnumber ' +
									modelNumber +
									' connectionType ' +
									connectionType +
									' dealType ' +
									dealType +
									'. Please contact your administrator.'
								);
								return;
							}
							Integer quantity = getQuantity(cpsByProductGroup, companyLevelServicesFound);

							for (Integer i = 0; i < quantity; i++) {
								// Assign a dummy CTN phone number to each item in the new set of CTNs. Each item should get the same number.
								String dummyCtnNumber = getDummyCtnNumber(connectionType, modelNumber);
								for (String productGroup : cpsByProductGroup.keySet()) {
									// Each group (exccept addons) should have only 1 entry.
									if (productGroup != PRODUCT_GROUP_ADDONS && cpsByProductGroup.get(productGroup).size() > 1) {
										NetProfit_Information__c netProfitInformationToAddError = npisByOrder.get(orderId);
										netProfitInformationToAddError.addError(
											'Amount of ' +
											productGroup +
											' contractedproducts not correct for group ' +
											modelNumber +
											' on order ' +
											orderId +
											'. Please contact your administrator.'
										);
										return;
									}
									for (Contracted_Products__c cp : cpsByProductGroup.get(productGroup)) {
										// All quantities in this subgroup equal to the group-level quantity?
										if (cp.Quantity__c != quantity && !companyLevelServicesFound) {
											// error! quantities do not match
											NetProfit_Information__c netProfitInformationToAddError = npisByOrder.get(orderId);
											netProfitInformationToAddError.addError(
												'CTN/addon quantities do not match for order ' +
												orderId +
												'. Please contact your administrator.'
											);
											return;
										} else {
											if (ALLOWED_ADDON_CONTRACTED_PRODUCTS.contains(cp.Proposition_Type__c)) {
												dummyCtnNumber = '';
											}

											// If no errors, create a new CTN Information record for each item in the group.
											ctnsToInsert.add(newCTN(npisByOrder.get(orderId).Id, cp, dummyCtnNumber));
										}
									}
								}
							}
						}
					}
				}
			}
		}
		insert ctnsToInsert;
	}

	// When the NetProfit_Information__c record is created the status is NULL
	// This allows the CTN's to be created without triggering the validation rules (they check for status NULL)
	// Here we need to update the status to New so that from now on the validation rules do fire
	@future
	private static void updateNPIStatus(Set<Id> newNPIIds) {
		List<NetProfit_Information__c> npiToUpdate = new List<NetProfit_Information__c>();
		for (NetProfit_Information__c npi : [SELECT Id FROM NetProfit_Information__c WHERE Id IN :newNPIIds]) {
			npi.Status__c = 'New';
			npiToUpdate.add(npi);
		}
		update npiToUpdate;
	}
}
