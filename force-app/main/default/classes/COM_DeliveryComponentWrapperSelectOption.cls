public with sharing class COM_DeliveryComponentWrapperSelectOption {
    public COM_DeliveryComponentWrapperSelectOption(){

    }

    @AuraEnabled
    public String label;

    @AuraEnabled
    public String value;
}