global class OppContractSharingOneTimeScript implements Database.Batchable<sObject>, Database.AllowsCallouts {

    String query = 'SELECT Id, UserOrGroupId, OpportunityId FROM OpportunityShare Where RowCause = \'Manual\'';

    /************************************************************
    *  constructor
    ************************************************************/
    global OppContractSharingOneTimeScript() {
    }


    /****************************************************************
    *  start(Database.BatchableContext BC)
    *****************************************************************/
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(this.query);
    }

    /*********************************************************************
    *  execute(Database.BatchableContext BC, List scope)
    **********************************************************************/
    global void execute(Database.BatchableContext BC, List<OpportunityShare> oppShareList) {
        Map<Id, Id> userMap = new Map<Id, Id>();
        Map<Id, Boolean> oppMap = new Map<Id, Boolean>();
        Map<Id, Boolean> userPartnerMap = new Map<Id, Boolean>();
        List<OpportunityShare> oppDeleteShare = new List<OpportunityShare>();
        for (OpportunityShare oppShare : oppShareList) {
            userMap.put(oppShare.UserOrGroupId, oppShare.UserOrGroupId);
            oppMap.put(oppShare.OpportunityId, null);
        }
        List<Opportunity> oppList = [Select Id, Owner.Partner_User__c From Opportunity Where Id IN : oppMap.keySet()];
        for (Opportunity opp : oppList) {
            oppMap.put(opp.Id, opp.Owner.Partner_User__c);
        }
        List<User> userList = [Select Id, Partner_User__c From User Where Id IN : userMap.keySet()];
        for (User usr: userList) {
            userPartnerMap.put(usr.Id, usr.Partner_User__c);
        }
        for (OpportunityShare oppShare : oppShareList) {
            if (userPartnerMap.containsKey(oppShare.UserOrGroupId)) {
                if (userPartnerMap.get(oppShare.UserOrGroupId)) {
                    if (oppMap.containsKey(oppShare.OpportunityId)) {
                        if (!oppMap.get(oppShare.OpportunityId)) {
                            oppDeleteShare.add(oppShare);
                        }
                    }

                }
            }
        }
        if (oppDeleteShare.size() > 0) {
            delete oppDeleteShare;
        }
    }

    /****************************************************
    *  finish(Database.BatchableContext BC)
    *****************************************************/
    global void finish(Database.BatchableContext BC) {

    }


}