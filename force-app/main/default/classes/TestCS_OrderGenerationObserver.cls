@isTest
private class TestCS_OrderGenerationObserver {
	@isTest
	private static void testExecute() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;
		System.runAs(simpleUser) {
			CS_OrderGenerationObserver orderGeneration = new CS_OrderGenerationObserver();

			Framework__c frameworkSetting = new Framework__c();
			frameworkSetting.Framework_Sequence_Number__c = 2;
			insert frameworkSetting;

			PriceReset__c priceResetSetting = new PriceReset__c();
			priceResetSetting.MaxRecurringPrice__c = 200.00;
			priceResetSetting.ConfigurationName__c = 'IP Pin';

			insert priceResetSetting;
			Account testAccount = CS_DataTest.createAccount('Test Account');
			insert testAccount;

			Sales_Settings__c ssettings = new Sales_Settings__c();
			ssettings.Postalcode_check_validity_days__c = 2;
			ssettings.Max_Daily_Postalcode_Checks__c = 2;
			ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
			ssettings.Postalcode_check_block_period_days__c = 2;
			ssettings.Max_weekly_postalcode_checks__c = 15;
			insert ssettings;

			Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', simpleUser.id);
			insert testOpp;

			csord__Order_Request__c coreq = new csord__Order_Request__c(csord__Module_Name__c = 'Test', csord__Module_Version__c = '1.0');
			insert coreq;

			csord__Order__c order = new csord__Order__c(
				Name = 'Test Order',
				csord__Account__c = testAccount.Id,
				csord__Status2__c = 'Order Submitted',
				csord__Order_Request__c = coreq.Id,
				csord__Identification__c = 'DWHTestBatchOn_' + system.now(),
				csordtelcoa__Opportunity__c = testOpp.Id
			);
			insert order;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
			basket.csbb__Synchronised_With_Opportunity__c = true;
			insert basket;

			csord__Solution__c solution = CS_DataTest.createSolution('BIMO');
			solution.cssdm__product_basket__c = basket.Id;
			solution.csord__Identification__c = 'SlovojedPapaKnjigu';
			insert solution;

			Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
				.get('Product Definition')
				.getRecordTypeId();

			cscfga__Product_Definition__c accessDef = CS_DataTest.createProductDefinition(CS_ConstantsCOM.PRODUCT_DEFINITION_ACCESS);
			accessDef.RecordTypeId = productDefinitionRecordType;
			accessDef.Product_Type__c = 'Fixed';
			insert accessDef;

			cscfga__Product_Definition__c businessInternetDef = CS_DataTest.createProductDefinition(
				CS_ConstantsCOM.PRODUCT_DEFINITION_BUSINESS_INTERNET
			);
			businessInternetDef.RecordTypeId = productDefinitionRecordType;
			businessInternetDef.Product_Type__c = 'Fixed';
			insert businessInternetDef;

			cscfga__Product_Configuration__c accessConf = CS_DataTest.createProductConfiguration(
				accessDef.Id,
				CS_ConstantsCOM.PRODUCT_DEFINITION_ACCESS,
				basket.Id
			);
			accessConf.cssdm__solution_association__c = solution.Id;
			insert accessConf;

			cscfga__Product_Configuration__c biConf = CS_DataTest.createProductConfiguration(
				businessInternetDef.Id,
				CS_ConstantsCOM.PRODUCT_DEFINITION_BUSINESS_INTERNET,
				basket.Id
			);
			biConf.cssdm__solution_association__c = solution.Id;
			insert biConf;

			csord__Subscription__c subscription = CS_DataTest.createSubscription(accessConf.Id);
			subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
			subscription.csord__Order__c = order.Id;
			insert subscription;

			csord__Service__c service = CS_DataTest.createService(accessConf.Id, subscription);
			service.csordtelcoa__Product_Configuration__c = accessConf.Id;
			service.csord__Identification__c = 'testService_123';
			service.csord__Subscription__c = subscription.Id;
			service.csord__Status__c = 'Test';
			service.csord__Order__c = order.Id;
			insert service;

			List<Id> serviceIds = new List<Id>();
			serviceIds.add(subscription.Id);

			csordtelcoa.OrderGenerationObservable observable = new csordtelcoa.OrderGenerationObservable(
				'',
				null,
				new List<Id>{ order.Id },
				new List<Id>{ subscription.Id },
				new List<Id>{ service.Id },
				null,
				null,
				null,
				null,
				null
			);
			orderGeneration.execute(observable, null);
		}

		List<csord__Order__c> ords = [SELECT Id, Name, csord__Status2__c FROM csord__Order__c];

		List<csord__Service__c> services = [
			SELECT Id, csord__Status__c, csord__Identification__c, csord__Subscription__c, csordtelcoa__Product_Configuration__c, csord__Order__c
			FROM csord__Service__c
		];

		System.assertEquals(services[0].csord__Order__c, ords[0].Id, 'Service should contain order Id.');
	}
}
