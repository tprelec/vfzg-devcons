/**
 * @description			Reusable class for doing things in batch when needed
 *						Replace query and logic and deploy to production, execute it by doing this in execute anonymous:
 *						Id batchJobId = Database.executeBatch(new UpdateStuffInBatch(), 500);
 * @author				Stjepan Pavuna
 */
global class UpdateStuffInBatch implements Database.Batchable <sObject>{
 
    global Database.QueryLocator start(Database.BatchableContext BC){
		//make your query here
        //return Database.getQueryLocator('select id, name, recordTypeId, Type, BIK_Code__c, Soho_Vertical__c from account where recordTypeId = \'012D0000000QPRa\' and (Type = \'Prospect\' OR Type = \'Customer\') and BIK_Code__c != null and Soho_Vertical__c = null');
        //return Database.getQueryLocator('select id, Bank_Account_Name__c, Bank_Account_Number__c, Payment_method__c, Financial_Account__r.Bank_Account_Name__c, Financial_Account__r.Bank_Account_Number__c, Financial_Account__r.Payment_method__c from Billing_Arrangement__c where Payment_method__c = null');
        //return Database.getQueryLocator('Select Id, Account__r.OwnerId, OwnerId from Ban__c Where temp__c = false ');
        
        return Database.getQueryLocator('Select Id, CLC__c,PriceBookEntry.Product2.CLC__c from OpportunityLineItem Where CLC__c = null');
        //return Database.getQueryLocator('Select Id, CLC__c,Product__r.CLC__c from Contracted_Products__c Where CLC__c = null ');
        
    } 
 
    global void execute(Database.BatchableContext BC, List<OpportunityLineItem> scope){
    //global void execute(Database.BatchableContext BC, List<Contracted_Products__c> scope){
    	// do your business logic here
    	
		for(OpportunityLineItem ba : scope){
            if(ba.PriceBookEntry.Product2.CLC__c != null)
                ba.CLC__c = ba.PriceBookEntry.Product2.CLC__c;
            else
                ba.CLC__c = 'Acq';
            
		}
/*
      for(Contracted_Products__c ba : scope){
            if(ba.Product__r.CLC__c != null)
                ba.CLC__c = ba.Product__r.CLC__c;
            else
                ba.CLC__c = 'Acq';
            
        }
*/        
    	Database.update(scope,false);    	
    	
    }
 
    global void finish(Database.BatchableContext BC){
    }
 
}