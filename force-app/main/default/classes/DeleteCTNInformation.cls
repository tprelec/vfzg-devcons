public without sharing class DeleteCTNInformation {
	public static Boolean deleteCtnInformation(Id basketId) {
		cscfga__Product_Basket__c theBasket = [
			SELECT Id, cscfga__Opportunity__c
			FROM cscfga__Product_Basket__c
			WHERE Id = :basketId
		];
		List<NetProfit_CTN__c> ctnsToDelete = [
			SELECT Id
			FROM NetProfit_CTN__c
			WHERE NetProfit_Information__r.Opportunity__c = :theBasket.cscfga__Opportunity__c
		];
		List<NetProfit_Information__c> npInfoToDelete = [
			SELECT Id
			FROM NetProfit_Information__c
			WHERE Opportunity__c = :theBasket.cscfga__Opportunity__c
		];
		Opportunity theOpp = [
			SELECT Id, NetProfit_Complete__c
			FROM Opportunity
			WHERE Id = :theBasket.cscfga__Opportunity__c
		];
		if (!npInfoToDelete.isEmpty()) {
			try {
				delete ctnsToDelete;
				delete npInfoToDelete;
				if (theOpp.NetProfit_Complete__c) {
					theOpp.NetProfit_Complete__c = false;
					update theOpp;
				}
				return true;
			} catch (exception e) {
				return false;
			}
		} else {
			return false;
		}
	}
}