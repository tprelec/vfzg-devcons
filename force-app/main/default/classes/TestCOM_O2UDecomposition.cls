@isTest
private class TestCOM_O2UDecomposition {
	@isTest
	private static void testOrderDecomposition() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112', testAccount, '1111AA', 'Breestraat', 'LEIDEN', 112);
		insert site;

		Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', null);
		insert testOpp;

		csord__Order_Request__c coreq = new csord__Order_Request__c(csord__Module_Name__c = 'Test', csord__Module_Version__c = '1.0');
		insert coreq;

		csord__Order__c order = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Order_Request__c = coreq.Id,
			csord__Identification__c = 'DWHTestBatchOn_' + system.now(),
			csordtelcoa__Opportunity__c = testOpp.Id
		);
		insert order;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c accessDef = CS_DataTest.createProductDefinition('Access Infrastructure');
		accessDef.RecordTypeId = productDefinitionRecordType;
		accessDef.Product_Type__c = 'Fixed';
		insert accessDef;

		cscfga__Product_Configuration__c accessConf = CS_DataTest.createProductConfiguration(accessDef.Id, 'Access Infrastructure', basket.Id);
		accessConf.ClonedSiteIds__c = '{"0000":"' + '", "0002":"' + '"}';
		accessConf.ClonedPBXIds__c = '{"0000":"' + '", "0001":"' + '"}';
		insert accessConf;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(accessConf.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(accessConf.Id, subscription);
		service.csordtelcoa__Product_Configuration__c = accessConf.Id;
		service.csord__Identification__c = 'testSubscription';
		service.csord__Subscription__c = subscription.Id;
		service.csord__Status__c = 'Test';
		service.csord__Order__c = order.Id;
		service.VFZ_Commercial_Component_Name__c = 'TestComponent';
		service.VFZ_Commercial_Article_Name__c = 'TestArticle';
		service.Site__c = site.Id;
		insert service;

		order.csord__Status2__c = 'Ready for Delivery';

		csord__Order__c oldOrder = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Order_Request__c = coreq.Id,
			csord__Identification__c = 'DWHTestBatchOn_' + system.now(),
			csordtelcoa__Opportunity__c = testOpp.Id
		);
		insert oldOrder;

		csord__Subscription__c oldSubscription = CS_DataTest.createSubscription(accessConf.Id);
		oldSubscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		oldSubscription.csord__Order__c = oldOrder.Id;
		insert oldSubscription;

		csord__Service__c oldService = CS_DataTest.createService(accessConf.Id, oldSubscription);
		oldService.csordtelcoa__Product_Configuration__c = accessConf.Id;
		oldService.csord__Identification__c = 'testSubscription';
		oldService.csord__Subscription__c = oldSubscription.Id;
		oldService.csord__Status__c = 'Test';
		oldService.csord__Order__c = oldOrder.Id;
		oldService.VFZ_Commercial_Component_Name__c = 'TestComponent';
		oldService.VFZ_Commercial_Article_Name__c = 'TestArticle';
		oldService.Site__c = site.Id;
		insert oldService;

		VF_Contract__c contr = new VF_Contract__c();
		contr.Opportunity__c = testOpp.Id;
		contr.Account__c = testAccount.Id;
		insert contr;

		OrderType__c ot = new OrderType__c();
		ot.Name = 'TestOT2';
		ot.ExportSystem__c = 'BOP';
		ot.Status__c = 'New';
		ot.ExternalID__c = 'Test12';
		insert ot;

		Order__c ord = new Order__c();
		ord.VF_Contract__c = contr.Id;
		ord.OrderType__c = ot.Id;
		ord.BEN_Number__c = '66666';
		insert ord;

		Map<Id, csord__Order__c> newOrdersMap = new Map<Id, csord__Order__c>{ order.Id => order };
		Map<Id, csord__Order__c> oldOrdersMap = new Map<Id, csord__Order__c>{ order.Id => oldOrder };

		Test.startTest();
		COM_O2UDecomposition.orderDecomposition(newOrdersMap, oldOrdersMap);
		Test.stopTest();

		csord__Order__c result = [SELECT Id, csord__Status2__c FROM csord__Order__c WHERE Id = :order.Id];

		System.assertEquals('In Delivery', result.csord__Status2__c, 'Status is not the same.');

		csord__Service__c updatedService = [
			SELECT
				Id,
				Installation_Required__c,
				csord__Order__r.csord__Account__c,
				Has_Delivery_Components__c,
				Delivery_Components__c,
				COM_Delivery_Order__c
			FROM csord__Service__c
			WHERE Id = :service.Id
		];
		System.assertEquals(true, updatedService.Installation_Required__c, 'Field value is not correct.');
		System.assertEquals(true, updatedService.Has_Delivery_Components__c, 'Field value is not correct.');
		System.assertEquals('TestComponent', updatedService.Delivery_Components__c, 'Field value is not correct.');

		List<Asset> assetList = [
			SELECT
				Id,
				Name,
				RecordTypeId,
				COM_Type__c,
				COM_Delivery_Order__c,
				COM_Delivery_Component__c,
				csord__Service__c,
				COM_BOP_C_code__c,
				AccountId
			FROM Asset
			WHERE Name = 'TestMaterial_Model'
		];
		System.assertEquals(1, assetList.size(), 'Number of assets is not 1.');
		Asset comAsset = assetList[0];

		List<COM_Delivery_Materials__mdt> delMaterials = [
			SELECT Id, Type__c, Model_Name__c, BOP_C_code__c
			FROM COM_Delivery_Materials__mdt
			WHERE DeveloperName = 'TestMaterial'
		];
		System.assertEquals(
			Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get(COM_Constants.COM_ASSET_RECORD_TYPE).getRecordTypeId(),
			comAsset.RecordTypeId,
			'Asset record type is incorrect.'
		);
		System.assertEquals(delMaterials[0].Model_Name__c, comAsset.Name, 'Asset name is incorrect.');
		System.assertEquals(delMaterials[0].Type__c, comAsset.COM_Type__c, 'Asset type is incorrect.');
		System.assertEquals(updatedService.COM_Delivery_Order__c, comAsset.COM_Delivery_Order__c, 'Asset Delivery Order is incorrect.');
		System.assertEquals(updatedService.Delivery_Components__c, comAsset.COM_Delivery_Component__c, 'Asset Delivery Component is incorrect.');
		System.assertEquals(updatedService.Id, comAsset.csord__Service__c, 'Asset Service Id is incorrect.');
		System.assertEquals(delMaterials[0].BOP_C_code__c, comAsset.COM_BOP_C_code__c, 'Asset BOP Code is incorrect.');
		System.assertEquals(updatedService.csord__Order__r.csord__Account__c, comAsset.AccountId, 'Asset Account Id is incorrect.');

		Order__c resultOrd = [SELECT Id, COM_Delivery_Order__c FROM Order__c WHERE BEN_Number__c = '66666'];

		System.assertNotEquals(null, resultOrd.COM_Delivery_Order__c, 'Delivery Order Id should not be empty.');
	}
}
