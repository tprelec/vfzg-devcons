public with sharing class COM_DXLNotificationListenerService {
	static final String INTERFACE_TYPE = 'createCustomer';

	public static List<COM_DXL_Integration_Log__c> getIntegrationLog(String transactionId) {
		return [
			SELECT Id, Name, Transaction_Id__c, Affected_Objects__c, Error__c, Opportunity__c, Status__c, Type__c
			FROM COM_DXL_Integration_Log__c
			WHERE Type__c = :INTERFACE_TYPE AND Transaction_Id__c = :transactionId
		];
	}

	public static List<COM_DxlAffectedObjects> getAffectedObjects(String affectedObjects) {
		return (List<COM_DxlAffectedObjects>) JSON.deserializeStrict(affectedObjects, List<COM_DxlAffectedObjects>.class);
	}

	public static List<COM_DxlAffectedObjects> getAffectedObjectByType(String type, List<COM_DxlAffectedObjects> affectedObjects) {
		List<COM_DxlAffectedObjects> returnValue = new List<COM_DxlAffectedObjects>();

		for (COM_DxlAffectedObjects affectedObject : affectedObjects) {
			if (type.equals(affectedObject.type)) {
				returnValue.add(affectedObject);
			}
		}

		return returnValue;
	}

	public static List<External_Account__c> getExternalAccounts(Id parentAccountId) {
		return [SELECT Id, Account__c, External_Source__c FROM External_Account__c WHERE Account__c = :parentAccountId];
	}

	public static List<Id> getExternalAccountsByType(List<External_Account__c> externalAccounts, String type) {
		Set<Id> returnValue = new Set<Id>();

		for (External_Account__c externalAccountRecord : externalAccounts) {
			if (type.equals(externalAccountRecord.External_Source__c)) {
				returnValue.add(externalAccountRecord.Id);
			}
		}
		return new List<Id>(returnValue);
	}

	public static String generateAsyncReponse(String name) {
		String returnValue = '';
		List<StaticResource> staticResources = [SELECT id, Name, body FROM StaticResource WHERE Name = :name];

		if (!Test.isRunningTest()) {
			returnValue = staticResources[0].body.toString();
		} else {
			if (name.equalsIgnoreCase('COM_DXL_CREATE_BSS_ORDER_ASYNC_RESPONSE')) {
				returnValue = '{"payload":{"orderLines":[{"componentAPID":"componentAPID","externalReferenceId":"yyyy","installedBaseId":"installedBaseId","billingorderId":"billingorderId","orderLineStatus":{"statusCode":"200","statusMessage":"OK"},"orderLineError":[]},{"componentAPID":"componentAPID","externalReferenceId":"xxxx","installedBaseId":"installedBaseId","billingorderId":"billingorderId","orderLineStatus":{"statusCode":"200","statusMessage":"OK"},"orderLineError":[]},{"componentAPID":"componentAPID","externalReferenceId":"zzzz","installedBaseId":"installedBaseId","billingorderId":"billingorderId","orderLineStatus":{"statusCode":"200","statusMessage":"OK"},"orderLineError":[]},{"componentAPID":"componentAPID","externalReferenceId":"kkkk","installedBaseId":"installedBaseId","billingorderId":"billingorderId","orderLineStatus":{"statusCode":"200","statusMessage":"OK"},"orderLineError":[]},{"componentAPID":"componentAPID","externalReferenceId":"mmmm","installedBaseId":"installedBaseId","billingorderId":"billingorderId","orderLineStatus":{"statusCode":"200","statusMessage":"OK"},"orderLineError":[]},{"componentAPID":"componentAPID","externalReferenceId":"nnnn","installedBaseId":"installedBaseId","billingorderId":"billingorderId","orderLineStatus":{"statusCode":"200","statusMessage":"OK"},"orderLineError":[]}]}}';
			} else if (name.equalsIgnoreCase('COM_DXL_CREATE_CUSTOMER_ASYNC_RESPONSE')) {
				returnValue = '{"payload":{"transactionId":"TR-1657110353379","customerDetails":{"companyDetails":{"unifyAccountRefId":"Unify-123456789","bopCompanyCode":"BOP-123456789"},"billingCustomer":{"unifyBillingCustomerRefId":"UBC-123"}},"billingEntities":{"unifyFinancialAccountRefId":"UFA-123","unifyBillingAccountRefId":"UBA-123"},"contacts":[{"unifyContactRefId":"Unify-123456789","salesforceAccountId":"0013O00000VLejmQAD","salesforceContactId":"0033O00000Uk4GbQAJ"},{"unifyContactRefId":"Unify-987654321","salesforceAccountId":"0013O00000VLejmQAD","salesforceContactId":"0033O00000Uk4FCQAZ"}],"sites":[{"unifySiteRefId":"unifySiteRefId","salesforceSiteId":"a063O000003vNFmQAM","salesforceAccountId":"0013O00000VLejmQAD","siteLevelServicesAPId":"siteLevelServicesAPId"}]}}';
			}
		}

		return returnValue;
	}
}
