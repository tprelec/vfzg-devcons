public with sharing class OpportunityNewOverrideController {
	private Account acc;
	public String accountId { get; set; }

	public OpportunityNewOverrideController(ApexPages.StandardController ignored) {
		accountId = ApexPages.currentPage().getParameters().get('accid');

		if (String.isNotEmpty(accountId)) {
			acc = [SELECT Id, Name FROM Account WHERE Id = :accountId];
		}
	}

	public PageReference url() {
		if (System.Userinfo.getUserType() == 'PowerPartner' && accountId == null) {
			return Page.AccountCustomSearch;
		}

		String pr = '/';
		String recordType = ApexPages.currentPage().getParameters().get('RecordType');

		// Classic mode URL
		if (UserInfo.getUiThemeDisplayed() == 'Theme3') {
			pr += '006/e?nooverride=1';

			if (accountId != null)
				pr += '&accid=' + accountId;
			if (recordType != null)
				pr += '&RecordType=' + recordType;
		} else {
			pr += 'lightning/o/Opportunity/new?nooverride=1';

			if (recordType != null)
				pr += '&recordTypeId=' + recordType;
			if (accountId != null)
				pr += '&defaultFieldValues=AccountId%3D' + accountId;
			if (acc != null)
				pr += ',Name%3D' + acc.Name.remove(',') + '';
		}

		PageReference p = new PageReference(pr);
		return p;
	}
}
