public with sharing class BiccBoaInformationBatchExecuteController {

	public BiccBoaInformationBatchExecuteController(ApexPages.StandardSetController ignored) {

	}

	public PageReference callBatch(){
		BiccBoaInformationBatch controller = new BiccBoaInformationBatch();
		database.executebatch(controller);
		Schema.DescribeSObjectResult prefix = BICC_BOA_Information__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}
}