@isTest
public with sharing class TestTriggerFlagsService {

    @isTest
    public static void testFlags() {
        TriggerFlagsService.turnOff('TestHandler1', 'testMethod');
        System.assert(!TriggerFlagsService.getFlag('TestHandler1', 'testMethod'));
        System.assert(TriggerFlagsService.getFlag('TestHandler2', 'testMethod'));
        TriggerFlagsService.turnOn('TestHandler1', 'testMethod');
        System.assert(TriggerFlagsService.getFlag('TestHandler1', 'testMethod'));
    }

}