public with sharing class RSAClientSecurity {
	// created for Digital Commerce authorisation check
	// CLIENT_ID and PRIVATE_KEY should be stored on a secure client-side place!
	static final String CLIENT_ID = '825cd97a-bd3d-4bf1-903c-36b9ad84a227';
	static final String PRIVATE_KEY = '-----BEGIN PRIVATE KEY-----' + '-----END PRIVATE KEY-----';
	public static final String API_BASE_URL = 'https://cs-ecommerce-sandbox-eu.herokuapp.com/';

	public static HttpResponse request(String method, String route, String payload) {
		String endpoint = API_BASE_URL + route;
		Map<String, String> headers = buildHeaders(PRIVATE_KEY, CLIENT_ID, method, '/services/current/' + route, payload);

		HttpRequest request = new HttpRequest();
		for (String header : headers.keySet()) {
			request.setHeader(header, headers.get(header));
		}
		request.setMethod(method);
		request.setEndpoint(endpoint);
		if (payload != null) {
			request.setBody(payload);
		}

		System.debug('request ' + request);
		Http http = new Http();
		HttpResponse response = http.send(request);
		return response;
	}

	public static String calculateRsaPayloadHash(String payload, String contentType) {
		String content = 'cs-elastic.payload\n' + parseContentType(contentType) + '\n';
		content += payload + '\n';

		Blob hash = Crypto.generateDigest('SHA-256', Blob.valueOf(content));
		return EncodingUtil.base64Encode(hash);
	}

	private static String parseContentType(String header) {
		return header != null ? header.split(';')[0].trim().toLowerCase() : '';
	}

	public static String generateRsaAuthorizationHeader(String privateKey, String clientId, String method, String url, String hash) {
		Long ts = System.currentTimeMillis();
		String nonce = genNonce();
		String sig = generateSignature(privateKey, ts, nonce, method, url, hash);

		String result = 'CS-Elastic ' + 'ts=' + quote(ts) + ', clientId=' + quote(clientId) + ', nonce=' + quote(nonce) + ', sig=' + quote(sig);

		if (hash != null) {
			result += ', hash=' + quote(hash);
		}

		return result;
	}

	private static String quote(Object value) {
		return '\"' + String.valueOf(value) + '\"';
	}

	private static String generateSignature(String privateKey, Long ts, String nonce, String method, String url, String hash) {
		if (method == null) {
			method = '';
		}
		if (url == null) {
			url = '';
		}

		String normalized = 'cs-elastic' + '\n' + ts + '\n' + nonce + '\n' + method.toUpperCase() + '\n' + url + '\n';

		if (hash != null) {
			normalized += hash + '\n';
		}

		privateKey = privateKey.replaceAll('-----BEGIN PRIVATE KEY-----', '').replaceAll('-----END PRIVATE KEY-----', '').replaceAll('\\n', '');

		Blob key = EncodingUtil.base64Decode(privateKey);
		Blob signature = Crypto.sign('RSA-SHA256', Blob.valueOf(normalized), key);
		return EncodingUtil.base64Encode(signature);
	}

	private static String genNonce() {
		Blob key = Crypto.GenerateAESKey(128);
		return EncodingUtil.convertToHex(key);
	}

	private static Map<String, String> buildHeaders(String privateKey, String clientId, String method, String url, String payload) {
		if (payload != null) {
			Map<String, String> headers = new Map<String, String>{
				'Authorization' => generateRsaAuthorizationHeader(
					privateKey,
					clientId,
					method,
					url,
					calculateRsaPayloadHash(payload, 'application/json')
				),
				'Content-Type' => 'application/json'
			};
			headers.put('Content-Length', String.valueOf(payload.length()));
			return headers;
		} else {
			Map<String, String> headers = new Map<String, String>{
				'Authorization' => generateRsaAuthorizationHeader(privateKey, clientId, method, url, null)
			};
			headers.put('Content-Length', '0');
			return headers;
		}
	}
}
