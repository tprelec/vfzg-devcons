@IsTest
public with sharing class TestValidationService {
	@TestSetup
	static void makeData() {
		TestUtils.createOrderValidationOpportunity();
		insert new Order_Validation__c(
			Active__c = true,
			API_Fieldname__c = 'Project_Comments__c',
			Description__c = 'Check for correct Opportunity Name',
			Error_message__c = 'Invalid Opportunity',
			Export_System__c = 'BOP;SIAS',
			Name = 'Opportunity Name Check',
			Object__c = 'Opportunity',
			Operator__c = '==',
			Proposition__c = 'One Fixed;One Net;Numberporting;One Fixed Express;IPVPN;Legacy;Internet;Mobile;Services;Ziggo',
			Value__c = 'Test Opp',
			Who__c = GeneralUtils.currentUser.Profile.Name
		);
		// Create Opportunity
		TestUtils.createCompleteOpportunity();
	}

	private static Opportunity getOpportunity() {
		return [SELECT Id, Name, Partner_Account__c FROM Opportunity LIMIT 1];
	}

	@IsTest
	private static void testOpportunityValidationWithError() {
		Opportunity opp = getOpportunity();
		opp.Project_Comments__c = 'Test Opp';
		update opp;

		Test.startTest();
		ValidationService.ValidationRequest req = new ValidationService.ValidationRequest(
			'ORDER',
			opp.Id
		);
		ValidationService.ValidationResponse resp = ValidationService.validate(
			new List<ValidationService.ValidationRequest>{ req }
		)[0];

		Test.stopTest();

		System.assert(
			resp.errors[0].contains('Invalid Opportunity'),
			'Validation should report an error.'
		);
	}

	private static void testOpportunityValidationWithoutError() {
		Opportunity opp = getOpportunity();
		opp.Project_Comments__c = 'Test Opp X';
		update opp;

		Test.startTest();
		ValidationService.ValidationRequest req = new ValidationService.ValidationRequest(
			'ORDER',
			opp.Id
		);
		ValidationService.ValidationResponse resp = ValidationService.validate(
			new List<ValidationService.ValidationRequest>{ req }
		)[0];

		Test.stopTest();

		System.assert(resp.isValid, 'Validation should not report an error.');
	}
}