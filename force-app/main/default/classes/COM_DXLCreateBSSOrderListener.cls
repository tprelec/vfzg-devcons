public with sharing class COM_DXLCreateBSSOrderListener {
	private COM_DXLCreateBSSOrderAsyncResponse payload;
	private String transactionId;
	public Boolean payloadValid {
		get {
			return payloadIsValid();
		}
	}

	public COM_DXLCreateBSSOrderListener(COM_DXLCreateBSSOrderAsyncResponse payload, String transactionId) {
		this.payload = payload;
		this.transactionId = transactionId;
	}

	public void processPayload() {
		List<COM_DXL_Integration_Log__c> integrationLogRecordList = COM_DXL_BSSListenerService.getIntegrationLog(this.transactionId);

		COM_Integration_setting__mdt dxlIntegrationSettings = COM_Integration_setting__mdt.getInstance(
			COM_DXL_Constants.COM_DXL_BSS_INTEGRATION_SETTING_NAME
		);

		String asyncSuccessStatus = dxlIntegrationSettings != null ? dxlIntegrationSettings.Async_success_status__c : 'Active';

		String asyncFailedStatus = dxlIntegrationSettings != null ? dxlIntegrationSettings.Async_failed_status__c : 'Failed';

		updateServiceLineItems(this.payload, asyncSuccessStatus, asyncFailedStatus);
		updateTransactionLogRecord(integrationLogRecordList[0]);
	}

	@TestVisible
	private void updateServiceLineItems(COM_DXLCreateBSSOrderAsyncResponse response, String asyncSuccessStatus, String asyncFailedStatus) {
		List<csord__Service_Line_Item__c> serviceLineItemsToUpdate = new List<csord__Service_Line_Item__c>();

		for (COM_DXLCreateBSSOrderAsyncResponse.OrderLines orderingResult : response.payload.orderLines) {
			if (COM_DXL_BSSListenerService.orderLineStatusSuccess(orderingResult)) {
				serviceLineItemsToUpdate.add(
					new csord__Service_Line_Item__c(
						Id = orderingResult.externalReferenceId,
						Billing_status__c = asyncSuccessStatus,
						Assigned_Product_Id__c = orderingResult.componentAPID
					)
				);
			} else {
				serviceLineItemsToUpdate.add(
					new csord__Service_Line_Item__c(Id = orderingResult.externalReferenceId, Billing_status__c = asyncFailedStatus)
				);
			}
		}

		if (!serviceLineItemsToUpdate.isEmpty()) {
			update serviceLineItemsToUpdate;
		}
	}

	@TestVisible
	private static void updateTransactionLogRecord(COM_DXL_Integration_Log__c integrationLogRecord) {
		if (integrationLogRecord != null) {
			integrationLogRecord.Status__c = COM_DXL_Constants.COM_DXL_PROCESSED_STATUS;
			update integrationLogRecord;
		}
	}

	//TODO according to the yaml file, make sure all the mandatory fields are filled
	private Boolean payloadIsValid() {
		if (!String.isBlank(this.transactionId)) {
			return true;
		} else {
			return false;
		}
	}
}
