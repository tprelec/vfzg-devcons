public with sharing class TrainingRegistrationController {
    
	public TrainingRegistrationController() {
		orderBy = 'StartDateTime';  // Default sort order
        SortAscending = true;      
        filterDate = new Opportunity();   // Used to get VF to provide a nice date input picker
	}

	public Id selectedTrainingId {get;set;}
	public Id selectedEnrollmentId {get;set;}

    // Input fields for the filters
    public String filterTraining {get;set;}
    public String filterLocation {get;set;}
    public Opportunity filterDate {get;set;}

    // variables to control the sorting
    public boolean SortAscending{get;set;} 
    public string orderBy{get;set;}
    public string orderByColumn{get;set;}

    // Only internal users have access to the chatter feed
    public boolean getChatterAccess() {
        if (!GeneralUtils.currentUserIsPartnerUser()) {
            return true;
        } else {
            return false;
        }
    }

    // Resets all the filters
    // The page block rerenders which refreshes the data
    public void clear() {
        filterTraining=null;
        filterLocation=null;
        filterDate.Activation_date__c=null;
    }

    public void search() {
        // The page block rerenders which refreshes the data
        // Interesting observation is that if there is no action specified on the button the entire page is refreshed (slower)    
    }

    // Reverses the sort order if the column is the same
    // If a new column has been selected sort ascending
    public pageReference sort() {
        if(orderBy == orderByColumn) { 
            SortAscending=!SortAscending;
        } else { 
            SortAscending = true; 
            orderBy = orderByColumn;            
        } 
        return null; 
    }

	public Id trainingRecordTypeId {
		get{
			if(trainingRecordTypeId==null){
				trainingRecordTypeId = GeneralUtils.recordTypeMap.get('Event').get('SFDC_Training');
			}
			return trainingRecordTypeId;
		}
		set;
	}

    // Gets the training chatter group so that it can be displayed on the page
    public Id trainingChatterId {
        get{
            if(trainingChatterId==null){
                trainingChatterId = [SELECT Id From CollaborationGroup WHERE CollaborationType='Public' and name='Training'].id;
            }
            return trainingChatterId;
        }
        set;
    }

    public List<Event> availableTrainings {
    	get{
    		List<Event> availableTrainings = new List<Event>();
    		// add open trainings

            // If the current user has accepted an event exclude it from the results
            List<EventRelation> eventRelationList = [select EventId from EventRelation WHERE Status = 'Accepted' AND RelationId = :UserInfo.getUserId()];
            Set<ID> excludeEventsSet = new Set<ID>();
            for (EventRelation er:eventRelationList) {
                excludeEventsSet.add(er.EventId);
            }

            // For invitational events we need a record where the user has not accepted yet or has declined
            eventRelationList = [select EventId from EventRelation WHERE (Status = 'New' OR Status = 'Declined') AND RelationId = :UserInfo.getUserId()];
            Set<ID> includeEventsSet = new Set<ID>();
            for (EventRelation er:eventRelationList) {
                includeEventsSet.add(er.EventId);
            }            

            // Retrieve the events in one main query so that we can adjust the sort order
            String dateTimeFormat = DateTime.now().format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
            string querystr = 'Select Id, Subject, Type, StartDateTime,EndDateTime,Location,Max_Participants__c,Public_Calendar__c,(Select Id From EventRelations Where Status = \'Accepted\' )';
            querystr += ' From Event e';
            querystr += ' Where RecordTypeId = \''+trainingRecordTypeId+'\'';
            querystr += ' AND IsChild = false';
            querystr += ' AND StartDateTime > '+dateTimeFormat;
            querystr += ' AND ((e.id NOT IN :excludeEventsSet';
            querystr += ' AND e.Type = \'Training (Open)\')';
            querystr += ' OR (e.id IN :includeEventsSet';
            querystr += ' AND e.Type = \'Training (Invitation-based)\'))';

            // Apply any filters that have been entered in the Search section of the page
            if (filterLocation!=null) {
                querystr += ' AND Location like \'%'+filterLocation+'%\'';
            }
            if (filterDate.Activation_date__c!=null) {
                Datetime dtStart = datetime.newInstanceGMT(filterDate.Activation_date__c.year(), filterDate.Activation_date__c.month(),filterDate.Activation_date__c.day(),0,0,1);                
                Datetime dtEnd = datetime.newInstanceGMT(filterDate.Activation_date__c.year(), filterDate.Activation_date__c.month(),filterDate.Activation_date__c.day(),23,59,59);                
                String filterDateTimeFormatStart = dtStart.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                String filterDateTimeFormatEnd = dtEnd.format('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
                querystr += ' AND StartDateTime >= '+filterDateTimeFormatStart;    
                querystr += ' AND EndDateTime <= '+filterDateTimeFormatEnd;                                
            }
            if (filterTraining!=null) {
                querystr += ' AND Subject like \'%'+filterTraining+'%\'';                
            }

            // Apply the current sort order
            querystr += ' ORDER BY '+orderBy;
            if (SortAscending) {
                querystr += ' ASC';
            } else {
                querystr += ' DESC';                
            }

            availableTrainings = database.query(querystr);

            return availableTrainings;

    	}
    	set;
    }


    public List<EventRelation> currentTrainingRegistrations {
    	get{

    		return [Select Id, Status, Relation.Name, EventId, Event.Subject, Event.StartDateTime,Event.EndDateTime,
    					Event.Location, Event.Type
    				From EventRelation 
    				Where Event.RecordTypeId = :trainingRecordTypeId 
    				AND RelationId = :UserInfo.getUserId()
    				AND Status = 'Accepted'];
    	}
    	set;
    }
        	

    public void enroll(){
    	
    	// check if there is an active invite already
		List<EventRelation> activeInvites = [Select Id, Status, EventId
		    				From EventRelation 
		    				Where EventId = :selectedTrainingId 
		    				AND RelationId = :UserInfo.getUserId()
		    				AND Status != 'Accepted'];    	

		if(!activeInvites.isEmpty()){
			// if there is an invite, just update the invite
			activeInvites[0].Status = 'Accepted';
			update activeInvites[0];
		} else {
			// create a new attendee
	    	EventRelation newAttendee = new EventRelation();
	    	newAttendee.EventId = selectedTrainingId;
	    	newAttendee.RelationId = UserInfo.getUserId();
	    	newAttendee.Status = 'Accepted';
            newAttendee.isParent = false;
            newAttendee.isWhat = false;
	    	newAttendee.isInvitee = true;

	    	SharingUtils.insertRecordsWithoutSharing(newAttendee);
		}

    }

    public void deleteEnrollment(){
    	// check if it's a training on invite base. If so, changed status to 'Declined'. For other training type, just remove the registration
        EventRelation er = [Select Status, Event.Type From EventRelation Where Id = :selectedEnrollmentId];
        if(er.Event.Type == 'Training (Invitation-based)'){
            er.Status = 'Declined';
            SharingUtils.updateRecordsWithoutSharing(er);
        } else {
    	   SharingUtils.deleteRecordsWithoutSharing(er);
        }
    	
    }

    // a utility method that checks whether the public calendars stored in the custom metadata are up-to-date
    public void checkAndUpdatePublicCalendars(){
        // create a map of all existing public calendars
        Pagereference r = new PageReference('/_ui/common/data/LookupResultsFrame?lkfm=swt&lknm=cal&lktp=023&cltp=resource&lksrch=#');
        String html = r.getContent().toString();
        Matcher m = Pattern.compile('lookupPick\\(\'swt\',\'cal_lkid\',\'cal\',\'\',\'(.*?)\',\'(.*?)\',\'\',\'\'\\)">(.*?)</a></TH><td class=" dataCell  ">(.*?)<\\/td><\\/tr>').matcher(html);
        Map<String,Id> calNameToId = new Map<String,Id>();
        
        while (m.find()) {
            calNameToId.put(m.group(2),m.group(1));
        }

        // fetch the calendars stored in metadata and update/add where needed
        List<Public_Calendar__c> calToUpsert = new List<Public_Calendar__c>();
        
        for (Public_Calendar__c cal : Public_Calendar__c.getall().values()) {
            if(calNameToId.containsKey(cal.Name)){
                if(calNameToId.get(cal.Name) != cal.PublicCalendarId__c){
                    cal.PublicCalendarId__c = calNameToId.get(cal.Name);
                    calToUpsert.add(cal);
                }
                calNameToId.remove(cal.Name);
            } 
        }   
        // for the remaining, create new entries     
        for(String calName : calNameToId.keySet()){
            Public_Calendar__c cal = new Public_Calendar__c();
            cal.PublicCalendarId__c = calNameToId.get(calName);
            cal.Name = calName;
            calToUpsert.add(cal);
        }
        SharingUtils.upsertRecordsWithoutSharing(calToUpsert);
    }

}