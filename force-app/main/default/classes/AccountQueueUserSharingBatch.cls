/**
 * @description       : reassign queue user sharing to partner manager when sales agent user is deactivated
 * @author            : mcubias
 * @group             : leads united
 * @last modified on  : 07-09-2022
 * @last modified by  :
 **/
public class AccountQueueUserSharingBatch implements Queueable {
	private Map<Id, Id> managerIdByUser = new Map<Id, Id>();

	/**
	 * @description
	 * @param managerIdByUser
	 **/
	public AccountQueueUserSharingBatch(Map<Id, Id> managerIdByUser) {
		this.managerIdByUser = managerIdByUser;
	}
	/**
	 * @description
	 * @param context
	 **/
	public void execute(QueueableContext context) {
		Map<Id, Queue_User_Sharing__c> queuesByManager = new Map<Id, Queue_User_Sharing__c>();
		for (Queue_User_Sharing__c qusManager : [SELECT Id, OwnerId FROM Queue_User_Sharing__c WHERE OwnerId = :managerIdByUser.values()]) {
			queuesByManager.put(qusManager.OwnerId, qusManager);
		}

		// If no Queue User Sharing record is not found for any Partner manager, create it
		List<Queue_User_Sharing__c> qusManagers = new List<Queue_User_Sharing__c>();
		for (Id managerId : managerIdByUser.values()) {
			if (!queuesByManager.containsKey(managerId)) {
				Queue_User_Sharing__c qusManager = new Queue_User_Sharing__c(OwnerId = managerId);
				qusManagers.add(qusManager);
				queuesByManager.put(managerId, qusManager);
			}
		}
		insert qusManagers;

		List<Account> accounts = [
			SELECT Id, Queue_User_Assigned__r.OwnerId
			FROM Account
			WHERE Queue_User_Assigned__r.OwnerId IN :managerIdByUser.keySet()
		];
		// get account records where newly inactive user previously was the Queue_User_Assigned__c
		for (Account a : accounts) {
			Id managerId = managerIdByUser.get(a.Queue_User_Assigned__r.OwnerId);
			if (queuesByManager.containsKey(managerId)) {
				a.Queue_User_Assigned__c = queuesByManager.get(managerId).Id;
			}
		}
		update accounts;
	}
}
