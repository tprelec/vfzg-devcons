/**
 * @description       : Business Processes related with contracted products
 * @author            : sebastian.melgin@vodafoneziggo.com
 * @last modified on  : 2022-07-02
 * @last modified by  : sebastian.melgin@vodafoneziggo.com
 **/
public with sharing class ContractedProductService {
	public final static String FEAT_FLAG_ODR_RED1 = 'ODR_RED1'; // One Data Redundancy
	public final static String FEAT_FLAG_ODR_HBO = 'ODR_HBO'; // HBO filling
	private final static String PRIMARY = 'Primary';
	private final static String SECONDARY = 'Secondary';
	private final static String SECURITY_EXCEPTION = 'The user does not have access to some fields. Check with an Administrator';

	/**
	 * Set the redundancy references used when sending messages to external service (BOP)
	 * @param  lContractedProducts	List of contracted Products
	 * @return set with the carrier product Ids
	 */
	public static Set<Id> setRedundancyReferences(List<Contracted_Products__c> lContractedProducts) {
		Set<String> setPrimaries = new Set<String>();
		Set<Id> setOrders = new Set<Id>();
		Map<String, Id> mapPrimaries = new Map<String, Id>();
		Set<Id> carrierProductsIds = new Set<Id>();
		List<Contracted_Products__c> lUpdateContractedProds = new List<Contracted_Products__c>();

		for (Contracted_Products__c cp : lContractedProducts) {
			if (
				String.isNotBlank(cp.Site_Availability__c) &&
				cp.Access_Redundancy__c.startsWithIgnoreCase(SECONDARY) &&
				String.isEmpty(cp.Access_Primary_Reference_ID__c)
			) {
				String arkKey = PRIMARY + cp.Model_Number_Identifier__c;
				// ex. Primary123123 | KEY0 (secondary)
				setPrimaries.add(arkKey);
			}
			if (!setOrders.contains(cp.Order__c)) {
				setOrders.add(cp.Order__c);
			}
		}

		// bring all the trunks ... build a set with them and set primaries.
		for (Contracted_Products__c cp : [
			SELECT Id, Access_Redundancy__c, Product__r.Role__c
			FROM Contracted_Products__c
			WHERE Order__c IN :setOrders AND Product__r.Role__c = 'Carrier'
		]) {
			if (
				String.isNotBlank(cp.Access_Redundancy__c) &&
				setPrimaries.contains(cp.Access_Redundancy__c) &&
				!mapPrimaries.containsKey(cp.Access_Redundancy__c)
			) {
				mapPrimaries.put(cp.Access_Redundancy__c, cp.Id);
			}
			carrierProductsIds.add(cp.Id);
		}
		// The following assumes an already existing Primary ...
		for (Contracted_Products__c cp : lContractedProducts) {
			if (
				String.isNotBlank(cp.Site_Availability__c) &&
				cp.Access_Redundancy__c.startsWithIgnoreCase(SECONDARY) &&
				String.isEmpty(cp.Access_Primary_Reference_ID__c)
			) {
				String arkKey = PRIMARY + cp.Model_Number_Identifier__c;
				String sAccessPrimaryId = mapPrimaries.get(arkKey);
				cp.Access_Primary_Reference_ID__c = sAccessPrimaryId;
				lUpdateContractedProds.add(cp);
			}
		}
		if (!lUpdateContractedProds.isEmpty()) {
			update lUpdateContractedProds;
		}
		return carrierProductsIds;
	}

	/**
	 * Set the hbo at the contracted product level to facilitate construction of order messages
	 * sent to external service (BOP).
	 * @param  lContractedProducts	List of contracted Products
	 * @param  setCarrierProducts Set of ids representing the carrier contracted products.
	 */
	public static void setHboS(List<Contracted_Products__c> lContractedProducts, Set<Id> setCarrierProducts) {
		List<Contracted_Products__c> lCtrPrdHbo = new List<Contracted_Products__c>();
		Set<Id> setSites = filterSites(lContractedProducts); // only the sites of carrier products
		Map<Id, Map<String, Id>> mapHboListsBySites = getHboListsBySites(setSites);
		System.debug('** mapHboListsBySites ..');
		System.debug(mapHboListsBySites);

		/**
		 * 1. Only send HBO_id on Redundacy products (and Legacy?)
		 * 2. When two or more HBO are in the same SITE, check to which HBO the contracted product is tied to.
		 * 2.1. If the contracted product is not tied to any of the HBO, then tie it randomly.
		 * 3. Put the HBO which is tied to that product on the contracted product.
		 */
		for (Contracted_Products__c lCtrPr : lContractedProducts) {
			if (String.isBlank(lCtrPr.HBO__c) && setCarrierProducts.contains(lCtrPr.Id)) {
				// Do this only with redundancy products on Automated flow.
				Id currSiteId = lCtrPr?.Site__c;
				Boolean bRedundancyIsNotNull = !(String.isBlank(lCtrPr?.Access_Redundancy__c) || lCtrPr?.Access_Redundancy__c.equals('None'));
				Boolean bIsPrimary = bRedundancyIsNotNull && lCtrPr?.Access_Redundancy__c.startsWithIgnoreCase(PRIMARY);
				Boolean bIsSecondary = bRedundancyIsNotNull && lCtrPr?.Access_Redundancy__c.startsWithIgnoreCase(SECONDARY);

				if (currSiteId != null && bRedundancyIsNotNull) {
					// check the map and pick up the right HBO id.
					Id hboId;
					if (bIsPrimary) {
						hboId = safeHboAssignation(mapHboListsBySites, currSiteId, PRIMARY, SECONDARY);
					} else if (bIsSecondary) {
						hboId = safeHboAssignation(mapHboListsBySites, currSiteId, SECONDARY, PRIMARY);
					}
					lCtrPr.HBO__c = hboId;
					lCtrPrdHbo.add(lCtrPr);
				} else if (currSiteId != null) {
					Id hboId = safeHboAssignation(mapHboListsBySites, currSiteId, PRIMARY, SECONDARY);
					lCtrPr.HBO__c = hboId;
					lCtrPrdHbo.add(lCtrPr);
				}
			}
		}
		if (!lCtrPrdHbo.isEmpty()) {
			update lCtrPrdHbo;
		}
	}

	/**
	 * Creates a set with the sites related to the current contractedProducts set.
	 * Returns the set with Site's Ids.
	 * @param lContractedProducts List of contracted products to process.
	 */
	private static Set<Id> filterSites(List<Contracted_Products__c> lContractedProducts) {
		Set<Id> setSites = new Set<Id>();
		Set<Id> setProductsCp = new Set<Id>();
		Map<Id, String> mapProdRoles = new Map<Id, String>();

		for (Contracted_Products__c lCtrPr : lContractedProducts) {
			setProductsCp.add(lCtrPr.Product__c);
		}
		for (Product2 currProd : [SELECT Id, Role__c FROM Product2 WHERE Id IN :setProductsCp AND Role__c = 'Carrier']) {
			mapProdRoles.put(currProd.Id, currProd.Role__c);
		}

		for (Contracted_Products__c lCtrPr : lContractedProducts) {
			if (lCtrPr.Site__c != null && mapProdRoles.containsKey(lCtrPr.Product__c)) {
				setSites.add(lCtrPr.Site__c);
			}
		}
		return setSites;
	}

	/**
	 * Stores per SiteId, a matrix where the Key is the Access Redundancy Type,
	 * and the stored element is the HBO.Id
	 * Returns Map with Matrix per site with the Access Redundanct Types.
	 * @param setSites the set of sites needed for building the matrix.
	 */
	private static Map<Id, Map<String, Id>> getHboListsBySites(Set<Id> setSites) {
		Map<Id, Map<String, Id>> mapHboListsBySites = new Map<Id, Map<String, Id>>();
		Map<String, Id> mapHboIds = new Map<String, Id>();

		for (List<HBO__c> lHboPerSite : [
			SELECT Id, Name, hbo_site__c, hbo_postal_check__r.Access_Redundancy__c
			FROM HBO__c
			WHERE hbo_site__c IN :setSites
			ORDER BY hbo_site__c
		]) {
			for (HBO__c currHbo : lHboPerSite) {
				if (mapHboListsBySites.containsKey(currHbo.hbo_site__c)) {
					mapHboIds = mapHboListsBySites.get(currHbo.hbo_site__c);
					mapHboIds.put(currHbo?.hbo_postal_check__r?.Access_Redundancy__c, currHbo.Id);
					mapHboListsBySites.put(currHbo.hbo_site__c, mapHboIds.clone());
				} else {
					mapHboIds = new Map<String, Id>();
					mapHboIds.put(currHbo?.hbo_postal_check__r?.Access_Redundancy__c, currHbo.Id);
					mapHboListsBySites.put(currHbo.hbo_site__c, mapHboIds.clone());
				}
			}
		}
		// Add the sites without HBO's with '3party' on the map.
		if (!mapHboListsBySites.isEmpty()) {
			List<Id> lHboMapKeys = new List<Id>(mapHboListsBySites.keySet());
			for (Id currSite : setSites) {
				if (!mapHboListsBySites.containsKey(currSite)) {
					// Look for the first Hbo you can find on the matrix.
					Map<String, Id> mapHbos = mapHboListsBySites.get(lHboMapKeys[0]);
					Id firstHboId = (mapHbos.values())[0];
					mapHboIds = new Map<String, Id>();
					mapHboIds.put('3party', firstHboId);
					mapHboListsBySites.put(currSite, mapHboIds.clone());
				}
			}
		}
		return mapHboListsBySites;
	}

	// Assign the right HboId if possible, otherwise a random one.
	private static String safeHboAssignation(Map<Id, Map<String, Id>> mapHboListsBySites, String sSiteId, String sPriority1, String sPriority2) {
		Map<String, Id> mapHboByAccessType = mapHboListsBySites.get(sSiteId);
		String sHboId = null;
		if (mapHboByAccessType != null) {
			if (mapHboByAccessType.containsKey(sPriority1)) {
				sHboId = mapHboByAccessType.get(sPriority1);
			} else if (mapHboByAccessType.containsKey(sPriority2)) {
				sHboId = mapHboByAccessType.get(sPriority2);
			} else {
				List<Id> lstHboVals = mapHboByAccessType.values();
				sHboId = lstHboVals[0];
			}
		}
		return sHboId;
	}

	public class CTPServiceNoAccessException extends Exception {
	}
}
