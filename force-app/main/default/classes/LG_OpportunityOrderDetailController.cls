public class LG_OpportunityOrderDetailController {
    
    private List<ProductInfo> productInfos { get; private set; }
    public List<Opportunity> penaltyFee{get;set;}
    //public Boolean flag{get;set;}
    public Decimal finalPenaltyFee{get;set;}
    
    
    // Product Configurations
    public List<ProductInfo> pcs {
        get {
            
            if (this.productInfos == null) {
                this.productInfos = getProductDisplayData();
            }
            
            return this.productInfos;
        }
        
        set {
            this.productInfos = value;
        }
    }
    
    /*
     * Opportunity record
    */
    public Opportunity relatedTo { get; set; }

    /*
     * Class Constructor
    */
    public LG_OpportunityOrderDetailController() {
        finalPenaltyFee = 0;
    }
    
    /*
     * Get Display Data (ProductInfo = wraper for date and OLI)
    */
    public List<ProductInfo> getProductDisplayData() {

        // Reset list
        this.pcs = new List<ProductInfo>();
        
        // Return empty list
        if (relatedTo == null) {
            return new List<ProductInfo>();
        }

        List<ProductInfo> tmp = new List<ProductInfo>();

        List<cscfga__Product_Configuration__c> pcs = [SELECT Id, Name, LG_InstallationWishDate__c, LG_Address__r.LG_FullAddressDetails__c,cscfga__Product_Basket__r.cscfga__Opportunity__r.LG_FinalPenalty__c 
                                                      ,cscfga__Product_Basket__r.cscfga__Opportunity__r.Account.LG_ExternalID__c 
                                                      FROM cscfga__Product_Configuration__c 
                                                        WHERE cscfga__Product_Basket__r.cscfga__Opportunity__c = :this.relatedTo.Id AND 
                                                        cscfga__Product_Basket__r.csordtelcoa__Synchronised_with_Opportunity__c  = true AND 
                                                        cscfga__Product_Family__c = 'Termination'
                                                        ORDER BY LG_Address__r.LG_FullAddressDetails__c, Name];

        // Populate product Info
        for (cscfga__Product_Configuration__c p :pcs) {
            finalPenaltyFee = p.cscfga__Product_Basket__r.cscfga__Opportunity__r.LG_FinalPenalty__c;
            ProductInfo pi = new ProductInfo();
            
            pi.productName = p.Name;
            pi.fullAddress = p.LG_Address__r.LG_FullAddressDetails__c;
            if(p.cscfga__Product_Basket__r.cscfga__Opportunity__r.Account.LG_ExternalID__c!= null){
                 pi.accountNumber = p.cscfga__Product_Basket__r.cscfga__Opportunity__r.Account.LG_ExternalID__c;
            }
           
            
           
            
            if (p.LG_InstallationWishDate__c != null) {
                pi.displayDate = p.LG_InstallationWishDate__c.format();
            }

            tmp.add(pi);
        }

        return tmp;
    }
    
    

    /*
     * Display helper class
    */
    public class ProductInfo {
        
        public string productName { get; set; }
        public string fullAddress { get; set; }
        public string displayDate { get; set; }
        public string accountNumber { get; set; }
        
    }
}