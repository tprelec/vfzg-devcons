@IsTest
public class TestCS_MFStatusUpdateStartupTime {
    public static final String CASE_STATUS_CLOSED = 'Closed';

    @IsTest
    private static void updateStartupTime() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        Group newGroup = new Group(Name='group name', type='Queue');
        insert newGroup;
        QueuesObject casesQueue = new QueueSObject(QueueID = newGroup.id, SobjectType = 'Case');
        insert casesQueue;


        System.runAs (simpleUser) {
            VF_Contract__c vfContract = createContract(simpleUser);
            List<Case> casesToInsert = new List<Case>();

            Case orderCleaningCase = CS_DataTest.createCase('CS MF Order Cleaning case','CS MF Order Cleaning', simpleUser, false);
            orderCleaningCase.Contract_VF__c = vfContract.Id;
            casesToInsert.add(orderCleaningCase);

            Case intakePreparationCase = new Case();
            intakePreparationCase.Subject = 'CS MF Project Intake and Preparation case';
            intakePreparationCase.RecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CS MF Project Intake and Preparation').getRecordTypeId();
            intakePreparationCase.ownerId = newGroup.Id;
            intakePreparationCase.Contract_VF__c = vfContract.Id;
            intakePreparationCase.Case_Start_Date__c = Date.today();
            casesToInsert.add(intakePreparationCase);
            insert casesToInsert;

            Test.startTest();
            orderCleaningCase.Status = CASE_STATUS_CLOSED;
            update orderCleaningCase;

            intakePreparationCase.OwnerId = simpleUser.Id;
            update intakePreparationCase;
            // Comment if the class is included as custom action on some PB process.
            List<Id> lIds = new List<Id>();
            lIds.add(intakePreparationCase.Id);
            CS_MFStatusUpdateStartupTime.calculateStartupTime2(lIds);
			//
            Test.stopTest();

            List<VF_Contract__c> contracts = [
                SELECT Id, Startup_Time__c, Start_Up_Time_Start__c
                FROM VF_Contract__c
                LIMIT 1
            ];
            System.assertEquals(CASE_STATUS_CLOSED, orderCleaningCase.Status);
            System.assertEquals(1, contracts.size());



            System.assertEquals(0, contracts[0].Startup_Time__c);
        }
    }


    private static VF_Contract__c createContract(User runningUser) {
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;

        Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', runningUser.id);
        testOpp.RoleCreateOrChangeOwner__c = 'VGE Global Account Manager';
        insert testOpp;

        VF_Contract__c vfContract = CS_DataTest.createDefaultVfContract(runningUser, false);
        vfContract.Opportunity__c = testOpp.Id;
        vfContract.Contract_Cleaning_Result__c = 'Clean';
        insert vfContract;
        return vfContract;
    }

}