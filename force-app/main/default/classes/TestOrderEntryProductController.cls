@IsTest
public with sharing class TestOrderEntryProductController {
	@TestSetup
	static void makeData() {
		TestUtils.theAdministrator = new User(Id = UserInfo.getUserId());
		OrderEntryTestDataFactory.createOpportunityWithAllRelatedRecords();
		List<OE_Add_On__c> addOns = OrderEntryTestDataFactory.createOrderEntryAddOns();
		List<OE_Product__c> oeProducts = OrderEntryTestDataFactory.createOrderEntryProducts();
		OrderEntryTestDataFactory.createPromotions();
		Id mainProductId;
		Id internetProductId;
		Id tvProductId;
		Id mobileProductId;
		for (OE_Product__c prod : oeProducts) {
			if (prod.Type__c == 'Main') {
				mainProductId = prod.Id;
			} else if (prod.Type__c == 'Internet') {
				internetProductId = prod.Id;
			} else if (prod.Type__c == 'TV') {
				tvProductId = prod.Id;
			} else if (prod.Type__c == 'Mobile') {
				mobileProductId = prod.Id;
			}
		}

		OrderEntryTestDataFactory.createOrderEntryProductAddOn(internetProductId, addOns[0].Id);
		OrderEntryTestDataFactory.createOEProductBundle(mainProductId, internetProductId, tvProductId);
		OrderEntryTestDataFactory.createOEMobileProductBundle(mainProductId, mobileProductId, '2');
		OrderEntryTestDataFactory.createOrderEntryProductAddOn(internetProductId, addOns[1].Id);
	}

	@IsTest
	static void testUpdateOpportunityJourneyType() {
		Opportunity opp = [SELECT Id, Journey_Type__c FROM Opportunity LIMIT 1 FOR UPDATE];

		OrderEntryProductController.updateOpportunityJourneyType(opp.Id, 'Fixed');

		opp = [SELECT Id, Journey_Type__c FROM Opportunity LIMIT 1];

		System.assertEquals('Fixed', opp.Journey_Type__c, 'Journey type is Fixed.');
	}

	@IsTest
	static void testGetProductsPerType() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1 FOR UPDATE];
		List<String> mainType = new List<String>{ 'Main' };

		Test.startTest();
		List<OE_Product__c> mainProducts = OrderEntryProductController.getProducts(opp.Id, mainType);
		Test.stopTest();

		Set<String> types = new Set<String>();

		for (OE_Product__c oep : mainProducts) {
			types.add(oep.Type__c);
		}

		List<String> typesList = new List<String>();
		typesList.addAll(types);

		System.assertEquals(1, types.size(), 'There must be only one type');
		System.assertEquals(mainType[0], typesList[0], 'There must be Main');
	}

	@IsTest
	static void testGetProductsAllTypes() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		List<OE_Product__c> allProducts = OrderEntryProductController.getProducts(opp.Id, null);
		Test.stopTest();

		Set<String> types = new Set<String>();

		for (OE_Product__c oep : allProducts) {
			types.add(oep.Type__c);
		}

		System.assert(types.size() > 1, 'There must be multiple types');
	}

	@IsTest
	static void testGetAddons() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
		List<OE_Product__c> products = [SELECT Id FROM OE_Product__c WHERE Type__c = 'Internet'];
		Test.startTest();
		List<OE_Product_Add_On__c> allAddons = OrderEntryProductController.getAddons(opp.Id, products[0].Id);
		Test.stopTest();

		Set<Id> parentProductIds = new Set<Id>();

		for (OE_Product_Add_On__c oepao : allAddons) {
			parentProductIds.add(oepao.Product__c);
		}

		System.assert(allAddons.size() > 1, 'There must be more then 1 addon');
		System.assert(parentProductIds.size() == 1, 'There must be only one parent product item');
	}

	@IsTest
	static void testGetAddonPrice() {
		Decimal oneOffPrice = 5.5;
		Decimal recurringPrice = 2.5;
		TestUtils.createOrderType();
		TestUtils.createOneOffProduct();
		TestUtils.createRecurringProduct();

		Product2 ooprod = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = :TestUtils.theOneOffProduct.ProductCode AND IsActive = TRUE];
		Product2 recprod = [SELECT Id, ProductCode FROM Product2 WHERE ProductCode = :TestUtils.theRecurringProduct.ProductCode AND IsActive = TRUE];

		cspmb__Add_On_Price_Item__c aopi = new cspmb__Add_On_Price_Item__c();
		aopi.cspmb__One_Off_Charge_External_Id__c = ooprod.ProductCode;
		aopi.cspmb__Recurring_Charge_External_Id__c = recprod.ProductCode;
		aopi.cspmb__One_Off_Charge__c = oneOffPrice;
		aopi.cspmb__Recurring_Charge__c = recurringPrice;
		aopi.cspmb__Is_Active__c = true;
		insert aopi;

		Test.startTest();
		cspmb__Add_On_Price_Item__c price = OrderEntryProductController.getAddonPrice(TestUtils.theOneOffProduct.ProductCode);
		Test.stopTest();

		System.assertEquals(TestUtils.theOneOffProduct.ProductCode, price.cspmb__One_Off_Charge_External_Id__c, 'Product code should match');
		System.assertEquals(oneOffPrice, price.cspmb__One_Off_Charge__c, 'Product price should match');
		System.assertEquals(recurringPrice, price.cspmb__Recurring_Charge__c, 'Product price should match');
	}

	@IsTest
	static void testGetDefaultBundle() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
		List<OE_Product__c> products = [SELECT Id FROM OE_Product__c WHERE Type__c = 'Main'];
		Test.startTest();
		OE_Product_Bundle__c defaultBundle = OrderEntryProductController.getDefaultBundle(opp.Id, products[0].Id);
		Test.stopTest();

		System.assertNotEquals(null, defaultBundle, 'Default bundle must exist');
		System.assertEquals(true, defaultBundle.Default__c, 'Bundle must be default');
	}

	@IsTest
	static void testGetProductsForTvOrTelephony() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		List<OE_Product__c> products = [SELECT Id FROM OE_Product__c WHERE Type__c = 'Internet'];

		Test.startTest();
		List<OE_Product__c> result = OrderEntryProductController.getTvAndTelephonyProducts(opp.Id, products[0].Id);
		Test.stopTest();

		System.assertEquals(result.size(), 1, 'TV product returned!');
	}

	@IsTest
	static void testGetInternetProducts() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
		Account acc = [SELECT Id FROM Account WHERE Id = :opp.AccountId];

		Test.startTest();

		List<OE_Product__c> result = OrderEntryProductController.getInternetProducts(opp.Id, null);

		Test.stopTest();
		System.assertEquals(1, result.size(), 'One Internet product is returned.');
	}

	@IsTest
	static void testGetBundle() {
		OrderEntryTestDataFactory.insertMockJsonData(false);

		List<OE_Product__c> oeProducts = [SELECT Id, Type__c FROM OE_Product__c];

		Id mainProductId;
		Id internetProductId;
		Id tvProductId;
		for (OE_Product__c prod : oeProducts) {
			if (prod.Type__c == 'Main') {
				mainProductId = prod.Id;
			} else if (prod.Type__c == 'Internet') {
				internetProductId = prod.Id;
			} else if (prod.Type__c == 'TV') {
				tvProductId = prod.Id;
			}
		}
		List<OrderEntryData.OrderEntryProduct> jsonProducts = new List<OrderEntryData.OrderEntryProduct>();

		OrderEntryData.OrderEntryProduct prod1 = new OrderEntryData.OrderEntryProduct();
		prod1.id = mainProductId;
		prod1.type = 'Main';
		jsonProducts.add(prod1);

		OrderEntryData.OrderEntryProduct prod2 = new OrderEntryData.OrderEntryProduct();
		prod2.id = internetProductId;
		prod2.type = 'Internet';
		jsonProducts.add(prod2);

		OrderEntryData.OrderEntryProduct prod3 = new OrderEntryData.OrderEntryProduct();
		prod3.id = tvProductId;
		prod3.type = 'TV';
		jsonProducts.add(prod3);

		Test.startTest();
		OrderEntryData.ProductBundle pb = OrderEntryProductController.getBundle(jsonProducts, '1');

		System.assertEquals('PD-00061', pb.code, 'Bundle is returned.');
		Test.stopTest();
	}

	@IsTest
	static void testSaveOpportunityProducts() {
		Opportunity opp = [SELECT Id, AccountId FROM Opportunity LIMIT 1];
		OrderEntryTestDataFactory.insertMockJsonData(false);
		Test.startTest();
		OrderEntryProductController.saveOpportunityProducts(opp.Id);
		System.assertNotEquals(0, [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :opp.Id].size(), 'Opportunit Product is created.');

		Test.stopTest();
	}

	@IsTest
	static void testGetMobileProducts() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		List<OE_Product__c> mobileProducts = OrderEntryProductController.getMobileProductsAndSIMCards(opp.Id);
		Test.stopTest();

		Set<String> types = new Set<String>();

		for (OE_Product__c oep : mobileProducts) {
			types.add(oep.Type__c);
		}

		System.assert(types.size() > 0, 'There must be one mobile product');
	}

	@IsTest
	static void testGetMobileContractTerms() {
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		List<OE_Product__c> mobileProducts = OrderEntryProductController.getMobileProductsAndSIMCards(opp.Id);
		Map<String, List<String>> mobileConractTerms = OrderEntryProductController.getMobileContractTerms(mobileProducts);
		Test.stopTest();

		System.assert(mobileConractTerms.get(mobileProducts[0].Id)[0] == '2', 'Incorect contract terms');
	}

	@IsTest
	static void testGetProductDependencies() {
		List<OE_Product__c> products = [SELECT Id, Parent_Product__c FROM OE_Product__c LIMIT 2];
		products[0].Parent_Product__c = products[1].Id;
		update products[0];
		Test.startTest();
		Map<String, List<String>> result = OrderEntryProductController.getProductDependencies();
		Test.stopTest();

		System.assert(!result.isEmpty(), 'There are not any dependencies');
	}
}
