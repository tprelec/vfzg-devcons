/**
 * @description     This class contains the minimum structure for the WebService classes extending the BaseWebService           
 * @author          Guy Clairbois
 */
public interface IWebService<T, U> {

    void setRequest(T request);
    U getResponse();
    void setWebServiceConfig(IWebServiceConfig webserviceConfig);
    void makeRequest(String requestType);
    
}