@isTest
private class TestAccordionFormController {
	@isTest
	private static void testAccordionFormControllerParse() {
		List<accordionFormController.accordionFormField> listFieldsRow1 = new List<accordionFormController.accordionFormField>();
		accordionFormController.accordionFormField acc1 = new accordionFormController.accordionFormField();
		acc1.fieldId = 'ClientNumber';
		acc1.fieldLabel = 'Client Number';
		acc1.fieldeReference = 'ClientNumber';
		acc1.fieldApiName = 'ClientNumber';
		acc1.isRequired = false;
		acc1.fieldTypeString = 'text';
		acc1.value = '';
		acc1.isInput = true;
		listFieldsRow1.add(acc1);

		accordionFormController.accordionFormField acc2 = new accordionFormController.accordionFormField();
		acc2.fieldId = 'ExistingSubscription';
		acc2.fieldLabel = 'Existing subscription';
		acc2.fieldeReference = 'ExistingSubscription';
		acc2.fieldApiName = 'ExistingSubscription';
		acc2.isRequired = false;
		acc2.value = '';
		acc2.fieldObjectApiName = 'Opportunity';
		acc2.isInput = false;
		acc2.fieldTypeString = 'date';
		acc2.isBlank = true;
		listFieldsRow1.add(acc2);

		List<accordionFormController.accordionSectionRow> listRow = new List<accordionFormController.accordionSectionRow>();
		accordionFormController.accordionSectionRow listRow1 = new accordionFormController.accordionSectionRow();
		listRow1.listFields = listFieldsRow1;
		listRow.add(listRow1);

		accordionFormController.accordionSection accSection = new accordionFormController.accordionSection();
		accSection.tittleLabel = 'Site 1';
		accSection.tittleReference = 'Site1';
		accSection.listRows = listRow;

		List<accordionFormController.accordionSection> lstAccordionSection = new List<accordionFormController.accordionSection>();
		lstAccordionSection.add(accSection);

		Test.startTest();
		LightningResponse response = AccordionFormController.parseJsonInput(JSON.serialize(lstAccordionSection));
		Test.stopTest();

		System.assert(response.body != null, 'Response variant must be success');
	}
}
