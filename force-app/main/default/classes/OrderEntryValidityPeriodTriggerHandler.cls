public with sharing class OrderEntryValidityPeriodTriggerHandler extends TriggerHandler {
	private List<OE_Validity_Period__c> newOEValidityPeriods;
	private Map<Id, OE_Validity_Period__c> newOEValidityPeriodsMap;
	private Map<Id, OE_Validity_Period__c> oldOEValidityPeriodsMap;

	private void init() {
		newOEValidityPeriods = (List<OE_Validity_Period__c>) this.newList;
		newOEValidityPeriodsMap = (Map<Id, OE_Validity_Period__c>) this.newMap;
		oldOEValidityPeriodsMap = (Map<Id, OE_Validity_Period__c>) this.oldMap;
	}

	public override void beforeInsert() {
		init();
		setExternalIds();
	}

	public void setExternalIds() {
		Sequence seq = new Sequence('OE_Validity_Period');

		if (seq.canBeUsed()) {
			seq.startMutex();
			for (OE_Validity_Period__c o : newOEValidityPeriods) {
				o.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}