/**
 * Created by nikola.culej on 13.7.2020..
 */

global with sharing class SubscriptionSpecification {

    public SubscriptionSpecification(String name, List<CS_EDMServiceSpecification> serviceSpecificationList, String subId) {
        this.serviceName = name;
        this.serviceSpecificationList = serviceSpecificationList;
        this.subscriptionId = subId;
    }
    
    public List<CS_EDMServiceSpecification> serviceSpecificationList { get; set; }
    public String serviceName { get; set; }
    public String subscriptionId { get; set; }
}