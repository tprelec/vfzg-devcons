global with sharing class CS_ProductConfigurationLookup extends cscfga.ALookupSearch {
	public override List<Object> doLookupSearch(
		Map<String, String> searchFields,
		String productDefinitionId,
		List<Id> excludeIds,
		Integer pageOffset,
		Integer pageLimit
	) {
		final String siteConnect = 'Site Connect';
		String accountId = searchFields.get('AccountID');
		String basketId = searchFields.get('BasketId');
		String siteId = searchFields.get('SiteId');
		System.debug(Logginglevel.DEBUG, 'Input parameters: SiteId: ' + siteId + ' AccountID: ' + accountId + ' BasketId: ' + basketId);

		List<cscfga__Product_Configuration__c> pcs = [
			SELECT Id, Name, cscfga__Description__c, cscfga__Product_Family__c, cscfga__Serial_Number__c, Site__c
			FROM cscfga__Product_Configuration__c
			WHERE cscfga__Product_Basket__c = :basketId AND Site__c = :siteId AND cscfga__Product_Definition__r.Name = :siteConnect
		];

		System.debug(Logginglevel.DEBUG, 'pcs.size: ' + pcs.size());

		List<csord__Service__c> services = [
			SELECT
				csordtelcoa__Product_Configuration__r.Id,
				csordtelcoa__Product_Configuration__r.Name,
				csordtelcoa__Product_Configuration__r.cscfga__Description__c,
				csordtelcoa__Product_Configuration__r.cscfga__Product_Family__c,
				csordtelcoa__Product_Configuration__r.cscfga__Serial_Number__c,
				csordtelcoa__Product_Configuration__r.Site__c
			FROM csord__Service__c
			WHERE
				csordtelcoa__Product_Basket__r.csbb__Account__c = :accountId
				AND csordtelcoa__Product_Configuration__c != NULL
				AND csordtelcoa__Product_Configuration__r.Site__c = :siteId
				AND csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name = :siteConnect
		];

		for (csord__Service__c service : services) {
			pcs.add(service.csordtelcoa__Product_Configuration__r);
		}

		System.debug(Logginglevel.DEBUG, 'pcs.size 2: ' + pcs.size());

		return pcs;
	}

	public override String getRequiredAttributes() {
		return '["AccountID", "BasketId", "SiteId"]';
	}
}
