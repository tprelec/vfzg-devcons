public with sharing class ManageExistingInfrasController {
	// Saving Infras edited in the datatable
	@AuraEnabled
	public static String saveInfras(List<Site_Postal_Check__c> spcs) {
		try {
			upsert spcs;
			return 'SUCCESS';
		} catch (DMLException ex) {
			return ex.getMessage();
		} catch (Exception ex) {
			return ex.getMessage();
		}
	}
	// Saving newly Added Infras
	@AuraEnabled
	public static List<Site_Postal_Check__c> saveNewInfras(List<Site_Postal_Check__c> spcs) {
		List<Site_Postal_Check__c> newSpcs = new List<Site_Postal_Check__c>();
		for (Site_Postal_Check__c spc : spcs) {
			Site_Postal_Check__c newSpc = new Site_Postal_Check__c();
			newSpc.Access_Vendor__c = spc.Access_Vendor__c;
			newSpc.Access_Result_Check__c = spc.Access_Result_Check__c;
			newSpc.Total_MB_Existing_EVC__c = spc.Total_MB_Existing_EVC__c;
			newSpc.Access_Site_ID__c = spc.Access_Site_ID__c;
			newSpc.Access_Site_ID__r = spc.Access_Site_ID__r;
			newSpc.Technology_Type__c = spc.Technology_Type__c;
			newSpc.Accessclass__c = spc.Accessclass__c;
			newSpc.Access_Max_Down_Speed__c = spc.Access_Max_Down_Speed__c;
			newSpc.Access_Max_Up_Speed__c = spc.Access_Max_Up_Speed__c;
			newSpc.Existing_Infra__c = true;
			newSpc.Access_Active__c = true;
			newSpcs.add(newSpc);
		}
		insert newSpcs;
		return newSpcs;
	}
	// Deleting selected infras from datatable
	@AuraEnabled
	public static String deleteInfras(List<Site_Postal_Check__c> spcs) {
		try {
			delete spcs;
			return 'SUCCESS';
		} catch (DMLException ex) {
			return ex.getMessage();
		} catch (Exception ex) {
			return ex.getMessage();
		}
	}
}
