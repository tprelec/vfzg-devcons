public with sharing class BillingAccountTriggerHandler extends TriggerHandler {
    private List<csconta__Billing_Account__c> newAccounts;
    private List<csconta__Billing_Account__c> oldAccounts;
    private Map<Id, csconta__Billing_Account__c> newAccountsMap;
    private Map<Id, csconta__Billing_Account__c> oldAccountsMap;

    private void init() {
        oldAccounts = (List<csconta__Billing_Account__c>) this.oldList;
        newAccounts = (List<csconta__Billing_Account__c>) this.newList;
        oldAccountsMap = (Map<Id, csconta__Billing_Account__c>) this.oldMap;
        newAccountsMap = (Map<Id, csconta__Billing_Account__c>) this.newMap;
    }

    public override void beforeInsert() {
        if (isTriggerActive()) {
            init();
            IBANService.validateIBAN(newAccounts);
            setDefaultBillingAccount();
        }
    }

    public override void beforeUpdate() {
        if (isTriggerActive()) {
            init();
            IBANService.validateIBAN(newAccounts);
            setDefaultBillingAccount();
        }
    }

    /**
     * For all the new/updated Billing Accounts that have a LG_Default__c set to true,
     * related Billing Accounts that belong to the same Account, must have the LG_Default__c
     * set to false - as we only allow one default Billing Account per Account
     */
    private void setDefaultBillingAccount() {
        //holds a set of Billing Accounts applicable for processing
        Set<csconta__Billing_Account__c> applicableBillingAccs = new Set<csconta__Billing_Account__c>();

        // Decide which accounts should be processed:
        // - only the accounts that are having a value change in the LG_Default__c, set to true
        for (csconta__Billing_Account__c newBillAcc : newAccounts) {
            Boolean isDefault =
                newBillAcc.LG_Default__c &&
                GeneralUtils.isRecordFieldChanged(newBillAcc, oldAccountsMap, 'LG_Default__c');

            if (isDefault) {
                applicableBillingAccs.add(newBillAcc);
            }
        }

        if (!applicableBillingAccs.isEmpty()) {
            updateDefaultAccounts(applicableBillingAccs);
        }
    }

    /**
     * Update other billing accounts from same account LG_Default__c to false if current one LG_Default__c is set to true
     */
    private void updateDefaultAccounts(Set<csconta__Billing_Account__c> applicableBillingAccs) {
        //Holds the AccountId to Default Billing Account map
        Map<Id, csconta__Billing_Account__c> accountToDefAccount = new Map<Id, csconta__Billing_Account__c>();
        //Holds the updated bill account ids
        Set<Id> updatedBillAccIds = new Set<Id>();

        for (csconta__Billing_Account__c billAcc : applicableBillingAccs) {
            accountToDefAccount.put(billAcc.csconta__Account__c, billAcc);
            if (billAcc.Id != null) {
                updatedBillAccIds.add(billAcc.Id);
            }
        }

        //Set Default flag to False for all currently default billing accounts that are not
        //in the trigger context, but relate to the Accounts of the applicable billing accounts
        //from the trigger
        List<csconta__Billing_Account__c> billAccsToDefaultFalse = [
            SELECT LG_Default__c
            FROM csconta__Billing_Account__c
            WHERE
                csconta__Account__c IN :accountToDefAccount.keySet()
                AND Id NOT IN :updatedBillAccIds
                AND LG_Default__c = TRUE
        ];

        for (csconta__Billing_Account__c billAcc : billAccsToDefaultFalse) {
            billAcc.LG_Default__c = false;
        }

        if (!billAccsToDefaultFalse.isEmpty()) {
            update billAccsToDefaultFalse;
        }

        //Undefault all the billing accounts that relate to the same Account, but are in the trigger context.
        //So at the end we are left with only one Default billing account
        for (csconta__Billing_Account__c billAcc : applicableBillingAccs) {
            if (accountToDefAccount.get(billAcc.csconta__Account__c) != billAcc) {
                billAcc.LG_Default__c = false;
            }
        }
    }
}