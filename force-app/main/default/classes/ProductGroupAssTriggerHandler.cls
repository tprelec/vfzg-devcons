public with sharing class ProductGroupAssTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<Product_Group_Association__c> newProducts = (List<Product_Group_Association__c>) this.newList;
		Sequence seq = new Sequence('Product Group Association');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (Product_Group_Association__c o : newProducts) {
				o.External_ID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
