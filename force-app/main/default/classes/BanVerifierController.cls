// Used by BanVerifier LWC Control
@SuppressWarnings('PMD.ExcessiveParameterList')
public with sharing class BanVerifierController {
	public static final String STATUS_STATUS = 'status';
	public static final String STATUS_AVAILABLE = 'available';
	public static final String STATUS_HAS_FIXED = 'hasFixed';
	public static final String STATUS_EXTERNAL_ACCT_ID = 'externalAccountId';
	public static final String STATUS_REL_EXTERNAL_ACCT_ID = 'relatedExternalAccountId';
	public static final String REQUEST_NEW_BAN = 'Requested New Unify BAN';
	public static final String STAT_SUBMITTED = 'Submitted';
	public static final String STAT_ACCEPTED = 'Accepted';
	public static final String STAT_REQUESTED = 'Requested';
	public static final String STAT_CLOSED = 'Closed';

	// Returns the current ban status based on many validations
	@AuraEnabled(cacheable=true)
	public static Map<String, String> getBanStatus(String banNumberId) {
		Map<String, String> mpResult = new Map<String, String>();
		try {
			Ban__c currBan = [
					SELECT
						BAN_Status__c,
						Has_Fixed__c,
						ExternalAccount__c,
						ExternalAccount__r.External_Account_Id__c,
						ExternalAccount__r.Related_External_Account__c,
						ExternalAccount__r.Related_External_Account__r.External_Account_Id__c
					FROM Ban__c
					WHERE Id = :banNumberId
				]
				?.get(0);
			String banStatus = currBan.BAN_Status__c;
			Boolean bClosedNonStandardOpps = Special_Authorizations__c.getInstance().Close_NonStandard_Opportunities__c;
			Boolean numAvailable = true;
			String externalAccountId = currBan?.ExternalAccount__r?.External_Account_Id__c;
			String relatedExternalAccountId = currBan?.ExternalAccount__r?.Related_External_Account__r?.External_Account_Id__c;
			String sHasFixed = currBan.Has_Fixed__c ? 'Y' : 'N';

			externalAccountId = String.isEmpty(externalAccountId) ? '--' : externalAccountId;
			relatedExternalAccountId = String.isEmpty(relatedExternalAccountId) ? '--' : relatedExternalAccountId;

			if (!bClosedNonStandardOpps && banStatus == STAT_CLOSED) {
				numAvailable = false;
			}
			mpResult.put(STATUS_STATUS, banStatus);
			mpResult.put(STATUS_AVAILABLE, String.valueOf(numAvailable));
			mpResult.put(STATUS_EXTERNAL_ACCT_ID, externalAccountId);
			mpResult.put(STATUS_REL_EXTERNAL_ACCT_ID, relatedExternalAccountId);
			mpResult.put(STATUS_HAS_FIXED, sHasFixed);
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
		return mpResult;
	}

	// Returns the current Ban Number given an Entity and its Id (Opportunity)
	@SuppressWarnings('PMD.ApexSOQLInjection')
	@AuraEnabled
	public static Map<String, String> getAssociatedAttributes(String objectId, String banFieldName, String acctFieldName) {
		Map<String, String> mpResult = new Map<String, String>();
		try {
			Id recordId = Id.valueOf(objectId);
			String sObjectApiName = recordId.getSObjectType().getDescribe().getName();

			String soqlQuery = 'SELECT ' + banFieldName + ', ' + acctFieldName + ' FROM ' + sObjectApiName + ' WHERE Id = \'' + objectId + '\'';

			List<sObject> resultList = Database.query(soqlQuery);
			String banFieldValue = '';
			if (resultList?.get(0)?.get(banFieldName) != null) {
				banFieldValue = String.valueOf(resultList[0].get(banFieldName));
			}
			mpResult.put('banFieldValue', banFieldValue);
			mpResult.put('acctFieldValue', String.valueOf(resultList[0].get(acctFieldName)));

			// Bizrule specific for an object type
			Boolean disabled = false;
			if (sObjectApiName == 'Opportunity') {
				disabled = getEditabilityOnOpportunity(recordId);
			}
			mpResult.put('disabled', String.valueOf(disabled));
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
		return mpResult;
	}

	@AuraEnabled
	public static Map<String, String> requestNew(String recordId, String currentBanFieldName, String accountId) {
		try {
			// create Unify BAN placeholder
			Ban__c banToInsert = new Ban__c(Name = REQUEST_NEW_BAN + ' ' + system.now(), Account__c = accountId, BAN_Status__c = STAT_REQUESTED);
			insert banToInsert;
			Map<String, String> mapResponse = new Map<String, String>();

			mapResponse.put('Id', banToInsert.Id);
			mapResponse.put('Name', banToInsert.Name);

			BanVerifierController.save(recordId, banToInsert.Id, currentBanFieldName, accountId);

			return mapResponse;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled
	public static void save(String recordId, String currentBanId, String currentBanFieldName, String accountId) {
		try {
			// For partner user we need to create manual sharing on the account,
			// if the current user is not the owner
			if (GeneralUtils.currentUserIsPartnerUser()) {
				Account a = [SELECT Id, OwnerId FROM Account WHERE Id = :accountId];
				if (
					GeneralUtils.getPartnerUserRoleGroupIdFromUserId(a.OwnerId) !=
					GeneralUtils.getPartnerUserRoleGroupIdFromUserId(System.Userinfo.getUserId())
				) {
					createAccountSharing(accountId, Userinfo.getUserId());
				}
			}
			Id iRecordId = Id.valueOf(recordId);
			SObject o1 = iRecordId.getSObjectType().newSObject(iRecordId);
			// Set the Name field dynamically
			o1.put(currentBanFieldName, currentBanId);
			update o1;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	public static void createAccountSharing(Id accountId, String userId) {
		AccountShare accShare = new AccountShare();
		accShare.AccountId = accountId;
		accShare.UserOrGroupId = GeneralUtils.getPartnerUserRoleGroupIdFromUserId(userId);
		accShare.CaseAccessLevel = 'None';
		accShare.OpportunityAccessLevel = 'None';
		accShare.AccountAccessLevel = 'Edit';

		insert accShare;
	}

	private static Boolean getEditabilityOnOpportunity(Id recordId) {
		List<Order__c> orderLst = [
			SELECT Id
			FROM Order__c
			WHERE VF_Contract__r.Opportunity__c = :recordId AND Status__c IN (:STAT_SUBMITTED, :STAT_ACCEPTED)
		];
		List<VF_Contract__c> contractLst = [SELECT Id FROM VF_Contract__c WHERE Opportunity__c = :recordId];
		if (orderLst.isEmpty() && contractLst.isEmpty()) {
			return false;
		} else {
			return true;
		}
	}
}
