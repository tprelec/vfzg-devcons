public with sharing class FeatureFlagging {

    public static Boolean isActive(String featureName) {
        Boolean result;
        Feature_Flagging__mdt currFlag = Feature_Flagging__mdt.getInstance(featureName);
        if(currFlag != null) {
            result = currFlag.Active__c;
        } else {
            result = false;
        }
        return result;
    }   
}