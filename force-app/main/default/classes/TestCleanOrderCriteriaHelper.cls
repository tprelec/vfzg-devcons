@isTest
private class TestCleanOrderCriteriaHelper {
	@testSetup
	static void prepareCommonTestData() {
		TestUtils.createAccount(GeneralUtils.currentUser);
		TestUtils.createContact(TestUtils.theAccount);
		TestUtils.createOpportunity(TestUtils.theAccount, Test.getStandardPricebookId());
		TestUtils.createSite(TestUtils.theAccount);
		cscfga__Product_Basket__c productBasketWithSites = new cscfga__Product_Basket__c(
			cscfga__Opportunity__c = TestUtils.theOpportunity.Id,
			csbb__Account__c = TestUtils.theAccount.Id,
			New_Portfolio__c = true
		);
		insert productBasketWithSites;
		cscfga__Product_Configuration__c productConfiguration = new cscfga__Product_Configuration__c(
			cscfga__Product_Basket__c = productBasketWithSites.Id,
			Site__c = TestUtils.theSite.Id,
			New_Portfolio__c = true
		);
		insert productConfiguration;
	}

	@IsTest
	private static void testValidateInstallationFieldsAreInvalid() {
		cscfga__Product_Configuration__c prodConfig = [SELECT Id, cscfga__Product_Basket__c FROM cscfga__Product_Configuration__c LIMIT 1];

		Test.startTest();
		String installationInformationValidationmessage = CleanOrderCriteriaHelper.validateFieldsOnOpportunity(prodConfig.cscfga__Product_Basket__c);
		Test.stopTest();

		System.assertEquals(
			System.Label.InstallationInformation_FieldsAreRequired,
			installationInformationValidationmessage,
			'We must get a validation error message.'
		);
	}

	@IsTest
	private static void testValidateInstallationFieldsAreValid() {
		Contact contact = [SELECT Id FROM Contact LIMIT 1];
		Site__c site = [SELECT Id FROM Site__c LIMIT 1];
		cscfga__Product_Configuration__c prodConfig = [SELECT Id, cscfga__Product_Basket__c FROM cscfga__Product_Configuration__c LIMIT 1];

		site.Technical_Contact__c = contact.Id;
		prodConfig.Installation_Wish_Date__c = System.today();
		update prodConfig;
		update site;

		Test.startTest();
		String installationInformationValidationmessage = CleanOrderCriteriaHelper.validateFieldsOnOpportunity(prodConfig.cscfga__Product_Basket__c);
		Test.stopTest();

		System.assertNotEquals(
			System.Label.InstallationInformation_FieldsAreRequired,
			installationInformationValidationmessage,
			'We must get a successfull validation message.'
		);
	}
}
