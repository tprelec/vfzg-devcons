global with sharing class CustomButtonSolutionManagement extends csbb.CustomButtonExt {
 
    global String performAction(String basketId, String pcrIds) {
        
        List<String> prodConfigRequestIds = (List<String>) System.JSON.deserialize(pcrIds, List<String>.class);
        List<csbb__product_configuration_request__c> pcrs = [
            select id, csbb__product_configuration__c,
                csbb__product_configuration__r.cssdm__solution_association__c
            from csbb__product_configuration_request__c
            where id in :prodConfigRequestIds
        ];
        Set<String> selectedSolutions = new Set<String>();
        for (csbb__product_configuration_request__c pcr : pcrs) {
            if (pcr.csbb__product_configuration__r.cssdm__solution_association__c != null) {
                selectedSolutions.add(pcr.csbb__product_configuration__r.cssdm__solution_association__c);
            }
        }
        if (!selectedSolutions.isEmpty() && selectedSolutions.size() > 1) {
            return '{"status":"error","title":"More than one network is selected","text":""}';
        } else if (!selectedSolutions.isEmpty()) {
            List<String> solutionList = new List<String>(selectedSolutions);
            return '{"status":"ok", "redirectURL":" '
                + cssmgnt.API_1.getSolutionManagementURL() + '?basketId=' + basketId + '&selectedSolution=' + solutionList[0] + '"}';
        } else {
            return '{"status":"ok", "title":"Nothing selected", "redirectURL":" '
                + cssmgnt.API_1.getSolutionManagementURL() + '?basketId=' + basketId + '" }';
        }
    }
}