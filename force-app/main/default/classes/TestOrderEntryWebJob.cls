@IsTest
private class TestOrderEntryWebJob {
	@TestSetup
	static void makeData() {
		OE_Product__c p = new OE_Product__c(Name = 'Mobile', Type__c = 'Main');
		insert p;
		OrderType__c ot = new OrderType__c(Name = 'Mobile', ExportSystem__c = 'EMAIL', Status__c = 'New', ExternalID__c = 'Test');
		insert ot;
		Id pricebookId = Test.getStandardPricebookId();
		Pricebook2 standardPricebook = new Pricebook2(Id = pricebookId, IsActive = true);
		update standardPricebook;
	}

	@IsTest
	private static void testRetrieveWebProducts() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);
		Test.startTest();
		List<String> jsonProducts = OrderEntryWebJob.retrieveProducts();
		System.assert(jsonProducts.size() > 0, 'There is no JSON products');
	}

	@IsTest
	private static void testMapProducts() {
		String jsonProducts = [SELECT Body FROM StaticResource WHERE Name = 'OrderEntryWebJSON' LIMIT 1].Body.toString();
		OrderEntryWebJSON products = (OrderEntryWebJSON) JSON.deserialize(jsonProducts, OrderEntryWebJSON.class);
		OrderEntryWebJob.mapProducts(products);
		System.assert([SELECT Id FROM Product2].size() > 0, 'Product are not created');
	}

	@IsTest
	private static void testExecuteJob() {
		StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
		mock.setStaticResource('OrderEntryWebJSON');
		mock.setStatusCode(200);
		mock.setHeader('Content-Type', 'application/json');
		Test.setMock(HttpCalloutMock.class, mock);
		Test.startTest();
		OrderEntryWebJob job = new OrderEntryWebJob();
		job.execute(null);
		Test.stopTest();
		System.assert([SELECT Id FROM Product2].size() > 0, 'Product are not creeated');
	}
}
