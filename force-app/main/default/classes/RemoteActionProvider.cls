/**
 * @description Handler for all Remote Action methods called from javascript resources
 * Provides extensibility to Solution Console in cases when some information in needed from the ORG, but it is not available in the Solution Console underlying data model
 */

global with sharing class RemoteActionProvider implements cssmgnt.RemoteActionDataProvider {
	private static final String DISCOUNT_VERSION = '3-0-0';

	private static Map<String, Object> inputMap;
	private static Map<String, Object> outputMap;

	@TestVisible
	private static ProductBasketService productBasketService {
		get {
			if (productBasketService == null) {
				productBasketService = new ProductBasketService();
			}
			return productBasketService;
		}
		set;
	}

	@TestVisible
	private static CommercialProductService commercialProductService {
		get {
			if (commercialProductService == null) {
				commercialProductService = new CommercialProductService();
			}
			return commercialProductService;
		}
		set;
	}

	/**
	 * @description Generic dispatcher for all remote methods called from the JS code. The particular method needs to be specified in the "method" parameter.
	 * @param inputData Map of all input parameters, including "method" parameter and then all input parameters for the specific method
	 * @return outputMap - result specific for each method
	 */
	@RemoteAction
	global static Map<String, Object> getData(Map<String, Object> inputData) {
		inputMap = inputData;
		outputMap = new Map<String, Object>();

		String method = String.valueOf(inputMap.get('method'));
		System.debug(LoggingLevel.DEBUG, 'Remote action method: ' + method);
		switch on method {
			when 'productBasketDetails' {
				getProductBasketDetails();
			}
			when 'applyAutomaticDiscount' {
				applyAutomaticDiscount();
			}
			when 'applyAutomaticDiscountAddOn' {
				applyAutomaticDiscountAddOn();
			}
			when 'getSdwans' {
				getSdwans();
			}
			when 'defaultHighestBandwidthAccess' {
				defaultHighestBandwidthAccess();
			}
			when 'defaultHighestBandwidthSiteConnect' {
				defaultHighestBandwidthSiteConnect();
			}
			when 'getCommercialProductDetails' {
				getCommercialProductDetails();
			}
			when 'incrementMacdContractNumber' {
				incrementMacdContractNumber();
			}
			when 'calculateListPriceJS' {
				calculateListPrice();
			}
			when 'checkUniqueSolutionConstraint' {
				checkUniqueSolutionConstraint();
			}
		}
		return outputMap;
	}

	/*
	 * @description For solutions which need to be unique per account checks whether there is already defined solution on particular account
	 */
	public static void checkUniqueSolutionConstraint() {
		System.debug(LoggingLevel.DEBUG, 'checkUniqueSolutionConstraint');
		String solutionName = String.valueOf(inputMap.get('solutionName'));
		String basketId = String.valueOf(inputMap.get('basketId'));

		try {
			cscfga__Product_Basket__c basket = ProductBasketService.queryProductBasketDetails(basketId);
			Boolean result = CS_SolutionValidationService.canAddAcquisitionSolution(basket.csbb__Account__c, solutionName);
			outputMap.put('status', true);
			outputMap.put('canAddSolution', result);
			System.debug(LoggingLevel.DEBUG, 'canAddSolution' + result);
		} catch (Exception ex) {
			outputMap.put('status', false);
			outputMap.put('message', ex.getMessage());
			System.debug(LoggingLevel.DEBUG, 'outputMap exception' + outputMap);
		}
	}

	/*
	 * @description Called after the solution has been saved.
	 * Calculates of the Recurring_Product_List_Price__c and OneOff_Product_List_Price__c fields for all configurations.
	 */
	public static void calculateListPrice() {
		System.debug(LoggingLevel.DEBUG, 'calculateListPrice');
		String pcs = String.valueOf(inputMap.get('pcs'));
		String basketId = String.valueOf(inputMap.get('basketId'));

		List<RemoteActionProvider.ConfigurationInfo> pcsDeserializedList = (List<RemoteActionProvider.ConfigurationInfo>) JSON.deserialize(
			pcs,
			List<RemoteActionProvider.ConfigurationInfo>.class
		);

		List<String> pcGuids = new List<String>();
		for (ConfigurationInfo listItem : pcsDeserializedList) {
			pcGuids.add(listItem.configurationGuid);
		}

		List<cscfga__Attribute__c> guidAtts = new List<cscfga__Attribute__c>();
		guidAtts = [
			SELECT cscfga__Product_Configuration__r.Id, cscfga__Value__c
			FROM cscfga__Attribute__c
			WHERE Name IN ('GUID') AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basketId
		];

		List<String> pcIdList = new List<String>();
		for (cscfga__Attribute__c att : guidAtts) {
			for (String pcGUID : pcGuids) {
				if (att.cscfga__Value__c == pcGUID) {
					pcIdList.add(att.cscfga__Product_Configuration__r.Id);
				}
			}
		}

		List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>();
		configs = [
			SELECT Id, Name, cscfga__Parent_Configuration__c, New_Portfolio__c, Recurring_Product_List_Price__c, OneOff_Product_List_Price__c
			FROM cscfga__Product_Configuration__c
			WHERE Id IN :pcIdList
		];

		if (configs.size() == 0) {
			System.debug(LoggingLevel.DEBUG, 'RemoteActionProvider.calculateListPrice - Cannot find configs that match the criteria: ' + pcIdList);
			outputMap.put('status', false);
			outputMap.put('message', 'Cannot find configs with Ids ' + pcIdList);
			return;
		}

		List<cscfga__Attribute__c> atts = new List<cscfga__Attribute__c>();
		atts = [
			SELECT Id, Name, cscfga__List_Price__c, cscfga__Recurring__c, cscfga__Product_Configuration__c
			FROM cscfga__Attribute__c
			WHERE cscfga__Product_Configuration__r.Id IN :pcIdList AND Name IN ('BaseMRC', 'BaseOTC')
		];

		//calculates OneOff_Product_List_Price__c and Recurring_Product_List_Price__c for all configs
		List<cscfga__Product_Configuration__c> configsForSum = sumProductListPrices(configs, atts);

		//returns all configurations that do not have a parent configuration
		List<cscfga__Product_Configuration__c> mainConfigs = getConfigsWithoutParent(configsForSum);

		//returns all configurations that have a parent configuration
		List<cscfga__Product_Configuration__c> childConfigs = getConfigsWithParent(configsForSum);

		//Checks whether configurations with no children contain oneOff and recurring values. If there are children with values, sum their values for the parent configuration.
		List<cscfga__Product_Configuration__c> pcsForUpdate = sumProductListPricesForParent(configsForSum, mainConfigs);

		pcsForUpdate.addAll(childConfigs);
		update pcsForUpdate;

		outputMap.put('status', true);
		outputMap.put('message', 'Updated succesfully');
	}

	public static List<cscfga__Product_Configuration__c> getConfigsWithoutParent(List<cscfga__Product_Configuration__c> configs) {
		List<cscfga__Product_Configuration__c> result = new List<cscfga__Product_Configuration__c>();
		for (cscfga__Product_Configuration__c config : configs) {
			if (config.cscfga__Parent_Configuration__c == null) {
				result.add(config);
			}
		}
		return result;
	}

	public static List<cscfga__Product_Configuration__c> getConfigsWithParent(List<cscfga__Product_Configuration__c> configs) {
		List<cscfga__Product_Configuration__c> result = new List<cscfga__Product_Configuration__c>();

		for (cscfga__Product_Configuration__c config : configs) {
			if (config.cscfga__Parent_Configuration__c != null) {
				result.add(config);
			}
		}
		return result;
	}

	public static List<cscfga__Product_Configuration__c> sumProductListPrices(
		List<cscfga__Product_Configuration__c> configs,
		List<cscfga__Attribute__c> attList
	) {
		List<cscfga__Product_Configuration__c> result = new List<cscfga__Product_Configuration__c>();

		for (cscfga__Product_Configuration__c config : configs) {
			if (config.New_Portfolio__c != false) {
				Decimal oneOff = 0.00;
				Decimal recurring = 0.00;
				for (cscfga__Attribute__c att : attList) {
					if (att.cscfga__Product_Configuration__c == config.Id) {
						if (att.cscfga__Recurring__c == true) {
							if (att.cscfga__List_Price__c != null) {
								recurring += att.cscfga__List_Price__c;
							}
						} else {
							if (att.cscfga__List_Price__c != null) {
								oneOff += att.cscfga__List_Price__c;
							}
						}
					}
				}
				config.OneOff_Product_List_Price__c = oneOff;
				config.Recurring_Product_List_Price__c = recurring;
				result.add(config);
			}
		}
		return result;
	}

	public static List<cscfga__Product_Configuration__c> sumProductListPricesForParent(
		List<cscfga__Product_Configuration__c> allConfigs,
		List<cscfga__Product_Configuration__c> parentConfigs
	) {
		for (cscfga__Product_Configuration__c parent : parentConfigs) {
			List<cscfga__Product_Configuration__c> childConfigs = new List<cscfga__Product_Configuration__c>();

			for (cscfga__Product_Configuration__c child : allConfigs) {
				if (child.cscfga__Parent_Configuration__c == parent.Id) {
					childConfigs.add(child);
				}
			}

			if (childConfigs.size() > 0) {
				Decimal oneOff = 0.00;
				Decimal recurring = 0.00;
				for (cscfga__Product_Configuration__c childCon : childConfigs) {
					oneOff += childCon.OneOff_Product_List_Price__c;
					recurring += childCon.Recurring_Product_List_Price__c;
				}
				parent.OneOff_Product_List_Price__c = oneOff;
				parent.Recurring_Product_List_Price__c = recurring;
			}
		}
		return parentConfigs;
	}

	/*
	 * @description Called after configuration is added to MACD basket
	 * Retrieves the config record and calls incrementMacdContractNumber
	 * Since Solution Console doesn't have Product Configuration Id for addon configurations, mandatory Attribute named GUID is used to filter the configurations
	 */
	public static void incrementMacdContractNumber() {
		String configurationGuid = String.valueOf(inputMap.get('configurationGuid'));
		String configurationId = String.valueOf(inputMap.get('configurationId'));
		String basketId = String.valueOf(inputMap.get('basketId'));
		List<cscfga__Attribute__c> attributes = [
			SELECT
				Id,
				cscfga__Product_Configuration__r.Id,
				cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c,
				cscfga__Product_Configuration__r.ContractNumber_JSON__c,
				cscfga__Product_Configuration__r.Contract_Number__c,
				cscfga__Product_Configuration__r.Contract_Number_Group__c,
				cscfga__Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,
				cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csordtelcoa__Change_Type__c,
				cscfga__Value__c
			FROM cscfga__Attribute__c
			WHERE
				Name = 'GUID'
				AND cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basketId
				AND (cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c = :configurationId
				OR cscfga__Product_Configuration__r.Id = :configurationId)
		];

		if (attributes == null || attributes.size() == 0) {
			System.debug(
				LoggingLevel.DEBUG,
				'RemoteActionProvider.incrementMacdContractNumber - Cannot find configs that match the criteria: ' + configurationGuid
			);
			outputMap.put('status', false);
			outputMap.put('message', 'Cannot find att with cscfga__Value__c ' + configurationGuid);
			return;
		}
		cscfga__Product_Configuration__c macdConfig;

		for (cscfga__Attribute__c att : attributes) {
			if (att.cscfga__Value__c == configurationGuid) {
				macdConfig = att.cscfga__Product_Configuration__r;
				break;
			}
		}

		if (macdConfig == null) {
			System.debug(
				LoggingLevel.DEBUG,
				'RemoteActionProvider.incrementMacdContractNumber - Cannot find attribute with GUID value: ' + configurationGuid
			);
			outputMap.put('status', false);
			outputMap.put('message', 'Cannot find att with cscfga__Value__c ' + configurationGuid);
			return;
		}
		System.debug(LoggingLevel.DEBUG, 'MACD config: ' + macdConfig);

		CS_ContractNumberGenerator.incrementMacdContractNumber(macdConfig);
		update macdConfig;

		outputMap.put('status', true);
		outputMap.put('message', macdConfig.Id);
	}

	/*
	 * @description Applies the discount on main configurations i.e. configurations based on the Price Item (Commercial Product)
	 * Based on the Commercial Product linked to the input Product Configuration, associated Discount Level is fetched and serialized to the configuration discount field
	 *
	 */
	public static void applyAutomaticDiscount() {
		String productConfigurationCommercialProductMap = String.valueOf(inputMap.get('pCcPMap'));
		System.debug(LoggingLevel.DEBUG, 'inputMap' + inputMap);

		System.debug(LoggingLevel.DEBUG, ' -- productConfigurationCommercialProductMap -- ' + productConfigurationCommercialProductMap);

		List<RemoteActionProvider.ProductConfigurationCommercialProductWrapper> pCcPDeserializedList = (List<RemoteActionProvider.ProductConfigurationCommercialProductWrapper>) JSON.deserialize(
			productConfigurationCommercialProductMap,
			List<RemoteActionProvider.ProductConfigurationCommercialProductWrapper>.class
		);

		System.debug(LoggingLevel.DEBUG, ' -- pCcPDeserializedList -- ' + pCcPDeserializedList);

		try {
			if (pCcPDeserializedList == null) {
				outputMap.put('status', true);
				outputMap.put('message', 'No discount needed.');
			} else {
				if (pCcPDeserializedList.size() > 0) {
					outputMap.put('status', true);
					outputMap.put('message', 'successfully parsed');
					List<String> cpIdList = new List<String>();
					List<String> configurationIdList = new List<String>();
					Map<Id, Set<cspmb__Discount_Level__c>> cpIdDiscountLevelsMap = new Map<Id, Set<cspmb__Discount_Level__c>>();

					for (ProductConfigurationCommercialProductWrapper listItem : pCcPDeserializedList) {
						if (listItem.commercialProductId != null) {
							cpIdList.add(listItem.commercialProductId);
							cpIdDiscountLevelsMap.put(listItem.commercialProductId, new Set<cspmb__Discount_Level__c>());
						}

						if (listItem.productConfigurationId != null) {
							configurationIdList.add(listItem.productConfigurationId);
						}
					}

					System.debug(LoggingLevel.DEBUG, 'cpIdList list -> ' + cpIdList);

					List<cspmb__Discount_Association__c> automaticDiscountAssociations = [
						SELECT Id, cspmb__Add_On_Price_Item__c, cspmb__Price_Item__c, cspmb__Discount_Level__c, cspmb__Profile_Name__c
						FROM cspmb__Discount_Association__c
						WHERE cspmb__Price_Item__c IN :cpIdList
					];

					List<Id> discountLevelIds = new List<Id>();

					for (cspmb__Discount_Association__c assocItem : automaticDiscountAssociations) {
						if (assocItem.cspmb__Discount_Level__c != null) {
							discountLevelIds.add(assocItem.cspmb__Discount_Level__c);
						}
					}

					// Commercial Product can have multiple discount levels associated
					// For automatic discount we need "above the line" (atl) item
					List<cspmb__Discount_Level__c> automaticDiscountLevels = [
						SELECT
							Name,
							Id,
							cspmb__Discount_Type__c,
							cspmb__Discount__c,
							cspmb__Charge_Type__c,
							cspmb__Discount_Level_Code__c,
							cspmb__duration__c,
							csdiscounts__Description__c,
							csdiscounts__Offset__c,
							csdiscounts__Period__c,
							cspmb__offset__c
						FROM cspmb__Discount_Level__c
						WHERE Id IN :discountLevelIds AND cspmb__Discount_Level_Code__c LIKE '%atl%'
					];

					System.debug(LoggingLevel.DEBUG, 'automaticDiscountLevels list -> ' + automaticDiscountLevels);

					for (cspmb__Discount_Association__c assocItem : automaticDiscountAssociations) {
						for (cspmb__Discount_Level__c discountLevelItem : automaticDiscountLevels) {
							if (discountLevelItem.Id == assocItem.cspmb__Discount_Level__c) {
								cpIdDiscountLevelsMap.get(assocItem.cspmb__Price_Item__c).add(discountLevelItem);
							}
						}
					}

					System.debug(LoggingLevel.DEBUG, 'cpIdDiscountLevelsMap -> ' + cpIdDiscountLevelsMap);

					if (automaticDiscountLevels == null || automaticDiscountLevels.isEmpty()) {
						String result = assembleReturnMessage('SUCCESS', '201', '', '', 'No automated discount to apply');
						outputMap.put('status', true);
						outputMap.put('message', result);
					}

					Map<Id, cscfga__Product_Configuration__c> productConfigurationMap = new Map<Id, cscfga__Product_Configuration__c>(
						[
							SELECT Id, Name, cscfga__discounts__c, cscfga__Product_Basket__c, csdiscounts__manual_discounts__c
							FROM cscfga__Product_Configuration__c
							WHERE Id IN :configurationIdList
						]
					);

					List<ProductConfigurationCommercialProductWrapper> configurationsDiscountAppliedWrapper = new List<ProductConfigurationCommercialProductWrapper>();

					for (ProductConfigurationCommercialProductWrapper listItem : pCcPDeserializedList) {
						cscfga__Product_Configuration__c pcItem = productConfigurationMap.get(listItem.productConfigurationId);
						cscfga.ProductConfiguration.ProductDiscount prodDiscountArray = new cscfga.ProductConfiguration.ProductDiscount();

						for (cspmb__Discount_Level__c discountLevel : cpIdDiscountLevelsMap.get(listItem.commercialProductId)) {
							cscfga.ProductConfiguration.Discount discountItem = createMemberDiscountFrom(discountLevel);
							prodDiscountArray.discounts.add(discountItem);
						}
						System.debug('pcItem: ' + pcItem + ' prodDiscountArray: ' + prodDiscountArray);
						cscfga.ProductConfiguration.setDiscounts(pcItem, prodDiscountArray);
						pcItem.csdiscounts__manual_discounts__c = pcItem.cscfga__discounts__c;

						ProductConfigurationCommercialProductWrapper wrapper = new ProductConfigurationCommercialProductWrapper();
						wrapper.productConfigurationId = listItem.productConfigurationId;
						wrapper.discountApplied = JSON.serialize(cpIdDiscountLevelsMap.get(listItem.commercialProductId));
						configurationsDiscountAppliedWrapper.add(wrapper);
					}

					update productConfigurationMap.values();
					cscfga.ProductConfigurationBulkActions.calculateTotals(
						new Set<Id>{ productConfigurationMap.values()[0].cscfga__Product_Basket__c }
					);
					outputMap.put('status', true);
					outputMap.put('message', JSON.serialize(configurationsDiscountAppliedWrapper));
				}
			}
		} catch (Exception ex) {
			outputMap.put('status', false);
			outputMap.put('message', ex.getMessage());
		}
	}

	//COM-2347 by sowparnika

	/*
	 * @description Applies the discount on addon configurations
	 * Based on the Add On linked to the addon Product Configuration, associated Discount Level is fetched and serialized to the configuration discount field
	 * Since we don't have the addon configuration Id as an input, addon configurations need to be filtered first based on the combination of the configuration GUID attribute (unique for each configuration)
	 * and parent configuration Id which is an available input
	 */
	public static void applyAutomaticDiscountAddOn() {
		String productConfigurationAddOnMap = String.valueOf(inputMap.get('pCaddOnMap'));
		System.debug(LoggingLevel.DEBUG, 'inputMap' + inputMap);
		System.debug(LoggingLevel.DEBUG, ' -- productConfigurationAddOnMap -- ' + productConfigurationAddOnMap);
		List<RemoteActionProvider.ProductConfigurationAddOnWrapper> pCaddOnDeserializedList = (List<RemoteActionProvider.ProductConfigurationAddOnWrapper>) JSON.deserialize(
			productConfigurationAddOnMap,
			List<RemoteActionProvider.ProductConfigurationAddOnWrapper>.class
		);
		System.debug(LoggingLevel.DEBUG, ' -- pCcPDeserializedList -- ' + pCaddOnDeserializedList);
		try {
			if (pCaddOnDeserializedList == null) {
				outputMap.put('status', true);
				outputMap.put('message', 'No discount needed.');
			} else {
				if (pCaddOnDeserializedList.size() > 0) {
					Set<Id> revalidationSet = new Set<Id>();
					outputMap.put('status', true);
					outputMap.put('message', 'successfully parsed');
					List<String> addOnIdList = new List<String>();
					Map<String, List<String>> configGuidsByParentCfgId = new Map<String, List<String>>(); // for each Parent Configuration, list of addon configuration guids is associated
					Map<String, RemoteActionProvider.ProductConfigurationAddOnWrapper> inputsByGuid = new Map<String, RemoteActionProvider.ProductConfigurationAddOnWrapper>();
					Map<Id, Set<cspmb__Discount_Level__c>> addOnDiscountLevelsMap = new Map<Id, Set<cspmb__Discount_Level__c>>();

					for (ProductConfigurationAddOnWrapper listItem : pCaddOnDeserializedList) {
						if (listItem.addOnId != null) {
							addOnIdList.add(listItem.addOnId);
							addOnDiscountLevelsMap.put(listItem.addOnId, new Set<cspmb__Discount_Level__c>());
						}
						if (listItem.parentConfigurationId != null) {
							if (!configGuidsByParentCfgId.containsKey(listItem.parentConfigurationId)) {
								configGuidsByParentCfgId.put(listItem.parentConfigurationId, new List<String>());
							}
							if (listItem.configurationGuid != null) {
								configGuidsByParentCfgId.get(listItem.parentConfigurationId).add(listItem.configurationGuid);
							}
						}
						inputsByGuid.put(listItem.configurationGuid, listItem);
					}
					System.debug(LoggingLevel.DEBUG, 'addOnIdList list -> ' + addOnIdList);

					List<cspmb__Discount_Association__c> automaticDiscountAssociations = [
						SELECT Id, cspmb__Add_On_Price_Item__c, cspmb__Price_Item__c, cspmb__Discount_Level__c, cspmb__Profile_Name__c
						FROM cspmb__Discount_Association__c
						WHERE cspmb__Add_On_Price_Item__c IN :addOnIdList
					];
					List<Id> discountLevelIds = new List<Id>();

					for (cspmb__Discount_Association__c assocItem : automaticDiscountAssociations) {
						if (assocItem.cspmb__Discount_Level__c != null) {
							discountLevelIds.add(assocItem.cspmb__Discount_Level__c);
						}
					}
					List<cspmb__Discount_Level__c> automaticDiscountLevels = [
						SELECT
							Name,
							Id,
							cspmb__Discount_Type__c,
							cspmb__Discount__c,
							cspmb__Charge_Type__c,
							cspmb__Discount_Level_Code__c,
							cspmb__duration__c,
							csdiscounts__Description__c,
							csdiscounts__Offset__c,
							csdiscounts__Period__c,
							cspmb__offset__c
						FROM cspmb__Discount_Level__c
						WHERE Id IN :discountLevelIds
					];

					System.debug(LoggingLevel.DEBUG, 'automaticDiscountLevels list -> ' + automaticDiscountLevels);

					for (cspmb__Discount_Association__c assocItem : automaticDiscountAssociations) {
						for (cspmb__Discount_Level__c discountLevelItem : automaticDiscountLevels) {
							if (discountLevelItem.Id == assocItem.cspmb__Discount_Level__c) {
								addOnDiscountLevelsMap.get(assocItem.cspmb__Add_On_Price_Item__c).add(discountLevelItem);
							}
						}
					}
					System.debug(LoggingLevel.DEBUG, 'addOnDiscountLevelsMap -> ' + addOnDiscountLevelsMap);

					if (automaticDiscountLevels == null || automaticDiscountLevels.isEmpty()) {
						String result = assembleReturnMessage('SUCCESS', '201', '', '', 'No automated discount to apply');
						outputMap.put('status', true);
						outputMap.put('message', result);
						return;
					}

					// we have addon configuration guids and parent configuration Id
					// based on that inputs, let's create a list of addonConfiguration Ids
					List<Id> addonConfigsFiltered = getAddonConfigurationIds(configGuidsByParentCfgId, inputsByGuid);

					Map<Id, cscfga__Product_Configuration__c> productConfigurationMap = new Map<Id, cscfga__Product_Configuration__c>(
						[
							SELECT Id, Name, cscfga__discounts__c, csdiscounts__manual_discounts__c
							FROM cscfga__Product_Configuration__c
							WHERE Id IN :addonConfigsFiltered
						]
					);
					List<ProductConfigurationAddOnWrapper> configurationsDiscountAppliedWrapper = new List<ProductConfigurationAddOnWrapper>();

					for (ProductConfigurationAddOnWrapper listItem : inputsByGuid.values()) {
						cscfga__Product_Configuration__c pcItem = productConfigurationMap.get(listItem.addonConfigurationId);
						cscfga.ProductConfiguration.ProductDiscount prodDiscountArray = new cscfga.ProductConfiguration.ProductDiscount();

						for (cspmb__Discount_Level__c discountLevel : addOnDiscountLevelsMap.get(listItem.addOnId)) {
							cscfga.ProductConfiguration.Discount discountItem = createMemberDiscountFrom(discountLevel);
							prodDiscountArray.discounts.add(discountItem);
						}
						System.debug('pcItem: ' + pcItem + ' prodDiscountArray: ' + prodDiscountArray);
						cscfga.ProductConfiguration.setDiscounts(pcItem, prodDiscountArray);
						pcItem.csdiscounts__manual_discounts__c = pcItem.cscfga__discounts__c;
						revalidationSet.add(pcItem.Id);

						ProductConfigurationAddOnWrapper wrapper = new ProductConfigurationAddOnWrapper();
						wrapper.addonConfigurationId = listItem.addonConfigurationId;
						wrapper.discountApplied = JSON.serialize(addOnDiscountLevelsMap.get(listItem.addOnId));
						configurationsDiscountAppliedWrapper.add(wrapper);
					}
					update productConfigurationMap.values();
					outputMap.put('status', true);
					outputMap.put('message', JSON.serialize(configurationsDiscountAppliedWrapper));

					String basketId = String.valueOf(inputMap.get('basketId'));
					cscfga.ProductConfigurationBulkActions.calculateTotals(new Set<Id>{ basketId });
					System.debug(LoggingLevel.DEBUG, 'outputMap' + outputMap);
				}
			}
		} catch (Exception ex) {
			outputMap.put('status', false);
			outputMap.put('message', ex.getMessage());
			System.debug(LoggingLevel.DEBUG, 'outputMap exception' + outputMap);
		}
	}

	/*
	 * @description Retrieves addon configuration Ids based on the input map of configuration GUID attibutes mapped to the parent product configurations
	 *
	 */
	private static List<Id> getAddonConfigurationIds(
		Map<String, List<String>> configGuidsByParentCfgId,
		Map<String, RemoteActionProvider.ProductConfigurationAddOnWrapper> inputsByGuid
	) {
		List<Id> addonConfigsFiltered = new List<Id>();

		Map<Id, cscfga__Product_Configuration__c> addonConfigsAll = new Map<Id, cscfga__Product_Configuration__c>(
			[SELECT Id FROM cscfga__Product_Configuration__c WHERE cscfga__Parent_Configuration__c IN :configGuidsByParentCfgId.keySet()]
		);
		List<cscfga__Attribute__c> attributes = [
			SELECT Id, cscfga__Product_Configuration__c, cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c, cscfga__Value__c
			FROM cscfga__Attribute__c
			WHERE Name = 'GUID' AND cscfga__Product_Configuration__c IN :addonConfigsAll.keySet()
		];

		for (cscfga__Attribute__c attribute : attributes) {
			if (
				attribute.cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c != null &&
				configGuidsByParentCfgId.containsKey(attribute.cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c)
			) {
				List<String> guids = configGuidsByParentCfgId.get(attribute.cscfga__Product_Configuration__r.cscfga__Parent_Configuration__c);
				if (guids.contains(attribute.cscfga__Value__c)) {
					addonConfigsFiltered.add(attribute.cscfga__Product_Configuration__c);
					inputsByGuid.get(attribute.cscfga__Value__c).addonConfigurationId = attribute.cscfga__Product_Configuration__c;
				}
			}
		}
		return addonConfigsFiltered;
	}

	public static String assembleReturnMessage(String status, String code, String appliedDiscountCodes, String discountsJSON, String message) {
		Map<String, String> result = new Map<String, String>();
		result.put('status', status);
		result.put('code', code);
		result.put('appliedDiscountCodes', appliedDiscountCodes);
		result.put('discountsJSON', JSON.serialize(discountsJSON));
		result.put('message', message);
		return JSON.serialize(result);
	}

	/**
	 * @description Retrieves Commercial Product for default Infrastructure solution when Access configuration is created automatically
	 */
	public static void defaultHighestBandwidthAccess() {
		String technology = String.valueOf(inputMap.get('technology'));
		String contractDuration = String.valueOf(inputMap.get('contractDuration'));
		String siteVendor = String.valueOf(inputMap.get('siteVendor'));
		Integer bandwidthDown = Integer.valueOf(inputMap.get('bandwidthDown'));
		Integer bandwidthUp = Integer.valueOf(inputMap.get('bandwidthUp'));
		try {
			if (technology == null || contractDuration == null) {
				outputMap.put('status', false);
			} else {
				cspmb__Price_Item__c commercialProduct = commercialProductService.queryCommercialProductDetailsAccess(
					siteVendor,
					technology,
					contractDuration,
					bandwidthDown,
					bandwidthUp
				);
				outputMap.put('status', true);
				outputMap.put('commercialProducts', commercialProduct);
			}
		} catch (Exception ex) {
			outputMap.put('status', false);
			outputMap.put('message', ex.getMessage());
		}
	}

	/**
	 * @description Retrieves commercial Product for default Infrastructure solution when Site Connect configuration is created automatically
	 */
	public static void defaultHighestBandwidthSiteConnect() {
		String contractDuration = String.valueOf(inputMap.get('contractDuration'));
		Integer banUp = Integer.valueOf(inputMap.get('accessUp'));
		Integer banDown = Integer.valueOf(inputMap.get('accessDown'));
		Boolean ownInfra = Boolean.valueOf(inputMap.get('ownInfra'));

		try {
			if (contractDuration == null) {
				outputMap.put('status', false);
			} else {
				cspmb__Price_Item__c commercialProduct = commercialProductService.queryCommercialProductDetailsSiteConnect(
					contractDuration,
					banUp,
					banDown,
					ownInfra
				);
				outputMap.put('status', true);
				outputMap.put('commercialProducts', commercialProduct);
			}
		} catch (Exception ex) {
			outputMap.put('status', false);
			outputMap.put('message', ex.getMessage());
		}
	}
	//COM-2220 by sowparnika
	/**
	 * @description Retrieves the commercial Product for InternetModemOnly configuration
	 */
	public static void getCommercialProductDetails() {
		String contractDuration = String.valueOf(inputMap.get('contractDuration'));

		try {
			if (contractDuration == null) {
				outputMap.put('status', false);
			} else {
				cspmb__Price_Item__c commercialProduct = commercialProductService.queryCommercialProductDetailsInternetModemOnly(contractDuration);
				outputMap.put('status', true);
				outputMap.put('commercialProduct', commercialProduct);
			}
		} catch (Exception ex) {
			outputMap.put('status', false);
			outputMap.put('message', ex.getMessage());
		}
	}
	//COM-2220 by sowparnika

	/**
	 * @description Retrieves additional fields for Product Basket based on given "productBasketId" parameter.
	 * Additionally, in case basket is inflight change basket, basket configurations are fetched
	 * For each configuration, information is obtained whether the configuration is applicable for the inflight change
	 */
	public static void getProductBasketDetails() {
		String productBasketId = String.valueOf(inputMap.get('productBasketId'));

		try {
			if (productBasketId == null) {
				outputMap.put('status', false);
			} else {
				cscfga__Product_Basket__c productBasket = productBasketService.queryProductBasketDetails(productBasketId);

				outputMap.put('status', true);
				outputMap.put('basketDetails', productBasket);
				outputMap.put('opportunityDirectIndirect', productBasket.cscfga__Opportunity__r.Direct_Indirect__c);
				outputMap.put('accountId', productBasket.csbb__Account__c);

				if (productBasket.csordtelcoa__in_flight_change_type__c != null && productBasket.csordtelcoa__in_flight_change_type__c != '') {
					Map<String, ConfigurationInfo> configurations = productBasketService.queryInflightConfigurations(productBasketId);
					outputMap.put('configurations', JSON.serialize(configurations));
				}
			}
		} catch (Exception ex) {
			outputMap.put('status', false);
			outputMap.put('message', ex.getMessage());
		}
	}

	public static void getSdwans() {
		try {
			System.debug(LoggingLevel.DEBUG, 'Started getSdwans');
			List<cspmb__Price_Item__c> result = CS_SdwanLookup.getSdwans();

			if (result.size() == 0) {
				outputMap.put('status', false);
				outputMap.put('message', 'No results');
				System.debug(LoggingLevel.DEBUG, 'getSdwans - NO RESULTS');
			} else {
				outputMap.put('status', true);
				outputMap.put('message', 'Success');
				outputMap.put('productVariant', result[0].Id);
				outputMap.put('productVariantName', result[0].Name);
				System.debug(LoggingLevel.DEBUG, 'getSdwans - results ' + JSON.serializePretty(result));
			}
		} catch (Exception ex) {
			outputMap.put('status', false);
			outputMap.put('message', ex.getMessage());
			System.debug(LoggingLevel.DEBUG, 'getSdwans - FAIL ' + ex.getMessage());
		}
	}

	/*
	 * @description Creates the Discount structure based on the input Discount Level
	 */
	public static cscfga.ProductConfiguration.Discount createMemberDiscountFrom(cspmb__Discount_Level__c discLevel) {
		cscfga.ProductConfiguration.Discount discount;

		discount = new cscfga.ProductConfiguration.Discount();
		discount.recordType = 'single';

		if (discLevel.cspmb__Discount_Type__c == 'Amount') {
			discount.type = 'absolute';
		} else if (discLevel.cspmb__Discount_Type__c == 'Percentage') {
			discount.type = discLevel.cspmb__Discount_Type__c.toLowerCase();
		}

		discount.amount = discLevel.cspmb__Discount__c;
		discount.chargeType = discLevel.cspmb__Charge_Type__c.toLowerCase();
		discount.source = discLevel.csdiscounts__Description__c;
		discount.discountCharge = '__PRODUCT__';
		discount.duration = discLevel.cspmb__Duration__c;
		discount.description = discLevel.csdiscounts__Description__c;
		discount.recurringOffset = discLevel.csdiscounts__Offset__c;

		Map<String, String> customDataMap = new Map<String, String>();
		customDataMap.put('discountCode', discLevel.cspmb__Discount_Level_Code__c);
		discount.customData = customDataMap;

		discount.version = DISCOUNT_VERSION;

		return discount;
	}

	global with sharing class ProductConfigurationCommercialProductWrapper {
		global String productConfigurationId;
		global String commercialProductId;
		global String discountApplied;
	}
	//COM-2347 by sowparnika
	global with sharing class ProductConfigurationAddOnWrapper {
		global String parentConfigurationId;
		global String addonConfigurationId;
		global String configurationGuid;
		global String addOnId;
		global String discountApplied;
	} //COM-2347 by sowparnika

	global with sharing class ConfigurationInfo {
		global Id configurationId { get; set; }
		global String configurationGuid { get; set; }
		global Boolean inflightApplicable { get; set; }
		global String configurationName { get; set; }
	}

	global with sharing class ProductConfigurationPrice {
		global String pcName;
		global String pcId;
		global String pcGuid;
	}
}
