public with sharing class CS_ComDependantDataWrapper {
    @AuraEnabled
    public String productName;

    @AuraEnabled
    public List<CS_ComDependantDataDetailsWrapper> details;
}