public with sharing class PriceItemActiveLogicBatch implements Database.Batchable<sObject> {
	public Boolean processAllRecords;

	public PriceItemActiveLogicBatch(Boolean processAllRecords) {
		this.processAllRecords = processAllRecords;
	}

	public Database.QueryLocator start(Database.BatchableContext BC) {
		String query;
		if (processAllRecords) {
			query = 'SELECT cspmb__Is_Active__c, cspmb__Effective_Start_Date__c, cspmb__Effective_End_Date__c FROM cspmb__Price_Item__c';
		} else {
			query = 'SELECT cspmb__Is_Active__c, cspmb__Effective_Start_Date__c, cspmb__Effective_End_Date__c FROM cspmb__Price_Item__c WHERE cspmb__Effective_Start_Date__c = TODAY OR cspmb__Effective_End_Date__c  = TODAY';
		}
		return Database.getQueryLocator(query);
	}

	public void execute(Database.BatchableContext BC, List<sObject> scope) {
		List<cspmb__Price_Item__c> priceItems = (List<cspmb__Price_Item__c>) scope;
		System.debug(scope);
		for (cspmb__Price_Item__c pi : priceItems) {
			if (
				Date.today() >= pi.cspmb__Effective_Start_Date__c &&
				Date.today() <= pi.cspmb__Effective_End_Date__c
			) {
				pi.cspmb__Is_Active__c = true;
			} else if (
				Date.today() >= pi.cspmb__Effective_Start_Date__c &&
				pi.cspmb__Effective_End_Date__c == null
			) {
				pi.cspmb__Is_Active__c = true;
			} else {
				pi.cspmb__Is_Active__c = false;
			}
		}
		update priceItems;
	}

	public void finish(Database.BatchableContext BC) {
	}

	public class SchedulePriceItemActiveLogicBatch implements Schedulable {
		public void execute(SchedulableContext SC) {
			PriceItemActiveLogicBatch batch = new PriceItemActiveLogicBatch(false);
			database.executebatch(batch);
		}
	}
}