@IsTest
public class TestScheduleUnicaCampaignBatch {
	@IsTest
	private static void testSchedule() {
		insert new Unica_Campaign__c(Campaign_Name__c = 'Test1');
		insert new Unica_Campaign__c(
			Campaign_Name__c = 'Test2',
			Selection_Name__c = 'january',
			Ban__c = '312312311',
			NTA_Id__c = '12345678',
			Treatment_01__c = 'Test Treatment',
			Campaign_Type__c = 'Recommit'
		);

		TestUtils.autoCommit = false;

		Map<String, String> mapping = new Map<String, String>{
			'Campaign_Name__c' => 'Name',
			'Selection_Name__c' => 'Selection_Name__c',
			'Campaign_Type__c' => 'Type'
		};
		List<Field_Sync_Mapping__c> fsmList = new List<Field_Sync_Mapping__c>();

		for (String field : mapping.keySet()) {
			fsmList.add(
				TestUtils.createSync('Unica_Campaign__c -> Campaign', field, mapping.get(field))
			);
		}
		fsmList.add(
			TestUtils.createSync('Unica_Campaign__c -> CampaignMember', 'NTA_Id__c', 'NTA_Id__c')
		);
		fsmList.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'NTA_Id__c', 'NTA_Id__c'));
		fsmList.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'Ban__c', 'BAN_Number__c'));
		fsmList.add(
			TestUtils.createSync('Unica_Campaign__c -> Lead', 'Treatment_01__c', 'Treatment_01__c')
		);
		fsmList.add(
			TestUtils.createSync('Unica_Campaign__c -> Lead', 'Campaign_Name__c', 'LastName')
		);
		insert fsmList;

		Test.startTest();
		new ScheduleUnicaCampaignBatch().execute(null);
		Test.stopTest();

		List<AsyncApexJob> jobsScheduled = [SELECT Id FROM AsyncApexJob];
		System.assertEquals(true, jobsScheduled.size() > 0, 'Job should be scheduled');
	}
}