public class ProductBasketOverrideController {
	@AuraEnabled
	public static String checkUserAccess(String parentId) {
		String returnString = '';
		try {
			/**
			 * Create Basket
			 */
			Opportunity opp = [SELECT Id, OwnerId FROM Opportunity WHERE Id = :parentId];

			/**
			 * Check to see if any Oracle Quotes Exist
			 */

			String profileName = OpportunityUtils.getUserProfile(opp.OwnerId);
			List<New_Quote_Profile__mdt> quoteProfileList = [
				SELECT profile__c
				FROM New_Quote_Profile__mdt
				WHERE profile__c = :profileName
			];
			if (quoteProfileList.size() == 1) {
				cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
				basket.cscfga__Opportunity__c = parentId;
				insert basket;
				returnString = basket.Id;
			}
		} catch (Exception e) {
		}

		return returnString;
	}
}