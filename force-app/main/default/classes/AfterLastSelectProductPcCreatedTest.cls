@isTest
private class AfterLastSelectProductPcCreatedTest {
    private static testMethod void test() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        
        User simpleUser = CS_DataTest.createUser(pList, roleList);  
        insert simpleUser;

        System.runAs (simpleUser) {
            Test.startTest();

            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity opp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert opp;
            
            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opp, 'Test Basket');
            basket.csordtelcoa__Basket_Stage__c = 'Prospecting';
            basket.cscfga__Basket_Status__c = 'Valid';
            basket.Primary__c = true;
            basket.csbb__Synchronised_with_Opportunity__c = true;
            basket.csbb__Account__c = testAccount.Id;
            basket.Contract_duration_Mobile__c = '24';
            basket.Contract_duration_Fixed__c = 24;
            basket.OneNet_Scenario__c = 'One Net Enterprise';
            basket.Fixed_Scenario__c = 'One Fixed Enterprise';
            basket.Expected_delivery_date_for_Fixed__c = Date.newInstance(2018, 09, 22);
            basket.Expected_delivery_date_for_Mobile__c = Date.newInstance(2018, 09, 25);
            basket.Number_of_SIP__c = 11;
            insert basket;
            
            cscfga__Product_Definition__c pd = CS_DataTest.createProductDefinition('Mobile CTN subscription');
            pd.Product_Type__c = 'Mobile';
            insert pd;
            
            cscfga__Product_Configuration__c pcAccessInf = CS_DataTest.createProductConfiguration(pd.Id, 'Access Infrastructure',basket.Id);
            pcAccessInf.cscfga__Contract_Term__c = 24;
            pcAccessInf.cscfga__total_one_off_charge__c = 100;
            pcAccessInf.cscfga__Total_Price__c = 20;
            pcAccessInf.cspl__Type__c = 'Mobile Voice Services';
            pcAccessInf.cscfga__Contract_Term_Period__c = 12;
            pcAccessInf.cscfga__Configuration_Status__c = 'Valid';
            pcAccessInf.cscfga__Quantity__c = 1;
            pcAccessInf.cscfga__total_recurring_charge__c = 22;
            pcAccessInf.RC_Cost__c = 0;
            pcAccessInf.NRC_Cost__c = 0;
            pcAccessInf.OneNet_Scenario__c = 'One Net';
            pcAccessInf.Deal_Type__c = 'Acquisition';
            pcAccessInf.cscfga__one_off_charge_product_discount_value__c = 0;
            pcAccessInf.cscfga__recurring_charge_product_discount_value__c = 0;
            pcAccessInf.ClonedPBXIds__c = 'adf54d8as87fe879ff';
            pcAccessInf.ClonedSiteIds__c = 'adf54d8as87fe879ff';
            pcAccessInf.cscfga__discounts__c = '{"discounts":[{"memberDiscounts":[{"version":"3-0-0","type":"absolute","source":"Negotiation","recordType":"single","discountCharge":"__PRODUCT__","description":"Recurring Charge - 2","chargeType":"oneOff","amount":208},{"chargeType":"recurring","recordType":"single","type":"absolute","source":"Negotiation","discountCharge":"__PRODUCT__","description":"OneOff Charge - 02","amount":23,"version":"3-0-0"}],"evaluationOrder":"serial","recordType":"group","version":"3-0-0"}]}';
            insert pcAccessInf;
            
            cscfga__Attribute_Definition__c aDipvn = new cscfga__Attribute_Definition__c(
                Name = 'IPVPN',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'String'
            );
            insert aDipvn;
            
            cscfga__Attribute__c attripvn = new cscfga__Attribute__c(
                Name = 'IPVPN',
                cscfga__Attribute_Definition__c = aDipvn.Id,
                cscfga__Product_Configuration__c = pcAccessInf.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = '0',
                cscfga__Price__c = 0
            );
            insert attripvn;
            
            cscfga__Attribute_Definition__c aDOneNet1 = new cscfga__Attribute_Definition__c(
                Name = 'One Net',
                cscfga__Product_Definition__c = pd.Id,
                cspl__Profit_and_loss__c = true,
                cscfga__Data_Type__c = 'String'
            );
            insert aDOneNet1;
            
            cscfga__Attribute__c attrOneNet1 = new cscfga__Attribute__c(
                Name = 'One Net',
                cscfga__Attribute_Definition__c = aDOneNet1.Id,
                cscfga__Product_Configuration__c = pcAccessInf.Id,
                cscfga__is_active__c = true,
                cscfga__Value__c = '0',
                cscfga__Price__c = 0
            );
            insert attrOneNet1;
            
            cspl__Report_Configuration__c reportConfiguration = new cspl__Report_Configuration__c(
                name = 'configFields',
                cspl__value__c = '["Name", "cscfga__One_Off_Charge__c", "cscfga__Recurring_Charge__c", "cspl__Type__c", "cspl__Month_Term__c"]'
            );
            insert reportConfiguration;

            string serializedJSON = '{"name":"AfterLastSelectProductPcCreated","selectedPcs":[],"selectedPcrs":[],"pcr":null,"pc":null,"context":"test","basket":{"attributes":{"type":"cscfga__Product_Basket__c","url":"/services/data/v50.0/sobjects/cscfga__Product_Basket__c/' + basket.Id + '"},"Id":"' + basket.Id + '"}}';

            csbb.ProductConfigurationObservable obsrv = (csbb.ProductConfigurationObservable) JSON.deserialize(serializedJSON, csbb.ProductConfigurationObservable.class);

            AfterLastSelectProductPcCreatedObserver.execute(obsrv, null);
            //AfterLastSelectProductPcCreatedObserver.executeMethod(basket);

            Test.stopTest();
        }
    }
}