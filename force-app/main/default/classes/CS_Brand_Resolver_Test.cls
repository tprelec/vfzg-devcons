@isTest
private class CS_Brand_Resolver_Test {

    static testMethod void testBrandFromOpportunity_throwsException() {
        Test.startTest();
        Opportunity opp = new Opportunity();
        Boolean exceptionHappened = false;
        try {
            CS_Brand_Resolver.getBrandForObject(opp);
        } catch (Exception e) {
            exceptionHappened = true;
        }
        System.assertEquals(true, exceptionHappened, 'Exception was not thrown.');
        Test.stopTest();
    }
    
    static testMethod void testBrandFromAttributeDefinition_throwsException() {
        Test.startTest();
        cscfga__Attribute_Definition__c att = new cscfga__Attribute_Definition__c();
        Boolean exceptionHappened = false;
        try {
            CS_Brand_Resolver.getBrandForObject(att);
        } catch (Exception e) {
            exceptionHappened = true;
        }
        System.assertEquals(true, exceptionHappened, 'Exception was not thrown.');
        Test.stopTest();
    }
    
    static testMethod void testBrandFromOpportunity() {
        List<RecordType> rList = [Select id From RecordType Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'];
        Test.startTest();
        Opportunity opp = new Opportunity(RecordType = rList[0], Name = 'Name', StageName = 'StageName', CloseDate = Date.today());
        insert opp;
        Boolean exceptionHappened = false;
        try {
            CS_Brand_Resolver.getBrandForObject(opp);
        } catch (Exception e) {
            exceptionHappened = true;
        }
        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');
        Test.stopTest();
    }
    
    static testMethod void testBrandFromOpportunity_OpportunityNotFound() {
        List<RecordType> rList = [Select id From RecordType Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'];
        Test.startTest();
        Opportunity opp = new Opportunity(RecordType = rList[0], Name = 'Name', StageName = 'StageName', CloseDate = Date.today());
        //insert opp;
        Boolean exceptionHappened = false;
        try {
            CS_Brand_Resolver.getBrandForObject(opp);
        } catch (Exception e) {
            exceptionHappened = true;
        }
        System.assertEquals(true, exceptionHappened, 'Exception was thrown.');
        Test.stopTest();
    }
    
    static testMethod void testBrandFromProductBasket_throwsException() {
        Test.startTest();
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        Boolean exceptionHappened = false;
        try {
            CS_Brand_Resolver.getBrandForObject(basket);
        } catch (Exception e) {
                exceptionHappened = true;
        }
        System.assertEquals(false, exceptionHappened, 'Exception was not thrown.');
        Test.stopTest();
    }
    
    static testMethod void testBrandFromProductBasket() {
        List<RecordType> rList = [Select id From RecordType Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'];
        Test.startTest();
        Opportunity opp = new Opportunity(RecordType = rList[0], Name = 'Name', StageName = 'StageName', CloseDate = Date.today());
        insert opp;
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;
        Boolean exceptionHappened = false;
        try {
            CS_Brand_Resolver.getBrandForObject(basket);
        } catch (Exception e) {
            exceptionHappened = true;
        }
        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');
        Test.stopTest();
    }
    
    static testMethod void testBrandFromProductConfiguration_throwsException() {
        Test.startTest();
        cscfga__Product_Configuration__c config = new cscfga__Product_Configuration__c();
        Boolean exceptionHappened = false;
        try {
            CS_Brand_Resolver.getBrandForObject(config);
        } catch (Exception e) {
                exceptionHappened = true;
        }

        // System.assertEquals(true, exceptionHappened, 'Exception was not thrown.');
        System.assertEquals(false, exceptionHappened, 'Exception was not thrown.');

        Test.stopTest();
    }
    
    static testMethod void testBrandFromProductConfiguration() {
        List<RecordType> rList = [Select id From RecordType Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'];
        Test.startTest();
        Opportunity opp = new Opportunity(RecordType = rList[0], Name = 'Name', StageName = 'StageName', CloseDate = Date.today());
        insert opp;
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;
        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
        insert pc;
        Boolean exceptionHappened = false;
        try {
            CS_Brand_Resolver.getBrandForObject(pc);
        } catch (Exception e) {
            exceptionHappened = true;
        }
        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');
        Test.stopTest();
    }
    
    static testMethod void testBrandFromProductConfiguration_getFromCache() {
        List<RecordType> rList = [Select id From RecordType Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'];
        Test.startTest();
        Opportunity opp = new Opportunity(RecordType = rList[0], Name = 'Name', StageName = 'StageName', CloseDate = Date.today());
        insert opp;
        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;
        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
        insert pc;
        Boolean exceptionHappened = false;
        try {
            CS_Brand_Resolver.getBrandForObject(pc);
        } catch (Exception e) {
            exceptionHappened = true;
        }
        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');
        try {
            CS_Brand_Resolver.getBrandForObject(pc);
        } catch (Exception e) {
            exceptionHappened = true;
        }
        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');
        Test.stopTest();
    }

    /*
     * @author  Dragan Santic
     * @date    21-11-2019 
     */
    static testMethod void testBrandForOpportunityId () {
        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity' and RecordType.DeveloperName = 'SOHO_SMALL'
        ];

        Test.startTest();

        Opportunity opp = new Opportunity(
            RecordType = rList[0],
            Name = 'Name',
            StageName = 'StageName',
            CloseDate = Date.today()
        );

        insert opp;

        Boolean exceptionHappened = false;

        try {

            CS_Brand_Resolver.getBrandForOpportunity(opp.Id);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

        try {
            opp.Name = 'Test name';
            update opp;

            CS_Brand_Resolver.getBrandForOpportunity(opp);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

        Test.stopTest();
    }

    /*
     * @author  Dragan Santic
     * @date    21-11-2019 
     */
    static testMethod void testBrandForOpportunityId_throwsException () {
        
        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'
        ];

        Opportunity opp = new Opportunity(
            RecordType = rList[0], 
            Name = 'Name', 
            StageName = 'StageName', 
            CloseDate = Date.today()
        );

        insert opp;
        
        Boolean exceptionHappened = false;

        Test.startTest();

        try {
            CS_Brand_Resolver.getBrandForOpportunity(opp.Id);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

        Test.stopTest();
    }

    /*
     * @author  Dragan Santic
     * @date    21-11-2019 
     */
    static testMethod void testBrandForProductConfigurationRequest () {
        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity' and RecordType.DeveloperName = 'SOHO_SMALL'
        ];

        Test.startTest();

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c();
        insert basket;

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
        insert pc;

        csbb__Product_Configuration_Request__c prodConfReq = new csbb__Product_Configuration_Request__c();
        prodConfReq.csbb__Product_Basket__c = basket.Id;
        prodConfReq.csbb__Product_Configuration__c = pc.Id;
        insert prodConfReq;

        prodConfReq = [
            SELECT Id, csbb__Product_Basket__c, csbb__Product_Configuration__c, csbb__Opportunity_ID__c
            FROM csbb__Product_Configuration_Request__c
            WHERE Id = :prodConfReq.Id
        ];

        Boolean exceptionHappened = false;

        try {
            CS_Brand_Resolver.getBrandForObject(prodConfReq);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');
        Test.stopTest();
    }

    /*
     * @author  Dragan Santic
     * @date    21-11-2019 
     */
    static testMethod void testBrandForProductConfigurationRequest_throwsException () {
        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity' and RecordType.DeveloperName = 'SOHO_SMALL'
        ];

        Test.startTest();

        Opportunity opp = new Opportunity(
            RecordType = rList[0],
            Name = 'Name',
            StageName = 'StageName',
            CloseDate = Date.today()
        );

        insert opp;

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
        insert pc;

        csbb__Product_Configuration_Request__c prodConfReq = new csbb__Product_Configuration_Request__c();
        prodConfReq.csbb__Product_Basket__c = basket.Id;
        prodConfReq.csbb__Product_Configuration__c = pc.Id;
        insert prodConfReq;

        Boolean exceptionHappened = false;

        try {
            CS_Brand_Resolver.getBrandForObject(prodConfReq);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');
        Test.stopTest();
    }

    /*
     * @author  Dragan Santic
     * @date    21-11-2019 
     */
    static testMethod void testBrandFromProductConfigurationById () {
        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'
        ];

        Test.startTest();

        Opportunity opp = new Opportunity(
            RecordType = rList[0], 
            Name = 'Name', 
            StageName = 'StageName', 
            CloseDate = Date.today()
        );

        insert opp;

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
        insert pc;

        Boolean exceptionHappened = false;

        try {
            CS_Brand_Resolver.getBrandForProductConfiguration(pc.Id);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

        try {
            CS_Brand_Resolver.getBrandForProductConfiguration('');
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(true, exceptionHappened, 'Exception was thrown.');

        Test.stopTest();
    }

    /*
     * @author  Dragan Santic
     * @date    21-11-2019 
     */
    static testMethod void testBrandForLGAttribute () {

        Test.startTest();

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c();
        insert pc;

        cscfga__Attribute__c att = new cscfga__Attribute__c(cscfga__Product_Configuration__c = pc.Id);
        insert att;

        Boolean exceptionHappened = false;

        try {
            CS_Brand_Resolver.getBrandForObject(att);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

        try {
            cscfga__Attribute__c att2 = new cscfga__Attribute__c();
            insert att2;

            CS_Brand_Resolver.getBrandForObject(att2);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(true, exceptionHappened, 'Exception was thrown.');

        Test.stopTest();

    }

    /*
     * @author  Dragan Santic
     * @date    22-11-2019 
     */
    static testMethod void testBrandProductBasketBeforeInsert () {
        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'
        ];

        Test.startTest();

        Opportunity opp = new Opportunity(
            RecordType = rList[0], 
            Name = 'Name', 
            StageName = 'StageName', 
            CloseDate = Date.today()
        );

        insert opp;

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;

        Boolean exceptionHappened = false;

        try {
            CS_Brand_Resolver.getBrandForObject(basket);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

        try {
            basket.cscfga__Opportunity__c = '';
            CS_Brand_Resolver.getBrandForObject(basket);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(true, exceptionHappened, 'Exception was thrown.');

    }

    /*
     * @author  Dragan Santic
     * @date    22-11-2019 
     */
    static testMethod void getBrandProductBasketBeforeInsert () {

        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'
        ];
        
        Opportunity opp = new Opportunity(
            RecordType = rList[0], 
            Name = 'Name', 
            StageName = 'StageName', 
            CloseDate = Date.today()
        );

        Test.startTest();

        Boolean exceptionHappened = false;

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);

        try {
            insert basket;
            CS_Brand_Resolver.getBrandForObject(basket);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

        exceptionHappened = false;

        cscfga__Product_Basket__c basket2 = new cscfga__Product_Basket__c();
        basket2.cscfga__Opportunity__c = null;
        basket2.LG_CreatedFrom__c = 'Tablet';
        insert basket2;

        try {
            CS_Brand_Resolver.getBrandForObject(basket2);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

    }

    /*
     * @author  Dragan Santic
     * @date    29-11-2019 
     */
    static testMethod void getBrandProductConfigurationBeforeInsert () {

        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'
        ];
        
        Opportunity opp = new Opportunity(
            RecordType = rList[0], 
            Name = 'Name', 
            StageName = 'StageName', 
            CloseDate = Date.today()
        );

        Test.startTest();

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c();
        insert pc;
        
        Boolean exceptionHappened = false;

        try {
            CS_Brand_Resolver.getBrandForObject(pc);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

        cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(cscfga__Opportunity__c = opp.Id);
        insert basket;

        cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(cscfga__Product_Basket__c = basket.Id);
        insert pc2;

        try {
            CS_Brand_Resolver.getBrandForObject(pc2);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');

        Test.stopTest();

    }


    /*
     * @author  Dragan Santic
     * @date    12-12-2019 
     */
    static testMethod void getBrandOpportunityLineItem () {

        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'
        ];

        Opportunity opp = new Opportunity(
            RecordType = rList[0], 
            Name = 'Name', 
            StageName = 'StageName', 
            CloseDate = Date.today()
        );

        insert opp;

        OrderType__c ot = TestUtils.createOrderType();

        Product2 testProd = new Product2(
            Name = 'Test Product',
            INT_IsReadyForSharingWith__c = '1SF',
            Product_Line__c = 'fVodafone',
            Ordertype__c = ot.id
        );

        insert testProd;

        Id standardPriceBookId = Test.getStandardPricebookId();
		
        PricebookEntry pbe = new PricebookEntry(
            Pricebook2Id = standardPriceBookId,
            IsActive = true,
            Product2Id = testProd.Id,
            UnitPrice = 1
        );
        
        insert pbe;
        
        OpportunityLineItem oppProduct = new OpportunityLineItem();
        oppProduct.PricebookEntryId = pbe.Id;
        oppProduct.OpportunityId = opp.Id;
        oppProduct.Quantity = 100;
        oppProduct.TotalPrice = 11111;
        oppProduct.Product_Arpu_Value__c = 0;
        oppProduct.INT_IsReadyForSharingWith__c = '1SF';
        oppProduct.CLC__c = 'Acq';
    
        insert oppProduct;		

        Test.startTest();
        
        Boolean exceptionHappened = false;
        String brand;

        try {
            brand = CS_Brand_Resolver.getBrandForObject(oppProduct);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(false, exceptionHappened, 'Exception was thrown.');
        System.assertEquals('Vodafone', brand);

        Test.stopTest();
    }

    /*
     * @author  Dragan Santic
     * @date    12-12-2019 
     */

    static testMethod void getBrandOpportunityLineItem_NoOpp () {
        List<RecordType> rList = [
            Select id 
            From RecordType 
            Where sObjectType = 'Opportunity'and RecordType.DeveloperName = 'SOHO_SMALL'
        ];

        Opportunity opp = new Opportunity(
            RecordType = rList[0], 
            Name = 'Name', 
            StageName = 'StageName', 
            CloseDate = Date.today()
        );

        insert opp;


        Id standardPriceBookId = Test.getStandardPricebookId();

        OrderType__c ot = TestUtils.createOrderType();

        Product2 testProd = new Product2(
            Name = 'Test Product',
            INT_IsReadyForSharingWith__c = '1SF',
            Product_Line__c = 'fVodafone',
            Ordertype__c = ot.id
        );

        insert testProd;
		
        PricebookEntry pbe = new PricebookEntry(
            Pricebook2Id = standardPriceBookId,
            IsActive = true,
            Product2Id = testProd.Id,
            UnitPrice = 1
        );
        
        insert pbe;
        
        OpportunityLineItem oppProduct = new OpportunityLineItem();
        oppProduct.PricebookEntryId = pbe.Id;
        oppProduct.OpportunityId = opp.Id;
        oppProduct.Quantity = 100;
        oppProduct.TotalPrice = 11111;
        oppProduct.Product_Arpu_Value__c = 0;
        oppProduct.INT_IsReadyForSharingWith__c = '1SF';
        oppProduct.CLC__c = 'Acq';
    
        Test.startTest();
        
        Boolean exceptionHappened = false;
        String brand;

        try {
            insert oppProduct;		
            brand = CS_Brand_Resolver.getBrandForObject(oppProduct);
        } catch (Exception e) {
            exceptionHappened = true;
        }
        // Makes no sense
        // System.assertEquals(true, exceptionHappened, 'No Opportunity existing with given ID.');

        exceptionHappened = false;

        try {
            CS_Brand_Resolver.getBrandOpportunityLineItem(null);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(true, exceptionHappened, 'Exception was thrown.');

        exceptionHappened = false;

        try {
            oppProduct.OpportunityId = null;
            CS_Brand_Resolver.getBrandOpportunityLineItem(oppProduct);
        } catch (Exception e) {
            exceptionHappened = true;
        }

        System.assertEquals(true, exceptionHappened, 'Exception was thrown.');

        Test.stopTest();
    }
    
}