global without sharing class CS_CustomStepUpdateStatus implements CSPOFA.ExecutionHandler{

    public List<SObject> process(List<SObject> steps) {
        List<SObject> result = new List<sObject>();
        Map<Id, CSPOFA__Orchestration_Step__c> processStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
        Map<Id, String> statusByparentServiceIds = new Map<Id, String>();
        List<CSPOFA__Orchestration_Step__c> updateDeliveryComponentsSteps = new List<CSPOFA__Orchestration_Step__c>();

        List<CSPOFA__Orchestration_Step__c> stepList = [SELECT ID, Name,
                COM_Status__c,
                CSPOFA__Orchestration_Process__c,
                CSPOFA__Status__c,
                CSPOFA__Completed_Date__c,
                CSPOFA__Message__c,
                CSPOFA__Expression_Result__c,
                CSPOFA__Orchestration_Process__r.COM_Delivery_Order__c,
                COM_Update_Delivery_Components__c,
                COM_Provisioning_Queue__c,
                CSPOFA__Orchestration_Process__r.CSPOFA__Orchestration_Process_Template__c,
                CSPOFA__Orchestration_Process__r.CSPOFA__Orchestration_Process_Template__r.MACD_Flow_Type__c,
                CSPOFA__Orchestration_Process__r.COM_Delivery_Order__r.Site__r.Footprint__c,
                CSPOFA__Orchestration_Process__r.COM_Delivery_Queue__c,
                CSPOFA__Orchestration_Process__r.CSPOFA__Parent_Process__c,
                CSPOFA__Orchestration_Process__r.CSPOFA__Parent_Process__r.CSPOFA__Orchestration_Process_Template__r.MACD_Flow_Type__c,
                COM_Update_Implemented_Data__c
        FROM CSPOFA__Orchestration_Step__c
        WHERE Id IN :steps];

        try{
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                processStepMap.put(step.CSPOFA__Orchestration_Process__c, step);
                if (step.COM_Update_Delivery_Components__c) {
                    updateDeliveryComponentsSteps.add(step);
                }
            }

            Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c>([
                    SELECT id, csordtelcoa__Service__c,
                            CSPOFA__Process_Type__c,
                            csordtelcoa__Subscription__c,
                            COM_Delivery_Order__c,
                            COM_Delivery_Order__r.Site__r.Footprint__c,
                            csordtelcoa__Service__r.csord__Service__c,
                            csordtelcoa__Service__r.csord__Subscription__c,
                            csordtelcoa__service__r.csord__subscription__r.csord__order__c
                    FROM CSPOFA__Orchestration_Process__c
                    WHERE id in :processStepMap.keySet()
            ]);

            for(CSPOFA__Orchestration_Process__c p : processesMap.values()){
                CSPOFA__Orchestration_Step__c step = processStepMap.get(p.Id);
                    if (p.csordtelcoa__Service__r.csord__Service__c == null) statusByparentServiceIds.put(p.csordtelcoa__Service__c, step.COM_Status__c);
            }

            //used by Juan I think but it gives me errors so commented out for now
            /*if(statusByparentServiceIds.size() > 0){
                CS_OrchestratorStepUtility.updateParentAndChildServices(statusByparentServiceIds);
            }*/
            
            if(updateDeliveryComponentsSteps.size() > 0){
                CS_OrchestratorStepUtility.updateDeliveryComponents(updateDeliveryComponentsSteps);
            }

            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_COMPLETE;
                step.CSPOFA__Completed_Date__c = Date.today();
                step.CSPOFA__Message__c = 'Custom step succeeded';
                result.add(step);
            }

        } catch(Exception ex){
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
                step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
                if(step.CSPOFA__Message__c.length() > 255 ){
                    step.CSPOFA__Message__c = step.CSPOFA__Message__c.substring(0, 254);
                }
                result.add(step);
            }
        }
        return result;
    }
}