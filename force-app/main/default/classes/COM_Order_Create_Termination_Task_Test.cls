/*
    Author: Rahul Sharma
    Description: Tests class for autolaunched flow: COM_Order_Create_Termination_Task.flow
    Jira ticket: COM-2764
*/

@IsTest
private class COM_Order_Create_Termination_Task_Test {
	@TestSetup
	private static void createTestData() {
		Account account = CS_DataTest.createAccount('Test Account');
		insert account;

		Opportunity opportunity = CS_DataTest.createOpportunity(account, 'Test Opp', UserInfo.getUserId());
		insert opportunity;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opportunity, 'Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c productDefinition = CS_DataTest.createProductDefinition('Product Definition');
		productDefinition.RecordTypeId = productDefinitionRecordType;
		productDefinition.Product_Type__c = 'Fixed';
		insert productDefinition;

		cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(productDefinition.Id, 'Test Conf', basket.Id);
		insert productConfiguration;

		csord__Order__c order = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
		insert order;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(productConfiguration.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		insert subscription;
	}

	@IsTest
	static void testTaskCreation() {
		List<csord__Subscription__c> subscriptions = [
			SELECT Id, Termination_Fee_Discount__c, Termination_Fee_Discount_Type__c
			FROM csord__Subscription__c
			LIMIT 2
		];
		System.assertEquals(1, subscriptions.size());
		update new csord__Subscription__c(
			Id = subscriptions[0].Id,
			Termination_Fee_Discount__c = 100,
			Termination_Fee_Discount_Type__c = 'EUR',
			Termination_Fee_Calculation__c = 100
		);

		Test.startTest();

		List<csord__Order__c> orders = [SELECT Id FROM csord__Order__c LIMIT 2];
		System.assertEquals(1, orders.size());
		update new csord__Order__c(Id = orders[0].Id, csord__Status2__c = 'Ready for billing');

		Test.stopTest();

		System.assertEquals([SELECT COUNT() FROM Task WHERE WhatId = :subscriptions[0].Id], 1);
	}

	@IsTest
	static void testNoTaskCreationForNoChangeAndNotReadyForBilling() {
		List<csord__Subscription__c> subscriptions = [
			SELECT Id, Termination_Fee_Discount__c, Termination_Fee_Discount_Type__c
			FROM csord__Subscription__c
			LIMIT 2
		];
		System.assertEquals(1, subscriptions.size());
		update new csord__Subscription__c(
			Id = subscriptions[0].Id,
			Termination_Fee_Discount__c = 100,
			Termination_Fee_Discount_Type__c = 'EUR',
			Termination_Fee_Calculation__c = 100
		);

		Test.startTest();

		List<csord__Order__c> orders = [SELECT Id FROM csord__Order__c LIMIT 2];
		System.assertEquals(1, orders.size());
		update new csord__Order__c(Id = orders[0].Id, csord__Status2__c = 'Order Submitted');

		Test.stopTest();

		System.assertEquals([SELECT COUNT() FROM Task WHERE WhatId = :subscriptions[0].Id], 0);
	}

	@IsTest
	static void testNoTaskCreationForChangeAndReadyForBilling() {
		List<csord__Subscription__c> subscriptions = [
			SELECT Id, Termination_Fee_Discount__c, Termination_Fee_Discount_Type__c
			FROM csord__Subscription__c
			LIMIT 2
		];
		System.assertEquals(1, subscriptions.size());
		update new csord__Subscription__c(
			Id = subscriptions[0].Id,
			Termination_Fee_Discount__c = null,
			Termination_Fee_Discount_Type__c = null,
			Termination_Fee_Calculation__c = null
		);

		Test.startTest();

		List<csord__Order__c> orders = [SELECT Id FROM csord__Order__c LIMIT 2];
		System.assertEquals(1, orders.size());
		update new csord__Order__c(Id = orders[0].Id, csord__Status2__c = 'Ready for billing');

		Test.stopTest();

		System.assertEquals([SELECT COUNT() FROM Task WHERE WhatId = :subscriptions[0].Id], 0);
	}
}
