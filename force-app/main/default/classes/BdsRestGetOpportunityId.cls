@RestResource(urlMapping='/getopportunityid/*')
global with sharing class BdsRestGetOpportunityId {
	@HttpGet
	global static void doGet() {
		doGetResponse();
	}

	@TestVisible
	private static void doGetResponse() {
		RestRequest request = RestContext.request;
		RestResponse res = RestContext.response;
		String orderId = request.requestURI.substring(request.requestURI.lastIndexOf('/') + 1);

		try {
			String result = OrderService.getOpportunityNumberWithOrderId(orderId);
			res.responseBody = Blob.valueOf(JSON.serialize(new BdsResponseClasses.OpportunityIdResponse(result)));
			res.statusCode = 200;
		} catch (Exception e) {
			res.statusCode = 400;
			res.responseBody = Blob.valueOf(
				JSON.serializePretty(new BdsResponseClasses.OpportunityIdError('Couldn\'t find an Opportunity for ' + orderId))
			);
		}
	}
}
