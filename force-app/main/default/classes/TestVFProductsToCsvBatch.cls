@isTest
private class TestVFProductsToCsvBatch {
	@isTest
	static void testBatch() {
		OrderType__c ot = TestUtils.createOrderType();

		List<VF_Product__c> vfps = new List<VF_Product__c>();

		VF_Product__c vfp1 = new VF_Product__c(
			Active__c = true,
			Family__c = 'Mobile Hardware',
			Product_Line__c = 'fZiggo Only',
			Brand__c = 'Ziggo',
			OrderType__c = ot.Id,
			ExternalID__c = 'VFP-02-0005039'
		);
		vfps.add(vfp1);

		VF_Product__c vfp2 = new VF_Product__c(
			Active__c = true,
			Family__c = 'Mobile Hardware',
			Product_Line__c = 'fVodafone',
			Brand__c = 'Apple',
			OrderType__c = ot.Id,
			ExternalID__c = 'VFP-02-0005040'
		);
		vfps.add(vfp2);

		insert vfps;

		Test.startTest();

		VFProductsToCsvBatch prodsToCsvBatch = new VFProductsToCsvBatch();
		VFProductsToCsvBatch.isTest = true;
		Database.executeBatch(prodsToCsvBatch);

		Test.stopTest();

		List<AsyncApexJob> resultJob = [
			SELECT Status, NumberOfErrors, TotalJobItems
			FROM AsyncApexJob
		];
		System.assertEquals(true, resultJob.size() > 1, 'Job should be executed.');
	}
}