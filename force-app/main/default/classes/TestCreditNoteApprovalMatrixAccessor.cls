/**
 * @description:    Test class for CreditNoteApprovalMatrixAccessor
 **/
@isTest
public class TestCreditNoteApprovalMatrixAccessor {
	private static final String TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_REFUND = Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_REFUND;
	private static final String TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL = Constants.CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL;
	private static final String TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE = Constants.CREDIT_NOTE_SUBSCRIPTION_MOBILE;
	private static final String TEST_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE;
	private static final String TEST_MATRIX_QUEUE_DEVELOPER_NAME = 'TestCreditNoteApprovalMatrixAccessorQueue';

	private static final Integer NUMBER_OF_MATRICES_TO_CREATE = 5;
	private static final Integer NUMBER_OF_TIMES_TO_CALL_MATRIX_RETRIEVAL_IN_SAME_TRANSACTION = 5;

	/**
	 * @description:    Tests retrieving credit note approval matrices
	 *                  when they exist
	 **/
	@isTest
	static void shouldRetrieveCreditNoteApprovalMatricesWhenTheyExist() {
		// prepare data
		List<CreditNote_Approval_Matrix__c> createdCreditNoteApprovalMatrices = createTestCreditNoteApprovalMatrices(NUMBER_OF_MATRICES_TO_CREATE);

		// perform testing
		Test.startTest();
		CreditNoteApprovalMatrixAccessor.initCreditNoteApprovalMatrixMapping(createdCreditNoteApprovalMatrices);

		List<CreditNote_Approval_Matrix__c> retrievedApprovalMatricesUsingAccessor = CreditNoteApprovalMatrixAccessor.getAllCreditNoteApprovalMatrices();
		Test.stopTest();

		// verify results
		System.assertEquals(
			NUMBER_OF_MATRICES_TO_CREATE,
			retrievedApprovalMatricesUsingAccessor.size(),
			'Proper number of credit note approval matrices should be retrieved'
		);
	}

	/**
	 * @description:    Tests retrieving empty list when no credit note approval
	 *                  matrices exist
	 **/
	@isTest
	static void shouldRetrieveEmptyListWhenNoCreditNoteApprovalMatricesExist() {
		// prepare data

		// perform testing
		Test.startTest();
		List<CreditNote_Approval_Matrix__c> retrievedApprovalMatricesUsingAccessor = CreditNoteApprovalMatrixAccessor.getAllCreditNoteApprovalMatrices();
		Test.stopTest();

		// verify results
		System.assertEquals(0, retrievedApprovalMatricesUsingAccessor.size(), 'No credit note approval matrices should be retrieved');
	}

	/**
	 * @description:    Tests doing only 1 SOQL when accessor is called
	 *                  multiple times in same transaction
	 **/
	@isTest
	static void shouldDoOneSOQLWhenCalledMultipleTimesInSameTransaction() {
		// prepare data
		List<CreditNote_Approval_Matrix__c> createdCreditNoteApprovalMatrices = createTestCreditNoteApprovalMatrices(NUMBER_OF_MATRICES_TO_CREATE);

		// perform testing
		Test.startTest();
		CreditNoteApprovalMatrixAccessor.initCreditNoteApprovalMatrixMapping(createdCreditNoteApprovalMatrices);

		for (Integer i = 0; i < NUMBER_OF_TIMES_TO_CALL_MATRIX_RETRIEVAL_IN_SAME_TRANSACTION; i++) {
			List<CreditNote_Approval_Matrix__c> retrievedApprovalMatricesUsingAccessor = CreditNoteApprovalMatrixAccessor.getAllCreditNoteApprovalMatrices();
		}
		Integer queries = Limits.getQueries();
		Test.stopTest();

		// verify results
		System.assertEquals(1, queries, 'Only 1 SOQL query should be done');
	}

	/**
	 * @description:    Creates test credit note approval matrices
	 * @param           numberOfMatricesToCreate - number of matrices to create
	 * @return          List<CreditNote_Approval_Matrix__c> - created credit note approval matrices
	 **/
	private static List<CreditNote_Approval_Matrix__c> createTestCreditNoteApprovalMatrices(Integer numberOfMatricesToCreate) {
		List<CreditNote_Approval_Matrix__c> createdCreditNoteApprovalMatrices = new List<CreditNote_Approval_Matrix__c>();

		for (Integer i = 0; i < numberOfMatricesToCreate; i++) {
			createdCreditNoteApprovalMatrices.add(
				new CreditNote_Approval_Matrix__c(
					CreditNoteRecordType__c = TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_CREDIT_REFUND,
					CreditNoteCreditType__c = TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_CONTRACTUAL,
					CreditNoteSubscription__c = TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE,
					AmountFrom__c = i * 100,
					AmountTo__c = (i + 1) * 100,
					ApprovalAssignmentRule__c = TEST_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE,
					QueueApprover__c = TEST_MATRIX_QUEUE_DEVELOPER_NAME,
					Level__c = i + 1
				)
			);
		}

		return createdCreditNoteApprovalMatrices;
	}
}
