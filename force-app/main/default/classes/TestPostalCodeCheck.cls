/**
 * @description         This class is responsible for testing the postcode check
 */
@isTest
private class TestPostalCodeCheck {

    @isTest
    static void testCombinedPostalCodeCheckFromPage() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);      
        Site__c s = createSite(acct, false);
        s.Postal_Code_Check_Status__c = 'Combined check requested';
        insert s;
        
        // create mappings
        TestUtils.createPortFolioVendorMappings();
        TestUtils.createPreferredSuppliers();
        
        Test.startTest();
        // use mockservice for testing
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());
        PageReference pageRef = Page.SitePostalCodeCheck;
        pageRef.getParameters().put('SiteId', s.Id);
        pageRef.getParameters().put('requestType', '');
        Test.setCurrentPage(pageRef);

        PostalCodeCheckController pc = new PostalCodeCheckController();
        pc.postalCodeCheckForSite();
        pc.backToSite();
        Test.stopTest();
        
        List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
        System.assertEquals(pcs.size(), 2, 'There are 2 new site postal checks.');
    }
    
    @isTest
    static void testCombinedPostalCodeCheckFromPageCanVas() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);      
        Site__c s = createSite(acct, true);
        s.Postal_Code_Check_Status__c = 'Combined check requested';
        insert s;
        
        // create mappings
        TestUtils.createPortFolioVendorMappings();
        TestUtils.createPreferredSuppliers();
        
        Test.startTest();
        // use mockservice for testing
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());
        PageReference pageRef = Page.SitePostalCodeCheck;
        pageRef.getParameters().put('SiteId', s.Id);
        pageRef.getParameters().put('requestType', '');
        Test.setCurrentPage(pageRef);

        PostalCodeCheckController pc = new PostalCodeCheckController();
        pc.postalCodeCheckForSite();        
        Test.stopTest();
		List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
        System.assertNotEquals(pcs.size(), 2, 'There are 2 new site postal checks.');
        // to add assert
    }

    @isTest
    static void testFiberPostalCodeCheckFromPage() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);      
        Site__c s = createSite(acct, false);
        s.Postal_Code_Check_Status__c = 'Fiber check requested';
        insert s;

        // create mappings
        TestUtils.createPortFolioVendorMappings();
        TestUtils.createPreferredSuppliers();       
        Test.startTest();
        
        // use mockservice for testing
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());
        PageReference pageRef = Page.SitePostalCodeCheck;
        pageRef.getParameters().put('SiteId', s.Id);
        pageRef.getParameters().put('requestType', 'fiber');
        Test.setCurrentPage(pageRef);

        PostalCodeCheckController pc = new PostalCodeCheckController();
        pc.postalCodeCheckForSite();

        pc.postalCodeCheckForSite();
        System.assertNotEquals(0, ApexPages.getMessages().size(),'parameter is not empty');
        Test.stopTest();
        
        List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
        System.assertEquals(pcs.size(), 1, 'There is a new site postal check.');
    }
    
    @isTest
    static void testFiberPostalCodeCheckFromPageCanvas() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        Site__c s = createSite(acct, true);
        insert s;
        
        // create mappings
        TestUtils.createPortFolioVendorMappings();
        TestUtils.createPreferredSuppliers();       
        Test.startTest();
        
        // use mockservice for testing
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());
        PageReference pageRef = Page.SitePostalCodeCheck;
        pageRef.getParameters().put('SiteId', s.Id);
        pageRef.getParameters().put('requestType', 'fiber');
        Test.setCurrentPage(pageRef);

        PostalCodeCheckController pc = new PostalCodeCheckController();
        pc.postalCodeCheckForSite();

        Test.stopTest();
		List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
        System.assertNotEquals(pcs.size(), 2, 'There are 2 new site postal checks.');
        // to add assert
    }

    @isTest
    static void testDslPostalCodeCheckFromPage() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);      
        
        Site__c s = createSite(acct, false);
        s.Postal_Code_Check_Status__c = 'Completed'; // prevent the trigger from runnning here..
        insert s;
        
        // create mappings
        TestUtils.createPortFolioVendorMappings();
        TestUtils.createPreferredSuppliers();

        PageReference pageRef = Page.SitePostalCodeCheck;
        pageRef.getParameters().put('SiteId', s.Id);
        pageRef.getParameters().put('requestType', 'dsl');
        Test.setCurrentPage(pageRef);

        Test.startTest();

        // use mockservice for testing
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());     

        PostalCodeCheckController pc = new PostalCodeCheckController();
        pc.postalCodeCheckForSite();
        
        pc.postalCodeCheckForSite();
        System.assertNotEquals(0, ApexPages.getMessages().size(),'parameter should not be empty');
        Test.stopTest();

        List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
        System.assertEquals(pcs.size(), 1, 'There is a new site postal check.');
    }       

    @isTest
    static void testDslPostalCodeCheckFromPageCanVas() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);      
        
        Site__c s = createSite(acct, true);
        s.Postal_Code_Check_Status__c = 'Completed';
        insert s;
        
        // create mappings
        TestUtils.createPortFolioVendorMappings();
        TestUtils.createPreferredSuppliers();

        PageReference pageRef = Page.SitePostalCodeCheck;
        pageRef.getParameters().put('SiteId', s.Id);
        pageRef.getParameters().put('requestType', 'dsl');
        Test.setCurrentPage(pageRef);

        Test.startTest();

        // use mockservice for testing
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());     

        PostalCodeCheckController pc = new PostalCodeCheckController();
        Test.setCurrentPage(pc.postalCodeCheckForSite());
        
        Test.stopTest();
		List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
        System.assertNotEquals(pcs.size(), 2, 'There are new site postal checks.');
        // to add assert
    }

    @isTest
    static void testPostalCodeCheckFromTrigger() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);      

        // create mappings
        TestUtils.createPortFolioVendorMappings();
        TestUtils.createPreferredSuppliers();           
        TestUtils.createSalesSettings();

        Test.startTest();

        // use mockservice for testing
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());     

        Site__c s = createSite(acct, false);
        insert s;
        
        Test.stopTest();

        List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
        System.assertEquals(pcs.size(), 2, 'There are 2 new site postal checks.');
    }

    @isTest
    static void testPostalCodeCheckFromTriggerCanvas() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);      

        // create mappings
        TestUtils.createPortFolioVendorMappings();
        TestUtils.createPreferredSuppliers();           
        TestUtils.createSalesSettings();

        Test.startTest();

        // use mockservice for testing
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());     

        Site__c s = createSite(acct, true);
        insert s;

        Test.stopTest();
		List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
        System.assertNotEquals(pcs.size(), 2, 'There are  new site postal checks.');
        // to add assert
    }
    
    @isTest
    static void testPostalCodeCheckFromPageBatch() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);      
        
        Site__c s = createSite(acct, false);
        s.Postal_Code_Check_Status__c = 'Combined check requested';
        insert s;       
        
        // create mappings
        TestUtils.createPortFolioVendorMappings();
        TestUtils.createPreferredSuppliers();
        
        Test.startTest();
        // use mockservice for testing
        Test.setMock(WebServiceMock.class, new TestUtilMockServices());
        SitePostalcodeCheckBatch obj = new SitePostalcodeCheckBatch();
        Database.executeBatch(obj);
        Test.stopTest();

        List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
        System.assertEquals(pcs.size(), 2, 'There are 2 new site postal checks.');
    }

    public static Site__c createSite(Account acct, Boolean hasCanvas) {
        Site__c s = new Site__c();
        s.Site_Account__c = acct.Id;
        s.Site_Street__c = 'test';
        s.Site_City__c = 'testCity';
        s.Site_Postal_Code__c = '1234AB';
        s.Site_House_Number__c = 5;
        s.Site_House_Number_Suffix__c = 'a';
        if (hasCanvas) {
            s.Canvas_Street__c = 'Canvas test Street';
            s.Canvas_City__c = 'Canvas test City';      
            s.Canvas_Postal_Code__c = '1234CD';
            s.Canvas_House_Number__c = 987;
            s.Canvas_House_Number_Suffix__c = 'Cprefex';
        }
        return s;
    }
    
    @isTest
  static void testFiberPostalCodeCheckFromPage1() {
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);    
    Site__c s = createSite(acct, false);
    s.Postal_Code_Check_Status__c = 'Fiber check requested';
    insert s;

    // create mappings
   // TestUtils.createPortFolioVendorMappings();
    PortfolioVendorMapping__c pvm = new PortfolioVendorMapping__c();
    pvm.Name = 'test-fiber-business';
    pvm.Vendor__c = 'TESTFIBER1';
    insert pvm;

    PortfolioVendorMapping__c pvm1 = new PortfolioVendorMapping__c();
    pvm1.Name = 'test-dsl-business';
    pvm1.Vendor__c = 'TESTFIBER';
    insert pvm1;
    //TestUtils.createPreferredSuppliers();   
     Preferred_Supplier__c ps2 = new Preferred_Supplier__c();
    ps2.Vendor__c = 'TESTFIBER1';
    ps2.Technology_Type__c = 'Fiber';
    ps2.Order__c = 0;
    ps2.ExternalID__c = 'PS-11-1233335';
    insert ps2; 
    Preferred_Supplier__c ps1 = new Preferred_Supplier__c();
    ps1.Vendor__c = 'TESTFIBER';
    ps1.Technology_Type__c = 'Fiber';
    ps1.Order__c = 5;
    ps1.ExternalID__c = 'PS-11-1233334';
    insert ps1; 
   
    
    Test.startTest();
    
    // use mockservice for testing
    Test.setMock(WebServiceMock.class, new TestUtilMockServices());
    PageReference pageRef = Page.SitePostalCodeCheck;
    pageRef.getParameters().put('SiteId', s.Id);
    pageRef.getParameters().put('requestType', 'fiber');
    Test.setCurrentPage(pageRef);

    PostalCodeCheckController pc = new PostalCodeCheckController();
    pc.postalCodeCheckForSite();

    pc.postalCodeCheckForSite();
    System.assertNotEquals(0, ApexPages.getMessages().size(),'parameter should not be empty');
    Test.stopTest();
    
    List<Site_Postal_Check__c> pcs = [SELECT Id FROM Site_Postal_Check__c WHERE Access_Site_ID__c = :s.Id];
    System.assertEquals(pcs.size(), 1, 'There is a new site postal check.');
  }
}