@IsTest
private class TestCS_COMCloseCaseController {    
	@IsTest
	static void testBehaviorInstallSolution() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs (simpleUser) {
			Task task1 = CS_DataTest.createTask('Install Solution', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Installation';
			insert task1;

			try {
				CS_COMCloseCaseController.closeCase(task1.Id);
			} catch (Exception e) {
				System.assertEquals(e.getMessage(), 'FIELD_CUSTOM_VALIDATION_EXCEPTION, Case needs to have at least one attachment');
			}
		}
	}

	@IsTest
	static void testBehaviorInstallSolution_Success() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs (simpleUser) {
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
			insert ord;

			Suborder__c suborder1 = CS_DataTest.createSuborder('Solution 1', ord.Id, true);

			Task task1 = CS_DataTest.createTask('Install Solution', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Installation';
			task1.Order__c = ord.Id;
			task1.Suborder__c = suborder1.Id;
			insert task1;

			Attachment att1 = new Attachment();
			att1.name = 'test1.txt';
			att1.parentId = suborder1.Id;
			att1.Body = Blob.valueOf('Test');
			insert att1;

			Attachment att2 = new Attachment();
			att2.name = 'test2.txt';
			att2.parentId = task1.Id;
			att2.Body = Blob.valueOf('Test');
			insert att2;

			Test.startTest();
			String result = CS_COMCloseCaseController.closeCase(task1.Id);
			Test.stopTest();

			task1 = [SELECT Id, Status, OwnerId FROM Task WHERE Id = :task1.Id];
			System.assertEquals('Task Closed', result);
			System.assertEquals('Completed', task1.Status, 'Task should be Completed');
		}
	}
	
	@IsTest
	static void testBehaviorInstallSolution1() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs (simpleUser) {
			Task task1 = CS_DataTest.createTask('Install Solution', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Installation Jeopardy';
			insert task1;

			try {
				CS_COMCloseCaseController.closeCase(task1.Id);
			} catch (Exception e) {
				System.assertEquals(e.getMessage(), 'FIELD_CUSTOM_VALIDATION_EXCEPTION, Case needs to have at least one attachment');
			}
		}
	}
	
	@IsTest
	static void testBehaviorInstallSolution2() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs (simpleUser) {
			Task task1 = CS_DataTest.createTask('Install Solution', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Prepare Project';
			insert task1;

			try {
				CS_COMCloseCaseController.closeCase(task1.Id);
			} catch (Exception e) {
				System.assertEquals(e.getMessage(), 'FIELD_CUSTOM_VALIDATION_EXCEPTION, Case needs to have at least one attachment');
			}
		}
	}

	@IsTest
	static void testBehaviorInstallSolutionJeopardy() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs (simpleUser) {
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
			insert ord;

			Suborder__c suborder1 = CS_DataTest.createSuborder('Solution 1', ord.Id, true);

			Task installSolutionTask = CS_DataTest.createTask('Install Solution', 'COM Delivery', simpleUser, false);
			installSolutionTask.Type = 'COM_Installation';
			installSolutionTask.Suborder__c = suborder1.Id;
			installSolutionTask.Status = 'Closed';
			insert installSolutionTask;

			Task task1 = CS_DataTest.createTask('Install Solution', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Installation Jeopardy';
			task1.Install_Solution_Jeopardy_Outcome__c = 'Reschedule';
			task1.Suborder__c = suborder1.Id;
			insert task1;

			CS_COMCloseCaseController.closeCase(task1.Id);

			List<Suborder__c> suborders = [SELECT Id, Installation_Jeopardy_Result__c
			                                      FROM Suborder__c
			                                      WHERE Id = :suborder1.Id];

			System.assertEquals(suborders[0].Installation_Jeopardy_Result__c,task1.Install_Solution_Jeopardy_Outcome__c);
		}
	}

	@IsTest
	static void testBehaviourPMProjectStartSolNotKickedOff() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs (simpleUser) {
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
			insert ord;

			List<Suborder__c> suborders = CS_DataTest.createMultipleSolutions('Solution 1', ord.Id, 5, true);

			Task task1 = CS_DataTest.createTask('Delivery Project Start', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Prepare Project';
			task1.Order__c = ord.Id;
			insert task1;

			try {
				CS_COMCloseCaseController.closeCase(task1.Id);
			} catch (Exception e) {
				System.assertEquals(CS_COMCloseCaseController.PM_START_CASE_SOLUTIONS_NOT_KICKED_OFF, e.getMessage());
			}
		}
	}

	@IsTest
	static void testBehaviourPMProjectStartSuccess() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs (simpleUser) {
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
			insert ord;

			List<Suborder__c> suborders = CS_DataTest.createMultipleSolutions('Solution 1', ord.Id, 5, false);
			for (Suborder__c sol : suborders) {
				sol.Start_Delivery__c = true;
			}
			insert suborders;

			Task task1 = CS_DataTest.createTask('Delivery Project Start', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Prepare Project';
			task1.Order__c = ord.Id;
			insert task1;

			task1.OwnerId = simpleUser.Id;
			update task1;
			
			Test.startTest();
			String result = CS_COMCloseCaseController.closeCase(task1.Id);
			Test.stopTest();

			task1 = [SELECT Id, Status, OwnerId FROM Task WHERE Id = :task1.Id];
			System.assertEquals('Task Closed', result);
			System.assertEquals('Completed', task1.Status, 'Task should be Completed');
		}
	}
}