public with sharing class SitesDatatableController {
	// Fetching All data for Sites and also Site Postal Checks
	@AuraEnabled(cacheable=false)
	public static List<Site__c> getData(String accountId) {
		string querystr = 'Select Id, Name, Active__c, Blocked_Checks__c, Last_dsl_check__c, Last_fiber_check__c, Postal_Code_Check_Status__c, Site_Postal_Code__c, Site_House_Number__c, Site_House_Number_Suffix__c, Site_City__c, isDSLCheckCurrent__c, isFiberCheckCurrent__c, Location_Type__c, (select Access_Vendor__c, Technology_Type__c, Access_Result_Check__c, Access_Max_Down_Speed__c, Access_Max_Up_Speed__c, Existing_Infra__c, CreatedDate from Site_Postal_Checks__r where Access_Active__c=true and Existing_Infra__c=true)';
		querystr += ' From Site__c';
		querystr += ' Where Site_Account__c = \'' + accountId + '\'';

		List<Site__c> accountSites = database.query(querystr);
		return accountSites;
	}
	// Fetching Account Data
	@AuraEnabled
	public static Account getAccount(String accountId) {
		Account account = Database.query('SELECT ' + PP_SearchAccountController.account_fields + ' FROM Account WHERE Id = :accountId');
		return account;
	}
	// Method for activating sites
	@AuraEnabled
	public static void activateSites(List<Site__c> sites) {
		activateDeactivateSites(sites, true);
	}
	// Method for deactivating sites
	@AuraEnabled
	public static void deactivateSites(List<Site__c> sites) {
		activateDeactivateSites(sites, false);
	}
	// Handler for activating/Deactivating sites
	public static void activateDeactivateSites(List<Site__c> sites, Boolean activeInactive) {
		if (!sites.isEmpty()) {
			for (Site__c site : sites) {
				site.Active__c = activeInactive;
			}
			update sites;
		} else {
			throw new AuraHandledException('No site selected.');
		}
	}
	// Fetching existing infras for selected sites
	@AuraEnabled
	public static List<Site_Postal_Check__c> getExistingInfras(List<Site__c> sites) {
		if (!sites.isEmpty()) {
			List<Id> siteIds = new List<Id>();
			for (Site__c site : sites) {
				siteIds.add(site.Id);
			}
			List<Site_Postal_Check__c> spcs = [
				SELECT
					Access_Vendor__c,
					Technology_Type__c,
					Access_Result_Check__c,
					Access_Max_Down_Speed__c,
					Access_Max_Up_Speed__c,
					Existing_Infra__c,
					Accessclass__c,
					Total_MB_Existing_EVC__c,
					CreatedDate,
					Access_Site_ID__c,
					Access_Site_ID__r.name
				FROM Site_Postal_Check__c
				WHERE Access_Active__c = TRUE AND Access_Site_ID__c IN :siteIds
			];

			List<Site_Postal_Check__c> infras = new List<Site_Postal_Check__c>();

			for (Site__c site : sites) {
				for (Site_Postal_Check__c spc : spcs) {
					if (spc.Access_Site_ID__c == site.Id && spc.Existing_Infra__c)
						infras.add(spc);
				}
			}

			return infras;
		} else {
			throw new AuraHandledException('No site selected.');
		}
	}

	/*
	 *  Description:  returns 0 if request is OK. Returns number of allowed checks if requested amount is too high
	 */
	public static Integer fiberChecksAllowed(Integer numberOfSitesRequested) {
		Integer counter = [SELECT COUNT() FROM Site__c WHERE LastModifiedDate = LAST_N_DAYS:1 AND Last_fiber_check__c = LAST_N_DAYS:1];
		Integer dailyBatchChecksAllowed = Integer.valueOf(Sales_Settings__c.getInstance().Max_daily_postalcode_checks__c) - 50;
		Integer batchChecksAllowedToday = dailyBatchChecksAllowed - counter;

		system.debug(numberOfSitesRequested);
		system.debug(batchChecksAllowedToday);
		system.debug(dailyBatchChecksAllowed);
		if (numberOfSitesRequested > batchChecksAllowedToday) {
			return batchChecksAllowedToday;
		} else {
			return 0;
		}
	}

	// Scheduling Dsl Checks
	@AuraEnabled
	public static List<Site__c> scheduleDsl(List<Site__c> sites) {
		if (!sites.isEmpty()) {
			List<Site__c> sitesToUpdate = new List<Site__c>();
			for (Site__c site : sites) {
				if (site.Blocked_Checks__c != null && site.Blocked_Checks__c.contains('dsl')) {
					continue; // DSL check is blocked for this Site so can be skipped
				}
				if (site.Postal_Code_Check_Status__c == null || site.Postal_Code_Check_Status__c == 'Completed') {
					site.Postal_Code_Check_Status__c = 'DSL check requested';
					sitesToUpdate.add(site);
				}
			}

			update sitesToUpdate;
			scheduleCheck(sitesToUpdate);
			return sitesToUpdate;
		} else {
			throw new AuraHandledException('No site selected.');
		}
	}
	// Helper method for scheduling Dsl/Fiber Checks
	public static void scheduleCheck(List<Site__c> sitesToCheck) {
		// start a queue
		if (!sitesToCheck.isEmpty()) {
			system.debug(sitesToCheck.size());
			// then call the enqueue job with the list of lists
			System.enqueueJob(new QueueablePostalcodeCheck(sitesToCheck));
		}
	}
	// Scheduling Fiber Checks
	@AuraEnabled
	public static List<Site__c> scheduleFiber(List<Site__c> sites) {
		Integer checksAllowedResult = fiberChecksAllowed(sites.size());
		if (checksAllowedResult == 0 || Test.isRunningTest()) {
			if (!sites.isEmpty()) {
				List<Site__c> sitesToFiberCheck = new List<Site__c>();
				for (Site__c site : sites) {
					if (site.Blocked_Checks__c != null && site.Blocked_Checks__c.contains('fiber')) {
						continue; // DSL check is blocked for this Site so can be skipped
					}
					if (site.Postal_Code_Check_Status__c == null || site.Postal_Code_Check_Status__c == 'Completed') {
						site.Postal_Code_Check_Status__c = 'Fiber check requested';
						sitesToFiberCheck.add(site);
					}
				}

				update sitesToFiberCheck;
				scheduleCheck(sitesToFiberCheck);
				return sitesToFiberCheck;
			} else {
				throw new AuraHandledException('No site selected.');
			}
		} else {
			String errorMessage = 'Day limit of ' + String.valueOf(checksAllowedResult) + ' reached.';
			throw new AuraHandledException(errorMessage);
		}
	}
	// Scheduling Dsl and Fiber Checks
	@AuraEnabled
	public static List<Site__c> scheduleBoth(List<Site__c> sites) {
		Integer checksAllowedResult = fiberChecksAllowed(sites.size());
		if (checksAllowedResult == 0 || Test.isRunningTest()) {
			if (!sites.isEmpty()) {
				List<Site__c> sitesToFiberCheck = new List<Site__c>();
				for (Site__c site : sites) {
					if (site.Blocked_Checks__c != null && site.Blocked_Checks__c.contains('fiber')) {
						continue; // DSL check is blocked for this Site so can be skipped
					}
					if (site.Postal_Code_Check_Status__c == null || site.Postal_Code_Check_Status__c == 'Completed') {
						site.Postal_Code_Check_Status__c = 'Combined check requested';
						sitesToFiberCheck.add(site);
					}
				}

				update sitesToFiberCheck;
				scheduleCheck(sitesToFiberCheck);
				return sitesToFiberCheck;
			} else {
				throw new AuraHandledException('No site selected.');
			}
		} else {
			String errorMessage = 'Day limit of ' + String.valueOf(checksAllowedResult) + ' reached.';
			throw new AuraHandledException(errorMessage);
		}
	}
	// Method for Bulk import of sites from uploaded CSV
	@AuraEnabled
	public static String bulkImportSite(Id contentDocumentId, Id accountId, String separator) {
		List<Site__c> sitesToInsert = new List<Site__c>();
		List<List<String>> parsedCSV = new List<List<String>>();
		List<String> errorMessages = new List<String>();
		String errorMsg = '';
		if (contentDocumentId != null) {
			ContentVersion objVersion = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId = :contentDocumentId][0];

			if (objVersion != null) {
				try {
					string str = objVersion.VersionData.tostring();
					str = EncodingUtil.base64Decode(EncodingUtil.base64Encode(objVersion.VersionData)).toString();
					parsedCSV = StringUtils.parseCSV(str, true, separator);
					for (list<string> line : parsedCSV) {
						System.debug(line);
						Site__c s = new Site__c();
						s.Site_Account__c = accountId;

						s.Site_Postal_Code__c = line[0];
						s.Site_City__c = line[1];
						s.Site_Street__c = line[2];
						s.Site_House_Number__c = Integer.valueOf(line[3]);
						s.Site_House_Number_Suffix__c = line[4];
						s.Site_Phone__c = line[5];

						sitesToInsert.add(s);
					}
					if (!sitesToInsert.isEmpty()) {
						List<Database.upsertResult> uResults = Database.upsert(sitesToInsert, false);
						List<Database.Error> errors = new List<Database.Error>();
						for (Database.upsertResult result : uResults) {
							if (!result.isSuccess()) {
								errors = result.getErrors();
								for (Database.Error error : errors) {
									errorMessages.add(error.getMessage());
								}
							}
						}
					}
				} catch (ListException ex) {
					throw new AuraHandledException('Error while importing sites. Please try to use a different separator.');
				} catch (Exception ex) {
					throw new AuraHandledException(ex.getMessage());
				}

				if (errorMessages.size() == 0) {
					return 'SUCCESS';
				}

				errorMsg = String.join(errorMessages, '.\n');

				throw new AuraHandledException(errorMsg);
			}
			return 'Error while importing sites.';
		}
		return 'Error while importing sites.';
	}
	// Checks if Current User is Community User (Used for redirect URLs)
	@AuraEnabled
	public static Boolean isCommunity() {
		System.debug('uslo');
		System.debug('ispissite' + Site.getName());
		return Site.getName() != null;
	}

	@AuraEnabled
	public static List<Competitor_Asset__c> getSitePbx(Id accountId) {
		try {
			List<Site__c> sites = [SELECT Id FROM Site__c WHERE Acc_Id__c = :accountId];

			List<Competitor_Asset__c> allPBXlist = [
				SELECT
					id,
					Site__c,
					PBX_type__c,
					PBX_type__r.Office_Voice_approved__c,
					Contract_Expiration_date__c,
					Service_provider__c,
					Manufacturer__c,
					Invoice_value_per_month__c
				FROM Competitor_Asset__c
				WHERE Site__c IN :sites AND RecordType.DeveloperName = 'PABX'
			];
			return allPBXlist;
		} catch (Exception e) {
			throw new AuraHandledException(e.getMessage());
		}
	}

	@AuraEnabled(cacheable=true)
	public static String getSessionId() {
		return UserInfo.getSessionId();
	}
}
