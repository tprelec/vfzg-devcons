@isTest
private class ChargesProcessorUtilsTest {
	@isTest
	static void getCommonCartDiscountForDecomposition() {
		CCAQDProcessor.CCDiscount aqdDiscount = new CCAQDProcessor.CCDiscount();
		aqdDiscount.source = 'discount1';
		aqdDiscount.isPRSDiscount = false;
		aqdDiscount.type = 'PER';
		aqdDiscount.amount = 2;
		aqdDiscount.discountCharge = 'charge 1';
		aqdDiscount.chargeType = 'oneOff';

		System.Test.startTest();
		cspsi.CommonCartWrapper.Discount commonCartDiscount = ChargesProcessorUtils.getCommonCartDiscount(
			aqdDiscount,
			ChargesProcessorUtils.QUANTITY_STRATEGY_DECOMPOSITION,
			2
		);
		System.Test.stopTest();

		System.assertEquals(2, commonCartDiscount.amount, 'should set the amount properly');
		System.assertEquals(ChargesProcessorUtils.PERCENTAGE_DISCOUNT_TYPE, commonCartDiscount.type, 'should set the type properly');
		System.assertEquals('oneOff', commonCartDiscount.chargeType, 'should set the chargeType properly');
		System.assertEquals('charge 1 Charge', commonCartDiscount.discountCharge, 'should set the discountCharge properly');
		System.assertEquals(ChargesProcessorUtils.SALES_DISCOUNT_PRICE, commonCartDiscount.discountPrice, 'should set the discountPrice properly');
		System.assertEquals('discount1', commonCartDiscount.source, 'should set the source properly');
		System.assertEquals(ChargesProcessorUtils.DISCOUNT_VERSION, commonCartDiscount.version, 'should set the version properly');
		System.assertEquals('discount1 for charge 1', commonCartDiscount.description, 'should set the description properly');
	}

	@isTest
	static void getCommonCartDiscountForMultiplier() {
		CCAQDProcessor.CCDiscount aqdDiscount = new CCAQDProcessor.CCDiscount();
		aqdDiscount.source = 'discount1';
		aqdDiscount.isPRSDiscount = false;
		aqdDiscount.type = 'ABS';
		aqdDiscount.amount = 2;
		aqdDiscount.discountCharge = 'charge 1';
		aqdDiscount.chargeType = 'oneOff';

		System.Test.startTest();
		cspsi.CommonCartWrapper.Discount commonCartDiscount = ChargesProcessorUtils.getCommonCartDiscount(
			aqdDiscount,
			ChargesProcessorUtils.QUANTITY_STRATEGY_MULTIPLIER,
			2
		);
		System.Test.stopTest();

		System.assertEquals(4, commonCartDiscount.amount, 'should set the amount properly');
		System.assertEquals(ChargesProcessorUtils.ABSOLUTE_DISCOUNT_TYPE, commonCartDiscount.type, 'should set the type properly');
	}

	@isTest
	static void getCommonCartDiscountFromAQDChargeListPrice() {
		CCAQDProcessor.CCCharge aqdCharge = new CCAQDProcessor.CCCharge();
		aqdCharge.source = 'charge1';
		aqdCharge.listPrice = 100;
		aqdCharge.salesPrice = 86;
		aqdCharge.name = 'charge 1';
		aqdCharge.chargeType = 'oneOff';
		aqdCharge.description = 'charge 1 description';

		System.Test.startTest();
		cspsi.CommonCartWrapper.Discount commonCartDiscount = ChargesProcessorUtils.getCommonCartDiscountFromAQDCharge(
			aqdCharge,
			ChargesProcessorUtils.LIST_DISCOUNT_PRICE,
			ChargesProcessorUtils.QUANTITY_STRATEGY_DECOMPOSITION,
			2
		);
		System.Test.stopTest();

		System.assertEquals(100, commonCartDiscount.amount, 'should set the amount properly');
		System.assertEquals(ChargesProcessorUtils.OVERRIDE_CHARGE_TYPE, commonCartDiscount.type, 'should set the type properly');
		System.assertEquals('oneOff', commonCartDiscount.chargeType, 'should set the chargeType properly');
		System.assertEquals('charge 1 Charge', commonCartDiscount.discountCharge, 'should set the discountCharge properly');
		System.assertEquals(ChargesProcessorUtils.LIST_DISCOUNT_PRICE, commonCartDiscount.discountPrice, 'should set the discountPrice properly');
		System.assertEquals('charge1', commonCartDiscount.source, 'should set the source properly');
		System.assertEquals(ChargesProcessorUtils.DISCOUNT_VERSION, commonCartDiscount.version, 'should set the version properly');
		System.assertEquals('charge 1 description', commonCartDiscount.description, 'should set the description properly');
	}

	@isTest
	static void getCommonCartDiscountFromAQDChargeSalesPrice() {
		CCAQDProcessor.CCCharge aqdCharge = new CCAQDProcessor.CCCharge();
		aqdCharge.source = 'charge1';
		aqdCharge.listPrice = 100;
		aqdCharge.salesPrice = 86;
		aqdCharge.name = 'charge 1';
		aqdCharge.chargeType = 'oneOff';
		aqdCharge.description = 'charge 1 description';

		System.Test.startTest();
		cspsi.CommonCartWrapper.Discount commonCartDiscount = ChargesProcessorUtils.getCommonCartDiscountFromAQDCharge(
			aqdCharge,
			ChargesProcessorUtils.SALES_DISCOUNT_PRICE,
			ChargesProcessorUtils.QUANTITY_STRATEGY_MULTIPLIER,
			2
		);
		System.Test.stopTest();

		System.assertEquals(172, commonCartDiscount.amount, 'should set the amount properly');
		System.assertEquals(ChargesProcessorUtils.OVERRIDE_CHARGE_TYPE, commonCartDiscount.type, 'should set the type properly');
		System.assertEquals('oneOff', commonCartDiscount.chargeType, 'should set the chargeType properly');
		System.assertEquals('charge 1 Charge', commonCartDiscount.discountCharge, 'should set the discountCharge properly');
		System.assertEquals(ChargesProcessorUtils.SALES_DISCOUNT_PRICE, commonCartDiscount.discountPrice, 'should set the discountPrice properly');
		System.assertEquals('charge1', commonCartDiscount.source, 'should set the source properly');
		System.assertEquals(ChargesProcessorUtils.DISCOUNT_VERSION, commonCartDiscount.version, 'should set the version properly');
		System.assertEquals('charge 1 description', commonCartDiscount.description, 'should set the description properly');
	}
}
