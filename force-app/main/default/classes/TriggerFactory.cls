@SuppressWarnings('PMD')
public with sharing class TriggerFactory {
	/**
	 * @description     Class which will be instanstiated if no handler can be located
	 */
	private class EmptyTriggerHandler extends TriggerHandler {
	}

	/**
	 * @description     Locates and instantiates the registered handler for an sObject.
	 * @param soType    The sObject type to locate the handler for
	 */
	public static void createHandler(Schema.sObjectType soType) {
		getHandler(soType).execute();
	}

	/**
	 * @description     Instantiates the correct handler for the sObject type being processed.
	 * @param soType    The sObject type to instaniate the handler for
	 * @return          Instance of a new trigger handler for the requested sObject type
	 */
	@TestVisible
	private static TriggerHandler getHandler(Schema.sObjectType soType) {
		if (soType == VF_Asset__c.sObjectType) {
			return new VF_AssetTriggerHandler();
		} else if (soType == Account.sObjectType) {
			return new AccountTriggerHandler();
		} else if (soType == Contact.sObjectType) {
			return new ContactTriggerHandler();
		} else if (soType == VF_Contract__c.sObjectType) {
			return new VF_ContractTriggerHandler();
		} else if (soType == Field_Sync_Mapping__c.sObjectType) {
			return new FieldSyncMappingTriggerHandler();
		} else if (soType == Lead.sObjectType) {
			return new LeadTriggerHandler();
		} else if (soType == Numberporting_row__c.sObjectType) {
			return new Numberporting_rowTriggerHandler();
		} else if (soType == Opportunity.sObjectType) {
			return new OpportunityTriggerHandler();
		} else if (soType == Order__c.sObjectType) {
			return new OrderTriggerHandler();
		} else if (soType == Site__c.sObjectType) {
			return new SiteTriggerHandler();
		} else if (soType == User.sObjectType) {
			return new UserTriggerHandler();
		} else if (soType == agf__ADM_Work__c.sObjectType) {
			return new ADMWorkTriggerHandler();
		} else if (soType == agf__ADM_Task__c.sObjectType) {
			return new ADMTaskTriggerHandler();
		} else if (soType == cscfga__Product_Configuration__c.sObjectType) {
			return new ProductConfigurationTriggerHandler();
		} else if (soType == Queue_User_Sharing__c.sObjectType) {
			return new QueueUserSharingTriggerHandler();
		} else if (soType == Task.sObjectType) {
			return new TaskTriggerHandler();
		}

		return new EmptyTriggerHandler();
	}
}
