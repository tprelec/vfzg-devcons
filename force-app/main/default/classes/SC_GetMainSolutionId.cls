/**
 * Get solution id for a given solution name
 * @author Petar Matkovic @ CloudSense
 * @since 21/08/2020
 *
 * Q2F-821
 */
global with sharing class SC_GetMainSolutionId implements cssmgnt.RemoteActionDataProvider {
	global Map<String, Object> getData(Map<String, Object> inputMap) {
		Map<String, Object> returnMap = new Map<String, Object>();

		if (!inputMap.containsKey('solutionName')) {
			throw new CS_Exception('SC_GetMainSolutionId: Solution name is not provided.');
		}

		String solutionName = String.valueOf(inputMap.get('solutionName'));

		returnMap.put('solutionId', getSolutionId(solutionName));

		return returnMap;
	}

	private Id getSolutionId(String solutionName) {
		List<cssdm__Solution_Definition__c> objs = [
			SELECT Id, Name
			FROM cssdm__Solution_Definition__c
			WHERE Name = :solutionName AND cssdm__type__c = 'Main' AND (cssdm__effective_end_date__c = NULL OR cssdm__effective_end_date__c > TODAY)
		];

		if (objs.isEmpty()) {
			throw new CS_Exception('Unable to find the folowing solution Id: ' + solutionName);
		}

		return objs[0].Id;
	}
}
