@isTest
private class TestCOM_DXLCreateCustomerListener {
	private static final String COM_DXL_ASYN_SUCCESS = '{"payload":{"transactionId":"TR-123456","state": "completed","customerDetails":{"companyDetails":{"unifyAccountRefId":"Unify-123456789","bopCompanyCode":"BOP-123456789"},"billingCustomer":{"unifyBillingCustomerRefId":"UBC-123"}},"billingEntities":{"unifyFinancialAccountRefId":"UFA-123","unifyBillingAccountRefId":"UBA-123"},"contacts":[{"unifyContactRefId":"Unify-123456789","salesforceAccountId":"","salesforceContactId":""},{"unifyContactRefId":"Unify-987654321","salesforceAccountId":"","salesforceContactId":""}],"sites":[{"unifySiteRefId":"unifySiteRefId","salesforceSiteId":"","salesforceAccountId":"","siteLevelServicesAPId":"siteLevelServicesAPId"}]}}';
	private static final String COM_DXL_AFFECTED_OBJECTS = '[{"type":"csord__Order__c","sfdcId":"a6D1l0000000d9EEAQ"},{"type":"Account","sfdcId":""},{"type":"BAN__c","sfdcId":""},{"type":"Financial_Account__c","sfdcId":""},{"type":"Billing_Arrangement__c","sfdcId":""},{"type":"Site__c","sfdcId":""},{"type":"Contact","sfdcId":""},{"type":"Contact","sfdcId":""}]';
	@testSetup
	static void setup() {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		List<External_Account__c> externalAccounts = new List<External_Account__c>();

		External_account__c externalaccountcObj1 = new External_account__c(Account__c = acct.Id, External_Source__c = 'Unify');

		External_account__c externalaccountcObj2 = new External_account__c(Account__c = acct.Id, External_Source__c = 'BOP');

		externalAccounts.add(externalaccountcObj1);
		externalAccounts.add(externalaccountcObj2);

		insert externalAccounts;

		Contact testContact = new Contact(AccountId = acct.Id, Phone = '+31 111111789', LastName = 'Fixed', Userid__c = owner.Id);
		insert testContact;

		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, testContact.Id);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);
		Site__c site = TestUtils.createSite(acct);
		csord__Order__c ord = CS_DataTest.createOrder();
		ord.csord__Status2__c = 'Created';
		insert ord;

		Com_dxl_integration_log__c comdxlintegrationlogcObj = new Com_dxl_integration_log__c(
			Transaction_Id__c = 'TR-123456',
			Type__c = 'createCustomer',
			Status__c = 'Success',
			Affected_Objects__c = updateAffectedObjects(COM_DXL_AFFECTED_OBJECTS, acct, ban, fa, bar, testContact, ord),
			Error__c = 'Test Value'
		);
		insert comdxlintegrationlogcObj;
	}

	private static String updateAffectedObjects(
		String affectedObjectsTemplate,
		Account acct,
		Ban__c ban,
		Financial_Account__c fa,
		Billing_Arrangement__c bar,
		Contact cnt,
		csord__Order__c ord
	) {
		List<COM_DxlAffectedObjects> affectedObjects = (List<COM_DxlAffectedObjects>) JSON.deserializeStrict(
			affectedObjectsTemplate,
			List<COM_DxlAffectedObjects>.class
		);
		for (COM_DxlAffectedObjects affectedObject : affectedObjects) {
			if (affectedObject.type == 'Account') {
				affectedObject.sfdcId = acct.Id;
			} else if (affectedObject.type == 'BAN__c') {
				affectedObject.sfdcId = ban.Id;
			} else if (affectedObject.type == 'Financial_Account__c') {
				affectedObject.sfdcId = fa.Id;
			} else if (affectedObject.type == 'Billing_Arrangement__c') {
				affectedObject.sfdcId = bar.Id;
			} else if (affectedObject.type == 'Contact') {
				affectedObject.sfdcId = cnt.Id;
			} else if (affectedObject.type == 'csord__Order__c' && ord != null) {
				affectedObject.sfdcId = ord.Id;
			}
		}

		return JSON.serialize(affectedObjects);
	}

	private static String updateAsyncResponse(String asyncResponseTemplate, Account acct, Contact cnt, Site__c site) {
		COM_DXLServiceAsyncResponse asyncResponse = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			asyncResponseTemplate,
			COM_DXLServiceAsyncResponse.class
		);

		for (COM_DXLServiceAsyncResponse.Contact responseContact : asyncResponse.payload.contacts) {
			responseContact.salesforceAccountId = acct.Id;
			responseContact.salesforceContactId = cnt.Id;
		}

		for (COM_DXLServiceAsyncResponse.Site responseSite : asyncResponse.payload.sites) {
			responseSite.salesforceAccountId = acct.Id;
			responseSite.salesforceSiteId = site.Id;
		}

		return JSON.serialize(asyncResponse);
	}

	@isTest
	static void testUpdateTransactionLogRecord() {
		String status = 'Processed';
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];

		List<Com_dxl_integration_log__c> integrationLogData = [SELECT Id, Status__c FROM Com_dxl_integration_log__c];

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);

		COM_DXLCreateCustomerListener createCustomer = new COM_DXLCreateCustomerListener(response);
		createCustomer.updateTransactionLogRecord(integrationLogData[0], COM_DXL_Constants.COM_DXL_PROCESSED_STATUS, '');

		List<Com_dxl_integration_log__c> integrationLogDataAfterUpdate = [SELECT Id, Status__c FROM Com_dxl_integration_log__c];

		System.assertEquals(
			status,
			integrationLogDataAfterUpdate[0].Status__c,
			'Expected ' +
			status +
			' Actual ' +
			integrationLogDataAfterUpdate[0].Status__c
		);
	}

	@isTest
	static void testCreateExternalSites() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);
		List<External_Account__c> externalAccounts = [SELECT Id, Account__c, External_Source__c FROM External_Account__c];

		COM_DXLCreateCustomerListener createCustomer = new COM_DXLCreateCustomerListener(response);
		Integer numOfResults = 1;
		List<External_Site__c> results = createCustomer.createExternalSites(response, externalAccounts);
		System.assertEquals(numOfResults, results.size(), 'Expected ' + numOfResults + ' Actual: ' + results.size());
	}

	@isTest
	static void testCreateExternalContacts() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);
		List<External_Account__c> externalAccounts = [SELECT Id, Account__c, External_Source__c FROM External_Account__c];

		COM_DXLCreateCustomerListener createCustomer = new COM_DXLCreateCustomerListener(response);

		Integer numOfResults = 2;
		List<External_Contact__c> results = createCustomer.createExternalContacts(response, externalAccounts);
		System.assertEquals(numOfResults, results.size(), 'Expected ' + numOfResults + ' Actual: ' + results.size());
	}

	@isTest
	static void testUpdateFinancialAccount() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];
		List<Ban__c> banList = [SELECT Id, Name FROM Ban__c];
		List<Financial_Account__c> faList = [SELECT Id, Name FROM Financial_Account__c];
		List<Billing_Arrangement__c> barList = [SELECT Id, Name FROM Billing_Arrangement__c];

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);
		List<COM_DxlAffectedObjects> affectedObjects = (List<COM_DxlAffectedObjects>) JSON.deserializeStrict(
			updateAffectedObjects(COM_DXL_AFFECTED_OBJECTS, acctList[0], banList[0], faList[0], barList[0], contactList[0], null),
			List<COM_DxlAffectedObjects>.class
		);

		COM_DXLCreateCustomerListener createCustomer = new COM_DXLCreateCustomerListener(response);

		Financial_Account__c result = createCustomer.updateFinancialAccount(response, affectedObjects);
		System.assertEquals(
			response.payload.billingEntities.unifyFinancialAccountRefId,
			result.Unify_Ref_Id__c,
			'Expected ' +
			response.payload.billingEntities.unifyFinancialAccountRefId +
			' Actual: ' +
			result.Unify_Ref_Id__c
		);
	}

	@isTest
	static void testUpdateBillingArangement() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];
		List<Ban__c> banList = [SELECT Id, Name FROM Ban__c];
		List<Financial_Account__c> faList = [SELECT Id, Name FROM Financial_Account__c];
		List<Billing_Arrangement__c> barList = [SELECT Id, Name FROM Billing_Arrangement__c];

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);
		List<COM_DxlAffectedObjects> affectedObjects = (List<COM_DxlAffectedObjects>) JSON.deserializeStrict(
			updateAffectedObjects(COM_DXL_AFFECTED_OBJECTS, acctList[0], banList[0], faList[0], barList[0], contactList[0], null),
			List<COM_DxlAffectedObjects>.class
		);

		COM_DXLCreateCustomerListener createCustomer = new COM_DXLCreateCustomerListener(response);

		Billing_Arrangement__c result = createCustomer.updateBillingArangement(response, affectedObjects);
		System.assertEquals(
			response.payload.billingEntities.unifyBillingAccountRefId,
			result.Unify_Ref_Id__c,
			'Expected ' +
			response.payload.billingEntities.unifyBillingAccountRefId +
			' Actual: ' +
			result.Unify_Ref_Id__c
		);
	}

	@isTest
	static void testUpdateBanRecord() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];
		List<Ban__c> banList = [SELECT Id, Name FROM Ban__c];
		List<Financial_Account__c> faList = [SELECT Id, Name FROM Financial_Account__c];
		List<Billing_Arrangement__c> barList = [SELECT Id, Name FROM Billing_Arrangement__c];

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);
		List<COM_DxlAffectedObjects> affectedObjects = (List<COM_DxlAffectedObjects>) JSON.deserializeStrict(
			updateAffectedObjects(COM_DXL_AFFECTED_OBJECTS, acctList[0], banList[0], faList[0], barList[0], contactList[0], null),
			List<COM_DxlAffectedObjects>.class
		);

		COM_DXLCreateCustomerListener createCustomer = new COM_DXLCreateCustomerListener(response);

		BAN__c result = createCustomer.updateBanRecord(response, affectedObjects);
		System.assertEquals(
			response.payload.customerDetails.billingCustomer.unifyBillingCustomerRefId,
			result.Unify_Ref_Id__c,
			'Expected ' +
			response.payload.customerDetails.billingCustomer.unifyBillingCustomerRefId +
			' Actual: ' +
			result.Unify_Ref_Id__c
		);
	}

	@isTest
	static void testUpdateAccountRecord() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Ban__c> banList = [SELECT Id, Name FROM Ban__c];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];
		List<Financial_Account__c> faList = [SELECT Id, Name FROM Financial_Account__c];
		List<Billing_Arrangement__c> barList = [SELECT Id, Name FROM Billing_Arrangement__c];

		List<COM_DxlAffectedObjects> affectedObjects = (List<COM_DxlAffectedObjects>) JSON.deserializeStrict(
			updateAffectedObjects(COM_DXL_AFFECTED_OBJECTS, acctList[0], banList[0], faList[0], barList[0], contactList[0], null),
			List<COM_DxlAffectedObjects>.class
		);

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);
		COM_DXLCreateCustomerListener createCustomer = new COM_DXLCreateCustomerListener(response);

		Account result = createCustomer.updateAccountRecord(affectedObjects);
		System.assertEquals(true, result.BOP_Export_Datetime__c != null, 'Expected ' + true + ' Actual: ' + result.BOP_Export_Datetime__c != null);
	}

	@isTest
	static void testCreateExternalAccounts() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];
		List<Ban__c> banList = [SELECT Id, Name FROM Ban__c];
		List<Financial_Account__c> faList = [SELECT Id, Name FROM Financial_Account__c];
		List<Billing_Arrangement__c> barList = [SELECT Id, Name FROM Billing_Arrangement__c];

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);

		List<COM_DxlAffectedObjects> affectedObjects = (List<COM_DxlAffectedObjects>) JSON.deserializeStrict(
			updateAffectedObjects(COM_DXL_AFFECTED_OBJECTS, acctList[0], banList[0], faList[0], barList[0], contactList[0], null),
			List<COM_DxlAffectedObjects>.class
		);

		COM_DXLCreateCustomerListener createCustomer = new COM_DXLCreateCustomerListener(response);

		List<External_Account__c> result = createCustomer.createExternalAccounts(response, affectedObjects);

		Integer numOfRecords = 2;
		System.assertEquals(numOfRecords, result.size(), 'Expected ' + numOfRecords + ' Actual: ' + result.size());
	}

	@isTest
	static void testProcessPayload() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];

		/*RestRequest req = new RestRequest();
		RestResponse res = new RestResponse();
		req.requestURI = '/services/apexrest/dxlnotificationListener';
		req.httpMethod = 'POST';
		req.requestBody = Blob.valueOf(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0])
		);
		RestContext.request = req;
		RestContext.response = res;*/

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);

		COM_DXLCreateCustomerListener customerListener = new COM_DXLCreateCustomerListener(response);
		customerListener.processPayload();

		COM_DXL_Integration_Log__c integrationLogrecord = [
			SELECT Id, Transaction_Id__c, Status__c, Affected_Objects__c
			FROM COM_DXL_Integration_Log__c
			WHERE Transaction_Id__c = 'TR-123456'
		][0];

		String result = 'Processed';
		System.assertEquals(result, integrationLogrecord.Status__c, 'Expected ' + result + ' Actual: ' + integrationLogrecord.Status__c);
	}

	@isTest
	static void testUpdateOrderStatus() {
		List<Account> acctList = [SELECT Id, Name FROM Account WHERE Name = 'Test Account'];
		List<Contact> contactList = [SELECT Id, Name FROM Contact];
		List<Site__c> siteList = [SELECT Id, Name FROM Site__c];
		List<csord__Order__c> orderList = [SELECT Id, Name, csord__Status2__c FROM csord__Order__c];

		COM_DXLServiceAsyncResponse response = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(
			updateAsyncResponse(COM_DXL_ASYN_SUCCESS, acctList[0], contactList[0], siteList[0]),
			COM_DXLServiceAsyncResponse.class
		);

		COM_DXL_Integration_Log__c integrationLogrecord = [
			SELECT Id, Transaction_Id__c, Status__c, Affected_Objects__c
			FROM COM_DXL_Integration_Log__c
			WHERE Transaction_Id__c = 'TR-123456'
		][0];

		COM_DXLCreateCustomerListener customerListener = new COM_DXLCreateCustomerListener(response);
		customerListener.updateOrderStatus(response, integrationLogrecord);

		List<csord__Order__c> ordersAfterUpdate = [SELECT Id, Name, csord__Status2__c FROM csord__Order__c WHERE Id = :orderList[0].Id];
		String expected = 'Test Status';
		String actual = ordersAfterUpdate[0].csord__Status2__c;
		String msg = 'Expected; ' + expected + ' Actual: ' + actual;
		System.assertEquals(expected, actual, msg);
	}
}
