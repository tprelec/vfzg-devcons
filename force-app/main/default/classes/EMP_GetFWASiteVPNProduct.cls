public class EMP_GetFWASiteVPNProduct {

    public static Response parse(String json) {
        return (Response) System.JSON.deserialize(json, Response.class);
    }

    public class Response{
        public Integer status;
        public List<Data> data;
        public List<Error> error;
    }

    public class Data {
        public FrameworkAgreementOutput frameworkAgreementOutput;
    }

    public class FrameworkAgreementOutput {
        public List<FrameworkAgreementData> frameworkAgreementData;
    }

    public class FrameworkAgreementData {
        public String frameworkAgreementId;
        public List<Template> template;
        public List<AgreementComponent> agreementComponents;
        public String mobileDeviceOfferID;
        public List<String> creatorDealerCode;
    }

    public class Template {
        public String templateId;
        public List<TemplateProperty> templateProperties;
        public List<String> creatorDealerCode;
    }

    public class TemplateProperty {
        public String name;
        public String value;
    }

    public class AgreementComponent {
        public String catalogCode;
        public List<ItemAttribute> itemAttributes;
    }

    public class ItemAttribute {
        public String catalogCode;
        public String selectedValue;
        public ValidValue validValue;
    }

    public class ValidValue {
        public String code;
        public String name;
    }
    
    public class Error {
        public String code;
        public String message;
    }
}