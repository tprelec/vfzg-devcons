/**
 * @description         This class schedules the processing of recurring contracted products, create billing transaction and send to SIAS.
 * @author              Rahul Sharma
 */
public class ScheduleSiasBatch implements Schedulable {

    /**
     * @description         This method executes the batch job.
     *                      Marked as readonly to get higher SOQL governor limits
     */
    @ReadOnly
    public void execute(SchedulableContext sc) {
        callSiasBatch();
    }

    // aggregates the order and send to SIAS with a batch class
    @TestVisible
    private static void callSiasBatch() {
        // query all approved CPs
        Set<Id> setOrderIds = new Set<Id>();
        for(AggregateResult cpAggregateResult: [SELECT Order__c orderId FROM
            Contracted_Products__c WHERE 
            Order__c != null AND
            Delivery_Status__c = :Constants.CONTRACTED_PRODUCT_DELIVERY_STATUS_APPROVED AND 
            Billing_Status__c = :Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW AND
            // CLC__c = :Constants.CONTRACTED_PRODUCT_CLC_ACQ AND - JvW 15-01-2020: Removed for W-002334
            Customer_Asset__c != null AND
            // Product__r.Billing_Type__c != :Constants.PRODUCT2_BILLINGTYPE_SPECIAL AND
            Order__r.O2C_Order__c = true AND
            Do_not_export__c = false
            GROUP BY Order__c]) {
            Id orderId = (Id) cpAggregateResult.get('orderId');
            System.debug('@@@@orderId: ' + orderId);
            setOrderIds.add(orderId);
        }

        // As 'Database.executeBatch' is treated as DML due to which callout fails, run th batch's code in future context
        if(Test.isRunningTest()) {
            SiasBatch.executeCalloutAsFuture(setOrderIds);
        } else {
            // as callout limit is 100 per transaction, set batch size as 25 to be on safe side 
            // send contracted products per order to SIAS for billing
            Database.executeBatch(new SiasBatch(setOrderIds), 25);
        }
        
    }

    // variable for batch name
    private static final String SIAS_BATCH_NAME = 'SIAS Job';
    
    public static void scheduleInFiveMinutes() {
        Integer nextFiveMinute = DateTime.now().minute();
        // in order to prevent scheduling conflicts add 1 minute 
        nextFiveMinute++;

        // find the next 5th minute divisible by 5
        do {
            nextFiveMinute++;
        } while (Math.mod(nextFiveMinute, 5) != 0);
        
        if(nextFiveMinute >= 59) {
            nextFiveMinute = 0;
        }

        System.schedule(SIAS_BATCH_NAME, 
            '0 ' + nextFiveMinute + ' * * * ?', 
            new scheduleSiasBatch());
    }

    public static void abort() {
        for(CronTrigger cronTrigger: [SELECT Id FROM CronTrigger where CronJobDetail.Name like :SIAS_BATCH_NAME]) {
            System.abortJob(cronTrigger.Id);
        }
    }
    
}