@isTest
public with sharing class CS_SdwanLookupTest {
	@isTest
	static void lookupTest() {
		List<Profile> pList = [
			SELECT Id, Name
			FROM Profile
			WHERE Name = 'System Administrator'
			LIMIT 1
		];
		List<UserRole> roleList = [
			SELECT Id, Name, DeveloperName
			FROM UserRole u
			WHERE ParentRoleId = NULL
		];

		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			OrderType__c orderType = CS_DataTest.createOrderType();
			insert orderType;
			Product2 product1 = CS_DataTest.createProduct('SDWAN', orderType);
			insert product1;

			Category__c category = CS_DataTest.createCategory('SDWAN');
			insert category;

			Vendor__c vendor1 = CS_DataTest.createVendor('KPNWEAS');
			insert vendor1;

			cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(
				product1,
				category,
				1000,
				500,
				vendor1,
				'',
				''
			);
			priceItem1.cspmb__Recurring_Charge__c = 12;
			priceItem1.cspmb__Price_Item_Code__c = 'SD-WAN';
			insert priceItem1;

			Account testAccount = CS_DataTest.createAccount('Test Account');
			insert testAccount;

			Opportunity testOpp = CS_DataTest.createOpportunity(
				testAccount,
				'Test Opp',
				simpleUser.id
			);
			insert testOpp;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(
				testOpp,
				'Test Basket'
			);
			insert basket;

			Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
				.get('Product Definition')
				.getRecordTypeId();

			cscfga__Product_Definition__c sdwanDefinition = CS_DataTest.createProductDefinition(
				'SDWAN'
			);
			sdwanDefinition.Product_Type__c = 'Fixed';
			sdwanDefinition.RecordTypeId = productDefinitionRecordType;
			insert sdwanDefinition;

			cscfga__Product_Configuration__c sdwanConfig = CS_DataTest.createProductConfiguration(
				sdwanDefinition.Id,
				'SD-WAN',
				basket.Id
			);
			sdwanConfig.cscfga__Root_Configuration__c = null;
			sdwanConfig.cscfga__Parent_Configuration__c = null;
			insert sdwanConfig;

			csbb__Product_Configuration_Request__c pcr = CS_DataTest.createPCR(sdwanConfig);
			pcr.csbb__Product_Configuration__c = sdwanConfig.Id;
			insert pcr;

			Map<String, String> searchFields = new Map<String, String>();
			searchFields.put('ContractTerm', '12');

			CS_SdwanLookup sdwanLookup = new CS_SdwanLookup();
			List<Object> staticResult = CS_SdwanLookup.getSdwans();
			List<Object> result = sdwanLookup.doLookupSearch(
				searchFields,
				String.valueOf(sdwanDefinition.Id),
				null,
				0,
				0
			);

			System.assertNotEquals(0, result.size());
			System.assertNotEquals(0, staticResult.size());
		}
	}
}
