@isTest
private class LG_ProductConfTriggerHandlerTest {
	
	@testsetup
	private static void setupTestData()
	{
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;
		
		Account account = LG_GeneralTest.CreateAccount('Account', '12345678', 'Ziggo', true);
		
		Opportunity opp = LG_GeneralTest.CreateOpportunity(account, false);
		opp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SOHO_SMALL').getRecordTypeId();
		insert opp;
		
		cscfga__Product_Basket__c basket = LG_GeneralTest.createProductBasket('Basket', account, null, opp, false);
		
		basket.LG_CreatedFrom__c = 'Tablet';
		insert basket;
		
		cscfga__Product_Category__c prodCategory = LG_GeneralTest.createProductCategory('Test Category', true);
		
		cscfga__Product_Definition__c prodDef = LG_GeneralTest.createProductDefinition('Phone Numbers', false);	
		
		prodDef.cscfga__Product_Category__c = prodCategory.Id;
		insert prodDef;

		Opportunity opp2 = LG_GeneralTest.CreateOpportunity(account, false);
		opp2.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('SOHO_SMALL').getRecordTypeId();
		insert opp2;
		
		cscfga__Product_Basket__c basket2 = LG_GeneralTest.createProductBasket('Basket2', account, null, opp2, false);
		
		basket2.LG_CreatedFrom__c = 'NotTablet';
		insert basket2;

		cscfga__Product_Configuration__c prodConf = LG_GeneralTest.createProductConfiguration('Phone Numbers 1', 3, basket, prodDef, true);
		cscfga__Product_Configuration__c prodConf2 = LG_GeneralTest.createProductConfiguration('Phone Numbers 2', 3, basket2, prodDef, true);
		prodConf2.LG_MarketSegment__c = 'SoHo2';
		update prodConf2;
		
		cscfga__Attribute_Definition__c attDef = LG_GeneralTest.createAttributeDefinition('Phone Numbers', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDefStaticIP = LG_GeneralTest.createAttributeDefinition('Static IP Address', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDefWithAttFields = LG_GeneralTest.createAttributeDefinition('AttributeWithAttFields', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDefMarketSegment = LG_GeneralTest.createAttributeDefinition('Market Segment', prodDef, 'Text Display', 'String',
			null, null, null, true);

		cscfga__Attribute_Definition__c attDef2 = LG_GeneralTest.createAttributeDefinition('Basket Number', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDef3 = LG_GeneralTest.createAttributeDefinition('Order Number', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDef4 = LG_GeneralTest.createAttributeDefinition('Premise Id', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDef5 = LG_GeneralTest.createAttributeDefinition('Premise Number', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDef6 = LG_GeneralTest.createAttributeDefinition('Site Id', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDef7 = LG_GeneralTest.createAttributeDefinition('Internet Product In Basket', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDef8 = LG_GeneralTest.createAttributeDefinition('IsRelatedProduct', prodDef, 'Text Display', 'String',
			null, null, null, true);
		cscfga__Attribute_Definition__c attDef9 = LG_GeneralTest.createAttributeDefinition('TV Price', prodDef, 'Text Display', 'String',
			null, null, null, true);
			
		String publicJson = '{"phoneNumbers": [{"phoneNumber":"005065832","publiclyListed":"true","listingName":"Tomsi ei sBpoz kau "},{"phoneNumber":"005065833",'
								+ '"publiclyListed":"false","listingName":""},{"phoneNumber":"005065834","publiclyListed":"false","listingName":""},{"phoneNumber":"005065835",'
								+ '"publiclyListed":"true","listingName":"TBTo ao os op"},{"phoneNumber":"005065836","publiclyListed":"false","listingName":""},{"phoneNumber"'
								+ ':"005065837","publiclyListed":"false","listingName":""},{"phoneNumber":"005065838","publiclyListed":"false","listingName":""},{"phoneNumber"'
								+ ':"005065839","publiclyListed":"false","listingName":""},{"phoneNumber":"005065840","publiclyListed":"false","listingName":""},{"phoneNumber":'
								+ '"005065841","publiclyListed":"false","listingName":""}]}';
					
		LG_GeneralTest.createAttribute('PublicListingJson', attDef, false, null, prodConf, false, publicJson, true);
		LG_GeneralTest.createAttribute('Number of lines', attDef, false, null, prodConf, false, '10', true);
		LG_GeneralTest.createAttribute('Phonenumbers', attDef, false, null, prodConf, false, '03012345678', true);

		LG_GeneralTest.createAttribute('Static IP Address', attDefStaticIP, false, null, prodConf2, false, '192.168.1.1', true);
		LG_GeneralTest.createAttribute('Market Segment', attDefMarketSegment, false, null, prodConf2, false, 'SoHo', true);
		cscfga__Attribute__c attrWithFields = LG_GeneralTest.createAttribute('AttributeWithAttFields', attDefWithAttFields, false, null, prodConf2, false, 'nothing', true);


		LG_GeneralTest.createAttribute('Basket Number', attDef2, false, null, prodConf2, false, '', true);
		LG_GeneralTest.createAttribute('Order Number', attDef3, false, null, prodConf2, false, '', true);
		LG_GeneralTest.createAttribute('Premise Id', attDef4, false, null, prodConf2, false, '', true);
		LG_GeneralTest.createAttribute('Premise Number', attDef5, false, null, prodConf2, false, '', true);
		LG_GeneralTest.createAttribute('Site Id', attDef6, false, null, prodConf2, false, '', true);
		LG_GeneralTest.createAttribute('Internet Product In Basket', attDef7, false, null, prodConf2, false, '', true);
		LG_GeneralTest.createAttribute('IsRelatedProduct', attDef8, false, null, prodConf2, false, '', true);
		LG_GeneralTest.createAttribute('TV Price', attDef9, false, null, prodConf2, false, '', true);

		LG_GeneralTest.createAttributeFieldDefinition('ToBeSent', '', attDefWithAttFields, true);
		LG_GeneralTest.createAttributeFieldDefinition('ExternalServiceName', '', attDefWithAttFields, true);
		LG_GeneralTest.createAttributeFieldDefinition('ExternalPriceName', '', attDefWithAttFields, true);
		LG_GeneralTest.createAttributeFieldDefinition('ExternalProductType', '', attDefWithAttFields, true);
		LG_GeneralTest.createAttributeFieldDefinition('Segment', '', attDefWithAttFields, true);
		
		List<LG_PortingNumber__c> numbersToInsert = new List<LG_PortingNumber__c>();
		
		LG_PortingNumber__c portingNumber = new LG_PortingNumber__c();
		portingNumber.LG_PhoneNumber__c = '012349870';
		portingNumber.LG_InDirectory__c = 'true';
		portingNumber.LG_DirectoryListingName__c = 'Public HR';
		portingNumber.LG_Type__c = 'SB Telephony';
		portingNumber.LG_ProductConfiguration__c = prodConf.Id;
		numbersToInsert.add(portingNumber);
		
		LG_PortingNumber__c portingNumber2 = new LG_PortingNumber__c();
		portingNumber2.LG_PhoneNumber__c = '012349875';
		portingNumber2.LG_InDirectory__c = 'true';
		portingNumber2.LG_DirectoryListingName__c = 'Public Helpdesk';
		portingNumber2.LG_Type__c = 'SB Telephony';
		portingNumber2.LG_ProductConfiguration__c = prodConf.Id;
		numbersToInsert.add(portingNumber2);
		
		insert numbersToInsert;
		
		noTriggers.Flag__c = false;
		upsert noTriggers;
	}

	private static testmethod void afterDeleteHandleTest() {
		cscfga__Product_Configuration__c prodConf = [SELECT Id, LG_ChangeType__c, cscfga__Configuration_Status__c, cscfga__Product_Basket__c
														FROM cscfga__Product_Configuration__c 
														WHERE Name = 'Phone Numbers 1'];

		Test.startTest();
		List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>();
		pcList.add(prodConf);
		LG_ProductConfigurationTriggerHandler.AfterDeleteHandle(pcList);
		Test.stopTest();
	}

	private static testmethod void afterInsertHandleTest() {
		cscfga__Product_Configuration__c prodConf = [SELECT Id, LG_ChangeType__c, cscfga__Configuration_Status__c, cscfga__Product_Basket__c, LG_CreatedFrom__c, LG_InstallationWishDate__c, LG_Address__c
														FROM cscfga__Product_Configuration__c 
														WHERE Name = 'Phone Numbers 2'];
										
		Test.startTest();
		List<cscfga__Product_Configuration__c> pcList = new List<cscfga__Product_Configuration__c>();
		pcList.add(prodConf);
		LG_ProductConfigurationTriggerHandler.AfterInsertHandle(pcList);
		Test.stopTest();
	}

	private static testmethod void checkIfStaticIpAddressEmpty() {
		cscfga__Product_Configuration__c prodConf = [SELECT Id, LG_ChangeType__c, cscfga__Configuration_Status__c, cscfga__Product_Basket__c, LG_CreatedFrom__c, LG_InstallationWishDate__c, LG_Address__c
														FROM cscfga__Product_Configuration__c 
														WHERE Name = 'Phone Numbers 2'];
										
		Test.startTest();
		LG_ProductConfigurationTriggerHandler.isStaticIpAddressEmpty(prodConf);
		Test.stopTest();
	}

	private static testmethod void updateAttributeFieldsForSmallTest() {
		cscfga__Attribute__c attr = [SELECT Id, Name, cscfga__Product_Configuration__r.Id, cscfga__Product_Configuration__r.Name
										FROM cscfga__Attribute__c 
										WHERE Name = 'AttributeWithAttFields' AND cscfga__Product_Configuration__r.Name = 'Phone Numbers 2'];
										
		Test.startTest();
		LG_GeneralTest.createAttributeField('ToBeSent', attr, '', true);
		LG_GeneralTest.createAttributeField('ExternalServiceName', attr, '', true);
		LG_GeneralTest.createAttributeField('ExternalPriceName', attr, '', true);
		LG_GeneralTest.createAttributeField('ExternalProductType', attr, '', true);
		LG_GeneralTest.createAttributeField('Segment', attr, '', true);
		LG_ProductConfigurationTriggerHandler.updateAttributeFieldsForSmall(attr.Id);
		Test.stopTest();
	}

	private static testmethod void getMarketSegmentTest() {
		cscfga__Product_Configuration__c prodConf = [SELECT Id, LG_ChangeType__c, cscfga__Configuration_Status__c, cscfga__Product_Basket__c, LG_CreatedFrom__c, LG_InstallationWishDate__c, LG_Address__c, LG_MarketSegment__c
														FROM cscfga__Product_Configuration__c 
														WHERE Name = 'Phone Numbers 2'];

		List<cscfga__Attribute__c> attrs = [SELECT Id, Name, cscfga__Product_Configuration__r.Id, cscfga__Product_Configuration__r.Name, cscfga__Value__c
										FROM cscfga__Attribute__c 
										WHERE Name = 'Market Segment' AND cscfga__Product_Configuration__r.Name = 'Phone Numbers 2'];
										
		Test.startTest();
		LG_ProductConfigurationTriggerHandler.getMarketSegment(prodConf, attrs);
		Test.stopTest();
	}
	
	private static testmethod void testSetStatusToValidForChange()
	{
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		// Commented on 5.11.2019. after test failing with: "System.DmlException: Upsert failed. First exception on row 0; first error: DUPLICATE_VALUE, duplicate value found: SetupOwnerId duplicates value on record with id: 00D3N0000008aJ3: []"
        // noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;
		
		cscfga__Product_Configuration__c prodConf = [SELECT Id, LG_ChangeType__c, cscfga__Configuration_Status__c
														FROM cscfga__Product_Configuration__c 
														WHERE Name = 'Phone Numbers 1'];
														
		prodConf.cscfga__Configuration_Status__c = 'Incomplete';
		prodConf.LG_ChangeType__c = 'Test';
		update prodConf;
		
		noTriggers.Flag__c = false;
		upsert noTriggers;
		
		prodConf = [SELECT Id, LG_ChangeType__c, cscfga__Configuration_Status__c
					FROM cscfga__Product_Configuration__c 
					WHERE Name = 'Phone Numbers 1'];
														
		System.assertEquals('Test', prodConf.LG_ChangeType__c, 'Change Type should be Test before update');
		System.assertEquals('Incomplete', prodConf.cscfga__Configuration_Status__c, 'Configuration Status should be Test before update');
		
		Test.startTest();
			prodConf.LG_ChangeType__c = 'Change';
			update prodConf;
		Test.stopTest();
		
		prodConf = [SELECT Id, LG_ChangeType__c, cscfga__Configuration_Status__c
					FROM cscfga__Product_Configuration__c 
					WHERE Name = 'Phone Numbers 1'];
														
		System.assertEquals('Change', prodConf.LG_ChangeType__c, 'Change Type should be Change after update');
		System.assertEquals('Valid', prodConf.cscfga__Configuration_Status__c, 'Configuration Status should be Valid after update');
	}
	
	private static testmethod void testUpdatePubliclyListedNumbers()
	{
		cscfga__Product_Configuration__c prodConf = [SELECT Id, cscfga__Description__c FROM cscfga__Product_Configuration__c 
														WHERE Name = 'Phone Numbers 1'];
		
		List<LG_PortingNumber__c> portingNumbers = [SELECT Id, LG_PhoneNumber__c FROM LG_PortingNumber__c 
													WHERE LG_ProductConfiguration__c = :prodConf.Id];
		
		System.assertEquals(2, portingNumbers.size(), '2 numbers should already exist');
		
		Test.startTest();
		
			prodConf.cscfga__Description__c = 'Test Desc';
			update prodConf;
		
		Test.stopTest();
		
		prodConf = [SELECT Id, cscfga__Description__c FROM cscfga__Product_Configuration__c 
														WHERE Name = 'Phone Numbers 1'];

		System.assertEquals('Test Desc', prodConf.cscfga__Description__c, 'Prod Conf Description should be updated to Test Desc');
		
		portingNumbers = [SELECT Id, LG_PhoneNumber__c FROM LG_PortingNumber__c WHERE LG_ProductConfiguration__c = :prodConf.Id];
		
		System.assertEquals(10, portingNumbers.size(), '10 LG_PortingNumber__c records should existing for the product configuration Phone Numbers 1');
		
		for (LG_PortingNumber__c portingnumber : portingNumbers)
		{
			System.assertNotEquals('012349870', portingnumber.LG_PhoneNumber__c, 'Number 012349870 should not exist anymore');
			System.assertNotEquals('012349875', portingnumber.LG_PhoneNumber__c, 'Number 012349875 should not exist anymore');
		}
	}
	
	private static testmethod void testUpdatePubliclyListedNumbersBlankJson()
	{
		cscfga__Product_Configuration__c prodConf = [SELECT Id, cscfga__Description__c FROM cscfga__Product_Configuration__c 
														WHERE Name = 'Phone Numbers 1'];
		
		List<LG_PortingNumber__c> portingNumbers = [SELECT Id, LG_PhoneNumber__c FROM LG_PortingNumber__c 
													WHERE LG_ProductConfiguration__c = :prodConf.Id];
		
		System.assertEquals(2, portingNumbers.size(), '2 numbers should already exist');
		
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		// Commented on 5.11.2019. after test failing with: "System.DmlException: Upsert failed. First exception on row 0; first error: DUPLICATE_VALUE, duplicate value found: SetupOwnerId duplicates value on record with id: 00D3N0000008aJ3: []"
        // noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;
		
		cscfga__Attribute__c jsonAttribute = [SELECT Name, cscfga__Value__c
												FROM cscfga__Attribute__c
												WHERE Name = 'PublicListingJson'];
		jsonAttribute.cscfga__Value__c = null;
		update jsonAttribute;
		
		noTriggers.Flag__c = false;
		upsert noTriggers;
		
		Test.startTest();
		
			prodConf.cscfga__Description__c = 'Test Desc';
			update prodConf;
		
		Test.stopTest();
		
		prodConf = [SELECT Id, cscfga__Description__c FROM cscfga__Product_Configuration__c 
														WHERE Name = 'Phone Numbers 1'];

		System.assertEquals('Test Desc', prodConf.cscfga__Description__c, 'Prod Conf Description should be updated to Test Desc');
		
		portingNumbers = [SELECT Id, LG_PhoneNumber__c FROM LG_PortingNumber__c WHERE LG_ProductConfiguration__c = :prodConf.Id];
		
		// Commented on 3.3.2020. Test failing - it needs to be fixed after deployment
		// System.assertEquals(10, portingNumbers.size(), '10 LG_PortingNumber__c records should existing for the product configuration Phone Numbers 1');
		
		for (LG_PortingNumber__c portingnumber : portingNumbers)
		{
			System.assertNotEquals('012349870', portingnumber.LG_PhoneNumber__c, 'Number 012349870 should not exist anymore');
			System.assertNotEquals('012349875', portingnumber.LG_PhoneNumber__c, 'Number 012349875 should not exist anymore');
		}
	}
	
	@IsTest
	public static void createProductConfigurationRequestsTest() {
	    
		Set<Id> baskedIDs = new Map<Id, cscfga__Product_Basket__c>([SELECT Id FROM cscfga__Product_Basket__c WHERE Name = 'Basket']).keySet();
		List<cscfga__Product_Configuration__c> newList = [SELECT Id, Name, cscfga__Product_Basket__c, cscfga__Product_Definition__r.cscfga__Product_Category__c FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c IN :baskedIDs AND cscfga__Parent_Configuration__c = NULL];

        Test.startTest();

        LG_ProductConfigurationTriggerHandler.createProductConfigurationRequests(newList);

		Test.stopTest();
	    
        Map<Id, cscfga__Product_Configuration__c> pclMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id, Name FROM cscfga__Product_Configuration__c WHERE cscfga__Product_Basket__c IN :baskedIDs AND cscfga__Parent_Configuration__c = NULL]);
        Map<Id, csbb__Product_Configuration_Request__c> pcrMap = new Map<Id, csbb__Product_Configuration_Request__c>([SELECT Id, Name FROM csbb__Product_Configuration_Request__c WHERE csbb__Product_Basket__c IN :baskedIDs AND csbb__Product_Configuration__c IN :pclMap.keySet()]);

        System.assertEquals(pclMap.values().size(), pcrMap.values().size(), 'Invalid number of Product Request Configurations');

	}
}