public with sharing class NC_AddInternetProductController {
    @AuraEnabled
    public static String addOfferToBasket(Id recordId, String product) {
        List<Lead> leadRecords = [SELECT Id,
                                    Name,
                                    Account_name__c
                                FROM Lead WHERE Id = :recordId];
System.debug('***leadRecords: ' + JSON.serializePretty(leadRecords));

        Opportunity oppRecord = new Opportunity();
        oppRecord.Name = 'NC Test Opp';
        oppRecord.AccountId = leadRecords[0].Account_name__c;
        oppRecord.StageName = 'Qualification';
        oppRecord.CloseDate = System.today();
        insert oppRecord;
        System.debug('***oppRecord: ' + JSON.serializePretty(oppRecord));

        cscfga__Product_Basket__c basketRecord = new cscfga__Product_Basket__c();
        basketRecord.Name = 'NC Test Basket';
        basketRecord.cscfga__Opportunity__c = oppRecord.Id;
        basketRecord.Lead__c = leadRecords[0].Id;
        insert basketRecord;
        System.debug('***basketRecord: ' + JSON.serializePretty(basketRecord));

        List<cscfga__Product_Configuration__c> configsFromOffer = [SELECT Id,Name, cscfga__Configuration_Offer__r.Name
        FROM cscfga__Product_Configuration__c WHERE Id = 'a7Z1l0000009XAbEAM' OR Id = 'a7Z1l0000009XAgEAM'];
        System.debug('***configsFromOffer: ' + JSON.serializePretty(configsFromOffer));

            Id result;
            if (product == 'Zakelijk Internet Complete') {
                    for (cscfga__Product_Configuration__c configOffer : configsFromOffer) {
                            if (configOffer.cscfga__Configuration_Offer__r.Name == 'NC Zakelijk Internet Complete'){
                                    result = cscfga.API_1.cloneConfiguration(configOffer,null,basketRecord, false);
                            }
                    }
            } else if (product == 'Zakelijk Internet Complete + sports channel'){
                    for (cscfga__Product_Configuration__c configOffer : configsFromOffer) {
                            if (configOffer.cscfga__Configuration_Offer__r.Name == 'NC Zakelijk Internet Complete + sports channel'){
                                    result = cscfga.API_1.cloneConfiguration(configOffer,null,basketRecord, false);
                            }
                    }            }
        System.debug('***result: ' + result);

        csbb__Product_Configuration_Request__c newPcr = new csbb__Product_Configuration_Request__c();
        newPcr.csbb__Product_Configuration__c = result;
        newPcr.csbb__Product_Basket__c = basketRecord.Id;
        newPcr.csbb__Status__c = 'finalized';
        insert newPcr;

        String returnValue;
            if (product == 'Zakelijk Internet Complete + sports channel') {
                returnValue = 'apex/cscfga__ConfigureProduct?linkedId='+basketRecord.Id+'&configId='+result+'&retURL=/apex/csbb__CSBasketRedirect?id='+basketRecord.Id+'&isdtp=vw';
            } else {
                returnValue = basketRecord.Id;
            }
        return returnValue;
    }
}