@RestResource(urlMapping='/createopportunity/*')
global with sharing class BdsRestCreateOpportunity {

    @HttpPost
    global static void doPost(String kvk, String dealercode, String comments, String projectcontact, String dealtype, String services, String locationCount, String solutionSalesEmail) {

        RestResponse res = RestContext.response;
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
            res.addHeader('Content-Type', 'text/json');
            res.statusCode = 200;
        }

        /** Validate the incoming body values */
        List<BdsResponseClasses.ValidationError> errFields = BdsRestCreateOpportunity.validateValues(kvk, dealercode, projectcontact, dealtype, services, locationCount );

        if (errFields.size() > 0) {
            /** return Error */
            BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
            returnClass.errors = errFields;
            res.statusCode = 400;
            res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
        } else {
            /** go forward processing the fields */
            BdsResponseClasses.OpportunityStatusClass returnClass = BdsRestCreateOpportunity.createOpportunity(kvk, dealercode, comments, projectcontact, dealtype, services, locationCount, solutionSalesEmail);
            if (!returnClass.isSuccess){
                res.statusCode = 400;
            }
            res.responseBody = Blob.valueOf(JSON.serializePretty(returnClass));
        }
    }

    /**
     * Create the opportunity
     *
     * @param kvk
     * @param dealercode
     * @param comments
     * @param projectcontact
     * @param dealtype
     *
     * @return BdsResponseClasses.OpportunityStatusClass
     */
    public static BdsResponseClasses.OpportunityStatusClass createOpportunity(String kvk, String dealercode, String comments, String projectcontact, String dealtype, String services, String locationCount, String solutionSalesEmail) {

        BdsResponseClasses.OpportunityStatusClass returnValue =  new BdsResponseClasses.OpportunityStatusClass();
        try {
            /** Def continueBool */
            Boolean continueBool = true;
            /** Def Account ID */
            Id accountId;
            /** Def Opportunity Name */
            String oppName = '';
            /** Def Project Comments */
            String projComment = 'BDS Order';
            /** Def Project Type */
            String projType = 'Other';
            /** Def StageName */
            String stageName = 'Closing';
            /** Def DealerUserCode Id */
            Id dealerCodeUserId;

            /** Def DealerCode Id */
            Id dealerCodeId;
            /** Def Contact Id */
            Id contactId;

            /** Comments Filled? */
            if (!String.isEmpty(comments)) {
                projComment = comments;
            }

            /** Search Account Information */
            List<Account> accList = [Select Id, Name From Account Where KVK_number__c =: kvk];
            if (accList.size() == 1) {
                oppName = accList[0].Name + ' ACQ/RET';
                accountId = accList[0].Id;
            } else {
                /** Raise error */
                continueBool = false;
                returnValue.errorMessage = 'No Account found with kvk-number: '+ kvk;
            }

            /** Search Dealer Information */
            List<Dealer_Information__c> dealerList = [Select Id, Contact__r.Userid__c From Dealer_Information__c Where Dealer_Code__c =: dealercode];
            if (dealerList.size() == 1) {
                if (!String.isEmpty(dealerList[0].Contact__r.Userid__c)) {
                    dealerCodeUserId = dealerList[0].Contact__r.Userid__c;
                    dealerCodeId = dealerList[0].Id;
                } else {
                    /** Raise error */
                    continueBool = false;
                    returnValue.errorMessage = 'Dealer has no attached user';
                }
            } else {
                /** Raise error */
                continueBool = false;
                returnValue.errorMessage = 'Unable to find dealer with dealercode: '+dealercode;
            }

            /** Search Project Contact */
            List<Contact> conList = [Select Id From Contact Where Email =: projectcontact AND AccountId =: accountId ];
            if (conList.size() == 1) {
                contactId = conList[0].Id;
            } else {
                /** Raise error */
                continueBool = false;
                returnValue.errorMessage = 'Unable to find contact with email-address: '+projectcontact;
            }

            /** Search Solution Sales */
            Id solutionUser;
            if (!String.isEmpty(solutionSalesEmail)) {
                List<User> ssList = [Select Id From User Where Email = :solutionSalesEmail AND IsActive = true];
                if (ssList.size() == 1) {
                    solutionUser = ssList[0].Id;
                } else {
                    /** Raise error */
                    continueBool = false;
                    returnValue.errorMessage = 'Unable to find user with email-address: ' + solutionSalesEmail;
                }
            }


            if (continueBool) {
                /** Create Opportunity */
                Account acc = [Select Id, OwnerId, Mobile_Dealer__c From Account Where Id =: accountId];
                acc.Mobile_Dealer__c = dealerCodeId;
                SharingUtils.updateObjectsWithoutSharingStatic(new List<Account>{acc});

                Map<Id, Id> accUserMap = new Map<Id, Id>();
                accUserMap.put(acc.Id, dealerCodeUserId);
                SharingUtils.createAccountSharing(accUserMap, new Set<Id> {dealerCodeUserId}, null);

                /** share acc and opp with current api user */
                try {
                    AccountShare accShare = new AccountShare();
                    accShare.AccountId = acc.Id;
                    accShare.UserOrGroupId = UserInfo.getUserId();
                    accShare.CaseAccessLevel = 'None';
                    accShare.OpportunityAccessLevel = 'Edit';
                    accShare.AccountAccessLevel = 'Read';
                    SharingUtils.insertObjectsWithoutSharingStatic(new List<AccountShare>{accShare});
                } catch (Exception e) {
                    system.debug('##sharing: '+e.getMessage());
                }

                Opportunity opp             = new Opportunity();
                if (!String.isEmpty(solutionUser)) {
                    opp.Solution_Sales__c = solutionUser;
                }
                opp.AccountId               = accountId;
                opp.Closedate               = Date.Today();
                opp.StageName               = stageName;
                opp.Special_project_type__c = projType;
                opp.Name                    = oppName;
                opp.Project_Comments__c     = 'BDS Opportunity';
                opp.Description             = projComment;
                opp.Deal_Type__c            = dealtype;
                opp.OwnerId                 = dealerCodeUserId;
                opp.Main_Contact_Person__c  = contactId;
                opp.RecordTypeId            = GeneralUtils.recordTypeMap.get('Opportunity').get('MAC');
                opp.Services__c             = services;
                opp.Location_Count__c       = locationCount;

                insert opp;

                opp = [Select Id, StageName, Opportunity_Number__c From Opportunity Where Id =: opp.Id];
                returnValue.isSuccess       = true;
                returnValue.opportunityId   = opp.Id;
                returnValue.opportunityStageName = opp.StageName;
                returnValue.opportunityNumber = opp.Opportunity_Number__c;
            }

        } catch(Exception e) {
            returnValue.errorMessage = e.getMessage();
        }
        return returnValue;
    }



    /**
     * Validate the body values
     *
     * @param kvk
     * @param dealercode
     * @param projectcontact
     * @param dealtype
     *
     * @return
     */
    public static List<BdsResponseClasses.ValidationError> validateValues(String kvk, String dealercode, String projectcontact, String dealtype, String services, String locationCount) {
        List<BdsResponseClasses.ValidationError> errFields = new List<BdsResponseClasses.ValidationError>();

        /** kvk */
        if (String.isEmpty(kvk)) {
            BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('kvk', 'kvk missing');
            errFields.add(errField);
        }
        /** dealercode */
        if (String.isEmpty(dealercode)) {
            BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('dealercode', 'dealercode missing');
            errFields.add(errField);
        }
        /** projectcontact */
        if (String.isEmpty(projectcontact)) {
            BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('projectcontact', 'projectcontact missing');
            errFields.add(errField);
        }
        /** dealtype */
        if (String.isEmpty(dealtype)) {
            BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('dealtype', 'dealtype missing');
            errFields.add(errField);
        }
        /** services */
        if (String.isEmpty(services)) {
            BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('services', 'services missing');
            errFields.add(errField);
        }
        /** locationType */
        if (String.isEmpty(locationCount)) {
            BdsResponseClasses.ValidationError errField = new BdsResponseClasses.ValidationError('locationCount', 'locationCount missing');
            errFields.add(errField);
        }

        return errFields;
    }


}