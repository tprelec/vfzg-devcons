@SuppressWarnings('PMD.ApexAssertionsShouldIncludeMessage')
@IsTest
private class TestOppDocumentGenerationController {
	private static final String DOCUMENT_TEMPLATE_OTHER_TEST = 'Some other value';
	@testSetup
	static void prepareCommonTestData() {
		MavenDocumentsTestFactory.createSettings();
		// package stuff
		mmdoc.PostInstall pi = new mmdoc.PostInstall();
		pi.onInstall(null);

		// to break the execution of async processes, document solution and template are inserted in testSetup
		List<mmdoc__Document_Solution__c> documentSolutions = new List<mmdoc__Document_Solution__c>{
			new mmdoc__Document_Solution__c(Name = OppDocumentGenerationController.DOCUMENT_TEMPLATE_BUSINESS_INTERNET),
			new mmdoc__Document_Solution__c(Name = DOCUMENT_TEMPLATE_OTHER_TEST)
		};
		insert documentSolutions;

		List<mmdoc__Document_Template__c> documentTemplates = new List<mmdoc__Document_Template__c>();

		for (mmdoc__Document_Solution__c documentSolution : documentSolutions) {
			documentTemplates.add(
				new mmdoc__Document_Template__c(
					Name = documentSolution.Name,
					mmdoc__Document_Solution__c = documentSolution.Id,
					mmdoc__Template_Type__c = 'Google Doc',
					mmdoc__Template_Document_Id__c = documentSolution.Id
				)
			);
		}
		insert documentTemplates;
	}

	@IsTest
	static void testGetPropositionSetting() {
		Test.startTest();
		List<Orderform_Proposition__mdt> settings = OppDocumentGenerationController.getPropositionSetting();
		Test.stopTest();
		System.assertNotEquals(null, settings);
	}

	@IsTest
	static void testSetError() {
		String errorMessage = 'Some message';
		Test.startTest();
		Savepoint savepoint = Database.setSavePoint();
		LightningResponse response = OppDocumentGenerationController.setError(savepoint, errorMessage);
		Test.stopTest();
		System.assertEquals(errorMessage, response.message);
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
	}

	@IsTest
	static void testGetBasketWithNoData() {
		Test.startTest();
		LightningResponse response = OppDocumentGenerationController.getBasket(null);
		Test.stopTest();
		System.assertEquals(null, response.message);
		System.assertEquals(null, response.variant);
		OppDocumentGenerationController.BasketResponse basketResponse = (OppDocumentGenerationController.BasketResponse) response.body;
		System.assertEquals(null, basketResponse.id);
		System.assertEquals(null, basketResponse.name);
		System.assertNotEquals(true, basketResponse.isStatusApproved);
	}

	@IsTest
	static void testGetBasketWithData() {
		Opportunity opportunity = createOpportunityWithBasket();

		Test.startTest();
		LightningResponse response = OppDocumentGenerationController.getBasket(opportunity.Id);
		Test.stopTest();
		System.assertEquals(null, response.message);
		System.assertEquals(null, response.variant);
		OppDocumentGenerationController.BasketResponse basketResponse = (OppDocumentGenerationController.BasketResponse) response.body;
		System.assertNotEquals(null, basketResponse.id);
		System.assertNotEquals(null, basketResponse.name);
		System.assertNotEquals(true, basketResponse.isStatusApproved);
	}

	@IsTest
	static void testCreateDocumentWithNoBasket() {
		Test.startTest();
		LightningResponse response = OppDocumentGenerationController.createDocument(null);
		Test.stopTest();
		System.assertEquals(System.Label.OppDocumentGeneration_BasketNotPrimaryMessage, response.message);
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
	}

	@IsTest
	static void testCreateDocumentWithNoValidDocumentTemplate() {
		// clear out the document template name
		List<mmdoc__Document_Template__c> documentTemplate = [
			SELECT Id, mmdoc__Document_Solution__c
			FROM mmdoc__Document_Template__c
			WHERE Name = :OppDocumentGenerationController.DOCUMENT_TEMPLATE_BUSINESS_INTERNET
		];
		System.assertNotEquals(0, documentTemplate.size());
		update new mmdoc__Document_Template__c(Id = documentTemplate[0].Id, Name = 'Other Name');

		Opportunity opportunity = createOpportunityWithBasket();
		Test.startTest();
		LightningResponse response = OppDocumentGenerationController.createDocument(opportunity.Id);
		Test.stopTest();
		System.assertEquals(System.Label.OppDocumentGeneration_BusinessInternetPropositionMissingOnProduct, response.message);
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
	}

	@IsTest
	static void testCreateDocumentWithNoPropositionOnOli() {
		Opportunity opportunity = createOpportunityWithBasket();

		Test.startTest();

		// setup the proposition custom setting
		OppDocumentGenerationController.propositionCMDTs = new List<Orderform_Proposition__mdt>{
			new Orderform_Proposition__mdt(MasterLabel = 'dummy')
		};
		LightningResponse response = OppDocumentGenerationController.createDocument(opportunity.Id);
		Test.stopTest();
		System.assertEquals(System.Label.OppDocumentGeneration_BusinessInternetPropositionMissingOnProduct, response.message);
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
	}

	@IsTest
	static void testSingleDocumentSuccess() {
		Opportunity opportunity = createOpportunityWithBasket();

		Test.startTest();

		// setup the proposition custom setting
		OppDocumentGenerationController.propositionCMDTs = new List<Orderform_Proposition__mdt>{
			new Orderform_Proposition__mdt(
				MasterLabel = OppDocumentGenerationController.DOCUMENT_TEMPLATE_BUSINESS_INTERNET,
				Contract_Template__c = OppDocumentGenerationController.DOCUMENT_TEMPLATE_BUSINESS_INTERNET
			)
		};

		LightningResponse response = OppDocumentGenerationController.createDocument(opportunity.Id);

		Test.stopTest();
		System.assertEquals(System.Label.OppDocumentGeneration_AgreementCreatedMessage, response.message);
		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant);
	}

	@IsTest
	static void testSendForApprovalForDocumentSignatureDocusign() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		Map<String, User> admins = TestUtils.createAdmins(1);
		admins.get('Admin1').ManagerId = UserInfo.getUserId();
		update admins.get('Admin1');

		// create test data
		Opportunity opportunity;

		System.runAs(admins.get('Admin1')) {
			opportunity = createOpportunityWithBasket();
			opportunity.OwnerId = admins.get('Admin1').Id;
			update opportunity;

			GenericMock mock = new GenericMock();
			mock.returns.put('attachDocumentsToSObjectForRegularAttachments', null);
			mock.returns.put('attachDocumentsToSObjectForContractConditions', null);
			DocumentService.instance = (DocumentService) Test.createStub(DocumentService.class, mock);
		}

		Test.startTest();

		LightningResponse response;
		System.runAs(admins.get('Admin1')) {
			// more test data
			opportunity = [SELECT Id, AccountId, Amount, Primary_Basket__c, OwnerId FROM Opportunity LIMIT 1];

			update new cscfga__Product_Basket__c(Id = opportunity.Primary_Basket__c, Sign_on_behalf_of_customer__c = 'both');

			Account acc = new Account(Id = opportunity.AccountId);
			TestUtils.autoCommit = false;
			List<Contact> signers = new List<Contact>{ TestUtils.createContact(acc), TestUtils.createContact(acc) };

			signers[0].Email = 'signer1@test.com';
			signers[1].Email = 'signer2@test.com';
			insert signers;
			TestUtils.autoCommit = true;
			acc.Authorized_to_sign_1st__c = signers[0].Id;
			acc.Authorized_to_sign_2nd__c = signers[1].Id;
			update acc;

			csclm__Agreement__c agreement = CS_DataTest.createAgreement('Test Agreement');
			agreement.csclm__Opportunity__c = opportunity.Id;
			agreement.recordTypeId = Schema.SObjectType.csclm__Agreement__c.getRecordTypeInfosByDeveloperName().get('Bespoke').getRecordTypeId();
			insert agreement;
			Attachment attachment = new Attachment(
				Name = 'I\'m testing attachments',
				Body = Blob.valueOf('Some body once told me the world is gonna roll me'),
				ParentId = agreement.Id
			);
			insert attachment;

			// actual testing
			response = OppDocumentGenerationController.sendForApproval(
				opportunity.Id,
				OppDocumentGenerationController.OPPORTUNITY_SIGNATURE_DOCUSIGN
			);
		}

		opportunity = [SELECT Id, Signature_Status__c FROM Opportunity WHERE Id = :opportunity.Id];

		System.assertEquals(OppDocumentGenerationController.OPPORTUNITY_SIGNATURE_STATUS_PENDING_FOR_APPROVAL, Opportunity.Signature_Status__c);

		Test.stopTest();

		System.assertEquals(System.Label.OppDocumentGeneration_SentToDocuSignMessage, response.message);
		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant);
	}

	@IsTest
	static void testSendForApprovalForDocumentSignatureClickapprove() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);

		Opportunity opportunity = createOpportunityWithBasket();

		Contact contact = new Contact(
			AccountId = [SELECT AccountId FROM Opportunity WHERE Id = :opportunity.Id LIMIT 1]
			.AccountId,
			LastName = 'TestLastname',
			FirstName = 'TestFirstname',
			Email = 'support@vodafone.nl.salesforce'
		);
		insert contact;

		opportunity.DS_Authorized_to_sign_1st__c = contact.Id;
		update opportunity;

		insert new CSCAP__Click_Approve_Setting__c(Name = OppDocumentGenerationController.CLICK_APPROVE_SETTING);

		Test.startTest();
		LightningResponse response = OppDocumentGenerationController.sendForApproval(
			opportunity.Id,
			OppDocumentGenerationController.OPPORTUNITY_SIGNATURE_CLICKAPPROVE
		);
		Test.stopTest();
		System.assertEquals(System.Label.OppDocumentGeneration_SentToClickApproveMessage, response.message);
		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant);
	}

	@IsTest
	static void testSendForApprovalForDocumentSignatureOther() {
		Opportunity opportunity = createOpportunityWithBasket();
		Test.startTest();
		LightningResponse response = OppDocumentGenerationController.sendForApproval(opportunity.Id, null);
		Test.stopTest();
		System.assertEquals(System.Label.OppDocumentGeneration_SignatureOtherMethodMessage, response.message);
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);
	}

	@IsTest
	static void testSendForApprovalVerifyDefaultAuthorizeToSign() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);

		Opportunity opportunity = createOpportunityWithBasket();

		Id accountId = [SELECT AccountId FROM Opportunity WHERE Id = :opportunity.Id LIMIT 1].AccountId;

		Contact contact = new Contact(
			AccountId = accountId,
			LastName = 'TestLastname',
			FirstName = 'TestFirstname',
			Email = 'support@vodafone.nl.salesforce'
		);
		insert contact;

		// set authorize to sign on account that is linked to opportunity's contact
		update new Account(Id = accountId, Authorized_to_sign_1st__c = contact.Id);

		insert new CSCAP__Click_Approve_Setting__c(Name = OppDocumentGenerationController.CLICK_APPROVE_SETTING);

		Test.startTest();
		LightningResponse response = OppDocumentGenerationController.sendForApproval(
			opportunity.Id,
			OppDocumentGenerationController.OPPORTUNITY_SIGNATURE_CLICKAPPROVE
		);
		Test.stopTest();
		System.assertEquals(System.Label.OppDocumentGeneration_SentToClickApproveMessage, response.message);
		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant);

		System.assertEquals(
			[SELECT Authorized_to_sign_1st__c FROM Account WHERE Id = :accountId]
			.Authorized_to_sign_1st__c,
			contact.Id,
			'Authorize to sign must be same as on Account'
		);
	}

	@IsTest
	static void testSendForApprovalVerifyNoDefaultAuthorizeToSign() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);

		Opportunity opportunity = createOpportunityWithBasket();

		Id accountId = [SELECT AccountId FROM Opportunity WHERE Id = :opportunity.Id LIMIT 1].AccountId;

		Contact contact = new Contact(LastName = 'TestLastname', FirstName = 'TestFirstname', Email = 'support@vodafone.nl.salesforce');
		insert contact;

		// set authorize to sign on account that is not linked to opportunity's contact
		update new Account(Id = accountId, Authorized_to_sign_1st__c = contact.Id);

		insert new CSCAP__Click_Approve_Setting__c(Name = OppDocumentGenerationController.CLICK_APPROVE_SETTING);

		Test.startTest();
		LightningResponse response = OppDocumentGenerationController.sendForApproval(
			opportunity.Id,
			OppDocumentGenerationController.OPPORTUNITY_SIGNATURE_CLICKAPPROVE
		);
		Test.stopTest();
		System.assertEquals(System.label.OppDocumentGeneration_Authorized_to_Sign_Empty_Message, response.message);
		System.assertEquals(LightningResponse.getErrorVariant(), response.variant);

		System.assertEquals(
			null,
			[SELECT DS_Authorized_to_sign_1st__c FROM Opportunity WHERE Id = :opportunity.Id]
			.DS_Authorized_to_sign_1st__c,
			'Authorize to sign must be same as on Account'
		);
	}

	static Opportunity createOpportunityWithBasket() {
		TestUtils.createCompleteOpportunity();

		List<OpportunityLineItem> oli = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :TestUtils.theOpportunity.Id LIMIT 1];

		System.assertNotEquals(0, oli.size());

		update new OpportunityLineItem(
			Id = oli[0].Id,
			Proposition__c = OppDocumentGenerationController.DOCUMENT_TEMPLATE_BUSINESS_INTERNET,
			Contract_Number__c = 'CS-2021-VZ-BI-00015 2.1 d.d. 16-12-2021',
			ContractNumber_JSON__c = '{"year":2021,"minorVersion":1,"majorVersion":2,"incrementNumber":3,"format":"CS-#YYYY#-VZ-#GROUP#-#NUMBER# #MAJOR#.#MINOR# d.d. #DATE#","contractGroup":"OM","contractDate":"2021-12-09"}',
			Location__c = TestUtils.theSite.Id
		);

		cscfga__Product_Basket__c basket = new cscfga__Product_Basket__c(
			cscfga__Opportunity__c = TestUtils.theOpportunity.Id,
			csbb__Account__c = TestUtils.theAccount.Id,
			Primary__c = true,
			Sign_on_behalf_of_customer__c = 'both'
		);
		insert basket;

		Opportunity opportunity = new Opportunity(Id = TestUtils.theOpportunity.Id, Primary_Basket__c = basket.Id, Amount = 500001);
		update opportunity;

		return opportunity;
	}

	// mock for docusign
	private class GenericMock implements System.StubProvider {
		private Map<String, Object> returns = new Map<String, Object>();
		private Map<String, Integer> callCount = new Map<String, Integer>();
		private Map<String, List<Object>> args = new Map<String, List<Object>>();

		@SuppressWarnings('PMD.ExcessiveParameterList')
		public Object handleMethodCall(
			Object stubbedObject,
			String stubbedMethodName,
			Type returnType,
			List<Type> listOfParamTypes,
			List<String> listOfParamNames,
			List<Object> listOfArgs
		) {
			if (!callCount.containsKey(stubbedMethodName)) {
				callCount.put(stubbedMethodName, 0);
			}
			callCount.put(stubbedMethodName, callCount.get(stubbedMethodName) + 1);
			if (!args.containsKey(stubbedMethodName)) {
				args.put(stubbedMethodName, new List<Object>());
			}
			args.put(stubbedMethodName, listOfArgs);
			return returns.get(stubbedMethodName);
		}
	}
}
