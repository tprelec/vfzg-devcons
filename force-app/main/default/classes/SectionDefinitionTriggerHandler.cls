public with sharing class SectionDefinitionTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<csclm__Section_Definition__c> newProducts = (List<csclm__Section_Definition__c>) this.newList;
		Sequence seq = new Sequence('Section Definition');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (csclm__Section_Definition__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
