@IsTest
private class TestCS_MFChangeOwnerForSmallImplCase {
    @IsTest
    static void testBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Test.startTest();

            VF_Contract__c vfContract1 = createVfContractWithAdditionalData(simpleUser, true, 'Mobile', 'Retention', 0);

            List<Case> cases = new List<Case>();
            Case case1 = CS_DataTest.createCase('Small Implementation', 'CS MF Small Implementation', simpleUser, false);
            case1.Contract_VF__c = vfContract1.Id;
            cases.add(case1);
            Case case2 = createCaseWithKtoData(simpleUser, vfContract1, true, 'test@test.com');
            cases.add(case2);
            insert cases;

            case2.Status = 'Closed';
            update case2;

            assertKtoData(case1, case2);
            Test.stopTest();
        }
    }

    private static Case createCaseWithKtoData(User simpleUser, VF_Contract__c vfContract, Boolean sendKto, String ktoEmail){
        Case returnCase = CS_DataTest.createCase('Project Intake', 'CS MF Project Intake and Preparation', simpleUser, false);
        returnCase.Contract_VF__c = vfContract.Id;
        returnCase.Send_KTO__c = sendKto;
        returnCase.KTO_Contact_Email__c = ktoEmail;

        return returnCase;
    }

    private static VF_Contract__c createVfContractWithAdditionalData(User simpleUser, Boolean isSmallImplementation, String typeOfService, String dealType, Decimal reworkSequence){
        VF_Contract__c returnVfContract = CS_DataTest.createDefaultVfContract(simpleUser, false);
        returnVfContract.Is_Small_Implementation__c = isSmallImplementation;
        returnVfContract.Type_of_Service__c = typeOfService;
        returnVfContract.Deal_Type__c = dealType;
        returnVfContract.Responsible_Quality_Officer__c = simpleUser.Id;
        returnVfContract.Rework_Sequence_Nr__c = reworkSequence;

        insert returnVfContract;
        return returnVfContract;
    }

    private static void assertKtoData(Case caseBeforeMethodCall, Case caseAfterMethodCall) {
        List<Case> caseList = [
                SELECT Id,
                        Case_Record_Type_Text__c,
                        Contract_VF__c
                FROM Case
                WHERE Id = :caseBeforeMethodCall.Id
        ];
        CS_MobileFlowService.setCaseFields(caseList);
        List<Case> newCase = [
                SELECT Id,
                        Send_KTO__c,
                        KTO_Contact_Email__c
                FROM Case
                WHERE Id = :caseBeforeMethodCall.Id
        ];

        for (Case c : newCase) {
            System.assertEquals(caseAfterMethodCall.Send_KTO__c, c.Send_KTO__c);
            System.assertEquals(caseAfterMethodCall.KTO_Contact_Email__c, c.KTO_Contact_Email__c);
        }
    }
}