@isTest
private class TestCOM_ImplementServicesTblController {
	@isTest
	private static void testOverviewByDeliveryOrderId() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, true);

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c parentService = new csord__Service__c();
		parentService.csord__Identification__c = 'TestService';
		parentService.Name = 'TestService';
		parentService.csord__Subscription__c = subscription.Id;
		parentService.Installation_Required__c = true;
		parentService.COM_Delivery_Order__c = delOrd.Id;
		insert parentService;

		csord__Service__c childService = new csord__Service__c();
		childService.csord__Identification__c = 'TestServiceChild';
		childService.Name = 'TestServiceChild';
		childService.csord__Subscription__c = subscription.Id;
		childService.COM_Delivery_Order__c = delOrd.Id;
		childService.csord__Service__c = parentService.Id;
		childService.Installation_Required__c = true;
		insert childService;

		Test.startTest();
		List<COM_ImplementServicesTblController.ServiceWrapper> wrappers = COM_ImplementServicesTblController.getServices(delOrd.Id);
		Test.stopTest();

		System.assertEquals(2, wrappers.size(), 'Size is not the same.');
		System.assertEquals(childService.Name, wrappers[1].name, 'Name is not the same.');
		System.assertEquals(parentService.Name, wrappers[0].parentServiceName, 'Name is not the same.');
	}

	@isTest
	private static void testOverviewByTaskId() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, true);
		Task testTask = new Task();
		testTask.Subject = 'Installation';
		testTask.COM_Delivery_Order__c = delOrd.Id;
		insert testTask;

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c parentService = new csord__Service__c();
		parentService.csord__Identification__c = 'TestService';
		parentService.Name = 'TestService';
		parentService.csord__Subscription__c = subscription.Id;
		parentService.Installation_Required__c = true;
		parentService.COM_Delivery_Order__c = delOrd.Id;
		insert parentService;

		csord__Service__c childService = new csord__Service__c();
		childService.csord__Identification__c = 'TestServiceChild';
		childService.Name = 'TestServiceChild';
		childService.csord__Subscription__c = subscription.Id;
		childService.COM_Delivery_Order__c = delOrd.Id;
		childService.csord__Service__c = parentService.Id;
		childService.Installation_Required__c = true;
		insert childService;

		Test.startTest();
		List<COM_ImplementServicesTblController.ServiceWrapper> wrappers = COM_ImplementServicesTblController.getServices(testTask.Id);
		Test.stopTest();

		System.assertEquals(2, wrappers.size(), 'Size is not the same.');
		System.assertEquals(childService.Name, wrappers[1].name, 'Name is not the same.');
		System.assertEquals(parentService.Name, wrappers[0].parentServiceName, 'Name is not the same.');
	}

	@isTest
	private static void testUpdateServices() {
		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd', null, true);

		csord__Subscription__c subscription = new csord__Subscription__c();
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c parentService = new csord__Service__c();
		parentService.csord__Identification__c = 'TestService';
		parentService.Name = 'TestService';
		parentService.csord__Subscription__c = subscription.Id;
		parentService.COM_Delivery_Order__c = delOrd.Id;
		insert parentService;

		csord__Service__c childService = new csord__Service__c();
		childService.csord__Identification__c = 'TestServiceChild';
		childService.Name = 'TestServiceChild';
		childService.csord__Subscription__c = subscription.Id;
		childService.COM_Delivery_Order__c = delOrd.Id;
		childService.csord__Service__c = parentService.Id;
		insert childService;

		List<COM_ImplementServicesTblController.ServiceWrapper> wrappers = new List<COM_ImplementServicesTblController.ServiceWrapper>();
		COM_ImplementServicesTblController.ServiceWrapper childServiceWrapper = new COM_ImplementServicesTblController.ServiceWrapper(childService);

		childServiceWrapper.implementedDate = Date.today();

		wrappers.add(childServiceWrapper);

		Test.startTest();
		COM_ImplementServicesTblController.updateServices(wrappers);
		Test.stopTest();

		for (csord__Service__c service : [SELECT Id, Implemented_Date__c, Name FROM csord__Service__c]) {
			if (service.Name == parentService.Name) {
				System.assertEquals(null, service.Implemented_Date__c, 'Date is not empty.');
			} else if (service.Name == childService.Name) {
				System.assertEquals(childServiceWrapper.implementedDate, service.Implemented_Date__c, 'Date is not empty.');
			}
		}
	}
}
