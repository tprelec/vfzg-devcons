/**
 * @description			This is the test class that contains the tests for the BICC Vodacom Data Ban Batch
 * @author				Ferdinand Bondt
 */
@isTest
private class TestBiccVodacomDataBanBatch {

	static testMethod void myUnitTest() {

		//Data preparation
		TestUtils.autoCommit = false;

		map<String, String> mapping = new map<String, String>{'Billing_Account_Num__c' => 'AR_Billing_Account_Num__c',
															  'InYear_Base__c' => 'AR_InYear_Base__c',
															  'InYear_Charges__c' => 'AR_InYear_Charges__c',
															  'Year_Month_Num__c' => 'AR_Year_Month_Num__c'};
		list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
		for (String field : mapping.keyset()) {
			fsmlist.add(TestUtils.createSync('BICC_Vodacom_Data_BAN__c -> Account_Revenue__c', field, mapping.get(field)));
		}
		insert fsmlist;

		Account acc = new Account(Name = 'Test Account', Ban_Number__c = '312312311'); //Match on Ban
		insert acc;

		Ban__c ban = new Ban__c(Name = '312312311', Account__c = acc.Id);
		insert ban;

		TestUtils.autoCommit = true;

		TestUtils.createBiccVodacomBan('312312311', '150', '150', '010114'); //Succesfull insert, match on Ban
		TestUtils.createBiccVodacomBan('412312316', '200', '200', '010114'); //No valid Ban
		TestUtils.createBiccVodacomBan('312312311', '150', '150', null); //No year month num
		TestUtils.createBiccVodacomBan('312312316', '200', '200', '010115'); //No matching Account

		//Test
		Test.startTest();

		PageReference pageRef = Page.BiccVodacomDataBanBatchExecute;
		Test.setCurrentPage(pageRef);
		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new list<BICC_Vodacom_Data_BAN__c>());
		BiccVodacomDataBanBatchExecuteController controller = new BiccVodacomDataBanBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		//Checks
		System.assertEquals([select AR_InYear_Base__c from Account_Revenue__c where AR_Billing_Account_Num__c =: '312312311' limit 1].AR_InYear_Base__c, 150); //Succesful update
	}
}