global with sharing class CustomButtonAttachment extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		String newUrl = CustomButtonAttachment.redirectToAttachment(basketId);
		return '{"status":"ok","redirectURL":"' + newUrl + '"}';
	}

	// Set url for redirect after action
	public static String redirectToAttachment(String basketId) {
		PageReference editPage = new PageReference('/apex/ProductBasketAttachmentManager?basketId=' + basketId);

		Id profileId = UserInfo.getProfileId();
		String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;

		if (profileName == 'VF Partner Portal User') {
			editPage = new PageReference('/partnerportal/apex/ProductBasketAttachmentManager?basketId=' + basketId);
		}

		//https://vfcloudsense--devcs.cs88.my.salesforce.com/
		return editPage.getUrl();
	}
}
