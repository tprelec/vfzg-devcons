public with sharing class BiccVodacomDataBanBatchExecuteController {

	public BiccVodacomDataBanBatchExecuteController(ApexPages.StandardSetController ignored) {

	}

	public PageReference callBatch(){
		BiccVodacomDataBanBatch controller = new BiccVodacomDataBanBatch();
		database.executebatch(controller, 100);
		Schema.DescribeSObjectResult prefix = BICC_Vodacom_Data_BAN__c.SObjectType.getDescribe();
		return new pageReference('/' + prefix.getKeyPrefix());
	}
}