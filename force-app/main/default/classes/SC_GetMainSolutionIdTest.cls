@isTest
public with sharing class SC_GetMainSolutionIdTest {
	@isTest
	private static void testGetData() {
		cssdm__Solution_Definition__c solutionDefinition = CS_DataTest.createSolutionDefinition(
			'Infrastructure',
			CS_ConstantsCOM.SOLUTION_DEFINITION_TYPE_MAIN
		);
		insert solutionDefinition;

		Test.startTest();

		SC_GetMainSolutionId testObject = new SC_GetMainSolutionId();
		Map<String, Object> inputMap = new Map<String, Object>{ 'solutionName' => 'Infrastructure' };

		Map<String, Object> resultData = testObject.getData(inputMap);
		Test.stopTest();

		System.assertNotEquals(0, resultData.size());
	}
}
