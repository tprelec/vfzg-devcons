public with sharing class COM_GenerateServiceSpecificationsJob implements Queueable, Database.AllowsCallouts {
    List<Id> subscriptionIdList;
    public COM_GenerateServiceSpecificationsJob(List<Id> subscriptionIdList) {
        this.subscriptionIdList = subscriptionIdList;
    }

    public void execute(QueueableContext context) {
        System.debug('****EXECUTING ENQUEUE job');
        if(Test.isRunningTest()) {
                System.debug('****Generating JSONS end test');
                Attachment att = new Attachment();
                att.ParentId = this.subscriptionIdList[0];
                att.Name = 'ServiceSpecification.json';
                att.Body = Blob.valueOf('test');
                insert att;
        }
        else {
            System.debug('****+Generating JSONS from JOB START');
            csedm.API_1.generateSpecifications(this.subscriptionIdList, new List<Id>(), true);
            System.debug('****+Generating JSONS from JOB END');
        }
    }
        
}