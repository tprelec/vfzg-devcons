@RestResource(urlMapping='/dxlnotificationListener')
global with sharing class COM_DXLNotificationListener {
	@HttpPost
	global static void processNotification() {
		Map<String, Boolean> deserializationResults = new Map<String, Boolean>();
		RestRequest req = RestContext.request;
		String requestBody = getRequestBody(req);
		COM_DXLServiceAsyncResponse createCustomerAsyncResponse;
		COM_DXLCreateBSSOrderAsyncResponse createBSSOrderAsyncResponse;

		try {
			createCustomerAsyncResponse = (COM_DXLServiceAsyncResponse) JSON.deserializeStrict(requestBody, COM_DXLServiceAsyncResponse.class);
			deserializationResults.put('createCustomerFailed', false);
		} catch (Exception e) {
			System.debug(LoggingLevel.ERROR, e.getMessage());
			deserializationResults.put('createCustomerFailed', true);
		}

		try {
			createBSSOrderAsyncResponse = (COM_DXLCreateBSSOrderAsyncResponse) JSON.deserializeStrict(
				requestBody,
				COM_DXLCreateBSSOrderAsyncResponse.class
			);
			deserializationResults.put('createBSSOrderFailed', false);
		} catch (Exception e) {
			System.debug(LoggingLevel.ERROR, e.getMessage());
			deserializationResults.put('createBSSOrderFailed', true);
		}

		handleRequestBody(deserializationResults, createCustomerAsyncResponse, createBSSOrderAsyncResponse);
	}

	@TestVisible
	private static void handleRequestBody(
		Map<String, Boolean> deserializationResults,
		COM_DXLServiceAsyncResponse createCustomerAsyncResponse,
		COM_DXLCreateBSSOrderAsyncResponse createBSSOrderAsyncResponse
	) {
		RestResponse res = RestContext.response;

		if (deserializationResults.get('createCustomerFailed') && deserializationResults.get('createBSSOrderFailed')) {
			res.statusCode = 400;
			res.responseBody = Blob.valueOf('{"code":"400","reason":"Invalid input","message":"Payload invalid"}');
			System.debug(LoggingLevel.DEBUG, '****Both failed');
		} else if (!deserializationResults.get('createCustomerFailed') && deserializationResults.get('createBSSOrderFailed')) {
			handleCreateCustomer(createCustomerAsyncResponse, res);
		} else if (deserializationResults.get('createCustomerFailed') && !deserializationResults.get('createBSSOrderFailed')) {
			handleCreateBSSOrder(createBSSOrderAsyncResponse, res);
		} else {
			res.statusCode = 400;
			res.responseBody = Blob.valueOf('{"code":"400","reason":"Invalid input","message":"Invalid palyoad"}');
		}
	}

	@TestVisible
	private static String getRequestBody(RestRequest req) {
		String mockType = req.headers.get('mockType') != null ? req.headers.get('mockType') : '';

		if (!String.isBlank(mockType) && mockType.equalsIgnoreCase('createCustomer')) {
			return mockCreateCustomer(req);
		} else if (!String.isBlank(mockType) && mockType.equalsIgnoreCase('createBSSOrder')) {
			return mockCreateBssOrder(req);
		} else {
			return req.requestBody.toString();
		}
	}

	@TestVisible
	private static void handleCreateCustomer(COM_DXLServiceAsyncResponse createCustomerAsyncResponse, RestResponse res) {
		COM_DXLCreateCustomerListener createCustomer = new COM_DXLCreateCustomerListener(createCustomerAsyncResponse);
		if (createCustomer.payloadValid) {
			try {
				createCustomer.processPayload();
				res.statusCode = 200;
				res.headers.put('transactionId', RestContext.request.headers.get('transactionId'));
			} catch (Exception e) {
				res.statusCode = 500;
				res.responseBody = Blob.valueOf('{"code":"500","reason":"General Exception","message":"' + e.getMessage() + '"}');
				LoggerService.log(LoggingLevel.ERROR, e.getMessage());
			}
		} else {
			res.statusCode = 400;
			res.responseBody = Blob.valueOf('{"code":"400","reason":"Invalid input","message":"Payload invalid"}');
		}
	}

	@TestVisible
	private static void handleCreateBSSOrder(COM_DXLCreateBSSOrderAsyncResponse createBSSOrderAsyncResponse, RestResponse res) {
		COM_DXLCreateBSSOrderListener createBSSOrder = new COM_DXLCreateBSSOrderListener(
			createBSSOrderAsyncResponse,
			RestContext.request.headers.get('transactionId')
		);

		if (createBSSOrder.payloadValid) {
			try {
				createBSSOrder.processPayload();
				res.statusCode = 200;
				res.headers.put('transactionId', RestContext.request.headers.get('transactionId'));
			} catch (Exception e) {
				res.statusCode = 500;
				res.responseBody = Blob.valueOf('{"statusCode":"500","statusMessage":"' + e.getMessage() + '"}');
				LoggerService.log(LoggingLevel.ERROR, e.getMessage());
			}
		} else {
			res.statusCode = 400;
			res.responseBody = Blob.valueOf('{"statusCode":"400","statusMessage":"Bad request"}');
		}
		System.debug(LoggingLevel.DEBUG, '****createCustomerFailed failed');
	}

	@TestVisible
	private static String mockCreateCustomer(RestRequest req) {
		COM_Integration_setting__mdt dxlIntegrationSettings = COM_Integration_setting__mdt.getInstance(
			COM_DXL_Constants.COM_DXL_INTEGRATION_SETTING_NAME
		);
		Boolean doNotExecuteReq = dxlIntegrationSettings != null ? dxlIntegrationSettings.Execute_mock__c : true;
		Boolean doNotExecuteStatus = dxlIntegrationSettings != null ? dxlIntegrationSettings.Mock_Execution_result__c : true;

		String reqBody;
		if (doNotExecuteReq && !Test.isRunningTest()) {
			if (doNotExecuteStatus) {
				reqBody = COM_DXLNotificationListenerService.generateAsyncReponse(COM_DXL_Constants.COM_DXL_ASYNC_RESPONSE_NAME);
			} else {
				reqBody = '{"Error":"Error Text"}';
			}
		} else {
			reqBody = req.requestbody.tostring();
		}
		return reqBody;
	}

	@TestVisible
	private static String mockCreateBSSOrder(RestRequest req) {
		COM_Integration_setting__mdt dxlIntegrationSettings = COM_Integration_setting__mdt.getInstance(
			COM_DXL_Constants.COM_DXL_BSS_INTEGRATION_SETTING_NAME
		);
		Boolean doNotExecuteReq = dxlIntegrationSettings != null ? dxlIntegrationSettings.Execute_mock__c : true;
		Boolean doNotExecuteStatus = dxlIntegrationSettings != null ? dxlIntegrationSettings.Mock_Execution_result__c : true;

		String reqBody;
		if (doNotExecuteReq && !Test.isRunningTest()) {
			if (doNotExecuteStatus) {
				reqBody = COM_DXLNotificationListenerService.generateAsyncReponse(COM_DXL_Constants.COM_DXL_BSS_ASYNC_RESPONSE_NAME);
			} else {
				reqBody = '{"Error":"Error Text"}';
			}
		} else {
			reqBody = req.requestbody.tostring();
		}
		return reqBody;
	}
}
