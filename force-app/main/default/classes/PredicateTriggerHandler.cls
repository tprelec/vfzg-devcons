public with sharing class PredicateTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<csclm__Predicate__c> newProducts = (List<csclm__Predicate__c>) this.newList;
		Sequence seq = new Sequence('Predicate');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (csclm__Predicate__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
