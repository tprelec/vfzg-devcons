/**
 * @description         This is the controller class for the Numberporting details on the Order Form. 
 * @author              Guy Clairbois
 */
public without sharing class OrderFormNumberPortingDataController {

    /**
     *      Variables
     */

    public String contractId{get; private set;}
    public String orderId{get; private set;}
    public List<NumberPortingRowWrapper> portingRows {get;set;}
    private Boolean errorFound{get;set;}
    public OrderWrapper theOrderWrapper {get;set;}
    public Boolean locked {get;set;}

    public VF_Contract__c contract {
        get{
            if(contract == null){
                contract = OrderUtils.getContractRecordById(contractId);
            } 
            return contract;
        }
        set;
    }   


    /**
     *      Controllers
     */

    public OrderFormNumberPortingDataController getApexController() {
        // used for passing the controller between form components
        return this;
    }
    
    public OrderFormNumberPortingDataController() {
        contractId = ApexPages.currentPage().getParameters().get('contractId');
        orderId = ApexPages.currentPage().getParameters().get('orderId');
        errorFound = false;
            
        if(contractId == null){
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.ERROR, 'No ContractId provided.'));
            errorFound = true;
        }
        
        // only continue if no errors were found earlier on
        if(!errorFound){
            theOrderWrapper = OrderUtils.retrieveOrderDetails(orderId,null);
            // fetch any existing portingRows
            portingRows = new List<NumberPortingRowWrapper>();
            
            retrieveNumberportings(); 
        }
        locked = OrderUtils.getLocked(theOrderWrapper.theOrder);

    }
    
    /**
     * @description         Retrieve any already existing numberportings
     */    
    private void retrieveNumberportings(){
            
        Map<Id,List<NumberPortingRowWrapper>> orderIdToPortingRows = OrderUtils.retrieveOrderIdToPortingRows(new Set<Id>{orderId});
        if(orderIdToPortingRows.containsKey(orderId)){
            portingRows = orderIdToPortingRows.get(orderId); 
        } else {
            portingRows = new List<NumberportingRowWrapper>();
        }

    }    
    

    /**
     *      Page References
     */

    public pageReference saveNumberPortings(){
        // if record is locked, prevent updating
        if(theOrderWrapper.theOrder.Record_Locked__c){
            Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR,Label.ERROR_Order_Locked));
            return null;
        }       
        //Loop through the numberporting row list and create a new storable list
        List<Numberporting_row__c> numberportingRows = new List<Numberporting_row__c>();
        //List<Contracted_Products__c> cpToUpdate = new List<Contracted_Products__c>();

        for (NumberportingRowWrapper npr : portingRows) {
            numberportingRows.add(npr.portingRow);
            // contractedproduct pbx needs to be updated as well - GC 29-03-2017 this should not happen, as biling  should not change
            /*if(npr.portingRow.Contracted_Product__c != null){
                cpToUpdate.add(new Contracted_Products__c(
                    Id=npr.portingRow.Contracted_Product__c,
                    PBX__c = npr.portingrow.PBX__c,
                    Site__c = npr.portingrow.Location__c));
            }*/
        }

        // savepoint only needed here. No prob if numberporting record is created.. 
        Savepoint sp = Database.setSavepoint();
        //Store the numberporting rows to the database
        try{
            upsert numberportingRows;
            //update cpToUpdate;

            // reload/recheck to correctly load the red/green flag
            updateNumberportingStatus();
            
        } catch(DmlException ex){
            Database.rollback(sp);
            ApexPages.addMessages(ex);
        }
        
        return null;
    }       

    public pageReference removeAllNumberPortings(){
        Set<Id> cpIdToUpdate = new Set<Id>();

        // Mark the row for deletion if it is selected
        for(NumberportingRowWrapper nprw : portingRows){
            if(nprw.selected == true) {            
                nprw.toDelete = true;
                cpIdToUpdate.add(nprw.portingRow.Contracted_Product__c);
            }
        }

        if (cpIdToUpdate.size()==0) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.Warning, 'No Numberblocks were removed as none were selected.'));    
            return null;       
        }

        List<Contracted_Products__c> cpToUpdate = new List<Contracted_Products__c>();
        for(Id cpId : cpIdToUpdate){
            Contracted_Products__c cp = new Contracted_Products__c(Id=cpId);
            cp.Order__c = null;
            cpToUpdate.add(cp);
        }
        update cpToUpdate;

        deleteNumberportingRows();

        retrieveNumberportings();
        return null;
    }

    // This will move any selected number porting rows and mark them as additional articles
    public PageReference setAsAdditionalArticles() {
        Set<Id> cpIdToUpdate = new Set<Id>();

        //We cycle through our list of Contracted_Products__c and will check to see if the selected property is set to true, if it is we add to the list
        for(NumberportingRowWrapper nprw : portingRows){
            if(nprw.selected == true) {
                cpIdToUpdate.add(nprw.portingRow.Contracted_Product__c);
            }
        }

        if (cpIdToUpdate.size()==0) {
            ApexPages.addMessage(New ApexPages.Message(ApexPages.severity.Warning, 'No Numberblocks were moved as none were selected.'));    
            return null;       
        }

        List<Contracted_Products__c> cpToUpdate = new List<Contracted_Products__c>();
        for(Id cpId : cpIdToUpdate){
            Contracted_Products__c cp = new Contracted_Products__c(Id=cpId);
            cp.Additional_Article__c = true;
            cpToUpdate.add(cp);
        }
        update cpToUpdate;

        retrieveNumberportings();
        return null;
    }    

    public pageReference regenerateNumberportings(){

       
        // get unassigned numberportings
        List<Contracted_Products__c> allCPs = OrderUtils.getContractedProductsByContractId(contractId);
        List<Contracted_Products__c> unassignedNumberportingCPs = new List<Contracted_Products__c>();
        for(Contracted_Products__c cp : allCPs){
            if(cp.Order__c == null && cp.Product__r.Role__c == 'Numberporting'){
                unassignedNumberportingCPs.add(cp);
            }
        }

        List<Numberporting_Row__c> numberportingsToInsert = OrderFormController.createNumberportings(unassignedNumberportingCPs,theOrderWrapper.theOrder.Account__c);
    
        for(Numberporting_Row__c npr : numberportingsToInsert){
            if (npr.Numberporting__c == null) {
                Numberporting__c np = OrderUtils.createPorting(theOrderWrapper.theOrder);
                npr.Numberporting__c = np.Id;
            }   
            portingRows.add(new NumberportingRowWrapper(portingRows.size(),true,npr,false));
        }
        // assign orderid to any newly created numberporting contractedproducts
        for(Contracted_Products__c cp : unassignedNumberportingCPs){
            if(cp.Order__c == null) cp.Order__c = orderId;
        }
        insert numberportingsToInsert;
        update unassignedNumberportingCPs;
                

        return null;
    }
    
    public void updateNumberportingStatus(){
        // reload to correctly load the red/green flag
        retrieveNumberportings();
        
        Boolean allNumberportingsReady = true;

        for(NumberPortingRowWrapper np : portingRows){
            if(OrderValidation.checkObject(np.portingRow,theOrderWrapper.theOrder) != ''){
                allNumberportingsReady = false;
                break;
            }

            if(OrderValidation.checkObject(np.portingRow.Location__r,theOrderWrapper.theOrder) != ''){
                allNumberportingsReady = false;
                break;                  
            }
        }
        system.debug(allNumberPortingsReady);
        if(theOrderWrapper.theOrder.NumberportingReady__c != allNumberPortingsReady){
            theOrderWrapper.theOrder.NumberportingReady__c = allNumberportingsReady;            
            SharingUtils.updateRecordsWithoutSharing(theOrderWrapper.theOrder);
        }
        
        // recheck if this order is now clean 
        OrderUtils.updateOrderStatus(theOrderWrapper.theOrder,true);            

    } 

    public pageReference deleteSingleNumberportingRow(){
        Integer delIndex = integer.valueof(ApexPages.currentPage().getParameters().get('toDelete'));
        system.debug(delIndex);
        portingRows[delIndex].toDelete = true;
        return deleteNumberportingRows();
    }

    public pageReference deleteNumberportingRows(){

        Map<Id,Contracted_Products__c> cpToUpdate = new Map<Id,Contracted_Products__c>();
        List<Numberporting_row__c> nprToDelete = new List<Numberporting_row__c>();
        LisT<NumberportingRowWrapper> survivingPortingRows = new List<NumberportingRowWrapper>();

        for(NumberportingRowWrapper nprw : portingRows){
            if(nprw.toDelete){
                // in case the numberporting was generated from a contractedproduct and connected to an order, remove that contractedproduct from the order
                if(nprw.portingRow.Contracted_Product__c != null){
                    // using a map in order to prevent double updates
                    cpToUpdate.put(nprw.portingRow.Contracted_Product__c,New Contracted_Products__c(Id=nprw.portingRow.Contracted_Product__c,Order__c=null));
                }
                if(nprw.portingRow.Id != null) nprToDelete.add(nprw.portingRow);

            } else {
                survivingPortingRows.add(nprw);
            }
        }

        Savepoint sp = Database.setSavepoint();
        try{
            // reset row id's in order to prevent missing indices later on..
            for(Integer i=0;i<survivingPortingRows.size();i++){
                survivingPortingRows[i].Id = i;
            }
            // only maintain the surviving ones
            portingRows = survivingPortingRows;
            //portingRows.remove(integer.valueof(ApexPages.currentPage().getParameters().get('toDelete')));
            update cpToUpdate.values();
            delete nprToDelete;
            updateNumberportingStatus();

            retrieveNumberportings();

        } catch(DmlException ex){
            Database.rollback(sp);
            ApexPages.addMessages(ex);
        }           
        return null;                        
    }   

    public string getHTMLFormatActionHelptext(){
        return 'Porting_in - port in from other provider<br/>Porting_out - port out to other provider<br/>Activate - get number from Vodafone pool<br/>Deactivate - number stays in Vodafone pool'; 
        // TODO make this flexible (copy the standard helptext for this field. Did not find a way yet to cope with line breaks)
        //return Numberporting_row__c.Action__c.getDescribe().getInlineHelpText().replaceAll('\r\n', '<br/>');
    }
    
    public List<SelectOption> getPbxSelectOptions(){
        
        // retrieve all sites
        List<SelectOption> pbxSelectOptions = new List<SelectOption>();
        
        set<Id> siteIds = new set<Id>();
        for(Contracted_Products__c cp : [Select Id, Site__c From Contracted_Products__c Where Site__c != null AND VF_Contract__c = :contractId]){
            siteIds.add(cp.Site__c);
        }
        
        // retrieve all pbxs that are selectable (so only on order locations) - UNDID that to fetch Onenet PBX's 
        for(Competitor_Asset__c ca : OrderUtils.retrieveAccountIdToPBXs(theOrderWrapper.theOrder.account__c,null)){
            SelectOption so = new SelectOption(ca.Id,ca.Name + '_' + ca.PBX_Type__r.Name + '_' + ca.Site__r.Name);
            pbxSelectOptions.add(so);
        }               
        return pbxSelectOptions;
                    
    }   
    
    public List<SelectOption> getLocationSelectOptions(){
        
        // retrieve all sites
        List<SelectOption> locationSelectOptions = new List<SelectOption>();
        
        // retrieve all locations that are selectable (all locations of the account)
        for(Site__c s : OrderUtils.retrieveAccountIdToSites(new Set<Id>{theOrderWrapper.theOrder.account__c}).get(theOrderWrapper.theOrder.account__c)){
            SelectOption so = new SelectOption(s.Id,s.Name + '_' + s.Site_Postal_Code__c+s.Site_House_Number__c+(s.Site_House_Number_Suffix__c==null?'':s.Site_House_Number_Suffix__c));
            locationSelectOptions.add(so);
        }               
        return locationSelectOptions;
                    
    }   
}