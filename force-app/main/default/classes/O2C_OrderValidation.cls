public with sharing class O2C_OrderValidation {
    private static Map<String, String> mapBillingObjectsWithConfigName = new Map<String, String>{
        String.valueOf(Ban__c.SObjectType) => 'Ban__r.',
        String.valueOf(
            Financial_Account__c.SObjectType
        ) => 'Financial_Account__r.',
        String.valueOf(
            Billing_Arrangement__c.SObjectType
        ) => 'Billing_Arrangement__r.'
    };

    @AuraEnabled(cacheable=true)
    public static LightningResponse getResult(String recordId) {
        try {
            System.debug('recordId: ' + recordId);
            if (String.isEmpty(recordId)) {
                return new LightningResponse()
                    .setError('A valid record Id is required!');
            }

            String validationObjectApiName = Id.valueOf(recordId)
                .getSObjectType()
                .getDescribe()
                .getName();

            System.debug('validationObjectApiName: ' + validationObjectApiName);

            if (
                !mapBillingObjectsWithConfigName.containsKey(
                    validationObjectApiName
                )
            ) {
                return new LightningResponse()
                    .setError(
                        'Record id is not from a valid Billing Object: ' +
                        validationObjectApiName
                    );
            }

            List<SObject> record = getRecordToValidate(
                recordId,
                validationObjectApiName
            );
            System.debug('record : ' + record);

            SObject recordToValidate = record[0];

            System.debug('recordToValidate: ' + recordToValidate);

            List<String> allErrors = new List<String>();
            allErrors.addAll(
                runValidation(recordToValidate, validationObjectApiName)
            );
            System.debug('allErrors: ' + JSON.serialize(allErrors));

            return new LightningResponse().setBody(allErrors);
        } catch (Exception e) {
            System.debug('StackTraceString: ' + e.getStackTraceString());
            System.debug('Message: ' + e.getMessage());
            return new LightningResponse().setError(e.getMessage());
        }
    }

    private static List<String> runValidation(
        sObject obj,
        String validationObjectApiName
    ) {
        List<String> errors = new List<String>();
        for (Order_Validation__c validation : [
            SELECT
                Id,
                Name,
                Proposition__c,
                Object__c,
                Who__c,
                Export_System__c,
                Operator__c,
                API_Fieldname__c,
                Value__c,
                Logical_Operator__c,
                Operator_2__c,
                API_Fieldname_2__c,
                Value_2__c,
                Operator_3__c,
                API_Fieldname_3__c,
                Value_3__c,
                Error_message__c
            FROM Order_Validation__c
            WHERE Active__c = TRUE AND Object__c = :validationObjectApiName
        ]) {
            String error = OrderValidation.runValidation(obj, validation);
            if (String.isNotEmpty(error)) {
                errors.add(error);
            }
        }
        System.debug(
            'errors for ' +
            obj.getSObjectType().getDescribe().getName().toLowerCase() +
            ': ' +
            errors
        );
        return errors;
    }

    // query the record that needs to be validation
    private static List<SObject> getRecordToValidate(
        Id orderId,
        String validationObjectApiName
    ) {
        Set<String> setAllFields = new Set<String>{ 'Id' };
        // Get the fields from OrderValidationField__c
        for (
            OrderValidationField__c ovf : OrderValidationField__c.getAll()
                .values()
        ) {
            if (ovf.object__c == 'Billing_Arrangement__c') {
                // substring the right field from configuration based on validation object api name
                String configName = mapBillingObjectsWithConfigName.get(
                    validationObjectApiName
                );
                String fieldApiName = ovf.Field__c.substringAfter(configName);
                if (fieldApiName != '') {
                    setAllFields.add(fieldApiName);
                }
            }
        }

        List<String> lstAllFields = new List<String>();
        lstAllFields.addAll(setAllFields);
        String fieldsCsv = String.join(lstAllFields, ', ');

        String query = String.format(
            'SELECT {0} FROM {1} WHERE Id = :orderId',
            new List<String>{ fieldsCsv, validationObjectApiName }
        );

        System.debug('query: ' + query);
        List<SObject> orderIdToBillingData = Database.query(query);
        return orderIdToBillingData;
    }
}