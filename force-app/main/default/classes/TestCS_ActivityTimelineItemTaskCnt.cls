@IsTest
private class TestCS_ActivityTimelineItemTaskCnt {
    @IsTest
    static void testBehavior() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            VF_Contract__c vfContract1 = CS_DataTest.createDefaultVfContract(simpleUser, false);
            vfContract1.Eligible_for_CS_Mobile_Flow__c = false;
            vfContract1.RecordTypeId = Schema.SObjectType.VF_Contract__c.getRecordTypeInfosByName().get('Contract Implementation').getRecordTypeId();
            insert vfContract1;

            Case case1 = CS_DataTest.createCase(
                'Test Case',
                'CS MF Order Cleaning',
                simpleUser,
                false);
            case1.Contract_VF__c = vfContract1.Id;
            insert case1;

            Contact cnt = CS_DataTest.createContact(
                'Name',
                'LastNAme',
                'Role',
                'email@mail.com',
                null);

            List<Task> newTasks = CS_DataTest.createTasks(
                cnt,
                case1,
                'ABCD',
                true,
                'Description',
                'Subjct',
                'In progress',
                simpleUser,
                10,
                true);

            Test.startTest();
            CS_ActivityTimelineItemTaskController.updateRecord(newTasks[0].Id, true);
            CS_ActivityTimelineItemTaskController.updateRecord(newTasks[1].Id, false);

            List<Task> tasksAfterUpdate = [
                SELECT Id, Status 
                FROM Task 
                WHERE Id IN :newTasks
            ];
            System.assertEquals(tasksAfterUpdate[0].Status, 'Completed');
            System.assertEquals(tasksAfterUpdate[1].Status, 'In Progress');
            Test.stopTest();
        }
    }
}