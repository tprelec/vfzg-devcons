/**
 * @description       : Apex Test Class for the OrderTypeTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 20-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   20-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestOrderTypeTriggerHandler {
	@testSetup
	static void setup() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'OrderType',
			External_Number__c = 1,
			Object_Prefix__c = 'OT-04-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;
	}

	@isTest
	static void testSetExternalIds() {
		OrderType__c objOrderType = CS_DataTest.createOrderType('MAC');

		Test.startTest();
		insert objOrderType;
		Test.stopTest();

		OrderType__c objOrderTypeUpdated = [SELECT Id, ExternalID__c FROM OrderType__c WHERE Id = :objOrderType.Id LIMIT 1];

		System.assertEquals('OT-04-000002', objOrderTypeUpdated.ExternalID__c, 'The External ID on Order Type was not set correctly.');
	}
}
