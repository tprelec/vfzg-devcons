@isTest
global class TestUtilMockServices implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) 
           	
	{
		system.debug(stub);
		system.debug(request);
			
		// PC availability check mock response
		//if(endpoint.endsWith('pcavailability')){
		if(requestName == 'locationsCombinedRequest'){		
			ECSSOAPPostalCodeCheck.locationsResponseType respElement = new ECSSOAPPostalCodeCheck.locationsResponseType();
			List<ECSSOAPPostalCodeCheck.locationTypeResponse> locs = new List<ECSSOAPPostalCodeCheck.locationTypeResponse>();
			ECSSOAPPostalCodeCheck.locationTypeResponse loc = new ECSSOAPPostalCodeCheck.locationTypeResponse();
			loc.zipcode = '1234AB';
        	loc.housenumber = '5';
        	loc.housenumberext = 'a';
        	ECSSOAPPostalCodeCheck.lineoptionsType lineops = new ECSSOAPPostalCodeCheck.lineoptionsType();
        	List<ECSSOAPPostalCodeCheck.optionType> ops = new List<ECSSOAPPostalCodeCheck.optionType>();
        	ops.add(getFiberOption());
        	ops.add(getDslOption());
        	lineops.option = ops;
        	loc.lineoptions = lineops;
        	locs.add(loc);
        	respElement.location = locs;
        	response.put('response_x', respElement);
		} else if(requestName == 'locationsDslRequest'){		
			ECSSOAPPostalCodeCheck.locationsResponseType respElement = new ECSSOAPPostalCodeCheck.locationsResponseType();
			List<ECSSOAPPostalCodeCheck.locationTypeResponse> locs = new List<ECSSOAPPostalCodeCheck.locationTypeResponse>();
			ECSSOAPPostalCodeCheck.locationTypeResponse loc = new ECSSOAPPostalCodeCheck.locationTypeResponse();
			loc.zipcode = '1234AB';
        	loc.housenumber = '5';
        	loc.housenumberext = 'a';
        	ECSSOAPPostalCodeCheck.lineoptionsType lineops = new ECSSOAPPostalCodeCheck.lineoptionsType();
        	List<ECSSOAPPostalCodeCheck.optionType> ops = new List<ECSSOAPPostalCodeCheck.optionType>();
        	ops.add(getDslOption());
        	lineops.option = ops;
        	loc.lineoptions = lineops;
        	locs.add(loc);
        	respElement.location = locs;
        	response.put('response_x', respElement);
		} else if(requestName == 'locationsFiberRequest'){		
			ECSSOAPPostalCodeCheck.locationsResponseType respElement = new ECSSOAPPostalCodeCheck.locationsResponseType();
			List<ECSSOAPPostalCodeCheck.locationTypeResponse> locs = new List<ECSSOAPPostalCodeCheck.locationTypeResponse>();
			ECSSOAPPostalCodeCheck.locationTypeResponse loc = new ECSSOAPPostalCodeCheck.locationTypeResponse();
			loc.zipcode = '1234AB';
        	loc.housenumber = '5';
        	loc.housenumberext = 'a';
        	ECSSOAPPostalCodeCheck.lineoptionsType lineops = new ECSSOAPPostalCodeCheck.lineoptionsType();
        	List<ECSSOAPPostalCodeCheck.optionType> ops = new List<ECSSOAPPostalCodeCheck.optionType>();
        	ops.add(getFiberOption());
        	lineops.option = ops;
        	loc.lineoptions = lineops;
        	locs.add(loc);
        	respElement.location = locs;
        	response.put('response_x', respElement);
		} else if(requestName == 'createCustomerRequest'){
			ECSSOAPCompany.customerResponse_element respElement = new ECSSOAPCompany.customerResponse_element();
			List<ECSSOAPCompany.companyResponseType> companies = new List<ECSSOAPCompany.companyResponseType>();
			ECSSOAPCompany.companyResponseType comp = new ECSSOAPCompany.companyResponseType();
			comp.corporateId = '';
			comp.BOPCode = 'XXX';
			comp.errorCode = '0';
			comp.errorMessage = 'success';
			companies.add(comp);
			respElement.customer = companies;
			response.put('response_x',respElement);
			
		} else if(requestName == 'updateCustomerRequest'){
			ECSSOAPCompany.customerResponse_element respElement = new ECSSOAPCompany.customerResponse_element();
			List<ECSSOAPCompany.companyResponseType> companies = new List<ECSSOAPCompany.companyResponseType>();
			ECSSOAPCompany.companyResponseType comp = new ECSSOAPCompany.companyResponseType();
			comp.corporateId = '';
			comp.BOPCode = 'XXX';
			comp.errorCode = '0';
			comp.errorMessage = 'success';
			companies.add(comp);
			respElement.customer = companies;
			response.put('response_x',respElement);					
		}
   	}
   	
   	private ECSSOAPPostalCodeCheck.optionType getFiberOption(){
    	ECSSOAPPostalCodeCheck.optionType op1 = new ECSSOAPPostalCodeCheck.optionType();
    	op1.technologytype = 'Fiber';
        ECSSOAPPostalCodeCheck.bandwidthType ul = new ECSSOAPPostalCodeCheck.bandwidthType(); 
        ul.min = 0;
        ul.max = 2048;
        op1.upload = ul;
        ECSSOAPPostalCodeCheck.bandwidthType dl = new ECSSOAPPostalCodeCheck.bandwidthType();  
        dl.min = 0;
        dl.max = 2048;
        op1.download = ul;
        op1.plandate = '';
        op1.accessclass = 'fiber';
        op1.serviceable = 'Groen';
        op1.classification = '';
        op1.portfolio = 'test-fiber-business';
        op1.linetype = 'fiber';
        
   		return op1;
   	}
   	
   	private ECSSOAPPostalCodeCheck.optionType getDslOption(){
    	ECSSOAPPostalCodeCheck.optionType op1 = new ECSSOAPPostalCodeCheck.optionType();
    	op1.technologytype = 'ADSL_ISDN';
        ECSSOAPPostalCodeCheck.bandwidthType ul = new ECSSOAPPostalCodeCheck.bandwidthType(); 
        ul.min = 0;
        ul.max = 2048;
        op1.upload = ul;
        ECSSOAPPostalCodeCheck.bandwidthType dl = new ECSSOAPPostalCodeCheck.bandwidthType();  
        dl.min = 0;
        dl.max = 2048;
        op1.download = ul;
        op1.plandate = '';
        op1.accessclass = 'ETH,ATM';
        op1.serviceable = 'Green';
        op1.classification = '';
        op1.portfolio = 'test-dsl-business';
        op1.linetype = 'dsl';
        
   		return op1;   		
   	}
}