/** 
 * @description			This exception should be thrown when expected data is missing in the database.
 * @author				Guy Clairbois
 */
public class ExMissingDataException extends Exception {}