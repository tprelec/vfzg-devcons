@IsTest
private class TestCS_ActivityTimelineController {
    @IsTest
    static void testGetActivityTimeline() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs (simpleUser) {
            Account tmpAcc = CS_DataTest.createAccount('Test Account');
            insert tmpAcc;

            VF_Contract__c vfContract1 = CS_DataTest.createDefaultVfContract(simpleUser, false);
            vfContract1.Eligible_for_CS_Mobile_Flow__c = false;
            vfContract1.RecordTypeId = Schema.SObjectType.VF_Contract__c.getRecordTypeInfosByName().get('Contract Implementation').getRecordTypeId();
            insert vfContract1;

            Contact con = CS_DataTest.createContact(
                'Contact Name',
                'String lastName',
                'not so importan role',
                'mail@address.com',
                tmpAcc.Id);
            insert con;

            Case orderCleaningCase = CS_DataTest.createCase(
                'Order Cleaning',
                'CS MF Order Cleaning',
                simpleUser,
                false);
            orderCleaningCase.Contract_VF__c = vfContract1.Id;
            insert orderCleaningCase;

            CS_DataTest.createTasks(
                con,
                orderCleaningCase,
                'A',
                true,
                'description',
                'Subject',
                'Open',
                simpleUser,
                10,
                true);

            CS_ActivityTimelineModel result = CS_ActivityTimelineController.getActivityTimeline(orderCleaningCase.Id);
            Integer expectedGroupSize = 1;
            Integer expectedItemsSize = 10;
            System.assertEquals(expectedGroupSize,result.groups.size());
            System.assertEquals(expectedItemsSize,result.items.size());
        }
    }
}