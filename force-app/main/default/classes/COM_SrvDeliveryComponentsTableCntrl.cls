public without sharing class COM_SrvDeliveryComponentsTableCntrl {
	@AuraEnabled(cacheable=true)
	public static List<ServiceWrapper> getServices(String recId) {
		List<ServiceWrapper> retlist = new List<ServiceWrapper>();
		List<Id> servicesId = new List<Id>();
		List<csord__Service__c> services = new List<csord__Service__c>();

		for (csord__Service__c srv : [
			SELECT
				Id,
				Name,
				csord__Service__c,
				csordtelcoa__Cancelled_By_Change_Process__c,
				(SELECT Id, Name, csord__Service__c, csordtelcoa__Cancelled_By_Change_Process__c FROM csord__Services__r)
			FROM csord__Service__c
			WHERE COM_Delivery_Order__c = :getDeliveryOrderId(recId) AND csord__Service__c = NULL
			ORDER BY Name
		]) {
			services.add(srv);
			servicesId.add(srv.Id);
			for (csord__Service__c childSrv : srv.csord__Services__r) {
				if (
					childSrv.csordtelcoa__Cancelled_By_Change_Process__c == false ||
					(childSrv.csordtelcoa__Cancelled_By_Change_Process__c == true &&
					srv.csordtelcoa__Cancelled_By_Change_Process__c == true)
				) {
					services.add(childSrv);
					servicesId.add(childSrv.Id);
				}
			}
		}

		List<COM_MetadataInformation.DeliveryComponent> deliveryComponents = new List<COM_MetadataInformation.DeliveryComponent>();
		COM_MetadataInformation metadataInformation = new COM_MetadataInformation();
		COM_DeliveryComponentsGenerator generator = new COM_DeliveryComponentsGenerator();
		metadataInformation = generator.prepareDataToGenerateComponents(servicesId);
		deliveryComponents = generator.generateDeliveryComponents(metadataInformation);

		for (csord__Service__c srv : services) {
			retList.add(new ServiceWrapper(srv));
			for (COM_MetadataInformation.DeliveryComponent delCom : deliveryComponents) {
				if (delCom.serviceId == srv.Id) {
					retList.add(new ServiceWrapper(delCom));
				}
			}
		}

		return retList;
	}

	private static Id getDeliveryOrderId(Id recId) {
		Id deliveryOrderId = recId;

		if (recId.getSObjectType().getDescribe().getName() == 'Task') {
			Task taskRec = [SELECT Id, COM_Delivery_Order__c FROM Task WHERE Id = :recId];
			deliveryOrderId = taskRec.COM_Delivery_Order__c;
		}
		return deliveryOrderId;
	}

	public class ServiceWrapper {
		@AuraEnabled
		public String parentServiceName;
		@AuraEnabled
		public String name;
		@AuraEnabled
		public String deliveryComponentName;
		@AuraEnabled
		public String deliveryArticleName;

		public ServiceWrapper(csord__Service__c service) {
			this.parentServiceName = service.csord__Service__c == null ? service.Name : null;
			this.name = service.csord__Service__c == null ? null : service.Name;
		}

		public ServiceWrapper(COM_MetadataInformation.DeliveryComponent delComponent) {
			this.deliveryComponentName = delComponent.name;
			this.deliveryArticleName = delComponent.articleName;
		}

		public ServiceWrapper() {
		}
	}
}
