@isTest
private class TestCOM_DXL_Sites {
	@testSetup
	static void setup() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		User owner = TestUtils.createAdministrator();

		Account acct = TestUtils.createAccount(owner);

		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test', Email = 'contact@test.com', Phone = '+31 111111789');
		insert c;

		Site__c testSite = TestUtils.createSite(acct);

		List<External_Site__c> externalSites = new List<External_Site__c>();
		External_Site__c extarnalSite1 = new External_Site__c(
			Site__c = testSite.Id,
			External_Source__c = 'Unify',
			External_Site_Id__c = 'UInifyID123654789'
		);
		External_Site__c extarnalSite2 = new External_Site__c(
			Site__c = testSite.Id,
			External_Source__c = 'BOP',
			External_Site_Id__c = 'BOPID123654789'
		);
		externalSites.add(extarnalSite1);
		externalSites.add(extarnalSite2);
		insert externalSites;

		Opportunity testOpportunity = CS_DataTest.createOpportunity(acct, 'Test Opportunity', owner.Id);
		testOpportunity.Main_Contact_Person__c = c.Id;
		insert testOpportunity;

		csord__Order__c ord = CS_DataTest.createOrder();
		ord.csord__Status2__c = 'Created';
		ord.csordtelcoa__Opportunity__c = testOpportunity.Id;
		insert ord;
	}

	@isTest
	static void testMethod1() {
		List<Opportunity> oppList = [
			SELECT
				Id,
				AccountId,
				Account.KVK_number__c,
				Account.date_of_establishment__c,
				Account.Legal_Structure_Code__c,
				Account.NumberOfEmployees,
				Account.Name,
				Account.Unify_Account_Type__c,
				Account.Unify_Account_SubType__c,
				Account.Fax,
				Account.duns_number__c,
				Account.BOPCode__c,
				BAN__r.BAN_Name__c,
				BAN__r.BAN_Number__c,
				BAN__r.Unify_Ref_Id__c,
				BAN__r.Unify_Customer_Type__c,
				BAN__r.Unify_Customer_SubType__c,
				Account.Visiting_Postal_Code__c,
				Account.Visiting_HouseNumber1__c,
				Account.Visiting_HouseNumber_Suffix__c,
				Account.Visiting_street__c,
				Account.Visiting_City__c,
				Account.Visiting_Country__c,
				Account.Fixed_Dealer__c,
				Account.Fixed_Dealer__r.Contact__r.UserId__r.UserType,
				Account.Fixed_Dealer__r.Contact__r.UserId__r.Name,
				Account.Fixed_Dealer__r.Contact__r.UserId__r.Manager.Name,
				Account.Fixed_Dealer__r.Contact__r.Account.BOPCode__c,
				Account.Mobile_Dealer__c,
				Account.Mobile_Dealer__r.Contact__r.UserId__r.UserType,
				Account.Mobile_Dealer__r.Contact__r.UserId__r.Name,
				Account.Mobile_Dealer__r.Contact__r.UserId__r.Manager.Name,
				Account.Mobile_Dealer__r.Contact__r.Account.BOPCode__c,
				Account.Owner.UserType,
				Account.Owner.Name,
				Account.Owner.Manager.Name,
				Account.Owner.Account.BOPCode__c,
				Account.Authorized_to_Sign_1st__c,
				Account.Authorized_to_Sign_1st__r.Email,
				Account.Authorized_to_Sign_1st__r.Phone,
				Account.Authorized_to_Sign_1st__r.BirthDate,
				Account.Authorized_to_Sign_1st__r.FirstName,
				Account.Authorized_to_Sign_1st__r.LastName,
				Account.Authorized_to_Sign_1st__r.AccountId,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Postal_Code__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Housenumber1__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Housenumber_Suffix__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Street__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_City__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Country__c,
				Financial_Account__c,
				Financial_Account__r.Payment_Method__c,
				Financial_Account__r.Financial_Contact__r.Name,
				Financial_Account__r.Unify_Ref_Id__c,
				Financial_Account__r.Bank_Account_Name__c,
				Financial_Account__r.Bank_Account_Number__c,
				Financial_Account__r.Financial_Contact__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Postal_Code__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Housenumber1__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Housenumber_Suffix__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Street__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_City__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Country__c,
				Financial_Account__r.Financial_Contact__r.Email,
				Financial_Account__r.Financial_Contact__r.Phone,
				Financial_Account__r.Financial_Contact__r.Birthdate,
				Financial_Account__r.Financial_Contact__r.FirstName,
				Financial_Account__r.Financial_Contact__r.LastName,
				Financial_Account__r.Financial_Contact__r.AccountId,
				Billing_Arrangement__r.Unify_Ref_Id__c,
				Billing_Arrangement__r.Billing_Arrangement_Alias__c,
				Billing_Arrangement__r.Bill_Format__c,
				Billing_Arrangement__r.Billing_Postal_Code__c,
				Billing_Arrangement__r.Billing_Housenumber__c,
				Billing_Arrangement__r.Billing_Housenumber_Suffix__c,
				Billing_Arrangement__r.Billing_Street__c,
				Billing_Arrangement__r.Billing_City__c,
				Billing_Arrangement__r.Billing_Country__c,
				Contact__c,
				Contact__r.Account.Visiting_Postal_Code__c,
				Contact__r.Account.Visiting_Housenumber1__c,
				Contact__r.Account.Visiting_Housenumber_Suffix__c,
				Contact__r.Account.Visiting_Street__c,
				Contact__r.Account.Visiting_City__c,
				Contact__r.Account.Visiting_Country__c,
				Contact__r.Email,
				Contact__r.Phone,
				Contact__r.MobilePhone,
				Contact__r.BirthDate,
				Contact__r.Document_Type__c,
				Contact__r.Document_Number__c,
				Contact__r.Jurisdiction_Type__c,
				Contact__r.Contact_Role__c,
				Contact__r.FirstName,
				Contact__r.LastName,
				Contact__r.AccountId
			FROM Opportunity
		];

		List<Site__c> sites = [
			SELECT
				Id,
				Site_Street__c,
				Site_House_Number__c,
				Site_House_Number_Suffix__c,
				Site_Postal_Code__c,
				Site_City__c,
				Country__c,
				Location_Alias__c,
				Unify_Site_Name__c,
				Canvas_Street__c,
				Canvas_House_Number__c,
				Canvas_House_Number_Suffix__c,
				Canvas_Postal_Code__c,
				Canvas_City__c,
				Canvas_Country__c,
				SLA__r.BOP_Code__c,
				Location_Type__c,
				Site_Phone__c,
				Building__c,
				Site_Account__c
			FROM Site__c
		];

		List<csord__Order__c> ordList = [SELECT Id, Name, csord__Status2__c, csordtelcoa__Opportunity__c FROM csord__Order__c];

		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(oppList[0].Id, ordList[0].Id);
		List<External_Site__c> externalSites = customerService.getExternalSites(new Map<Id, Site__c>(sites).keySet());
		Map<Id, List<External_Site__c>> sitesWithExternalSites = customerService.getSiteExternalSites(externalSites);
		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		COM_DXL_Sites dxlSites = new COM_DXL_Sites(generator, sites, sitesWithExternalSites);
		dxlSites.generateSites();
		generator.writeEndObject();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(generator.getAsString());
		List<Object> sitesList = (List<Object>) m.get('sites');
		Map<String, Object> x = (Map<String, Object>) sitesList[0];

		System.assertEquals('UInifyID123654789', x.get('unifySiteRefId'), 'Expected -> UInifyID123654789 -> Actual: ' + x.get('unifySiteRefId'));
	}
}
