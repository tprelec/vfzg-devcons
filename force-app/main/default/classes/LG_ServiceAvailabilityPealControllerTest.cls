@isTest
private class LG_ServiceAvailabilityPealControllerTest {
	
	@testsetup
	private static void setupTestData()
	{
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		List<LG_RfsCheckVariables__c> rfsVariables = new List<LG_RfsCheckVariables__c>();
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'AnalogueTv', LG_Value__c = 'Catv'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DigitalTv', LG_Value__c = 'DMM'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownBasic', LG_Value__c = '130'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownFp200', LG_Value__c = '300'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'DownFp500', LG_Value__c = '500'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Fp200', LG_Value__c = 'UPC Fiber Power Internet'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Fp500', LG_Value__c = 'UPC_superfast'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Internet', LG_Value__c = 'UPC Internet'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Mobile', LG_Value__c = 'mobile'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'MobileInternet', LG_Value__c = 'mobile_internet'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Qos', LG_Value__c = 'QoS'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'Telephony', LG_Value__c = 'telephony'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpBasic', LG_Value__c = '30'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpFp200', LG_Value__c = '40'));
		rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'UpFp500', LG_Value__c = '40'));
        //This variable added as part of CATGOV-1052 Start.
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'ZakelijkInternetGiga' , LG_Value__c = 'Ziggo_Giga'));
        rfsVariables.add(new LG_RfsCheckVariables__c(Name = 'GigaDownFp1000' , LG_Value__c = '1000'));
	   //CATGOV-1052 End.
		insert rfsVariables;
		
		noTriggers.Flag__c = false;
		upsert noTriggers;
	}
		
	private static testmethod void testGetExtensionSuggestions()
	{		
		LG_ServiceAvailabilityPealController controller = new LG_ServiceAvailabilityPealController();
        controller.houseNumberAttribute = '47';
		controller.postalCodeAttribute = 'testPostalCode';
        
		String response = LG_AddressResponse.buildAddressResponse(String.isNotBlank(controller.extensionAttribute));
		Test.setMock(HttpCalloutMock.class, new LG_MACDConfRfsCheckMock(200, 'OK', response));
		
		Test.startTest();
			controller.updateServiceAvailability();
		Test.stopTest();
		
		System.assertNotEquals(null, controller.extensionSuggestions, 'House Number extensions should not be null');
		System.assertEquals(true, controller.extensionSuggestions.size() > 0, 'There should be multiple available extensions');
	}
	
	private static testmethod void testUseWithExtension()
	{		
		LG_ServiceAvailabilityPealController controller = new LG_ServiceAvailabilityPealController();
		controller.houseNumberAttribute = '47';
		controller.postalCodeAttribute = 'testPostalCode';
		controller.extensionAttribute = 'Ext';
		
		
		String response = LG_AddressResponse.buildAddressResponse(String.isNotBlank(controller.extensionAttribute));
		Test.setMock(HttpCalloutMock.class, new LG_MACDConfRfsCheckMock(200, 'OK', response));
		
		Test.startTest();
			controller.updateServiceAvailability();
		Test.stopTest();
		
		System.assertNotEquals(null, controller.serviceAvailabilityInformation, 'Service Availability should not be null.');
	}
	
	private static testmethod void testRfsCheckAndParsingResponse()
	{
		LG_ServiceAvailabilityPealController controller = new LG_ServiceAvailabilityPealController();
		
		Test.setMock(HttpCalloutMock.class, new LG_MACDConfRfsCheckMock(200, 'OK', LG_RfsCheckUtility.buildRfsResponse(true, true, false, true, true, true)));
		
		Test.startTest();
			List<LG_ServiceAvailabilityPealController.ServiceAvailability> availabilityList = controller.rfsCheck('addressIdTest');
		Test.stopTest();
	}
}