public with sharing class CS_OrderTriggerHandler extends TriggerHandler {
	public override void beforeInsert() {
		List<csord__Order__c> lst = (List<csord__Order__c>) this.newList;
		populateBillingInformation(lst);
	}

	public override void afterUpdate() {
		Map<Id, csord__Order__c> newMap = (Map<Id, csord__Order__c>) this.newMap;
		Map<Id, csord__Order__c> oldMap = (Map<Id, csord__Order__c>) this.oldMap;

		COM_O2UDecomposition.orderDecomposition(newMap, oldMap);
	}

	private void populateBillingInformation(List<csord__Order__c> newMap) {
		Set<Id> opportunityIds = new Set<Id>();
		for (csord__Order__c order : newMap) {
			if (order.csordtelcoa__Opportunity__c != null) {
				opportunityIds.add(order.csordtelcoa__Opportunity__c);
			}
		}

		if (!opportunityIds.isEmpty()) {
			Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>(
				[SELECT Id, BAN__c, Financial_Account__c, Billing_Arrangement__c FROM Opportunity WHERE Id = :opportunityIds]
			);

			for (csord__Order__c order : newMap) {
				Opportunity opportunity = opportunityMap.get(order.csordtelcoa__Opportunity__c);
				if (
					opportunity != null &&
					(order.BAN_Information__c != opportunity.BAN__c ||
					order.Financial_Account__c != opportunity.Financial_Account__c ||
					order.Billing_Arrangement__c != opportunity.Billing_Arrangement__c)
				) {
					order.BAN_Information__c = opportunity.BAN__c;
					order.Financial_Account__c = opportunity.Financial_Account__c;
					order.Billing_Arrangement__c = opportunity.Billing_Arrangement__c;
				}
			}
		}
	}
}
