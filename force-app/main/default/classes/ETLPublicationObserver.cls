/**
 * Created by bojan.zunko on 11/05/2020.
 * Calls ETL service to complete publication process using dispatcher.
 */

global with sharing class ETLPublicationObserver implements csam.ObserverApi.IObserver {
	public class CustomException extends Exception {
	}

	global void exceptionHandling(csam.ObserverApi.Observable o, Exception e) {
		Attachment errorOut = new Attachment();
		errorOut.Name = 'Error during observer';
		errorOut.Body = Blob.valueOf(e.getMessage());

		if (o != null) {
			csam.InboundMessageObservable observable = (csam.InboundMessageObservable) o;
			errorOut.ParentId = observable.getMessages()[0].Id;
			insert errorOut;
		}
	}

	global void execute(csam.ObserverApi.Observable o, Object arg) {
		try {
			if (o instanceof csam.InboundMessageObservable) {
				csam.InboundMessageObservable observable = (csam.InboundMessageObservable) o;
				execute(observable.getMessages());
			}
		} catch (Exception e) {
			exceptionHandling(o, e);
		}
	}

	global void execute(List<csam__Incoming_Message__c> messages) {
		String publicationHandlerRegEx = '^ECommerce\\sPublication(\\s|\\sv\\d\\s)Sync$';
		for (csam__Incoming_Message__c incomingMessage : messages) {
			if (
				incomingMessage.csam__Final_Chunk__c &&
				incomingMessage.csam__Outgoing_Message__c != null &&
				incomingMessage.csam__Outgoing_Message__r.csam__ObjectGraph_Callout_Handler__c != null &&
				Pattern.matches(publicationHandlerRegEx, incomingMessage.csam__Outgoing_Message__r.csam__ObjectGraph_Callout_Handler__r.Name) &&
				!incomingMessage.csam__Incoming_URL_Path__c.endsWith('error')
			) {
				List<csam__Outgoing_Message_Record__c> omrInProcess = [
					SELECT csam__Object_Record_Id__c
					FROM csam__Outgoing_Message_Record__c
					WHERE csam__Outgoing_Message__c = :incomingMessage.csam__Outgoing_Message__c
				];
				ETLCallout.doCallout(omrInProcess[0].csam__Object_Record_Id__c);
			}
		}
	}
}
