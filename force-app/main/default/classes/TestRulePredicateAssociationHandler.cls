@isTest
public with sharing class TestRulePredicateAssociationHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Rule/Predicate Association';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'RPA-19-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;

		csclm__Clause_Definition__c cd = new csclm__Clause_Definition__c();
		cd.ExternalID__c = '1';
		insert cd;

		csclm__Applicability_Rule__c ar = new csclm__Applicability_Rule__c();
		ar.csclm__Clause_Definition__c = cd.Id;
		ar.ExternalID__c = '1';
		insert ar;

		csclm__Predicate__c predicate = new csclm__Predicate__c();
		predicate.ExternalID__c = '1';
		predicate.csclm__Subject__c = 'Name';
		predicate.csclm__Subject_Type__c = 'cscfga__Product_Basket__c';
		insert predicate;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		csclm__Rule_Predicate_Association__c testRpa = new csclm__Rule_Predicate_Association__c();
		testRpa.csclm__Applicability_Rule__c = [SELECT Id FROM csclm__Applicability_Rule__c LIMIT 1].Id;
		testRpa.csclm__Predicate__c = [SELECT Id FROM csclm__Predicate__c LIMIT 1].Id;
		insert testRpa;

		csclm__Rule_Predicate_Association__c queryRecord = [SELECT Id, ExternalID__c FROM csclm__Rule_Predicate_Association__c LIMIT 1];
		System.assert(queryRecord.ExternalID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
