@RestResource(urlMapping='/RemedyUpdate')
global without sharing class CST_RemedyUpdateService {
	public CST_RemedyUpdateService() {
	}

	public class RemedyRequestWrapper {
		public String IncidentId { get; set; }
		public String Status { get; set; }
		public String Assignee { get; set; }
		public String Priority { get; set; }
	}

	@HttpPatch
	global static String updateTicket() {
		CST_External_Ticket__c remedyTicket = new CST_External_Ticket__c();
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		String responseString = '';
		try {
			String reqBody = req.requestBody.toString();
			System.debug('reqBody -> ' + reqBody);

			if (reqBody != null) {
				RemedyRequestWrapper responseWrapper = (RemedyRequestWrapper) System.JSON.deserialize(reqBody, RemedyRequestWrapper.class);
				System.debug(responseWrapper);
				System.debug(responseWrapper.IncidentId);
				if (responseWrapper.IncidentId != null) {
					remedyTicket.CST_IncidentID__c = responseWrapper.IncidentId;
					remedyTicket.CST_Status__c = responseWrapper.Status;
					remedyTicket.CST_Assignee__c = responseWrapper.Assignee;
					remedyTicket.CST_Ticket_Priority__c = responseWrapper.Priority;
				}
				upsert remedyTicket CST_IncidentID__c; //upsert only because of the externalId, easier to map. Remedy will ALWAYS have a Case associate
				responseString = updateCSTCase(remedyTicket.CST_IncidentID__c);
			}

			return responseString;
		} catch (System.DmlException e) {
			System.debug('exception ->' + e.getMessage());
			responseString = e.getMessage();
		} catch (Exception e) {
			//something else happened
			responseString = e.getMessage();
		}

		return responseString;
	}

	private static String updateCSTCase(String remedyId) {
		CST_External_Ticket__c remedy = [
			SELECT id, CST_Status__c, CST_Case__c, CST_IncidentID__c, CST_Status_Reason__c
			FROM CST_External_Ticket__c
			WHERE CST_IncidentID__c = :remedyId
			LIMIT 1
		];
		Id caseId = remedy.CST_Case__c;
		if (caseId == null) {
			//this means it has inserted without a Case being associated (wrong IncidentId for instance.) - In this case, we should delete the remedy ticket and send a friendly message
			delete remedy;
			return 'No Remedy Ticket found with the given IncidentId.';
		}

		Case remedyCase = [
			SELECT id, casenumber, status, CST_Sub_Status__c, CST_End2End_Owner__c, Reason, OwnerId, CST_Case_is_with_Team__c
			FROM Case
			WHERE id = :caseId
		];
		System.debug('remedyCase -> ' + remedyCase);
		System.debug('remedy status -> ' + remedy.CST_Status__c);

		//do some logic here for status In Progress
		if (remedy.CST_Status__c == 'In Progress') {
			System.debug('remedy status In Progress' + remedy.CST_Status__c);
			remedyCase.Status = 'In Progress';
			remedyCase.CST_Sub_Status__c = 'Working on it';
			sendNewNotification(remedy, remedyCase);
		} else if (remedy.CST_Status__c == 'Pending') {
			// update on Case object
			remedyCase.Status = 'In Progress';
			remedyCase.CST_Sub_Status__c = 'Working on it';
			remedyCase.OwnerId = remedyCase.CST_End2End_Owner__c;
			remedyCase.CST_Case_is_with_Team__c = 'Internal - Advisor';
			remedyCase.Remedy_Ticket_No__c = remedy.CST_IncidentID__c;

			// update on External Ticket object
			remedy.CST_Status_Reason__c = 'Client Action Required';

			createNewTask(remedyCase, remedy);
		} else if (remedy.CST_Status__c == 'Resolved') {
			//update on Case object
			remedyCase.Status = 'Resolved';
			remedyCase.CST_Sub_Status__c = '';
			remedyCase.CST_Case_is_with_Team__c = '';
			remedyCase.OwnerId = remedyCase.CST_End2End_Owner__c;
		} else if (remedy.CST_Status__c == 'Cancelled') {
			//update on Case object
			remedyCase.Status = 'In Progress';
			remedyCase.OwnerId = remedyCase.CST_End2End_Owner__c;
			remedyCase.CST_Sub_Status__c = 'Working on it';
			remedyCase.CST_Case_is_with_Team__c = 'Internal - Advisor';

			createNewTask(remedyCase, remedy);
		}
		update remedyCase;
		update remedy;
		return 'SUCCESS';
	}

	private static void sendNewNotification(CST_External_Ticket__c remedy, Case remedyCase) {
		CustomNotificationType notificationType = [
			SELECT Id, DeveloperName
			FROM CustomNotificationType
			WHERE DeveloperName = 'CST_Notification_External_Ticket'
		];
		System.debug('notification type' + notificationType);
		Messaging.CustomNotification notification = new Messaging.CustomNotification();

		notification.setTitle('Remedy Ticket updated to ' + ' ' + remedy.CST_Status__c);
		notification.setBody('Incident ID:' + ' ' + remedy.CST_IncidentID__c + '\n' + 'Case Number:' + ' ' + remedyCase.CaseNumber);

		notification.setNotificationTypeId(notificationType.Id);
		notification.setTargetId(remedyCase.Id);

		Set<String> addressee = new Set<String>();
		addressee.add(remedyCase.CST_End2End_Owner__c);
		notification.send(addressee);
	}

	private static void createNewTask(Case remedyCase, CST_External_Ticket__c remedyStatus) {
		recordType taskRecordType = [SELECT Id, DeveloperName FROM RecordType WHERE DeveloperName = 'CST_New_Task' LIMIT 1];

		Task newTask = new Task();

		newTask.Subject = 'Remedy Ticket updated to' + ' ' + remedyStatus.CST_Status__c;
		newTask.OwnerId = remedyCase.CST_End2End_Owner__c;
		newTask.WhatId = remedyCase.Id;
		newTask.Status = 'Open';
		newTask.Type = 'Other';
		newTask.RecordTypeId = taskRecordType.Id;
		newTask.IsReminderSet = true;
		newTask.ReminderDateTime = System.now();

		Date createdDate = System.today();
		System.debug('hoje ->' + createdDate);

		Integer holidaysCount = 0;
		BusinessHours bh = [SELECT ID, Name, IsDefault, IsActive FROM BusinessHours WHERE IsDefault = TRUE LIMIT 1];

		Integer nbDaysAdded = 0;
		Date dueDate;
		while (nbDaysAdded < 2) {
			createdDate = createdDate.addDays(1);
			Datetime now = Datetime.newInstance(createdDate.year(), createdDate.month(), createdDate.day(), 11, 0, 0);
			Boolean isHoliday = BusinessHours.isWithin(bh.Id, now);
			System.debug('now -> ' + now);
			System.debug('isHoliday -> ' + isHoliday);

			if (isHoliday) {
				nbDaysAdded++;
			}
			dueDate = createdDate;
			System.debug('duedate -> ' + dueDate);
			System.debug('nbDaysAdded -> ' + nbDaysAdded);
			System.debug('createdDate -> ' + createdDate);
		}

		newTask.ActivityDate = dueDate;

		insert newTask;
	}
}
