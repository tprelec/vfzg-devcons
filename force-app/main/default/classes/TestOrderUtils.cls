@isTest
public class TestOrderUtils {
	public static Order__c theOrder;
	public static User owner;
	public static Numberporting__c numPort;
	public static Contracted_Products__c cp;
	@testSetup
	public static void setupTestData() {
		TestUtils.createEMPCustomSettings();
		TestUtils.createNetProfitCustomSettings();

		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOrderValidationSite();
		Testutils.createOrderValidationNumberportingRow();
		TestUtils.createOrderValidationCompetitorAsset();
		TestUtils.createOrderValidationPhonebookRegistration();
		TestUtils.createOrderValidationHBO();
		// create the order validations
		TestUtils.createOrderValidations();

		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunityWithBan(
			acct,
			Test.getStandardPricebookId(),
			ban,
			owner
		);
		opp.Select_Journey__c = 'Sales';
		update opp;
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);

		OrderType__c ot = TestUtils.createOrderType();

		Contact claimerContact = TestUtils.createContact(acct);
		claimerContact.Userid__c = owner.Id;
		claimerContact.Email = 'a@b.com';
		update claimerContact;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, claimerContact.id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);
		Dealer_Information__c dealerInfo = TestUtils.createDealerInformation(
			claimerContact.id,
			'123321'
		);
		contr.Dealer_Information__c = dealerInfo.Id;
		contr.Unify_Framework_Id__c = '9634563A';
		contr.Signed_by_Customer__c = claimerContact.Id;
		update contr;

		Order__c order = new Order__c(
			Status__c = 'New',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			Billing_Arrangement__c = ba.id
		);
		insert order;
		TestUtils.createNetProfitInformation(order);

		TestUtils.autoCommit = false;
		VF_Product__c vfp = new VF_Product__c(
			Name = 'tst',
			Family__c = 'tst',
			Product_Line__c = 'fVodafone',
			ExternalID__c = 'VFP-02-1234567',
			ProductCode__c = 'DUMMYADDMOBILEPRODUCT',
			OrderType__c = ot.Id,
			AllowsGroupDataSharing__c = 'YES',
			Unify_Group_Level_Product_CatalogId__c = '3720595',
			Unify_Group_Level_Product_Code__c = 'GL_MASS_MARKET',
			Unify_Group_Lvl_Pricing_Element_Branch__c = 'Shared_Allowance',
			Unify_Group_Lvl_Billing_Offer_CatalogId__c = '8883473',
			Unify_Group_Level_Billing_Offer_Code__c = 'GL_AO_GROUP_DATA_3_BIZ_RED'
		);
		insert vfp;
		List<Product2> products = new List<Product2>();
		products.add(TestUtils.createProduct());
		products[0].VF_Product__c = vfp.Id;
		insert products;
		List<PricebookEntry> pbEntries = new List<PricebookEntry>();

		pbEntries.add(TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products[0]));

		insert pbEntries;

		Contracted_Products__c cp = new Contracted_Products__c(
			Quantity__c = 4,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Unify_Template_Id__c = '12345678A',
			Product__c = products[0].Id,
			VF_Contract__c = contr.Id,
			Order__c = order.Id,
			Site__c = site.Id
		);
		insert cp;
		Contracted_Products__c cp2 = new Contracted_Products__c(
			Quantity__c = 4,
			UnitPrice__c = 10,
			Arpu_Value__c = 10,
			Duration__c = 1,
			Unify_Template_Id__c = '12345678A',
			Product__c = products[0].Id,
			VF_Contract__c = contr.Id,
			Order__c = order.Id
		);
		insert cp2;
	}

	private static Order__c getOrder() {
		return [
			SELECT
				Id,
				Name,
				Account__c,
				Status__c,
				Remark__c,
				Remark_Information__c,
				CustomerReady__c,
				BillingReady__c,
				DeliveryReady__c,
				propositions__c,
				OrderReady__c,
				CTNReady__c,
				Mobile_Fixed__c,
				NumberPortingReady__c,
				PhonebookregistrationReady__c,
				VF_Contract__c,
				Record_Locked__c,
				Ready_for_Inside_Sales__c,
				VF_Contract__r.Opportunity__r.Escalation__c,
				EMP_Automated_Mobile_order__c
			FROM Order__c
			LIMIT 1
		];
	}

	private static VF_Contract__c getContract() {
		return [SELECT Id, Account__c FROM VF_Contract__c LIMIT 1];
	}
	private static Contracted_Products__c getContractedProduct(Id orderId) {
		return [
			SELECT Id, Order__c, Order__r.Propositions__c
			FROM Contracted_Products__c
			WHERE Order__c = :orderId
			LIMIT 1
		];
	}

	private static Account getAccount(Id accId) {
		return [SELECT Id FROM Account WHERE Id = :accId];
	}
	private static Site__c getSite() {
		return [SELECT Id FROM Site__c LIMIT 1];
	}

	@isTest
	public static void testOverloadMethodUpdateOrderStatus() {
		Order__c sObjOrder = getOrder();
		VF_Contract__c sObjContract = getContract();

		Test.startTest();
		try {
			//using try catch to capture ExMissingDataException
			OrderUtils.updateOrderStatus(new List<Order__c>(), null);
		} catch (Exception e) {
			System.assertEquals(
				'No order data provided.',
				e.getMessage(),
				'when there is no order'
			);
		}
		OrderUtils.updateOrderStatus(new List<Order__c>{ sObjOrder }, null);
		System.debug(LoggingLevel.INFO, '<---test-afterUpdate----> ' + getOrder());
		System.assertEquals('New', getOrder().Status__c, 'Order status should be New');
		OrderUtils.updateOrderStatus(sObjOrder, true);
		System.assertEquals('New', getOrder().Status__c, 'Order status should be New');
		PBX_Type__c pbx = new PBX_Type__c(
			Name = 'Onenet Virtual PBX',
			Vendor__c = 'Onenet',
			Product_Name__c = 'Virtual PBX'
		);
		insert pbx;
		Id rtPABX = GeneralUtils.recordTypeMap.get('Competitor_Asset__c').get('PABX');
		Competitor_Asset__c objCAsset = new Competitor_Asset__c();
		objCAsset.Account__c = sObjContract.account__c;
		objCAsset.RecordTypeId = rtPABX;
		objCAsset.PBX_type__c = pbx.Id;
		insert objCAsset;
		OrderWrapper ow = OrderUtils.retrieveOrderDetails(sObjOrder.Id, sObjOrder.VF_Contract__c);
		ow.SiteMap = OrderUtils.retrieveOrderIdToLocationIdToLocation(
				new Set<Id>{},
				new Set<Id>{ ow.theOrder.Id }
			)
			.get(ow.theOrder.Id);
		OrderUtils.updateOrderStatus(new List<Order__c>{ sObjOrder }, ow);
		ow.locations = ow.SiteMap.values();
		sObjOrder.CustomerReady__c = true;
		sObjOrder.BillingReady__c = true;
		sObjOrder.DeliveryReady__c = true;
		sObjOrder.OrderReady__c = true;
		sObjOrder.CTNReady__c = true;
		sObjOrder.PhonebookregistrationReady__c = true;
		update sObjOrder;
		OrderUtils.updateOrderStatus(sObjOrder, null, true);
		Test.stopTest();
	}

	@isTest
	public static void testRetrieveOrderDetails() {
		Order__c sObjOrder = getOrder();
		VF_Contract__c sObjContract = getContract();

		Test.startTest();
		OrderWrapper objOWrapperWithOrder = OrderUtils.retrieveOrderDetails(sObjOrder.Id, null);
		System.assertEquals(
			true,
			objOWrapperWithOrder != null,
			'Validate that with the OrderId get the wrapper'
		);
		OrderWrapper objOWrapperWithContract = OrderUtils.retrieveOrderDetails(
			null,
			sObjContract.Id
		);
		System.assertEquals(
			true,
			objOWrapperWithContract != null,
			'Validate that with the OrderId get the wrapper'
		);
		Test.stopTest();
	}

	@isTest
	public static void testGetOrderContacts() {
		VF_Contract__c sObjContract = getContract();

		Test.startTest();
		List<SelectOption> lstOptions = OrderUtils.getOrderContacts(
			sObjContract.Account__c,
			sObjContract.Account__c,
			true
		);
		System.assertEquals(
			true,
			lstOptions.size() > 1,
			'meaning the option list contains the contact'
		);
		Test.stopTest();
	}

	@isTest
	public static void testBooleanMethods() {
		Order__c sObjOrder = getOrder();
		Contracted_Products__c sObjContractedProduct = getContractedProduct(sObjOrder.Id);

		Test.startTest();
		Boolean blnIsLocked = OrderUtils.getLocked(sObjOrder);
		System.assertEquals(false, blnIsLocked, 'Meaning the record is not locked');
		sObjOrder.Record_Locked__c = true;
		update sObjOrder;
		blnIsLocked = OrderUtils.getLocked(sObjOrder);
		System.assertEquals(true, blnIsLocked, 'Meaning the record is locked');
		sObjOrder.Record_Locked__c = false;
		sObjOrder.Ready_for_Inside_Sales__c = true;
		update sObjOrder;
		blnIsLocked = OrderUtils.getLocked(sObjOrder);
		System.assertEquals(true, blnIsLocked, 'Meaning the record is locked');
		Boolean blnIsAutomatedCP = OrderUtils.isAutomatedStandardProduct(sObjContractedProduct);
		System.assertEquals(false, blnIsAutomatedCP, 'Meaning the record is NOT automated');
		Test.stopTest();
	}

	@isTest
	public static void testPortingAndAccounts() {
		Order__c sObjOrder = getOrder();
		VF_Contract__c sObjContract = getContract();

		Test.startTest();
		Numberporting__c sobjNumPorting = OrderUtils.createPorting(sObjOrder);
		System.assertEquals(true, sobjNumPorting != null, 'Meaning the number porting was created');
		//delete old porting to retest creation
		delete sobjNumPorting;
		Numberporting__c np = new Numberporting__c();
		np.Name = 'Numberporting for ' + sObjOrder.Name;
		np.Customer__c = sObjContract.account__c;
		np.Order__c = sObjOrder.Id;
		insert np;
		Numberporting__c sobjNumPortingNew = OrderUtils.createPorting(sObjOrder);
		System.assertEquals(
			true,
			sobjNumPortingNew != null,
			'Meaning the number porting was queried'
		);
		Map<Id, List<Site__c>> mapIdSite = OrderUtils.retrieveAccountIdToSites(
			new Set<Id>{ sObjContract.Account__c }
		);
		System.assertEquals(
			true,
			!mapIdSite.get(sObjContract.Account__c).isEmpty(),
			'Meaning there are sites in the map'
		);
		Id rtPABX = GeneralUtils.recordTypeMap.get('Competitor_Asset__c').get('PABX');
		Competitor_Asset__c objCAsset = new Competitor_Asset__c();
		objCAsset.Account__c = sObjContract.account__c;
		objCAsset.RecordTypeId = rtPABX;
		insert objCAsset;
		List<Competitor_Asset__c> lstCAsset = OrderUtils.retrieveAccountIdToPBXs(
			sObjContract.account__c,
			null
		);
		System.assertEquals(true, !lstCAsset.isEmpty(), 'Meaning there are competitor Assets');
		Test.stopTest();
	}

	@isTest
	public static void testgetContractedProductsBy() {
		Order__c sObjOrder = getOrder();
		VF_Contract__c sObjContract = getContract();
		Contracted_Products__c sObjContractedProduct = getContractedProduct(sObjOrder.Id);

		Test.startTest();
		List<Contracted_Products__c> lstContractedProducts = OrderUtils.getContractedProductsByOrderId(
			sObjOrder.Id
		);
		System.assertEquals(
			true,
			!lstContractedProducts.isEmpty(),
			'Meaning the order has contracted products'
		);
		lstContractedProducts = OrderUtils.getContractedProductsByContractId(sObjContract.Id);
		System.assertEquals(
			true,
			!lstContractedProducts.isEmpty(),
			'Meaning the order has contracted products'
		);
		lstContractedProducts = OrderUtils.getContractedProductsByOrderId(null);
		System.assertEquals(true, lstContractedProducts == null, 'Meaning there is no Order');
		lstContractedProducts = OrderUtils.getContractedProductsByContractId(null);
		System.assertEquals(true, lstContractedProducts == null, 'Meaning theres is no contract');
		Map<Id, List<Contracted_Products__c>> mapIdContractedProducts = OrderUtils.retrieveOrderIdToAdditionalArticles(
			new Set<Id>{ sObjOrder.Id }
		);
		System.assertEquals(
			true,
			!mapIdContractedProducts.get(sObjOrder.Id).isEmpty(),
			'Meaning there are Contracted Products in the map'
		);
		Map<Id, List<Contracted_Products__c>> mapIdContractedProducts2 = OrderUtils.getOrderIdToContractedProducts(
			new Set<Id>{ sObjContractedProduct.Id }
		);
		System.assertEquals(
			true,
			!mapIdContractedProducts2.get(sObjOrder.Id).isEmpty(),
			'Meaning there are Contracted Products in the map'
		);
		Map<Id, List<Contracted_Products__c>> mapIdContractedProducts3 = OrderUtils.getOrderIdToContractedProducts(
			null
		);
		System.assertEquals(
			true,
			mapIdContractedProducts3.get(sObjOrder.Id) == null,
			'Meaning there are NO Contracted Products in the map'
		);
		Test.stopTest();
	}

	@isTest
	public static void testMethodToGetContracts() {
		VF_Contract__c sObjContract = getContract();

		Test.startTest();
		VF_Contract__c sObjContratQ = OrderUtils.getContractRecordById(sObjContract.Id);
		System.assertEquals(true, sObjContratQ != null, 'meaning the contract is returned');
		sObjContratQ = OrderUtils.getContractRecordById(null);
		System.assertEquals(true, sObjContratQ == null, 'meaning the contract is NOT returned');
		List<Order__c> lstOrder = OrderUtils.getOrderDataByContractId(sObjContract.Id, null);
		System.assertEquals(
			true,
			!lstOrder.isEmpty(),
			'Validate the order is queried from Contract'
		);
		Test.stopTest();
	}

	@isTest
	public static void testNonLegacyOrderUtils() {
		Test.startTest();
		createCompleteContract(null, false);
		OrderWrapper ow = OrderUtils.retrieveOrderDetails(theOrder.Id, theOrder.VF_Contract__c);
		ow.SiteMap = OrderUtils.retrieveOrderIdToLocationIdToLocation(
				new Set<Id>{},
				new Set<Id>{ ow.theOrder.Id }
			)
			.get(ow.theOrder.Id);
		OrderUtils.updateOrderStatus(new List<Order__c>{ theOrder }, ow);
		Boolean blnIsAutomatedCP = OrderUtils.isAutomatedStandardProduct(cp);
		System.assertEquals(true, blnIsAutomatedCP, 'Meaning the record is NOT automated');
		Test.stopTest();
	}

	public static void createCompleteContract(String bopOrderId, Boolean userPartnerAccount) {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OrderTriggerHandler', null, 0);
		owner = TestUtils.generateTestUser('admin', 'user', 'System Administrator');

		System.runAs(owner) {
			Profile p = [SELECT Id FROM Profile WHERE Name = 'VF Indirect Inside Sales' LIMIT 1];
			User is = TestUtils.generateTestUser('testing', 'is', p.Id, null);
			insert is;
			UserRole ur = [
				SELECT ID, Name
				FROM UserRole
				WHERE DeveloperName = 'Network_Planner'
				LIMIT 1
			];
			User networkPlanner = TestUtils.generateTestUser('Network', 'Planner', p.Id, ur.Id);
			insert networkPlanner;
			Account acct = userPartnerAccount
				? TestUtils.createPartnerAccount()
				: TestUtils.createAccount(owner);
			Contact generalContact = new Contact(
				AccountId = acct.Id,
				LastName = 'Current',
				FirstName = 'User',
				Phone = '06012345678',
				MobilePhone = '06012345678',
				Email = 'tstcontact@mail.com'
			);
			insert generalContact;
			Ban__c ban = createBan(acct);

			TestUtils.theOrderType = [
				SELECT Id, ExportSystem__c, Name, Status__c, ExternalId__c
				FROM OrderType__c
			];
			Opportunity opp = TestUtils.createOpportunityWithBan(
				acct,
				Test.getStandardPricebookId(),
				ban
			);
			Site__c site = TestUtils.createSite(acct);
			//disable auto commit for the TestUtils class so we can insert Lists instead of entries one-by-one
			TestUtils.autoCommit = false;
			List<String> productRoles = new List<String>{
				'Link (Internet)',
				'Link',
				'Carrier',
				'Numberporting',
				'Phone',
				'ERS',
				'Dark Fiber',
				'Router'
			};
			List<String> linkTypesProducts = new List<String>{
				'ADSL',
				'EOC',
				'FIBER',
				'SDSL',
				'VDSL',
				'WEAS',
				'WEAS',
				'ADSL'
			};

			List<Product2> products = new List<Product2>();
			for (Integer i = 0; i < productRoles.size(); i++) {
				Product2 prod = TestUtils.createProduct();
				prod.Role__c = productRoles[i];
				prod.Link_type__c = linkTypesProducts[i];
				prod.Quantity_type__c = 'Monthly';
				prod.Product_Group__c = 'Priceplan';
				if (productRoles[i] == 'Router') {
					prod.Brand__c = 'TestBrand';
					prod.Model__c = 'TestModel';
				}
				products.add(prod);
			}
			insert products;

			List<PricebookEntry> pbEntries = new List<PricebookEntry>();
			for (Integer i = 0; i < productRoles.size(); i++) {
				pbEntries.add(
					TestUtils.createPricebookEntry(Test.getStandardPricebookId(), products.get(i))
				);
			}
			insert pbEntries;
			Decimal totalAmount = 0.0;
			List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
			for (Integer i = 0; i < productRoles.size(); i++) {
				OpportunityLineItem oppLineItem = TestUtils.createOpportunityLineItem(
					opp,
					pbEntries.get(i)
				);
				oppLineItem.Site_List__c = site.Id + ',' + site.Id;
				oppLineItem.Quantity = 2;
				oppLineItems.add(oppLineItem);
				totalAmount +=
					oppLineItem.Product_Arpu_Value__c *
					oppLineItem.Duration__c *
					oppLineItem.Quantity;
			}

			Site_Availability__c sa = new Site_Availability__c(
				Site__c = site.Id,
				Access_Infrastructure__c = 'Coax',
				Bandwith_Down_Entry__c = 50,
				Bandwith_Down_Premium__c = 50,
				Bandwith_Up_Entry__c = 100,
				Bandwith_Up_Premium__c = 100,
				Existing_Infra__c = false,
				Vendor__c = 'ZIGGO',
				Premium_Vendor__c = 'ZIGGO',
				Result_Check__c = 'test',
				Region__c = 'Test'
			);
			insert sa;
			Site_Postal_Check__c spc = new Site_Postal_Check__c(
				Access_Site_ID__c = site.Id,
				Access_Active__c = false,
				Access_Vendor__c = 'ZIGGO',
				Access_Result_Check__c = 'OFFNET'
			);
			insert spc;

			User np = [
				SELECT Id, Name, UserRoleId, UserRole.DeveloperName
				FROM User
				WHERE FirstName = 'Network' AND LastName = 'Planner'
				LIMIT 1
			];
			HBO__c hbo = new HBO__c(
				hbo_account__c = acct.Id,
				hbo_opportunity__c = opp.Id,
				hbo_status__c = 'New',
				hbo_site__c = site.Id,
				hbo_network_planner__c = np.Id,
				hbo_delivery_time__c = 12,
				hbo_digging_distance__c = 100,
				hbo_total_costs__c = 500,
				hbo_redundancy_type__c = 'hbo',
				hbo_redundancy__c = false,
				hbo_result_type__c = 'OnNet',
				hbo_availability__c = 'Yes',
				hbo_postal_check__c = spc.Id
			);
			insert hbo;
			hbo.hbo_status__c = 'Approved';
			update hbo;

			oppLineItems[0].Site_Availability_List__c = sa.Id + ';' + sa.Id;

			insert oppLineItems;
			// create Contract
			VF_Contract__c c = new VF_Contract__c(
				Account__c = acct.Id,
				Opportunity__c = opp.id,
				Solution_Sales__c = is.Id
			);

			insert c;
			Integer quantity = 1;

			theOrder = new Order__c();
			theOrder.VF_Contract__c = c.Id;
			theOrder.OrderType__c = TestUtils.theOrderType.Id;
			theOrder.Account__c = c.Account__c;
			theOrder.Escalation_Level__c = 'high';
			theOrder.Export__c = 'SIAS';
			if (bopOrderId != null) {
				theOrder.BOP_Order_Id__c = bopOrderId;
			}
			theOrder.Status__c = 'Assigned to Inside Sales';
			theOrder.Inside_Sales_Owner__c = is.Id;
			theOrder.Project_Contact__c = generalContact.Id;
			theOrder.Infra_Contact__c = generalContact.Id;
			theOrder.CC_Contact__c = generalContact.Id;
			theOrder.Porting_Contact__c = generalContact.Id;
			theOrder.Propositions__c = 'Internet;IPVPN;One Net;One Fixed;Voice';
			insert theOrder;

			List<Contracted_Products__c> cpInsert = new List<Contracted_Products__c>();
			List<String> linkTypes = new List<String>{
				'EthernetOverFiber',
				'FTTH',
				'EthernetOverCopper',
				'VVDSL',
				'COAX'
			};
			cp = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[0].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENETX2014,ONENET2014,ONENET2014SUB30,One Net Enterprise,One Net Express',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp);
			Contracted_Products__c cp0 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[0].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENET2014',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp0);
			Contracted_Products__c cp01 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[0].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENET2014SUB30',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp01);
			Contracted_Products__c cp02 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[0].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENETX2014',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp02);
			Contracted_Products__c cp1 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[1].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Link_type__c = linkTypes[1],
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'IPVPN',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp1);
			// COAX != link type and product role carrier/dark fiber
			Contracted_Products__c cp2 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[3].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Net',
				Family_Condition__c = 'ONENET2014SUB30,One Net Express',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp2);
			// role = phone, order preposition not legacy
			Contracted_Products__c cp3 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[4].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Fixed'
			);
			cpInsert.add(cp3);
			// ERS
			Contracted_Products__c cp4 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[5].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Link_type__c = linkTypes[2],
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Proposition__c = 'One Fixed',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp4);
			// CPE - router with brand and model
			Contracted_Products__c cp5 = new Contracted_Products__c(
				Quantity__c = quantity,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = products[7].Id,
				VF_Contract__c = c.Id,
				Site__c = site.Id,
				Order__c = theOrder.Id,
				Link_type__c = linkTypes[3],
				Proposition_Component__c = 'Voice; Internet; IPVPN',
				Number_of_sessions__c = 2
			);
			cpInsert.add(cp5);

			for (Integer i = 0; i < linkTypes.size(); i++) {
				Contracted_Products__c cpi = new Contracted_Products__c(
					Quantity__c = quantity,
					UnitPrice__c = 10,
					Arpu_Value__c = 10,
					Duration__c = 1,
					Product__c = products[i].Id,
					VF_Contract__c = c.Id,
					Order__c = theOrder.Id,
					Proposition_Component__c = 'Voice; Internet; IPVPN',
					link_type__c = linkTypes[i]
				);
				cpInsert.add(cpi);
			}
			insert cpInsert;

			TestUtils.autoCommit = true;
			PBX_Type__c pbx = new PBX_Type__c(
				Name = 'Onenet Virtual PBX',
				Vendor__c = 'Onenet',
				Product_Name__c = 'Virtual PBX'
			);
			insert pbx;
			Competitor_Asset__c ca = new Competitor_Asset__c();
			ca = PBXSelectionWizardController.createAsset(site, ca, pbx.id);
			Competitor_Asset__c ca2 = new Competitor_Asset__c();
			ca2 = PBXSelectionWizardController.createAsset(site, ca2, pbx.id);
			insert ca;
			ca2.Overflow_PBX__c = ca.Id;
			insert ca2;

			numPort = TestUtils.createNumberPorting(theOrder);
			Numberporting_row__c npr = TestUtils.createNumberPortingRow(numPort, cpInsert[5]);
			npr.Location__c = site.Id;
			npr.PBX__c = ca2.Id;
			update npr;
			TestUtils.createPhonebookRegistration(numPort, 2);
		}
	}

	public static Ban__c createBan(Account a) {
		Ban__c b = new Ban__c(
			Account__c = a.Id,
			Unify_Customer_Type__c = 'C',
			Unify_Customer_SubType__c = 'A',
			Name = '388888889',
			Dealer_code__c = '123456'
		);
		insert b;
		return b;
	}
}