public with sharing class CS_ComProcessCreator {

    private List<SObject> objectList;
    //private Map<Id, Id> objectIdRecordTypeIdMap;
    //private List<CSPOFA.ProcessRequest> processRequests;

    public CS_ComProcessCreator(List<SObject> objects) {
        this.objectList = objects;
    }

    public void run() {
        Set<Id> objectIds = filterObjects(this.objectList);
        //this.objectIdRecordTypeIdMap = filterObjects(this.objectList);
        List<CSPOFA.ProcessRequest> processRequests = createProcessRequests(objectIds);
        logResult(createProcess(processRequests));        
    }
    
    private Map<Id, String> getOrderNameMap(Set<Id> orderIds) {
        Map<Id, String> orderNameMap = new Map<Id, String>();
        List<COM_Delivery_Order__c> listOfOrders = [SELECT Id, Name FROM COM_Delivery_Order__c WHERE id IN :orderIds];
        
        for(COM_Delivery_Order__c order : listOfOrders) {
            orderNameMap.put(order.Id, order.Name);
        }
        
        return orderNameMap;
    }

    private List<CSPOFA.ProcessRequest> createProcessRequests(Set<Id> objectIds) {
        List<CSPOFA.ProcessRequest> processRequests = new List<CSPOFA.ProcessRequest>();
        
        Map<Id, String> orderNameMap = getOrderNameMap(objectIds);
        
        for (Id objectId : objectIds) {
            String processTemplateName = getProcessTemplateName(objectId);
            Map<SObjectField, Id> objectFieldMap = getObjectFieldMap(objectId);

            String processName = orderNameMap.get(objectId);
            
            if(processName != null) {
                processRequests.add(new CSPOFA.ProcessRequest().templateName(processTemplateName).relationships(objectFieldMap).processName(processName));    
            } else {
                processRequests.add(new CSPOFA.ProcessRequest().templateName(processTemplateName).relationships(objectFieldMap));
            }
        }
        
        return processRequests;
    }

    private List<CSPOFA.ApiResult> createProcess(List<CSPOFA.ProcessRequest> processRequests) {
        List<CSPOFA.ApiResult> results = CSPOFA.API_V1.processes.create(processRequests);
        return results;
    }

    private Map<SObjectField, Id> getObjectFieldMap(Id objectId) {
        String sobjectType = objectId.getSObjectType().getDescribe().getName();
        Map<SObjectField, Id> retValue;

        if (sobjectType == 'csord__Order__c') {
            retValue = new Map<SObjectField, Id>{
                CSPOFA__Orchestration_Process__c.Order__c => objectId
            };
        } else if(sobjectType == 'COM_Delivery_Order__c') {
            retValue = new Map<SObjectField, Id>{
                CSPOFA__Orchestration_Process__c.COM_Delivery_Order__c => objectId
            };
        } else if (sobjectType == 'VF_Contract__c') {
            retValue = new Map<SObjectField, Id>{
                CSPOFA__Orchestration_Process__c.Contract_VF__c => objectId
            };
        } else {
            retValue = new Map<SObjectField, Id>{};
        }

        return retValue;
    }

    private String getProcessTemplateName(Id objectId) {
        String retValue;
        String sobjectType = objectId.getSObjectType().getDescribe().getName();

        if (sobjectType == 'csord__Order__c' || sobjectType == 'COM_Delivery_Order__c') {
            retValue = COM_Constants.ORDER_TEMPLATE_NAME;
        } else if (sobjectType == 'VF_Contract__c') {
            retValue = COM_Constants.MOBILE_FLOW_TEMPLATE_NAME;
        } else {
            retValue = '';
        }

        return retValue;
    }

    private Set<Id> filterObjects(List<SObject> objects) {
        Set<Id> results = new Set<Id>();
        Set<Id> vfContractsWithExistingProcesses = new Set<Id>();
        Set<Id> vfContractIds = new Set<Id>();
        List<cspofa__Orchestration_Process__c> vfContractOrchestrationProcesses = new List<cspofa__Orchestration_Process__c>();

        for (SObject obj : objects) {
            String sobjectType = obj.Id.getSObjectType().getDescribe().getName();

            if (sobjectType == 'VF_Contract__c') {
                vfContractIds.add(obj.Id);
            }
        }

        if(!vfContractIds.isEmpty()){
            vfContractOrchestrationProcesses = [SELECT Id,
                                                        Contract_VF__c
                                                FROM cspofa__Orchestration_Process__c
                                                WHERE Contract_VF__c IN :vfContractIds
            ];
        }

        for (cspofa__Orchestration_Process__c orchProcess :vfContractOrchestrationProcesses) {
            vfContractsWithExistingProcesses.add(orchProcess.Contract_VF__c);
        }

        for (SObject obj : objects) {
            String sobjectType = obj.Id.getSObjectType().getDescribe().getName();

            if(sobjectType == 'csord__Service__c'
                || sobjectType == 'csord__Order__c'
                || sobjectType == 'COM_Delivery_Order__c'
                || (sobjectType == 'VF_Contract__c' && (vfContractOrchestrationProcesses.isEmpty() || !vfContractsWithExistingProcesses.contains(obj.Id)))
            ) {
                results.add(obj.Id);
            }
        }

        return results;
    }

    /*private Map<Id, Id> filterObjects(List<SObject> objects) {
        Map<Id, Id> results = new Map<Id, Id>();
        Set<Id> vfContractsWithExistingProcesses = new Set<Id>();
        Set<Id> vfContractIds = new Set<Id>();
        List<cspofa__Orchestration_Process__c> vfContractOrchestrationProcesses = new List<cspofa__Orchestration_Process__c>();

        for (SObject obj : objects) {
            String sobjectType = obj.Id.getSObjectType().getDescribe().getName();

            if (sobjectType == 'VF_Contract__c') {
                vfContractIds.add(obj.Id);
            }
        }

        if(!vfContractIds.isEmpty()){
            vfContractOrchestrationProcesses = [SELECT Id,
                                                        Contract_VF__c
                                                FROM cspofa__Orchestration_Process__c
                                                WHERE Contract_VF__c IN :vfContractIds
            ];
        }

        for (cspofa__Orchestration_Process__c orchProcess :vfContractOrchestrationProcesses) {
            vfContractsWithExistingProcesses.add(orchProcess.Contract_VF__c);
        }

        for (SObject obj : objects) {
            String sobjectType = obj.Id.getSObjectType().getDescribe().getName();

            if (sobjectType == 'csord__Service__c') {
                csord__Service__c ser = (csord__Service__c) obj;
            } else if (sobjectType == 'csord__Order__c') {
                csord__Order__c ord = (csord__Order__c) obj;
                results.put(ord.Id, ord.RecordTypeId);
            } else if (sobjectType == 'VF_Contract__c') {
                VF_Contract__c vfContract = (VF_Contract__c) obj;
                if(vfContractOrchestrationProcesses.isEmpty() || !vfContractsWithExistingProcesses.contains(vfContract.Id)){
                    results.put(vfContract.Id, vfContract.RecordTypeId);
                }
            }
        }

        return results;
    }*/

    private void logResult(List<CSPOFA.ApiResult> results) {
        for (CSPOFA.ApiResult result : results) {
            if (result.isSuccess()) {
                String resourceId = result.getId();
            } else {
                List<CSPOFA.ApiError> errors = result.getErrors();
            }
        }
    }
}