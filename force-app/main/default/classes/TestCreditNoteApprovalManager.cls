/**
 * @description:    Test class for CreditNoteApprovalManager
 **/
@isTest
public class TestCreditNoteApprovalManager {
	private static final String TEST_FAKE_USER_ID = TestUtils.getFakeId(User.SObjectType);
	private static final String TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_INDIRECT = Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT;
	private static final String TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_COULANCE = Constants.CREDIT_NOTE_CREDIT_TYPE_COULANCE;
	private static final String TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE = Constants.CREDIT_NOTE_SUBSCRIPTION_MOBILE;
	private static final String TEST_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE;
	private static final String TEST_MATRIX_QUEUE_DEVELOPER_NAME = 'TestCreditNoteApprovalManagerQueue';
	private static final Integer TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT = 1000;

	private static final Integer CREDIT_NOTE_APPROVAL_LEVEL_0 = 0;
	private static final Integer CREDIT_NOTE_APPROVAL_LEVEL_1 = 1;
	private static final Integer CREDIT_NOTE_APPROVAL_LEVEL_2 = 2;
	private static final Integer CREDIT_NOTE_APPROVAL_LEVEL_3 = 3;

	private static final Integer NUMBER_OF_MATRICES_TO_CREATE_1 = 1;

	private static final String TEST_CREDIT_NOTE_APPROVAL_RECORD_NAME = 'TestCreditNoteApprovalManagerCNA';

	/**
	 * @description:    Tests filtering of only non FZiggo record type
	 * 					credit notes on before insert
	 **/
	@isTest
	static void shouldFilterOnlyNonFZiggoRecordTypeCreditNotesOnBeforeInsert() {
		// prepare data
		List<Credit_Note__c> creditNotes = new List<Credit_Note__c>();
		creditNotes.add( //satisfies all conditions
			new Credit_Note__c(
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				)
			)
		);
		creditNotes.add( //wrong record type
			new Credit_Note__c(
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_FZIGGO
				)
			)
		);

		// perform testing
		Test.startTest();
		List<Credit_Note__c> filteredCreditNotes = new CreditNoteApprovalManager().filterRecordsBeforeInsert(creditNotes);
		Test.stopTest();

		// verify results
		System.assertEquals(1, filteredCreditNotes.size(), 'Proper number of credit notes should be returned');
		System.assertEquals(creditNotes[0].Id, filteredCreditNotes[0].Id, 'Proper credit note should be returned');
	}

	/**
	 * @description:    Tests filtering of only non FZiggo record type
	 * 					credit notes who satisfy all conditions on before update
	 **/
	@isTest
	static void shouldFilterOnlyNonFZiggoRecordTypeCreditNotesWhoSatisfyAllConditionsOnBeforeUpdate() {
		// prepare data
		List<Credit_Note__c> creditNotes = new List<Credit_Note__c>();
		Map<Id, Credit_Note__c> oldCreditNotes = new Map<Id, Credit_Note__c>();
		creditNotes.add( //satisfies all conditions
			new Credit_Note__c(
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				),
				Is_Matrix_Approval_Process__c = true,
				Is_Final_Approver__c = false,
				Status__c = Constants.CREDIT_NOTE_STATUS_APPROVED
			)
		);
		Credit_Note__c oldCreditNote = creditNotes[0].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNotes.put(creditNotes[0].Id, oldCreditNote);

		creditNotes.add(
			new Credit_Note__c( //wrong record type
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_FZIGGO
				),
				Is_Matrix_Approval_Process__c = true,
				Is_Final_Approver__c = false,
				Status__c = Constants.CREDIT_NOTE_STATUS_APPROVED
			)
		);
		oldCreditNote = creditNotes[1].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNotes.put(creditNotes[1].Id, oldCreditNote);

		creditNotes.add(
			new Credit_Note__c( //Is_Matrix_Approval_Process__c = false
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				),
				Is_Matrix_Approval_Process__c = false,
				Is_Final_Approver__c = false,
				Status__c = Constants.CREDIT_NOTE_STATUS_APPROVED
			)
		);
		oldCreditNote = creditNotes[2].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNotes.put(creditNotes[2].Id, oldCreditNote);

		creditNotes.add(
			new Credit_Note__c( //Is_Final_Approver__c = true
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				),
				Is_Matrix_Approval_Process__c = true,
				Is_Final_Approver__c = true,
				Status__c = Constants.CREDIT_NOTE_STATUS_APPROVED
			)
		);
		oldCreditNote = creditNotes[3].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNotes.put(creditNotes[3].Id, oldCreditNote);

		creditNotes.add(
			new Credit_Note__c( //wrong status
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				),
				Is_Matrix_Approval_Process__c = true,
				Is_Final_Approver__c = false,
				Status__c = Constants.CREDIT_NOTE_STATUS_DRAFT
			)
		);
		oldCreditNote = creditNotes[4].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNotes.put(creditNotes[4].Id, oldCreditNote);

		// perform testing
		Test.startTest();
		List<Credit_Note__c> filteredCreditNotes = new CreditNoteApprovalManager().filterRecordsBeforeUpdate(creditNotes, oldCreditNotes);
		Test.stopTest();

		// verify results
		System.assertEquals(1, filteredCreditNotes.size(), 'Proper number of credit notes should be returned');
		System.assertEquals(creditNotes[0].Id, filteredCreditNotes[0].Id, 'Proper credit note should be returned');
	}

	/**
	 * @description:    Tests filtering of only non FZiggo record type
	 * 					credit notes who satisfy all conditions on after update
	 **/
	@isTest
	static void shouldFilterOnlyNonFZiggoRecordTypeCreditNotesWhoSatisfyAllConditionsOnAfterUpdate() {
		// prepare data
		List<Credit_Note__c> creditNotes = new List<Credit_Note__c>();
		Map<Id, Credit_Note__c> oldCreditNotes = new Map<Id, Credit_Note__c>();
		creditNotes.add( //satisfies all conditions
			new Credit_Note__c(
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				),
				Is_Matrix_Approval_Process__c = true,
				Is_Final_Approver__c = false,
				Status__c = Constants.CREDIT_NOTE_STATUS_APPROVED
			)
		);
		Credit_Note__c oldCreditNote = creditNotes[0].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNotes.put(creditNotes[0].Id, oldCreditNote);

		creditNotes.add( //satisfies all conditions with previous Is_Final_Approver__c = false
			new Credit_Note__c(
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				),
				Is_Matrix_Approval_Process__c = true,
				Is_Final_Approver__c = true,
				Status__c = Constants.CREDIT_NOTE_STATUS_APPROVED
			)
		);
		oldCreditNote = creditNotes[1].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNote.Is_Final_Approver__c = false;
		oldCreditNotes.put(creditNotes[1].Id, oldCreditNote);

		creditNotes.add(
			new Credit_Note__c( //wrong record type
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_FZIGGO
				),
				Is_Matrix_Approval_Process__c = true,
				Is_Final_Approver__c = false,
				Status__c = Constants.CREDIT_NOTE_STATUS_APPROVED
			)
		);
		oldCreditNote = creditNotes[2].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNotes.put(creditNotes[2].Id, oldCreditNote);

		creditNotes.add(
			new Credit_Note__c( //Is_Matrix_Approval_Process__c = false
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				),
				Is_Matrix_Approval_Process__c = false,
				Is_Final_Approver__c = false,
				Status__c = Constants.CREDIT_NOTE_STATUS_APPROVED
			)
		);
		oldCreditNote = creditNotes[3].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNotes.put(creditNotes[3].Id, oldCreditNote);

		creditNotes.add(
			new Credit_Note__c( //Is_Final_Approver__c = true and previous Is_Final_Approver__c = true
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				),
				Is_Matrix_Approval_Process__c = true,
				Is_Final_Approver__c = true,
				Status__c = Constants.CREDIT_NOTE_STATUS_APPROVED
			)
		);
		oldCreditNote = creditNotes[4].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNote.Is_Final_Approver__c = true;
		oldCreditNotes.put(creditNotes[4].Id, oldCreditNote);

		creditNotes.add(
			new Credit_Note__c( //wrong status
				Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
				RecordTypeId = RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(
					Credit_Note__c.SObjectType,
					Constants.CREDIT_NOTE_RECORD_TYPE_DEVELOPER_NAME_CREDIT_NOTE_INDIRECT
				),
				Is_Matrix_Approval_Process__c = true,
				Is_Final_Approver__c = false,
				Status__c = Constants.CREDIT_NOTE_STATUS_DRAFT
			)
		);
		oldCreditNote = creditNotes[5].clone(true, true);
		oldCreditNote.Status__c = Constants.CREDIT_NOTE_STATUS_PENDING_APPROVAL;
		oldCreditNotes.put(creditNotes[5].Id, oldCreditNote);

		// perform testing
		Test.startTest();
		List<Credit_Note__c> filteredCreditNotes = new CreditNoteApprovalManager().filterRecordsAfterUpdate(creditNotes, oldCreditNotes);
		Test.stopTest();

		// verify results
		System.assertEquals(2, filteredCreditNotes.size(), 'Proper number of credit notes should be returned');
		System.assertEquals(creditNotes[0].Id, filteredCreditNotes[0].Id, 'Proper credit note 1 should be returned');
		System.assertEquals(creditNotes[1].Id, filteredCreditNotes[1].Id, 'Proper credit note 2 should be returned');
	}

	/**
	 * @description:    Tests populating current and next matrix values on before
	 * 					insert when both exist
	 **/
	@isTest
	static void shouldPopulateCurrentAndNextMatrixValuesOnBeforeInsertWhenBothExist() {
		// prepare data
		Credit_Note__c creditNote = getCreditNoteWithEmptyApprovalFields(
			RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(Credit_Note__c.SObjectType, TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_INDIRECT)
		);

		List<CreditNote_Approval_Matrix__c> creditNoteApprovalMatrices = new List<CreditNote_Approval_Matrix__c>();
		creditNoteApprovalMatrices.addAll(
			createTestCreditNoteApprovalMatrices(
				NUMBER_OF_MATRICES_TO_CREATE_1,
				Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER,
				CREDIT_NOTE_APPROVAL_LEVEL_1
			)
		);
		creditNoteApprovalMatrices.addAll(
			createTestCreditNoteApprovalMatrices(
				NUMBER_OF_MATRICES_TO_CREATE_1,
				Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE,
				CREDIT_NOTE_APPROVAL_LEVEL_2
			)
		);

		// perform testing
		Test.startTest();
		CreditNoteApprovalMatrixAccessor.initCreditNoteApprovalMatrixMapping(creditNoteApprovalMatrices);

		new CreditNoteApprovalManager().mapApprovalFieldsFromMatrixBeforeInsert(new List<Credit_Note__c>{ creditNote });
		Test.stopTest();

		// verify results
		System.assert(
			creditNote.Current_Approval_Assignment_Rules__c.contains(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER),
			'Credit note Current_Approval_Assignment_Rules__c should be properly populated'
		);
		System.assertEquals(TEST_FAKE_USER_ID, creditNote.Current_Approver_1__c, 'Credit note Current_Approver_1__c should be properly populated');
		System.assertEquals(true, creditNote.Is_Matrix_Approval_Process__c, 'Credit note Is_Matrix_Approval_Process__c should be properly populated');
		System.assertEquals(CREDIT_NOTE_APPROVAL_LEVEL_1, creditNote.Approval_Level__c, 'Credit note Approval_Level__c should be properly populated');

		System.assert(
			creditNote.Next_Approval_Assignment_Rules__c.contains(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE),
			'Credit note Next_Approval_Assignment_Rules__c should be properly populated'
		);
		System.assertEquals(
			TEST_MATRIX_QUEUE_DEVELOPER_NAME,
			creditNote.Next_Queue_Approver__c,
			'Credit note Next_Queue_Approver__c should be properly populated'
		);
		System.assertEquals(false, creditNote.Is_Final_Approver__c, 'Credit note Is_Final_Approver__c should be properly populated');
	}

	/**
	 * @description:    Tests populating current matrix values and marking
	 * 					record as final approval on before insert when only
	 * 					current level matrix exists
	 **/
	@isTest
	static void shouldPopulateCurrentMatrixValuesAndMarkRecordAsFinalApprovalOnBeforeInsertWhenOnlyCurrentLevelMatrixExists() {
		// prepare data
		Credit_Note__c creditNote = getCreditNoteWithEmptyApprovalFields(
			RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(Credit_Note__c.SObjectType, TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_INDIRECT)
		);

		List<CreditNote_Approval_Matrix__c> creditNoteApprovalMatrices = new List<CreditNote_Approval_Matrix__c>();
		creditNoteApprovalMatrices.addAll(
			createTestCreditNoteApprovalMatrices(
				NUMBER_OF_MATRICES_TO_CREATE_1,
				Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER,
				CREDIT_NOTE_APPROVAL_LEVEL_1
			)
		);

		// perform testing
		Test.startTest();
		CreditNoteApprovalMatrixAccessor.initCreditNoteApprovalMatrixMapping(creditNoteApprovalMatrices);

		new CreditNoteApprovalManager().mapApprovalFieldsFromMatrixBeforeInsert(new List<Credit_Note__c>{ creditNote });
		Test.stopTest();

		// verify results
		System.assert(
			creditNote.Current_Approval_Assignment_Rules__c.contains(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER),
			'Credit note Current_Approval_Assignment_Rules__c should be properly populated'
		);
		System.assertEquals(TEST_FAKE_USER_ID, creditNote.Current_Approver_1__c, 'Credit note Current_Approver_1__c should be properly populated');
		System.assertEquals(true, creditNote.Is_Matrix_Approval_Process__c, 'Credit note Is_Matrix_Approval_Process__c should be properly populated');
		System.assertEquals(CREDIT_NOTE_APPROVAL_LEVEL_1, creditNote.Approval_Level__c, 'Credit note Approval_Level__c should be properly populated');

		System.assertEquals(null, creditNote.Next_Approval_Assignment_Rules__c, 'Credit note Next_Approval_Assignment_Rules__c should be null');
		System.assertEquals(null, creditNote.Next_Queue_Approver__c, 'Credit note Next_Queue_Approver__c should be null');
		System.assertEquals(true, creditNote.Is_Final_Approver__c, 'Credit note Is_Final_Approver__c should be properly populated');
	}

	/**
	 * @description:    Tests copying next matrix approval values to current
	 * 					and populating new next values from matrix on before
	 * 					update when next values exist
	 **/
	@isTest
	static void shouldCopyNextMatrixApprovalValuesToCurrentAndPopulateNewNextValuesFromMatrixOnBeforeUpdateWhenNextValuesExist() {
		// prepare data
		Credit_Note__c creditNote = getCreditNoteWithEmptyApprovalFields(
			RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(Credit_Note__c.SObjectType, TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_INDIRECT)
		);
		creditNote.Approval_Level__c = CREDIT_NOTE_APPROVAL_LEVEL_1;
		creditNote.Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER;
		creditNote.Next_Approver_1__c = TEST_FAKE_USER_ID;

		List<CreditNote_Approval_Matrix__c> creditNoteApprovalMatrices = new List<CreditNote_Approval_Matrix__c>();
		creditNoteApprovalMatrices.addAll(
			createTestCreditNoteApprovalMatrices(
				NUMBER_OF_MATRICES_TO_CREATE_1,
				Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE,
				CREDIT_NOTE_APPROVAL_LEVEL_3
			)
		);

		// perform testing
		Test.startTest();
		CreditNoteApprovalMatrixAccessor.initCreditNoteApprovalMatrixMapping(creditNoteApprovalMatrices);

		new CreditNoteApprovalManager().mapApprovalFieldsFromMatrixBeforeUpdate(new List<Credit_Note__c>{ creditNote });
		Test.stopTest();

		// verify results
		System.assert(
			creditNote.Current_Approval_Assignment_Rules__c.contains(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER),
			'Credit note Current_Approval_Assignment_Rules__c should be properly populated'
		);
		System.assertEquals(TEST_FAKE_USER_ID, creditNote.Current_Approver_1__c, 'Credit note Current_Approver_1__c should be properly populated');
		System.assertEquals(CREDIT_NOTE_APPROVAL_LEVEL_2, creditNote.Approval_Level__c, 'Credit note Approval_Level__c should be properly populated');

		System.assert(
			creditNote.Next_Approval_Assignment_Rules__c.contains(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE),
			'Credit note Next_Approval_Assignment_Rules__c should be properly populated'
		);
		System.assertEquals(
			TEST_MATRIX_QUEUE_DEVELOPER_NAME,
			creditNote.Next_Queue_Approver__c,
			'Credit note Next_Queue_Approver__c should be properly populated'
		);
		System.assertEquals(false, creditNote.Is_Final_Approver__c, 'Credit note Is_Final_Approver__c should be properly populated');
	}

	/**
	 * @description:    Tests copying next matrix approval values to current
	 * 					and marking record as final approval on before
	 * 					update when next values do not exist
	 **/
	@isTest
	static void shouldCopyNextMatrixApprovalValuesToCurrentAndMarkRecordAsFinalApprovalBeforeUpdateWhenNextValuesDoNotExist() {
		// prepare data
		Credit_Note__c creditNote = getCreditNoteWithEmptyApprovalFields(
			RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(Credit_Note__c.SObjectType, TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_INDIRECT)
		);
		creditNote.Approval_Level__c = CREDIT_NOTE_APPROVAL_LEVEL_1;
		creditNote.Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER;
		creditNote.Next_Approver_1__c = TEST_FAKE_USER_ID;

		// perform testing
		Test.startTest();
		CreditNoteApprovalMatrixAccessor.initCreditNoteApprovalMatrixMapping(new List<CreditNote_Approval_Matrix__c>());

		new CreditNoteApprovalManager().mapApprovalFieldsFromMatrixBeforeUpdate(new List<Credit_Note__c>{ creditNote });
		Test.stopTest();

		// verify results
		System.assert(
			creditNote.Current_Approval_Assignment_Rules__c.contains(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER),
			'Credit note Current_Approval_Assignment_Rules__c should be properly populated'
		);
		System.assertEquals(TEST_FAKE_USER_ID, creditNote.Current_Approver_1__c, 'Credit note Current_Approver_1__c should be properly populated');
		System.assertEquals(CREDIT_NOTE_APPROVAL_LEVEL_2, creditNote.Approval_Level__c, 'Credit note Approval_Level__c should be properly populated');

		System.assertEquals(null, creditNote.Next_Approval_Assignment_Rules__c, 'Credit note Next_Approval_Assignment_Rules__c should be null');
		System.assertEquals(null, creditNote.Next_Queue_Approver__c, 'Credit note Next_Queue_Approver__c should be null');
		System.assertEquals(true, creditNote.Is_Final_Approver__c, 'Credit note Is_Final_Approver__c should be properly populated');
	}

	/**
	 * @description:    Tests not chaning current or next credit note
	 * 					approval field values if validation failed on
	 * 					before update
	 **/
	@isTest
	static void shouldNotChangeCurrentOrNextCreditNoteApprovalFieldValuesIfValidationFailedOnBeforeUpdate() {
		// prepare data
		Credit_Note__c creditNote = getCreditNoteWithEmptyApprovalFields(
			RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(Credit_Note__c.SObjectType, TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_INDIRECT)
		);
		creditNote.Approval_Level__c = CREDIT_NOTE_APPROVAL_LEVEL_1;
		creditNote.Next_Approval_Assignment_Rules__c = Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER;
		creditNote.Next_Approver_1__c = null;

		List<CreditNote_Approval_Matrix__c> creditNoteApprovalMatrices = new List<CreditNote_Approval_Matrix__c>();
		creditNoteApprovalMatrices.addAll(
			createTestCreditNoteApprovalMatrices(
				NUMBER_OF_MATRICES_TO_CREATE_1,
				Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE,
				CREDIT_NOTE_APPROVAL_LEVEL_3
			)
		);

		// perform testing
		Test.startTest();
		CreditNoteApprovalMatrixAccessor.initCreditNoteApprovalMatrixMapping(creditNoteApprovalMatrices);

		new CreditNoteApprovalManager().mapApprovalFieldsFromMatrixBeforeUpdate(new List<Credit_Note__c>{ creditNote });
		Test.stopTest();

		// verify results
		System.assertEquals(
			null,
			creditNote.Current_Approval_Assignment_Rules__c,
			'Credit note Current_Approval_Assignment_Rules__c should be properly populated'
		);
		System.assertEquals(null, creditNote.Current_Approver_1__c, 'Credit note Current_Approver_1__c should be properly populated');
		System.assertEquals(CREDIT_NOTE_APPROVAL_LEVEL_1, creditNote.Approval_Level__c, 'Credit note Approval_Level__c should be properly populated');

		System.assert(
			creditNote.Next_Approval_Assignment_Rules__c.contains(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER),
			'Credit note Next_Approval_Assignment_Rules__c should be properly populated'
		);
		System.assertEquals(null, creditNote.Next_Queue_Approver__c, 'Credit note Next_Queue_Approver__c should be properly populated');
		System.assertEquals(false, creditNote.Is_Final_Approver__c, 'Credit note Is_Final_Approver__c should be properly populated');
		System.assertEquals(1, creditNote.getErrors().size(), 'Credit note should contain error');
	}

	/**
	 * @description:    Tests submitting record for approval on after
	 * 					update if all fields are properly populated
	 **/
	@isTest
	static void shouldSubmitRecordForApprovalOnAfterUpdateIfAllFieldsAreProperlyPopulated() {
		// prepare data
		Credit_Note__c creditNote = getCreditNoteWithEmptyApprovalFields(
			RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(Credit_Note__c.SObjectType, TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_INDIRECT)
		);
		creditNote.Id = null;
		creditNote.Current_Approver_1__c = TestUtils.createAdministrator()?.Id;
		creditNote.Status__c = Constants.CREDIT_NOTE_STATUS_DRAFT;
		creditNote.Is_Matrix_Approval_Process__c = true;
		insert creditNote;

		// perform testing
		Test.startTest();
		new CreditNoteApprovalManager().reSubmitMatrixRecordsForApproval(new List<Credit_Note__c>{ creditNote });
		Test.stopTest();

		// verify results
		ProcessInstance objProcessInstance = [SELECT Id, TargetObjectId, CreatedDate FROM ProcessInstance WHERE TargetObjectId = :creditNote.Id];

		System.assertEquals(creditNote.Id, objProcessInstance.TargetObjectId, 'Approval process should be created for credit note');
	}

	/**
	 * @description:    Tests not submitting record for approval on after
	 * 					update if submission for approval failed
	 **/
	@isTest
	static void shouldNotSubmitRecordForApprovalOnAfterUpdateIfSubmissionForApprovalFailed() {
		// prepare data
		Credit_Note__c creditNote = getCreditNoteWithEmptyApprovalFields(
			RecordTypeAccessor.getRecordTypeIdForSObjectTypeAndDeveloperName(Credit_Note__c.SObjectType, TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_INDIRECT)
		);
		creditNote.Id = null;
		creditNote.Current_Approver_1__c = TestUtils.createAdministrator()?.Id;
		creditNote.Status__c = Constants.CREDIT_NOTE_STATUS_DRAFT;
		creditNote.Is_Matrix_Approval_Process__c = false; //causes no valid approval processes
		insert creditNote;

		Exception caughtException;

		// perform testing
		Test.startTest();
		try {
			new CreditNoteApprovalManager().reSubmitMatrixRecordsForApproval(new List<Credit_Note__c>{ creditNote });
		} catch (Exception ex) {
			caughtException = ex;
		}
		Test.stopTest();

		// verify results
		List<ProcessInstance> objProcessInstances = [
			SELECT Id, TargetObjectId, CreatedDate
			FROM ProcessInstance
			WHERE TargetObjectId = :creditNote.Id
		];

		System.assertEquals(0, objProcessInstances.size(), 'Approval process should not be created for credit note');

		System.assertNotEquals(null, caughtException, 'Exception should be thrown');
	}

	/**
	 * @description:    Retrieves credit note with empty approval fields
	 * @param 			recordTypeId - record type id
	 * @return:         Credit_Note__c - credit note with empty approval fields
	 **/
	private static Credit_Note__c getCreditNoteWithEmptyApprovalFields(Id recordTypeId) {
		return new Credit_Note__c(
			Id = TestUtils.getFakeId(Credit_Note__c.SObjectType),
			RecordTypeId = recordTypeId,
			Credit_Type__c = TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_COULANCE,
			Subscription__c = TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE,
			Credit_Amount__c = TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT - 1,
			Current_Approval_Assignment_Rules__c = null,
			Current_Approver_1__c = null,
			Current_Approver_2__c = null,
			Current_Approver_3__c = null,
			Current_Approver_Manual__c = null,
			Current_Queue_Approver__c = null,
			Approval_Level__c = CREDIT_NOTE_APPROVAL_LEVEL_0,
			Next_Approval_Assignment_Rules__c = null,
			Next_Approver_1__c = null,
			Next_Approver_2__c = null,
			Next_Approver_3__c = null,
			Next_Approver_Manual__c = null,
			Next_Queue_Approver__c = null,
			Approved__c = false,
			Is_Matrix_Approval_Process__c = false
		);
	}

	/**
	 * @description:    Creates test credit note approval matrices
	 * @param           numberOfMatricesToCreate - number of matrices to create
	 * @param           approvalAssignmentRule - approval assignment rule
	 * @param           level - level of the matrix
	 * @return          List<CreditNote_Approval_Matrix__c> - created credit note approval matrices
	 **/
	private static List<CreditNote_Approval_Matrix__c> createTestCreditNoteApprovalMatrices(
		Integer numberOfMatricesToCreate,
		String approvalAssignmentRule,
		Integer level
	) {
		List<CreditNote_Approval_Matrix__c> createdCreditNoteApprovalMatrices = new List<CreditNote_Approval_Matrix__c>();

		for (Integer i = 0; i < numberOfMatricesToCreate; i++) {
			createdCreditNoteApprovalMatrices.add(
				new CreditNote_Approval_Matrix__c(
					CreditNoteRecordType__c = TEST_MATRIX_CREDIT_NOTE_RECORD_TYPE_INDIRECT,
					CreditNoteCreditType__c = TEST_MATRIX_CREDIT_NOTE_CREDIT_TYPE_COULANCE,
					CreditNoteSubscription__c = TEST_MATRIX_CREDIT_NOTE_SUBSCRIPTION_MOBILE,
					AmountFrom__c = i * TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT,
					AmountTo__c = (i + 1) * TEST_MATRIX_AMOUNT_INCREMENT_AMOUNT,
					ApprovalAssignmentRule__c = approvalAssignmentRule,
					QueueApprover__c = approvalAssignmentRule.equalsIgnoreCase(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_QUEUE)
						? TEST_MATRIX_QUEUE_DEVELOPER_NAME
						: null,
					UserApprover__r = approvalAssignmentRule.equalsIgnoreCase(Constants.APPROVAL_MATRIX_APPROVAL_ASSIGNMENT_RULE_USER)
						? getCreditNoteApprovalRecord()
						: null,
					Level__c = level != null ? level : i + 1
				)
			);
		}

		return createdCreditNoteApprovalMatrices;
	}

	/**
	 * @description:    Retrieves credit note approvals record
	 * @return          CreditNote_Approvals__c - credit note approvals record
	 **/
	private static CreditNote_Approvals__c getCreditNoteApprovalRecord() {
		return new CreditNote_Approvals__c(Name = TEST_CREDIT_NOTE_APPROVAL_RECORD_NAME, User__c = TEST_FAKE_USER_ID);
	}
}
