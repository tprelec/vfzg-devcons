public with sharing class AddMobileProductsJourneyHandler implements IJourney {
	public Boolean checkVisibility(String accountId) {
		List<Account> account = [SELECT Id, Count_Active_Assets__c FROM Account WHERE Id = :accountId AND Count_Active_Assets__c > 0];

		return !account.isEmpty();
	}

	public Map<String, Object> handleClick(String accountId) {
		Map<String, Object> opportunityToReturn = new Map<String, Object>();
		Account account = [SELECT Id, Name FROM Account WHERE Id = :accountId];

		Opportunity opportunity = new Opportunity();
		opportunity.Name = account.Name + ' ACQ Deepsell ' + DateTime.now().format('yyyy-MM-dd');
		opportunity.StageName = 'Qualify';
		opportunity.CloseDate = Date.today();
		opportunity.AccountId = accountId;
		opportunity.Select_Journey__c = 'Add Mobile Product';
		opportunity.Journey_Steps__c = JSON.serialize(AddMobileProductsJourneyHandler.getInitialJourneySteps());
		insert opportunity;

		opportunityToReturn.put(opportunity.Id, opportunity);

		return opportunityToReturn;
	}

	public static JourneyStepWrapper getInitialJourneySteps() {
		List<JourneyStep> journeySteps = new List<JourneyStep>{
			new JourneyStep('pp_Journey_Step_Billing_Details', 'Billing Details', true),
			new JourneyStep('pp_Journey_Step_Add_Mobile_Products', 'Add Mobile Products', false),
			new JourneyStep('pp_Journey_Step_CTN_Details', 'CTN Details', false)
		};
		return new JourneyStepWrapper(journeySteps, false);
	}

	public class JourneyStepWrapper {
		private List<JourneyStep> journeySteps;
		private Boolean isFinished;

		public JourneyStepWrapper(List<journeyStep> journeySteps, Boolean isFinished) {
			this.journeySteps = journeySteps;
			this.isFinished = isFinished;
		}
	}

	public class JourneyStep {
		public String label { get; set; }
		public String value { get; set; }
		public Boolean isActive { get; set; }

		public JourneyStep(String label, String value, Boolean isActive) {
			if (Test.isRunningTest()) {
				this.label = label;
			} else {
				this.label = StringUtils.translateLabel(label, null).substringBetween('"', '"');
			}
			this.value = value;
			this.isActive = isActive;
		}
	}
}
