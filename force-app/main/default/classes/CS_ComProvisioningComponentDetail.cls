public with sharing class CS_ComProvisioningComponentDetail {
    public CS_ComProvisioningComponentDetail(){

    }

    @AuraEnabled
    public String name;

    @AuraEnabled
    public String value;

    @AuraEnabled
    public Boolean readOnly;

    @AuraEnabled
    public String serviceId;

    @AuraEnabled
    public String serviceSpecificationGuid;

    @AuraEnabled
    public String pattern;
}