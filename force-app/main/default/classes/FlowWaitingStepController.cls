public with sharing class FlowWaitingStepController {
	@AuraEnabled
	public static SObject getRecord(String recordId, String fieldName) {
		String sObjectName = ((Id) recordId).getSobjectType().getDescribe().getName();
		String query = 'SELECT ' + fieldName + ' FROM ' + sObjectName + ' WHERE Id =: recordId';
		List<SObject> recordList = Database.query(query);

		if (recordList.size() > 0) {
			return recordList[0];
		}
		return null;
	}

	@AuraEnabled
	//contracted products take some time to create so we need to check if they exist before redirecting to not show the error "No contracted products..."
	public static Boolean getContractedProducts(String recordId) {
		List<VF_Contract__c> lstContracts = [SELECT Id FROM VF_Contract__c WHERE Opportunity__c = :recordId];
		if (!lstContracts.isEmpty()) {
			List<Contracted_Products__c> lstContractedProducts = [SELECT Id FROM Contracted_Products__c WHERE VF_Contract__c = :lstContracts[0].Id];
			return !lstContractedProducts.isEmpty() ? true : false;
		}
		return false;
	}
}
