public with sharing class Com_InitiateInflightChange {
    @AuraEnabled(cacheable=true)
    public static LightningResponse getData(Id recordId) {
        try {
            InflightChangeData data = new InflightChangeData();

            data.columns = new List<DataTableColumn>{
                new DataTableColumn(
                    COM_Delivery_Order__c.Name.getDescribe().getLabel(),
                    'name'
                ),
                new DataTableColumn(
                    COM_Delivery_Order__c.Products__c.getDescribe().getLabel(),
                    'productName'
                )
            };

            for (COM_Delivery_Order__c objDeliveryOrder : [
                SELECT Id, Name, Status__c, PONR_Reached__c, Products__c
                FROM COM_Delivery_Order__c
                WHERE Order__c = :recordId AND Parent_Delivery_Order__c != NULL
            ]) {
                if (objDeliveryOrder.PONR_Reached__c) {
                    data.unavailableDeliveries.add(
                        new DeliveryOrder(objDeliveryOrder)
                    );
                } else {
                    data.availableDeliveries.add(
                        new DeliveryOrder(objDeliveryOrder)
                    );
                }
            }

            return new LightningResponse().setBody(data);
        } catch (Exception objEx) {
            return new LightningResponse().setError(objEx.getMessage());
        }
    }

    @AuraEnabled
    public static LightningResponse saveData(Id orderId, List<Id> selectedIds) {
        Savepoint savepoint = Database.setSavePoint();
        try {
            /*

            1. Order has not been set to the "Ready for Delivery" - there would be no delivery orders related to the Order
                a. all the services need to be marked with Applicable for Inflight Change = true, so that CPQ could handle them properly.
                b. initiate the inflight change process by utilising the cssmgnt.API_1.createInflightRequest(StringsolutionId)

            2. Order set to "Ready for Delivery"
                a. Delivery Orders need to be marked with On Hold = true
                b. related Delivery Order Main Process needs to be marked with On Hold = true
                c. related (parent and child level) Services need to be marked with Applicable for Inflight Change = true, set to false for others.
                d. clear the Inflight Change Applied flag on the related Commercial Order
                e. initiate the inflight change process by utilising the cssmgnt.API_1.createInflightRequest(StringsolutionId)

            */

            updateDeliveryOrdersToOnHold(selectedIds);

            // updateServicesWithApplicableForInflightChange();
            // clearInFlightChangeFlagOnCommercialOrder();
            // invokeInflightChange();

            return new LightningResponse()
                .setInfo('saveDataCalled with params: ' + selectedIds);
        } catch (DMLException objEx) {
            return setError(savepoint, objEx.getDmlMessage(0));
        } catch (Exception objEx) {
            return setError(savepoint, objEx.getMessage());
        }
    }

    private static void updateDeliveryOrdersToOnHold(List<Id> selectedIds) {
        if (!selectedIds.isEmpty()) {
            List<COM_Delivery_Order__c> deliveryOrders = new List<COM_Delivery_Order__c>();
            for (COM_Delivery_Order__c objDeliveryOrder : [
                SELECT
                    Id,
                    Name,
                    Status__c,
                    PONR_Reached__c,
                    Parent_Delivery_Order__c
                FROM COM_Delivery_Order__c
                WHERE Id IN :selectedIds
            ]) {
                deliveryOrders.add(
                    new COM_Delivery_Order__c(Id = objDeliveryOrder.Id)
                );
            }
            update deliveryOrders;
        }
    }

    private static String isOrderReadyForDelivery() {
        return '';
    }

    private static LightningResponse setError(
        Savepoint savepoint,
        String message
    ) {
        if (savepoint != null) {
            Database.rollback(savepoint);
        }
        return new LightningResponse().setError(message);
    }

    public class InflightChangeData {
        @AuraEnabled
        public List<DeliveryOrder> availableDeliveries { get; set; }
        @AuraEnabled
        public List<DeliveryOrder> unavailableDeliveries { get; set; }
        @AuraEnabled
        public List<DataTableColumn> columns { get; set; }

        public InflightChangeData() {
            availableDeliveries = new List<DeliveryOrder>();
            unavailableDeliveries = new List<DeliveryOrder>();
        }
    }

    public class DataTableColumn {
        @AuraEnabled
        public String label { get; set; }
        @AuraEnabled
        public String fieldName { get; set; }
        public DataTableColumn(String label, String fieldName) {
            this.label = label;
            this.fieldName = fieldName;
        }
    }

    public class DeliveryOrder {
        @AuraEnabled
        public String recordId { get; set; }
        @AuraEnabled
        public String name { get; set; }
        @AuraEnabled
        public String productName { get; set; }

        public DeliveryOrder(COM_Delivery_Order__c objDeliveryOrder) {
            this.recordId = objDeliveryOrder.Id;
            this.name = objDeliveryOrder.Name;
            this.productName = objDeliveryOrder.Products__c;
        }
    }
}