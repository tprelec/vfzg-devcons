global with sharing class CS_PBX_Lookup extends cscfga.ALookupSearch{
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionID,Id[] excludeIds, Integer pageOffset, Integer pageLimit){
           
        System.debug('**** searchFields: ' + JSON.serializePretty(searchFields));   
        system.debug('productDefinitionID: ' + productDefinitionID);
        system.debug('pageOffset: ' + pageOffset);
        system.debug('pageLimit: ' + pageLimit);
           
        final Integer SELECT_LIST_LOOKUP_PAGE_SIZE_LIMIT = pageLimit + 1; 
        Integer recordOffset = pageOffset * pageLimit;   
        
        Map<Id,cspmb__Price_Item__c> priceItems;
        List<Competitor_Asset__c> pbxList = new List<Competitor_Asset__c>();
        
        Id siteId = searchFields.get('Site Check SiteID');
  
        System.debug('Result+++ == '+siteId);
      
        
        pbxList = [SELECT Id, Name,LastModifiedDate, Site__c, Site__r.Name, PBX_type__r.Name From Competitor_Asset__c Where Site__c = :siteId AND RecordType.DeveloperName = 'PABX' ORDER BY LastModifiedDate DESC];
    
        return pbxList;   
    }


    public override String getRequiredAttributes(){
      return '["Site Check SiteID"]';
    }  
}