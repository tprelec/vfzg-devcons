public with sharing class CS_SubscriptionTriggerHandler extends TriggerHandler{
    public override void afterUpdate(){
        Map<Id,csord__Subscription__c> newMap = (Map<Id,csord__Subscription__c>)Trigger.newMap;
        Map<Id,csord__Subscription__c> oldMap = (Map<Id,csord__Subscription__c>)Trigger.oldMap;
        Set<Id> toUpdateAllSolutions = new Set<Id>();

        for(Id x :newMap.keySet()){
            if(newMap.get(x) == null || oldMap.get(x) == null || newMap.get(x).csord__Status__c == null || oldMap.get(x).csord__Status__c == null) {
                continue;
            }
            
            if ((newMap.get(x).csord__Status__c.equalsIgnoreCase('Implemented') && !oldMap.get(x).csord__Status__c.equalsIgnoreCase('Implemented')) ||
                    (newMap.get(x).csord__Status__c.equalsIgnoreCase('Deprovisioned') && !oldMap.get(x).csord__Status__c.equalsIgnoreCase('Deprovisioned'))) {
                toUpdateAllSolutions.add(newMap.get(x).Suborder__c);
            }
        }

        if(!toUpdateAllSolutions.isEmpty()){
            updateAllSolutionStatus(toUpdateAllSolutions);
        }
    }

    public void updateAllSolutionStatus(Set<Id> solutionIds){
        List<Suborder__c> solutions = [SELECT Id,
                                            (SELECT Id,
                                                    csord__Status__c
                                            FROM Subscriptions__r)
                                            FROM Suborder__c
                                            WHERE Id IN :solutionIds];

        List<Suborder__c> solutionsToUpdate = new List<Suborder__c>();

        for(Suborder__c sol :solutions){
            Boolean solutionImplemented = true;
            Boolean solutionDeprovisioned = true;

            for(csord__Subscription__c subs :sol.Subscriptions__r){
                if (!subs.csord__Status__c.equalsIgnoreCase('Implemented')) {
                    solutionImplemented = false;
                }

                if (!subs.csord__Status__c.equalsIgnoreCase('Deprovisioned')) {
                    solutionDeprovisioned = false;
                }
            }


            if (solutionImplemented) {
                sol.Status__c = 'Implemented';
            }

            if (solutionDeprovisioned) {
                sol.Status__c = 'Deprovisioned';
            }

            if(solutionImplemented || solutionDeprovisioned){
                solutionsToUpdate.add(sol);
            }
        }

        if(!solutionsToUpdate.isEmpty()){
            update solutionsToUpdate;
        }
    }
}