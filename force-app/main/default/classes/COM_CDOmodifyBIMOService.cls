public with sharing class COM_CDOmodifyBIMOService {
	/*
	@TestVisible
	public static final String COM_CDO_INTEGRATION_SETTING_NAME = 'modifyBIMOService';

	private static final String MOCKSYNCSUCCESS = '{ "id":"b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "href":"https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId":"COM", "priority":"1", "description":"Internet Modem Only Service Order", "category":"Internet Modem Only Service", "state":"acknowledged", "orderDate":"2022-02-09T10:37:15.97221Z", "@type":"standard", "orderItem":[ { "id":"1", "action":"add", "state":"acknowledged", "@type":"standard", "service":{ "id":"4ed3dece-d5bd-442f-a1f9-687c0a1c52221", "serviceCharacteristic":[ { "name":"sourcesystem", "valueType":"string", "value":{ "@type":"string", "sourcesystem":"SF" } }, { "name":"tenantId", "valueType":"string", "value":{ "@type":"string", "tenantId":"Some Tenant Id" } }, { "name":"accessL1PostalCode", "valueType":"string", "value":{ "@type":"string", "accessL1PostalCode":"123456" } }, { "name":"accessL1HouseNumber", "valueType":"integer", "value":{ "@type":"integer", "accessL1HouseNumber":"101" } }, { "name":"accessL1HouseNumberExt", "valueType":"string", "value":{ "@type":"string", "accessL1HouseNumberExt":"9" } }, { "name":"accessL1Article", "valueType":"string", "value":{ "@type":"string", "accessL1Article":"HFC" } }, { "name":"circuitL2Article", "valueType":"string", "value":{ "@type":"string", "circuitL2Article":"int_l2_zz1" } }, { "name":"circuitL2Quality", "valueType":"string", "value":{ "@type":"string", "circuitL2Quality":"entry" } }, { "name":"internetL3Article", "valueType":"string", "value":{ "@type":"string", "internetL3Article":"int_service_l3" } }, { "name":"ipv4BlockIPPool", "valueType":"string", "value":{ "@type":"string", "ipv4BlockIPPool":"/32" } } ] }, "serviceSpecification":{ "id":"bdab731e-d5de-4135-8849-5c724ea01041", "invariantID":"930d7161-0eb8-444f-8d11-5971812246d5", "version":"1.0.0", "name":"Internet Modem Only Service", "@type":"ServiceSpecification" } } ] }';
	private static final String MOCKSYNCFAILURE = '{ "code": 40020, "reason": "SERVICE_ORDER_CHARACTERISTICS_VALIDATION_ERROR", "message": "Request validation has failed due the following issue(s) - [ Input parameter \'accessL1Article\' violates constraint, Invalid value ]" }';

	public static final String COM_CDO_INTEGRATION_FAILED_STATUS = 'Failure';
	public static final String COM_CDO_INTEGRATION_COMPLETE_STATUS = 'Complete';

	public static final String MOCK_MODIFY_BIMO_SYNC_FAILED = 'COM_CDO_Modify_BIMO_Service_Sync_Resp_Failure';
	public static final String MOCK_MODIFY_BIMO_SYNC_SUCCESS = 'COM_CDO_Modify_BIMO_Service_Sync_Resp_Success';

	public static COM_MetadataInformation metadata;

	public static String modifyBIMOService(Id deliveryOrderId) {
		String result = '';
		result = COM_CDO_createBIMOService.generateBIMOService(deliveryOrderId, true);

		return result;
	}

	*/
}
