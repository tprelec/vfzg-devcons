public with sharing class SiteManagerController {

    public SiteManagerController(ApexPages.StandardSetController controller) {
        accountId = ApexPages.currentPage().getParameters().get('id');
        try {
            basketId = ApexPages.Currentpage().getParameters().get('basketId');
        } catch (Exception e) {}

        if (basketId != null) {
            // fetch accountid
            accountId = [select cscfga__opportunity__r.accountid from cscfga__Product_Basket__c where id = : basketId].cscfga__opportunity__r.accountid;
        }
        // fetch account
        acct = [Select Id, Name, KVK_number__c From Account Where Id = : accountId];

        orderBy = 'Location_Type__c'; // Default sort order
        SortAscending = false;

        fetchPaginationController();
        showCSVImportSections = false;

        urlValue = '/apex/SiteManager?id=' + accountId + '&basketId=' + basketId;
        /** check from community */
        if (PP_LightningController.isLightningCommunity()) {
            urlValue = '/partnerportal/SiteManager?id=' + accountId;
        }
    }

    public Id accountId {
        get;
        set;
    }
    public Id basketId {
        get;
        set;
    }
    public Account acct {
        get;
        set;
    }
    public List < SiteWrapper > siteList {
        get;
        set;
    }
    public Boolean showCSVImportSections {
        get;
        set;
    }
    public String searchPostalCode {
        get;
        set;
    }
    public String searchHouseNumber {
        get;
        set;
    }
    public id siteID {
        get;
        set;
    }
    public string urlValue {
        get;
        set;
    }
    public boolean showRemove;
    public list < Competitor_Asset__c > pbxList {
        get;
        set;
    }
    public ApexPages.StandardSetController paginationController {
        get;
        set;
    }

    // variables to control the sorting
    public boolean SortAscending {
        get;
        set;
    }
    public string orderBy {
        get;
        set;
    }
    public string orderByColumn {
        get;
        set;
    }

    //
    public string listView {
        get;
        set;
    }

    public void filterSites() {
        fetchPaginationController();
    }
    
    public void activateSites() {
        activateDeactivateSites(true);
    }

    public void deactivateSites() {
        activateDeactivateSites(false);
    }

    public void activateDeactivateSites(Boolean activeInactive) {
        
        List<Site__c> sites = new List<Site__c>();
        for (SiteWrapper siteWrap : siteList) {
            if (siteWrap.selected) sites.add(new Site__c(Id = siteWrap.theSite.Id, Active__c = activeInactive));
        }

        if (sites.isEmpty()) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You need to select a site first'));
        } else {
            update sites;
            fetchPaginationController();
        }
    }

    public class SiteWrapper {
        // Determine if the checkmark for PBX should be green or grey
        // If one or more PBX's have PBX_type__r.Office_Voice_approved__c = G.711 or G.729 the checkmark should be green, otherwise it is grey
        public string pbxCheckmark(List < Competitor_Asset__c > pbxList) {
            String image = 'checkmark_grey';
            if (pbxList != null) {
                for (Competitor_Asset__c pbx: pbxList) {
                    if (pbx.PBX_type__r.Office_Voice_approved__c != null) {
                        if (pbx.PBX_type__r.Office_Voice_approved__c.contains('G.711') || pbx.PBX_type__r.Office_Voice_approved__c.contains('G.729')) {
                            image = 'checkmark_green';
                            break;
                        }
                    }
                }
            }
            image = PageReference.forResource(image).getUrl();
            // Test Method will not find the static resource
            if (!Test.isRunningTest()) {
                image = image.subString(0, image.indexOf('?'));
            }
            return image;
        }
        public SiteWrapper(Id accountId) {
            theSite = new Site__c(Site_Account__c = accountId);
            selected = false;
        }
        public SiteWrapper(Site__c s, Boolean sel, List < Competitor_Asset__c > pbxList) {
            theSite = s;
            selected = sel;
            pbx = pbxList;
            checkmark_image = pbxCheckmark(pbxList);
            if (s.Postal_Code_Check_Status__c != null) {
                if (s.Postal_Code_Check_Status__c == 'Completed') {
                    status = 'completed';
                } else {
                    status = 'pending';
                }
            }
            infras = new List<Site_Postal_Check__c>();
            for (Site_Postal_Check__c spc : s.Site_Postal_Checks__r) {
                if (spc.Existing_infra__c) infras.add(spc);
            }
        }
        public Site__c theSite {
            get;
            set;
        }
        public Boolean selected {
            get;
            set;
        }
        public Boolean hasValidAddressInput {
            get;
            set;
        }
        public String status {
            get;
            set;
        }
        public List < SelectOption > searchResult {
            get;
            set;
        } // A list of matches returned from the search
        public string selectedAddress {
            get;
            set;
        } // The address selected by the user from the searchResult
        public string errorMessage {
            get;
            set;
        } // Used to display an error following an insert that fails
        public List < Competitor_Asset__c > pbx {
            get;
            set;
        } // List of PBX at this site
        public string checkmark_image {
            get;
            set;
        } // Either checkmark green or checkmark grey
        public List <Site_Postal_Check__c> infras {
            get;
            set;
        }
    }

    public void fetchPaginationController() {

        // Get the sites for this account
        string querystr = 'Select Id, Name, Active__c, Blocked_Checks__c, Last_dsl_check__c, Last_fiber_check__c, Postal_Code_Check_Status__c, Site_Postal_Code__c, Site_House_Number__c, Site_House_Number_Suffix__c, Site_City__c, isDSLCheckCurrent__c, isFiberCheckCurrent__c, Location_Type__c, (select Access_Vendor__c, Technology_Type__c, Access_Result_Check__c, Access_Max_Down_Speed__c, Access_Max_Up_Speed__c, Existing_Infra__c, CreatedDate from Site_Postal_Checks__r where Access_Active__c=true)';
        querystr += ' From Site__c';
        querystr += ' Where Site_Account__c = \'' + accountId + '\'';
        
        if (listView == 'Active') querystr += ' AND Active__c = true';
        if (listView == 'Inactive') querystr += ' AND Active__c = false';

        // Apply the current sort order
        querystr += ' ORDER BY ' + orderBy;
        if (SortAscending) {
            querystr += ' ASC';
        } else {
            querystr += ' DESC';
        }

        List < Site__c > accountSites = database.query(querystr);

        // By using a standardsetcontroller here we can limit the page size so that we don't hit the viewstate limit
        paginationController = new ApexPages.StandardSetController(accountSites);
        paginationController.setPageSize(20);
        SiteList = fetchPage((List < Site__c > ) paginationController.getRecords());
    }

    public List < SiteWrapper > fetchPage(List < Site__c > accountSites) {
        List < SiteWrapper > tempSiteList = new List < SiteWrapper > ();
        sitesInProgress = false;

        // Get all the PBX for all of the Sites
        List < Competitor_Asset__c > allPBXlist = [select id,
                Site__c,
                PBX_type__c,
                PBX_type__r.Office_Voice_approved__c,
                Contract_Expiration_date__c,
                Service_provider__c,
                Manufacturer__c,
                Invoice_value_per_month__c
            from Competitor_Asset__c
            where Site__c in : accountSites
            AND RecordType.DeveloperName = 'PABX'
        ];

        // Map of Site ID to List of PBX for that Site
        Map < ID, List < Competitor_Asset__c >> siteToPBXmap = new Map < ID, List < Competitor_Asset__c >> ();
        for (Competitor_Asset__c pbx: allPBXlist) {
            // Get map entry for this site
            List < Competitor_Asset__c > pbxList = siteToPBXmap.get(pbx.Site__c);
            if (pbxList == null) {
                pbxList = new List < Competitor_Asset__c > ();
            }
            // Add this PBX to the list
            pbxList.add(pbx);
            // Put the list back into the map
            siteToPBXmap.put(pbx.site__c, pbxList);
        }

        for (Site__c s: accountSites) {
            if (s.Postal_Code_Check_Status__c == 'DSL check requested' || s.Postal_Code_Check_Status__c == 'Fiber check requested' || s.Postal_Code_Check_Status__c == 'Combined check requested' || s.Postal_Code_Check_Status__c == 'Requested') {
                sitesInProgress = true;
            }
            tempSiteList.add(new SiteWrapper(s, false, siteToPBXmap.get(s.id)));
        }
        return tempSiteList;
    }

    public void previous() {
        // get and save to avoid a viewstate change error
        paginationController.getRecord();
        paginationController.save();

        paginationController.previous();
        SiteList = fetchPage((List < Site__c > ) paginationController.getRecords());
    }
    public void next() {
        // get and save to avoid a viewstate change error
        paginationController.getRecord();
        paginationController.save();

        paginationController.next();
        SiteList = fetchPage((List < Site__c > ) paginationController.getRecords());
    }

    // This refreshes the data and stays on the same page
    public void refresh() {
        // Store the page number we are currently on
        Integer p = paginationController.getPageNumber();
        // requery the database
        fetchPaginationController();
        // set the page back to the page we were on
        paginationController.setpageNumber(p);
        // Load the correct data into the page
        SiteList = fetchPage((List < Site__c > ) paginationController.getRecords());
    }

    // Reverses the sort order if the column is the same
    // If a new column has been selected sort ascending
    public pageReference sort() {
        if (orderBy == orderByColumn) {
            SortAscending = !SortAscending;
        } else {
            SortAscending = true;
            orderBy = orderByColumn;
        }
        fetchPaginationController();
        return null;
    }

    public Boolean sitesInProgress {
        get;
        set;
    }

    public pageReference backToAccount() {
        return new pageReference('/' + accountId);
    }
    public pageReference backToBasket() {
        return new PageReference('/' + basketId);
    }
    public void goToImport() {
        showCSVImportSections = true;
    }

    // If any site row has not been saved display the Remove button on the page so that these rows can be removed
    public boolean getshowRemove() {
        for (SiteWrapper site: siteList) {
            if (site.theSite.id == null) {
                showRemove = true;
                return showRemove;
            }
        }
        showRemove = false;
        return showRemove;
    }

    // Method to search all sites that have been selected and have not yet been saved
    // Can only perform 100 callouts per this method so only 100 rows can be selected
    public void bulkSearch() {
        Boolean valid = false;
        OlbicoServiceJSON jService = new OlbicoServiceJSON();

        // Check that less than  100 sites have been selected to search/validate
        //if (siteList.size()>100) {
        //  ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Only 100 sites can be validated at a time.'));
        //  return;
        //}

        // Loop on selected sites in SiteWrapper
        Integer count = 0;
        for (SiteWrapper site: siteList) {
            if (site.selected && site.theSite.id == null) {
                count++;
                valid = true;
                site.selected = false;
                site.errorMessage = null;
                searchPostalCode = site.theSite.Site_Postal_Code__c;
                searchHouseNumber = String.valueof(site.theSite.Site_House_Number__c);

                string Zipcode = searchPostalCode + searchHouseNumber;
                Zipcode = Zipcode.deleteWhitespace().toUpperCase();

                System.Debug('GJN Zipcode:' + Zipcode);

                jService.setRequestRestJsonStreet(Zipcode);
                jService.makeRequestRestJsonStreet(Zipcode);

                // Put the search result onto the sitewrapper
                // If a house number suffix was provided filter the returned list
                if (site.theSite.Site_House_Number_Suffix__c != null) {
                    site.searchResult = new List < SelectOption > ();
                    for (selectOption option: jService.AdresInfo()) {
                        String[] field = option.getvalue().split('\\|');
                        System.Debug('2:' + field[2]);
                        System.Debug(site.theSite.Site_House_Number_Suffix__c.toUpperCase());
                        if (field[2].trim() == site.theSite.Site_House_Number_Suffix__c.toUpperCase().trim()) {
                            site.searchResult.add(option);
                        }
                    }
                } else {
                    // otherwise put the whole list into the search result
                    site.searchResult = jService.AdresInfo();
                }

                // Remove any whitespace (causes error in VF page)
                for (selectOption option: site.searchResult) {
                    option.setValue(option.getValue().replaceAll('(\\s+)', ' '));
                }
                if (jService.addressFound) {
                    site.hasValidAddressInput = true;
                } else {
                    site.hasValidAddressInput = false;
                    site.searchResult = new List < SelectOption > ();
                    if (jService.errorMessage == null) {
                        site.searchResult.add(new SelectOption(LABEL.ERROR_No_Exact_Address_Found, LABEL.ERROR_No_Exact_Address_Found));
                    } else {
                        // Most likely error from more than 120 requests in a minute. Keep the row selected so the user can try again
                        site.searchResult.add(new SelectOption(jService.errorMessage, jService.errorMessage));
                        site.selected = true;
                    }
                }
            }
            if (count == 100) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, LABEL.LABEL_Warning_100_Sites));
                break;
            }
        }
        if (!valid) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, LABEL.ERROR_Select_Site));
        }
    }

    public void bulkSave() {

        Boolean valid = false;
        List < Site__c > sitesToInsert = new List < Site__c > ();
        List < SiteWrapper > wrappersToProcess = new List < SiteWrapper > ();

        // Loop on selected sites in SiteWrapper
        for (SiteWrapper site: siteList) {
            if (site.selected && site.theSite.id == null && site.selectedAddress != null && site.selectedAddress != LABEL.ERROR_No_Exact_Address_Found) {
                wrappersToProcess.add(site);
                valid = true;

                integer i = 0;

                string street;
                integer houseNumber;
                string houseLetter;
                String houseNumberSuffix;
                string postcode;
                string city;
                string selectedAddress;

                selectedAddress = site.selectedAddress;
                String[] arrTest = selectedAddress.split('\\|');

                for (String sValue: arrTest) {
                    if (i == 0) {
                        street = sValue;
                    }
                    if (i == 1) {
                        houseNumber = Integer.valueof(sValue.Trim());
                    }
                    if (i == 2) {
                        houseLetter = sValue;
                    }
                    if (i == 3) {
                        houseNumberSuffix = sValue;
                    }
                    if (i == 4) {
                        postcode = sValue;
                    }
                    if (i == 5) {
                        city = sValue;
                    }
                    if (i == 6) {
                        city = city + ' - ' + sValue;
                    }
                    i++;
                }

                Site__c mySite = new Site__c();
                mySite.Site_Account__c = accountId;
                mySite.KVK_Number__c = acct.KVK_number__c;
                mySite.Site_Street__c = street;
                mySite.Site_House_Number__c = houseNumber;
                mySite.Site_House_Number_Suffix__c = houseLetter + ' ' + houseNumberSuffix;
                mySite.Site_Postal_Code__c = postcode;
                mySite.Site_City__c = city;
                mySite.Olbico__c = true;
                mySite.Name = 'Calculating....';

                // Set the phone number from the wrapper
                mySite.Site_Phone__c = site.theSite.Site_Phone__c;

                // Link the new site to the wrapper
                site.theSite = mySite;

                sitesToInsert.add(mySite);
            }
        }

        if (valid) {
            Set < ID > sitesToQuery = new Set < ID > ();
            MAP < ID, Site__c > newSiteMap;

            // Insert the new sites
            Database.SaveResult[] sr = Database.insert(sitesToInsert, false);

            for (Integer i = 0; i < sr.size(); i++) {
                if (sr[i].isSuccess()) {
                    sitesToQuery.add(sr[i].getId());
                    sitesToInsert[i].id = sr[i].getId();
                }
            }

            // Retrieve the new sites
            newSiteMap = new Map < ID, Site__c > ([Select Id,
                    Name,
                    Blocked_Checks__c,
                    Last_dsl_check__c,
                    Last_fiber_check__c,
                    Postal_Code_Check_Status__c,
                    Site_Postal_Code__c,
                    Site_House_Number__c,
                    Site_House_Number_Suffix__c,
                    Site_City__c,
                    isDSLCheckCurrent__c,
                    isFiberCheckCurrent__c,
                    Location_Type__c, (select Access_Vendor__c, Technology_Type__c, Access_Result_Check__c, Access_Max_Down_Speed__c, Access_Max_Up_Speed__c, Existing_Infra__c, CreatedDate from Site_Postal_Checks__r where Access_Active__c = true)
            From
                    Site__c
            Where
                    ID = : sitesToQuery
            ]);

            // Update the wrapper rows that have been processed
            for (SiteWrapper site: wrappersToProcess) {
                if (newSiteMap.get(site.theSite.id) != null) {
                    site.theSite = newSiteMap.get(site.theSite.id);
                    site.errorMessage = null;
                } else {
                    site.errorMessage = 'Duplicate';
                }
            }

            // Check the site wrapper to see if all rows have now been saved. If so activate the pagerefresh to get the results back
            sitesInProgress = true;
            for (SiteWrapper site: siteList) {
                if (site.theSite.id == null) {
                    sitesInProgress = false;
                    break;
                }
            }
        } else {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, LABEL.ERROR_Select_Site));
        }
    }

    public void remove() {
        Boolean valid = false;

        // Loop on selected sites in SiteWrapper
        //for (Integer i = 0; i < siteList.size(); i++) {
        //  SiteWrapper site = siteList[i];
        //  if (site.selected && site.theSite.id==null) {
        //    siteList.remove(i);
        //    valid = true;

        //  }
        //}

        Integer i = 0;
        while (i < siteList.size()) {
            SiteWrapper site = siteList[i];
            if (site.selected && site.theSite.id == null) {
                siteList.remove(i);
                valid = true;
            } else {
                i++;
            }
        }

        if (!valid) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, LABEL.ERROR_Select_Site));
        }
    }

    /*
     *  Description:  returns 0 if request is OK. Returns number of allowed checks if requested amount is too high
     */
    public Integer fiberChecksAllowed(Integer numberOfSitesRequested) {
        Integer counter = [
            SELECT count()
            FROM Site__c
            WHERE LastModifiedDate = LAST_N_DAYS: 1 AND Last_fiber_check__c = LAST_N_DAYS: 1
        ];
        Integer dailyBatchChecksAllowed = Integer.valueOf(Sales_Settings__c.getInstance().Max_daily_postalcode_checks__c) - 50;
        Integer batchChecksAllowedToday = dailyBatchChecksAllowed - counter;

        system.debug(numberOfSitesRequested);
        system.debug(batchChecksAllowedToday);
        system.debug(dailyBatchChecksAllowed);
        if (numberOfSitesRequested > batchChecksAllowedToday) {
            return batchChecksAllowedToday;
        } else {
            return 0;
        }

    }

    public pageReference scheduleDsl() {

        System.Debug('scheduleDsl');

        List < Site__c > sitesToUpdate = new List < Site__c > ();

        for (SiteWrapper sw: siteList) {
            // Check is an unsaved site has been selected. Throw error is one has
            if (sw.selected && sw.theSite.id == null) {
                Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR, Label.ERROR_No_Checks_On_Unsaved_Sites));
                return null;
            }
            // Check if there are any unsaved sites. If so throw error because they will be lost if a check is executed.
            if (sw.theSite.id == null) {
                Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR, Label.ERROR_Save_Or_Remove_Unsaved_Sites));
                return null;
            }
            if (sw.theSite.Blocked_checks__c != null && sw.theSite.Blocked_checks__c.contains('dsl')) {
                continue; // DSL check is blocked for this Site so can be skipped
            }
            if (sw.selected && (sw.theSite.Postal_Code_Check_Status__c == null || sw.theSite.Postal_Code_Check_Status__c == 'Completed')) {
                sw.theSite.Postal_Code_Check_Status__c = 'DSL check requested';
                sw.status = 'pending';
                sitesToUpdate.add(sw.theSite);
            }
            sw.selected = false;

        }
        update sitesToUpdate;
        scheduleCheck(sitesToUpdate);

        sitesInProgress = true;
        return null;
    }

    public pageReference scheduleFiber() {
        List < Site__c > sitesToUpdate = new List < Site__c > ();
        List < SiteWrapper > sitesToFiberCheck = new List < SiteWrapper > ();

        // first count number of selected sites
        Integer sitesSelected = 0;
        for (SiteWrapper sw: siteList) {
            // Check is an unsaved site has been selected. Throw error is one has
            if (sw.selected && sw.theSite.id == null) {
                Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR, Label.ERROR_No_Checks_On_Unsaved_Sites));
                return null;
            }
            // Check if there are any unsaved sites. If so throw error because they will be lost if a check is executed.
            if (sw.theSite.id == null) {
                Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR, Label.ERROR_Save_Or_Remove_Unsaved_Sites));
                return null;
            }
            if (!sw.selected || (sw.theSite.Blocked_checks__c != null && sw.theSite.Blocked_checks__c.contains('fiber'))) {
                continue; // Fiber check is blocked for this Site so can be skipped (or it was not selected)
            }
            if (sw.theSite.Postal_Code_Check_Status__c == null || sw.theSite.Postal_Code_Check_Status__c == 'Completed') {
                sitesSelected++;
                sitesToFiberCheck.add(sw);
            }
        }

        Integer checkAllowed = fiberChecksAllowed(sitesSelected);
        if (checkAllowed == 0) {
            for (SiteWrapper sw: sitesToFiberCheck) {
                sw.theSite.Postal_Code_Check_Status__c = 'Fiber check requested';
                sw.status = 'pending';
                sitesToUpdate.add(sw.theSite);
                sw.selected = false;
            }
            update sitesToUpdate;
            scheduleCheck(sitesToUpdate);

            sitesInProgress = true;
        } else {
            Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR, 'Daily max reached for batch fiber checks. Only ' + checkAllowed + ' more checks allowed.'));
        }
        return null;
    }

    public pageReference scheduleBoth() {

        List < Site__c > sitesToUpdate = new List < Site__c > ();
        List < SiteWrapper > sitesToFiberCheck = new List < SiteWrapper > ();

        // first count number of selected sites
        Integer sitesSelected = 0;
        for (SiteWrapper sw: siteList) {
            // Check is an unsaved site has been selected. Throw error is one has
            if (sw.selected && sw.theSite.id == null) {
                Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR, Label.ERROR_No_Checks_On_Unsaved_Sites));
                return null;
            }
            // Check if there are any unsaved sites. If so throw error because they will be lost if a check is executed.
            if (sw.theSite.id == null) {
                Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR, Label.ERROR_Save_Or_Remove_Unsaved_Sites));
                return null;
            }
            if (!sw.selected || sw.theSite.Blocked_checks__c != null) {
                continue; // Check is blocked for this Site so can be skipped (or it was not selected)
            }
            if (sw.theSite.Postal_Code_Check_Status__c == null || sw.theSite.Postal_Code_Check_Status__c == 'Completed') {
                sitesSelected++;
                sitesToFiberCheck.add(sw);
            }
        }

        Integer checkAllowed = fiberChecksAllowed(sitesSelected);
        if (checkAllowed == 0) {

            for (SiteWrapper sw: sitesToFiberCheck) {
                sw.theSite.Postal_Code_Check_Status__c = 'Combined check requested';
                sw.status = 'pending';
                sitesToUpdate.add(sw.theSite);
                sw.selected = false;
            }
            update sitesToUpdate;
            scheduleCheck(sitesToUpdate);

            sitesInProgress = true;
        } else {
            Apexpages.addMessage(New Apexpages.Message(ApexPages.severity.ERROR, 'Daily max reached for batch fiber checks. Only ' + checkAllowed + ' more checks allowed.'));
        }
        return null;
    }

    public void scheduleCheck(List < Site__c > sitesToCheck) {

        // start a queue
        if (!sitesToCheck.isEmpty()) {
            system.debug(sitesToCheck.size());
            // then call the enqueue job with the list of lists
            System.enqueueJob(new QueueablePostalcodeCheck(sitesToCheck));
        }

    }

    // logic for the csv import

    public string csvName {
        get;
        set;
    }
    public Blob csvBody {
        get;
        set;
    }

    public String separator {
        get {
            if (separator == null) separator = ',';
            return separator;
        }
        set;
    }
    public List < SelectOption > getSeparators() {
        List < SelectOption > separators = new List < SelectOption > ();
        separators.add(new SelectOption(',', ','));
        separators.add(new SelectOption(';', ';'));
        return separators;
    }

    public void handleCSV() {

        List < Site__c > sitesToInsert = new List < Site__c > ();
        List < List < String >> parsedCSV = new List < List < String >> ();

        try {
            parsedCSV = StringUtils.parseCSV(csvBody.toString(), true, separator);
            for (list < string > line: parsedCSV) {

                Site__c s = new Site__c();
                s.Site_Account__c = accountId;

                s.Site_Postal_Code__c = line[0];
                s.Site_House_Number__c = Integer.valueOf(line[1]);
                s.Site_House_Number_Suffix__c = line[2];
                s.Site_Phone__c = line[3];

                sitesToInsert.add(s);
            }

        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'CSV parsing error: ' + ex));
            //return null;
        }

        for (Site__c s: sitesToInsert) {
            siteList.add(new SiteWrapper(s, false, pbxList));
        }
        showCSVImportSections = false;

    }

    public pageReference downloadExample() {
        PageReference pr = Page.SiteManagerImportExample;
        return pr;

    }

    public List < String > failedRows {
        get;
        set;
    }

    public pageReference addPBX() {
        PageReference pr = new PageReference('/apex/PBXSelectionWizard?id=' + siteID + '&retURL=' + urlValue);
        return pr;
    }

    public pageReference manageInfra() {
        PageReference pr = new PageReference('/apex/SiteExistingInfra?id=' + siteID + '&retURL=' + urlValue);
        return pr;
    }

    public pageReference manageInfras() {
        String siteIds = '';
        for (SiteWrapper sw : siteList) {
            if (sw.selected) siteIds += sw.theSite.Id + ',';
        }

        if (String.isBlank(siteIds)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You need to select a site first'));
            return null;
        } else {
            PageReference pr = new PageReference('/apex/SiteExistingInfra?ids=' + siteIds + '&retURL=' + urlValue);
            return pr;
        }
    }

    public pageReference addSite() {
        PageReference pr = new PageReference('/apex/SiteWizard?id=' + accountId + '&basketId=' + basketId + '&retURL=' + urlValue);
        return pr;
    }

}