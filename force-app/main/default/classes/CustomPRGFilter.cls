/**
 * Created by bojan.zunko on 15/06/2020.
 */

public  with sharing class CustomPRGFilter {

    public static List<cspmb__Pricing_Rule_Group__c> getPRGsForPublication(ID publicationID) {
        List<cspmb__Pricing_Rule_Group__c> returnList = new List<cspmb__Pricing_Rule_Group__c>();
        List<csb2c__B2C_Setting__mdt> prgSetting = [
                SELECT id,DeveloperName,csb2c__text_value__c
                FROM csb2c__B2C_Setting__mdt
                WHERE DeveloperName = 'PS_DefaultPRGCodes' LIMIT 1];

        //if any result exists
        if(prgSetting.size() > 0) {

            //split resulting CSV list
            List<String> prgCodes = prgSetting[0].csb2c__text_value__c.split(',');
            //query by result of split
            returnList = [
                    SELECT Id, Name, cspmb__pricing_rule_group_code__c
                    FROM cspmb__Pricing_Rule_Group__c
                    WHERE cspmb__pricing_rule_group_code__c IN :prgCodes];
        }
        return returnList;
    }

}