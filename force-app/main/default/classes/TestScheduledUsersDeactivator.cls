@isTest
private class TestScheduledUsersDeactivator {

	private static final Integer testInteger = 10;
	private static final String TEST_USER_EMAIL = 'testuser@vodafone.com';

	@TestSetup
	private static void setup() {
		TestUtils.createAdministrator();
		insert new Scheduled_Users_Deactivation_Settings__c(
			Freezing_Delay__c = testInteger,
			First_Email_Alert_Delay__c = testInteger / 2,
			Second_Email_Alert_Delay__c = testInteger / testInteger
		);
	}

	@isTest
	private static void freezeUsersTest() {
		Test.startTest();
			ScheduledUsersDeactivator deactivator = new ScheduledUsersDeactivator();
			deactivator.execute(null);
		Test.stopTest();

		Set<Id> testUsersIds = new Map<Id, User>([SELECT Id FROM User WHERE Email = :TEST_USER_EMAIL]).keySet();
		for (UserLogin userLoginForVerification: [SELECT Id, IsFrozen FROM UserLogin WHERE UserId IN :testUsersIds]) {
			System.assert(userLoginForVerification.IsFrozen, 'Test User is not frozen');
		}
	}

	@isTest
	private static void createEmailAlertByEmailTemplateIdTest() {
		User testUser = [SELECT Id FROM User WHERE Email = :TEST_USER_EMAIL LIMIT 1];
		EmailTemplate testEmailTemplate = [SELECT Id, DeveloperName, Body FROM EmailTemplate where DeveloperName = 'First_Scheduled_User_Freezing_Alert'];
		Messaging.SingleEmailMessage testEmail;

		Test.startTest();
			ScheduledUsersDeactivator deactivator = new ScheduledUsersDeactivator();
			testEmail = deactivator.createEmailAlertByEmailTemplateId(testEmailTemplate.Id, testUser);
		Test.stopTest();

		System.assertEquals(testEmail.getTargetObjectId(), testUser.Id, 'Test user was not set as email receiver');
	}
}