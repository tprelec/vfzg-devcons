/**
 * @description: This class is responsible for invoking the CreateBSSOrder REST service
 * @author: Jurgen van Westreenen
 */
@SuppressWarnings('PMD')
public class CreateBSSOrderRest {
	public static final String BILLING_STATUS_REQUESTED = 'Requested';
	public static final String BILLING_STATUS_REQUEST_FAILED = 'Failed';
	public static final Integer MAX_NR_OF_FAILED_REQUESTS = 3;
	@TestVisible
	private static final String INTEGRATION_SETTING_NAME = 'SIASRESTCreateBSSOrder';
	@TestVisible
	private static final String STATUS_OK = 'OK';
	@TestVisible
	private static final String MOCK_URL = 'http://example.com/CreateBSSOrderRest';

	public static CollectionWrapper createBSSOrder(String transactionId, List<Id> contProdIds) {
		CollectionWrapper collectionWrapper = new CollectionWrapper();

		try {
			// Retrieve the necessary product data based on passed Contracted Product Id
			List<Contracted_Products__c> prodList = [
				SELECT
					Id,
					Site__r.Assigned_Product_Id__c,
					Customer_Asset__c,
					Customer_Asset__r.Installed_Base_Id__c,
					/*Customer_Asset__r.Quantity__c,
                    Customer_Asset__r.Price__c,
                    Customer_Asset__r.PO_Number__c,*/
					Totalprice__c,
					Gross_List_Price__c,
					Billing_Arrangement__r.Unify_Ref_Id__c,
					Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c,
					Delivery_Date__c,
					Activation_Date__c,
					Start_Invoicing_Date__c,
					Is_Recurring__c,
					Unify_Charge_Description__c,
					Net_Unit_Price__c,
					Quantity__c,
					Discount__c,
					Discount_description__c,
					Product__r.ProductCode,
					Product__r.Unify_Charge_Code__c,
					Product__r.Taxonomy__r.Name,
					Product__r.Taxonomy__r.Product_Family__c,
					Product__r.Taxonomy__r.Portfolio__c,
					External_Reference_Id__c,
					Error_Info__c,
					Billing_Status__c,
					Number_of_Failed_Requests__c,
					Order__c,
					Order__r.Account__c /*
                    PO_Number__c,
                    Cost_Center__c,
                    OC_Charge__c,
                    OC_Charge__r.Installed_Base_Id__c,
                    OC_Charge__r.Quantity__c,
                    OC_Charge__r.Price__c,
                    OC_Charge__r.PO_Number__c,
                    Credit__c,
                    Row_Type__c,
                    Site__c,*/,
					Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.ExternalAccount__c
				FROM Contracted_Products__c
				WHERE Id IN :contProdIds
			];

			if (prodList.size() > 0) {
				HTTPResponse response = makeCallout(transactionId, prodList);
				System.debug('@@@@response: ' + response.getBody());

				Order_Billing_Transaction__c billingTransaction = createBillingTransaction(
					transactionId,
					prodList
				);

				// If the request is successful, parse the JSON response.
				if (response.getStatusCode() == 200) {
					// Deserializes the JSON string.
					CreateBSSOrderSuccessResponse result = (CreateBSSOrderSuccessResponse) JSON.deserializeStrict(
						response.getBody(),
						CreateBSSOrderSuccessResponse.class
					);
					// When successfull set the processing timestamp

					List<Contracted_Products__c> updContProds = new List<Contracted_Products__c>();
					for (Contracted_Products__c contProd : prodList) {
						contProd.Billing_Status__c = BILLING_STATUS_REQUESTED;
						contProd.Number_of_Failed_Requests__c = 0; // Reset counter
						contProd.Activation_Date__c = getActivationDate(); // set activation date only if activation was successful
						updContProds.add(contProd);
					}

					collectionWrapper.contractedProductsToUpdate = updContProds;
					// update updContProds;
				} else {
					// Otherwise write the error response to the Contracted Product
					List<Contracted_Products__c> updContProds = new List<Contracted_Products__c>();
					for (Contracted_Products__c contProd : prodList) {
						contProd.Number_of_Failed_Requests__c++; // Increase counter
						// Set status to 'Request Failed' after 3 failed attempts
						if (contProd.Number_of_Failed_Requests__c >= MAX_NR_OF_FAILED_REQUESTS) {
							contProd.Billing_Status__c = BILLING_STATUS_REQUEST_FAILED;
						}
						updContProds.add(contProd);
					}

					collectionWrapper.contractedProductsToUpdate = updContProds;
					billingTransaction.Error_Info__c = response.getBody();
				}

				collectionWrapper.billingTransactionToInsert = billingTransaction;
			}
		} catch (Exception e) {
			System.debug('@@@@inside exception: ' + e + ' -- ' + e.getStackTraceString());
			throw e;
			// TODO: need to check proper way to handle exception here
		} finally {
		}
		return collectionWrapper;
	}

	private static HTTPResponse makeCallout(
		String transactionId,
		List<Contracted_Products__c> prodList
	) {
		Map<Id, String> extSiteMap = createExtSiteMap(prodList);
		// Generate the request JSON message
		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		generator.writeFieldName('createBSSOrderRequest');
		generator.writeStartObject();
		generator.writeStringField('transactionID', transactionId);
		generator.writeFieldName('orderLinesList');
		generator.writeStartArray();
		for (Contracted_Products__c contProd : prodList) {
			generator.writeStartObject();
			// generator.writeStringField('SLSAPID', contProd.Site__r.Assigned_Product_Id__c != null ? string.valueOf(contProd.Site__r.Assigned_Product_Id__c) : '');
			// KEY RING
			generator.writeStringField(
				'SLSAPID',
				extSiteMap.get(contProd.Id) != null
					? string.valueOf(extSiteMap.get(contProd.Id))
					: ''
			);

			generator.writeStringField(
				'SFIBID',
				contProd.Customer_Asset__r.Installed_Base_Id__c != null
					? string.valueOf(contProd.Customer_Asset__r.Installed_Base_Id__c)
					: ''
			);
			generator.writeStringField(
				'BAID',
				contProd.Billing_Arrangement__r.Unify_Ref_Id__c != null
					? string.valueOf(contProd.Billing_Arrangement__r.Unify_Ref_Id__c)
					: ''
			);
			generator.writeStringField(
				'BCID',
				contProd.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c != null
					? string.valueOf(
							contProd.Billing_Arrangement__r.Financial_Account__r.Ban__r.Unify_Ref_Id__c
					  )
					: ''
			);
			generator.writeStringField('componentAPID', ''); // Not required
			generator.writeStringField('activityType', 'Provide'); // For now hardcoded

			// Rahul using proper new activation date field
			// generator.writeStringField('activationDate', contProd.Delivery_Date__c != null ? string.valueOf(contProd.Delivery_Date__c) : '');
			generator.writeStringField('activationDate', string.valueOf(getActivationDate()));
			generator.writeStringField(
				'invoicingStartDate',
				contProd.Start_Invoicing_Date__c != null
					? string.valueOf(contProd.Start_Invoicing_Date__c)
					: ''
			);
			generator.writeStringField('typeOfCharge', contProd.Is_Recurring__c ? 'RC' : 'OC');
			generator.writeStringField(
				'chargeDescription',
				contProd.Unify_Charge_Description__c != null
					? replaceSpecialCharacters(string.valueOf(contProd.Unify_Charge_Description__c))
					: ''
			);

			// Rahul fixed the charge amount
			generator.writeStringField(
				'chargeAmount',
				contProd.Gross_List_Price__c != null
					? string.valueOf(contProd.Gross_List_Price__c)
					: ''
			);
			// generator.writeStringField('chargeAmount', contProd.Net_Unit_Price__c != null ? string.valueOf(contProd.Net_Unit_Price__c) : '');

			// Rahul changed quantity to integer as per interface detail
			// generator.writeStringField('quantity', contProd.Quantity__c != null ? string.valueOf(contProd.Quantity__c) : '');
			generator.writeNumberField(
				'quantity',
				contProd.Quantity__c != null ? Integer.valueOf(contProd.Quantity__c) : 0
			);
			generator.writeStringField(
				'discountAmount',
				contProd.Discount__c != null ? string.valueOf(contProd.Discount__c) : ''
			);
			generator.writeStringField(
				'discountDescription',
				contProd.Discount_description__c != null
					? replaceSpecialCharacters(string.valueOf(contProd.Discount_description__c))
					: ''
			);
			generator.writeStringField('discountMethod', 'Percentage'); // For now hardcoded
			generator.writeStringField(
				'chargeCode',
				contProd.Product__r.Unify_Charge_Code__c != null
					? string.valueOf(contProd.Product__r.Unify_Charge_Code__c)
					: ''
			);
			if (String.isNotBlank(contProd.Product__c)) {
				generator.writeStringField(
					'BItag1',
					contProd.Product__r.ProductCode != null
						? string.valueOf(contProd.Product__r.ProductCode)
						: ''
				);
				generator.writeStringField(
					'BItag2',
					contProd.Product__r.Taxonomy__r.Name != null
						? string.valueOf(contProd.Product__r.Taxonomy__r.Name)
						: ''
				);
				generator.writeStringField(
					'BItag3',
					contProd.Product__r.Taxonomy__r.Product_Family__c != null
						? string.valueOf(contProd.Product__r.Taxonomy__r.Product_Family__c)
						: ''
				);
				generator.writeStringField(
					'BItag4',
					contProd.Product__r.Taxonomy__r.Portfolio__c != null
						? string.valueOf(contProd.Product__r.Taxonomy__r.Portfolio__c)
						: ''
				);
				//    generator.writeStringField('BItag5', 'BItag5');
			}
			generator.writeStringField('externalReferenceID', contProd.Id);
			generator.writeEndObject();
		}
		generator.writeEndArray();
		generator.writeEndObject();
		generator.writeEndObject();

		String requestBody = generator.getAsString();

		/*
        IWebServiceConfig webServiceConfig;
        if(!Test.isRunningTest()) {
            webServiceConfig = WebServiceConfigLocator.getConfig(INTEGRATION_SETTING_NAME);
        } else {
            webServiceConfig = WebServiceConfigLocator.createConfig();
            webServiceConfig.setEndpoint(MOCK_URL);
        }

        string endpointURL  = webServiceConfig.getEndpoint();

        if(String.isNotEmpty(webServiceConfig.getCertificateName())) {
            reqData.setClientCertificateName(webServiceConfig.getCertificateName());
        }
        */
		// Retrieve the credentials from the custom setting
		External_WebService_Config__c webServiceConfig = External_WebService_Config__c.getValues(
			INTEGRATION_SETTING_NAME
		);

		string endpointURL = webServiceConfig.URL__c;
		// String authorizationHeaderString = webServiceConfig.Authorization_Header__c;
		// string authorizationHeader = authorizationHeaderString; // 'Bearer ' + authorizationHeaderString;
		String authorizationHeaderString =
			webServiceConfig.Username__c +
			':' +
			webServiceConfig.Password__c;
		String authorizationHeader =
			'Bearer ' + EncodingUtil.base64Encode(Blob.valueOf(authorizationHeaderString));
		System.debug('authorizationHeader: ' + authorizationHeader);

		HttpRequest reqData = new HttpRequest();
		Http http = new Http();

		reqData.setHeader('Content-Type', 'application/json');
		reqData.setHeader('Connection', 'keep-alive');
		reqData.setHeader('Content-Length', '0');

		reqData.setHeader('Authorization', authorizationHeader);
		// rahul: increased the timeout
		reqData.setTimeout(120000);
		reqData.setEndpoint(endpointURL);
		if (String.isNotEmpty(webServiceConfig.Certificate_Name__c)) {
			reqData.setClientCertificateName(webServiceConfig.Certificate_Name__c);
		}

		system.debug('### Request body: ' + requestBody);
		reqData.setBody(requestBody);
		reqData.setMethod('POST');

		system.debug('@@@@ reqData: ' + reqData);

		HTTPResponse response = new HttpResponse();
		response = http.send(reqData);
		system.debug('@@@@ response: ' + response);

		// Rahul: This is for testing as the API URL in custom setting is set to some dummy URL which throws an exception
		// mocking here to finish integration with my code
		// TODO: Remote when we get proper API
		// success mock

		// response.setStatusCode(200);
		// response.setBody('{"status": "OK"}');

		// failure mock
		// response.setStatusCode(400);
		// response.setBody('"Callout failed with an unknown reason!"');

		return response;
	}

	private static Order_Billing_Transaction__c createBillingTransaction(
		String transactionId,
		List<Contracted_Products__c> contractedProducts
	) {
		List<Id> contractedProductIds = new List<Id>();
		List<Id> assetIds = new List<Id>();
		Id orderId;
		Id accountId;
		// iterate over the contracted products to collect the contracted product ids and asset ids
		for (Contracted_Products__c contractedProduct : contractedProducts) {
			contractedProductIds.add(contractedProduct.Id);
			if (String.isNotEmpty(contractedProduct.Customer_Asset__c)) {
				assetIds.add(contractedProduct.Customer_Asset__c);
			}
			if (contractedProduct.Order__c != null && orderId == null) {
				orderId = contractedProduct.Order__c;
				accountId = contractedProduct.Order__r.Account__c;
			}
		}

		Order_Billing_Transaction__c billingTransaction = new Order_Billing_Transaction__c(
			Contracted_Products__c = JSON.serialize(contractedProductIds),
			Customer_Assets__c = JSON.serialize(assetIds),
			Order__c = orderId,
			Account__c = accountId,
			Transaction_Id__c = transactionId
		);
		return billingTransaction;
	}

	private static String replaceSpecialCharacters(String input) {
		String output = input;
		Map<String, String> replaceMap = new Map<String, String>{ '+' => ' plus ', ';' => ',' };
		if (input.containsAny(String.join(new List<String>(replaceMap.keySet()), ''))) {
			for (String toReplace : replaceMap.keySet()) {
				output = output.replace(toReplace, replaceMap.get(toReplace));
			}
		}
		return output;
	}

	// activationDate to 1st of next month after today (unless it's already on 1st of month).
	@TestVisible
	private static Date getActivationDate() {
		return system.today().day() == 1
			? system.today()
			: system.today().addMonths(1).toStartOfMonth();
	}

	private class CreateBSSOrderSuccessResponse {
		public String status;
	}

	public class CollectionWrapper {
		public Order_Billing_Transaction__c billingTransactionToInsert = new Order_Billing_Transaction__c();
		public List<Contracted_Products__c> contractedProductsToUpdate = new List<Contracted_Products__c>();
	}
	/*  private class CreateBSSOrderErrorResponse {
        public ErrorInfo errorInfo;  
    }
    private class ErrorInfo {
        public String errorCode;
        public String errorDescription;
        public String targetSystemName;
        public String targetServiceName;
    }   */

	private static Map<Id, String> createExtSiteMap(List<Contracted_Products__c> cpList) {
		Map<Id, String> esMap = new Map<Id, String>();
		Set<Id> extAcctIds = new Set<Id>();
		String extSource = 'Unify';
		for (Contracted_Products__c cp : cpList) {
			if (
				cp.Order__r
					?.Billing_Arrangement__r
					?.Financial_Account__r
					?.Ban__r
					?.ExternalAccount__c != null
			) {
				extAcctIds.add(
					cp.Order__r
						?.Billing_Arrangement__r
						?.Financial_Account__r
						?.Ban__r
						?.ExternalAccount__c
				);
			}
		}
		if (extAcctIds.size() > 0) {
			List<External_Site__c> extSiteList = KeyRingService.getExternalsitesList(
				extAcctIds,
				extSource
			);
			for (Contracted_Products__c contProd : cpList) {
				for (External_Site__c extSite : extSiteList) {
					if (
						contProd.Site__c == extSite.Site__c &&
						contProd.Order__r
							?.Billing_Arrangement__r
							?.Financial_Account__r
							?.Ban__r
							?.ExternalAccount__c == extSite.External_Account__c
					) {
						esMap.put(contProd.Id, extSite.SLSAPID__c);
					}
				}
			}
		}
		return esMap;
	}

	/*private static String nullCheck(Object inp) {
    if (inp != null) {
      return String.valueOf(inp);
    }
    return '';
  }*/
}