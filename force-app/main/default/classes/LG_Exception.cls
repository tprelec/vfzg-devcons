public class LG_Exception extends Exception {

    // this constructor is added for covering unit test and should not be used in main class
    public LG_Exception(String internalMessage, String externalMessage) {
        // the first parameter is ignored by purpose
        // sending externalMessage for to the base class
        this(externalMessage);
    }
}