@isTest
public class CS_SiteLookupTest {
    private static testMethod void testRequiredAttributes(){
        CS_SiteLookup caLookup = new  CS_SiteLookup();
        caLookup.getRequiredAttributes();
    }

    private static testMethod void test() {
        Account testAccount = CS_DataTest.createAccount('Test Account');
        insert testAccount;

         Opportunity testOpp = new Opportunity(
            Name = 'New Opportunity',
            OwnerId = UserInfo.getUserId(),
            StageName = 'Qualification',
            Probability = 0,
            CloseDate = system.today(),
            //ForecastCategoryName = 'Pipeline',
            AccountId = testAccount.id
        );
        insert testOpp;

        cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        insert basket;

        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 

        cscfga__Product_Definition__c accessProductDef = CS_DataTest.createProductDefinition('Access Infrastructure');
        accessProductDef.Product_Type__c = 'Fixed';
        accessProductDef.RecordTypeId = productDefinitionRecordType;
        insert accessProductDef;

        cscfga__Product_Configuration__c accessProductConf = CS_DataTest.createProductConfiguration(accessProductDef.Id, 'Access Infrastructure',basket.Id);
        accessProductConf.cscfga__Root_Configuration__c = null;
        accessProductConf.cscfga__Parent_Configuration__c = null;
        insert accessProductConf;

        csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(accessProductConf);
        pcr.csbb__Product_Configuration__c = accessProductConf.Id;
        insert pcr;

        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('componentType', 'Access');

        CS_SiteLookup caLookup = new CS_SiteLookup();
        caLookup.doLookupSearch(searchFields, String.valueOf(accessProductDef.Id),null, 0, 0);
        searchFields.put('componentType', 'Internet');
        caLookup.doLookupSearch(searchFields, String.valueOf(accessProductDef.Id),null, 0, 0);
        searchFields.put('componentType', 'Access');
        caLookup.doLookupSearch(searchFields, String.valueOf(accessProductDef.Id),null, 0, 0);
        
    }
}