public class CS_Kpi_Json_Element {
    public String category {get;set;}
    public String taxonomy {get;set;}
    public Decimal paybackPeriod {get;set;}
    public Decimal NPV {get;set;}
    public Decimal netIncrementalBilledRevenue {get;set;}
    public Decimal freeCashFlowDivNetRevenue {get;set;}
    public Decimal ebitdaDivNetRevenue {get;set;}
    public Decimal salesMarginDivNetRevenue {get;set;}
    
    public CS_Kpi_Json_Element() {
    }
}