public without sharing class BillingLogicHelper {
	// Create main and billing assets when a order is approved by BOP or delivered
	public static void createAssets(List<Order__c> newOrders, Map<Id, Order__c> oldOrdersMap) {
		Map<Id, Order__c> submittedOneOffOrderMap = new Map<Id, Order__c>();

		for (Order__c newOrder : newOrders) {
			Order__c oldOrder = oldOrdersMap.get(newOrder.Id);

			if (
				oldOrder.status__c != newOrder.status__c &&
				newOrder.status__c == Constants.ORDER_STATUS_CLEAN &&
				newOrder.O2C_Order__c
			) {
				submittedOneOffOrderMap.put(newOrder.Id, newOrder);
			}
		}

		// status submitted (submitted to BOP)
		if (!submittedOneOffOrderMap.isEmpty()) {
			// query related contracted place in single point
			List<Contracted_Products__c> cpsOfSubmittedOrders = [
				SELECT
					Id,
					Totalprice__c,
					Order__c,
					Order__r.Status__c,
					Order__r.Billing_Arrangement__c,
					Order__r.Account__c,
					Site__c,
					Is_Recurring__c,
					Customer_Asset__c,
					Product__r.Billing_Type__c,
					PBX__c,
					Product__c,
					Product__r.Family_Tag__c,
					Product__r.Family_Tag__r.Name,
					Order__r.BOP_export_datetime__c
				FROM Contracted_Products__c
				WHERE
					Order__c IN :submittedOneOffOrderMap.keySet()
					AND Customer_Asset__c = NULL
					AND Do_not_export__c = FALSE
			];

			// create main customer assets for each contracted products
			createCustomerAssetAndLinkCPs(cpsOfSubmittedOrders);
		}
	}

	// generate install base for contracted product for orders with billing logic as percent and status submitted
	public static void createInstallBaseForCps(List<Contracted_Products__c> contractedProducts) {
		List<Contracted_Products__c> filteredContractedProducts = new List<Contracted_Products__c>();
		List<Contracted_Products__c> contractedProductsToProcess = new List<Contracted_Products__c>();

		// filter contracted products based on its field
		for (Contracted_Products__c contractedProduct : contractedProducts) {
			if (
				contractedProduct.Customer_Asset__c == null &&
				contractedProduct.Order__c != null &&
				contractedProduct.Do_not_export__c == false
			) {
				filteredContractedProducts.add(contractedProduct);
			}
		}

		// filter contracted products based on its parent fields
		for (Contracted_Products__c contractedProduct : [
			SELECT
				Id,
				Totalprice__c,
				Order__c,
				Order__r.Status__c,
				Order__r.Billing_Arrangement__c,
				Order__r.Account__c,
				Site__c,
				Is_Recurring__c,
				Customer_Asset__c,
				PBX__c,
				Product__c,
				Product__r.Family_Tag__c,
				Product__r.Family_Tag__r.Name,
				Order__r.BOP_export_datetime__c
			FROM Contracted_Products__c
			WHERE Id IN :filteredContractedProducts AND Order__r.O2C_Order__c = TRUE
		]) {
			contractedProductsToProcess.add(contractedProduct);
		}
		if (!contractedProductsToProcess.isEmpty()) {
			createCustomerAssetAndLinkCPs(contractedProductsToProcess);
		}
	}

	public static final String DELIVERY_STATUS_OPEN = 'OPEN';

	// generate install base or main customer asset for passed contracted products
	private static void createCustomerAssetAndLinkCPs(
		List<Contracted_Products__c> contractedProducts
	) {
		List<Customer_Asset__c> customerAssetsToCreate = new List<Customer_Asset__c>();
		List<Contracted_Products__c> contractedProductToUpdate = new List<Contracted_Products__c>();
		Map<Id, Competitor_Asset__c> mapCompetitorAssetToUpdate = new Map<Id, Competitor_Asset__c>();

		// create main customer asset for contracted products
		for (Contracted_Products__c contractedProduct : contractedProducts) {
			if (contractedProduct.Customer_Asset__c == null && contractedProduct.Order__c != null) {
				// collected the filtered contracted products to link them with customer asserts in next for loop
				contractedProductToUpdate.add(contractedProduct);
				// collect customer assets to insert, install base id would be generated from trigger
				customerAssetsToCreate.add(
					new Customer_Asset__c(
						Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW,
						Order__c = contractedProduct.Order__c,
						Type__c = Constants.CUSTOMER_ASSET_TYPE_NORMAL,
						Account__c = contractedProduct.Order__r.Account__c,
						Site__c = contractedProduct.Site__c
					)
				);
			}
		}

		if (!customerAssetsToCreate.isEmpty()) {
			insert customerAssetsToCreate;
		}

		// create main customer asset for contracted products
		for (Integer cpCount = 0; cpCount < contractedProductToUpdate.size(); cpCount++) {
			Contracted_Products__c contractedProduct = contractedProductToUpdate[cpCount];
			if (contractedProduct.Customer_Asset__c != null) {
				continue;
			}

			// update statuses and link to a customer asset
			contractedProduct.Billing_Status__c = Constants.CONTRACTED_PRODUCT_BILLING_STATUS_NEW;
			// trick to get the right customer asset as Contracted product and its related assets have same indexes in the list
			contractedProduct.Customer_Asset__c = customerAssetsToCreate[cpCount].Id;

			// update billing arrangement Id's on contracted product and competiter assets
			if (
				contractedProduct.Order__c != null &&
				contractedProduct.Order__r.Billing_Arrangement__c != null &&
				contractedProduct.Order__r.BOP_export_datetime__c == null
			) {
				contractedProduct.Billing_Arrangement__c = contractedProduct.Order__r.Billing_Arrangement__c;

				// check if contracted product is related to competiter asset with family tag as PBX Trunking
				if (
					contractedProduct.PBX__c != null &&
					!mapCompetitorAssetToUpdate.containsKey(contractedProduct.PBX__c) &&
					contractedProduct.Product__c != null &&
					contractedProduct.Product__r.Family_Tag__c != null &&
					contractedProduct.Product__r.Family_Tag__r.Name ==
					Constants.PRODUCT2_FAMILYTAG_PBX_Trunking
				) {
					mapCompetitorAssetToUpdate.put(
						contractedProduct.PBX__c,
						new Competitor_Asset__c(
							Id = contractedProduct.PBX__c,
							Billing_Arrangement__c = contractedProduct.Order__r.Billing_Arrangement__c
						)
					);
				}
			}
		}

		if (!contractedProductToUpdate.isEmpty()) {
			update contractedProductToUpdate;
		}

		if (!mapCompetitorAssetToUpdate.isEmpty()) {
			update mapCompetitorAssetToUpdate.values();
		}
	}
}