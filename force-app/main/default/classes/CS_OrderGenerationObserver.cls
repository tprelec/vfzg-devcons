/*
 * @description Observer executing after generations of O&S records is done (orders, subscriptions, services etc.)
 */
@SuppressWarnings('PMD.AvoidGlobalModifier')
global with sharing class CS_OrderGenerationObserver implements csordcb.ObserverApi.IObserver {
	/*
	 * @description Contains the main logic of the oberver - O&S records postprocessing
	 * Specifically, the link between Order and Solution is created as well as the contract records
	 */
	global void execute(csordcb.ObserverApi.Observable observableInst, Object arg) {
		csordtelcoa.OrderGenerationObservable observable = (csordtelcoa.OrderGenerationObservable) observableInst;
		// There should be only one Order per Opportunity
		List<Id> orderIds = observable.getOrderIds();
		csord__Order__c rootOrder = [
			SELECT Id, csordtelcoa__Opportunity__c, csordtelcoa__Opportunity__r.Primary_Basket__c, Ecommerce_Order_Reference_Id__c
			FROM csord__Order__c
			WHERE Id IN :orderIds
			LIMIT 1
		];

		try {
			// Link newly created records with the solution
			linkOrderAndSolution(observable, rootOrder);
			updateServiceAndSubscriptionFields(observable);
			mapOrderReferenceFromBasket(rootOrder);
			CS_ContractManagement.createContractRecords(observable.getSubscriptionIds());
			firePlatformEvent(rootOrder, true);
		} catch (Exception ex) {
			System.debug(LoggingLevel.DEBUG, 'CS_OrderGenerationObserver failed ' + ex.getMessage());
			firePlatformEvent(rootOrder, false);
		}
	}

	/*
	 * @description Link Order, Subscription and Services with the Solution object
	 */
	@TestVisible
	private void linkOrderAndSolution(csordtelcoa.OrderGenerationObservable observable, csord__Order__c rootOrder) {
		cssmgnt.API_1.linkSubscriptions(observable.getSubscriptionIds());

		Map<Id, csord__Solution__c> mapSolutions = new Map<Id, csord__Solution__c>(
			[
				SELECT Id, Is_Active__c, cssdm__replaced_solution__c
				FROM csord__Solution__c
				WHERE
					cssdm__product_basket__r.cscfga__Opportunity__c = :rootOrder.csordtelcoa__Opportunity__c
					AND (cssdm__product_basket__r.csbb__Synchronised_With_Opportunity__c = TRUE
					OR cssdm__product_basket__r.csordtelcoa__Synchronised_with_Opportunity__c = TRUE)
			]
		);

		List<csord__Solution__c> solutions = mapSolutions.values();

		List<cscfga__Product_Configuration__c> solutionConfigurations = [
			SELECT Id, csordtelcoa__cancelled_by_change_process__c, cssdm__solution_association__c, cscfga__Product_Definition__r.Name
			FROM cscfga__Product_Configuration__c
			WHERE cssdm__solution_association__c IN :mapSolutions.keyset()
		];

		System.debug(LoggingLevel.DEBUG, 'solutionConfigurations ' + JSON.serializePretty(solutionConfigurations));

		Set<Id> replacedSolutionIds = new Set<Id>();

		for (csord__Solution__c solution : solutions) {
			solution.csord__Order__c = rootOrder.Id;
			Id replacedSolution = setSolutionIsActive(solution, solutionConfigurations);
			if (replacedSolution != null) {
				replacedSolutionIds.add(replacedSolution);
			}
		}

		if (!replacedSolutionIds.isEmpty()) {
			List<csord__Solution__c> replacedSolutions = [SELECT Id, Is_Active__c FROM csord__Solution__c WHERE Id IN :replacedSolutionIds];

			for (csord__Solution__c replacedSolution : replacedSolutions) {
				replacedSolution.Is_Active__c = false;
				solutions.add(replacedSolution);
			}
		}

		if (!solutions.isEmpty()) {
			update solutions;
		}
	}

	/*
	 * @description If there is a Product Configuration associated to a solution which is not terminated, decomposed solution's field Is_Active__c is set to true, false otherwise
	 * @returns Id of the replaced solution which needs to be deactivated anyways
	 */
	private Id setSolutionIsActive(csord__Solution__c solution, List<cscfga__Product_Configuration__c> allConfigurations) {
		Boolean allTerminated = true;
		for (cscfga__Product_Configuration__c configuration : allConfigurations) {
			if (
				configuration.cssdm__solution_association__c == solution.Id &&
				!isMainComponentConfiguration(configuration) &&
				!configuration.csordtelcoa__cancelled_by_change_process__c
			) {
				allTerminated = false;
				break;
			}
		}

		solution.Is_Active__c = !allTerminated;
		return solution.cssdm__replaced_solution__c;
	}

	// @description Checks whether confguration is a main component configuration - these configurations are not actually delivering
	// subscriptions or services so they need to be excluded from termination check
	private Boolean isMainComponentConfiguration(cscfga__Product_Configuration__c configuration) {
		if (configuration.cscfga__Product_Definition__r.Name == CS_ConstantsCOM.PRODUCT_DEFINITION_BUSINESS_INTERNET) {
			return true;
		}

		return false;
	}

	private void firePlatformEvent(csord__Order__c rootOrder, Boolean isSuccessful) {
		firePlatformEvent(rootOrder.Id, rootOrder.csordtelcoa__Opportunity__c, isSuccessful);
	}

	public static void firePlatformEvent(String orderId, String opportunityId, Boolean isSuccessful) {
		O_S_Order_Generated__e orderGeneratedEvent = new O_S_Order_Generated__e();
		orderGeneratedEvent.Order_Id__c = orderId;
		orderGeneratedEvent.Opportunity_Id__c = opportunityId;
		orderGeneratedEvent.IsSuccessful__c = isSuccessful;

		Database.SaveResult result = EventBus.publish(orderGeneratedEvent);
		if (result.isSuccess()) {
			System.debug(
				LoggingLevel.DEBUG,
				String.format(
					'Successfully published O_S_Order_Generated event with parameters: orderId - {0}, opportunityId - {1}, isSuccessful - {2}',
					new List<Object>{ orderId, opportunityId, isSuccessful }
				)
			);
		} else {
			System.debug(
				LoggingLevel.DEBUG,
				String.format(
					'Failed event publish for O_S_Order_Generated for parameters: orderId - {0}, opportunityId - {1}, isSuccessful - {2}',
					new List<Object>{ orderId, opportunityId, isSuccessful }
				)
			);
			for (Database.Error err : result.getErrors()) {
				System.debug(LoggingLevel.DEBUG, 'Error returned: ' + err.getStatusCode() + ' - ' + err.getMessage());
			}
		}
	}

	private void updateServiceAndSubscriptionFields(csordtelcoa.OrderGenerationObservable observable) {
		List<Id> serviceIds = observable.getServiceIds();
		List<Id> subscriptionIds = observable.getSubscriptionIds();

		List<csord__Service__c> services = [
			SELECT
				Id,
				Installation_Wishdate__c,
				Termination_Fee_Discount__c,
				Termination_Fee_Discount_Type__c,
				Termination_Wish_Date__c,
				csordtelcoa__Product_Configuration__r.Installation_Wish_Date__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Discount__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Discount_Type__c,
				csordtelcoa__Product_Configuration__r.Termination_Wish_Date__c
			FROM csord__Service__c
			WHERE Id IN :serviceIds AND csordtelcoa__Product_Configuration__c != ''
		];

		for (csord__Service__c service : services) {
			service.Installation_Wishdate__c = service.csordtelcoa__Product_Configuration__r.Installation_Wish_Date__c;
			service.Termination_Fee_Discount__c = service.csordtelcoa__Product_Configuration__r.Termination_Fee_Discount__c;
			service.Termination_Fee_Discount_Type__c = service.csordtelcoa__Product_Configuration__r.Termination_Fee_Discount_Type__c;
			service.Termination_Wish_Date__c = service.csordtelcoa__Product_Configuration__r.Termination_Wish_Date__c;
		}

		List<csord__Subscription__c> subscriptions = [
			SELECT
				Id,
				Termination_Fee_Discount__c,
				Termination_Fee_Discount_Type__c,
				Termination_Wish_Date__c,
				Termination_Fee_Calculation__c,
				csordtelcoa__Product_Configuration__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Discount__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Discount_Type__c,
				csordtelcoa__Product_Configuration__r.Termination_Wish_Date__c,
				csordtelcoa__Product_Configuration__r.Termination_Fee_Calculation__c
			FROM csord__Subscription__c
			WHERE Id IN :subscriptionIds AND csordtelcoa__Product_Configuration__c != ''
		];

		for (csord__Subscription__c subscription : subscriptions) {
			subscription.Termination_Fee_Discount__c = subscription.csordtelcoa__Product_Configuration__r.Termination_Fee_Discount__c;
			subscription.Termination_Fee_Discount_Type__c = subscription.csordtelcoa__Product_Configuration__r.Termination_Fee_Discount_Type__c;
			subscription.Termination_Wish_Date__c = subscription.csordtelcoa__Product_Configuration__r.Termination_Wish_Date__c;
			subscription.Termination_Fee_Calculation__c = subscription.csordtelcoa__Product_Configuration__r.Termination_Fee_Calculation__c;
		}

		if (!services.isEmpty()) {
			update services;
		}

		if (!subscriptions.isEmpty()) {
			update subscriptions;
		}
	}

	private void mapOrderReferenceFromBasket(csord__Order__c rootOrder) {
		cscfga__Product_Basket__c primaryBasket = [
			SELECT Id, csb2c__API_Order_Reference__c
			FROM cscfga__Product_Basket__c
			WHERE Id = :rootOrder.csordtelcoa__Opportunity__r.Primary_Basket__c
		];
		rootOrder.Ecommerce_Order_Reference_Id__c = primaryBasket.csb2c__API_Order_Reference__c;
		update rootOrder;
	}
}
