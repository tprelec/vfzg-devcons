public inherited sharing class BanDAO {
	/**
	 * Retrieve the Ban details based on Set of Account Ids
	 */
	public static Map<Id, Ban__c> getBanDetailsForRelatedAccounts(
		Set<String> fieldSet,
		Set<Id> accountsIdSet
	) {
		String query = String.format(
			'SELECT {0} FROM Ban__c WHERE Account__c IN: accountsIdSet',
			new List<String>{ String.join(new List<String>(fieldSet), ',') }
		);
		return new Map<Id, Ban__c>((List<Ban__c>) Database.query(query));
	}
}