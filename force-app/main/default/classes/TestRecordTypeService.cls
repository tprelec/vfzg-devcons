@isTest
public with sharing class TestRecordTypeService {
	@IsTest
	private static void testGetRecordTypeNameById() {
		System.assertEquals(
			'Default',
			RecordTypeService.getRecordTypeNameById('Opportunity', getDefaultOppRecordType().Id),
			'Record type is Default'
		);
	}

	@IsTest
	private static void testGetRecordTypeIdByName() {
		System.assertEquals(
			getDefaultOppRecordType().Id,
			RecordTypeService.getRecordTypeIdByName('Opportunity', 'Default'),
			'Record type is Default'
		);
	}

	private static RecordType getDefaultOppRecordType() {
		return [
			SELECT Id, Name, DeveloperName
			FROM RecordType
			WHERE SObjectType = 'Opportunity' AND Name = 'Default'
		];
	}
}