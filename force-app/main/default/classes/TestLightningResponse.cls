/*
 * @author Rahul Sharma
 *
 * @description Test class for LightningResponse
 */
@IsTest
private class TestLightningResponse {
	static final String MESSAGE = 'Some message';

	@IsTest
	static void testSetBody() {
		Test.startTest();
		LightningResponse response = new LightningResponse().setBody(MESSAGE);
		Test.stopTest();

		System.assertEquals(null, response.variant, 'We expect a null variant');
		System.assertEquals(null, response.message, 'We expect a null message');
		System.assertEquals(message, response.body, 'We expect message in body');
	}

	@IsTest
	static void testSetSuccessMessageWithBody() {
		Test.startTest();
		Account account = new Account(Name = MESSAGE);
		LightningResponse response = new LightningResponse().setSuccess(MESSAGE).setBody(account);
		Test.stopTest();

		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant, 'We expect a success variant');
		System.assertEquals(message, response.message, 'We expect message in message');
		System.assertEquals(account, response.body, 'We expect account in body');
	}

	@IsTest
	static void testSetSuccessMessage() {
		Test.startTest();
		LightningResponse response = new LightningResponse().setSuccess(MESSAGE);
		Test.stopTest();

		System.assertEquals(LightningResponse.getSuccessVariant(), response.variant, 'We expect a success variant');
		System.assertEquals(MESSAGE, response.message, 'We expect message in message');
		System.assertEquals(null, response.body, 'We expect null in body');
	}

	@IsTest
	static void testSetErrorMessage() {
		Test.startTest();
		LightningResponse response = new LightningResponse().setError(MESSAGE);
		Test.stopTest();

		System.assertEquals(LightningResponse.getErrorVariant(), response.variant, 'We expect an error variant');
		System.assertEquals(MESSAGE, response.message, 'We expect message in message');
		System.assertEquals(null, response.body, 'We expect null in body');
	}

	@IsTest
	static void testSetWarningMessage() {
		Test.startTest();
		LightningResponse response = new LightningResponse().setWarning(MESSAGE);
		Test.stopTest();

		System.assertEquals(LightningResponse.getWarningVariant(), response.variant, 'We expect a warning variant');
		System.assertEquals(MESSAGE, response.message, 'We expect message in message');
		System.assertEquals(null, response.body, 'We expect null in body');
	}

	@IsTest
	static void testSetInfoMessage() {
		Test.startTest();
		LightningResponse response = new LightningResponse().setInfo(MESSAGE);
		Test.stopTest();

		System.assertEquals(LightningResponse.getInfoVariant(), response.variant, 'We expect a info variant');
		System.assertEquals(MESSAGE, response.message, 'We expect message in message');
		System.assertEquals(null, response.body, 'We expect null in body');
	}
}
