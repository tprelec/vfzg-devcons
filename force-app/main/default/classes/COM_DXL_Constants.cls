public with sharing class COM_DXL_Constants {
	public static final String ATTACHMENT_NAME = 'DXL_Request_payload';
	public static final String TRANSACTION_TYPE = 'createCustomer';
	public static final String TRANSACTION_STATUS_SUCCESS = 'Success';
	public static final String TRANSACTION_STATUS_ERROR = 'Error';

	public static final String ACCOUNT_MANAGER_FIXED = 'fixed_dealer';
	public static final String ACCOUNT_MANAGER_MOBILE = 'mobile_dealer';
	public static final String ACCOUNT_MANAGER_OWNER = 'owner';
	public static final String UNIFY_EXTERNAL_SYSTEM = 'Unify';

	public static final String DIRECTOR_CONTACT_ROLE = 'director';
	public static final String FINANCIAL_CONTACT_ROLE = 'Financial';

	public static final String COM_DXL_INTEGRATION_SLS_CATALOG_ID_SETTING_NAME = 'Site_Level_Services_CatalogId';

	public static final String SFDC_ACCOUNT_ID = 'salesforceAccountId';

	public static final String BUSINESS_TRANSACTION_ID = 'businessTransactionId';

	public static final String CREATECUSTOMER_SYNC_RESPONSE_TRANSACTION_ID = 'CreateCustomer_Sync_Response_TransId';

	public static final String COM_DXL_WEB_PARAM = 'DXLcreateCustomer';
	public static final String COM_DXL_OSS_CODE = 'BOP';

	public static final String COM_DXL_CUSTOMER_PROVISIONED_ORDER_STATUS = 'Customer Provisioned';
	public static final String COM_DXL_ASYNC_COMPLETED_STATUS = 'Completed';

	public static final String COM_DXL_INTEGRATION_SETTING_NAME = 'dxlCreateCustomer';
	public static final String COM_DXL_EXTERNAL_SOURCE_UNIFY = 'Unify';
	public static final String COM_DXL_EXTERNAL_SOURCE_BOP = 'BOP';
	public static final String COM_DXL_PROCESSED_STATUS = 'Processed';
	public static final String COM_DXL_ASYNC_ERROR_STATUS = 'Async Error';
	public static final String COM_DXL_ACCOUNT_API_NAME = 'Account';
	public static final String COM_DXL_CONTACT_API_NAME = 'Contact';
	public static final String COM_DXL_SITE_API_NAME = 'Site__c';
	public static final String COM_DXL_BAN_API_NAME = 'BAN__c';
	public static final String COM_DXL_ORDER_API_NAME = 'csord__Order__c';
	public static final String COM_DXL_FINANCIAL_ACCOUNT_API_NAME = 'Financial_Account__c';
	public static final String COM_DXL_BILLING_ARRANGEMENT_API_NAME = 'Billing_Arrangement__c';
	public static final String COM_DXL_ASYNC_RESPONSE_NAME = 'COM_DXL_ASYNC_SUCCESS';
	public static final String COM_DXL_BSS_ASYNC_RESPONSE_NAME = 'COM_DXL_CREATE_BSS_ORDER_ASYNC_RESPONSE';

	public static final String COM_DXL_UNIFY_CONTACT_REFID = 'unifyContactRefId';
	public static final String COM_DXL_CONTACT_EMAIL = 'email';
	public static final String COM_DXL_CONTACT_ROLE_TYPE = 'roleType';
	public static final String COM_DXL_CONTACT_PRIMARY_PHONE = 'primaryPhone';
	public static final String COM_DXL_CONTACT_SFDC_ACCOUNT_ID = 'salesforce_Account_ID';
	public static final String COM_DXL_CONTACT_SFDC_CONTACT_ID = 'salesforce_Contact_ID';
	public static final String COM_DXL_CONTACT_ADDRESS = 'address';
	public static final String COM_DXL_CONTACT_PERSON_DATA = 'personData';
	public static final String COM_DXL_CONTACT_GENDER = 'gender';
	public static final String COM_DXL_CONTACT_BIRTHDATE = 'birthDate';
	public static final String COM_DXL_CONTACT_FIRSTNAME = 'firstName';
	public static final String COM_DXL_CONTACT_LASTNAME = 'lastName';
	public static final String COM_DXL_CONTACT_PHONE_NUMBER = 'phoneNumber';
	public static final String COM_DXL_CONTACT_ID_TYPE = 'type';
	public static final String COM_DXL_CONTACT_ID_ID = 'id';
	public static final String COM_DXL_CONTACT_ID_COUNTRY = 'country';

	public static final String COM_DXL_UNIFY_ADDRESS_REFID = 'unifyAddressRefId';
	public static final String COM_DXL_ADDRESS_STREET = 'street';
	public static final String COM_DXL_ADDRESS_HOUSE_NUMBER = 'houseNumber';
	public static final String COM_DXL_ADDRESS_HOUSE_NUM_EXT = 'houseNumberExtension';
	public static final String COM_DXL_ADDRESS_POSTCODE = 'postalCode';
	public static final String COM_DXL_ADDRESS_CITY = 'city';
	public static final String COM_DXL_ADDRESS_COUNTRY = 'country';

	public static final String COM_DXL_ACCOUNT_MANAGER_USER_TYPE = 'userType';
	public static final String COM_DXL_ACCOUNT_MANAGER_NAME = 'name';
	public static final String COM_DXL_ACCOUNT_MANAGER_MANAGER_NAME = 'manager_Name';
	public static final String COM_DXL_ACCOUNT_MANAGER_BOP_CODE = 'BOPCode_c';
	public static final String COM_DXL_ACCOUNT_MANAGER_TYPE = 'type';

	public static final String COM_DXL_LEGAL_ENTITY_MAPPING = 'legalEntity';
	public static final String COM_DXL_COUNTRY_MAPPING = 'countryCode';

	public static final String COM_DXL_BSS_INTEGRATION_SETTING_NAME = 'createBSSOrder_setting';
	public static final String COM_DXL_BSS_ACTIVE_STATUS = 'Active';
	public static final String COM_DXL_BSS_TERMINATED_STATUS = 'Terminated';
	public static final String COM_DXL_BSS_CLOSED_REPLACED_STATUS = 'Closed Replaced';
	public static final String COM_DXL_BSS_FAILED_STATUS = 'Failed';
	public static final String COM_DXL_BSS_REQUESTED_STATUS = 'Requested';
	public static final String COM_DXL_BSS_NA_STATUS = 'N/A';

	public static final String DXL_BSS_WEB_PARAM = 'DXLcreateBSSOrder';
	public static final String COM_DXL_BSS_SPECIFICATION_NAME = 'ServiceSpecifications.json';
	public static final Integer COM_DXL_BSS_MAX_NR_OF_FAILED_REQUESTS = 3;
	public static final String COM_DXL_BSS_SPECIFICATION_KEY = 'specifications';
	public static final String COM_DXL_BSS_ATTRIBUTES_KEY = 'attributes';

	public static final List<String> COM_DXL_BSS_SLI_CHARGE_DISCOUNT = new List<String>{ 'Time Limited Discount Charge', 'Discount Charge' };

	public static final String COM_DELTA_STATUS_CONTINUING = 'Continuing In Subscription';
	public static final String COM_DELTA_STATUS_ADDITION = 'Added To Subscription';
	public static final String COM_DELTA_STATUS_DELETION = 'Deleted From Subscription';

	public static final List<String> COM_DXL_BSS_SLI_CHARGE = new List<String>{ 'Charge' };
	public static final String COM_DXL_BSS_DISCOUNT_PERCENTAGE = 'Percentage';
	public static final String COM_DXL_BSS_DISCOUNT_AMOUNT = 'Absolute';
	public static final String COM_DXL_BSS_CHARGE_TYPE_ONEOFF = 'OC';
	public static final String COM_DXL_BSS_CHARGE_TYPE_RECURRING = 'RC';

	public static final String COM_DXL_BSS_ACTIVITY_TYPE_PROVIDE = 'Provide';
	public static final String COM_DXL_BSS_ACTIVITY_TYPE_CHANGE = 'Change';
	public static final String COM_DXL_BSS_ACTIVITY_TYPE_CEASE = 'Cease';

	public static final String COM_DXL_BSS_SETTING_BITAG5 = 'BITag5';
	public static final String COM_DXL_BSS_SETTING_B2B_GENERIC_PRODUCT_ID = 'b2bGenericProductCatalogId';

	public static final String COM_DXL_BSS_SETTING_GP_CI_MOCK = '8873941';
	public static final String COM_DXL_BSS_SETTING_BITAG5_MOCK = 'OS';

	public static final String COM_DXL_BSS_ELEMENT_BAID = 'billingArrangementId';
	public static final String COM_DXL_BSS_ELEMENT_BCID = 'billingCustomerId';

	public static final String COM_DXL_BSS_ELEMENT_BILLING_ORDER = 'billingOrder';
	public static final String COM_DXL_BSS_ELEMENT_ORDER_LINES = 'orderLines';
	public static final String COM_DXL_BSS_ELEMENT_CUSTOMER_DETAILS = 'customerDetails';

	public static final String COM_DXL_BSS_ELEMENT_SFIBID = 'salesforceInstalledBaseId';
	public static final String COM_DXL_BSS_ELEMENT_COMPONENT_APID = 'componentAssignedProductId';
	public static final String COM_DXL_BSS_ELEMENT_ACTIVITY_TYPE = 'activityType';
	public static final String COM_DXL_BSS_ELEMENT_ACTIVATION_DATE = 'activationDate';
	public static final String COM_DXL_BSS_ELEMENT_INVOICING_START_DATE = 'invoicingStartDate';
	public static final String COM_DXL_BSS_ELEMENT_TYPE_OF_CHARGE = 'typeOfCharge';
	public static final String COM_DXL_BSS_ELEMENT_CHARGE_DESCRIPTION = 'chargeDescription';
	public static final String COM_DXL_BSS_ELEMENT_CHARGE_AMOUNT = 'chargeAmount';
	public static final String COM_DXL_BSS_ELEMENT_QUANTITY = 'quantity';
	public static final String COM_DXL_BSS_ELEMENT_DISCOUNT_AMOUNT = 'discountAmount';
	public static final String COM_DXL_BSS_ELEMENT_DISCOUNT_DESCRIPTION = 'discountDescription';
	public static final String COM_DXL_BSS_ELEMENT_DISCOUNT_METHOD = 'discountMethod';
	public static final String COM_DXL_BSS_ELEMENT_CHARGE_CODE = 'chargeCode';
	public static final String COM_DXL_BSS_ELEMENT_BITAG_1 = 'businessIntelligenceTag1';
	public static final String COM_DXL_BSS_ELEMENT_BITAG_2 = 'businessIntelligenceTag2';
	public static final String COM_DXL_BSS_ELEMENT_BITAG_3 = 'businessIntelligenceTag3';
	public static final String COM_DXL_BSS_ELEMENT_BITAG_4 = 'businessIntelligenceTag4';
	public static final String COM_DXL_BSS_ELEMENT_BITAG_5 = 'businessIntelligenceTag5';
	public static final String COM_DXL_BSS_ELEMENT_EXTERNAL_REFERENCE_ID = 'externalReferenceId';
	public static final String COM_DXL_BSS_ELEMENT_CEASE_AUTOMATICALLY = 'ceaseAutomatically';
	public static final String COM_DXL_BSS_ELEMENT_ALLOCATION_CODE = 'allocationCode';
	public static final String COM_DXL_BSS_ELEMENT_PO_NUMBER = 'purchaseOrderNumber';
	public static final String COM_DXL_BSS_ELEMENT_B2B_GENERIC_PRODUCT_ID = 'b2bGenericProductCatalogId';
	public static final String COM_DXL_BSS_ELEMENT_SLSAPID = 'SLSAPID';

	public static final String COM_DXL_BSS_SYNC_MOCK_RESPONSE_SUCCESS = '{"status":"success"}';
	public static final String COM_DXL_BSS_SYNC_MOCK_RESPONSE_FAILED = '{"error":"Error"}';
}
