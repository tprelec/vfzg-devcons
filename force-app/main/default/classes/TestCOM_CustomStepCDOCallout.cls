@isTest
private class TestCOM_CustomStepCDOCallout {
	@TestSetup
	private static void testSetup() {
		csord__Order__c order = new csord__Order__c(Name = 'TestOrder', csord__Identification__c = 'ID_4568978');
		insert order;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = CS_DataTest.createOrchProcessTemplate('Test process Template', '5', true);

		CSPOFA__Orchestration_Step_Template__c step1Template = CS_DataTest.createOrchStepTemplate(testProcessTemplate.Id, 'Test Step 1', '3', false);
		insert step1Template;

		cscfga__Product_Configuration__c productConfiguration = CS_DataTest.createProductConfiguration(null, 'Test Conf', null);
		insert productConfiguration;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(productConfiguration.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Order__c = order.Id;
		insert subscription;

		Account account = CS_DataTest.createAccount('Test Umbrella Account');
		account.CDO_Tenant_Id__c = 'someTenantId';
		account.CDO_Umbrella_Service_Id__c = '111-222-333-444';
		insert account;

		Site__c testSite = CS_DataTest.createSite('Test Site', account, '1032AB', 'Street', 'City', 10.0);
		testSite.Footprint__c = null;
		insert testSite;
	}

	@isTest
	private static void testProcessMethodCreateTenantSuccess() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c WHERE Name = 'TestOrder'];

		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd1', null, false);
		delOrd.Order__c = order.Id;
		delOrd.Status__c = 'In progress';
		delOrd.CDO_Service_Order_Request_Id__c = '88888-11111';
		insert delOrd;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = [
			SELECT Id
			FROM CSPOFA__Orchestration_Process_Template__c
			WHERE Name = 'Test process Template'
		];

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = delOrd.Id;
		testProcess.Name = COM_Constants.CDO_CREATE_TENANT_PROCESS_NAME;
		insert testProcess;

		CSPOFA__Orchestration_Step_Template__c step1Template = [SELECT Id FROM CSPOFA__Orchestration_Step_Template__c WHERE Name = 'Test Step 1'];

		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);

		cscfga__Product_Configuration__c productConfiguration = [SELECT Id FROM cscfga__Product_Configuration__c WHERE Name = 'Test Conf'];
		csord__Subscription__c subscription = [
			SELECT Id
			FROM csord__Subscription__c
			WHERE csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0'
		];
		Site__c testSite = [SELECT Id FROM Site__c WHERE Site_Postal_Code__c = '1032AB' LIMIT 1];

		csord__Service__c service = CS_DataTest.createService(productConfiguration.Id, subscription, 'Service Created');
		service.csord__Identification__c = 'testSubscription';
		service.COM_Delivery_Order__c = delOrd.Id;
		service.csord__Order__c = order.Id;
		service.VFZ_Product_Abbreviation__c = 'ZIPRO';
		service.VFZ_Commercial_Article_Name__c = 'Coax aansluiting 1000/50 Mbps';
		service.VFZ_Commercial_Article_Name__c = 'Coax HFC';
		service.Site__c = testSite.Id;
		insert service;

		List<CSPOFA__Orchestration_Step__c> steps = CS_OrchestratorStepUtility.getStepList(new List<CSPOFA__Orchestration_Step__c>{ step1 });

		String umbrellaService = '111-222-333-444';
		String tenantId = umbrellaService;
		String serviceOrderRequest = COM_WebServiceConfig.getUUID();
		COM_Integration_setting__mdt tenantSpecificationSetting = COM_Integration_setting__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);
		COM_CDO_integration_settings__mdt createTenantCDOSettings = COM_CDO_integration_settings__mdt.getInstance(
			COM_CDO_Constants.COM_CDO_INTEGRATION_SETTING_NAME_CREATE_TENANT
		);

		String mockASyncSuccess =
			'{ "eventId": "deff54ee-ba37-4dba-b1c9-1fe1e554a4e1", "eventTime": "2022-02-08T11:30:15.97221Z", "eventType": "ServiceOrderStateChangeNotification", "event": { "serviceOrder": { "id": "' +
			serviceOrderRequest +
			'", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId": "' +
			createTenantCDOSettings.externalId__c +
			'", "priority": "' +
			+createTenantCDOSettings.priority__c +
			'", "description": "' +
			+createTenantCDOSettings.Description__c +
			+'", "category": "' +
			+createTenantCDOSettings.Category__c +
			'", "state": "' +
			+createTenantCDOSettings.serviceOrderItemAsyncSuccessStatus__c +
			'", "orderDate": "2022-02-08T10:37:15.97221Z", "startDate": "2022-02-08T10:37:15.97321Z","completionDate":"2022-02-08T10:37:15.97321Z", "@type": "' +
			+createTenantCDOSettings.requestType__c +
			'", "orderItem": [ { "id": "1", "action": "add", "state": "' +
			+createTenantCDOSettings.serviceOrderItemAsyncSuccessStatus__c +
			'", "@type": "standard", "service": { "id": "' +
			umbrellaService +
			'", "name": "' +
			createTenantCDOSettings.serviceName__c +
			+'", "serviceState": "' +
			tenantSpecificationSetting.Async_success_status__c +
			+'", "@type": "' +
			createTenantCDOSettings.serviceType__c +
			'", "serviceCharacteristic":[{"name":"sourceSystem","valueType":"string","value":{"@type":"string","sourceSystem":"COM"}},{"name":"tenantName","valueType":"string","value":{"@type":"string","tenantName":"{{TenantName}}"}},{"name":"tenantContactName","valueType":"string","value":{"@type":"string","tenantContactName":"{{TenantContact}}"}},{"name":"tenantContactPhone","valueType":"string","value":{"@type":"string","tenantContactPhone":"{{TenantContactPhone}}"}},{"name":"tenantContactEmail","valueType":"string","value":{"@type":"string","tenantContactEmail":"{{TenantContactEmail}}"}}],"serviceSpecification":{"id":"' +
			createTenantCDOSettings.serviceSpecificationId__c +
			'", "invariantID": "' +
			createTenantCDOSettings.serviceSpecification_invariantID__c +
			'", "version": "' +
			createTenantCDOSettings.serviceSpecification_version__c +
			'", "name": "' +
			createTenantCDOSettings.serviceSpecification_name__c +
			'", "@type": "' +
			createTenantCDOSettings.serviceSpecificationType__c +
			'" } } } ] } } }';

		RestRequest request = new RestRequest();
		request.requestUri = '/testCDOCreateTenant/*';
		request.httpMethod = 'POST';
		request.requestBody = Blob.valueOf(mockAsyncSuccess);
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;

		Test.startTest();
		COM_CDONotificationListener.processNotification();
		COM_CustomStepCDOCallout cdoCall = new COM_CustomStepCDOCallout();
		Test.stopTest();

		List<Account> accounts = [SELECT Id, CDO_Integration_Status__c, CDO_Umbrella_Service_Id__c FROM Account LIMIT 1];

		System.assertEquals(accounts[0].CDO_Integration_Status__c, 'Complete', 'Integration status is not Complete');

		System.assertEquals(accounts[0].CDO_Umbrella_Service_Id__c, umbrellaService, 'Umbrella service not set correctly in sync success test');
	}

	@isTest
	private static void testProcessMethodProvisioningSuccess() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c WHERE Name = 'TestOrder'];

		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd1', null, false);
		delOrd.Order__c = order.Id;
		delOrd.Status__c = 'In progress';
		delOrd.CDO_Service_Order_Request_Id__c = '88888-11111';
		insert delOrd;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = [
			SELECT Id
			FROM CSPOFA__Orchestration_Process_Template__c
			WHERE Name = 'Test process Template'
		];

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = delOrd.Id;
		testProcess.Name = COM_Constants.CDO_PROVISION_PROCESS_NAME;
		insert testProcess;

		CSPOFA__Orchestration_Step_Template__c step1Template = [SELECT Id FROM CSPOFA__Orchestration_Step_Template__c WHERE Name = 'Test Step 1'];

		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);

		cscfga__Product_Configuration__c productConfiguration = [SELECT Id FROM cscfga__Product_Configuration__c WHERE Name = 'Test Conf'];
		csord__Subscription__c subscription = [
			SELECT Id
			FROM csord__Subscription__c
			WHERE csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0'
		];
		Site__c testSite = [SELECT Id FROM Site__c WHERE Site_Postal_Code__c = '1032AB' LIMIT 1];

		csord__Service__c service = CS_DataTest.createService(productConfiguration.Id, subscription, 'Service Created');
		service.csord__Identification__c = 'testSubscription';
		service.COM_Delivery_Order__c = delOrd.Id;
		service.csord__Order__c = order.Id;
		service.VFZ_Product_Abbreviation__c = 'ZIPRO';
		service.VFZ_Commercial_Article_Name__c = 'Coax aansluiting 1000/50 Mbps';
		service.VFZ_Commercial_Article_Name__c = 'Coax HFC';
		service.Site__c = testSite.Id;
		insert service;

		List<CSPOFA__Orchestration_Step__c> steps = CS_OrchestratorStepUtility.getStepList(new List<CSPOFA__Orchestration_Step__c>{ step1 });

		String umbrellaService = '2222-4444-5555';
		String serviceOrderRequest = '2222-4444';
		String mockSyncSuccess =
			'{ "id":"' +
			serviceOrderRequest +
			'", "href":"https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId":"COM", "priority":"1", "description":"Internet Modem Only Service Order", "category":"Internet Modem Only Service", "state":"acknowledged", "orderDate":"2022-02-09T10:37:15.97221Z", "@type":"standard", "orderItem":[ { "id":"1", "action":"add", "state":"acknowledged", "@type":"standard", "service":{ "id":"' +
			umbrellaService +
			'", "serviceCharacteristic":[ { "name":"sourcesystem", "valueType":"string", "value":{ "@type":"string", "sourcesystem":"SF" } }, { "name":"tenantId", "valueType":"string", "value":{ "@type":"string", "tenantId":"Some Tenant Id" } }, { "name":"accessL1PostalCode", "valueType":"string", "value":{ "@type":"string", "accessL1PostalCode":"123456" } }, { "name":"accessL1HouseNumber", "valueType":"integer", "value":{ "@type":"integer", "accessL1HouseNumber":"101" } }, { "name":"accessL1HouseNumberExt", "valueType":"string", "value":{ "@type":"string", "accessL1HouseNumberExt":"9" } }, { "name":"accessL1Article", "valueType":"string", "value":{ "@type":"string", "accessL1Article":"HFC" } }, { "name":"circuitL2Article", "valueType":"string", "value":{ "@type":"string", "circuitL2Article":"int_l2_zz1" } }, { "name":"internetL3Article", "valueType":"string", "value":{ "@type":"string", "internetL3Article":"int_service_l3" } }, { "name":"ipv4BlockIPPool", "valueType":"string", "value":{ "@type":"string", "ipv4BlockIPPool":"/32" } } ], "serviceSpecification":{ "id":"bdab731e-d5de-4135-8849-5c724ea01041", "invariantID":"930d7161-0eb8-444f-8d11-5971812246d5", "version":"1.0.0", "name":"Internet Modem Only Service", "@type":"ServiceSpecification" } } } ] }';

		Test.startTest();
		COM_MockWebService.setTestMockResponse(200, 'OK', mockSyncSuccess);
		COM_CustomStepCDOCallout cdoCall = new COM_CustomStepCDOCallout();
		List<SObject> result = cdoCall.process(steps);
		Test.stopTest();

		System.assertEquals(
			Constants.ORCHESTRATOR_STEP_COMPLETE,
			((CSPOFA__Orchestration_Step__c) result[0]).CSPOFA__Status__c,
			'Status is not the same.'
		);

		COM_Delivery_Order__c updatedDelOrd = [
			SELECT CDO_Integration_Status__c, CDO_Error_Message__c, CDO_Service_Order_Request_Id__c
			FROM COM_Delivery_Order__c
			WHERE Id = :delOrd.Id
		];

		System.assertNotEquals(COM_Constants.CDO_INTEGRATION_STATUS_FAILURE, updatedDelOrd.CDO_Integration_Status__c, 'Status is failed.');
		System.assertEquals(null, updatedDelOrd.CDO_Error_Message__c, 'Error Message is not empty.');
		System.assertEquals(serviceOrderRequest, updatedDelOrd.CDO_Service_Order_Request_Id__c, 'Service Order Request Id is not as expected.');
	}

	@isTest
	private static void testProcessMethodProvisioningFailure() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c WHERE Name = 'TestOrder'];

		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd1', null, false);
		delOrd.Order__c = order.Id;
		delOrd.Status__c = 'In progress';
		delOrd.CDO_Service_Order_Request_Id__c = '88888-11111';
		insert delOrd;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = [
			SELECT Id
			FROM CSPOFA__Orchestration_Process_Template__c
			WHERE Name = 'Test process Template'
		];

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = delOrd.Id;
		testProcess.Name = COM_Constants.CDO_PROVISION_PROCESS_NAME;
		insert testProcess;

		CSPOFA__Orchestration_Step_Template__c step1Template = [SELECT Id FROM CSPOFA__Orchestration_Step_Template__c WHERE Name = 'Test Step 1'];

		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);

		cscfga__Product_Configuration__c productConfiguration = [SELECT Id FROM cscfga__Product_Configuration__c WHERE Name = 'Test Conf'];
		csord__Subscription__c subscription = [
			SELECT Id
			FROM csord__Subscription__c
			WHERE csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0'
		];
		Site__c testSite = [SELECT Id FROM Site__c WHERE Site_Postal_Code__c = '1032AB' LIMIT 1];

		csord__Service__c service = CS_DataTest.createService(productConfiguration.Id, subscription, 'Service Created');
		service.csord__Identification__c = 'testSubscription';
		service.COM_Delivery_Order__c = delOrd.Id;
		service.csord__Order__c = order.Id;
		service.VFZ_Product_Abbreviation__c = 'ZIPRO';
		service.VFZ_Commercial_Article_Name__c = 'Coax aansluiting 1000/50 Mbps';
		service.VFZ_Commercial_Article_Name__c = 'Coax HFC';
		service.Site__c = testSite.Id;
		insert service;

		List<CSPOFA__Orchestration_Step__c> steps = CS_OrchestratorStepUtility.getStepList(new List<CSPOFA__Orchestration_Step__c>{ step1 });

		String mockSyncFailed = '{ "code": 40020, "reason": "SERVICE_ORDER_CHARACTERISTICS_VALIDATION_ERROR", "message": "Request validation has failed due the following issue(s) - [ Input parameter accessL1Article violates constraint, Invalid value ]" }';

		Test.startTest();
		COM_MockWebService.setTestMockResponse(200, 'Ok', mockSyncFailed);
		COM_CustomStepCDOCallout cdoCall = new COM_CustomStepCDOCallout();
		List<SObject> result = cdoCall.process(steps);
		Test.stopTest();

		System.assertEquals(
			Constants.ORCHESTRATOR_STEP_COMPLETE,
			((CSPOFA__Orchestration_Step__c) result[0]).CSPOFA__Status__c,
			'Status is not the same.'
		);

		COM_Delivery_Order__c updatedDelOrd = [
			SELECT CDO_Integration_Status__c, CDO_Error_Message__c, CDO_Service_Order_Request_Id__c
			FROM COM_Delivery_Order__c
			WHERE Id = :delOrd.Id
		];

		System.assertEquals(COM_Constants.CDO_INTEGRATION_STATUS_FAILURE, updatedDelOrd.CDO_Integration_Status__c, 'Status is not failed.');
		System.assertNotEquals(null, updatedDelOrd.CDO_Error_Message__c, 'Error Message is empty.');
	}

	@isTest
	private static void testProcessMethodDeprovisioningSuccess() {
		csord__Order__c order = [SELECT Id FROM csord__Order__c WHERE Name = 'TestOrder'];

		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd1', null, false);
		delOrd.Order__c = order.Id;
		delOrd.Status__c = 'In progress';
		delOrd.CDO_Service_Order_Request_Id__c = '88888-11111';
		insert delOrd;

		CSPOFA__Orchestration_Process_Template__c testProcessTemplate = [
			SELECT Id
			FROM CSPOFA__Orchestration_Process_Template__c
			WHERE Name = 'Test process Template'
		];

		CSPOFA__Orchestration_Process__c testProcess = CS_DataTest.createOrchProcess(
			testProcessTemplate.Id,
			null,
			Datetime.newInstance(2020, 3, 27),
			Datetime.newInstance(2020, 3, 27),
			false
		);
		testProcess.COM_Delivery_Order__c = delOrd.Id;
		testProcess.Name = COM_Constants.CDO_DEPROVISION_PROCESS_NAME;
		insert testProcess;

		CSPOFA__Orchestration_Step_Template__c step1Template = [SELECT Id FROM CSPOFA__Orchestration_Step_Template__c WHERE Name = 'Test Step 1'];

		CSPOFA__Orchestration_Step__c step1 = CS_DataTest.createOrchStep(step1Template.Id, testProcess.Id, true);

		cscfga__Product_Configuration__c productConfiguration = [SELECT Id FROM cscfga__Product_Configuration__c WHERE Name = 'Test Conf'];
		csord__Subscription__c subscription = [
			SELECT Id
			FROM csord__Subscription__c
			WHERE csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0'
		];
		Site__c testSite = [SELECT Id FROM Site__c WHERE Site_Postal_Code__c = '1032AB' LIMIT 1];

		csord__Service__c service = CS_DataTest.createService(productConfiguration.Id, subscription, 'Service Created');
		service.csord__Identification__c = 'testSubscription';
		service.COM_Delivery_Order__c = delOrd.Id;
		service.csord__Order__c = order.Id;
		service.VFZ_Product_Abbreviation__c = 'ZIPRO';
		service.VFZ_Commercial_Article_Name__c = 'Coax aansluiting 1000/50 Mbps';
		service.VFZ_Commercial_Article_Name__c = 'Coax HFC';
		service.Site__c = testSite.Id;
		insert service;

		List<CSPOFA__Orchestration_Step__c> steps = CS_OrchestratorStepUtility.getStepList(new List<CSPOFA__Orchestration_Step__c>{ step1 });

		String umbrellaService = 'aaa-bbb-ccc';
		String serviceOrderRequest = '88888-2222';

		String mockSyncSuccess =
			'{ "id": "' +
			serviceOrderRequest +
			'", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2f34dg", "externalId": "COM", "priority": "1", "description": "Internet Modem Only Service Order", "category": "Internet Modem Only Service", "state": "acknowledged", "orderDate": "2022-02-08T10:37:15.97221Z", "@type": "standard", "orderItem": [ { "id": "1", "action": "delete", "state": "acknowledged", "@type": "standard", "service": { "id": "' +
			umbrellaService +
			'" } } ] }';

		Test.startTest();
		COM_MockWebService.setTestMockResponse(200, 'Ok', mockSyncSuccess);
		COM_CustomStepCDOCallout cdoCall = new COM_CustomStepCDOCallout();
		List<SObject> result = cdoCall.process(steps);
		Test.stopTest();

		System.assertEquals(
			Constants.ORCHESTRATOR_STEP_COMPLETE,
			((CSPOFA__Orchestration_Step__c) result[0]).CSPOFA__Status__c,
			'Status is not the same.'
		);

		COM_Delivery_Order__c updatedDelOrd = [
			SELECT CDO_Integration_Status__c, CDO_Error_Message__c, CDO_Service_Order_Request_Id__c
			FROM COM_Delivery_Order__c
			WHERE Id = :delOrd.Id
		];

		System.assertNotEquals(COM_Constants.CDO_INTEGRATION_STATUS_FAILURE, updatedDelOrd.CDO_Integration_Status__c, 'Status is failed.');
		System.assertEquals(null, updatedDelOrd.CDO_Error_Message__c, 'Error Message is not empty.');
		System.assertEquals(serviceOrderRequest, updatedDelOrd.CDO_Service_Order_Request_Id__c, 'Service Order Request Id is not as expected.');
	}
}
