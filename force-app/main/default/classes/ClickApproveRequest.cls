public with sharing class ClickApproveRequest {
	@InvocableVariable(
		label='Related Record ID'
		description='ID of the record related to the approval'
		required=true
	)
	public Id relatedToId;

	@InvocableVariable(
		label='Recepient Contact ID'
		description='ID of the Contact that will receive approval'
		required=true
	)
	public Id recepientId;

	@InvocableVariable(
		label='ClickApprove Settings Name'
		description='Please provide ClickApprove Settings Name or ClickApprove Settings ID'
		required=false
	)
	public String clickApproveSettingsName;

	@InvocableVariable(
		label='ClickApprove Settings ID'
		description='Please provide ClickApprove Settings Name or ClickApprove Settings ID'
		required=false
	)
	public String clickApproveSettingsId;

	@InvocableVariable(
		label='Attachment IDs'
		description='Attachment IDs that will be sent to for approval.'
		required=true
	)
	public List<Id> attachmentIds;

	@InvocableVariable(
		label='CC Emails'
		description='List of Emails that will be CCd.'
		required=false
	)
	public List<String> ccEmails;

	@InvocableVariable(
		label='BCC Emails'
		description='List of Emails that will be BCCd.'
		required=false
	)
	public List<String> bccEmails;

	public CSCAP.API_1.MultipleSendApprovalRequestRecord getClickApproveAPIRequest() {
		CSCAP.API_1.MultipleSendApprovalRequestRecord apiRequest = new CSCAP.API_1.MultipleSendApprovalRequestRecord();
		apiRequest.approvalObjectId = this.relatedToId;
		apiRequest.recipientContactId = this.recepientId;
		apiRequest.clickApproveSettingId = this.clickApproveSettingsId;
		apiRequest.attachmentIds = new Set<Id>(this.attachmentIds);
		apiRequest.cc = this.ccEmails;
		apiRequest.bcc = this.bccEmails;
		return apiRequest;
	}
}