@isTest
public with sharing class TestAccountKeysService {
	@TestSetup
	static void makeData() {
		User u = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];
		TestUtils.autoCommit = true;
		Account acc = TestUtils.createAccount(u);
		acc.KVK_number__c = '12345678';
		update acc;

		External_Account__c externalAccount = new External_Account__c();
		externalAccount.Account__c = acc.Id;
		externalAccount.External_Source__c = 'Unify';
		externalAccount.External_Account_Id__c = '12345678';
		insert externalAccount;

		Account acc2 = TestUtils.createAccount(u);
		acc2.KVK_number__c = '12345679';
		update acc2;
	}

	@isTest
	static void testGetAccountKeys() {
		Test.startTest();
		AccountKeysService service = new AccountKeysService();

		AccountKey body1 = new AccountKey('unify', '12345678');
		List<AccountKey> accountKeys = service.getAccountKeys(body1);
		System.assertEquals(3, accountKeys.size(), '3 Account Keys exist.');

		service = new AccountKeysService();
		AccountKey body2 = new AccountKey('kvk', '12345678');
		accountKeys = service.getAccountKeys(body2);
		System.assertEquals(3, accountKeys.size(), '3 Account Keys exist.');

		service = new AccountKeysService();
		AccountKey body3 = new AccountKey('salescloud', [SELECT Id FROM Account LIMIT 1].Id);
		accountKeys = service.getAccountKeys(body3);
		System.assertEquals(3, accountKeys.size(), '3 Account Keys exist.');

		service = new AccountKeysService();
		AccountKey body4 = new AccountKey('test', [SELECT Id FROM Account LIMIT 1].Id);
		accountKeys = service.getAccountKeys(body4);
		System.assertEquals(new List<AccountKey>(), accountKeys, 'List is empty');

		service = new AccountKeysService();
		AccountKey body5 = new AccountKey('kvk', '12345679');
		accountKeys = service.getAccountKeys(body5);
		System.assertEquals(2, accountKeys.size(), 'External account not exist');

		service = new AccountKeysService();
		AccountKey body6 = new AccountKey(
			'salescloud',
			[SELECT Id FROM Account WHERE KVK_number__c = '12345679' LIMIT 1]
			.Id
		);
		accountKeys = service.getAccountKeys(body6);
		System.assertEquals(2, accountKeys.size(), 'External account not exist');
		Test.stopTest();
	}
}