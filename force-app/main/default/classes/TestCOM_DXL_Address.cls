@isTest
private class TestCOM_DXL_Address {
	@isTest
	static void testMethod1() {
		Map<String, String> addressData = new Map<String, String>();
		addressData.put('street', 'TEST STREET');
		addressData.put('houseNumber', '20');
		addressData.put('houseNumberExtension', 'a');
		addressData.put('postalCode', '20145');
		addressData.put('city', 'AMSTERDAM');
		addressData.put('country', 'NEtherlands');

		JSONGenerator generator = JSON.createGenerator(false);
		COM_DXL_Address dxlAddress = new COM_DXL_Address(generator, addressData);
		dxlAddress.generateAddress();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(generator.getAsString());
		String addressCity = (String) m.get('city');

		System.assertEquals('AMSTERDAM', addressCity, 'Expected -> AMSTERDAM -> Actual: ' + addressCity);
	}
}
