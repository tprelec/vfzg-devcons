public class MockedEndpoints_CDORoute extends RestRoute {
	public MockedEndpoints_CDORoute() {
	}

	protected override Object doPost() {
		//TODO read from custom metadata/settings and decide if it is 200 or 400 and change the responseBodyAlso
		Object response = prepareResponse();

		//trigger async notification
		System.enqueueJob(new MockedEndpoint_QueueablePoller(MockedEndpoint_QueueablePoller.AsyncImplementation.CDO_ASYNC, 15, RestContext.request));

		return response;
	}

	private static Object prepareResponse() {
		RestRequest request = RestContext.request;
		System.debug('request ' + request);
		RestResponse response = RestContext.response;
		response.responseBody = Blob.valueOf(formulateResponse(request.requestBody.toString()));
		response.statusCode = 200;
		insert new COM_Delivery_Order__c(Name = 'testinsert' + DateTime.now());
		return response;
	}

	private static String formulateResponse(String requestBody) {
		//TODO check if createTenant or create/modify/deleteService
		return requestBody;
	}
}
