/**
 * @description         This class schedules the batch processing of the Unica Campaign.
 * @author              Guy Clairbois
 */
global class ScheduleUnicaCampaignBatch implements Schedulable {
	/**
	 * @description         This method executes the batch job.
	 */
	global void execute(SchedulableContext sBatch) {
		UnicaCampaignBatch ucb = new UnicaCampaignBatch();
		Database.executeBatch(ucb, 10);
	}
}