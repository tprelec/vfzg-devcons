@isTest
private class PostalCodeAssignmentTest {
	
	@isTest static void test_method_one() {
        
        Lead newLead1 = new Lead(
            Company = 'Test Account', LastName= 'Test Lead',
            LeadSource = 'Web',  
            Status = 'Closed - Converted',
            Town__c='testtown',
            House_Number__c='1',
            Salutation='Mr.',
            Street__c='teststreet',
            Postcode_number__c='1234',
            Postcode_char__c='AB',
            Visiting_Housenumber1__c=1);
        
        Insert newLead1;
        
		//create user1 and contac1
		//create user2 and contac2
		//create user3 and contac3
		//create user4 and contac4
        List<Contact> contactsForUpdate = new List<Contact>();
        User owner = TestUtils.createAdministrator();
        Account acct1 = TestUtils.createAccount(owner);
        Contact cont1 = TestUtils.createContact(acct1);
        User u1 = TestUtils.createManager();
        cont1.Userid__c = u1.Id;
        contactsForUpdate.add(cont1);

        Account acct2 = TestUtils.createAccount(owner);
        Contact cont2 = TestUtils.createContact(acct1);
        User u2 = TestUtils.createManager();
        cont2.Userid__c = u2.Id;
        contactsForUpdate.add(cont2);

        Account acct3 = TestUtils.createAccount(owner);
        Contact cont3 = TestUtils.createContact(acct1);
        User u3 = TestUtils.createManager();
        cont3.Userid__c = u3.Id;
        contactsForUpdate.add(cont3);

        Account acct4 = TestUtils.createAccount(owner);
        Contact cont4 = TestUtils.createContact(acct1);
        User u4 = TestUtils.createManager();
        cont4.Userid__c = u4.Id;
        contactsForUpdate.add(cont4);
        update contactsForUpdate;
        Test.startTest();

        //create dealerinfo1 with user1 and user2
        Dealer_Information__c di1 = TestUtils.createDealerInformation(cont1.Id, '999999');
        di1.Major_Account_Manager_SoHo__c = cont2.id;
        update di1;

        //create dealerinfo2 with user3 and user4
        Dealer_Information__c di2 = TestUtils.createDealerInformation(cont3.Id, '999998');
		di2.Major_Account_Manager_SoHo__c = cont4.id;
		update di2;

        //create postalcode assignment with dealerinfo1
        Postal_Code_Assignment__c psa = TestUtils.createPCA('1337', di1.id);
        psa = [select id, ZSP__c, MajorAMSoHo__c from Postal_Code_Assignment__c where id = :psa.id];
        
		//check fields
		
		System.assertEquals(psa.ZSP__c, u1.id);
		System.assertEquals(psa.MajorAMSoHo__c, u2.id);

		//update postalcode assignment with dealerinfo2
		psa.Dealer_Information__c = di2.id;
		update psa;
		psa = [select id, ZSP__c, MajorAMSoHo__c from Postal_Code_Assignment__c where id = :psa.id];

		//check fields
		System.assertEquals(psa.ZSP__c, u3.id);
		System.assertEquals(psa.MajorAMSoHo__c, u4.id);

		//change dealerinfo2 major acc manager
		di2.Major_Account_Manager_SoHo__c = cont1.id;
		update di2;

		//check fields
		psa = [select id, ZSP__c, MajorAMSoHo__c from Postal_Code_Assignment__c where id = :psa.id];
		System.assertEquals(psa.MajorAMSoHo__c, u1.id);

		//change dealerinfo2 zsp
		di2.Contact__c = cont2.id;
		update di2;

		//check fields
		psa = [select id, ZSP__c, MajorAMSoHo__c from Postal_Code_Assignment__c where id = :psa.id];
		System.assertEquals(psa.ZSP__c, u2.id);
		Test.stopTest();
	}
	
}