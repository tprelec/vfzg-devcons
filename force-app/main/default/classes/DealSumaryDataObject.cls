public with sharing class DealSumaryDataObject {


    @AuraEnabled
    public List<CS_Basket_Snapshot_Transactional__c> snapshots;
   
    @AuraEnabled
    public String basketId;
    
    @AuraEnabled
    public DealSummaryTotalOverviewData totalOverviewData;
       
    // used to display "traffic lights" on deal summary screen
    // if any snapshot has Netprofit_trigger__c = TRUE OR Basket_requires_netprofit__c = TRUE then yellow rectangle is displayed, green otherwise
    @AuraEnabled
    public Boolean isGreen;
    
    @AuraEnabled
    public List<CS_DealSummarySnapshotFields> snapshotFields;
        
    public DealSumaryDataObject(){
    	
    }

    public DealSumaryDataObject(List<CS_Basket_Snapshot_Transactional__c> snapshots, Id basketId){
        
        this.basketId = String.valueOf(basketId);
        this.snapshots = snapshots;

        isGreen = true;

        for(CS_Basket_Snapshot_Transactional__c snap : snapshots)
        {
            if(snap.Netprofit_trigger__c || snap.Basket_requires_netprofit__c)
            {
                isGreen = false;
                break;
            }
        }
    }
    
    public DealSumaryDataObject(List<CS_Basket_Snapshot_Transactional__c> snapshots, Id basketId, cscfga__Product_Basket__c basket, List<CS_DealSummarySnapshotFields> snapFields){
        
        DealSummaryTotalOverviewData totalOverviewData = new DealSummaryTotalOverviewData(snapshots, basket);
        
        this.basketId = String.valueOf(basketId);
        this.snapshots = snapshots;
        this.totalOverviewData = totalOverviewData;
        this.snapshotFields = snapFields;

        isGreen = true;

        for(CS_Basket_Snapshot_Transactional__c snap : snapshots)
        {
            if(snap.Netprofit_trigger__c || snap.Basket_requires_netprofit__c)
            {
                isGreen = false;
                break;
            }
        }
    }
}