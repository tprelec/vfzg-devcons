/*
	Author: Rahul Sharma
	Description: Tests class for SFCC_OrderOverviewApi.cls
	Jira ticket: COM-3241
*/

@IsTest
private class TestSFCC_OrderOverviewApi {
	@TestSetup
	static void makeData() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();

		// create a test data
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOpportunityFieldMappings();
		TestUtils.createCompleteOpportunity();

		Account account = TestUtils.theAccount;
		Opportunity opportunity = TestUtils.theOpportunity;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opportunity, 'Test Basket');
		basket.Primary__c = true;
		basket.cscfga__Basket_Status__c = 'Contract Created';
		insert basket;

		VF_Contract__c contr = TestUtils.createVFContract(account, opportunity);
		OrderType__c ot = TestUtils.createOrderType();
		System.assertNotEquals(null, ot.Id, 'Order Type shouldnt be null');

		TestUtils.autoCommit = false;

		Order__c order = new Order__c(
			Status__c = 'In progress',
			Propositions__c = 'Legacy',
			OrderType__c = ot.Id,
			VF_Contract__c = contr.Id,
			Number_of_items__c = 100,
			O2C_Order__c = true,
			ProvisioningProcessStatus__c = 'Initializing'
		);
		insert order;

		System.assertNotEquals(null, order.Id, 'Order Id shouldnt be null');

		update new Order__c(Id = order.Id, Status__c = 'Submitted');

		order = [SELECT SFCC_Status__c, Status__c FROM Order__c WHERE Id = :order.Id];

		// verify the status of order as asserts later depends upon it
		System.assertEquals('Submitted', order.Status__c);

		csord__Order__c csOrder = CS_DataTest.createOrder();
		csOrder.csord__Account__c = account.Id;
		csOrder.csordtelcoa__Opportunity__c = opportunity.Id;
		csOrder.Name = 'TestOrder1';
		csOrder.csord__Status2__c = 'In Delivery';
		insert csOrder;

		csOrder = [SELECT SFCC_Status__c, csord__Status2__c FROM csord__Order__c WHERE Id = :csOrder.Id];

		// verify the status of CS order as asserts later depends upon it
		System.assertEquals('In Delivery', csOrder.csord__Status2__c);
	}

	@IsTest
	static void testMessageBadRequestDataFormat() {
		setRequest('Invalid JSON Request Body');

		Test.startTest();

		SFCC_OrderOverviewApi.getOverview();

		Test.stopTest();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_400, RestContext.response.statusCode);

		String responseString = RestContext.response.responseBody.toString();
		SFCC_OrderOverviewApi.ResponseWrapper responseWrapper = (SFCC_OrderOverviewApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderOverviewApi.ResponseWrapper.class
		);
		System.assertEquals(System.Label.SFCC_OrderApi_UnableToParseRequestMessage, responseWrapper.errorMessage);
		System.assertEquals(null, responseWrapper.orders);
	}

	@IsTest
	static void testMessageMandatoryAccountId() {
		setRequest('{"a": 1}');

		Test.startTest();

		SFCC_OrderOverviewApi.getOverview();

		Test.stopTest();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_400, RestContext.response.statusCode);

		String responseString = RestContext.response.responseBody.toString();
		SFCC_OrderOverviewApi.ResponseWrapper responseWrapper = (SFCC_OrderOverviewApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderOverviewApi.ResponseWrapper.class
		);
		System.assertEquals(System.label.SFCC_OrderApi_InvalidAccountIdMessage, responseWrapper.errorMessage);
		System.assertEquals(null, responseWrapper.orders);
	}

	@IsTest
	static void testOrdersSorting() {
		Date date1 = Date.today().addDays(10);
		Date date2 = Date.today();
		Date date3 = Date.today().addDays(-10);

		Test.startTest();

		List<SFCC_OrderOverviewApi.OrderWrapper> orders = new List<SFCC_OrderOverviewApi.OrderWrapper>{
			new SFCC_OrderOverviewApi.OrderWrapper(new Order__c(Order_Date__c = date2)),
			new SFCC_OrderOverviewApi.OrderWrapper(new Order__c(Order_Date__c = date1)),
			new SFCC_OrderOverviewApi.OrderWrapper(new Order__c(Order_Date__c = date1)),
			new SFCC_OrderOverviewApi.OrderWrapper(new Order__c(Order_Date__c = date3))
		};
		orders.sort();

		Test.stopTest();

		// Verify the sort order
		System.assertEquals(date1, orders[0].orderDate);
		System.assertEquals(date1, orders[1].orderDate);
		System.assertEquals(date2, orders[2].orderDate);
		System.assertEquals(date3, orders[3].orderDate);
	}

	@IsTest
	static void testRequestWithAccountId() {
		Account account = [SELECT Id FROM Account LIMIT 1];

		SFCC_OrderOverviewApi.RequestWrapper requestWrapper = new SFCC_OrderOverviewApi.RequestWrapper(account.Id, null, null, null);
		setRequest(JSON.serialize(requestWrapper));

		Test.startTest();

		SFCC_OrderOverviewApi.getOverview();

		Test.stopTest();

		String responseString = RestContext.response.responseBody.toString();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_200, RestContext.response.statusCode, responseString);
		SFCC_OrderOverviewApi.ResponseWrapper responseWrapper = (SFCC_OrderOverviewApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderOverviewApi.ResponseWrapper.class
		);
		System.assertEquals(null, responseWrapper.errorMessage);
		System.assertNotEquals(null, responseWrapper.orders);
		System.assertEquals(2, responseWrapper.orders.size());
	}

	@IsTest
	static void testRequestWithAccountIdDatesAndStatus() {
		Account account = [SELECT Id FROM Account LIMIT 1];

		SFCC_OrderOverviewApi.RequestWrapper requestWrapper = new SFCC_OrderOverviewApi.RequestWrapper(
			account.Id,
			new List<String>{ SFCC_OrderOverviewApi.ORDER_STATUS_IN_PROGRESS, SFCC_OrderOverviewApi.ORDER_STATUS_CANCELLED },
			Date.today().addYears(-1),
			Date.today().addYears(1)
		);
		setRequest(JSON.serialize(requestWrapper));

		Test.startTest();

		SFCC_OrderOverviewApi.getOverview();

		Test.stopTest();

		String responseString = RestContext.response.responseBody.toString();

		System.assertEquals(SFCC_OrderOverviewApi.SUCCESS_CODE_200, RestContext.response.statusCode, responseString);
		SFCC_OrderOverviewApi.ResponseWrapper responseWrapper = (SFCC_OrderOverviewApi.ResponseWrapper) JSON.deserialize(
			responseString,
			SFCC_OrderOverviewApi.ResponseWrapper.class
		);
		System.assertEquals(null, responseWrapper.errorMessage);
		System.assertNotEquals(null, responseWrapper.orders);
		System.assertNotEquals(0, responseWrapper.orders.size());
	}

	private static void setRequest(String requestBody) {
		RestRequest req = new RestRequest();
		req.requestURI = '/orders/getOverview';
		req.httpMethod = 'POST';
		RestContext.request = req;
		req.requestBody = Blob.valueOf(requestBody);
		RestResponse res = new RestResponse();
		RestContext.response = res;
	}
}
