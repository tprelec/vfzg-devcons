public with sharing class SendOrderConfirmationCtrl {
	private static final String NEW_CONFIRMATION = 'Send new confirmation (without signed quote)';
	private static final String SIGNED_CONFIRMATION = 'Send signed quote';

	private static final String ZIGGO_DOC_TEMPLATE_NAME = 'Ziggo Order Confirmation';
	private static final String ZIGGO_EMAIL_TEMPLATE_SIGNED = 'Ziggo Order Confirmation - Signed';
	private static final String ZIGGO_EMAIL_TEMPLATE_UNSIGNED = 'Ziggo Order Confirmation - Unsigned';
	private static final String ZIGGO_FROM_EMAIL = 'zakelijk@ziggo.com';

	private static final String VODAFONE_DOC_TEMPLATE_NAME = 'Vodafone Order Confirmation';
	private static final String VODAFONE_EMAIL_TEMPLATE_SIGNED = 'Vodafone Order Confirmation - Signed';
	private static final String VODAFONE_EMAIL_TEMPLATE_UNSIGNED = 'Vodafone Order Confirmation - Unsigned';
	private static final String VODAFONE_FROM_EMAIL = 'zakelijk@vodafone.nl';

	@AuraEnabled
	public static void validateOpportunity(Id oppId) {
		Opportunity opp = getOpportunity(oppId);
		if (opp.LG_PrimaryContact__r?.Email == null) {
			throw new AuraHandledException('Please check if primary contact is added and has a valid email address.');
		}
		List<OpportunityLineItem> olis = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :opp.Id];
		if (olis.isEmpty()) {
			throw new AuraHandledException('No Opportunity Products. Please make sure you have selected products before sending confirmation.');
		}
	}

	@AuraEnabled
	public static List<AuraSelectOption> getAvailableOptions(Id oppId) {
		Opportunity opp = getOpportunity(oppId);
		List<AuraSelectOption> options = new List<AuraSelectOption>();
		options.add(new AuraSelectOption(NEW_CONFIRMATION, NEW_CONFIRMATION));
		if (opp.LG_SignedQuoteAvailable__c == 'true') {
			options.add(new AuraSelectOption(SIGNED_CONFIRMATION, SIGNED_CONFIRMATION));
		}
		return options;
	}

	@AuraEnabled
	public static mmdoc__Document_Request__c createNewConfirmation(Id oppId) {
		Opportunity opp = getOpportunity(oppId);
		String docTemplateName = opp.Journey_Type__c == 'Mobile' ? VODAFONE_DOC_TEMPLATE_NAME : ZIGGO_DOC_TEMPLATE_NAME;
		mmdoc__Document_Template__c docTemplate = MavenDocumentsService.getDocumentTemplate(docTemplateName);
		return MavenDocumentsService.createDocRequest(oppId, docTemplate, 'Attachment', 'PDF', 'Queue', true);
	}

	@AuraEnabled
	public static mmdoc__Document_Request__c getDocumentRequest(Id requestId) {
		return MavenDocumentsService.getDocumentRequest(requestId);
	}

	@AuraEnabled
	public static void sendNewConfirmation(Id oppId, Id attachmentId) {
		Opportunity opp = getOpportunity(oppId);
		// If already in this state, it will be sent via flow
		if (opp.StageName == 'Customer Approved' && opp.LG_SignedQuoteAvailable__c != 'true') {
			return;
		}
		String templateName = opp.Journey_Type__c == 'Mobile' ? VODAFONE_EMAIL_TEMPLATE_UNSIGNED : ZIGGO_EMAIL_TEMPLATE_UNSIGNED;
		sendConfirmation(oppId, attachmentId, getEmailTemplate(templateName).Id);
	}

	@AuraEnabled
	public static void sendSignedConfirmation(Id oppId) {
		Opportunity opp = getOpportunity(oppId);
		String templateName = opp.Journey_Type__c == 'Mobile' ? VODAFONE_EMAIL_TEMPLATE_SIGNED : ZIGGO_EMAIL_TEMPLATE_SIGNED;
		sendConfirmation(oppId, (Id) opp.LG_SignedQuoteAttachmentId__c, getEmailTemplate(templateName).Id);
	}

	private static void sendConfirmation(Id oppId, Id attachmentId, Id emailTemplateId) {
		Opportunity opp = getOpportunity(oppId);
		String fromEmail = opp.Journey_Type__c == 'Mobile' ? VODAFONE_FROM_EMAIL : ZIGGO_FROM_EMAIL;
		OrgWideEmailAddress fromAddress = EmailService.getOrgWideEmailAddress(fromEmail);
		List<Id> attachments = new List<Id>{ attachmentId };
		if (String.isNotBlank(opp.Contract_Summary_Attachment_Id__c)) {
			attachments.add((Id) opp.Contract_Summary_Attachment_Id__c);
		}
		EmailService.sendEmail(opp.LG_PrimaryContact__c, opp.Id, null, null, null, emailTemplateId, new List<Id>{ attachmentId }, fromAddress.Id);
	}

	private static Opportunity getOpportunity(Id oppId) {
		return [
			SELECT
				Id,
				StageName,
				LG_SignedQuoteAvailable__c,
				LG_SignedQuoteAttachmentId__c,
				Contract_Summary_Attachment_Id__c,
				LG_PrimaryContact__c,
				LG_PrimaryContact__r.Email,
				Journey_Type__c
			FROM Opportunity
			WHERE Id = :oppId
		];
	}

	private static EmailTemplate getEmailTemplate(String name) {
		return [SELECT Id FROM EmailTemplate WHERE Name = :name];
	}
}
