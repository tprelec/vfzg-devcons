/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 */
@isTest
private class TestFieldSyncMappingTriggerHandler {
	
    static TestMethod void TestFieldMappingCreationAndUpdate() {
    	Field_Sync_Mapping__c fsm = new Field_Sync_Mapping__c();
    	fsm.Object_Mapping__c = 'Opportunity -> VF_Contract__c';
    	fsm.Source_Field_Name__c = 'AccountId';
    	fsm.Target_Field_Name__c = 'Account__c';
    	insert fsm;
    	System.Assert(fsm.Id != null,'Record should have saved successfully.');
    	
    	fsm.Target_Field_Name__c = 'Owner__c';
    	update fsm;
    	
    	
    }
    	
    
    static TestMethod void TestFieldMappingCreationSourceObjectError() {
    	Field_Sync_Mapping__c fsm = new Field_Sync_Mapping__c();
    	fsm.Object_Mapping__c = 'Oportunity -> VF_Contract__c';
    	fsm.Source_Field_Name__c = 'AccountId';
    	fsm.Target_Field_Name__c = 'Account__c';
    	try{
    		insert fsm;
    	} catch (Exception e){}
    	
    	System.Assert(fsm.Id == null,'Record insert should have failed.');    	    	
    }

    static TestMethod void TestFieldMappingCreationTargetObjectError() {
    	Field_Sync_Mapping__c fsm = new Field_Sync_Mapping__c();
    	fsm.Object_Mapping__c = 'Opportunity -> VF_Contract';
    	fsm.Source_Field_Name__c = 'AccountId';
    	fsm.Target_Field_Name__c = 'Account__c';
    	try{
    		insert fsm;
    	} catch (Exception e){}
    	
    	System.Assert(fsm.Id == null,'Record insert should have failed.');    	    	
    }

    static TestMethod void TestFieldMappingCreationSourceFieldError() {
    	Field_Sync_Mapping__c fsm = new Field_Sync_Mapping__c();
    	fsm.Object_Mapping__c = 'Opportunity -> VF_Contract__c';
    	fsm.Source_Field_Name__c = 'AccountId__c';
    	fsm.Target_Field_Name__c = 'Account__c';
    	try{
    		insert fsm;
    	} catch (Exception e){}
    	
    	System.Assert(fsm.Id == null,'Record insert should have failed.');    	    	
    }

    static TestMethod void TestFieldMappingCreationTargetFieldError() {
    	Field_Sync_Mapping__c fsm = new Field_Sync_Mapping__c();
    	fsm.Object_Mapping__c = 'Opportunity -> VF_Contract__c';
    	fsm.Source_Field_Name__c = 'AccountId';
    	fsm.Target_Field_Name__c = 'Account';
    	try{
    		insert fsm;
    	} catch (Exception e){}
    	
    	System.Assert(fsm.Id == null,'Record insert should have failed.');    	    	
    }
    


}