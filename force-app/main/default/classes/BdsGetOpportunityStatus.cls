@RestResource(urlMapping='/getopportunitystatus/*')
global without sharing class BdsGetOpportunityStatus {

    @HttpPost
    global static void doPost(String opportunityID) {
        RestResponse res = RestContext.response;
        if (res == null) {
            res = new RestResponse();
            RestContext.response = res;
            res.addHeader('Content-Type', 'text/json');
            res.statusCode = 200;
        }
        /** Validate the incoming body values */
        BdsResponseClasses.ValidationErrorReturnClass returnClass = new BdsResponseClasses.ValidationErrorReturnClass();
        List<BdsResponseClasses.ValidationError> errFields = new List<BdsResponseClasses.ValidationError>{new BdsResponseClasses.ValidationError('kvk', 'kvk Missing')};
        returnClass.errors = errFields;

        BdsResponseClasses.OpportunityStatusClass validatedResultClass = BdsGetOpportunityStatus.validateValues(opportunityID);
        validatedResultClass.opportunityId = opportunityID;
        if (!validatedResultClass.isSuccess && !String.isEmpty(validatedResultClass.errorMessage)) {
            res.responseBody = Blob.valueOf(JSON.serializePretty(validatedResultClass));
            res.statusCode = 400;
        } else {
            res.responseBody = Blob.valueOf(JSON.serializePretty(validatedResultClass));
        }
    }

    public static BdsResponseClasses.OpportunityStatusClass validateValues(String opportunityID) {
        Boolean errorFound = false;
        BdsResponseClasses.OpportunityStatusClass returnClass = new BdsResponseClasses.OpportunityStatusClass();
        if (String.isEmpty(opportunityID)) {
            returnClass.errorMessage = 'No Opportunity ID provided';
            errorFound = true;
        }
        if (!errorFound) {
            try {
                Id oppId = opportunityID;
            } catch (Exception e) {
                returnClass.errorMessage = 'No valid Opportunity ID provided';
                errorFound = true;
            }
        }
        /** Query to see if there actually is an Opportunity with this ID */
        List<Opportunity> opps;
        if (!errorFound) {
            try {
                opps = [SELECT Id, StageName, BAN__r.BAN_Number__c FROM Opportunity WHERE Id = :opportunityID];
            } catch (Exception e) {
                returnClass.errorMessage = 'No opportunity found with this ID';
                errorFound = true;
            }
        }
        /** Check to see if the opportunity is already closed */
        if (!errorFound) {
            if (opps.size() == 1) {
                returnClass.isSuccess = true;
                returnClass.opportunityStageName = opps[0].StageName;
                returnClass.billingCustomerId = String.isNotBlank(opps[0].BAN__r.BAN_Number__c) ? opps[0].BAN__r.BAN_Number__c : '';
            } else {
                returnClass.errorMessage = 'Opportunity not found';
            }
        }
        return returnClass;
    }
}