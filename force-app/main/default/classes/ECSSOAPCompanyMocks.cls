@isTest
@SuppressWarnings('PMD.ExcessiveParameterList')
public class ECSSOAPCompanyMocks {
	public class UpdateResellersResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			Account refAccPartner = [SELECT Id, BOPCode__c FROM Account WHERE Name = 'TestAccount Reseller' LIMIT 1];
			ECSSOAPCompany.resellerResponse_element responseX = new ECSSOAPCompany.resellerResponse_element();
			ECSSOAPCompany.companyResponseType[] resellers = new List<ECSSOAPCompany.companyResponseType>{};
			responseX.reseller = resellers;
			ECSSOAPCompany.companyResponseType myResponse = new ECSSOAPCompany.companyResponseType();
			myResponse.corporateId = null;
			myResponse.bopCode = 'XYZ';
			myResponse.referenceId = refAccPartner.Id;
			myResponse.errorCode = '0';
			myResponse.errorMessage = 'OK';
			responseX.reseller.add(myResponse);
			response.put('response_x', responseX);
		}
	}

	public class CreateResellersResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			Account refAccPartner = [SELECT Id, BOPCode__c FROM Account WHERE Name = 'TestAccount Reseller' LIMIT 1];
			ECSSOAPCompany.resellerResponse_element responseX = new ECSSOAPCompany.resellerResponse_element();
			ECSSOAPCompany.companyResponseType[] resellers = new List<ECSSOAPCompany.companyResponseType>{};
			responseX.reseller = resellers;
			ECSSOAPCompany.companyResponseType myResponse = new ECSSOAPCompany.companyResponseType();
			myResponse.corporateId = null;
			myResponse.bopCode = 'WXY';
			myResponse.referenceId = refAccPartner.Id;
			myResponse.errorCode = '0';
			myResponse.errorMessage = 'OK';
			responseX.reseller.add(myResponse);
			response.put('response_x', responseX);
		}
	}
	public class CreateCustomersResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			Account refAccPartner = [SELECT Id, BOPCode__c FROM Account WHERE Name = 'TestAccount Customer' LIMIT 1];
			ECSSOAPCompany.customerResponse_element responseX = new ECSSOAPCompany.customerResponse_element();
			ECSSOAPCompany.companyResponseType[] customers = new List<ECSSOAPCompany.companyResponseType>{};
			responseX.customer = customers;
			ECSSOAPCompany.companyResponseType myResponse = new ECSSOAPCompany.companyResponseType();
			myResponse.corporateId = null;
			myResponse.bopCode = 'TES';
			myResponse.referenceId = refAccPartner.Id;
			myResponse.errorCode = '0';
			myResponse.errorMessage = 'OK';
			responseX.customer.add(myResponse);
			response.put('response_x', responseX);
		}
	}
	public class UpdateCustomersResponse implements WebServiceMock {
		public void doInvoke(
			Object stub,
			Object request,
			Map<String, Object> response,
			String endpoint,
			String soapAction,
			String requestName,
			String responseNS,
			String responseName,
			String responseType
		) {
			Account refAccPartner = [SELECT Id, BOPCode__c FROM Account WHERE Name = 'TestAccount Customer' LIMIT 1];
			ECSSOAPCompany.customerResponse_element responseX = new ECSSOAPCompany.customerResponse_element();
			ECSSOAPCompany.companyResponseType[] customers = new List<ECSSOAPCompany.companyResponseType>{};
			responseX.customer = customers;
			ECSSOAPCompany.companyResponseType myResponse = new ECSSOAPCompany.companyResponseType();
			myResponse.corporateId = null;
			myResponse.bopCode = 'TES';
			myResponse.referenceId = refAccPartner.Id;
			myResponse.errorCode = '0';
			myResponse.errorMessage = 'OK';
			responseX.customer.add(myResponse);
			response.put('response_x', responseX);
		}
	}
}
