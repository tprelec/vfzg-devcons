@isTest
public with sharing class TestCOM_ApplyInflightChangeController {
	@TestSetup
	private static void createTestData() {
		Account account = CS_DataTest.createAccount('Test Account');
		insert account;

		Contact testContact = CS_DataTest.createContact('Test Name', 'TestLastName', 'Partner', 'mail@address.com', account.Id);
		testContact.Phone = '+31123121212';
		testContact.Title = 'Ms';
		insert testContact;

		Opportunity opportunity = CS_DataTest.createOpportunity(account, 'Test Opp', UserInfo.getUserId());
		opportunity.Main_Contact_Person__c = testContact.Id;
		insert opportunity;

		Id recordTypeId = Schema.SObjectType.cscfga__Product_Basket__c.getRecordTypeInfosByDeveloperName().get('Inflight_Change').getRecordTypeId();

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(opportunity, 'Test Basket');
		basket.RecordTypeId = recordTypeId;
		basket.Primary__c = true;
		insert basket;
	}

	@isTest
	public static void testGetProductBasketId() {
		Opportunity opp = [SELECT Id FROM Opportunity WHERE Name = 'Test Opp'];
		cscfga__Product_Basket__c pb = [SELECT Id FROM cscfga__Product_Basket__c];

		Test.startTest();
		Id pbId = COM_ApplyInflightChangeController.getProductBasketId(opp.Id);
		Test.stopTest();

		System.assertEquals(pb.Id, pbId, 'Id is not the same');
	}

	@isTest
	public static void testApplyInflightChangeActionFailure() {
		cscfga__Product_Basket__c pb = [SELECT Id FROM cscfga__Product_Basket__c];

		Boolean exceptionOccured = false;
		Test.startTest();

		try {
			COM_ApplyInflightChangeController.performAction(pb.Id);
		} catch (Exception e) {
			exceptionOccured = true;
		}
		Test.stopTest();

		System.assertEquals(true, exceptionOccured, 'Exception should occure.');
	}
}
