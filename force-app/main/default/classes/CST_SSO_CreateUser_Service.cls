@RestResource(urlMapping='/CreateUser')
global without sharing class CST_SSO_CreateUser_Service {
	public CST_SSO_CreateUser_Service() {
	}

	@HttpPost
	global static String createUser() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		String response = '';

		try {
			String reqBody = req.requestBody.toString();

			if (reqBody != null) {
				response = parseXML(reqBody);
			} else {
				response = 'XML Request was not provided.';
			}
		} catch (System.DmlException e) {
			System.debug('CreateUser::: DmlException exception caught->' + e.getMessage());
			response = e.getMessage();
		} catch (Exception e) {
			System.debug('CreateUser::: Unknown Exception exception caught->' + e.getMessage());
			response = e.getMessage();
		}

		return response;
	}

	public static String parseXML(String url) {
		String response = '';
		Dom.Document doc = new Dom.Document();
		doc.load(url);

		Dom.XMLNode root = doc.getRootElement();
		Dom.XMLNode bodyElement = root.getChildElement('Body', 'http://schemas.xmlsoap.org/soap/envelope/');
		Dom.XMLNode createRequest = bodyElement.getChildElement('createUsersRequest', 'http://sb.ecs.vodafone.nl/servicebus/');
		Dom.XMLNode userNode = createRequest.getChildElement('user', null);
		String accRefId = userNode.getChildElement('company', null).getChildElement('referenceId', null).getText();
		List<Account> accList = [SELECT id, name FROM Account WHERE Unify_Ref_Id__c = :accRefId LIMIT 1];
		response = accList.size() == 0
			? 'Account does not exist in Salesforce. Please provide a valid ReferenceId'
			: validateUserInSalesforce(userNode, accList[0].Id);
		return response;
	}

	private static String validateUserInSalesforce(Dom.XMLNode userNode, Id accountId) {
		String contactExtId = userNode.getChildElement('referenceId', null).getText();
		String response = '';
		List<Contact> existingContactList = [SELECT id, Userid__c FROM Contact WHERE Unify_Ref_Id__c = :contactExtId];
		List<User> existingUser = [
			SELECT id, username, CommunityNickname, isActive, IsPortalEnabled
			FROM User
			WHERE CommunityNickname = :contactExtId
		];

		//3 scenarios
		//The user and Contact don't exist in salesforce {}>>> Create a new Contact and User associated with the correct account
		if (existingContactList.size() == 0 && existingUser.size() == 0) {
			response = createPortalUser(userNode, accountId);
			if (String.isBlank(response)) {
				response = 'Customer Portal User created successfully!';
			}
		} else if (existingUser.size() == 0 && existingContactList.size() > 0) {
			//The user doesn't exist but the contact yes >>> Create a new user and associate with the contact **
			response = createPortalUser(userNode, accountId);
			if (String.isBlank(response)) {
				response = 'User created successfully. Contact ' + contactExtId + ' is now an active Portal User!';
			}
		} else if (existingUser.size() > 0 && existingContactList.size() > 0) {
			//The user exists in salesforce but it's inactive >>> Activate the user and associated contact. Make sure the account is correct
			if (!existingUser[0].isActive && existingUser[0].IsPortalEnabled) {
				existingUser[0].isActive = true;
				update existingUser[0];
				response = 'Customer Portal User ' + contactExtId + ' was Inactive and it is now Active.';
			} else if (!existingUser[0].IsPortalEnabled) {
				existingUser[0].CommunityNickname = null;
				update existingUser[0];
				System.debug('existingUser[0] -> ' + existingUser[0]);
				response = createPortalUser(userNode, accountId);
				if (String.isBlank(response)) {
					response = 'Customer Portal User ' + contactExtId + ' was no longer a Portal user, and a new user was created.';
				}
			} else {
				response = 'Customer User ' + contactExtId + ' already exists as an active Portal User.';
			}
		}

		return response;
	}

	private static Contact createOrUpdateContact(Dom.XMLNode userNode, String contactExtId, Id accountId) {
		Contact newContact = new Contact(
			AccountId = accountId,
			Unify_Ref_Id__c = contactExtId,
			Gender__c = 'M/F',
			Salutation = userNode.getChildElement('prefix', null).getText(),
			FirstName = userNode.getChildElement('firstname', null).getText(),
			Lastname = userNode.getChildElement('lastname', null).getText(),
			Title = userNode.getChildElement('jobTitle', null).getText(),
			Phone = userNode.getChildElement('phone', null).getText(),
			MobilePhone = userNode.getChildElement('mobile', null).getText(),
			Fax = userNode.getChildElement('fax', null).getText(),
			Language__c = userNode.getChildElement('language', null).getText(),
			Description = userNode.getChildElement('notes', null).getText(),
			Email = userNode.getChildElement('email', null).getText(),
			MailingStreet = userNode.getChildElement('address', null).getText(),
			MailingPostalCode = userNode.getChildElement('zipcode', null).getText(),
			MailingCity = userNode.getChildElement('city', null).getText(),
			MailingState = userNode.getChildElement('county', null).getText(),
			MailingCountry = userNode.getChildElement('countryId', null).getText(),
			OtherStreet = userNode.getChildElement('address_1', null).getText()
		);

		upsert newContact Unify_Ref_Id__c;

		return newContact;
	}

	private static void createUser(Dom.XMLNode userNode, Contact newContact) {
		String firstname = userNode.getChildElement('firstname', null).getText();
		String lastname = userNode.getChildElement('lastname', null).getText();
		Profile portalProfile = [SELECT Id, name FROM Profile WHERE name = 'VF CST Customer' LIMIT 1];

		User usr = new User(
			FirstName = firstname,
			LastName = lastname,
			Title = userNode.getChildElement('jobTitle', null).getText(),
			Email = userNode.getChildElement('email', null).getText(),
			Street = userNode.getChildElement('address', null).getText(),
			PostalCode = userNode.getChildElement('zipcode', null).getText(),
			City = userNode.getChildElement('city', null).getText(),
			State = userNode.getChildElement('county', null).getText(),
			Country = userNode.getChildElement('countryId', null).getText(),
			Phone = userNode.getChildElement('phone', null).getText(),
			MobilePhone = userNode.getChildElement('mobile', null).getText(),
			Fax = userNode.getChildElement('fax', null).getText(),
			Alias = '' + firstname.substring(0, 1) + lastname.substring(0, 4),
			username = userNode.getChildElement('email', null).getText() + '.devp2',
			CommunityNickname = newContact.Unify_Ref_Id__c,
			TimeZoneSidKey = 'Europe/Amsterdam',
			LocaleSidKey = 'nl_NL',
			LanguageLocaleKey = 'nl_NL',
			ProfileId = portalProfile.Id,
			EmailEncodingKey = 'ISO-8859-1',
			ContactId = newContact.Id
		);
		insert usr;

		assignPermissionSets(usr.Id);
	}

	private static String createPortalUser(Dom.XMLNode userNode, Id accountId) {
		String response = '';
		String contactExtId = userNode.getChildElement('referenceId', null).getText();

		try {
			Contact newContact = createOrUpdateContact(userNode, contactExtId, accountId);
			createUser(userNode, newContact);
		} catch (System.DmlException e) {
			System.debug('CreateUser::: DmlException exception caught->' + e.getMessage());
			response = e.getMessage();
		} catch (Exception e) {
			System.debug('CreateUser::: Unknown Exception exception caught->' + e.getMessage());
			response = e.getMessage();
		}

		return response;
	}

	@future
	public static void assignPermissionSets(Id usrId) {
		PermissionSet permSet = [SELECT id, name FROM PermissionSet WHERE name = 'CST_Customer_Permission_Set_Group' LIMIT 1];
		PermissionSetAssignment psa = new PermissionSetAssignment(PermissionSetId = permSet.Id, AssigneeId = usrId);
		insert psa;
	}
}
