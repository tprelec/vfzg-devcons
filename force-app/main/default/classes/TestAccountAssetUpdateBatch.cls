@isTest
private class TestAccountAssetUpdateBatch {
	private static final String CTN_STATUS_ACTIVE = 'Active';
	private static final String PRICEPLAN_CLASS_VOICE = 'Voice';
	private static final String PRICEPLAN_CLASS_DATA = 'Data';
	private static final Date END_DATE = Date.today() + 121;

	@isTest
	static void AssetCount() {
		// create an account and all objects needed for that
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		User u = TestUtils.createManager();

		VF_Asset__c vfaVoice = new VF_Asset__c(
			Account__c = acct.id,
			Contract_End_Date__c = END_DATE,
			CTN_Status__c = CTN_STATUS_ACTIVE,
			Priceplan_Class__c = PRICEPLAN_CLASS_VOICE
		);
		insert vfaVoice;

		VF_Asset__c vfaData = new VF_Asset__c(
			Account__c = acct.id,
			Contract_End_Date__c = END_DATE,
			CTN_Status__c = CTN_STATUS_ACTIVE,
			Priceplan_Class__c = PRICEPLAN_CLASS_DATA
		);
		insert vfaData;

		test.startTest();

		AccountAssetUpdateBatch controller = new AccountAssetUpdateBatch();
		Database.executeBatch(controller, 1);

		test.stopTest();
	}

	@isTest
	static void canSchedule() {
		AccountAssetUpdateBatch controller = new AccountAssetUpdateBatch();
		String start = '0 0 23 * * ?';
		system.schedule('Test AccountAssetUpdateBatch', start, controller);
	}
}
