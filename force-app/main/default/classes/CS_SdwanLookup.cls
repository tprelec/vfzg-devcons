global with sharing class CS_SdwanLookup extends cscfga.ALookupSearch {
	public override Object[] doLookupSearch(
		Map<String, String> searchFields,
		String productDefinitionID,
		Id[] excludeIds,
		Integer pageOffset,
		Integer pageLimit
	) {
		System.debug(Logginglevel.DEBUG, '****searchfields CS_SdwanLookup: ' + JSON.serializePretty(searchFields));

		final Integer selectListLookupPageLimit = pageLimit + 1;
		Integer recordOffset = pageOffset * pageLimit;

		return [
			SELECT
				Id,
				Name,
				Max_Duration__c,
				Min_Duration__c,
				cspmb__Is_Active__c,
				cspmb__Recurring_Charge__c,
				cspmb__One_Off_Charge__c,
				Category__c,
				cspmb__Recurring_Charge_External_Id__c,
				Recurring_Charge_Product__c,
				One_Off_Charge_Product__c,
				cspmb__One_Off_Cost__c,
				cspmb__recurring_cost__c,
				VFZ_Commercial_Article_Name__c,
				VFZ_Commercial_Component_Name__c,
				Contract_Term_Number__c,
				One_Off_Charge_Product__r.Name,
				One_Off_Charge_Product__r.ThresholdGroup__c,
				Recurring_Charge_Product__r.Name,
				Recurring_Charge_Product__r.ThresholdGroup__c,
				cspmb__Contract_Term__c,
				cspmb__Price_Item_Code__c
			FROM cspmb__Price_Item__c
			WHERE cspmb__Is_Active__c = TRUE AND cspmb__Price_Item_Code__c = 'Flex CPE'
			ORDER BY Name ASC
			LIMIT :selectListLookupPageLimit
			OFFSET :recordOffset
		];
	}

	public override String getRequiredAttributes() {
		return '["ContractTerm"]';
	}

	public static List<cspmb__Price_Item__c> getSdwans() {
		// no input params at this moment

		return [
			SELECT
				Id,
				Name,
				Max_Duration__c,
				Min_Duration__c,
				cspmb__Is_Active__c,
				cspmb__Recurring_Charge__c,
				cspmb__One_Off_Charge__c,
				Category__c,
				cspmb__Recurring_Charge_External_Id__c,
				Recurring_Charge_Product__c,
				One_Off_Charge_Product__c,
				cspmb__One_Off_Cost__c,
				cspmb__recurring_cost__c,
				VFZ_Commercial_Article_Name__c,
				VFZ_Commercial_Component_Name__c,
				Contract_Term_Number__c,
				One_Off_Charge_Product__r.Name,
				One_Off_Charge_Product__r.ThresholdGroup__c,
				Recurring_Charge_Product__r.Name,
				Recurring_Charge_Product__r.ThresholdGroup__c,
				cspmb__Contract_Term__c,
				cspmb__Price_Item_Code__c
			FROM cspmb__Price_Item__c
			WHERE cspmb__Is_Active__c = TRUE AND cspmb__Price_Item_Code__c = 'Flex CPE'
		];
	}
}
