@isTest
public with sharing class TestBdsRestGetOpportunityId {
	@isTest
	public static void testDoGetPositive() {
		TestUtils.autocommit = true;
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, null);
		Billing_Arrangement__c bar = TestUtils.createBillingArrangement(fa);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		opp.Ban__c = ban.Id;
		opp.Financial_Account__c = fa.Id;
		opp.Billing_Arrangement__c = bar.Id;
		update opp;

		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		Order__c ord = TestUtils.createOrder(contr);
		ord.Billing_Arrangement__c = bar.Id;
		update ord;

		RestRequest request = new RestRequest();
		RestResponse res = new RestResponse();
		String orderName = [SELECT Id, Name FROM Order__c WHERE Id = :ord.Id].Name;

		request.requestUri = URL.getSalesforceBaseUrl().toExternalForm() + '/apexrest/getopportunityid/' + orderName;

		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = res;

		Test.startTest();
		BdsRestGetOpportunityId.doGetResponse();
		Test.stopTest();

		System.assert(res.responseBody.toString().contains('opportunityNumber'), 'The API response should contain an associated opportunity Id');
	}

	@isTest
	public static void testDoGetNegative() {
		RestRequest request = new RestRequest();
		RestResponse res = new RestResponse();

		request.requestUri = URL.getSalesforceBaseUrl().toExternalForm() + '/apexrest/getopportunityid/' + 'WrongValue';

		request.httpMethod = 'GET';
		RestContext.request = request;
		RestContext.response = res;

		Test.startTest();
		BdsRestGetOpportunityId.doGetResponse();
		Test.stopTest();

		System.assert(res.responseBody.toString().contains('error'), 'The API response should return an error');
	}
}
