global with sharing class SiteDetails implements Comparable{
    
    @AuraEnabled
    public Id siteId;
    
    @AuraEnabled
    public Id siteCId;
    
     @AuraEnabled
    public Boolean selectedPrev;
    
    @AuraEnabled
    public String pbx;
    
    @AuraEnabled
    public Boolean applicable;
    
    @AuraEnabled
    public Boolean isOrigin;
    
    @AuraEnabled
    public Boolean showInUI;
    
    @AuraEnabled
    public Boolean configExists;
    
    @AuraEnabled
    public String rootPcrId;
    
    @AuraEnabled
    public List<FieldValues> siteFields;
    
    @AuraEnabled
    public List<FieldValues> comparissonAttributes;

    public SiteDetails(){}
    
    public SiteDetails(Id siteId, Id siteCId,String pbx, List<FieldValues> siteFields) {
        this.siteId = siteId;
        this.siteCId = siteCId;
        this.pbx = pbx;
        this.siteFields = siteFields;
        this.applicable = false;
        this.showInUI = false;
        this.configExists = false;
        this.rootPcrId = '';
        this.isOrigin = false;
        this.selectedPrev = false;
        this.comparissonAttributes = new List<FieldValues>();

    }
    
    
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        SiteDetails compareToSite = (SiteDetails)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        if (this.applicable == true &&  compareToSite.applicable == false) {
            // Set return value to a positive value.
            returnValue = -1;
        } else if (this.applicable == false &&  compareToSite.applicable == true) {
            // Set return value to a negative value.
            returnValue = 1;
        }
        
        return returnValue;       
    }

}