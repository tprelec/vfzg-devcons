@isTest
public class CS_OneNetSingleLookupTest {
private static testMethod void testRequiredAttributes(){
         CS_OneNetSingleLookup caLookup = new  CS_OneNetSingleLookup();
        caLookup.getRequiredAttributes();
    }
  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
     System.runAs (simpleUser) { 
      
     
      Framework__c frameworkSetting = new Framework__c();
      frameworkSetting.Framework_Sequence_Number__c = 2;
      insert frameworkSetting;
      
      PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
     insert priceResetSetting;
      Account testAccount = CS_DataTest.createAccount('Test Account');
      insert testAccount;
      
      Sales_Settings__c ssettings = new Sales_Settings__c();
      ssettings.Postalcode_check_validity_days__c = 2;
      ssettings.Max_Daily_Postalcode_Checks__c = 2;
      ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
      ssettings.Postalcode_check_block_period_days__c = 2;
      ssettings.Max_weekly_postalcode_checks__c = 15;
      insert ssettings;
      
      
      
      
      
      Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
      insert testOpp;
      
      
      cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        insert basket;
        
       
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
        
        cscfga__Product_Definition__c oneNetFixedDef = CS_DataTest.createProductDefinition('OneNetFixed');
        oneNetFixedDef.Product_Type__c = 'Fixed';
        oneNetFixedDef.RecordTypeId = productDefinitionRecordType;
        insert oneNetFixedDef;
        
        
        
        
        cscfga__Product_Configuration__c oneNetFixedConf = CS_DataTest.createProductConfiguration(oneNetFixedDef.Id, 'OneNetFixed',basket.Id);
        oneNetFixedConf.cscfga__Root_Configuration__c = null;
        oneNetFixedConf.cscfga__Parent_Configuration__c = null;
        insert oneNetFixedConf;
        
        
        
       
        csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(oneNetFixedConf);
        pcr.csbb__Product_Configuration__c = oneNetFixedConf.Id;
        insert pcr;
        
        
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('Contract Duration','36');
        searchFields.put('Scenario','One Net Express');
        searchFields.put('Deal Type','Retention');
        
        
        CS_OneNetSingleLookup oneNetLookup = new CS_OneNetSingleLookup();
        oneNetLookup.doLookupSearch(searchFields, String.valueOf(oneNetFixedDef.Id),null, 0, 0);
        
        searchFields.put('Scenario','One Net Enterprise');
        oneNetLookup.doLookupSearch(searchFields, String.valueOf(oneNetFixedDef.Id),null, 0, 0);
     
     }
  }

}