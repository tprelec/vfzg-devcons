global class COM_DXLServiceAsyncResponse {
	public Payload payload;

	global class Payload {
		public String transactionId;
		public String state;
		public CustomerDetails customerDetails;
		public BillingEntities billingEntities;
		public List<Contact> contacts;
		public List<Site> sites;
	}

	global class CustomerDetails {
		public CompanyDetails companyDetails;
		public BillingCustomer billingCustomer;
	}

	global class CompanyDetails {
		public String unifyAccountRefId;
		public String bopCompanyCode;
		public ErrorMessage error;
	}

	global class BillingCustomer {
		public String unifyBillingCustomerRefId;
		public ErrorMessage error;
	}

	global class BillingEntities {
		public String unifyFinancialAccountRefId;
		public String unifyBillingAccountRefId;
		public ErrorMessage error;
	}

	global class Contact {
		public String unifyContactRefId;
		public String salesforceAccountId;
		public String salesforceContactId;
		public ErrorMessage error;
	}

	global class Site {
		public String unifySiteRefId;
		public String salesforceSiteId;
		public String salesforceAccountId;
		public String siteLevelServicesAPId;
		public ErrorMessage error;
	}

	global class ErrorMessage {
		public String code;
		public String reason;
		public String message;
	}
}
