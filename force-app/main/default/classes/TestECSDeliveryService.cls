@isTest
private class TestECSDeliveryService {
	@TestSetup
	static void makeData() {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OrderTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContractedProductsTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('OpportunityLineItemTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('VF_ContractTriggerHandler', null, 0);

		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		Ban__c ban = TestUtils.createBan(acct);
		Site__c site = TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		Contact c = new Contact(AccountId = acct.Id, LastName = 'Test');
		insert c;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, c.Id);
		Billing_Arrangement__c ba = TestUtils.createBillingArrangement(fa);

		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct();
		insert product;

		Opportunity newOpp = [SELECT Id, Ban__c FROM Opportunity LIMIT 1];
		newOpp.Ban__c = ban.Id;
		newOpp.Name = 'Test Opp Delivery';
		update newOpp;

		Order__c ord = new Order__c();
		ord.O2C_Order__c = true;
		ord.Sales_Order_Id__c = '12345';
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = contr.Id;
		ord.Account__c = acct.Id;
		insert ord;

		Customer_Asset__c ca = new Customer_Asset__c();
		ca.Billing_Arrangement__c = ba.Id;
		ca.Order__c = ord.Id;
		ca.Site__c = site.Id;
		insert ca;

		Competitor_Asset__c compAsset = new Competitor_Asset__c();
		insert compAsset;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.Order__c = ord.Id;
		cp.External_Reference_Id__c = '7777777';
		cp.CLC__c = 'Acq';
		cp.VF_Contract__c = contr.Id;
		cp.Customer_Asset__c = ca.Id;
		cp.Product__c = product.Id;
		cp.PBX__c = compAsset.Id;
		insert cp;
	}

	@isTest
	static void testMakeUpdateRequest() {
		Contracted_Products__c objContractedProduct = [
			SELECT
				Id,
				Name,
				Quantity__c,
				BOP_Status__c,
				Order__r.Id,
				External_Reference_Id__c,
				Group__c,
				Cost_Center__c,
				Order__r.Sales_Order_Id__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Postal_Code__c,
				Customer_Asset__r.Installed_Base_Id__c
			FROM contracted_Products__c
			WHERE External_Reference_Id__c = '7777777'
		];

		ECSDeliveryService deliveryService = new ECSDeliveryService();
		List<ECSDeliveryService.request> requests = new List<ECSDeliveryService.request>();
		ECSDeliveryService.request req = new ECSDeliveryService.request();

		req.deliveryNumber = objContractedProduct.Order__r.Sales_Order_Id__c;
		req.referenceId = 'not Used'; //based on ECSDeliveryService this value is no used/null

		req.deliveryRows = new List<ECSSOAPDelivery.deliveryRowType>();
		ECSSOAPDelivery.deliveryRowType objDeliveryRow = new ECSSOAPDelivery.deliveryRowType();
		objDeliveryRow.id = objContractedProduct.External_Reference_Id__c;

		objDeliveryRow.additionalReference = new ECSSOAPDelivery.additionalRefType();
		ECSSOAPDelivery.customReferenceType addCustRefType = new ECSSOAPDelivery.customReferenceType();
		addCustRefType.name = 'installedBaseId';
		addCustRefType.value = objContractedProduct.Customer_Asset__r.Installed_Base_Id__c;
		objDeliveryRow.additionalReference.customReference = addCustRefType;

		objDeliveryRow.action = 'update';

		objDeliveryRow.location = new ECSSOAPDelivery.locationRefType();
		objDeliveryRow.location.housenumber = String.valueOf(objContractedProduct.Site__r.Site_House_Number__c);
		objDeliveryRow.location.housenumberExt = objContractedProduct.Site__r.Site_House_Number_Suffix__c;
		objDeliveryRow.location.zipcode = objContractedProduct.Site__r.Site_Postal_Code__c;
		objDeliveryRow.quantity = Integer.valueOf(objContractedProduct.Quantity__c);
		objDeliveryRow.group_x = String.isEmpty(objContractedProduct.Group__c) ? 1 : Integer.valueOf(objContractedProduct.Group__c);
		objDeliveryRow.costHeading = objContractedProduct.Cost_Center__c;
		objDeliveryRow.status = objContractedProduct.BOP_Status__c;

		req.deliveryRows.add(objDeliveryRow);
		requests.add(req);
		Test.setMock(WebServiceMock.class, new ECSSOAPDeliveryMock());
		Test.startTest();
		deliveryService.setRequest(requests);
		deliveryService.makeRequest('update');
		List<ECSDeliveryService.response> responseList = deliveryService.getResponse();
		System.assertEquals('12345', responseList[0].deliveryNumber, 'deliveryNumber returned: ' + responseList[0].deliveryNumber);
		Test.stopTest();
	}

	@isTest
	public static void testExceptions() {
		Test.startTest();
		ECSDeliveryService deliveryService = new ECSDeliveryService();
		List<ECSDeliveryService.request> requests = new List<ECSDeliveryService.request>();
		ECSDeliveryService.request req = new ECSDeliveryService.request();
		requests.add(req);
		deliveryService.setRequest(requests);
		try {
			//remove items from list
			requests.clear();
			deliveryService.setRequest(requests);
			deliveryService.makeRequest('update');
			// deliveryService.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		try {
			//remove items from list
			requests = null;
			deliveryService.setRequest(requests);
			deliveryService.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		Test.stopTest();
	}

	@isTest
	static void testExWebServiceCalloutException() {
		Contracted_Products__c objContractedProduct = [
			SELECT
				Id,
				Name,
				Quantity__c,
				BOP_Status__c,
				Order__r.Id,
				External_Reference_Id__c,
				Group__c,
				Cost_Center__c,
				Order__r.Sales_Order_Id__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Postal_Code__c,
				Customer_Asset__r.Installed_Base_Id__c
			FROM contracted_Products__c
			WHERE External_Reference_Id__c = '7777777'
		];

		ECSDeliveryService deliveryService = new ECSDeliveryService();
		List<ECSDeliveryService.request> requests = new List<ECSDeliveryService.request>();
		ECSDeliveryService.request req = new ECSDeliveryService.request();

		req.deliveryNumber = objContractedProduct.Order__r.Sales_Order_Id__c;
		req.referenceId = 'not Used'; //based on ECSDeliveryService this value is no used/null

		req.deliveryRows = new List<ECSSOAPDelivery.deliveryRowType>();
		ECSSOAPDelivery.deliveryRowType objDeliveryRow = new ECSSOAPDelivery.deliveryRowType();
		objDeliveryRow.id = objContractedProduct.External_Reference_Id__c;

		objDeliveryRow.additionalReference = new ECSSOAPDelivery.additionalRefType();
		ECSSOAPDelivery.customReferenceType addCustRefType = new ECSSOAPDelivery.customReferenceType();
		addCustRefType.name = 'installedBaseId';
		addCustRefType.value = objContractedProduct.Customer_Asset__r.Installed_Base_Id__c;
		objDeliveryRow.additionalReference.customReference = addCustRefType;

		objDeliveryRow.action = 'update';

		objDeliveryRow.location = new ECSSOAPDelivery.locationRefType();
		objDeliveryRow.location.housenumber = String.valueOf(objContractedProduct.Site__r.Site_House_Number__c);
		objDeliveryRow.location.housenumberExt = objContractedProduct.Site__r.Site_House_Number_Suffix__c;
		objDeliveryRow.location.zipcode = objContractedProduct.Site__r.Site_Postal_Code__c;

		objDeliveryRow.quantity = Integer.valueOf(objContractedProduct.Quantity__c);
		objDeliveryRow.group_x = String.isEmpty(objContractedProduct.Group__c) ? 1 : Integer.valueOf(objContractedProduct.Group__c);
		objDeliveryRow.costHeading = objContractedProduct.Cost_Center__c;

		// when the method is called from batch, the delivery status would not be committed to database
		objDeliveryRow.status = objContractedProduct.BOP_Status__c;

		req.deliveryRows.add(objDeliveryRow);
		requests.add(req);
		deliveryService.setRequest(requests);
		Test.startTest();
		try {
			deliveryService.makeRequest('update');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to update the delivery(s): '),
				'when there is a mock Created'
			);
		}
		Test.stopTest();
	}
}
