global with sharing class CustomButtonGenerateProposal extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		try {
			String newUrl = CustomButtonGenerateProposal.redirectToProposal(basketId);
			return '{"status":"ok","redirectURL":"' + newUrl + '"}';
		} catch (System.CalloutException e) {
			System.debug('ERROR:' + e);
			return '{"status":"error"}';
		}
	}

	// Set url for redirect after action
	public static String redirectToProposal(String basketId) {
		CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();

		PageReference editPage = new PageReference(url + '/apex/ProposalPage?Id=' + basketId);

		Id profileId = UserInfo.getProfileId();
		String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;

		if (profileName == 'VF Partner Portal User') {
			editPage = new PageReference('/partnerportal/apex/ProposalPage?Id=' + basketId);
		} else
			editPage = new PageReference('/apex/ProposalPage?Id=' + basketId);

		return editPage.getUrl();
	}
}
