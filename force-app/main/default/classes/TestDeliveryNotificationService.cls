@IsTest
private class TestDeliveryNotificationService {
	@IsTest
	static void testUpdateOrderSuccess() {
		prepareTestData(null);

		Test.startTest();
		DeliveryNotificationService.updateOrdersService();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responsebody.tostring());
		System.assertEquals('OK', m.get('status'), 'Expecting response status to be "OK"');
	}

	@IsTest
	static void testUpdateOrderErrorEmptyOrderId() {
		prepareRequestResponseData('', null);

		Test.startTest();
		DeliveryNotificationService.updateOrdersService();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responsebody.tostring());
		System.assertEquals('FAILED', m.get('status'));

		List<Object> l = (List<Object>) m.get('errors');
		Map<String, Object> a = (Map<String, Object>) l[0];
		System.assertEquals('SFEC-0001', a.get('errorCode'));
	}

	@IsTest
	static void testUpdateOrderErrorIncorrectOrderId() {
		prepareRequestResponseData('foobar', null);

		Test.startTest();
		DeliveryNotificationService.updateOrdersService();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responsebody.tostring());
		System.assertEquals('FAILED', m.get('status'));

		List<Object> l = (List<Object>) m.get('errors');
		Map<String, Object> a = (Map<String, Object>) l[0];
		System.assertEquals('SFEC-0002', a.get('errorCode'));
	}

	@IsTest
	static void testUpdateOrderErrorIncorrectEmail() {
		prepareTestData('johndoe.com');

		Test.startTest();
		DeliveryNotificationService.updateOrdersService();
		Test.stopTest();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(RestContext.response.responsebody.tostring());
		System.assertEquals('FAILED', m.get('status'));

		List<Object> l = (List<Object>) m.get('errors');
		Map<String, Object> a = (Map<String, Object>) l[0];
		System.assertEquals('SFEC-9999', a.get('errorCode'));
	}

	private static void prepareTestData(String overrideProjectManagerEmail) {
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		TestUtils.createBan(acct);
		TestUtils.createSite(acct);
		Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId());
		VF_Contract__c contr = TestUtils.createVFContract(acct, opp);
		OrderType__c ot = TestUtils.createOrderType();
		TestUtils.autoCommit = false;
		Product2 product = TestUtils.createProduct();
		insert product;

		Order__c ord = new Order__c();
		ord.Status__c = 'New';
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.Id;
		ord.Number_of_items__c = 100;
		ord.BOP_Order_Status__c = 'In Progress';
		ord.PM_Email__c = 'test@test.com';
		ord.PM_First_Name__c = 'Test';
		ord.PM_Last_Name__c = 'von Test';
		ord.PM_Phone__c = '0031612345678';
		ord.VF_Contract__c = contr.Id;
		insert ord;

		String ordId = ord.Id;
		Order__c newOrder = [SELECT BOP_Export_Order_Id__c FROM Order__c WHERE Id = :ordId];
		prepareRequestResponseData(newOrder.BOP_Export_Order_Id__c, overrideProjectManagerEmail);
	}

	private static void prepareRequestResponseData(String orderId, String overrideProjectManagerEmail) {
		DeliveryNotificationService.ProjectManager projMgr = new DeliveryNotificationService.ProjectManager();
		projMgr.firstName = 'John';
		projMgr.lastName = 'Doe';
		projMgr.phone = '0031687654321';
		projMgr.email = overrideProjectManagerEmail == null ? 'john@doe.com' : overrideProjectManagerEmail;
		DeliveryNotificationService.DeliveryNotification delNot = new DeliveryNotificationService.DeliveryNotification();
		delNot.orderId = orderId;
		delNot.projectManager = projMgr;
		delNot.status = 'Completed';

		RestRequest request = new RestRequest();
		request.requestBody = Blob.valueOf(JSON.serializePretty(delNot));
		RestContext.request = request;
		RestResponse response = new RestResponse();
		RestContext.response = response;
	}
}
