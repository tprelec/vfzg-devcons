public with sharing class COM_Helper {
	public static List<csord__Service__c> getServiceList(List<Id> serviceIdList) {
		return [
			SELECT
				Id,
				Name,
				COM_Delivery_Order__c,
				Installation_Wishdate__c,
				csord__Service__c,
				csord__Service__r.COM_Delivery_Order__c,
				csord__Service__r.Site__c,
				csord__Service__r.csordtelcoa__Replaced_Service__c,
				csord__Service__r.csordtelcoa__Replacement_Service__c,
				csord__Service__r.Name,
				Site__c,
				Site__r.Name,
				Site__r.Technical_Contact__c,
				csord__Subscription__c,
				csord__Subscription__r.Name,
				csord__Order__c,
				csord__Order__r.csord__Account__c,
				csord__Order__r.csord__Account__r.Name,
				csord__Order__r.csordtelcoa__Opportunity__c,
				csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
				csord__Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
				csordtelcoa__Replaced_Service__c,
				csordtelcoa__Replaced_Service__r.Site__c,
				csordtelcoa__Replaced_Service__r.COM_Delivery_Order__c,
				csordtelcoa__Replaced_Service__r.csord__Subscription__c,
				csordtelcoa__Replacement_Service__c,
				csordtelcoa__Replacement_Service__r.csord__Subscription__c,
				csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__r.Name,
				csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__c,
				csordtelcoa__Replacement_Service__r.csord__Order__c,
				csordtelcoa__Replacement_Service__r.Site__c,
				csordtelcoa__Cancelled_By_Change_Process__c,
				VFZ_Commercial_Article_Name__c,
				VFZ_Commercial_Component_Name__c,
				VFZ_Product_Abbreviation__c,
				csord__Status__c,
				Termination_Wish_Date__c,
				csord__Subscription__r.Termination_Wish_Date__c,
				csordtelcoa__Delta_Status__c,
				csord__Service__r.csordtelcoa__Delta_Status__c
			FROM csord__Service__c
			WHERE Id IN :serviceIdList
		];
	}

	public static String getAccountName(csord__Service__c service) {
		String accountName;

		if (service.csordtelcoa__Replacement_Service__c == null) {
			accountName = service.csord__Order__r.csord__Account__r.Name;
		} else {
			accountName = service.csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__r.Name;
		}

		return accountName;
	}

	public static List<csord__Service__c> getParentServiceList(List<csord__Service__c> servicesList) {
		List<csord__Service__c> parentServicesList = new List<csord__Service__c>();

		for (csord__Service__c serviceIterate : servicesList) {
			if (serviceIterate.csord__Service__c == null) {
				parentServicesList.add(serviceIterate);
			}
		}

		return parentServicesList;
	}

	public static List<Id> getSubscriptionIdList(List<csord__Service__c> servicesList) {
		List<Id> subscriptionIdList = new List<Id>();

		for (csord__Service__c serviceIterate : servicesList) {
			subscriptionIdList.add(serviceIterate.csord__Subscription__c);

			if (serviceIterate.csordtelcoa__Replaced_Service__c != null) {
				subscriptionIdList.add(serviceIterate.csordtelcoa__Replaced_Service__r.csord__Subscription__c);
			}
		}

		return subscriptionIdList;
	}

	public static List<csord__Order__c> getOrders(List<csord__Service__c> servicesList) {
		Set<Id> orderIdList = new Set<Id>();

		for (csord__Service__c serviceIterate : servicesList) {
			if (serviceIterate.csord__Order__c != null) {
				orderIdList.add(serviceIterate.csord__Order__c);
			}
		}

		return [SELECT Id, Name, csord__Account__c FROM csord__Order__c WHERE Id IN :orderIdList];
	}

	public static List<Id> getReplacedServiceIdList(List<csord__Service__c> servicesList) {
		List<Id> replacedServiceIdList = new List<Id>();

		for (csord__Service__c serviceIterate : servicesList) {
			if (
				serviceIterate.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c == COM_Constants.OPPORTUNITY_CHANGE_TYPE_MOVE
			) {
				replacedServiceIdList.add(serviceIterate.csordtelcoa__Replaced_Service__c);
			}
		}

		return replacedServiceIdList;
	}

	public static String getSiteId(csord__Service__c service) {
		String siteId = null;

		if (service.Site__c != null) {
			siteId = String.valueOf(service.Site__c);
		}

		return siteId;
	}

	public static Set<String> getSiteIdSet(List<csord__Service__c> serviceList) {
		Set<String> siteIdSet = new Set<String>();

		for (csord__Service__c serviceIterate : serviceList) {
			siteIdSet.add(COM_Helper.getSiteId(serviceIterate));
		}

		return siteIdSet;
	}

	public static Id getOrderId(csord__Service__c service) {
		String orderId;

		if (service.csordtelcoa__Replacement_Service__c == null) {
			orderId = service.csord__Order__c;
		} else {
			orderId = service.csordtelcoa__Replacement_Service__r.csord__Order__c;
		}

		return orderId;
	}

	public static List<csord__Subscription__c> getSubscriptionList(List<Id> subscriptionIds) {
		return [SELECT Id, Name FROM csord__Subscription__c WHERE Id IN :subscriptionIds];
	}

	public static void updateList(List<SObject> objects) {
		if (!objects.isEmpty()) {
			update objects;
		}
	}

	public static List<COM_Delivery_Order__c> fetchParentDeliveryOrders(List<csord__Order__c> orders) {
		return [SELECT Id FROM COM_Delivery_Order__c WHERE Order__c IN :orders AND Parent_Delivery_Order__c = NULL];
	}

	public static List<csord__Service__c> getServiceList(List<csord__Service__c> serviceList) {
		return [
			SELECT
				Id,
				Name,
				COM_Delivery_Order__c,
				Installation_Wishdate__c,
				csord__Service__c,
				csord__Service__r.COM_Delivery_Order__c,
				csord__Service__r.Site__c,
				csord__Service__r.csordtelcoa__Replaced_Service__c,
				csord__Service__r.csordtelcoa__Replacement_Service__c,
				Site__c,
				Site__r.Name,
				Site__r.Technical_Contact__c,
				csord__Subscription__c,
				csord__Subscription__r.Name,
				csord__Order__c,
				csord__Order__r.csord__Account__c,
				csord__Order__r.csord__Account__r.Name,
				csord__Order__r.csordtelcoa__Opportunity__c,
				csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
				csord__Service__r.csord__Order__r.csordtelcoa__Opportunity__r.csordtelcoa__Change_Type__c,
				csordtelcoa__Replaced_Service__c,
				csordtelcoa__Replaced_Service__r.Site__c,
				csordtelcoa__Replaced_Service__r.COM_Delivery_Order__c,
				csordtelcoa__Replaced_Service__r.csord__Subscription__c,
				csordtelcoa__Replacement_Service__c,
				csordtelcoa__Replacement_Service__r.csord__Subscription__c,
				csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__r.Name,
				csordtelcoa__Replacement_Service__r.csord__Order__r.csord__Account__c,
				csordtelcoa__Replacement_Service__r.csord__Order__c,
				csordtelcoa__Replacement_Service__r.Site__c,
				csordtelcoa__Cancelled_By_Change_Process__c,
				VFZ_Commercial_Article_Name__c,
				VFZ_Commercial_Component_Name__c,
				VFZ_Product_Abbreviation__c,
				csord__Status__c,
				Termination_Wish_Date__c,
				csordtelcoa__Delta_Status__c,
				csord__Service__r.csordtelcoa__Delta_Status__c
			FROM csord__Service__c
			WHERE Id IN :serviceList
		];
	}

	public static List<csord__Service__c> getServiceList_CustomQuery(List<csord__Service__c> serviceList, String additionalFields) {
		return Database.query(COM_Constants.CUSTOM_SERVICE_QUERY_PART1 + additionalFields + COM_Constants.CUSTOM_SERVICE_QUERY_PART2);
	}

	public static Id getTechnicalContactId(csord__Service__c service) {
		Id technicalContactId = null;

		if (service.Site__c != null) {
			technicalContactId = service.Site__r.Technical_Contact__c;
		}

		return technicalContactId;
	}

	public static List<Asset> prepareCOMAssets(
		csord__Service__c service,
		COM_MetadataInformation.DeliveryComponent deliveryComponent,
		List<COM_Delivery_Article_Materials__mdt> deliveryArticleMaterials
	) {
		List<Asset> comAssets = new List<Asset>();
		Map<String, List<COM_Delivery_Materials__mdt>> applicableDeliveryMaterialsByType = new Map<String, List<COM_Delivery_Materials__mdt>>();

		for (COM_Delivery_Article_Materials__mdt deliveryArticleMaterial : deliveryArticleMaterials) {
			if (
				deliveryArticleMaterial.COM_Delivery_Article__r.Delivery_Article_Name__c == deliveryComponent.articleName &&
				deliveryArticleMaterial.COM_Delivery_Materials__r.Generate_Asset__c
			) {
				if (applicableDeliveryMaterialsByType.get(deliveryArticleMaterial.COM_Delivery_Materials__r.Type__c) == null) {
					applicableDeliveryMaterialsByType.put(
						deliveryArticleMaterial.COM_Delivery_Materials__r.Type__c,
						new List<COM_Delivery_Materials__mdt>()
					);
				}

				applicableDeliveryMaterialsByType.get(deliveryArticleMaterial.COM_Delivery_Materials__r.Type__c)
					.add(deliveryArticleMaterial.COM_Delivery_Materials__r);
			}
		}

		for (String type : applicableDeliveryMaterialsByType.keySet()) {
			comAssets.add(createCOMAssetRecord(service, deliveryComponent, applicableDeliveryMaterialsByType.get(type)));
		}

		return comAssets;
	}

	public static Asset createCOMAssetRecord(
		csord__Service__c service,
		COM_MetadataInformation.DeliveryComponent deliveryComponent,
		List<COM_Delivery_Materials__mdt> deliveryMaterials
	) {
		Asset comAsset = new Asset();
		comAsset.RecordTypeId = Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName()
			.get(COM_Constants.COM_ASSET_RECORD_TYPE)
			.getRecordTypeId();
		comAsset.Name = deliveryMaterials.size() == 1
			? deliveryMaterials[0].Model_Name__c
			: (deliveryComponent.name + ' ' + deliveryMaterials[0].Type__c);
		comAsset.COM_Type__c = deliveryMaterials[0].Type__c;
		comAsset.COM_Delivery_Order__c = service.COM_Delivery_Order__c;
		comAsset.COM_Delivery_Component__c = deliveryComponent.name;
		comAsset.csord__Service__c = service.Id;
		comAsset.COM_BOP_C_code__c = deliveryMaterials.size() == 1 ? deliveryMaterials[0].BOP_C_code__c : '';
		comAsset.AccountId = service.csord__Order__r.csord__Account__c;

		return comAsset;
	}

	public static Map<Id, List<Asset>> fetchAssetMapByDeliveryOrderId(Set<Id> deliveryOrderIds) {
		Map<Id, List<Asset>> assetsByDeliveryOrderId = new Map<Id, List<Asset>>();
		for (Asset a : [SELECT Id, Name, COM_Delivery_Order__c FROM Asset WHERE COM_Delivery_Order__c IN :deliveryOrderIds]) {
			List<Asset> assetList = assetsByDeliveryOrderId.get(a.COM_Delivery_Order__c);
			if (assetList == null) {
				assetList = new List<Asset>();
			}

			assetList.add(a);
			assetsByDeliveryOrderId.put(a.COM_Delivery_Order__c, assetList);
		}

		return assetsByDeliveryOrderId;
	}

	public static Map<String, COM_Delivery_Materials__mdt> fetchDeliveryMaterialsMapByModelName() {
		Map<String, COM_Delivery_Materials__mdt> materialsByModelName = new Map<String, COM_Delivery_Materials__mdt>();

		for (COM_Delivery_Materials__mdt delMat : [SELECT Id, Model_Name__c, Returnable_by_Box__c FROM COM_Delivery_Materials__mdt]) {
			materialsByModelName.put(delMat.Model_Name__c, delMat);
		}

		return materialsByModelName;
	}
}
