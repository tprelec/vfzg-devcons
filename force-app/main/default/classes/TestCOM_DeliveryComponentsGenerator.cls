@IsTest
private class TestCOM_DeliveryComponentsGenerator {
	@IsTest
	static void testBehavior() {
		Map<Id, csord__Service__c> serviceMap = new Map<Id, csord__Service__c>();
		Set<Id> orderIds = new Set<Id>();

		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', null);
		testOpp.csordtelcoa__Change_Type__c = 'Add';
		insert testOpp;

		csord__Order_Request__c orderRequest = CS_DataTest.createOrderRequest(true);
		List<csord__Order__c> ordersList = CS_DataTest.createOrders(1, 'testOrderName', testAccount, 'Order Created', orderRequest, testOpp, true);
		for (csord__Order__c orderRecord : ordersList) {
			orderIds.add(orderRecord.Id);
		}

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
		testDef.RecordTypeId = productDefinitionRecordType;
		testDef.Product_Type__c = 'Fixed';
		insert testDef;

		cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf', basket.Id);
		insert testConfConf;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(testConfConf.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.csord__Status__c = 'Subscription Created';
		insert subscription;

		csord__Service__c zipParentService = CS_DataTest.createService(testConfConf.Id, subscription, 'Service Created');
		zipParentService.csord__Identification__c = 'testSubscription';
		zipParentService.VFZ_Commercial_Component_Name__c = 'TestComponent';
		zipParentService.VFZ_Commercial_Article_Name__c = 'TestArticle';
		zipParentService.csord__Order__c = ordersList[0].Id;
		insert zipParentService;

		csord__Service__c ipAddressService = CS_DataTest.createService(testConfConf.Id, subscription, 'Service Created');
		ipAddressService.csord__Identification__c = 'testSubscription';
		ipAddressService.VFZ_Commercial_Component_Name__c = 'TestComponent';
		ipAddressService.VFZ_Commercial_Article_Name__c = 'TestArticle';
		ipAddressService.csord__Service__c = zipParentService.Id;
		ipAddressService.csord__Order__c = ordersList[0].Id;
		insert ipAddressService;

		serviceMap.put(zipParentService.Id, zipParentService);
		serviceMap.put(ipAddressService.Id, ipAddressService);

		Boolean stepPassed;

		try {
			Test.startTest();
			COM_DeliveryComponentsGenerator generator = new COM_DeliveryComponentsGenerator();
			generator.run(orderIds);
			stepPassed = true;
			Test.stopTest();
		} catch (Exception e) {
			stepPassed = false;
		}

		System.assert(true, stepPassed);
	}
}
