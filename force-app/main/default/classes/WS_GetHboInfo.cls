@RestResource(urlMapping='/GET_HBO_info')
@SuppressWarnings('PMD.NcssMethodCount, PMD.StdCyclomaticComplexity, PMD.CyclomaticComplexity')
global without sharing class WS_GetHboInfo {
    @HttpGet
    global static void getRecord() {
        RestRequest req = RestContext.request;
        RestResponse res = Restcontext.response == null ? new RestResponse() : Restcontext.response;
        String reqBodyString = req.requestBody != null ? req.requestBody.toString() : '';
        Map<String, String> reqBody = String.isBlank(reqBodyString)
            ? new Map<String, String>()
            : (Map<String, String>) JSON.deserializeUntyped(reqBodyString);
        String orderNo = reqBody.isEmpty() ? req.params.get('orderNoSF') : reqBody.get('orderNoSF');
        String siteZipcode = reqBody.isEmpty() ? req.params.get('zipcode') : reqBody.get('zipcode');
        String siteHouseNo = reqBody.isEmpty() ? req.params.get('houseNo') : reqBody.get('houseNo');
        String siteHouseNoExt = reqBody.isEmpty()
            ? req.params.get('houseNoExt')
            : reqBody.get('houseNoExt');

        if (String.isBlank(orderNo) || String.isBlank(siteZipcode) || String.isBlank(siteHouseNo)) {
            // error validation
            String paramsReq =
                (String.isBlank(orderNo) ? 'orderNoSF ' : '') +
                (String.isBlank(siteZipcode) ? 'zipcode ' : '') +
                (String.isBlank(siteHouseNo) ? 'houseNo ' : '');
            HboError hboErr = new HboError('hbo001', 'Required params: ' + paramsReq);
            res.responseBody = Blob.valueOf(JSON.serialize(hboErr));
            res.statuscode = 400;
            return;
        }

        if (!Pattern.compile('[1-9][0-9]{3}\\s?[a-zA-Z]{2}$').matcher(siteZipcode).matches()) {
            HboError hboErr = new HboError('hbo002', 'Zipcode is not valid.');
            res.responseBody = Blob.valueOf(JSON.serialize(hboErr));
            res.statuscode = 400;
            return;
        }

        if (!Pattern.compile('^[0-9]*$').matcher(siteHouseNo).matches()) {
            HboError hboErr = new HboError('hbo002', 'House number is not valid.');
            res.responseBody = Blob.valueOf(JSON.serialize(hboErr));
            res.statuscode = 400;
            return;
        }

        List<Order__c> orders = queryOrder(orderNo);
        if (orders == null || orders.isEmpty()) {
            //order not found
            HboError hboErr = new HboError('hbo004', 'Order doesn\'t exist');
            res.responseBody = Blob.valueOf(JSON.serialize(hboErr));
            res.statusCode = 404;
            return;
        }

        List<Site__c> hboSite = querySite(
            siteZipcode,
            siteHouseNo,
            siteHouseNoExt,
            orders[0].Account__c
        );
        if (hboSite.isEmpty()) {
            //site not found
            HboError hboErr = new HboError('hbo003', 'Site doesn\'t exist');
            res.responseBody = Blob.valueOf(JSON.serialize(hboErr));
            res.statusCode = 404;
            return;
        }

        List<HBO__c> hbos = queryHBOs(hboSite[0].Id, orders[0].VF_Contract__r.Opportunity__c);
        if (hbos.isEmpty()) {
            HboError hboErr = new HboError('hbo005', 'Hbo not found');
            res.responseBody = Blob.valueOf(JSON.serialize(hboErr));
            res.statusCode = 404;
            return;
        }

        HboWrapper hbo = new HboWrapper(hbos[0]);
        //return hbos
        res.statusCode = 200;
        res.responseBody = Blob.valueOf(JSON.serialize(hbo));
    }

    private static List<HBO__c> queryHBOs(Id siteId, Id oppId) {
        List<HBO__c> hbos = [
            SELECT
                Id,
                hbo_redundancy__c,
                hbo_redundancy_type__c,
                hbo_digging_distance__c,
                hbo_delivery_time__c,
                hbo_hbo_comments__c,
                hbo_civil_cost__c,
                hbo_result_type__c
            FROM HBO__c
            WHERE
                hbo_site__c = :siteId
                AND hbo_status__c = 'Approved'
                AND hbo_opportunity__c = :oppId
            ORDER BY CreatedDate DESC
        ];
        return hbos;
    }

    @SuppressWarnings('PMD.ExcessiveParameterList')
    private static List<Site__c> querySite(
        String siteZipcode,
        String siteHouseNo,
        String siteHouseNoExt,
        String accountId
    ) {
        List<Site__c> sites = [
            SELECT Id
            FROM Site__c
            WHERE
                Site_Postal_Code__c = :siteZipcode
                AND Site_House_Number__c = :Integer.valueOf(siteHouseNo)
                AND Site_House_Number_Suffix__c = :siteHouseNoExt
                AND Acc_Id__c = :accountId
        ];
        return sites;
    }

    private static List<Order__c> queryOrder(String orderNo) {
        List<Order__c> orders = [
            SELECT Id, VF_Contract__r.Opportunity__c, Account__c
            FROM Order__c
            WHERE BOP_Export_Order_Id__c = :orderNo
        ];
        return orders;
    }

    global class HboWrapper {
        public Boolean redundancy;
        public String redundancyType;
        public Decimal diggingDistance;
        public Decimal deliveryTime;
        public String comments;
        public Decimal civilCosts;
        public String resultType;
        public HboWrapper(HBO__c hbo) {
            this.redundancy = hbo.hbo_redundancy__c;
            this.redundancyType = (String)hbo.get('hbo_redundancy_type__c');  //EDV 2021-10-22
            this.diggingDistance = hbo.hbo_digging_distance__c;
            this.deliveryTime = hbo.hbo_delivery_time__c; // weeks
            this.comments = hbo.hbo_hbo_comments__c;
            this.civilCosts = hbo.hbo_civil_cost__c;
            this.resultType = hbo.hbo_result_type__c;
        }
    }

    global class HboError {
        public String statusCode;
        public String message;
        public HboError(String statusCode, String message) {
            this.statusCode = statusCode;
            this.message = message;
        }
    }
}