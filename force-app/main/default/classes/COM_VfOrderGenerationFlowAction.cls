public class COM_VfOrderGenerationFlowAction {
	@InvocableMethod(callout=false label='Generate Vf Orders')
	public static List<GenerationResult> generateVfOrders(List<GenerationRequest> generationRequests) {
		List<GenerationResult> generationResults = new List<GenerationResult>();

		for (GenerationRequest generationRequest : generationRequests) {
			GenerationResult generationResult = new GenerationResult();
			generationResult.contractId = generationRequest.contractId;
			try {
				OrderFormController orderFormController = new OrderFormController(null);
				orderFormController.contractId = generationRequest.contractId;
				orderFormController.prepareData();
				generationResult.generationSuccess = true;

				Set<Id> orderIds = new Set<Id>();
				//TB just checking ordercleaning
				for (Order__c o : OrderUtils.getOrderDataByContractId(generationRequest.contractId, new Set<id>{})) {
					orderIds.add(o.Id);
				}
				OrderUtils.updateOrderStatus(orderIds, null);
			} catch (Exception e) {
				generationResult.generationSuccess = false;
			}
			generationResults.add(generationResult);
		}

		return generationResults;
	}

	public class GenerationRequest {
		@InvocableVariable
		public Id contractId;
	}

	public class GenerationResult {
		@InvocableVariable
		public Id contractId;

		@InvocableVariable
		public Boolean generationSuccess;
	}
}
