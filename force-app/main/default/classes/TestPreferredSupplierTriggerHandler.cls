@isTest
public with sharing class TestPreferredSupplierTriggerHandler {
	@TestSetup
	static void makeData() {
		ExternalIDNumber__c newCustomSettingExternalIdNumber = new ExternalIDNumber__c();
		newCustomSettingExternalIdNumber.Name = 'Preferred Supplier';
		newCustomSettingExternalIdNumber.Object_Prefix__c = 'PS-11-';
		newCustomSettingExternalIdNumber.External_Number__c = 1;
		newCustomSettingExternalIdNumber.runningOrg__c = UserInfo.getOrganizationId();
		insert newCustomSettingExternalIdNumber;
	}

	@isTest
	private static void testCreateRecord() {
		Test.startTest();
		Preferred_Supplier__c testPS = new Preferred_Supplier__c();
		testPS.Order__c = 1;
		insert testPS;

		Preferred_Supplier__c queryRecord = [SELECT Id, ExternalID__c FROM Preferred_Supplier__c LIMIT 1];
		System.assert(queryRecord.ExternalID__c != null, 'External Id should exist.');

		Test.stopTest();
	}
}
