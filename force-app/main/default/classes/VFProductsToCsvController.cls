global class VFProductsToCsvController {
	
	global static boolean isTest = False;

    public VFProductsToCsvController(ApexPages.StandardSetController ignored) {
    }


	public PageReference callBatch(){
		VFProductsToCsvBatch controller = new VFProductsToCsvBatch();
		if(isTest)VFProductsToCsvBatch.isTest=true;
		database.executebatch(controller);
		return new pageReference('/a0t');
	}

}