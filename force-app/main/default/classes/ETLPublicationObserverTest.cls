@isTest
global class ETLPublicationObserverTest {
	testMethod static void testObserverFailure() {
		Test.startTest();
		ETLPublicationObserver observer = new ETLPublicationObserver();
		ETLPublicationObserver.CustomException e = new ETLPublicationObserver.CustomException();
		e.setMessage('test message');
		observer.exceptionHandling(null, (Exception) e);
		observer.execute(null, null);
		Test.stopTest();
	}

	testMethod static void testObserver() {
		CCETLSettings__c ssettings = new CCETLSettings__c();
		ssettings.Client_ID__c = 'xxx';
		insert ssettings;

		cspmb__Catalogue__c catalogue = new cspmb__Catalogue__c();
		catalogue.Name = 'test_catalogue';
		insert catalogue;

		csb2c__E_Commerce_Publication__c publicationObj = new csb2c__E_Commerce_Publication__c();
		insert publicationObj;

		csb2c__Publication_Catalogue_Association__c publicationAssociation = new csb2c__Publication_Catalogue_Association__c();
		publicationAssociation.csb2c__Catalogue__c = catalogue.Id;
		publicationAssociation.csb2c__Publication__c = publicationObj.Id;
		insert publicationAssociation;

		cspmb__Pricing_Rule_Group__c prgg = new cspmb__Pricing_Rule_Group__c();
		prgg.cspmb__pricing_rule_group_code__c = 'Group-001';
		insert prgg;

		csam__AsyncMessOptions__c asyncsettings = new csam__AsyncMessOptions__c();
		asyncsettings.csam__Disable_Validations__c = false;
		insert asyncsettings;

		csam__Integration_Endpoint__c endpointsettings = new csam__Integration_Endpoint__c();
		endpointsettings.Name = 'ECommerceIntegration';
		endpointsettings.csam__Callout_Host__c = 'cs-ecommerce-int-sandbox-eu.herokuapp.com';
		insert endpointsettings;

		csam__Integration_Endpoint__c endpointsettingsETL = new csam__Integration_Endpoint__c();
		endpointsettingsETL.Name = 'DigitalCommerceETL';
		endpointsettingsETL.csam__Callout_Host__c = 'cs-messaging-dispatcher-eu-dev.herokuapp.com';
		insert endpointsettingsETL;

		ETLPublicationObserver etlPubObs = new ETLPublicationObserver();
		ETLCallout.PublicationPayload publicationPayload = new ETLCallout.PublicationPayload();
		ETLCallout.PRG prg = new ETLCallout.PRG();

		prg.id = '12345';
		prg.name = 'Test PRG';
		prg.currencyCode = '01';

		List<ETLCallout.PRG> prgsList = new List<ETLCallout.PRG>();
		prgsList.add(prg);

		Map<String, Object> customFieldsSchemaMap = new Map<String, Object>();

		publicationPayload.clientId = '2222';
		publicationPayload.elasticCatalogId = '12345555';
		publicationPayload.elasticCatalogName = 'Catalog Name';
		publicationPayload.prgs = prgsList;
		publicationPayload.customFieldsSchema = customFieldsSchemaMap;

		ETLPublicationObserver observer = new ETLPublicationObserver();

		csam__ObjectGraph_Callout_Handler__c etlCalloutHandler = new csam__ObjectGraph_Callout_Handler__c();
		etlCalloutHandler.Name = 'Digital Commerce ETL';
		etlCalloutHandler.csam__Integration_Endpoint_Name__c = 'DigitalCommerceETL';
		etlCalloutHandler.csam__Startpoint_Type_Name__c = 'csb2c__E_Commerce_Publication__c';
		insert etlCalloutHandler;

		csam__ObjectGraph_Callout_Handler__c ogCalloutHandler = new csam__ObjectGraph_Callout_Handler__c();
		ogCalloutHandler.Name = 'ECommerce Publication v3 Sync';
		ogCalloutHandler.csam__Integration_Endpoint_Name__c = 'ECommerceIntegration';
		ogCalloutHandler.csam__Startpoint_Type_Name__c = 'csb2c__E_Commerce_Publication__c';
		insert ogCalloutHandler;

		System.debug('ogCalloutHandler -> ' + ogCalloutHandler);

		csam__Outgoing_Message__c outgoingMessage = new csam__Outgoing_Message__c();
		outgoingMessage.csam__ObjectGraph_Callout_Handler__c = ogCalloutHandler.Id;
		outgoingMessage.csam__Content_Type__c = 'application/json; charset=utf-8';
		outgoingMessage.csam__URL_Host__c = 'cs-messaging-dispatcher-eu-dev.herokuapp.com';
		insert outgoingMessage;

		System.debug('outgoingMessage -> ' + outgoingMessage);

		csam__Incoming_Message__c incomingMessage = new csam__Incoming_Message__c();
		incomingMessage.csam__Final_Chunk__c = true;
		incomingMessage.csam__Outgoing_Message__c = outgoingMessage.Id;
		incomingMessage.csam__Incoming_URL_Path__c = 'test';
		incomingMessage.csam__HTTP_Method__c = 'POST';
		incomingMessage.csam__Incoming_URL_Path__c = 'good';
		insert incomingMessage;

		System.debug('incomingMessage -> ' + incomingMessage);

		Test.startTest();
		List<csam__Incoming_Message__c> messages = [
			SELECT
				Id,
				csam__Final_Chunk__c,
				csam__Outgoing_Message__c,
				csam__Outgoing_Message__r.csam__ObjectGraph_Callout_Handler__c,
				csam__Outgoing_Message__r.csam__ObjectGraph_Callout_Handler__r.Name,
				csam__Incoming_URL_Path__c
			FROM csam__Incoming_Message__c
		];

		csam__Outgoing_Message_Record__c outgoingMessageRecord = new csam__Outgoing_Message_Record__c();
		outgoingMessageRecord.csam__Object_Record_Id__c = publicationObj.Id;
		outgoingMessageRecord.csam__Outgoing_Message__c = incomingMessage.csam__Outgoing_Message__c;
		outgoingMessageRecord.csam__Object_Name__c = 'csb2c__E_Commerce_Publication__c';
		insert outgoingMessageRecord;

		System.debug('incomingMessage.csam__Final_Chunk__c -> ' + incomingMessage.csam__Final_Chunk__c);
		System.debug('incomingMessage.csam__Outgoing_Message__c -> ' + incomingMessage.csam__Outgoing_Message__c);
		System.debug(
			'incomingMessage.csam__Outgoing_Message__r.csam__ObjectGraph_Callout_Handler__c -> ' +
			incomingMessage.csam__Outgoing_Message__r.csam__ObjectGraph_Callout_Handler__c
		);
		System.debug('incomingMessage.csam__Incoming_URL_Path__c -> ' + incomingMessage.csam__Incoming_URL_Path__c);

		observer.execute(messages);
		Test.stopTest();
	}
}
