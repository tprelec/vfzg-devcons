/**
 * @description			This exception should be thrown if a configuration is illegal.
 * @author				Guy Clairbois
 */
public class ExInvalidConfigurationException extends Exception {}