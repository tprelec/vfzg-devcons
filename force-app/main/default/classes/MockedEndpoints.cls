@RestResource(urlMapping='/mockedEndpoints/*')
global class MockedEndpoints {
	private static void handleRequest() {
		MockedEndpointsRoute router = new MockedEndpointsRoute();
		router.execute();
	}

	@HttpGet
	global static void handleGet() {
		handleRequest();
	}

	@HttpPost
	global static void handlePost() {
		handleRequest();
	}

	@HttpPut
	global static void handlePut() {
		handleRequest();
	}

	@HttpDelete
	global static void handleDelete() {
		handleRequest();
	}
}
