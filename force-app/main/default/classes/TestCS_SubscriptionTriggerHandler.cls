@IsTest
private class TestCS_SubscriptionTriggerHandler {
    @IsTest
    static void testUpdateAllSolutionStatus() {
        User simpleUser = CS_DataTest.createSystemAdministratorUser();

        System.runAs(simpleUser){
            List<csord__Subscription__c> newSubscriptions = new List<csord__Subscription__c>();
            List<csord__Subscription__c> subscriptionsToUpdate = new List<csord__Subscription__c>();
            List<Id> solutionIds = new List<Id>();

            Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId();

            cscfga__Product_Definition__c testDef = CS_DataTest.createProductDefinition('Product Definition');
            testDef.RecordTypeId = productDefinitionRecordType;
            testDef.Product_Type__c = 'Fixed';
            insert testDef;

            Account testAccount = CS_DataTest.createAccount('Test Account');
            insert testAccount;

            Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
            insert testOpp;


            cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
            insert basket;

            cscfga__Product_Configuration__c testConfConf = CS_DataTest.createProductConfiguration(testDef.Id, 'Test Conf',basket.Id);
            insert testConfConf;

            csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
            insert ord;

            Suborder__c solution1 = CS_DataTest.createSolution('Solution 1',ord.Id, true);
            solutionIds.add(solution1.Id);

            csord__Subscription__c subscription1 = CS_DataTest.createSubscription(testConfConf.Id);
            subscription1.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
            subscription1.Suborder__c = solution1.Id;
            subscription1.csord__Status__c = 'Subscription Created';
            subscription1.Installation_Required__c = true;
            newSubscriptions.add(subscription1);

            csord__Subscription__c subscription2 = CS_DataTest.createSubscription(testConfConf.Id);
            subscription2.csord__Identification__c = 'Subscription_a4D9E0010009BCTUA2_0';
            subscription2.Suborder__c = solution1.Id;
            subscription2.csord__Status__c = 'Subscription Created';
            subscription2.Installation_Required__c = true;
            newSubscriptions.add(subscription2);

            insert newSubscriptions;

            subscription1.csord__Status__c = 'Implemented';
            subscriptionsToUpdate.add(subscription1);

            subscription2.csord__Status__c = 'Implemented';
            subscriptionsToUpdate.add(subscription2);

            update subscriptionsToUpdate;

            List<Suborder__c> newSolutions = [SELECT Id, Ready_for_billing__c,
                    Status__c
            FROM Suborder__c
            WHERE Id IN :solutionIds];

            for(Suborder__c sol :newSolutions){
                system.assertEquals(sol.Status__c, 'Implemented');
                system.assertEquals(sol.Ready_for_billing__c, false);
            }
        }
    }
}