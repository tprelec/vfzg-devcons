/**
 * @description         This class schedules the batch processing of ContactRole export.
 * @author              Guy Clairbois
 */
global class ScheduleContactRoleExportBatch implements Schedulable {
    
    /**
     * @description         This method executes the batch job.
     */
    global void execute (SchedulableContext SBatch){
        
		ContactRoleExportBatch contactRoleBatch = new ContactRoleExportBatch();
		Id batchprocessId = Database.executeBatch(contactRoleBatch,100);
    }
    
}