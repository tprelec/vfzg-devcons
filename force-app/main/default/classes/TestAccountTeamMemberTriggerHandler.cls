@isTest
private class TestAccountTeamMemberTriggerHandler {
	private static Account acc;

	private static void createTestData() {
		acc = TestUtils.createAccount(GeneralUtils.currentUser);
	}

	private static User createUser(Id profileId, String username) {
		User user2 = new User(
			Username = username,
			LastName = 'Admin',
			Alias = 'tomtest',
			Email = username,
			TimeZoneSidKey = 'Europe/Amsterdam',
			LocaleSidKey = 'nl_NL',
			EmailEncodingKey = 'ISO-8859-1',
			LanguageLocaleKey = 'en_US',
			ProfileId = profileId
		);
		insert user2;
		return user2;
	}

	@isTest
	static void testPreventAnotherChannelManager() {
		createTestData();
		User user1 = [SELECT Id FROM User WHERE Id = :UserInfo.getUserId()];

		System.runAs(user1) {
			Id profileId = [SELECT Id FROM Profile WHERE Name = 'System Administrator' LIMIT 1].Id;
			User user2 = createUser(profileId, 'tomtestadmin2@vodafone.com');

			AccountTeamMember atm = new AccountTeamMember(
				AccountId = acc.Id,
				UserId = user2.Id,
				TeamMemberRole = 'Account Manager',
				AccountAccessLevel = 'Read'
			);
			insert atm;
	
			Test.startTest();
	
			insert new AccountTeamMember(
				AccountId = acc.Id,
				UserId = UserInfo.getUserId(),
				TeamMemberRole = 'Channel Manager',
				AccountAccessLevel = 'Read'
			);
	
			atm.TeamMemberRole = 'Channel Manager';

			try {
				update atm;
			}
			catch (Exception ex) {
				System.assertEquals(
					true,
					ex.getMessage().contains(Label.Prevent_Another_Channel_Manager),
					'Exception should be thrown.'
				);
			}
	
			Test.stopTest();
		}
	}
}