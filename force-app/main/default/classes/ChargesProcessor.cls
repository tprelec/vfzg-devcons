/**
 * Created by bojan.zunko on 07/07/2020.
 */

global with sharing class ChargesProcessor {
	// global static String DISCOUNT_VERSION = '2-0-0';
	// global static String OVERRIDE_CHARGE_TYPE = 'override';
	// global static String ABSOLUTE_DISCOUNT_TYPE = 'absolute';
	// global static String PERCENTAGE_DISCOUNT_TYPE = 'percentage';
	// global static String ONE_OFF_CHARGE_TYPE = 'oneOff';
	// global static String RECURRING_CHARGE_TYPE = 'recurring';
	// global static String SALES_DISCOUNT_PRICE = 'sales';
	// global static String LIST_DISCOUNT_PRICE = 'list';
	// global static String PRODUCT_LEVEL_CHARGE = '__PRODUCT__';
	// global static String QUANTITY_STRATEGY_MULTIPLIER = 'Multiplier';
	// global static String QUANTITY_STRATEGY_DECOMPOSITION = 'Decomposition';
	// global static String QUANTITY_STRATEGY_QBS = 'Quantity Based Selling';

	// global static cscfga.ProductConfiguration.ProductDiscount processCharges(cscfga__Product_Configuration__c pc, cscfga.ProductConfiguration.ProductDiscount discountArray, List<CCAQDProcessor.CCCharge> charges, String quantityStrategy) {
	//     if (quantityStrategy != null && quantityStrategy != QUANTITY_STRATEGY_MULTIPLIER && charges != null && !charges.isEmpty()){
	//         pc.cscfga__Quantity__c = charges[0].quantity;
	//     }

	//     for (CCAQDProcessor.CCCharge iter : charges) {
	//         cscfga.ProductConfiguration.Discount chargeSalesOverride = createDiscountToInitCharge(iter,SALES_DISCOUNT_PRICE, quantityStrategy);
	//         cscfga.ProductConfiguration.Discount chargeListOverride = createDiscountToInitCharge(iter,LIST_DISCOUNT_PRICE, quantityStrategy);

	//         discountArray.discounts.add(chargeListOverride);
	//         discountArray.discounts.add(chargeSalesOverride);

	//     }
	//     return discountArray;
	// }

	global static cscfga.ProductConfiguration.ProductDiscount processCharges(
		cscfga__Product_Configuration__c pc,
		cscfga.ProductConfiguration.ProductDiscount discountArray,
		List<CCAQDProcessor.CCCharge> charges,
		String CS_QuantityStrategy,
		Integer quantity
	) {
		for (CCAQDProcessor.CCCharge iter : charges) {
			cscfga.ProductConfiguration.Discount chargeSalesOverride = ChargesProcessorUtils.createDiscountToInitCharge(
				iter,
				ChargesProcessorUtils.SALES_DISCOUNT_PRICE,
				CS_QuantityStrategy,
				quantity
			);
			cscfga.ProductConfiguration.Discount chargeListOverride = ChargesProcessorUtils.createDiscountToInitCharge(
				iter,
				ChargesProcessorUtils.LIST_DISCOUNT_PRICE,
				CS_QuantityStrategy,
				quantity
			);

			discountArray.discounts.add(chargeListOverride);
			discountArray.discounts.add(chargeSalesOverride);
		}
		return discountArray;
	}

	// global static cscfga.ProductConfiguration.ProductDiscount processDiscounts(cscfga__Product_Configuration__c pc, cscfga.ProductConfiguration.ProductDiscount discountArray, List<CCAQDProcessor.CCDiscount> discounts) {

	//     /* Testing only */
	//     Attachment customDiscounts = new Attachment();
	//     customDiscounts.Name = 'CustomDiscounts.json';
	//     customDiscounts.ParentId = pc.Id;
	//     customDiscounts.Body = Blob.valueOf(JSON.serialize(discounts));
	//     insert customDiscounts;

	//     if (discounts != null){
	//         for (CCAQDProcessor.CCDiscount iter : discounts) {
	//             cscfga.ProductConfiguration.Discount chargeDiscount = createChargeDiscount(iter);
	//             discountArray.discounts.add(chargeDiscount);
	//         }
	//     }
	//     return discountArray;
	// }

	global static cscfga.ProductConfiguration.ProductDiscount processDiscounts(
		cscfga__Product_Configuration__c pc,
		cscfga.ProductConfiguration.ProductDiscount discountArray,
		List<CCAQDProcessor.CCDiscount> discounts,
		String CS_QuantityStrategy,
		Integer quantity
	) {
		/* Testing only */
		Attachment customDiscounts = new Attachment();
		customDiscounts.Name = 'CustomDiscounts.json';
		customDiscounts.ParentId = pc.Id;
		customDiscounts.Body = Blob.valueOf(JSON.serialize(discounts));
		insert customDiscounts;

		if (discounts != null) {
			for (CCAQDProcessor.CCDiscount iter : discounts) {
				cscfga.ProductConfiguration.Discount chargeDiscount = ChargesProcessorUtils.createChargeDiscount(iter, CS_QuantityStrategy, quantity);
				discountArray.discounts.add(chargeDiscount);
			}
		}
		return discountArray;
	}

	// global static List<SObject> processPricing(cscfga__Product_Configuration__c targetPC, List<CCAQDProcessor.CCCharge> charges, List<CCAQDProcessor.CCDiscount> discounts, String quantityStrategy) {
	//     cscfga.ProductConfiguration.ProductDiscount discountArray = new cscfga.ProductConfiguration.ProductDiscount();

	//     //discountArray = processCharges(targetPC, discountArray, charges, quantityStrategy);
	//     discountArray = processDiscounts(targetPC, discountArray, discounts);

	//     //targetPC.CC_Quantity__c = charges[0].quantity;

	//     cscfga.ProductConfiguration.setDiscounts(targetPC,discountArray);

	//     return new List<SObject>{targetPC};
	// }

	global static List<SObject> processPricing(
		cscfga__Product_Configuration__c targetPC,
		List<CCAQDProcessor.CCCharge> charges,
		List<CCAQDProcessor.CCDiscount> discounts,
		String CS_QuantityStrategy,
		Integer quantity,
		Boolean isExternalPricingEnabled
	) {
		//Set Quantity
		if (targetPC.cscfga__Root_Configuration__c == null) {
			if (CS_QuantityStrategy == ChargesProcessorUtils.QUANTITY_STRATEGY_DECOMPOSITION) {
				targetPC.cscfga__Quantity__c = quantity;
			}
		}

		// for now, since we don't have pricing service, isExternalPricingEnabled = FALSE
		// commenting out the code but keeping for reference
		// if(isExternalPricingEnabled) {
		//     List<cspsi.CommonCartWrapper.Discount> sfccContribution = OmniChannelChargesProcessor.getSFCCContribution(discounts, CS_QuantityStrategy, quantity);
		//     targetPC.sfcc_contribution__c = JSON.serialize(sfccContribution);

		//     List<cspsi.CommonCartWrapper.Discount> psContribution = OmniChannelChargesProcessor.getPSContribution(charges, discounts, CS_QuantityStrategy, quantity);
		//     targetPC.ps_contribution__c = JSON.serialize(psContribution);

		//     cspsi.CommonCartWrapper.CartItem cartItem = new cspsi.CommonCartWrapper.CartItem();
		//     cartItem.id = targetPC.Id;
		//     cartItem.customData = new Map<String, String>{'customFields' => Json.serialize(
		//         new Map<String, String> {
		//             'sfcc_contribution__c' => targetPC.sfcc_contribution__c,
		//             'ps_contribution__c' => targetPC.ps_contribution__c
		//         }
		//     )};
		//     List<cspsi.CommonCartWrapper.Discount> aggregatedDiscounts = cspsi.PricingAggregator.aggregateCartItemPricing(cartItem);
		//     Map<String, Object> discountsMap = new Map<String, Object>{'discounts' => aggregatedDiscounts};
		//     targetPC.cscfga__discounts__c = Json.serialize(discountsMap);
		// } else {
		cscfga.ProductConfiguration.ProductDiscount discountArray = new cscfga.ProductConfiguration.ProductDiscount();
		discountArray = processCharges(targetPC, discountArray, charges, CS_QuantityStrategy, quantity);
		discountArray = processDiscounts(targetPC, discountArray, discounts, CS_QuantityStrategy, quantity);
		cscfga.ProductConfiguration.setDiscounts(targetPC, discountArray);
		//}

		return new List<SObject>{ targetPC };
	}

	public class DataValidationException extends Exception {
	}

	/*
        The only way to create Charges in CSCFGA is
        to create Discounts with type Override and set them against PC and call calc totals
        Calc totals will be called by csb2c package once all observers are done
    */
	/*
    private Static cscfga.ProductConfiguration.Discount createDiscountToInitCharge(CCAQDProcessor.CCCharge aqdCharge, String discountPrice, String quantityStrategy) {
        cscfga.ProductConfiguration.Discount returnCharge = new cscfga.ProductConfiguration.Discount();
        if (aqdCharge.quantity == null) { // Should be the same as the quantity for the root product line item.
            aqdCharge.quantity = 1;
        }

        Decimal price = (discountPrice == SALES_DISCOUNT_PRICE) ? aqdCharge.salesPrice : aqdCharge.listPrice;
        if (quantityStrategy == QUANTITY_STRATEGY_MULTIPLIER){
            returnCharge.amount = price * aqdCharge.quantity;
        } else {
            returnCharge.amount = price;
        }

        returnCharge.chargeType = aqdCharge.chargeType;
        returnCharge.type = OVERRIDE_CHARGE_TYPE;
        //returnCharge.discountCharge = aqdCharge.isProductLevelPricing ? 'Charge for ' + aqdCharge.name : aqdCharge.name;
        //charge name has to be the same and not be changed from what CC sends in so discounts can correctly target them
        returnCharge.discountCharge = aqdCharge.name + ' Charge';
        returnCharge.discountPrice = discountPrice;
        returnCharge.source = aqdCharge.source;
        returnCharge.version = DISCOUNT_VERSION;
        returnCharge.description = aqdCharge.description;

        return returnCharge;
    }

    private Static cscfga.ProductConfiguration.Discount createChargeDiscount(CCAQDProcessor.CCDiscount aqdDiscount) {
        cscfga.ProductConfiguration.Discount returnDiscount = new cscfga.ProductConfiguration.Discount();

        returnDiscount.amount = aqdDiscount.amount;
        returnDiscount.type = aqdDiscount.type == 'PER' ? PERCENTAGE_DISCOUNT_TYPE : ABSOLUTE_DISCOUNT_TYPE;
        returnDiscount.chargeType = aqdDiscount.chargeType;
        returnDiscount.discountCharge = '__PRODUCT__'; // aqdDiscount.discountCharge + ' Charge'; // this is just to follow old line item structure, should be changed in the future
        returnDiscount.discountPrice = SALES_DISCOUNT_PRICE;
        returnDiscount.source = aqdDiscount.source;
        returnDiscount.version = DISCOUNT_VERSION;
        returnDiscount.description = aqdDiscount.source + ' for ' + aqdDiscount.discountCharge;

        return returnDiscount;
    }
    */
}
