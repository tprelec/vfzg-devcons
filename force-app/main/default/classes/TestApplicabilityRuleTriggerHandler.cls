/**
 * @description       : Apex Test Class for the ApplicabilityRuleTriggerHandler Apex Class
 * @author            : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * @group             :
 * @last modified on  : 19-07-2022
 * @last modified by  : Daniel Guzman (daniel.guzman@vodafoneziggo.com)
 * Modifications Log
 * Ver   Date         Author                                            Modification
 * 1.0   19-07-2022   Daniel Guzman (daniel.guzman@vodafoneziggo.com)   Initial Version
 **/

@isTest
public class TestApplicabilityRuleTriggerHandler {
	@testSetup
	static void setup() {
		ExternalIDNumber__c objExternalIDNumber = new ExternalIDNumber__c(
			Name = 'Applicability Rule',
			External_Number__c = 1,
			Object_Prefix__c = 'AR-17-',
			blocked__c = false,
			runningOrg__c = UserInfo.getOrganizationId()
		);
		insert objExternalIDNumber;

		csclm__Clause_Definition__c objClauseDefinition = new csclm__Clause_Definition__c(
			ExternalID__c = CS_DataTest.generateRandomString(10),
			Promo_start_date__c = System.today().addDays(-10),
			Promo_end_date__c = System.today().addDays(-3),
			DynamicId__c = 'Test'
		);
		insert objClauseDefinition;
	}

	@isTest
	static void testSetExternalIds() {
		csclm__Clause_Definition__c objClauseDefinition = [SELECT Id FROM csclm__Clause_Definition__c LIMIT 1];
		csclm__Applicability_Rule__c objApplicabilityRule = new csclm__Applicability_Rule__c(csclm__Clause_Definition__c = objClauseDefinition.Id);

		Test.startTest();
		insert objApplicabilityRule;
		Test.stopTest();

		csclm__Applicability_Rule__c objApplicabilityRuleUpdated = [
			SELECT Id, ExternalID__c
			FROM csclm__Applicability_Rule__c
			WHERE Id = :objApplicabilityRule.Id
			LIMIT 1
		];

		System.assertEquals(
			'AR-17-000002',
			objApplicabilityRuleUpdated.ExternalID__c,
			'The External ID on Applicability Rule was not set correctly.'
		);
	}
}
