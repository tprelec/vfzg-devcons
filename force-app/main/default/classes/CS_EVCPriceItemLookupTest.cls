@isTest
public class CS_EVCPriceItemLookupTest {
private static testMethod void testRequiredAttributes(){
         CS_EVCPriceItemLookup caLookup = new  CS_EVCPriceItemLookup();
        caLookup.getRequiredAttributes();
    }
  private static testMethod void test() {
      List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        User simpleUser = CS_DataTest.createUser(pList, roleList); 
        insert simpleUser;
     System.runAs (simpleUser) { 
      
     
      Framework__c frameworkSetting = new Framework__c();
      frameworkSetting.Framework_Sequence_Number__c = 2;
      insert frameworkSetting;
      
      PriceReset__c priceResetSetting = new PriceReset__c();
       
        priceResetSetting.MaxRecurringPrice__c = 200.00;
        priceResetSetting.ConfigurationName__c = 'IP Pin';
         
     insert priceResetSetting;
      Account testAccount = CS_DataTest.createAccount('Test Account');
      insert testAccount;
      
      Sales_Settings__c ssettings = new Sales_Settings__c();
      ssettings.Postalcode_check_validity_days__c = 2;
      ssettings.Max_Daily_Postalcode_Checks__c = 2;
      ssettings.Number_of_Days_for_Closed_Won_Date__c = 15;
      ssettings.Postalcode_check_block_period_days__c = 2;
      ssettings.Max_weekly_postalcode_checks__c = 15;
      insert ssettings;
      
      
      //price items
      OrderType__c orderType = CS_DataTest.createOrderType();
      insert orderType;
      Product2 product1 = CS_DataTest.createProduct('EVC', orderType);
      insert product1;
      
      Category__c mobileCategory = CS_DataTest.createCategory('EVC');
      insert mobileCategory;
      
      Vendor__c vendor1 = CS_DataTest.createVendor('KPNWEAS');
      insert vendor1;
    
        
    
        cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(product1, mobileCategory, 20480.0, 20480.0, vendor1, 'ONNET', 'OfficeTTR2BD');
        
        insert priceItem1;
      
      
      
      Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp',simpleUser.id);
      insert testOpp;
      
      
      cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
        insert basket;
        
       
        
        Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName().get('Product Definition').getRecordTypeId(); 
        
        
        cscfga__Product_Definition__c evcProductDef = CS_DataTest.createProductDefinition('EVC');
        evcProductDef.Product_Type__c = 'Fixed';
        evcProductDef.RecordTypeId = productDefinitionRecordType;
        insert evcProductDef;
        
        
        
        
        cscfga__Product_Configuration__c evcProductConf = CS_DataTest.createProductConfiguration(evcProductDef.Id, 'EVC',basket.Id);
        evcProductConf.cscfga__Root_Configuration__c = null;
        evcProductConf.cscfga__Parent_Configuration__c = null;
        insert evcProductConf;
        
        
        
       
        csbb__Product_Configuration_Request__c pcr= CS_DataTest.createPCR(evcProductConf);
        pcr.csbb__Product_Configuration__c = evcProductConf.Id;
        insert pcr;
        
        
        Map<String, String> searchFields = new Map<String, String>();
        searchFields.put('Access Type', 'EthernetOverFiber');
        searchFields.put('AdditionType', 'Manual');
        searchFields.put('Quality of Service', 'Yes');
        searchFields.put('Available bandwidth up', '20480.0');
        searchFields.put('Available bandwidth down', '20480.0');
        searchFields.put('Num WANLAN', '2');
        searchFields.put('Num WAN', '1');
        searchFields.put('Num LAN', '1');
        searchFields.put('CPE SLA', 'OfficeTTR2BD');
        searchFields.put('Contract Duration', '36');
        searchFields.put('Technology type', 'ISDN30');
        searchFields.put('Codec', 'G711');
        searchFields.put('SIP Channels', '4');
        searchFields.put('Wireless backup', 'Yes');
        searchFields.put('Overbooking Type', 'IAD');
        searchFields.put('Num SFP','4');
        searchFields.put('Premium Applicable','true');
        // return '["Available bandwidth down","Overbooking Type","Access Type","Available bandwidth up","Infra SLA","Vendor","Proposition","Contract Duration", "Overbooking Data", "Overbooking Voip", "AdditionType", "Min. Upstream Bandwidth", "Min. Upstream Bandwidth 2", "Min.Downstream Bandwidth", "Min.Downstream Bandwidth 2", "Total Bandwidth Up", "Total Bandwidth Down", "Total Bandwidth Up Entry", "Total Bandwidth Down Entry", "Total Bandwidth Up Premium", "Total Bandwidth Down Premium", "Available bandwidth up premium", "Available bandwidth down premium", "Premium Applicable", "Overbooking Internet", "Min. Upstream Bandwidth Internet", "Min.Downstream Bandwidth Internet", "Proposition Internet", "Overbooking Transport", "Proposition OneFixed", "Overbooking OneFixed", "Min Bandwidth OneFixed", "IPVPN", "Managed Internet", "OneFixed", "Entry BW 6MB","Overbooking OneNet","Min Bandwidth OneNet","Proposition OneNet", "One Net"]';
      
        
        
        CS_EVCPriceItemLookup caLookup = new CS_EVCPriceItemLookup();
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
        searchFields.put('Overbooking Type', 'Data');
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
        searchFields.put('Overbooking Type', 'Voip');
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
        searchFields.put('Overbooking Type', 'Internet');
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
        searchFields.put('Overbooking Type', 'Transport Data');
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
        searchFields.put('Overbooking Type', 'Transport Voip');
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
        searchFields.put('Overbooking Type', 'Transport Internet');
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
        searchFields.put('Overbooking Type', 'OneNet');
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
        searchFields.put('Overbooking Type', 'OneFixed');
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
        searchFields.put('Overbooking Type', 'Transport EVC');
        caLookup.doLookupSearch(searchFields, String.valueOf(evcProductDef.Id),null, 0, 0);
     
     }
  }

}