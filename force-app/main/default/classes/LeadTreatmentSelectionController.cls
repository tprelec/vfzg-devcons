public class LeadTreatmentSelectionController {

	private final Lead lead;
	public Boolean treatmentIsEditable{get;private set;}
	public Boolean treatmentChanged {get;set;}
	public String error {get;set;}

	public LeadTreatmentSelectionController(ApexPages.StandardController stdController) {
		Lead myLead = (Lead)stdController.getRecord();
		Id LeadId = myLead.Id;
		this.lead = [select Id,
							Name, 
							LeadSource, 
							Treatment_01__c, 
							Treatment_02__c, 
							Treatment_03__c, 
							Treatment_04__c, 
							Treatment_05__c, 
							Treatment_06__c, 
							Treatment_07__c,
							Treatment_08__c,
							Chosen_Treatment__c
						from Lead
						where Id = : leadId];
		treatmentChanged = false;
		error = null;
		if(treatments.size() == 1){
			treatmentIsEditable = false;
		} else {
			treatmentIsEditable = true;
		}
		if(lead.Chosen_Treatment__c != null)selectedTreatment = lead.Chosen_Treatment__c;
	}

	public String selectedTreatment {get;set;}

	public List<SelectOption> treatments {
		get{
			return LeadTreatMentHelper.GetTreatmentSelectOptions(lead);
		}
		private set;
	}

	public pageReference changeTreatment(){
		treatmentChanged = true;
		return null;
	}

	public PageReference updateTreatment(){
		
		Savepoint sp = Database.setSavepoint();
		lead.Chosen_Treatment__c = selectedTreatment;

		try{
			update lead;
			treatmentChanged = false;
		} catch (Exception e){
			error = 'You can\'t change the treatment after the review period has ended';
			Database.rollback(sp);
		}
		
		return null;
	}
}