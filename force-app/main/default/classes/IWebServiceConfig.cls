/**
 * @description      This interface represents a single web service configuration item, i.e. the username
 *            and endpoint information etc.
 * @author        Guy Clairbois
 */
public interface IWebServiceConfig {
  String getUsername();
  void setUsername(String username);
  String getPassword();
  void setPassword(String password);
  String getEndpoint();
  void setEndpoint(String endpoint);
  String getCertificateName();
  void setCertificateName(String certificateName);  
}