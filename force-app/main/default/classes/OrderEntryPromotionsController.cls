@SuppressWarnings('PMD.ExcessiveParameterList')
public with sharing class OrderEntryPromotionsController {
	/**
	 * @description Gets Bundle Promotions
	 * @param  opportunityId Opportunity ID
	 * @param  b2cChecked    Defines if customer is already B2C Customer
	 * @param  products      Selected OE Products
	 * @param  addons        Selected OE Add Ons
	 * @param  bundle        OE Bundle
	 * @param  site          Installation Site
	 * @return               List of avaliable Bundle Promotions
	 */
	@AuraEnabled
	public static List<OE_Promotion__c> getBundlePromotions(
		Id opportunityId,
		Boolean b2cChecked,
		List<OrderEntryData.OrderEntryProduct> products,
		List<OrderEntryData.OrderEntryAddon> addons,
		OrderEntryData.OrderEntryBundle bundle,
		OrderEntryData.OrderEntrySite site
	) {
		return getPromotions(opportunityId, b2cChecked, products, addons, bundle.contractTerm, site.addressCheckResult, site.offNetType, 'Standard');
	}

	/**
	 * @description Gets Special Promotions
	 * @param  opportunityId Opportunity ID
	 * @param  b2cChecked    Defines if customer is already B2C Customer
	 * @param  products      Selected OE Products
	 * @param  addons        Selected OE Add Ons
	 * @param  bundle        OE Bundle
	 * @param  site          Installation Site
	 * @return               List of avaliable Special Promotions
	 */
	@AuraEnabled
	public static List<OE_Promotion__c> getSpecialPromotions(
		Id opportunityId,
		Boolean b2cChecked,
		List<OrderEntryData.OrderEntryProduct> products,
		List<OrderEntryData.OrderEntryAddon> addons,
		OrderEntryData.OrderEntryBundle bundle,
		OrderEntryData.OrderEntrySite site
	) {
		return getPromotions(opportunityId, b2cChecked, products, addons, bundle.contractTerm, site.addressCheckResult, site.offNetType, 'Special');
	}

	/**
	 * @description Gets Automatic Promotions
	 * @param  opportunityId      Opportunity ID
	 * @param  b2cChecked         Defines if customer is already B2C Customer
	 * @param  contractTerm       Contract Term (Duration)
	 * @param  addressCheckResult Address Check Result (On-Net, Off-Net)
	 * @param  offNetType         Address Check Off-Net Type
	 * @param  products           Selected OE Products
	 * @param  addons             Selected OE Add Ons
	 * @return                    List of Automatic Promotions
	 */
	@AuraEnabled
	public static List<OE_Promotion__c> getAutomaticPromotions(
		Id opportunityId,
		Boolean b2cChecked,
		Integer contractTerm,
		String addressCheckResult,
		String offNetType,
		List<OrderEntryData.OrderEntryProduct> products,
		List<OrderEntryData.OrderEntryAddon> addons
	) {
		return getPromotions(opportunityId, b2cChecked, products, addons, String.valueOf(contractTerm), addressCheckResult, offNetType, 'Automatic');
	}

	/**
	 * @description Gets Promotions for provided parameters
	 * @param  opportunityId      Opportunity ID
	 * @param  b2cChecked         Defines if customer is already B2C Customer
	 * @param  products           Selected OE Products
	 * @param  addons             Selected OE Add Ons
	 * @param  contractTerm       Contract Term (Duration)
	 * @param  addressCheckResult Address Check Result (On-Net, Off-Net)
	 * @param  offNetType         Address Check Off-Net Type
	 * @param  promotionType      Promotion Type (Bundle, Special, Automatic)
	 * @return                    List of availale promotions
	 */
	private static List<OE_Promotion__c> getPromotions(
		Id opportunityId,
		Boolean b2cChecked,
		List<OrderEntryData.OrderEntryProduct> products,
		List<OrderEntryData.OrderEntryAddon> addons,
		String contractTerm,
		String addressCheckResult,
		String offNetType,
		String promotionType
	) {
		String customerType = b2cChecked != null && b2cChecked ? 'Existing' : 'New';
		Set<Id> productIds = getProductIds(products);
		Set<Id> addonIds = getAddonIds(addons);
		List<OE_Promotion__c> promotions = [
			SELECT
				Id,
				Promotion_Name__c,
				Contract_Term__c,
				Customer_Type__c,
				Connection_Type__c,
				Off_net_Type__c,
				(
					SELECT
						Discount__r.Name,
						Discount__r.Type__c,
						Discount__r.Value__c,
						Discount__r.One_Off_Value__c,
						Discount__r.Duration__c,
						Discount__r.Level__c,
						Discount__r.Product__c,
						Discount__r.Add_On__c
					FROM Discounts__r
				)
			FROM OE_Promotion__c
			WHERE
				Active__c = TRUE
				AND Type__c = :promotionType
				AND Customer_Type__c INCLUDES (:customerType)
				AND Contract_Term__c INCLUDES (:contractTerm)
				AND (Connection_Type__c = :addressCheckResult
				OR Connection_Type__c = NULL)
				AND (Off_net_Type__c = :offNetType
				OR Off_net_Type__c = NULL)
			ORDER BY Promotion_Name__c
		];
		return filterValidPromotions(promotions, opportunityId, productIds, addonIds);
	}

	/**
	 * @description Filters queired promotions based on selected products, addons and validity period.
	 * @param  promotions    Queried OE Promotions
	 * @param  opportunityId Opportunity ID
	 * @param  productIds    Selected OE Products
	 * @param  addonIds      Selected OE Add Ons
	 * @return               Filtered list of available Promotions
	 */
	public static List<OE_Promotion__c> filterValidPromotions(
		List<OE_Promotion__c> promotions,
		Id opportunityId,
		Set<Id> productIds,
		Set<Id> addonIds
	) {
		List<OE_Promotion__c> filterPromotions = filterAvailablePromotions(promotions, productIds, addonIds);
		List<OE_Promotion__c> promotionForReturn = new List<OE_Promotion__c>();
		Set<Id> promotionIds = OrderEntryValidityService.validatePeriod(opportunityId, filterPromotions);
		for (OE_Promotion__c promo : filterPromotions) {
			if (promotionIds.contains(promo.Id)) {
				promotionForReturn.add(promo);
			}
		}
		return promotionForReturn;
	}

	/**
	 * @description Filters Promootions based on selected products and addons
	 * @param  promotions Queried OE Promotions
	 * @param  productIds Selected OE Products
	 * @param  addonIds   Selected OE Add Ons
	 * @return            Filtered list of available Promotions
	 */
	private static List<OE_Promotion__c> filterAvailablePromotions(List<OE_Promotion__c> promotions, Set<Id> productIds, Set<Id> addonIds) {
		List<OE_Promotion__c> availablePromotions = new List<OE_Promotion__c>();

		for (OE_Promotion__c promo : promotions) {
			Boolean isPromotionAvailable = true;

			for (OE_Promotion_Discount__c discount : promo.Discounts__r) {
				if (discount.Discount__r.Product__c != null && !productIds.contains(discount.Discount__r.Product__c)) {
					isPromotionAvailable = false;
					break;
				} else if (discount.Discount__r.Add_On__c != null && !addonIds.contains(discount.Discount__r.Add_On__c)) {
					isPromotionAvailable = false;
					break;
				}
			}
			if (isPromotionAvailable) {
				availablePromotions.add(promo);
			}
		}
		return availablePromotions;
	}

	/**
	 * @description Gets IDs of selected OE Products.
	 * @param  products Selected OE Products
	 * @return          Set of Product IDs
	 */
	private static Set<Id> getProductIds(List<OrderEntryData.OrderEntryProduct> products) {
		Set<Id> productIds = new Set<Id>();
		if (products != null) {
			for (OrderEntryData.OrderEntryProduct product : products) {
				productIds.add(product.id);
			}
		}
		return productIds;
	}

	/**
	 * @description Gets IDs of selected OE Add Ons.
	 * @param  addons Selected OE Add Ons
	 * @return        Set of Add On IDs
	 */
	private static Set<Id> getAddonIds(List<OrderEntryData.OrderEntryAddon> addons) {
		Set<Id> addonIds = new Set<Id>();
		if (addons != null) {
			for (OrderEntryData.OrderEntryAddon addon : addons) {
				addonIds.add(addon.id);
			}
		}
		return addonIds;
	}
}
