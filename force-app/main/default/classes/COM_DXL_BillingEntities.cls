public with sharing class COM_DXL_BillingEntities {
	JSONGenerator generator;
	List<Opportunity> opportunityData;
	public COM_DXL_BillingEntities(JSONGenerator generator, List<Opportunity> opportunityData) {
		this.generator = generator;
		this.opportunityData = opportunityData;
	}

	public void generateBillingEntities() {
		generator.writeFieldName('billingEntities');
		generator.writeStartObject();

		generator.writeStringField(
			'paymentMethod',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Payment_method__c)
		);
		generator.writeStringField(
			'financialAccountName',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Financial_Contact__r.Name)
		);
		generator.writeStringField(
			'unifyFinancialAccountRefId',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Unify_Ref_Id__c)
		);
		generator.writeStringField(
			'unifyBillingAccountRefId',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Billing_Arrangement__r.Unify_Ref_Id__c)
		);
		generator.writeStringField(
			'billFormat',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Billing_Arrangement__r.Bill_Format__c)
		);
		generator.writeStringField(
			'billingArrangementName',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Billing_Arrangement__r.Billing_Arrangement_Alias__c)
		);

		generatePayMeans();
		generateAddress();

		generator.writeEndObject();
	}

	private void generatePayMeans() {
		generator.writeFieldName('payMeans');
		generator.writeStartObject();

		generator.writeStringField(
			'bankAccountName',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Bank_Account_Name__c)
		);
		generator.writeStringField(
			'bankAccountNumber',
			COM_DXL_CreateCustomerService.checkNullAndEmptyValues(opportunityData[0].Financial_Account__r.Bank_Account_Number__c)
		);

		generator.writeEndObject();
	}

	private void generateAddress() {
		generator.writeFieldName('invoiceAddress');
		Map<String, String> addressData = generateAddressData(opportunityData[0]);
		COM_DXL_Address dxlAddress = new COM_DXL_Address(generator, addressData);
		dxlAddress.generateAddress();
	}

	private Map<String, String> generateAddressData(Opportunity opportunityData) {
		Map<String, String> returnData = new Map<String, String>();
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_STREET, opportunityData.Billing_Arrangement__r.Billing_Street__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUMBER, String.valueOf(opportunityData.Billing_Arrangement__r.Billing_Housenumber__c));
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUM_EXT, opportunityData.Billing_Arrangement__r.Billing_Housenumber_Suffix__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_POSTCODE, opportunityData.Billing_Arrangement__r.Billing_Postal_Code__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_CITY, opportunityData.Billing_Arrangement__r.Billing_City__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_COUNTRY, opportunityData.Billing_Arrangement__r.Billing_Country__c);
		return returnData;
	}
}
