public with sharing class COM_DXL_Sites {
	JSONGenerator generator;
	Map<Id, List<External_Site__c>> sitesWithExternalSites;
	List<Site__c> sites;

	public COM_DXL_Sites(JSONGenerator generator, List<Site__c> sites, Map<Id, List<External_Site__c>> sitesWithExternalSites) {
		this.generator = generator;
		this.sites = sites;
		this.sitesWithExternalSites = sitesWithExternalSites;
	}

	public void generateSites() {
		generator.writeFieldName('sites');
		generator.writeStartArray();

		for (Site__c siteRecord : sites) {
			generator.writeStartObject();
			generator.writeStringField(
				'unifySiteRefId',
				getExternalSiteExternalId(sitesWithExternalSites.get(siteRecord.Id), COM_DXL_Constants.UNIFY_EXTERNAL_SYSTEM)
			);
			generator.writeStringField('name', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(siteRecord.Unify_Site_Name__c));
			generator.writeStringField('unifySitename', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(siteRecord.Unify_Site_Name__c));
			generator.writeStringField('salesforceSiteId', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(siteRecord.Id));
			generator.writeStringField(
				COM_DXL_Constants.SFDC_ACCOUNT_ID,
				COM_DXL_CreateCustomerService.checkNullAndEmptyValues(siteRecord.Site_Account__c)
			);
			generator.writeStringField('slaCode', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(siteRecord.SLA__r.BOP_Code__c));
			generator.writeStringField('locationTypeName', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(siteRecord.Location_Type__c));
			generator.writeStringField('phone', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(siteRecord.Site_Phone__c));
			generator.writeStringField('buildingName', COM_DXL_CreateCustomerService.checkNullAndEmptyValues(siteRecord.Building__c));
			generator.writeStringField('site_Level_Services_CI', getSiteLeveLServiceCatalogId());

			generateSiteAddress(generator, siteRecord);
			generateSiteCanvasAddress(generator, siteRecord);

			generator.writeEndObject();
		}

		generator.writeEndArray();
	}

	private void generateSiteAddress(JSONGenerator generator, Site__c siteRecord) {
		generator.writeFieldName('address');
		Map<String, String> addressData = generateSiteAddressData(siteRecord);
		COM_DXL_Address dxlAddress = new COM_DXL_Address(generator, addressData);
		dxlAddress.generateAddress();
	}

	private void generateSiteCanvasAddress(JSONGenerator generator, Site__c siteRecord) {
		generator.writeFieldName('canvasAddress');
		Map<String, String> addressData = generateSiteCanvasAddressData(siteRecord);
		COM_DXL_Address dxlAddress = new COM_DXL_Address(generator, addressData);
		dxlAddress.generateAddress();
	}

	private String getExternalSiteExternalId(List<External_Site__c> externalSites, String externalSystemName) {
		String returnValue = '';

		if (externalSites != null) {
			for (External_Site__c externalSite : externalSites) {
				if (!String.isBlank(externalSite.External_Site_Id__c) && externalSystemName.equalsIgnoreCase(externalSite.External_Source__c)) {
					returnValue = externalSite.External_Site_Id__c;
				}
			}
		}

		return returnValue;
	}

	private Map<String, String> generateSiteAddressData(Site__c siteRecord) {
		Map<String, String> returnData = new Map<String, String>();
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_STREET, siteRecord.Site_Street__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUMBER, String.valueOf(siteRecord.Site_House_Number__c));
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUM_EXT, siteRecord.Site_House_Number_Suffix__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_POSTCODE, siteRecord.Site_Postal_Code__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_CITY, siteRecord.Site_City__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_COUNTRY, siteRecord.Country__c);
		return returnData;
	}

	private Map<String, String> generateSiteCanvasAddressData(Site__c siteRecord) {
		Map<String, String> returnData = new Map<String, String>();
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_STREET, siteRecord.Canvas_Street__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUMBER, String.valueOf(siteRecord.Canvas_House_Number__c));
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_HOUSE_NUM_EXT, siteRecord.Canvas_House_Number_Suffix__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_POSTCODE, siteRecord.Canvas_Postal_Code__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_CITY, siteRecord.Canvas_City__c);
		returnData.put(COM_DXL_Constants.COM_DXL_ADDRESS_COUNTRY, siteRecord.Canvas_Country__c);
		return returnData;
	}

	private String getSiteLeveLServiceCatalogId() {
		COM_DXL_settings__mdt dxlSettings = COM_DXL_settings__mdt.getInstance(COM_DXL_Constants.COM_DXL_INTEGRATION_SLS_CATALOG_ID_SETTING_NAME);

		String slsCatalogId = dxlSettings != null ? dxlSettings.Value__c : '5154509';

		return slsCatalogId;
	}
}
