@RestResource(urlMapping='/customer')
global without sharing class CustomerInfoRestService {
	@HttpPost
	global static void getOrGenerateContactData() {
		RestRequest req = RestContext.request;
		RestResponse res = RestContext.response;
		// Build response
		if (res == null) {
			res = new RestResponse();
			RestContext.response = res;
		}
		res.addHeader('Content-Type', 'text/json');

		try {
			// Deserialize JSON
			CustomerInfo body = (CustomerInfo) JSON.deserializeStrict(req.requestBody.tostring(), CustomerInfo.class);

			// Fetch data
			CustomerInfo responseCustomerInfo = new CustomerInfo();

			if (Pattern.matches('[0-9]{8}', body.KVK_number_c)) {
				Account acc = CostumerInfoDataService.getAccountData(body.KVK_number_c);
				if (acc == null) {
					throw new CustomerInfoException(
						'Unable to create the account. Contact your administrator and provide him the KVK number ' + body.KVK_number_c
					);
				}

				List<Site__c> sites = CostumerInfoDataService.getSiteData(acc.Id, prepareSiteData(body.sites));

				List<Contact> contacts = CostumerInfoDataService.getContactData(parseContacts(body.contacts, acc.Id));
				if (contacts.isEmpty()) {
					throw new CustomerInfoException('Unable to get/create contacts.');
				}

				CostumerInfoDataService.checkTypeAndSubType(acc.Id);

				// Prepare response data
				responseCustomerInfo.AccountId = acc.Id;
				responseCustomerInfo.Name = acc.Name;
				responseCustomerInfo.KVK_number_c = acc.KVK_number__c;
				responseCustomerInfo.csb2c_E_Commerce_Customer_Id_c = body.csb2c_E_Commerce_Customer_Id_c;
				responseCustomerInfo.contacts = castToContacts(contacts);
				responseCustomerInfo.sites = prepareSitesForOnline(sites);
			} else {
				throw new CustomerInfoException('Invalid KVK number format.');
			}

			res.statusCode = 200;
			res.responseBody = Blob.valueOf(JSON.serialize(responseCustomerInfo));
		} catch (Exception e) {
			res.statusCode = 500;
			res.responseBody = Blob.valueOf('errors: [ message: ' + e.getMessage() + ']');
		}
	}

	// Helper method to parse Contacts obj that is received from B2B online service to stasndard sf Contact  obj
	@testVisible
	private static List<Contact> parseContacts(List<Contacts> contacts, Id accId) {
		List<Contact> cntList = new List<Contact>();

		for (Contacts c : contacts) {
			Contact newCont = new Contact();
			newCont.FirstName = c.FirstName;
			if (c.MiddleName != null) {
				newCont.LastName = c.MiddleName + ' ' + c.LastName;
			} else {
				newCont.LastName = c.LastName;
			}
			newCont.AccountId = accId;
			newCont.Gender__c = determinGender(c.Gender_c.capitalize(), false);
			newCont.MobilePhone = c.MobilePhone;
			newCont.Email = c.Email;
			newCont.Authorized_to_sign__c = Boolean.valueOf(c.Authorized_to_sign_c);
			// Parse date from dd-mm-yyyy format
			if (c.DateOfBirth != null) {
				List<String> datePatrs = c.DateOfBirth.split('-');
				newCont.Birthdate = date.newInstance(Integer.valueOf(datePatrs[2]), Integer.valueOf(datePatrs[1]), Integer.valueOf(datePatrs[0]));
			}
			cntList.add(newCont);
		}
		return cntList;
	}

	// Helper method to cast from standard sf Contact to Contacts obj that is received from B2B online service
	@testVisible
	private static List<Contacts> castToContacts(List<Contact> contacts) {
		List<Contacts> cntList = new List<Contacts>();

		for (Contact c : contacts) {
			Contacts newCont = new Contacts();
			newCont.Id = c.Id;
			newCont.FirstName = c.FirstName;
			// Check if there is multiple last names, and return it in middle and last name
			List<String> lastNameArray = c.LastName.split(' ');
			if (lastNameArray.size() > 1) {
				newCont.LastName = lastNameArray.remove(lastNameArray.size() - 1);
				newCont.MiddleName = String.join(lastNameArray, ' ');
			} else {
				newCont.LastName = c.LastName;
			}
			// Handle gender format
			newCont.Gender_c = determinGender(c.Gender__c, true);
			newCont.MobilePhone = c.MobilePhone;
			newCont.Email = c.Email;
			newCont.Authorized_to_sign_c = String.valueOf(c.Authorized_to_sign__c);
			newCont.DateOfBirth = '';
			// Parse date to expected format
			if (c.Birthdate != null) {
				List<String> datePatrs = String.valueOf(c.Birthdate).split('-');
				newCont.DateOfBirth = datePatrs[2] + '-' + datePatrs[1] + '-' + datePatrs[0];
			}
			cntList.add(newCont);
		}
		return cntList;
	}

	// Helper method to map between the gender values from commerce cloud and sales cloud
	@testVisible
	private static String determinGender(String gender, Boolean toCommerceCloud) {
		String formatedGender = '';
		if (toCommerceCloud) {
			if (gender == 'Male') {
				formatedGender = 'male';
			} else if (gender == 'Female') {
				formatedGender = 'female';
			} else {
				formatedGender = 'neutral';
			}
		} else {
			if (gender.capitalize().startsWith('M') || gender.capitalize().startsWith('F')) {
				formatedGender = gender.capitalize();
			} else {
				formatedGender = '.';
			}
		}
		return formatedGender;
	}

	public static final string POSTCODE = 'Postcode';
	public static final string HOUSENUMBER = 'HouseNumber';
	public static final string HOUSENRSUFFIX = 'HouseNrSuffix';
	public static final string SITESTREET = 'Street';
	public static final string SITECITY = 'City';

	// Helper method to prepare site data for service

	// Prepare received site data for service
	@testVisible
	private static List<Map<String, String>> prepareSiteData(List<Site> sites) {
		List<Map<String, String>> sitesData = new List<Map<String, String>>();

		for (Site s : sites) {
			Map<String, String> sd = new Map<String, String>();
			sd.put(POSTCODE, s.Postcode);
			sd.put(HOUSENUMBER, s.HouseNumber);
			sd.put(HOUSENRSUFFIX, s.HouseNrSuffix);
			sd.put(SITESTREET, s.Street);
			sd.put(SITECITY, s.City);

			sitesData.add(sd);
		}
		return sitesData;
	}

	// Prepare the returned Site__c data for commerce cloud
	@testVisible
	private static List<Site> prepareSitesForOnline(List<Site__c> sites) {
		List<Site> sitesData = new List<Site>();

		for (Site__c s : sites) {
			Site st = new Site();
			st.HouseNrSuffix = s.Site_House_Number_Suffix__c;
			st.HouseNumber = String.valueOf(s.Site_House_Number__c);
			st.Postcode = s.Site_Postal_Code__c;
			st.Id = s.Id;
			st.City = s.Site_City__c;
			st.Street = s.Site_Street__c;

			sitesData.add(st);
		}
		return sitesData;
	}

	@SuppressWarnings('PMD.FieldNamingConventions')
	public class CustomerInfo {
		public String AccountId;
		public String Name;
		public String KVK_number_c;
		public String csb2c_E_Commerce_Customer_Id_c;
		public List<Contacts> contacts;
		public List<Site> sites;
	}

	@SuppressWarnings('PMD.FieldNamingConventions')
	public class Contacts {
		public String Id;
		public String FirstName;
		public String MiddleName;
		public String LastName;
		public String Gender_c;
		public String MobilePhone;
		public String Email;
		public String Authorized_to_sign_c;
		public String DateOfBirth;
	}

	@SuppressWarnings('PMD.FieldNamingConventions')
	public class Site {
		public String Id;
		public String Postcode;
		public String HouseNumber;
		public String HouseNrSuffix;
		public String City;
		public String Street;
	}

	public class CustomerInfoException extends Exception {
	}
}
