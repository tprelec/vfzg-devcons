@isTest
private class TestOrderValidation {
	@isTest
	static void testOpportunityValidation() {
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createCompleteOpportunity();

		Test.startTest();
		Set<Id> oppids = new Set<Id>{ TestUtils.theOpportunity.Id };
		String result = OrderValidation.checkCompleteOpportunities(null, oppids);
		Map<String, Map<String, Set<String>>> tstMap = OrderValidation.objectIdToFieldNameToChildrenTypes;
		OrderValidation.childrenTypesFound(TestUtils.theAccount.Id, 'contact_roles__r');
		OpportunityLineItem opli = [SELECT Id FROM OpportunityLineItem LIMIT 1];
		opli.family_condition__c = 'asdf';
		opli.proposition__c = 'Fixed';
		update opli;

		Opportunity_Attachment__c oa = new Opportunity_Attachment__c(Opportunity__c = TestUtils.TheOpportunity.Id, Attachment_Type__c = 'Other');
		insert oa;
		OrderValidation.attachmentTypesFound(TestUtils.theOpportunity.Id, 'opportunity_attachments__r');
		OrderValidation.attachmentTypesFound(TestUtils.theOpportunity.Id, 'opportunitylineItems');
		Test.stopTest();
		System.assertNotEquals(opli, null);
	}

	@isTest
	public static void testOrderValidations() {
		TriggerHandler.preventRecursiveTrigger('UserTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('AccountTriggerHandler', null, 0);
		TriggerHandler.preventRecursiveTrigger('ContactTriggerHandler', null, 0);
		EMP_Automatic_Order_Flow__c empSettings = TestUtils.createEMPCustomSettings();
		NetProfit_Settings__c settings = TestUtils.createNetProfitCustomSettings();
		TestUtils.createOrderValidationNetProfitInformation();
		TestUtils.createOrderValidationOrder();
		TestUtils.createOrderValidationContractedProducts();
		TestUtils.createVFContractValidationOpportunity();
		TestUtils.createOrderValidationBilling();
		TestUtils.createOrderValidationOpportunity();
		TestUtils.createOrderValidationSite();
		Testutils.createOrderValidationNumberportingRow();
		TestUtils.createOrderValidationCompetitorAsset();
		TestUtils.createOrderValidationPhonebookRegistration();
		TestUtils.createOrderValidationContactRoles();
		TestUtils.createOrderValidations();

		Ordertype__c ot = new Ordertype__c(
			Name = 'Enterprise Services',
			ExportSystem__c = 'BOP',
			Status__c = 'Assigned to Inside Sales',
			ExternalId__c = 'OD-04-' + Integer.valueOf(math.rint(math.random() * 1000000))
		);
		insert ot;

		Account partnerAcc = TestUtils.createPartnerAccount();
		User pu = TestUtils.createPortalUser(partnerAcc);
		Test.startTest();
		TestUtils.createCompleteContract();
		Test.stopTest();

		Contact_Role__c cr = new Contact_Role__c();
		cr.Type__c = 'main';
		cr.Contact__c = TestUtils.thePortalContact.Id;
		cr.Contact__r = TestUtils.thePortalContact;
		cr.Account__c = TestUtils.TheContract.Account__c;
		insert cr;

		// generate the order via the orderform
		PageReference pageRef = Page.OrderFormPreload;
		pageRef.getParameters().put('contractId', TestUtils.TheContract.Id);
		Test.setCurrentPage(pageRef);
		OrderFormController controller = new OrderFormController(null);
		controller.prepareData();
		List<Order__c> orders = [
			SELECT
				Id,
				CustomerReady__c,
				BillingReady__c,
				DeliveryReady__c,
				Propositions__c,
				OrderReady__c,
				CTNReady__c,
				Status__c,
				Mobile_Fixed__c,
				PhonebookregistrationReady__c,
				Account__c,
				NumberportingReady__c,
				VF_Contract__c,
				VF_Contract__r.Contract_Sign_Date__c,
				VF_Contract__r.Opportunity__r.Escalation__c,
				VF_Contract__r.BAN__c,
				VF_Contract__r.Opportunity__r.Ban__c,
				VF_Contract__r.Apple_DEP_ID__c,
				VF_Contract__r.Count_Apple_DEP__c,
				Ready_for_Inside_Sales__c,
				Export_system_Customerdata__c
			FROM Order__c
		];
		Order__c theOrder = orders[0];

		Contract_Attachment__c ca = new Contract_Attachment__c(Contract_VF__c = TestUtils.TheContract.Id, Attachment_Type__c = 'Other');
		insert ca;
		theOrder.Status__c = 'Processed manually';
		theOrder.Contract_Ready__c = false;
		theOrder.Ready_for_Inside_Sales__c = false;
		update theOrder;
		OrderUtils.updateOrderStatus(theOrder, true);

		OrderValidation.checkCompleteCustomerFlag(
			OrderUtils.getContractRecordById(TestUtils.theContract.Id),
			new List<Contact_Role__c>{ cr },
			theOrder
		);
		OrderValidation.checkObject(pu, theOrder);
		OrderValidation.checkObject(cr, theOrder);

		System.assertNotEquals(ca, null);
	}

	@isTest
	public static void testOrderFieldChecker() {
		TestUtils.createOrderValidations();
		Account partnerAcc = TestUtils.createPartnerAccount();
		User pu = TestUtils.createPortalUser(partnerAcc);
		Test.startTest();
		TestUtils.createCompleteContract();
		Test.stopTest();

		Contact_Role__c cr = new Contact_Role__c();
		cr.Type__c = 'main';
		cr.Contact__c = TestUtils.thePortalContact.Id;
		cr.Account__c = partnerAcc.Id;
		insert cr;

		update new Account(Id = TestUtils.theAccount.Id, NumberOfEmployees = 4);
		Account acc = [
			SELECT
				Id,
				Name,
				NumberOfEmployees,
				(
					SELECT
						Id,
						Type__c,
						Account__c,
						Account__r.Name,
						Contact__c,
						Contact__r.FirstName,
						Contact__r.LastName,
						Contact__r.AccountId,
						Contact__r.Account.Name
					FROM Contact_Roles__r
				)
			FROM Account
			WHERE Id = :TestUtils.TheAccount.Id
		];

		OrderValidation.fieldChecker(acc, '!=', 'NumberOfEmployees', '3,4,5');
		OrderValidation.fieldChecker(acc, '>', 'NumberOfEmployees', '3');
		OrderValidation.fieldChecker(acc, '>=', 'NumberOfEmployees', '3');
		OrderValidation.fieldChecker(acc, '<', 'NumberOfEmployees', '5');
		OrderValidation.fieldChecker(acc, '<=', 'NumberOfEmployees', '5');
		OrderValidation.fieldChecker(acc, 'contains', 'Name', 'Test');
		OrderValidation.fieldChecker(acc, 'startsWith', 'Name', 'Test');
		OrderValidation.fieldChecker(acc, 'notStartsWith', 'Name', 'Account');
		OrderValidation.fieldChecker(acc, 'minlength', 'Name', '15');
		OrderValidation.fieldChecker(acc, 'maxlength', 'Name', '10');
		OrderValidation.fieldChecker(acc, 'isnotblank', 'Name', '');
		OrderValidation.fieldChecker(acc, 'requiredChildTypeOR', 'contact_roles__r', 'main');
		OrderValidation.fieldChecker(acc, 'hasChildType', 'contact_roles__r', 'main');
		OrderValidation.fieldChecker(acc, 'hasChildTypeOR', 'contact_roles__r', 'main');

		System.assertNotEquals(acc, null);
	}

	@isTest
	public static void testChildrenTypesFound_1() {
		Set<String> result = new Set<String>();

		VF_Contract__c obj = new VF_Contract__c();
		insert obj;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.VF_Contract__c = obj.Id;
		cp.family_condition__c = 'test';
		cp.CLC__c = 'Acq';
		insert cp;

		Test.startTest();
		result = OrderValidation.childrenTypesFound(obj.Id, 'family_condition__c');
		System.assertNotEquals(result, null);
		Test.stopTest();
	}

	@isTest
	public static void testChildrenTypesFound_2() {
		Set<String> result = new Set<String>();

		VF_Contract__c obj = new VF_Contract__c();
		insert obj;

		VF_Family_Tag__c familyTag = new VF_Family_Tag__c();
		familyTag.Name = Constants.PRODUCT2_FAMILYTAG_PBX_Trunking;
		familyTag.ExternalID__c = 'FT-00012345';
		insert familyTag;

		Ordertype__c orderTyp = TestUtils.createOrderType();

		Product2 prod = new Product2();
		prod.Name = 'test';
		prod.Product_Line__c = 'fVodafone';
		prod.Ordertype__c = orderTyp.id;
		prod.Family_Tag__r = familyTag;
		insert prod;

		Contracted_Products__c cp = new Contracted_Products__c();
		cp.VF_Contract__c = obj.Id;
		cp.family_condition__c = 'test';
		cp.CLC__c = 'Acq';
		cp.Product__c = prod.Id;
		insert cp;

		Test.startTest();
		result = OrderValidation.childrenTypesFound(cp.Id, 'contracted_Products__r');
		System.assertNotEquals(result, null);
		Test.stopTest();
	}

	@isTest
	public static void testChildrenTypesFound_3() {
		Set<String> result = new Set<String>();
		Ordertype__c orderTyp = TestUtils.createOrderType();

		Product2 prod = new Product2();
		prod.Name = 'test';
		prod.Product_Line__c = 'fVodafone';
		prod.Ordertype__c = orderTyp.id;
		insert prod;

		Opportunity opp = TestUtils.createOpportunity(TestUtils.createAccount(null), Test.getStandardPricebookId());
		PricebookEntry pbEntry = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
		OpportunityLineItem oppLine = TestUtils.createOpportunityLineItem(opp, pbEntry);

		Test.startTest();
		result = OrderValidation.childrenTypesFound(opp.Id, 'opportunityLineItems');
		System.assertNotEquals(result, null);
		Test.stopTest();
	}

	@isTest
	public static void testChildrenTypesFound_4() {
		Set<String> result = new Set<String>();
		Ordertype__c orderTyp = TestUtils.createOrderType();

		VF_Product__c vfProd = new VF_Product__c();
		vfProd.Marketing_Description__c = '(Mobius)';
		vfProd.Product_Line__c = 'fVodafone';
		vfProd.Ordertype__c = orderTyp.Id;
		insert vfProd;

		Product2 prod = new Product2();
		prod.Name = 'test';
		prod.Product_Line__c = 'fVodafone';
		prod.Ordertype__c = orderTyp.id;
		prod.VF_Product__c = vfProd.Id;
		insert prod;

		Opportunity opp = TestUtils.createOpportunity(TestUtils.createAccount(null), Test.getStandardPricebookId());
		PricebookEntry pbEntry = TestUtils.createPricebookEntry(Test.getStandardPricebookId(), prod);
		OpportunityLineItem oppLine = TestUtils.createOpportunityLineItem(opp, pbEntry);
		oppLine.family_condition__c = 'test';
		oppLine.proposition__c = 'test';
		oppLine.deal_type__c = 'test';
		update oppLine;

		Test.startTest();
		result = OrderValidation.childrenTypesFound(opp.Id, 'family_condition__c');
		OrderValidation.childrenTypesFound(opp.Id, 'proposition__c');
		OrderValidation.childrenTypesFound(opp.Id, 'clc__c');
		OrderValidation.childrenTypesFound(opp.Id, 'deal_type__c');
		OrderValidation.childrenTypesFound(opp.Id, 'Marketing_Description__c');
		System.assertNotEquals(result, null);
		Test.stopTest();
	}

	@isTest
	public static void testCheckCompleteOrderFlag() {
		Account acc = TestUtils.createAccount(null);
		Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

		VF_Contract__c testVFContract = new VF_Contract__c();
		testVFContract.Opportunity__c = opp.Id;
		testVFContract.Account__c = acc.Id;
		insert testVFContract;

		OrderType__c ot = TestUtils.createOrderType();

		Order__c ord = new Order__c();
		ord.VF_Contract__c = testVFContract.Id;
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.id;
		ord.Account__c = acc.Id;
		insert ord;

		Test.startTest();
		Boolean result = OrderValidation.checkCompleteOrderFlag(ord);
		System.assertNotEquals(result, null);
		Test.stopTest();
	}

	@isTest
	public static void testCheckCompleteValidationBeforeBOP() {
		Account acc = new Account();
		acc.Name = 'Test Account1';
		acc.OwnerId = GeneralUtils.currentUser.Id;
		acc.Unify_Account_Type__c = 'C';
		acc.Unify_Account_SubType__c = 'CRP';
		acc.Phone = '020-45678 96';
		acc.Frame_Work_Agreement__c = 'VF-0001';
		acc.claimed__c = true;
		acc.Visiting_Country__c = 'NL';
		insert acc;

		Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

		if (TestUtils.theContact == null) {
			Contact newContact = TestUtils.createContact(acc);
		}
		if (TestUtils.theBan == null) {
			Ban__c newBan = TestUtils.createBan(acc);
		}
		if (TestUtils.theFA == null) {
			Financial_Account__c fa = TestUtils.createFinancialAccount(TestUtils.theBan, TestUtils.theContact.Id);
		}
		if (TestUtils.theBAR == null) {
			Billing_Arrangement__c bar = TestUtils.createBillingArrangement(TestUtils.theFA);
		}

		VF_Contract__c testVFContract = new VF_Contract__c();
		testVFContract.Account__c = acc.Id;
		testVFContract.Opportunity__c = opp.Id;
		insert testVFContract;

		OrderType__c ot = TestUtils.createOrderType();

		Order__c ord = new Order__c();
		ord.VF_Contract__c = testVFContract.Id;
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.id;
		ord.Account__c = acc.Id;
		upsert ord;

		Test.startTest();
		String result = OrderValidation.checkCompleteValidationBeforeBOP(ord);
		OrderValidation.checkCompleteBillingFlag(TestUtils.theBan, TestUtils.theFA, TestUtils.theBAR, ord);
		System.assertNotEquals(result, null);
		Test.stopTest();
	}

	@isTest
	public static void testCheckCompleteDeliveryWrapper() {
		Account acc = new Account();
		acc.Name = 'Test Account1';
		acc.OwnerId = GeneralUtils.currentUser.Id;
		acc.Unify_Account_Type__c = 'C';
		acc.Unify_Account_SubType__c = 'CRP';
		acc.Phone = '020-45678 96';
		acc.Frame_Work_Agreement__c = 'VF-0001';
		acc.claimed__c = true;
		acc.Visiting_Country__c = 'NL';
		insert acc;

		Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());

		VF_Contract__c testVFContract = new VF_Contract__c();
		testVFContract.Account__c = acc.Id;
		testVFContract.Opportunity__c = opp.Id;
		insert testVFContract;

		OrderType__c ot = TestUtils.createOrderType();

		Order__c ord = new Order__c();
		ord.VF_Contract__c = testVFContract.Id;
		ord.Propositions__c = 'Legacy';
		ord.OrderType__c = ot.id;
		ord.Account__c = acc.Id;
		upsert ord;

		Test.startTest();
		//checkCompleteBillingFlag(Ban__c ban, Financial_Account__c fa, Billing_Arrangement__c bar, Order__c currentOrder)
		//Boolean result = OrderValidation.checkCompleteDeliveryWrapper(DeliveryWrapper dw, Order__c currentOrder);
		DeliveryWrapper dw = new DeliveryWrapper();
		OrderValidation.checkCompleteDeliveryWrapper(dw, ord);
		Test.stopTest();
	}
}
