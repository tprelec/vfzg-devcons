/**
 * @description: This class is responsible for updating Delivery Rows to ECS.
 * @author: Jurgen van Westreenen
 */
public class ECSDeliveryService extends ECSSOAPBaseWebService implements IWebService<List<Request>, List<Response>> {
	private Request[] requests;
	private Response[] responses;
	/**
	 * @description      This is the request which will be sent to ECS update a delivery
	 */
	public class Request {
		public String referenceId;
		public String deliveryNumber;
		public ECSSOAPDelivery.additionalRefType additionalReference;
		public List<ECSSOAPDelivery.deliveryRowType> deliveryRows;
	}

	/**
	 * @description      This is the response that will be returned after making a request to ECS.
	 */
	public class Response {
		public String referenceId;
		public String deliveryNumber;
		public List<ECSSOAPDelivery.deliveryRowResponseType> deliveryRows;
		public String errorCode;
		public String errorMessage;
	}

	/**
	 * @description      The constructor is responsible for setting the default web service configuration.
	 */
	public ECSDeliveryService() {
		if (!Test.isRunningTest()) {
			if (FeatureFlagging.isActive('BOP_Delivery')) {
				setWebServiceConfig(WebServiceConfigLocator.getConfigNamedCredential('ECSSOAPDelivery'));
			} else {
				setWebServiceConfig(WebServiceConfigLocator.getConfig('ECSSOAPDelivery'));
			}
		} else {
			setWebServiceConfig(WebServiceConfigLocator.createConfig());
		}
	}

	/**
	 * @description      This sets and makes the request sent to ECS.
	 */
	public void setRequest(Request[] request) {
		this.requests = request;
	}

	public void makeRequest(String requestType) {
		if (requestType == 'update') {
			makeUpdateRequest();
		}
	}

	/**
	 * @description      This will make the request to ECS to update deliveries with the request.
	 */
	public void makeUpdateRequest() {
		checkExceptions();
		ECSSOAPDelivery.DeliverySOAP service = setServiceSOAP();

		List<ECSSOAPDelivery.deliveryType> updateRequest = buildDeliveryUpdateUsingRequest();
		System.debug(LoggingLevel.INFO, 'updateRequest: ' + JSON.serialize(updateRequest));

		// create queryResult
		List<ECSSOAPDelivery.updateDeliveryResponseType> updateResults;

		try {
			System.debug(LoggingLevel.INFO, updateRequest);
			updateResults = service.updateDeliveries(updateRequest);
			System.debug(LoggingLevel.INFO, updateResults);
		} catch (Exception ex) {
			throw new ExWebServiceCalloutException('Error received from BOP when attempting to update the delivery(s): ' + ex.getMessage(), ex);
		}

		System.debug(LoggingLevel.INFO, '$$$$$$$$$$ ' + updateResults);
		parseResponse(updateResults);
	}

	/**
	 * @description      This will build the create request object which will be sent to ECS.
	 */
	private List<ECSSOAPDelivery.deliveryType> buildDeliveryUpdateUsingRequest() {
		List<ECSSOAPDelivery.deliveryType> requestList = new List<ECSSOAPDelivery.deliveryType>();

		for (Request request : this.requests) {
			ECSSOAPDelivery.deliveryType thisDelivery = new ECSSOAPDelivery.deliveryType();
			thisDelivery.referenceId = request.referenceId;
			thisDelivery.deliveryNumber = request.deliveryNumber;
			thisDelivery.additionalReference = request.additionalReference;
			thisDelivery.deliveryRows = new ECSSOAPDelivery.deliveryRowListType();
			thisDelivery.deliveryRows.deliveryRow = request.deliveryRows;
			requestList.add(thisDelivery);
		}
		return requestList;
	}

	/**
	 * @description      This will parse the result returned by ECS.
	 * @param  updateResult  The result object returned by ECS which contains messages and the delivery number.
	 */
	private void parseResponse(List<ECSSOAPDelivery.updateDeliveryResponseType> theResults) {
		responses = new List<Response>{};
		if (theResults == null) {
			return;
		}

		System.debug(LoggingLevel.INFO, '##### RAW RESPONSE: ' + theResults);

		for (ECSSOAPDelivery.updateDeliveryResponseType result : theResults) {
			if (result != null) {
				Response response = new Response();
				response.referenceId = result.referenceId;
				response.deliveryNumber = result.deliveryNumber;
				response.deliveryRows = result.deliveryRows.deliveryRow;
				if (result.error != null) {
					response.errorCode = result.error.code;
					response.errorMessage = result.error.message;
				}

				System.debug(LoggingLevel.INFO, '##### Messages: ' + response.errorMessage);

				responses.add(response);
			}
		}
	}

	/**
	 * @description      This will return the response returned by ECS. It will contain the delivery
	 *            number and optionally a list of messages that may need to be displayed to the user.
	 */
	public Response[] getResponse() {
		return responses;
	}

	private ECSSOAPDelivery.DeliverySOAP setServiceSOAP() {
		// WebService callout logic
		ECSSOAPDelivery.DeliverySOAP service = new ECSSOAPDelivery.DeliverySOAP();
		service.endpoint_x = webserviceConfig.getEndpoint();
		service.timeout_x = MAX_TIMEOUT;

		// create header element
		ECSSOAPDelivery.authenticationHeader_element header = new ECSSOAPDelivery.authenticationHeader_element();
		header.applicationId = 'sfdc';
		header.username = webServiceConfig.getUsername();
		header.password = webServiceConfig.getPassword();
		service.authenticationHeader = header;
		System.debug(LoggingLevel.INFO, 'updateRequest Endpoint: ' + JSON.serialize(service.endpoint_x));
		System.debug(LoggingLevel.INFO, 'updateRequest header: ' + JSON.serialize(header));
		return service;
	}

	@testVisible
	private void checkExceptions() {
		if (webserviceConfig == null) {
			throw new ExWebServiceCalloutException('Missing configuration');
		}
		if (requests == null) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}
		if (requests.isEmpty()) {
			throw new ExWebServiceCalloutException('A request has not been set');
		}
	}
}
