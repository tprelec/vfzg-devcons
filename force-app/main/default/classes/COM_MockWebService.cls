@IsTest
public class COM_MockWebService implements HttpCalloutMock {
	Map<String, HttpResponse> responseByEndpointOrContent;
	HttpResponse res;
	HttpRequest request;

	public COM_MockWebService(HttpResponse r, Map<String, HttpResponse> responseByEndpointOrContent) {
		this.res = r;
		this.responseByEndpointOrContent = responseByEndpointOrContent;
	}

	public COM_MockWebService(HttpRequest request, HttpResponse r) {
		this.res = r;
		this.request = request;
	}

	public COM_MockWebService(HttpResponse response) {
		this.res = response;
	}

	public HttpResponse respond(HttpRequest req, String body) {
		this.request = req;
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody(body);
		res.setStatusCode(200);
		return res;
	}

	public HttpResponse respond(HttpRequest req) {
		if (responseByEndpointOrContent == null) {
			return res;
		} else {
			if (responseByEndpointOrContent.containsKey(req.getEndpoint())) {
				return responseByEndpointOrContent.get(req.getEndpoint());
			}

			for (String bodyPart : responseByEndpointOrContent.keySet()) {
				if (req.getBody() != null && req.getBody().containsIgnoreCase(bodyPart)) {
					return responseByEndpointOrContent.get(bodyPart);
				}
			}
		}
		return res;
	}

	public static void setTestMockResponse(Integer statusCode, String status, String body) {
		MockResponse mockResponseData = new MockResponse();
		mockResponseData.statusCode = statusCode;
		mockResponseData.status = status;
		mockResponseData.body = body;
		mockResponseData.responseByEndpointOrContent = null;
		setTestMockResponse(mockResponseData);
	}

	public static void setTestMockResponse(MockResponse mockResponseData) {
		HttpResponse mockResp = new HttpResponse();

		if (mockResponseData.statusCode != null) {
			mockResp.setStatusCode(mockResponseData.statusCode);
		}

		if (mockResponseData.status != null) {
			mockResp.setStatus(mockResponseData.status);
		}

		if (mockResponseData.body != null) {
			mockResp.setBody(mockResponseData.body);
		}

		System.debug(LoggingLevel.DEBUG, '****Mock response ');
		System.debug(LoggingLevel.DEBUG, mockResp);

		test.setMock(HttpCalloutMock.class, new COM_MockWebService(mockResp, mockResponseData.responseByEndpointOrContent));
	}

	private HTTPResponse cdoCreateTenantSyncSuccess() {
		String mockSyncSuccess = '{ "id":"b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "href":"https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId":"SF", "priority":"1", "description":"Tenant Service Order", "category":"Tenant Service", "state":"acknowledged", "orderDate":"2022-02-09T10:37:15.97221Z", "@type":"standard", "orderItem":[ { "id":"1", "action":"add", "state":"ana2", "@type":"standard", "service":{ "id":"4ed3dece-d5bd-442f-a1f9-687c0a1c52221", "serviceCharacteristic":[ { "name":"sourcesystem", "valueType":"string", "value":{ "@type":"string", "sourcesystem":"SF" } } ], "serviceSpecification":{ "id":"bdab731e-d5de-4135-8849-5c724ea01041", "invariantID":"930d7161-0eb8-444f-8d11-5971812246d5", "version":"1.0.0", "name":"Tenant Service", "@type":"ServiceSpecification" } } } ] }';
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody(mockSyncSuccess);
		res.setStatusCode(200);
		return res;
	}

	private HTTPResponse cdoCreateTenantSyncFailure() {
		String mockSyncFailure = '{ "code": 40020, "reason": "SERVICE_ORDER_CHARACTERISTICS_VALIDATION_ERROR", "message": "Request validation has failed due the following issue(s) - [ Input parameter tenantContactEmail violates constraint, Invalid value ]" }';
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody(mockSyncFailure);
		res.setStatusCode(400);
		return res;
	}

	private HTTPResponse cdoCreateTenantAsyncSuccess() {
		String mockAsyncSuccess = '{ "eventId": "deff54ee-ba37-4dba-b1c9-1fe1e554a4e1", "eventTime": "2022-02-08T11:30:15.97221Z", "eventType": "ServiceOrderStateChangeNotification", "event": { "serviceOrder": { "id": "b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId": "COM", "priority": "1", "description": "Tenant Service Order", "category": "Tenant Service", "state": "completed", "orderDate": "2022-02-08T10:37:15.97221Z", "startDate": "2022-02-08T10:37:15.97321Z", "type": "standard", "orderItem": [ { "id": "1", "action": "add", "state": "completed", "@type": "standard", "service": { "id": "4ed3dece-d5bd-442f-a1f9-687c0a1c5422", "serviceState": "active", "serviceCharacteristic": [ { "name": "sourcesystem", "valueType": "string", "value": { "@type": "string", "sourcesystem": "SF" } }, { "name": "tenantId", "valueType": "string", "value": { "@type": "string", "tenantId": "Some Tenant Id" } }, { "name": "tenantContactName", "valueType": "string", "value": { "@type": "string", "tenantContactName": "Some Contact Name" } }, { "name": "tenantContactPhone", "valueType": "string", "value": { "@type": "string", "tenantContactPhonee": "+311122334455" } }, { "name": "tenantContactEmail", "valueType": "string", "value": { "@type": "string", "tenantContactEmail": "somecontact@sometenant.com" } } ], "serviceSpecification": { "id": "bdab731e-d5de-4135-8849-5c724ea01041", "invariantID": "930d7161-0eb8-444f-8d11-5971812246d5", "version": "1.0.0", "name": "Tenant Service", "@type": "ServiceSpecification" } } } ] } } }';
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody(mockAsyncSuccess);
		res.setStatusCode(200);
		return res;
	}

	private HTTPResponse cdoCreateTenantAsyncFailure() {
		String mockAsyncFailure = '{ "eventId": "9577add4-5200-47cb-b787-f6e617b396d9", "eventTime": "2022-02-08T10:45:15.97221Z", "eventType": "ServiceOrderStateChangeNotification", "event": { "serviceOrder": { "id": "b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "href": "https://ncso-tmf641-nbi/serviceOrder/b0d13c8a-5d77-4e19-96f1-2405ee2fe291", "externalId": "SF", "priority": "1", "description": "Tenant Service Order", "category": "Tenant Service", "state": "held", "orderDate": "2022-02-08T10:37:15.97221Z", "startDate": "2022-02-08T10:37:15.97321Z", "@type": "standard", "orderItem": [ { "id": "1", "action": "add", "state": "held", "type": "standard", "service": { "id": "b537bfff-bb10-4662-8831-30ff4e7a150f", "serviceState": "inactive", "serviceCharacteristic": [ { "name": "sourcesystem", "valueType": "string", "value": { "@type": "string", "sourcesystem": "SF" } }, { "name": "tenantContactName", "valueType": "string", "value": { "@type": "string", "tenantContactName": "Some Contact Name" } }, { "name": "tenantContactPhone", "valueType": "string", "value": { "@type": "string", "tenantContactPhone": "+311122334455" } }, { "name": "tenantContactEmail", "valueType": "string", "value": { "@type": "string", "tenantContactEmail": "somecontact@sometenant.com" } } ], "serviceSpecification": { "id": "bdab731e-d5de-4135-8849-5c724ea01041", "invariantID": "930d7161-0eb8-444f-8d11-5971812246d5", "version": "1.0.0", "name": "Tenant Service", "@type": "ServiceSpecification" } } } ] }, "errorEvents": [ { "code": 510105000, "reason": "EXTERNAL_SYSTEM_ERROR", "message": "A fulfillment activity failed with error-code \'5000\', error-message \'some error message\'." } ] } }';
		HttpResponse res = new HttpResponse();
		res.setHeader('Content-Type', 'application/json');
		res.setBody(mockAsyncFailure);
		res.setStatusCode(400);
		return res;
	}

	public class MockResponse {
		public Integer statusCode;
		public String status;
		public String body;
		public Map<String, HttpResponse> responseByEndpointOrContent;
	}
}
