/**
 * This is a dummy test class to obtain 100% coverage for the generated WSDL2Apex code, it is not a funcitonal test class
 **/ 
@isTest  
public class TestMetadataService
{    
    /**
     * Dummy Metadata API web service mock class
     **/
	public class WebServiceMockImpl implements WebServiceMock 
	{
		public void doInvoke(
			Object stub, Object request, Map<String, Object> response,
			String endpoint, String soapAction, String requestName,
			String responseNS, String responseName, String responseType) 
		{
			if(request instanceof MetadataService.retrieve_element)
				response.put('response_x', new MetadataService.retrieveResponse_element());
			else if(request instanceof MetadataService.listMetadata_element){
				//MetadataService.FileProperties[]
				MetadataService.listMetadataResponse_element mockRes = new MetadataService.listMetadataResponse_element();
				mockRes.result = new List<MetadataService.FileProperties>();
				MetadataService.FileProperties fp1 = new MetadataService.FileProperties();
				fp1.fullName = 'Contact';
				mockRes.result.add(fp1);
				MetadataService.FileProperties fp2 = fp1.clone();
				fp2.fullName = 'Account';
				mockRes.result.add(fp2);
				response.put('response_x', mockRes);
			}
			else if(request instanceof MetadataService.checkRetrieveStatus_element)
				response.put('response_x', new MetadataService.checkRetrieveStatusResponse_element());
			else if(request instanceof MetadataService.describeMetadata_element)
				response.put('response_x', new MetadataService.describeMetadataResponse_element());
            else if(request instanceof MetadataService.updateMetadata_element){
            	MetadataService.updateMetadataResponse_element mockRes = new MetadataService.updateMetadataResponse_element();
            	mockRes.result = new List<MetadataService.SaveResult>();
            	
            	MetadataService.SaveResult result = new MetadataService.SaveResult();
            	// simulate an update error
            	MetadataService.Error theError = new MetadataService.Error();
            	theError.message = 'error1';
            	theError.statuscode = '99';
            	result.errors = new List<MetadataService.Error>();
            	result.errors.add(theError);
            	result.fullName = 'Account.field1name__c';
            	result.success = false;
            	mockRes.result.add(result);
                response.put('response_x', mockRes);
            }
            else if(request instanceof  MetadataService.deleteMetadata_element)
                response.put('response_x', new MetadataService.deleteMetadataResponse_element());
            else if(request instanceof  MetadataService.upsertMetadata_element)
                response.put('response_x', new MetadataService.upsertMetadataResponse_element());
            else if(request instanceof  MetadataService.createMetadata_element)
                response.put('response_x', new MetadataService.createMetadataResponse_element());
            else if(request instanceof  MetadataService.readMetadata_element){
			    MetadataService.readMetadata_element requestReadMetadata_element = (MetadataService.readMetadata_element) request;
			    // This allows you to generalize the mock response by type of metadata read
			    if (requestReadMetadata_element.type_x == 'CustomObject') { 
			        MetadataService.readCustomObjectResponse_element mockRes = new MetadataService.readCustomObjectResponse_element();
			        mockRes.result = new MetaDataService.ReadCustomObjectResult();
			        mockRes.result.records = createCustomObjects();
			        response.put('response_x', mockRes);
			    } else if (requestReadMetadata_element.type_x == 'CustomField') { 
			        MetadataService.readCustomFieldResponse_element mockRes = new MetadataService.readCustomFieldResponse_element();
			        mockRes.result = new MetaDataService.ReadCustomFieldResult();
			        mockRes.result.records = createCustomFields();
			        response.put('response_x', mockRes);
			    } else {
			    	response.put('response_x', null);
			    }
			}                      
			return;
		}
	}   

	@IsTest
	private static MetadataService.CustomObject[] createCustomObjects(){
	    MetadataService.CustomObject[] objects = new MetadataService.CustomObject[]{};

	    MetadataService.CustomObject obj = new MetadataService.CustomObject();
	    obj.fullName = 'Contact';

	    MetadataService.CustomField field = new MetadataService.CustomField();
	    field.fullName = 'field1name__c';
	    obj.fields = new List<MetaDataService.CustomField>();
	    obj.fields.add(field);
	    //obj.listViews = createListViews();
	    objects.add(obj);

	    MetadataService.CustomObject obj2 = new MetadataService.CustomObject();
	    obj2.fullName = 'Account';
	    MetadataService.CustomField field2 = new MetadataService.CustomField();
	    field2.fullName = 'field1name__c';
	    obj2.fields = new List<MetaDataService.CustomField>();
	    obj2.fields.add(field2);	    
	    //obj2.listViews = createListViews();
	    objects.add(obj2);
	    return objects;
	}	 
		
	@IsTest
	private static MetadataService.CustomField[] createCustomFields(){
	    MetadataService.CustomField[] fields = new MetadataService.CustomField[]{};

	    MetadataService.CustomField field = new MetadataService.CustomField();
	    field.fullName = 'Account.field1name__c';
	    field.label = 'thelabel';
	    field.Description = 'theDescription';
	    field.InlineHelpText = 'theInlineHelpText';
	    fields.add(field);

	    //MetadataService.CustomField field2 = field.clone();
	    //field2.fullName = 'Contact.field1name__c';
	    //fields.add(field2);
	    return fields;
	}	 		

	@IsTest
	private static void coverGeneratedCodeCRUDOperations()
	{	
    	// Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations         
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
	}
	
	@IsTest
    private static void coverGeneratedCodeFileBasedOperations1()
    {    	
    	// Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations         
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
        metaDataPort.retrieve(null);
        metaDataPort.listMetadata(null, null);
        metaDataPort.describeMetadata(null);
        metaDataPort.updateMetadata(null);
    }

    @IsTest
    private static void coverGeneratedCodeFileBasedOperations2()
    {       
        // Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        // Only required to workaround a current code coverage bug in the platform
        MetadataService metaDataService = new MetadataService();
        // Invoke operations         
        MetadataService.MetadataPort metaDataPort = new MetadataService.MetadataPort();
        metaDataPort.deleteMetadata(null, null);
        metaDataPort.upsertMetadata(null);
        metaDataPort.createMetadata(null);
        metaDataPort.readMetadata('CustomObject',new List<String>{'Account','Contact'});
        metaDataPort.checkRetrieveStatus(null, null);
    }
        
	@IsTest
    private static void coverGeneratedCodeTypes()
    {    	       
        // Reference types
        new MetadataService();
        new MetadataService.listMetadataResponse_element();
        new MetadataService.DescribeMetadataResult();
        new MetadataService.RecordType();
        new MetadataService.FilterItem();
        new MetadataService.LogInfo();
        new MetadataService.WebLink();
        new MetadataService.describeMetadataResponse_element();
        new MetadataService.RecordTypePicklistValue();
        new MetadataService.describeMetadata_element();
        new MetadataService.ProfileObjectPermissions();
        new MetadataService.RetrieveResult();
        new MetadataService.retrieve_element();
        new MetadataService.DescribeMetadataObject();
        new MetadataService.ListViewFilter();
        new MetadataService.CustomField();
        new MetadataService.FileProperties();
        new MetadataService.ListView();
        new MetadataService.checkRetrieveStatus_element();
        new MetadataService.RetrieveRequest();
        new MetadataService.ListMetadataQuery();
        new MetadataService.FieldSet();
        new MetadataService.DebuggingHeader_element();
        new MetadataService.SharingRules();
        new MetadataService.SharedTo();
        new MetadataService.Picklist();
        new MetadataService.listMetadata_element();
        new MetadataService.ValidationRule();
        new MetadataService.Metadata();
        new MetadataService.RetrieveMessage();
        new MetadataService.SessionHeader_element();
        new MetadataService.ActionOverride();
        new MetadataService.CustomObject();
        new MetadataService.PackageTypeMembers();
        new MetadataService.PicklistValue();
        new MetadataService.retrieveResponse_element();
        new MetadataService.ArticleTypeTemplate();
        new MetadataService.SharingRecalculation();
        new MetadataService.checkRetrieveStatusResponse_element();
        new MetadataService.SharingReason();
        new MetadataService.SearchLayouts();
        new MetadataService.deleteMetadataResponse_element();
        new MetadataService.Error();
        new MetadataService.SaveResult();
        new MetadataService.readMetadataResponse_element();
        new MetadataService.deleteMetadata_element();
        new MetadataService.createMetadataResponse_element();
        new MetadataService.updateMetadata_element();
        new MetadataService.LookupFilter();
        new MetadataService.updateMetadataResponse_element();
        new MetadataService.createMetadata_element();
        new MetadataService.readMetadata_element();
        new MetadataService.ReadSharingRulesResult();
        new MetadataService.readSharingRulesResponse_element();
        new MetadataService.ReadCustomObjectResult();
        new MetadataService.readCustomObjectResponse_element();
        new MetadataService.ReadCustomFieldResult();
        new MetadataService.readCustomFieldResponse_element();
        new MetadataService.ReadResult();
        new MetadataService.DeleteResult();
        new MetadataService.upsertMetadata_element();
        new MetadataService.upsertMetadataResponse_element();
        new MetadataService.UpsertResult();
        new MetaDataService.SharingBaseRule();
        new MetaDataService.AccountSharingRuleSettings();
        new MetaDataService.ReadSharingOwnerRuleResult();
        new MetaDataService.readSharingOwnerRuleResponse_element();
        new MetaDataService.SharingOwnerRule();
        new MetadataService.PicklistValue();
        new MetaDataService.SessionSettings();
        new MetaDataService.SharingCriteriaRule();
        new MetaDataService.DebuggingInfo_element();
        new MetaDataService.CallOptions_element();
        new MetaDataService.ArticleTypeChannelDisplay();
        new MetaDataService.BusinessProcess();
        new MetaDataService.CompactLayout();
        new MetaDataService.FieldSetItem();
        new MetaDataService.HistoryRetentionPolicy();
        new MetaDataService.AllOrNoneHeader_element();
        new MetaDataService.ExtendedErrorDetails();
        new MetaDataService.Package_x();
        new MetaDataService.AsyncResult();

    }

    
}