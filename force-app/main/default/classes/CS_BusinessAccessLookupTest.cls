@isTest
public class CS_BusinessAccessLookupTest {
	@TestSetup
	static void makeData() {
		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c accessDefinition = CS_DataTest.createProductDefinition('Access PD');
		accessDefinition.Product_Type__c = 'Mobile';
		accessDefinition.RecordTypeId = productDefinitionRecordType;
		insert accessDefinition;

		OrderType__c orderType = CS_DataTest.createOrderType();
		insert orderType;

		Product2 productTest = CS_DataTest.createProduct('BIMO product', orderType);
		insert productTest;

		Category__c categoryTest = CS_DataTest.createCategory('Business Access');
		insert categoryTest;

		Vendor__c vendor = CS_DataTest.createVendor('ZIGGO');
		insert vendor;

		cspmb__Price_Item__c priceItemMaster = new cspmb__Price_Item__c(Name = 'Master Price Item', cspmb__Is_Active__c = true);
		priceItemMaster.cspmb__Type__c = 'Commercial Product';
		priceItemMaster.cspmb__Role__c = 'Master';
		priceItemMaster.cspmb__Price_Item_Code__c = 'masterCode';
		insert priceItemMaster;

		cspmb__Price_Item__c priceItem1 = CS_DataTest.createPriceItem(productTest, categoryTest, 100, 30, vendor, '', '');
		priceItem1.Name = 'CP1 30/100 Mbps';
		priceItem1.cspmb__Master_Price_item__c = priceItemMaster.Id;
		priceItem1.Access_Type_Multi__c = 'Coax';
		priceItem1.cspmb__Contract_Term__c = '12 Months';
		priceItem1.cspmb__Effective_Start_Date__c = Date.today().addDays(-300);
		priceItem1.cspmb__Effective_End_Date__c = Date.today().addDays(300);
		priceItem1.cspmb__Type__c = 'Commercial Product';
		priceItem1.cspmb__Role__c = 'Variant';
		priceItem1.cspmb__Price_Item_Code__c = 'priceItem1Code';

		cspmb__Price_Item__c priceItem2 = CS_DataTest.createPriceItem(productTest, categoryTest, 30, 100, vendor, '', '');
		priceItem2.Name = 'CP2 100/30';
		priceItem2.cspmb__Master_Price_item__c = priceItemMaster.Id;
		priceItem2.Access_Type_Multi__c = 'Coax';
		priceItem2.cspmb__Contract_Term__c = '24 Months';
		priceItem2.cspmb__Effective_Start_Date__c = Date.today().addDays(-300);
		priceItem2.cspmb__Effective_End_Date__c = Date.today().addDays(300);
		priceItem2.cspmb__Type__c = 'Commercial Product';
		priceItem2.cspmb__Role__c = 'Variant';
		priceItem2.cspmb__Price_Item_Code__c = 'priceItem2Code';

		cspmb__Price_Item__c priceItem3 = CS_DataTest.createPriceItem(productTest, categoryTest, 50, 1000, vendor, '', '');
		priceItem3.Name = 'CP3 1000/50';
		priceItem3.cspmb__Master_Price_item__c = priceItemMaster.Id;
		priceItem3.Access_Type_Multi__c = 'EthernetOverFiber';
		priceItem3.cspmb__Contract_Term__c = '24 Months';
		priceItem3.cspmb__Effective_Start_Date__c = Date.today().addDays(-300);
		priceItem3.cspmb__Effective_End_Date__c = Date.today().addDays(300);
		priceItem3.cspmb__Type__c = 'Commercial Product';
		priceItem3.cspmb__Role__c = 'Variant';
		priceItem3.cspmb__Price_Item_Code__c = 'priceItem3Code';

		List<cspmb__Price_Item__c> priceItems = new List<cspmb__Price_Item__c>{ priceItem1, priceItem2, priceItem3 };
		insert priceItems;
	}

	@isTest
	private static void testRequiredAttributes() {
		CS_BusinessAccessLookup caLookup = new CS_BusinessAccessLookup();
		String result = caLookup.getRequiredAttributes();
		System.assertNotEquals(null, result, 'Return string must not be null');
	}

	@isTest
	public static void lookupTest() {
		cscfga__Product_Definition__c accessDefinition = [SELECT Id FROM cscfga__Product_Definition__c WHERE Name = 'Access PD' LIMIT 1];

		CS_BusinessAccessLookup bimoAccessLookup = new CS_BusinessAccessLookup();

		Map<String, String> searchFields1 = new Map<String, String>();
		searchFields1.put('contractDuration', '24');
		searchFields1.put('Technology', 'Coax');
		searchFields1.put('siteVendor', 'ZIGGO');
		searchFields1.put('BandwidthSellableDown', '10000');
		searchFields1.put('Today', string.valueOf(Date.today() - 30));
		List<Object> result = bimoAccessLookup.doLookupSearch(searchFields1, String.valueOf(accessDefinition.Id), null, 0, 0);
		System.assert(result.size() > 0, 'Items should exist.');

		Map<String, String> searchFields2 = new Map<String, String>();
		searchFields2.put('contractDuration', '24');
		searchFields2.put('Technology', 'Coax');
		searchFields2.put('siteVendor', 'Something');
		searchFields2.put('BandwidthSellableDown', '10000');
		searchFields2.put('Today', string.valueOf(Date.today()));
		result = bimoAccessLookup.doLookupSearch(searchFields2, String.valueOf(accessDefinition.Id), null, 0, 0);
		System.assertEquals(0, result.size(), 'Items should not exist.');
	}
}
