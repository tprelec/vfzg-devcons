@isTest
public with sharing class TestCOM_Helper {
	@TestSetup
	static void testSetup() {
		Account testAccount = CS_DataTest.createAccount('Test Account_setup');
		insert testAccount;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasketOnly('Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinition('TestDef');
		prodDef.RecordTypeId = productDefinitionRecordType;
		prodDef.Product_Type__c = 'Fixed';
		insert prodDef;

		cscfga__Product_Configuration__c prodConfig = CS_DataTest.createProductConfiguration(prodDef.Id, 'TestConfig', basket.Id);
		insert prodConfig;

		csord__Order__c order = new csord__Order__c(
			Name = 'testOrder_helpertest',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'testOrder_helpertest'
		);
		insert order;

		COM_Delivery_Order__c delOrd = CS_DataTest.createDeliveryOrder('TestDelOrd_helpertest', order.Id, true);

		COM_Delivery_Order__c delOrd2 = CS_DataTest.createDeliveryOrder('TestDelOrd_helpertest_asset', order.Id, true);

		Asset asset = new Asset();
		asset.Name = 'Test_Asset';
		asset.accountId = testAccount.Id;
		asset.COM_Delivery_Order__c = delOrd2.Id;
		insert asset;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(prodConfig.Id);
		subscription.csord__Identification__c = 'testSubscription_helpertest';
		insert subscription;

		csord__Service__c service = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service.csord__Identification__c = 'testService_helpertest';
		service.csord__Status__c = 'Test';
		service.CDO_Umbrella_Service_Id__c = '123';
		service.csord__Order__c = order.Id;
		service.COM_Delivery_Order__c = delOrd.Id;
		insert service;
	}

	@isTest
	private static void testGetServiceList() {
		csord__Service__c service = [SELECT Id FROM csord__Service__c WHERE csord__Identification__c = 'testService_helpertest'];
		csord__Subscription__c subscription = [SELECT Id FROM csord__Subscription__c WHERE csord__Identification__c = 'testSubscription_helpertest'];

		csord__Order__c order = [SELECT Id FROM csord__Order__c WHERE csord__Identification__c = 'testOrder_helpertest'];

		List<Id> serviceIdList = new List<Id>{ service.Id };

		Test.startTest();
		List<csord__Service__c> serviceList = COM_Helper.getServiceList(serviceIdList);
		Test.stopTest();

		System.assertEquals(1, serviceList.size(), 'Size is not the same.');
		System.assertEquals(service.Id, serviceList[0].Id, 'ID is not the same.');
		System.assertEquals(subscription.Id, serviceList[0].csord__Subscription__c, 'ID is not the same.');
		System.assertEquals(order.Id, serviceList[0].csord__Order__c, 'ID is not the same.');
	}

	@isTest
	private static void testGetServiceList_CustomQuery() {
		List<csord__Service__c> serviceList = [
			SELECT Id, CDO_Umbrella_Service_Id__c
			FROM csord__Service__c
			WHERE csord__Identification__c = 'testService_helpertest'
		];

		Test.startTest();
		List<csord__Service__c> resultList = COM_Helper.getServiceList_CustomQuery(serviceList, ',CDO_Umbrella_Service_Id__c');
		Test.stopTest();

		System.assertEquals(1, resultList.size(), 'Size is not the same.');
		System.assertEquals(serviceList[0].Id, resultList[0].Id, 'ID is not the same.');
		System.assertEquals(serviceList[0].CDO_Umbrella_Service_Id__c, resultList[0].CDO_Umbrella_Service_Id__c, 'Field is not the same.');
	}

	@isTest
	private static void testGetAccountName() {
		Account testAccount1 = CS_DataTest.createAccount('Test Account1');
		insert testAccount1;

		Account testAccount2 = CS_DataTest.createAccount('Test Account2');
		insert testAccount2;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasketOnly('Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinition('TestDef');
		prodDef.RecordTypeId = productDefinitionRecordType;
		prodDef.Product_Type__c = 'Fixed';
		insert prodDef;

		cscfga__Product_Configuration__c prodConfig = CS_DataTest.createProductConfiguration(prodDef.Id, 'TestConfig', basket.Id);
		insert prodConfig;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(prodConfig.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Order__c order1 = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount1.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now()
		);
		insert order1;

		csord__Service__c service1 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service1.csord__Identification__c = 'testService1';
		service1.csord__Status__c = 'Test';
		service1.csord__Order__c = order1.Id;
		insert service1;

		csord__Order__c order2 = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount2.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now()
		);
		insert order2;

		csord__Service__c service2 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service2.csord__Identification__c = 'testService2_p';
		service2.csord__Status__c = 'Test';
		service2.csord__Order__c = order2.Id;
		insert service2;

		csord__Service__c service3 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service3.csord__Identification__c = 'testService2_p';
		service3.csord__Status__c = 'Test';
		service3.csordtelcoa__Replacement_Service__c = service2.Id;
		insert service3;

		List<Id> serviceIdList = new List<Id>{ service1.Id, service3.Id };
		List<csord__Service__c> serviceList = COM_Helper.getServiceList(serviceIdList);

		Test.startTest();
		String accName1 = COM_Helper.getAccountName(serviceList[0]);
		String accName2 = COM_Helper.getAccountName(serviceList[1]);
		Test.stopTest();

		System.assertEquals('Test Account1', accName1, 'Name is not the same.');
		System.assertEquals('Test Account2', accName2, 'Name is not the same.');
	}

	@isTest
	private static void testGetParentServiceList() {
		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasketOnly('Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinition('TestDef');
		prodDef.RecordTypeId = productDefinitionRecordType;
		prodDef.Product_Type__c = 'Fixed';
		insert prodDef;

		cscfga__Product_Configuration__c prodConfig = CS_DataTest.createProductConfiguration(prodDef.Id, 'TestConfig', basket.Id);
		insert prodConfig;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(prodConfig.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c service1 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service1.csord__Identification__c = 'testService2_p';
		service1.csord__Status__c = 'Test';
		insert service1;

		csord__Service__c service2 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service2.csord__Identification__c = 'testService2_p';
		service2.csord__Status__c = 'Test';
		service2.csord__Service__c = service1.Id;
		insert service2;

		List<csord__Service__c> servicesList = new List<csord__Service__c>{ service1, service2 };

		Test.startTest();
		List<csord__Service__c> parentServices = COM_Helper.getParentServiceList(servicesList);
		Test.stopTest();

		System.assertEquals(1, parentServices.size(), 'Size is not the same.');
		System.assertEquals(service1.Id, parentServices[0].Id, 'ID is not the same.');
	}

	@isTest
	private static void testGetSubscriptionIdListAndGetSubscriptionList() {
		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasketOnly('Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinition('TestDef');
		prodDef.RecordTypeId = productDefinitionRecordType;
		prodDef.Product_Type__c = 'Fixed';
		insert prodDef;

		cscfga__Product_Configuration__c prodConfig = CS_DataTest.createProductConfiguration(prodDef.Id, 'TestConfig', basket.Id);
		insert prodConfig;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(prodConfig.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		subscription.Name = 'TestSub';
		insert subscription;

		csord__Service__c service1 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service1.csord__Identification__c = 'testService1';
		service1.csord__Status__c = 'Test';
		insert service1;

		csord__Service__c service2 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service2.csord__Identification__c = 'testService2_p';
		service2.csord__Status__c = 'Test';
		insert service2;

		csord__Service__c service3 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service3.csord__Identification__c = 'testService2_p';
		service3.csord__Status__c = 'Test';
		service3.csordtelcoa__Replaced_Service__c = service2.Id;
		insert service3;

		List<Id> serviceIdList = new List<Id>{ service1.Id, service3.Id };
		List<csord__Service__c> serviceList = COM_Helper.getServiceList(serviceIdList);

		List<Id> subscriptionIdList = new List<Id>{ subscription.Id };

		Test.startTest();
		List<Id> subIds = COM_Helper.getSubscriptionIdList(serviceList);
		List<csord__Subscription__c> subList = COM_Helper.getSubscriptionList(subscriptionIdList);
		Test.stopTest();

		Set<Id> subIdsSet = new Set<Id>(subIds);
		System.assertEquals(true, subIdsSet.contains(subscription.Id), 'ID is not found.');
		System.assertEquals(1, subList.size(), 'Size is not the same.');
		System.assertEquals('TestSub', subList[0].Name, 'Name is not the same.');
		System.assertEquals(subscription.Id, subList[0].Id, 'ID is not the same.');
	}

	@isTest
	private static void testGetOrderIdListANDGetOrderIdList() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasketOnly('Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinition('TestDef');
		prodDef.RecordTypeId = productDefinitionRecordType;
		prodDef.Product_Type__c = 'Fixed';
		insert prodDef;

		cscfga__Product_Configuration__c prodConfig = CS_DataTest.createProductConfiguration(prodDef.Id, 'TestConfig', basket.Id);
		insert prodConfig;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(prodConfig.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Order__c order = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now()
		);
		insert order;

		csord__Service__c service1 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service1.csord__Identification__c = 'testService1';
		service1.csord__Status__c = 'Test';
		service1.csord__Order__c = order.Id;
		insert service1;

		csord__Service__c service2 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service2.csord__Identification__c = 'testService2_p';
		service2.csord__Status__c = 'Test';
		service2.csordtelcoa__Replacement_Service__c = service1.Id;
		service2.csord__Order__c = order.Id;
		insert service2;

		csord__Service__c service3 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service3.csord__Identification__c = 'testService2_p';
		service3.csord__Status__c = 'Test';
		service3.csord__Order__c = order.Id;
		insert service3;

		List<Id> serviceIdList = new List<Id>{ service1.Id, service2.Id, service3.Id };
		List<csord__Service__c> serviceList = COM_Helper.getServiceList(serviceIdList);

		Test.startTest();
		String orderId1 = COM_Helper.getOrderId(serviceList[0]);
		String orderId2 = COM_Helper.getOrderId(serviceList[1]);
		List<csord__Order__c> ordIds = COM_Helper.getOrders(serviceList);
		Test.stopTest();

		System.assertEquals(order.Id, orderId1, 'ID is not the same.');
		System.assertEquals(order.Id, orderId2, 'ID is not the same.');
		System.assertEquals(1, ordIds.size(), 'Size is not the same.');
		System.assertEquals(order.Id, ordIds[0].Id, 'ID is not the same.');
	}

	@isTest
	private static void testGetReplacedServiceIdList() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasketOnly('Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinition('TestDef');
		prodDef.RecordTypeId = productDefinitionRecordType;
		prodDef.Product_Type__c = 'Fixed';
		insert prodDef;

		cscfga__Product_Configuration__c prodConfig = CS_DataTest.createProductConfiguration(prodDef.Id, 'TestConfig', basket.Id);
		insert prodConfig;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(prodConfig.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'TestOpp', UserInfo.getUserId());
		testOpp.csordtelcoa__Change_Type__c = COM_Constants.OPPORTUNITY_CHANGE_TYPE_MOVE;
		insert testOpp;

		csord__Order__c order1 = new csord__Order__c(
			Name = 'Test Order',
			csord__Account__c = testAccount.Id,
			csord__Status2__c = 'Created',
			csord__Identification__c = 'DWHTestBatchOn_' + system.now(),
			csordtelcoa__Opportunity__c = testOpp.Id
		);
		insert order1;

		csord__Service__c service1 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service1.csord__Identification__c = 'testService1';
		service1.csord__Status__c = 'Test';
		service1.csord__Order__c = order1.Id;
		insert service1;

		csord__Service__c service2 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service2.csord__Identification__c = 'testService2';
		service2.csord__Status__c = 'Test';
		service2.csordtelcoa__Replaced_Service__c = service1.Id;
		service2.csord__Order__c = order1.Id;
		insert service2;

		List<Id> serviceIdList = new List<Id>{ service2.Id };
		List<csord__Service__c> serviceList = COM_Helper.getServiceList(serviceIdList);

		Test.startTest();
		List<Id> replServIds = COM_Helper.getReplacedServiceIdList(serviceList);
		Test.stopTest();

		Set<Id> replServIdsSet = new Set<Id>(replServIds);
		System.assertEquals(true, replServIdsSet.contains(service1.Id), 'ID is not found.');
	}

	@isTest
	private static void testGetSiteIdAndGetSiteIdSet() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		Site__c site = CS_DataTest.createSite('LEIDEN, Breestraat 112', testAccount, '1111AA', 'Breestraat', 'LEIDEN', 112);
		insert site;

		cscfga__Product_Basket__c basket = CS_DataTest.createProductBasketOnly('Test Basket');
		insert basket;

		Id productDefinitionRecordType = Schema.SObjectType.cscfga__Product_Definition__c.getRecordTypeInfosByName()
			.get('Product Definition')
			.getRecordTypeId();

		cscfga__Product_Definition__c prodDef = CS_DataTest.createProductDefinition('TestDef');
		prodDef.RecordTypeId = productDefinitionRecordType;
		prodDef.Product_Type__c = 'Fixed';
		insert prodDef;

		cscfga__Product_Configuration__c prodConfig = CS_DataTest.createProductConfiguration(prodDef.Id, 'TestConfig', basket.Id);
		insert prodConfig;

		csord__Subscription__c subscription = CS_DataTest.createSubscription(prodConfig.Id);
		subscription.csord__Identification__c = 'Subscription_a4D9E0000009BCTUA2_0';
		insert subscription;

		csord__Service__c service1 = CS_DataTest.createService(prodConfig.Id, subscription, 'Created');
		service1.csord__Identification__c = 'testService1';
		service1.csord__Status__c = 'Test';
		service1.Site__c = site.Id;
		insert service1;

		List<Id> serviceIdList = new List<Id>{ service1.Id };
		List<csord__Service__c> serviceList = COM_Helper.getServiceList(serviceIdList);

		Test.startTest();
		String siteId = COM_Helper.getSiteId(service1);
		Set<String> siteSet = COM_Helper.getSiteIdSet(serviceList);
		Test.stopTest();

		System.assertEquals(site.Id, siteId, 'ID is not the same.');
		System.assertEquals(1, siteSet.size(), 'Size is not the same.');
		System.assertEquals(true, siteSet.contains(site.Id), 'ID is not found.');
	}

	@isTest
	private static void testUpdateList() {
		Account testAccount = CS_DataTest.createAccount('Test Account');
		insert testAccount;

		testAccount.Name = 'Test Account - Changed';

		List<Account> accList = new List<Account>{ testAccount };

		Test.startTest();
		COM_Helper.updateList(accList);
		Test.stopTest();

		Account updatedAcc = [SELECT Name FROM Account WHERE Id = :testAccount.Id];
		System.assertEquals('Test Account - Changed', updatedAcc.Name, 'Name is not the same.');
	}

	@isTest
	private static void testCreateCOMAssetRecord_1Record() {
		csord__Service__c services = [SELECT Id FROM csord__Service__c WHERE csord__Identification__c = 'testService_helpertest'];
		List<Id> serviceIdList = new List<Id>{ services.Id };
		List<csord__Service__c> serviceList = COM_Helper.getServiceList(serviceIdList);

		COM_MetadataInformation.DeliveryComponent deliveryComponent = new COM_MetadataInformation.DeliveryComponent();
		deliveryComponent.Name = 'TestDelCompName';

		List<COM_Delivery_Materials__mdt> deliveryMaterials = [
			SELECT Id, Model_Name__c, Type__c, BOP_C_code__c
			FROM COM_Delivery_Materials__mdt
			LIMIT 1
		];

		Test.startTest();
		Asset comAsset = COM_Helper.createCOMAssetRecord(serviceList[0], deliveryComponent, deliveryMaterials);
		Test.stopTest();

		System.assertEquals(
			Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get(COM_Constants.COM_ASSET_RECORD_TYPE).getRecordTypeId(),
			comAsset.RecordTypeId,
			'Asset record type is incorrect.'
		);
		System.assertEquals(deliveryMaterials[0].Model_Name__c, comAsset.Name, 'Asset name is incorrect.');
		System.assertEquals(deliveryMaterials[0].Type__c, comAsset.COM_Type__c, 'Asset type is incorrect.');
		System.assertEquals(serviceList[0].COM_Delivery_Order__c, comAsset.COM_Delivery_Order__c, 'Asset Delivery Order is incorrect.');
		System.assertEquals(deliveryComponent.name, comAsset.COM_Delivery_Component__c, 'Asset Delivery Component is incorrect.');
		System.assertEquals(serviceList[0].Id, comAsset.csord__Service__c, 'Asset Service Id is incorrect.');
		System.assertEquals(deliveryMaterials[0].BOP_C_code__c, comAsset.COM_BOP_C_code__c, 'Asset BOP Code is incorrect.');
		System.assertEquals(serviceList[0].csord__Order__r.csord__Account__c, comAsset.AccountId, 'Asset Account Id is incorrect.');
	}

	@isTest
	private static void testCreateCOMAssetRecord_2Records() {
		csord__Service__c services = [SELECT Id FROM csord__Service__c WHERE csord__Identification__c = 'testService_helpertest'];
		List<Id> serviceIdList = new List<Id>{ services.Id };
		List<csord__Service__c> serviceList = COM_Helper.getServiceList(serviceIdList);

		COM_MetadataInformation.DeliveryComponent deliveryComponent = new COM_MetadataInformation.DeliveryComponent();
		deliveryComponent.Name = 'TestDelCompName';

		List<COM_Delivery_Materials__mdt> deliveryMaterials = [
			SELECT Id, Model_Name__c, Type__c, BOP_C_code__c
			FROM COM_Delivery_Materials__mdt
			LIMIT 2
		];

		Test.startTest();
		Asset comAsset = COM_Helper.createCOMAssetRecord(serviceList[0], deliveryComponent, deliveryMaterials);
		Test.stopTest();

		System.assertEquals(
			Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get(COM_Constants.COM_ASSET_RECORD_TYPE).getRecordTypeId(),
			comAsset.RecordTypeId,
			'Asset record type is incorrect.'
		);
		System.assertEquals(deliveryComponent.name + ' ' + deliveryMaterials[0].Type__c, comAsset.Name, 'Asset name is incorrect.');
		System.assertEquals(deliveryMaterials[0].Type__c, comAsset.COM_Type__c, 'Asset type is incorrect.');
		System.assertEquals(serviceList[0].COM_Delivery_Order__c, comAsset.COM_Delivery_Order__c, 'Asset Delivery Order is incorrect.');
		System.assertEquals(deliveryComponent.name, comAsset.COM_Delivery_Component__c, 'Asset Delivery Component is incorrect.');
		System.assertEquals(serviceList[0].Id, comAsset.csord__Service__c, 'Asset Service Id is incorrect.');
		System.assertEquals('', comAsset.COM_BOP_C_code__c, 'Asset BOP Code is incorrect.');
		System.assertEquals(serviceList[0].csord__Order__r.csord__Account__c, comAsset.AccountId, 'Asset Account Id is incorrect.');
	}

	@isTest
	private static void testPrepareCOMAssets() {
		csord__Service__c services = [SELECT Id FROM csord__Service__c WHERE csord__Identification__c = 'testService_helpertest'];
		List<Id> serviceIdList = new List<Id>{ services.Id };
		List<csord__Service__c> serviceList = COM_Helper.getServiceList(serviceIdList);

		List<COM_Delivery_Article_Materials__mdt> deliveryArticleMaterials = [
			SELECT
				Id,
				DeveloperName,
				COM_Delivery_Article__c,
				COM_Delivery_Article__r.DeveloperName,
				COM_Delivery_Article__r.Label,
				COM_Delivery_Article__r.Delivery_Article_Name__c,
				COM_Delivery_Article__r.Delivery_Component__r.DeveloperName,
				COM_Delivery_Materials__c,
				COM_Delivery_Materials__r.DeveloperName,
				COM_Delivery_Materials__r.Label,
				COM_Delivery_Materials__r.Returnable__c,
				COM_Delivery_Materials__r.Returnable_by_Box__c,
				COM_Delivery_Materials__r.Model_Name__c,
				COM_Delivery_Materials__r.BOP_C_code__c,
				COM_Delivery_Materials__r.Generate_Asset__c,
				COM_Delivery_Materials__r.Type__c
			FROM COM_Delivery_Article_Materials__mdt
			WHERE COM_Delivery_Materials__r.Generate_Asset__c = TRUE
			LIMIT 1
		];

		COM_MetadataInformation.DeliveryComponent deliveryComponent = new COM_MetadataInformation.DeliveryComponent();
		deliveryComponent.Name = 'TestDelCompName';
		deliveryComponent.articleName = deliveryArticleMaterials[0].COM_Delivery_Article__r.DeveloperName;

		Test.startTest();
		List<Asset> comAssets = COM_Helper.prepareCOMAssets(serviceList[0], deliveryComponent, deliveryArticleMaterials);
		Test.stopTest();

		Asset comAsset = comAssets[0];

		System.assertEquals(
			Schema.SObjectType.Asset.getRecordTypeInfosByDeveloperName().get(COM_Constants.COM_ASSET_RECORD_TYPE).getRecordTypeId(),
			comAsset.RecordTypeId,
			'Asset record type is incorrect.'
		);
		System.assertEquals(deliveryArticleMaterials[0].COM_Delivery_Materials__r.Model_Name__c, comAsset.Name, 'Asset name is incorrect.');
		System.assertEquals(deliveryArticleMaterials[0].COM_Delivery_Materials__r.Type__c, comAsset.COM_Type__c, 'Asset type is incorrect.');
		System.assertEquals(serviceList[0].COM_Delivery_Order__c, comAsset.COM_Delivery_Order__c, 'Asset Delivery Order is incorrect.');
		System.assertEquals(deliveryComponent.name, comAsset.COM_Delivery_Component__c, 'Asset Delivery Component is incorrect.');
		System.assertEquals(serviceList[0].Id, comAsset.csord__Service__c, 'Asset Service Id is incorrect.');
		System.assertEquals(
			deliveryArticleMaterials[0].COM_Delivery_Materials__r.BOP_C_code__c,
			comAsset.COM_BOP_C_code__c,
			'Asset BOP Code is incorrect.'
		);
		System.assertEquals(serviceList[0].csord__Order__r.csord__Account__c, comAsset.AccountId, 'Asset Account Id is incorrect.');
	}

	@isTest
	private static void testFetchAssetMapByDeliveryOrderId() {
		COM_Delivery_Order__c delOrd = [SELECT Id FROM COM_Delivery_Order__c WHERE Name = 'TestDelOrd_helpertest_asset'];

		Test.startTest();
		Map<Id, List<Asset>> result = COM_Helper.fetchAssetMapByDeliveryOrderId(new Set<Id>{ delOrd.Id });
		Test.stopTest();

		System.assertEquals(1, result.get(delOrd.Id).size());
		System.assertEquals('Test_Asset', result.get(delOrd.Id)[0].Name);
	}

	@isTest
	private static void testFetchDeliveryMaterialsMapByModelName() {
		Test.startTest();
		Map<String, COM_Delivery_Materials__mdt> result = COM_Helper.fetchDeliveryMaterialsMapByModelName();
		Test.stopTest();

		System.assertNotEquals(null, result.get('TestMaterial_Model'));
	}
}
