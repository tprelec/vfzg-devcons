global with sharing class CustomButtonApplyInflightChange extends csbb.CustomButtonExt {
    public String performAction(String basketId) {
        CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();
        try{
            callCSApplyInflightRequestAPI(basketId);
            String newUrl = CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(basketId, null);
            String reqExp = '^/'+ '\\' +'w{15,18}';
            Pattern ptr = Pattern.compile(reqExp);
            Matcher m = ptr.matcher(newUrl);
            if(m.matches() && m.hitEnd()){
                return '{"status":"ok","text":"Basket has been synced successfully","redirectURL":"' + newUrl + '"}';
            } else {
                return '{"status":"error","text":"Basket was not Synched correctly"}';
            }
        }catch(Exception e){
            return '{"status":"error","text":"' + e.getMessage() + '"}';
        }
    }

    private static void callCSApplyInflightRequestAPI(Id basketId) {
        System.debug('Apply Inflight change!');
        csordtelcoa.API_V1.applyInflightChanges(basketId, false);
        System.debug('Apply Inflight applied!');
    }
}