/*
 * @author Juan Cardona
 *
 * @description Controller for page to launch installation information from custom button configured by cloudsense
 */

// global modifier is required by cloudsense interface
@SuppressWarnings('PMD.AvoidGlobalModifier')
global with sharing class CustomButtonInstallationInformation extends csbb.CustomButtonExt {
	// redirects to a specific URL with base URL configured from salesforce, hence this is good and suppress is needed
	// using full path because page opens in iFrame
	@SuppressWarnings('PMD.ApexOpenRedirect')
	public String performAction(String basketId) {
		CustomButtonRedirectURL__c url = CustomButtonRedirectURL__c.getOrgDefaults();
		PageReference editPage = new PageReference(url.URL__c + Page.InstallationInformation.getUrl() + '?recordId=' + basketId);
		return '{"status":"ok","redirectURL":"' +
			editPage.getUrl() +
			'", "displayInDialog":true, "size":"l", "modalTitle":"Installation Information"}';
	}
}
