public with sharing class OrderEntryInstallationController {
	/**
	 * @description Saves Notes into Opportunity Description
	 * @param  opportunityId Opportunity Id
	 * @param  notes         Notes
	 * @return               Opportunity
	 */
	@AuraEnabled
	public static Opportunity saveOpportunityDescription(Id opportunityId, String notes) {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);
		Opportunity opp = [SELECT Id, Description FROM Opportunity WHERE Id = :opportunityId];
		opp.Description = notes;
		update opp;
		return opp;
	}

	/**
	 * @description Update Opportunity with OverstapService data
	 * @param  opportunityId Opportunity Id
	 * @param  operatorSwitch         Boolean
	 * @param  currentProvider         String
	 * @param  currentProviderContractNumber         String
	 */
	@AuraEnabled
	public static void updateOpportunityOverstapService(
		Id opportunityId,
		Boolean operatorSwitch,
		String currentProvider,
		String currentProviderContractNumber
	) {
		TriggerHandler.preventRecursiveTrigger('OpportunityTriggerHandler', null, 0);

		Opportunity opp = [
			SELECT Id, Operator_Switch__c, Provider__c, Current_Provider_Contract_Number__c
			FROM Opportunity
			WHERE Id = :opportunityId
		];
		opp.Operator_Switch__c = operatorSwitch;
		opp.Provider__c = currentProvider;
		opp.Current_Provider_Contract_Number__c = currentProviderContractNumber;

		update opp;
	}
}
