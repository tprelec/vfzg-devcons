public without sharing class QueueUserSharingTriggerHandler extends TriggerHandler{

    public override void beforeInsert() {

        List<Queue_User_Sharing__c> newQueueUserList = (List<Queue_User_Sharing__c>) this.newList;

        setQueueUserName(newQueueUserList, null);
    }

    public override void beforeUpdate() {

        List<Queue_User_Sharing__c> newQueueUserList = (List<Queue_User_Sharing__c>) this.newList;
        Map<Id, Queue_User_Sharing__c> oldQueueUserMap = (Map<Id, Queue_User_Sharing__c>) this.oldMap;

        setQueueUserName(newQueueUserList, oldQueueUserMap);
    }

    private void setQueueUserName(List<Queue_User_Sharing__c> newQueueUserList, Map<Id, Queue_User_Sharing__c> oldQueueUserMap){

        set<Id> ownersId = new set<Id>();
        map<Id, string> mapOwnersId = new map<Id, string>();

        for(Queue_User_Sharing__c qus : newQueueUserList)
            if(qus.OwnerId!=null && (oldQueueUserMap == null || oldQueueUserMap.containsKey(qus.Id) && oldQueueUserMap.get(qus.Id).OwnerId != qus.OwnerId))
                ownersId.add(qus.OwnerId);

        if(!ownersId.isEmpty()){

            for(User usr : [SELECT Id, Name FROM User WHERE Id IN: ownersId])
                mapOwnersId.put(usr.Id, usr.Name);

            for(Group grp : [SELECT Id, Name FROM Group WHERE Type = 'Queue' AND ID IN:ownersId])
                mapOwnersId.put(grp.Id, grp.Name);

            for(Queue_User_Sharing__c qus : newQueueUserList)
                if(mapOwnersId.containsKey(qus.OwnerId))
                    qus.Name = mapOwnersId.get(qus.OwnerId);

        }
    }

}