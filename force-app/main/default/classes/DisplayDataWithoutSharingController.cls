public without sharing class DisplayDataWithoutSharingController {
	public String objectsJson { get; set; }
	public String columnNamesJson { get; set; }
	public String columnReferencesJson { get; set; }

	public DisplayDataWithoutSharingController() {
		String dataWithoutSharingName = ApexPages.currentPage().getParameters().get('dwsname').escapeEcmaScript();
		DataWithoutSharing__mdt dataWithoutSharing = [
			SELECT Query__c, Column_Names__c
			FROM DataWithoutSharing__mdt
			WHERE DeveloperName = :dataWithoutSharingName
		];
		columnNamesJson = JSON.serialize(dataWithoutSharing.Column_Names__c.split(';'));

		String parsedQuery = replaceQueryPlaceholders(dataWithoutSharing.Query__c);

		columnReferencesJson = extractColumnReferences(parsedQuery);

		List<SObject> queriedObjects = Database.query(parsedQuery);
		if (queriedObjects.size() > 0) {
			objectsJson = JSON.serialize(queriedObjects);
		}
	}

	private String replaceQueryPlaceholders(String query) {
		Pattern paramPattern = Pattern.compile('\\[\\w+\\]');
		Matcher paramMatcher = paramPattern.matcher(query);
		while (paramMatcher.find()) {
			String currentParameterNamePlaceholder = paramMatcher.group();
			try {
				String currentParameterName = currentParameterNamePlaceholder.removeStart('[').removeEnd(']');
				String currentParameterValue = ApexPages.currentPage().getParameters().get(currentParameterName).escapeEcmaScript();
				//escape for regex in replaceAll
				String regexParamPlaceholder = '\\[' + currentParameterName + '\\]';
				query = query.replaceAll(regexParamPlaceholder, currentParameterValue);
			} catch (Exception ex) {
				LoggerService.log(ex);
			}
		}
		return query;
	}

	private String extractColumnReferences(String query) {
		String lowerCaseQuery = query.toLowerCase();
		String prefix = 'select ';
		String suffix = ' from ';
		Integer startIndex = lowerCaseQuery.indexOf(prefix);
		Integer endIndex = lowerCaseQuery.indexOf(suffix);
		String fieldsSelected = query.substring(startIndex + prefix.length(), endIndex);
		List<String> fieldsSelectedSplit = fieldsSelected.split(',');
		for (Integer splitIndex = 0; splitIndex < fieldsSelectedSplit.size(); splitIndex++) {
			fieldsSelectedSplit[splitIndex] = fieldsSelectedSplit[splitIndex].trim();
		}
		return JSON.serialize(fieldsSelectedSplit);
	}
}
