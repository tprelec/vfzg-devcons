@SuppressWarnings('PMD.ExcessivePublicCount')
public with sharing class OrderFlowService {
	@TestVisible
	private static CustomOrderData data;
	@TestVisible
	private static String accId;
	@TestVisible
	private static String pbId;
	@TestVisible
	private static final String AGREEMENT_MAVEN_DOC_RECORD_TYPE_NAME = 'Maven_Document';
	@TestVisible
	private static final String AGREEMENT_OUTPUT_FORMAT_PDF = 'pdf';
	@TestVisible
	private static final String AGREEMENT_APPROVAL_TYPE_NO_APPROVAL = 'No Approval';
	@TestVisible
	private static final String DOCUMENT_TEMPLATE_MASTER_AGREEMENT_DUTCH = 'Mantelovereenkomst';
	@TestVisible
	private static final String AGREEMENT_APPROVAL_STATUS_APPROVED = 'Approved';
	@TestVisible
	private static final String DOCUMENT_REQUEST_STATUS_NEW = 'New';
	@TestVisible
	private static final String DOCUMENT_REQUEST_DOCUMENT_FORMAT_PDF = 'PDF';
	@TestVisible
	private static final String DOCUMENT_REQUEST_SAVE_AS_ATTACHMENT = 'Attachment';
	@TestVisible
	private static final String DOCUMENT_REQUEST_PROCESSING_TYPE_BATCH = 'Batch';

	@TestVisible
	private static Account acc;

	private static Opportunity opp;
	private static Opportunity_Attachment__c contractAtt;
	private static Opportunity_Attachment__c copyKvkAtt;
	private static Opportunity_Attachment__c numberlistAtt;
	private static Ban__c ban;
	private static Financial_Account__c finAcc;
	private static Billing_Arrangement__c billingArr;
	private static VF_Contract__c vfContr;
	private static List<Installation> installations;

	public static void parseData(String jsonStr, String accountId, String productBasketId) {
		data = (CustomOrderData) JSON.deserialize(jsonStr, CustomOrderData.class);
		if (accountId == null) {
			accId = getAccountFromEcommerceOrderRequest(data.customer.identification.purchaseContactId);
		} else {
			accId = accountId;
		}

		if (data.installation != null) {
			installations = data.installation;
		}

		pbId = productBasketId;
		init();
	}

	// If the account id is not populated on the poduct basket we can fetch it using the Ecommerce Order Request object
	@TestVisible
	private static Id getAccountFromEcommerceOrderRequest(Id purchaseContactId) {
		return [SELECT AccountId FROM Contact WHERE Id = :purchaseContactId].AccountId;
	}

	// Start creating the data
	private static void init() {
		if (String.isNotBlank(accId)) {
			acc = [
				SELECT
					Id,
					Name,
					Frame_Work_Agreement__c,
					VZ_Framework_Agreement__c,
					Create_Framework_Agreement__c,
					Visiting_City__c,
					Visiting_Housenumber1__c,
					Visiting_Housenumber_Suffix__c,
					Visiting_Postal_Code__c,
					Visiting_street__c,
					Authorized_to_sign_1st__c
				FROM Account
				WHERE Id = :accId
			];

			createBan();
			createFinancialAccount();
			createBillingArrangment();

			createTechnicalContactRoles();

			if (String.isBlank(acc.Frame_Work_Agreement__c)) {
				AccountGenerateFrameworkButtonController.generateFrameworkForVodafoneAccount(acc.Id);
			}

			opp = createOpportunity();
			connectProductBasketWithOpportunity();

			contractAtt = createOpportunityAttachment('Contract (Signed)');
			copyKvkAtt = createOpportunityAttachment('Copy KvK');
			numberlistAtt = createOpportunityAttachment('Number List');

			syncOppAndHandleContractCreation(pbId);
		}
	}

	@future(callout=true)
	private static void syncOppAndHandleContractCreation(String pbId) {
		Map<Id, CCAQDProcessor.CCAQDStructure> pcData = CCAQDProcessor.extractPCAqdData(new List<Id>{ pbId });

		CustomButtonSynchronizeWithOpportunity.syncWithOpportunityOnlineScenario(pbId, pcData);

		if (installations != null) {
			mapPcCustomDataToPcAndOli(pcData);
			mapSiteAvailability(pcData);
		}

		List<cscfga__Product_Basket__c> baskets = [
			SELECT
				Id,
				csbb__Account__c,
				csbb__Account__r.Create_VZ_Framework_Agreement__c,
				cscfga__Opportunity__r.Direct_Indirect__c,
				Sign_on_behalf_of_customer__c,
				Contract_Language__c,
				cscfga__Opportunity__c,
				Name,
				TodayFormatted__c,
				Basket_Number__c,
				cscfga__Basket_Status__c
			FROM cscfga__Product_Basket__c
			WHERE Id = :pbId
		];

		baskets[0].cscfga__Basket_Status__c = 'Approved';
		update baskets[0];

		acc = [
			SELECT
				Id,
				Name,
				Frame_Work_Agreement__c,
				VZ_Framework_Agreement__c,
				Create_VZ_Framework_Agreement__c,
				Create_Framework_Agreement__c,
				Visiting_City__c,
				Visiting_Housenumber1__c,
				Visiting_Housenumber_Suffix__c,
				Visiting_Postal_Code__c,
				Visiting_street__c,
				Authorized_to_sign_1st__c
			FROM Account
			WHERE Id = :accId
		];

		generateContractsWithMavenDocs(baskets[0]);

		CCOrderRequestProcessing.generateCtnRecordsOnlineScenario(baskets);
	}

	@TestVisible
	private static Opportunity createOpportunity() {
		Account acc = [SELECT Id, Name FROM Account WHERE Id = :accId];
		COM_One_Mobile_Online_Channel_Setting__mdt settings = [
			SELECT Online_Chanel_Opportunity_Owner__c
			FROM COM_One_Mobile_Online_Channel_Setting__mdt
			WHERE DeveloperName = 'SFCC_Integration_User'
			LIMIT 1
		];
		User b2bDefaultOwner = [SELECT Id, Name FROM User WHERE Username = :settings.Online_Chanel_Opportunity_Owner__c LIMIT 1];
		Id defaultRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('Default').getRecordTypeId();

		opp = new Opportunity(
			AccountId = accId,
			RecordTypeId = defaultRecordTypeId,
			Name = acc.Name + ' Acq + Proposition',
			CloseDate = Date.today(),
			StageName = 'Offer',
			Type = 'New Business',
			Deal_Type__c = 'Acquisition',
			OwnerId = b2bDefaultOwner.Id,
			BAN__c = ban.Id,
			Document_Type__c = data.customer.identification.identificationType,
			Document_Number__c = data.customer.identification.identificationNumber,
			Financial_Account__c = finAcc.Id,
			Billing_Arrangement__c = billingArr.Id,
			Signature_Status__c = 'Approved',
			Document_Signature__c = 'No Method',
			B2B_Online__c = true
		);

		insert opp;
		return opp;
	}

	// Connect the product basket with the created opportunity
	@TestVisible
	private static void connectProductBasketWithOpportunity() {
		cscfga__Product_Basket__c pb = [
			SELECT Id, cscfga__Opportunity__c, Sign_on_behalf_of_customer__c, Contract_Language__c
			FROM cscfga__Product_Basket__c
			WHERE Id = :pbId
		];
		pb.cscfga__Opportunity__c = opp.Id;
		pb.Sign_on_behalf_of_customer__c = 'first';
		pb.Contract_Language__c = 'Dutch';
		update pb;
	}

	// Create Opportunity Attachment
	@TestVisible
	private static Opportunity_Attachment__c createOpportunityAttachment(String attType) {
		Opportunity_Attachment__c oppAtt = new Opportunity_Attachment__c(Attachment_Type__c = attType, Opportunity__c = opp.Id);

		insert oppAtt;
		return oppAtt;
	}

	// Create BAN
	@TestVisible
	private static Ban__c createBan() {
		// Create correct ban name preserving the format of system now and adjusting the time zone difference
		String banName = 'Requested New Unify BAN ';
		banName += String.valueOf(Date.Today().Year());
		banName += '-';
		if (Date.Today().Month() < 10) {
			banName += '0';
		}
		banName += String.valueOf(Date.Today().Month());
		banName += '-';
		if (Date.Today().Day() < 10) {
			banName += '0';
		}
		banName += String.valueOf(Date.Today().Day());
		banName += ' ';
		Datetime dt = Datetime.now();
		if (dt.Hour() < 10) {
			banName += '0';
		}
		banName += String.valueOf(dt.Hour());
		banName += ':';
		if (dt.Minute() < 10) {
			banName += '0';
		}
		banName += String.valueOf(dt.Minute());
		banName += ':';
		if (dt.Second() < 10) {
			banName += '0';
		}
		banName += String.valueOf(dt.Second());

		ban = new Ban__c(
			Account__c = accId,
			Name = banName,
			BAN_Status__c = 'Requested',
			Unify_Customer_Type__c = 'B',
			Unify_Customer_SubType__c = 'BU'
		);

		insert ban;
		return ban;
	}

	// Create Financial Account
	@TestVisible
	private static Financial_Account__c createFinancialAccount() {
		finAcc = new Financial_Account__c(
			BAN__c = ban.Id,
			Bank_Account_Number__c = data.order.payment.IBAN,
			Payment_method__c = data.order.payment.paymentMethod,
			Financial_Contact__c = acc.Authorized_to_sign_1st__c
		);

		insert finAcc;
		return finAcc;
	}

	// Create Billing Arrangement
	@TestVisible
	private static Billing_Arrangement__c createBillingArrangment() {
		billingArr = new Billing_Arrangement__c(
			Financial_Account__c = finAcc.Id,
			Payment_method__c = data.order.payment.paymentMethod,
			Bank_Account_Number__c = data.order.payment.IBAN,
			Bank_Account_Name__c = acc.Name,
			Billing_Arrangement_Alias__c = StringUtils.truncate(acc.Name, 50),
			Bill_Format__c = 'EB',
			Billing_City__c = acc.Visiting_City__c,
			Billing_Housenumber__c = String.valueOf(acc.Visiting_Housenumber1__c),
			Billing_Housenumber_Suffix__c = acc.Visiting_Housenumber_Suffix__c,
			Billing_Postal_Code__c = acc.Visiting_Postal_Code__c,
			Billing_Street__c = acc.Visiting_street__c
		);

		insert billingArr;
		return billingArr;
	}

	// Create VF Contract
	@TestVisible
	private static VF_Contract__c createVFContract() {
		vfContr = new VF_Contract__c(
			Signed_by_Customer__c = acc.Authorized_to_sign_1st__c,
			Account__c = acc.Id,
			Opportunity__c = opp.Id,
			Document_Type__c = data.customer.identification.identificationType.capitalize(),
			Document_Number__c = data.customer.identification.identificationNumber
		);

		insert vfContr;
		return vfContr;
	}

	//Adding Contact Role for the Contact
	public static void createTechnicalContactRoles() {
		if (installations != null) {
			List<Contact_Role__c> contactRolesToAdd = new List<Contact_Role__c>();
			for (Installation installation : installations) {
				Contact_Role__c contactRole = new Contact_Role__c(
					Account__c = accId,
					Active__c = true,
					Contact__c = installation.technicalContact.id,
					Site__c = installation.site.id,
					Type__c = 'maintenance'
				);
				contactRolesToAdd.add(contactRole);
			}
			insert contactRolesToAdd;
		}
	}

	private static void mapSiteAvailability(Map<Id, CCAQDProcessor.CCAQDStructure> pcData) {
		Map<Id, Id> pcWithSiteMap = new Map<Id, Id>();
		List<cscfga__Product_Configuration__c> prodConfToUpdate = new List<cscfga__Product_Configuration__c>();

		for (Id pcId : pcData.keyset()) {
			pcWithSiteMap.put(pcData.get(pcId).customData.get('siteId'), pcId);
		}

		List<Site_Availability__c> siteAvailabilityList = [
			SELECT Id, Site__c, Usable__c, Access_Infrastructure__c
			FROM Site_Availability__c
			WHERE Site__c IN :pcWIthSiteMap.values() AND Usable__c = TRUE AND Access_Infrastructure__c = 'Coax'
		];

		for (Site_Availability__c siteAv : siteAvailabilityList) {
			for (Id pcId : pcWithSiteMap.keyset()) {
				if (pcWithSiteMap.get(pcId) == siteAv.Site__c) {
					prodConfToUpdate.add(new cscfga__Product_Configuration__c(Id = pcId, Site_Availability_ID__c = siteAv.Id));
				}
			}
		}

		update prodConfToUpdate;
	}

	private static void mapPcCustomDataToPcAndOli(Map<Id, CCAQDProcessor.CCAQDStructure> pcData) {
		List<OpportunityLineItem> oppLineItemList = [
			SELECT Id, Location__c, cscfga__Attribute__r.cscfga__Product_Configuration__c, Installation_Wish_Date__c
			FROM OpportunityLineItem
			WHERE cscfga__Attribute__r.cscfga__Product_Configuration__c IN :pcData.keySet()
		];

		List<cscfga__Product_Configuration__c> pcListToUpdate = new List<cscfga__Product_Configuration__c>();
		List<OpportunityLineItem> oppLineItemListToUpdate = new List<OpportunityLineItem>();

		for (OpportunityLineItem oli : oppLineItemList) {
			if (pcData.get(oli.cscfga__Attribute__r.cscfga__Product_Configuration__c) != null) {
				CustomDataInstallationInformation installationInformation = (CustomDataInstallationInformation) JSON.deserialize(
					pcData.get(oli.cscfga__Attribute__r.cscfga__Product_Configuration__c).customData.get('InstallationInformation'),
					CustomDataInstallationInformation.class
				);
				oli.Location__c = installationInformation.siteId;
				oli.Installation_Wish_Date__c = Date.valueOf(installationInformation.installationWIshDate);
				pcListToUpdate.add(
					new cscfga__Product_Configuration__c(
						Id = oli.cscfga__Attribute__r.cscfga__Product_Configuration__c,
						Site__c = installationInformation.siteId,
						Installation_Wish_Date__c = Date.valueOf(installationInformation.installationWishDate)
					)
				);
				oppLineItemListToUpdate.add(oli);
			}
		}

		update pcListToUpdate;
		update oppLineItemListToUpdate;
	}

	@TestVisible
	public static void generateContractsWithMavenDocs(cscfga__Product_Basket__c basket) {
		List<Credit_check__c> creditCheck = [
			SELECT Id, Credit_Check_Status_Number__c, Opportunity__c
			FROM Credit_check__c
			WHERE Opportunity__c = :opp.Id
			ORDER BY CreatedDate DESC
		];

		//generate contracts only if credit check is approved
		if (!creditCheck.isEmpty() && creditCheck[0].Credit_Check_Status_Number__c == 2) {
			List<csclm__Agreement__c> agreements = createAgreementRecords(basket.cscfga__Opportunity__c);
			//create framework agreement if needed
			if (basket.csbb__Account__r.Create_VZ_Framework_Agreement__c) {
				agreements.add(createMasterAgreement(basket));
			}

			createMavenDocumentRequests(agreements);

			//Update product basket and opportunity after contract creation
			basket.cscfga__Basket_Status__c = 'Contract created';
			update basket;
			opp.StageName = 'Closing';
			update opp;
		}
	}

	@TestVisible
	public static csclm__Agreement__c createMasterAgreement(cscfga__Product_Basket__c basket) {
		Id agreementMavenDocRecordTypeId = Schema.SObjectType.csclm__Agreement__c.getRecordTypeInfosByDeveloperName()
			.get(AGREEMENT_MAVEN_DOC_RECORD_TYPE_NAME)
			.getRecordTypeId();

		Id documentTemplateId = [SELECT Id FROM mmdoc__Document_Template__c WHERE Name = :DOCUMENT_TEMPLATE_MASTER_AGREEMENT_DUTCH LIMIT 1][0].Id;

		csclm__Agreement__c agreement = new csclm__Agreement__c(
			RecordTypeId = agreementMavenDocRecordTypeId,
			Name = 'Master Agreement ' + basket.Name,
			Maven_Document_Template__c = documentTemplateId,
			csclm__Opportunity__c = basket.cscfga__Opportunity__c,
			Product_Basket__c = basket.Id,
			csclm__Account__c = basket.csbb__Account__c,
			csclm__Output_Format__c = AGREEMENT_OUTPUT_FORMAT_PDF,
			csclm__Approval_Type__c = AGREEMENT_APPROVAL_TYPE_NO_APPROVAL,
			csclm__Approval_Status__c = AGREEMENT_APPROVAL_STATUS_APPROVED
		);
		return agreement;
	}

	@TestVisible
	public static void createMavenDocumentRequests(List<csclm__Agreement__c> agreements) {
		List<mmdoc__Document_Request__c> docReqToInsert = new List<mmdoc__Document_Request__c>();
		Id mDocSolutionId = [SELECT Id, Name FROM mmdoc__Document_Solution__c WHERE Name = 'Agreements'].Id;
		for (csclm__Agreement__c agreement : agreements) {
			docReqToInsert.add(
				new mmdoc__Document_Request__c(
					mmdoc__Record_Id__c = agreement.Id,
					Agreement__c = agreement.Id,
					mmdoc__Document_Template__c = agreement.Maven_Document_Template__c,
					mmdoc__Document_Solution__c = mDocSolutionId,
					mmdoc__Status__c = DOCUMENT_REQUEST_STATUS_NEW,
					mmdoc__Document_Format__c = DOCUMENT_REQUEST_DOCUMENT_FORMAT_PDF,
					mmdoc__Save_As__c = DOCUMENT_REQUEST_SAVE_AS_ATTACHMENT,
					mmdoc__Processing_Type__c = DOCUMENT_REQUEST_PROCESSING_TYPE_BATCH
				)
			);
		}
		insert docReqToInsert;
	}

	@TestVisible
	private static List<csclm__Agreement__c> createAgreementRecords(Id opportunityId) {
		List<csclm__Agreement__c> agreements = OppDocumentGenerationController.getAgreementsObjects(opportunityId);

		insert agreements;
		return agreements;
	}

	// Classes to deserialize JSON
	public class CustomOrderData {
		public Order order;
		public Integer dataCap;
		public Customer customer;
		public AppliedPromotions appliedPromo;
		public List<Installation> installation;
	}

	public class Order {
		public Payment payment;
	}
	@SuppressWarnings('PMD.FieldNamingConventions')
	public class Payment {
		public String paymentMethod;
		public String IBAN;
	}

	public class Customer {
		public Identification identification;
	}

	public class Identification {
		public string purchaseContactId;
		public String nationality;
		public String identificationType;
		public String identificationNumber;
		public String identificationExpirationdate;
	}

	public class AppliedPromotions {
		public String promoId;
		public String billingCode;
	}

	public class CustomContact {
		public String initials;
		public String middleName;
		public String lastName;
		public String email;
		public String phone;
		public String comment;
	}

	public class Installation {
		public Site site;
		public String wishDate;
		public TechnicalContact technicalContact;
	}

	public class TechnicalContact {
		public String email;
		public String id;
	}

	public class Address {
		public String houseNumber;
		public String houseNumberAddition;
		public String postcode;
	}

	public class Site {
		public String id;
		public Address address;
	}

	public class CustomDataInstallationInformation {
		public String siteId;
		public String installationWishDate;
	}
}
