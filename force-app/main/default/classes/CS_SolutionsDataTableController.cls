/**
 * Created by nikola.culej on 4.6.2020.
 */

public with sharing class CS_SolutionsDataTableController {

	@AuraEnabled
	public static List<Suborder__c> getSolutionList(Id recordId){
		//List<csord__Solution__c> returnList = new List<csord__Solution__c>();
		List<Suborder__c> returnSuborderList = new List<Suborder__c>();

		// Case cList = [SELECT Id, COM_Order__c FROM Case WHERE Id = :recordId];
		
		Task task = [SELECT Id, Order__c, WhatId
		                       FROM Task
		                       WHERE Id = :recordId];
        
		// returnList = [SELECT Id, Site__c, Name, Technical_Contact__c, Site__r.Unify_Site_Name__c, Technical_Contact__r.Name, Products__c, Wish_Date__c, Start_Delivery__c/*, Suppress_Automated_Communication__c*/ FROM csord__Solution__c WHERE csord__Order__c = :cList.COM_Order__c/* AND RecordType.DeveloperName = 'Suborder'*/];
	    
	    returnSuborderList = [SELECT
	                            Id, Site__c, Name, Technical_Contact__c, Site__r.Unify_Site_Name__c, Technical_Contact__r.Name, Products__c, Wish_Date__c, Start_Delivery__c, Suppress_Automated_Communication__c 
	                          FROM Suborder__c 
	                          WHERE Order__c = :task.WhatId];
	                          
        

		return returnSuborderList;
	}

	@AuraEnabled
	public static String updateSolutions(List<sObject> records){
		return JSON.serialize(Database.update(records));
	}
}