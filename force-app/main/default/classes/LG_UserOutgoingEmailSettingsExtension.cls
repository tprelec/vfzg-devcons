public with sharing class LG_UserOutgoingEmailSettingsExtension {
    
    public User selectedUser { get; private set; }
    public Boolean editing { get; private set; }
    public String infoMessage { get; private set; }

    public Boolean canEditRecord {
        get {
            UserRecordAccess accessInformation = [SELECT RecordId, HasReadAccess, HasTransferAccess, HasEditAccess, MaxAccessLevel
                                                  FROM UserRecordAccess
                                                  WHERE UserId = :UserInfo.getUserId() AND RecordId = :selectedUser.Id];
            return accessInformation.HasEditAccess;
        }
    }

    public LG_UserOutgoingEmailSettingsExtension(ApexPages.StandardController controller) {
        ID selectedUserId = ((User) controller.getRecord()).Id;
        selectedUser = [SELECT u.SenderName, u.SenderEmail FROM User u WHERE u.Id = :selectedUserId];
        editing = false;
    }

    public System.PageReference startEditing() {
        editing = canEditRecord ? true : false;
        infoMessage = null;
        return null;
    }

    public System.PageReference confirm() {
        if (canEditRecord) {
            Database.update(selectedUser);
            editing = false;
            infoMessage = Label.LG_OutgoingEmailSettingsConfirmationNeeded;
            return null;
        } else {
            cancelEditing();
            return (new ApexPages.StandardController(selectedUser)).cancel();
        }
    }

    public System.PageReference cancelEditing() {
        selectedUser = [SELECT u.SenderName, u.SenderEmail FROM User u WHERE u.Id = :selectedUser.Id];
        editing = false;
        infoMessage = null;
        return null;
    }
}