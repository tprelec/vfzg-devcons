@IsTest
public with sharing class TestOrderEntrySiteController {
	@TestSetup
	static void makeData() {
		OrderEntryTestDataFactory.createOpportunityWithAllRelatedRecords();
	}

	@IsTest
	public static void testGetSites() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		insert new Site__c(
			Site_Street__c = 'Balistraat',
			Site_Postal_Code__c = '3531EC',
			Site_House_Number__c = 59,
			Site_City__c = 'Utrecht',
			Site_Account__c = acc.Id
		);

		Test.startTest();
		List<Site__c> fetchedSites = OrderEntrySiteController.getSites(acc.Id);
		Test.stopTest();

		System.assert(fetchedSites.size() > 0, 'There should be at least one site.');
	}
	@IsTest
	public static void testGetSite() {
		Site__c site = [SELECT Id, Site_Postal_Code__c FROM Site__c LIMIT 1];

		Test.startTest();

		Site__c fetchedSite = OrderEntrySiteController.getSite(site.Id);
		Test.stopTest();

		System.assertEquals(site.Id, fetchedSite.Id, 'Ids don\'t match');
		System.assertEquals(site.Site_Postal_Code__c, fetchedSite.Site_Postal_Code__c, 'Postal codes don\'t match.');
	}
	@IsTest
	public static void testCheckSite() {
		Test.setMock(HttpCalloutMock.class, new MockHttpAddressCheck());

		Test.startTest();
		String result = OrderEntrySiteController.checkSite('1111XX', 11, 'a');
		Test.stopTest();

		AddressCheckService.AddressCheckResult addressCheck = (AddressCheckService.AddressCheckResult) JSON.deserialize(
			result,
			AddressCheckService.AddressCheckResult.class
		);
		Boolean isGigaAvailable = false;
		for (AddressCheckService.AddressCheckAvailabilityResult availability : addressCheck.availability) {
			if (availability.name == 'fp1000') {
				isGigaAvailable = true;
			}
		}
		System.assert(isGigaAvailable, 'Giga speed should be available.');
	}
	@IsTest
	public static void testUpdateSite() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
		Site__c site = new Site__c(
			Site_Street__c = 'Balistraat',
			Site_Postal_Code__c = '3531EC',
			Site_House_Number_Suffix__c = '',
			Site_House_Number__c = 59,
			Site_City__c = 'Utrecht',
			Site_Account__c = acc.Id
		);
		insert site;

		Site__c initSite = [
			SELECT Id, Site_Street__c, Site_Postal_Code__c, Site_House_Number_Suffix__c, Site_House_Number__c, Site_City__c, Site_Account__c
			FROM Site__c
			WHERE Id = :site.Id
		];
		initSite.Site_House_Number__c = 69;
		initSite.Site_City__c = 'Utrecht Noord';

		Test.startTest();
		Boolean isUpdated = OrderEntrySiteController.updateSite(initSite, opp.Id);
		Test.stopTest();

		Site__c updatedSite = [SELECT Id, Site_Street__c, Site_Postal_Code__c, Site_House_Number__c, Site_City__c FROM Site__c WHERE Id = :site.Id];

		System.assert(isUpdated, 'Site should be updated.');
		System.assertEquals(site.Id, updatedSite.Id, 'Site id should match.');
		System.assertEquals(site.Site_Street__c, updatedSite.Site_Street__c, 'Site street should match.');
		System.assertNotEquals(site.Site_House_Number__c, updatedSite.Site_House_Number__c, 'Site house number should not match.');
		System.assert('Utrecht Noord' == updatedSite.Site_City__c, 'Site city should be updated.');
	}

	@IsTest
	public static void testOblicoSearch() {
		OlbicoSettings__c oblico = new OlbicoSettings__c();
		oblico.Olbico_JSon_Endpoint__c = 'Fake endpoint';
		oblico.Olbico_Json_Username__c = 'Fake username';
		oblico.Olbico_Json_Password__c = 'Fake password';
		oblico.Olbico_Json_BAG_Streets__c = 'Fake bag street';
		insert oblico;

		Test.startTest();
		Map<String, String> result = OrderEntrySiteController.oblicoSearch('1111XX');
		Test.stopTest();

		System.assertEquals(result.size(), 0, 'There shuld be no options returned.');
	}
	@IsTest
	public static void testSaveSite() {
		Account acc = [SELECT Id FROM Account LIMIT 1];

		Test.startTest();
		Site__c newSIte = OrderEntrySiteController.saveSite('Medanstraat | 1 |  |  | 3531EC | Utrecht', acc.Id);
		Test.stopTest();

		List<Site__c> postSites = [SELECT Id FROM Site__c WHERE Site_Account__c = :acc.Id];
		System.assert(postSites.size() > 0, 'There should be one new site.');
		System.assertEquals('Medanstraat', newSIte.Site_Street__c.trim(), 'There should be one new site.');
		System.assertEquals(1, newSIte.Site_House_Number__c, 'There should be one new site.');
		System.assertEquals('3531EC', newSIte.Site_Postal_Code__c.trim(), 'There should be one new site.');
	}
}
