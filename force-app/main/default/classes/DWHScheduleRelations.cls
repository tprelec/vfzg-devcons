global class DWHScheduleRelations implements Schedulable{

    global void execute(SchedulableContext sc) {

        DWHGenericBatch biccBatch = new DWHGenericBatch('DWH_Relation_SF_Interface__c');
        biccBatch.chain = true;
        Database.executeBatch(biccBatch, 100);
        
    }

}