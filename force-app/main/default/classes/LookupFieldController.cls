public with Sharing class LookupFieldController {
    @AuraEnabled
    public static List<sObject> getRecentRecords(String objectName, List<String> returnFields, Integer maxResults) {

        List<id> recentids = new List<id>();
        for(RecentlyViewed recent : [SELECT id FROM RecentlyViewed WHERE Type = :objectName ORDER BY LastViewedDate DESC LIMIT :maxResults]) {
            recentids.add(recent.id);
        }

        String sQUERY = 'SELECT id, ';

        if (returnFields != null && returnFields.Size() > 0) {
            sQuery += String.join(returnFields, ',');
        } else {
            sQuery += 'Name';
        }

        sQuery += ' FROM ' + objectName + ' WHERE id IN :recentids';

        List<sObject> searchResult = Database.query(sQuery);
        System.debug(searchResult);
        return searchResult;
    }

    @AuraEnabled
    public static List<sObject> searchRecords(String objectName, List<String> returnFields, List<String> queryFields, String searchText, String sortColumn, String sortOrder, Integer maxResults, String filter) {

        //always put a limit on the results
        if (maxResults == null || maxResults == 0) {
            maxResults = 5;
        }

        searchText = '%' + searchText + '%';

        List <sObject > returnList = new List < sObject > ();

        String sQuery =  'SELECT id, ';

        if (returnFields != null && returnFields.Size() > 0) {
            sQuery += String.join(returnFields, ',');
        } else {
            sQuery += 'Name';
        }

        sQuery += ' FROM ' + objectName + ' WHERE ';

        if (queryFields == null || queryFields.isEmpty()) {
            sQuery += ' Name LIKE :searchText ';
        } else {
            string likeField = '';
            for(string field : queryFields) {
                likeField += ' OR ' + field + ' LIKE :searchText ';
            }
            sQuery += ' (' + likeField.removeStart(' OR ') + ') ';
        }

        //if (filter != null) {
        //    sQuery += ' AND (' + filter + ')';
        //}

        if(string.isNotBlank(sortColumn) && string.isNotBlank(sortOrder)) {
            sQuery += ' ORDER BY ' + sortColumn + ' ' + sortOrder;
        }

        sQuery += ' LIMIT ' + maxResults;

        System.debug(sQuery);

        List <sObject> searchResult = Database.query(sQuery);

        return searchResult;
    }

    @AuraEnabled
    public static List<sObject> getRecord(String objectName, List<String> returnFields, String id) {
        String sQUERY = 'SELECT id, ';

        if (returnFields != null && returnFields.Size() > 0) {
            sQuery += String.join(returnFields, ',');
        } else {
            sQuery += 'Name';
        }

        sQuery += ' FROM ' + objectName + ' WHERE id = :id';

        List<sObject> searchResult = Database.query(sQuery);

        return searchResult;
    }

    @AuraEnabled
    public static string findObjectIcon(String objectName) {
        String u;
        List<Schema.DescribeTabResult> tabDesc = new List<Schema.DescribeTabResult>();
        List<Schema.DescribeIconResult> iconDesc = new List<Schema.DescribeIconResult>();

        for(Schema.DescribeTabSetResult describeTabSetResult : Schema.describeTabs()) {
            for(Schema.DescribeTabResult describeTabResult : describeTabSetResult.getTabs()) {
                if(describeTabResult.getSobjectName() == objectName) {
                    if( describeTabResult.isCustom() == true ) {
                        for (Schema.DescribeIconResult describeIcon : describeTabResult.getIcons()) {
                            if (describeIcon.getContentType() == 'image/svg+xml'){
                                return 'custom:' + describeIcon.getUrl().substringBetween('custom/','.svg').substringBefore('_');
                            }
                        }
                    } else {
                        return 'standard:' + objectName.toLowerCase();
                    }
                }
            }
        }

        return 'standard:default';
    }

    @AuraEnabled
    public static objectDetails getObjectDetails(String objectName) {
        System.debug('objectName   ' + objectName);
        objectDetails details = new objectDetails();

        Schema.DescribeSObjectResult describeSobjectsResult = Schema.describeSObjects(new List<String>{objectName})[0];

        details.label = describeSobjectsResult.getLabel();
        details.pluralLabel = describeSobjectsResult.getLabelPlural();

        details.iconName = findObjectIcon(objectName);
        System.debug('details   ' + details);
        return details;
    }

    public class ObjectDetails {
        @AuraEnabled
        public string iconName;
        @AuraEnabled
        public string label;
        @AuraEnabled
        public string pluralLabel;
    }
}