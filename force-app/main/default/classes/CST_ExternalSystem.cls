/*
 *  Miguel Alves
 *  @date 2022-04-26
 *  @description trigger handler for picking up fields in the Case Object for the Remedy ticket.
 */
public without sharing class CST_ExternalSystem extends TriggerHandler {
	private static final String REMEDY_NAMED_CREDENTIAL = 'CST_Remedy_Layer7';
	private static final String REMEDY_URL = '/v1/Remedy/arsys/WSDL/public/remedyapptest/VZ_GenericIncidentAPI';
	//To use when in need for trigger to CST_External_Ticket
	// lists and maps for the trigger records
	private List<CST_External_Ticket__c> newExternalSystems = (List<CST_External_Ticket__c>) this.newList;
	private Map<Id, CST_External_Ticket__c> newExternalSystemMap = (Map<Id, CST_External_Ticket__c>) this.newMap;
	private Map<Id, CST_External_Ticket__c> oldExternalSystemMap = (Map<Id, CST_External_Ticket__c>) this.oldMap;

	//private Map<Id, Case> newCaseMap = (Map<Id, Case>) this.newMap;

	public class Response {
		public String error;
		public String status;
		public String message;
		public String ticketid;
		public String caseId;
		public String contentversionid;
		public String contentdocumentid;
		public String linkedentityid;
		public List<uploadInfo> files;
		public Response() {
		}
	}

	public class uploadInfo {
		public String id;
		public String linkid;
		public String status;
		public String ticketid;
		public String contentversionid;
		public String contentdocumentid;
		public String linkedentityid;
		public String title;
		public String filename;
		public String fileContent;
		public Integer fileSize;
	}

	// initializing those references
	private void init() {
		newExternalSystems = (List<CST_External_Ticket__c>) this.newList;
		newExternalSystemMap = (Map<Id, CST_External_Ticket__c>) this.newMap;
		oldExternalSystemMap = (Map<Id, CST_External_Ticket__c>) this.oldMap;

		//newCaseMap = (Map<Id, Case>) this.newMap;
	}

	public override void beforeInsert() {
		system.debug('INIT');
		init();
	}

	public override void beforeUpdate() {
		init();
		validateStatusChanged();
		Map<id, Case> MapCase = new Map<id, Case>();
		for (integer i = 0; i < newExternalSystems.size(); i++) {
			MapCase.put(newExternalSystems[i].CST_Case__c, new Case());
		}
		system.debug('MapCase: ' + MapCase);
		List<Case> newListCase = [SELECT id, RecordTypeId, CST_End2End_Owner__c, IsStopped FROM Case WHERE id IN :(MapCase.keySet())];
		//List<Case> newListCase = [SELECT id,RecordTypeId,IsStopped FROM Case WHERE id IN :(MapCase.keySet())];
		system.debug('newListCase: ' + newListCase);

		for (Case cs : newListCase) {
			System.debug('CST_End2End_Owner__c: ' + cs.CST_End2End_Owner__c);

			Case nCase = MapCase.get(cs.id);
			nCase.CST_End2End_Owner__c = cs.CST_End2End_Owner__c;
			nCase.id = cs.id;
		}

		CST_EntitlementsHandler.checkMilestoneClockOLA(newListCase, MapCase);
	}

	public override void afterUpdate() {
		init();
	}

	private void validateStatusChanged() {
		Set<Id> statusChangedIds = new Set<Id>();
		Map<Id, String> mapStatusbyId = new Map<Id, String>();
		for (CST_External_Ticket__c et : newExternalSystems) {
			if (et.CST_Status__c != oldExternalSystemMap.get(et.Id).CST_Status__c && oldExternalSystemMap.get(et.Id).CST_Status__c != 'Draft') {
				statusChangedIds.add(et.CST_Case__c);
				mapStatusbyId.put(et.CST_Case__c, et.CST_Status__c);
			}
		}

		if (statusChangedIds.size() > 0) {
			futureUpdateStatus_InRemedy(statusChangedIds, JSON.serialize(mapStatusbyId));
		}
	}

	// to when we have real communication with Layer 7 System
	@AuraEnabled
	public static String submitExternalTicket(String ticketid) {
		Response resp = new Response();
		resp.error = '';
		resp.message = 'Ticket Updated';
		resp.ticketid = ticketid;

		return JSON.serialize(resp);
	}

	@AuraEnabled
	public static List<CST_Questionnaire_Line_Item__c> getQuestions(ID caseId) {
		List<CST_Questionnaire_Line_Item__c> Quest4Remedy = [
			SELECT Id, name, CST_Question__c, CST_Answer__c
			FROM CST_Questionnaire_Line_Item__c
			WHERE CST_Questionnaire__c = :caseId OR CST_Case__c = :caseId
			ORDER BY name ASC
		];

		system.debug(Quest4Remedy);
		return Quest4Remedy;
	}
	@AuraEnabled
	public static String getFiles(String ids) {
		System.debug('getFiles --> recordid ' + ids);

		System.debug('validateFiles ids ' + ids);
		List<String> idList = (List<String>) JSON.deserialize(ids, List<String>.class);

		// Map<Id, String> mapStatus = (Map<Id, String>)JSON.deserialize(mapStatusByIds, Map<Id, String>.class);

		System.debug('validateFiles idList ' + idList);

		List<ContentDocumentLink> contDocLinks = [
			SELECT ContentDocumentId, LinkedEntityId, ContentDocument.Title, id, ContentDocument.ContentSize
			FROM ContentDocumentLink
			WHERE ContentDocumentId IN :idList
		];
		System.debug('getFiles contDocLinks ' + contDocLinks);

		Map<String, uploadInfo> fileMapOutput = new Map<String, uploadInfo>();

		Map<String, ContentDocumentLink> contDocLinksMap = new Map<String, ContentDocumentLink>();
		for (ContentDocumentLink cdl : contDocLinks) {
			contDocLinksMap.put(cdl.ContentDocumentId, cdl);
		}

		System.debug('getFiles contDocLinksMap ' + contDocLinksMap);

		String warning;
		String msg;
		Integer fSize;
		Response response = new Response();

		for (ContentDocumentLink linkDoc : contDocLinksMap.values()) {
			uploadInfo fi = new uploadInfo();

			fSize = linkDoc.ContentDocument.ContentSize;

			response.status = 'success';

			fi.status = 'smallFile';
			fi.title = linkDoc.ContentDocument.Title;
			fi.id = linkDoc.id;
			fi.contentdocumentid = linkDoc.contentdocumentid;
			fi.linkedentityid = linkDoc.LinkedEntityId;
			fi.fileSize = fSize;

			fileMapOutput.put(fi.ContentDocumentId, fi);
		}
		System.debug('getFiles fileMapOutput ' + fileMapOutput);

		response.files = fileMapOutput.values();

		String respString = JSON.serialize(response);
		return respString;
	}

	@AuraEnabled
	public static String deleteFiles(String recordid) {
		Response response = new Response();
		try {
			System.debug('deleteFiles recordid ' + recordid);

			List<ContentVersion> contentVersions = [
				SELECT Id, Title, ContentDocumentId
				FROM ContentVersion
				WHERE ContentDocumentId = :recordid
				LIMIT 1
			];

			System.debug('contentVersions ' + contentVersions);

			List<ContentDocumentLink> links = [
				SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility
				FROM ContentDocumentLink
				WHERE contentdocumentid = :contentVersions[0].ContentDocumentId
			];
			System.debug('links: ' + links);

			String entityId = links[0].LinkedEntityId;
			System.debug('entityId ' + entityId);

			List<String> linkIds = new List<String>();
			for (ContentDocumentLink cds : links) {
				linkIds.add(cds.contentdocumentid);
			}
			System.debug('linkIds: ' + linkIds);

			List<ContentDocument> cds = [SELECT Id, Title, ParentId, FileType, ContentAssetId FROM ContentDocument WHERE id IN :(linkIds)];
			System.debug('cds ' + cds);

			response.error = '';
			response.status = 'success';
			delete cds;
		} catch (Exception ex) {
			response.error = '';
			response.status = 'error';
			String template = 'error: {0} detail: {1}';
			response.message = String.format(template, new List<String>{ ex.getMessage(), ex.getStackTraceString() });

			System.debug(ex.getMessage() + ' -> ' + ex.getStackTraceString());
			// throw new AuraHandledException(ex.getMessage());
		}

		String strResponse = JSON.serialize(response);
		System.debug('strResponse ' + strResponse);
		return JSON.serialize(strResponse);
	}

	@AuraEnabled
	public static String submitTicketToRemedy(Id caseid) {
		// deploy CST_LeveL1__c >>
		Map<String, CSTRemedyData__c> mapRemedyCustomSettings = new Map<String, CSTRemedyData__c>();
		Case remedyCase = [
			SELECT
				id,
				casenumber,
				Subject,
				status,
				Account.name,
				Priority,
				CST_LeveL1__c,
				CST_Sub_Status__c,
				CST_End2End_Owner__c,
				CST_End2End_Owner__r.name,
				CST_End2End_Owner__r.Alias,
				Reason,
				OwnerId,
				CST_Case_is_with_Team__c,
				ContactId,
				Contact.Name,
				Contact.Phone,
				Contact.MobilePhone,
				Contact.Email
			FROM Case
			WHERE id = :caseId
		];
		System.debug('createTicketInRemedy remedyCase -> ' + remedyCase);

		Response resp = new Response();

		try {
			system.debug('submitTicketToRemedy');
			system.debug('remedyCase ' + remedyCase);

			for (CSTRemedyData__c rem : [SELECT Name, Case_Priority__c, Remedy_Priority__c, ExtSupportGroup__c FROM CSTRemedyData__c]) {
				//remedyPriority = rem.Remedy_Priority__c;
				if (rem.Case_Priority__c != null) {
					mapRemedyCustomSettings.put(rem.Name + '-' + rem.Case_Priority__c, rem);
				} else {
					mapRemedyCustomSettings.put(rem.Name, rem);
				}
			}

			String remedyTicketMessage = buildCreateRemedyXML(remedyCase, mapRemedyCustomSettings);
			system.debug('Built XML String is ' + remedyTicketMessage);
			CST_External_Ticket__c remedyTicket = createRemedyTicketInSDFC(null, remedyCase, remedyTicketMessage, mapRemedyCustomSettings);

			system.debug('remedyTicket: ' + remedyTicket);
			system.debug('remedyTicketMessage: ' + remedyTicketMessage);
			system.debug('remedyCase: ' + remedyCase);

			// deploy
			sendTicketToRemedy(remedyTicket.Id, remedyTicketMessage, remedyCase.CST_level1__c);
			//return remedyTicketNumber;

			resp.error = '';
			resp.message = 'Ticket created';
			resp.ticketid = remedyTicket.Id;

			String jResp = JSON.serialize(resp);
			System.debug('resp ' + resp);
			return jResp;
			//*
		} catch (Exception e) {
			resp.error = 'error';
			resp.message = 'Ticket not created';
			resp.ticketid = '';

			//handle exception - what to do? logs to store?
			System.debug('Error: ' + e.getMessage());
			return JSON.serialize(resp);
		}
		//*/

		// deploy CST_LeveL1__c >> CST_level1__c
	}

	@future(callout=true)
	public static void sendTicketToRemedy(Id remedyticketid, String remedyTicketMessage, String remedyCaseSubType) {
		CST_External_Ticket__c remedyTicket = [
			SELECT id, Name, CST_Status__c, CST_ErrorMessage__c
			FROM CST_External_Ticket__c
			WHERE id = :remedyticketid
		];
		try {
			//to remove after we have a real http connection
			if (remedyTicket.CST_Status__c == 'Draft') {
				remedyTicket.CST_Status__c = 'Assigned';
			}

			Http http = new Http();
			HttpRequest request = new HttpRequest();
			//request.setEndpoint('callout:' + REMEDY_NAMED_CREDENTIAL + REMEDY_URL);
			request.setEndpoint('https://sgw.heerlen.nl.sit.vodafoneziggo.com:443/v1/Remedy/arsys/WSDL/public/remedyapptest/VZ_GenericIncidentAPI');
			request.setMethod('POST');
			request.setBody(remedyTicketMessage);

			Map<String, String> keyValueHeadersMap = new Map<String, String>();

			String username = 'salesforce_oa_arn-dmz_01'; //not to use here, to test endpoint alternative
			String password = 'jf9VuBHlsrn8@7pN,)GKAm';
			Blob headerValue = Blob.valueOf(username + ':' + password);
			String authorizationHeader = 'BASIC ' + EncodingUtil.base64Encode(headerValue);

			keyValueHeadersMap.put('Content-Type', 'application/soap+xml');
			keyValueHeadersMap.put('Authorization', authorizationHeader);
			keyValueHeadersMap.put('Accept-Encoding', 'gzip;deflate');
			for (String key : keyValueHeadersMap.keySet()) {
				request.setHeader(key, keyValueHeadersMap.get(key));
			}
			request.setTimeout(20000);

			//WHEN ENDPOINT IS WORKING PROPERLY, UNCOMMENT THIS SECTION AND COMMENT THE PACKETLOSS IF ELSE STATEMENT

			/*
            HttpResponse response = new HttpResponse();
            system.debug('request -> '+request);

            //when communication is established!
            response = http.send(request);
            system.debug('response -> '+response);

            if(response.getStatusCode() == 200){ //simulating Success response
                System.debug('Body: ' + response.getBody());
                remedyTicket.CST_Status__c='Assigned';
            }
            else { //what to do when fail?
                System.debug('Response: ' + response.getBody());
                remedyTicket.CST_Status__c='Integration Error';
                remedyTicket.CST_ErrorMessage__c = 'something wrong.';
            }*/
			//update remedyTicket;
		} catch (Exception e) {
			//handle exception - what to do? logs to store?
			System.debug('Error: ' + e.getMessage());
			remedyTicket.CST_Status__c = 'Integration Error';
			remedyTicket.CST_ErrorMessage__c = e.getMessage();
		}
		/*
        if(remedyCaseSubType != 'Packetloss'){
            remedyTicket.CST_Status__c='Assigned';
        }
        else{
            remedyTicket.CST_Status__c='Integration Error';
            remedyTicket.CST_ErrorMessage__c = 'There is no Packetloss as a Subtype in Remedy System.';
        }
        */
		update remedyTicket;
	}

	public static String buildCreateRemedyXML(Case remedyCase, Map<String, CSTRemedyData__c> mapRemedyCustomSettings) {
		List<CST_Questionnaire_Line_Item__c> lineItems = [
			SELECT id, CST_Question__c, CST_Answer__c
			FROM CST_Questionnaire_Line_Item__c
			WHERE CST_Case__c = :remedyCase.Id
		];

		DOM.Document doc = new DOM.Document();
		String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String urnNamespace = 'urn:VZ_CreateIncidentAPI';

		dom.XmlNode envelopeTag = doc.createRootElement('Envelope', soapNS, 'soapenv');
		envelopeTag.setNamespace('urn', urnNamespace);
		dom.XmlNode body = envelopeTag.addChildElement('Body', soapNS, 'soapenv');
		dom.XmlNode remedyTag = body.addChildElement('CreateIncident', soapNS, 'urn');
		remedyTag.addChildElement('Action', soapNS, 'urn').addTextNode('CREATE');
		remedyTag.addChildElement('SourceSystem', soapNS, 'urn').addTextNode('VodafoneZiggo ITSM');
		remedyTag.addChildElement('ExternalSystem', soapNS, 'urn').addTextNode('CST');
		remedyTag.addChildElement('ExternalTicketNumber', soapNS, 'urn').addTextNode((String) remedyCase.casenumber);

		System.debug('ExternalSupportGroup get ' + mapRemedyCustomSettings.get('ExternalSupportGroup'));
		System.debug('ExternalSupportGroup containsKey ' + mapRemedyCustomSettings.containsKey('ExternalSupportGroup'));

		if (mapRemedyCustomSettings.containsKey('ExternalSupportGroup')) {
			System.debug('ExtSupportGroup__c ' + (String) mapRemedyCustomSettings.get('ExternalSupportGroup').ExtSupportGroup__c);
			remedyTag.addChildElement('ExternalSupportGroup', soapNS, 'urn')
				.addTextNode((String) mapRemedyCustomSettings.get('ExternalSupportGroup').ExtSupportGroup__c);
		} else {
			remedyTag.addChildElement('ExternalSupportGroup', soapNS, 'urn').addTextNode('');
		}

		remedyTag.addChildElement('Customer', soapNS, 'urn').addTextNode((String) remedyCase.CST_End2End_Owner__r.name);

		//remedyTag.addChildElement('ExternalPriority', soapNS, 'urn').addTextNode((String)remedyCase.Priority);
		string extPriorityKey = 'ExternalPriority-' + remedyCase.Priority;
		String remedyPriority = mapRemedyCustomSettings.get(extPriorityKey) != null
			? mapRemedyCustomSettings.get(extPriorityKey).Remedy_Priority__c
			: 'P4';

		remedyTag.addChildElement('ExternalPriority', soapNS, 'urn').addTextNode((String) remedyPriority);
		remedyTag.addChildElement('Summary', soapNS, 'urn').addTextNode((String) remedyCase.Subject);
		remedyTag.addChildElement('ExternalService', soapNS, 'urn').addTextNode('TBD');
		dom.XmlNode detailedDescriptionTag = remedyTag.addChildElement('DetailedDescription', soapNS, 'urn');
		detailedDescriptionTag.addChildElement('ServiceId', soapNS, 'urn').addTextNode('ServiceId here...');
		detailedDescriptionTag.addChildElement('AccessId', soapNS, 'urn').addTextNode('AccessId here...');
		detailedDescriptionTag.addChildElement('Account', soapNS, 'urn').addTextNode((String) remedyCase.Account.name);
		dom.XmlNode customerInformationTag = detailedDescriptionTag.addChildElement('CustomerInformation', soapNS, 'urn');
		customerInformationTag.addChildElement('Contact', soapNS, 'urn')
			.addTextNode(String.isNotEmpty((String) remedyCase.Contact.Name) ? (String) remedyCase.Contact.Name : '');
		customerInformationTag.addChildElement('ContactPhone', soapNS, 'urn')
			.addTextNode(String.isNotEmpty((String) remedyCase.Contact.Phone) ? (String) remedyCase.Contact.Phone : '');
		customerInformationTag.addChildElement('ContactEmail', soapNS, 'urn')
			.addTextNode(String.isNotEmpty((String) remedyCase.Contact.Email) ? (String) remedyCase.Contact.Email : '');
		customerInformationTag.addChildElement('ContactMobile', soapNS, 'urn')
			.addTextNode(String.isNotEmpty((String) remedyCase.Contact.MobilePhone) ? (String) remedyCase.Contact.MobilePhone : '');
		dom.XmlNode questionnaireTag = detailedDescriptionTag.addChildElement('Questionnaire', soapNS, 'urn');

		System.debug('lineItems ' + lineItems);

		for (CST_Questionnaire_Line_Item__c lineItem : lineItems) {
			System.debug('lineItem ' + lineItem);
			System.debug('lineItem ' + lineItem.CST_Question__c);
			System.debug('lineItem ' + lineItem.CST_Answer__c);

			dom.XmlNode pairQuestionnaire = questionnaireTag.addChildElement('QuestionLine', soapNS, 'urn');
			pairQuestionnaire.addChildElement('Question', soapNS, 'urn')
				.addTextNode(String.isNotEmpty((String) lineItem.CST_Question__c) ? (String) lineItem.CST_Question__c : '');
			pairQuestionnaire.addChildElement('Question', soapNS, 'urn')
				.addTextNode(String.isNotEmpty((String) lineItem.CST_Answer__c) ? (String) lineItem.CST_Answer__c : '');
		}

		System.debug('doc -> ' + doc.toXmlString());

		return doc.toXmlString();
	}

	public static CST_External_Ticket__c createRemedyTicketInSDFC(
		String body,
		Case remedyCase,
		String requestMessage,
		Map<String, CSTRemedyData__c> mapRemedyCustomSettings
	) {
		system.debug('createRemedyTicketInSDFC body: ' + body);
		system.debug('createRemedyTicketInSDFC remedyCase: ' + remedyCase);
		system.debug('createRemedyTicketInSDFC requestMessage: ' + requestMessage);
		system.debug('createRemedyTicketInSDFC mapRemedyCustomSettings: ' + mapRemedyCustomSettings);

		//read body and retrieve Remedy Incident number
		User integrationUser = [SELECT id, username FROM User WHERE alias = 'integ' LIMIT 1]; //to change once we have final Integration User
		CST_External_Ticket__c newRemedyTicket = new CST_External_Ticket__c();
		List<CST_External_Ticket__c> remedyticketInCase = [SELECT id FROM CST_External_Ticket__c WHERE CST_Case__c = :remedyCase.id];

		if (body != null && body != '') {
			//read body and retrieve Remedy Incident number
		} else {
			newRemedyTicket.CST_IncidentID__c = '101' + remedyCase.casenumber + '-' + (remedyticketInCase.size() + 1);
		}

		System.debug('newRemedyTicket.CST_IncidentID__c -> ' + newRemedyTicket.CST_IncidentID__c);
		newRemedyTicket.CST_Request_Message__c = requestMessage;
		newRemedyTicket.CST_Case__c = remedyCase.Id;
		newRemedyTicket.CST_Status__c = 'Draft';
		newRemedyTicket.CST_Summary__c = remedyCase.Subject;
		string extPriorityKey = 'ExternalPriority-' + remedyCase.Priority;
		if (mapRemedyCustomSettings.get(extPriorityKey) != null) {
			newRemedyTicket.CST_Ticket_Priority__c = mapRemedyCustomSettings.get(extPriorityKey).Remedy_Priority__c;
		} else {
			newRemedyTicket.CST_Ticket_Priority__c = 'P4';
		}

		//newRemedyTicket.CST_Ticket_Priority__c = r

		//newRemedyTicket.CST_IncidentID__c = '101'+remedyCase.casenumber+'-'+remedyticketInCase.size();
		//newRemedyTicket.OwnerId = remedyCase.CST_End2End_Owner__c;

		System.debug('newRemedyTicket before: ' + newRemedyTicket);

		insert newRemedyTicket;

		System.debug('newRemedyTicket after: ' + newRemedyTicket);

		remedyCase.OwnerId = integrationUser.id;
		remedyCase.CST_Sub_Status__c = 'Assigned';
		remedyCase.CST_Case_is_with_Team__c = 'External - Remedy';
		remedyCase.CST_RemedyTicketCreated__c = true;
		remedyCase.Remedy_Ticket_No__c = newRemedyTicket.CST_IncidentID__c;
		update remedyCase;

		System.debug('remedyCase to be updated... -> ' + remedyCase);

		//return newRemedyTicket.CST_IncidentID__c;
		return newRemedyTicket;
	}

	@future(callout=true)
	public static void futureUpdateEnd2EndOwner_InRemedy(Set<ID> caseIds) {
		sendUpdatesToRemedy(caseIds, true, null);
	}

	@future(callout=true)
	public static void futureUpdateStatus_InRemedy(Set<Id> caseIds, String mapStatusByIds) {
		Map<Id, String> mapStatus = (Map<Id, String>) JSON.deserialize(mapStatusByIds, Map<Id, String>.class);
		System.debug('mapStatus -> ' + mapStatus);
		sendUpdatesToRemedy(caseIds, false, mapStatus);
	}

	private static void sendUpdatesToRemedy(Set<Id> caseIds, Boolean ownerChanged, Map<Id, String> mapStatus) {
		List<Case> remedyCaseList = [SELECT id, casenumber, CST_End2End_Owner__r.name, Remedy_Ticket_No__c FROM Case WHERE id IN :caseIds];
		List<CST_External_Ticket__c> tickets2update = new List<CST_External_Ticket__c>();

		for (Case remedyCase : remedyCaseList) {
			String status = mapStatus != null ? mapStatus.get(remedyCase.Id) : '';
			System.debug('status -> ' + status);
			tickets2update.add(buildXMLMessage_Update(remedyCase, ownerChanged, status));
		}

		upsert tickets2update CST_IncidentID__c;
	}

	private static CST_External_Ticket__c buildXMLMessage_Update(Case remedyCase, Boolean ownerChanged, String status) {
		CST_External_Ticket__c remedyTicket = new CST_External_Ticket__c(
			CST_IncidentID__c = remedyCase.Remedy_Ticket_No__c,
			CST_Case__c = remedyCase.Id
		);
		DOM.Document doc = new DOM.Document();
		String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
		String urnNamespace = 'urn:VZ_GenericIncidentAPI';

		dom.XmlNode envelopeTag = doc.createRootElement('Envelope', soapNS, 'soapenv');
		envelopeTag.setNamespace('urn', urnNamespace);
		dom.XmlNode body = envelopeTag.addChildElement('Body', soapNS, 'soapenv');
		dom.XmlNode remedyTag = body.addChildElement('UpdateIncident', soapNS, 'urn');
		remedyTag.addChildElement('SourceSystem', soapNS, 'urn').addTextNode('VodafoneZiggo ITSM');
		remedyTag.addChildElement('ExternalSystem', soapNS, 'urn').addTextNode('CST');
		remedyTag.addChildElement('RemedyTicketNumber', soapNS, 'urn').addTextNode((String) remedyCase.Remedy_Ticket_No__c);
		remedyTag.addChildElement('ExternalTicketNumber', soapNS, 'urn').addTextNode((String) remedyCase.casenumber);
		remedyTag.addChildElement('Action', soapNS, 'urn').addTextNode('UPDATE');

		if (ownerChanged) {
			remedyTag.addChildElement('ExternalOwner', soapNS, 'urn').addTextNode((String) remedyCase.CST_End2End_Owner__r.name);
		}
		if (String.isNotBlank(status)) {
			remedyTag.addChildElement('ExternalStatus', soapNS, 'urn').addTextNode((String) status);
		}

		String xmlRequest = doc.toXmlString();
		System.debug('xmlRequest -> ' + xmlRequest);
		remedyTicket.CST_Request_Message__c = xmlRequest;

		return remedyTicket;
	}
}
