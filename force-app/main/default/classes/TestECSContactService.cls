@isTest
private class TestECSContactService {
	@TestSetup
	static void makeData() {
		TestUtils.autoCommit = true;
		User owner = TestUtils.createAdministrator();
		Account acct = TestUtils.createAccount(owner);
		External_Account__c externalAcc = new External_Account__c();
		externalAcc.Account__c = acct.Id;
		externalAcc.External_Account_Id__c = 'AFD';
		externalAcc.External_Source__c = 'BOP';
		insert externalAcc;
		Ban__c ban = TestUtils.createBan(acct);
		ban.BOP_export_datetime__c = System.now();
		update ban;
		acct.BAN_Number__c = '346474';
		update acct;
		Contact cont = TestUtils.createContact(acct);
		cont.Email = 'test@gmail.com';
		update cont;
		TestUtils.autoCommit = false;
		Site__c site = TestUtils.createSite(acct);
		site.Last_dsl_check__c = System.today().addDays(-1);
		insert site;
		Contact_Role__c contRole = new Contact_Role__c();
		contRole.Account__c = acct.Id;
		contRole.Contact__c = cont.Id;
		contRole.Site__c = site.Id;
		contRole.Active__c = true;
		insert contRole;
	}

	@isTest
	static void testMakeCreateRequest() {
		Contact_Role__c theContactRole = [
			SELECT
				Id,
				Account__c,
				Account__r.BAN_Number__c,
				Account__r.BOPCode__c,
				Contact__r.AccountId,
				Contact__r.Account.BAN_Number__c,
				Contact__r.Account.BOPCode__c,
				Contact__r.BOP_reference__c,
				Contact__r.Email,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Account__c,
				Site__r.Site_Account__r.BAN_Number__c,
				Site__r.Site_Account__r.BOPCode__c,
				Type__c,
				BOP_export_Errormessage__c,
				BOP_export_datetime__c
			FROM Contact_Role__c
			WHERE Contact__r.Email = 'test@gmail.com'
			LIMIT 1
		];
		ECSContactService contactService = new ECSContactService();
		List<ECSContactService.request> requests = new List<ECSContactService.request>();
		ECSContactService.request req = new ECSContactService.request();

		req.email = theContactRole.Contact__r.Email;
		req.companyBanNumber = theContactRole.Account__r.BAN_Number__c;
		req.companyCorporateId = 'not Used'; //not used in ContactExport when Using ECSContactService
		req.companyBopCode = theContactRole.Account__r.BOPCode__c;
		req.houseNumber = String.valueOf(theContactRole.Site__r.Site_House_Number__c);
		req.houseNumberExtension = theContactRole.Site__r.Site_House_Number_Suffix__c;
		req.zipCode = theContactRole.Site__r.Site_Postal_Code__c;
		req.type = theContactRole.Type__c;
		req.service = 'not used???'; //not used in ContactExport when Using ECSContactService

		requests.add(req);
		Test.setMock(WebServiceMock.class, new ECSSOAPUserMocks.CreateContactResponse());
		Test.startTest();
		contactService.setRequest(requests);
		contactService.makeRequest('create');
		List<ECSContactService.response> responseList = contactService.getResponse();
		System.assertEquals('NGL', responseList[0].bopCode, 'Bop Code returned: ' + responseList[0].bopCode);
		Test.stopTest();
	}

	@isTest
	static void testMakeDeleteRequest() {
		Contact_Role__c theContactRole = [
			SELECT
				Id,
				Account__c,
				Account__r.BAN_Number__c,
				Account__r.BOPCode__c,
				Contact__r.AccountId,
				Contact__r.Account.BAN_Number__c,
				Contact__r.Account.BOPCode__c,
				Contact__r.BOP_reference__c,
				Contact__r.Email,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Account__c,
				Site__r.Site_Account__r.BAN_Number__c,
				Site__r.Site_Account__r.BOPCode__c,
				Type__c,
				BOP_export_Errormessage__c,
				BOP_export_datetime__c
			FROM Contact_Role__c
			WHERE Contact__r.Email = 'test@gmail.com'
			LIMIT 1
		];
		ECSContactService contactService = new ECSContactService();
		List<ECSContactService.request> requests = new List<ECSContactService.request>();
		ECSContactService.request req = new ECSContactService.request();

		req.email = theContactRole.Contact__r.Email;
		req.companyBanNumber = theContactRole.Account__r.BAN_Number__c;
		req.companyCorporateId = 'not Used'; //not used in ContactExport when Using ECSContactService
		req.companyBopCode = theContactRole.Account__r.BOPCode__c;
		req.houseNumber = String.valueOf(theContactRole.Site__r.Site_House_Number__c);
		req.houseNumberExtension = theContactRole.Site__r.Site_House_Number_Suffix__c;
		req.zipCode = theContactRole.Site__r.Site_Postal_Code__c;
		req.type = theContactRole.Type__c;
		req.service = 'not used???'; //not used in ContactExport when Using ECSContactService

		requests.add(req);
		Test.setMock(WebServiceMock.class, new ECSSOAPUserMocks.DeleteContactResponse());
		Test.startTest();
		contactService.setRequest(requests);
		contactService.makeRequest('delete');
		List<ECSContactService.response> responseList = contactService.getResponse();
		System.assertEquals('NGI', responseList[0].bopCode, 'Bop Code returned: ' + responseList[0].bopCode);
		Test.stopTest();
	}

	@isTest
	public static void testExceptions() {
		Test.startTest();
		ECSContactService contactService = new ECSContactService();
		List<ECSContactService.request> requests = new List<ECSContactService.request>();
		ECSContactService.request req = new ECSContactService.request();
		requests.add(req);
		contactService.setRequest(requests);
		try {
			//remove items from list
			requests.clear();
			contactService.setRequest(requests);
			contactService.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		try {
			//remove items from list
			requests = null;
			contactService.setRequest(requests);
			contactService.checkExceptions();
		} catch (Exception e) {
			System.assertEquals(true, e.getMessage().contains('A request has not been set'), 'when there is no items in list');
		}
		Test.stopTest();
	}

	@isTest
	public static void testAdditionalErrors() {
		Contact_Role__c theContactRole = [
			SELECT
				Id,
				Account__c,
				Account__r.BAN_Number__c,
				Account__r.BOPCode__c,
				Contact__r.AccountId,
				Contact__r.Account.BAN_Number__c,
				Contact__r.Account.BOPCode__c,
				Contact__r.BOP_reference__c,
				Contact__r.Email,
				Site__r.Site_Postal_Code__c,
				Site__r.Site_House_Number__c,
				Site__r.Site_House_Number_Suffix__c,
				Site__r.Site_Account__c,
				Site__r.Site_Account__r.BAN_Number__c,
				Site__r.Site_Account__r.BOPCode__c,
				Type__c,
				BOP_export_Errormessage__c,
				BOP_export_datetime__c
			FROM Contact_Role__c
			WHERE Contact__r.Email = 'test@gmail.com'
			LIMIT 1
		];
		ECSContactService contactService = new ECSContactService();
		List<ECSContactService.request> requests = new List<ECSContactService.request>();
		ECSContactService.request req = new ECSContactService.request();

		req.email = theContactRole.Contact__r.Email;
		req.companyBanNumber = theContactRole.Account__r.BAN_Number__c;
		req.companyCorporateId = 'not Used'; //not used in ContactExport when Using ECSContactService
		req.companyBopCode = theContactRole.Account__r.BOPCode__c;
		req.houseNumber = String.valueOf(theContactRole.Site__r.Site_House_Number__c);
		req.houseNumberExtension = theContactRole.Site__r.Site_House_Number_Suffix__c;
		req.zipCode = theContactRole.Site__r.Site_Postal_Code__c;
		req.type = theContactRole.Type__c;
		req.service = 'not used???'; //not used in ContactExport when Using ECSContactService

		requests.add(req);
		contactService.setRequest(requests);
		Test.startTest();
		try {
			contactService.makeRequest('create');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to create the contact(s): '),
				'when there is no mock Create'
			);
		}
		try {
			contactService.makeRequest('delete');
		} catch (Exception e) {
			System.assertEquals(
				true,
				e.getMessage().contains('Error received from BOP when attempting to delete the contact(s): '),
				'when there is no mock delete'
			);
		}
		Test.stopTest();
	}
}
