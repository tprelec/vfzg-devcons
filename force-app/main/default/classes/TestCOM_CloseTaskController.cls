@IsTest
private class TestCOM_CloseTaskController {
	@IsTest
	static void testBehaviorInstallDeliveryOrder() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
			Task task1 = CS_DataTest.createTask('Install DeliveryOrder', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Installation';
			insert task1;

			try {
				Test.startTest();
				COM_CloseTaskController.closeTask(task1.Id);
				Test.stopTest();
			} catch (Exception e) {
				System.assertEquals(e.getMessage(), COM_Constants.TEST_VALIDATION_MSG, 'Message is not the same.');
			}
		}
	}

	@IsTest
	static void testBehaviorInstallDeliveryOrder1() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
			Task task1 = CS_DataTest.createTask('Install DeliveryOrder', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Installation Jeopardy';
			insert task1;

			try {
				Test.startTest();
				COM_CloseTaskController.closeTask(task1.Id);
				Test.stopTest();
			} catch (Exception e) {
				System.assertEquals(e.getMessage(), COM_Constants.TEST_VALIDATION_MSG, 'Message is not the same.');
			}
		}
	}

	@IsTest
	static void testBehaviorInstallDeliveryOrderJeopardy() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
			insert ord;

			COM_Delivery_Order__c deliveryOrder1 = CS_DataTest.createDeliveryOrder('DeliveryOrder 1', ord.Id, true);

			Task installDeliveryOrderTask = CS_DataTest.createTask('Install DeliveryOrder', 'COM Delivery', simpleUser, false);
			installDeliveryOrderTask.Type = 'COM_Installation';
			installDeliveryOrderTask.COM_Delivery_Order__c = deliveryOrder1.Id;
			installDeliveryOrderTask.Status = 'Closed';
			insert installDeliveryOrderTask;

			Task task1 = CS_DataTest.createTask('Install DeliveryOrder', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Installation Jeopardy';
			task1.COM_Delivery_Order__c = deliveryOrder1.Id;
			insert task1;

			Boolean stepPassed;

			try {
				Test.startTest();
				COM_CloseTaskController.closeTask(task1.Id);
				stepPassed = true;
				Test.stopTest();
			} catch (Exception e) {
				stepPassed = false;
			}

			System.assert(true, stepPassed);
		}
	}

	@IsTest
	static void testBehaviourPMProjectStartSuccess() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
			insert ord;

			COM_Delivery_Order__c parentDelOrd = CS_DataTest.createDeliveryOrder('Parent DeliveryOrder', ord.Id, true);

			List<COM_Delivery_Order__c> deliveryOrders = CS_DataTest.createMultipleDeliveryOrders(
				'DeliveryOrder 1',
				ord.Id,
				parentDelOrd.Id,
				5,
				false
			);
			insert deliveryOrders;

			Task task1 = CS_DataTest.createTask('Delivery Project Start', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Prepare Project';
			task1.COM_Delivery_Order__c = parentDelOrd.Id;
			insert task1;

			task1.OwnerId = simpleUser.Id;
			update task1;

			Test.startTest();
			String result = COM_CloseTaskController.closeTask(task1.Id);
			Test.stopTest();

			task1 = [SELECT Id, Status, OwnerId FROM Task WHERE Id = :task1.Id];
			System.assertEquals('Task Closed', result, 'Message is not the same.');
			System.assertEquals('Completed', task1.Status, 'Task should be Completed');
		}
	}

	@IsTest
	static void testBehaviorInstallDeliveryOrder_NoImplementedDate() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
			insert ord;

			COM_Delivery_Order__c parentDelOrd = CS_DataTest.createDeliveryOrder('Parent DeliveryOrder', ord.Id, true);

			csord__Subscription__c sub = new csord__Subscription__c();
			sub.csord__Identification__c = 'TestSub_CloseTask';
			insert sub;

			csord__Service__c service = new csord__Service__c();
			service.csord__Identification__c = 'TestServ_CloseTask';
			service.csord__Subscription__c = sub.Id;
			service.COM_Delivery_Order__c = parentDelOrd.Id;
			service.Has_Delivery_Components__c = true;
			insert service;

			Task task1 = CS_DataTest.createTask('Delivery Project Start', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Installation';
			task1.COM_Delivery_Order__c = parentDelOrd.Id;
			insert task1;

			try {
				Test.startTest();
				COM_CloseTaskController.closeTaskForDeliveryOrderInstallation(task1);
				Test.stopTest();
			} catch (Exception e) {
				System.assertEquals(COM_Constants.TEST_VALIDATION_2_MSG, e.getMessage(), 'Message is not the same.');
			}
		}
	}

	@IsTest
	static void testBehaviorInstallDeliveryOrder_OnHold() {
		User simpleUser = CS_DataTest.createSystemAdministratorUser();

		System.runAs(simpleUser) {
			csord__Order__c ord = new csord__Order__c(Name = 'Order 1', csord__Identification__c = 'ID_4568978');
			insert ord;

			COM_Delivery_Order__c parentDelOrd = CS_DataTest.createDeliveryOrder('Parent DeliveryOrder', ord.Id, false);
			insert parentDelOrd;

			Task task1 = CS_DataTest.createTask('Install DeliveryOrder', 'COM Delivery', simpleUser, false);
			task1.Type = 'COM_Installation';
			task1.COM_Delivery_Order__c = parentDelOrd.Id;
			insert task1;

			parentDelOrd.On_Hold__c = true;
			update parentDelOrd;

			try {
				Test.startTest();
				COM_CloseTaskController.closeTask(task1.Id);
				Test.stopTest();
			} catch (Exception e) {
				System.assertEquals(COM_Constants.NO_CHANGES_DURING_INFLIGHT_ERROR_MSG, e.getMessage(), 'Message is not the same.');
			}
		}
	}
}
