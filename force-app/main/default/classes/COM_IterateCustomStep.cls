global without sharing class COM_IterateCustomStep implements CSPOFA.ExecutionHandler{

    global List<SObject> process(List<SObject> steps) {
        List<SObject> result = new List<sObject>();
        Map<Id, CSPOFA__Orchestration_Step__c> processStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();
        Map<Id, CSPOFA__Orchestration_Step__c> solutionStepMap = new Map<Id, CSPOFA__Orchestration_Step__c>();

        List<CSPOFA__Orchestration_Step__c> stepList = [SELECT ID,
                Name,
                CSPOFA__Orchestration_Process__c,
                CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c,
                CSPOFA__Orchestration_Process__r.Suborder__c,
                CSPOFA__Status__c,
                CSPOFA__Completed_Date__c,
                CSPOFA__Message__c,
                CSPOFA__Expression_Result__c,
                CSPOFA__Related_Object__c,
                CSPOFA__Related_Object_ID__c,
                COM_Case_Status__c
        FROM CSPOFA__Orchestration_Step__c
        WHERE Id IN :steps];

        try {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                processStepMap.put(step.CSPOFA__Orchestration_Process__c, step);
                solutionStepMap.put(step.CSPOFA__Orchestration_Process__r.Suborder__c, step);
            }

            Map<Id, CSPOFA__Orchestration_Process__c> processesMap = new Map<Id, CSPOFA__Orchestration_Process__c>([
                    SELECT Id
                    FROM CSPOFA__Orchestration_Process__c
                    WHERE Id IN :processStepMap.keySet()
            ]);

            kickOffSubprocess(stepList);

            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = 'Waiting'; //Constants.ORCHESTRATOR_STEP_COMPLETE;
                step.CSPOFA__Completed_Date__c = Date.today();
                step.CSPOFA__Message__c = 'Custom step succeeded';
                result.add(step);
                Attachment attachment = new Attachment();
                attachment.Body = Blob.valueOf('testtxt');
                attachment.Name = String.valueOf('test.txt');
                attachment.ParentId = step.Id; 
                insert attachment;
            }
        } catch (Exception ex) {
            for (CSPOFA__Orchestration_Step__c step : stepList) {
                step.CSPOFA__Status__c = Constants.ORCHESTRATOR_STEP_ERROR;
                step.CSPOFA__Message__c = 'Error occurred while executing the step: ' + ex.getMessage() + ' on line ' + ex.getLineNumber();
                if(step.CSPOFA__Message__c.length() > 255 ){
                    step.CSPOFA__Message__c = step.CSPOFA__Message__c.substring(0, 254);
                }
                result.add(step);
            }
        }
        return result;
    }

    private void kickOffSubprocess(List<CSPOFA__Orchestration_Step__c> stepList){
        Set<String> provisionigQuesu = new Set<String>();
        provisionigQuesu.add('Robot');
        provisionigQuesu.add('Team');
        provisionigQuesu.add('Coordinator');
        List<CSPOFA.ProcessRequest> processRequests = new List<CSPOFA.ProcessRequest>();
        for (CSPOFA__Orchestration_Step__c step : stepList) {
            for (String queueName : provisionigQuesu) {
                processRequests.add(
                    new CSPOFA.ProcessRequest().templateName('TB_Provision').relationships( new Map<Schema.SObjectField, Id>{
                    CSPOFA__Orchestration_Process__c.CSPOFA__Root_Process__c => step.CSPOFA__Orchestration_Process__r.CSPOFA__Root_Process__c,
                    CSPOFA__Orchestration_Process__c.CSPOFA__Parent_Process__c => step.CSPOFA__Orchestration_Process__c, 
                    CSPOFA__Orchestration_Process__c.Suborder__c => step.CSPOFA__Orchestration_Process__r.Suborder__c}
                    ).processName('Provision by - ' + queueName));
            }
        }

        List<CSPOFA.ApiResult> results = CSPOFA.API_V1.processes.create(processRequests);
        for (CSPOFA.ApiResult result : results) { if (result.isSuccess()) {
        String resourceId = result.getId(); } else {
                List<CSPOFA.ApiError> errors = result.getErrors();
            }
        }
    }
}