/*
	Author: Juan Cardona
	Description: Component serves as controller of the Accordion Component.
	Jira ticket: COM-2682
*/

public with sharing class ZiggoMigrationFieldsFormController {
	private static Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

	/*
	 * Get the set of information sorted to be used by the Accordion Component.
	 */
	@AuraEnabled
	public static LightningResponse getFields(Id recordId) {
		List<AccordionFormController.accordionSection> lstAccordionSection = new List<AccordionFormController.accordionSection>();
		set<Id> sitesIds = new Set<Id>();
		LightningResponse response;
		for (cscfga__Product_Configuration__c pc : getProductConfigurations(recordId)) {
			if (pc?.cscfga__Product_Basket__r?.cscfga__Opportunity__r?.ZiggoMigrationInformation__c != null) {
				return new LightningResponse()
					.setBody(
						(List<AccordionFormController.accordionSection>) JSON.deserialize(
							pc.cscfga__Product_Basket__r.cscfga__Opportunity__r.ZiggoMigrationInformation__c,
							List<AccordionFormController.accordionSection>.class
						)
					);
			} else if (!sitesIds.contains(pc.Site__r.Id)) {
				lstAccordionSection.add(getSection(pc));
			}
			sitesIds.add(pc.Site__r.Id);
		}
		if (!lstAccordionSection.isEmpty()) {
			response = new LightningResponse().setBody(lstAccordionSection);
		} else {
			response = new LightningResponse().setBody(null);
		}
		return response;
	}

	/*
	 * Update the Opportunity with the migration information set by the component.
	 */
	@AuraEnabled
	public static LightningResponse save(Id recordId, String jsonInput) {
		Savepoint savepoint = Database.setSavePoint();
		try {
			List<AccordionFormController.accordionSection> accordion = (List<AccordionFormController.accordionSection>) JSON.deserialize(
				jsonInput,
				List<AccordionFormController.accordionSection>.class
			);
			update new Opportunity(
				Id = getProductConfigurations(recordId)[0].cscfga__Product_Basket__r.cscfga__Opportunity__r.Id,
				ZiggoMigrationInformation__c = jsonInput
			);
			return new LightningResponse().setSuccess(System.Label.InstallationInformation_InformationSuccessfullyUpdated);
		} catch (DMLException objEx) {
			return setError(savepoint, objEx.getDmlMessage(0));
		} catch (Exception objEx) {
			return setError(savepoint, objEx.getMessage());
		}
	}

	/*
	 * Roll Back saved changes and get a Lightning Response error instance
	 */
	private static LightningResponse setError(Savepoint savepoint, String message) {
		if (savepoint != null) {
			Database.rollback(savepoint);
		}
		return new LightningResponse().setError(message);
	}

	/*
	 * Get and Instance from the AccordionFormController.AccordionSection object
	 */
	private static AccordionFormController.AccordionSection getSection(cscfga__Product_Configuration__c productConfiguration) {
		AccordionFormController.AccordionSection accSection = new AccordionFormController.AccordionSection();
		accSection.tittleLabel = productConfiguration.Site__r.Name;
		accSection.tittleReference = productConfiguration.Site__r.Unify_Site_Name__c;

		List<AccordionFormController.AccordionFormField> listFieldsRow1 = new List<AccordionFormController.AccordionFormField>();
		listFieldsRow1.add(getField('Opportunity', 'LG_ClientNumber__c', null, 'text', false, true));
		listFieldsRow1.add(
			getField(
				'Opportunity',
				'LG_ExistingSubscription__c',
				productConfiguration.cscfga__Product_Basket__r.cscfga__Opportunity__r.Id,
				null,
				false,
				false
			)
		);

		List<AccordionFormController.AccordionFormField> listFieldsRow2 = new List<AccordionFormController.AccordionFormField>();
		listFieldsRow2.add(
			getField('Opportunity', 'LG_IPAddress__c', productConfiguration.cscfga__Product_Basket__r.cscfga__Opportunity__r.Id, null, false, false)
		);
		listFieldsRow2.add(
			getField(
				'Opportunity',
				'Zakelijk_Hosted_Bellen__c',
				productConfiguration.cscfga__Product_Basket__r.cscfga__Opportunity__r.Id,
				null,
				false,
				false
			)
		);

		List<AccordionFormController.AccordionFormField> listFieldsRow3 = new List<AccordionFormController.AccordionFormField>();
		listFieldsRow3.add(getField('Opportunity', 'Contract_Number__c', null, 'text', false, true));
		listFieldsRow3.add(
			getField('Opportunity', 'LG_DTV__c', productConfiguration.cscfga__Product_Basket__r.cscfga__Opportunity__r.Id, null, false, false)
		);

		List<AccordionFormController.AccordionFormField> listFieldsRow4 = new List<AccordionFormController.AccordionFormField>();
		listFieldsRow4.add(
			getField('Opportunity', 'LG_Internet__c', productConfiguration.cscfga__Product_Basket__r.cscfga__Opportunity__r.Id, null, false, false)
		);
		listFieldsRow4.add(
			getField('Opportunity', 'Multi_Wifi__c', productConfiguration.cscfga__Product_Basket__r.cscfga__Opportunity__r.Id, null, false, false)
		);

		List<AccordionFormController.AccordionFormField> listFieldsRow5 = new List<AccordionFormController.AccordionFormField>();
		listFieldsRow5.add(
			getField(
				'Opportunity',
				'Coax_Product_Already_Active_On_Site__c',
				productConfiguration.cscfga__Product_Basket__r.cscfga__Opportunity__r.Id,
				null,
				false,
				false
			)
		);

		AccordionFormController.AccordionSectionRow row1 = new AccordionFormController.AccordionSectionRow();
		row1.listFields = listFieldsRow1;
		AccordionFormController.AccordionSectionRow row2 = new AccordionFormController.AccordionSectionRow();
		row2.listFields = listFieldsRow2;
		AccordionFormController.AccordionSectionRow row3 = new AccordionFormController.AccordionSectionRow();
		row3.listFields = listFieldsRow3;
		AccordionFormController.AccordionSectionRow row4 = new AccordionFormController.AccordionSectionRow();
		row4.listFields = listFieldsRow4;
		AccordionFormController.AccordionSectionRow row5 = new AccordionFormController.AccordionSectionRow();
		row5.listFields = listFieldsRow5;

		List<AccordionFormController.AccordionSectionRow> listRow = new List<AccordionFormController.AccordionSectionRow>();
		listRow.add(row1);
		listRow.add(row2);
		listRow.add(row3);
		listRow.add(row4);
		listRow.add(row5);

		accSection.listRows = listRow;
		return accSection;
	}

	/*
	 * Get instance from a AccordionFormController.AccordionFormField object
	 */
	private static AccordionFormController.AccordionFormField getField(
		String objectName,
		String fieldName,
		String fieldRecordId,
		String fieldType,
		Boolean isRequired,
		Boolean isInput
	) {
		AccordionFormController.AccordionFormField formField = new AccordionFormController.AccordionFormField();
		Schema.DescribeFieldResult fieldDescribe = globalDescribe.get(objectName).getDescribe().fields.getMap().get(fieldName).getDescribe();
		formField.fieldObjectApiName = objectName;
		formField.fieldId = fieldDescribe.getName();
		formField.fieldLabel = fieldDescribe.getLabel();
		formField.fieldeReference = fieldDescribe.getName();
		formField.fieldApiName = fieldDescribe.getName();
		formField.fieldRecordId = fieldRecordId;
		formField.isRequired = isRequired;
		formField.fieldTypeString = fieldType;
		formField.isInput = isInput;
		return formField;
	}

	/*
	 * Retrieve the Product Configurations associated to the Basket
	 */
	private static List<cscfga__Product_Configuration__c> getProductConfigurations(Id basketId) {
		return [
			SELECT
				Id,
				Site__c,
				Site__r.Id,
				Site__r.Name,
				Site__r.Unify_Site_Name__c,
				cscfga__Product_Basket__c,
				cscfga__Product_Basket__r.cscfga__Opportunity__c,
				cscfga__Product_Basket__r.cscfga__Opportunity__r.Id,
				cscfga__Product_Basket__r.cscfga__Opportunity__r.ZiggoMigrationInformation__c
			FROM cscfga__Product_Configuration__c
			WHERE Site__c != NULL AND cscfga__Product_Basket__c = :basketId
			ORDER BY Site__r.Unify_Site_Name__c
		];
	}
}
