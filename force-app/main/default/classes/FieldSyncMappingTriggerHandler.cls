public with sharing class FieldSyncMappingTriggerHandler extends TriggerHandler {

    public override void beforeInsert() {
        List<Field_Sync_Mapping__c> newFieldSyncMappings = (List<Field_Sync_Mapping__c>) this.newList;
        checkIfValidFields(newFieldSyncMappings);
    }

    public override void beforeUpdate() {
        List<Field_Sync_Mapping__c> newFieldSyncMappings = (List<Field_Sync_Mapping__c>) this.newList;
        checkIfValidFields(newFieldSyncMappings);
    }

    private void checkIfValidFields(List<Field_Sync_Mapping__c> newFieldSyncMappings) {
        // when creating or updating a sync mapping, check if the entered values are real fields
        for (Field_Sync_Mapping__c fsm : newFieldSyncMappings) {
            String[] objectMapping = fsm.Object_Mapping__c.split(' -> ');
            String sourceObject = objectMapping[0].toLowerCase();
            String targetObject = objectMapping[1].toLowerCase();

            if (allObjectsMap.containsKey(sourceObject)) {
                if (fsm.Source_Field_Name__c != null && !Schema.getGlobalDescribe().get(sourceObject).getDescribe().Fields.getMap().conTainsKey(fsm.Source_Field_Name__c.toLowerCase())) {
                    fsm.addError('Source field ' + fsm.Source_Field_Name__c + ' not found on object ' + sourceObject);
                }
            } else {
                fsm.addError('Source object ' + sourceObject + ' not found (use \' -> \' as separator)');
            }

            if (allObjectsMap.containsKey(targetObject)) {
                // GN: Permit N/A in the target mapping to allow a field that is not being mapped to be queried
                if (fsm.Target_Field_Name__c.left(3)!='N/A') {
                    if (!Schema.getGlobalDescribe().get(targetObject).getDescribe().Fields.getMap().conTainsKey(fsm.Target_Field_Name__c.toLowerCase())) {
                        fsm.addError('Target field ' + fsm.Target_Field_Name__c + ' not found on object ' + targetObject);
                    }
                }
            } else {
                fsm.addError('Target object ' + targetObject + ' not found (use \' -> \' as separator)');
            }
        }
    }

    private Map<String, Schema.SObjectType> allObjectsMap {
        get {
            if (allObjectsMap == null) {
                allObjectsMap = Schema.getGlobalDescribe();
            }
            return allObjectsMap;
        }
        set;
    }
}