@isTest
private class TestCOM_DXL_Contacts {
	@testSetup
	static void setup() {
		No_Triggers__c noTriggers = No_Triggers__c.getInstance(UserInfo.getUserId());
		noTriggers.Flag__c = true;
		noTriggers.SetupOwnerId = UserInfo.getOrganizationId();
		upsert noTriggers;

		User owner = TestUtils.createAdministrator();

		TestUtils.autoCommit = false;
		Account acct = TestUtils.createAccount(owner);
		acct.KVK_number__c = '12345678';
		acct.date_of_establishment__c = Date.newInstance(2022, 5, 20);
		acct.Legal_Structure_Code__c = 'Structure code';
		acct.NumberOfEmployees = 12;
		acct.name = 'ACME Company';
		acct.Unify_Account_Type__c = 'Unify Type';
		acct.Unify_Account_SubType__c = 'Unify Subtype';
		acct.fax = '';
		acct.duns_number__c = '123456789';
		acct.Visiting_Postal_Code__c = '123';
		acct.Visiting_Housenumber1__c = 15;
		acct.Visiting_Housenumber_Suffix__c = '';
		acct.Visiting_street__c = 'Street';
		acct.Visiting_City__c = 'Eindhoven';
		acct.Visiting_Country__c = 'Curacao';
		insert acct;

		Contact testContact = new Contact(
			AccountId = acct.Id,
			Phone = '+31 111111789',
			LastName = 'Fixed',
			Email = 'test@test.com',
			Userid__c = owner.Id
		);
		insert testContact;

		List<External_Contact__c> externalContacts = new List<External_Contact__c>();
		External_Contact__c extarnalContact1 = new External_Contact__c(
			Contact__c = testContact.Id,
			External_Source__c = 'Unify',
			External_Contact_Id__c = 'UInifyID123654789',
			ExternalId__c = 'UInifyID123654789'
		);
		External_Contact__c extarnalContact2 = new External_Contact__c(
			Contact__c = testContact.Id,
			External_Source__c = 'BOP',
			External_Contact_Id__c = 'BOPID123654789',
			ExternalId__c = 'BOPID123654789'
		);
		externalContacts.add(extarnalContact1);
		externalContacts.add(extarnalContact2);
		insert externalContacts;

		TestUtils.autoCommit = true;
		Ban__c ban = TestUtils.createBan(acct);

		TestUtils.autoCommit = false;
		Financial_Account__c fa = TestUtils.createFinancialAccount(ban, testContact.Id);
		fa.Bank_Account_Name__c = 'test bank account';
		fa.Bank_Account_Number__c = 'NL91 ABNA 0417 1643 00';
		fa.Payment_method__c = 'Invoice';
		fa.Unify_Ref_Id__c = '';
		insert fa;

		update new Account(Id = acct.Id, Authorized_to_sign_1st__c = testContact.Id);

		Opportunity testOpportunity = CS_DataTest.createOpportunity(acct, 'Test Opportunity', owner.Id);
		testOpportunity.Contact__c = testContact.Id;
		testOpportunity.Financial_Account__c = fa.Id;
		testOpportunity.Main_Contact_Person__c = testContact.Id;
		insert testOpportunity;

		csord__Order__c ord = CS_DataTest.createOrder();
		ord.csord__Status2__c = 'Created';
		ord.csordtelcoa__Opportunity__c = testOpportunity.Id;
		insert ord;

		List<Contact_Role__c> contactRoleList = new List<Contact_Role__c>();
		Contact_Role__c contactRole1 = new Contact_Role__c();
		contactRole1.Type__c = 'main';
		contactRole1.Account__c = acct.Id;
		contactRole1.Contact__c = testContact.Id;

		contactRoleList.add(contactRole1);

		insert contactRoleList;
	}

	@isTest
	static void testMethod1() {
		List<Opportunity> oppList = [
			SELECT
				Id,
				AccountId,
				Account.KVK_number__c,
				Account.date_of_establishment__c,
				Account.Legal_Structure_Code__c,
				Account.NumberOfEmployees,
				Account.Name,
				Account.Unify_Account_Type__c,
				Account.Unify_Account_SubType__c,
				Account.Fax,
				Account.duns_number__c,
				Account.BOPCode__c,
				BAN__r.BAN_Name__c,
				BAN__r.BAN_Number__c,
				BAN__r.Unify_Ref_Id__c,
				BAN__r.Unify_Customer_Type__c,
				BAN__r.Unify_Customer_SubType__c,
				Account.Visiting_Postal_Code__c,
				Account.Visiting_HouseNumber1__c,
				Account.Visiting_HouseNumber_Suffix__c,
				Account.Visiting_street__c,
				Account.Visiting_City__c,
				Account.Visiting_Country__c,
				Account.Fixed_Dealer__c,
				Account.Fixed_Dealer__r.Contact__r.UserId__r.UserType,
				Account.Fixed_Dealer__r.Contact__r.UserId__r.Name,
				Account.Fixed_Dealer__r.Contact__r.UserId__r.Manager.Name,
				Account.Fixed_Dealer__r.Contact__r.Account.BOPCode__c,
				Account.Mobile_Dealer__c,
				Account.Mobile_Dealer__r.Contact__r.UserId__r.UserType,
				Account.Mobile_Dealer__r.Contact__r.UserId__r.Name,
				Account.Mobile_Dealer__r.Contact__r.UserId__r.Manager.Name,
				Account.Mobile_Dealer__r.Contact__r.Account.BOPCode__c,
				Account.Owner.UserType,
				Account.Owner.Name,
				Account.Owner.Manager.Name,
				Account.Owner.Account.BOPCode__c,
				Account.Authorized_to_Sign_1st__c,
				Account.Authorized_to_Sign_1st__r.Gender__c,
				Account.Authorized_to_Sign_1st__r.Email,
				Account.Authorized_to_Sign_1st__r.Phone,
				Account.Authorized_to_Sign_1st__r.MobilePhone,
				Account.Authorized_to_Sign_1st__r.BirthDate,
				Account.Authorized_to_Sign_1st__r.FirstName,
				Account.Authorized_to_Sign_1st__r.LastName,
				Account.Authorized_to_Sign_1st__r.Document_Type__c,
				Account.Authorized_to_Sign_1st__r.Document_Number__c,
				Account.Authorized_to_Sign_1st__r.Jurisdiction_Type__c,
				Account.Authorized_to_Sign_1st__r.AccountId,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Postal_Code__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Housenumber1__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Housenumber_Suffix__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Street__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_City__c,
				Account.Authorized_to_Sign_1st__r.Account.Visiting_Country__c,
				Financial_Account__c,
				Financial_Account__r.Payment_Method__c,
				Financial_Account__r.Financial_Contact__r.Name,
				Financial_Account__r.Unify_Ref_Id__c,
				Financial_Account__r.Bank_Account_Name__c,
				Financial_Account__r.Bank_Account_Number__c,
				Financial_Account__r.Financial_Contact__c,
				Financial_Account__r.Financial_Contact__r.Gender__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Postal_Code__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Housenumber1__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Housenumber_Suffix__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Street__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_City__c,
				Financial_Account__r.Financial_Contact__r.Account.Visiting_Country__c,
				Financial_Account__r.Financial_Contact__r.Email,
				Financial_Account__r.Financial_Contact__r.Phone,
				Financial_Account__r.Financial_Contact__r.MobilePhone,
				Financial_Account__r.Financial_Contact__r.Birthdate,
				Financial_Account__r.Financial_Contact__r.FirstName,
				Financial_Account__r.Financial_Contact__r.LastName,
				Financial_Account__r.Financial_Contact__r.Document_Type__c,
				Financial_Account__r.Financial_Contact__r.Document_Number__c,
				Financial_Account__r.Financial_Contact__r.Jurisdiction_Type__c,
				Financial_Account__r.Financial_Contact__r.AccountId,
				Billing_Arrangement__r.Unify_Ref_Id__c,
				Billing_Arrangement__r.Billing_Arrangement_Alias__c,
				Billing_Arrangement__r.Bill_Format__c,
				Billing_Arrangement__r.Billing_Postal_Code__c,
				Billing_Arrangement__r.Billing_Housenumber__c,
				Billing_Arrangement__r.Billing_Housenumber_Suffix__c,
				Billing_Arrangement__r.Billing_Street__c,
				Billing_Arrangement__r.Billing_City__c,
				Billing_Arrangement__r.Billing_Country__c,
				Contact__c,
				Contact__r.Account.Visiting_Postal_Code__c,
				Contact__r.Account.Visiting_Housenumber1__c,
				Contact__r.Account.Visiting_Housenumber_Suffix__c,
				Contact__r.Account.Visiting_Street__c,
				Contact__r.Account.Visiting_City__c,
				Contact__r.Account.Visiting_Country__c,
				Contact__r.Email,
				Contact__r.Phone,
				Contact__r.MobilePhone,
				Contact__r.BirthDate,
				Contact__r.Document_Type__c,
				Contact__r.Document_Number__c,
				Contact__r.Jurisdiction_Type__c,
				Contact__r.Contact_Role__c,
				Contact__r.FirstName,
				Contact__r.LastName,
				Contact__r.AccountId
			FROM Opportunity
		];

		List<Contact_Role__c> contactRoles = [
			SELECT
				Id,
				Name,
				Contact__c,
				Contact__r.Email,
				Type__c,
				Contact__r.MobilePhone,
				Account__c,
				Contact__r.Account.Visiting_Street__c,
				Contact__r.Account.Visiting_Housenumber1__c,
				Contact__r.Account.Visiting_Housenumber_Suffix__c,
				Contact__r.Account.Visiting_Postal_Code__c,
				Contact__r.Account.Visiting_City__c,
				Contact__r.Account.Visiting_Country__c,
				Contact__r.Gender__c,
				Contact__r.Birthdate,
				Contact__r.FirstName,
				Contact__r.LastName,
				Contact__r.Phone,
				Contact__r.Document_Type__c,
				Contact__r.Document_Number__c,
				Contact__r.Jurisdiction_Type__c
			FROM Contact_Role__c
			WHERE Account__c = :oppList[0].AccountId
		];

		List<csord__Order__c> ordList = [SELECT Id, Name, csord__Status2__c, csordtelcoa__Opportunity__c FROM csord__Order__c];
		List<Id> orderIds = new List<Id>();
		orderIds.add(ordList[0].Id);

		COM_DXL_CreateCustomerService customerService = new COM_DXL_CreateCustomerService(oppList[0].Id, ordList[0].Id);

		JSONGenerator generator = JSON.createGenerator(false);
		generator.writeStartObject();
		COM_DXL_Contacts dxlContacts = new COM_DXL_Contacts(generator, oppList, customerService.contactsWithExternalContacts, contactRoles);
		dxlContacts.generateContacts();
		generator.writeEndObject();

		Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(generator.getAsString());
		Object contacts = (Object) m.get('contacts');
		List<Object> contactItemsList = (List<Object>) contacts;
		Map<String, Object> contactItem = (Map<String, Object>) contactItemsList[0];

		System.assertEquals(false, contactItemsList.isEmpty(), 'List of contacts should not be empty:  isEmpty = ' + contactItemsList.isEmpty());
		System.assertEquals(
			'UInifyID123654789',
			contactItem.get('unifyContactRefId'),
			'Contact unifyContactRefID: Expected UInifyID123654789 == Actual: ' + contactItem.get('unifyContactRefID')
		);
	}
}
