global without sharing class ProposalExcelController {

    public String xmlheader {
        get {
            return '<?xml version="1.0"?><?mso-application progid="Excel.Sheet"?>';
        }
        set;
    }

    /**
     * Opportuntiy
     */
    public final Opportunity opp;

    /**
     * Basket
     */
    public final cscfga__Product_Basket__c basket;

    public String docName {
        get {
            if (docName == null) {
                docName = '';
                if (basket != null) {
                    docName = basket.Basket_Number__c;
                }
            }
            return docName;
        }
        private set;
    }

    /**
     * Parent Account Details
     */
    public AccountDetails acc {
        get {
            if (acc == null) {
                if (basket != null) {
                    Account tmpAcc = [Select Id
                            , Name
                            , Phone
                            , Fax
                            , Billing_Street__c
                            , Billing_City__c
                            , Billing_Housenumber__c
                            , Billing_Housenumber_Suffix__c
                            , Billing_Postal_Code__c
                            , Visiting_street__c
                            , Visiting_Postal_Code__c
                            , Visiting_Housenumber1__c
                            , Visiting_Housenumber_Suffix__c
                            , Visiting_City__c
                    From Account
                    Where Id =: basket.csbb__Account__c];
                    AccountDetails ad = new AccountDetails();
                    ad.name = tmpAcc.Name;
                    ad.billingHouseNumber 		= (tmpAcc.Billing_Housenumber__c != null ? String.valueOf(tmpAcc.Billing_Housenumber__c) : '');
                    ad.billingHouseNumberExt	= (!String.isEmpty(tmpAcc.Billing_Housenumber_Suffix__c) ? tmpAcc.Billing_Housenumber_Suffix__c : '');
                    ad.billingStreet 			= (!String.isEmpty(tmpAcc.Billing_Street__c ) ? tmpAcc.Billing_Street__c  : '');
                    ad.billingCity 				= (!String.isEmpty(tmpAcc.Billing_City__c ) ? tmpAcc.Billing_City__c  : '');
                    ad.billingPostalCode 		= (!String.isEmpty(tmpAcc.Billing_Postal_Code__c) ? tmpAcc.Billing_Postal_Code__c : '');
                    ad.visitingHouseNumber 		= (tmpAcc.Visiting_Housenumber1__c != null ? String.valueOf(tmpAcc.Visiting_Housenumber1__c) : '');
                    ad.visitingHouseNumberExt	= (!String.isEmpty(tmpAcc.Visiting_Housenumber_Suffix__c) ? tmpAcc.Visiting_Housenumber_Suffix__c : '');
                    ad.visitingStreet 			= (!String.isEmpty(tmpAcc.Visiting_street__c) ? tmpAcc.Visiting_street__c : '');
                    ad.visitingCity 			= (!String.isEmpty(tmpAcc.Visiting_City__c ) ? tmpAcc.Visiting_City__c  : '');
                    ad.visitingPostalCode 		= (!String.isEmpty(tmpAcc.Visiting_Postal_Code__c) ? tmpAcc.Visiting_Postal_Code__c : '');
                    ad.phone					= (!String.isEmpty(tmpAcc.Phone) ? tmpAcc.Phone : '');
                    ad.fax					= (!String.isEmpty(tmpAcc.Fax) ? tmpAcc.Fax : '');
                    acc = ad;
                } else {
                    acc = new AccountDetails();
                }
            }
            return acc;
        }
        private set;
    }

    public String basketListCounter {
        get {
            if (basketListCounter == null) {
                // Add 3
                basketListCounter = String.valueOf(this.basketDetailList.size() + 3);
            }
            return basketListCounter;
        }
        private set;
    }

    public Decimal sumBrutRecurringTotalPrice {
        get {
            if (sumBrutRecurringTotalPrice == null) {
                sumBrutRecurringTotalPrice = 0;
                if (basketDetailList != null) {
                    for (BasketDetail bd : basketDetailList) {
                        sumBrutRecurringTotalPrice += bd.totalLocationonerecurringTotalPrice;
                    }
                }

            }
            return sumBrutRecurringTotalPrice;
        }
        private set;
    }

    public Decimal sumBrutProductTotalPrice {
        get {
            if (sumBrutProductTotalPrice == null) {
                sumBrutProductTotalPrice = 0;
                if (basketDetailList != null) {
                    for (BasketDetail bd : basketDetailList) {
                        sumBrutProductTotalPrice += bd.totalLocationoneproductTotalPrice;
                    }
                }

            }
            return sumBrutProductTotalPrice;
        }
        private set;
    }


    public Boolean showOneOffDiscount {
        get {
            if (showOneOffDiscount == null) {
                showOneOffDiscount = true;
                if (sumOneOffDiscount == 0) {
                    showOneOffDiscount = false;
                }
            }
            return showOneOffDiscount;
        }
        private set;
    }

    public Boolean showRecurringDiscount {
        get {
            if (showRecurringDiscount == null) {
                showRecurringDiscount = true;
                if (sumRecurringDiscount == 0) {
                    showRecurringDiscount = false;
                }
            }
            return showRecurringDiscount;
        }
        private set;
    }

    public String hiddenColumnMerge {
        get {
            if (hiddenColumnMerge == null) {
                Integer i = 0;
                if (!showOneOffDiscount) {
                    i += 1;
                }
                if (!showRecurringDiscount) {
                    i += 1;
                }
                hiddenColumnMerge = String.valueOf(i);
            }
            return hiddenColumnMerge;
        }
        private set;
    }

    public String fullRowCount {
        get {
            if (fullRowCount == null) {
                Integer i = 7;
                fullRowCount = String.valueOf(i);
            }
            return fullRowCount;
        }
        private set;
    }

    /**
     *
     */
    public Decimal sumProductTotalPrice {
        get {
            if (sumProductTotalPrice == null) {
                sumProductTotalPrice = 0;
                if (basketDetailList != null) {
                    for (BasketDetail bd : basketDetailList) {
                        sumProductTotalPrice += bd.totalLocationoneproductTotalPriceNet;
                    }
                }
            }
            return sumProductTotalPrice;
        }
        private set;
    }


    public Decimal sumOneOffPrice {
        get {
            if (sumOneOffPrice == null) {
                sumOneOffPrice = 0;
                if (basketDetailList != null) {
                    for (BasketDetail bd : basketDetailList) {
                        for (ProductItem pi : bd.prodItems) {
                            if (pi.oneOffPrice != null) {
                                sumOneOffPrice += pi.oneOffPrice;
                            }
                        }
                    }
                }
            }
            return sumOneOffPrice;
        }
        private set;
    }


    public Decimal sumOneOffDiscount {
        get {
            if (sumOneOffDiscount == null) {
                sumOneOffDiscount = 0;
                if (basketDetailList != null) {
                    for (BasketDetail bd : basketDetailList) {
                        for (ProductItem pi : bd.prodItems) {
                            if (pi.oneOffDiscount != null) {
                                sumOneOffDiscount += (pi.oneOffDiscount * pi.quantity);
                            }
                        }
                    }
                }
            }
            return sumOneOffDiscount;
        }
        private set;
    }

    public Decimal sumRecurringDiscount {
        get {
            if (sumRecurringDiscount == null) {
                sumRecurringDiscount = 0;
                if (basketDetailList != null) {
                    for (BasketDetail bd : basketDetailList) {
                        for (ProductItem pi : bd.prodItems) {
                            if (pi.recurringDiscount != null) {
                                sumRecurringDiscount += pi.recurringDiscount;
                            }
                        }
                    }
                }
            }
            return sumRecurringDiscount;
        }
        private set;
    }

    public Decimal sumRecurringPrice {
        get {
            if (sumRecurringPrice == null) {
                sumRecurringPrice = 0;
                if (basketDetailList != null) {
                    for (BasketDetail bd : basketDetailList) {
                        for (ProductItem pi : bd.prodItems) {
                            if (pi.recurringPrice != null) {
                                sumRecurringPrice += pi.recurringPrice;
                            }
                        }
                    }
                }
            }
            return sumRecurringPrice;
        }
        private set;
    }

    public Decimal sumRecurringTotalPrice {
        get {
            if (sumRecurringTotalPrice == null) {
                sumRecurringTotalPrice = 0;
                if (basketDetailList != null) {
                    for (BasketDetail bd : basketDetailList) {
                        sumRecurringTotalPrice += bd.totalLocationonerecurringTotalPriceNet;
                    }
                }
            }
            return sumRecurringTotalPrice;
        }
        private set;
    }

    public Decimal totalPrice {
        get {
            if (totalPrice == null) {
                totalPrice = 0;
                if (basketDetailList != null) {
                    for (BasketDetail bd : basketDetailList) {
                        for (ProductItem pi : bd.prodItems) {
                            if (pi.recurring) {
                                totalPrice += pi.productTotalPrice;
                            }
                        }
                    }
                }
            }
            return totalPrice;
        }
        private set;
    }
    /**
     *
     */
    public List<BasketDetail> basketDetailList {
        get {
            if (basketDetailList == null && this.basket != null) {

                List<BasketDetail> tmpBasketListList = new List<BasketDetail>();
                Map<Id, String> siteIdMap = new Map<Id, String>();
                Map<String, BasketDetail> basketMap = new Map<String, BasketDetail>();


                List<cscfga__Product_Configuration__c> pcList = [Select
                        Id,
                        Name,
                        cscfga__one_off_charge_product_discount_value__c,
                        cscfga__Quantity__c,
                        OneOff_List_Price__c,
                        cscfga__One_Off_Charge__c,
                        cscfga__total_one_off_charge__c,
                        csdiscounts__One_off_non_discounted__c,
                        Recuring_List_price__c,
                        cscfga__Recurring_Charge__c,
                        Recurring_Product_List_Price__c,
                        OneOff_Product_List_Price__c,
                        cscfga__recurring_charge_product_discount_value__c,
                        cscfga__total_recurring_charge__c,
                        cscfga__Parent_Configuration__r.Site_Check_SiteID__c,
                        cscfga__Parent_Configuration__r.ClonedSiteIds__c,
                        cscfga__Parent_Configuration__r.cscfga__Product_Basket__c,
                        cscfga__Parent_Configuration__r.cscfga__Product_Basket__r.Basket_Number__c,
                        cscfga__Parent_Configuration__r.cscfga__Product_Basket__r.Contract_duration_Fixed__c,
                        cscfga__Parent_Configuration__r.cscfga__Product_Basket__r.Contract_duration_Mobile__c
                From cscfga__Product_Configuration__c
                Where Name != 'PAYU Fee' AND 
                        cscfga__Parent_Configuration__r.cscfga__Product_Basket__c = :  this.basket.Id
                ];

                Map<Id, List<CS_Basket_Snapshot_Transactional__c>> pcSnapMap = new Map<Id, List<CS_Basket_Snapshot_Transactional__c>>();
                for(cscfga__Product_Configuration__c pc : pcList) {
                    pcSnapMap.put(pc.Id, null);
                }
                List<CS_Basket_Snapshot_Transactional__c> snapList = [Select Id, Product_Configuration__c, ProductName__c, ConnectionType__c, Quantity__c From CS_Basket_Snapshot_Transactional__c Where Product_Configuration__c IN : pcSnapMap.keySet() AND Product_Basket__c = : this.basket.Id];
                for (CS_Basket_Snapshot_Transactional__c snap : snapList) {
                    if (snap.ProductName__c == 'ONE Business Voice' && snap.ConnectionType__c == 'Disconnect') {
                        if (pcSnapMap.get(snap.Product_Configuration__c) != null) {
                            List<CS_Basket_Snapshot_Transactional__c> tmpList = pcSnapMap.get(snap.Product_Configuration__c);
                            tmpList.add(snap);
                            pcSnapMap.put(snap.Product_Configuration__c, tmpList);
                        } else {
                            List<CS_Basket_Snapshot_Transactional__c> tmpList = new List<CS_Basket_Snapshot_Transactional__c>();
                            tmpList.add(snap);
                            pcSnapMap.put(snap.Product_Configuration__c, tmpList);
                        }
                    }
                }

                system.debug('##pcList: '+pcList.size());
                for(cscfga__Product_Configuration__c pc : pcList) {
                    BasketDetail bd;
                    system.debug('##pc.cscfga__Parent_Configuration__r.ClonedSiteIds__c: '+pc.cscfga__Parent_Configuration__r.ClonedSiteIds__c);
                    /** Site ID / Clones Site ID's */
                    List<String> siteList = new List<String>();
                    if (!String.isEmpty(pc.cscfga__Parent_Configuration__r.ClonedSiteIds__c)) {
                        String jsonInput = pc.cscfga__Parent_Configuration__r.ClonedSiteIds__c;
                        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(jsonInput);
                        for (String key : m.keySet()) {
                            String val = (String)m.get(key);
                            siteList.add(val);
                        }
                    } else {
                        siteList.add(pc.cscfga__Parent_Configuration__r.Site_Check_SiteID__c);
                    }
                    system.debug('##siteList: '+siteList);
                    for (String siteId : siteList) {
                        system.debug('##basketMap: '+basketMap);
                        if (basketMap.containsKey(siteId)) {
                            bd = basketMap.get(siteId);
                        } else {
                            bd = new BasketDetail();
                        }
                        bd.durationFixed = String.valueOf(pc.cscfga__Parent_Configuration__r.cscfga__Product_Basket__r.Contract_duration_Fixed__c);
                        bd.durationMobile = pc.cscfga__Parent_Configuration__r.cscfga__Product_Basket__r.Contract_duration_Mobile__c;
                        ProductItem pi = new ProductItem();
                        if (String.isEmpty(pc.Name)) {
                            pi.productName = pc.Name;
                        } else {
                            pi.productName = pc.Name;
                        }
                        pi.productId = pc.Id;
                        pi.oneOffPrice = pc.OneOff_Product_List_Price__c;
                        pi.oneOffDiscount = pc.cscfga__one_off_charge_product_discount_value__c;
                        pi.productTotalPrice = 0;
                        //if (pc.OneOff_List_Price__c != null && pc.cscfga__Quantity__c != null) {
                        //pi.productTotalPrice = pc.csdiscounts__One_off_non_discounted__c * pc.cscfga__Quantity__c;
                        pi.productTotalPrice = pc.cscfga__total_one_off_charge__c;
                        //}
                        pi.quantity = Integer.valueOf(pc.cscfga__Quantity__c);

                        /** Checking for disconnects */
                        Boolean skipRow = false;
                        if (pcSnapMap.get(pc.Id) != null) {
                            List<CS_Basket_Snapshot_Transactional__c> snapDisconnectList = pcSnapMap.get(pc.Id);
                            if (snapDisconnectList.size() > 0) {
                                skipRow = true;
                            }
                            Decimal disconnectAmount = 0;
                            for (CS_Basket_Snapshot_Transactional__c sn : snapDisconnectList) {
                                pi.productName += ' / '+sn.Id;
                                if (sn.Quantity__c != null) {
                                    disconnectAmount += sn.Quantity__c;
                                }
                            }
                            if (disconnectAmount > 0) {
                                //pi.quantity = pi.quantity - Integer.valueOf(disconnectAmount);
                            }
                        }


                        pi.recurringPrice = pc.Recurring_Product_List_Price__c;
                        //if (pc.Recuring_List_price__c != null && pc.cscfga__Quantity__c != null) {
                        //pi.recurringTotalPrice = pc.cscfga__Recurring_Charge__c * pc.cscfga__Quantity__c;
                        pi.recurringTotalPrice = pc.cscfga__total_recurring_charge__c;
                        //}
                        pi.recurringDiscount = pc.cscfga__recurring_charge_product_discount_value__c;

                        if (!skipRow) {
                            bd.prodItems.add(pi);
                        }

                        basketMap.put(siteId, bd);
                        if (!String.isEmpty(siteId)) {
                            siteIdMap.put(siteId, '');
                        }
                    }
                }

                List<Site__c> siteList = [Select Id, Name From Site__c Where Id IN : siteIdMap.keySet()];
                for (Site__c site : siteList) {
                    if (siteIdMap.containsKey(site.Id)) {
                        siteIdMap.put(site.Id, site.Name);
                    }
                }
                List<Site_Availability__c> siteAvailableList = [Select Id, Site__r.Name From Site_Availability__c Where Id IN : siteIdMap.keySet()];
                for (Site_Availability__c site : siteAvailableList) {
                    if (siteIdMap.containsKey(site.Id)) {
                        siteIdMap.put(site.Id, site.Site__r.Name);
                    }
                }

                system.debug('##siteIdMap: '+siteIdMap);
                Integer i = 1;
                Boolean generalAvailable = false;
                for (String siteId : basketMap.keySet()) {
                    if (!siteIdMap.containsKey(siteId)) {
                        generalAvailable = true;
                    }
                }
                for (String siteId : basketMap.keySet()) {
                    BasketDetail bd = basketMap.get(siteId);
                    if (siteIdMap.containsKey(siteId)) {
                        bd.siteName = siteIdMap.get(siteId);
                        bd.locationnumber = i;
                        i += 1;
                    } else {
                        bd.siteName = 'General';
                        bd.locationnumber = 0;
                    }
                    bd.totalLocations = basketMap.size();
                    if (generalAvailable) {
                        bd.totalLocations = bd.totalLocations -1;
                    }

                    tmpBasketListList.add(bd);

                }
                basketDetailList = tmpBasketListList;

            }
            basketDetailList.sort();
            return basketDetailList;
        }
        private set;
    }

    /**
     * Constructor
     */
    //public ProposalExcelController(ApexPages.StandardController stdController) {
    public ProposalExcelController() {
        //system.debug('##stdController: '+stdController);
        //system.debug('##stdController: '+(cscfga__Product_Basket__c)stdController.getRecord());
        system.debug('##ApexPages.currentPage().getParameters().get(\'id\'): '+ApexPages.currentPage().getParameters().get('id'));
        //cscfga__Product_Basket__c tmpBasket = (cscfga__Product_Basket__c)stdController.getRecord();
        this.basket = [Select Id, Basket_Number__c, cscfga__Opportunity__c, csbb__Account__c From cscfga__Product_Basket__c Where Id =: ApexPages.currentPage().getParameters().get('id')];
    }

    /**
     *
     */
    public Class AccountDetails {
        public String name {get;set;}
        public String phone {get;set;}
        public String fax {get;set;}
        public String billingStreet {get;set;}
        public String billingHouseNumber {get;set;}
        public String billingHouseNumberExt {get;set;}
        public String billingCity {get;set;}
        public String billingPostalCode {get;set;}
        public String visitingStreet {get;set;}
        public String visitingHouseNumber {get;set;}
        public String visitingHouseNumberExt {get;set;}
        public String visitingCity {get;set;}
        public String visitingPostalCode {get;set;}
        public String visitingAddresss {
            get {
                if (visitingAddresss == null) {
                    visitingAddresss = '';
                    if (!String.isEmpty(visitingStreet)) {
                        visitingAddresss = visitingStreet;
                    }
                    if (!String.isEmpty(String.valueOf(visitingHouseNumber))) {
                        visitingAddresss += ' ' + String.valueOf(visitingHouseNumber);
                    }
                    if (!String.isEmpty(visitingHouseNumberExt)) {
                        visitingAddresss += ' ' + visitingHouseNumberExt;
                    }
                }
                return visitingAddresss;
            }
            private set;
        }
    }

    /**
     *
     */
    global Class BasketDetail implements Comparable {
        public String siteId {get;set;}
        public String siteName {get;set;}
        public String durationMobile {get;set;}
        public String durationFixed {get;set;}
        public Integer totalLocations {get;set;}
        public Integer locationnumber {get;set;}

        public Decimal totalLocationDiscount {
            get {
                if (totalLocationDiscount == null) {
                    totalLocationDiscount = 0;
                    for (ProductItem pd : prodItems) {
                        if (pd.oneOffDiscount != null) {
                            totalLocationDiscount += (pd.oneOffDiscount * pd.quantity);
                        }
                    }
                }
                return totalLocationDiscount;
            }
            private set;
        }

        public Decimal totalLocationrecurringDiscount {
            get {
                if (totalLocationrecurringDiscount == null) {
                    totalLocationrecurringDiscount = 0;
                    for (ProductItem pd : prodItems) {
                        if (pd.recurringDiscount != null) {
                            totalLocationrecurringDiscount += (pd.recurringDiscount * pd.quantity);
                        }
                    }
                }
                return totalLocationrecurringDiscount;
            }
            private set;
        }

        public Decimal totalLocationoneproductTotalPrice {
            get {
                if (totalLocationoneproductTotalPrice == null) {
                    totalLocationoneproductTotalPrice = 0;
                    for (ProductItem pd : prodItems) {
                        if (pd.productTotalPrice != null) {
                            totalLocationoneproductTotalPrice += pd.productTotalPrice;
                        }
                    }
                    /** Add the diuscount to get the Bruto */
                    totalLocationoneproductTotalPrice = totalLocationoneproductTotalPrice + totalLocationDiscount;
                }
                return totalLocationoneproductTotalPrice;
            }
            private set;
        }

        public Decimal totalLocationoneproductTotalPriceNet {
            get {
                if (totalLocationoneproductTotalPriceNet == null) {
                    totalLocationoneproductTotalPriceNet = totalLocationoneproductTotalPrice;
                    if (totalLocationDiscount != null) {
                        totalLocationoneproductTotalPriceNet = totalLocationoneproductTotalPriceNet - totalLocationDiscount;
                    }
                }
                return totalLocationoneproductTotalPriceNet;
            }
            private set;
        }

        public Decimal totalLocationonerecurringTotalPrice {
            get {
                if (totalLocationonerecurringTotalPrice == null) {
                    totalLocationonerecurringTotalPrice = 0;
                    for (ProductItem pd : prodItems) {
                        if (pd.recurringTotalPrice != null) {
                            totalLocationonerecurringTotalPrice += pd.recurringTotalPrice;
                        }
                    }
                    totalLocationonerecurringTotalPrice = totalLocationonerecurringTotalPrice + totalLocationrecurringDiscount;
                }
                return totalLocationonerecurringTotalPrice;
            }
            private set;
        }

        public Decimal totalLocationonerecurringTotalPriceNet {
            get {
                if (totalLocationonerecurringTotalPriceNet == null) {
                    totalLocationonerecurringTotalPriceNet = totalLocationonerecurringTotalPrice;
                    if (totalLocationrecurringDiscount != null) {
                        totalLocationonerecurringTotalPriceNet = totalLocationonerecurringTotalPriceNet - totalLocationrecurringDiscount;
                    }
                }
                return totalLocationonerecurringTotalPriceNet;
            }
            private set;
        }





        public List<ProductItem> prodItems {
            get {
                if (prodItems == null) {
                    prodItems = new List<ProductItem>();
                }
                return prodItems;
            }
            private set;
        }

        global Integer compareTo(Object compareTo) {
            BasketDetail compareToEmp = (BasketDetail)compareTo;
            if (locationnumber < compareToEmp.locationnumber ) {
                return 0;
            } else {
                return 1;
            }
        }

    }

    public Class ProductItem {
        public String productId {get;set;}
        public String productName {get;set;}
        public Decimal productUnitPrice {get;set;}
        public Decimal productTotalPrice {get;set;}
        public Decimal recurringTotalPrice {get;set;}
        public Integer quantity {get;set;}
        public Boolean recurring {
            get {
                if (recurring == null) {
                    recurring = false;
                }
                return recurring;
            }
            set;
        }
        public Decimal recurringAnnualy {get;set;}

        public Decimal oneOffPrice {get;set;}
        public Decimal oneOffDiscount {get;set;}
        public Decimal recurringPrice {get;set;}
        public Decimal recurringDiscount {get;set;}

    }
}