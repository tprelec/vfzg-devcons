@isTest
public with sharing class TestOrderEntryValidityPeriodTrigger {
	@isTest
	public static void testAutoNumbering() {
		ExternalIDNumber__c custSetting = new ExternalIDNumber__c(
			Name = 'OE_Validity_Period',
			External_Number__c = 1,
			Object_Prefix__c = 'OEVP-57-',
			blocked__c = false,
			runningOrg__c = '00D1l0000008i1vEAA'
		);
		insert custSetting;

		String orgId = UserInfo.getOrganizationId();
		OE_Product__c product = OrderEntryTestFactory.createOEProduct('Main', true);

		Test.startTest();
		OrderEntryTestFactory.createOEValidityPeriod(
			Date.today(),
			Date.today().addDays(7),
			product,
			true
		);
		Test.stopTest();

		OE_Validity_Period__c result = [SELECT Id, External_Id__c FROM OE_Validity_Period__c];
		if (custSetting.runningOrg__c == orgId) {
			System.assertEquals(
				'OEVP-57-000002',
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		} else {
			System.assertEquals(
				null,
				result.External_Id__c,
				'If this class is run in DEVBAU it should set External ID otherwise it shouldn\'t'
			);
		}
	}
}