@isTest
private class AssignPriceBookToProductBasketImplTest {

	private static testMethod void test() {
        List<Profile> pList = [select Id, Name from Profile where Name = 'System Administrator' LIMIT 1];
        List<UserRole> roleList = [select Id, Name, DeveloperName From UserRole u where ParentRoleId = null];
        ID rId = roleList[0].Id;
        ID pId = pList[0].Id;
        
        User simpleUser = new User(
            UserRoleId = rId,
            ProfileId = pId,
            Alias = 'standard', 
            Email='standarduser@testorg.com',  
            EmailEncodingKey='UTF-8', 
            LastName='Testing', 
            LanguageLocaleKey='nl_NL', 
            LocaleSidKey='nl_NL', 
            TimeZoneSidKey='Europe/Amsterdam', 
            UserName='testUserA@testorganise.com'
            );
            
        insert simpleUser;
        System.runAs (simpleUser) { 
        Test.startTest();
        
        No_Triggers__c notriggers = new No_Triggers__c();
        notriggers.SetupOwnerId = simpleUser.Id;
        notriggers.Flag__c = true;
        insert notriggers;
        
        Account tmpAcc = CS_DataTest.createAccount('Test Account');
        insert tmpAcc;
        
        Opportunity tmpOpp = CS_DataTest.createOpportunity(tmpAcc, 'Test Opportunity' ,simpleUser.Id);
        insert tmpOpp;
        
        cscfga__Product_Basket__c tmpProductBasket = CS_DataTest.createProductBasket(tmpOpp, 'Test basket', tmpAcc);  
        insert tmpProductBasket;
            
        AssignPriceBookToProductBasketImpl bpImpl = new AssignPriceBookToProductBasketImpl();
        bpImpl.AssignPriceBook(new Set<String>{tmpProductBasket.Id});
        
        Test.stopTest();
        }
	}

}