/**
 * @description       : provide code and use case coverages for FieldDescribeUtil apex class
 * @author            : mcubias
 * @last modified on  : 01-06-2022
 **/
@IsTest
public class TestFieldDescribeUtil {
	@IsTest
	static void getDependantValuesForPicklist() {
		Test.startTest();
		Map<String, List<String>> rejectReasonsByStatus = FieldDescribeUtil.getDependentOptions(Schema.Lead.Reject_Reason__c, Schema.Lead.Status);
		Test.stopTest();
		System.assert(!rejectReasonsByStatus.isEmpty(), 'Lead Reject Reason is dependant on Lead.Status, empty was returned.');
		System.assert(!rejectReasonsByStatus.get('Not Reached').isEmpty(), 'Not Reached lead status has dependent options, empty was returned.');
	}
}
