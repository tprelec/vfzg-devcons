/**
* Custom Endpoints Callouts ZG Test
*
* @author Petar Miletic
* @ticket SFDT-1270
* @since  22/07/2016
*/
@IsTest
public class ZG_EndpointResolverTest {

    @testSetup
    private static void setupTestData() {

        LG_GeneralTest.setupServiceRequestUrls();
    }

    @IsTest
    public static void getAddressCheckEndpointTest() {

        string uri = '';
        string params = '&houseFlatNumber=20&houseFlatExt=A&postcode=92361AB';

        ZG_EndpointResolver resolver = new ZG_EndpointResolver();

        Test.startTest();

        uri = resolver.getAddressCheckEndpoint('20', 'A', '92361AB');

        Test.stopTest();

        System.assertEquals('callout:LG_OracleAccessGateway/peal/api/b2b/addresses?cty=NL&chl=B2B_CATALYST_NL' + params, uri, 'Invalid Address Check Endpoint');
    }

    @IsTest
    public static void getAddressCheckEndpointWithCustomSettingTest() {

        ZG_EndpointResolver.fieldMap fieldMap = new ZG_EndpointResolver.fieldMap();
        fieldMap.sfFieldName = 'LG_VisitPostalCode__c';
        fieldMap.searchFilter = true;
        fieldMap.webServiceParam = 'postcode';
        fieldMap.jsonField = '';
        fieldMap.resultVisible = true;
        fieldMap.displayLabel = '';
        fieldMap.requiredParam = true;

        // Name is not populated on Account, this covers `isValuePopulated` if condition
        ZG_EndpointResolver.fieldMap fieldMap1 = new ZG_EndpointResolver.fieldMap();
        fieldMap1.sfFieldName = 'Name';
        fieldMap1.searchFilter = true;
        fieldMap1.webServiceParam = 'postcode';
        fieldMap1.jsonField = '';
        fieldMap1.resultVisible = true;
        fieldMap1.displayLabel = '';
        fieldMap1.requiredParam = true;
        ZG_EndpointResolver.mapAndDisplay mapAndDisplay = new ZG_EndpointResolver.mapAndDisplay();
        mapAndDisplay.fieldMap = new List<ZG_EndpointResolver.fieldMap> { fieldMap, fieldMap1 };

        string uri = '';

        ZG_EndpointResolver resolver = new ZG_EndpointResolver();

        Account objAccount = new Account(
            LG_VisitPostalCode__c = '92361AB'
        );

        Test.startTest();

        uri = resolver.getAddressCheckEndpoint(mapAndDisplay, objAccount);

        Test.stopTest();

        System.assertEquals('callout:LG_OracleAccessGateway/peal/api/b2b/addresses?cty=NL&chl=B2B_CATALYST_NL&postcode=92361AB', uri, 'Invalid Address Check Endpoint');
    }

    @IsTest
    public static void getAvailabilityCheckEndpointTest() {

        string uri = '';

        ZG_EndpointResolver resolver = new ZG_EndpointResolver();

        Test.startTest();

        uri = resolver.getAvailabilityCheckEndpoint('10000');

        Test.stopTest();

        System.assertEquals('callout:LG_OracleAccessGateway/peal/api/b2b/addresses/10000/details?cty=NL&chl=B2B_CATALYST_NL', uri, 'Invalid Availability Check Endpoint');
    }

    @IsTest
    public static void getAvailabilityCheckEndpointNegativeTest() {

        string uri = '';

        ZG_EndpointResolver resolver = new ZG_EndpointResolver();

        Test.startTest();

        try {
            uri = resolver.getAvailabilityCheckEndpoint('');
        } catch (LG_Exception e) {
            System.assertEquals('Address Id can not be null or empty', e.getMessage());
        }

        Test.stopTest();

        System.assertEquals('', uri, 'Endpoint must be null');
    }

    @IsTest
    public static void getOrderNEwEndpointTest() {

        LG_ServiceRequestUrl__c setting = new LG_ServiceRequestUrl__c();
        setting.Name = 'Test Setting';
        setting.LG_OrderRequestNew__c = 'LG_OracleAccessGateway/peal/api/orders/submit?cty={countryCode}&chl=DEALER';
        insert setting;

        string uri = '';

        ZG_EndpointResolver resolver = new ZG_EndpointResolver();

        Test.startTest();

        uri = resolver.getOrderNewEndpoint('92361AB');
        //ZG_EndpointResolver.fieldMap fm = new ZG_EndpointResolver.fieldMap();

        Test.stopTest();

       // System.assertEquals('callout:LG_OracleAccessGateway/peal/api/b2b/addresses/10000/details?cty=NL&chl=B2B_CATALYST_NL', uri, 'Invalid Availability Check Endpoint');
    }


}