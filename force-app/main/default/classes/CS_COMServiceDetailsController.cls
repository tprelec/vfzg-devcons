public with sharing class CS_COMServiceDetailsController {
    // private static String EXTERNAL_SERVICE_ID_ATTRIBUTE_NAME = 'External Service Id';
    private static String CDO_SERVICE_ID_ATTRIBUTE_NAME = 'CDO Service Id';
    
    @AuraEnabled
    public static String saveDeliveryComponentAttributes(String details){
        String returnValue = 'Details saved!';
        List<CS_ComDeliveryComponentWrapperDetail> attributes = (List<CS_ComDeliveryComponentWrapperDetail>)JSON.deserialize(details,List<CS_ComDeliveryComponentWrapperDetail>.class);
        List<SObject> sobjectsToUpdate = new List<SObject>();

        for (CS_ComDeliveryComponentWrapperDetail componentAttribute :attributes) {
            if (componentAttribute.name == CDO_SERVICE_ID_ATTRIBUTE_NAME) {
                sobjectsToUpdate.add(new Delivery_Component__c(
                        Id = componentAttribute.deliveryComponentId,
                        CDO_Service_ID__c = componentAttribute.attributeValue
                ));
            }
        }

        sobjectsToUpdate.sort();

        if (!sobjectsToUpdate.isEmpty()) {
            try {
                update sobjectsToUpdate;
            } catch (Exception e) {
                returnValue = e.getMessage();
            }
        }

        return returnValue;
    }
}