global class DWHDeleteKvkDeltaBatch implements Database.Batchable<sObject>{
    static Date today30 = Date.today().addDays(-7);

    public Database.QueryLocator start(Database.BatchableContext context) {
        return Database.getQueryLocator('Select ID from DWH_KvkDelta_SF_Interface__c where Processed__c = true AND LastModifiedDate < :today30');
    }

    public void execute(Database.BatchableContext context, List<SObject> records) {
        delete records;
    }

    global void finish(Database.BatchableContext BC) {
        Database.executeBatch(new DWHDeleteRelationBatch());
    }
}