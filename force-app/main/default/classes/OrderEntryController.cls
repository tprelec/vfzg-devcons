public without sharing class OrderEntryController {
	private static final String JSON_NAME = 'OrderEntryData.json';

	/**
	 * @description Get the initial data from JSON or initialize a new JSON
	 * @param  oppId oppId description
	 * @return       return description
	 */
	@AuraEnabled
	public static OrderEntryData getOrderEntryData(String oppId) {
		OrderEntryData data = new OrderEntryData();
		Attachment att = getDataAttachment(oppId);
		if (att != null) {
			data = (OrderEntryData) JSON.deserialize(att.Body.toString(), OrderEntryData.class);
			data.init();
		} else {
			Opportunity opp = [SELECT Id, AccountId FROM Opportunity WHERE Id = :oppId];
			data.opportunityId = opp.Id;
			data.accountId = opp.AccountId;
			data.init();
			insert new Attachment(Name = JSON_NAME, Body = Blob.valueOf(JSON.serializePretty(data, true)), ParentId = oppId);
		}
		return data;
	}

	/**
	 * @description Gets Data Attachment
	 * @param  oppId Opportunity ID.
	 * @return       Attachment if found, null otherwise
	 */
	private static Attachment getDataAttachment(Id oppId) {
		List<Attachment> atts = [SELECT Id, Body FROM Attachment WHERE ParentId = :oppId AND Name = :JSON_NAME];
		if (atts.isEmpty()) {
			return null;
		} else {
			return atts[0];
		}
	}

	/**
	 * @description Updates Data Attachment with provided state
	 * @param  oppId Opportunity ID.
	 * @param  state State JSON
	 * @return       true if success, false otherwise
	 */
	@AuraEnabled
	public static Boolean updateState(Id oppId, String state) {
		try {
			Attachment att = getDataAttachment(oppId);
			att.Body = Blob.valueOf(state);
			update att;
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * @description Gets info that is displayed in the header
	 * @param  oppId Opportunity
	 * @return       Header Info
	 */
	@AuraEnabled(cacheable=true)
	public static String getHeaderInfo(String oppId) {
		Opportunity opp = [SELECT Id, ContactId, Account.Name FROM Opportunity WHERE Id = :oppId];
		List<OpportunityContactRole> oppCont = [
			SELECT Id, Contact.Name
			FROM OpportunityContactRole
			WHERE opportunityId = :oppId AND IsPrimary = TRUE
		];
		String header = oppCont.isEmpty() ? ' ' : oppCont[0].Contact.Name;
		return opp.Account.Name + '  •  ' + header;
	}

	/**
	 * @description Gets Opportunity with list of provided fields
	 * @param  oppId  Opportunity Id
	 * @param  fields Fields
	 * @return        Opportunity
	 */
	@AuraEnabled
	public static Opportunity getOpportunity(Id oppId, List<String> fields) {
		String query = 'SELECT ';
		query += String.join(fields, ',');
		query += ' FROM Opportunity WHERE Id =: oppId';
		List<Opportunity> opportunityList = Database.query(query);
		if (!opportunityList.isEmpty()) {
			return opportunityList[0];
		}
		return null;
	}

	/**
	 * @description Updates Opportunity Stage
	 * @param  oppId Opportunity ID
	 * @param  value Stage Name
	 */
	@AuraEnabled
	public static void updateOpportunityStage(Id oppId, String value) {
		try {
			// Update with the flag
			Opportunity opp = [SELECT Id, StageName FROM Opportunity WHERE Id = :oppId];
			opp.StageName = value;
			opp.Allow_Stage_Change__c = true;
			update opp;

			// Clear the flag
			opp.Allow_Stage_Change__c = false;
			update opp;
		} catch (Exception ex) {
			throw new AuraException(ex.getMessage());
		}
	}

	/**
	 * @description Clones Opportunity together with JSON attachment
	 * @param  oppId   Opportunity ID
	 * @param  oppName New Opportunity Name
	 * @return         New Opportunity
	 */
	@AuraEnabled
	public static Opportunity cloneOrder(Id oppId, String oppName) {
		System.debug(oppId);
		System.debug(oppName);
		try {
			Opportunity opp = [SELECT Id, Name, AccountId, RecordTypeId, Journey_Type__c, LeadSource FROM Opportunity WHERE Id = :oppId];

			Opportunity newOpp = new Opportunity();
			newOpp.AccountId = opp.AccountId;
			newOpp.Name = oppName;
			newOpp.RecordTypeId = opp.RecordTypeId;
			newOpp.CloseDate = Date.today();
			newOpp.StageName = 'Awareness of interest';
			newOpp.Journey_Type__c = opp.Journey_Type__c;
			newOpp.LeadSource = opp.LeadSource;
			insert newOpp;

			OrderEntryData data = getOrderEntryData(oppId);
			data.opportunityId = newOpp.Id;
			data.lastStep = 'Select Product';
			data.init();

			Attachment att = new Attachment(Name = JSON_NAME, Body = Blob.valueOf(JSON.serializePretty(data)), ParentId = newOpp.Id);
			insert att;
			return newOpp;
		} catch (Exception e) {
			System.debug(e.getMessage());
			throw new OrderEntryException(e.getMessage());
		}
	}

	/**
	 * @description Create new Opportunity with OrderEntry Attachment
	 * @param  oppId Opportunity
	 * @return       Opportunity record
	 */
	@AuraEnabled
	public static Opportunity newOrder(Id oppId) {
		OrderEntryData oldData = new OrderEntryData();
		OrderEntryData data = new OrderEntryData();
		Id ziggoOppRecordTypeId = GeneralUtils.getRecordTypeIdByName('Opportunity', 'Ziggo');
		List<Attachment> att = [SELECT Id, Body FROM Attachment WHERE ParentId = :oppId AND Name = :JSON_NAME];

		Opportunity newOpp = new Opportunity();

		if (!att.isEmpty()) {
			oldData = (OrderEntryData) JSON.deserialize(att[0].Body.toString(), OrderEntryData.class);

			Account acc = [SELECT Id, Name FROM Account WHERE Id = :oldData.accountId];

			newOpp.RecordTypeId = ziggoOppRecordTypeId;
			newOpp.Name = acc.Name;
			newOpp.StageName = 'Awareness of interest';
			newOpp.CloseDate = Date.today();
			newOpp.AccountId = acc.Id;

			insert newOpp;

			data.opportunityId = newOpp.Id;
			data.accountId = oldData.accountId;
			data.primaryContactId = oldData.primaryContactId;

			List<OrderEntryData.OrderEntrySite> sites = new List<OrderEntryData.OrderEntrySite>();
			OrderEntryData.OrderEntrySite site = new OrderEntryData.OrderEntrySite();

			for (OrderEntryData.OrderEntrySite s : oldData.sites) {
				if (s.type == 'Installation') {
					site.siteId = s.siteId;
					site.type = s.type;
					sites.add(site);
				}
			}

			data.sites = sites;
			data.lastStep = 'Select Product';
			data.init();

			Attachment newAtt = new Attachment(
				Name = 'OrderEntryData.json',
				Body = Blob.valueOf(JSON.serializePretty(data)),
				ParentId = data.opportunityId
			);

			insert newAtt;
		}

		return newOpp;
	}
	@AuraEnabled
	public static Boolean isCommunity() {
		System.debug('uslo');
		System.debug('ispissite' + Site.getName());
		return Site.getName() != null;
	}

	private class OrderEntryException extends Exception {
	}
}
