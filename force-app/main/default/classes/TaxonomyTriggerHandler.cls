public with sharing class TaxonomyTriggerHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<Taxonomy__c> newProducts = (List<Taxonomy__c>) this.newList;
		Sequence seq = new Sequence('Taxonomy');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (Taxonomy__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
