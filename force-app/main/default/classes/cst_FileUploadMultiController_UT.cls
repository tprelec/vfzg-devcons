@isTest
public class cst_FileUploadMultiController_UT {
	@TestSetup
	static void makeData() {
		String recordtypeId = Schema.SObjectType.Case.getRecordTypeInfosByDeveloperName().get('CST_Incident').getRecordTypeId();
		String usrId = UserInfo.getUserId();
		Account acc = TestUtils.createAccount(GeneralUtils.currentUser);
		Contact con = TestUtils.createContact(acc);
		con.Email = '123456abcde@mail.com';
		update con;

		RecordType incidentRecordType = [SELECT Id FROM RecordType WHERE DeveloperName = 'CST_Incident'];
		Id incidentRecordTypeId = (incidentRecordType != null) ? incidentRecordType.Id : null;

		Case cs = new Case(
			status = 'New',
			CST_Sub_Status__c = 'Working on it',
			CST_End2End_Owner__c = usrId,
			Subject = 'CST Case',
			Priority = 'P4',
			OwnerId = usrId,
			AccountId = acc.Id,
			ContactId = con.Id,
			CST_Case_is_with_Team__c = 'Internal - Advisor',
			RecordTypeId = incidentRecordTypeId,
			Remedy_Ticket_No__c = '123'
		);
		insert cs;

		CST_External_Ticket__c remedy = new CST_External_Ticket__c(
			CST_Case__c = cs.id,
			CST_IncidentID__c = '565656',
			CST_Summary__c = 'test case',
			CST_Status__c = 'Assigned'
		);
		insert remedy;

		ContentVersion cv = new ContentVersion();
		cv.Title = 'Test Document';
		cv.PathOnClient = 'TestDocument.pdf';
		cv.VersionData = Blob.valueOf('Test Content');
		cv.IsMajorVersion = true;
		insert cv;

		//Get Content Documents
		Id conDocId = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id].ContentDocumentId;

		//Create ContentDocumentLink
		ContentDocumentLink cdl = new ContentDocumentLink();
		cdl.LinkedEntityId = remedy.Id;
		cdl.ContentDocumentId = conDocId;
		cdl.shareType = 'V';
		insert cdl;
	}

	@isTest
	static void getTicketInfo_Test() {
		Case testCase = [SELECT id FROM Case LIMIT 1];
		CST_External_Ticket__c remedyTicket = [SELECT id FROM CST_External_Ticket__c LIMIT 1];

		Test.startTest();
		try {
			String result = cst_FileUploadMultiController.getTicketInfo(testCase.id);

			System.assertNotEquals(result, null, 'result is null');
		} catch (DmlException e) {
			System.debug(e);
		}
		Test.stopTest();
	}

	@isTest
	static void changeStatusToCancelled_Test() {
		Test.startTest();
		changeStatus('Cancelled');
		Test.stopTest();
	}

	@isTest
	static void changeStatusToAssigned_Test() {
		Test.startTest();
		changeStatus('Assigned');
		Test.stopTest();
	}
	@isTest
	static void changeStatusToClosed_Test() {
		Test.startTest();
		changeStatus('Closed');
		Test.stopTest();
	}
	@isTest
	static void getFiles_Test() {
		System.debug('getFiles_Test');
		String csId = [SELECT Id FROM CST_External_Ticket__c LIMIT 1].Id;

		System.debug('csId ' + csId);

		ContentDocumentLink links = [
			SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility
			FROM ContentDocumentLink
			WHERE LinkedEntityId = :csId
			LIMIT 1
		];

		List<ContentVersion> contentVersions = [
			SELECT Id, Title, ContentDocumentId
			FROM ContentVersion
			WHERE ContentDocumentId = :links.ContentDocumentId
			LIMIT 1
		];

		System.debug('links ' + links);

		Test.startTest();
		List<String> idsList = new List<String>{ links.ContentDocumentId };
		System.debug('idsList: ' + idsList);

		String res = cst_FileUploadMultiController.getFiles(JSON.serialize(idsList));
		System.debug('getFiles_Test res: ' + res);

		Test.stopTest();
	}
	@isTest
	static void deleteFile_Test() {
		String csId = [SELECT Id FROM CST_External_Ticket__c LIMIT 1].Id;

		System.debug('csId ' + csId);

		List<ContentDocumentLink> links = [
			SELECT Id, LinkedEntityId, ContentDocumentId, IsDeleted, SystemModstamp, ShareType, Visibility
			FROM ContentDocumentLink
			WHERE LinkedEntityId = :csId
		];

		System.debug('links ' + links);

		List<ContentVersion> contentVersions = [
			SELECT Id, Title, ContentDocumentId
			FROM ContentVersion
			WHERE ContentDocumentId = :links[0].ContentDocumentId
			LIMIT 1
		];

		System.debug('contentVersions ' + contentVersions);

		System.debug(contentVersions);
		Test.startTest();
		cst_FileUploadMultiController.deleteFiles(contentVersions[0].ContentDocumentId);
		Test.stopTest();
	}
	@isTest
	static void buildWorkLogToRemedyXML_Test() {
		Case cs = [SELECT Id, casenumber FROM Case LIMIT 1];
		StaticResource sr = [SELECT Id, Name, ContentType, BodyLength, Body FROM StaticResource WHERE name = 'sldsOverrides'];

		cst_FileUploadMultiController.uploadInfo fileInfo = new cst_FileUploadMultiController.uploadInfo();
		fileInfo.fileName = '123.css';
		fileInfo.fileSize = 5432;
		fileInfo.fileContent = EncodingUtil.base64Encode(sr.Body);

		List<cst_FileUploadMultiController.uploadInfo> fileList = new List<cst_FileUploadMultiController.uploadInfo>();
		fileList.add(fileInfo);

		System.debug(cs);

		Test.startTest();
		cst_FileUploadMultiController.buildWorkLogToRemedyXML(cs.Id, '123', 'Assigned', fileList);
		Test.stopTest();
	}
	private static void changeStatus(String newStatus) {
		Case testCase = [SELECT id FROM Case LIMIT 1];

		Blob body = EncodingUtil.convertFromHex('4A4B4C');

		String comment = 'comment';

		StaticResource sr = [SELECT Id, Name, ContentType, BodyLength, Body FROM StaticResource WHERE name = 'sldsOverrides'];

		cst_FileUploadMultiController.uploadInfo fileInfo = new cst_FileUploadMultiController.uploadInfo();
		fileInfo.fileName = '123.jpg';
		fileInfo.fileSize = 5432;
		fileInfo.fileContent = EncodingUtil.base64Encode(sr.Body);

		List<cst_FileUploadMultiController.uploadInfo> fileList = new List<cst_FileUploadMultiController.uploadInfo>();
		fileList.add(fileInfo);

		String j = JSON.serializePretty(fileList);

		try {
			String result = cst_FileUploadMultiController.createWorkLog(testCase.id, comment, newStatus);
			System.assertNotEquals(result, null, 'result is null');
		} catch (DmlException e) {
			System.debug(e);
		}
	}
}
