public with sharing class AccountKeysService {
	public List<AccountKey> accountKeys;
	public String externalAccountQuery { get; set; }
	public String accountQuery { get; set; }

	public AccountKeysService() {
		this.externalAccountQuery =
			'SELECT' +
			' Id,' +
			' External_Source__c,' +
			' External_Account_Id__c,' +
			' Account__c,' +
			' Account__r.KVK_number__c' +
			' FROM External_Account__c WHERE External_Source__c = \'Unify\' ';

		this.accountQuery = 'SELECT  Id, KVK_number__c FROM Account ';
	}

	public List<AccountKey> getAccountKeys(AccountKey accountKey) {
		this.accountKeys = new List<AccountKey>();
		switch on accountKey.sourceSystem.toLowerCase() {
			when 'salescloud' {
				getAccountKeysForSalesCloud(accountKey.value);
			}
			when 'kvk' {
				getAccountKeysForKvK(accountKey.value);
			}
			when 'unify' {
				getAccountKeysForUnify(accountKey.value);
			}
		}
		return this.accountKeys;
	}

	private void getAccountKeysForSalesCloud(String value) {
		this.accountQuery += 'WHERE Id = :value';
		this.getAccountKeysForAccount(value);
	}

	private void getAccountKeysForKvK(String value) {
		this.accountQuery += 'WHERE KVK_number__c = :value';
		this.getAccountKeysForAccount(value);
	}

	private void getAccountKeysForAccount(String value) {
		List<Account> accounts = Database.query(this.accountQuery);
		if (accounts.size() > 0) {
			this.createAccountKeysFromAccount(accounts[0]);
			this.getExternalAccountKeys(accounts[0]);
		}
	}

	private void getExternalAccountKeys(Account acc) {
		String value = acc.Id;
		this.externalAccountQuery += 'AND Account__c = :value';
		List<External_Account__c> externalAccounts = Database.query(this.externalAccountQuery);
		this.createAccountKeysFromExternalAccounts(externalAccounts);
	}

	private void getAccountKeysForUnify(String value) {
		this.externalAccountQuery += ' AND External_Account_Id__c = :value';
		List<External_Account__c> externalAccounts = Database.query(this.externalAccountQuery);
		this.createAccountKeysFromExternalAccounts(externalAccounts);
		if (externalAccounts.size() > 0) {
			this.createAccountKeysFromAccount(externalAccounts[0].Account__r);
		}
	}

	private void createAccountKeysFromExternalAccounts(List<External_Account__c> externalAccounts) {
		for (External_Account__c externalAccount : externalAccounts) {
			AccountKey unify = new AccountKey('Unify', externalAccount.External_Account_Id__c);
			this.accountKeys.add(unify);
		}
	}

	private void createAccountKeysFromAccount(Account account) {
		if (account != null) {
			AccountKey salesCloud = new AccountKey('SalesCloud', account.Id);
			this.accountKeys.add(salesCloud);
			if (account.KVK_number__c != null) {
				AccountKey kvk = new AccountKey('KvK', account.KVK_number__c);
				accountKeys.add(kvk);
			}
		}
	}
}