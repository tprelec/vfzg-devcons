@isTest
public class TestRetentionOpportunityController {
    @isTest
    public static void testRetentionOpportunityController() {
        // create a VF_Contract and all objects needed for that
        User owner = TestUtils.createAdministrator();
        Account acct = TestUtils.createAccount(owner);
        //Pricebook2 pricebook = TestUtils.createPricebook(true);
        Opportunity opp = TestUtils.createOpportunity(acct, Test.getStandardPricebookId() );
        VF_Contract__c contract = TestUtils.createVFContract(acct, opp);

        test.startTest();

        PageReference pageRef = Page.RetentionOpportunity;

        // store the test amount
        Decimal testAmount = contract.Net_Revenues__c;

        // put the id of the created contract in the page paramenter
        pageRef.getParameters().put('vf_contract_id', contract.id);

        Test.setCurrentPage(pageRef);

        Opportunity op = new Opportunity();
        //instantiate the standard controller
        Apexpages.StandardController sc = new Apexpages.standardController(op);

        //Instantiate and construct the controller class and pass the standard controller to the extension.   
        RetentionOpportunityController retentionOppcontroller = new RetentionOpportunityController(sc);

        // test if all fields are set
        system.assertEquals(testAmount, op.Amount);
        system.assertEquals(acct.Id, op.AccountId);
        system.assertEquals('Existing Business', op.Type);
        system.assertEquals(contract.Id, op.Retention_Contract_VF__c);
        system.assertEquals(acct.OwnerId, op.OwnerId);



        //change the stage field
        op.StageName = 'Closing';
        retentionOppcontroller.resetProbability();
        //check if the probability is also filled now with the correct probability
        List<OpportunityStage> OppStage = [Select DefaultProbability From OpportunityStage where MasterLabel = 'Closing'];
        Decimal prob = OppStage.get(0).DefaultProbability;
        system.assertEquals(prob, op.Probability);

        test.stopTest();
    }
}