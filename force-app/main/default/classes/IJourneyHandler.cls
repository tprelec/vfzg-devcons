public without sharing class IJourneyHandler {
	public static Boolean checkVisibility(String handlerName, String accountId) {
		IJourney iJourneyInstance = (IJourney) Type.forName(handlerName).newInstance();
		return iJourneyInstance.checkVisibility(accountId);
	}

	@AuraEnabled
	public static Map<String, Object> handleClick(String handlerName, String accountId) {
		IJourney iJourneyInstance = (IJourney) Type.forName(handlerName).newInstance();
		return iJourneyInstance.handleClick(accountId);
	}
}