public with sharing class RulePredicateAssociationHandler extends TriggerHandler {
	public override void BeforeInsert() {
		increaseExternalId();
	}

	public void increaseExternalId() {
		List<csclm__Rule_Predicate_Association__c> newProducts = (List<csclm__Rule_Predicate_Association__c>) this.newList;
		Sequence seq = new Sequence('Rule/Predicate Association');
		if (seq.canBeUsed()) {
			seq.startMutex();
			for (csclm__Rule_Predicate_Association__c o : newProducts) {
				o.ExternalID__c = seq.incr(1, 8, false);
			}
			seq.endMutex();
		}
	}
}
