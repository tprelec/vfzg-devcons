@isTest
private class TestSAPBatchExecuteController {

    @isTest
    static void TestSAPBatchExecuteController() {
        TestUtils.createSAPHardwareFieldMappings();
        TestUtils.createSync('SAP_Hardware_Information__c -> cspmb__Price_Item__c', 'Item_Description__c', 'Name');
        pageReference page = new SAPBatchExecuteController(new ApexPages.standardController(new Account())).callBatch();
        System.assertEquals(new pageReference('/a0U').getUrl(), page.getUrl());
    }
}