@SuppressWarnings(
	'PMD.StdCyclomaticComplexity,PMD.CyclomaticComplexity,PMD.ExcessivePublicCount'
) //TO-DO: refactor legacy code cyclomatic complexity (technical debt)
public without sharing class ClaimContractController {
	private final SObject parent;
	List<VF_Contract__c> contracts;
	List<Contracted_Products__c> details;
	private String vcIdOld;
	@TestVisible
	List<VF_Contract__c> selectedVFs = new List<VF_Contract__c>();
	public list<CVFContractc> vfContractList { get; set; }
	private User currentUser { get; set; }
	private List<Dealer_Information__c> currentDealerInfo { get; set; }
	Map<Id, User> ownersMap { get; set; }
	Map<Id, List<VF_Contract__c>> ownerContracts { get; set; }
	public String vcId { get; set; }
	public Boolean isDetail { get; set; }
	public Boolean isSelected { get; set; }
	public String approvalComment { get; set; }
	public Boolean selected { get; set; }

	public Boolean isBPM { get; set; }
	public Claimed_Contract_Approval_Request__c dealerCR { get; set; }

	@SuppressWarnings('PMD.PropertyNamingConventions') // VFCDetail used in apex page
	public String VFCDetail { get; set; }

	public ClaimContractController(ApexPages.StandardController controller) {
		parent = controller.getRecord();
		this.currentUser = [SELECT Id, UserType, ManagerId, Manager.ManagerId, AccountId, Partner_User__c FROM User WHERE Id = :UserInfo.getUserId()];
		this.currentDealerInfo = new List<Dealer_Information__c>();
		this.ownerContracts = new Map<Id, List<VF_Contract__c>>();
		getCurrentUserBPM();
		this.dealerCR = new Claimed_Contract_Approval_Request__c();
		if (!isBPM) {
			this.currentDealerInfo = [SELECT Id FROM Dealer_Information__c WHERE ContactUserId__c = :UserInfo.getUserId() LIMIT 1];
		}
	}

	@SuppressWarnings('PMD.CyclomaticComplexity') //TO-DO: refactor legacy code cyclomatic complexity (technical debt)
	public List<CVFContractc> getContracts() {
		Account acc = [SELECT Id, Ziggo_Customer__c, Invoiced_Vodafone_Fixed_Converged__c, Vodafone_Customer__c FROM Account WHERE Id = :parent.Id];

		// don't show any VF Contracts
		if (acc.Ziggo_Customer__c && !acc.Invoiced_Vodafone_Fixed_Converged__c && !acc.Vodafone_Customer__c) {
			return new List<CVFContractc>();
		}

		if (vfContractList == null && (!this.currentDealerInfo.isEmpty() || isBPM)) {
			vfContractList = new List<CVFContractc>();
			Set<Id> ownerIds = new Set<Id>();

			// get contracts that are not already in the claim contract process and not in SoHo channel
			for (VF_Contract__c c : [
				SELECT
					Id,
					Dealer_Information__c,
					Dealer_Information__r.Name,
					Dealer_Information__r.Contact__r.Account.OwnerId,
					Claimed_Contract_Approval_Request__c,
					Name,
					OwnerId,
					Owner.ProfileId,
					Account__c,
					Account__r.SF1_Direct_Indirect_Indicator__c,
					Account__r.Ziggo_Customer__c,
					Account__r.Invoiced_Vodafone_Fixed_Converged__c,
					Account__r.Vodafone_Customer__c,
					(
						SELECT Name, Product_Category__r.Product_Family__c, Product_Category__r.Portfolio__c, CTN__c, RecordType.Name
						FROM Assets__r
						WHERE (RecordType.Name = 'CTN' OR CTN__c != NULL) AND CTN_Status__c = 'Active'
					)
				FROM VF_Contract__c
				WHERE
					Account__c = :parent.Id
					AND Dealer_Information__r.Sales_Channel__c != 'SoHo'
					AND Dealer_Information__c != :dealerCR.Dealer_Info__c
					AND OwnerId != :this.currentUser.Id
					AND Claimed_Contract_Approval_Request__c NOT IN (
						SELECT Id
						FROM Claimed_Contract_Approval_Request__c
						WHERE
							First_Approval__c = 'pending'
							OR Second_Approval__c = 'pending'
							OR Third_Approval_Status__c = 'pending'
							OR (First_Approval__c = 'rejected'
							AND (Second_Approval__c = NULL
							OR Second_Approval__c = 'Not Started'))
							OR (First_Approval__c = 'rejected'
							AND Second_Approval__c = 'rejected'
							AND (Third_Approval_Status__c = NULL
							OR Third_Approval_Status__c = 'Not Started'))
					)
				ORDER BY Name DESC NULLS LAST
			]) {
				if (c.Assets__r.size() > 0) {
					vfContractList.add(new CVFContractc(c));
				}
				ownerIds.add(c.OwnerId);

				if (!this.ownerContracts.containsKey(c.OwnerId)) {
					this.ownerContracts.put(c.OwnerId, new List<VF_Contract__c>());
				}
				this.ownerContracts.get(c.OwnerId).add(c);
			}

			this.ownersMap = new Map<Id, User>([SELECT Id, UserType, ManagerId, Manager.ManagerId, Partner_User__c FROM User WHERE Id IN :ownerIds]);
			System.debug(LoggingLevel.INFO, ' list ' + vfContractList);
		} else if (this.currentDealerInfo.isEmpty() && !isBPM) {
			ApexPages.addMessage(
				new ApexPages.message(
					ApexPages.Severity.ERROR,
					'You do not have a Dealer Information Record please consult your administrator for help'
				)
			);
		}
		return vfContractList;
	}
	@SuppressWarnings(
		'PMD.MethodNamingConventions,PMD.ApexXSSFromURLParam'
	) //TO-DO: refactor legacy code APEX CLASS AND VFPage XSS Attacks (technical debt)
	public void ShowDetail() {
		vcId = ApexPages.currentPage().getParameters().get('VFCDetail');
		if (vcId == vcIdOld && isDetail == true) {
			isDetail = false;
		} else {
			isDetail = true;
		}
		system.debug(LoggingLevel.INFO, this.vcId);
		system.debug(LoggingLevel.INFO, 'isDetail ' + isDetail);
		vcIdOld = vcId;
	}

	@SuppressWarnings('PMD.StdCyclomaticComplexity,PMD.CyclomaticComplexity') //TO-DO: refactor legacy code cyclomatic complexity (technical debt)
	public PageReference processSelected() {
		if (isBPM && dealerCR.Dealer_Info__c == null) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, System.Label.ClaimContract_SelectDealer));
			for (CVFContractc cCon : getContracts()) {
				if (cCon != null && cCon.selected == true) {
					cCon.selected = false;
				}
			}
			return null;
		}

		Boolean isPartnerOwned = false;

		for (CVFContractc cCon : getContracts()) {
			if (cCon != null && cCon.selected == true) {
				selectedVFs.add(cCon.con);
				if (GeneralUtils.isPartnerUser(ownersMap.get(cCon.con.OwnerId))) {
					isPartnerOwned = true;
				}
			}
		}

		if (selectedVFs.isEmpty()) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, ('Please select a contract to claim')));
			return null;
		}

		System.debug(LoggingLevel.INFO, 'These are the selected Contracts...');
		List<String> contractNames = new List<String>();
		for (VF_Contract__c con : selectedVFs) {
			system.debug(LoggingLevel.INFO, con.Name);
			contractNames.add(con.Name);
		}
		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, (contractNames + ' are selected')));

		if (isPartnerOwned || GeneralUtils.isPartnerUser(this.currentUser)) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, (System.Label.ClaimContract_PartnerInfo)));
		}

		contractNames = null;
		isSelected = true;
		return null;
	}

	@SuppressWarnings('PMD.StdCyclomaticComplexity,PMD.CyclomaticComplexity') //TO-DO: refactor legacy code cyclomatic complexity (technical debt)
	public void selectAll() {
		if (isBPM && dealerCR.Dealer_Info__c == null) {
			selected = false;
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, System.Label.ClaimContract_SelectDealer));
			return;
		}

		Boolean isPartnerOwned = false;
		List<CVFContractc> contracts = getContracts();
		List<String> contractNames = new List<String>();
		for (CVFContractc cCon : contracts) {
			isSelected = selected;
			if (cCon != null) {
				cCon.selected = selected;
				if (selected) {
					selectedVFs.add(cCon.con);
				}
				if (cCon.con != null) {
					contractNames.add(cCon.con.Name);
					if (GeneralUtils.isPartnerUser(ownersMap.get(cCon.con.OwnerId))) {
						isPartnerOwned = true;
					}
				}
			}
		}

		if (selected) {
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM, (contractNames + ' are selected')));
			if (isPartnerOwned) {
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, (System.Label.ClaimContract_PartnerInfo)));
			}
		} else {
			selectedVFs = new List<VF_Contract__c>();
			contractNames = new List<String>();
			ApexPages.getMessages().clear();
		}
	}

	public PageReference cancelSelected() {
		isSelected = false;
		selectedVFs = new List<VF_Contract__c>();
		vfContractList = null;
		selected = false;
		return null;
	}
	@SuppressWarnings('PMD.StdCyclomaticComplexity,PMD.CyclomaticComplexity') //TO-DO: refactor legacy code cyclomatic complexity (technical debt)
	public PageReference confirmSelected() {
		if (isBPM && (dealerCR.Dealer_Info__c == null || (dealerCR.Dealer_Info__c != null && dealerCR.Dealer_Info__r.Contact__c == null))) {
			if (dealerCR.Dealer_Info__c == null) {
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, (System.Label.ClaimContract_SelectDealer)));
			}
			if (dealerCR.Dealer_Info__c != null && dealerCR.Dealer_Info__r.Contact__c == null) {
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, (System.Label.ClaimContract_DealerWithoutContact)));
			}
			return null;
		}

		isSelected = false;
		Set<Id> owners = new Set<Id>();
		for (VF_Contract__c c : selectedVFs) {
			owners.add(c.OwnerId);
		}

		List<Claimed_Contract_Approval_Request__c> contractsToClaim = new List<Claimed_Contract_Approval_Request__c>();
		for (Id ownerId : owners) {
			User contractOwner = ownersMap.get(ownerId);
			Claimed_Contract_Approval_Request__c ccReq = new Claimed_Contract_Approval_Request__c();
			ccReq.Comment__c = approvalComment;
			ccReq.OwnerId = (isBPM && dealerCR.Dealer_Info__c != null) ? dealerCR.Dealer_Info__r.Contact__r.UserId__c : this.currentUser.Id;
			ccReq.Account__c = parent.Id;
			ccReq.Old_Owner__c = contractOwner.Id;
			ccReq.First_Approval__c = 'Not Started';
			ccReq.Second_Approval__c = 'Not Started';
			ccReq.Third_Approval_Status__c = 'Not Started';

			ccReq.Approver__c = contractOwner.Id;
			if (GeneralUtils.isPartnerUser(contractOwner)) {
				VF_Contract__c vfc = this.ownerContracts.get(ownerId)[0];
				ccReq.Secondary_Approver__c = 'Partner Account Manager';
				if (vfc != null) {
					ccReq.Second_Approver__c = vfc.Dealer_Information__r.Contact__r.Account.OwnerId;
				}
			} else {
				ccReq.Secondary_Approver__c = 'Sales Manager';
				ccReq.Second_Approver__c = contractOwner.ManagerId;
			}

			// Commercial_Management_Contract_Ownership queue
			ccReq.Third_Approval__c = 'Commercial Management';
			// set 3rd approver if they have same director
			if (!isBPM && !GeneralUtils.isPartnerUser(contractOwner) && contractOwner.Manager.ManagerId == this.currentUser.Manager.ManagerId) {
				ccReq.Third_Approver__c = contractOwner.Manager.ManagerId;
				ccReq.Third_Approval__c = 'Sales Director';
			}

			contractsToClaim.add(ccReq);
		}

		//27-07-2019 - juan.cardona@vasscompany.com : The code below
		// allows partner and non partner users to create Claimed Contract Approval Request.
		System.debug(LoggingLevel.INFO, 'contracts to claim ' + contractsToClaim);

		try {
			insert contractsToClaim;
		} catch (Exception ex) {
			LoggerService.log(ex, new Set<Id>{ parent.Id });
			LoggerService.log(Logginglevel.ERROR, JSON.serializePretty(contractsToClaim));
			ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, System.Label.ClaimContract_ErrorInsertingContractsToClaim + ex));
			return cancelSelected();
		}

		for (Claimed_Contract_Approval_Request__c ccc : contractsToClaim) {
			for (VF_Contract__c contract : selectedVFs) {
				if (contract.OwnerId == ccc.Old_Owner__c) {
					contract.Claimed_Contract_Approval_Request__c = ccc.Id;
				}
			}
		}
		System.debug(LoggingLevel.INFO, 'SelectedVFs ' + selectedVFs);
		update selectedVFs;

		approvalComment = null;
		dealerCR.Dealer_Info__c = null;
		ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Claim contract process has started, you can now drink some coffee.'));

		return cancelSelected();
	}

	public void getCurrentUserBPM() {
		String profileName = GeneralUtils.ProfileIdToProfileName.get(UserInfo.getProfileId());
		isBPM = profileName.contains('Business Partner Manager');
	}

	public void setCurrentDealerInfo() {
		if (dealerCR.Dealer_Info__c != null) {
			this.currentDealerInfo = [
				SELECT Id, Contact__c, Contact__r.UserId__c
				FROM Dealer_Information__c
				WHERE Id = :dealerCR.Dealer_Info__c
				LIMIT 1
			];
			dealerCR.Dealer_Info__r = this.currentDealerInfo[0];
			if (this.currentDealerInfo[0].Contact__c == null) {
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, System.Label.ClaimContract_DealerWithoutContact));
			}
		} else {
			this.currentDealerInfo = new List<Dealer_Information__c>();
		}
		cancelSelected();
		getContracts();
	}

	public class CVFContractc {
		public VF_Contract__c con { get; set; }
		public Boolean selected { get; set; }

		public CVFContractc(VF_Contract__c c) {
			con = c;
			selected = false;
		}

		public CVFContractc(VF_Contract__c c, boolean sel) {
			con = c;
			selected = sel;
		}
	}
}
