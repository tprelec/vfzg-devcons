@isTest
public with sharing class TestNewOpportunityContainerController {

    @isTest
    public static void testGetRecordTypes() {

        Test.startTest();
            List<NewOpportunityContainerController.RecordTypePicklistOption> recTypes = NewOpportunityContainerController.getRecordTypes();
        Test.stopTest();

        System.assert(recTypes.size() != 0, 'The controller should return a list of record types');
    }
}