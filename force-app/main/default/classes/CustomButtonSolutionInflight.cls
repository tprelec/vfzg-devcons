global with sharing class CustomButtonSolutionInflight extends csbb.CustomButtonExt {
	public String performAction(String basketId) {
		try {
			System.debug('Starting inflight for basket: ' + basketId);
			csord__Solution__c changingSolution = [SELECT Id, Name FROM csord__Solution__c WHERE cssdm__product_basket__c = :basketId];
			String responseString = cssmgnt.API_1.createInflightRequest(changingSolution.Id);
			System.debug('Step 1 done with response: ' + responseString);

			Map<String, Object> responseObjMap = (Map<String, Object>) JSON.deserializeUntyped(responseString);
			Map<String, String> resultMap = new Map<String, String>();

			for (String objKey : responseObjMap.keyset()) {
				resultMap.put(objKey, (String) responseObjMap.get(objKey));
			}

			System.debug('resultMap: ' + resultMap);
			performCallout(resultMap);
			System.debug('processing done');

			PageReference redirectPage = new PageReference('/' + resultMap.get('targetBasketId'));
			return '{"status":"ok","redirectURL":"' + redirectPage.getUrl() + '"}';
		} catch (Exception ex) {
			System.debug('Error occurred while starting Inflight change ' + ex.getMessage());
		}
		return '{"status":"error"}';
	}

	@future(callout=true)
	static void performCallout(Map<String, String> inputMap) {
		cssmgnt.SolutionMACDController.createHerokuMACDSolution(inputMap);
	}
}
