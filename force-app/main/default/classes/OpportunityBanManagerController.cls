public without sharing class OpportunityBanManagerController
{
    private final Opportunity opp;

    public BanManagerData   theBmData       { get; private set; }
    public Boolean          banIsEditable   { get; private set; }
    public Boolean          refreshPage     { get; set; }
    

    public OpportunityBanManagerController(ApexPages.StandardController stdController)
    {
        this.opp = [Select Id, AccountId, Ban__r.Name, Ban__c From Opportunity Where Id = :stdController.getId()];

        Boolean fixed = false;
        Boolean onenet = false;

        for (OpportunityLineItem oli : [
                Select Id, Fixed_Mobile__c, Proposition__c, Opportunity.Hidden_Fixed_Products__c
                From OpportunityLineItem
                Where OpportunityId = :this.opp.Id ])
        {
            if (oli.Proposition__c != null && oli.Proposition__c.contains('One Net'))
            {
                onenet = true;
            }
            else if (oli.Fixed_Mobile__c == 'Fixed' || oli.opportunity.Hidden_Fixed_Products__c > 0)
            {
                // any order under an opportunity that has fixed products should follow the 'fixed' rules. Even if the order is mobile.
                fixed = true;
            }
        }

        theBmData = new BanManagerData(new Set<Id>{this.opp.AccountId},this.opp.Ban__c,fixed,onenet,false,false);

        // fetch the contract status to determine if the BAN should be editable
        banIsEditable = true;
        for (Order__c o : [Select Id, Status__c From Order__c Where VF_Contract__r.Opportunity__c = :this.opp.Id])
        {
            if (o.Status__c == 'Submitted' || o.Status__c == 'Accepted')
            {
                banIsEditable = false;
            }
        }

        for (VF_Contract__c vfc : [
                Select Id, Implementation_Status__c
                From VF_Contract__c
                Where Opportunity__c = :this.opp.Id ])
        {
            banIsEditable = false;
        }
        //System.debug(banIsEditable);

        refreshPage = false;
    }


    public PageReference updateBan()
    {
        theBmData.errorText = null;

        Savepoint sp = Database.setSavepoint();

        Ban__c banForOpportunity = new Ban__c();

        refreshPage = true;

        try
        {
            banForOpportunity = theBmData.getBan(this.opp.AccountId);
            if(banForOpportunity != null){
                this.opp.Ban__c = banForOpportunity.Id;
            } else {
                this.opp.Ban__c = null;
            }
            update this.opp;
            theBmData.banChanged = false;
            theBmData.infoText = 'Ban updated';
        }
        catch (Exception e)
        {
            theBmData.errorText = e.getMessage();
            Database.rollback(sp);
        }

        return null;
    }


    public PageReference cancelUpdateBan()
    {
        theBmData.errorText = null;
        theBmData.banChanged = false;
        // requery the current ban to get the correct value
        theBmData.banSelected = [Select Ban__r.Name From Opportunity Where Id = :this.opp.Id].Ban__r.Name;
        return null;
    }
}