@isTest
private class TestLeadConvertPage {
    @TestSetup 
    static void setup() {
        Account newAccount = new Account(
            name = 'Test Account'
        );
        Insert newAccount;
        
        Lead newLead1 = new Lead(
            Company = 'Test Account', LastName= 'Test Lead',
            LeadSource = 'Web',  
            Status = 'Closed - Converted',
            Town__c='testtown',
            House_Number__c='1',
            Salutation='Mr.',
            Street__c='teststreet',
            Postcode_number__c='1234',
            Postcode_char__c='AB',
            Visiting_Housenumber1__c=1);
        
        Insert newLead1;
        
        Lead newLead2 = new Lead(
            Company = 'Test Account', LastName= 'KVK Test Lead',
            LeadSource = 'New KVK',  
            Status = 'Closed - Converted',
            Town__c='testtown',
            House_Number__c='1',
            Salutation='Mr.',
            Street__c='teststreet',
            Postcode_number__c='1234',
            Postcode_char__c='AB',
            Visiting_Housenumber1__c=1,
            KVK_number__c='11111111');
        
        Insert newLead2;        
    }
    
    static testMethod void newKVK() {		
        Lead ld = [SELECT Id,LastName,Company,LeadSource FROM Lead WHERE LastName='KVK Test Lead' AND LeadSource = 'New KVK' LIMIT 1]; 
        system.debug('The value of lead source' +ld.LeadSource);
        ApexPages.StandardController stdController = new ApexPages.StandardController(ld);
        leadConvertController leadController = new leadConvertController(stdController);  
        leadcontroller.leadToConvert = ld;
        
        leadController.getMyComponentController();
        leadController.getmyDescriptionComponentController();
        leadController.getmyTaskComponentController();
        leadController.getThis();
        
        leadController.setComponentController(new leadConvertCoreComponentController());
        leadController.setDescriptionComponentController(new leadConvertTaskDescComponentController());
        leadController.setTaskComponentController(new leadConvertTaskInfoComponentController() );
        
        system.assert(leadController.myTaskComponentController != null);
        leadController.myTaskComponentController.taskID.Subject = 'TEST TASK';
        leadController.myTaskComponentController.taskID.Priority = 'High';
        leadController.myTaskComponentController.taskID.Status = 'Not Started';
        leadController.myTaskComponentController.taskID.ActivityDate = system.today();
        leadController.myComponentController.leadConvert = ld; 
        leadController.myComponentController.leadConvert.Status = 'Accept';
        leadController.myComponentController.leadConvert.KVK_number__c='55884499';
        leadController.myDescriptionComponentController.sendNotificationEmail = true;
        leadController.myComponentController.sendOwnerEmail = true;
        leadController.myComponentController.doNotCreateOppty = false;
        leadcontroller.leadToConvert.leadSource = 'New KVK';
        
        Test.StartTest();
        leadController.olbicoConvertLead();
        ld = [SELECT ConvertedAccountId FROM Lead WHERE Id = :ld.Id];
        System.assertNotEquals(null, ld.ConvertedAccountId, 'Expected lead to be converted.'); 
        PageReference pageRef = Page.leadConvertPage;
        pageRef.getParameters().put('id', ld.ConvertedAccountId);
        Test.setCurrentPage(pageRef); 
        leadController.PrintErrors(new List<Database.Error>());
        leadController.PrintError('Test');
        Test.StopTest(); 
    }
    
    static testMethod void testobligo() {
    
    Lead ld = [SELECT Id,LastName,Company,OwnerId,Street__c,Visiting_Housenumber1__c,Town__c,
                   Visiting_postalcode__c,Country,Status FROM Lead WHERE LastName='Test Lead' LIMIT 1];
        System.assertNotEquals(null, ld);
        Account acct = [SELECT Id FROM Account WHERE Name='Test Account' LIMIT 1]; 
        System.assertNotEquals(null, acct);
        ApexPages.StandardController stdController = new ApexPages.StandardController(ld);
        leadConvertController leadController = new leadConvertController(stdController);
        
        leadcontroller.leadToConvert = ld;
        
        leadController.getMyComponentController();
        leadController.getmyDescriptionComponentController();
        leadController.getmyTaskComponentController();
        leadController.getThis();
        
        PageControllerBase pgBase = new PageControllerBase();
        pgBase.getMyComponentController();
        pgBase.getmyDescriptionComponentController();
        pgBase.getmyTaskComponentController();
        pgBase.getThis();
        pgBase.getmyReminderComponentController();
        
        ComponentControllerBase compBase = new ComponentControllerBase();
        compBase.pageController = pgBase;
        compBase.pageControllerDescription = pgBase;
        compBase.pageControllerReminder = pgBase;
        compBase.pageControllerTask = pgBase;
        
        
        leadController.setComponentController(new leadConvertCoreComponentController());
        leadController.setDescriptionComponentController(new leadConvertTaskDescComponentController());
        leadController.setTaskComponentController(new leadConvertTaskInfoComponentController() );
        
        system.assert(leadController.myTaskComponentController != null);
        leadController.myTaskComponentController.taskID.Subject = 'TEST TASK';
        leadController.myTaskComponentController.taskID.Priority = 'High';
        leadController.myTaskComponentController.taskID.Status = 'Not Started';
        leadController.myTaskComponentController.taskID.ActivityDate = system.today();
        leadController.myComponentController.selectedAccount = acct.Id;
        leadController.myComponentController.leadConvert = ld;
        
        
        Contact contactID = leadController.myComponentController.contactID;
        leadController.myComponentController.doNotCreateOppty = false;
        List<SelectOption> leadStatuses = leadController.myComponentController.LeadStatusOption;
        
        Opportunity opportunityID = leadController.myComponentController.opportunityID;
        //leadController.reminder = true;
        String reminderTime = leadController.myTaskComponentController.remCon.reminderTime;
        List<SelectOption> timeOptions = leadController.myTaskComponentController.remCon.ReminderTimeOption;
        leadController.myDescriptionComponentController.sendNotificationEmail = true;
        leadController.myComponentController.sendOwnerEmail = true;
        List<SelectOption> priorityOptions = leadController.myTaskComponentController.TaskPriorityOption;
        List<SelectOption> statusOptions = leadController.myTaskComponentController.TaskStatusOption;
        
        Test.StartTest();
        leadController.olbicoConvertLead();
        PageReference pageRef = Page.OlbicoCdpConvertLead;
        pageRef.getParameters().put('leadId', ld.Id);
        Test.setCurrentPage(pageRef); 
        leadController.PrintErrors(new List<Database.Error>());
        leadController.PrintError('Test');  
        leadController.convertLead();       
        Test.StopTest();  
    }
    
    static testMethod void testconvertDateToString(){
        Lead ld = [SELECT Id,Name FROM Lead WHERE LastName='Test Lead' LIMIT 1];
        System.assertNotEquals(null, ld);
        Date d = Date.newInstance(2016, 12, 9);
        leadConvertController.convertDateToString(d);
        
    }
    
    static testMethod void testDatetoDateTimeAMPM() {
        Lead ld = [SELECT Id,Name FROM Lead WHERE LastName='Test Lead' LIMIT 1];
        System.assertNotEquals(null, ld);        
        ApexPages.StandardController stdController = new ApexPages.StandardController(ld);
        leadConvertController leadController = new leadConvertController(stdController);       
        Profile p = [select id from profile where name='Standard User'];
        Date d = Date.newInstance(2016, 12, 9);
        String t = '9:30 AM';
        leadConvertController.convertToDatetime(d,t);
    }

     static testMethod void testDatetoDateTime() {
        Lead ld = [SELECT Id,Name FROM Lead WHERE LastName='Test Lead' LIMIT 1];
        System.assertNotEquals(null, ld);        
        ApexPages.StandardController stdController = new ApexPages.StandardController(ld);
        leadConvertController leadController = new leadConvertController(stdController);       
        Profile p = [select id from profile where name='Standard User'];
        Date d = Date.newInstance(2016, 12, 9);
        String t = '21:30';
        leadConvertController.convertToDatetime(d,t);
    }                
}