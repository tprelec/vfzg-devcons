/**
 * @description			This is the test class that contains the tests for the BICC BAN Information Batch
 * @author				Ferdinand Bondt
 */
 
@isTest
private class TestBiccBanInformationBatch {

	static testMethod void myUnitTest() {

		//Data preparation
		TestUtils.autoCommit = false;

		list<Field_Sync_Mapping__c> fsmlist = new list<Field_Sync_Mapping__c>();
			fsmlist.add(TestUtils.createSync('BICC_BAN_Information__c -> Ban__c', 'BAN_name__c', 'BAN_Name__c'));
			fsmlist.add(TestUtils.createSync('BICC_BAN_Information__c -> Ban__c', 'BAN__c', 'Name'));
			fsmlist.add(TestUtils.createSync('BICC_BAN_Information__c -> Ban__c', 'COC_Number__c', 'KVK_number__c'));
			fsmlist.add(TestUtils.createSync('BICC_BOA_Information__c -> Ban__c', 'BAN__c', 'Name'));
			fsmlist.add(TestUtils.createSync('BICC_Vodacom_Data_BAN__c -> Account_Revenue__c', 'Billing_Account_Num__c', 'AR_Billing_Account_Num__c'));
			fsmlist.add(TestUtils.createSync('BICC_BAN_CUST_VALUE__c -> Ban__c', 'BAN__c', 'Name'));
			fsmlist.add(TestUtils.createSync('BICC_CTN_Information__c -> VF_Asset__c', 'BAN__c', 'BAN_Number__c'));
		insert fsmlist;

		list<Account> accountList = new list<Account>{
		new Account(Name = 'Test Account 00', KVK_number__c = '22312351', Visiting_Postal_Code__c = '1491 HV', Ban_Number__c = '312312311'),
		new Account(Name = 'Vodafone Libertel B.V.')};
		insert accountList;

		Account acc = [select Id from Account where KVK_number__c = '22312351'];
		list<Ban__c> banList = new list<Ban__c>{ new BAN__c (Account__c = acc.Id, Name = '312312311')};
		insert banList;

		TestUtils.autoCommit = true;

		TestUtils.createBICC('312312311', 'Test Account 11 BV.', '223123510000', '1481 HV', '15', 'EINDHOVEN'); //Match existing account and BAN
		TestUtils.createBICC('312312351', 'Test Account 12', '22312351', '1485 HV', '16', 'EINDHOVEN'); //Match existing account and new BAN
		TestUtils.createBICC('412312351', 'Test Account 12', '22312351', '1485 HV', '16', 'EINDHOVEN'); //Wrong BAN
		TestUtils.createBICC('312312352', 'Test Account 12', '32312351', '1485 HV', '16', 'EINDHOVEN'); //No matching KVK
		TestUtils.createBICC('312312353', 'Vodafone', null, '1485 HV', '16', 'EINDHOVEN'); //No KVK

		//Test
		Test.startTest();

		PageReference pageRef = Page.BiccBanInformationBatchExecute;
		Test.setCurrentPage(pageRef);
		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new list<BICC_BAN_Information__c>());
		BiccBanInformationBatchExecuteController controller = new BiccBanInformationBatchExecuteController(sc);
		controller.callBatch();
		controller.callBatchChain();

		Test.stopTest();

		//Checks
		System.assertEquals([select BAN_Name__c from Ban__c where BAN_Number__c =: '312312311' limit 1].BAN_Name__c, 'Test Account 11 BV.'); //Succesful update
		System.assertEquals([select Id from Ban__c where BAN_Number__c =: '312312351'].size(), 1); //Succesful insert
	}
}