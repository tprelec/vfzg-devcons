@isTest
public with sharing class TestLoggerService {
	@isTest
	static void testLogError() {
		Test.startTest();
		LoggerService.log(LoggingLevel.ERROR, 'Message');
		Test.stopTest();
		List<Log__c> logs = getLogs();
		System.assertEquals(1, logs.size(), 'One log should be inserted');
		System.assertEquals('ERROR', logs[0].Level__c, 'Log level should be error');
	}

	@isTest
	static void testDontLogFinest() {
		Test.startTest();
		LoggerService.log(LoggingLevel.FINEST, 'Message');
		Test.stopTest();
		List<Log__c> logs = getLogs();
		System.assertEquals(0, logs.size(), 'No logs should be inserted');
	}

	@isTest
	static void testLogException() {
		Test.startTest();
		try {
			Integer error = 1 / 0;
		} catch (Exception e) {
			LoggerService.log(e);
		}
		Test.stopTest();
		List<Log__c> logs = getLogs();
		System.assertEquals(1, logs.size(), 'One log should be inserted');
		System.assertEquals('ERROR', logs[0].Level__c, 'Log level should be error');
	}

	@isTest
	static void testLogExceptionWithRecordIds() {
		Opportunity opp = new Opportunity();

		opp.Name = 'Test Opportunity';
		opp.StageName = 'Prospect';
		opp.CloseDate = system.today();
		insert opp;

		Set<Id> oppIds = new Set<Id>();
		oppIds.add(opp.Id);
		Test.startTest();
		try {
			Integer error = 1 / 0;
		} catch (Exception e) {
			LoggerService.log(e, oppIds);
		}
		Test.stopTest();
		List<Log__c> logs = getLogs();
		System.debug(LoggingLevel.ERROR, '##############' + logs);
		System.assertEquals(1, logs.size(), 'One log should be inserted');
		System.assertEquals('ERROR', logs[0].Level__c, 'Log level should be error');
		System.assertEquals(logs[0].Record_ID__c, opp.Id, 'No recored inserted');
	}

	static List<Log__c> getLogs() {
		return [SELECT Id, Record_ID__c, Level__c FROM Log__c];
	}
}
