@isTest
private class TestUnicaCampaignBatch {
	private static Account acc;

	private static void createTestData() {
		acc = new Account(Name = 'Test Account 01', KVK_number__c = '22312351');
		insert acc;

		TestUtils.autoCommit = false;

		Map<String, String> mapping = new Map<String, String>{
			'Campaign_Name__c' => 'Name',
			'Selection_Name__c' => 'Selection_Name__c',
			'Campaign_Type__c' => 'Type'
		};

		List<Field_Sync_Mapping__c> fsmList = new List<Field_Sync_Mapping__c>();

		for (String field : mapping.keySet()) {
			fsmList.add(TestUtils.createSync('Unica_Campaign__c -> Campaign', field, mapping.get(field)));
		}

		fsmList.add(TestUtils.createSync('Unica_Campaign__c -> CampaignMember', 'NTA_Id__c', 'NTA_Id__c'));
		fsmList.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'NTA_Id__c', 'NTA_Id__c'));
		fsmList.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'Ban__c', 'BAN_Number__c'));
		fsmList.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'Treatment_01__c', 'Treatment_01__c'));
		fsmList.add(TestUtils.createSync('Unica_Campaign__c -> Lead', 'Campaign_Name__c', 'LastName'));
		insert fsmList;
	}

	@isTest
	static void testDealerCampaign() {
		createTestData();

		insert new Unica_Campaign__c(
			Campaign_Name__c = 'ETRECO0020',
			Selection_Name__c = 'january',
			Ban__c = '312312311',
			NTA_Id__c = '12345678',
			Treatment_01__c = 'Test Treatment',
			Campaign_Type__c = 'Recommit',
			Campaign_Channel__c = 'ITW',
			Post_Code__c = '2222'
		);

		Contact c = new Contact(AccountId = acc.Id, LastName = 'Test', Userid__c = UserInfo.getUserId());
		insert c;

		Dealer_Information__c di = new Dealer_Information__c(
			Dealer_Code__c = '200900',
			Parent_Dealer_Code__c = '333333',
			Contact__c = c.Id,
			Name = 'Test'
		);
		insert di;

		Account acc2 = new Account(
			Name = 'Test Account 02',
			KVK_number__c = '22312352',
			Mobile_Dealer__c = di.Id,
			ITW_Indicator__c = true,
			ITW_Team_Member__c = UserInfo.getUserId()
		);
		insert acc2;

		Postal_Code_Assignment__c pca = new Postal_Code_Assignment__c(Name = '2222', Postcode_Number__c = '2222', Dealer_Information__c = di.Id);
		insert pca;

		insert new BAN__c(Account__c = acc2.Id, Name = '312312311', Dealer_Code__c = '00200900');
		insert new Campaign(Name = 'ETRECO0020');

		UnicaCampaignSettings__c ucs = new UnicaCampaignSettings__c(
			Name = 'Main',
			StatusEmailRecepient1__c = 'test1@testmm.com',
			StatusEmailRecepient2__c = 'test2@testmm.com',
			StatusEmailRecepient3__c = 'test3@testmm.com',
			StatusEmailRecepient4__c = 'test4@testmm.com',
			StatusEmailRecepient5__c = 'test5@testmm.com',
			StatusEmailSubject__c = 'Test Subject',
			StatusEmailText__c = 'Test Text'
		);
		insert ucs;

		TestUtils.autoCommit = true;

		Test.startTest();

		PageReference pageRef = Page.UnicaCampaignBatchExecute;
		Test.setCurrentPage(pageRef);

		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new List<Unica_Campaign__c>());

		UnicaCampaignBatchExecuteController controller = new UnicaCampaignBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		List<Lead> leads = [SELECT Id FROM Lead];
		List<Unica_Campaign__c> unicaCampaigns = [SELECT Processed__c FROM Unica_Campaign__c];

		System.assertEquals(1, leads.size(), 'One lead should be created');
		System.assertEquals(true, unicaCampaigns[0].Processed__c, 'Unica Campaign should be processed');
	}

	@isTest
	static void testNoDealerCampaign() {
		createTestData();

		insert new Unica_Campaign__c(
			Campaign_Name__c = 'ETREC',
			Selection_Name__c = 'january',
			Ban__c = '312312311',
			NTA_Id__c = '12345678',
			Treatment_01__c = 'Test Treatment',
			Campaign_Type__c = 'Recommit',
			Campaign_Channel__c = 'ITW',
			Post_Code__c = '2222'
		);

		Contact c = new Contact(AccountId = acc.Id, LastName = 'Test', Userid__c = UserInfo.getUserId());
		insert c;

		Dealer_Information__c di = new Dealer_Information__c(
			Dealer_Code__c = '200900',
			Parent_Dealer_Code__c = '333333',
			Contact__c = c.Id,
			Name = 'Test'
		);
		insert di;

		Account acc2 = new Account(
			Name = 'Test Account 02',
			KVK_number__c = '22312352',
			Mobile_Dealer__c = di.Id,
			ITW_Indicator__c = true,
			ITW_Team_Member__c = UserInfo.getUserId()
		);
		insert acc2;

		Postal_Code_Assignment__c pca = new Postal_Code_Assignment__c(Name = '2222', Postcode_Number__c = '2222', Dealer_Information__c = di.Id);
		insert pca;

		insert new BAN__c(Account__c = acc2.Id, Name = '312312311', Dealer_Code__c = '00200900');

		TestUtils.autoCommit = true;

		Test.startTest();

		PageReference pageRef = Page.UnicaCampaignBatchExecute;
		Test.setCurrentPage(pageRef);

		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new List<Unica_Campaign__c>());

		UnicaCampaignBatchExecuteController controller = new UnicaCampaignBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		List<Lead> leads = [SELECT Id FROM Lead];
		List<Unica_Campaign__c> unicaCampaigns = [SELECT Processed__c FROM Unica_Campaign__c];

		System.assertEquals(1, leads.size(), 'One lead should be created');
		System.assertEquals(true, unicaCampaigns[0].Processed__c, 'Unica Campaign should be processed');
	}

	@isTest
	static void testOwnerNotFound() {
		createTestData();

		insert new Unica_Campaign__c(
			Campaign_Name__c = 'ETRECO0020',
			Selection_Name__c = 'january',
			Ban__c = '312312311',
			NTA_Id__c = '12345678',
			Treatment_01__c = 'Test Treatment',
			Campaign_Type__c = 'Recommit',
			Post_Code__c = '2222'
		);

		List<Ban__c> banList = new List<Ban__c>{
			new BAN__c(Account__c = acc.Id, Name = '312312311', Ban_Name__c = 'Test Ban', Dealer_Code__c = '00200900')
		};
		insert banList;

		TestUtils.autoCommit = true;

		Test.startTest();

		PageReference pageRef = Page.UnicaCampaignBatchExecute;
		Test.setCurrentPage(pageRef);

		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new List<Unica_Campaign__c>());

		UnicaCampaignBatchExecuteController controller = new UnicaCampaignBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		List<Lead> leads = [SELECT Id FROM Lead];
		List<Unica_Campaign__c> unicaCampaigns = [SELECT Processed__c FROM Unica_Campaign__c];

		System.assertEquals(0, leads.size(), 'No leads should be created');
		System.assertEquals(true, unicaCampaigns[0].Processed__c, 'Unica Campaign should be processed');
	}

	@isTest
	static void testNoCampaignName() {
		createTestData();

		insert new Unica_Campaign__c(
			Selection_Name__c = 'january',
			Ban__c = '312312311',
			NTA_Id__c = '12345678',
			Treatment_01__c = 'Test Treatment',
			Campaign_Type__c = 'Recommit',
			Post_Code__c = '2222'
		);

		insert new BAN__c(Account__c = acc.Id, Name = '312312311', Ban_Name__c = 'Test Ban', Dealer_Code__c = '00200900');

		TestUtils.autoCommit = true;

		Test.startTest();

		PageReference pageRef = Page.UnicaCampaignBatchExecute;
		Test.setCurrentPage(pageRef);

		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new List<Unica_Campaign__c>());

		UnicaCampaignBatchExecuteController controller = new UnicaCampaignBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		List<Lead> leads = [SELECT Id FROM Lead];
		List<Unica_Campaign__c> unicaCampaigns = [SELECT Processed__c FROM Unica_Campaign__c];

		System.assertEquals(0, leads.size(), 'No leads should be created');
		System.assertEquals(true, unicaCampaigns[0].Processed__c, 'Unica Campaign should be processed');
	}
	@isTest
	static void testNoPostalCode() {
		createTestData();

		insert new Unica_Campaign__c(
			Campaign_Name__c = 'ETREC',
			Selection_Name__c = 'january',
			Ban__c = '312312311',
			NTA_Id__c = '12345678',
			Treatment_01__c = 'Test Treatment',
			Campaign_Type__c = 'saves',
			Campaign_Channel__c = 'ITW'
			//Post_Code__c = ''
		);

		Contact c = new Contact(AccountId = acc.Id, LastName = 'Test', Userid__c = UserInfo.getUserId());
		insert c;

		Dealer_Information__c di = new Dealer_Information__c(
			Dealer_Code__c = '200900',
			Parent_Dealer_Code__c = '333333',
			Contact__c = c.Id,
			Name = 'Test'
		);
		insert di;

		Account acc2 = new Account(
			Name = 'Test Account 02',
			KVK_number__c = '22312352',
			Mobile_Dealer__c = di.Id,
			ITW_Indicator__c = true,
			ITW_Team_Member__c = UserInfo.getUserId()
		);
		insert acc2;

		Postal_Code_Assignment__c pca = new Postal_Code_Assignment__c(Name = '2222', Postcode_Number__c = '2222', Dealer_Information__c = di.Id);
		insert pca;

		insert new BAN__c(Account__c = acc2.Id, Name = '312312311', Dealer_Code__c = '00200900');

		TestUtils.autoCommit = true;

		Test.startTest();

		PageReference pageRef = Page.UnicaCampaignBatchExecute;
		Test.setCurrentPage(pageRef);

		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new List<Unica_Campaign__c>());

		UnicaCampaignBatchExecuteController controller = new UnicaCampaignBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		List<Lead> leads = [SELECT Id FROM Lead];
		List<Unica_Campaign__c> unicaCampaigns = [SELECT Processed__c FROM Unica_Campaign__c];

		System.assertEquals(1, leads.size(), 'One lead should be created');
		System.assertEquals(true, unicaCampaigns[0].Processed__c, 'Unica Campaign should be processed');
	}
	@isTest
	static void testDifferentCampaignType() {
		createTestData();

		insert new Unica_Campaign__c(
			Campaign_Name__c = 'ETREC',
			Selection_Name__c = 'january',
			Ban__c = '312312311',
			NTA_Id__c = '12345678',
			Treatment_01__c = 'Test Treatment',
			Campaign_Type__c = 'saves',
			Campaign_Channel__c = 'ITW'
			//Post_Code__c = ''
		);

		Contact c = new Contact(AccountId = acc.Id, LastName = 'Test', Userid__c = UserInfo.getUserId());
		insert c;

		Dealer_Information__c di = new Dealer_Information__c(
			Dealer_Code__c = '200900',
			Parent_Dealer_Code__c = '333333',
			Contact__c = c.Id,
			Name = 'Test'
		);
		insert di;

		Account acc2 = new Account(
			Name = 'Test Account 02',
			KVK_number__c = '22312352',
			Mobile_Dealer__c = di.Id,
			ITW_Indicator__c = true,
			ITW_Team_Member__c = UserInfo.getUserId()
		);
		insert acc2;

		Postal_Code_Assignment__c pca = new Postal_Code_Assignment__c(Name = '2222', Postcode_Number__c = '2222', Dealer_Information__c = di.Id);
		insert pca;

		insert new BAN__c(Account__c = acc2.Id, Name = '312312311', Dealer_Code__c = '00200900');

		TestUtils.autoCommit = true;

		Test.startTest();

		PageReference pageRef = Page.UnicaCampaignBatchExecute;
		Test.setCurrentPage(pageRef);

		Apexpages.StandardSetController sc = new Apexpages.standardSetController(new List<Unica_Campaign__c>());

		UnicaCampaignBatchExecuteController controller = new UnicaCampaignBatchExecuteController(sc);
		controller.callBatch();

		Test.stopTest();

		List<Lead> leads = [SELECT Id FROM Lead];
		List<Unica_Campaign__c> unicaCampaigns = [SELECT Processed__c FROM Unica_Campaign__c];

		System.assertEquals(1, leads.size(), 'One lead should be created');
		System.assertEquals(true, unicaCampaigns[0].Processed__c, 'Unica Campaign should be processed');
	}
}
