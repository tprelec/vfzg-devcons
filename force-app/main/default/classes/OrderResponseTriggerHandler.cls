/**
 * @description         This is the trigger handler for the Order_Response__c sObject.
 * @author              Guy Clairbois
 */
@SuppressWarnings('PMD')
public with sharing class OrderResponseTriggerHandler extends TriggerHandler {
	private static set<String> siasBlockingOrderResponseErrorcodes = new Set<String>{
		'SIAS008',
		'SIAS101',
		'SIAS034',
		'SIAS035'
	};
	private static Map<Id, Order__c> orderMap = new Map<Id, Order__c>();
	// key ring
	private static Map<Id, List<External_Site__c>> extSiteMap = new Map<Id, List<External_Site__c>>();
	private static Map<Id, List<External_Account__c>> extAcctMap = new Map<Id, List<External_Account__c>>();
	private static Map<Id, List<External_Contact__c>> extConMap = new Map<Id, List<External_Contact__c>>();

	private final Set<String> RESPONSE_SITE_TYPE = new Set<String>{
		'UnifySite',
		'BOPLocation',
		'UnifySiteAPID'
	};

	/**
	 * @description         This handles the before insert trigger event.
	 */
	public override void AfterInsert() {
		List<Order_Response__c> newResponses = (List<Order_Response__c>) this.newList;
		Map<Id, Order_Response__c> newResponsesMap = (Map<Id, Order_Response__c>) this.newMap;

		processUpdates(newResponses);
	}

	/**
	 * @description         This handles the before update trigger event.
	 */
	public override void BeforeUpdate() {
		List<Order_Response__c> newResponses = (List<Order_Response__c>) this.newList;
	}

	/**
	 * @description         This handles the before update trigger event.
	 */
	public override void AfterUpdate() {
		List<Order_Response__c> newResponses = (List<Order_Response__c>) this.newList;
		List<Order_Response__c> oldResponses = (List<Order_Response__c>) this.oldList;
		Map<Id, Order_Response__c> newResponsesMap = (Map<Id, Order_Response__c>) this.newMap;
		Map<Id, Order_Response__c> oldResponsesMap = (Map<Id, Order_Response__c>) this.oldMap;

		doReprocessing(newResponses, oldResponsesMap);
	}

	/**
	 * @description         This handles the before delete trigger event.
	 */
	public override void BeforeDelete() {
		List<Order_Response__c> newResponses = (List<Order_Response__c>) this.newList;
		List<Order_Response__c> oldResponses = (List<Order_Response__c>) this.oldList;
		Map<Id, Order_Response__c> oldResponsesMap = (Map<Id, Order_Response__c>) this.oldMap;
	}

	private static final String UNIFY_CONTACTS_TYPE = 'UnifyContact',
		BOP_CONTACTS_TYPE = 'BOPContact';

	private void processUpdates(List<Order_Response__c> orderResponseList) {
		RelevantOrderIdInformation rInformation = new RelevantOrderIdInformation(orderResponseList);
		retrieveOrders(rInformation.orderIds);
		retrieveExistingKeyringData(rInformation);

		Map<String, Map<Boolean, List<Order_Response__c>>> distributedResponseMap = rInformation.distributedResponseMap;

		updateUnifyContacts(distributedResponseMap.get(UNIFY_CONTACTS_TYPE)); //(newResponses);
		updateBOPContacts(distributedResponseMap.get(BOP_CONTACTS_TYPE)); //(newResponses);
		updateUnifySiteIds(orderResponseList);
		updateUnifyCustomerIds(orderResponseList);
		updateBOPCustomerIds(orderResponseList);
		updateOrderStatus(orderResponseList);
	}

	private void retrieveExistingKeyringData(RelevantOrderIdInformation rInformation) {
		if (!rInformation.siteIds.isEmpty()) {
			extSiteMap = KeyRingService.getExternalSite(rInformation.siteIds);
		}
		if (!rInformation.acctIds.isEmpty()) {
			extAcctMap = KeyRingService.getExternalAccount(rInformation.acctIds);
		}
		if (!rInformation.conIds.isEmpty()) {
			extConMap = KeyRingService.getExternalcontact(rInformation.conIds);
		}
	}

	/**
	 * @description         This method handles all order responses that lead to order updates.
	 */
	private void updateOrderStatus(List<Order_Response__c> newResponses) {
		List<Order__c> ordersToUpdate = new List<Order__c>();
		Map<Id, Id> orderIdToResponseId = new Map<Id, Id>();
		for (Order_Response__c response : newResponses) {
			if (
				response.Type__c == 'StartOrder' &&
				orderMap.get(response.Order__c).Status__c != 'Accepted'
			) {
				if (response.Unify_Error_Number__c == 'SIAS000') {
					Order__c o = new Order__c(Id = response.Order__c);
					o.Unify_Customer_Export_Datetime__c = System.now();
					o.Unify_Customer_Export_Errormessage__c = '';
					// re-submit order (for order notification)
					o.Status__c = 'Validation'; // locked
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				} else {
					Order__c o = new Order__c(Id = response.Order__c);
					o.Unify_Customer_Export_Errormessage__c = ('StartOrder failed: ' +
						response.Unify_Error_Number__c +
						'-' +
						response.Unify_Error_Description__c)
						.abbreviate(255);
					// reset order status and make order editable (to repair any errors); but only when not yet Accepted!
					if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
						o.Status__c = 'Clean';
					}
					o.Record_Locked__c = false;
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				}
			} else if (response.Type__c == 'BopOrderCreation') {
				if (response.Unify_Error_Number__c == '0') {
					Order__c o = new Order__c(Id = response.Order__c);
					o.BOP_export_datetime__c = system.now();
					o.BOP_export_Errormessage__c = '';
					o.BOP_Order_Id__c = response.BOP_Order_Id__c;
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				} else if (response.Unify_Error_Number__c == 'SIAS102') {
					// this is the response for a mobile order
					Order__c o = new Order__c(Id = response.Order__c);
					o.Status__c = 'Accepted';
					// put the description in so the end user knows that a ticket was created
					o.Unify_Order_Errormessage__c = (response.Unify_Error_Description__c)
						.abbreviate(255);
					o.Unify_Order_Export_Datetime__c = system.now();
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				} else {
					Order__c o = new Order__c(Id = response.Order__c);
					o.BOP_export_Errormessage__c =
						'BopOrderCreation failed: ' +
						response.Unify_Error_Number__c +
						'-' +
						response.Unify_Error_Description__c;
					// reset order status and make order editable (to repair any errors)
					o.Status__c = 'Refused';
					o.Record_Locked__c = false;
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				}
			} else if (response.Type__c == 'UnifyOrderCreation' || response.Type__c == 'Order') {
				if (
					response.Type__c == 'UnifyOrderCreation' &&
					response.Unify_Error_Number__c == 'SIAS000'
				) {
					Order__c o = new Order__c(Id = response.Order__c);
					o.Status__c = 'Accepted';
					o.Unify_Order_Errormessage__c = '';
					o.Unify_Order_Export_Datetime__c = system.now();
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				} else if (
					siasBlockingOrderResponseErrorcodes.contains(response.Unify_Error_Number__c)
				) {
					Order__c o = new Order__c(Id = response.Order__c);
					o.Unify_Order_Errormessage__c = ('UnifyOrderCreation failed: ' +
						response.Unify_Error_Number__c +
						'-' +
						response.Unify_Error_Description__c)
						.abbreviate(255);
					// order is processed manually and should be blocked in SFDC
					o.Status__c = 'Processed Manually in Unify';
					o.Record_Locked__c = true;
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				} else {
					Order__c o = new Order__c(Id = response.Order__c);
					o.Unify_Order_Errormessage__c =
						'UnifyOrderCreation failed: ' +
						response.Unify_Error_Number__c +
						'-' +
						response.Unify_Error_Description__c;
					// reset order status and make order editable (to repair any errors)
					o.Status__c = 'Refused';
					o.Record_Locked__c = false;
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				}
			} else if (response.Type__c == 'SendMappingObject') {
				if (response.Unify_Error_Number__c == 'SIAS000') {
					// we noticed we sometimes receive SendMappingObject without sales_order_id. Prevent this from overwriting an already present Sales_Order_Id
					if (response.BOP_Sales_Order_Id__c != null) {
						Order__c o = new Order__c(Id = response.Order__c);
						o.Sales_Order_Id__c = response.BOP_Sales_Order_Id__c;
						o.Status__c = 'Accepted';
						ordersToUpdate.add(o);
						orderIdToResponseId.put(response.Order__c, response.Id);
					}
				} else {
					Order__c o = new Order__c(Id = response.Order__c);
					o.Unify_Order_Errormessage__c = ('SendMappingObject failed: ' +
						response.Unify_Error_Number__c +
						'-' +
						response.Unify_Error_Description__c +
						'. No action required. Order will be processed manually in Unify.')
						.abbreviate(255);
					// order status doesn't need to be reset, as order will be processed manually.
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				}
			}
		}
		if (!ordersToUpdate.isEmpty()) {
			updateOrders(ordersToUpdate, orderIdToResponseId);
		}
	}

	/*
	 *  Description:    Utility method to support updating orders, and if it fails updating the order response result
	 */
	private void updateOrders(List<Order__c> ordersToUpdate, Map<Id, Id> orderIdToResponseId) {
		if (!ordersToUpdate.isEmpty()) {
			List<Order_Response__c> responseToUpdate = new List<Order_Response__c>();

			Database.saveResult[] results = SharingUtils.databaseUpdateRecordsWithoutSharing(
				ordersToUpdate
			);

			for (Integer i = 0; i < ordersToUpdate.size(); i++) {
				Database.SaveResult s = results[i];
				Order_Response__c orToUpdate = new Order_Response__c(
					Id = orderIdToResponseId.get(ordersToUpdate[i].Id)
				);
				if (!s.isSuccess()) {
					String errors = 'Error(s) occurred: ';
					for (Database.Error err : s.getErrors()) {
						errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
					}
					orToUpdate.Processing_result__c = errors.abbreviate(255);
					responseToUpdate.add(orToUpdate);
				}
			}
			SharingUtils.databaseUpdateRecordsWithoutSharing(responseToUpdate);
		}
	}

	/**
	 * @description         This method handles all order responses that lead to updates of Unify Contact Ids.
	 */
	private void updateBOPContacts(Map<Boolean, List<Order_Response__c>> orderResponseMap) {
		if (orderResponseMap == null) {
			return;
		}
		List<Contact> contactsToUpdate = new List<Contact>();
		Map<Id, Id> contactIdToResponseId = new Map<Id, Id>();
		List<Order__c> ordersToUpdate = new List<Order__c>();
		Map<Id, Id> orderIdToResponseId = new Map<Id, Id>();
		if (orderResponseMap.containsKey(true)) {
			// Successfull messages
			for (Order_Response__c response : orderResponseMap.get(true)) {
				// succesful BOP contact creation. Put datetime stamp on Contact (to support later direct updates)
				Contact c = new Contact(Id = response.SFDC_Contact_Id__c);
				c.BOP_Export_Datetime__c = response.CreatedDate;
				contactsToUpdate.add(c);
				contactIdToResponseId.put(response.SFDC_Contact_Id__c, response.Id);
			}
		}
		if (orderResponseMap.containsKey(false)) {
			// Errored state
			for (Order_Response__c response : orderResponseMap.get(false)) {
				Order__c o = new Order__c(Id = response.Order__c);
				o.Unify_Customer_Export_Errormessage__c = (response.Type__c +
					' failed: ' +
					response.Unify_Error_Number__c +
					'-' +
					response.Unify_Error_Description__c)
					.abbreviate(255);
				// reset order status and make order editable (to repair any errors); but only when not yet Accepted!
				if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
					o.Status__c = 'Clean';
				}
				o.Record_Locked__c = false;
				ordersToUpdate.add(o);
				orderIdToResponseId.put(response.Order__c, response.Id);
			}
		}

		Map<Id, Order_Response__c> responseToUpdate = new Map<Id, Order_Response__c>();

		if (!contactsToUpdate.isEmpty()) {
			responseToUpdate = updateContacts(contactsToUpdate, contactIdToResponseId);
		}

		updateResponse(responseToUpdate, ordersToUpdate, orderIdToResponseId);
	}

	/**
	 * @description         This method handles all order responses that lead to updates of Unify Contact Ids.
	 */
	private void updateUnifyContacts(Map<Boolean, List<Order_Response__c>> orderResponseMap) {
		if (orderResponseMap == null) {
			return;
		}
		List<Contact> contactsToUpdate = new List<Contact>();
		List<External_Contact__c> extContactsToInsert = new List<External_Contact__c>();
		Map<Id, Id> contactIdToResponseId = new Map<Id, Id>();
		List<Order__c> ordersToUpdate = new List<Order__c>();
		Map<Id, Id> orderIdToResponseId = new Map<Id, Id>();
		if (orderResponseMap.containsKey(true)) {
			// Successfull messages
			for (Order_Response__c response : orderResponseMap.get(true)) {
				if (response.SFDC_Contact_Id__c != null && response.Unify_Contact_Id__c != null) {
					extContactsToInsert.addAll(
						KeyRingService.getExternalContactsList(response, extConMap)
					);
					Contact c = new Contact(Id = response.SFDC_Contact_Id__c);
					c.Unify_Ref_Id__c = response.Unify_Contact_Id__c;
					contactsToUpdate.add(c);
					contactIdToResponseId.put(response.SFDC_Contact_Id__c, response.Id);
				}
			}
		}

		if (orderResponseMap.containsKey(false)) {
			// Errored state
			for (Order_Response__c response : orderResponseMap.get(false)) {
				Order__c o = new Order__c(Id = response.Order__c);
				o.Unify_Customer_Export_Errormessage__c = ('UnifyContact failed: ' +
					response.Unify_Error_Number__c +
					'-' +
					response.Unify_Error_Description__c)
					.abbreviate(255);
				// reset order status and make order editable (to repair any errors); but only when not yet Accepted!
				if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
					o.Status__c = 'Clean';
				}
				o.Record_Locked__c = false;
				ordersToUpdate.add(o);
				orderIdToResponseId.put(response.Order__c, response.Id);
			}
		}

		Map<Id, Order_Response__c> responseToUpdate = new Map<Id, Order_Response__c>();

		if (!contactsToUpdate.isEmpty()) {
			responseToUpdate = updateContacts(contactsToUpdate, contactIdToResponseId);
		}
		if (!extContactsToInsert.isEmpty()) {
			responseToUpdate = insertExternalContacts(
				extContactsToInsert,
				contactIdToResponseId,
				responseToUpdate
			);
		}

		updateResponse(responseToUpdate, ordersToUpdate, orderIdToResponseId);
	}

	private void updateResponse(
		Map<Id, Order_Response__c> responseToUpdate,
		List<Order__c> ordersToUpdate,
		Map<Id, Id> orderIdToResponseId
	) {
		SharingUtils.databaseUpdateRecordsWithoutSharing(responseToUpdate.values());

		if (!ordersToUpdate.isEmpty()) {
			updateOrders(ordersToUpdate, orderIdToResponseId);
		}
	}

	private Map<Id, Order_Response__c> updateContacts(
		List<Contact> contactList,
		Map<Id, Id> contactIdToResponseId
	) {
		Map<Id, Order_Response__c> responseToUpdate = new Map<Id, Order_Response__c>();
		Database.saveResult[] results = SharingUtils.databaseUpdateRecordsWithoutSharing(
			contactList
		);

		for (Integer i = 0; i < contactList.size(); i++) {
			Database.SaveResult s = results[i];
			Id orId = contactIdToResponseId.get(contactList[i].Id);
			Order_Response__c orToUpdate = new Order_Response__c(Id = orId);
			if (!s.isSuccess()) {
				String errors = 'Error(s) occurred: ';
				for (Database.Error err : s.getErrors()) {
					errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
				}
				orToUpdate.Processing_result__c = errors.abbreviate(255);
			} else {
				orToUpdate.Processing_result__c = 'Processed';
			}
			responseToUpdate.put(orId, orToUpdate);
		}

		return responseToUpdate;
	}

	private Map<Id, Order_Response__c> insertExternalContacts(
		List<External_Contact__c> extContactsToInsert,
		Map<Id, Id> contactIdToResponseId,
		Map<Id, Order_Response__c> responseToUpdate
	) {
		// try inserting all objects
		Database.SaveResult[] results = Database.insert(extContactsToInsert, false);

		for (Integer i = 0; i < extContactsToInsert.size(); i++) {
			Database.SaveResult s = results[i];
			Id orId = contactIdToResponseId.get(extContactsToInsert[i].Contact__c);
			Order_Response__c orToUpdate = new Order_Response__c(Id = orId);
			if (!s.isSuccess()) {
				String errors = 'Error(s) occurred: ';
				for (Database.Error err : s.getErrors()) {
					errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
				}
				orToUpdate.Processing_result__c = errors.abbreviate(255);
			} else {
				orToUpdate.Processing_result__c = 'Processed';
			}
			responseToUpdate.put(orId, orToUpdate);
		}
		return responseToUpdate;
	}

	/**
	 * @description         This method handles all order responses that lead to updates of Unify Site Ids.
	 */
	private void updateUnifySiteIds(List<Order_Response__c> newResponses) {
		List<Site__c> sitesToUpdate = new List<Site__c>();
		List<External_Site__c> extSitesToUpsert = new List<External_Site__c>();
		Map<Id, Id> siteIdToResponseId = new Map<Id, Id>();
		List<Order__c> ordersToUpdate = new List<Order__c>();
		Map<Id, Id> orderIdToResponseId = new Map<Id, Id>();
		for (Order_Response__c response : newResponses) {
			if (RESPONSE_SITE_TYPE.contains(response.Type__c)) {
				if (response.SFDC_Site_Id__c != null) {
					Site__c c = new Site__c(Id = response.SFDC_Site_Id__c);
					if (response.Unify_Site_Id__c != null) {
						c.Unify_Ref_Id__c = response.Unify_Site_Id__c;
						c.Unify_Account_Id__c = response.Unify_Account_Id__c;
						extSitesToUpsert.addAll(
							KeyRingService.getExternalSitesList(response, extSiteMap)
						);
					}
					if (response.Unify_Site_AP_Id__c != null) {
						c.Assigned_Product_Id__c = response.Unify_Site_AP_Id__c;
					}

					if (response.BOP_Location_Id__c != null)
						c.BOP_Location_Id__c = response.BOP_Location_Id__c;
					if (response.Type__c == 'BOPLocation')
						c.BOP_Export_datetime__c = response.createddate;
					sitesToUpdate.add(c);
					siteIdToResponseId.put(response.SFDC_Site_Id__c, response.Id);
				}
				if (response.Unify_Error_Number__c != 'SIAS000') {
					Order__c o = new Order__c(Id = response.Order__c);
					o.Unify_Customer_Export_Errormessage__c = ('UnifySite failed: ' +
						response.Unify_Error_Number__c +
						'-' +
						response.Unify_Error_Description__c)
						.abbreviate(255);
					// reset order status and make order editable (to repair any errors); but only when not yet Accepted!
					if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
						o.Status__c = 'Clean';
					}
					o.Record_Locked__c = false;
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				}
			}
		}
		Map<Id, Order_Response__c> responseToUpdate = new Map<Id, Order_Response__c>();
		if (!sitesToUpdate.isEmpty()) {
			Database.saveResult[] results = SharingUtils.databaseUpdateRecordsWithoutSharing(
				sitesToUpdate
			);

			for (Integer i = 0; i < sitesToUpdate.size(); i++) {
				Database.SaveResult s = results[i];
				Id orId = siteIdToResponseId.get(sitesToUpdate[i].Id);
				Order_Response__c orToUpdate = new Order_Response__c(Id = orId);
				if (!s.isSuccess()) {
					String errors = 'Error(s) occurred: ';
					for (Database.Error err : s.getErrors()) {
						errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
					}
					orToUpdate.Processing_result__c = errors.abbreviate(255);
				} else {
					orToUpdate.Processing_result__c = 'Processed';
				}
				responseToUpdate.put(orId, orToUpdate);
			}
		}
		if (!extSitesToUpsert.isEmpty()) {
			Database.UpsertResult[] results = Database.upsert(
				extSitesToUpsert,
				External_Site__c.fields.External_Site_Id__c,
				false
			);

			for (Integer i = 0; i < extSitesToUpsert.size(); i++) {
				Database.UpsertResult u = results[i];
				Id orId = siteIdToResponseId.get(extSitesToUpsert[i].Site__c);
				Order_Response__c orToUpdate = new Order_Response__c(Id = orId);
				if (!u.isSuccess()) {
					String errors = 'Error(u) occurred: ';
					for (Database.Error err : u.getErrors()) {
						errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
					}
					orToUpdate.Processing_result__c = errors.abbreviate(255);
				} else {
					orToUpdate.Processing_result__c = 'Processed';
				}
				responseToUpdate.put(orId, orToUpdate);
			}
		}
		SharingUtils.databaseUpdateRecordsWithoutSharing(responseToUpdate.values());
		if (!ordersToUpdate.isEmpty()) {
			updateOrders(ordersToUpdate, orderIdToResponseId);
		}
	}

	/**
	 * @description         This method handles all order responses that lead to updates of Unify Account/Customer Ids.
	 */
	private void updateUnifyCustomerIds(List<Order_Response__c> newResponses) {
		List<Order__c> ordersToUpdate = new List<Order__c>();
		Map<Id, Id> orderIdToResponseId = new Map<Id, Id>();
		List<Account> accountsToUpdate = new List<Account>();
		List<External_Account__c> extAcctsToInsert = new List<External_Account__c>();
		List<Ban__c> bansToUpdate = new List<Ban__c>();
		List<Financial_Account__c> fasToUpdate = new List<Financial_Account__c>();
		List<Billing_Arrangement__c> barsToUpdate = new List<Billing_Arrangement__c>();
		Map<Id, Order_Response__c> responsesToUpdate = new Map<Id, Order_Response__c>();
		Map<Id, Id> accountIdToResponseId = new Map<Id, Id>();
		Map<Id, Id> banIdToResponseId = new Map<Id, Id>();
		Map<Id, Id> faIdToResponseId = new Map<Id, Id>();
		Map<Id, Id> barIdToResponseId = new Map<Id, Id>();
		Set<Id> responseIds = new Set<Id>();
		for (Order_Response__c response : newResponses) {
			if (response.Type__c == 'UnifyAccount') {
				responseIds.add(response.Id);
				// When there is an error always update the order with the failure
				if (response.Unify_Error_Number__c != 'SIAS000') {
					Order__c o = new Order__c(Id = response.Order__c);
					o.Unify_Customer_Export_Errormessage__c = ('UnifyAccount failed: ' +
						response.Unify_Error_Number__c +
						'-' +
						response.Unify_Error_Description__c)
						.abbreviate(255);
					// reset order status and make order editable (to repair any errors); but only when not yet Accepted!
					if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
						o.Status__c = 'Clean';
					}
					o.Record_Locked__c = false;
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				}
			}
		}
		if (!responseIds.isEmpty()) {
			// requery responses to get full info on fa, bar, ban, account
			for (Order_Response__c response : [
				SELECT
					Id,
					Order__r.Billing_Arrangement__c,
					Order__r.Billing_Arrangement__r.Financial_Account__c,
					Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__c,
					Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Account__c,
					Unify_Account_Id__c,
					Unify_Billing_Customer_Id__c,
					BAN_Corporate_Id__c,
					Unify_Financial_Account_Id__c,
					Unify_Billing_Arrangement_Id__c
				FROM Order_Response__c
				WHERE Id IN :responseIds
			]) {
				// Process any ID's returned from Unify (it does not matter if there was an overall error or not)
				if (response.Unify_Account_Id__c != null) {
					extAcctsToInsert.addAll(
						KeyRingService.getExternalAccountsList(response, extAcctMap)
					);

					accountsToUpdate.add(
						new Account(
							Id = response.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Account__c,
							Unify_Ref_Id__c = response.Unify_Account_Id__c
						)
					);

					accountIdToResponseId.put(
						response.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Account__c,
						response.Id
					);
				}

				if (response.Unify_Billing_Customer_Id__c != null) {
					bansToUpdate.add(
						new Ban__c(
							Id = response.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__c,
							Name = response.Unify_Billing_Customer_Id__c,
							BAN_Status__c = 'Opened',
							Unify_Ref_Id__c = response.Unify_Billing_Customer_Id__c,
							Corporate_Id__c = response.BAN_Corporate_Id__c,
							ExternalAccount__r = new External_Account__c(
								External_Account_Id__c = response.Unify_Account_Id__c
							)
						)
					);
					banIdToResponseId.put(
						response.Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__c,
						response.Id
					);
				}

				if (response.Unify_Financial_Account_Id__c != null) {
					fasToUpdate.add(
						new Financial_Account__c(
							Id = response.Order__r.Billing_Arrangement__r.Financial_Account__c,
							Unify_Ref_Id__c = response.Unify_Financial_Account_Id__c
						)
					);
					faIdToResponseId.put(
						response.Order__r.Billing_Arrangement__r.Financial_Account__c,
						response.Id
					);
				}

				if (response.Unify_Billing_Arrangement_Id__c != null) {
					barsToUpdate.add(
						new Billing_Arrangement__c(
							Id = response.Order__r.Billing_Arrangement__c,
							Unify_Ref_Id__c = response.Unify_Billing_Arrangement_Id__c
						)
					);
					barIdToResponseId.put(response.Order__r.Billing_Arrangement__c, response.Id);
				}

				response.Processing_result__c = '';
				responsesToUpdate.put(response.Id, response);
			}

			if (!accountsToUpdate.isEmpty()) {
				// try updating all objects
				Database.saveResult[] accountResults = SharingUtils.databaseUpdateRecordsWithoutSharing(
					accountsToUpdate
				);

				for (Integer i = 0; i < accountsToUpdate.size(); i++) {
					Database.SaveResult s = accountResults[i];
					Order_Response__c orToUpdate = responsesToUpdate.get(
						accountIdToResponseId.get(accountsToUpdate[i].Id)
					);
					if (!s.isSuccess()) {
						String errors =
							orToUpdate.Processing_result__c +
							'Error(s) occurred while updating Account: ';
						for (Database.Error err : s.getErrors()) {
							errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
						}
						orToUpdate.Processing_result__c = errors.abbreviate(255);
					}
					responsesToUpdate.put(orToUpdate.Id, orToUpdate);
				}
			}

			if (!extAcctsToInsert.isEmpty()) {
				// try inserting all objects
				Database.SaveResult[] results = Database.insert(extAcctsToInsert, false);

				for (Integer i = 0; i < extAcctsToInsert.size(); i++) {
					Database.SaveResult s = results[i];
					Order_Response__c orToUpdate = responsesToUpdate.get(
						accountIdToResponseId.get(extAcctsToInsert[i].Account__c)
					);
					if (!s.isSuccess()) {
						String errors =
							orToUpdate.Processing_result__c +
							'Error(s) occurred while updating Account: ';
						for (Database.Error err : s.getErrors()) {
							errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
						}
						orToUpdate.Processing_result__c = errors.abbreviate(255);
					}
					responsesToUpdate.put(orToUpdate.Id, orToUpdate);
				}
			}

			if (!bansToUpdate.isEmpty()) {
				Database.saveResult[] banResults = SharingUtils.databaseUpdateRecordsWithoutSharing(
					bansToUpdate
				);
				for (Integer i = 0; i < bansToUpdate.size(); i++) {
					Database.SaveResult s = banResults[i];
					Order_Response__c orToUpdate = responsesToUpdate.get(
						banIdToResponseId.get(bansToUpdate[i].Id)
					);
					if (!s.isSuccess()) {
						String errors =
							orToUpdate.Processing_result__c +
							'Error(s) occurred while updating Ban: ';
						for (Database.Error err : s.getErrors()) {
							errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
						}
						orToUpdate.Processing_result__c = errors.abbreviate(255);
					}
					responsesToUpdate.put(orToUpdate.Id, orToUpdate);
				}
			}

			if (!fasToUpdate.isEmpty()) {
				Database.saveResult[] faResults = SharingUtils.databaseUpdateRecordsWithoutSharing(
					fasToUpdate
				);
				for (Integer i = 0; i < fasToUpdate.size(); i++) {
					Database.SaveResult s = faResults[i];
					Order_Response__c orToUpdate = responsesToUpdate.get(
						faIdToResponseId.get(fasToUpdate[i].Id)
					);
					if (!s.isSuccess()) {
						String errors =
							orToUpdate.Processing_result__c +
							'Error(s) occurred while updating Financial Account: ';
						for (Database.Error err : s.getErrors()) {
							errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
						}
						orToUpdate.Processing_result__c = errors.abbreviate(255);
					}
					responsesToUpdate.put(orToUpdate.Id, orToUpdate);
				}
			}

			if (!barsToUpdate.isEmpty()) {
				Database.saveResult[] barResults = SharingUtils.databaseUpdateRecordsWithoutSharing(
					barsToUpdate
				);
				for (Integer i = 0; i < barsToUpdate.size(); i++) {
					Database.SaveResult s = barResults[i];
					Order_Response__c orToUpdate = responsesToUpdate.get(
						barIdToResponseId.get(barsToUpdate[i].Id)
					);
					if (!s.isSuccess()) {
						String errors =
							orToUpdate.Processing_result__c +
							'Error(s) occurred while updating Billing Arrangement: ';
						for (Database.Error err : s.getErrors()) {
							errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
						}
						orToUpdate.Processing_result__c = errors.abbreviate(255);
					}
					responsesToUpdate.put(orToUpdate.Id, orToUpdate);
				}
			}
			if (!responsesToUpdate.isEmpty()) {
				// lastly, put ok for all responses that have no errors
				for (Id responseId : responsesToUpdate.keySet()) {
					if (responsesToUpdate.get(responseId).Processing_result__c == '') {
						Order_Response__c response = responsesToUpdate.get(responseId);
						response.Processing_result__c = 'Processed';
						responsesToUpdate.put(responseId, response);
					}
				}
				SharingUtils.databaseUpdateRecordsWithoutSharing(responsesToUpdate.values());
			}
		}

		if (!ordersToUpdate.isEmpty()) {
			updateOrders(ordersToUpdate, orderIdToResponseId);
		}
	}

	/**
	 * @description         This method handles all order responses that lead to updates of BOP Customer Ids.
	 * JvW 26-07-2019: Replaced all references to BAN with Account references
	 */
	private void updateBOPCustomerIds(List<Order_Response__c> newResponses) {
		List<Order__c> ordersToUpdate = new List<Order__c>();
		Map<Id, Id> orderIdToResponseId = new Map<Id, Id>();
		List<Account> accntsToUpdate = new List<Account>();
		List<External_Account__c> extAccntsToInsert = new List<External_Account__c>();
		List<External_Account__c> extAccntsToUpdate = new List<External_Account__c>();
		Map<Id, Order_Response__c> responsesToUpdate = new Map<Id, Order_Response__c>();
		Map<Id, Id> accIdToResponseId = new Map<Id, Id>();
		Set<Id> responseIds = new Set<Id>();
		for (Order_Response__c response : newResponses) {
			if (response.Type__c == 'BOPCompany') {
				responseIds.add(response.Id);
				if (response.Unify_Error_Number__c != 'SIAS000') {
					Order__c o = new Order__c(Id = response.Order__c);
					o.Unify_Customer_Export_Errormessage__c = ('BOPCompany failed: ' +
						response.Unify_Error_Number__c +
						'-' +
						response.Unify_Error_Description__c)
						.abbreviate(255);
					// reset order status and make order editable (to repair any errors); but only when not yet Accepted!
					if (orderMap.get(response.Order__c).Status__c != 'Accepted') {
						o.Status__c = 'Clean';
					}
					o.Record_Locked__c = false;
					ordersToUpdate.add(o);
					orderIdToResponseId.put(response.Order__c, response.Id);
				}
			}
		}

		if (!responseIds.isEmpty()) {
			// requery responses to get full info on fa, bar, ban, account
			for (Order_Response__c response : [
				SELECT
					Id,
					Order__r.Account__c,
					BAN_BopCode__c,
					CreatedDate,
					Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.Account__c,
					Order__r.Billing_Arrangement__r.Financial_Account__r.Ban__r.ExternalAccount__c
				FROM Order_Response__c
				WHERE Id IN :responseIds
			]) {
				if (response.BAN_BopCode__c != null) {
					Map<String, List<External_Account__c>> extAccntToProcessMap = KeyRingService.getExternalBOPAccountsList(
						response,
						extAcctMap
					);
					extAccntsToInsert.addAll(extAccntToProcessMap.get('insert'));
					extAccntsToUpdate.addAll(extAccntToProcessMap.get('update'));
					accntsToUpdate.add(
						new Account(
							Id = response.Order__r.Account__c,
							BOPCode__c = response.BAN_BopCode__c,
							BOP_Export_Datetime__c = response.CreatedDate
						)
					);
					accIdToResponseId.put(response.Order__r.Account__c, response.Id);
					response.Processing_result__c = '';
					responsesToUpdate.put(response.Id, response);
				}
			}

			if (!accntsToUpdate.isEmpty()) {
				// try updating all objects
				Database.saveResult[] accResults = SharingUtils.databaseUpdateRecordsWithoutSharing(
					accntsToUpdate
				);
				for (Integer i = 0; i < accntsToUpdate.size(); i++) {
					Database.SaveResult s = accResults[i];
					Order_Response__c orToUpdate = responsesToUpdate.get(
						accIdToResponseId.get(accntsToUpdate[i].Id)
					);
					if (!s.isSuccess()) {
						String errors =
							orToUpdate.Processing_result__c +
							'Error(s) occurred while updating Account: ';
						for (Database.Error err : s.getErrors()) {
							errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
						}
						orToUpdate.Processing_result__c = errors.abbreviate(255);
					}
					responsesToUpdate.put(orToUpdate.Id, orToUpdate);
				}

				// lastly, put ok for all responses that have no errors
				for (Id responseId : responsesToUpdate.keySet()) {
					if (responsesToUpdate.get(responseId).Processing_result__c == '') {
						Order_Response__c response = responsesToUpdate.get(responseId);
						response.Processing_result__c = 'Processed';
						responsesToUpdate.put(responseId, response);
					}
				}
			}

			if (!extAccntsToInsert.isEmpty()) {
				// try updating all objects
				System.debug('inserting: ' + extAccntsToInsert);
				Database.SaveResult[] accResults = Database.insert(extAccntsToInsert, false);
				for (Integer i = 0; i < extAccntsToInsert.size(); i++) {
					Database.SaveResult s = accResults[i];
					Order_Response__c orToUpdate = responsesToUpdate.get(
						accIdToResponseId.get(extAccntsToInsert[i].Account__c)
					);
					if (!s.isSuccess()) {
						String errors =
							orToUpdate.Processing_result__c +
							'Error(s) occurred while updating Account: ';
						for (Database.Error err : s.getErrors()) {
							errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
						}
						orToUpdate.Processing_result__c = errors.abbreviate(255);
					}
					responsesToUpdate.put(orToUpdate.Id, orToUpdate);
				}

				// lastly, put ok for all responses that have no errors
				for (Id responseId : responsesToUpdate.keySet()) {
					if (responsesToUpdate.get(responseId).Processing_result__c == '') {
						Order_Response__c response = responsesToUpdate.get(responseId);
						response.Processing_result__c = 'Processed';
						responsesToUpdate.put(responseId, response);
					}
				}
			}
			// The following is to update existing External Accounts to link them with the above created bop-external-account records. There is no explicit relationship with the orderresponse..
			if (!extAccntsToUpdate.isEmpty()) {
				Database.SaveResult[] accResults = Database.update(extAccntsToUpdate, false);
				for (Integer i = 0; i < extAccntsToUpdate.size(); i++) {
					Database.SaveResult s = accResults[i];
					if (!s.isSuccess()) {
						String errors = 'Error(s) occurred while updating Account: ';
						for (Database.Error err : s.getErrors()) {
							errors += err.getStatusCode() + ' ' + err.getMessage() + '. ';
						}
						System.debug('errors: ' + errors);
					}
				}
			}

			SharingUtils.databaseUpdateRecordsWithoutSharing(responsesToUpdate.values());
		}
		if (!ordersToUpdate.isEmpty()) {
			updateOrders(ordersToUpdate, orderIdToResponseId);
		}
	}

	private class RelevantOrderIdInformation {
		private final String SUCCESS_CODE = 'SIAS000';
		public Set<Id> orderIds = new Set<Id>(), siteIds = new Set<Id>();
		Set<String> acctIds = new Set<String>(), conIds = new Set<String>();
		Map<String, Map<Boolean, List<Order_Response__c>>> distributedResponseMap = new Map<String, Map<Boolean, List<Order_Response__c>>>();

		public RelevantOrderIdInformation(List<Order_Response__c> orderResponseList) {
			distributeIds(orderResponseList);
		}

		private void distributeIds(List<Order_Response__c> orderResponseList) {
			for (Order_Response__c resp : orderResponseList) {
				orderIds.add(resp.Order__c);
				siteIds.add(resp.SFDC_Site_Id__c);
				acctIds.add(resp.Unify_Account_Id__c);
				acctIds.add(resp.BAN_BopCode__c);
				conIds.add(resp.SFDC_Contact_Id__c);

				String respType = resp.Type__c;

				Boolean isSuccess = SUCCESS_CODE.equalsIgnoreCase(resp.Unify_Error_Number__c);

				if (!distributedResponseMap.containsKey(respType)) {
					distributedResponseMap.put(
						respType,
						new Map<Boolean, List<Order_Response__c>>()
					);
				}
				if (!distributedResponseMap.get(respType).containsKey(isSuccess)) {
					distributedResponseMap.get(respType)
						.put(isSuccess, new List<Order_Response__c>());
				}

				distributedResponseMap.get(respType).get(isSuccess).add(resp);
			}
			orderIds.remove(null);
			siteIds.remove(null);
			acctIds.remove(null);
			conIds.remove(null);
		}
	}

	/**
	 * @description         This method retrieves all orders corresponding to the order responses.
	 */
	private void retrieveOrders(Set<Id> orderIds) {
		if (!orderIds.isEmpty()) {
			orderMap = new Map<Id, Order__c>(
				[SELECT Id, Status__c FROM Order__c WHERE Id IN :orderIds]
			);
		}
	}

	/**
	 * @description         This method allows for reprocessing of an order respons by removing the value from the Processing result field.
	 */
	private void doReprocessing(
		List<Order_Response__c> newResponses,
		Map<Id, Order_Response__c> oldResponsesMap
	) {
		List<Order_Response__c> orToReprocess = new List<Order_Response__c>();

		for (Order_Response__c o : newResponses) {
			if (
				o.Processing_result__c == null &&
				oldResponsesMap.get(o.Id).Processing_result__c != null
			) {
				orToReprocess.add(o);
			}
		}

		processUpdates(orToReprocess);
	}
}