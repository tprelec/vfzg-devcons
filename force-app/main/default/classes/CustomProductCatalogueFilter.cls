global with sharing class CustomProductCatalogueFilter implements cssmgnt.ProductCatalogueFilter {
	global List<cssmgnt.SolutionComponent> filter(List<cssmgnt.SolutionComponent> definitions, String params) {
		List<cssmgnt.SolutionComponent> filteredDefinitions = new List<cssmgnt.SolutionComponent>();
		try {
			Id profileId = UserInfo.getProfileId();
			String profileName = [SELECT Id, Name FROM Profile WHERE Id = :profileId].Name;

			Id userId = UserInfo.getUserId();
			String username = [SELECT Id, Name FROM User WHERE Id = :userId].Name;

			/*  String mobileSolutionFilter = '%MobileSolution%';
            Integer countSolutions = [
                      SELECT count()
                      FROM csord__Solution__c
                      WHERE csord__Account__c =:accountId 
                      AND Name LIKE :mobileSolutionFilter
              ]; */

			for (cssmgnt.SolutionComponent sc : definitions) {
				/* if (username == 'Jurica Bacani' && sc.Name.contains('Business')) {
                    continue;
                } */
				filteredDefinitions.add(sc);
			}
		} catch (Exception e) {
			System.debug('Exception occurred while filtering product catalogue : ' + e.getMessage());
			return definitions;
		}
		return filteredDefinitions;
	}
}
