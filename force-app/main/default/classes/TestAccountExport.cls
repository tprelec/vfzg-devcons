@isTest
private class TestAccountExport {
	@TestSetup
	static void makeData() {
		TestUtils.autoCommit = true;

		User u = TestUtils.createManager();

		Account a = TestUtils.createAccount(u);

		Ban__c banana = new Ban__c(
			Name = '399999999',
			Ban_Number__c = '399999999',
			BAN_Status__c = 'Open',
			Account__c = a.Id
		);
		insert banana;

		External_Account__c extAcct = new External_Account__c(
			Account__c = a.Id,
			External_Account_Id__c = 'ACD',
			External_Source__c = 'BOP'
		);
		insert extAcct;
	}

	@isTest
	static void testScheduleBANExportFromBanIds() {
		Ban__c banana = [SELECT Id FROM Ban__c LIMIT 1];
		Set<Id> banane = new Set<Id>{ banana.Id };

		Test.startTest();
		AccountExport.scheduleBANExportFromBanIds(banane);
		Test.stopTest();

		Ban__c postBan = [SELECT BOP_export_Errormessage__c FROM Ban__c WHERE Id = :banana.Id];
		System.assertEquals(
			'Pending scheduled export..',
			postBan.BOP_export_Errormessage__c,
			'Export error message not set.'
		);
	}

	@isTest
	static void testBanToReqInitialPart() {
		ECSCompanyService.request req = new ECSCompanyService.request();
		Ban__c banana = [
			SELECT
				Id,
				Account__r.Name,
				Account__r.Phone,
				Account__r.Fax,
				Account__r.KVK_number__c,
				Account__r.Website,
				Account__r.BillingStreet,
				Account__r.BillingPostalCode,
				Account__r.BillingCity,
				Account__r.BillingCountry,
				BOPCode__c
			FROM Ban__c
			LIMIT 1
		];
		banana.BOPCode__c = null;
		update banana;

		System.assertEquals(null, req.referenceId, 'Request referenceId should be null');
		System.assertEquals(null, req.name, 'Request name should not be null.');
		System.assertEquals(null, req.phone, 'Request phone should not be null.');
		System.assertEquals(null, req.kvkNumber, 'Request kvkNumber should not be null.');

		Test.startTest();
		AccountExport.banToReqInitialPart(banana, req);
		Test.stopTest();

		System.assertEquals(banana.Id, req.referenceId, 'Request referenceId should not be null.');
		System.assertEquals(banana.Account__r.Name, req.name, 'Request name should not be null.');
		System.assertEquals(
			banana.Account__r.Phone,
			req.phone,
			'Request phone should not be null.'
		);
		System.assertEquals(
			banana.Account__r.KVK_number__c,
			req.kvkNumber,
			'Request kvkNumber should not be null.'
		);
	}

	@isTest
	static void testBanToRequest() {
		// b.Account__r.Fixed_Dealer__r.Contact__r.UserId__r.UserType == 'PowerPartner'
		Account a = TestUtils.createPartnerAccount();
		User u = TestUtils.createPortalUser(a);

		Contact c = new Contact(
			LastName = 'TestLastname',
			FirstName = 'TestFirstname',
			Userid__c = u.Id,
			AccountId = a.Id,
			Marketing_Achiever__c = 'Ja',
			Marketing_Status__c = 'Actief'
		);
		insert c;

		Dealer_Information__c di = new Dealer_Information__c(
			Dealer_Code__c = '655555',
			Contact__c = c.Id,
			Name = 'test'
		);
		insert di;

		// a.Mobile_Dealer__c = di.Id;
		// update a;

		Ban__c banana = [
			SELECT
				Id,
				Name,
				Account__r.Name,
				Account__r.Phone,
				Account__r.Fax,
				Account__r.KVK_number__c,
				Account__r.Website,
				Account__r.BillingStreet,
				Account__r.BillingPostalCode,
				Account__r.BillingCity,
				Account__r.BillingCountry,
				Account__r.Fixed_Dealer__c,
				Account__r.Mobile_Dealer__c,
				Account__r.Owner.UserType,
				Account__r.Owner.Name,
				Account__r.GT_Fixed__c,
				Ban_Number__c,
				BOPCode__c
			FROM Ban__c
			LIMIT 1
		];
		banana.BOPCode__c = null;
		banana.Dealer_Code__c = '655555';
		banana.Account__c = a.Id;
		update banana;

		ECSCompanyService.request req = new ECSCompanyService.request();
		System.assertEquals(null, req.referenceId, 'Request referenceId should be null');

		Test.startTest();
		ECSCompanyService.request editedReq = AccountExport.banToRequest(
			banana,
			new Map<Id, Account>()
		);
		Test.stopTest();

		System.assertEquals(
			banana.Id,
			editedReq.referenceId,
			'Request referenceId should not be null.'
		);
	}

	@isTest
	static void testAccToRequest() {
		Account a = [
			SELECT Id, Name, Website, Phone, Fax, KVK_number__c, Owner.Name, Owner.UserType, OwnerId
			FROM Account
			LIMIT 1
		];
		User u = [SELECT Id, Name FROM User WHERE Id = :a.OwnerId];
		Account parent = TestUtils.createAccount(u);
		parent.BAN_Number__c = '399999993';
		update parent;

		a.BAN_Number__c = '399999999';
		a.BOPCode__c = '';
		a.BillingStreet = 'Balistraat';
		a.BillingPostalCode = '6969AZ';
		a.BillingCity = 'Utrecht';
		a.BillingCountry = 'Netherland';
		a.Dealer_code__c = '655555';
		a.Parent = parent;
		update a;

		Test.startTest();
		ECSCompanyService.request req = AccountExport.accountToRequest(a, new Map<Id, Account>());
		Test.stopTest();

		System.assertEquals(a.Id, req.referenceId, 'Request referenceId should not be null.');
		System.assertEquals(
			a.BillingPostalCode,
			req.billingPostalCode,
			'Request billingPostalCode should not be null.'
		);
		System.assertEquals(
			a.BAN_Number__c,
			req.banNumber,
			'Request banNumber should not be null.'
		);
	}

	@isTest
	static void testCreateBanWithRef() {
		Ban__c b = [SELECT Id FROM Ban__c LIMIT 1];
		Map<String, Id> refToBanId = new Map<String, Id>{ 'test' => b.Id };
		String reference = 'test';
		Set<Id> exportedBanIds = new Set<Id>();

		Test.startTest();
		Ban__c ban = AccountExport.createBan(refToBanId, reference, exportedBanIds);
		Test.stopTest();

		System.assertEquals(b.Id, ban.Id, 'Ban should have the same Id.');
	}

	@isTest
	static void testCreateBanWithExportedBans() {
		Ban__c b = [SELECT Id FROM Ban__c LIMIT 1];
		Map<String, Id> refToBanId = new Map<String, Id>();
		String reference = b.Id;
		Set<Id> exportedBanIds = new Set<Id>{ b.Id };

		Test.startTest();
		Ban__c ban = AccountExport.createBan(refToBanId, reference, exportedBanIds);
		Test.stopTest();

		System.assertEquals(b.Id, ban.Id, 'Ban should have the same Id.');
	}

	@isTest
	static void testHandleResponse() {
		Ban__c b = [SELECT Id, BOP_export_datetime__c FROM Ban__c LIMIT 1];
		Map<String, Id> refToBanId = new Map<String, Id>{ b.Id => b.Id };
		Set<Id> exportedBanIds = new Set<Id>();

		ECSCompanyService.response resp = new ECSCompanyService.response();
		resp.referenceId = b.Id;
		resp.bopCode = b.Id;
		List<ECSCompanyService.response> responses = new List<ECSCompanyService.response>();
		responses.add(resp);

		System.assertEquals(
			b.BOP_export_datetime__c,
			null,
			'BOP export date time should be empty.'
		);

		Test.startTest();
		AccountExport.newAccounts = new Set<Id>();
		AccountExport.banIdSet = new Set<Id>();
		AccountExport.bansToUpdate = new List<Ban__c>();
		AccountExport.handleResponse(responses, refToBanId, exportedBanIds);
		Test.stopTest();

		System.assert(
			!String.isBlank(b.BOP_export_datetime__c + ''),
			'BOP export date time should be populated.'
		);
	}

	@isTest
	static void testHandleResponseWithExpBans() {
		Ban__c b = [SELECT Id, BOP_export_datetime__c FROM Ban__c LIMIT 1];
		Map<String, Id> refToBanId = new Map<String, Id>();
		Set<Id> exportedBanIds = new Set<Id>{ b.Id };

		ECSCompanyService.response resp = new ECSCompanyService.response();
		resp.referenceId = b.Id;
		List<ECSCompanyService.response> responses = new List<ECSCompanyService.response>();
		responses.add(resp);

		System.assertEquals(
			b.BOP_export_datetime__c,
			null,
			'BOP export date time should be empty.'
		);

		Test.startTest();
		AccountExport.newAccounts = new Set<Id>();
		AccountExport.banIdSet = new Set<Id>();
		AccountExport.bansToUpdate = new List<Ban__c>();
		AccountExport.handleResponse(responses, refToBanId, exportedBanIds);
		Test.stopTest();

		System.assert(
			!String.isBlank(b.BOP_export_datetime__c + ''),
			'BOP export date time should be populated.'
		);
	}

	@isTest
	static void testHandleResponseError() {
		Ban__c b = [SELECT Id, BOP_export_datetime__c FROM Ban__c LIMIT 1];
		Map<String, Id> refToBanId = new Map<String, Id>();
		Set<Id> exportedBanIds = new Set<Id>{ b.Id };

		ECSCompanyService.response resp = new ECSCompanyService.response();
		resp.referenceId = b.Id;
		resp.errorCode = '48';
		List<ECSCompanyService.response> responses = new List<ECSCompanyService.response>();
		responses.add(resp);

		System.assertEquals(
			b.BOP_export_datetime__c,
			null,
			'BOP export date time should be empty.'
		);

		Test.startTest();
		AccountExport.newAccounts = new Set<Id>();
		AccountExport.banIdSet = new Set<Id>();
		AccountExport.bansToUpdate = new List<Ban__c>();
		AccountExport.handleResponse(responses, refToBanId, exportedBanIds);
		Test.stopTest();

		System.assert(
			!String.isBlank(b.BOP_export_datetime__c + ''),
			'BOP export date time should be populated.'
		);
	}

	@isTest
	static void testMakeCallout() {
		Ban__c b = [
			SELECT
				Id,
				BAN_Number__c,
				Account__r.Name,
				Account__r.Phone,
				Account__r.Fax,
				Account__r.KVK_number__c,
				Account__r.Website,
				Account__r.BillingStreet,
				Account__r.BillingCity,
				Account__r.BillingPostalCode,
				Account__r.BillingCountry,
				Account__r.Fixed_Dealer__c,
				Account__r.Mobile_Dealer__c,
				Account__r.Owner.UserType,
				Account__r.Owner.Name,
				Account__r.GT_Fixed__c,
				Unify_ref_Id__c,
				BOPCode__c,
				BOP_export_datetime__c,
				BOP_export_Errormessage__c
			FROM Ban__c
			LIMIT 1
		];
		List<Ban__c> banList = new List<Ban__c>{ b };
		String opp = 'createCustomer';
		Map<Id, User> banOwnerToUser = new Map<Id, User>();

		Test.startTest();
		AccountExport.banIdSet = new Set<Id>();
		AccountExport.bansToUpdate = new List<Ban__c>();
		AccountExport.makeCallout(banList, banOwnerToUser, opp);
		Test.stopTest();

		Ban__c b2 = [
			SELECT Id, BOP_export_datetime__c, BOP_export_Errormessage__c
			FROM Ban__c
			WHERE Id = :b.Id
		];

		System.assert(
			!String.isBlank(b2.BOP_export_datetime__c + ''),
			'BOP export date time should be populated.'
		);
		System.assert(
			String.isBlank(b2.BOP_export_Errormessage__c),
			'BOP export error message should be populated.'
		);
	}

	@isTest
	static void testValidateBans() {
		Account acc = [SELECT Id FROM Account LIMIT 1];
		Ban__c b = [
			SELECT Id, OwnerId, BAN_Number__c, BOPCode__c, BOP_export_Errormessage__c
			FROM Ban__c
			LIMIT 1
		];
		b.BOPCode__c = '666';
		update b;

		Ban__c b2 = new Ban__c(
			Name = '512345678',
			BOPCode__c = '999',
			Ban_Number__c = null,
			Account__c = acc.Id
		);
		insert b2;

		List<Ban__c> banList = new List<Ban__c>{ b, b2 };
		Map<Id, User> banOwnerToUser = new Map<Id, User>(
			[SELECT Id, UserType, Name, Manager.Name, AccountId FROM User]
		);

		Test.startTest();
		AccountExport.banIdSet = new Set<Id>();
		AccountExport.bansToUpdate = new List<Ban__c>();
		AccountExport.updateBanCustomerAccounts = new List<Ban__c>();
		AccountExport.validateBans(banList, banOwnerToUser);
		Test.stopTest();

		System.assertEquals(
			null,
			b.BOP_export_Errormessage__c,
			'BOP export error message should not be populated.'
		);
		System.assertEquals(
			'Valid BAN Number is required for exporting an Account',
			b2.BOP_export_Errormessage__c,
			'BOP export error message should be populated.'
		);
	}

	@isTest
	static void testCreateBopAccount() {
		Ban__c b = [
			SELECT Id, OwnerId, BAN_Number__c, BOPCode__c, BOP_export_Errormessage__c
			FROM Ban__c
			LIMIT 1
		];
		ExportBatchSchedule__c ebs = new ExportBatchSchedule__c();
		ebs.createBOPAccount__c = true;
		insert ebs;

		Test.startTest();
		AccountExport.createBanCustomerAccounts = new List<Ban__c>();
		AccountExport.createBopAccount(b);

		System.assertEquals(
			AccountExport.createBanCustomerAccounts[0],
			b,
			'List should contain one element.'
		);
		Test.stopTest();
	}

	@isTest
	static void testCreatePartnerAccMap() {
		Account acc = [SELECT Id FROM Account LIMIT 1];

		Test.startTest();
		AccountExport.partnerAcctIdToAcct = new Map<Id, Account>{ acc.Id => null };
		AccountExport.createPartnerAccMap();
		System.assert(
			AccountExport.partnerAcctIdToAcct.get(acc.Id) != null,
			'Map value should be populated.'
		);
		Test.stopTest();
	}

	@isTest
	static void testExportBans() {
		Ban__c b = [SELECT Id, BOPCode__c, OwnerId FROM Ban__c LIMIT 1];
		b.BOPCode__c = 'TNF';
		update b;

		Set<Id> banIds = new Set<Id>{ b.Id };
		Set<Id> acctIds = new Set<Id>();

		Test.startTest();
		AccountExport.exportBans(acctIds, banIds);
		Test.stopTest();
		Ban__c b2 = [SELECT Id, BOP_export_Errormessage__c FROM Ban__c LIMIT 1];

		System.assert(
			b2.BOP_export_Errormessage__c != null,
			'Export error message should be populated.'
		);
	}

	@isTest
	static void testHandleAccResponse() {
		String operation = 'createReseller';
		Account acc = [SELECT Id, BOPCode__c, BOP_export_datetime__c FROM Account LIMIT 1];
		Set<Id> acctId = new Set<Id>{ acc.Id };

		ECSCompanyService.response resp = new ECSCompanyService.response();
		resp.referenceId = acc.Id;
		resp.bopCode = acc.Id;
		List<ECSCompanyService.response> responses = new List<ECSCompanyService.response>();
		responses.add(resp);

		Test.startTest();
		AccountExport.newAccounts = new Set<Id>();
		AccountExport.accountsToUpdate = new List<Account>();
		AccountExport.handleResponse(responses, acctId, operation);
		Test.stopTest();

		System.assert(
			!String.isBlank(acc.BOP_export_datetime__c + ''),
			'BOP export date time should be populated.'
		);
		System.assert(!String.isBlank(acc.BOPCode__c), 'BOP code should be populated.');
	}

	@isTest
	static void testHandleAccErrorsResponse() {
		String operation = 'createReseller';
		Account acc = [SELECT Id, BOPCode__c, BOP_export_Errormessage__c FROM Account LIMIT 1];
		Set<Id> acctId = new Set<Id>{ acc.Id };

		ECSCompanyService.response resp = new ECSCompanyService.response();
		resp.referenceId = acc.Id;
		resp.bopCode = acc.Id;
		resp.errorCode = '48';
		resp.errorMessage = 'testError';
		List<ECSCompanyService.response> responses = new List<ECSCompanyService.response>();
		responses.add(resp);

		Test.startTest();
		AccountExport.newAccounts = new Set<Id>();
		AccountExport.accountsToUpdate = new List<Account>();
		AccountExport.handleResponse(responses, acctId, operation);

		Test.stopTest();
		System.assertEquals(
			acc.Id,
			AccountExport.accountsToUpdate[0].Id,
			'Account should be mapped for update.'
		);
	}

	@isTest
	static void testMakeAccCallout() {
		String operation = 'createReseller';
		Account acc = [
			SELECT
				Id,
				Name,
				Phone,
				Fax,
				KVK_number__c,
				Website,
				BillingStreet,
				BillingPostalCode,
				BillingCity,
				BillingCountry,
				Owner.UserType,
				Dealer_code__c,
				BAN_Number__c,
				BOPCode__c,
				BOP_export_datetime__c,
				BOP_export_Errormessage__c,
				Owner.Name,
				Parent.BAN_Number__c,
				parent.BOPCode__c
			FROM Account
			LIMIT 1
		];
		List<Account> accLst = new List<Account>{ acc };

		Test.startTest();
		AccountExport.newAccounts = new Set<Id>();
		AccountExport.accountsToUpdate = new List<Account>();
		AccountExport.makeCallout(accLst, operation);
		Test.stopTest();

		System.assert(
			!String.isBlank(acc.BOP_export_datetime__c + ''),
			'BOP export date time should be populated.'
		);
		System.assert(!String.isBlank(acc.BOPCode__c), 'BOP code should be populated.');
	}

	@isTest
	static void testBanValidation() {
		Account acc = [SELECT Id, BAN_Number__c, BOP_export_Errormessage__c FROM Account LIMIT 1];

		Test.startTest();
		AccountExport.accountsToUpdate = new List<Account>();
		Boolean validation = AccountExport.banValidation(acc);
		acc.BAN_Number__c = '223885';
		update acc;
		AccountExport.banValidation(acc);
		Test.stopTest();

		System.assertEquals(
			true,
			validation,
			'Method should return true to notyfy validation status.'
		);
		System.assertEquals(
			'Valid BAN Number is required for exporting an Account',
			acc.BOP_export_Errormessage__c,
			'Wrong error message.'
		);
	}

	@isTest
	static void testFilterOutAccounts() {
		Account acc = [SELECT Id, BAN_Number__c, BOP_export_Errormessage__c FROM Account LIMIT 1];
		acc.BAN_Number__c = '812345678';
		acc.BOPCode__c = '225';
		update acc;
		Account acc2 = Testutils.createPartnerAccount();
		acc2.BOPCode__c = '883';
		acc2.IsPartner = true;
		update acc2;
		Set<Id> acctIds = new Set<Id>{ acc.Id, acc2.Id };

		Test.startTest();
		AccountExport.filterOutAccounts(acctIds);
		Test.stopTest();

		System.assertEquals(
			acc2.Id,
			AccountExport.updateResellerAccounts[0].Id,
			'List should contain partner account.'
		);
	}
}