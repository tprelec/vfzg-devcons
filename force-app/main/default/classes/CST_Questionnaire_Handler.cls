public class CST_Questionnaire_Handler {
	@AuraEnabled
	public static List<CST_Questionnaire_Line_Item__c> getQuestionnaireLineItems(ID questionnaireId) {
		List<CST_Questionnaire_Line_Item__c> lineItemsList = [
			SELECT Id, name, CST_Question__c, CST_Answer__c, CST_QuestionType__c, CST_Questionaire_HelpText__c, CST_Mandatory__c
			FROM CST_Questionnaire_Line_Item__c
			WHERE CST_Questionnaire__c = :questionnaireId OR CST_Case__c = :questionnaireId
			ORDER BY name ASC
		];

		return lineItemsList;
	}
}
