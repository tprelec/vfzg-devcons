@isTest
public with sharing class TestClickApproveReminderService {
	@TestSetup
	static void makeData() {
		User u = GeneralUtils.currentUser;
		No_Triggers__c noTriggers = new No_Triggers__c(SetupOwnerId = u.Id, Flag__c = true);
		insert noTriggers;
		ClickApproveTestDataFactory.createClickApproveSettings();
		Account acc = TestUtils.createAccount(u);
		TestUtils.autoCommit = false;
		Contact con = TestUtils.createContact(acc);
		con.Email = 'test@test.com';
		insert con;
		Opportunity opp = TestUtils.createOpportunity(acc, Test.getStandardPricebookId());
		opp.RecordTypeId = GeneralUtils.getRecordTypeIdByName('Opportunity', 'Ziggo');
		opp.OwnerId = u.Id;
		insert opp;
	}

	@IsTest
	static void testClickApproveReminder() {
		Test.startTest();
		CSCAP__Customer_Approval__c approval = ClickApproveTestDataFactory.createApproval(getOpportunity().Id, getClickApproveSetting().Id);
		CSCAP__ClickApprove_Approver__c approver = ClickApproveTestDataFactory.createApprover(approval.Id, getContact().Id);
		ClickApproveReminderService.ClickApproveReminderRequest req = new ClickApproveReminderService.ClickApproveReminderRequest();
		req.approvalId = approval.Id;
		req.approverId = approver.CSCAP__Contact__c;
		req.templateId = getEmailTemplate().Id;
		ClickApproveReminderService.sendReminder(new List<ClickApproveReminderService.ClickApproveReminderRequest>{ req });
		Test.stopTest();
		if (EmailService.checkDeliverability()) {
			List<EmailMessage> emails = [SELECT Id FROM EmailMessage WHERE RelatedToId = :approval.Id];
			System.assert(emails.size() > 0, 'Reminder email should be sent.');
		}
	}

	private static CSCAP__Click_Approve_Setting__c getClickApproveSetting() {
		return [SELECT Id FROM CSCAP__Click_Approve_Setting__c LIMIT 1];
	}

	private static Contact getContact() {
		return [SELECT Id FROM Contact LIMIT 1];
	}

	private static Opportunity getOpportunity() {
		return [SELECT Id FROM Opportunity LIMIT 1];
	}

	private static EmailTemplate getEmailTemplate() {
		return [SELECT Id FROM EmailTemplate WHERE Name = 'Ziggo Quote Reminder'];
	}

	private static CSCAP__Customer_Approval__c getApproval() {
		return [SELECT Id, (SELECT Id, CSCAP__Contact__c FROM CSCAP__ClickApprove_Approvers__r) FROM CSCAP__Customer_Approval__c];
	}
}
