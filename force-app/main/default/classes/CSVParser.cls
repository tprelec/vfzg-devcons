/**
 * @description			This is a CSV parser which should be extended by a sub-class which
 *						contains the logic on how the file should be interpreted.
 * @author				Guy Clairbois
 */
public abstract class CSVParser {

    /**
     * @description			Comma String as defined by IETF RFC 4180.
     */
    private static final String DEFAULT_DELIMITER = ',';


	/**
	 * @description			This value is used to store the original row value into the list of values
	 *						which belong to a CSV row being processed. The reason why we store the original
	 *						value with the row values is to make it easier to ensure that the values and original
	 *						value are kept together after removing blank rows from the CSV.
	 */
	private static final String ORIGINAL_ROW_VALUE_PARAM = '__ORIGINAL_ROW_VALUE__:';


	/**
	 * @description			This exception will be thrown if any errors are encounted
	 *						when attempting to parse the CSV.
	 */
	public class ParseException extends Exception {}
	
	
	/**
	 * @description			This exception will be thrown if a parameter value is invalid, 
	 *						such as a null value or if it is not an acceptable value.
	 */
	public class ParameterValueException extends Exception {}
	
	
	/**
	 * @description			This exception will be thrown when the configuration in the class instance
	 *						is invalid when attempting to perform an action.
	 */
	public class InvalidConfigurationException extends Exception {}
	
	
	/**
	 * @description			This is the original input value used for processing the header row in the CSV. If
	 *						the CSV did not include a header row then this value will be blank.
	 */
	private String headerRowOrigRawInput = '';
	
	
	/**
	 * @description			This controls whether we will store the original CSV input memory (header row and 
	 *						individual rows). When setting this to true the raw input can be accessed using the
	 *						following methods:
	 *
	 *							header row - parser.getOrigHeaderRowRawInput()
	 *							row	- csvRow.getOrigRawInput()
	 */
	private Boolean storeOriginalRawInput = false;
	

	/**
	 * @description			Represents a single column in the CSV. It holds the name
	 *						of the column heading, or if not using headings it will
	 *						contain a given name such as "col1" or "col2" etc.
	 */
	public class Column {
		
		private String name { get; set; }
		
		public String getName(){
			return name;
		}
		
		public Column(String name){
			this.name = name;
		}
		
	}
	
	
	/**
	 * @description			Represents a single row in the CSV. It is an abstract class
	 *						because the child class will provide all of the class properties
	 *						which will hold the values for each cell.
	 */
	public abstract class CSVRow {
		
		public Integer rowNumber {get;set;}
		public String origRawInput {get;set;}
		private Map<String, Object> columnPropertyMap = new Map<String, Object>(); 
		private Map<String, RowValueWriter> columnCustomWriterMap = new Map<String, RowValueWriter>();
		
		/**
		 * @description			This will map a column name to a property in the CSV instance found in the child
		 *						class. This is used when generating a CSV using the data held in memory.
		 */
		public void mapColumnToProperty(String columnName, Object property){
			columnPropertyMap.put(columnName, property);
		}
		
		/**
		 * @description			This will map a column name to a property in the CSV instance found in the child
		 *						class. This is used when generating a CSV using the data held in memory. This method
		 *						however allows setting a custom row value writer which will be used instead of simply
		 *						outputting the value in the CSV.
		 */
		public void mapColumnToProperty(String columnName, Object property, RowValueWriter customWriter){
			mapColumnToProperty(columnName, property);
			columnCustomWriterMap.put(columnName, customWriter);
		}
		
		/**
		 * @description			This will return the property reference which belongs to the column name provided.
		 *						It is the responsibility of the child class to map the properties to the column names.
		 */
		public Object getPropertyForColumnName(String columnName){
			return columnPropertyMap.get(columnName);
		}

		/**
		 * @description			This will return the custom row value writer instance which is associated to the
		 *						column name provided.
		 */
		public RowValueWriter getCustomRowValueWriterForColumnName(String columnName){
			return columnCustomWriterMap.get(columnName);
		}
		
		/**
		 * @description			This will call the setupColumnPropertyMappings method in the child class
		 *						to request it to setup the mappings between the properties found in the child class
		 * 						and the column names found in the CSV.
		 */
		public virtual void setupColumnPropertyMappings(){}
		
	}
	
	
	/**
	 * @description			This list contains all of the CSV rows loaded.
	 */
	private List<CSVParser.CSVRow> csvRows {
		get {
			if(csvRows == null) csvRows = new List<CSVParser.CSVRow>();
			return csvRows;
		}
		set;
	}
	
	
	/**
	 * @description			This list contains all of the columns which are expected
	 *						to be in the CSV.
	 */
	private List<Column> columns {
		get {
			if(columns == null) columns = new List<Column>();
			return columns;
		}
		set;
	}
	
	
	/**
	 * @description			This stores the index number of where a column name was found
	 *						in the CSV. It allows columns to be positioned in any order
	 *						in the CSV and to allow retrieval of the correct column names.
	 */
	private Map<Integer, String> indexColumnNameMap {
		get {
			if(indexColumnNameMap == null) indexColumnNameMap = new Map<Integer, String>();
			return indexColumnNameMap;
		}
		set;
	}
	
	
	/**
	 * @description			This holds the delimiter value which will be used to split
	 *						the values in the CSV. It defaults to being a comma.
	 */
	private String delimiter {
		get {
			if(delimiter == null) delimiter = DEFAULT_DELIMITER;
			return delimiter;
		}
		set;
	}
	
	
	/**
	 * @description			Indication of whether the CSV being processed contains a heading
	 *						row. If this is set to false then each column will be given a unique
	 *						name such as "col1" or "col2" etc.
	 */
	private Boolean hasHeadingRow {
		get {
			if(hasHeadingRow == null) hasHeadingRow = true;
			return hasHeadingRow;
		}
		set;
	}
	
	
	/**
	 * @description			This stores all of the rows and values loaded from the CSV.
	 */
	private List<List<String>> fileValues {
		get {
			if(fileValues == null) fileValues = new List<List<String>>();
			return fileValues;
		}
		set;
	}
	
	
	/**
	 * @description			If explicit column header groups have been defined in the sub-class
	 *						then this property stores the friendly group name and a set of the column
	 *						names.
	 */
	private Map<String, Set<String>> groupColumnNamesMap {
		get {
			if(groupColumnNamesMap == null) groupColumnNamesMap = new Map<String, Set<String>>();
			return groupColumnNamesMap;
		}
		set;
	}
	
	
	/**
	 * @descripion			If explicit column header groups have been defined in the sub-class
	 *						then this property stores the friendly name of the group matched.
	 */
	private String groupNameMatched {
		get {
			if(groupNameMatched == null) groupNameMatched = '';
			return groupNameMatched;
		}
		set;
	}
	
	
	/**
	 * @description			This holds the number of rows processed after a file has been read.
	 */
	private Integer numberRowsProcessed {
		get {
			if(numberRowsProcessed == null) numberRowsProcessed = 0;
			return numberRowsProcessed;
		}
		set;
	}
	
	
	/**
	 * @description			This indicates whether the sub-class has called the method readFile with
	 *						a valid file.
	 */
	private Boolean hasFileBeenRead {
		get {
			if(hasFileBeenRead == null) hasFileBeenRead = false;
			return hasFileBeenRead;
		}
		set;
	}
	
	
	/**
	 * @description			This indicates when parsing a CSV if the initial rows are empty then they will be 
	 *						removed, kind of like TRIM functionality.
	 */
	private Boolean removeBlankRows {
		get {
			if(removeBlankRows == null) removeBlankRows = false;
			return removeBlankRows;
		}
		set;
	}
	
	
	/**
	 * @description			This should be called in the sub-class to start the process of
	 *						reading the CSV and setting up the columns and rows. It will 
	 *						ultimately it will call the sub-classes setRowCellValue method
	 *						will populate a class with a rows values.
	 */
	public virtual void readFile(Blob file){
		// Ensure that we are working with a file
		if(file == null){
			throw new ParameterValueException('Cannot parse file when the file is null');
		}
		
		// Ensure the file ends with a line feed and it is a text file
		String fileString;
			
		try {
			fileString = file.toString();
		} catch(Exception ex){
			throw new ParseException('The file being processed is not a CSV file');
		}
        
        // Retrieve all of the rows and values in the CSV
        hasFileBeenRead = true;
		fileValues.clear();
		List<String> rowValues = new List<String>();
        List<String> csvRows = fileString.split( '\n|\r' );
        Integer index = 0;
        
    	for(String csvRow : csvRows){
    		rowValues = splitCSVRow(csvRow);
    			
			if(rowValues.size() == 1 && rowValues[0] == '') continue;
			if(storeOriginalRawInput) rowValues.add(ORIGINAL_ROW_VALUE_PARAM + csvRow);
    		
    		fileValues.add( rowValues );
    		index++;
    	}
		
		// Process the CSV rows and values
		removeBlankRow();
		setupColumnsFoundInCSV();
		setupRowsFoundInCSV();
	}
	
	
	/**
	 * @description			This will split one individual row up into the individual cell values using regular
	 *						expressions. The reason why regular expressions are used is due to the system not having
	 *						enough script statements available for complex CSV's.
	 */
	public List<String> splitCSVRow( String line ) { 
		List<String> elements = new List<String>();		
		Matcher m = Pattern.compile( '(?:^|' + delimiter + ')(\"(?:[^\"]|\"\")*\"|[^' + delimiter + ']*)' ).matcher( line );
		
		while( m.find() ) { 
			String value = m.group() 
				.replaceAll( '^' + delimiter, '' ) // remove first comma if any 
				.replaceAll( '^?\"(.*)\"$', '$1' ) // remove outer quotations if any 
				.replaceAll( '\"\"', '\"' )  // replace double inner quotations if any
				.replaceAll( '\\uFEFF', '' ); // Remove any single no width chars 
			elements.add( value ); 
		}
		
		if(line.startsWith(delimiter)){
			List<String> tmp = new List<String>{ '' };
			tmp.addAll(elements);
			elements = tmp;
		}
			 
		return elements;
	}
    
    
    /**
     * @description			This will remove all of the empty rows found in the CSV. An empty row is 
     *						a row which has no values found (i.e. not a blank string).
     */
    private void removeBlankRow(){
		if(!removeBlankRows) return;
    		
    	List<List<String>> fileValuesTmp = new List<List<String>>();
    	Integer numRemoved = 0;
    	
		for(List<String> rowValues : fileValues){
			if(!rowValues.isEmpty()){
				// Check that there are values on this row - a value is not a blank string or null
				Boolean rowHasContent = false;
				
				for(String cellValue : rowValues){
					if(cellValue != '') rowHasContent = true;
				}
			
				if(rowHasContent){
					fileValuesTmp.add(rowValues);
				} else {
					numRemoved++;
				}
			} else {
				numRemoved++;
			}
		}
	
    	fileValues = fileValuesTmp;
    	numberRowsProcessed -= numRemoved;
    }
    
    
    /**
     * @description			This will take the first row from the CSV and setup new columns
     *						for each value found. If the CSV has no heading row, then this
     *						method will create sudo column names such as "col1" or "col2" etc.
     *						The number of values found on the first row will then dictate the
     *						number of sudo columns to be created.
     */
    private void setupColumnsFoundInCSV(){
    	columns.clear();
    	indexColumnNameMap.clear();
    	List<String> firstRow = !fileValues.isEmpty()? fileValues[0] : new List<String>();
    	Integer columnIndex = 0;
    	Set<String> columnNames = new Set<String>();
    	groupNameMatched = null;
    	
    	// Setup the columns using the values on the first row
		for(Integer i=0; i < firstRow.size(); i++){
			String columnName = hasHeadingRow? firstRow[i].toUpperCase().trim().replace('  ', ' ') : 'COL' + i;
			
			if(columnName != '' && !columnName.startsWith( ORIGINAL_ROW_VALUE_PARAM )){
				columnNames.add(columnName);
				columns.add(new Column(columnName));
				indexColumnNameMap.put(columnIndex++, columnName);
			}
			
			if(columnName != '' && columnName.startsWith( ORIGINAL_ROW_VALUE_PARAM ) && storeOriginalRawInput){
				headerRowOrigRawInput = columnName.substringAfter( ORIGINAL_ROW_VALUE_PARAM );
			}
		}
		
		// If explicit column header groups have been defined, check to ensure that the loaded columns match
		if(!groupColumnNamesMap.isEmpty()){
			Boolean foundGroup = false;
			
			for(String groupName : groupColumnNamesMap.keySet()){
				Set<String> explicitColumns = groupColumnNamesMap.get(groupName);
				System.debug(LoggingLevel.ERROR, 'columnNames: ' + JSON.serialize(columnNames));
				System.debug(LoggingLevel.ERROR, 'explicitColumns: ' + JSON.serialize(explicitColumns));
				
				if(explicitColumns.containsAll(columnNames) && explicitColumns.size() == columnNames.size()){
					foundGroup = true;
					groupNameMatched = groupName;
					break;
				}
			}
		
			if(!foundGroup){
				throw new ParseException('Column headers found in CSV do not match any of the explicit column groups expected');
			}
		}
    }
    
    
    /**
     * @description			This will setup all of the values on one row in the CSV
     *						and convert the values into the correct data type. It does
     *						this by calling the sub-classes setRowCellValue method.
     */
    private void setupRowsFoundInCSV(){
    	csvRows.clear();
    	fileValues.size();
    	Integer startRow = hasHeadingRow? 1 : 0;
    	numberRowsProcessed = 0;
    	
    	for(Integer i=startRow; i < fileValues.size(); i++){
    		List<String> rowValues = fileValues[i];
    		CSVParser.CSVRow csvRow = createCSVRow();
    			
			// Ensure that the sub-class has defined a class which will hold the CSV row data
			if(csvRow == null){
				throw new InvalidConfigurationException('Sub-class must return an instance of CSVRow for the method createCSVRow');
			}
    		
    		csvRow.rowNumber = i;
			// Ensure that the row has the correct amount of values, taking into account the original row
			// values entry which is added to the end of the list (__ORIGINAL_ROW_VALUE__:)
			if(rowValues.size() - (storeOriginalRawInput? 1:0) != columns.size()){
  				throw new ParseException('Number of values do not match number of columns on row: ' + i + ' : Column size: '+columns.size() + ' : Row size: '+rowValues.size());
  			}
   					
			// Now convert the value to the correct data type in the sub-class
			for(Integer j=0; j < rowValues.size(); j++){
				String columnName = indexColumnNameMap.get(j);
   						
				try {
					if(rowValues[j].trim() != '' && !rowValues[j].startsWith(ORIGINAL_ROW_VALUE_PARAM) ){
						setRowCellValue(csvRow, columnName, rowValues[j].trim());
						
					} else if(rowValues[j].startsWith(ORIGINAL_ROW_VALUE_PARAM) && storeOriginalRawInput){
						csvRow.origRawInput = rowValues[j].substringAfter(ORIGINAL_ROW_VALUE_PARAM);
					}
				} catch(ParseException ex){
					throw new ParseException('Unable to process value on row ' + (i+1) + ' for the column ' + columnName + ': ' + ex.getMessage());
				}
			}
				
   			csvRows.add(csvRow);
   			numberRowsProcessed++;
   		}
    }
	
	
	/**
	 * @description			This will add a group of column names which are expected to be in the CSV.
	 *						Each group is given a name so the sub-class can determine which group has been
	 *						loaded.
	 * @param	columnNames	A set of column names which are expected to be in the CSV.
	 * @param	groupName	A friendly name which represents the group of column names to allow the sub-class
	 *						easy access for checking which columns are present in the CSV.
	 */
	protected void addExplicitColumnHeaderGroupWithName(Set<String> columnNames, String groupName){
			if(!hasHeadingRow) throw new InvalidConfigurationException('Explicit column headers cannot be used for a CSV which does not have a heading row');
			if(columnNames == null) throw new ParameterValueException('Cannot add explicit column header group when the column names is null');
			if(columnNames.isEmpty()) throw new ParameterValueException('Cannot add explicit column header group when the column names set is empty');
			if(groupName == null) throw new ParameterValueException('Cannot add explicit column header group when the group name is null');
			if(groupName.trim() == '') throw new ParameterValueException('Cannot add explicit column header group when the group name is blank');
			
		groupName = groupName.trim().toUpperCase();
		Set<String> tmpColumnNames = new Set<String>();
			
			for(String columnName : columnNames){
				tmpColumnNames.add(columnName.trim().toUpperCase());
			}
		
		groupColumnNamesMap.put(groupName, tmpColumnNames);
	}
	
	
	/**
	 * @description			This is used by the sub-class to dertermine which explicit column header
	 *						group was found. It cannot be called when the CSV has no heading row
	 *						or if no explicit column header groups have been defined.
	 * @return				The friendly name of the explicit column heading group found in the CSV.
	 */
	public String getExplicitColumnHeaderGroupNameFound(){
			if(!hasHeadingRow) throw new InvalidConfigurationException('Explicit column headers cannot be used for a CSV which does not have a heading row');
			if(groupColumnNamesMap.isEmpty()) throw new InvalidConfigurationException('Explicit column header groups must be defined first before attempting to identify which group is present');
			if(!hasFileBeenRead) throw new InvalidConfigurationException('Cannot get the explicit column header group name found before a CSV has been read');
			
		return groupNameMatched;
	}
	
	
	/**
	 * @return				List of rows loaded from the CSV file.
	 */
	public List<CSVParser.CSVRow> getCSVRows(){
		return csvRows;
	}


	/**
	 * @return				Indication of whether the CSV being processed has a heading row.
	 */
	protected Boolean getHasHeadingRow(){
		return hasHeadingRow;
	}
	
	
	/**
	 * @description			Allows setting of whether the CSV being processed has a heading row
	 *						containing the column names or not. When this is set to false then
	 *						each column will be given a unique name such as "col1" or "col2" etc.
	 */
	protected void setHasHeadingRow(Boolean hasHeadingRow){
			if(hasHeadingRow == null) throw new ParameterValueException('Cannot set has heading row to null');
			if(!groupColumnNamesMap.isEmpty() && !hasHeadingRow){
				throw new InvalidConfigurationException('Cannot set has heading row to false when explicit column groups have been defined');
			}
		
		this.hasHeadingRow = hasHeadingRow;
	}
	
	
	/**
	 * @description			This will set whether the blank rows at the beginning of a CSV should be removed
	 *						before processing takes place.
	 */
	protected void setRemoveBlankRows(Boolean removeBlankRows){
			if(removeBlankRows == null) throw new ParameterValueException('Cannot set remove blank rows to null');
			
		this.removeBlankRows = removeBlankRows;
	}
	
	
	/**
	 * @return				Returns whether the blank rows at the beginning of a CSV should be removed
	 *						before processing takes place.
	 */
	protected Boolean getRemoveBlankRows(){
		return removeBlankRows;
	}
	
	
	/**
	 * @return				The number of rows which have been found in the CSV. The number does not 
	 *						include the heading row in its calculation. 
	 */
	public Integer getNumberOfRows(){
			if(!hasFileBeenRead){
				throw new InvalidConfigurationException('Cannot get the number of rows processed before a CSV has been read or if the file is empty');
			}
			
		return numberRowsProcessed;
	}
	
	
	/**
	 * @description			This controls whether the original values used when parsing the CSV should be
	 *						kept in memory to allow the application to retrieve the original values. I.e. it 
	 *						may require the original data to recreate the CSV as output.
	 */
	public void setStoreOriginalRawInput(Boolean storeOriginalRawInput){
			if(storeOriginalRawInput == null) throw new ExInvalidParameterException('Cannot set storeOriginalRawInput to null');
			
		this.storeOriginalRawInput = storeOriginalRawInput;
	}
	
	
	/**
	 * @description			The original value read as being the header row in the CSV. 
	 */
	public String getOrigHeaderRowRawInput(){
			if(!hasFileBeenRead){
				throw new InvalidConfigurationException('Cannot get the raw input for the header row before a CSV has been read or if the file is empty');
			}
		
		return headerRowOrigRawInput;
	}
	
	
	/**
	 * @description			This will convert a text value in a CSV into an
	 *						boolean primitive type. If the value is null or invalid
	 *						then it will return null.
	 */
	protected Boolean convertToBoolean(String value){
		Boolean compiledBool;
			
			if(value.toLowerCase() == 'true' || value.toLowerCase() == 'false'){
				try {
					compiledBool = Boolean.valueOf(value);
				} catch(Exception ex){
					throw new ParseException('Cannot convert value to an integer');
				}
			}
		
		return compiledBool;
	}
	
	/**
	 * @description			This will convert a number stored in a CSV into an
	 *						integer primitive type. If the value is null or invalid
	 *						then it will return null.
	 */
	protected Integer convertToInteger(String value){
		Integer compiledInt;
			
			if(value != null && value != ''){
				try {
					compiledInt = Integer.valueOf(value);
				} catch(Exception ex){
					throw new ParseException('Cannot convert value to an integer');
				}
			}
		
		return compiledInt;
	}
	
	
	/**
	 * @description			This will convert a number stored in a CSV into an
	 *						double primitive type. If the value is null or invalid
	 *						then it will return null.
	 */
	protected Double convertToDouble(String value){
		Double compiledDbl;
			
			if(value != null && value != ''){
				try {
					compiledDbl = Double.valueOf(value);
				} catch(Exception ex){
					throw new ParseException('Cannot convert value to a double');
				}
			}
		
		return compiledDbl;
	}
	
	
	/**
	 * @description			This will convert a number stored in a CSV into an
	 *						double primitive type. If the value is null or invalid
	 *						then it will return null. However, unlike the 
	 *						convertToDouble method, this method expects the string
	 *						to be in the format 1.2334.33,02.
	 */
	protected Double convertToDoubleNL(String cellValue){
		Double compiledDbl;
		cellValue = cellValue != null? cellValue.trim() : '';
		
			if(cellValue != ''){
				cellValue = cellValue.replace('\\.', '').replace(',', '.');
				compiledDbl = convertToDouble(cellValue);
			}
		
		return compiledDbl;
	}
	
	
	/**
	 * @description			This will convert a number stored in a CSV into an
	 *						decimal primitive type. If the value is null or invalid
	 *						then it will return null.
	 */
	protected Double convertToDecimal(String value){
		Double compiledDec;
			
			if(value != null && value != ''){
				try {
					compiledDec = Decimal.valueOf(value);
				} catch(Exception ex){
					throw new ParseException('Cannot convert value to a decimal');
				}
			}
		
		return compiledDec;
	}
	
	/**
	 * @description			This will convert a string stored in the CSV into a date
	 *						primative type. If any errors occur during compilation of the
	 *						date the method will return null.
	 */
	protected Date convertToDate(String value){
			if(value == null || value == '') return null;
			
		String dateStr;
		Date compiledDate;
		
		// Attempt to put the data into the correct format
		// e.g. 2012-01-01
		// from the format 01012012
		if(!value.contains('-')){
			try {
				dateStr = value.substring(4,8) + '-'
						+ value.substring(2,4) + '-'
						+ value.substring(0,2);
			} catch(Exception ex){
				throw new ParseException('Date value is invalid');
			}
			
		}else{
			try {
				dateStr = value.substring(6,10) + '-'
						+ value.substring(3,5) + '-'
						+ value.substring(0,2);
			} catch(Exception ex){
				throw new ParseException('Date value is invalid');
			}
		
		}
		
		// Now attempt to compile the string into a date
		try {
			compiledDate = Date.valueOf(dateStr);
		} catch(Exception ex){
			throw new ParseException('Date value is invalid');
		}
		
		return compiledDate;
	}
	
	
	/**
	 * @description			This will convert a string stored in the CSV into a date time
	 *						primative type. If any errors occur during compilation of the
	 *						date the method will return null.
	 */
	protected DateTime convertToDateTime(String value){
		if(value == null || value == '') return null;
			
		String dateStr;
		DateTime compiledDateTime;
		
		// Attempt to put the data into the correct format
		// e.g. 2012-01-01 12:00:55
		// from the format 01012012 120055
		try {
			dateStr = value.substring(4,8) + '-'
					+ value.substring(2,4) + '-'
					+ value.substring(0,2) + ' '
					
					+ value.substring(9, 11) + ':'
					+ value.substring(11, 13) + ':'
					+ value.substring(13, 15);
		} catch(Exception ex){
			throw new ParseException('Date/time value is invalid');
		}
		
		// Now attempt to compile the string into a date
		try {
			compiledDateTime = DateTime.valueOf(dateStr);
		} catch(Exception ex){
			throw new ParseException('Date/time value is invalid');
		}
		
		return compiledDateTime;
	}
	
	
	/**
	 * @return				The delimiter which is found in the CSV file between values.
	 */
	public String getDelimiter(){
		return delimiter;
	}
	
	
	/**
	 * @description			This sets the delimiter which will be used to seperate the values
	 *						in the CSV file.
	 * @param	delimiter	The delimiter which will be used to seperate the values in the
	 *						CSV file.
	 */
	public void setDelimiter(String delimiter){
		if(delimiter == null) throw new ParameterValueException('Cannot set the delimiter to null');
		if(delimiter.trim() == '') throw new ParameterValueException('Cannot set the delimiter value to blank');
		
		this.delimiter = delimiter;
	}
	
	
	/**
	 * @return				Indication of whether all of the column names in the CSV load exactly
	 *						match the list of column names provided.
	 */
	protected Boolean hasCSVColumnHeadingNames(Set<String> expectedColumnNames){
		if(expectedColumnNames == null) throw new ParameterValueException('Expected column names cannot be null');
		if(expectedColumnNames.isEmpty()) throw new ParameterValueException('Expected column names cannot be empty');
		
		// Convert all of the expected column names to upper case and trim them
		Set<String> columnNames = new Set<String>();
			
		for(String expectedColumnName : expectedColumnNames){
			if(expectedColumnName != null){
				columnNames.add(expectedColumnName.toUpperCase().trim());
			}
		}
			
		// Now create a set of the loaded column names
		Set<String> loadedColumnNames = new Set<String>();

		for(Column column : columns){
			if(column.getName() != null){
				loadedColumnNames.add(column.getName().toUpperCase().trim());
			}
		}
					
		return loadedColumnNames.containsAll(columnNames);
	}
	
	
	/**
	 * @description			This method is used to convert the CSV value into the correct
	 *						data type and assign it to a class property for a CSV row.
	 * @param	csvRow		This will be a concrete instance of a class stored in the 
	 *						sub-class, it represents the structure of the row.
	 * @param	columnName	This is the name of the row the cell value was found under.
	 *						It is provided to the method to allow it to convert the cell value
	 *						into the correct data type and then to store it in the csvRow.
	 * @param	cellValue	The value held in the CSV file as a string.
	 */
	protected abstract void setRowCellValue(CSVParser.CSVRow csvRow, String columnName, String cellValue);
	
	
	/**
	 * @description			This forces the sub-class to instantiate a concrete class which
	 *						is an instance of CSVRow. This is used when setting a CSV row
	 *						cell value.
	 */
	protected abstract CSVParser.CSVRow createCSVRow();
	
	
	
	/**
	 * @description			This will add an additional column which will be outputted in the CSV if generateCSV
	 *						method is called.
	 */
	protected void addColumn(String columnName){
		if(columnName == null) throw new ExInvalidParameterException('Cannot added a new column with a null column name');
		
		columnName = columnName.toUpperCase().trim().replace('  ', '');
		columns.add(new Column(columnName));
		indexColumnNameMap.put(indexColumnNameMap.size(), columnName);
	}
	
	
	
	/**
	 * @description			This will regenerate the CSV using the values derived from the CSV classes and their
	 *						new values populated by the application. It will output the columns in the same order
	 *						as they were originally provided.
	 */
	public virtual String generateCSV(){
		String csv = '';

		// Setup the header row, if one was provided originally
		if(hasHeadingRow){
			for(Column column : columns){	
				csv +=  (csv != ''? delimiter : '') + column.getName();
			}
			csv += '\n';
		}
			
		// Now add all of the values from the CSV rows
		for(CSVRow csvRow : csvRows){
			csvRow.setupColumnPropertyMappings();
			
			for(Integer i=0; i < columns.size(); i++){
				String columnName = columns[i].getName();
				Object value = csvRow.getPropertyForColumnName( columnName );
				String csvValue;
				RowValueWriter customWriter = csvRow.getCustomRowValueWriterForColumnName( columnName );
				
				if(value != null && customWriter == null){
					if(value instanceOf String){
						csvValue = (String) value;
					} else if(value instanceOf Integer){
						csvValue = ((Integer) value).format();
					} else if(value instanceOf Decimal){
						csvValue = ((Decimal) value).format();
					} else {
						throw new ExInvalidConfigurationException('The object value is not supported when generating a CSV (' + columnName + ')');
					}
					
				} else if(value != null && customWriter != null){
					csvValue = customWriter.convertToString( columnName, value );
				}
				csv += (i > 0? delimiter : '') + (csvValue != null? csvValue : '');
			}
			csv += '\n';
		}
		return csv;
	}
	
	
	/**
	 * @description			This class is responsible for converting a value such as a date or integer back into 
	 *						the format it is expected when generating a CSV.
	 */
	public abstract class RowValueWriter {
		public abstract String convertToString(String columnName, Object value);
	}
	
}