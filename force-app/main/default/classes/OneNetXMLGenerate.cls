public class OneNetXMLGenerate {
	private static String ATTACHMENT_PROVISIONING = 'Provisioning XML';

	public static void createAndSaveProvisioningXML(Id orderId) {
		// fetch contractedproducts (priceplan)
		Set<Id> orderIds = new Set<Id>{ orderId };
		Map<Id, Map<Id, DeliveryWrapper>> orderSiteMap = OrderUtils.retrieveOrderIdToLocationIdToLocation(null, orderIds);
		Map<Id, DeliveryWrapper> siteMap = orderSiteMap.get(orderId);

		Savepoint sp = Database.setSavepoint();
		try {
			// fetch order details
			Order__c order = OrderUtils.getOrderDataByContractId(null, orderIds)[0];
			
			// generate xml
			String xml = '';
			if (siteMap != null) {
				xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes" ?><CustomerOrder>';
				for (DeliveryWrapper dw : siteMap.values()) {
					if (dw.getHasFixedVoice() && !dw.allVoiceLinks.isEmpty()) {
						String benNo = (dw.pbxs.isEmpty() ? '0' : (dw.pbxs[0].BEN_Number__c != null ? dw.pbxs[0].BEN_Number__c : order.BEN_Number__c));
						xml += '<Customer>' + '<BANBEN>' + order.VF_Contract__r.Opportunity__r.BAN__r.BAN_Number__c + '/' + benNo + '</BANBEN>' + '<Postcode>'+ dw.site.Site_Postal_Code__c.subString(0,4) + '</Postcode>'
							+ '<StreetNumber>' + dw.Site.Site_House_Number__c + (dw.Site.Site_House_Number_Suffix__c == null ? '' : dw.Site.Site_House_Number_Suffix__c) + '</StreetNumber>'
							+ '<CODEC>' + dw.allLinks[0].cp.Codec__c + '</CODEC>' + '<VoiceChannels>' + dw.allVoiceLinks[0].cp.Number_of_sessions__c + '</VoiceChannels>' + '</Customer>';
					}
				}
				xml += '</CustomerOrder>';
			}

			// check if xml already exists as contract attachment (if so, remove)
			List<Contract_Attachment__c> caList = [
				SELECT 
					Id, 
					Name,
					Attachment_Type__c, 
					Description_Optional__c,
					CreatedById,
					CreatedDate
				FROM 
					Contract_Attachment__c 
				WHERE 
					Contract_VF__c = :order.VF_Contract__c
				AND
					Attachment_Type__c = :ATTACHMENT_PROVISIONING
			];
			if (!caList.isEmpty()) {
				delete caList;
			}

			// save contract_attachment
			Contract_Attachment__c ca = new Contract_Attachment__c();
			ca.Attachment_Type__c = ATTACHMENT_PROVISIONING;
			ca.Description_Optional__c = 'Generated from Order Form';
			ca.Contract_VF__c = order.VF_Contract__c;
			insert ca;

			// save xml
			if (Test.isRunningTest()) { xml = 'Test'; }
			Blob xmlBlob = Blob.valueOf(xml);
			ContentVersion cv = new ContentVersion();
			cv.Title = ATTACHMENT_PROVISIONING;
			cv.PathOnClient = order.Name + '_' + ATTACHMENT_PROVISIONING.replace(' ', '_') + '.xml';
			cv.VersionData = xmlBlob;
			insert cv;

			cv = [SELECT ContentDocumentId FROM ContentVersion WHERE Id = :cv.Id];
			ContentDocumentLink cdl = new ContentDocumentLink();
			cdl.ContentDocumentId = cv.ContentDocumentId;
			cdl.LinkedEntityId = ca.Id; // Link the document to the contract attachment
			cdl.ShareType = 'V';
			insert cdl;
		} catch (Exception e) {
			Database.rollback(sp);
			ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, 'Error generating XML document: ' + e));
		}
	}
}