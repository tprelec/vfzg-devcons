public class COM_MACDController {
	public COM_MACDResult executeBusinessLogic(
		List<csord__Service__c> serviceList,
		COM_MetadataInformation metadataInformation,
		List<COM_MetadataInformation.DeliveryComponent> deliveryComponents,
		List<COM_Delivery_Order__c> createdParentDeliveryOrderList
	) {
		COM_MACDResult macdResult = new COM_MACDResult();

		List<csord__Service__c> terminateServiceList = new List<csord__Service__c>();
		List<csord__Service__c> moveServiceList = new List<csord__Service__c>();
		List<csord__Service__c> changeServiceList = new List<csord__Service__c>();

		for (csord__Service__c macdParentServiceIterate : serviceList) {
			if (isTerminateScenario(macdParentServiceIterate)) {
				terminateServiceList.add(macdParentServiceIterate);
			} else if (isMoveScenario(macdParentServiceIterate)) {
				moveServiceList.add(macdParentServiceIterate);
			} else if (isChangeScenario(macdParentServiceIterate)) {
				changeServiceList.add(macdParentServiceIterate);
			}
		}

		if (!terminateServiceList.isEmpty()) {
			deliveryComponents = terminateScenario(terminateServiceList, deliveryComponents, macdResult);
		}

		if (!moveServiceList.isEmpty()) {
			deliveryComponents = moveScenario(moveServiceList, deliveryComponents, macdResult, createdParentDeliveryOrderList);
			macdResult.replacedServiceList = getReplacedServiceList(moveServiceList);
		}

		if (!changeServiceList.isEmpty()) {
			changeScenario(changeServiceList, metadataInformation, deliveryComponents, macdResult);
		}

		COM_MACDServiceFieldMapping msfm = new COM_MACDServiceFieldMapping();
		List<csord__Service__c> updatedServiceList = COM_Helper.getServiceList_CustomQuery(serviceList, msfm.commaSeparatedFieldsWithStartingComma);
		msfm.mapFieldsFromReplacedToNewServices(updatedServiceList);

		macdResult.servicesList = updatedServiceList;
		macdResult.deliveryComponents = deliveryComponents;

		return macdResult;
	}

	public List<COM_MetadataInformation.DeliveryComponent> terminateScenario(
		List<csord__Service__c> serviceList,
		List<COM_MetadataInformation.DeliveryComponent> deliveryComponents,
		COM_MACDResult macdResult
	) {
		List<csord__Service__c> replacedServiceList = getReplacedServiceList(serviceList);
		Map<Id, csord__Service__c> deliveryOrderIdServiceId = new Map<Id, csord__Service__c>();

		for (csord__Service__c service : serviceList) {
			for (COM_MetadataInformation.DeliveryComponent delCom : deliveryComponents) {
				if (delCom.serviceId == service.Id) {
					delCom.macdAction = COM_Constants.MACD_ACTION_COMPONENT_DELETE;
				}
			}

			for (csord__Service__c replacedService : replacedServiceList) {
				if (deliveryOrderIdServiceId.get(service.COM_Delivery_Order__c) == null) {
					deliveryOrderIdServiceId.put(service.COM_Delivery_Order__c, service);
				}
			}
		}

		if (deliveryOrderIdServiceId.size() > 0) {
			updateTerminateDeliveryOrder(deliveryOrderIdServiceId);
		}

		return deliveryComponents;
	}

	public void updateTerminateDeliveryOrder(Map<Id, csord__Service__c> deliveryOrderIdServiceId) {
		Map<Id, COM_Delivery_Order__c> mapOfDeliveryOrders = new Map<Id, COM_Delivery_Order__c>(
			[SELECT Id, Name, Parent_Delivery_Order__c, Termination_Date__c FROM COM_Delivery_Order__c WHERE Id IN :deliveryOrderIdServiceId.keySet()]
		);

		for (Id deliveryOrderId : deliveryOrderIdServiceId.keySet()) {
			if (deliveryOrderIdServiceId.get(deliveryOrderId).csord__Subscription__r.Termination_Wish_Date__c != null) {
				mapOfDeliveryOrders.get(deliveryOrderId).Termination_Date__c = deliveryOrderIdServiceId.get(deliveryOrderId)
					.csord__Subscription__r.Termination_Wish_Date__c.date();
			}
		}

		update mapOfDeliveryOrders.values();
	}

	public List<COM_MetadataInformation.DeliveryComponent> moveScenario(
		List<csord__Service__c> serviceList,
		List<COM_MetadataInformation.DeliveryComponent> deliveryComponents,
		COM_MACDResult macdResult,
		List<COM_Delivery_Order__c> createdParentDeliveryOrderList
	) {
		List<csord__Service__c> replacedServiceList = getReplacedServiceList(serviceList);
		List<csord__Service__c> serviceToDeleteList = new List<csord__Service__c>();
		List<csord__Service__c> serviceToUpdateList = new List<csord__Service__c>();
		List<csord__Subscription__c> subscriptionToUpdateList = new List<csord__Subscription__c>();
		List<Id> replacedServiceIdList = new List<Id>();
		Set<Id> moveOrderNewSiteList = new Set<Id>();

		for (csord__Service__c service : serviceList) {
			if (service.csordtelcoa__Cancelled_By_Change_Process__c && service.Site__c != service.csordtelcoa__Replaced_Service__r.Site__c) {
				serviceToDeleteList.add(service);
			} else {
				service.csordtelcoa__Replaced_Service__c = null;
				service.csordtelcoa__Delta_Status__c = null;
				service.csord__Subscription__r.csordtelcoa__Replaced_Subscription__c = null;
				serviceToUpdateList.add(service);
				// services related to the new site (implementation delivery order for Site B) - replaced_service field should be set to null, and set the Delta_Status field to null;
				// parent subscription replaced_subscription field should be set to null
			}

			moveOrderNewSiteList.add(service.COM_Delivery_Order__c);
		}

		for (csord__Service__c replacedService : replacedServiceList) {
			if (replacedService.csord__Service__c == null) {
				for (COM_MetadataInformation.DeliveryComponent delCom : deliveryComponents) {
					if (delCom.serviceId == replacedService.Id) {
						delCom.macdAction = COM_Constants.MACD_ACTION_COMPONENT_DELETE;
					}
				}
			}
			replacedService.csordtelcoa__Replacement_Service__c = null;
			replacedService.csordtelcoa__Delta_Status__c = COM_Constants.SERVICES_DELTA_STATUS_DELETE;
			replacedService.csord__Subscription__r.csordtelcoa__Replacement_Subscription__c = null;
			subscriptionToUpdateList.add(replacedService.csord__Subscription__r);
			serviceToUpdateList.add(replacedService);
			// services related to the old site (termination delivery order for Site A) - replacement_service field should be set to null, and set the Delta_Status field to Deleted from Subscription;
			// parent subscription replacement_subscription field should be set to null
		}

		List<COM_Delivery_Order__c> moveOrderNewSiteListToUpdate = [
			SELECT id, MACD_move__c
			FROM COM_Delivery_Order__c
			WHERE id IN :moveOrderNewSiteList
		];

		for (COM_Delivery_Order__c deliveryOrder : moveOrderNewSiteListToUpdate) {
			deliveryOrder.MACD_move__c = true;
		}

		update moveOrderNewSiteListToUpdate;

		COM_OrderGenerationObserverHandler comOrderGenerationObserverHandler = new COM_OrderGenerationObserverHandler();
		List<COM_Delivery_Order__c> macdCreatedDeliveryOrderList = comOrderGenerationObserverHandler.createDeliveryOrders(
			COM_Helper.getServiceList(replacedServiceList),
			createdParentDeliveryOrderList,
			null
		);
		comOrderGenerationObserverHandler.linkServiceToDeliveryOrder(COM_Helper.getServiceList(replacedServiceList), macdCreatedDeliveryOrderList);

		for (COM_Delivery_Order__c deliveryOrder : macdCreatedDeliveryOrderList) {
			deliveryOrder.MACD_Type__c = COM_Constants.MACD_TYPE_TERMINATION;
			deliveryOrder.MACD_Actions__c = COM_Constants.MACD_ACTION_COMPONENT_DELETE;
			if (moveOrderNewSiteListToUpdate.size() > 0) {
				deliveryOrder.Moved_from__c = moveOrderNewSiteListToUpdate[0].Id;
			}
			deliveryOrder.Termination_Date__c = replacedServiceList[0].Termination_Wish_Date__c.date();
			// add termination wish date
		}
		update subscriptionToUpdateList;
		update serviceToUpdateList;
		update macdCreatedDeliveryOrderList;
		delete serviceToDeleteList;

		return deliveryComponents;
	}

	public Map<Id, List<COM_MetadataInformation.DeliveryComponent>> getServiceDeliveryComponentMap(
		List<csord__Service__c> serviceList,
		List<COM_MetadataInformation.DeliveryComponent> deliveryComponents
	) {
		Map<Id, List<COM_MetadataInformation.DeliveryComponent>> serviceDeliveryComponentMap = new Map<Id, List<COM_MetadataInformation.DeliveryComponent>>();

		for (csord__Service__c service : serviceList) {
			for (COM_MetadataInformation.DeliveryComponent delCom : deliveryComponents) {
				if (delCom.serviceId == service.Id) {
					if (serviceDeliveryComponentMap.get(service.Id) == null) {
						List<COM_MetadataInformation.DeliveryComponent> newComponentList = new List<COM_MetadataInformation.DeliveryComponent>();
						newComponentList.add(delCom);
						serviceDeliveryComponentMap.put(service.Id, newComponentList);
					} else {
						serviceDeliveryComponentMap.get(service.Id).add(delCom);
					}
				}
			}
		}

		return serviceDeliveryComponentMap;
	}
	/*
	public Map<Id, List<COM_MetadataInformation.DeliveryComponent>> getParentServiceDeliveryComponentMap(List<csord__Service__c> serviceList, List<COM_MetadataInformation.DeliveryComponent> deliveryComponents) {
	    Map<Id, List<COM_MetadataInformation.DeliveryComponent>> parentServiceDeliveryComponentMap = new Map<Id, List<COM_MetadataInformation.DeliveryComponent>>();
	    
	    for(csord__Service__c parentService : getParentServiceList(serviceList)) {
	        for(csord__Service__c childService : serviceList) {
	            if(childService.csord__Service__c == parentService.Id) {
	                for (COM_MetadataInformation.DeliveryComponent delCom : deliveryComponents) {
        	            if (delCom.serviceId == childService.Id) {
        	                if(parentServiceDeliveryComponentMap.get(parentService.Id) == null) {
        	                    List<COM_MetadataInformation.DeliveryComponent> newComponentList = new List<COM_MetadataInformation.DeliveryComponent>();
        	                    newComponentList.add(delCom);
        	                    parentServiceDeliveryComponentMap.put(parentService.Id, newComponentList);
        	                } else {
        	                    parentServiceDeliveryComponentMap.get(parentService.Id).add(delCom);
        	                }
        	            }
        	        }
	            }
	        }
	    }
	    
	    return parentServiceDeliveryComponentMap;
	}*/

	public List<COM_MetadataInformation.DeliveryComponent> changeScenario(
		List<csord__Service__c> serviceList,
		COM_MetadataInformation metadataInformation,
		List<COM_MetadataInformation.DeliveryComponent> deliveryComponents,
		COM_MACDResult macdResult
	) {
		List<csord__Service__c> replacedServiceList = getReplacedServiceList(serviceList);

		COM_MetadataInformation replacedMetadataInformation = new COM_MetadataInformation();
		COM_DeliveryComponentsGenerator generator = new COM_DeliveryComponentsGenerator();

		List<Id> replacedServiceIdList = new List<Id>();
		List<COM_MetadataInformation.DeliveryComponent> deliveryComponentsToDelete = new List<COM_MetadataInformation.DeliveryComponent>();

		for (csord__Service__c replacedServiceRecord : replacedServiceList) {
			replacedServiceIdList.add(replacedServiceRecord.Id);
		}

		replacedMetadataInformation = generator.prepareDataToGenerateComponents(replacedServiceIdList);
		List<COM_MetadataInformation.DeliveryComponent> replacedDeliveryComponents = generator.generateDeliveryComponents(
			replacedMetadataInformation
		);

		Map<Id, List<COM_MetadataInformation.DeliveryComponent>> serviceIdDeliveryComponentMap = getServiceDeliveryComponentMap(
			serviceList,
			deliveryComponents
		);
		Map<Id, List<COM_MetadataInformation.DeliveryComponent>> replacedServiceIdDeliveryComponentMap = getServiceDeliveryComponentMap(
			replacedServiceList,
			replacedDeliveryComponents
		);

		for (csord__Service__c service : serviceList) {
			for (COM_MetadataInformation.DeliveryComponent delCom : deliveryComponents) {
				if (delCom.serviceId == service.Id) {
					for (csord__Service__c replacedService : replacedServiceList) {
						for (COM_MetadataInformation.DeliveryComponent repDelCom : replacedDeliveryComponents) {
							if (repDelCom.serviceId == replacedService.Id) {
								if (delCom.Name == repDelCom.Name && delCom.articleCode == repDelCom.articleCode) {
									if (service.csordtelcoa__Cancelled_By_Change_Process__c == false) {
										delCom.macdAction = COM_Constants.MACD_ACTION_COMPONENT_NO_CHANGE;
									} else {
										delCom.macdAction = COM_Constants.MACD_ACTION_COMPONENT_DELETE;
									}
								} else if (delCom.Name == repDelCom.Name && delCom.articleCode != repDelCom.articleCode) {
									delCom.macdAction = COM_Constants.MACD_ACTION_COMPONENT_CHANGE;

									if (metadataInformation.getDeliveryComponentsRequiringInstallation().contains(delCom.Name)) {
										delCom.installationRequired = true;
									}
								}
							}

							if (!serviceIdDeliveryComponentMap.get(service.Id).contains(repDelCom)) {
								deliveryComponentsToDelete.add(repDelCom);
							}
						}
					}
				}
			}
		}

		List<COM_MetadataInformation.DeliveryComponent> finalComponentList = new List<COM_MetadataInformation.DeliveryComponent>();

		for (COM_MetadataInformation.DeliveryComponent delCom : deliveryComponents) {
			Boolean deleteComponent = false;
			for (COM_MetadataInformation.DeliveryComponent delComToDelete : deliveryComponentsToDelete) {
				if (delCom.Name == delComToDelete.Name && delCom.articleCode == delComToDelete.articleCode) {
					deleteComponent = true;
				}
			}
			if (deleteComponent == false) {
				finalComponentList.add(delCom);
			}
		}

		return finalComponentList;
	}

	public static Boolean isTerminateScenario(csord__Service__c parentService) {
		if (
			parentService.csordtelcoa__Replaced_Service__c != null &&
			parentService.csordtelcoa__Cancelled_By_Change_Process__c == true &&
			parentService.Site__c == parentService.csordtelcoa__Replaced_Service__r.Site__c
		) {
			return true;
		}

		return false;
	}

	private Boolean isMoveScenario(csord__Service__c parentService) {
		if (
			parentService.csordtelcoa__Replaced_Service__c != null &&
			parentService.Site__c != parentService.csordtelcoa__Replaced_Service__r.Site__c
		) {
			return true;
		}

		return false;
	}

	private Boolean isChangeScenario(csord__Service__c parentService) {
		if (
			parentService.csordtelcoa__Replaced_Service__c != null &&
			parentService.csordtelcoa__Cancelled_By_Change_Process__c == false &&
			parentService.Site__c == parentService.csordtelcoa__Replaced_Service__r.Site__c
		) {
			return true;
		}

		return false;
	}
	/*
	private Set<csord__Service__c> getParentServiceList(List<csord__Service__c> serviceList) {
		Set<csord__Service__c> parentServiceList = new Set<csord__Service__c>();

		for (csord__Service__c serviceIterate : serviceList) {
			if (serviceIterate.csord__Service__c == null) {
				parentServiceList.add(serviceIterate);
			}
		}

		return parentServiceList;
	}*/

	private List<csord__Service__c> getReplacedServiceList(List<csord__Service__c> serviceList) {
		Set<Id> replcedServiceIds = new Set<Id>();

		for (csord__Service__c service : serviceList) {
			replcedServiceIds.add(service.csordtelcoa__Replaced_Service__c);
		}

		List<csord__Service__c> replacedServiceList = [
			SELECT
				Id,
				Name,
				csord__Service__c,
				Site__c,
				csordtelcoa__Cancelled_By_Change_Process__c,
				Installation_Wishdate__c,
				Termination_Wish_Date__c,
				CDO_Umbrella_Service_Id__c,
				COM_Delivery_Order__c,
				csord__Subscription__r.Termination_Wish_Date__c,
				Implemented_Date__c,
				csordtelcoa__Delta_Status__c,
				csord__Service__r.csordtelcoa__Delta_Status__c,
				csord__Service__r.COM_Delivery_Order__c,
				csordtelcoa__Replaced_Service__c
			FROM csord__Service__c
			WHERE Id IN :replcedServiceIds
		];

		return replacedServiceList;
	}
}
