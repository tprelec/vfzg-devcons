@isTest
public with sharing class TestFlowWaitingStepController {
	@TestSetup
	public static void makeData() {
		List<Profile> pList = [SELECT Id, Name FROM Profile WHERE Name = 'System Administrator' LIMIT 1];
		List<UserRole> roleList = [SELECT Id, Name, DeveloperName FROM UserRole u WHERE ParentRoleId = NULL];
		User simpleUser = CS_DataTest.createUser(pList, roleList);
		insert simpleUser;

		System.runAs(simpleUser) {
			TestUtils.createLead();

			Account testAccount = CS_DataTest.createAccount('Test Account');
			insert testAccount;

			Opportunity testOpp = CS_DataTest.createOpportunity(testAccount, 'Test Opp', simpleUser.id);
			insert testOpp;

			cscfga__Product_Basket__c basket = CS_DataTest.createProductBasket(testOpp, 'Test Basket');
			insert basket;

			VF_Contract__c testVFContract = new VF_Contract__c();
			testVFContract.Opportunity__c = testOpp.Id;
			testVFContract.Account__c = testAccount.Id;
			insert testVFContract;

			Ordertype__c testOrderType = new Ordertype__c(
				Name = 'Fixed/OneNet',
				ExportSystem__c = 'SIAS',
				Status__c = 'New',
				ExternalId__c = 'OT-04-1234561'
			);
			insert testOrderType;

			Product2 product = new Product2(
				Name = 'Test product',
				Brand__c = 'Testing Brand',
				Cost_Price__c = 10,
				Description = 'This is a test product',
				IsActive = true,
				ProductCode = 'C999999',
				Product_Group__c = 'Priceplan',
				Quantity_type__c = 'Months',
				SDC_Product_matrix__c = 'Fixed - Data',
				CLC__c = 'Acq',
				Product_Line__c = 'fVodafone',
				Ordertype__c = testOrderType.id,
				ExternalID__c = 'P2-01-2923773'
			);
			insert product;

			Contracted_Products__c cp = new Contracted_Products__c(
				Quantity__c = 1,
				UnitPrice__c = 10,
				Arpu_Value__c = 10,
				Duration__c = 1,
				Product__c = product.Id,
				VF_Contract__c = testVFContract.Id
				//Site__c = site.Id
			);
			insert cp;
		}
	}
	@isTest
	public static void testGetRecord() {
		Test.startTest();
		SObject resp = FlowWaitingStepController.getRecord([SELECT Id FROM Lead LIMIT 1][0].Id, 'Name');
		System.assertEquals('Test Lead', resp.get('Name'), 'Test Lead existing');
		Test.stopTest();
	}

	@isTest
	public static void testGetContractedProducts_positive() {
		Opportunity testOpp = [SELECT Id FROM Opportunity LIMIT 1];
		Test.startTest();
		System.assertEquals(true, FlowWaitingStepController.getContractedProducts(testOpp.Id), 'Contracted Products should exist for this opp');
		Test.stopTest();
	}

	@isTest
	public static void testGetContractedProducts_negative() {
		Test.startTest();
		System.assertEquals(false, FlowWaitingStepController.getContractedProducts(null), 'No Contracted Products should exist for this opp');
		Test.stopTest();
	}
}
