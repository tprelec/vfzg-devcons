({
    startInit: function(component, event, helper) {
        helper.fetchOpportunity(component, event, helper);
        helper.fetchListOfRecordTypes(component, event, helper);
    },

    createRecord: function(component, event, helper, sObjectRecord) {
        var selectedRecordTypeId = component.get("v.value");
        console.log('##selectedRecordTypeId: '+selectedRecordTypeId);
        //var selectedRecordTypeName = component.find("recordTypePickList").get("v.value");
        if(selectedRecordTypeId != "") {
            helper.closeModal();
            helper.showCreateRecordModal(component, selectedRecordTypeId, "Case");
        } else{
            alert('You did not select any record type');
        }
    },

    closeModal : function(component, event, helper){
        helper.closeModal();
    }
})