({
    refreshView: function(component, event, helper) {
        // refresh the view
        $A.get("e.force:refreshView").fire();
    }
});