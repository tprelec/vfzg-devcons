({
	// Handle address search using Olbico Service
	addressSearch: function (component, event, helper) {
		component.set('v.loading', true);
		var zipCode = component.get('v.postalCode');
		var action = component.get('c.oblicoSearch');

		action.setParams({ zip: zipCode });
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				var result = response.getReturnValue();

				var returnedAddresses = [];
				for (let propName in result) {
					returnedAddresses.push({
						value: result[propName],
						label: propName
					});
				}
				if (returnedAddresses.length != 0) {
					component.set('v.addressCheckResult', returnedAddresses);
					component.set('v.dropdownDisabled', false);
				} else {
					component.set('v.loading', false);
					component.set('v.dropdownDisabled', true);

					this.handleToastEvent('Warning!', 'Please enter a valid Postal Code and House Number.', 'dismissable');
				}

				component.set('v.loading', false);
			} else if (state == 'ERROR') {
				var error = response.getError();
				this.handleToastEvent('Warning!', error[0].message, 'dismissable');
			}
		});
		$A.enqueueAction(action);
	},
	// Setting Account Name Label
	setTitle: function (component, event, helper) {
		var accountName = component.get('v.accountName');
		var title = 'Add new site for ' + accountName;
		component.set('v.title', title);
	},
	// Helper on close modal
	helperCloseModal: function (component, event, helper) {
		var compEvent = component.getEvent('closeModalEvent');
		compEvent.setParams({ sourceComponent: 'addSite' });
		compEvent.fire();
	},
	// Handle saving of new site for an Account, based on what button was clicked Save or Save and New Modal closes or stays open
	handleSaveSite: function (component, event, helper) {
		component.set('v.loading', true);
		var selectedAddress = component.get('v.selectedAddress');
		var accountId = component.get('v.accountId');
		var saveAndNew = event.getSource().getLocalId();
		var action = component.get('c.saveSite');
		action.setParams({ selectedAddress: selectedAddress, accId: accountId });
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				this.handleToastEvent('Success!', 'New site added successfully.', 'dismissable');
				component.set('v.loading', false);
				var compEvent = component.getEvent('addSiteHandleSaveEvent');

				saveAndNew == 'saveAndNew' ? compEvent.setParams({ saveAndNew: true }) : compEvent.setParams({ saveAndNew: false });

				compEvent.fire();
			} else if (state == 'ERROR') {
				var error = response.getError();
				component.set('v.loading', false);

				this.handleToastEvent('Warning!', error[0].message, 'dismissable');
			}
		});
		$A.enqueueAction(action);
	},
	// Handler for selecting sites
	handleSelectedSite: function (component, event, helper) {
		component.set('v.selectedAddress', event.getParam('value'));
	},
	handleToastEvent: function (title, message, mode) {
		var toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
			title: title,
			message: message,
			mode: mode
		});
		toastEvent.fire();
	}
});
