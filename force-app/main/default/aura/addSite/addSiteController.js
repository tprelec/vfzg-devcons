({
	callAddressCheck: function (component, event, helper) {
		helper.addressSearch(component, event, helper);
	},
	doInit: function (component, event, helper) {
		helper.setTitle(component, event, helper);
	},
	closeModal: function (component, event, helper) {
		helper.helperCloseModal(component, event, helper);
	},
	handleSave: function (component, event, helper) {
		helper.handleSaveSite(component, event, helper);
	},
	handleSelected: function (component, event, helper) {
		helper.handleSelectedSite(component, event, helper);
	}
});
