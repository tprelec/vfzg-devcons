({
	setupDataTable: function (component) {
		component.set('v.columns', [
			{
				initialWidth: 150,
				label: 'Location Type',
				fieldName: 'Location_Type__c',
				type: 'text',
				sortable: true
			},
			{
				label: 'Site Name',
				fieldName: 'RedirectUrl',
				initialWidth: 280,
				type: 'url',
				typeAttributes: { label: { fieldName: 'Name' }, name: 'siteName', variant: 'base' }
			},
			{
				initialWidth: 140,
				label: 'Postal Code',
				fieldName: 'Site_Postal_Code__c',
				type: 'text',
				sortable: true
			},
			{
				label: 'House Number',
				initialWidth: 150,
				fieldName: 'Site_House_Number__c',
				type: 'text',
				sortable: true
			},
			{
				initialWidth: 150,
				label: 'Last dsl check',
				fieldName: 'Last_dsl_check__c',
				cellAttributes: { iconName: { fieldName: 'dslFlagIconName' }, iconPosition: 'left' },
				type: 'date',
				sortable: true
			},
			{
				initialWidth: 150,
				label: 'Last fiber check',
				fieldName: 'Last_fiber_check__c',
				cellAttributes: { iconName: { fieldName: 'fiberFlagIconName' }, iconPosition: 'left' },
				type: 'date',
				sortable: true
			},
			{
				initialWidth: 100,
				label: 'Active',
				fieldName: 'Active__c',
				type: 'checkbox'
			},
			{
				label: 'PBX',
				type: 'button',
				initialWidth: 95,
				fieldName: 'Pbx',
				typeAttributes: {
					label: { fieldName: 'Pbx' },
					name: 'addPbx',
					variant: 'base',
					iconName: { fieldName: 'pbxIconName' },
					iconPosition: 'right'
				}
			},
			{
				label: 'Infra',
				type: 'button',
				initialWidth: 95,
				fieldName: 'Infra',
				typeAttributes: {
					label: { fieldName: 'Infra' },
					name: 'infra',
					variant: 'base',
					iconName: { fieldName: 'infraIconName' },
					iconPosition: 'right'
				}
			}
		]);
	},
	// Fetching site Pbx records (Competitor_Asset__c)
	getSitesPbx: function (component) {
		var action = component.get('c.getSitePbx');
		var accId = component.get('v.selectedAccountId');
		action.setParams({ accountId: accId });
		action.setCallback(this, (response) => {
			const state = response.getState();

			if (state == 'SUCCESS') {
				var result = response.getReturnValue();

				component.set('v.allPbx', result);
			} else if (state == 'ERROR') {
				var errors = response.getError();
				var toastEvent = $A.get('e.force:showToast');
				toastEvent.setParams({
					title: 'Warning!',
					message: errors[0].message,
					mode: 'dismissible'
				});
				toastEvent.fire();
			}
		});
		$A.enqueueAction(action);
	},

	// Checks if current User is community user, based on the return the redirect URL is set for Click on site in datatable
	isCommunity: function (component) {
		var action = component.get('c.isCommunity');
		action.setCallback(this, (response) => {
			const state = response.getState();
			if (state === 'SUCCESS') {
				var result = response.getReturnValue();
				component.set('v.isCommunityUser', result);
			} else if (state === 'ERROR') {
				var errors = response.getError();
				var toastEvent = $A.get('e.force:showToast');
				toastEvent.setParams({
					title: 'Warning!',
					message: errors[0].message,
					mode: 'dismissible'
				});
				toastEvent.fire();
			}
		});
		$A.enqueueAction(action);
	},

	checkCompletedOrFiber: function (site) {
		return site.Postal_Code_Check_Status__c
			? site.Postal_Code_Check_Status__c == 'Completed' || site.Postal_Code_Check_Status__c.includes('Fiber')
			: false;
	},

	checkDlsOrCombined: function (site) {
		return site.Postal_Code_Check_Status__c
			? site.Postal_Code_Check_Status__c.includes('DSL') || site.Postal_Code_Check_Status__c.includes('Combined')
			: false;
	},
	checkCompletedOrDsl: function (site) {
		return site.Postal_Code_Check_Status__c
			? site.Postal_Code_Check_Status__c == 'Completed' || site.Postal_Code_Check_Status__c.includes('DSL')
			: false;
	},
	checkFiberOrCombined: function (site) {
		return site.Postal_Code_Check_Status__c
			? site.Postal_Code_Check_Status__c.includes('Fiber') || site.Postal_Code_Check_Status__c.includes('Combined')
			: false;
	},
	// Fetch Account Sites  (datatable data)
	// Also sets some new properties to Site__c object used in datatable
	getData: function (component, event, helper) {
		return this.callAction(component)
			.then(
				$A.getCallback((data) => {
					var ppUrl = $A.get('$Label.c.pp_Community_Base_URL');
					var isCommunity = component.get('v.isCommunityUser');
					var allPbx = component.get('v.allPbx');
					var sitePbx = [];

					// Setup cell icons for Last DSL and Fiber checks, Pbx and Infra icons
					data.forEach(function (record) {
						if (record.isDSLCheckCurrent__c && helper.checkCompletedOrFiber(record)) {
							record.dslFlagIconName = 'action:approval';
						}

						if (!record.isDSLCheckCurrent__c && helper.checkCompletedOrFiber(record)) {
							record.Last_dsl_check__c = '';
							record.dslFlagIconName = '';
						}

						if (helper.checkDlsOrCombined(record)) {
							record.dslFlagIconName = 'action:defer';
						}

						if (record.isFiberCheckCurrent__c && helper.checkCompletedOrDsl(record)) {
							record.fiberFlagIconName = 'action:approval';
						}

						if (!record.isFiberCheckCurrent__c && helper.checkCompletedOrDsl(record)) {
							record.Last_fiber_check__c = '';
							record.fiberFlagIconName = '';
						}

						if (helper.checkFiberOrCombined(record)) {
							record.fiberFlagIconName = 'action:defer';
						}

						if (record.Site_Postal_Checks__r && record.Site_Postal_Checks__r.length > 0) {
							record.infraIconName = 'action:approval';
						}

						sitePbx = allPbx.filter((pbx) => pbx.Site__c == record.Id);

						if (sitePbx.length > 0) {
							record.pbxIconName = 'action:approval';
							sitePbx.forEach((pbx) => {
								if (pbx.PBX_type__r.Office_Voice_approved__c != null) {
									if (
										pbx.PBX_type__r.Office_Voice_approved__c.includes('G.711') ||
										pbx.PBX_type__r.Office_Voice_approved__c.includes('G.729')
									) {
										record.pbxIconName = 'action:approval';
									}
								}
							});
						}
						record.RedirectUrl = isCommunity ? ppUrl + '/s/detail/' + record.Id : '/' + record.Id;
						record.Pbx = 'Add PBX';
						record.Infra = 'Infra';
					});

					// data = data.map((value,index)=>({...value,index});
					component.set('v.allData', data);
					component.set('v.tableData', data);
					component.set('v.filteredData', data);
					helper.listViewChange(component, event, helper);
					this.preparePagination(component, data);
				})
			)
			.catch(
				$A.getCallback((errors) => {
					if (errors && errors.length > 0) {
						$A.get('e.force:showToast')
							.setParams({
								message: errors[0].message != null ? errors[0].message : errors[0],
								type: 'error'
							})
							.fire();
					}
				})
			);
	},

	callAction: function (component) {
		component.set('v.isLoading', true);
		return new Promise(
			$A.getCallback((resolve, reject) => {
				const action = component.get('c.getData');
				action.setParams({
					accountId: component.get('v.selectedAccountId')
				});

				action.setCallback(this, (response) => {
					const state = response.getState();
					if (state === 'SUCCESS') {
						component.set('v.isLoading', false);
						return resolve(response.getReturnValue());
					} else if (state === 'ERROR') {
						component.set('v.isLoading', false);
						return reject(response.getError());
					}
					return null;
				});
				$A.enqueueAction(action);
			})
		);
	},
	//Prepares records based on selected list view
	listViewChange: function (component, event, helper) {
		let selectedView = component.get('v.selectedView');
		let allData = component.get('v.allData');
		let filteredData = allData;

		if (selectedView === 'Active') {
			filteredData = allData.filter((record) => record.Active__c);
		} else if (selectedView === 'Inactive') {
			filteredData = allData.filter((record) => !record.Active__c);
		}

		component.set('v.filteredData', filteredData);
		helper.preparePagination(component, filteredData);
	},
	// Methods used for pagination when Datatable changes (eg. when the number of records shown is changed)
	preparePagination: function (component, records) {
		let countTotalPage = Math.ceil(records.length / component.get('v.pageSize'));
		let totalPage = countTotalPage > 0 ? countTotalPage : 1;
		component.set('v.totalPages', totalPage);
		component.set('v.currentPageNumber', 1);
		this.setPageDataAsPerPagination(component);
	},

	setPageDataAsPerPagination: function (component) {
		let data = [];
		let pageNumber = component.get('v.currentPageNumber');
		let pageSize = component.get('v.pageSize');
		let filteredData = component.get('v.filteredData');
		let x = (pageNumber - 1) * pageSize;
		for (; x < pageNumber * pageSize; x++) {
			if (filteredData[x]) {
				data.push(filteredData[x]);
			}
		}
		let currentlySelectedRows = [];
		let allSelectedRows = component.get('v.allSelectedRows');

		data.forEach((record) => {
			if (allSelectedRows.includes(record.Id)) {
				currentlySelectedRows.push(record.Id);
			}
		});
		component.set('v.tableData', data);
		component.set('v.currentSelectedRows', currentlySelectedRows);
	},

	searchRecordsBySearchPhrase: function (component) {
		let searchPhrase = component.get('v.searchPhrase');
		if (!$A.util.isEmpty(searchPhrase)) {
			let allData = component.get('v.allData');
			let filteredData = allData.filter((record) => record.Name.includes(searchPhrase));
			component.set('v.filteredData', filteredData);
			this.preparePagination(component, filteredData);
		}
	},

	sortBy: function (field, reverse, primer) {
		let key = primer
			? function (x) {
					return primer(x[field]);
			  }
			: function (x) {
					return x[field];
			  };

		return function (a, b) {
			a = key(a);
			b = key(b);
			return reverse * ((a > b) - (b > a));
		};
	},

	handleSort: function (component, event) {
		let sortedBy = event.getParam('fieldName');
		let sortDirection = event.getParam('sortDirection');

		let cloneData = component.get('v.tableData').slice(0);
		cloneData.sort(this.sortBy(sortedBy, sortDirection === 'asc' ? 1 : -1));

		component.set('v.tableData', cloneData);
		component.set('v.sortDirection', sortDirection);
		component.set('v.sortedBy', sortedBy);
	},

	// Fetch account name used for display on Add Site modal
	getAccountName: function (component) {
		var action = component.get('c.getAccount');
		var accountId = component.get('v.selectedAccountId');

		action.setParams({
			accountId: accountId
		});

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				var result = response.getReturnValue();
				component.set('v.selectedAccountName', result.Name);
			} else if (state == 'ERROR') {
				var errors = response.getError();
				this.handleToastEvent('Warning!', errors[0].message, 'dismissable');
			}
		});
		$A.enqueueAction(action);
	},
	// Helper method for closing modals, based on the sourceComponent parameter
	helperCloseModal: function (component, event, helper) {
		var sourceComponent = event.getParam('sourceComponent');
		switch (sourceComponent) {
			case 'addSite':
				helper.getData(component, event, helper);
				component.set('v.showAddSite', false);
				break;
			case 'manageExistingInfras':
				helper.getData(component, event, helper);

				component.set('v.showManageExistingInfras', false);
				break;
			case 'bulkImport':
				helper.getData(component, event, helper);

				component.set('v.showBulkImport', false);
				break;
			case 'addPbx':
				helper.getSitesPbx(component);
				helper.getData(component, event, helper);

				component.set('v.showAddPBX', false);
			default:
				break;
		}
	},

	// Onclick method for activating sites
	activateSites: function (component, event, helper) {
		component.set('v.isLoading', true);

		this.getSelectedSites(component, event, helper);
		var selectedSites = component.get('v.selectedSites');

		var action = component.get('c.activateSites');
		action.setParams({
			sites: selectedSites
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				helper.getData(component, event, helper);
				var toastEvent = $A.get('e.force:showToast');
				toastEvent.setParams({
					title: 'Success!',
					message: 'Sites successfully activated.',
					mode: 'dismissible'
				});
				toastEvent.fire();
			} else if (state == 'ERROR') {
				var errors = response.getError();
				var toastEvent = $A.get('e.force:showToast');
				toastEvent.setParams({
					title: 'Warning!',
					message: errors[0].message,
					mode: 'dismissible'
				});
				toastEvent.fire();
			}
			component.set('v.isLoading', false);
		});
		$A.enqueueAction(action);
	},
	// Onclick method for deactivating sites
	deactivateSites: function (component, event, helper) {
		component.set('v.isLoading', true);
		this.getSelectedSites(component, event, helper);
		var selectedSites = component.get('v.selectedSites');

		var action = component.get('c.deactivateSites');
		action.setParams({
			sites: selectedSites
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				helper.getData(component, event, helper);

				var toastEvent = $A.get('e.force:showToast');
				toastEvent.setParams({
					title: 'Success!',
					message: 'Sites successfully deactivated.',
					mode: 'dismissible'
				});
				toastEvent.fire();
			} else if (state == 'ERROR') {
				var errors = response.getError();
				var toastEvent = $A.get('e.force:showToast');
				toastEvent.setParams({
					title: 'Warning!',
					message: errors[0].message,
					mode: 'dismissible'
				});
				toastEvent.fire();
			}
			component.set('v.isLoading', false);
		});
		$A.enqueueAction(action);
	},
	// Onclick method for Manage Existing Infras button, Fetches Existing infras for selected Site, If no infra exists for a site, an infra is added
	manageExistingInfras: function (component, event, helper) {
		component.set('v.isLoading', true);
		this.getSelectedSites(component, event, helper);
		var selectedSites = component.get('v.selectedSites');
		var action = component.get('c.getExistingInfras');
		action.setParams({
			sites: selectedSites
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				var infras = response.getReturnValue();
				let manualCreatedInfras = [];
				selectedSites.forEach((site) => {
					let filteredInfras = infras.filter((record) => record.Access_Site_ID__c == site.Id);

					if (filteredInfras.length == 0) {
						let infra = {
							Access_Max_Down_Speed__c: 1048576,
							Access_Max_Up_Speed__c: 1048576,
							Access_Site_ID__c: site.Id,
							Access_Site_ID__r: site,
							Access_Vendor__c: 'IPVPN-VF',
							Accessclass__c: 'Fiber',
							Technology_Type__c: 'Fiber'
						};
						manualCreatedInfras.push(infra);
					}
				});
				infras.push(...manualCreatedInfras);
				component.set('v.existingInfras', infras);
				component.set('v.manualCreatedInfras', manualCreatedInfras);
				component.set('v.isLoading', false);
				component.set('v.showManageExistingInfras', true);
				var test = component.get('v.showManageExistingInfras');
			}
			if (state == 'ERROR') {
				var errors = response.getError();
				var toastEvent = $A.get('e.force:showToast');
				toastEvent.setParams({
					title: 'Warning!',
					message: errors[0].message,
					mode: 'dismissable'
				});
				toastEvent.fire();
			}
			component.set('v.isLoading', false);
		});

		$A.enqueueAction(action);
	},

	scheduleDLSChecks: function (component, event, helper) {
		component.set('v.isLoading', true);

		this.getSelectedSites(component, event, helper);
		var selectedSites = component.get('v.selectedSites');
		var data = component.get('v.tableData');
		var action = component.get('c.scheduleDsl');
		action.setParams({ sites: selectedSites });

		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state == 'SUCCESS') {
				var result = response.getReturnValue();
				if (result.length != 0) {
					this.handleToastEvent('Success!', 'DSL Check scheduled successfully.', 'dismissable');
					helper.getData(component, event, helper);
				} else {
					this.handleToastEvent('Warning!', 'DSL Check already requested.', 'dismissable');
				}
			}
			if (state == 'ERROR') {
				component.set('v.isLoading', false);
				var errors = response.getError();
				this.handleToastEvent('Warning!', errors[0].message, 'dismissable');
			}
			component.set('v.isLoading', false);
		});

		$A.enqueueAction(action);
	},
	scheduleFiberChecks: function (component, event, helper) {
		component.set('v.isLoading', true);
		var data = component.get('v.tableData');
		this.getSelectedSites(component, event, helper);
		var selectedSites = component.get('v.selectedSites');

		var action = component.get('c.scheduleFiber');
		action.setParams({ sites: selectedSites });

		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state == 'SUCCESS') {
				var result = response.getReturnValue();
				if (result.length != 0) {
					this.handleToastEvent('Success!', 'Fiber Check scheduled successfully.', 'dismissable');
					helper.getData(component, event, helper);
				} else {
					this.handleToastEvent('Warning!', 'Fiber Check already requested.', 'dismissable');
				}
			}
			if (state == 'ERROR') {
				component.set('v.isLoading', false);
				var errors = response.getError();
				this.handleToastEvent('Warning!', errors[0].message, 'dismissable');
			}
			component.set('v.isLoading', false);
		});

		$A.enqueueAction(action);
	},
	scheduleCombinedChecks: function (component, event, helper) {
		component.set('v.isLoading', true);
		var data = component.get('v.tableData');
		this.getSelectedSites(component, event, helper);
		var selectedSites = component.get('v.selectedSites');

		var action = component.get('c.scheduleBoth');
		action.setParams({ sites: selectedSites });

		action.setCallback(this, function (response) {
			var state = response.getState();

			if (state == 'SUCCESS') {
				var result = response.getReturnValue();
				if (result.length != 0) {
					this.handleToastEvent('Success!', 'DSL and Fiber Check scheduled successfully.', 'dismissable');
					helper.getData(component, event, helper);
				} else {
					this.handleToastEvent('Warning!', 'DSL and Fiber Check already requested.', 'dismissable');
				}
			}
			if (state == 'ERROR') {
				component.set('v.isLoading', false);
				var errors = response.getError();
				this.handleToastEvent('Warning!', errors[0].message, 'dismissable');
			}
			component.set('v.isLoading', false);
		});

		$A.enqueueAction(action);
	},

	bulkImport: function (component, event, helper) {
		component.set('v.showBulkImport', true);
	},

	handleToastEvent: function (title, message, mode) {
		var toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
			title: title,
			message: message,
			mode: mode
		});
		toastEvent.fire();
	},

	rowAction: function (component, event, helper) {
		var row = event.getParam('row');
		var action = event.getParam('action');
		switch (action.name) {
			case 'addPbx':
				component.set('v.siteId', row.Id);
				component.set('v.showAddPBX', true);
				break;
			case 'infra':
				component.set('v.allSelectedRows', row.Id);
				helper.manageExistingInfras(component, event, helper);
				break;
			case 'siteName':
				component.set('v.siteId', row.Id);
				component.set('v.showSiteWindow', true);
				break;
			default:
				break;
		}
	},
	getSelectedSites: function (component, event, helper) {
		var selectedRowsIds = component.get('v.allSelectedRows');
		var data = component.get('v.allData');
		var selectedSites = [];
		data.forEach((record) => {
			if (selectedRowsIds.includes(record.Id)) {
				selectedSites.push(record);
			}
		});
		component.set('v.selectedSites', selectedSites);
	},
	platformEvent: function (component, event, helper) {
		helper.getData(component, event, helper);
		this.handleToastEvent('Success!', 'Site Postal Code Check completed successfully.', 'dismissable');
	}
});
