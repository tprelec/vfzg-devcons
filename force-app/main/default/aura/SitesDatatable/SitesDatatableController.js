({
	doInit: function (component, event, helper) {
		helper.getSitesPbx(component);
		helper.setupDataTable(component);
		helper.isCommunity(component);
		helper.getData(component, event, helper);
		helper.getAccountName(component);
	},

	onNext: function (component, event, helper) {
		let pageNumber = component.get('v.currentPageNumber');
		component.set('v.currentPageNumber', pageNumber + 1);
		helper.setPageDataAsPerPagination(component);
	},

	onPrev: function (component, event, helper) {
		let pageNumber = component.get('v.currentPageNumber');
		component.set('v.currentPageNumber', pageNumber - 1);
		helper.setPageDataAsPerPagination(component);
	},

	onFirst: function (component, event, helper) {
		component.set('v.currentPageNumber', 1);
		helper.setPageDataAsPerPagination(component);
	},

	onLast: function (component, event, helper) {
		component.set('v.currentPageNumber', component.get('v.totalPages'));
		helper.setPageDataAsPerPagination(component);
	},

	onPageSizeChange: function (component, event, helper) {
		helper.preparePagination(component, component.get('v.filteredData'));
	},

	onChangeSearchPhrase: function (component, event, helper) {
		if ($A.util.isEmpty(component.get('v.searchPhrase'))) {
			let allData = component.get('v.allData');
			component.set('v.filteredData', allData);
			helper.preparePagination(component, allData);
		}
	},

	onListViewChange: function (component, event, helper) {
		helper.listViewChange(component, event, helper);
	},

	handleSearch: function (component, event, helper) {
		helper.searchRecordsBySearchPhrase(component);
	},

	handleSort: function (component, event, helper) {
		helper.handleSort(component, event);
	},

	handleRowSelection: function (component, event, helper) {
		// List of selected items from the data table event.
		var updatedItemsSet = new Set();
		// List of selected items we maintain.
		var selectedRows = component.get('v.allSelectedRows');

		var selectedItemsSet = new Set(selectedRows);
		// List of items currently loaded for the current view.
		var loadedItemsSet = new Set();

		var data = component.get('v.tableData');
		data.map((event) => {
			loadedItemsSet.add(event.Id);
		});

		if (event.getParam('selectedRows').length != 0) {
			event.getParam('selectedRows').map((event) => {
				updatedItemsSet.add(event.Id);
			});

			// Add any new items to the selection list
			updatedItemsSet.forEach((id) => {
				if (!selectedItemsSet.has(id)) {
					selectedItemsSet.add(id);
				}
			});
		}

		loadedItemsSet.forEach((id) => {
			if (selectedItemsSet.has(id) && !updatedItemsSet.has(id)) {
				// Remove any items that were unselected.
				selectedItemsSet.delete(id);
			}
		});

		selectedRows = [...selectedItemsSet];
		component.set('v.allSelectedRows', selectedRows);
	},
	handleAddSite: function (component, event, helper) {
		component.set('v.showAddSite', true);
	},
	closeModal: function (component, event, helper) {
		helper.helperCloseModal(component, event, helper);
	},
	addSiteHandleSave: function (component, event, helper) {
		helper.getData(component, event, helper);
		var saveAndNew = event.getParam('saveAndNew');
		if (saveAndNew == false) {
			helper.helperCloseModal(component, event, helper);
		}
	},
	handleActivateSites: function (component, event, helper) {
		helper.activateSites(component, event, helper);
	},
	handleDeactivateSites: function (component, event, helper) {
		helper.deactivateSites(component, event, helper);
	},
	handleManageExistingInfras: function (component, event, helper) {
		helper.manageExistingInfras(component, event, helper);
	},
	handleScheduleDLSChecks: function (component, event, helper) {
		helper.scheduleDLSChecks(component, event, helper);
	},

	handleScheduleFiberChecks: function (component, event, helper) {
		helper.scheduleFiberChecks(component, event, helper);
	},
	handleScheduleCombinedChecks: function (component, event, helper) {
		helper.scheduleCombinedChecks(component, event, helper);
	},
	handleBulkImport: function (component, event, helper) {
		helper.bulkImport(component, event, helper);
	},
	handleRowAction: function (component, event, helper) {
		helper.rowAction(component, event, helper);
	},
	handleBackFromSiteWindow: function (component, event, helper) {
		helper.backFromSiteWindow(component, event, helper);
	},
	handlePlatformEvent: function (component, event, helper) {
		helper.platformEvent(component, event, helper);
	}
});
