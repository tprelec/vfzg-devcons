/**
 * Created by edelrabe on 27/02/2018.
 */
({
    onInit: function (component, action, helper) {
        var oppty = component.get("v.oppty");
        var currentStep = component.get("v.currentStep");

        if (oppty.IsClosed == true) {
            currentStep = 'order';
        }
        else if (oppty.Primary_Quote__c != null) {
            currentStep = 'oppty';
        }
        else if (oppty.Primary_Quote__c != null) {
            currentStep = 'oppty';
        }
        //console.log(oppty.Name);

        var oppStatus = '';
        if (oppty.StageName == 'Closed Lost') {
            oppStatus = 'slds-is-lost';
        }
        else {
            oppStatus = 'slds-is-current';
        }

        component.set('v.oppStatus', oppStatus);

    },

    toggleDetails: function (component, action, helper) {

        var expandable = component.find('path-content-3').getElement();
        $A.util.toggleClass(expandable, "slds-hide");
        $A.util.toggleClass(expandable, "slds-show");
    }
})