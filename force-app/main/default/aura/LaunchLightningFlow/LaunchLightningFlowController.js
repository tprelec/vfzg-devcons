({
    doInit: function(component, event, helper){
        const flow = component.find("flowData");
        var inputVariables = [
            {
                name : "recordId",
                type : "String",
                value: component.get("v.recordId")
            }
        ];
        flow.startFlow("CleanOrderCriteriaProcess", inputVariables);
    },

    handleStatusChange: function (component, event) {
        if(event.getParam("status") === "FINISHED") {
            $A.get("e.force:closeQuickAction").fire();
            $A.get('e.force:refreshView').fire();
        }
    }
})