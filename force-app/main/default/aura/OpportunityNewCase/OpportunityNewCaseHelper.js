({
    fetchOpportunity: function(component, event, helper) {

        var action = component.get("c.fetchOpportunityDetails");

        action.setParams({
            "oppId" : component.get("v.recordId")
        });

        action.setCallback(this, function(response) {
            var oppDetails = response.getReturnValue();
            if (oppDetails.AccountId !== undefined) {
                component.set("v.accountId", oppDetails.AccountId);
            }
            if (oppDetails.Contract_VF__c !== undefined) {
                component.set("v.contractvfId", oppDetails.Contract_VF__c);
            }
            if (oppDetails.Primary_Basket__c !== undefined) {
                component.set("v.basketId", oppDetails.Primary_Basket__c);
            }
            if (oppDetails.Primary_Quote__c !== undefined) {
                component.set("v.quoteId", oppDetails.Primary_Quote__c);
            }
        });

        $A.enqueueAction(action);

    },

    fetchListOfRecordTypes: function(component, event, helper) {

        var action = component.get("c.fetchRecordTypeValues");

        action.setParams({
            "objectName" : "Case"
        });

        action.setCallback(this, function(response) {
            var mapOfRecordTypes = response.getReturnValue();
            component.set("v.options", mapOfRecordTypes);
        });

        $A.enqueueAction(action);

    },

    showCreateRecordModal : function(component, recordTypeId, entityApiName) {

        debugger;

        var createRecordEvent = $A.get("e.force:createRecord");
        if(createRecordEvent){ //checking if the event is supported
            if(recordTypeId){//if recordTypeId is supplied, then set recordTypeId parameter
                createRecordEvent.setParams({
                    "entityApiName": entityApiName,
                    "recordTypeId": recordTypeId,
                    "defaultFieldValues": {
                        "Opportunity__c": component.get("v.recordId"),
                        "Account__c": component.get("v.accountId"),
                        "Contract_VF__c": component.get("v.contractvfId"),
                        "Quote__c": component.get("v.quoteId"),
                        "Product_Basket__c": component.get("v.basketId")
                    }
                });

            } else{//else create record under master recordType

                createRecordEvent.setParams({
                    "entityApiName": entityApiName,
                    "defaultFieldValues": {
                        "Opportunity__c": component.get("v.recordId"),
                        "Account__c": component.get("v.accountId"),
                        "Contract_VF__c": component.get("v.contractvfId"),
                        "Quote__c": component.get("v.quoteId"),
                        "Product_Basket__c": component.get("v.basketId")
                    }

                });

            }

            createRecordEvent.fire();

        } else{

            alert('This event is not supported');

        }

    },



    /*

     * closing quickAction modal window

     * */

    closeModal : function(){

        var closeEvent = $A.get("e.force:closeQuickAction");

        if(closeEvent){

            closeEvent.fire();

        } else{

            alert('force:closeQuickAction event is not supported in this Ligthning Context');

        }

    },

})