({
    handleCancelClick : function(component, event, helper) {
        component.destroy();
    },
    handleOkClick : function(component, event, helper) {
        var confirmEvent = component.getEvent("confirm");
        confirmEvent.setParam("param", component.get("v.param"));
        confirmEvent.fire();
        component.destroy();
    }
});