({
    init: function (cmp, event, helper) {
        //console.log('init');
        var currentStep = cmp.get("v.currentStep");
        helper.updateStepHelpText(cmp, currentStep);
    },

    toggleHelpSection: function (cmp, event, helper) {
        //console.log('toggleHelpSection');
        try {
            $A.util.toggleClass(cmp.find("help-section"), "slds-is-open");
        }
        catch (exc) {
            console.error(exc);
        }
    },

    stepsUpdated: function (cmp, event, helper) {
        try {
            var steps = event.getParam('value');
            var eventCmp = event.getSource();

            var currentStep = 'account';
            var activeStepIncomplete = true;
            for (var i=0; i<steps.length; i++) {
                var step = steps[i];
                if (step.isActive) {
                    currentStep = step.name;
                    activeStepIncomplete = !step.isComplete;
                    break;
                }
            }

            cmp.set('v.nextDisabled', activeStepIncomplete);

            //console.log(':::stepsUpdated: ' + (currentStep === 'account') + '; ' + activeStepIncomplete);
        }
        catch (exc) {
            console.log(exc);
        }
    },


    onCurrentStepUpdated: function(cmp, event, helper) {
        //console.log('::: header onCurrentStepUpdated');
        var currentStep = event.getParam('value');
        helper.updateStepHelpText(cmp, currentStep);
    },


    previousStep: function (cmp, event, helper) {
        var pathNavigationButtonEvent = cmp.getEvent('pathNavigationButtonEvent');
        pathNavigationButtonEvent.setParam('previous', true);
        pathNavigationButtonEvent.fire();
    },


    nextStep: function (cmp, event, helper) {
        var pathNavigationButtonEvent = cmp.getEvent("pathNavigationButtonEvent");
        pathNavigationButtonEvent.setParam('next', true);
        pathNavigationButtonEvent.fire();
    }
})