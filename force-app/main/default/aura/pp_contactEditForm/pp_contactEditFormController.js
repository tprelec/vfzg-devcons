({
    onInit: function (cmp, event, helper) {
        helper.doInit(cmp);
    },


    onSubmit: function (cmp, event, helper) {
        console.log(':::onSubmit');
        var fields = event.getParam("fields");
        //fields['AccountId'] = cmp.get("v.accountId");
        //event.setParam('fields', fields);

        console.log(fields.AccountId);
        console.log(fields.RecordTypeId);

        /*
        var customRequiredFields = cmp.find('customRequired');
        console.log(customRequiredFields);
        for (var i=0; i<customRequiredFields.length; i++) {
            var field = customRequiredFields[i];
            if (!field.get('v.value')) {
                console.error(field.get("v.name") + ' field is empty');
            }
        }
        */
    },

    onSuccess: function(cmp, event, helper) {
        console.log(":::onSuccess");

        var response = event.getParam("response");

        try {
            //cmp.set("v.newContactId", response.id);
            var formEvent = $A.get("e.c:pp_contactEditFormEvent");
            formEvent.setParam("contactId", response.id);
            formEvent.setParam("isNew", !cmp.get("v.contactId"));
            formEvent.fire();

            cmp.set("v.contactId", response.id);

            // Now closing the modal
            var event = $A.get("e.c:pp_contactEditFormButtonClicked");
            event.setParam("isSave", false);
            event.fire();
            //console.log(response.fields.AccountId);
            //console.log(response.recordTypeInfo.recordTypeId);
        }
        catch (exc) {
            console.error(exc);
        }
        //console.log(fields.fields);
        //console.log(fields.fields.AccountId);
    },

    handleModalButtonClicked: function (cmp, event, helper) {
        console.log(':::handleModalButtonClicked');
        var isSave = event.getParam('isSave');
        if (isSave) {
            console.log(cmp.find("recordEditForm"));
            cmp.find("recordEditForm").submit();
            //cmp.find("submitButton").click();
        }

    }
})