({
    doInit: function (cmp) {
        try {
            var action = cmp.get("c.getContactGeneralRecordTypeId");

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    //cmp.set('v.showSpinner', false);
                    var recordTypeId = response.getReturnValue();
                    //var form = cmp.find("recordEditForm");
                    //form.set('v.recordTypeId', recordTypeId);

                    cmp.set("v.recordTypeId", recordTypeId);
                    console.log(':::recordTypeId = ' + recordTypeId);
                }
                else if (state === "INCOMPLETE") {
                    // do something
                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    if (errors) {
                        if (errors[0] && errors[0].message) {
                            console.error("Error message: " +
                                errors[0].message);
                        }
                    } else {
                        console.error("Unknown error");
                    }
                }
            });

            //cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    }
})