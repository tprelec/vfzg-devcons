({
	doInit: function (component, event, helper) {
		try {
			helper.checkIfSitesExist(component);
		} catch (exc) {
			console.error(exc);
		}
	}
});
