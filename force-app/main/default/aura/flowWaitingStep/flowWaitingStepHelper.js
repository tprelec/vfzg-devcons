({
	waitConfirmation: function (component) {
		var action = component.get('c.getRecord');
		action.setParams({
			recordId: component.get('v.recordID'),
			fieldName: component.get('v.fieldName')
		});

		action.setCallback(this, function (resp) {
			if (resp.getState() === 'SUCCESS') {
				let fieldValue = resp.getReturnValue()[component.get('v.fieldName')];
				let completedValue = component.get('v.fieldValue');
				let errorValue = component.get('v.errorValue');
				let isCompleted = fieldValue === completedValue;
				let isError = fieldValue === errorValue;

				if (isCompleted) {
					//check if we are calling this from opp
					if (component.get('v.recordID').substring(0, 3) == '006') {
						let self = this; //assign this to new variable becase this is different inside the promise
						this.getContractedProducts(component) //check if contracted products are generated after close won
							.then(function (result) {
								if (result) {
									self.redirect(component, false);
								} else {
									return self.delayPromise(9000); //if they are not generated give them some time to be
								}
							})
							.then(function (result) {
								return self.getContractedProducts(component); //check again if the contracted products are generated
							})
							.then(function (result) {
								if (result) {
									self.redirect(component, false); //redirect to order form page
								} else {
									self.redirect(component, true); //redirect to error page "No contracted products..."
								}
							});
					} else {
						this.redirect(component, false);
					}
				} else if (isError) {
					this.redirect(component, true);
				} else {
					window.setTimeout(
						$A.getCallback(function () {
							$A.enqueueAction(action);
						}),
						component.get('v.stepInterval')
					);
				}
			}
		});
		$A.enqueueAction(action);
	},

	redirect: function (component, isError) {
		let pageReference;
		let toastParams;
		if (isError) {
			pageReference = JSON.parse(component.get('v.errorPageReference'));
			let msg = component.get('v.errorMessage');
			if (msg) {
				toastParams = {
					message: msg,
					type: 'error'
				};
			}
		} else {
			pageReference = JSON.parse(component.get('v.pageReference'));
			let msg = component.get('v.successMessage');
			if (msg) {
				toastParams = {
					message: msg,
					type: 'success'
				};
			}
		}
		if (toastParams) {
			var toastEvent = $A.get('e.force:showToast');
			toastEvent.setParams(toastParams);
			toastEvent.fire();
		}
		// Build full URL for community
		if (pageReference.type === 'standard__webPage') {
			let currentURL = window.location.href;
			if (currentURL.indexOf('/s/') > 0) {
				var n = currentURL.indexOf('/s/');
				let siteUrl = currentURL.substring(0, n != -1 ? n : currentURL.length);
				pageReference.attributes.url = siteUrl + pageReference.attributes.url;
				window.location.href = pageReference.attributes.url;
				return;
			}
		}
		let navService = component.find('navService');
		navService.navigate(pageReference);
	},
	getContractedProducts: function (component) {
		return new Promise(
			$A.getCallback(function (resolve, reject) {
				let contractedProductsAction = component.get('c.getContractedProducts');
				contractedProductsAction.setParams({
					recordId: component.get('v.recordID')
				});

				contractedProductsAction.setCallback(this, function (response) {
					if (response.getState() === 'SUCCESS') {
						resolve(response.getReturnValue());
					}
				});
				$A.enqueueAction(contractedProductsAction);
			})
		);
	},
	delayPromise: function (time, value) {
		return new Promise(function (resolve) {
			setTimeout(resolve.bind(null, value), time);
		});
	}
});
