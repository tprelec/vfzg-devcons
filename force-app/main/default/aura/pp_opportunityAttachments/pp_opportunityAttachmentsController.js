({
    onInit: function (cmp, event, helper) {
        var opptyId = cmp.get("v.opportunityId");

        helper.retrieveAttachments(cmp, opptyId);
    },


    refreshList: function(cmp, event, helper) {
        var opptyId = cmp.get("v.opportunityId");
        helper.retrieveAttachments(cmp, opptyId);
    },


    hideIframe: function (cmp, event, helper) {
        cmp.set('v.showIframe', false);

        var opptyId = cmp.get("v.opportunityId");
        helper.retrieveAttachments(cmp, opptyId);
    },


    handleManageAttachmentsClick: function (cmp, event, helper) {
        cmp.set('v.showIframe', true);
    }
})