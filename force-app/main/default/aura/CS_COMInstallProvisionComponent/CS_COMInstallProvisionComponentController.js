({
  init: function(cmp, event, helper) {

    var recordId = cmp.get("v.recordId");
    var action = cmp.get("c.getData");
    action.setParams({
      recordId: recordId
    });

    action.setCallback(
      this,
      $A.getCallback(function(response) {
        var state = response.getState();
        if (state === "SUCCESS") {
          var rows = response.getReturnValue();
          cmp.set("v.data", rows);
          
          var action_type = cmp.get("c.getDeliveryTaskType");
          action_type.setParams({
            recordId: recordId
          });
          action_type.setCallback(
            this,
            $A.getCallback(function(response_type) {
              var type = response_type.getReturnValue();
              cmp.set("v.taskType", type);
              helper.getColumns(cmp,event);
            }));

            $A.enqueueAction(action_type);
        } else if (state === "ERROR") {
          var errors = response.getError();
          console.error(errors);
        }
      })
    );
    $A.enqueueAction(action);
  }
});