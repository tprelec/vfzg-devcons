({
    getColumns : function (cmp, event){
        var caseRecordType = cmp.get('v.CaseRecordType');
        var taskType = cmp.get('v.taskType');
        
        var columnNames = [];

        if (taskType === 'COM_Provisioning') {
            columnNames = ["Product Name", "External Service ID", "Component Name", "Article Name", "Provisioning Code", "Previous Provisioning Code", "MACD Action", "Service Id"];
        } else if (taskType === 'COM_Installation') {
            columnNames = ["Product Name", "Component Name", "Article Name", "Provisioning Code", "Previous Provisioning Code", "MACD Action", "Installed", "Installed Date", "Activated", "Activated Date", "Tested", "Tested Date", "Implemented", "Implemented Date"];
        }

        cmp.set("v.columns", columnNames);
    }
});