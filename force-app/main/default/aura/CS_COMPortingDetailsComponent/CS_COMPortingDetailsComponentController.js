({
    init: function(cmp, event, helper){
        cmp.set('v.columns',[
            {label: 'Name', fieldName: 'name', type: 'text'},
            {label: 'Value', fieldName: 'value', type: 'text'},
            ]);
    }});