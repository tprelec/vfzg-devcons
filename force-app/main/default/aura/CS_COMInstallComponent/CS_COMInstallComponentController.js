({
    init : function(cmp, event, helper){
        var taskType = cmp.get("v.taskType");
        if(taskType == "COM_Returnbox") {
            var deliveryComponentWrappers = cmp.get("v.deliveryComponentWrappers");
            for(let i = 0; i < deliveryComponentWrappers.length; i++) {
                deliveryComponentWrappers[i].editable = false;
            }

            cmp.set("v.deliveryComponentWrappers",deliveryComponentWrappers);
            cmp.set("v.disableSaveChanges",true);
        }
    },
    handleSaveChanges : function(cmp, event, helper) {
        var allValid = true;
        var hasDirtyComponent = false;

        var childCmp = cmp.find("childComponent");
        if (Array.isArray(childCmp)) {
            for (let childCmpKey in childCmp) {
                if (!childCmp[childCmpKey].performValidation()) {
                    allValid = false;
                }

                if (childCmp[childCmpKey].performCheckIsDirty()) {
                    hasDirtyComponent = true;
                }
            }
        } else if (childCmp !== undefined) {
            allValid = childCmp.performValidation();
            hasDirtyComponent = childCmp.performCheckIsDirty();
        }

        if (allValid && !hasDirtyComponent) {
            var deliveryComponentWrappersToUpdate = cmp.get("v.deliveryComponentWrappersToUpdate");
            if (deliveryComponentWrappersToUpdate.length > 0) {
                helper.handleShowSpinner(cmp,event);
                var action = cmp.get("c.updateDeliveryComponents");
                action.setParams({
                    "wrappers": JSON.stringify(deliveryComponentWrappersToUpdate)
                });

                action.setCallback(
                    this,
                    $A.getCallback(function(response) {
                        var state = response.getState();
                        if (state === "SUCCESS") {
                            console.log(response.getReturnValue());
                            helper.refresh(cmp);
                            helper.handleHideSpinner(cmp,event);
                            helper.successMessage('Changes Saved!');
                        } else if (state === "ERROR") {
                            var errors = response.getError();
                            helper.errorMessage('Error while saving Task!' + errors);
                        }
                    })
                );
                $A.enqueueAction(action);
            } else {
                helper.infoMessage('No changes made!');
            }
        } else {
            helper.errorMessage("Component details not saved or invalid!");
        }
    },
    handleImplDateSelect : function(cmp, event, helper){
        helper.getChangedData(cmp,event);
    },
    handleGlobalImplDateChange : function (cmp, event, helper) {
        helper.getChangedDataGlobal(cmp,event);
    },
    handleCloseTask: function(cmp, event, helper) {
        var allValid = true;
        var hasDirtyComponent = false;

        var childCmp = cmp.find("childComponent");
        if (Array.isArray(childCmp)) {
            for (let childCmpKey in childCmp) {
                if (!childCmp[childCmpKey].performValidation()) {
                    allValid = false;
                }

                if (childCmp[childCmpKey].performCheckIsDirty()) {
                    hasDirtyComponent = true;
                }
            }
        } else if (childCmp !== undefined) {
            allValid = childCmp.performValidation();
            hasDirtyComponent = childCmp.performCheckIsDirty();
        }

        if (allValid && !hasDirtyComponent) {
            helper.closeTask(cmp, event);
        } else {
            helper.errorMessage("Component details not saved or invalid!");
        }
    }
});