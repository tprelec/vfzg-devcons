({
    closeTask :function (cmp, event) {
        this.handleShowSpinner(cmp,event);
        var recordId = cmp.get('v.recordId');

        var action = cmp.get("c.closeTask");
        action.setParams({
            "recordId" : recordId
        })

        action.setCallback(this, function(a){
            if(action.getState() == 'SUCCESS'){
                var response = a.getReturnValue();
                if(response === 'Task Closed'){
                    this.handleHideSpinner(cmp,event);
                    this.successMessage('Task Closed!');
                } else {
                    this.handleHideSpinner(cmp,event);
                    var errors = response;
                    this.errorMessage('Error while saving Task! ' + errors);
                }
            } else {
                this.errorMessage('Error while saving Task!');
            }
        });

        $A.enqueueAction(action);
    },
    refresh : function(cmp){
        var emptyList = [];
        cmp.set('v.deliveryComponentWrappersToUpdate',emptyList);
        $A.get('e.force:refreshView').fire();
    },
    getChangedData : function(cmp,event){
        var deliveryComponentWrappersToUpdate = cmp.get('v.deliveryComponentWrappersToUpdate');
        
        var recordExists = false;
        var recordExistsNumber;
        var record = {
            deliveryComponentId: event.getSource().get('v.id'),
            value: event.getSource().get('v.value')
        };
        
        if (deliveryComponentWrappersToUpdate.length > 0) {
            for(var i in deliveryComponentWrappersToUpdate){
                if (deliveryComponentWrappersToUpdate[i].deliveryComponentId == record.deliveryComponentId){
                    recordExists = true;
                    recordExistsNumber = i;
                }
            }
        }

        if (recordExists) {
            deliveryComponentWrappersToUpdate[recordExistsNumber].value = record.value;
        } else {
            deliveryComponentWrappersToUpdate.push(record);
        }

        cmp.set('v.deliveryComponentWrappersToUpdate', deliveryComponentWrappersToUpdate);
    },
    getChangedDataGlobal : function(cmp,event){
        var deliveryComponentWrappersToUpdate = cmp.get('v.deliveryComponentWrappersToUpdate');
        var deliveryComponentWrappers = cmp.get("v.deliveryComponentWrappers");
        
        for(let i = 0; i < deliveryComponentWrappers.length; i++) {
            deliveryComponentWrappers[i].implementedDate = event.getParam("value");
            
            var record = {
                deliveryComponentId: deliveryComponentWrappers[i].deliveryComponentId,
                implementedDate: event.getParam("value"),
                value: event.getParam("value")
            };
            
            var recordExists = false;
            var recordExistsNumber;
            
            if (deliveryComponentWrappersToUpdate.length > 0) {
                for(var j in deliveryComponentWrappersToUpdate){
                    if (deliveryComponentWrappersToUpdate[j].deliveryComponentId == record.deliveryComponentId){
                        recordExists = true;
                        recordExistsNumber = j;
                    }
                }
            }
    
            if (recordExists) {
                deliveryComponentWrappersToUpdate[recordExistsNumber].implementedDate = record.implementedDate;
                deliveryComponentWrappersToUpdate[recordExistsNumber].value = record.value;
            } else {
                deliveryComponentWrappersToUpdate.push(record);
            }
        }
        
        cmp.set('v.deliveryComponentWrappersToUpdate', deliveryComponentWrappersToUpdate);
        
    },
    handleShowSpinner: function(component, event, helper) {
        component.set("v.isSpinner", true);
    },
    handleHideSpinner : function(component,event,helper){
        component.set("v.isSpinner", false);
    },
    successMessage : function(successMessage) {
        iziToast.success({
            title: 'Success',
            message: successMessage,
            position: 'topCenter',
            progressBar: false
        });
    },
    errorMessage : function(messageError) {
        iziToast.error({
            title: 'Error',
            message: messageError,
            position: 'topCenter',
            progressBar: false
        });
    },
    infoMessage : function(infoMessage) {
        iziToast.info({
            title: 'Info',
            message: infoMessage,
            position: 'topCenter',
            progressBar: false
            }
        )
    }
});