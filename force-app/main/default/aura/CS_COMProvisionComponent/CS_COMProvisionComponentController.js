({
    init : function(cmp, event, helper){

    },
    handleCloseCase: function(cmp, event, helper) {
        var allValid = true;
        var hasDirtyComponent = false;

        var childCmp = cmp.find("childComponent");
        if (Array.isArray(childCmp)) {
            for (let childCmpKey in childCmp) {
                if (!childCmp[childCmpKey].performValidation()) {
                    allValid = false;
                }

                if (childCmp[childCmpKey].performCheckIsDirty()) {
                    hasDirtyComponent = true;
                }
            }
        } else if (childCmp !== undefined) {
            allValid = childCmp.performValidation();
            hasDirtyComponent = childCmp.performCheckIsDirty();
        }

        if (allValid && !hasDirtyComponent) {
            helper.closeTask(cmp, event);
        } else {
            helper.errorMessage("Component details not saved or invalid!");
        }
    }
});