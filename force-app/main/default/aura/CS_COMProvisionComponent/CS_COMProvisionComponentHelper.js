({
    closeTask :function (cmp, event) {
        this.handleShowSpinner(cmp,event);

        var recordId = cmp.get('v.recordId');

        var action = cmp.get("c.closeTask");
        action.setParams({
            "recordId" : recordId
        })

        action.setCallback(this, function(a){
            if(action.getState() == 'SUCCESS'){
                var response = a.getReturnValue();
                if(response === 'Task Closed'){
                    this.handleHideSpinner(cmp,event);
                    this.successMessage('Task Closed!');
                } else {
                    var errors = response.getError();
                    this.errorMessage('Error while saving Task!' + errors);
                }
            } else {
                this.errorMessage('Error while saving Task!');
            }
        });

        $A.enqueueAction(action);
    }, successMessage : function(successMessage) {
        iziToast.success({
            title: 'Success',
            message: successMessage,
            position: 'topCenter',
            progressBar: false
        });
    },
    errorMessage : function(messageError) {
        iziToast.error({
            title: 'Error',
            message: messageError,
            position: 'topCenter',
            progressBar: false
        });
    },
    handleShowSpinner: function(component, event, helper) {
        component.set("v.isSpinner", true);
    },
    handleHideSpinner : function(component,event,helper){
        component.set("v.isSpinner", false);
    },
    getColumns : function (cmp, event){
        var caseRecordType = cmp.get('v.CaseRecordType');
        var columnNames = [];

        if (caseRecordType.includes('CS_COM_Provisioning')) {
            columnNames = ["Product Name", "Component Name", "Article Name", "Provisioning Code"/*, "Service Id"*/];
        } else if (caseRecordType.includes('CS_COM_Install')) {
            columnNames = ["Product Name", "Component Name", "Installed", "Installed Date", "Activated", "Activated Date", "Tested", "Tested Date", "Implemented", "Implemented Date"];
        }

        cmp.set("v.columns", columnNames);
    }
});