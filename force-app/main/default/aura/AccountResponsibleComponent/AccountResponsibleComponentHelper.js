({
	doInit: function (component, event) {
		var action = component.get('c.fetchAll');
		action.setParams({
			recordId: component.get('v.recordId')
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				var res = response.getReturnValue();
				component.set('v.record', res['acc']);
				component.set('v.currentUser', res['currUser']);
				component.set('v.currentOwner', res['currOwner']);
				if (res['msg']) {
					component.set('v.isMsg', true);
					component.set('v.msg', res['msg']);
				}
				if (res['dealer']) component.set('v.dealer', res['dealer']);
			} else if (state === 'ERROR') {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						component.set('v.msg', errors[0].message);
					}
				} else {
					console.log('Unknown error');
				}
			}
		});
		$A.enqueueAction(action);
	},
	claimRecord: function (component, event) {
		event.srcElement.disabled = true;
		var action = component.get('c.claim');
		var c = component.get('v.comment');
		action.setParams({
			recordId: component.get('v.recordId'),
			comment: c
		});
		action.setCallback(this, function (response) {
			var state = response.getState();
			var res = response.getReturnValue();
			if (state === 'SUCCESS') {
				component.set('v.msg', 'Claim process started!');
				component.set('v.isMsg', true);
				component.set('v.isSuccess', true);
				event.srcElement.disabled = false;
				$A.get('e.force:refreshView').fire();
			} else {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						component.set('v.msg', errors[0].message);
					}
				} else {
					console.log('Unknown error');
				}
			}
		});
		$A.enqueueAction(action);
	},
	closeModal: function () {
		var closeEvent = $A.get('e.force:closeQuickAction');
		if (closeEvent) {
			closeEvent.fire();
		} else {
			console.log('force:closeQuickAction event is not supported in this Ligthning Context');
		}
	}
});
