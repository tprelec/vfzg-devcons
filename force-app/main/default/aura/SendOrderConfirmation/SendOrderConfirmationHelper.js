({
	closeModal: function () {
		$A.get("e.force:closeQuickAction").fire();
	},
	refreshView: function () {
		$A.get("e.force:refreshView").fire();
	},
	validateAndGetOptions: function (component, event, helper) {
		let action = component.get("c.validateOpportunity");
		let oppId = component.get("v.recordId");
		action.setParams({ oppId: oppId });
		action.setCallback(this, function (response) {
			let state = response.getState();
			if (state === "SUCCESS") {
				helper.getOptions(component, event, helper);
			} else {
				component.set("v.isValid", false);
				component.set("v.validationMsg", response.getError()[0].message);
			}
		});
		$A.enqueueAction(action);
	},
	getOptions: function (component, event, helper) {
		let action = component.get("c.getAvailableOptions");
		let oppId = component.get("v.recordId");
		action.setParams({ oppId: oppId });
		action.setCallback(this, function (response) {
			let state = response.getState();
			if (state === "SUCCESS") {
				let options = response.getReturnValue();
				component.set("v.options", options);
				component.set("v.option", options[0].value);
			} else {
				console.log("Failed with state: " + state);
			}
		});
		$A.enqueueAction(action);
	},
	generateNewConfirmation: function (component, event, helper) {
		let action = component.get("c.createNewConfirmation");
		let oppId = component.get("v.recordId");
		component.set("v.isLoading", true);
		action.setParams({ oppId: oppId });
		action.setCallback(this, function (response) {
			let state = response.getState();
			if (state === "SUCCESS") {
				let docRequest = response.getReturnValue();
				component.set("v.docRequest", docRequest);
				helper.checkDocumentRequest(component, event, helper);
			} else {
				console.log("Failed with state: " + state);
			}
		});
		$A.enqueueAction(action);
	},
	checkDocumentRequest: function (component, event, helper) {
		let action = component.get("c.getDocumentRequest");
		let docRequest = component.get("v.docRequest");
		action.setParams({ requestId: docRequest.Id });
		action.setCallback(this, function (response) {
			let state = response.getState();
			if (state === "SUCCESS") {
				let docRequest = response.getReturnValue();
				component.set("v.docRequest", docRequest);
				if (docRequest.mmdoc__Status__c === "Completed") {
					helper.send(component, event, helper, true);
				} else if (docRequest.mmdoc__Status__c === "Error") {
					component.set("v.isLoading", false);
					console.log("Failed with message: " + docRequest.mmdoc__Error_Message__c);
					helper.reportError("Error while generating document.");
				} else {
					window.setTimeout(
						$A.getCallback(function () {
							helper.checkDocumentRequest(component, event, helper);
						}),
						1000
					);
				}
			} else {
				console.log("Failed with state: " + state);
				helper.reportError("Error while generating document.");
			}
		});
		$A.enqueueAction(action);
	},
	send: function (component, event, helper, isNew) {
		let action;
		if (isNew) {
			action = component.get("c.sendNewConfirmation");
			let oppId = component.get("v.recordId");
			let docRequest = component.get("v.docRequest");
			action.setParams({ oppId: oppId, attachmentId: docRequest.mmdoc__File_Id__c });
		} else {
			action = component.get("c.sendSignedConfirmation");
			let oppId = component.get("v.recordId");
			action.setParams({ oppId: oppId });
		}
		action.setCallback(this, function (response) {
			component.set("v.isLoading", false);
			let state = response.getState();
			if (state === "SUCCESS") {
				helper.reportSuccess("Confirmation sent!");
				helper.closeModal();
				helper.refreshView();
			} else {
				console.log("Failed with state: " + state);
				helper.reportError("Error while sending confirmation.");
			}
		});
		$A.enqueueAction(action);
	},
	reportSuccess: function (message) {
		this.showToast(message, "success");
	},
	reportError: function (message) {
		this.showToast(message, "error");
	},
	showToast: function (message, messageType) {
		var title = messageType.charAt(0).toUpperCase() + messageType.substring(1);
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			title: title,
			message: message,
			type: messageType
		});
		toastEvent.fire();
	}
});