({
	init: function (component, event, helper) {
		helper.validateAndGetOptions(component, event, helper);
	},
	handleCloseModal: function (component, event, helper) {
		helper.closeModal();
	},
	handleSendConfirmation: function (component, event, helper) {
		let option = component.get("v.option");
		if (option === "Send new confirmation (without signed quote)") {
			helper.generateNewConfirmation(component, event, helper);
		} else {
			helper.send(component, event, helper, false);
		}
	}
});