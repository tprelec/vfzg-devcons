({
	resetAllSteps: function (cmp) {
		cmp.set('v.selectedAccountId', '');
		cmp.set('v.selectedAccount', null);

		cmp.getEvent('resetAllStepsEvent').fire();
	},

	goToStep: function (cmp, nextStep) {
		cmp.getEvent('resetAllStepsEvent').fire();

		let goToStepEvent = cmp.getEvent('goToStepEvent');
		goToStepEvent.setParam('stepName', nextStep);
		goToStepEvent.fire();
	},

	createAccountSharing: function (cmp, account) {
		try {
			let action = cmp.get('c.createAccountSharing');
			action.setParams({ accountId: account['Id'] });

			action.setCallback(this, function (response) {
				let state = response.getState();
				if (state === 'SUCCESS') {
					let result = response.getReturnValue();

					if (cmp.isValid() && result.success) {
						cmp.set('v.selectedAccountId', account['Id']);
						cmp.set('v.selectedAccount', account);

						let stepCompletedEvent = cmp.getEvent('stepCompletedEvent');
						stepCompletedEvent.setParam('stepName', 'account');
						stepCompletedEvent.fire();

						cmp.set('v.showSpinner', false);
					}
				} else if (state === 'INCOMPLETE') {
					console.error('state INCOMPLETE');
					cmp.set('v.showSpinner', false);
				} else if (state === 'ERROR') {
					let errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.error('Error message: ' + errors[0].message);
						}
					} else {
						console.error('Unknown error');
					}
					cmp.set('v.showSpinner', false);
				}
			});
			if (account.Location_Status__c === 'E') {
				var errorMsg = $A.get('$Label.c.InvalidKvKLabel');
				var toastEvent = $A.get('e.force:showToast');
				toastEvent.setParams({
					title: 'Error',
					type: 'error',
					message: errorMsg,
					mode: 'sticky'
				});
				toastEvent.fire();
			}
			cmp.set('v.showSpinner', true);
			$A.enqueueAction(action);
		} catch (exc) {
			console.error(exc);
		}
	}
});
