({
	accountSelectedLWC: function (component, event, helper) {
		let selectedAccount = event.getParam("data").record;

		helper.resetAllSteps(component);

		if (selectedAccount) {
			helper.createAccountSharing(component, selectedAccount);
		}
	},

	goToStep: function (component, event, helper) {
		let nextStep = event.getParam("nextStep");

		helper.goToStep(component, nextStep);
	}
});