({
    handleSaveClick : function(cmp,event,helper){
        if(helper.handleValidate(cmp,event,helper)){
            helper.handleShowSpinner(cmp,event,helper);

            var details = cmp.get('v.deliveryComponentWrapperDetails');

            var action = cmp.get('c.saveDeliveryComponentAttributes');
            action.setParams({
                "details" : JSON.stringify(details)
            })

            action.setCallback(this, $A.getCallback(function (response) {
                var state = response.getState();
                helper.handleHideSpinner(cmp,event,helper);
                if (state === "SUCCESS") {
                    var result = response.getReturnValue();
                    cmp.set('v.isDirty',false);
                    helper.successMessage(result);
                    $A.get('e.force:refreshView').fire();
                } else if (state === "ERROR") {
                    var errors = response.getError();
                    helper.errorMessage(errors);
                }
            }));
            $A.enqueueAction(action);
        } else {
            helper.errorMessage('Fields are not valid!');
        }
    },
    performValidation : function (cmp,event,helper) {
        return helper.handleValidate(cmp,event,helper);
    },
    performCheckIsDirty : function (cmp, event, helper) {
        var isDirty = cmp.get('v.isDirty');
        return isDirty;
    },
    handleOnChange : function(cmp,event,helper){
        cmp.set('v.isDirty',true);
    }
});