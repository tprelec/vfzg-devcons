({
    successMessage : function(successMessage) {
        iziToast.success({
            title: 'Success',
            message: successMessage,
            position: 'topCenter',
            progressBar: false
        });
    },

    errorMessage : function(messageError) {

        iziToast.error({
            title: 'Error',
            message: messageError,
            position: 'topCenter',
            progressBar: false
        });
    },
    handleShowSpinner: function(component, event, helper) {
        component.set("v.isSpinner", true);
    },
    handleHideSpinner : function(component,event,helper){
        component.set("v.isSpinner", false);
    },
    handleValidate :function (cmp,event, helper) {
        var result = cmp.find('validationId');
        var allValid;

        debugger;
        if(cmp.get('v.editable')==false) {
            allValid = true;
        } else if(result != undefined && Array.isArray(result)){
            allValid = cmp.find('validationId').reduce(function (validSoFar, inputCmp) {
                inputCmp.showHelpMessageIfInvalid();
                return validSoFar && (inputCmp.checkValidity());
            }, true);
        } else if (result != undefined) {
            allValid = result.checkValidity();
        } else {
            allValid = true;
        }

        return allValid;
    }
});