({
    onInit: function (cmp, action, helper) {
        helper.getDefaultOpptyRecordType(cmp);
        helper.getJourneyData(cmp);
        helper.getContractingFlowData(cmp);
        
        var accountId = cmp.get('v.accountId');
        cmp.find('account-field').set('v.value', accountId);
    },


    onFormLoad: function(cmp, event, helper) {

    },


    onSubmit: function (cmp, event, helper) {
        // submitted fields
        //var fields = event.getParam("fields");
        cmp.set("v.showSpinner", true);
    },


    onSuccess: function (cmp, event, helper) {
        try {
            var response = event.getParam("response");
            //var rti = response.recordTypeInfo;
            var recordId = response.id;

            var isCreate = !cmp.get("v.opportunityId");

            //console.log('Oppty ' + (isCreate ? 'created' : 'saved') + ' successfully; ' + ' opptyId = ' + recordId);

            cmp.set("v.showSpinner", false);
            cmp.set("v.editMode", false);
            //cmp.set("v.opportunityId", recordId);

            console.log('success => ' + recordId);


            var toastEvent = $A.get("e.force:showToast");
            var successMessage = isCreate ? $A.get("$Label.c.pp_Opportunity_Created") : $A.get("$Label.c.pp_Record_Saved");
            toastEvent.setParams({
                "title": "Success!",
                "message": successMessage,
                "type": "success"
            });
            toastEvent.fire();


            if (isCreate) {
                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": "/journey-path?opportunityId=" + recordId
                });
                urlEvent.fire();
            }

            /*
            var opptySavedEvent = cmp.getEvent("opptySavedEvent");
            opptySavedEvent.setParam("isCreate", isCreate);
            opptySavedEvent.setParam("opptyId", recordId);
            opptySavedEvent.fire();
            */
            $A.get('e.force:refreshView').fire();
        }
        catch (exc) {
            console.error(exc);
        }
    },


    onError: function(cmp, event, helper) {
        try {
            // lightning:messages shows errors by default
            cmp.set("v.showSpinner", false);

            var error = event.getParam("error");
            console.error('Failed to create record: ' + error.errorCode + '; ' + error.message);
        }
        catch (exc) {
            console.error(exc);
        }
    }
})