({
    CONSTANTS: {
        STATUS_CANCELLED: 'Cancelled',
        STATUS_IN_CANCELLATION: 'Cancellation Requested'
    },
    callServer: function (cmp, actionName, parameters, onSuccess, onError) {
        var action = cmp.get(actionName);
        if (parameters) {
            action.setParams(parameters);
        }
        action.setCallback(this, function (response) {
            if (response) {
                var state = response.getState();
                if (cmp.isValid()) {
                    if (state === "SUCCESS") {
                        var result = response.getReturnValue();

                        onSuccess(result);
                    } else if (state == "ERROR") {
                        var errors = response.getError();
                        if (!onError) {
                            this.handleError(errors);
                        } else {
                            onError(errors);
                        }
                    }
                }
            } else {
                onSuccess();
            }
        });
        $A.enqueueAction(action);
    },
    createInitRequest: function (cmp, event, helper) {
        let request = {
            recordId: cmp.get('v.recordId')
        };
        return request;
    },
    createRequest: function (cmp, event, helper) {
        let request = {
            deliveryOrderList: cmp.get('v.selectedRows')
        };
        return request;
    },
    readInitResponse: function (cmp, event, helper, responseJson) {
        var result = JSON.parse(responseJson);
        if (!result.deliveryOrderList) {
            helper.showToast('info', 'Unavailable', 'There are no available deliveryOrders for this order!', 'sticky');
        } else {
            var availableDeliveryOrders = [];
            var unavailableDeliveryOrders = [];
            
            result.deliveryOrderList.forEach(element => {
                if(element.PONR_Reached__c === true 
                    || element.Status__c === helper.CONSTANTS.STATUS_CANCELLED
                    || element.Status__c === helper.CONSTANTS.STATUS_IN_CANCELLATION) {
                        unavailableDeliveryOrders.push(element);  
                } else {
                    availableDeliveryOrders.push(element);
                }
            });
            cmp.set('v.data', availableDeliveryOrders);
            cmp.set('v.unavailData', unavailableDeliveryOrders);
        }
    },
    readResponse: function (cmp, event, helper, responseJson) {
        var result = JSON.parse(responseJson);
        if (!result.DeliveryOrderCancelled) {
            helper.showToast('info', 'Unavailable', 'Delivery Order cannot be cancelled if it has been Deprovisioned or Customer already Signed Off', 'sticky');
        } else {
            helper.showToast('success', 'Success', 'Delivery Order Cancellation Process has started.', 'sticky');
        }

        helper.hideElement(cmp, 'spinner');
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    hideElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.addClass(elm, 'slds-hide');
    },
    showElement: function (cmp, elementId) {
        var elm = cmp.find(elementId);
        $A.util.removeClass(elm, 'slds-hide');
    },
    showToast: function (type, title, message) {
        var toastEvent = $A.get("e.force:showToast");
        if (toastEvent) {
            toastEvent.setParams({
                "title": title,
                "message": message,
                "type": type
            });

            toastEvent.fire();
        }
    },
    handleError: function (errors) {
        if (errors) {
            if (console) {
                console.log('errors', errors);
            }
            this.showToast('error', 'Error:', errors[0].message);
        }
    }
})