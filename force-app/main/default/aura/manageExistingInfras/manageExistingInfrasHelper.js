({
	// Event fire on close modal
	helperCloseModal: function (component, event, helper) {
		var compEvent = component.getEvent('closeModalEvent');
		compEvent.setParams({ sourceComponent: 'manageExistingInfras' });
		compEvent.fire();
	},
	setupDataTable: function (component) {
		component.set('v.columns', [
			{
				label: 'Vendor',
				fieldName: 'Access_Vendor__c',
				type: 'picklist',
				editable: true,
				typeAttributes: {
					placeholder: 'Choose Access Vendor',
					options: [
						{ label: 'IPVPN-VF', value: 'IPVPN-VF' },
						{ label: 'IPVPN-ZIGGO', value: 'IPVPN-ZIGGO' },
						{ label: 'IPVPN-FLEX', value: 'IPVPN-FLEX' },
						{ label: 'IPVPN-EF', value: 'IPVPN-EF' },
						{ label: 'IPVPN-KPN', value: 'IPVPN-KPN' }
					],
					value: { fieldName: 'Access_Vendor__c' },
					context: { fieldName: 'Id' },
					fieldName: 'Access_Vendor__c',
					label: 'Access_Vendor__c'
				}
			},
			{
				label: 'Total MB Existing EVC',
				fieldName: 'Total_MB_Existing_EVC__c',
				type: 'number',
				sortable: true,
				editable: true
			},
			{
				label: 'Site ID',
				fieldName: 'RedirectUrl',
				type: 'url',
				typeAttributes: { label: { fieldName: 'SiteName' }, variant: 'base' }
			},
			{
				label: 'Technology Type',
				fieldName: 'Technology_Type__c',
				type: 'text',
				sortable: true
			},
			{
				label: 'Accessclass',
				fieldName: 'Accessclass__c',
				type: 'text',
				sortable: true
			},
			{
				label: 'Max Down Speed',
				fieldName: 'Access_Max_Down_Speed__c',
				type: 'number'
			},
			{
				label: 'Max Up Speed',
				fieldName: 'Access_Max_Up_Speed__c',
				type: 'number'
			}
		]);
	},
	// Check if there are Manualy created Infras and if there are enable Save manual added Infras button
	checkManualCreatedInfras: function (component) {
		var manualCreatedInfras = component.get('v.manualCreatedInfras');

		if (manualCreatedInfras.length != 0) {
			component.set('v.disableManualCreatedInfrasButton', false);
			component.set('v.disableOtherButtons', true);

			this.handleToastEvent(
				'Warning!',
				'Some of selected sites did not have an Existing Infra. Please save manual added Infras before proceeding.',
				'dismissable'
			);
		}
	},
	handleToastEvent: function (title, message, mode) {
		var toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
			title: title,
			message: message,
			mode: mode
		});
		toastEvent.fire();
	},
	// Onclick saving Manual Added Infras
	saveManualInfras: function (component, event, helper) {
		component.find('infrasDatatable').toggleSpinner(true);

		var manualInfras = component.get('v.manualCreatedInfras');
		var action = component.get('c.saveNewInfras');
		action.setParams({ spcs: manualInfras });
		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				component.set('v.disableManualCreatedInfrasButton', true);
				component.set('v.disableOtherButtons', false);
				component.find('infrasDatatable').toggleSpinner(false);
				this.handleToastEvent('Success!', 'Manual added Infras saved successfully.', 'dismissable');
			} else if (state == 'ERROR') {
				var error = response.getError();
				this.handleToastEvent('Warning!', error[0].message, 'dismissable');

				component.find('infrasDatatable').toggleSpinner(false);
			}
		});
		$A.enqueueAction(action);
	},
	// Iterating trough data and setting the URL for redirect when clicked on Site Name
	setupSiteIdLink: function (component) {
		var data = component.get('v.addedInfras');
		if (data) {
			let tempList = [];
			var isCommunity = component.get('v.isCommunityUser');
			var ppUrl = $A.get('$Label.c.pp_Community_Base_URL');
			data.forEach(function (record) {
				debugger;
				record.RedirectUrl = isCommunity ? ppUrl + '/s/detail/' + record.Access_Site_ID__c : '/' + record.Access_Site_ID__c;
				record.SiteName = record.Access_Site_ID__r.Name;
			});
			component.set('v.addedInfras', data);
			component.set('v.infras', data);
		}
	},
	// Onclick method for adding new Infras to selected sites
	addInfra: function (component, event, helper) {
		component.find('infrasDatatable').toggleSpinner(true);
		var selectedRows = component.get('v.allSelectedRows');

		if (selectedRows.length != 0) {
			var availableVendors = component.get('v.columns')[0].typeAttributes.options;
			var duplicateVendors = false;
			availableVendors = availableVendors.map((x) => x.value);

			var data = component.get('v.infras');
			var selectedSiteIds = [];
			var selectedSites = [];
			selectedRows.forEach((row) => {
				if (!selectedSiteIds.includes(row.Access_Site_ID__r.Id)) {
					selectedSites.push(row);
					selectedSiteIds.push(row.Access_Site_ID__r.Id);
				}
			});

			var alreadyInstalledVendors = [];
			selectedSites.forEach((site) => {
				data.forEach((infra) => {
					if (site.SiteName == infra.SiteName) {
						alreadyInstalledVendors.push(infra.Access_Vendor__c);
					}
				});
				let vendors = availableVendors.filter((x) => !alreadyInstalledVendors.includes(x));
				if (vendors.length != 0) {
					site.Access_Vendor__c = vendors[0];
				} else {
					component.find('infrasDatatable').toggleSpinner(false);
					this.handleToastEvent('Warning!', 'Selected site has a Infra by each Vendor.', 'dismissable');
					duplicateVendors = true;
				}
			});
			if (!duplicateVendors) {
				var action = component.get('c.saveNewInfras');
				action.setParams({ spcs: selectedSites });
				action.setCallback(this, function (response) {
					var state = response.getState();
					if (state == 'SUCCESS') {
						var result = response.getReturnValue();
						var isCommunity = component.get('v.isCommunityUser');
						var ppUrl = $A.get('$Label.c.pp_Community_Base_URL');
						result.forEach(function (record) {
							record.SiteName = record.Access_Site_ID__r.Name;
							record.RedirectUrl = isCommunity ? ppUrl + '/s/detail/' + record.Access_Site_ID__c : '/' + record.Access_Site_ID__c;
						});
						data = [...data, ...result];
						component.set('v.infras', data);
						component.set('v.allSelectedRows', []);
						component.find('infrasDatatable').toggleSpinner(false);
					} else if (state == 'ERROR') {
						var error = response.getError();
						this.handleToastEvent('Error!', error[0].pageErrors[0].message, 'dismissable');

						component.find('infrasDatatable').toggleSpinner(false);
					}
				});
				$A.enqueueAction(action);
			}
		} else {
			component.find('infrasDatatable').toggleSpinner(false);
			this.handleToastEvent('Warning!', 'Please select sites you wish to add Infra to.', 'dismissable');
		}
	},
	// Handle row selection
	helperRowSelection: function (component, event, helper) {
		component.set('v.allSelectedRows', event.getParam('selectedRows'));
	},
	// Handle delete selected Infras
	delete: function (component, event, helper) {
		debugger;
		component.find('infrasDatatable').toggleSpinner(true);
		var selectedRows = component.get('v.allSelectedRows');
		if (selectedRows.length != 0) {
			var action = component.get('c.deleteInfras');
			action.setParams({ spcs: selectedRows });
			action.setCallback(this, function (response) {
				var state = response.getState();
				if (state == 'SUCCESS') {
					var infras = component.get('v.infras');
					selectedRows.forEach((row) => {
						infras = infras.filter((data) => data.Id != row.Id);
					});
					component.set('v.infras', infras);
					this.handleToastEvent('Success!', 'Site Infras successfully deleted.', 'dismissable');

					component.find('infrasDatatable').toggleSpinner(false);
				} else {
					var errors = response.getError();
					this.handleToastEvent('Warning!', errors[0].message, 'dismissable');
				}
				component.find('infrasDatatable').toggleSpinner(false);
			});
			$A.enqueueAction(action);
		} else {
			component.find('infrasDatatable').toggleSpinner(false);
			this.handleToastEvent('Warning!', 'Please select Infras you wish to delete.', 'dismissable');
		}
	}
});
