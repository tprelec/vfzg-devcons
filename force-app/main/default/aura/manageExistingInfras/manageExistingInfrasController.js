({
	closeModal: function (component, event, helper) {
		helper.helperCloseModal(component, event, helper);
	},
	doInit: function (component, event, helper) {
		helper.setupDataTable(component);
		helper.setupSiteIdLink(component);
		helper.checkManualCreatedInfras(component);
	},
	handleOpenSite: function (component, event, helper) {
		helper.openSite(component, event, helper);
	},
	handleBackFromSiteWindow: function (component, event, helper) {
		helper.backFromSiteWindow(component, event, helper);
	},
	handleAddInfra: function (component, event, helper) {
		helper.addInfra(component, event, helper);
	},
	handleSave: function (component, event, helper) {
		helper.save(component, event, helper);
	},
	handleRowSelection: function (component, event, helper) {
		helper.helperRowSelection(component, event, helper);
	},
	handleDelete: function (component, event, helper) {
		helper.delete(component, event, helper);
	},
	handleSaveManualInfras: function (component, event, helper) {
		helper.saveManualInfras(component, event, helper);
	}
});
