({
	redirectToOrderEntry: function (component, event, helper) {
		try {
			var communityUser = component.get('c.isCommunity');
			communityUser.setCallback(this, function (response) {
				var isCommunity = response.getReturnValue();

				var pageRef;
				console.log('asd ' + isCommunity);
				if (isCommunity) {
					pageRef = {
						type: 'comm__namedPage',
						attributes: {
							name: 'Order_Entry__c'
						},
						state: {
							c__oppId: component.get('v.recordId')
						}
					};
				} else {
					pageRef = {
						type: 'standard__component',
						attributes: {
							componentName: 'c__OrderEntry'
						},
						state: {
							c__oppId: component.get('v.recordId')
						}
					};
				}
				var navService = component.find('navService');

				navService.navigate(pageRef);
				$A.get('e.force:closeQuickAction').fire();
			});
			$A.enqueueAction(communityUser);
		} catch (error) {
			console.error(error);
		}
	}
});
