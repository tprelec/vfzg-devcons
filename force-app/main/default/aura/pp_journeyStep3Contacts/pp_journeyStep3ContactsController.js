({
	onInit: function (cmp, event, helper) {
		helper.retrieveContacts(cmp);
		var actions = helper.getRowActions.bind(this);
		helper.setColumns(cmp, actions);
	},

	handleRowAction: function (cmp, event, helper) {
		var action = event.getParam('action');
		var contactId = event.getParam('row').Id;
		var accountId = cmp.get('v.selectedAccountId');

		if (action.name === 'edit') {
			helper.openEditContactModal(cmp, contactId);
		} else if (action.name === 'authorizeToSignFirst') {
			helper.authorizeToSignFirst(cmp, accountId, contactId);
		} else if (action.name === 'authorizeToSignSecond') {
			helper.authorizeToSignSecond(cmp, accountId, contactId);
		} else if (action.name === 'removeAuthorizeToSign') {
			helper.removeAuthorizeToSign(cmp, accountId, contactId);
		}
	},

	loadMoreContacts: function (cmp, event, helper) {
		helper.retrieveContacts(cmp, true);
	},

	createRecord: function (component, event, helper) {
		var createRecordEvent = $A.get('e.force:createRecord');
		createRecordEvent.setParams({
			entityApiName: 'Contact',
			defaultFieldValues: {
				AccountId: component.get('v.selectedAccountId')
			}
		});
		createRecordEvent.fire();
	},
	refreshComponent: function (component, event, helper) {
		helper.reloadContacts(component);
	},

	handleContactEditFormEvent: function (cmp, event, helper) {
		var contactId = event.getParam('contactId');
		var isNew = event.getParam('isNew');

		helper.retrieveContact(cmp, contactId, isNew);
	},

	handleContactMenuSelect: function (cmp, event, helper) {
		try {
			var selectedMenuItemValue = event.getParam('value');
			var contactId = event.getSource().get('v.value');
			var accountId = cmp.get('v.selectedAccountId');

			if ('edit' === selectedMenuItemValue) {
				helper.openEditContactModal(cmp, contactId);
			} else if ('authorizeToSignFirst' === selectedMenuItemValue) {
				helper.authorizeToSignFirst(cmp, accountId, contactId);
			} else if ('authorizeToSignSecond' === selectedMenuItemValue) {
				helper.authorizeToSignSecond(cmp, accountId, contactId);
			} else if ('removeAuthorizeToSign' === selectedMenuItemValue) {
				helper.removeAuthorizeToSign(cmp, accountId, contactId);
			}
		} catch (exc) {
			console.log(exc);
		}
	},

	openEditContactModal: function (cmp, event, helper) {
		helper.openEditContactModal(cmp, null);
	},

	addNewContactRow: function (cmp, event, helper) {
		helper.addNewContactRow(cmp);
	},

	handleContactEditFormModalEvent: function (cmp, event, helper) {
		var eventName = event.getParam('eventName');
		if (eventName === 'close') {
			helper.closeEditContactModal(cmp);
		} else if (eventName === 'save') {
			var contactId = event.getParam('contactId');
			var isNew = event.getParam('isNew');
			helper.retrieveContact(cmp, contactId, isNew);
		}
	},

	showAddNewContactRowTooltip: function (cmp, event, helper) {
		if (cmp.get('v.isAddNewContactRowButtonDisabled')) {
			let tooltip = cmp.find('addNewContactRowTooltip');
			$A.util.removeClass(tooltip, 'slds-transition-hide');
			$A.util.addClass(tooltip, 'slds-transition-show');
		}
	},

	hideAddNewContactRowTooltip: function (cmp, event, helper) {
		if (cmp.get('v.isAddNewContactRowButtonDisabled')) {
			let tooltip = cmp.find('addNewContactRowTooltip');
			$A.util.removeClass(tooltip, 'slds-transition-show');
			$A.util.addClass(tooltip, 'slds-transition-hide');
		}
	}
});
