({
	reloadContacts: function (cmp) {
		cmp.set('v.searchOffset', 0);
		cmp.set('v.searchMoreAvailable', false);
		cmp.set('v.contacts', []);
		cmp.get('v.contactsMap', {});
		this.retrieveContacts(cmp);
		cmp.set('v.draftValues', '[]');
		cmp.set('v.isAddNewContactRowButtonDisabled', false);
	},

	retrieveContacts: function (component, isMore) {
		var action = component.get('c.getContacts');
		var offset = component.get('v.searchOffset');
		var params = {
			accountId: component.get('v.selectedAccountId'),
			offsetString: offset + ''
		};
		action.setParams(params);

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				var retrievedContacts = response.getReturnValue();

				if (component.isValid()) {
					var limit = component.get('v.searchLimit');
					component.set('v.searchMoreAvailable', retrievedContacts.length === limit);

					var contacts = component.get('v.contacts');
					var contactsMap = component.get('v.contactsMap');

					for (var i = 0; i < retrievedContacts.length; i++) {
						var cont = retrievedContacts[i];
						contactsMap[cont.Id] = cont;

						if (cont.Account.Authorized_to_sign_1st__c === cont.Id) {
							cont.iconName = 'utility:user';
							cont.iconClass = 'slds-text-body_small slds-align_absolute-center';
							cont.iconMessage = 'Authorized to Sign 1st';
						} else if (cont.Account.Authorized_to_sign_2nd__c === cont.Id) {
							cont.iconName = 'utility:people';
							cont.iconClass = 'slds-text-body_small slds-align_absolute-center';
							cont.iconMessage = 'Authorized to Sign 2nd';
						} else if (!cont.Email || !cont.MobilePhone) {
							cont.iconName = 'utility:warning';
							cont.iconClass = 'warning-icon slds-text-body_small slds-align_absolute-center';
							cont.iconMessage = 'User cannot be used in OrderForm if Email and/or MobilePhone are missing';
						} else {
							cont.iconName = '';
							cont.iconClass = '';
							cont.iconMessage = '';
						}
					}

					contacts = this.getMapValues(contactsMap);
					component.set('v.searchOffset', contacts.length);
					component.set('v.contacts', contacts);
					component.set('v.contactsMap', contactsMap);

					var noContactsBox = component.find('no-contacts-found-box');
					if (contacts.length > 0) {
						$A.util.addClass(noContactsBox, 'slds-hide');

						var stepCompletedEvent = component.getEvent('stepCompletedEvent');
						stepCompletedEvent.setParam('stepName', 'contacts');
						stepCompletedEvent.fire();
					} else {
						$A.util.removeClass(noContactsBox, 'slds-hide');
					}

					this.closeEditContactModal(component);
				}
			} else if (state === 'INCOMPLETE') {
				console.error('state INCOMPLETE');
			} else if (state === 'ERROR') {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.error('Error message: ' + errors[0].message);
					}
				} else {
					console.error('Unknown error');
				}
			}

			if (component.isValid()) {
				component.set('v.showSpinner', false);
			}
		});
		component.set('v.showSpinner', true);
		$A.enqueueAction(action);
	},

	setColumns: function (cmp, actions) {
		cmp.set('v.columns', [
			{ label: 'FIRST NAME', fieldName: 'FirstName', type: 'text', editable: true },
			{ label: 'MIDDLE NAME', fieldName: 'MiddleName', type: 'text', editable: true },
			{ label: 'LAST NAME', fieldName: 'LastName', type: 'text', editable: true },
			{
				label: 'GENDER',
				fieldName: 'Gender__c',
				type: 'picklist',
				editable: true,
				typeAttributes: {
					placeholder: 'Choose gender',
					options: [
						{ label: 'Male', value: 'Male' },
						{ label: 'Female', value: 'Female' },
						{ label: 'M/F', value: 'M/F' },
						{ label: 'X', value: '.' }
					],
					value: { fieldName: 'Gender__c' },
					context: { fieldName: 'Id' },
					fieldName: 'Gender__c',
					label: 'Gender__c'
				}
			},
			{ label: 'JOB TITLE', fieldName: 'Title', type: 'text', editable: true },
			{ label: 'EMAIL', fieldName: 'Email', type: 'email', editable: true },
			{ label: 'PHONE', fieldName: 'Phone', type: 'phone', editable: true },
			{ label: 'MOBILE', fieldName: 'MobilePhone', type: 'phone', editable: true },
			{
				label: 'AUTHORIZED TO SIGN',
				type: 'button-icon',
				typeAttributes: {
					iconName: { fieldName: 'iconName' },
					name: { fieldName: 'iconName' },
					class: { fieldName: 'iconClass' },
					title: { fieldName: 'iconMessage' },
					disabled: false,
					variant: 'container'
				}
			},
			{ type: 'action', typeAttributes: { rowActions: actions, menuAlignment: 'auto' } }
		]);
	},

	getRowActions: function (row, doneCallback) {
		var actions = [{ label: 'Edit', name: 'edit' }];
		let contactRowId = row['Id'];
		let emailOrMobilePhoneMissing = !row['Email'] || !row['MobilePhone'];
		let authorizedToSignFirstId = row['Account']['Authorized_to_sign_1st__c'];
		let authorizedToSignSecondtId = row['Account']['Authorized_to_sign_2nd__c'];

		actions.push({
			label: 'Authorize to Sign 1st',
			name: 'authorizeToSignFirst',
			disabled: emailOrMobilePhoneMissing || authorizedToSignFirstId === contactRowId
		});

		actions.push({
			label: 'Authorize to Sign 2nd',
			name: 'authorizeToSignSecond',
			disabled: emailOrMobilePhoneMissing || authorizedToSignSecondtId === contactRowId
		});

		if (authorizedToSignFirstId === contactRowId || authorizedToSignSecondtId === contactRowId) {
			actions.push({ label: 'Remove Authorized to Sign', name: 'removeAuthorizeToSign' });
		}

		setTimeout(
			$A.getCallback(function () {
				doneCallback(actions);
			}),
			200
		);
	},

	retrieveContact: function (component, contactId, isNew) {
		var action = component.get('c.getContact');
		action.setParams({ contactId: contactId });

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === 'SUCCESS' && component.isValid()) {
				var contact = response.getReturnValue();
				console.log('##contact:', contact);
				var contacts = component.get('v.contacts');
				var contactsMap = component.get('v.contactsMap');

				contactsMap[contact.Id] = contact;
				contacts = this.getMapValues(contactsMap);

				component.set('v.contacts', contacts);
				component.set('v.contactsMap', contactsMap);

				this.closeEditContactModal(component);

				var toastEvent = $A.get('e.force:showToast');
				toastEvent.setParams({
					title: 'Success!',
					type: 'success',
					message: 'The record has been updated successfully.'
				});
				toastEvent.fire();
			} else if (state === 'INCOMPLETE') {
				console.error('state INCOMPLETE');
			} else if (state === 'ERROR') {
				var errors = response.getError();
				if (errors) {
					if (errors[0] && errors[0].message) {
						console.error('Error message: ' + errors[0].message);
					}
				} else {
					console.error('Unknown error');
				}
			}

			if (component.isValid()) {
				component.set('v.showSpinner', false);
			}
		});
		component.set('v.showSpinner', true);
		$A.enqueueAction(action);
	},

	editRecord: function (cmp, recordId) {
		var editRecordEvent = $A.get('e.force:editRecord');
		editRecordEvent.setParams({
			recordId: recordId
		});
		editRecordEvent.fire();
	},

	authorizeToSignFirst: function (cmp, accountId, contactId) {
		try {
			var action = cmp.get('c.authorizeToSignFirst');

			var params = {
				accountId: accountId,
				contactId: contactId
			};
			action.setParams(params);

			action.setCallback(this, function (response) {
				cmp.set('v.showSpinner', false);

				var state = response.getState();
				if (state === 'SUCCESS') {
					var result = response.getReturnValue();

					if (result.success) {
						this.toastMessage('Success!', 'Record Updated', 'success');
						this.reloadContacts(cmp);
					} else {
						this.toastMessage('Error!', result.message.message, 'error');
					}
				} else if (state === 'INCOMPLETE') {
					console.error('state INCOMPLETE');
				} else if (state === 'ERROR') {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.error('Error message: ' + errors[0].message);
						}
					} else {
						console.error('Unknown error');
					}
				}
			});
			cmp.set('v.showSpinner', true);
			$A.enqueueAction(action);
		} catch (exc) {
			console.error(exc);
		}
	},

	authorizeToSignSecond: function (cmp, accountId, contactId) {
		try {
			var action = cmp.get('c.authorizeToSignSecond');

			var params = {
				accountId: accountId,
				contactId: contactId
			};
			action.setParams(params);

			action.setCallback(this, function (response) {
				cmp.set('v.showSpinner', false);

				var state = response.getState();
				if (state === 'SUCCESS') {
					var result = response.getReturnValue();

					if (result.success) {
						this.toastMessage('Success!', 'Record Updated', 'success');
						this.reloadContacts(cmp);
					} else {
						this.toastMessage('Error!', result.message.message, 'error');
					}
				} else if (state === 'INCOMPLETE') {
					console.error('state INCOMPLETE');
				} else if (state === 'ERROR') {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.error('Error message: ' + errors[0].message);
						}
					} else {
						console.error('Unknown error');
					}
				}
			});
			cmp.set('v.showSpinner', true);
			$A.enqueueAction(action);
		} catch (exc) {
			console.error(exc);
		}
	},

	removeAuthorizeToSign: function (cmp, accountId, contactId) {
		try {
			var action = cmp.get('c.removeAuthorizeToSign');

			var params = {
				accountId: accountId,
				contactId: contactId
			};
			action.setParams(params);

			action.setCallback(this, function (response) {
				cmp.set('v.showSpinner', false);

				var state = response.getState();
				if (state === 'SUCCESS') {
					var result = response.getReturnValue();

					if (result.success) {
						this.toastMessage('Success!', 'Record Updated', 'success');
						this.reloadContacts(cmp);
					} else {
						this.toastMessage('Error!', result.message.message, 'error');
					}
				} else if (state === 'INCOMPLETE') {
					console.error('state INCOMPLETE');
				} else if (state === 'ERROR') {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.error('Error message: ' + errors[0].message);
						}
					} else {
						console.error('Unknown error');
					}
				}
			});
			cmp.set('v.showSpinner', true);
			$A.enqueueAction(action);
		} catch (exc) {
			console.error(exc);
		}
	},

	addNewContactRow: function (cmp) {
		let contactList = cmp.get('v.contacts');
		let accountId = cmp.get('v.selectedAccountId');

		contactList.push({
			sobjectType: 'Contact',
			Id: '',
			AccountId: accountId,
			FirstName: '',
			MiddleName: '',
			LastName: '',
			Gender__c: '.',
			Title: '',
			Email: '',
			Phone: '',
			MobilePhone: ''
		});

		cmp.set('v.contacts', contactList);
		cmp.set('v.draftValues', '[{"Id":"row-0","Gender__c":".","Salutation":""}]');
		cmp.set('v.isAddNewContactRowButtonDisabled', true);
	},

	openEditContactModal: function (cmp, contactId) {
		console.log(':::openEditContactModal');
		cmp.set('v.selectedContactId', contactId);
		cmp.set('v.showContactForm', true);
	},

	closeEditContactModal: function (cmp) {
		cmp.set('v.showContactForm', false);
	},

	getMapValues: function (mapData) {
		var values = [];
		for (var key in mapData) {
			if (mapData.hasOwnProperty(key)) {
				values.push(mapData[key]);
			}
		}
		return values;
	},

	toastMessage: function (title, message, type) {
		var toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
			title: title,
			message: message,
			type: type
		});
		toastEvent.fire();
	}
});
