/**
 * Created by nikola.culej on 4.6.2020..
 */

({
  init: function (cmp, event, helper) {
    var recordFormMode = "view";
    var componentReadOnly = cmp.get("v.componentReadOnly");
    if (componentReadOnly) {
      recordFormMode = "readonly";
    }
    cmp.set("v.recordFormMode", recordFormMode);
    cmp.set("v.columns", [
      { label: "Site", fieldName: "SiteAddress", type: "text", wrapText: true },
      {
        label: "Technical Contact",
        type: "button",
        typeAttributes: {
          iconName: "utility:edit",
          name: "edit_technical_contact",
          title: "Edit Technical Contact",
          variant: "base",
          iconPosition: "right",
          label: { fieldName: "TechnicalContactName" }
        }
      },
      { label: "Products", fieldName: "Products__c", type: "text", wrapText: true },
      {
        label: "Wish Date",
        fieldName: "Wish_Date__c",
        type: "date",
        typeAttributes: {
          day: "numeric",
          month: "numeric",
          year: "numeric"
        }
      },
      {
        label: "Start Delivery",
        fieldName: "Start_Delivery__c",
        type: "boolean",
        editable: !componentReadOnly
      },
      {
        label: "Suppress Automated Communication",
        fieldName: "Suppress_Automated_Communication__c",
        type: "boolean",
        editable: !componentReadOnly
      }
    ]);
    helper.getData(cmp);
  },
  handleSaveEdition: function (cmp, event, helper) {
    var draftValues = event.getParam("draftValues");

    helper.saveEdition(cmp, draftValues);
  },
  handleTechContactOnSuccess: function (component, event, helper) {
    component.set("v.editSolution", false);
    helper.getData(component);
  },
  handleRowAction: function (component, event, helper) {
    var action = event.getParam("action");
    var solution = event.getParam("row");
    switch (action.name) {
      case "edit_technical_contact":
        component.set("v.selectedSolutionId", solution.Id);
        component.set("v.selectedSolutionName", solution.Name);
        break;
      default:
        component.set("v.selectedSolutionId", solution.Id);
        component.set("v.selectedSolutionName", solution.Name);
        break;
    }
    if (component.get("v.selectedSolutionId")) {
      component.set("v.editSolution", true);
    }
  },
  closeModal: function(component, event, helper) {
    component.set("v.editSolution", false);
 }
});