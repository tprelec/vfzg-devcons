/**
 * Created by nikola.culej on 4.6.2020..
 */

({
    getData: function (cmp) {
        var recordId = cmp.get('v.recordId');

        var action = cmp.get('c.getSolutionList');
        action.setParams({
            "recordId": recordId
        })

        action.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var rows = response.getReturnValue();
                for (var i = 0; i < rows.length; i++) {
                    var row = rows[i];
                    //as data columns with relationship __r can not be displayed directly in data table, so generating dynamic columns
                    if (row.Site__r) {
                        row.SiteAddress = row.Site__r.Unify_Site_Name__c;
                    }
                    if (row.Technical_Contact__r) {
                        row.TechnicalContactName = row.Technical_Contact__r.Name;
                    }
                }
                cmp.set('v.data', rows);
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(action);
    },
    saveEdition: function (cmp, draftValues) {
        var self = this;
        var saveAction = cmp.get('c.updateSolutions');
        saveAction.setParams({
            "records": draftValues
        });

        saveAction.setCallback(this, $A.getCallback(function (response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var errors = [];
                var returnValue = JSON.parse(response.getReturnValue());

                for (var index = 0; index < returnValue.length; index++) {
                    if (returnValue[index].errors.length > 0) {
                        errors.push(returnValue[index].errors);
                    }
                }

                if (errors.length > 0) {
                    // the draft values have some errors, setting them will show it on the table
                    cmp.set('v.errors', errors);
                } else {
                    // Yay! success, initialize everything back
                    cmp.set('v.errors', []);
                    cmp.set('v.draftValues', []);
                    self.getData(cmp);
                }
            } else if (state === "ERROR") {
                var errors = response.getError();
                console.error(errors);
            }
        }));
        $A.enqueueAction(saveAction);
    }
});