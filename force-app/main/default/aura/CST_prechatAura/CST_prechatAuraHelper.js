({
	/**
	 * Create an HTML input element, set necessary attributes, add the element to the DOM.
	 *
	 * @param inputField - Email prechat field object with attributes needed to render.
	 */
	renderField: function (inputField, parent) {
		// Dynamically create input HTML element.
		var input = document.createElement('input');
		console.log('inputField ', inputField);

		// Set general attributes.
		input.type = inputField.type;
		input.class = inputField.label;
		input.placeholder = inputField.label;

		// Set attributes required for starting a chat.
		input.name = inputField.name;
		input.label = inputField.label;
		input.value = inputField.value;

		// Add email input to the DOM.

		parent.appendChild(input);
		// document.querySelector(".prechatFields").appendChild(input);
	},

	/**
	 * Create an array of data to pass to the prechatAPI component's startChat function.
	 */
	createStartChatDataArray: function () {
		console.log('starting createStartChatDataArray...');
		var inputs = document.querySelector('.prechatFields').childNodes;
		console.log('logging input -> ' + inputs);
		var infos = [];
		for (var i = 0; i < inputs.length; i++) {
			console.log('inputs[i].name -> ' + inputs[i].name);
			console.log('inputs[i].label -> ' + inputs[i].label);
			console.log('inputs[i].value -> ' + inputs[i].value);
			var info = {
				name: inputs[i].name,
				label: inputs[i].label,
				value: inputs[i].value
			};
			infos.push(info);
		}
		return infos;
	}
});
