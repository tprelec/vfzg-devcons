({
	/**
	 * After this component has rendered, create an email input field.
	 *
	 * @param component - This prechat UI component.
	 * @param event - The Aura event.
	 * @param helper - This component's helper.
	 */
	onRender: function (component, event, helper) {
		// Get array of prechat fields defined in setup using the prechatAPI component.
		var prechatFields = component.find('prechatAPI').getPrechatFields();

		console.log('prechatFields -> ' + JSON.stringify(prechatFields));
		// Append an input element to the prechatForm div.
		// helper.renderEmailField(emailField);
		var prechatForm = document.querySelector('.prechatFields');

		for (let i = 0; i < prechatFields.length; i++) {
			console.log(prechatFields[i]);
			helper.renderField(prechatFields[i], prechatForm);
		}
		console.log('prechatFields', prechatForm);

		console.log('before startchatDataArray ');
		var prechatInfo = helper.createStartChatDataArray();
		console.log('pre chat info -> ' + prechatInfo);
		//*
		if (component.find('prechatAPI').validateFields(prechatInfo).valid) {
			console.log('validate Fields returned true  ');

			component.find('prechatAPI').startChat(prechatInfo);
		} else {
			// Show some error.
		}
		//*/
	},

	/**
	 * Handler for when the start chat button is clicked.
	 *
	 * @param component - This prechat UI component.
	 * @param event - The Aura event.
	 * @param helper - This component's helper.
	 */
	onStartButtonClick: function (component, event, helper) {
		console.log('before startchatDataArray ');
		/*
		var prechatInfo = helper.createStartChatDataArray();
		console.log('pre chat info -> '+prechatInfo);
		
		if(component.find("prechatAPI").validateFields(prechatInfo).valid) {
            console.log('validate Fields returned true  ');
			component.find("prechatAPI").startChat(prechatInfo);
		} else {
			// Show some error.
		}
		*/
	}
});
