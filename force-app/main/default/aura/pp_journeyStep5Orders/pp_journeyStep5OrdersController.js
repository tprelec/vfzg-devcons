({
    onInit: function (cmp, event, helper) {
        var contract = cmp.get("v.contract");
        var contractId = contract.Id;
        cmp.set("v.contractId", contractId);
    },


    onIframeLoadStart: function (cmp, event, helper) {
        console.log(':::321');
    },


    onIframeLoaded: function (cmp, event, helper) {
        console.log(':::123');
    },


    onCurrentStepChange: function (cmp, event, helper) {
        var currentStep = event.getParam("value");
        var orders = cmp.get("v.orders");
        console.log(orders.length < 1);
        if (currentStep === 'order' && orders.length < 1) {
            helper.retrieveOrders(cmp);
        }
    }
})