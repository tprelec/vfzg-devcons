({

    doInit : function(component) {
        var action = component.get("c.getLightningBanValues");
        var accId = component.get("v.recordId");
        action.setParams({ accId: accId });
        var opts=[];
        action.setCallback(this, function(data) {
            if (data.getState() == "SUCCESS") {
                var bans = data.getReturnValue();
                component.set("v.picklistValues", bans);
            }

        });
        $A.enqueueAction(action);
    },
    handleLoad: function(component, event, helper) {

    },
    onRecordSubmit: function(component, event, helper) {
        event.preventDefault(); // stop form submission
        var banId = component.get("v.banId");
        console.log('##banId : '+banId );
        var eventFields = event.getParam("fields");
        eventFields["Customer_BAN__c"] = banId;
        event.setParam("fields", eventFields);
        component.find('cnForm').submit(eventFields);
    },
    handleOnError : function(component, event, helper) {

    },

    handleSuccess : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The Credit Note has been created.",
            "duration":' 5000',
            "key": "info_alt",
            "type": "success",
            "mode": "pester"
        });
        toastEvent.fire();
        $A.get('e.force:refreshView').fire();


    },
    cancel : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }
})