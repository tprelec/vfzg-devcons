({
    doInit : function(component,event,helper) {
        var recordId = component.get('v.recordId');

        var action = component.get("c.getActivityTimeline");
        action.setParams({
            "recordId" : recordId
        })
        action.setCallback(this, function(a) {
            component.set("v.timeLineItems", a.getReturnValue().items);
            component.set("v.timeLineGroups", a.getReturnValue().groups);
        });
        $A.enqueueAction(action);
    }
});