({
  init : function (component, event, helper) {

    var action = component.get("c.getRecordTypes");
    action.setCallback(this, function(response) {
      var mapOfRecordTypes = response.getReturnValue();
      component.set("v.options", mapOfRecordTypes);
      component.set("v.selectedValue", mapOfRecordTypes[0].value);
    });

    $A.enqueueAction(action);

  },

  pressbutton : function (component, event, helper) {
    component.set("v.goToFlow", true);
    var flow = component.find("flowData");
    var inputVariables = [
      {
          name : 'recordTypeId',
          type : 'String',
          value : component.get("v.selectedValue")
      },
      {
          name : 'accountId',
          type : 'String',
          value : component.get("v.recordId")
      }
      ];
    flow.startFlow("New_Opportunity", inputVariables);

  }
})