/**
 * Created by nikola.culej on 28.5.2020..
 */

({
    handleCancelClick : function(component, event, helper) {
        component.destroy();
    },

    handleOkClick : function(component, event, helper) {

        //fire the confirmation event
        var confirmEvent = component.getEvent("confirm");
        confirmEvent.setParam("param", component.get("v.param"));
        confirmEvent.fire();

        //destroy the component
        component.destroy();
    },
    handleEvent : function(component, event, helper){
        //destroy the component
        debugger;
        component.destroy();
    }
});