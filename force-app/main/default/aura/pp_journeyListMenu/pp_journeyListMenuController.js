({
    onInit: function(component, event, helper) {
        console.log(':::menu onInit');

        
    },

    onClick : function(component, event, helper) {
        console.log(':::menu onClick');
        var id = event.target.dataset.menuItemId;
        console.log(':::menu onClick; id = ' + id);
        if (id) {
            component.getSuper().navigate( id );
        }
    }
})