/**
 * Created by nikola.culej on 28.5.2020..
 */

({
    doInit : function(component,event,helper){
    },

    onClick:function (component,event,handler) {
        var recordId = component.get('v.recordId');

        var action = component.get("c.closeCase");
        action.setParams({
            "recordId" : recordId
        })

        // set call back instructions
        action.setCallback(this, function(a){
            // assign server retrieved items to component variable
            var response = a.getReturnValue();
        });

        // queue action on the server
        $A.enqueueAction(action);
    },
    showConfirmation : function(cmp) {
        $A.createComponent(
            "c:CS_COMModalConfirmation",
            {
                "title": "Close Task",
                "tagline": "",
                "message": "Are you sure you want to change the status of the Task to Closed?",
                "confirm": cmp.getReference("c.handleConfirm"),
                "param": cmp.get("v.recordId")
            },
            function(modalWindow, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(modalWindow);
                    cmp.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    handleConfirm : function(cmp, event, helper) {
        //retrieve the value of the parameter
        var recordId = cmp.get('v.recordId');

        var action = cmp.get("c.closeCase");
        action.setParams({
            "recordId" : recordId
        })

        // set call back instructions
        action.setCallback(this, function(a){
            // assign server retrieved items to component variable
            if(a.getState() == 'SUCCESS'){
                var result = a.getReturnValue().split('FIELD_CUSTOM_VALIDATION_EXCEPTION, ').pop();
                if(result !== ''){
                    helper.errorMessage(result);
                } else {
                    helper.successMessage('Task Closed!');
                }
            }

            $A.get('e.force:refreshView').fire();
        });

        // queue action on the server
        $A.enqueueAction(action);
    }
});