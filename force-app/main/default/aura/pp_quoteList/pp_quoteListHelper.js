({
    getQuotes: function(cmp, opportunityId) {
        try {
            var action = cmp.get("c.getQuotes");
            action.setParam('opportunityId', opportunityId);

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    //console.log(response.getReturnValue());

                    var quotes = response.getReturnValue();
                    cmp.set('v.quotes', quotes);
                }
                else if ("INCOMPLETE" === state) {
                    console.error("Network Error / Server is Down");
                }
                else if ("ERROR" === state) {
                    var message = "Response State is ERROR; Unknown error";
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        message = "Error message: " + errors[0].message;
                    }
                    console.error(message);
                }

                if (cmp.isValid()) {
                    cmp.set('v.showSpinner', false);
                    this.getOracleSiteId(cmp, false);
                }
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    getOracleSiteId: function(cmp, showIFrame) {
        try {
            var action = cmp.get("c.getOracleSiteId");

            cmp.set("v.iframeError", false);

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {
                    var oracleSiteId = response.getReturnValue();

                    if (!oracleSiteId) {
                        cmp.set("v.iframeError", true);
                    }
                    else {
                        cmp.set('v.oracleSiteId', oracleSiteId);

                        this.getIframeURL(cmp);

                        if (showIFrame) {
                            cmp.set("v.iframeHeight", '1000px');
                            cmp.set('v.showIframe', true);
                        }
                    }
                }
                else if ("INCOMPLETE" === state) {
                    console.error("Network Error / Server is Down");
                }
                else if ("ERROR" === state) {
                    var message = "Response State is ERROR; Unknown error";
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        message = "Error message: " + errors[0].message;
                    }
                    console.error(message);
                }
                if (cmp.isValid()) {
                    cmp.set('v.showSpinner', false);
                }
            });

            cmp.set('v.showSpinner', true);
            $A.enqueueAction(action);
        }
        catch (exc) {
            console.error(exc);
        }
    },


    /***
     * IFrame url for new Quote creation
     */
    getIframeURL: function (cmp) {
        var url = $A.get("$Label.c.pp_Community_Base_URL") + '/apex/BigMachines__QuoteEdit?actId='
                    + cmp.get("v.accountId") + '&siteId=' + cmp.get("v.oracleSiteId")
                    + '&oppId=' + cmp.get("v.opportunityId") + '&isdtp=lt';
        cmp.set("v.iframeHeight", '1000px');
        cmp.set("v.iframeURL", url);
    },


    /***
     * IFrame url for new Quote edit
     */
    setIframeEditURL: function (cmp, quoteId, showIFrame) {
        var url = $A.get("$Label.c.pp_Community_Base_URL")
                        + '/apex/BigMachines__QuoteEdit?id=' + quoteId + '&isdtp=lt';
        cmp.set("v.iframeURL", url);
        
        if (showIFrame) {
            cmp.set("v.iframeHeight", '1000px');
            cmp.set('v.showIframe', true);
        }
    },


    /***
     * IFrame url to make Quote primary
     */
    setIframePrimaryURL: function (cmp, quoteId, showIFrame) {
        var url = $A.get("$Label.c.pp_Community_Base_URL")
            + '/apex/BigMachines__QuoteSetAsPrimary?id=' + quoteId + '&isdtp=lt';
        cmp.set("v.iframeURL", url);

        if (showIFrame) {
            cmp.set("v.iframeHeight", '150px');
            cmp.set('v.showIframe', true);
        }
    }
})