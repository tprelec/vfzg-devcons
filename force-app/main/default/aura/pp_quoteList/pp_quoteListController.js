({
    onInit: function (cmp, event, helper) {
        var oppId = cmp.get('v.opportunityId');

        helper.getQuotes(cmp, oppId);
    },

    refreshList: function(cmp, event, helper) {
        var oppId = cmp.get('v.opportunityId');
        helper.getQuotes(cmp, oppId);
    },

    onOpportunityIdChange: function (cmp, event, helper) {
        var oppId = event.getParam('value');

        helper.getQuotes(cmp, oppId);

    },

    createQuote: function (cmp, event, helper) {
        //console.log(':::createQuote quoteList');
       //helper.getOracleSiteId(cmp);

        if (!cmp.get('v.oracleSiteId')) {
            helper.getOracleSiteId(cmp, true);
        }
        else {
            helper.getIframeURL(cmp);
            cmp.set('v.showIframe', true);
        }
    },


    hideIframe: function (cmp, event, helper) {
        cmp.set('v.showIframe', false);
        cmp.set('v.iframeError', false);
        cmp.set("v.iframeURL", "");

        var oppId = cmp.get('v.opportunityId');
        helper.getQuotes(cmp, oppId);
    },


    handleQuoteMenuSelect: function (cmp, event, helper) {
        try {
            var selectedMenuItemValue = event.getParam("value");
            var quoteId = event.getSource().get('v.value');

            if ('edit' === selectedMenuItemValue) {
                cmp.set("v.iframeURL", "");
                helper.setIframeEditURL(cmp, quoteId, true);
            }
            else if ('set-primary' === selectedMenuItemValue) {
                cmp.set("v.iframeURL", "");
                helper.setIframePrimaryURL(cmp, quoteId, true);
            }
        }
        catch (exc) {
            console.log(exc);
        }
    }
})