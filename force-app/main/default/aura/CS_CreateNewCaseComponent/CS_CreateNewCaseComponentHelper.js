({
	successfulCaseCreation : function() {
		iziToast.success({
              title: 'Success',
              message: 'Case created successfully',
              position: 'topCenter',
              progressBar: false
            });
	},
	
	errorMessage : function(messageError) {

    	iziToast.error({
              title: 'Error',
              message: messageError,
              position: 'topCenter',
              progressBar: false
            });
    },
})