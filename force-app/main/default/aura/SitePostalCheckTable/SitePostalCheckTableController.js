({
	doInit : function(component, event, helper) {

        component.set("v.selectAllVal", false);

        console.log('Got value '+JSON.stringify(component.get("v.selectAllVal")));

		var currentlySelectedSites = {};
		currentlySelectedSites['sites'] = [];
		component.set("v.selectedSites",currentlySelectedSites);

		var	pcrId = component.get("v.pcrId");

		console.log('got components');
		var action = component.get("c.getSiteAddresses");


		console.log('PCRID =' +pcrId);
		
		action.setParams({pcrId : pcrId});

		action.setCallback(this, function(response) {
			console.log('callback');	
			var state = response.getState();
			if (state === "SUCCESS") {		
				console.log('SUCCESS');	
				var sites = response.getReturnValue();
				component.set("v.siteRecords", sites.sites);
				component.set("v.fields", sites.fields);
                component.set("v.referencePCConfigId", sites.referencePCConfigId);
                component.set("v.packageConfig", sites.packageConfig);

                component.set("v.selectedAvailableSiteId", sites.selectedAvailableSiteId)

				var originSite;

				var allSites = sites.sites;
				for(var x in allSites){
					if(allSites[x].siteId == sites.selectedAvailableSiteId){
						originSite = allSites[x];
					}
				}
				component.set("v.siteFromOrigin", originSite);

                console.log('Origin site=');    
				console.log(originSite);
				
                console.log('All sites=');  
				console.log(sites);

                console.log('Package config=');  
                console.log(component.get("v.packageConfig"));
			}
			else {
				console.log('FAIL');
				console.log('No sites found.');
			}
		});
		$A.enqueueAction(action);
	},

	handleSiteItemSelected: function(component, event, helper) {
        var sitesToAdd = event.getParam("selectedSiteItem");
        var sitesToRemove = event.getParam("unSelectedSiteItem");

        var currentlySelectedSites = component.get("v.selectedSites");
        
        currentlySelectedSites = JSON.parse(JSON.stringify(currentlySelectedSites));

        //console.log('Sites to add==');
        //console.log(sitesToAdd);

        //console.log('Sites to remove==');
        //console.log(sitesToRemove);

        if(sitesToAdd!=""){

        	console.log('Adding siteItem :'+sitesToAdd);
            
            if(!currentlySelectedSites){
            	console.log('Initialising');
            	currentlySelectedSites = {};
                currentlySelectedSites['sites'] = [];
            }
            var oneSite = sitesToAdd;
            //oneSite.saId = {};

            /*
        	oneSite.saId = {};
            oneSite.saId = sitesToAdd;
            oneSite.rootPCR = helper.findRootPCRForSiteId(sitesToAdd, component);
            */

            currentlySelectedSites['sites'].push(oneSite);
        }

        
        if(sitesToRemove!=""){
        	console.log('Removing siteItem :'+sitesToRemove);
            if(currentlySelectedSites['sites']){
                var indexToRemove;
                for(var sIdx in currentlySelectedSites['sites']){
                    if(currentlySelectedSites['sites'][sIdx].saId == sitesToRemove){
                        indexToRemove = sIdx;
                    }
                }
                if(indexToRemove ==0){
                    currentlySelectedSites['sites'] = [];
                }
                else{
                    currentlySelectedSites['sites'].splice(indexToRemove,1);
                }
            }
        }

        component.set("v.selectedSites",currentlySelectedSites);

        console.log('Currently selected sites:');
        console.log(JSON.stringify(currentlySelectedSites));


        var basketId = component.get("v.basketId");
        var pcrId = component.get("v.pcrId");
        var sites = component.get("v.selectedSites");

        var referenceAccessId = component.get("v.referencePCConfigId");

        var pcrsForDeletion = {};

        console.log('Sites '+JSON.stringify(sites));

        var sitesToSend = {};
        for(var s in sites.sites){
            sitesToSend[sites.sites[s].saId] = 'true';
        }

        console.log('Params sent  ->Selected sites =='+JSON.stringify(sitesToSend));
        

        console.log('Params sent referenceAccessId='+referenceAccessId);//if(isOverride==true){
        console.log('Params sent pcrId='+pcrId);
        console.log('Params sent basketId='+basketId);

    },
    applySelectAllSelect: function(component, event, helper) {
        console.log('TABLE ---- select all event');

        var currentSelectAllVal = component.get("v.selectAllValue");

        
       
        //component.set("v.selectAllValue", currentSelectAllVal);
        console.log('SELECT ALL -- table controller =='+currentSelectAllVal);

    }
})