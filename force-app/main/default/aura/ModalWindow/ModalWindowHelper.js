({
	deleteIfOverride : function() {
		
	},

	successMessage : function() {

    	iziToast.success({
              title: 'Success',
              message: 'Cloning started.',
              position: 'topCenter',
              progressBar: false
            });
    },
    errorMessage : function() {

    	iziToast.error({
              title: 'Error',
              message: 'Cloning failed. Please try again',
              position: 'topCenter',
              progressBar: false
            });
    },
    advanceClone : function(component, isOverride) {

		var basketId = component.get("v.basketId");
		var pcrId = component.get("v.pcrId");
		var sites = component.get("v.selectedSites");

		var referenceAccessId = component.get("v.referencePCConfigId");
		var packageConfig = component.get("v.packageConfig");

		console.log('Package config in MODAL =='+packageConfig);

		var originSiteId = component.get("v.configOrigin");
		console.log('originSiteId == '+originSiteId.siteId);

		var originPBX = '';
		var pcrsForDeletion = {};

		console.log('THIS IS OVERIDE == '+isOverride);


		console.log('Sites '+JSON.stringify(sites));

		var sitesToSend = {};
		for(var s in sites.sites){

			console.log('NEW = '+JSON.stringify(sites.sites[s].idSelected));
			var keySite = sites.sites[s].idSelected;
			//sitesToSend[keySite] = '';
			sitesToSend[keySite] = sites.sites[s].pbx;

			if(JSON.stringify(sites.sites[s].rootPCRSelected)){
				sitesToSend[keySite] = '';
				console.log('Had root = '+ JSON.stringify(sites.sites[s].idSelected)+'--end--');

				if(isOverride == true){
					pcrsForDeletion[sites.sites[s].rootPCRSelected] = true;
				}
			}
			else{
				console.log('Does NOT =='+JSON.stringify(sites.sites[s].idSelected));
			}
		}

		console.log('Params sent  ->Selected sites =='+JSON.stringify(sitesToSend));
		

		console.log('Params sent referenceAccessId='+referenceAccessId);//if(isOverride==true){

		console.log('Params sent referenceAccessId='+packageConfig);
		console.log('Params sent pcrId='+pcrId);
		console.log('Params sent basketId='+basketId);
		console.log('Params sent pcrsForDeletion='+JSON.stringify(pcrsForDeletion));
		//}

		
		
		var action = component.get("c.performAdvanceClone");
		action.setParams({sites: sitesToSend, basketId : basketId, pcrId:pcrId, referenceAccessId:referenceAccessId, originSiteId: originSiteId.siteId, originPBX: originSiteId.pbx,packageConfigId: packageConfig});
		action.setCallback(this, function(response) {
		  var state = response.getState();
		  if (state === "SUCCESS") {   
            this.successMessage();

		     //window.location.replace('/'+component.get("v.basketId"));
		     
            var basketId = component.get("v.basketId");
            
            if(window.location.href.indexOf("partnerportal") > -1) {
                window.open('/partnerportal/' + basketId,'_top');
                     //window.location.replace('/partnerportal/apex/ProposalPage?Id='+basket.Id);
                     
            } else {
                 //window.location.replace('/apex/ProposalPage?Id='+basket.Id);
                window.open('/' + basketId,'_top');
            }
		     
		  }
		  else {
		      console.log('Cloning failed.');
		      this.errorMessage();
		      
		  }
		})
		$A.enqueueAction(action); 
		
		
	}
})