({
	helperMethod: function () {},
	getButtonVisibility: function (component) {
		var action = component.get('c.showComponent');
		action.setParams({
			recordId: component.get('v.recordId')
		});

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state == 'SUCCESS') {
				console.log('status', response.getReturnValue());
				if (response.getReturnValue() == 'Waiting for Customer Validation') {
					console.log('status1', response.getReturnValue());
					component.set('v.showComponent', true);
					component.set('v.showConfirm', true);
					component.set('v.showClose', false);
				} else if (response.getReturnValue() == 'Closed') {
					console.log('status2', response.getReturnValue());
					component.set('v.showComponent', false);
				} else {
					console.log('status2', response.getReturnValue());
					component.set('v.showComponent', true);
					component.set('v.showConfirm', false);
					component.set('v.showClose', true);
				}
			}
		});
		$A.enqueueAction(action);
	},
	fireToastEvent: function (title, message, type) {
		var toast = $A.get('e.force:showToast');
		toast.setParams({
			title: title,
			message: message,
			type: type
		});

		$A.get('e.force:refreshView').fire();
		toast.fire();
	}
});
