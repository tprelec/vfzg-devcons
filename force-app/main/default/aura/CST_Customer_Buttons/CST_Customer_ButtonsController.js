({
	doInit: function (component, event, helper) {
		helper.getButtonVisibility(component);
	},
	ConfirmCase: function (component, event, helper) {
		var recordId = component.get('v.recordId');
		var action = component.get('c.updateCloseCase');
		action.setParams({
			recordId: recordId
		});

		action.setCallback(this, function (response) {
			console.log('ConfirmCase', response.getState());
			if (response.getState() === 'SUCCESS') {
				helper.fireToastEvent('Solution validated!', 'Case is now closed.', 'info');
			}
		});

		$A.enqueueAction(action);
	},
	DoNotConfirmSolution: function (component, event, helper) {
		var recordId = component.get('v.recordId');
		var action = component.get('c.updateCaseToInProgress');
		action.setParams({
			recordId: recordId
		});

		action.setCallback(this, function (response) {
			if (response.getState() === 'SUCCESS') {
				helper.fireToastEvent('Solution not validated!', 'Case will be reopened.', 'info');
			}
		});

		$A.enqueueAction(action);
	},

	BeforeCaseClose: function (component, event, helper) {
		var recordId = component.get('v.recordId');
		component.set('v.isModalOpen', true);
	},

	CloseModal: function (component, event, helper) {
		// Set isModalOpen attribute to false
		component.set('v.isModalOpen', false);
	},

	SubmitDetails: function (component, event, helper) {
		var recordId = component.get('v.recordId');
		component.set('v.isModalOpen', false);
		var action = component.get('c.caseClosed');
		action.setParams({
			recordId: recordId,
			caseReason: component.find('select').get('v.value')
		});

		action.setCallback(this, function (response) {
			console.log('CaseClosed', response.getState());

			if (response.getState() === 'SUCCESS') {
				helper.fireToastEvent('Case Closed Sucessfully!', 'Case is now closed.', 'info');
			}
		});

		$A.enqueueAction(action);
	}

	/*onChange: function (cmp, evt, helper) {
        alert(cmp.find('select').get('v.value'));
        console.log("value", cmp.find('select').get('v.value'));
    }*/
});
