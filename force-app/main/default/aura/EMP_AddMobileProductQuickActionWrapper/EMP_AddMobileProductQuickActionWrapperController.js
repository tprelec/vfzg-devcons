({
    closeQA : function(component, event, helper) {
      $A.get("e.force:closeQuickAction").fire();
    },
    handleSave : function(component, event, helper) {
      component.find('emp_AddMobileProductByFWATemplate').selectSaveOLI();
    },
    handleClose : function(component, event, helper) {
      component.find('emp_AddMobileProductByFWATemplate').close();
    },
    disableSave : function(component, event, helper) {
      let isdisabled = component.get('v.disabled');
      component.set('v.disabled',!isdisabled);
    }
})