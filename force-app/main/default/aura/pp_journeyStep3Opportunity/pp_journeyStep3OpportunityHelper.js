({
	openCloseWonFrame: function (cmp) {
		cmp.set('v.showCloseWon', true);
	},

	closeCloseWonFrame: function (cmp) {
		//console.log(cmp.isValid());
		cmp.set('v.showCloseWon', false);

		var opptyId = cmp.get('v.opportunityId');
		if (!opptyId) {
			return;
		}

		this.retrieveOppty(cmp, opptyId);
	},

	retrieveOppty: function (cmp, opptyId) {
		try {
			let opp = cmp.get('v.opportunity');
			let opptyStepCompleted = false;
			let contract = opp.Contract_VF__r;
			if (contract) {
				cmp.set('v.contract', contract);
				opptyStepCompleted = true;
			}

			if (opptyStepCompleted) {
				let stepCompletedEvent = cmp.getEvent('stepCompletedEvent');
				stepCompletedEvent.setParam('stepName', 'opportunity');
				stepCompletedEvent.setParam('moveNext', !!contract);
				stepCompletedEvent.fire();
			}

			let showBasketList = opp.Select_Journey__c === 'Sales' && !recordTypeIsMac;
			cmp.set('v.showBasketList', showBasketList);

			// TODO:
			// 1) add Closed status processing: Lost - red, Won - go to next
			// 2) process 'Closing by SAG' Stage
		} catch (exc) {
			console.error(exc);
		}
	},

	//EMP project Helper functions
	openProductMobileChangeSetting: function (cmp, event, isAddMobile, isChangeSetting) {
		cmp.set('v.showProductMobileSetting', true);
		cmp.set('v.showChangeSetting', isChangeSetting);
		this.insertParam('isOpenJourney', 'true');
	},

	showToastAMP: function (labelMessage) {
		var toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
			title: 'Warning!',
			message: labelMessage,
			type: 'warning'
		});
		toastEvent.fire();
	},

	insertParam: function (key, value) {
		key = encodeURIComponent(key);
		value = encodeURIComponent(value);

		// kvp looks like ['key1=value1', 'key2=value2', ...]
		var kvp = document.location.search.substr(1).split('&');
		let i = 0;

		for (; i < kvp.length; i++) {
			if (kvp[i].startsWith(key + '=')) {
				let pair = kvp[i].split('=');
				pair[1] = value;
				kvp[i] = pair.join('=');
				break;
			}
		}

		if (i >= kvp.length) {
			kvp[kvp.length] = [key, value].join('=');
		}

		// can return this or...
		let params = kvp.join('&');

		// reload page with new params
		document.location.search = params;
	},

	createDefaultBppOpportunity: function (component) {
		try {
			let accountId = component.get('v.selectedAccountId');
			let action = component.get('c.createDefaultBppOpportunity');
			action.setParam('accountId', accountId);

			action.setCallback(this, function (response) {
				let state = response.getState();
				if (state === 'SUCCESS' && component.isValid()) {
					let opportunityId = response.getReturnValue();

					let toastEvent = $A.get('e.force:showToast');
					let successMessage = $A.get('$Label.c.pp_Opportunity_Created');
					toastEvent.setParams({
						title: 'Success!',
						message: successMessage,
						type: 'success'
					});
					toastEvent.fire();

					let urlEvent = $A.get('e.force:navigateToURL');
					urlEvent.setParams({
						url: '/journey-path?opportunityId=' + opportunityId + '&editMode=true'
					});
					urlEvent.fire();
				} else if (state === 'INCOMPLETE') {
					console.error('State INCOMPLETE');
				} else if (state === 'ERROR') {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.error('Error message: ' + errors[0].message);
						}
					} else {
						console.error('Unknown error');
					}
				}

				if (component.isValid()) {
					component.set('v.showSpinner', false);
				}
			});

			component.set('v.showSpinner', true);
			$A.enqueueAction(action);
		} catch (exception) {
			console.error(exception);
		}
	}
});
