({
	onInit: function (cmp, event, helper) {
		var iFrameMessageReceivedCallback = $A.getCallback(function (frameEvent) {
			if (frameEvent.origin !== vfOrigin) {
				// Not the expected origin: Reject the message!
				return;
			}
			var messageType = frameEvent.data.type;
			if (messageType === 'frameEvent') {
				var pageName = frameEvent.data.pageName;
				var actionName = frameEvent.data.actionName;
				if (pageName === 'OpportunityClosedWon') {
					if (actionName === 'close') {
						helper.closeCloseWonFrame(cmp);
					}
				}
			}
		});

		var vfOrigin = 'https://' + cmp.get('v.vfHost');
		window.addEventListener('message', iFrameMessageReceivedCallback, false);
		let urlParams = decodeURIComponent(window.location.search.substring(1)).split('&');

		for (var i = 0; i < urlParams.length; i++) {
			let sParameterName = urlParams[i].split('=');
			let paramName = sParameterName[0];

			if (paramName === 'isOpenJourney') {
				let paramValue = sParameterName[1];
				if (paramValue) {
					cmp.set('v.isOpenJourney', paramValue.toLowerCase() === 'true');
				}
			} else if (paramName === 'editMode') {
				let paramValue = sParameterName[1];
				if (paramValue) {
					cmp.set('v.editMode', paramValue.toLowerCase() === 'true');
				}
			}
		}

		let opptyId = cmp.get('v.opportunityId');
		if (!opptyId) {
			return;
		}

		helper.retrieveOppty(cmp, opptyId);
	},

	handleChangeIsOpenJourney: function (cmp) {
		var parent = cmp.get('v.parent');
		parent.showHeader(!cmp.get('v.isOpenJourney'));
	},

	onCurrentStepChange: function (component, event, helper) {
		let currentStep = event.getParam('value');
		if (currentStep === 'opportunity') {
			let opportunityId = component.get('v.opportunityId');
			let accountId = component.get('v.selectedAccountId');

			if (!opportunityId && accountId) {
				opportunityId = helper.createDefaultBppOpportunity(component);
			}
		}
	},

	createOppty: function (cmp, event, helper) {
		var createOpptyEvent = $A.get('e.force:createRecord');
		createOpptyEvent.setParams({
			entityApiName: 'Opportunity',
			defaultFieldValues: {
				AccountId: cmp.get('v.selectedAccountId')
			}
		});
		createOpptyEvent.fire();
	},

	onSaveOpportunityClick: function (component, event, helper) {
		component.find('editOpportunity').get('e.recordSave').fire();
		component.set('v.isSaving', true);
	},

	handleStartWaiting: function (component, event, helper) {
		if (component.get('v.isSaving')) {
			component.set('v.showSpinner', true);
		}
	},

	handleDoneWaiting: function (component, event, helper) {
		if (component.get('v.isSaving')) {
			component.set('v.showSpinner', false);
			component.set('v.isSaving', false);
		}
	},

	handleSaveSuccess: function (component) {
		component.set('v.showSpinner', true);
		let action = component.get('c.redirectToViewMode');
		$A.enqueueAction(action);
	},

	redirectToViewMode: function (component) {
		let urlEvent = $A.get('e.force:navigateToURL');
		urlEvent.setParams({
			url: '/journey-path?opportunityId=' + component.get('v.opportunityId') + '&editMode=false'
		});
		urlEvent.fire();
	},

	redirectToEditMode: function (component) {
		let urlEvent = $A.get('e.force:navigateToURL');
		urlEvent.setParams({
			url: '/journey-path?opportunityId=' + component.get('v.opportunityId') + '&editMode=true'
		});
		urlEvent.fire();
	},

	onEditOpptyClick: function (component) {
		component.set('v.editMode', true);
	},

	onCancelOpportunityClick: function (component) {
		let action = component.get('c.redirectToViewMode');
		$A.enqueueAction(action);
	},

	onCloseWonClickFlow: function (cmp, event, helper) {
		helper.openCloseWonFrame(cmp);
		var flowParams = [{ name: 'recordId', type: 'String', value: cmp.get('v.opportunityId') }];
		var flow = cmp.find('flowData');
		flow.startFlow('Close_Win_Opportunity', flowParams);
	},

	handleCloseCloseWon: function (cmp, event, helper) {
		helper.closeCloseWonFrame(cmp);
	},

	handleCloseWonFlowStatusChange: function (cmp, event, helper) {
		if (event.getParam('status') === 'FINISHED') {
			helper.closeCloseWonFrame(cmp);
			window.location.href = window.location.href;
		}
	},

	handleOpptySavedEvent: function (cmp, event, handler) {
		try {
			var isCreate = event.getParam('isCreate');
			var opptyId = event.getParam('opptyId') || cmp.get('v.opportunityId');
			var toastEvent = $A.get('e.force:showToast');
			var successMessage = isCreate ? $A.get('$Label.c.pp_Opportunity_Created') : $A.get('$Label.c.pp_Record_Saved');
			toastEvent.setParams({
				title: 'Success!',
				message: successMessage,
				type: 'success'
			});
			toastEvent.fire();

			// fix for "Error in $A.getCallback() [Cannot read property 'querySelectorAll' of null]"
			if (opptyId) {
				var urlEvent = $A.get('e.force:navigateToURL');
				urlEvent.setParams({
					url: '/journey-path/?opportunityId=' + opptyId
				});
				setTimeout(urlEvent.fire(), 1000);
			} else {
				$A.get('e.force:refreshView').fire();
			}

			cmp.set('v.editMode', false);
		} catch (exc) {
			console.error(exc);
		}
	},

	onOpptyIdChange: function (cmp, event, helper) {
		var opptyId = event.getParam('value');
		if (!opptyId) {
			return;
		}
		helper.retrieveOppty(cmp, opptyId);
	},

	downloadContract: function (cmp, event, helper) {
		var urlEvent = $A.get('e.force:navigateToURL');
		urlEvent.setParams({
			url: '/apex/ProposalPage?Id=a4A9E0000004bqb'
		});
		urlEvent.fire();
	},

	onAddMobileProductClick: function (cmp, event, helper) {
		var opptyId = cmp.get('v.opportunityId');
		var action = cmp.get('c.getBANforAddMobileJourney');
		action.setParam('opportunityId', opptyId);

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === 'SUCCESS' && cmp.isValid()) {
				let oppty = response.getReturnValue();
				helper.openProductMobileChangeSetting(cmp, event, true, false);
			} else if ('INCOMPLETE' === state) {
				console.error('Network Error / Server is Down');
			} else if ('ERROR' === state) {
				var message = 'Response State is ERROR; Unknown error';
				var errors = response.getError();
				if (errors && errors[0] && errors[0].message) {
					message = 'Error message: ' + errors[0].message;
				}
				console.error(message);
			}
		});
		$A.enqueueAction(action);
	},

	onChangeProductSettingsClick: function (cmp, event, helper) {
		helper.openProductMobileChangeSetting(cmp, event, false, true);
	},
	returnToOppCS: function (cmp, event, helper) {
		cmp.set('v.showProductMobileSetting', false);
		cmp.set('v.showChangeSetting', false);
	},
	returnToOppAMP: function (cmp, event, helper) {
		cmp.set('v.showProductMobileSetting', false);
		cmp.set('v.showAddMobileProduct', false);
	},

	handleBanSaved: function (component, event) {
		let action;
		let editMode = component.get('v.editMode');

		if (editMode) {
			action = component.get('c.redirectToEditMode');
		} else {
			action = component.get('c.redirectToViewMode');
		}
		$A.enqueueAction(action);
	}
});
