<aura:component access="global" implements="force:hasRecordId" controller="ContractRelatedQuestionsController">
	<ltng:require
		scripts="{!$Resource.iziToast + '/iziToast.min.js'}"
		styles="{!$Resource.iziToast + '/iziToast.min.css'}"
		afterScriptsLoaded="{!c.errorMessage}"
	/>

	<aura:attribute name="basketId" type="String" />
	<aura:attribute name="enabledContract" type="Object" default="false" />
	<aura:attribute name="contractButtonDisabled" type="boolean" default="false" />
	<aura:attribute name="basket" type="Map" />
	<aura:attribute name="Spinner" type="boolean" default="false" />
	<aura:attribute name="hideExistingContractFields" type="boolean" default="false" />
	<aura:attribute name="hideExistingMobileContractFields" type="boolean" default="false" />
	<aura:attribute name="hideExistingFixedContractFields" type="boolean" default="false" />
	<aura:attribute name="enableLanguageChange" type="boolean" default="false" />
	<aura:attribute name="contractSettings" type="Map[]" />

	<aura:handler name="init" value="{!this}" action="{!c.doInit}" />
	<aura:handler event="aura:waiting" action="{!c.showSpinner}" />
	<aura:handler event="aura:doneWaiting" action="{!c.hideSpinner}" />

	<lightning:layout horizontalAlign="spread" multipleRows="true">
		<aura:if isTrue="{!v.Spinner}">
			<div aura:id="spinnerId" class="slds-spinner_container slds-is-fixed">
				<div class="slds-spinner--brand slds-spinner slds-spinner--large slds-is-fixed" role="alert">
					<span class="slds-assistive-text">Loading</span>
					<div class="slds-spinner__dot-a"></div>
					<div class="slds-spinner__dot-b"></div>
				</div>
			</div>
		</aura:if>

		<lightning:layoutItem padding="around_xxx-small" flexibility="grow" size="12">
			<div class="slds-page-header">
				<div class="slds-media">
					<div class="slds-media__body">
						<h1
							class="slds-page-header__title slds-truncate slds-align_absolute-center slds-page-header__detail-row"
							title="Contract related questions"
						>
							Contract and proposal options
						</h1>
					</div>
				</div>
			</div>
		</lightning:layoutItem>

		<lightning:layoutItem padding="around_xxx-small" flexibility="grow" size="12">
			<lightning:button label="Back to basket" onclick="{!c.gotoURL}" iconName="utility:back" class="slds-float--right" />
		</lightning:layoutItem>

		<lightning:layoutItem padding="around_xxx-small" flexibility="grow" size="12">
			<lightning:card iconName="utility:description" title="Account details" class="slds-box slds-theme_alert-texture">
				<p>
					<label class="slds-form-element__label slds-p-left_small">Authorized to sign 1st</label><br />
					<lightning:formattedText
						linkify="false"
						class="slds-p-left_small"
						value="{!v.basket.csbb__Account__r.Authorized_to_sign_1st__r.Name}"
					/>
				</p>
				<br />
				<p>
					<label class="slds-form-element__label slds-truncate slds-p-left_small">Authorized to sign 2nd</label><br />
					<lightning:formattedText
						linkify="false"
						class="slds-p-left_small"
						value="{!v.basket.csbb__Account__r.Authorized_to_sign_2nd__r.Name}"
					/>
				</p>
				<br />
				<p>
					<label class="slds-form-element__label slds-truncate slds-p-left_small">Frame Work Agreement</label><br />
					<lightning:formattedText class="slds-p-left_small" linkify="false" value="{!v.basket.csbb__Account__r.Frame_Work_Agreement__c}" />
				</p>
				<br />
				<p>
					<label class="slds-form-element__label slds-truncate slds-p-left_small">Frameworkagreement version /4.</label><br />
					<lightning:formattedNumber
						class="slds-p-left_small"
						value="{!v.basket.csbb__Account__r.Version_FWA__c}"
						maximumFractionDigits="0"
						minimumFractionDigits="0"
					/>
				</p>
				<br />
				<p>
					<label class="slds-form-element__label slds-truncate slds-p-left_small">Framework agreement date</label><br />
					<lightning:formattedText
						class="slds-p-left_small"
						linkify="false"
						value="{!v.basket.csbb__Account__r.Framework_agreement_date__c }"
					/>
				</p>
				<br />
				<p>
					<label class="slds-form-element__label slds-truncate slds-p-left_small">Contract rule no mailing</label><br />
					<lightning:input
						type="checkbox"
						class="slds-p-left_small"
						linkify="false"
						checked="{!v.basket.csbb__Account__r.Contract_rule_no_mailing__c}"
						disabled="true"
					/>
				</p>
			</lightning:card>
		</lightning:layoutItem>

		<!-- Contract part -->
		<lightning:layoutItem padding="around_xxx-small" flexibility="grow" size="auto">
			<lightning:card iconName="utility:center_align_text" title="Contract Options" class="slds-box slds-theme_shade">
				<lightning:layoutItem padding="around_xxx-small" flexibility="grow" size="12">
					<lightning:button
						label="Generate contracts"
						iconName="utility:contract"
						onclick="{!c.generateContractAndSaveOnAgreement}"
						disabled="{!v.contractButtonDisabled}"
						class="slds-float-right"
						aura:id="generateContractButton"
					/>
				</lightning:layoutItem>

				<!-- Who will sign on behalf of customer? part -->
				<lightning:layoutItem padding="around-small" flexibility="grow" size="4">
					<lightning:select
						aura:id="signOnBehalfOfCustomer"
						label="Who will sign on behalf of customer?"
						required="true"
						disabled="{!or(v.basket.cscfga__Basket_Status__c == 'Contract created', empty(v.basket.csbb__Account__r.Authorized_to_sign_2nd__c))}"
					>
						<option selected="true" value="first">Authorized to sign 1st</option>
						<option value="second">Authorized to sign 2nd</option>
						<option value="both">Both</option>
					</lightning:select>
				</lightning:layoutItem>

				<!-- Contract language part -->
				<lightning:layoutItem padding="around-small" flexibility="grow" size="4">
					<lightning:select
						aura:id="contractLanguage"
						label="Contract language:"
						required="true"
						disabled="{!or(v.basket.cscfga__Basket_Status__c == 'Contract created', not(v.enableLanguageChange))}"
					>
						<option selected="true" value="Dutch">Dutch</option>
						<option value="English">English</option>
					</lightning:select>
				</lightning:layoutItem>

				<!-- Is the infrastructure part of an existing contract? part -->
				<lightning:layoutitem class="displayNone" padding="around-small" flexibility="grow" size="4">
					<lightning:select
						name="existingContractSelect"
						aura:id="existingContractSelect"
						label="Is the infrastructure part of an existing contract?"
						required="false"
						onchange="{!c.hideExistingContractFields}"
						disabled="{!v.basket.cscfga__Basket_Status__c == 'Contract created'}"
					>
						<option value="No">No</option>
						<option value="YesIPVPN">Yes, IPVPN</option>
						<option value="YesOneFixed">Yes, OneFixed</option>
					</lightning:select>
				</lightning:layoutitem>

				<aura:if isTrue="{!not(v.hideExistingContractFields)}">
					<lightning:layoutitem padding="around-small" flexibility="grow" size="4">
						<div class="slds-form-element">
							<label class="slds-form-element__label" for="contractIDExistingContract">Contract ID</label>
							<div class="slds-form-element__control">
								<ui:inputText aura:id="contractIDExistingContract" class="slds-input" required="true"> </ui:inputText>
							</div>
						</div>
					</lightning:layoutitem>
				</aura:if>

				<aura:if isTrue="{!not(v.hideExistingContractFields)}">
					<lightning:layoutitem padding="around-small" flexibility="grow" size="4">
						<div class="slds-form-element">
							<label class="slds-form-element__label" for="contractDate">Contract Date</label>
							<div class="slds-form-element__control">
								<ui:inputDate
									aura:id="contractDate"
									class="slds-input input-date-width"
									displayDatePicker="true"
									format="dd/MM/yyyy"
								/>
							</div>
						</div>
					</lightning:layoutitem>
				</aura:if>

				<!-- Does the customer have an existing mobile contract? part -->
				<lightning:layoutitem class="displayNone" padding="around-small" flexibility="grow" size="4">
					<lightning:select
						name="existingMobileContractSelect"
						aura:id="existingMobileContractSelect"
						label="Does the customer have an existing mobile contract?"
						required="false"
						onchange="{!c.hideExistingMobileContractFields}"
						disabled="{!v.basket.cscfga__Basket_Status__c == 'Contract created'}"
					>
						<option value="No">No</option>
						<option value="OneBusiness">One Business</option>
						<option value="DataOnly">Data-only</option>
						<option value="OneBusinessIOT">OneBusiness IOT</option>
						<option value="Other">Other</option>
					</lightning:select>
				</lightning:layoutitem>

				<aura:if isTrue="{!not(v.hideExistingMobileContractFields)}">
					<lightning:layoutitem padding="around-small" flexibility="grow" size="4">
						<div class="slds-form-element">
							<label class="slds-form-element__label" for="contractIDMobileExistingContract">Contract ID</label>
							<div class="slds-form-element__control">
								<ui:inputText aura:id="contractIDMobileExistingContract" class="slds-input"> </ui:inputText>
							</div>
						</div>
					</lightning:layoutitem>
				</aura:if>

				<aura:if isTrue="{!not(v.hideExistingMobileContractFields)}">
					<lightning:layoutitem padding="around-small" flexibility="grow" size="4">
						<div class="slds-form-element">
							<label class="slds-form-element__label" for="contractDateMobile">Contract Date</label>
							<div class="slds-form-element__control">
								<ui:inputDate
									aura:id="contractDateMobile"
									class="slds-input input-date-width"
									displayDatePicker="true"
									format="dd/MM/yyyy"
								/>
							</div>
						</div>
					</lightning:layoutitem>
				</aura:if>

				<!-- Does the customer also have/buys a Vodafone Fixed phone service? part -->
				<lightning:layoutitem class="displayNone" padding="around-small" flexibility="grow" size="4">
					<lightning:select
						name="existingFixedContractSelect"
						aura:id="existingFixedContractSelect"
						label="Does the customer also have/buy a Vodafone Fixed phone service?"
						required="false"
						onchange="{!c.hideExistingFixedContractFields}"
						disabled="{!v.basket.cscfga__Basket_Status__c == 'Contract created'}"
					>
						<option value="No">No</option>
						<option value="Yes">Yes</option>
					</lightning:select>
				</lightning:layoutitem>

				<aura:if isTrue="{!not(v.hideExistingFixedContractFields)}">
					<lightning:layoutitem padding="around-small" flexibility="grow" size="4">
						<div class="slds-form-element">
							<label class="slds-form-element__label" for="contractIDFixedExistingContract">Contract ID</label>
							<div class="slds-form-element__control">
								<ui:inputText class="slds-input" aura:id="contractIDFixedExistingContract"> </ui:inputText>
							</div>
						</div>
					</lightning:layoutitem>
				</aura:if>

				<aura:if isTrue="{!not(v.hideExistingFixedContractFields)}">
					<lightning:layoutitem padding="around-small" flexibility="grow" size="4">
						<div class="slds-form-element">
							<label class="slds-form-element__label" for="contractDateFixed">Contract Date</label>
							<div class="slds-form-element__control">
								<ui:inputDate
									aura:id="contractDateFixed"
									class="slds-input input-date-width"
									displayDatePicker="true"
									format="dd/MM/yyyy"
								/>
							</div>
						</div>
					</lightning:layoutitem>
				</aura:if>

				<!-- Will the customer offer devices or services that locate children or other dependents for tracking purposes? part -->
				<lightning:layoutitem class="displayNone" padding="around-small" flexibility="grow" size="4">
					<lightning:select
						aura:id="locate"
						label="Will the customer offer devices or services that locate children or other dependents for tracking purposes?"
						required="false"
						disabled="{!v.basket.cscfga__Basket_Status__c == 'Contract created'}"
					>
						<option value="No">No</option>
						<option value="YesChildren">Yes, for children</option>
						<option value="YesOther">Yes, for Others</option>
					</lightning:select>
				</lightning:layoutitem>

				<!-- checkboxes part -->
				<lightning:layoutitem class="displayNone" padding="around-small" flexibility="grow" size="6">
					<lightning:input
						aura:id="integretedOneFixed"
						type="checkbox"
						label="Will the mobile service be integrated with One Fixed?"
						disabled="{!v.basket.cscfga__Basket_Status__c == 'Contract created'}"
						checked="{!v.basket.Mobile_integrated_with_OneFixed__c}"
					>
					</lightning:input>
				</lightning:layoutitem>

				<aura:if isTrue="{!v.basket.cscfga__Opportunity__r.Direct_Indirect__c == 'Direct'}">
					<lightning:layoutitem class="displayNone" padding="around-small" flexibility="grow" size="6">
						<lightning:input
							aura:id="newFWA"
							type="checkbox"
							label="Create new framework agreement"
							disabled="{!v.basket.cscfga__Basket_Status__c == 'Contract created'}"
							checked="{!v.basket.Create_new_FWA__c}"
						>
						</lightning:input>
					</lightning:layoutitem>
				</aura:if>

				<aura:if isTrue="{!v.basketcscfga__Opportunity__r.Direct_Indirect__c == 'Direct'}">
					<lightning:layoutitem class="displayNone" padding="around-small" flexibility="grow" size="6">
						<lightning:input
							aura:id="latestGeneralTandC"
							type="checkbox"
							label="Add the latest general terms and conditions"
							disabled="{!v.basket.cscfga__Basket_Status__c == 'Contract created'}"
							checked="{!v.basket.Latest_TandC__c}"
						>
						</lightning:input>
					</lightning:layoutitem>
				</aura:if>

				<lightning:layoutitem class="displayNone" padding="around-small" flexibility="grow" size="6">
					<lightning:input
						aura:id="useFax"
						type="checkbox"
						label="Customer wishes to make use of support for fax service"
						disabled="{!v.basket.cscfga__Basket_Status__c == 'Contract created'}"
						checked="{!v.basket.Support_use_of_fax__c}"
					>
					</lightning:input>
				</lightning:layoutitem>

				<lightning:layoutitem class="displayNone" padding="around-small" flexibility="grow" size="6">
					<lightning:input
						aura:id="sellthroughConnectivity"
						type="checkbox"
						label="Does sellthrough connectivity apply?"
						disabled="{!v.basket.cscfga__Basket_Status__c == 'Contract created'}"
						checked="{!v.basket.Sellthrough_connectivity__c}"
					>
					</lightning:input>
				</lightning:layoutitem>
			</lightning:card>
		</lightning:layoutItem>

		<!-- Optional clauses -->
		<lightning:layoutItem padding="around_xxx-small" flexibility="grow" size="4" aura:id="optionalClauses">
			<lightning:card iconName="utility:edit" title="Optional clauses" class="slds-box slds-theme_shade">
				<!-- Show Clause Mobile Hardware & acc. Discount -->
				<lightning:layoutItem aura:id="mobileHardwareAccDiscount" padding="around-small" flexibility="grow" size="12">
					<lightning:select aura:id="mobileHardwareAccDiscountSelect" label="Korting op mobiele toestellen en accessoires" required="true">
						<option value="Yes">Ja</option>
						<option value="No" selected="true">Nee</option>
					</lightning:select>
				</lightning:layoutItem>
			</lightning:card>
		</lightning:layoutItem>

		<!-- Proposal part -->
		<lightning:layoutItem padding="around_xxx-small" flexibility="grow" size="auto">
			<lightning:card iconName="utility:file" title="Proposal Options" class="slds-box slds-theme_shade">
				<lightning:layoutitem padding="around_xxx-small" flexibility="grow" size="12">
					<lightning:button label="Generate Proposal Excel" onclick="{!c.downloadExcel}" iconName="utility:download" />
				</lightning:layoutitem>
			</lightning:card>
		</lightning:layoutItem>
	</lightning:layout>
</aura:component>

<!--    -->
