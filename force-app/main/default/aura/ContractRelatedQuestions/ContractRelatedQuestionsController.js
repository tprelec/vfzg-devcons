({
	hideExistingContractFields: function (component, event, helper) {
		var sel = component.find('existingContractSelect');
		var val = sel.get('v.value');

		if (val != '' && val != 'No' && val != null) {
			component.set('v.hideExistingContractFields', false);
		} else {
			component.set('v.hideExistingContractFields', true);
			component.find('contractIDExistingContract').set('v.value', '');
			component.find('contractDate').set('v.value', '');
		}
	},
	hideExistingMobileContractFields: function (component, event, helper) {
		var sel = component.find('existingMobileContractSelect');
		var val = sel.get('v.value');

		if (val != '' && val != 'No' && val != null) {
			component.set('v.hideExistingMobileContractFields', false);
		} else {
			component.set('v.hideExistingMobileContractFields', true);
			component.find('contractIDMobileExistingContract').set('v.value', '');
			component.find('contractDateMobile').set('v.value', '');
		}
	},
	hideExistingFixedContractFields: function (component, event, helper) {
		var sel = component.find('existingFixedContractSelect');
		var val = sel.get('v.value');

		if (val != '' && val != 'No' && val != null) {
			component.set('v.hideExistingFixedContractFields', false);
		} else {
			component.set('v.hideExistingFixedContractFields', true);
			component.find('contractIDFixedExistingContract').set('v.value', '');
			component.find('contractDateFixed').set('v.value', '');
		}
	},
	doInit: function (component, event, helper) {
		var basketId = component.get('v.basketId');
		var action = component.get('c.getBasketDetails');
		action.setParams({ basketId: basketId });

		action.setCallback(this, function (response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				console.log('Basket fetched');

				var basket = response.getReturnValue();
				component.set('v.basket', basket);

				var actionGetContractSettings = component.get('c.getContractCustomSettings');
				actionGetContractSettings.setParams({ productBasket: basket });
				actionGetContractSettings.setCallback(this, function (response) {
					var state = response.getState();
					if (state == 'SUCCESS') {
						console.log('Contract settings fetched');
						var contractSettings = response.getReturnValue();
						component.set('v.contractSettings', contractSettings);

						var res = !helper.enableGenerateContract(component, contractSettings);
						component.set('v.contractButtonDisabled', res);

						var enableLanguageChange = helper.enableLanguageChange(contractSettings);
						component.set('v.enableLanguageChange', enableLanguageChange);

						var basket = component.get('v.basket');
						if (basket.Contract_Language__c == 'English') {
							component.find('contractLanguage').set('v.value', 'English');
						}

						var actionGetCurrentUser = component.get('c.getCurrentUser');
						actionGetCurrentUser.setCallback(this, function (response) {
							var state = response.getState();
							if (state == 'SUCCESS') {
								var currentUser = response.getReturnValue();
								console.log('User set!');
								var optionalClausesJSON = helper.getOptionalClausesJSON(basket, currentUser, contractSettings);

								if (optionalClausesJSON['flexMobile'] == false) {
									var clauseCheckbox = component.find('mobileHardwareAccDiscount');
									$A.util.toggleClass(clauseCheckbox, 'displayNone');
									var optionalClauses = component.find('optionalClauses');
									$A.util.toggleClass(optionalClauses, 'displayNone');
								}
							} else {
								console.log('No user found.');
							}
						});
						$A.enqueueAction(actionGetCurrentUser);
					} else {
						console.log('Fetching contract settings failed.');
					}
				});
				$A.enqueueAction(actionGetContractSettings);
			} else {
				console.log('No basket found.');
			}

			var hideOne = component.get('c.hideExistingContractFields');
			$A.enqueueAction(hideOne);

			var hideTwo = component.get('c.hideExistingMobileContractFields');
			$A.enqueueAction(hideTwo);

			var hideThree = component.get('c.hideExistingFixedContractFields');
			$A.enqueueAction(hideThree);
		});
		$A.enqueueAction(action);
	},
	gotoURL: function (component, event, helper) {
		var basket = component.get('v.basket');

		if (window.location.href.indexOf('partnerportal') > -1) {
			window.open('/partnerportal/' + basket.Id, '_top');
		} else {
			window.open('/' + basket.Id, '_top');
		}
	},
	generateContractAndSaveOnAgreement: function (component, event, helper) {
		var templatesJSON = {
			Direct: {
				Dutch: {
					V1: 'Direct',
					V2: 'Direct 2'
				},
				English: 'Direct English'
			},
			Indirect: {
				English: 'Indirect English',
				Dutch: 'Indirect'
			}
		};

		var basket = component.get('v.basket');
		var directIndirect = basket.cscfga__Opportunity__r.Direct_Indirect__c;
		var contractLanguage = component.find('contractLanguage').get('v.value');
		var contractSettings = component.get('v.contractSettings');
		var requirements = helper.getRequirements(contractSettings, basket);

		var updateBasketAndCreateAgreementsAction = component.get('c.updateBasket');
		var mapToSend = helper.basketUpdateFields(component, event);
		updateBasketAndCreateAgreementsAction.setParams({ basket: basket, fieldsToUpdate: mapToSend });

		updateBasketAndCreateAgreementsAction.setCallback(this, function (response) {
			var state = response.getState();
			if (state === 'SUCCESS') {
				console.log('Basket updated');
				var createAgreementsAction = component.get('c.createBPAIfNeeded');
				createAgreementsAction.setParams({ basket: basket });
				createAgreementsAction.setCallback(this, function (response) {
					var state = response.getState();

					if (state === 'SUCCESS') {
						if (requirements == null) {
							// Indirect
							var actionCreateAgreementIndirect = component.get('c.createAgreement2');
							actionCreateAgreementIndirect.setParams({ basket: basket, template: templatesJSON[directIndirect][contractLanguage] });
							actionCreateAgreementIndirect.setCallback(this, function (response) {
								helper.redirectToOppOnSuccess(response, actionCreateAgreementIndirect, basket);
							});
							$A.enqueueAction(actionCreateAgreementIndirect);
						} else {
							// Direct
							console.log(requirements['reqV1']);
							console.log(requirements['reqV2']);

							// Dutch direct
							if (contractLanguage == 'Dutch') {
								// Both old and new format contracts required
								if (requirements['reqV1'] && requirements['reqV2']) {
									var actionCreateDirectAgreementV1 = component.get('c.createAgreement2');
									actionCreateDirectAgreementV1.setParams({
										basket: basket,
										template: templatesJSON[directIndirect][contractLanguage]['V1']
									});

									actionCreateDirectAgreementV1.setCallback(this, function (response) {
										var state = response.getState();
										var retValue = response.getReturnValue();

										if (state == 'SUCCESS' && retValue != null) {
											var actionCreateDirectAgreementV2 = component.get('c.createAgreement2');
											actionCreateDirectAgreementV2.setParams({
												basket: basket,
												template: templatesJSON[directIndirect][contractLanguage]['V2']
											});

											actionCreateDirectAgreementV2.setCallback(this, function (response) {
												helper.redirectToOppOnSuccess(response, actionCreateDirectAgreementV2, basket);
											});
											$A.enqueueAction(actionCreateDirectAgreementV2);

											helper.redirectToOpp(basket);
										} else {
											helper.callbackError(actionCreateDirectAgreementV1);
										}
									});

									$A.enqueueAction(actionCreateDirectAgreementV1);
								} else if (requirements['reqV1'] || requirements['reqV2']) {
									// Single contract
									var actionCreateDirectAgreement = component.get('c.createAgreement2');
									actionCreateDirectAgreement.setParams({
										basket: basket,
										template: templatesJSON[directIndirect][contractLanguage][requirements['reqV1'] ? 'V1' : 'V2']
									});
									actionCreateDirectAgreement.setCallback(this, function (response) {
										helper.redirectToOppOnSuccess(response, actionCreateDirectAgreement, basket);
									});
									$A.enqueueAction(actionCreateDirectAgreement);
								}
							} else if (contractLanguage == 'English') {
								// English direct
								var actionCreateDirectAgreementEnglish = component.get('c.createAgreement2');
								actionCreateDirectAgreementEnglish.setParams({
									basket: basket,
									template: templatesJSON[directIndirect][contractLanguage]
								});
								actionCreateDirectAgreementEnglish.setCallback(this, function (response) {
									helper.redirectToOppOnSuccess(response, actionCreateDirectAgreementEnglish, basket);
								});
								$A.enqueueAction(actionCreateDirectAgreementEnglish);
							}
						}
					} else {
						helper.callbackError(createAgreementsAction);
					}
				});
				$A.enqueueAction(createAgreementsAction);
			} else {
				helper.callbackError(updateBasketAndCreateAgreementsAction);
			}
		});
		$A.enqueueAction(updateBasketAndCreateAgreementsAction);
	},
	downloadExcel: function (component, event, helper) {
		var basket = component.get('v.basket');

		if (window.location.href.indexOf('partnerportal') > -1) {
			window.location.replace('/partnerportal/apex/ProposalPage?Id=' + basket.Id);
		} else {
			window.location.replace('/apex/ProposalPage?Id=' + basket.Id);
		}
	},
	showSpinner: function (component, event, helper) {
		component.set('v.Spinner', true);
	},
	hideSpinner: function (component, event, helper) {
		component.set('v.Spinner', false);
	},
	errorMessage: function (component, event, helper) {}
});
