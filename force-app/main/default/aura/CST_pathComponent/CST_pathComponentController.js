({
	doInit: function (component, event) {
		//var cmp = document.querySelector('.slds-path__action');
		//cmp.classList.add('cstNewBackground');

		var action = component.get('c.getUserInfo');
		console.log('CST_pathComponent JS');

		var recordId = component.get('v.recordId');
		console.log('recordId: ' + component.get('v.recordId'));
		//*
		action.setParams({
			CaseId: recordId
		});
		//*/

		action.setCallback(this, function (response) {
			console.log('CST_pathComponent setCallback');

			var state = response.getState();
			console.log('response: ' + response);
			console.log('state: ' + state);
			console.log('CST_pathComponent response 1:  ' + response.getReturnValue());
			if (state === 'SUCCESS') {
				var hideBtn = response.getReturnValue();
				console.log('CST_pathComponent response 2 hideBtn:  ' + hideBtn);
				component.set('v.hideUpdateButton', hideBtn);
			}
		});

		$A.enqueueAction(action);
	},
	handleSelect: function (component, event) {
		console.log('select');
	}
});
