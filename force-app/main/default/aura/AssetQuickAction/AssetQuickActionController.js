({    
    
    handleLoad: function(component, event, helper) {
        
    },
    
   
    handleOnError : function(component, event, helper) {
        
    },
    
    handleSuccess : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            "title": "Success!",
            "message": "The Asset has been created."
        });
        toastEvent.fire();		
        $A.get('e.force:refreshView').fire();
    },
    cancel : function(component, event, helper) {
        $A.get("e.force:closeQuickAction").fire();
    }

})