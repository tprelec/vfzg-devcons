({
	sortFields : function(snapshotFields) {
		//getOne Off fields
        var result = {};
        
        var oneOff = [];
        var recurring = [];
        
        for(var idx in snapshotFields){
        	if(snapshotFields[idx].recurringOneOff =='both'){
        		oneOff.push(snapshotFields[idx]);
        		recurring.push(snapshotFields[idx]);
        	}
        	else if(snapshotFields[idx].recurringOneOff =='oneOff'){
        		oneOff.push(snapshotFields[idx]);
        	}
        	else if(snapshotFields[idx].recurringOneOff =='recurring'){
        		recurring.push(snapshotFields[idx]);
        	}
        }
        
        
        function mySorter(a, b) {
            var x = a.sequence;
            var y = b.sequence;
            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        }
        
        oneOff = oneOff.sort(mySorter);
        recurring = recurring.sort(mySorter);
        
        var headers = [];
        //get headers
        
        var max = recurring.length;
        
        if(oneOff.length >recurring.length){
        	max = oneOff.length;
        }
        
        for(var idx = 0;idx<max;idx++){
        	if(oneOff[idx].label == recurring[idx].label){
        		headers.push(oneOff[idx].label);
        	}
        	else{
        		headers.push(oneOff[idx].label);
        		headers.push(recurring[idx].label);
        	}
        }
        
        result.oneOffFields = oneOff;
        result.recurringFields = recurring;
        result.headers = headers;
        
        return result;
	}
	
	
})