({
	doInit : function(component, event, helper) {
	     //console.log('A Test');
		var	basketId = component.get("v.basketId");
		console.log('got component '+basketId);
		
		var profileName;
		var getProfile = component.get("c.getProfileName");
		getProfile.setCallback(this, function(response) {
			var state = response.getState();
			if (state === "SUCCESS") {		
				profileName = response.getReturnValue();
			} else {
				throw new Error("Error during profile check. Please contact your Administrator");
			}
		});
		$A.enqueueAction(getProfile);
		
		var action = component.get("c.getDealSummaryData");
		action.setParams({basketId : basketId});
		
		action.setCallback(this, function(response) {
			console.log('callback');	
			var state = response.getState();
			if (state === "SUCCESS") {		
				console.log('SUCCESS');	
				var dealSummaryData = response.getReturnValue();
				console.log('dealSummaryData=');
				console.log(dealSummaryData);
				component.set("v.snapshots", dealSummaryData.snapshots);
				component.set("v.dealSummaryTotalOverviewData", dealSummaryData.totalOverviewData);
				component.set("v.isGreen", dealSummaryData.isGreen);

			    var snapshotFields = dealSummaryData.snapshotFields;
			    
			    var fieldsSorted  = helper.sortFields(snapshotFields);
			    
			    component.set("v.columnNames", fieldsSorted.headers);
			    component.set("v.oneOffFields", fieldsSorted.oneOffFields);
			    component.set("v.recurringFields", fieldsSorted.recurringFields);
			    
			    var snapshotsFull = [];
			    
			    for(var idx in dealSummaryData.snapshots){
			        
                    var thisSnap = dealSummaryData.snapshots[idx];
                    if(thisSnap.Product_Configuration__r.Deal_type__c == 'Disconnect' && (profileName!='System Administrator' && profileName!='VF EBU/WBU Control' && profileName!='VF Financial Sales Control')) {
                        continue;
                    }
					
			        var snapFieldsOneOff = [];
			        var snapFieldsRecurring = [];
			        
			        var snap = {};
			        snap.snap = dealSummaryData.snapshots[idx];
					
					if(thisSnap.Product_Configuration__r.Deal_type__c != 'Retention')
					{
						for(var oidx in fieldsSorted.oneOffFields){
			           
							var ofField = {};
						   ofField.label = fieldsSorted.oneOffFields[oidx].label;
						   ofField.name = fieldsSorted.oneOffFields[oidx].name;
						   ofField.recurringOneOff = fieldsSorted.oneOffFields[oidx].recurringOneOff;
						   ofField.sequence = fieldsSorted.oneOffFields[oidx].sequence;
						   ofField.type = fieldsSorted.oneOffFields[oidx].type;
						   ofField.sequence = fieldsSorted.oneOffFields[oidx].sequence;
						   ofField.visibleIndirect = fieldsSorted.oneOffFields[oidx].visibleIndirect;
						   ofField.visibleDirect = fieldsSorted.oneOffFields[oidx].visibleDirect;
						   
						   
						   var field = ofField.name;
						  
						   var fieldValue = thisSnap[field];
						   
						   if(field=='TYPE'){
							   fieldValue = 'One Off';
							   ofField.value = fieldValue;
						   }
						   
							if(field=='OneOffProductName__c'){
							   ofField.value = thisSnap.OneOffProductName__c;
							   
						   }
						   ofField.value = fieldValue;
						   snapFieldsOneOff.push(ofField);
						   
					   }
					}
			        
			        for(var ridx in fieldsSorted.recurringFields){
			            
			            var recField = {};
			            recField.label = fieldsSorted.recurringFields[ridx].label;
			            recField.name = fieldsSorted.recurringFields[ridx].name;
			            recField.recurringOneOff = fieldsSorted.recurringFields[ridx].recurringOneOff;
			            recField.sequence = fieldsSorted.recurringFields[ridx].sequence;
			            recField.type = fieldsSorted.recurringFields[ridx].type;
			            recField.sequence = fieldsSorted.recurringFields[ridx].sequence;
			            recField.visibleIndirect = fieldsSorted.recurringFields[ridx].visibleIndirect;
			            recField.visibleDirect = fieldsSorted.recurringFields[ridx].visibleDirect;
			            var field = recField.name;
			            
			            var fieldValue = thisSnap[field];
			            recField.value = fieldValue;
			            if(field=='TYPE'){
			                fieldValue = 'Recurring';
			                recField.value = fieldValue;
			            }
			            recField.value = fieldValue;
			            if(field=='RecurringProductName__c'){
			                recField.value = thisSnap.RecurringProductName__c;
			            }
			            snapFieldsRecurring.push(recField);
			            
			        }
			        
			        snap.snapFieldsRecurring = snapFieldsRecurring;
			        snap.snapFieldsOneOff = snapFieldsOneOff;
			        
			        snapshotsFull.push(snap);
			        
			    }
			    
			    console.log('FULL SNAPS');
			    console.log(snapshotsFull);
			    component.set("v.snapshots",snapshotsFull);
			}
			else {
				console.log('FAIL');
			}
		});
		$A.enqueueAction(action);
	},
	gotoURL: function(component, event, helper) {
	    var	basketId = component.get("v.basketId");
	    
	    if(window.location.href.indexOf("partnerportal") > -1) {
            window.open('/partnerportal/'+basketId,'_top');
    	         //window.location.replace('/partnerportal/apex/ProposalPage?Id='+basket.Id);
    	         
        } else {
             //window.location.replace('/apex/ProposalPage?Id='+basket.Id);
             window.open('/'+basketId,'_top');
        }
	
	}
})