({
	onInit: function (cmp, event, helper) {
		helper.doInit(cmp);
	},

	showHeader: function (cmp, event) {
		var params = event.getParam('arguments');
		cmp.set('v.isNewJourney', params.newJourney);
	},

	handleAccountIdChange: function (cmp, event, helper) {
		var newAccountId = event.getParam('value');
		var oldAccountId = event.getParam('oldValue');

		// form cleared or new account search executed
		if (!newAccountId) {
			helper.initiateSteps(cmp);
		}
	},

	handleStepCompletedEvent: function (cmp, event, helper) {
		var completedStep = event.getParam('stepName');
		helper.completeStep(cmp, completedStep);

		var moveNext = event.getParam('moveNext');
		if (moveNext) {
			var currentStep = cmp.get('v.currentStep');
			helper.moveStep(cmp, currentStep, true);
		}
	},

	handleResetAllStepsEvent: function (cmp, event, helper) {
		helper.resetAllSteps(cmp);
	},

	handleGoToStepEvent: function (cmp, event, helper) {
		var stepName = event.getParam('stepName');
		helper.goToStep(cmp, stepName);
	},

	handlePathNavigationButtonEvent: function (cmp, event, helper) {
		var isNext = event.getParam('next');
		var currentStep = cmp.get('v.currentStep');
		helper.moveStep(cmp, currentStep, isNext);
	},

	onPathItemClick: function (cmp, event, helper) {
		var selectedStepName = event.currentTarget.dataset.stepName;
		var prevStep = cmp.get('v.currentStep');
		helper.updateCurrentStep(cmp, prevStep, selectedStepName);
	}
});
