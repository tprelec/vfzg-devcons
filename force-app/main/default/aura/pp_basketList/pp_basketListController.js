({
	// Initialises the data for the component
	// by loading in the baskets for the opportunity
	doInit : function(cmp, event, helper) {
        var oppId = cmp.get('v.opportunityId');
        helper.getBaskets(cmp, oppId);
	},

	// Refreshes the list of baskets
	// The user needs to click this after creating a new basket so that they can see it in the list
    refreshList: function(cmp, event, helper) {
        var oppId = cmp.get('v.opportunityId');
        helper.getBaskets(cmp, oppId);
    },

    // Inserts a new basket record and opens the CS basket page
    createApexBasket : function (cmp, event, helper) {
        var oppId = cmp.get('v.opportunityId');        
        helper.createBasketHelper(cmp, oppId);        
    },    

    // Edits the selected basket on the CS basket page
    handleMenuSelect: function (cmp, event, helper) {
        try {
            var selectedMenuItemValue = event.getParam("value");
            var basketId = event.getSource().get('v.value');

            if ('edit' === selectedMenuItemValue) {
                cmp.set("v.iframeURL", "");
                helper.setIframeEditURL(cmp, basketId, true);
            }
        }
        catch (exc) {
            console.log(exc);
        }
/*        
        var basketId = event.getSource().get('v.value');    	
        var url = $A.get("$Label.c.pp_Community_Base_URL")
                        + '/apex/csbb__BasketbuilderApp?id='+ basketId; 
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
            "url": url
        });
        urlEvent.fire();
*/
    }    

})