({
    getBaskets: function(cmp, opportunityId) {
        try {
        	// Run the method in the apex controller to query the baskets for this opportunity
			var action = cmp.get("c.getBaskets");
			action.setParams({"opportunityID": cmp.get("v.opportunityId")});
			action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {

                	// Get the baskets returned and put them into the component
                    var baskets = response.getReturnValue();
                    cmp.set('v.baskets', baskets);
                    
                }
                else if ("INCOMPLETE" === state) {
                    console.error("Network Error / Server is Down");
                }
                else if ("ERROR" === state) {
                    var message = "Response State is ERROR; Unknown error";
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        message = "Error message: " + errors[0].message;
                    }
                    console.error(message);
                }

                if (cmp.isValid()) {
                    cmp.set('v.showSpinner', false);
                }
			});
			
			$A.enqueueAction(action);
        } catch (exc) {
            console.error(exc);
        }

    },

    createBasketHelper: function(cmp, opportunityId) {

        try {
        	// Run the method in the apex controller to create a new basket
			var action = cmp.get("c.createBasket");
			action.setParams({"opportunityID": cmp.get("v.opportunityId")});
			
			action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS" && cmp.isValid()) {

                	// Get the ID of the new basket
                    var basketId = response.getReturnValue();
                    console.log('##basketId: '+basketId);
	
                    if (basketId === 'Error' || basketId === 'oracleError') {
                        var errorMsg = $A.get("$Label.c.pp_basketCreateError");
                        if (basketId === 'oracleError') {
                            errorMsg = $A.get("$Label.c.pp_basketCreateOracleError");
                        }
                        var toastEvent = $A.get("e.force:showToast");
                        toastEvent.setParams({
                            "title": 'Error',
                            "message": errorMsg,
                            "type": 'error'
                        });
                        toastEvent.fire();
                    } else {
                        // open a new page to edit the new basket
                        //var url = $A.get("$Label.c.pp_Community_Base_URL") + '/apex/csbb__BasketbuilderApp?id='+ basketId + '&isdtp=mn';
                        var url = $A.get("$Label.c.pp_Community_Base_URL") + '/csbb__BasketbuilderApp?id='+ basketId + '&isdtp=mn';
						var urlEvent = $A.get("e.force:navigateToURL");
                        console.log(url);
                        urlEvent.setParams({
                            "url": url
                        });
                        urlEvent.fire();
                    }


                }
                else if ("INCOMPLETE" === state) {
                    console.error("Network Error / Server is Down");
                }
                else if ("ERROR" === state) {
                    var message = "Response State is ERROR; Unknown error";
                    var errors = response.getError();
                    if (errors && errors[0] && errors[0].message) {
                        message = "Error message: " + errors[0].message;
                    }
                    console.error(message);
                }

                if (cmp.isValid()) {
                    cmp.set('v.showSpinner', false);
                }
			});
			
			$A.enqueueAction(action);
        } catch (exc) {
            console.error(exc);
        }

    },    
    
    /***
     * IFrame url for Basket edit
     */
    setIframeEditURL: function (cmp, basketId, showIFrame) {
        var url = $A.get("$Label.c.pp_Community_Base_URL")
                        + '/'+ basketId;
        //url = $A.get("$Label.c.pp_Community_Base_URL")
        //                + '/apex/csbb__BasketbuilderApp?id='+ basketId + '&isdtp=mn';     
        url = $A.get("$Label.c.pp_Community_Base_URL")
                        + '/apex/csbb__CSBasketRedirect?id='+ basketId+ '&amp;isdtp=mn';             
        
        cmp.set("v.iframeURL", url);
        
        if (showIFrame) {
            cmp.set("v.iframeHeight", '1000px');
            cmp.set('v.showIframe', true);
        }
    }    

})