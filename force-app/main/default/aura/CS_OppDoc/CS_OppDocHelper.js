({
	getApexDocuments: function (component) {
		component.set('v.spinner', true);
		var objId = component.get('v.recordId');

		var docRecordsFound = false;
		var sdRecordsFound = false;
		var tariffRecordsFound = false;
		var contractSummaryRecordsFound = false;

		var action = component.get('c.getDocuments');
		action.setParams({ objId: objId });

		action.setCallback(this, function (data) {
			if (data.getReturnValue().length > 0) {
				var tmpSdList = [];
				var tmpDocList = [];
				var tmpTariffList = [];
				var tmpContractSummaryList = [];

				var res = data.getReturnValue();

				for (var i = 0; i < res.length; i++) {
					if (res[i].isServiceDescription === true) {
						tmpSdList.push(res[i]);
						sdRecordsFound = true;
					} else if (res[i].isTariffDescription === true) {
						tmpTariffList.push(res[i]);
						tariffRecordsFound = true;
					} else if (res[i].isContractSummary === true) {
						tmpContractSummaryList.push(res[i]);
						contractSummaryRecordsFound = true;
					} else {
						tmpDocList.push(res[i]);
						docRecordsFound = true;
					}
				}
				component.set('v.sdRecordsFound', sdRecordsFound);
				component.set('v.tariffsRecordsFound', tariffRecordsFound);
				component.set('v.contractSummaryRecordsFound', contractSummaryRecordsFound);
				component.set('v.recordsFound', docRecordsFound);
				component.set('v.docList', tmpDocList);
				component.set('v.sdList', tmpSdList);
				component.set('v.tariffsList', tmpTariffList);
				component.set('v.contractSummaryList', tmpContractSummaryList);
			}
			component.set('v.hasRun', true);
			component.set('v.spinner', false);
		});
		$A.enqueueAction(action);
	}
});
