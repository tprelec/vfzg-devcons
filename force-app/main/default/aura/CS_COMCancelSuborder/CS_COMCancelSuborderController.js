({
    onInit: function(cmp, event, helper) {
        debugger;
        cmp.set('v.columns', [
            {label: 'Available Suborders', fieldName: 'Name', type: 'text'}
        ]);
        cmp.set('v.unavailColumns', [
            {label: 'Unavailable Suborders', fieldName: 'Name', type: 'text'}
        ]);
        helper.callServer(cmp,
            'c.getSuborderList', {
                requestJson: JSON.stringify(helper.createInitRequest(cmp, event, helper))
            },
            helper.readInitResponse.bind(this, cmp, event, helper)
        );
        cmp.find("confirmButton").set("v.disabled", true);
    },
    onRowSelect: function (cmp, event) {
        var selectedRows = event.getParam('selectedRows');
        cmp.set('v.selectedRows', selectedRows);
        cmp.set('v.selectedRowsCount', selectedRows.length);
        if(selectedRows.length === 0) {
            cmp.find("confirmButton").set("v.disabled", true);
        } else {
            cmp.find("confirmButton").set("v.disabled", false);
        }
    },
    handleConfirmClick: function (cmp, event) {
        $A.createComponent(
            "c:CS_COMModalConfirmation",
            {
                "title": "Cancel Suborder(s)",
                "tagline": "",
                "message": "You are about to cancel " + cmp.get('v.selectedRowsCount') + " suborder(s), are you sure you want to proceed?",
                "confirm": cmp.getReference("c.onCancel"),
                "param": cmp.get("v.selectedRows")
            },
            function(modalWindow, status, errorMessage){
                //Add the new button to the body array
                if (status === "SUCCESS") {
                    var body = cmp.get("v.body");
                    body.push(modalWindow);
                    cmp.set("v.body", body);
                }
                else if (status === "INCOMPLETE") {
                    console.log("No response from server or client is offline.")
                    // Show offline error
                }
                else if (status === "ERROR") {
                    console.log("Error: " + errorMessage);
                    // Show error message
                }
            }
        );
    },
    handleCancelClick: function (cmp, event) {
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    onCancel: function(cmp, event, helper) {
        debugger;
        helper.showElement(cmp, 'spinner');
        helper.callServer(cmp,
            'c.cancelSuborder', {
                requestJson: JSON.stringify(helper.createRequest(cmp, event, helper))
            },
            helper.readResponse.bind(this, cmp, event, helper)
        );
    }
})