({
	doInit: function (component, event, helper) {
		helper.getOppStageName(component, event, helper);
		helper.getPortfolioStatus(component, event, helper);
		helper.getOrder(component, event, helper);
	},

	handleConfirm: function (component, event, helper) {
		var today = new Date();
		var fieldDate = new Date(component.find("closeDate").get("v.value"));
		if (fieldDate <= today) {
			component.set("v.isSpinnerVisible", true);
			component.set("v.dateError", false);
			component.set("v.showFlow", true);
			component.set("v.closeDate", fieldDate);
			let action = component.get("c.updateOppCloseDate");
			action.setParams({ recordId: component.get("v.recordId"), fieldDate: component.get("v.closeDate") });
			action.setCallback(this, function (response) {
				let state = response.getState();
				if (state === "SUCCESS") {
					let result = response.getReturnValue();
					component.set("v.isSpinnerVisible", false);
					if (result != null) {
						helper.initializeFlow(component, event, helper);
					}
				} else if (state === "ERROR") {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							// log the error passed in to AuraHandledException
							console.log("Error message: " + errors[0].message);
							helper.showToastAdvanced(component, event, helper, 'Error while updating Opportunity', errors[0].message, 'info', 'sticky');
						}
					}
				}
				$A.get("e.force:closeQuickAction").fire();
			});
			$A.enqueueAction(action);
		} else {
			component.set("v.dateError", true);
		}
	},

	handleStatusChange: function (component, event, helper) {
		helper.handleStatusChangeHelper(component, event, helper);
	},

	handleCancel: function (component, event, helper) {
		$A.get("e.force:closeQuickAction").fire();
	}
});