({
	getPortfolioStatus: function (component, event, helper) {
		let action = component.get("c.getProductBasketPortfolioStatus");
		action.setParams({ opportunityId: component.get("v.recordId") });

		action.setCallback(this, function (response) {
			let state = response.getState();
			if (state === "SUCCESS") {
				let result = response.getReturnValue();

				if (result != true) {
					helper.redirectToPage(component, event, helper);
				} else {
					component.set("v.isNewPortfolio", result);
				}
			} else if (state === "ERROR") {
				console.log("Error getting New Portfolio from Product Basket.");
				helper.redirectToPage(component, event, helper);
			}
		});
		$A.enqueueAction(action);
	},

	getOrder: function (component, event, helper) {
		let action = component.get("c.getOrder");

		action.setParams({ opportunityId: component.get("v.recordId") });

		action.setCallback(this, function (response) {
			let state = response.getState();
			if (state === "SUCCESS") {
				let result = response.getReturnValue();
				if (result == null) {
					helper.redirectToPage(component, event, helper);
				} else {
					component.set("v.orderId", result);
				}
			} else if (state === "ERROR") {
				console.log("Error getting Order for this Opportunity.");
				helper.redirectToPage(component, event, helper);
			}
		});
		$A.enqueueAction(action);
	},

	initializeFlow: function (component, event, helper) {
		const flow = component.find("flowData");
		var inputVariables = [
			{
				name: "recordId",
				type: "String",
				value: component.get("v.orderId")
			}
		];
		flow.startFlow("CleanOrderCriteriaProcess", inputVariables);
	},

	handleStatusChangeHelper: function (component, event, helper) {
		if (event.getParam("status") === "FINISHED") {
			var orderId = component.get("v.orderId");
			var navEvt = $A.get("e.force:navigateToSObject");
			navEvt.setParams({
				recordId: orderId,
				slideDevName: "Detail"
			});
			navEvt.fire();
		}
	},

	getOppStageName: function (component, event, helper) {
		let action = component.get("c.isOpportunityClosedWon");
		action.setParams({ recordId: component.get("v.recordId") });

		action.setCallback(this, function (response) {
			let state = response.getState();
			if (state === "SUCCESS") {
				let result = response.getReturnValue();

				if (result == true) {
					component.set("v.isClosedWon", true);
					helper.showToast(component, event, helper);
					$A.get("e.force:closeQuickAction").fire();
				} else {
					component.set("v.isClosedWon", false);
				}
			}else if (state === "ERROR") {
				console.log("Error getting Opportunity StageName.");
			}
		});
		$A.enqueueAction(action);
	},

	showToast: function (component, event, helper) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			title: $A.get("$Label.c.OCWC_Toast_Title"),
			message: $A.get("$Label.c.OCWC_Toast_Message"),
			type: "info"
		});
		toastEvent.fire();
	},

	showToastAdvanced: function (component, event, helper, title, message, type, mode) {
		var toastEvent = $A.get("e.force:showToast");
		toastEvent.setParams({
			mode: mode,
			title: title,
			message: message,
			type: type
		});
		toastEvent.fire();
	},

	redirectToPage: function (component, event, helper) {
		var urlEvent = $A.get("e.force:navigateToURL");
		var opportunityId = component.get("v.recordId");
		urlEvent.setParams({
			url: "/apex/OpportunityClosedWon?Id=" + opportunityId,
			isredirect: "true"
		});
		urlEvent.fire();
	}
});