/**
 * Created by nikola.culej on 28.5.2020..
 */

({
	handleCloseTask: function (cmp, event, helper) {
		helper.closeTask(cmp, event);
		$A.get('e.force:refreshView').fire();
	}
});
