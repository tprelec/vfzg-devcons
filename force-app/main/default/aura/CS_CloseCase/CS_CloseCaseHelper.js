({
    successMessage : function(successMessage) {
        iziToast.success({
            title: 'Success',
            message: successMessage,
            position: 'topCenter',
            progressBar: false
        });
    },
    errorMessage : function(messageError) {
        iziToast.error({
            title: 'Error',
            message: messageError,
            position: 'topCenter',
            progressBar: false
        });
    }
});