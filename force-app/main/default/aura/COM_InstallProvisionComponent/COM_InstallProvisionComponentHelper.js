({
    getColumns : function (cmp, event){
        var taskRecordType = cmp.get('v.TaskRecordType');
        var taskType = cmp.get('v.taskType');
        
        var columnNames = [];

        if (taskType === 'COM_Provisioning') {
            columnNames = ["Product Name", "CDO Service ID", "Component Name", "Article Name", "Provisioning Code", "Previous Provisioning Code", "MACD Action"];
        } else if (taskType === 'COM_Installation' || taskType === 'COM_Installation Jeopardy' || taskType === 'COM_Returnbox') {
            columnNames = ["Product Name", "Component Name", "Article Name", "Provisioning Code", "Previous Provisioning Code", "MACD Action", "CDO Service ID", "Implemented Date"];
        }

        cmp.set("v.columns", columnNames);
    }
});