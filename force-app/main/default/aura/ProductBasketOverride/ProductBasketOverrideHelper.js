({
    checkPrivileges : function(component) {
        component.set("v.spinner", true);
        var parentId = component.get("v.parentRecordId");
        var action = component.get("c.checkUserAccess");
        action.setParams({ parentId: parentId  });
        action.setCallback(this, function(data) {

        component.set("v.userAccess", data.getReturnValue());
        if (data.getReturnValue() !== null) {
            if (data.getReturnValue() !== '' && data.getReturnValue() !== 'oracle') {

                var urlEvent = $A.get("e.force:navigateToURL");
                urlEvent.setParams({
                    "url": "/apex/csbb__basketbuilderapp?id="+data.getReturnValue(),
                    "isredirect": "true"
                });
                urlEvent.fire();
            }
            component.set("v.spinner", false);
        }
        });
        $A.enqueueAction(action);
    },
    getParameterByName: function(component, event, name) {
        name = name.replace(/[\[\]]/g, "\\$&");
        var url = window.location.href;
        var regex = new RegExp("[?&]" + name + "(=1\.([^&#]*)|&|#|$)");
        var results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
})