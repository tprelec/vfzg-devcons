({
	doInit: function (cmp) {
		try {
			var action = cmp.get('c.getContactFields');

			var contactId = cmp.get('v.contactId') ? cmp.get('v.contactId') : null;
			action.setParam('contactId', contactId);

			action.setCallback(this, function (response) {
				var state = response.getState();
				if (state === 'SUCCESS' && cmp.isValid()) {
					var fields = response.getReturnValue();

					for (var i = 0; i < fields.length; i++) {
						var field = fields[i];
						if (field.name === 'Salutation') {
							cmp.set('v.salutation', field);
						} else if (field.name === 'Gender__c') {
							cmp.set('v.gender', field);
						} else if (field.name === 'RecordTypeId') {
							cmp.set('v.recordTypeId', field.value);
						}
					}

					cmp.set('v.modalSpinner', false);
				} else if (state === 'INCOMPLETE') {
					console.error('Network error');
				} else if (state === 'ERROR') {
					var errors = response.getError();
					if (errors) {
						if (errors[0] && errors[0].message) {
							console.error('Error message: ' + errors[0].message);
						}
					} else {
						console.error('Unknown error');
					}
				}
			});

			cmp.set('v.modalSpinner', true);
			$A.enqueueAction(action);
		} catch (exc) {
			console.error(exc);
		}
	},

	validation: function (fields) {
		var validationInfo = { status: true, messages: [] };

		if (!fields.FirstName) {
			validationInfo.status = false;
			validationInfo.messages.push('Missing First Name');
		}
		if (!fields.LastName) {
			validationInfo.status = false;
			validationInfo.messages.push('Missing Last Name');
		}
		if (!fields.Email) {
			validationInfo.status = false;
			validationInfo.messages.push('Missing Email');
		}
		if (!fields.MobilePhone) {
			validationInfo.status = false;
			validationInfo.messages.push('Missing Mobile Phone');
		}
		if (!fields.Gender__c) {
			validationInfo.status = false;
			validationInfo.messages.push('Missing Gender');
		}

		return validationInfo;
	},

	doSubmit: function (cmp, submitEvent) {
		var fields = submitEvent.getParam('fields');
		var validationInfo = this.validation(fields);
		var owner = fields.OwnerId === cmp.get('v.userId');
		if (validationInfo.status) {
			if (owner) {
				if (cmp.get('v.salutation').value !== fields.Salutation) {
					if (fields.Salutation === 'de heer') {
						fields.Gender__c = 'Male';
					} else if (fields.Salutation === 'mevrouw') {
						fields.Gender__c = 'Female';
					}
				}
				if (cmp.get('v.gender').value !== fields.Gender__c) {
					if (fields.Gender__c === 'Male') {
						fields.Salutation = 'de heer';
					} else if (fields.Gender__c === 'Female') {
						fields.Salutation = 'mevrouw';
					}
				}
			} else {
				var salutation = cmp.get('v.salutation') ? cmp.get('v.salutation').value : null;
				if (salutation) {
					fields.Salutation = salutation;
				}

				var gender = cmp.get('v.gender') ? cmp.get('v.gender').value : null;
				if (gender) {
					fields.Gender__c = gender;
				}
			}

			submitEvent.setParam('fields', fields);
		} else {
			this.toastMessage('Error!', validationInfo.messages.join(';'), 'error');
			$A.enqueueAction(cmp.get('c.handleClose'));
		}
	},
	toastMessage: function (title, message, type) {
		var toastEvent = $A.get('e.force:showToast');
		toastEvent.setParams({
			title: title,
			message: message,
			type: type
		});
		toastEvent.fire();
	}
});
