({
	onInit: function (cmp, event, helper) {
		helper.doInit(cmp);
	},

	handleSubmit: function (cmp, event, helper) {
		helper.doSubmit(cmp, event);
	},

	handleSuccess: function (cmp, event, helper) {
		var isNew = !cmp.get("v.contactId");
		var response = event.getParam("response");
		var contactId = response.id;
		//cmp.set("v.contactId", response.id);

		cmp.set("v.modalSpinner", false);

		var modalEvent = cmp.getEvent("contactEditFormModalEvent");
		modalEvent.setParams({
			eventName: "save",
			contactId: contactId,
			isNew: isNew
		});
		modalEvent.fire();
	},

	handleError: function (cmp, event, helper) {
		var error = event.getParam("error");
		console.error(error);
		cmp.set("v.modalSpinner", false);
	},

	handleOnload: function (cmp, event, helper) {
		cmp.set("v.modalSpinner", false);
	},

	handleClose: function (cmp, event, helper) {
		cmp.set("v.modalSpinner", false);
		var modalEvent = cmp.getEvent("contactEditFormModalEvent");
		modalEvent.setParams({
			eventName: "close"
		});
		modalEvent.fire();
	}
});