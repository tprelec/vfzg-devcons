({
	redirectTo: function (component, event, helper) {
		var communityUser = component.get('c.isCommunity');
		communityUser.setCallback(this, function (response) {
			var isCommunity = response.getReturnValue();
			var pageRef;

			if (isCommunity) {
				pageRef = {
					type: 'comm__namedPage',
					attributes: {
						name: 'Order_Entry__c'
					},
					state: {
						c__oppId: component.get('v.oppId')
					}
				};
			} else {
				pageRef = {
					type: 'standard__component',
					attributes: {
						componentName: 'c__OrderEntry'
					},
					state: {
						c__oppId: component.get('v.oppId')
					}
				};
			}

			var navService = component.find('navService');
			navService.navigate(pageRef);
		});
		$A.enqueueAction(communityUser);
	}
});
